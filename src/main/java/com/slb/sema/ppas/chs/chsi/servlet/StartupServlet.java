////////////////////////////////////////////////////////////////////////////////
//
//   FILE NAME       :       StartupServlet.Java
//   DATE            :       18 May 2006
//   AUTHOR          :       Kanta Goswami
//   REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//   COPYRIGHT       :       WM-data 2006
//
//   DESCRIPTION     :       Startup servlet for CHSI.
//
////////////////////////////////////////////////////////////////////////////////
//   CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY  | <name>     | <brief description of           | <reference>
//           |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.servlet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcConnectionPool;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.HttpSessionsTable;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.is.isc.IsClient;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Start up servlet for CHSI. Servlet engines prior to servlet 2.3 had no web application startup process,
 * instead the typical solution was to implement a servlet which is instantiated first and never mapped (i.e.
 * it is initialised before all other servlets and never has requets passed to it - it's only job is to
 * initialise the web application). As we are now using servlet 2.3, it is likely this servlet will be
 * superseeded with a proper web application start up event and so it will be removed.
 */
public class StartupServlet extends PpasServlet
{

    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "StartupServlet";

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */
    public StartupServlet()
    {
        super();
        

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME,
                           10090,
                           this,
                           "Constructed " + C_CLASS_NAME);
        }
        return;

    } // End of constructor StartupServlet()

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";

    /**
     * Initialises the CHSI web application. This includes loading configuration, creating log file, creating
     * and starting JDBC pool and PAMI pool, loading business config cache,...
     * @throws PpasException If an exception occurs during startup - CHSI application is not available.
     */
    protected void doInit() throws PpasException
    {

        ServletContext l_servletContext;
        ChsiContext l_chsiContext;
        PpasProperties l_configProperties;
        PpasSession l_ppasSession = null;
        PpasRequest l_ppasRequest = null;
        PpasException l_ppasException;
        BusinessConfigCache l_businessConfigCache;
        JdbcConnection l_connection = null;
        JdbcConnectionPool l_connectionPool = null;
        PpasLoggerPool l_loggerPool = null;
        Logger l_logger = null;
        InstrumentManager l_instrumentManager;
        IsClient l_isClient = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                           WebDebug.C_ST_START,
                           C_CLASS_NAME,
                           11100,
                           this,
                           "ENTERED " + C_METHOD_doInit);
        }

        l_servletContext = getServletContext();
        
        //  Create PpasRequest from l_ppasSession.
        l_ppasRequest = new PpasRequest(l_ppasSession);
        try
        {
            l_configProperties = new PpasProperties(null);
            l_configProperties.loadLayeredProperties();
        }
        catch (PpasConfigException l_configEx)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                               WebDebug.C_ST_ERROR,
                               C_CLASS_NAME,
                               11105,
                               this,
                               "Fatal error starting CHSI application. Failed to load layered configuration property "
                                       + "files. " + l_configEx);
            }
            l_ppasException = new PpasConfigException(C_CLASS_NAME,
                                                      C_METHOD_doInit,
                                                      10347,
                                                      this,
                                                      l_ppasRequest,
                                                      0,
                                                      ConfigKey.get().errorReading("chsiconfig.properties"),
                                                      l_configEx);

            //
            // Can't log (logging not initialised yet), so write to System.err.
            //
            System.err.println("FATAL ERROR STARTING CHSI APPLICATION : ");
            l_ppasException.printStackTrace(System.err);

            // If a previous system initialisation error has not already
            // occured, then store this one so that it can later by presented
            // as a sensible response message.
            if (PpasServlet.i_sysInitE == null)
            {
                PpasServlet.i_sysInitE = l_ppasException;
            }

            throw (l_ppasException);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                           WebDebug.C_ST_TRACE,
                           C_CLASS_NAME,
                           11107,
                           this,
                           "Loaded chsiconfig.properties");
        }
        WebDebug.setDebug(WebDebug.C_FLAG_SYSTEM_OVERRIDES, l_configProperties);

        // Initialise DatePatch class.
        // This enables it to be used, even though the datepatch offset
        // will not have yet been applied.
        // Do this now so that it can be used to set the name of the error log.
        DatePatch.init(C_CLASS_NAME, C_METHOD_doInit, 15556, this, l_ppasRequest, 0, null, //logger
                       l_configProperties);

        //
        // Create the application context wide logger pool.
        //
        try
        {
            l_loggerPool = new PpasLoggerPool("LoggerPool", l_configProperties, System
                    .getProperty("ascs.procName"));
            
        }
        catch (PpasConfigException l_configE)
        {
            //
            // Can't log (logging not initialised yet), so write to System.err.
            //
            System.err.println("FATAL ERROR STARTING CHSI APPLICATION : ");
            l_configE.printStackTrace(System.err);

            // If a previous system initialisation error has not already
            // occured, then store this one so that it can later by presented
            // as a sensible response message.
            if (PpasServlet.i_sysInitE == null)
            {
                PpasServlet.i_sysInitE = l_configE;
            }

            throw (l_configE);
        }

        l_logger = l_loggerPool.getLogger();

        if (l_logger == null)
        {
            // The requested logger could not be found ... create and throw
            // appropriate exception

            // Create and throw a 'missing configuration' exception
            PpasConfigException l_configE = new PpasConfigException(C_CLASS_NAME,
                                                                    C_METHOD_doInit,
                                                                    10420,
                                                                    this,
                                                                    l_ppasRequest,
                                                                    0,
                                                                    ConfigKey.get().badLogger("csrWebLogger",
                                                                                              "LoggerPool"));

            // Can't log (logging not initialised yet), so write to System.err.
            System.err.println("FATAL ERROR STARTING CHSI APPLICATION : ");
            l_configE.printStackTrace(System.err);

            // If a previous system initialisation error has not already
            // occured, then store this one so that it can later by presented
            // as a sensible response message.
            if (PpasServlet.i_sysInitE == null)
            {
                PpasServlet.i_sysInitE = l_configE;
            }
            throw (l_configE);
        }

        //
        // Create and populate a context.
        //
        l_chsiContext = new ChsiContext(l_ppasRequest , 0 ,l_logger);
        l_chsiContext.setAttribute("LoggerPool", l_loggerPool);

        try
        {
            l_chsiContext.init(l_ppasRequest, 0, l_configProperties, this.getClass().getName());

            l_instrumentManager = (InstrumentManager)l_chsiContext
                    .getAttribute("com.slb.sema.ppas.util.instrumentation.InstrumentManager");

            l_logger.setInstrumentManager(l_instrumentManager);
            l_configProperties.setInstrumentManager(l_instrumentManager);

            l_servletContext.setAttribute("com.slb.sema.ppas.chs.chsi.support.ChsiContext", l_chsiContext);

            //
            // Get a jdbc connection and use it to load up all the business
            // configuration data.
            //
            l_businessConfigCache = (BusinessConfigCache)l_chsiContext.getAttribute("BusinessConfigCache");
            l_connectionPool = (JdbcConnectionPool)l_chsiContext.getAttribute("JdbcConnectionPool");

            l_connection = l_connectionPool.getConnection(C_CLASS_NAME,
                                                          C_METHOD_doInit,
                                                          10123,
                                                          null,
                                                          l_ppasRequest,
                                                          0,
                                                          10000); // Wait at least 10 seconds for connection

            l_businessConfigCache.loadAll(l_ppasRequest, l_connection);
        }
        catch (PpasException l_ppasE)
        {
            // Store this one so that it can later by presented
            // as a sensible response message.
            if (PpasServlet.i_sysInitE == null)
            {
                PpasServlet.i_sysInitE = l_ppasE;
            }
            throw (l_ppasE);
        }
        finally
        {
            // Return connection back to pool.
            if (l_connection != null)
            {
                l_connectionPool.putConnection(0, l_connection);
            }
        }

        //
        // Create the application context wide HttpSessionsTable for managing
        // and monitoring sessions.
        //
        l_servletContext.setAttribute("HttpSessionsTable", new HttpSessionsTable(l_instrumentManager));
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                           WebDebug.C_ST_TRACE,
                           C_CLASS_NAME,
                           11110,
                           this,
                           "Created HttpSessionsTable");
        }

        l_isClient = new IsClient(l_logger, l_configProperties, l_instrumentManager);

        l_chsiContext.setAttribute("IsClient", l_isClient);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                           WebDebug.C_ST_END,
                           C_CLASS_NAME,
                           11200,
                           this,
                           "LEAVING " + C_METHOD_doInit);
        }
    } // End of method doInit()

    /**
     * Service method to control requests and responses to the JSP.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     */
    public void service(HttpServletRequest p_request, HttpServletResponse p_response)
    {

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST | WebDebug.C_ST_ERROR,
                           C_CLASS_NAME,
                           11500,
                           this,
                           "SHOULD NOT HAPPEN - entered service method of " + " StartupServlet.");
        }

        // <<Log error here!!!!>>
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST | WebDebug.C_ST_ERROR,
                           C_CLASS_NAME,
                           11500,
                           this,
                           "SHOULD NOT HAPPEN - leaving service method of " + " StartupServlet.");
        }
        return;
    } // End of private method service(HttpServletRequest, HttpServletResponse)

} // End of public class StartupServlet
