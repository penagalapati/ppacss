////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       ChsiContext.java
//  DATE            :       09-May-2006
//  AUTHOR          :       Kanta Goswami
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       Startup code for Chsi.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+--------------------
//           |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.support;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.util.logging.Logger;

/** Startup code for Chsi. */
public class ChsiContext extends PpasContext
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiContext";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a ChsiContext object.
     * @param p_request The request being processed.
     * @param p_flags General process flags (not used).
     * @param p_logger The logger to direct messages.
     */
    public ChsiContext(PpasRequest p_request, long p_flags, Logger p_logger)
    {
        super(p_logger);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_INIT,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_INIT,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME,
                           10090,
                           this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    /**
     * Populates the context with all the required attributes to run Chsi.
     * @param p_ppasRequest The request being processed.
     * @param p_flags General process flags (not used).
     * @param p_configProperties Configuration.
     * @param p_remoteInstrumentClassName The full class name that will be used as part of the binding name
     * for the remote instrument manager. Normally this will be the name of the calling class, which can be
     * obtained using "this.getClass().getName()". If a remote instrument manager is not used (as controlled
     * by flags), then this can be null.
     * @throws PpasException If an error occurs in the super class.
     */
    public void init(PpasRequest p_ppasRequest,
                     long p_flags,
                     PpasProperties p_configProperties,
                     String p_remoteInstrumentClassName) throws PpasException
    {
        String l_verifyLoginStr;
        Boolean l_verifyLoginBool;
        String l_defaultBankCode;
        String l_defaultPaymentAddress1;
        String l_defaultPaymentAddress2;
        String l_defaultPaymentAddress3;
        String l_defaultPostalCode;
        String l_maxInactiveTime;
        PpasConfigException l_pCE;

        super.init(p_ppasRequest, 0, p_configProperties, p_remoteInstrumentClassName);

        System.out.println("In ChsiContext init" );
        l_maxInactiveTime = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.maxInactiveInterval", "1800");

        try
        {
            i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.maxInactiveInterval", Integer
                    .valueOf(l_maxInactiveTime));
        }
        catch (NumberFormatException l_e)
        {
            l_pCE = new PpasConfigException(C_CLASS_NAME, "init", 20015, this, p_ppasRequest, 0, ConfigKey
                    .get().badConfigValue("com.slb.sema.ppas.chs.chsi.support.ChsiContext.maxInactiveInterval",
                                          l_maxInactiveTime,
                                          "numeric"), l_e);

            i_logger.logMessage(l_pCE);

            throw l_pCE;
        }

        // Get the value of verifyLogin from the properties file.
        // If this is set to true we will force all clients to log in.
        // If it is set to false clients can use any available service
        // without logging in. If no value is found in the properties file we
        // will default to true.

        // PpaLon#1570/6641 [BEGIN]
        l_verifyLoginStr = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.verifyLogin", "true");
        // PpaLon#1570/6641 [END]

        // Unless the value is set to false then we want the login
        // service to be used. So default to true.
        if (l_verifyLoginStr.equalsIgnoreCase("false")) // PpaLon#1570/6641
        {
            l_verifyLoginBool = Boolean.FALSE;
        }
        else
        {
            l_verifyLoginBool = Boolean.TRUE;
        }

        // PpaLon#1570/6641 [BEGIN]
        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.verifyLogin", l_verifyLoginBool);
        // PpaLon#1570/6641 [END]

        //
        // Get configuration data for defaults used by Payment service.
        //

        // PpaLon#1570/6641 [BEGIN]
        //   Change package identifiers for defaults to use in payment service

        l_defaultBankCode = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultBankCode", "1");

        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultBankCode", l_defaultBankCode);

        l_defaultPaymentAddress1 = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress1", "");

        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress1",
                         l_defaultPaymentAddress1);

        l_defaultPaymentAddress2 = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress2", "");

        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress2",
                         l_defaultPaymentAddress2);

        l_defaultPaymentAddress3 = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress3", "");
        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPaymentAddress3",
                         l_defaultPaymentAddress3);

        l_defaultPostalCode = p_configProperties
                .getTrimmedProperty("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPostalCode", "");

        i_attributes.put("com.slb.sema.ppas.chs.chsi.support.ChsiContext.defaultPostalCode", l_defaultPostalCode);
    }
}