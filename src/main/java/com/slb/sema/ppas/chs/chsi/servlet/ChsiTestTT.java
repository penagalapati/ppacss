////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       ChsiTestTT.java
//  DATE            :       11-May-2006
//  AUTHOR          :       Kanta Goswami
//  REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//  DD/MM/YY | <name>     | <brief description of           | <reference>
//           |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.AssertionFailedError;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;

/**
 * Super class for CHSI automated test classes.
 */
public class ChsiTestTT extends CommonTestCaseTT
{
    /** Name of class for use in middleware. Value is {@value}. */
    private static final String      C_CLASS_NAME                    = "ChsiTestTT";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** Error handler which extends <code>DefaultHandler</code>. */
    protected ValidatingErrorHandler i_errorHandler                  = null;

    /**
     * Current <code>WebConversation</code>. This manages sessions and enabled us to send requests to CHSI.
     */
    protected WebConversation        i_webConv                       = new WebConversation();

    /** Next available MSISDN, as a string in the correct format for CHSI. */
    protected String                 i_nextMsisdn                    = null;

    /**
     * <code>DOMParser</code> to parse the XML, allowing validation against the schema also allowing us to
     * get the <code>Document</code> object containing the response, in order to extract element values from
     * the XML.
     */
    protected DOMParser              i_domParser                     = null;

    /** Array used to store whether a feature licence has been set during a test. */
    protected boolean                i_licSetArr[]                   = new boolean[5];

    //-------------------------------------------------------------------------
    // Class level attributes
    //-------------------------------------------------------------------------
    /** CHSI http port. */
    protected static int             c_chsiHttpPort                  = 0;

    /** CHSI host. */
    protected static String          c_chsiHostName                  = null;

    /** CHSI url prefix e.g. "http://holmes:2008/" */
    private static String            c_urlPrefix                     = null;

    /**
     * Instance of <code>DbServiceTT</code> to allow us to get the mext available MSISDN for installation.
     */
    protected static DbServiceTT     c_dbServiceTT                   = null;

    /** Has all the static data been initialised? */
    private static boolean           c_initialised                   = false;
    
    /** Get the response in a formatted manner. */
    protected String i_formattedResponse;

    //-------------------------------------------------------------------------
    // Class constants
    //-------------------------------------------------------------------------
    

    /**
     * CHSI test tool class.
     * @param p_name the test name
     */
    public ChsiTestTT(String p_name)
    {
        super(p_name, "CHS_SRV_A1001");

        i_domParser = new DOMParser();

        try
        {
            i_domParser.setFeature("http://xml.org/sax/features/validation", true);
            
            i_domParser.setFeature("http://apache.org/xml/features/validation/schema", true);
        }
        catch (SAXNotRecognizedException e)
        {
            failedTestException(e);
        }
        catch (SAXNotSupportedException e)
        {
            failedTestException(e);
        }

        if (!c_initialised)
        {
            try
            {
                c_chsiHttpPort = Integer.parseInt(c_properties
                                                  .getPortNumber("com.slb.sema.ppas.webFileTokenReplace.XX_HTTP_PORT/port"));
                c_chsiHostName = System.getProperty("ascs.chsHost", "localhost");
                c_urlPrefix = "http://" + c_chsiHostName + ":" + c_chsiHttpPort + "/";

                BusinessConfigCache l_bCC = (BusinessConfigCache)c_ppasContext
                        .getAttribute("BusinessConfigCache");

                JdbcConnection l_conn = getConnection(C_CLASS_NAME, "Constructor", 10100);

                try
                {
                    l_bCC.loadAll(c_ppasRequest, l_conn);
                }
                finally
                {
                    putConnection(l_conn);
                }

                if (c_dbServiceTT == null)
                {
                    c_dbServiceTT = new DbServiceTT(c_ppasRequest, c_logger);
                    setDbServicable(c_dbServiceTT);
                }
            }
            catch (NumberFormatException e)
            {
                failedTestException(e);
            }
            catch (PpasConfigException e)
            {
                failedTestException(e);
            }
            catch (PpasSqlException e)
            {
                failedTestException(e);
            }

            c_initialised = true;
        }

        c_writeUtText = true; // Always need CHSI output.
    }

    /**
     * Set up method. Calls <code>login</code> and gets the next free msisdn. There are now 2 versions of
     * the login method, for versions 2 and 3 of CHSI The default call is set to v3 here
     */

    public void setUp()
    {
        loginV3();

        Msisdn l_nextMsisdn = null;

        try
        {
            l_nextMsisdn = c_dbServiceTT.getNextFreeMsisdn(new Market(1, 2), "00");
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        catch (PpasServiceException e)
        {
            failedTestException(e);
        }

        i_nextMsisdn = c_ppasContext.getMsisdnFormatInput().format(l_nextMsisdn);
    }

    /**
     * Sends a LOGIN request to CHSI version 3, storing the session id (the cookie) in the
     * <code>WebConversation</code> and allowing further requests to be sent.
     */
    protected void loginV3()
    {
        try
        {
            i_webConv.getResponse(c_urlPrefix
                    + "chs/chsi/Session?command=LOGIN&userName=SUPER&password=SEMAUK&version=1");
        }
        catch (MalformedURLException e)
        {
            failedTestException(e);
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
    }

    /**
     * Sends a LOGIN request to CHSI version 2, storing the session id (the cookie) in the
     * <code>WebConversation</code> and allowing further requests to be sent.
     */
    protected void loginV2()
    {
        try 
        {
            i_webConv.getResponse(c_urlPrefix
                    + "chs/chsi/Session?command=LOGIN&userName=SUPER&password=SEMAUK&version=2");
        }
        catch (MalformedURLException e)
        {
            failedTestException(e);
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
    }    

    /** Extract XML fields from default XML encoding.
    *
    * @param p_fieldName XML tag to extract.
    * @return Array of fields matching the tag.
    */
   public String[] getFields(String p_fieldName)
   {
       return getFields(p_fieldName, i_formattedResponse);
   }
   
   /** Extract XML fields from a specified piece of XML.
   *
   * @param p_fieldName XML tag to extract.
   * @param p_xml       Extract of XML to be parsed.
   * @return Array of fields matching the tag.
   */
  public String[] getFields(String p_fieldName, String p_xml)
  {
      Pattern l_pattern = Pattern.compile("<" + p_fieldName + ">(.*?)</" + p_fieldName + ">",
                                          Pattern.DOTALL);
      
      Matcher l_matcher = l_pattern.matcher(p_xml);

      Vector l_vector = new Vector();

      while(l_matcher.find())
      {
          l_vector.add(l_matcher.group(1));
      }

      String[] l_return = (String[])l_vector.toArray(new String [l_vector.size()]);

      return l_return;
  }
  
    /** Check a field has a specified value.
     * 
     * @param p_fieldName     Name of tag.
     * @param p_expectedValue Expected value.
     */
    public void checkFieldValue(String p_fieldName, String p_expectedValue)
    {
        checkFieldValue(p_fieldName, p_expectedValue, i_formattedResponse);
    }
    
    /** Check a field has a specified value in a specified extract of XML.
     * 
     * @param p_fieldName     Name of tag.
    * @param p_xml       Extract of XML to be parsed.
     * @param p_expectedValue Expected value.
     */
    public void checkFieldValue(String p_fieldName, String p_expectedValue, String p_xml)
    {
        String[] l_actual = getFields(p_fieldName, p_xml);
        
        assertEquals("Incorrect number of fields", 1, l_actual.length);
        assertEquals("Incorrect value", p_expectedValue, l_actual[0]);
    }
    
    /** Check a field has the correct number of fields.
     * 
     * @param p_fieldName     Name of tag.
     * @param p_expectedValue Expected value.
     */
    public void checkFieldCount(String p_fieldName, int p_expectedValue)
    {
        checkFieldCount(p_fieldName, p_expectedValue, i_formattedResponse);
    }
    
    /** Check a field has the correct number of fields in a specified piece of XML.
     * 
     * @param p_fieldName     Name of tag.
    * @param p_xml       Extract of XML to be parsed.
     * @param p_expectedValue Expected value.
     */
    public void checkFieldCount(String p_fieldName, int p_expectedValue, String p_xml)
    {
        String[] l_actual = getFields(p_fieldName, p_xml);
        
        assertEquals("Incorrect value", p_expectedValue, l_actual.length);
    }
    
    /**
     * Sends the request to CHSI, validates that the severity and status key in the response match the
     * expected values, and validates the the XML against the schema.
     * @param p_request the CHSI request, minus the http:// <code>host</code>:<code>port</code>/
     * section e.g. chs/chsi/AccountDetail?command=GET_ACCOUNT_DETAILS&msisdn=440832000500
     * @param p_statusSeverity the expected response severity
     * @param p_statusKey the expected response status key
     */    
    
    protected void sendRequest(String p_request, String p_statusSeverity, String p_statusKey)
    {
        sendRequest(p_request, p_statusSeverity, p_statusKey, true, true);
    }

    /**
     * Sends the request to CHSI, validates that the severity and status key in the response match the
     * expected values, and optionally validates the the XML against the schema.
     * @param p_request the CHSI request, minus the http:// <code>host</code>:<code>port</code>/
     * section e.g. chs/chsi/AccountDetail?command=GET_ACCOUNT_DETAILS&msisdn=440832000500
     * @param p_statusSeverity the expected response severity
     * @param p_statusKey the expected response status key
     * @param p_validateSchema True if the schema should be validated.
     */
    protected void sendRequest(String p_request,
                               String p_statusSeverity,
                               String p_statusKey,
                               boolean p_validateSchema)
    {
        sendRequest(p_request, p_statusSeverity, p_statusKey, null, p_validateSchema, true);
    }

    /**
     * Sends the request to CHSI, validates that the severity and status key in the response match the
     * expected values, and optionally validates the the XML against the schema.
     * @param p_request the CHSI request, minus the http:// <code>host</code>:<code>port</code>/
     * section e.g. chs/chsi/AccountDetail?command=GET_ACCOUNT_DETAILS&msisdn=440832000500
     * @param p_statusSeverity the expected response severity
     * @param p_statusKey the expected response status key
     * @param p_validateSchema True if the schema should be validated.
     * @param p_sleep Sleep for a second?
     */
    protected void sendRequest(String p_request,
                               String p_statusSeverity,
                               String p_statusKey,
                               boolean p_validateSchema,
                               boolean p_sleep)
    {
        sendRequest(p_request, p_statusSeverity, p_statusKey, null, p_validateSchema, p_sleep);
    }

    /**
     * Sends the request to CHSI, validates that the severity and status key in the response match the
     * expected values, and validates the the XML against the schema.
     * @param p_request the CHSI request, minus the http:// <code>host</code>:<code>port</code>/
     * section e.g. chs/chsi/AccountDetail?command=GET_ACCOUNT_DETAILS&msisdn=440832000500
     * @param p_statusSeverity the expected response severity
     * @param p_statusKey the expected response status key
     * @param p_message StringBuffer to which the failure message is appended before the failure exception is
     * thrown
     * @param p_validateSchema True if the schema should be validated.
     * @param p_sleep Sleep for a second?
     */
    private void sendRequest(String p_request,
                             String p_statusSeverity,
                             String p_statusKey,
                             StringBuffer p_message,
                             boolean p_validateSchema,
                             boolean p_sleep)
    {
        WebResponse l_webResponse = null;

        i_errorHandler = new ValidatingErrorHandler();
        i_domParser.setErrorHandler(i_errorHandler);

        try
        {
            System.out.println("Issue: " + p_request);
            l_webResponse = i_webConv.getResponse(c_urlPrefix + p_request);
            i_domParser.parse(new InputSource(l_webResponse.getInputStream()));
            i_formattedResponse = l_webResponse.getText();
            System.out.println(i_formattedResponse);
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }

        if (p_statusSeverity != null && p_statusKey != null)
        {
            validateSeverityAndKey(p_statusSeverity, p_statusKey, p_message, i_domParser);
        }

        if (p_validateSchema)
        {
            validateXmlAgainstSchema(p_message);
        }

        if (p_sleep)
        {
            snooze(1); // Ensure action is complete
        }
    }

    /**
     * Perform the tests that check the response severities and status keys that are returned when errors
     * occur on the Ericsson nodes. This is done by setting the response code in the emulator. The request
     * passed to this method should pass all the validation that is carried out within ISIL, allowing the
     * request to be sent to the network and the error to be returned.
     * @param p_request a CHSI request (@see #sendRequest) - this request must pass the ISIL validation
     * @param p_emulatorConfig the name of the emulator class that we need to call the remote methods on e.g.
     * com.slb.sema.ppas.emulation.service.PaymentService
     * @param p_vector a vector containing <code>EsiTestCriteria</code> objects
     */
    protected void performEsiTests(String p_request, String[] p_emulatorConfig, Vector p_vector)
    {
        say("Performing ESI tests.");

        StringBuffer l_message = new StringBuffer(200);
        int l_failures = 0;

        try
        {
            for (int i = 0; i < p_vector.size(); i++)
            {
                EsiTestCriteria l_test = (EsiTestCriteria)p_vector.get(i);

                setResponseCode(p_emulatorConfig, l_test.getErrorCode());

                try
                {
                    sendRequest(p_request, l_test.getSeverity(), l_test.getKey(), l_message, true, true);
                }
                catch (AssertionFailedError e)
                {
                    l_failures++;
                }
            }

            say(l_message.toString());

            if (l_failures > 0)
            {
                fail(l_message.toString());
            }
        }
        finally
        {
            setResponseCode(p_emulatorConfig, "0");
        }
    }

    /**
     * Validate severity and key.
     * @param p_expectedSeverity Expected severity.
     * @param p_expectedKey Expected key.
     * @param p_message Message to display.
     * @param p_domParser XML Parser.
     */
    private void validateSeverityAndKey(String p_expectedSeverity,
                                        String p_expectedKey,
                                        StringBuffer p_message,
                                        DOMParser p_domParser)
    {
        String l_actualSeverity = getStatusSeverity(p_domParser.getDocument());
        String l_actualKey = getStatusKey(p_domParser.getDocument());
        String l_errorString = "expected severity: " + p_expectedSeverity + ", actual severity: "
                + l_actualSeverity + ", expected key: " + p_expectedKey + ", actual key: " + l_actualKey;

        if (p_message != null)
        {
            p_message.append(l_errorString);
        }

        if (!p_expectedSeverity.equals(l_actualSeverity) || !p_expectedKey.equals(l_actualKey))
        {
            if (p_message != null)
            {
                p_message.append(": Failed\n");
            }

            fail(l_errorString);
        }

        if (p_message != null)
        {
            p_message.append(": Passed\n");
        }
    }

    /**
     * Validate XML against the schema.
     * @param p_message Message to which error description should be added.
     */
    private void validateXmlAgainstSchema(StringBuffer p_message)
    {

        int l_errorCount = i_errorHandler.getErrorCount();
        String l_errorString;

        if (l_errorCount > 0)
        {
            l_errorString = l_errorCount + " validation errors occured:\n" + i_errorHandler.getErrorText();

            if (p_message != null)
            {
                p_message.append(l_errorString + "\n");
            }

            fail(l_errorString);
        }
    }

    /**
     * Get severity of the status.
     * @param p_document CHSI response.
     * @return Status severity.
     */
    private String getStatusSeverity(Node p_document)
    {
        return getValueFromXml(p_document, "po:StatusSeverity");
    }

    /**
     * Get key of the status.
     * @param p_document CHSI response.
     * @return Status key.
     */
    private String getStatusKey(Node p_document)
    {
        return getValueFromXml(p_document, "po:StatusKey");
    }

    /**
     * Get value from the response.
     * @param p_key Key in the CHSI response.
     * @return First value associated with the key.
     */
    protected String getResponseValue(String p_key)
    {
        return getValueFromXml(i_domParser.getDocument(), p_key);
    }

    /**
     * Get specified value from the response.
     * @param p_document CHSI response.
     * @param p_name Key of the value.
     * @return Value as string.
     */
    private String getValueFromXml(Node p_document, String p_name)
    {
        boolean l_found = false;
        String l_return = null;

        int type = p_document.getNodeType();

        // check if element

        if (type == Node.ELEMENT_NODE)
        {
            //System.out.println ("Element: " + p_document.getNodeName() );

            if (p_document.getNodeName().equals(p_name))
            {
                l_return = p_document.getChildNodes().item(0).getNodeValue();
                l_found = true;
            }

        }

        // check if current node has any children
        NodeList children = p_document.getChildNodes();
        if (children != null && !l_found)
        {
            // if it does, iterate through the collection
            for (int i = 0; i < children.getLength() && !l_found; i++)
            {
                // recursively call function to proceed to next level
                l_return = getValueFromXml(children.item(i), p_name);
                if (l_return != null)
                {
                    l_found = true;
                }
            }
        }

        return l_return;
    }

    /** Set response code in emulator.
     * 
     * @param p_emulationConfig Emulation configuration.
     * @param p_responseCode    Desired response cpde.
     */
    private void setResponseCode(String[] p_emulationConfig, String p_responseCode)
    {
        setEmulatorReaction(p_emulationConfig[0], // class name
                            p_emulationConfig[1], // method name
                            p_responseCode);
    }   

    /** Class defining the ESI test criteria. */
    protected class EsiTestCriteria
    {
        /** Error code. */
        private int    i_errorCode      = 0;

        /** Severity of the status. */
        private String i_statusSeverity = null;

        /** Key of the status. */
        private String i_statusKey      = null;

        /** Standard constructor.
         * 
         * @param p_errorCode      Error code.
         * @param p_statusSeverity Severity of the status.
         * @param p_statusKey      Key of the status.
         */
        protected EsiTestCriteria(int p_errorCode, String p_statusSeverity, String p_statusKey)
        {
            i_errorCode = p_errorCode;
            i_statusSeverity = p_statusSeverity;
            i_statusKey = p_statusKey;
        }

        /** Get the error code.
         * 
         * @return Error code.
         */
        private String getErrorCode()
        {
            return Integer.toString(i_errorCode);
        }

        /** Get the status severity.
         * 
         * @return STatus severity.
         */
        private String getSeverity()
        {
            return i_statusSeverity;
        }

        /** Get the status key.
         * 
         * @return Status key.
         */
        private String getKey()
        {
            return i_statusKey;
        }
    }

    /**
     * Private inner class which implements the parsing / validation error
     * handling behaviour. See class description for more details.
     * This class is NOT thread safe (but neither is a SAX parser)!
     */
    private class ValidatingErrorHandler extends DefaultHandler
    {

        /** Count of errors since start or last clear (reset). */
        private int          i_errorCount = 0;

        /** Error text. */
        private StringBuffer i_errorText  = new StringBuffer();

        /** Clear (reset) error count. */
        public void reset()
        {
            i_errorCount = 0;
            i_errorText = new StringBuffer();
        }

        /** Get error count since start or last clear (reset).
         * @return Number of errors.
         */
        public int getErrorCount()
        {
            return (i_errorCount);
        }

        /** Get the error text.
         * 
         * @return Error text.
         */
        public String getErrorText()
        {
            return i_errorText.toString();
        }

        /**
         * Handles parsing / validation errors - implementation for
         * ErrorHandler interface.
         * @param p_saxException The originating exception.
         */
        public void error(SAXParseException p_saxException)
        {
            i_errorCount++;
            i_errorText.append("PARSING ERROR : " + p_saxException + "\n");
        }

        /**
         * Handles parsing / validation warnings - implementation for
         * ErrorHandler interface.
         * @param p_saxException The originating exception.
         */
        public void warning(SAXParseException p_saxException)
        {
            i_errorCount++;
            i_errorText.append("PARSING WARNING : " + p_saxException + "\n");
        }

        /**
         * Handles parsing / validation fatal errors - implementation for
         * ErrorHandler interface.
         * @param p_saxException The originating exception.
         */
        public void fatalError(SAXParseException p_saxException)
        {
            i_errorCount++;
            i_errorText.append("PARSING FATAL ERROR : " + p_saxException + "\n");
        }
    }
}
