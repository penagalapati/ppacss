////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiSessionServlet.Java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Handles all Session-based activities (eg
//                              login/logout/keepalive).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 26/03/07 | K Bond     | Use locale GB not UK.           | PpacLon#1867/11221
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.servlet;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.service.ChsiSessionService;
import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.chs.chsi.support.ChsiSession;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasRuntimeException;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.MultiTimer;

/**
 * This servlet calls methods on the ChsiSessionService class to carry
 * out Session related activities (eg login/logout).
 */
public class ChsiSessionServlet extends ChsiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiSessionServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** The Service to be used by this servlet. */
    private ChsiSessionService i_chsiSessionService = null;
    
    /** Total number of Keep Alive requests received by posi session. */
    protected CounterInstrument     i_keepaliveReceivedCounterInstrument; 
    
    /** Total number of Keep Alive requests processed by posi session. */
    protected CounterInstrument     i_keepaliveProcessedCounterInstrument; 

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */
    public ChsiSessionServlet()
    {        
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing ChsiSessionServlet()");
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10090, this,
                "Constructed ChsiSessionServlet()");
        }
        return;
    }

    //-------------------------------------------------------------------------
    // Overridden Superclass Methods
    //-------------------------------------------------------------------------
    /** Overrides superclass service method to allow us to deal with login
     *  requests.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     * @throws javax.servlet.ServletException If an error occurs in the servlet.
     * @throws java.io.IOException If an I/O error occurs.
     */
    public void service(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response)
        throws
            javax.servlet.ServletException,
            java.io.IOException
    {
        String        l_forwardUrl = null;    
        ChsiRequest   l_chsiRequest = new ChsiRequest (null);
        ChsiResponse  l_chsiResponse = null;
        ChsiSession   l_chsiSession = null;
        String        l_command = "";
        String        l_userName = "";
        String        l_password = "";
        String        l_chsiVersion = null;
        int           l_timerId;
        MultiTimer    l_timer = null;
        ChsiContext   l_chsiContext;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                C_CLASS_NAME, 11200, null,
                "Servlet path is [" + p_request.getServletPath() + "]");
        }

        Exception l_initE = getInitException();
        
        if (l_initE != null)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_MODERATE,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    C_CLASS_NAME, 11200, null,
                    "Servlet Exception is " + l_initE.getMessage());
            }
            // Reset the URL to forward to the general JSP page for handling
            // a response with no service specific data.
            
            
            l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

            String l_msgText = "";
            if (l_initE instanceof PpasException)
            {
                l_msgText = ((PpasException)l_initE).getMsgText (Locale.UK);
            }
            else if (l_initE instanceof PpasRuntimeException)
            {
                l_msgText = ((PpasRuntimeException)l_initE).getMsgText (Locale.UK);
            }
            else
            {
                l_msgText = l_initE.toString();
            }

            l_chsiResponse = new ChsiResponse(
                                 l_chsiRequest,
                                 new Locale("en","GB"),
                                 ChsiResponse.C_KEY_SYS_INIT_FAILURE,
                                 new Object [] {l_msgText},
                                 ChsiResponse.C_SEVERITY_FAILURE);

            p_request.setAttribute("p_chsiRequest", l_chsiRequest);

            l_chsiContext = new ChsiContext (l_chsiRequest, 0, i_logger);
            p_request.setAttribute("p_chsiContext", l_chsiContext);

            // Store the response in the request
            p_request.setAttribute("p_chsiResponse", l_chsiResponse);
        }
        else
        {
            try
            {
                l_timer = i_timerSet.getNewTimer();
                l_timerId = l_timer.start(i_timerSetRegisteredNameId);
                p_request.setAttribute ("com.slb.sema.ppas.util.MultiTimer", l_timer);

                // Increment requests received counter.
                i_requestsReceivedCounterInstrument.increment();

                // Forward to the NoServiceDataXML.jsp.
                l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

                // Create a ChsiRequest object and do related processing...
                l_chsiRequest = createChsiRequest(p_request, p_response, l_timer);

                // Construct ChsiSessionService if it does not already exist.
                if (i_chsiSessionService == null)
                {
                    i_chsiSessionService = new ChsiSessionService(l_chsiRequest, i_logger, i_chsiContext);
                }

                getDefaultParams(p_request,
                                 l_chsiRequest);

                l_chsiSession = l_chsiRequest.getChsiSession();

                // If the login service is turned off just return an XML message
                // telling the client that there is no need to log in.
                if (!i_verifyLogin)
                {
                    // if login is disabled, default the version to 2
                    l_chsiRequest.setChsiVersion(ChsiSession.C_CHSI_VERSION_1);
                    
                    l_chsiResponse = new ChsiResponse
                            (l_chsiRequest,
                             l_chsiRequest.getChsiSession().getSelectedLocale(),
                             ChsiResponse.C_KEY_NO_LOGIN_REQUIRED,
                             (Object[])null,
                             ChsiResponse.C_SEVERITY_SUCCESS);
                }
                else
                {
                    // Get the command from the http request
                    l_command = getValidParam(l_chsiRequest,
                                              0,
                                              p_request,
                                              "command",
                                              true,  // command is mandatory ...
                                              "",    // so there is no default
                                             new String [] {"LOGIN", "LOGOUT","KEEPALIVE"});
                    if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
                    {
                        if (i_instrumentSet.i_isEnabled)
                        {
                            if (l_command.equals("KEEPALIVE"))
                            {
                                i_keepaliveReceivedCounterInstrument.increment();
                            }
                            else
                            {
                                i_requestsReceivedCounterInstrument.increment();
                            }
                        }
                    }

                    if ((l_command.equals("LOGIN")) &&
                        !l_chsiSession.isLoggedIn())
                    {
                        // Get the userName and password parameters...
                        l_userName = getValidParam(l_chsiRequest,
                                                   0,
                                                   p_request,
                                                   "userName",
                                                   true,
                                                   "",
                                                   PpasServlet.C_TYPE_ANY,
                                                   8,
                                                   PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

                        l_password = getValidParam(
                                   l_chsiRequest,
                                   0,
                                   p_request,
                                   "password",
                                   true,
                                   "",
                                   PpasServlet.C_TYPE_ANY,
                                   20,
                                   PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

                        
                        l_chsiVersion = getValidParam(l_chsiRequest,
                                                      0,
                                                      p_request,
                                                      "version",
                                                      false,
                                                      null,
                                                      ChsiServlet.C_TYPE_ANY);
        
                        if (l_chsiVersion != null)
                        {
                            // version was supplied, check that it is valid
                            if (!l_chsiVersion.equals(ChsiSession.C_CHSI_VERSION_1) )
                            {
                                l_chsiResponse = new ChsiResponse
                                        (l_chsiRequest,
                                         l_chsiRequest.getChsiSession().getSelectedLocale(),
                                         ChsiResponse.C_KEY_INVALID_VERSION,
                                         new String[] {l_chsiVersion},
                                         ChsiResponse.C_SEVERITY_FAILURE);

                                // Append the response to the Http request.
                                p_request.setAttribute("p_chsiResponse", l_chsiResponse);

                                // Forward to the NoServiceDataXML.jsp.
                                l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
                            }
                        }
                        else
                        {
                            l_chsiResponse = new ChsiResponse
                                    (l_chsiRequest,
                                     l_chsiRequest.getChsiSession().getSelectedLocale(),
                                     ChsiResponse.C_KEY_MESSAGE_VERSION_REQUIRED,
                                     (Object[])null,
                                     ChsiResponse.C_SEVERITY_FAILURE);

                            // Append the response to the Http request.
                            p_request.setAttribute("p_chsiResponse", l_chsiResponse);

                            // Forward to the NoServiceDataXML.jsp.
                            l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
                        }
                        
                        if (l_chsiResponse == null)
                        {
                            l_chsiSession.setChsiVersion(l_chsiVersion);
                            l_chsiRequest.setChsiVersion(l_chsiVersion);

                            // Call the Login service on ChsiSessionService.
                            l_chsiResponse = i_chsiSessionService.login(
                                                                    l_chsiRequest,
                                                                    i_timeout,
                                                                    p_request.getRemoteAddr() + " (" +
                                                                        p_request.getRemoteHost() + ")",
                                                                    l_userName,
                                                                    l_password);
                        }
                    }
                    else if ((l_command.equals("LOGIN")) &&
                              l_chsiSession.isLoggedIn())
                    {
                        // Create response message indicating that the client
                        // is already logged in.
                        l_chsiResponse = new ChsiResponse
                                         (l_chsiRequest,
                                          l_chsiRequest.getChsiSession()
                                              .getSelectedLocale(),
                                          ChsiResponse.C_KEY_ALREADY_LOGGED_IN,
                                          (Object[])null,
                                          ChsiResponse.C_SEVERITY_SUCCESS);
                    }
                    else if ((l_command.equals("LOGOUT")) && l_chsiSession.isLoggedIn())
                    {
                        // Call the Logout service on ChsiSessionService.
                        l_chsiResponse = i_chsiSessionService.logout(l_chsiRequest,
                                                                     i_timeout,
                                                                     p_request.getRemoteAddr() + " (" +
                                                                     p_request.getRemoteHost() + ")");
                    }
                    else if ((l_command.equals("LOGOUT")) &&
                              !l_chsiSession.isLoggedIn())
                    {
                        // Create response message indicating that the client
                        // is not currently logged in.
                        l_chsiResponse = new ChsiResponse
                                         (l_chsiRequest,
                                          l_chsiRequest.getChsiSession()
                                              .getSelectedLocale(),
                                          ChsiResponse.C_KEY_NOT_LOGGED_IN,
                                          (Object[])null,
                                          ChsiResponse.C_SEVERITY_SUCCESS);
                    }
                    else if ((l_command.equals("KEEPALIVE")) && l_chsiSession.isLoggedIn())
                    {
                        // Call the KeepAlive service on ChsiSessionService.
                        l_forwardUrl = new String("/jsp/chsi/noservicedataxml.jsp");
                        l_chsiResponse = keepAliveService(l_chsiRequest, l_chsiSession);

                    }
                    else if ((l_command.equals("KEEPALIVE")) &&
                            !l_chsiSession.isLoggedIn())
                  {
                      // Create response message indicating that the client
                      // is not currently logged in.
                      l_chsiResponse = new ChsiResponse
                                       (l_chsiRequest,
                                        l_chsiRequest.getChsiSession()
                                            .getSelectedLocale(),
                                        ChsiResponse.C_KEY_NOT_LOGGED_IN,
                                        (Object[])null,
                                        ChsiResponse.C_SEVERITY_SUCCESS);
                  }
                }

                // Increment requests received counter.
                i_requestsProcessedCounterInstrument.increment();
                l_timer.end(l_timerId);
            }
            catch (PpasServletException l_servletE)
            {
                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

                // Handle the case of an invalid or missing parameter in the incoming request
                l_chsiResponse = handleErrorResponse(l_chsiRequest, 0, l_servletE);
            }

            // Add catch all so that we can generate Chsi response if a runtime
            // exception has occurred
            catch (RuntimeException l_rtE)
            {
                String l_exceptionTxt = "";
                if (!(l_rtE instanceof PpasRuntimeException))
                {
                    // Should generate and log exception descended from
                    // PpasRuntimeException here ... but for the time being,
                    // just output to system console.
                    l_rtE.printStackTrace(System.err);

                    l_exceptionTxt = l_rtE.toString();
                }
                else
                {
                    l_exceptionTxt = ((PpasRuntimeException)l_rtE).getMsgText(Locale.getDefault());
                }

                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

                l_chsiResponse = new ChsiResponse(
                                     l_chsiRequest,
                                     new Locale("en", "GB"),
                                     ChsiResponse.C_KEY_RUNTIME_FAILURE,
                                     new Object [] {l_exceptionTxt},
                                     ChsiResponse.C_SEVERITY_FAILURE);

                p_request.setAttribute("p_chsiRequest", l_chsiRequest);

                l_chsiContext = new ChsiContext (l_chsiRequest, 0, i_logger);
                p_request.setAttribute("p_chsiContext", l_chsiContext);
            }

            // Append the response to the Http request.
            p_request.setAttribute("p_chsiResponse", l_chsiResponse);

        } // end if (not system initialisation failure)

        forwardToUrl(p_request, p_response, l_forwardUrl, l_chsiRequest);

        return;
    }

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /**
     * Currently does nothing but will construct a ChsiSessionService in
     * the fullness of time.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "In ChsiSessionServlet.doInit()");
        }

        i_keepaliveProcessedCounterInstrument = new CounterInstrument("Total KEEPALIVE Requests Processed",
                                                                      "Total Number of requests processed by the posi session servlet.");

        i_instrumentSet.add(i_keepaliveProcessedCounterInstrument);

        i_keepaliveReceivedCounterInstrument = new CounterInstrument("Total KEEPALIVE Requests Received",
                                                                     "Total Number of requests received by the posi session servlet.");

        i_instrumentSet.add(i_keepaliveReceivedCounterInstrument);

        i_instrumentManager = (InstrumentManager)(i_chsiContext
                .getAttribute("com.slb.sema.ppas.util.instrumentation.InstrumentManager"));

        i_instrumentManager.registerObject(this);

        // Should probably be constructing ChsiSessionService here...
        // unfortunately no ChsiRequest is available.

        return;
    }

    /**
     * Must be implemented in order to extend ChsiServlet but actually has no functional role.
     * @param p_request The servlet request.
     * @param p_response The servlet response.
     * @param p_httpSession The session.
     * @param p_chsiRequest The Chsi request.
     * @return The URL to which the request should be forwarded.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        ChsiRequest            p_chsiRequest)
    {
        String                 l_forwardUrl = null;

        return(l_forwardUrl);
    }
    
    /**
     * @param p_chsiRequest The Chsi request.
     * @param p_chsiSession
     * @return ChsiResponse The Chsi response.
     */
    public ChsiResponse keepAliveService(ChsiRequest p_chsiRequest,
                                         ChsiSession p_chsiSession)
    {
        ChsiResponse l_chsiResponse = null;

        // Increment keep alive request received counter.

        p_chsiSession.requestReceived(p_chsiRequest, ChsiSession.C_FLAG_KEEP_ALIVE_REQUEST);
        i_chsiContext.requestReceived();

        // Set the forward to URL to be the JSP that displays the full keep alive data.

        // l_forwardUrl = new String("/jsp/chsi/noservicedataxml.jsp");

        // Call the keepalive service on ChsiSessionService.

        l_chsiResponse = i_chsiSessionService.keepAlive(p_chsiRequest);

        if (l_chsiResponse != null)
        {
            // Notify context and session that request has been processed.

            p_chsiSession.requestProcessed(p_chsiRequest, ChsiSession.C_FLAG_KEEP_ALIVE_REQUEST);
            i_chsiContext.requestProcessed();
        }

        return(l_chsiResponse);
    }

    
}
