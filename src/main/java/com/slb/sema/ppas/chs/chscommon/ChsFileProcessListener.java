////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileProcessListener.java
//      DATE            :       23-May-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Interface defining a listener of Call History
//                              CDR files process events.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chscommon;


/**
 * This <code>ChsFileProcessListener</code> interface defines a listener of
 * Call History CDR files process events.
 */
public interface ChsFileProcessListener
{
    /**
     * Handles the given Call History CDR files process event.
     * 
     * @param p_event  the Call History CDR files process event.
     */
    public void handleEvent(ChsFileProcessEvent p_event);
} //End of interface 'ChsFileProcessListener'.
