////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       ChsiMessage_en_GB.java
//  DATE            :       27-Mar-2007
//  AUTHOR          :       Kevin Bond
//  COPYRIGHT       :       LogicaCMG
//
//  DESCRIPTION     :       Resource bundle loader for internationalisation 
//                          objects. Loads the csrmessage_en_uk.txt file 
//                          which contains message text for our english 
//                          readers. The text file MUST be stored in the 
//                          same directory/JAR file file as this .class 
//                          file.
//  
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+--------------------
//  DD/MM/YY | <name>     | <brief description of           | <reference>
//           |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
//
//TODO: what to write here? The copyright in this work belongs to Sema. The information
//contained in this work is confidential and must not be reproduced or
//disclosed to others without the prior written permission of Sema
//or the company within the Sema group of companies which supplied it.
//'Sema' is a registered trade mark of Sema.
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.localisation;

import java.io.IOException;
import java.util.PropertyResourceBundle;

import com.slb.sema.ppas.common.web.support.WebDebug;

/**
* Resource bundle loader for internationalisation objects.
* Loads the ChsiMessage_en_uk.txt file which contains message text
* for our english readers. The text file MUST be stored 
* in the same directory/JAR file as this .class file.
*/
public class ChsiMessage_en_GB extends PropertyResourceBundle 
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME   = "ChsiMessage_en_GB";

    /**
     * Resource file containing text for english messages.
     */
    private static final String C_MESSAGE_FILE = "chsimessage_en_gb.txt";

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /**
     * Called by ChsiResponse class upon construction of first ChsiXResponse object. Loads UK English text
     * file into PropertyResourceBundle making message text available to the various ChsiXResponse objects.
     * @throws IOException If teh expected resource
     *  cannot be read.
     */

    public ChsiMessage_en_GB() throws IOException
    {
        super(ChsiMessage_en_UK.class.getResourceAsStream(C_MESSAGE_FILE));

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_MWARE,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing ChsiMessage_en_GB()");
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_MWARE,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME,
                           10010,
                           this,
                           "Constructed ChsiMessage_en_GB()");
        }
    }
}
