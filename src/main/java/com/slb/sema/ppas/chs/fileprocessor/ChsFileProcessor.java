////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileProcessor.java
//      DATE            :       21-Mar-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class is responsible for the processing of
//                              the Call History CDR files.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.fileprocessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;

import com.slb.sema.ppas.chs.chscommon.ChsFileProcessEvent;
import com.slb.sema.ppas.chs.chscommon.ChsFileProcessListener;
import com.slb.sema.ppas.chs.chscommon.SqlLoaderLog;
import com.slb.sema.ppas.chs.dataclass.ChsFile;
import com.slb.sema.ppas.chs.exceptions.CallHistoryException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryFileNotFoundException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryKey;
import com.slb.sema.ppas.chs.exceptions.CallHistoryPlatformException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryKey.CallHistoryExceptionKey;
import com.slb.sema.ppas.chs.filehandler.ChsFileHandler;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.platform.Platform;
import com.slb.sema.ppas.common.platform.PlatformException;
import com.slb.sema.ppas.common.platform.PlatformMsg;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.ChsCallHistoryLoadService;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.instrumentation.MaxInstrument;
import com.slb.sema.ppas.util.instrumentation.MinInstrument;
import com.slb.sema.ppas.util.instrumentation.MinMaxAbstractInstrument;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.pool.Poolable;

/**
 * The <code>ChsFileProcessor</code> is a container for the main CDR processing logic. It cycles through
 * each CDR file and storing details in the ASCS database as appropriate.
 * A pool of file processors is used by the Call History Data Loader to process multiple CDR files.
 */
public class ChsFileProcessor implements InstrumentedObjectInterface, Poolable
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                    = "ChsFileProcessor";

    /** The property prefix for <code>ChsFileProcessor</code> properties. Value is {@value}. */
    private static final String C_CHS_FILE_PROC_PROPERTY_PREFIX =
        "com.slb.sema.ppas.chs.fileprocessor.ChsFileProcessor";

    /** The platform command timeout property. Value is {@value}. */
    private static final String C_PROPERTY_PLF_CMD_TIMEOUT      = C_CHS_FILE_PROC_PROPERTY_PREFIX +
                                                                  ".plfCmdTimeout";

    /** The default value (in ms) of the platform command timeout property. Value is {@value}. */
    private static final long   C_DEFVAL_PLF_CMD_TIMEOUT        = 300000;

    /** The IS API timeout property. Value is {@value}. */
    private static final String C_PROPERTY_IS_API_TIMEOUT       = C_CHS_FILE_PROC_PROPERTY_PREFIX +
                                                                  ".isApiTimeout";

    /** The default value (in ms) of the IS API timeout property. Value is {@value}. */
    private static final long   C_DEFVAL_IS_API_TIMEOUT         = 100000;

    /** The SQL Loader log file extension. Value is {@value}. */
    private static final String C_SQL_LOADER_LOG_EXT            = ".log";

    /** The SQL Loader bad file extension. Value is {@value}. */
    private static final String C_SQL_LOADER_BAD_EXT            = ".bad";

    /** The SQL Loader discard file extension. Value is {@value}. */
    private static final String C_SQL_LOADER_DISCARD_EXT        = ".dsc";

    /** The OS Standard Ouput file extension. Value is {@value}. */
    private static final String C_OS_STANDARD_OUTPUT_EXT        = ".os_log";

    /** The OS Standard Error file extension. Value is {@value}. */
    private static final String C_OS_STANDARD_ERROR_EXT         = ".os_err";

    /** The SQL*Loader FAILURE exit code. */
    private static final String C_SQL_LOADER_EXIT_CODE_FAILURE  = "1";

    /** The SQL*Loader WARNING exit code. */
    private static final String C_SQL_LOADER_EXIT_CODE_WARNING  = "2";

    /** The SQL Loader command. */
    private static final String C_SQL_LOADER_COMMAND            = "sqlldr " + "userid={0}, "
                                                                            + "control={1}, "
                                                                            + "data={2}, "
                                                                            + "log={3}, "
                                                                            + "bad={4}, "
                                                                            + "discard={5}, "
                                                                            + "silent={6}, "
                                                                            + "readSize={7}, "
                                                                            + "bindSize={8}";

    /** List of statuses that are acceptable as warnings from SQL Loader. */
    private static final int[] C_WARNING_STATUS = new int[] {2};
    
    //==========================================================================
    // Private instance attributes(s).
    //==========================================================================
    /** The list of the Call History CDR file process event listeners. */
    private ArrayList                 i_chsFileProcessListenerList  = null;
    
    /** The <code>ChsFileProcessorPool</code> object. */
    private ChsFileProcessorPool      i_chsFileProcessorPool          = null;

    /** The internal thread used to process CDR files. */
    private ProcessorThread           i_processorThread               = null;

    /** The thread started indicator. */
    private boolean                   i_threadStarted                 = false;

    /** The name of the CDR file currently processed. */
    private String                    i_cdrFileCurrentlyProcessed     = null;

    /** The context, holding system and business configuration and other resources. */
    private PpasContext               i_ppasContext                   = null;

    /** The <code>InstrumentManager</code> used by this <code>ChsFileProcessor</code> instance. */
    private InstrumentManager         i_instrumentManager             = null;

    /** The <code>InstrumentSet</code> used by this <code>ChsFileProcessor</code> instance. */
    private InstrumentSet             i_instrumentSet                 = null;

    /** If <code>true</code> then this object is assigned, <code>false</code> otherwise. */
    private boolean                   i_isAssigned                    = false;

    /** The last time this processor was assigned from the pool (in ms) */
    private long                      i_lastAssignedDateTime          = 0;

    /** The last time this processor was returned to the pool (in ms) */
    private long                      i_lastReturnedDateTime          = 0;

    /** Used to provide operating system dependent methods. */
    private Platform                  i_platform                      = null;

    /** The platform command timeout value. */
    private long                      i_plfCmdTimeout                 = C_DEFVAL_PLF_CMD_TIMEOUT;

    /** The IS API timeout value. */
    private long                      i_isApiTimeout                  = C_DEFVAL_IS_API_TIMEOUT;

    /** The <code>Logger</code> object used by this <code>ChsFileProcessor</code> instance. */
    private Logger                    i_logger                        = null;

    /** The name of this <code>ChsFileProcessor</code> instance. */
    private String                    i_id                            = null;

    /** ChsCallHistoryLoadService to maintain the CFCI table. */
    private ChsCallHistoryLoadService i_chsCallHistoryLoadService     = null;

    /** The <code>PpasProperties</code> object. */
    private PpasProperties            i_ppasProperties                = null;

    // The instrumented counters.
    /** The total number of processed CDR files. */
    private CounterInstrument         i_totNumberOfProcessedFiles     = null;

    /** The total number of processed records. */
    private CounterInstrument         i_totNumberOfProcessedRecs      = null;

    /** The total number of successfully processed records. */
    private CounterInstrument         i_totNumberOfSuccProcessedRecs  = null;

    /** The total number of rejected records. */
    private CounterInstrument         i_totNumberOfRejectedRecs       = null;

    /** The total number of discarded records. */
    private CounterInstrument         i_totNumberOfDiscardedRecs      = null;

    /** The maximum number of records in a file. */
    private MaxInstrument             i_maxNoOfRecordsInAFile         = null;

    /** The minimum number of records in a file. */
    private MinInstrument             i_minNoOfRecordsInAFile         = null;

    /** The longest processing time of a CDR file. */
    private MaxInstrument             i_longestProcessingTime         = null;

    /** The shortest processing time of a CDR file. */
    private MinInstrument             i_shortestProcessingTime        = null;

    /** The total length of time processing CDR files. */
    private MaxInstrument             i_totProcessingTime             = null;


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Contructs a <code>ChsFileProcessor</code> instance using the passed parameters.
     * 
     * @param p_chsFileProcessorPool Processor pool to which this processor belongs.
     * @param p_ppasContext        Session information.
     * @param p_logger             The <code>Logger</code> object.
     * @param p_id                 A unique id used to identify this instance.
     * @param p_instrumentManager  The <code>InstrumentManager</code> object.
     * @throws PpasConfigException if any mandatory property is missing.
     */
    public ChsFileProcessor(ChsFileProcessorPool p_chsFileProcessorPool,
                            PpasContext          p_ppasContext,
                            Logger               p_logger,
                            String               p_id,
                            InstrumentManager    p_instrumentManager) throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_chsFileProcessorPool       = p_chsFileProcessorPool;
        i_ppasContext                = p_ppasContext;
        i_logger                     = p_logger;
        i_id                         = p_id;
        i_instrumentManager          = p_instrumentManager;
        i_ppasProperties             = i_ppasContext.getProperties();
        i_chsCallHistoryLoadService  = new ChsCallHistoryLoadService(null, i_logger, i_ppasContext);
        i_platform                   = Platform.getThisPlatform(i_logger);
        i_chsFileProcessListenerList = new ArrayList();
        i_processorThread            = new ProcessorThread(0);
        i_threadStarted              = false;

        init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_addChsFileProcessorListener = "addChsFileProcessorListener";
    /**
     * Adds the given Call History CDR file process listener to receive file processing events
     * from this processor.
     * If the given listener is null, no exception is thrown and no action is performed.
     * 
     * @param p_listener  the Call History CDR file processing listener.
     */
    public void addChsFileProcessorListener(ChsFileProcessListener p_listener)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering " + C_METHOD_addChsFileProcessorListener);
        }

        if (p_listener != null)
        {
            synchronized (i_chsFileProcessListenerList)
            {
                i_chsFileProcessListenerList.add(p_listener);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Leaving " + C_METHOD_addChsFileProcessorListener);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_removeChsFileProcessorListener = "removeChsFileProcessorListener";
    /**
     * Removes the given Call History CDR file process listener so that it no longer will receive
     * file processing events from this processor.
     * This method performs no function, nor does it throw an exception, if the given listener was not
     * previously added to this component.
     * If the given listener is null, no exception is thrown and no action is performed.
     *  
     * @param p_listener  the Call History CDR file processing listener.
     */
    public void removeChsFileProcessorListener(ChsFileProcessListener p_listener)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10040, this,
                            "Entering " + C_METHOD_removeChsFileProcessorListener);
        }

        if (p_listener != null)
        {
            synchronized (i_chsFileProcessListenerList)
            {
                i_chsFileProcessListenerList.remove(p_listener);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this,
                            "Leaving " + C_METHOD_removeChsFileProcessorListener);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_start = "start";
    /**
     * Starts the internal thread that does the work of this <code>ChsFileProcessor</code> by invoking start
     * on the thread.
     */
    public void start()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10060, this,
                            "Entering " + C_METHOD_start);
        }

        synchronized (i_processorThread)
        {
            if (!i_threadStarted && i_processorThread != null  && !i_processorThread.isRunning())
            {
                // Reset counters.
                if (InstrumentManager.i_on)
                {
                    if (i_instrumentSet.i_isEnabled)
                    {
                        i_totProcessingTime.reset();
                        i_longestProcessingTime.reset();
                        i_shortestProcessingTime.reset();
                        i_totNumberOfProcessedFiles.reset();
                        i_totNumberOfProcessedRecs.reset();
                        i_totNumberOfSuccProcessedRecs.reset();
                        i_totNumberOfRejectedRecs.reset();
                        i_totNumberOfDiscardedRecs.reset();
                        i_maxNoOfRecordsInAFile.reset();
                        i_minNoOfRecordsInAFile.reset();
                    }
                }
    
                // Start the file processor thread.
                i_processorThread.start();
                i_threadStarted = true;
            }
        } //End of 'synchronized' block.

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10070, this,
                            "Leaving " + C_METHOD_start);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_stop = "stop";
    /**
     * Stops the internal thread that does the work of this <code>ChsFileProcessor</code> by invoking stop
     * on the thread.
     */
    public void stop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10080, this,
                            "Entering " + C_METHOD_stop);
        }

        if (i_processorThread != null && i_processorThread.isRunning())
        {
            // Stop the file processor thread.
            i_processorThread.setStopRequested(true);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10090, this,
                            "Leaving " + C_METHOD_stop);
        }
    }


    /** Method name for middleware calls. Value is {@value}. */
    private static final String C_METHOD_processFile_1 = "processFile(ChsFile, ChsFileHandler, int)";
    /**
     * Processes the given <code>ChsFile</code> object.
     * 
     * @param p_chsFile         the <code>ChsFile</code> object that encapsulates the CDR file
     *                          to be processed.
     * @param p_chsFileHandler  the <code>ChsFileHandler</code> object to be used for CDR file
     *                          renaming purposes.
     * @param p_distrThreadId   the file distributor thread id used in the filename extension.
     */
    public void processFile(ChsFile p_chsFile, ChsFileHandler p_chsFileHandler, int p_distrThreadId)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10100, this,
                            "Entering " + C_METHOD_processFile_1);
        }

        i_processorThread.addProcessData(new ProcessData(p_chsFile, p_chsFileHandler, p_distrThreadId));

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10120, this,
                            "Leaving " + C_METHOD_processFile_1);
        }
    }


    //--------------------------------------------------------------------------
    // Implementation of the InstrumentedObjectInterface interface.
    //--------------------------------------------------------------------------
    /**
     * Returns the <code>InstrumentSet</code> object for this <code>ChsFileProcessor</code> instance.
     * 
     * @return the <code>InstrumentSet</code> object for this <code>ChsFileProcessor</code> instance.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }


    /**
     * Returns the name (id) of this <code>ChsFileProcessor</code> instance.
     * 
     * @return the name (id) of this <code>ChsFileProcessor</code> instance.
     */
    public String getName()
    {
        return C_CLASS_NAME + "-" + i_id;
    }
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    // Implementation of the Poolable interface.
    //--------------------------------------------------------------------------
    /**
     * Set object instance to be assigned or not.
     * @param p_isAssigned Whether the object is assigned (true) or released (false).
     */
    public void setAssigned(boolean p_isAssigned)
    {
        i_isAssigned = p_isAssigned;
        if (i_isAssigned)
        {
            i_lastAssignedDateTime = System.currentTimeMillis();
        }
        else
        {
            i_lastReturnedDateTime = System.currentTimeMillis();
        }
    }

    /**
     * Identify whether object instance is assigned or not.
     * @return Assigned (true) or released (false).
     */
    public boolean isAssigned()
    {
        return i_isAssigned;
    }

    /**
     * Get date/time when the <code>Poolable</code> object was last assigned to the pool. Returns zero if
     * never assigned.
     * @return Date/time in milli-seconds when last assigned.
     */
    public long getLastAssignedDateTime()
    {
        return i_lastAssignedDateTime;
    }

    /**
     * Get date/time when the <code>Poolable</code> object was last returned to the pool. Returns zero if
     * never released.
     * @return Date/time in milli-seconds when last returned to the pool.
     */
    public long getLastReturnedDateTime()
    {
        return i_lastReturnedDateTime;
    }

    /**
     * Manager method that returns the date/time when this <code>Poolable</code> object was last assigned to
     * the pool as a <code>String</code>. Returns zero if never assigned.
     * @return Date/time in milli-seconds when last assigned.
     */
    public String mngMthGetLastAssignedDateTime()
    {
        return (getLastAssignedDateTime() == 0 ? "" : new Date(getLastAssignedDateTime()).toString());
    }

    /**
     * Manager method that returns the date/time when the <code>Poolable</code> object was last returned to
     * the pool. Returns zero if never released.
     * @return Date/time in milli-seconds when last returned to the pool.
     */
    public String mngMthGetLastReturnedDateTime()
    {
        return (getLastReturnedDateTime() == 0 ? "" : new Date(getLastReturnedDateTime()).toString());
    }

    /** Specific tidy-up activities for the class. */
    public void destroy()
    {
        stop();
    }
    //--------------------------------------------------------------------------


    /**
     * Returns a verbose <code>String</code> respresentation of this object suitable for debugging and trace.
     * 
     * @return A verbose <code>String</code> respresentation of this object suitable for debugging and trace.
     */
    public synchronized String toVerboseString()
    {
        StringBuffer l_verboseStr = new StringBuffer(C_CLASS_NAME);

        l_verboseStr.append(" =[");
        l_verboseStr.append("i_id = " + i_id);
        l_verboseStr.append(", i_cdrFileCurrentlyProcessed = " + 
                            (i_cdrFileCurrentlyProcessed == null  ?  "" : i_cdrFileCurrentlyProcessed));
        l_verboseStr.append(", i_totProcessingTime = " + i_totProcessingTime.getDisplayValue());
        l_verboseStr.append(", i_longestProcessingTime = " + i_longestProcessingTime.getDisplayValue());
        l_verboseStr.append(", i_shortestProcessingTime = " + i_shortestProcessingTime.getDisplayValue());
        l_verboseStr.append(", i_totNumberOfProcessedFiles = " + i_totNumberOfProcessedFiles.getValue());
        l_verboseStr.append(", i_totNumberOfProcessedRecs = " + i_totNumberOfProcessedRecs.getValue());
        l_verboseStr.append(", i_totNumberOfSuccProcessedRecs = " +
                            i_totNumberOfSuccProcessedRecs.getValue());
        l_verboseStr.append(", i_totNumberOfRejectedRecs = " + i_totNumberOfRejectedRecs.getValue());
        l_verboseStr.append(", i_totNumberOfDiscardedRecs = " + i_totNumberOfDiscardedRecs.getValue());
        l_verboseStr.append(", i_maxNoOfRecordsInAFile = " + i_maxNoOfRecordsInAFile.getValue());
        l_verboseStr.append(", i_minNoOfRecordsInAFile = " + i_minNoOfRecordsInAFile.getValue());
        l_verboseStr.append("]");

        return l_verboseStr.toString();
    }

    // ==========================================================================
    // Private method(s).
    // ==========================================================================
    /** Method name for middleware calls. Value is {@value}. */
    private static final String C_METHOD_processFile = "processFile";
    /**
     * Processes the given <code>ProcessData</code> object.
     * 
     * @param p_processData  the <code>ProcessData</code> to be processed.
     * @throws CallHistoryException if it fails to process the passed <code>ChsFile</code> file.
     * @throws PpasServiceException if it fails to access the ASCS database.
     */
    private void processFile(ProcessData p_processData) throws CallHistoryException, PpasServiceException
    {
        ChsFile                 l_chsFile          = null;
        ChsFileHandler          l_chsFileHandler   = null;
        String                  l_fileStatus       = null;
        StringBuffer            l_logFile          = null;
        SqlLoaderLog            l_sqlLoaderLog     = null;
        boolean                 l_renamed          = false;
        CallHistoryException    l_chsEx            = null;
        long                    l_startProcessTime = System.currentTimeMillis();
        long                    l_processingTime   = 0L;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10130, this,
                            "Entering " + C_METHOD_processFile);
        }

        l_chsFile        = p_processData.i_chsFile;
        l_chsFileHandler = p_processData.i_chsFileHandler;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10132, this,
                            "Process file '" + l_chsFile.getCdrFile().getAbsolutePath() + "'");
        }

        // Get the file status from database (could be 'null' if file details not yet stored).
        l_fileStatus = i_chsCallHistoryLoadService.getStatus(null,
                                                             l_chsFile.getCfciFileInfo1(),
                                                             l_chsFile.getCfciFileInfo2(),
                                                             l_chsFile.getCfciFileInfo3(),
                                                             l_chsFile.getCfciFileInfo4(),
                                                             i_isApiTimeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10134, this,
                            "File status = '" + l_fileStatus + "'");
        }

        if (l_fileStatus != null && l_fileStatus.equals(ChsFile.C_PROCESSING_STATUS_SUCCESSFULLY_PROC))
        {
            // This CDR file has already been processed, i.e. it's a duplicate.
            // Log a WARNING message, rename the CDR file and return.
            l_chsEx = new CallHistoryFileNotFoundException(C_CLASS_NAME,
                                                           C_METHOD_processFile,
                                                           10135,
                                                           this,
                                                           null,
                                                           0L,
                                                           CallHistoryKey.get().duplicateCDRFile(
                                                               l_chsFile.getCdrFile().getName(),
                                                               l_chsFileHandler.getDuplicateCDRFilesDir()),
                                                           null);
            i_logger.logMessage(l_chsEx);

            l_chsFileHandler.renameToDuplicate(l_chsFile);
            return;
        }

        if (l_fileStatus == null)
        {
            // This CDR file is new, add file info into the database.
            // NB: This CDR file could be processed by another thread and therefore already stored
            // in the database, i.e. needs to check the exception for the duplicate key.
            try
            {
                addFileDetails(l_chsFile);
            }
            catch (PpasServiceException l_ppasServEx)
            {
                if (l_ppasServEx.getMessage().equals(PpasServiceMsg.C_KEY_DUPLICATE_KEY))
                {
                    // This CDR file is already processed by another thread, skip it and try with next.
                    return;
                }
                throw l_ppasServEx;
            }
        }

        // Rename the file to the 'loading' extension and set the status to 'loading' if not yet done.
        if (!l_chsFile.getStatus().equals(ChsFile.C_PROCESSING_STATUS_DATA_LOADING))
        {
            l_renamed = l_chsFileHandler.renameToLoading(l_chsFile, p_processData.i_chsFileDistrThreadId);
            if (!l_renamed)
            {
                // Failed to rename the file to the 'in progress' state,
                // most likely gathered by another thread.
                // Just log it and continue with next file.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10136, this,
                                    C_METHOD_processFile + ", " + Thread.currentThread().getName()
                                            + " -- ***ERROR: Failed to rename the file '"
                                            + l_chsFile.getCdrFile().getAbsolutePath() + "' to 'loading', "
                                            + "most likely gathered by another thread.");
                }

                l_chsEx = new CallHistoryFileNotFoundException(C_CLASS_NAME,
                                                               C_METHOD_processFile,
                                                               10138,
                                                               this,
                                                               null,
                                                               0L,
                                                               CallHistoryKey.get().cdrFileRenameFailed(
                                                                   l_chsFile.getCdrFile().getAbsolutePath(),
                                                                   "loading"),
                                                               null);
                i_logger.logMessage(l_chsEx);
                return;
            }
        }

        if (l_fileStatus == null || !l_fileStatus.equals(ChsFile.C_PROCESSING_STATUS_DATA_LOADING))
        {
            i_chsCallHistoryLoadService.setStatus(null,
                                                  l_chsFile.getCfciFileInfo1(),
                                                  l_chsFile.getCfciFileInfo2(),
                                                  l_chsFile.getCfciFileInfo3(),
                                                  l_chsFile.getCfciFileInfo4(),
                                                  l_chsFile.getStatus(),
                                                  i_isApiTimeout);
        }

        // Create the SQL Loader log file name.
        l_logFile = new StringBuffer(i_chsFileProcessorPool.getLogDir());
        l_logFile.append(l_chsFile.getBaseFilename());
        l_logFile.append(C_SQL_LOADER_LOG_EXT);

        // Load Call History data into the database.
        l_sqlLoaderLog = loadFile(l_chsFile, l_logFile.toString());

        if (l_sqlLoaderLog.getStatus() == SqlLoaderLog.C_STATUS_SUCCESS ||
            l_sqlLoaderLog.getStatus() == SqlLoaderLog.C_STATUS_WARNING)
        {
            l_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_SUCCESSFULLY_PROC);

            // Update CDR file info.
            updateFileDetails(l_chsFile, l_sqlLoaderLog);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10140, this,
                                "***SUCCESS: Move file to the completed directory.");
            }
            l_chsFileHandler.renameToSuccessfullyProcessed(l_chsFile);
        }
        else
        {
            l_chsFileHandler.renameToErroneous(l_chsFile);

            // Update CDR file info.
            updateFileDetails(l_chsFile, l_sqlLoaderLog);
        }

        l_processingTime = System.currentTimeMillis() - l_startProcessTime;

        // Update counters.
        if (InstrumentManager.i_on)
        {
            if (i_instrumentSet.i_isEnabled)
            {
                i_totProcessingTime.compare(i_totProcessingTime.getValue() + l_processingTime);
                i_longestProcessingTime.compare(l_processingTime);
                i_shortestProcessingTime.compare(l_processingTime);
                i_totNumberOfProcessedFiles.increment();
                i_totNumberOfProcessedRecs.increment(l_sqlLoaderLog.getTotalLogicalRecordRead());
                i_totNumberOfSuccProcessedRecs.increment(l_sqlLoaderLog.getNumOfSuccRowsLoaded());
                i_totNumberOfRejectedRecs.increment(l_sqlLoaderLog.getTotalLogicalRecordRejected());
                i_totNumberOfDiscardedRecs.increment(l_sqlLoaderLog.getTotalLogicalRecordDiscarded());
                i_maxNoOfRecordsInAFile.compare(l_sqlLoaderLog.getTotalLogicalRecordRead());
                i_minNoOfRecordsInAFile.compare(l_sqlLoaderLog.getTotalLogicalRecordRead());
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME, 10145, this,
                        "i_totProcessingTime = " + i_totProcessingTime.getDisplayValue()                  +
                        ", i_longestProcessingTime = " + i_longestProcessingTime.getDisplayValue()        +
                        ", i_shortestProcessingTime = " + i_shortestProcessingTime.getDisplayValue()      +
                        ", i_totNumberOfProcessedFiles = " + i_totNumberOfProcessedFiles.getValue()       +
                        ", i_totNumberOfProcessedRecs = " + i_totNumberOfProcessedRecs.getValue()         +
                        ", i_totNumberOfSuccProcessedRecs = " + i_totNumberOfSuccProcessedRecs.getValue() +
                        ", i_totNumberOfRejectedRecs = " + i_totNumberOfRejectedRecs.getValue()           +
                        ", i_totNumberOfDiscardedRecs = " + i_totNumberOfDiscardedRecs.getValue()         +
                        ", i_maxNoOfRecordsInAFile = " + i_maxNoOfRecordsInAFile.getValue()               +
                        ", i_minNoOfRecordsInAFile = " + i_minNoOfRecordsInAFile.getValue());
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10150, this,
                            "Leaving " + C_METHOD_processFile);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_notifyChsFileProcessListeners = "notifyChsFileProcessListeners";
    /**
     * Notifies any Call History CDR file process listener that the given event has occurred.
     * 
     * @param p_eventCode  the code for the current event.
     */
    private void notifyChsFileProcessListeners(int p_eventCode)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10160, this,
                            "Entering " + C_METHOD_notifyChsFileProcessListeners);
        }

        ChsFileProcessEvent l_event = new ChsFileProcessEvent(p_eventCode, this);
        for (int l_ix = 0; l_ix < i_chsFileProcessListenerList.size(); l_ix++)
        {
            ((ChsFileProcessListener)i_chsFileProcessListenerList.get(l_ix)).handleEvent(l_event);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10170, this,
                            "Leaving " + C_METHOD_notifyChsFileProcessListeners);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_init = "init";
    /**
     * Initiates this <code>ChsFileProcessor</code> instance.
     * 
     * @throws PpasConfigException if any mandatory property is missing.
     */
    private void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10180, this,
                            "Entering " + C_METHOD_init);
        }

        i_plfCmdTimeout = getLongProperty(C_PROPERTY_PLF_CMD_TIMEOUT, false, C_DEFVAL_PLF_CMD_TIMEOUT);

        i_isApiTimeout  = getLongProperty(C_PROPERTY_IS_API_TIMEOUT, false, C_DEFVAL_IS_API_TIMEOUT);

        // Init. the instrument set.
        i_instrumentSet = new InstrumentSet("ChsFileProcessor");

        i_totProcessingTime =
            new MaxInstrument("Total time processing CDR files",
                              "A timer to keep track of the total CDR files processing time.",
                              MinMaxAbstractInstrument.C_TYPE_DELTA_TIME);
        i_instrumentSet.add(i_totProcessingTime);

        i_longestProcessingTime =
            new MaxInstrument("Longest processing time of a CDR file",
                              "A timer to keep track of the longest processing time of a CDR file.",
                              MinMaxAbstractInstrument.C_TYPE_DELTA_TIME);
        i_instrumentSet.add(i_longestProcessingTime);

        i_shortestProcessingTime =
            new MinInstrument("Shortest processing time of a CDR file",
                              "A timer to keep track of the shortest processing time of a CDR file.",
                              MinMaxAbstractInstrument.C_TYPE_DELTA_TIME);
        i_instrumentSet.add(i_shortestProcessingTime);

        i_totNumberOfProcessedFiles =
            new CounterInstrument("Total Number Of Processed Files",
                                  "A counter for the total number of files processed by this processor.");
        i_instrumentSet.add(i_totNumberOfProcessedFiles);

        i_totNumberOfProcessedRecs =
            new CounterInstrument("Total Number Of Processed Records",
                                  "A counter for the total number of records processed by this processor.");
        i_instrumentSet.add(i_totNumberOfProcessedRecs);

        i_totNumberOfSuccProcessedRecs =
            new CounterInstrument("Total Number Of Successfully Processed Records",
                                  "A counter for the total number of records successfully processed " +
                                  "by this processor.");
        i_instrumentSet.add(i_totNumberOfSuccProcessedRecs);

        i_totNumberOfRejectedRecs =
            new CounterInstrument("Total Number Of Rejected Records",
                                  "A counter for the total number of rejected records processed " +
                                  "by this processor.");
        i_instrumentSet.add(i_totNumberOfRejectedRecs);

        i_totNumberOfDiscardedRecs =
            new CounterInstrument("Total Number Of Discarded Records",
                                  "A counter for the total number of discarded records processed " +
                                  "by this processor.");
        i_instrumentSet.add(i_totNumberOfDiscardedRecs);

        i_maxNoOfRecordsInAFile    =
            new MaxInstrument("Maximum number of records in a file",
                              "An instrument used to keep track of the maximum number of records " +
                              "in a processed CDR file.");
        i_instrumentSet.add(i_maxNoOfRecordsInAFile);

        i_minNoOfRecordsInAFile    =
            new MinInstrument("Minimum number of records in a file",
                              "An instrument used to keep track of the minimum number of records " +
                              "in a processed CDR file.");
        i_instrumentSet.add(i_minNoOfRecordsInAFile);

        i_instrumentManager.registerObject(this);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10190, this,
                            "Leaving " + C_METHOD_init);
        }
    }


    /**
     * Returns the value, as a <code>long</code>, of the specified property.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value.
     * 
     * @return The value of the property if defined, otherwise the default value.
     * 
     * @throws PpasConfigException if a mandatory property is missing.
     */
    private long getLongProperty(String p_property, boolean p_mandatory, long p_defaultValue)
            throws PpasConfigException
    {
        String l_defaultValue = (p_mandatory ? null : Long.toString(p_defaultValue));
        return Long.parseLong(getProperty(p_property, p_mandatory, l_defaultValue));
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getProperty = "getProperty";
    /**
     * Returns the value, as a <code>String</code>, of the specified property.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value (should be <code>null</code> for a mandatory property).
     * 
     * @return The value of the property if defined, otherwise the default value.
     * 
     * @throws PpasConfigException if a mandatory property is missing.
     */
    private String getProperty(String p_property, boolean p_mandatory, String p_defaultValue)
            throws PpasConfigException
    {
        String              l_propValue    = null;
        String              l_defaultValue = null;
        PpasConfigException l_ppasConfigEx = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10200, this,
                            "Entering " + C_METHOD_getProperty);
        }

        l_defaultValue = (p_mandatory ? null : p_defaultValue);

        if (i_ppasProperties != null)
        {
            l_propValue = i_ppasProperties.getTrimmedProperty(p_property, l_defaultValue);
        }
        else
        {
            l_propValue = l_defaultValue;
        }

        if (p_mandatory && l_propValue == null)
        {
            // ***ERROR: This is a missing mandatory property, throw a 'PpasConfigException'.
            l_ppasConfigEx = new PpasConfigException(C_CLASS_NAME,
                                                     C_METHOD_getProperty,
                                                     10202,
                                                     this,
                                                     null,
                                                     0,
                                                     ConfigKey.get().missingConfigValue(p_property,
                                                                                        C_CLASS_NAME));
            i_logger.logMessage(l_ppasConfigEx);
            throw l_ppasConfigEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10210, this,
                            "Leaving " + C_METHOD_getProperty);
        }
        return l_propValue;
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_addFileDetails = "addFileDetails";
    /**
     * Adds file details retrieved from the <code>ChsFile</code> object
     * to the CFCI_CDR_FILE_CTL database table.
     * 
     * @param p_chsFile  the <code>ChsFile</code> object.
     * 
     * @throws PpasServiceException
     */
    private void addFileDetails(ChsFile p_chsFile) throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10220, this,
                            "Entering " + C_METHOD_addFileDetails);
        }

        i_chsCallHistoryLoadService.addFileDetails(null,// p_request
                                                   p_chsFile.getCfciFileInfo1(),
                                                   p_chsFile.getCfciFileInfo2(),
                                                   p_chsFile.getCfciFileInfo3(),
                                                   p_chsFile.getCfciFileInfo4(),
                                                   p_chsFile.getCdrFile().getAbsolutePath(),
                                                   p_chsFile.getLoggedDateTime(),
                                                   p_chsFile.getStatus(),
                                                   0,
                                                   0,
                                                   0,
                                                   i_isApiTimeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10230, this,
                            "Leaving " + C_METHOD_addFileDetails);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_updateFileDetails = "updateFileDetails";
    /**
     * Updates the CFCI_CDR_FILE_CTL database table with details retrieved from the passed
     * <code>SqlLoaderLog</code> and <code>ChsFile</code> objects.
     * 
     * @param p_chsFile       the <code>ChsFile</code> object.
     * @param p_sqlLoaderLog  the <code>SqlLoaderLog</code> object.
     * 
     * @throws PpasServiceException
     */
    private void updateFileDetails(ChsFile p_chsFile, SqlLoaderLog p_sqlLoaderLog)
        throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10240, this,
                            "Entering " + C_METHOD_updateFileDetails);
        }

        i_chsCallHistoryLoadService.updateFileDetails(null, // p_request,
                                                      p_chsFile.getStatus(),
                                                      p_sqlLoaderLog.getNumOfSuccRowsLoaded(),
                                                      p_sqlLoaderLog.getTotalLogicalRecordRejected(),
                                                      p_sqlLoaderLog.getTotalLogicalRecordDiscarded(),
                                                      p_chsFile.getCfciFileInfo1(),
                                                      p_chsFile.getCfciFileInfo2(),
                                                      p_chsFile.getCfciFileInfo3(),
                                                      p_chsFile.getCfciFileInfo4(),
                                                      i_isApiTimeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10250, this,
                            "Leaving " + C_METHOD_updateFileDetails);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_loadFile = "loadFile";
    /**
     * Loads the data given in the data file encapsulated by the passed <code>ChsFile</code> object into the
     * CHDR_CALL_HISTORY_DATA database table.
     * Logging info about the data loading process (performed by the Oracle SQL*Loader utility),
     * is stored in the passed log file.
     * 
     * @param p_chsFile  the <code>ChsFile</code> object.
     * @param p_logFile  the name of the log file (including the full directory path) into which logging info
     *                   about the data loading process is stored.
     * @return Parsed log details about the loaded file.
     * @throws CallHistoryPlatformException if the SQL*Loader command fails with an exit code
     *                                      other than WARNING.
     */
    private SqlLoaderLog loadFile(ChsFile p_chsFile, String p_logFile) throws CallHistoryPlatformException
    {
        String                       l_dataFilename      = null;
        String                       l_sqlldrCommand     = null;
        StringBuffer                 l_badFile           = null;
        StringBuffer                 l_discardFile       = null;
        StringBuffer                 l_osLogFile         = null;
        StringBuffer                 l_osErrFile         = null;
        String                       l_cmdExitCode       = null;
        SqlLoaderLog                 l_sqlLoaderLog      = null;
        PlatformException            l_platformfEx       = null;
        int                          l_processedRecs     = 0;
        int                          l_failedRecs        = 0;
        float                        l_tooBigProportion  = 0.0f;
        float                        l_failureProportion = 0.0f;
        String                       l_osErrorStr        = null;
        CallHistoryPlatformException l_chsPlatformEx     = null;
        CallHistoryExceptionKey      l_chsExKey          = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10260, this,
                            "Entering " + C_METHOD_loadFile);
        }

        // Create the SQL Loader bad file name.
        l_badFile = new StringBuffer(i_chsFileProcessorPool.getBadDir());
        l_badFile.append(p_chsFile.getBaseFilename());
        l_badFile.append(C_SQL_LOADER_BAD_EXT);

        // Create the SQL Loader discard file name.
        l_discardFile = new StringBuffer(i_chsFileProcessorPool.getDiscardDir());
        l_discardFile.append(p_chsFile.getBaseFilename());
        l_discardFile.append(C_SQL_LOADER_DISCARD_EXT);

        // Create the OS Standard Output log file name.
        l_osLogFile = new StringBuffer(i_chsFileProcessorPool.getOsLogDir());
        l_osLogFile.append(p_chsFile.getBaseFilename());
        l_osLogFile.append(C_OS_STANDARD_OUTPUT_EXT);

        // Create the OS Standard Error log file name.
        l_osErrFile = new StringBuffer(i_chsFileProcessorPool.getOsErrDir());
        l_osErrFile.append(p_chsFile.getBaseFilename());
        l_osErrFile.append(C_OS_STANDARD_ERROR_EXT);

        l_dataFilename = p_chsFile.getCdrFile().getAbsolutePath();

        // Create the SQL*Loader command.
        l_sqlldrCommand = MessageFormat.format(C_SQL_LOADER_COMMAND,
                                               new Object[] {i_chsFileProcessorPool.getOracleConnectionStr(),
                                                             i_chsFileProcessorPool.getControlFile(),
                                                             l_dataFilename,
                                                             p_logFile,
                                                             l_badFile.toString(),
                                                             l_discardFile.toString(),
                                                             i_chsFileProcessorPool.getSuppressMsg(),
                                                             i_chsFileProcessorPool.getBufferSizeAsString(),
                                                             i_chsFileProcessorPool.getBufferSizeAsString()});
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10262, this,
                            "The SQL Loader command: '" + l_sqlldrCommand + "'");
        }

        try
        {
            i_platform.performCommandAndOutputToFile(l_sqlldrCommand,
                                                     l_osLogFile.toString(),
                                                     l_osErrFile.toString(),
                                                     i_plfCmdTimeout,
                                                     C_WARNING_STATUS);
        }
        catch (PlatformException e)
        {
            l_platformfEx = e;
            l_osErrorStr  = readFile(l_osErrFile.toString());
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10063, this,
                                "l_osErrorStr: '" + l_osErrorStr + "'");
            }
        }
        finally
        {
            boolean                      l_seriousFailure    = false;
            boolean                      l_failure           = false;

            // Create an SqlLoaderLog instance in order to evalute the SQL*Loader log file.
            l_sqlLoaderLog = new SqlLoaderLog(p_logFile, i_chsFileProcessorPool.getDbmsDataErrors());

            // Check if a 'PlatformException' was thrown.
            if (l_platformfEx != null)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10264, this,
                                    "***ERROR: PlatformException occurred: " + l_platformfEx +
                                    ",  Msg. key: " + l_platformfEx.getMsgKey());
                }
                if (l_platformfEx.getMsgKey().equals(PlatformMsg.C_KEY_PROCESS_RETURNED_ERROR_STATUS))
                {
                    // Obtain the SQL Loader exit code from the PlatformException's message parameters.
                    l_cmdExitCode = (String)l_platformfEx.getMsgParams()[0];
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10266, this,
                                        "***ERROR: PlatformException exit code = " + l_cmdExitCode);
                    }
                    
                    if (l_cmdExitCode.equals(C_SQL_LOADER_EXIT_CODE_WARNING) ||
                        l_cmdExitCode.equals(C_SQL_LOADER_EXIT_CODE_FAILURE))
                    {
                        // It is a WARNING or a FAILURE exit code.
                        // Check if it should be treated as a serious failure.
                        if (l_sqlLoaderLog.getStatus() == SqlLoaderLog.C_STATUS_SERIOUS_FAILURE ||
                            (l_osErrorStr != null && l_osErrorStr.length() > 0))
                        {
                            l_seriousFailure = true;
                        }
                        else
                        {
                            // Not a serious failure, but we need to evalute if it should be treated
                            // as a Warning or an Error.
                            l_tooBigProportion = i_chsFileProcessorPool.getTooBigFailureProportion();
                            l_processedRecs = l_sqlLoaderLog.getTotalLogicalRecordRead();
                            l_failedRecs    = l_sqlLoaderLog.getTotalLogicalRecordRejected() + 
                                              l_sqlLoaderLog.getTotalLogicalRecordDiscarded();
                            l_failureProportion = (float)(l_failedRecs * 100) / (float)l_processedRecs;

                            if (l_failureProportion >= l_tooBigProportion)
                            {
                                // It should be treated as an Error.
                                l_failure = true;
                                l_sqlLoaderLog.setStatus(SqlLoaderLog.C_STATUS_FAILURE);
                            }
                        }
                    }
                    else
                    {
                        // It was a FATAL exit code, always a serious failure!
                        l_seriousFailure = true;
                    }
                }
                else
                {
                    // It was a serious failure!
                    l_seriousFailure = true;
                }

                if (l_seriousFailure)
                {
                    // It was a serious failure, create, log and throw a 'CallHistoryPlatformException'.
                    l_chsExKey = CallHistoryKey.get().sqlLoaderError(l_dataFilename, l_osErrorStr);
                    l_chsPlatformEx = new CallHistoryPlatformException(C_CLASS_NAME,
                                                                       C_METHOD_loadFile,
                                                                       10268,
                                                                       this,
                                                                       null,
                                                                       0L,
                                                                       l_chsExKey,
                                                                       l_platformfEx);
                    i_logger.logMessage(l_chsPlatformEx);
                    throw l_chsPlatformEx;
                }
                else if (l_failure)
                {
                    // It was a failure, create and log, but don't throw, a 'CallHistoryPlatformException'.
                    l_chsExKey = CallHistoryKey.get().tooManyFailedRecords(l_failedRecs,
                                                                           l_processedRecs,
                                                                           l_dataFilename);
                    l_chsPlatformEx = new CallHistoryPlatformException(C_CLASS_NAME,
                                                                       C_METHOD_loadFile,
                                                                       10270,
                                                                       this,
                                                                       null,
                                                                       0L,
                                                                       l_chsExKey,
                                                                       l_platformfEx);
                    i_logger.logMessage(l_chsPlatformEx);
                }
                else
                {
                    // It was warning or an ignorable 'PlatformException' that could be treated as a Warning.
                    // Create and log, but don't throw, a 'CallHistoryPlatformException'.
                    l_chsExKey =
                        CallHistoryKey.get().ignoredPlatformException(l_platformfEx.getExceptionId());
                    l_chsPlatformEx = new CallHistoryPlatformException(C_CLASS_NAME,
                                                                       C_METHOD_loadFile,
                                                                       10272,
                                                                       this,
                                                                       null,
                                                                       0L,
                                                                       l_chsExKey,
                                                                       l_platformfEx);
                    i_logger.logMessage(l_chsPlatformEx);
                }
            }
            else
            {
                // No exception thrown but check how many data errors.

                if (l_sqlLoaderLog.checkFailedProportion(i_chsFileProcessorPool.getTooBigFailureProportion(),
                                                         i_chsFileProcessorPool.getMinRows()))
                {
                    // Too many rows failed so log an error (but don't throw it.
                    l_chsPlatformEx = new CallHistoryPlatformException(
                                            C_CLASS_NAME,
                                            C_METHOD_loadFile,
                                            10275,
                                            this,
                                            null,
                                            0L,
                                            CallHistoryKey.get().tooManyFailedRecords(l_failedRecs,
                                                                                      l_processedRecs,
                                                                                      l_dataFilename),
                                            null);
                    i_logger.logMessage(l_chsPlatformEx);
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10280, this,
                            "Leaving " + C_METHOD_loadFile + " with " + l_sqlLoaderLog);
        }
        
        return l_sqlLoaderLog;
    }

    /**
     * Reads the file with the given filename and returns the contents in one <code>String</code>.
     * 
     * @param p_filename  the name of the file (including the full directory path) to be read.
     * 
     * @return  the contents of the given file in one <code>String</code>,
     *          or an empty string if the file is not found or can't be read.
     */
    private String readFile(String p_filename)
    {
        StringBuffer l_allLines = null;
        BufferedReader l_reader = null;
        String l_line = null;

        l_allLines = new StringBuffer();

        try
        {
            l_reader = new BufferedReader(new FileReader(new File(p_filename)));
            while ((l_line = l_reader.readLine()) != null)
            {
                l_allLines.append(l_line + "\n");
            }
        }
        catch (FileNotFoundException l_fnfEx)
        {
            l_fnfEx = null;
            l_allLines = null;
        }
        catch (IOException l_ioEx)
        {
            l_ioEx = null;
            l_allLines = null;
        }
        finally
        {
            if (l_reader != null)
            {
                try
                {
                    l_reader.close();
                }
                catch (IOException l_ioEx)
                {
                    l_ioEx = null;
                }
                l_reader = null;
            }
        }

        return (l_allLines != null ? l_allLines.toString() : "");
    }

    // ==========================================================================
    // Inner class(es).
    // ==========================================================================
    /**
     * The purpose of this <code>ProcessData</code> inner class defined within the
     * <code>ChsFileProcessor</code> class is to serve as a holder for the data 
     * needed when processing a CDR file.
     */
    private final class ProcessData
    {
        /** File being loaded. */
        private ChsFile        i_chsFile              = null;
        
        /** File handler. */
        private ChsFileHandler i_chsFileHandler       = null;
        
        /** Thread processing the file. */
        private int            i_chsFileDistrThreadId = -1;

        /** Standard constructor.
         * 
         * @param p_chsFile              File being loaded.
         * @param p_chsFileHandler       File handler.
         * @param p_chsFileDistrThreadId Thread processing the file.
         */
        private ProcessData(ChsFile p_chsFile, ChsFileHandler p_chsFileHandler, int  p_chsFileDistrThreadId)
        {
            i_chsFile              = p_chsFile;
            i_chsFileHandler       = p_chsFileHandler;
            i_chsFileDistrThreadId = p_chsFileDistrThreadId;
        }
    } //End of class 'ProcessData'.


    /**
     * The purpose of this <code>ProcessorThread</code> inner class defined within the
     * <code>ChsFileProcessor</code> class is to process a CDR file.
     * It extends the <code>ThreadObject</code> class in order to run the process
     * in a separate thread.
     */
    private final class ProcessorThread extends ThreadObject
    {
        // =====================================================================
        // Private class level constant(s).
        // =====================================================================
        /** The name of the current class to be used in calls to middleware. Value is {@value}. */
        private static final String C_CLASS_NAME_Thread = "ProcessorThread";

        // =====================================================================
        // Private instance attribute(s).
        // =====================================================================
        /** The current thread id. */
        private int       i_threadId        = -1;

        /** The "processor thread is running" indicator. */
        private boolean   i_running         = false;

        /** The "processor thread is requested to stop" indicator. */
        private boolean   i_stopRequested   = false;

        /** The "processor thread is requested to pause" indicator. */
        private boolean   i_pauseRequested  = false;

        /** The <code>ProcessData</code> objects to process. */
        private ArrayList i_processDataList = null;

        // =====================================================================
        // Constructor(s).
        // =====================================================================
        /**
         * Constructs an instance of the <code>ProcessorThread</code> class using the passed parameters.
         * @param p_threadId Identifier of this thread.
         */
        private ProcessorThread(int p_threadId)
        {
            super();
            i_threadId = p_threadId;
            i_processDataList = new ArrayList();
        }

        // =====================================================================
        // Public method(s).
        // =====================================================================
        /** The name of the following method. Used for calls to middleware. Value is (@value). */
        private static final String C_METHOD_doRun = "doRun";

        /**
         * Implements the CDR file processing functionality. It is an implementation of the abstract
         * <code>doRun</code> method of the <code>ThreadObject</code> class.
         */
        public void doRun()
        {
            ProcessData l_processData = null;
            boolean     l_continue    = true;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                C_CLASS_NAME_Thread, 90000, this,
                                "Entering " + C_METHOD_doRun);
            }

            setRunning(true);
            while (l_continue)
            {
                if (!isPauseRequested() && !isStopRequested())
                {
                    l_processData = getProcessData();
                    if (l_processData != null)
                    {
                        // Process file.
                        i_cdrFileCurrentlyProcessed = l_processData.i_chsFile.getCdrFile().getAbsolutePath();
                        try
                        {
                            processFile(l_processData);

                            // Notify process listeners that process is completed.
                            notifyChsFileProcessListeners(ChsFileProcessEvent.C_EVENT_TYPE_PROCESS_COMPLETED);
                        }
                        catch (CallHistoryException l_Ex)
                        {
                            // ***FATAL ERROR: Notify process listeners.
                            notifyChsFileProcessListeners(ChsFileProcessEvent.C_EVENT_TYPE_FATAL_ERROR);
                        }
                        catch (PpasServiceException l_Ex)
                        {
                            // ***FATAL ERROR: Notify process listeners.
                            notifyChsFileProcessListeners(ChsFileProcessEvent.C_EVENT_TYPE_FATAL_ERROR);
                        }
                        finally
                        {
                            i_cdrFileCurrentlyProcessed = null;
                        }
                    }
                    else
                    {
                        synchronized (this)
                        {
                            // Needs to check if stop is requested within the same synchronized block as
                            // the wait statement is called in order to avoid waiting after a request
                            // to stop already has been done.
                            if (!i_stopRequested)
                            {
                                try
                                {
                                    if (PpasDebug.on)
                                    {
                                        PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                                        PpasDebug.C_ST_TRACE,
                                                        C_CLASS_NAME_Thread, 90002, this,
                                                        "Let's wait for a CDR file to process.");
                                    }
                                    this.wait();
                                }
                                catch (InterruptedException l_intEx)
                                {
                                    // Nothing to do.
                                    l_intEx = null;
                                }
                            }
                            else
                            {
                                l_continue = false;
                            }
                        } //End of 'synchronized' block.
                    }
                }
                else
                {
                    if (isStopRequested())
                    {
                        l_continue = false;
                    }
                    else
                    {
                        // Pause is requested, but needs to check the pause flag within the same synchronized
                        // block as the wait statement.
                        synchronized (this)
                        {
                            if (i_pauseRequested)
                            {
                                try
                                {
                                    if (PpasDebug.on)
                                    {
                                        PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                                        PpasDebug.C_ST_TRACE,
                                                        C_CLASS_NAME_Thread, 90004, this,
                                                        "Let's pause until resumed or stopped.");
                                    }
                                    this.wait();
                                }
                                catch (InterruptedException l_intEx)
                                {
                                    // Nothing to do.
                                    l_intEx = null;
                                }
                            }
                        }
                        
                    }
                }
            } //End of 'while' loop.

            // Indicate not running.
            setRunning(false);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                C_CLASS_NAME_Thread, 90010, this,
                                "Leaving " + C_METHOD_doRun);
            }
        }

        /**
         * Adds the given <code>ProcessData</code> to the list of files to be processed.
         * 
         * @param p_processData Details of the file to be processed.
         */
        public synchronized void addProcessData(ProcessData p_processData)
        {
            i_processDataList.add(p_processData);

            // Wake up the processing thread if it is waiting on this object's monitor.
            this.notify();
        }

        /**
         * Returns the <code>ProcessData</code> object to be processed, or <code>null</code> if there is no
         * process data.
         * 
         * @return Deatisl of the file to process.
         */
        public synchronized ProcessData getProcessData()
        {
            ProcessData l_processData = null;
            if (!i_processDataList.isEmpty())
            {
                l_processData = (ProcessData)i_processDataList.remove(0);
            }
            return l_processData;
        }

        /**
         * Returns <code>true</code> if the processor thread is running,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_running</code> attribute is returned (assuming that
         *     a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the processor thread is running,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isRunning()
        {
            return i_running;
        }

        /**
         * Sets the running status of the processor thread.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_running</code> attribute is visible for other threads
         *     (assuming that the reading of this value also is done within a <code>synchronized</code>
         *     context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_running  If <code>true</code> the processor thread is running.
         */
        public synchronized void setRunning(boolean p_running)
        {
            i_running = p_running;
        }

        /**
         * Returns <code>true</code> if the processor thread is requested to stop,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_stopRequested</code> attribute is returned (assuming
         *     that a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the processor thread is requested to stop,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isStopRequested()
        {
            return i_stopRequested;
        }

        /**
         * Requests the processor thread to stop if the given
         * <code>boolean</code> parameter is <code>true</code>.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_stopRequested</code> attribute is visible for other
         *     threads (assuming that the reading of this value also is done within a
         *     <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_stopRequested  If <code>true</code> the processor thread is requested to stop.
         */
        public synchronized void setStopRequested(boolean p_stopRequested)
        {
            i_stopRequested = p_stopRequested;

            // Wake up the processing thread if it is waiting on this object's monitor.
            this.notify();
        }

        /**
         * Returns <code>true</code> if the processor thread is requested to pause,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_pauseRequested</code> attribute is returned (assuming
         *     that a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the processor thread is requested to pause,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isPauseRequested()
        {
            return i_pauseRequested;
        }

        /**
         * Requests the processor thread to pause if the given
         * <code>boolean</code> parameter is <code>true</code>.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_pauseRequested</code> attribute is visible for other
         *     threads (assuming that the reading of this value also is done within a
         *     <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_pauseRequested  If <code>true</code> the processor thread is requested to pause.
         */
        public synchronized void setPauseRequested(boolean p_pauseRequested)
        {
            i_pauseRequested = p_pauseRequested;

            // Wake up the processing thread if it is waiting on this object's monitor.
            this.notify();
        }

        //=====================================================================
        // Protected method(s).
        //=====================================================================
        /**
         * Returns a meaningful name for the thread.
         * It is an implementation of the abstract <code>getThreadName</code> method of
         * the <code>ThreadObject</code> class.
         * 
         * @return a meaningful name for the thread.
         */
        protected String getThreadName()
        {
            return (ChsFileProcessor.this.getName() + "_" + C_CLASS_NAME + "-"+ i_threadId);
        }
    } // End of the 'ProcessorThread' inner class.
} // End of the 'ChsFileProcessor' class.
