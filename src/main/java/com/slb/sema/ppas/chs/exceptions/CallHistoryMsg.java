////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryMsg.java
//      DATE            :       16-Mar-2006
//      AUTHOR          :       Lars Lundberg (h79023d)
//      REFERENCE       :       PRD_ASCS_DEV_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Call History keys resource bundle loader.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.exceptions;

import java.io.IOException;

import com.slb.sema.ppas.common.exceptions.XmlExceptionBundle;

/**
 * This class implements the resource bundle containing the text messages
 * associated with Call History exceptions and contains the keys into that
 * resource bundle.
 */
public class CallHistoryMsg extends XmlExceptionBundle
{
    //==========================================================================
    // Public class level constant(s).
    //==========================================================================
    /** The key into the message formats file describing the format of the output
     *  for all service related exceptions. Value is {@value}. */
    public static final String C_CHS_MSG_FMT_KEY                = "ppas_format";

    /** The name of the message texts file containing the messages associated with
     *  all service type exceptions. Value is {@value}. */
    public static final String C_CHS_MSG_BASENAME               = 
        "com.slb.sema.ppas.chs.exceptions.CallHistoryMsg";

    /** The key identifying a procesor pool timeout exception. Value is {@value}. */
    public static final String C_KEY_POOL_TIMEOUT               = "POOL_TIMEOUT";
    
    /** The key identifying that an ignorable platform exception was thrown. Value is {@value}. */
    public static final String C_KEY_NO_POOL_ENTRIES            = "NO_POOL_ENTRIES";

    /** The key identifying a SQL*Loader command exception. Value is {@value}. */
    public static final String C_KEY_SQL_LOADER_ERROR           = "SQL_LOADER_ERROR";
    
    /** The key identifying that a CDR file could not be found. Value is {@value}. */
    public static final String C_KEY_CDR_FILE_NOT_FOUND         = "CDR_FILE_NOT_FOUND";

    /** The key identifying that the root directory to search for the CDR files in could not be found.
     *  Value is {@value}. */
    public static final String C_KEY_ROOT_DIR_NOT_FOUND         = "ROOT_DIR_NOT_FOUND";

    /** The key identifying that the CDR files could not be renamed. Value is {@value}. */
    public static final String C_KEY_CDR_FILE_RENAME_FAILED     = "CDR_FILE_RENAME_FAILED";

    /** The key identifying that an ignorable platform exception was thrown. Value is {@value}. */
    public static final String C_KEY_IGNORED_PLATFORM_EXCEPTION = "IGNORED_PLATFORM_EXCEPTION";

    /** The key identifying that a duplicate CDR file was going to be processed. Value is {@value}. */
    public static final String C_KEY_DUPLICATE_CDR_FILE         = "DUPLICATE_CDR_FILE";

    /** The key identifying that a duplicate CDR file was going to be processed. Value is {@value}. */
    public static final String C_KEY_TOO_MANY_FAILED_RECORDS    = "TOO_MANY_FAILED_RECORDS";


    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the file which will contain all the message texts. */
    private static final String C_CHS_MSG_FILE          = "callhistory_msg.xml";


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an Msi message resource bundle object.
     * 
     * @throws IOException when unable to get resource bundle as stream.
     */
    public CallHistoryMsg() throws IOException
    {
        super(CallHistoryMsg.class.getResourceAsStream(C_CHS_MSG_FILE));
    }
}
