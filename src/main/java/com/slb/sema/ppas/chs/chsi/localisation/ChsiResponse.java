 ////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiResponse.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Response returned by a Chsi service method.
//
////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME        | DESCRIPTION                        | REFERENCE
//----------+-------------+------------------------------------+--------------------
//          |             |                                    |
////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.localisation; 

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.localisation.PpasResponse;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;

/**
 * Response returned by a Chsi service method. Super class for all
 * ChsiXResponse objects returned by the various ChsiXServices.
 */
public class ChsiResponse extends PpasResponse 
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /**
     * The C_MSGRESOURCESBASENAME constant stores the base class name
     * for ResourceBundle loading via PropertyResourceBundle classes.
     * The specific PropertyResourceBundle class name is created by
     * adding the Locale constant.
     */
    private static final String C_MSGRESOURCESBASENAME = "com.slb.sema.ppas.chs.chsi.localisation.ChsiMessage";

    /**
     * Constants for message IDs must be defined in this class for each new message
     * added to Chsi.
     */
        // Message keys for Install Service
    
    public static final String C_KEY_INVALID_DATA = "INVALID_DATA";
    
        
    

    // General service failure/success message keys.
    /** Service unavailable key. Value is {@value}. */
    public static final String C_KEY_SERVICE_UNAVAILABLE = "SERVICE_UNAVAILABLE";
    /** Service response unknown key. Value is {@value}. */
    public static final String C_KEY_SERVICE_RESPONSE_UNKNOWN = "SERVICE_RESPONSE_UNKNOWN";
    /** Service request timed out key. Value is {@value}. */
    public static final String C_KEY_SERVICE_REQUEST_TIMEOUT = "SERVICE_REQUEST_TIMEOUT";
    /** Service response timed out key. Value is {@value}. */
    public static final String C_KEY_SERVICE_RESPONSE_TIMEOUT = "SERVICE_RESPONSE_TIMEOUT";
    /** Service failure key. Value is {@value}. */
    public static final String C_KEY_SERVICE_FAILURE = "SERVICE_FAILURE";
    /** Service success key. Value is {@value}. */
    public static final String C_KEY_SERVICE_SUCCESS = "SERVICE_SUCCESS"; 
    /** Mobile number is blank key. Value is {@value}. */
    public static final String C_KEY_INPUT_MSISDN_BLANK = "INPUT_MSISDN_BLANK";
    /** Mobile number is not numeric key. Value is {@value}. */
    public static final String C_KEY_INPUT_MSISDN_NOT_NUMERIC = "INPUT_MSISDN_NOT_NUMERIC";
    /** Service is not specified key. Value is {@value}. */
    public static final String C_KEY_NO_SERVICE_SPECIFIED = "NO_SERVICE_SPECIFIED";
    /** Internal configuration error key. Value is {@value}. */
    public static final String C_KEY_INTERNAL_CONFIG_ERROR = "INTERNAL_CONFIG_ERROR";
    /** Internal data error key. Value is {@value}. */
    public static final String C_KEY_INTERNAL_DATA_ERROR = "INTERNAL_DATA_ERROR";    
    
    // Keys for invalid http input parameter
    /** Missing mandatory parameter key. Value is {@value}. */
    public static final String C_KEY_MISSING_MANDATORY_PARAM = "MISSING_MANDATORY_PARAM";
    /** Invalid numeric parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_NUMERIC_VALUE = "INVALID_NUMERIC_VALUE";    
    /** Invalid decimal numeric parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_DECIMAL_NUMERIC_VALUE = "INVALID_DECIMAL_NUMERIC_VALUE";
    /** Invalid boolean parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_BOOLEAN_VALUE = "INVALID_BOOLEAN_VALUE";
    /** Invalid date parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_DATE_VALUE = "INVALID_DATE_VALUE";
    /** Invalid date/time parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_DATETIME_VALUE = "INVALID_DATETIME_VALUE";
    /** Invalid parameter length key. Value is {@value}. */
    public static final String C_KEY_INVALID_PARAM_LENGTH = "INVALID_PARAM_LENGTH";
    /** Invalid enumerator parameter key. Value is {@value}. */
    public static final String C_KEY_INVALID_ENUM_VALUE = "INVALID_ENUM_VALUE";
    /** Non-blank parameter key. Value is {@value}. */
    public static final String C_KEY_NON_BLANK_PARAM = "NON_BLANK_PARAM";
    /** Invalid negative value key. Value is {@value}. */
    public static final String C_KEY_INVALID_NEGATIVE_VAL = "INVALID_NEGATIVE_VAL";
    /** Invalid plus sign key. Value is {@value}. */
    public static final String C_KEY_INVALID_PLUS_SIGN = "INVALID_PLUS_SIGN";

    // Message keys for dates
    /** Date before today key. Value is {@value}. */
    public static final String C_KEY_DATE_BEFORE_TODAYS_DATE = "DATE_BEFORE_TODAYS_DATE";
    /** End date before start date key. Value is {@value}. */
    public static final String C_KEY_END_DATE_BEFORE_START_DATE = "END_DATE_BEFORE_START_DATE";
    /** Date cannot be blank key. Value is {@value}. */
    public static final String C_KEY_DATE_CANNOT_BE_BLANK = "DATE_CANNOT_BE_BLANK";

    // Message keys for Session Services (eg login/logout)
    /** Login required key. Value is {@value}. */
    public static final String C_KEY_LOGIN_REQUIRED = "LOGIN_REQUIRED";
    /** Login failed key. Value is {@value}. */
    public static final String C_KEY_LOGIN_FAILURE = "LOGIN_FAILURE";
    /** Login not required key. Value is {@value}. */
    public static final String C_KEY_NO_LOGIN_REQUIRED = "NO_LOGIN_REQUIRED";
    /** Already logged in  key. Value is {@value}. */
    public static final String C_KEY_ALREADY_LOGGED_IN = "ALREADY_LOGGED_IN";
    /** Not logged in key. Value is {@value}. */
    public static final String C_KEY_NOT_LOGGED_IN = "NOT_LOGGED_IN";        
    /** Insufficient privilege key. Value is {@value}. */
    public static final String C_KEY_INSUFFICIENT_PRIVILEGE = "INSUFFICIENT_PRIVILEGE";    

    // Message keys for Call History service
    /** Invalid search leve. Value is {@value}. */
    public static final String C_KEY_INVALID_SEARCH_LEVEL = "INVALID_SEARCH_LEVEL";

    // System initialisation failed.
    /** System initialisation failed key. Value is {@value}. */
    public static final String C_KEY_SYS_INIT_FAILURE = "SYS_INIT_FAILURE";

    // An unexpected unchecked runtime exception was thrown.
    /** Unexpected run-time failure key. Value is {@value}. */
    public static final String C_KEY_RUNTIME_FAILURE = "RUNTIME_FAILURE";

    // The requested service has not been configured.
    /** Service is not configured key. Value is {@value}. */
    public static final String C_KEY_SERVICE_NOT_CONFIGURED = "SERVICE_NOT_CONFIGURED";
    
    // Message to indicate that Call History request has succeeded but there is no relevant call data in the database.
    /** Service Success no calls. Value is {@value}. */
    public static final String C_KEY_SERVICE_SUCCESS_NO_CALLS = "SERVICE_SUCCESS_NO_CALLS";

    // Versioning message keys
    /** A message version was not supplied. Value is {@value}. */
    public static final String C_KEY_MESSAGE_VERSION_REQUIRED = "MESSAGE_VERSION_REQUIRED";
    /** An invalid message version was 
     * supplied. Value is {@value}. */
    public static final String C_KEY_INVALID_VERSION = "INVALID_VERSION";
    /**
     * The request received was for a included a parameter which activates additional 
     * licenced functionality in an otherwise unlicenced service, and no valid licence exists for
     * that feature.
     */
    public static final String C_KEY_INVALID_FEATURE_LICENCE_PARAM = "INVALID_FEATURE_LICENCE_PARAM";    
    

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** Locale of the response - used to derive appropriate text. */
    private Locale i_messageLocale;

    /** Money formatter for Chsi. */
    protected MoneyFormat i_moneyFormat = null;
    
    /** Chsi version of this request/response pair. */
    private String i_chsiVersion;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /**
     * No Locale required for this constructor - always uses default Locale.
     * 
     * @param p_chsiRequest   The request being processed.
     * @param p_messageKey    Message key.
     * @param p_messageParams Parameters for this message.
     * @param p_messageSeverity Severity of this message.
     */
    public ChsiResponse(ChsiRequest p_chsiRequest,
                        String      p_messageKey,
                        Object[]    p_messageParams,
                        int         p_messageSeverity)
    {
        this(p_chsiRequest,
             null,
             p_messageKey,
             p_messageParams,
             p_messageSeverity);
    }

    /**
     * Uses the Locale passed in to construct text of message.
     * 
     * @param p_chsiRequest   The request being processed.
     * @param p_messageLocale The locale to use when formatting messages.
     * @param p_messageKey    Message key.
     * @param p_messageParams Parameters for this message.
     * @param p_messageSeverity Severity of this message.
     */
    public ChsiResponse(ChsiRequest p_chsiRequest,
                        Locale      p_messageLocale,
                        String      p_messageKey,
                        Object[]    p_messageParams,
                        int         p_messageSeverity)
    {
        super(p_chsiRequest,
              C_MSGRESOURCESBASENAME,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VHIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                null, // Need to add the ChsiRequest request at some point!
                C_CLASS_NAME, 10000, this,
                "Constructing ChsiResponse()");
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VHIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_APPLICATION_DATA,
                null, // Need to add the ChsiRequest request at some point!
                C_CLASS_NAME, 10010, this,
                "Key=" + p_messageKey + "; Severity=" + p_messageSeverity);
        }

        if (p_messageParams != null)
        {
            for(int l_index = 0; l_index < p_messageParams.length; l_index++)
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_VHIGH,
                        WebDebug.C_APP_SERVICE,
                        WebDebug.C_ST_APPLICATION_DATA,
                        null, // Need to add the ChsiRequest request at some point!
                        C_CLASS_NAME, 10020, this,
                        "Param[" + l_index + "]=" + p_messageParams[l_index]);
                }
            }
        }

        i_messageLocale = p_messageLocale;
        if (i_messageLocale == null)
        {
            // If locale not specified, use default Locale.
            i_messageLocale = Locale.getDefault();
        }

        i_moneyFormat = p_chsiRequest.getContext().getMoneyFormat();
        
        i_chsiVersion = p_chsiRequest.getChsiVersion();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VHIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                null, // Need to add the ChsiRequest request at some point!
                C_CLASS_NAME, 10090, this,
                "Constructed ChsiResponse()");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------
    /**
     * Returns a String containing the message text.
     * @return Message text.
     */
    public String getText()
    {
        return(super.getText(i_messageLocale));
    }

    /** Return formatted financial amount for Chsi.
     * @param p_amount Amount that needs formatting.
     * @return Formatted amount.
     */
    public String getFormattedAmount(Money p_amount)
    {
        String l_value = i_moneyFormat.format(p_amount);

        return l_value == null ? "0" : l_value;
    }

    /** Return currency as a string for Chsi.
     * @param p_amount Money from which currency will be obtained.
     * @return String representing the currency code.
     */
    public String getCurrencyAsString(Money p_amount)
    {
        String l_result = "";
        
        if (p_amount != null && p_amount.getCurrency() != null)
        {
            l_result = p_amount.getCurrency().getCurrencyCode();
        }

        return l_result;
    }

    /** Get the money format.
     * 
     * @return Money formatter.
     */
    public MoneyFormat getMoneyFormat()
    {
        return (i_moneyFormat);
    }
    
    /**Returns the MSISDN as a string.
     * @param p_msisdn - The MSISDN that will be converted to a string.
     * @return the MSISDN as a string.
     */
    public String formatMsisdn(Msisdn p_msisdn)
    {
        return getRequest().getContext().getMsisdnFormatOutput().format(p_msisdn);
    }
    
    /**
     * Get the Chsi version of this response.
     * @return the Chsi version of this response
     */
    public String getChsiVersion()
    {
        return i_chsiVersion;
    }
}
