////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryProcessor.java
//      DATE            :       20-Mar-2006
//      AUTHOR          :       Lars Lundberg (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       The top level class for the call history
//                              data load process.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 04/12/06 | John Lee   | Correct a mismatch between the  | PpacLon#2739/10577
//          |            | code and the properties file.   |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chscommon;

import com.slb.sema.ppas.chs.filedistributor.ChsFileDistributor;
import com.slb.sema.ppas.chs.fileprocessor.ChsFileProcessorPool;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.util.lang.DelayedSystemExitDaemon;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/**
 * This <code>CallHistoryProcessor</code> class is the top level class
 * for the call history data load process.
 */
public class CallHistoryProcessor extends AbstractCallHistoryProcessor
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CallHistoryProcessor";

    //==========================================================================
    // Private instance attribute(s).
    //==========================================================================
    /** The file distributor. */
    private ChsFileProcessorPool i_chsFileProcessorPool          = null;

    /** The file distributor. */
    private ChsFileDistributor   i_chsFileDistrbutor             = null;

    /** The process instance identifier. */
    private String               i_chsInstance                   = null;

    /** The time for system exit daemon to wait for threads to stop. 
     */
    private int                  i_exitDaemonTO                  = 0;

    /** The timeout used when stopping the processor pool. 
     */
    private int                  i_poolStopTimeout               = 0;

    /**
     * Attribute used to distinguish between testing and live modes.  When this
     * attribute is set to <code>true</code>, the delayed system exit daemon is
     * not used.
     */
    private boolean              i_createDelayedSystemExitDeamon = true;

    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an instance of the <code>CallHistoryProcessor</code> class.
     * 
     * @throws PpasException  if it fails to construct an instance of this class,
     *                        for instance if a mandatory property is missing.
     */
    public CallHistoryProcessor() throws PpasException
    {
        super();
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_chsInstance = i_processName.split("_")[2];

        // Create the 'ChsFileProcessorPool' object.
        i_chsFileProcessorPool = new ChsFileProcessorPool(i_ppasContext,
                                                          i_logger,
                                                          i_instrumentManager,
                                                          "ChsFileProcessorPool_" + i_chsInstance,
                                                          i_ppasProperties);

        // Create the 'ChsFileDistributor' object.
        i_chsFileDistrbutor = new ChsFileDistributor(this,
                                                     i_logger,
                                                     i_instrumentManager,
                                                     i_ppasProperties,
                                                     i_chsInstance,
                                                     i_chsFileProcessorPool);

        // Register this Call History Processor object.
        if (i_instrumentManager != null)
        {
            i_instrumentManager.registerObject(this);
        }
        else
        {
            i_instrumentSet.disable();
        }

        i_exitDaemonTO = i_ppasProperties.getIntProperty("com.slb.sema.ppas.chs.exitDaemonTimeout",  20000);
        i_poolStopTimeout = i_ppasProperties.getIntProperty(
                            "com.slb.sema.ppas.chs.chscommon.CallHistoryProcessor.poolStopTimeout", 20000);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //==========================================================================
    // Protected method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_doStart = "doStart";
    /**
     * Performs the Call History Processor specific startup.
     * This method overrides the empty default implementation in the super class.
     */    
    protected void doStart()
    {
        boolean l_startDistributor  = false;
        int     l_minFreeProcessors = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Entering " + C_METHOD_doStart);
        }

        // Start the Chs Processor Pool first
        i_chsFileProcessorPool.start();
        l_minFreeProcessors = i_chsFileProcessorPool.getMinFree();

        // Wait for the pool to start
        synchronized (this)
        {
            while (!l_startDistributor)
            {
                if ( (i_chsFileProcessorPool.isPoolRunning()) &&
                     (i_chsFileProcessorPool.getFreeNumber() >= l_minFreeProcessors) )
                {
                    l_startDistributor =  true;
                }
                else
                {
                    try
                    {
                        this.wait(1000);
                    }
                    catch (InterruptedException l_intEx)
                    {
                        // Interrupted on a wait, possibly stopping, so log and continue.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_ERROR,
                                            C_CLASS_NAME, 10032, this,
                                            "Interrupted exception [" + l_intEx + "] CHS must be stopping.");
                        }
                        l_intEx = null;
                    }
                }
            }
        }

        // Now start the distributor
        i_chsFileDistrbutor.start();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this,
                            "Leaving " + C_METHOD_doStart);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_doStop = "doStop";
    /**
     * Performs the Call History Processor specific stop (shutdown) processing.
     * This method overrides the empty default implementation in the super class.
     */    
    protected void doStop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this,
                            "Entering " + C_METHOD_doStop);
        }

        // Delayed system exit daemon used to ensure that all threads are
        // eventually terminated
        DelayedSystemExitDaemon l_delayedSystemExitDaemon = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_INIT,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10052,
                this,
                "Stopping " + C_CLASS_NAME);
        }

        // If in testing mode then do not use delayed system exit daemon
        if (i_createDelayedSystemExitDeamon)
        {
            // Use a delayed system exit daemon to ensure process stops eventually
            l_delayedSystemExitDaemon = new DelayedSystemExitDaemon(0, i_logger, i_exitDaemonTO);
            l_delayedSystemExitDaemon.start();
        }

        // Stop file distributor if it exists
        if (i_chsFileDistrbutor != null)
        {
            i_chsFileDistrbutor.stop();

            // Wait for distributor processes to stop before stopping the 'ChsFileProcessorPool'.
            while (!i_chsFileDistrbutor.isAllProcessesStopped())
            {
                // There is at least one process still running.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10054, this,
                                    C_METHOD_doStop + " -- Waiting for distributor processes to stop.");
                }
                try
                {
                    Thread.sleep(1500);
                }
                catch (InterruptedException l_intEx)
                {
                    // Ignore and carry on.
                    l_intEx = null;
                }
            }
        }

        // Stop pool if it exists and is running.
        if (i_chsFileProcessorPool != null  &&  i_chsFileProcessorPool.isPoolRunning())
        {
            i_chsFileProcessorPool.stop(i_poolStopTimeout);
            i_chsFileProcessorPool = null;
        }

        i_logger.logMessage(new LoggableEvent("Call History Data Load process is stopping.",
                                              LoggableInterface.C_SEVERITY_INFO));

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10060, this,
                            "Leaving " + C_METHOD_doStop);
        }
    }


    //==========================================================================
    // The command line interface (the 'main' method).
    //==========================================================================
    /**
     * The command line interface (main method) of the <code>CallHistoryProcessor</code> class.
     * Creats and starts an instance of the <code>CallHistoryProcessor</code> class.
     * 
     * @param p_args The arguments passed in (none expected).
     */
    public static void main(String[] p_args)
    {
        CallHistoryProcessor l_callHistoryProcess = null;

        try
        {
            // Create the Call History Processor.
            l_callHistoryProcess = new CallHistoryProcessor();

            // Start the process.
            l_callHistoryProcess.start();
        }
        catch (PpasException l_ppasEx)
        {
            System.err.println("Call History Processor startup exiting due to the following exception: ");
            l_ppasEx.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
