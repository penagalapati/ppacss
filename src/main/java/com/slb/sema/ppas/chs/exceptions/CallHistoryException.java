////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryException.java
//      DATE            :       16-Mars-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class is the base class for all Exceptions
//                              needed by the Call History Server classes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.exceptions;

import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasExceptionTypeConstants;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * This class is the base class for all Exceptions needed by
 * the Call History Server classes.
 */
public class CallHistoryException extends PpasException
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The unique identifier for this class of exception. */
    private static final String C_EXCEPTION_ID = PpasExceptionTypeConstants.C_EXCEPTION_ID_CHS;

    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an instance of the <code>CallHistoryException</code> class.
     * The exception is identified by the base name of the resource bundle defining
     * the message texts associated with a class of related exceptions and by the key
     * into that resource bundle (this identifies the exception type but not a particular
     * instance of the exception object).
     * 
     * @param p_callingClass     The name of the class invoking this constructor.
     * @param p_callingMethod    The name of the method from which this constructor was invoked.
     * @param p_stmtNo           Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject    The object from which this constructor was invoked,
     *                           <code>null</code> if the constructor was invoked from a static method.
     * @param p_request          Request which gave rise to this exception.
     * @param p_flags            Bitmask flag ... if bit C_FLAG_INC_STACK_TRACE is set,
     *                           then a stack trace will be output when some exception objects are logged.
     * @param p_exceptionKey     Exception key/parameters defining this exception.
     * @param p_sourceException  An earlier exception which gave rise to this exception.
     */
    public CallHistoryException(String       p_callingClass,
                                String       p_callingMethod,
                                int          p_stmtNo,
                                Object       p_callingObject,
                                PpasRequest  p_request,
                                long         p_flags,
                                ExceptionKey p_exceptionKey,
                                Exception    p_sourceException)
    {
        super(p_callingClass,
              p_callingMethod,
              p_stmtNo,
              p_callingObject,
              p_request,
              p_flags,
              C_EXCEPTION_ID,
              CallHistoryMsg.C_CHS_MSG_BASENAME,
              p_exceptionKey,
              CallHistoryMsg.C_CHS_MSG_FMT_KEY,
              p_sourceException);
    }
}
