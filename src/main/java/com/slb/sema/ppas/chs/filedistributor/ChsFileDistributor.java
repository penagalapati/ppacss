////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileDistributor.java
//      DATE            :       11-Mars-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class is responsible for looking for new
//                              Call History CDR files to process.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.filedistributor;

import java.io.File;
import java.text.MessageFormat;
import java.util.Vector;

import com.slb.sema.ppas.chs.chscommon.CallHistoryProcessor;
import com.slb.sema.ppas.chs.chscommon.ChsFileProcessEvent;
import com.slb.sema.ppas.chs.chscommon.ChsFileProcessListener;
import com.slb.sema.ppas.chs.dataclass.ChsFile;
import com.slb.sema.ppas.chs.exceptions.CallHistoryException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryFileNotFoundException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryKey;
import com.slb.sema.ppas.chs.exceptions.CallHistoryTimeoutException;
import com.slb.sema.ppas.chs.exceptions.CallHistoryKey.CallHistoryExceptionKey;
import com.slb.sema.ppas.chs.filehandler.ChsFileHandler;
import com.slb.sema.ppas.chs.fileprocessor.ChsFileProcessor;
import com.slb.sema.ppas.chs.fileprocessor.ChsFileProcessorPool;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.file.FileSearch;
import com.slb.sema.ppas.util.file.FileSearchException;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * This <code>ChsFileDistributor</code> class is responsible for looking for
 * new Call History CDR files to process.
 */
public class ChsFileDistributor implements InstrumentedObjectInterface
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                       = "ChsFileDistributor";

    /** The property prefix for <code>ChsFileDistributor</code> properties. Value is {@value}. */
    private static final String C_CHS_FILE_DISTR_PROPERTY_PREFIX   =
        "com.slb.sema.ppas.chs.filedistributor.ChsFileDistributor";

    /** The property name for the number of file distributor threads. Value is {@value}. */
    private static final String C_PROPERTY_NUMBER_OF_DISTR_THREADS = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".noOfDistrThreads";

    /** The default value for the number of file distributor threads. Value is {@value}. */
    private static final String C_DEFAULT_NUMBER_OF_DISTR_THREADS  = "1";

    /** The property name for the time out to obtain a file processor from the Pool. Value is {@value}. */
    private static final String C_PROPERTY_PROC_FROM_POOL_TIMEOUT  = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".procFromPoolTimeout";

    /** The default value for the time out to obtain a file processor from the pool. Value is {@value}. */
    private static final String C_DEFAULT_PROC_FROM_POOL_TIMEOUT   = "10000";


    // Properties defined for each distributor thread.
    // Each disitribution thread will have its own unique property.
    // However, an overall property could be defined which could be used by any thread if suitable.
    // For example the directory depth property could be defined as an overall property:
    // com.slb.sema.ppas.chs.chsFileDistributor.dirDepth=2
    // or as a specific property for a certain thread (which then overrides the overall property):
    // com.slb.sema.ppas.chs.chsFileDistributor.n.dirDepth=3 (where 'n' is the thread number).

    /** The overall property name of the root CDR directory. Value is {@value}. */
    private static final String C_OVERALL_PROPERTY_ROOT_DIRECTORY   = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".rootDirectory";

    /** The property name of the root CDR directory.
     *  Each disitribution thread has its own unique property name. Value is {@value}. */
    private static final String C_PROPERTY_ROOT_DIRECTORY           = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".{0}.rootDirectory";

    /** The overall property name of the directory depth in which to search for the files.
     *  Value is {@value}. */
    private static final String C_OVERALL_PROPERTY_DIRECTORY_DEPTH  = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".dirDepth";

    /** The property name of the directory depth in which to search for the files.
     *  Each disitribution thread has its own unique property name. Value is {@value}. */
    private static final String C_PROPERTY_DIRECTORY_DEPTH          = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".{0}.dirDepth";

    /** The default value for the directory depth property. Value is {@value}. */
    private static final short  C_DEFAULT_DIRECTORY_DEPTH           = 0;

    /** The overall property name of the directories search direction. Value is {@value}. */
    private static final String C_OVERALL_PROPERTY_SEARCH_DIRECTION = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".searchDirection";

    /** The property name of the directories search direction.
     *  Each disitribution thread has its own unique property name. Value is {@value}. */
    private static final String C_PROPERTY_SEARCH_DIRECTION         = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                      ".{0}.searchDirection";

    /** The normal search direction through the CDR directories. Value is {@value}. */
    private static final String C_NORMAL_SEARCH_DIRECTION           = "N";

    /** The reverse search direction through the CDR directories. Value is {@value}. */
    private static final String C_REVERSE_SEARCH_DIRECTION          = "R";

    /** The overall property name of the poll time value (the time in ms to wait between processing cycles).
     *  Value is {@value}. */
    private static final String C_OVERALL_PROPERTY_POLL_TIME        = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                    "pollTime";

    /** The property name of the poll time value (the time in ms to wait between processing cycles).
     *  Each disitribution thread has its own unique property name. Value is {@value}. */
    private static final String C_PROPERTY_POLL_TIME                = C_CHS_FILE_DISTR_PROPERTY_PREFIX +
                                                                     ".{0}.pollTime";

    /** The default poll time value in ms. Value is {@value}. */
    private static final long   C_DEFAULT_POLL_TIME                 = 5000;


    //==========================================================================
    // Private instance attributes(s).
    //==========================================================================
    /** The <code>CallHistoryProcessor</code> object. */
    private CallHistoryProcessor i_callHistoryProcessor = null;

    /** The <code>InstrumentManager</code> object used by the <code>ChsFileDistributor</code>. */
    private InstrumentManager    i_instrumentManager    = null;

    /** The <code>InstrumentSet</code> for the Call History file distributor. */
    private InstrumentSet        i_instrumentSet        = null;

    /** The number of files found. */
    private CounterInstrument    i_noOfFoundFiles       = null;

    /** The number of files found with an invalid filename. */
    private CounterInstrument    i_noOfFilesWithInvName = null;

    /** The <code>Logger</code> instance used to log events and messages. */
    private Logger               i_logger               = null;

    /** The ASCS properties object. */
    private PpasProperties       i_ppasProperties       = null;

    /** The file distributor. */
    private ChsFileProcessorPool i_chsFileProcessorPool = null;

    /** The time out to obtain a file processor from the pool. */
    private int                  i_procFromPoolTimeout  = -1;

    /** The process instance identifier. */
    private String               i_chsInstance          = null;

    /** The distributor threads. */
    private DistributorThread[]  i_distributorThreadArr = null;

    /** The process termination requested indicator. */
    private boolean              i_terminationRequested = false;


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs a <code>ChsFileDistributor</code> instance using the passed parameters.
     *  
     * @param p_callHistoryProcessor  The <code>CallHistoryProcessor</code> object.
     * @param p_logger                The logger instance used by the Call History Server process.
     * @param p_instrumentManager     The <code>InstrumentManager</code> used for Admin Instrumentation.
     * @param p_ppasProperties        Configuration properties.
     * @param p_chsInstance           The CHS instance identifier.
     * @param p_chsFileProcessorPool  The pool of CDR file processor threads.
     * 
     * @throws PpasConfigException  if any mandatory property is missing.
     */
    public ChsFileDistributor(CallHistoryProcessor p_callHistoryProcessor,
                              Logger               p_logger,
                              InstrumentManager    p_instrumentManager,
                              PpasProperties       p_ppasProperties,
                              String               p_chsInstance,
                              ChsFileProcessorPool p_chsFileProcessorPool)
        throws PpasConfigException
    {
        super();
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_callHistoryProcessor = p_callHistoryProcessor;
        i_instrumentManager    = p_instrumentManager;
        i_logger               = p_logger;
        i_ppasProperties       = p_ppasProperties;
        i_chsFileProcessorPool = p_chsFileProcessorPool;
        i_chsInstance          = p_chsInstance;

        init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }


    //==========================================================================
    // Public method(s).
    //==========================================================================
    //--------------------------------------------------------------------------
    // Implementation of the InstrumentedObjectInterface interface.
    //--------------------------------------------------------------------------
    /** 
     * Returns the <code>InstrumentSet</code> object for this <code>ChsFileDistributor</code> instance.
     * 
     * @return the <code>InstrumentSet</code> object for this <code>ChsFileDistributor</code> instance.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }


    /** 
     * Returns the name (id) of this <code>ChsFileDistributor</code> instance.
     * 
     * @return the name (id) of this <code>ChsFileDistributor</code> instance.
     */
    public String getName()
    {
        return (C_CLASS_NAME + '_' + i_chsInstance);
    }
    //--------------------------------------------------------------------------


    /**
     * Starts the distributor threads.
     * <br>
     * NB: This method is declared <code>synchronized</code> to make this operation on the
     *     distribution threads atomic.
     */
    public synchronized void start()
    {
        if (i_distributorThreadArr != null)
        {
            // Reset the instrumented counters.
            i_noOfFoundFiles.reset();
            i_noOfFilesWithInvName.reset();

            // Start all distributor threads.
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                if (!i_distributorThreadArr[l_ix].isRunning())
                {
                    i_distributorThreadArr[l_ix].start();
                }
            }
        }
    }


    /**
     * Requests the distributor threads to stop.
     * <br>
     * NB: This method is declared <code>synchronized</code> to make this operation on the
     *     distribution threads atomic.
     */
    public synchronized void stop()
    {
        if (i_distributorThreadArr != null)
        {
            // Request all distributor threads to stop.
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                i_distributorThreadArr[l_ix].setStopRequested(true);
            }
        }
    }

    /** Check whether all threads have stopped.
     * 
     * @return True if all threads have stopped.
     */
    public boolean isAllProcessesStopped()
    {
        boolean l_allStopped = true;

        if (i_distributorThreadArr != null)
        {
            // Check if all distributor threads are stopped.
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                if (i_distributorThreadArr[l_ix].isRunning())
                {
                    l_allStopped = false;
                    break;
                }
            }
        }

        return l_allStopped;
    }

    /**
     * Requests the distributor threads to pause.
     * <br>
     * NB: This method is declared <code>synchronized</code> to make this operation on the
     *     distribution threads atomic.
     */
    public synchronized void pause()
    {
        if (i_distributorThreadArr != null)
        {
            // Request all distributor threads to pause.
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                i_distributorThreadArr[l_ix].setPauseRequested(true);
            }
        }
    }


    /**
     * Resumes the distributor threads.
     * <br>
     * NB: This method is declared <code>synchronized</code> to make this operation on the
     *     distribution threads atomic.
     */
    public synchronized void resume()
    {
        if (i_distributorThreadArr != null)
        {
            // Resumes all distributor threads.
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                if (i_distributorThreadArr[l_ix].isRunning())
                {
                    i_distributorThreadArr[l_ix].setPauseRequested(false);
                }
            }
        }
    }


    /**
     * Manager method that pauses the distributor threads.
     */
    public void mngMthPauseDistributorThreads()
    {
        pause();
    }


    /**
     * Manager method that resumes the distributor threads.
     */
    public void mngMthResumeDistributorThreads()
    {
        resume();
    }


    //==========================================================================
    // Protected method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_terminateProcess = "terminateProcess";
    /**
     * Terminates the complete Call History Data Load process.
     * To be called if a serious non-recoverable error occurs, for instance if
     * the configured number of file processors in the pool is set to zero.
     */
    protected synchronized void terminateProcess()
    {
        Thread l_terminationThread = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Entering " + C_METHOD_terminateProcess);
        }

        if (!i_terminationRequested && i_callHistoryProcessor != null)
        {
            // Create a local thread that will call the 'stop()' method in the 'CallHistoryProcessor'.
            // Otherwise this thread will wait for ever for all distributor threads to be stopped,
            // since this thread probably is a distributor thread.
            l_terminationThread = new Thread(new Runnable()
                                             {
                                                 public void run()
                                                 {
                                                    i_callHistoryProcessor.stop();
                                                 }
                                             });

            // Request the Call History Data Load process to terminate.
            l_terminationThread.start();
            i_terminationRequested = true;
        }


        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this,
                            "Leaving " + C_METHOD_terminateProcess);
        }
    }


    //==========================================================================
    // Private method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_init = "init";
    /**
     * Initiates this <code>ChsFileDistributor</code> instance.
     * 
     * @throws PpasConfigException  if any mandatory property is missing.
     */
    private void init() throws PpasConfigException
    {
        int l_noOfDistrThreads = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this,
                            "Entering " + C_METHOD_init);
        }

        // Init. the instrument set.
        i_instrumentSet = new InstrumentSet(C_CLASS_NAME);

        // Create the instrumented counters etc.
        i_noOfFoundFiles = new CounterInstrument("The total number of found files",
                                                 "A counter used for the total number of found files.");
        i_instrumentSet.add(i_noOfFoundFiles);

        /** The number of files found with an invalid filename. */
        i_noOfFilesWithInvName =
            new CounterInstrument("The total number of files found with an invalid filename",
                                  "A counter used for the number of files found with an invalid filename.");
        i_instrumentSet.add(i_noOfFilesWithInvName);


        // Register this instrumented object.
        i_instrumentManager.registerObject(this);

        // Get configuration.
        i_procFromPoolTimeout =
            Integer.parseInt(i_ppasProperties.getTrimmedProperty(C_PROPERTY_PROC_FROM_POOL_TIMEOUT,
                                                                 C_DEFAULT_PROC_FROM_POOL_TIMEOUT));

        l_noOfDistrThreads =
            Integer.parseInt(i_ppasProperties.getTrimmedProperty(C_PROPERTY_NUMBER_OF_DISTR_THREADS,
                                                                 C_DEFAULT_NUMBER_OF_DISTR_THREADS));

        createDistributionThreads(l_noOfDistrThreads);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10060, this,
                            "Leaving " + C_METHOD_init);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_createDistributionThreads = "createDistributionThreads";
    /**
     * Creates the requested number of call history files distributor threads.
     * NB: This method is declared <code>synchronized</code> to make this creation of the
     *     distribution threads atomic.
     * 
     * @param p_noOfThreads  the requested number of call history files distributor threads.
     * 
     * @throws PpasConfigException  if any mandatory property is missing.
     */
    private synchronized void createDistributionThreads(int p_noOfThreads) throws PpasConfigException
    {
        int            l_distrThreadId           = 0;
        String         l_distrThreadIdStr        = null;
        FileSearch     l_fileSearch              = null;
        ChsFileHandler l_chsFileHandler          = null;
        String         l_rootDirPropName         = null;
        String         l_rootDir                 = null;
        String         l_dirDepthPropName        = null;
        short          l_dirDepth                = 0;
        String         l_searchDirectionPropName = null;
        String         l_searchDirection         = null;
        String         l_pollTimePropName        = null;
        long           l_pollTime                = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10070, this,
                            "Entering " + C_METHOD_createDistributionThreads);
        }

        if (i_distributorThreadArr == null)
        {
            i_distributorThreadArr = new DistributorThread[p_noOfThreads];
            for (int l_ix = 0; l_ix < i_distributorThreadArr.length; l_ix++)
            {
                l_distrThreadId = l_ix + 1;
                l_distrThreadIdStr = Integer.toString(l_distrThreadId);

                // Read the root directory property. NB: This is a mandatory property.
                l_rootDirPropName = MessageFormat.format(C_PROPERTY_ROOT_DIRECTORY,
                                                         new Object[]{l_distrThreadIdStr});
                l_rootDir = getProperty(l_rootDirPropName, C_OVERALL_PROPERTY_ROOT_DIRECTORY, true, null);

                // Read the directory depth property.
                l_dirDepthPropName = MessageFormat.format(C_PROPERTY_DIRECTORY_DEPTH,
                                                          new Object[]{l_distrThreadIdStr});
                l_dirDepth = getShortProperty(l_dirDepthPropName,
                                              C_OVERALL_PROPERTY_DIRECTORY_DEPTH,
                                              false,
                                              C_DEFAULT_DIRECTORY_DEPTH);

                // Read the search direction property.
                l_searchDirectionPropName = MessageFormat.format(C_PROPERTY_SEARCH_DIRECTION,
                                                                 new Object[]{l_distrThreadIdStr});
                l_searchDirection = getProperty(l_searchDirectionPropName,
                                                C_OVERALL_PROPERTY_SEARCH_DIRECTION,
                                                false,
                                                C_NORMAL_SEARCH_DIRECTION);

                // Read the poll time property.
                l_pollTimePropName = MessageFormat.format(C_PROPERTY_POLL_TIME,
                                                          new Object[]{l_distrThreadIdStr});
                l_pollTime = getLongProperty(l_pollTimePropName,
                                             C_OVERALL_PROPERTY_POLL_TIME,
                                             false,
                                             C_DEFAULT_POLL_TIME);

                // Create the file search and the file handler instances.
                // Each thread shall have its own instances to avoid multi-threading problems.
                l_fileSearch     = new FileSearch();
                l_chsFileHandler = new ChsFileHandler(i_logger, i_ppasProperties, i_chsInstance);

                // Create the distributor thread.
                i_distributorThreadArr[l_ix] = new DistributorThread(l_distrThreadId,
                                                                     l_rootDir,
                                                                     l_dirDepth,
                                                                     null,
                                                                     l_searchDirection,
                                                                     l_pollTime,
                                                                     l_fileSearch,
                                                                     l_chsFileHandler);
                i_distributorThreadArr[l_ix].setStopRequested(false);
                i_distributorThreadArr[l_ix].setPauseRequested(false);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10080, this,
                            "Leaving " + C_METHOD_createDistributionThreads);
        }
    }

    /**
     * Get a specified long property.
     * 
     * @param p_property         The specific property.
     * @param p_overallProperty  The overall property (could be <code>null</code>).
     * @param p_mandatory        True if this property must exist.
     * @param p_defaultValue     The default value.
     * @return  The value of the property if defined, otherwise the default value.
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private long getLongProperty(String  p_property,
                                 String  p_overallProperty,
                                 boolean p_mandatory,
                                 long    p_defaultValue) throws PpasConfigException
    {
        return Long.parseLong(getProperty(p_property,
                                          p_overallProperty,
                                          p_mandatory,
                                          Long.toString(p_defaultValue)));
    }

    /**
     * Get a specified short property.
     * 
     * @param p_property         The specific property.
     * @param p_overallProperty  The overall property (could be <code>null</code>).
     * @param p_mandatory        True if this property must exist.
     * @param p_defaultValue     The default value.
     * @return  The value of the property if defined, otherwise the default value.
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private short getShortProperty(String  p_property,
                                   String  p_overallProperty,
                                   boolean p_mandatory,
                                   short   p_defaultValue) throws PpasConfigException
    {
        return Short.parseShort(getProperty(p_property,
                                            p_overallProperty,
                                            p_mandatory,
                                            Short.toString(p_defaultValue)));
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getProperty = "getProperty";
    /**
     * Get a specified property.
     * 
     * @param p_property         The specific property.
     * @param p_overallProperty  The overall property (could be <code>null</code>).
     * @param p_mandatory        <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue     The default value (should be <code>null</code> for a mandatory property).
     * @return  The value of the property if defined, otherwise the default value.
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private String getProperty(String  p_property,
                               String  p_overallProperty,
                               boolean p_mandatory,
                               String  p_defaultValue) throws PpasConfigException
    {
        String              l_propValue        = null;
        String              l_overallPropValue = null;
        String              l_defaultValue     = null;
        PpasConfigException l_ppasConfigEx     = null;
        int                 l_propertyNameIx   = -1;
        String              l_propertyName     = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10090, this,
                            "Entering " + C_METHOD_getProperty);
        }

        l_defaultValue = (p_mandatory  ?  null : p_defaultValue);

        if (i_ppasProperties != null)
        {
            l_overallPropValue = i_ppasProperties.getTrimmedProperty(p_overallProperty, l_defaultValue);
            l_propValue        = i_ppasProperties.getTrimmedProperty(p_property, l_overallPropValue);
                                                    
        }
        else
        {
            l_propValue = l_defaultValue;
        }

        if (p_mandatory && l_propValue == null)
        {
            // ***ERROR: This is a missing mandatory property, throw a 'PpasConfigException'.
            l_propertyNameIx = p_property.lastIndexOf(".");
            l_propertyName = p_property.substring(l_propertyNameIx + 1);
            l_ppasConfigEx =
                new PpasConfigException(C_CLASS_NAME,
                                        C_METHOD_getProperty,
                                        10092,
                                        this,
                                        null,
                                        0,
                                        ConfigKey.get().missingConfigValue(l_propertyName,
                                                                           C_CLASS_NAME));
            i_logger.logMessage(l_ppasConfigEx);
            throw l_ppasConfigEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10100, this,
                            "Leaving " + C_METHOD_getProperty);
        }

        return l_propValue;
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_processFiles = "processFiles";
    /**
     * Searches after and processes found Call Data Record (CDR) files.
     * Returns <code>true</code> if any file was found, <code>false</code> otherwise.
     * Represents one lifecycle for the ChsDistributor.
     * 
     * @param p_distrThread Thread processing the files.
     * @return <code>true</code> if any file was found, <code>false</code> otherwise.
     * @throws CallHistoryException  if the request to the <code>ChsFileProcessorPool</code> returns
     *                               without a <code>ChsFileProcessor</code> object or a file cannot be found.
     */
    private boolean processFiles(DistributorThread p_distrThread)
        throws CallHistoryException
    {
        ChsFileHandler          l_chsFileHandler     = null;
        boolean                 l_found              = false;
        Vector                  l_fileListVec        = null;
        Vector                  l_reversefileListVec = null;
        File                    l_file               = null;
        ChsFile                 l_chsFile            = null;
        ChsFileProcessor        l_chsFileProcessor   = null;
        boolean                 l_renameSuccessful   = false;
        CallHistoryExceptionKey l_chsExKey           = null;
        CallHistoryException    l_chsEx              = null;
        

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10110, this,
                            "Entering " + C_METHOD_processFiles);
        }

        if (p_distrThread.isRunning() &&
            !p_distrThread.isPauseRequested() &&
            !p_distrThread.isStopRequested())
        {
            try
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10112, this,
                                    C_METHOD_processFiles + " -- " +
                                    "Looking for file in directory: '" + p_distrThread.getSearchDirectory() +
                                    "',  file type: '" + p_distrThread.i_fileType + "'");
                }

                l_chsFileHandler = p_distrThread.i_chsFileHandler;

                // Search for files in the configured root directory.
                l_fileListVec =
                    p_distrThread.i_fileSearch.getFileList(new File(p_distrThread.getSearchDirectory()),
                                                           p_distrThread.i_fileType,
                                                           true,
                                                           p_distrThread.i_dirDepth);
                if (p_distrThread.i_searchDirection.equals(C_REVERSE_SEARCH_DIRECTION))
                {
                    l_reversefileListVec = new Vector(l_fileListVec.size());
                    for (int l_ix = (l_fileListVec.size() - 1); l_ix >= 0; l_ix--)
                    {
                        l_reversefileListVec.add(l_fileListVec.elementAt(l_ix));
                    }
                    l_fileListVec = l_reversefileListVec;
                }

                // Process found files.
                l_found = !l_fileListVec.isEmpty();
                for (int l_ix = 0; l_ix < l_fileListVec.size(); l_ix++)
                {
                    if (p_distrThread.isRunning() &&
                        !p_distrThread.isPauseRequested() &&
                        !p_distrThread.isStopRequested())
                    {
                        l_file = (File)l_fileListVec.elementAt(l_ix);
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME, 10114, this,
                                            "Process file: '" + l_file.getAbsolutePath() + "'");
                        }
                        if (l_file.exists())
                        {
                            l_chsFile = l_chsFileHandler.create(l_file);
                            incrementCounter(i_noOfFoundFiles);
                            if (l_chsFile != null)
                            {
                                // It was a CDR file with a valid name. Let's process the file.
                                // Obtain a free file processor from the processor pool.
                                l_chsFileProcessor = null;
                                while (l_chsFileProcessor == null)
                                {
                                    try
                                    {
                                        l_chsFileProcessor =
                                            i_chsFileProcessorPool.getProcessor(i_procFromPoolTimeout);
                                    }
                                    catch (CallHistoryTimeoutException l_chsTimeoutEx)
                                    {
                                        // No file processor was obtained within the time-out time.
                                        l_chsFileProcessor = null;
                                        if (i_chsFileProcessorPool.getMaxSize() == 0)
                                        {
                                            // There will never be any processors in the pool,
                                            // i.e. meaningless to continue.
                                            l_chsExKey = CallHistoryKey.get().noPoolEntries();
                                            l_chsEx = new CallHistoryTimeoutException(C_CLASS_NAME,
                                                                                      C_METHOD_processFiles,
                                                                                      10115,
                                                                                      this,
                                                                                      null,
                                                                                      0L,
                                                                                      l_chsExKey,
                                                                                      l_chsTimeoutEx);

                                            i_logger.logMessage(l_chsEx);
                                            throw l_chsEx;
                                        }
                                        
                                        if (p_distrThread.isPauseRequested() ||
                                                p_distrThread.isStopRequested())
                                        {
                                            // Requested to pause or stop, Stop trying to obtain
                                            // a file processor.
                                            break;
                                        }
                                    }
                                } //End of 'while' loop.

                                l_renameSuccessful =
                                    l_chsFileHandler.renameToInProgress(l_chsFile, p_distrThread.i_threadId);
                                if (l_renameSuccessful)
                                {
                                    // Add the distributor thread as a file process listener.
                                    l_chsFileProcessor.addChsFileProcessorListener(p_distrThread);

                                    l_chsFileProcessor.processFile(l_chsFile,
                                                                   l_chsFileHandler,
                                                                   p_distrThread.i_threadId);
                                }
                                else
                                {
                                    // Failed to rename the file to the 'in progress' state,
                                    // most likely gathered by another thread.
                                    // Just log it and continue with next file.
                                    if (PpasDebug.on)
                                    {
                                        PpasDebug.print(PpasDebug.C_LVL_HIGH,
                                                        PpasDebug.C_APP_SERVICE,
                                                        PpasDebug.C_ST_TRACE,
                                                        C_CLASS_NAME, 10116, this,
                                                        C_METHOD_processFiles + ", " +
                                                        p_distrThread.getThreadName() +
                                                        " -- ***ERROR: Failed to rename the file '" +
                                                        l_file.getAbsolutePath() +
                                                        "' to 'in progress', " +
                                                        "most likely gathered by another thread.");
                                    }
                                    l_chsExKey =
                                        CallHistoryKey.get().cdrFileRenameFailed(l_file.getAbsolutePath(),
                                                                                 "in progress");
                                    l_chsEx =
                                        new CallHistoryFileNotFoundException(C_CLASS_NAME,
                                                                             C_METHOD_processFiles,
                                                                             10118,
                                                                             this,
                                                                             null,
                                                                             0L,
                                                                             l_chsExKey,
                                                                             null);
                                    i_logger.logMessage(l_chsEx);

                                    // Put the file processor back into the pool.
                                    i_chsFileProcessorPool.putProcessor(l_chsFileProcessor);
                                }
                            }
                            else
                            {
                                // The found file has an invalid filename.
                                incrementCounter(i_noOfFilesWithInvName);
                                
                                // Move it to the specified directory for such files.
                                l_chsFileHandler.moveToInvFilenameDir(l_file);
                            }
                        }
                        else
                        {
                            // The picked file doesn't exist, most likely gathered by another thread.
                            // Just log it and continue with next file.
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_HIGH,
                                                PpasDebug.C_APP_SERVICE,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME, 10120, this,
                                                C_METHOD_processFiles + ", " + p_distrThread.getThreadName() +
                                                " -- The picked file '" + l_file.getAbsolutePath() +
                                                "' doesn't exist, most likely gathered by another thread.");
                            }
                            l_chsExKey =
                                CallHistoryKey.get().cdrFileNotFound(l_file.getAbsolutePath());
                            l_chsEx =
                                new CallHistoryFileNotFoundException(C_CLASS_NAME,
                                                                     C_METHOD_processFiles,
                                                                     10122,
                                                                     this,
                                                                     null,
                                                                     0L,
                                                                     l_chsExKey,
                                                                     null);
                            i_logger.logMessage(l_chsEx);
                        }
                    }
                    else
                    {
                        // The process is requested to be paused or stopped.
                        break;
                    }
                } //End of 'for' loop.
            }
            catch (FileSearchException l_fileSearchEx)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_HIGH,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10124, this,
                                    C_METHOD_processFiles + ", " + p_distrThread.getThreadName() +
                                    " -- ***ERROR: An error occurred  during the file search operation: " +
                                    l_fileSearchEx);
                }
                l_chsEx = new CallHistoryFileNotFoundException(C_CLASS_NAME,
                                                               C_METHOD_processFiles,
                                                               10126,
                                                               this,
                                                               null,
                                                               0L,
                                                               CallHistoryKey.get().rootDirNotFound(
                                                                                 p_distrThread.i_rootDirName),
                                                               l_fileSearchEx);
                i_logger.logMessage(l_chsEx);
                throw l_chsEx;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10130, this,
                            "Leaving " + C_METHOD_processFiles);
        }
        return l_found;
    }


    /**
     * Generic method for incrementing a <code>CounterInstrument</code> counter.
     * 
     * @param p_counterInstrument  an instance of the <code>CounterInstrument</code> class
     *                             that is to be incremented.
     */
    private void incrementCounter(CounterInstrument p_counterInstrument)
    {
        if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
        {
            if (i_instrumentSet.i_isEnabled)
            {
                p_counterInstrument.increment();
            }
        }
    } // end of incrementCounter(...)


    //==========================================================================
    // Inner class(es).
    //==========================================================================
    /**
     * The purpose of this <code>DistributorThread</code> (inner) class defined within
     * the <code>ChsFileDistributor</code> class is to run the Call History data files
     * distributing process.
     * It extends the <code>ThreadObject</code> class in order to run the process in
     * a separate thread.
     */
    private final class DistributorThread extends ThreadObject implements ChsFileProcessListener
    {
        //=====================================================================
        // Private class level constant(s).
        //=====================================================================
        /** The name of the current class to be used in calls to middleware. Value is {@value}. */
        private static final String C_CLASS_NAME_thread = "ChsFileDistributor.DistributorThread";

        //=====================================================================
        // Private instance attribute(s).
        //=====================================================================
        /** The current thread id. */
        private int            i_threadId        = -1;

        /** The root directory into which the new CDR files are placed. */
        private String         i_rootDirName     = null;

        /** The directory from which the search for CDR files starts. */
        private String         i_searchDirectory = null;

        /** The directory depth in which to search for the files. */
        private short          i_dirDepth        = 0;

        /** The file type to search for. */
        private String         i_fileType        = null;

        /** The search direction. */
        private String         i_searchDirection = null;

        /** The poll time (the time to wait between the processing cycles). */
        private long           i_pollTime        = 0;

        /** The "distribution process thread is running" indicator. */
        private boolean        i_running         = false;

        /** The "distribution process thread is requested to stop" indicator. */
        private boolean        i_stopRequested   = false;

        /** The "distribution process thread is requested to pause" indicator. */
        private boolean        i_pauseRequested  = false;

        /** The <code>FileSearch</code> object.
         *  Each thread has its own instances to avoid multi-threading problems. */
        private FileSearch     i_fileSearch      = null;

        /** The <code>ChsFileHandler</code> object.
         *  Each thread has its own instances to avoid multi-threading problems. */
        private ChsFileHandler i_chsFileHandler  = null;


        //=====================================================================
        // Constructor(s).
        //=====================================================================
        /**
         * Constructs an instance of the <code>DistributorThread</code> class using
         * the passed parameters.
         * 
         * @param p_threadId         the unique thread id.
         * @param p_rootDirName      the root directory from which the search of CDR files starts.
         * @param p_dirDepth         the directory depth in which to search for the files.
         * @param p_fileType         the file type to search for (searches for all files if 'null').
         * @param p_searchDirection  the search direction ('N'(normal) or 'R'(reverse)).
         * @param p_pollTime         the poll time (the time to wait between the processing cycles
         *                           if no files were found during the previous cycle).
         * @param p_fileSearch       the <code>FileSearch</code> object.
         * @param p_chsFileHandler   the <code>ChsFileHandler</code> object.
         * 
         * @throws PpasConfigException  if it fails to constructs an instance of the
         *                              <code>DistributorThread</code> class.
         */
        private DistributorThread(int            p_threadId,
                                  String         p_rootDirName,
                                  short          p_dirDepth,
                                  String         p_fileType,
                                  String         p_searchDirection,
                                  long           p_pollTime,
                                  FileSearch     p_fileSearch,
                                  ChsFileHandler p_chsFileHandler)
            throws PpasConfigException
        {
            super();
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                C_CLASS_NAME_thread, 30000, this,
                                "Constructing " + C_CLASS_NAME_thread);
            }

            i_threadId        = p_threadId;
            i_rootDirName     = p_rootDirName;
            i_dirDepth        = p_dirDepth;
            i_fileType        = p_fileType;
            i_searchDirection = p_searchDirection;
            i_pollTime        = p_pollTime;
            i_fileSearch      = p_fileSearch;
            i_chsFileHandler  = p_chsFileHandler;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME_thread, 30002, this,
                                "Passed parameters: " +
                                "i_threadId="         + i_threadId +
                                ", i_rootDirName="    + i_rootDirName +
                                ", i_dirDepth="       + i_dirDepth +
                                ", i_fileType="       + i_fileType +
                                ", i_searchDirection" + i_searchDirection +
                                ", i_pollTime="       + i_pollTime);
            }
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                                C_CLASS_NAME_thread, 30010, this,
                                "Constructed " + C_CLASS_NAME_thread);
            }
        }

        //=====================================================================
        // Public method(s).
        //=====================================================================
        /** The name of the following method. Used for calls to middleware. Value is (@value). */
        private static final String C_METHOD_doRun = "doRun";
        /**
         * Implements the file search functionality for the Call History data files,
         * in order to locate new files to process.
         * It is an implementation of the abstract <code>doRun</code> method of
         * the <code>ThreadObject</code> class.
         */
        public void doRun()
        {
            boolean l_continue     = true;
            boolean l_filesFound   = false;
            boolean l_firstRun     = true;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                C_CLASS_NAME_thread, 30020, this,
                                "Entering " + C_METHOD_doRun);
            }

            setRunning(true);
            while (l_continue)
            {
                try
                {
                    // Check for CDR files to be recovered if this is the first run.
                    if (l_firstRun)
                    {
                        i_searchDirectory = i_chsFileHandler.getFileInProgressDir();
                        processFiles(this);
                        l_firstRun = false;
                    }
                    
                    i_searchDirectory = i_rootDirName;
                    l_filesFound = processFiles(this);
                }
                catch (PpasException l_Ex1)
                {
                    ChsFileDistributor.this.terminateProcess();
                    setStopRequested(true);
                }
                synchronized(this)
                {
                    if (!i_stopRequested)
                    {
                        // The process is NOT requested to be stopped.
                        if (!i_pauseRequested)
                        {
                            // The process is NOT requested to be paused.
                            if (!l_filesFound)
                            {
                                // No files found during the latest process cycle,
                                // let's wait for some time before next cycle.
                                if (PpasDebug.on)
                                {
                                    PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                                    PpasDebug.C_ST_TRACE,
                                                    C_CLASS_NAME_thread, 30022, this,
                                                    "No files found during the latest process cycle, " +
                                                    "let's wait for " + i_pollTime + " ms.");
                                }
                                try
                                {
                                    this.wait(i_pollTime);
                                }
                                catch (InterruptedException l_Ex)
                                {
                                    // Nothing to do.
                                    l_Ex = null;
                                }
                            }
                        }
                        else
                        {
                            // The process is requested to be paused,
                            // let's wait until resumed or stopped.
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME_thread, 30024, this,
                                                "The process is requested to be paused, " +
                                                "let's wait until resumed or stopped.");
                            }

                            try
                            {
                                this.wait();
                            }
                            catch (InterruptedException l_Ex)
                            {
                                // Nothing to do.
                                l_Ex = null;
                            }
                        }
                    }
                    else
                    {
                        // The process is requested to be stopped.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME_thread, 30026, this,
                                            "The process is requested to be stopped.");
                        }
                        l_continue = false;
                        i_running = false;
                    }
                } //End of 'synchronized (this)'
            } //End of 'while (l_continue)'

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                C_CLASS_NAME_thread, 30030, this,
                                "Leaving " + C_METHOD_doRun);
            }
        }

        /** The name of the following method. Used for calls to middleware. Value is (@value). */
        private static final String C_METHOD_handleEvent = "handleEvent";
        /**
         * Handles the given Call History CDR files process event.
         * 
         * @param p_event  the Call History CDR files process event.
         */
        public void handleEvent(ChsFileProcessEvent p_event)
        {
            Object l_source = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                C_CLASS_NAME_thread, 30040, this,
                                "Entering " + C_METHOD_handleEvent);
            }

            l_source = p_event.getSource();
            if (l_source != null && l_source instanceof ChsFileProcessor)
            {
                if (p_event.getType() == ChsFileProcessEvent.C_EVENT_TYPE_FATAL_ERROR)
                {
                    // Remove this distributor thread as a file process listener.
                    ((ChsFileProcessor)l_source).removeChsFileProcessorListener(this);

                    // Put the processor back into the pool.
                    i_chsFileProcessorPool.putProcessor((ChsFileProcessor)l_source);

                    // Stop all further processing.
                    ChsFileDistributor.this.terminateProcess();
                }
                else if (p_event.getType() == ChsFileProcessEvent.C_EVENT_TYPE_PROCESS_COMPLETED)
                {
                    // Remove this distributor thread as a file process listener.
                    ((ChsFileProcessor)l_source).removeChsFileProcessorListener(this);

                    // The process is completed, put processor back into the pool.
                    i_chsFileProcessorPool.putProcessor((ChsFileProcessor)l_source);
                }
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                C_CLASS_NAME_thread, 30050, this,
                                "Leaving " + C_METHOD_handleEvent);
            }
        }

        /**
         * Returns the directory from which the search for CDR files should start.
         * 
         * @return the directory from which the search for CDR files should start.
         */
        public String getSearchDirectory()
        {
            return i_searchDirectory;
        }

        /**
         * Returns <code>true</code> if the distribution thread is running,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_running</code> attribute is returned (assuming that
         *     a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the distribution thread is running,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isRunning()
        {
            return i_running;
        }

        /**
         * Sets the running status of the distribution thread.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_running</code> attribute is visible for other threads
         *     (assuming that the reading of this value also is done within a <code>synchronized</code>
         *     context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_running  If <code>true</code> the distribution thread is running.
         */
        public synchronized void setRunning(boolean p_running)
        {
            i_running = p_running;
        }

        /**
         * Returns <code>true</code> if the distribution thread is requested to pause,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_pauseRequested</code> attribute is returned (assuming
         *     that a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the distribution thread is requested to pause,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isPauseRequested()
        {
            return i_pauseRequested;
        }

        /**
         * Requests the distribution thread to pause if the given
         * <code>boolean</code> parameter is <code>true</code>.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_pauseRequested</code> attribute is visible for other
         *     threads (assuming that the reading of this value also is done within a
         *     <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_pauseRequested  If <code>true</code> the distribution thread is requested to pause.
         */
        public synchronized void setPauseRequested(boolean p_pauseRequested)
        {
            i_pauseRequested = p_pauseRequested;

            // Wake up the processing thread if it is waiting on this object's monitor.
            this.notify();
        }

        /**
         * Returns <code>true</code> if the distribution thread is requested to stop,
         * <code>false</code> otherwise.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_stopRequested</code> attribute is returned (assuming
         *     that a change of this value also is done within a <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @return <code>true</code> if the distribution thread is requested to stop,
         *         <code>false</code> otherwise.
         */
        public synchronized boolean isStopRequested()
        {
            return i_stopRequested;
        }

        /**
         * Requests the distribution thread to stop if the given
         * <code>boolean</code> parameter is <code>true</code>.
         * <br>
         * NB: This method needs to be <code>synchronized</code> in order to assure that the most
         *     recently written value of the <code>i_stopRequested</code> attribute is visible for other
         *     threads (assuming that the reading of this value also is done within a
         *     <code>synchronized</code> context).
         *     This is especially important if the thread that change the value and the thread that reads
         *     the value are running in different CPUs (if each CPU has its own memory cache).
         * 
         * @param p_stopRequested  If <code>true</code> the distribution thread is requested to stop.
         */
        public synchronized void setStopRequested(boolean p_stopRequested)
        {
            i_stopRequested = p_stopRequested;

            // Wake up the processing thread if it is waiting on this object's monitor.
            this.notify();
        }

        //=====================================================================
        // Protected method(s).
        //=====================================================================
        /**
         * Returns a meaningful name for the thread.
         * It is an implementation of the abstract <code>getThreadName</code> method of
         * the <code>ThreadObject</code> class.
         * 
         * @return a meaningful name for the thread.
         */
        protected String getThreadName()
        {
            return (C_CLASS_NAME_thread + "_" + i_threadId + "-" + i_searchDirection);
        }
    } // End of the 'DistributorThread' inner class.
} // End of the 'ChsFileDistributor' class.
