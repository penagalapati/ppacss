////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryPlatformException.java
//      DATE            :       26-April-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Exception indicating that a platform command failure
//                              occurred in the Call History CDR file processing.
//                              The exception key should indicate where the timeout
//                              occured.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.exceptions;

import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;


/**
 * Exception indicating that a platform command failure occurred
 * in the Call History CDR file processing.
 * The exception key should indicate where the timeout occured.
 */
public class CallHistoryPlatformException extends CallHistoryException
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CallHistoryPlatformException";


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an instance of the <code>CallHistoryPlatformException</code> class.
     * The exception is identified by the base name of the resource bundle defining
     * the message texts associated with a class of related exceptions and by the key
     * into that resource bundle (this identifies the exception type but not a particular
     * instance of the exception object).
     * 
     * @param p_callingClass     The name of the class invoking this constructor.
     * @param p_callingMethod    The name of the method from which this constructor was invoked.
     * @param p_stmtNo           Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject    The object from which this constructor was invoked,
     *                           <code>null</code> if the constructor was invoked from a static method.
     * @param p_ppasRequest      The request which gave rise to this exception.
     * @param p_flags            Bitmask flag ... if bit C_FLAG_INC_STACK_TRACE is set,
     *                           then a stack trace will be output when some exception objects are logged.
     * @param p_exceptionKey     Exception key/parameters defining this exception.
     * @param p_sourceException  An earlier exception which gave rise to this exception.
     */
    public CallHistoryPlatformException(String       p_callingClass,
                                        String       p_callingMethod,
                                        int          p_stmtNo,
                                        Object       p_callingObject,
                                        PpasRequest  p_ppasRequest,
                                        long         p_flags,
                                        ExceptionKey p_exceptionKey,
                                        Exception    p_sourceException)
    {
        super(p_callingClass,
              p_callingMethod,
              p_stmtNo,
              p_callingObject,
              p_ppasRequest,
              p_flags,
              p_exceptionKey,
              p_sourceException);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
}
