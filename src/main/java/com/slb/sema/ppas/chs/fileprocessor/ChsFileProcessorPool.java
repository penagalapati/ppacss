////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ChsFileProcessorPool.java
//    DATE            :       20-March-2006
//    AUTHOR          :       Remi Isaacs
//    REFERENCE       :       PRD_ASCS00_DEV_SS_100
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       The Chs File Processor Pool for CDR.
//
///////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
///////////////////////////////////////////////////////////////////////////////////
//    DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-------------+------------+---------------------------------+--------------------
//02/10/06     | A Kutthan  | Various changes for CA66        | PpasLon#2658/10082
//-------------+------------+---------------------------------+--------------------
///////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.fileprocessor;

import java.io.File;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Vector;

import com.slb.sema.ppas.chs.exceptions.CallHistoryKey;
import com.slb.sema.ppas.chs.exceptions.CallHistoryTimeoutException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.pool.DynamicPool;
import com.slb.sema.ppas.util.pool.Poolable;

/**
 * Implements a pool of Call History CDR File Processors.
 */
public class ChsFileProcessorPool extends DynamicPool
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                           = "ChsFileProcessorPool";


    // JDBC cofiguration constants.
    /** The property prefix for <code>JdbcConnectionPool</code> properties. Value is {@value}. */
    private static final String C_JDBC_PROPERTY_PREFIX                 =
        "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool";

    /** The username property. Value is {@value}. */
    private static final String C_JDBC_PROP_USERNAME                   = C_JDBC_PROPERTY_PREFIX + ".username";
    
    /** The password property. Value is {@value}. */
    private static final String C_JDBC_PROP_PASSWORD                   = C_JDBC_PROPERTY_PREFIX + ".password";

    /** The database service property. Value is {@value}. */
    private static final String C_JDBC_PROP_DB_SERVICE                 = C_JDBC_PROPERTY_PREFIX +
                                                                         ".dbService";

    // Processor pool cofiguration constants.
    /** The property prefix for <code>ChsFileProcessorPool</code> properties. Value is {@value}. */
    private static final String C_CHS_PROC_POOL_PROPERTY_PREFIX        =
        "com.slb.sema.ppas.chs.fileprocessor.ChsFileProcessorPool";

    /** The processor pool min size property. Value is {@value}. */
    private static final String C_PROPERTY_POOL_MIN_SIZE               = C_CHS_PROC_POOL_PROPERTY_PREFIX + 
                                                                         ".minSize";
    
    /** The processor pool min size default value. Value is {@value}. */
    private static final int    C_DEFVAL_POOL_MIN_SIZE                 = 2;
    
    /** The processor pool max size property. Value is {@value}. */
    private static final String C_PROPERTY_POOL_MAX_SIZE               = C_CHS_PROC_POOL_PROPERTY_PREFIX + 
                                                                         ".maxSize";
    
    /** The processor pool max size default value. Value is {@value}. */
    private static final int    C_DEFVAL_POOL_MAX_SIZE                 = 10;
    
    /** The processor pool min free property. Value is {@value}. */
    private static final String C_PROPERTY_POOL_MIN_FREE               = C_CHS_PROC_POOL_PROPERTY_PREFIX + 
                                                                         ".minFree";
    
    /** The processor pool min free default value. Value is {@value}. */
    private static final int    C_DEFVAL_POOL_MIN_FREE                 = 4;
    
    /** The processor pool max free property. Value is {@value}. */
    private static final String C_PROPERTY_POOL_MAX_FREE               = C_CHS_PROC_POOL_PROPERTY_PREFIX + 
                                                                         ".maxFree";
    
    /** The processor pool max free default value. Value is {@value}. */
    private static final int    C_DEFVAL_POOL_MAX_FREE                 = 10;
    

    // SQL*Loader command-line cofiguration constants.
    /** The property prefix for the SQL*Loader command-line properties. Value is {@value}. */
    private static final String C_SQL_LOADER_PROPERTY_PREFIX           =
        "com.slb.sema.ppas.chs.fileprocessor.sqlldr";

    /** The control file property for the control filename extension. Value is {@value}. */
    private static final String C_PROPERTY_CONTROL_FILE_EXT            = ".controlFileExtension";
    
    /** The control file property for the control file stem. Value is {@value}. */
    private static final String C_PROPERTY_CONTROL_FILE_STEM            = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".controlFileStem";
    
    /** The default value of the control file property for the control file stem. Value is {@value}. */
    private static final String C_DEFVAL_CONTROL_FILE_STEM              =
        "${ASCS_LOCAL_ROOT}/conf/chs/chdr_call_history_data_";
    
    /** The default value of the control file property for the control file extension. Value is {@value}. */
    private static final String C_DEFVAL_CONTROL_FILE_EXT               = ".ctl";
    
    /** The log directory property. Value is {@value}. */
    private static final String C_PROPERTY_LOG_DIR                     = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".logDir";
    
    /** The default value of the log directory property. Value is {@value}. */
    private static final String C_DEFVAL_LOG_DIR                       = "${ASCS_LOCAL_ROOT}/chsfiles/log";
    
    /** The bad directory property. Value is {@value}. */
    private static final String C_PROPERTY_BAD_DIR                     = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".badDir";
    
    /** The default value of the bad directory property. Value is {@value}. */
    private static final String C_DEFVAL_BAD_DIR                       = "${ASCS_LOCAL_ROOT}/chsfiles/bad";
    
    /** The name of the discard directory property. Value is {@value}. */
    private static final String C_PROPERTY_DISCARD_DIR                 = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".discardDir";
    
    /** The default value of the discard directory property. Value is {@value}. */
    private static final String C_DEFVAL_DISCARD_DIR                   =
        "${ASCS_LOCAL_ROOT}/chsfiles/discard";

    /** The OS Standard Output log directory property. Value is {@value}. */
    private static final String C_PROPERTY_OS_LOG_DIR                  = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".osLogDir";
    
    /** The default value of the OS Standard Output log directory property. Value is {@value}. */
    private static final String C_DEFVAL_OS_LOG_DIR                    = "${ASCS_LOCAL_ROOT}/chsfiles/os/log";
    
    /** The OS Standard Error log directory property. Value is {@value}. */
    private static final String C_PROPERTY_OS_ERR_DIR                  = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".osErrDir";
    
    /** The default value of the OS Standard Error log directory property. Value is {@value}. */
    private static final String C_DEFVAL_OS_ERR_DIR                    = "${ASCS_LOCAL_ROOT}/chsfiles/os/err";
    
    /** The buffer size (number of bytes to read and process before a commit is required) property name.
     * Value is {@value}. */
    private static final String C_PROPERTY_BUFFER_SIZE                 = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".bufferSize";

    /** The buffer size default value in number of bytes. Value is {@value}. */
    private static final int    C_DEFVAL_BUFFER_SIZE                   = 65536; // 64 KByte

    /** The buffer size min value in number of bytes. Value is {@value}. */
    private static final int    C_MINVAL_BUFFER_SIZE                   = C_DEFVAL_BUFFER_SIZE;

    /** The buffer size max value in number of bytes. Value is {@value}. */
    private static final int    C_MAXVAL_BUFFER_SIZE                   = 20971520; // = 20 MByte

    /** The supress message (comma separated list) property name. Value is {@value}. */
    private static final String C_PROPERTY_SUPRESS_MESSAGE             = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".supressMessage";

    /** The supress message (comma separated list) default value. Value is {@value}. */
    private static final String C_DEFVAL_SUPRESS_MESSAGE               = "HEADER, FEEDBACK";


    // Other SQL*Loader cofiguration constants.
    /** Property defining list of DBMS data errors. Value is {@value}. */
    private static final String C_PROPERTY_DBMS_DATA_ERRORS = C_SQL_LOADER_PROPERTY_PREFIX +
                                                              ".dbmsDataErrors";

    /** The too big failure proportion property name. Value is {@value}. */
    private static final String C_PROPERTY_TOO_BIG_FAILURE_PROPORTION  = C_SQL_LOADER_PROPERTY_PREFIX +
                                                                         ".tooBigProportion";

    /** The too big failure proportion default value (per cent). Value is {@value}. */
    private static final int    C_DEFVAL_TOO_BIG_FAILURE_PROPORTION    = 0; // per cent(%)

    /** The too big failure proportion min value (per cent). Value is {@value}. */
    private static final int    C_MINVAL_TOO_BIG_FAILURE_PROPORTION    = 0; // per cent(%)

    /** The too big failure proportion max value (per cent). Value is {@value}. */
    private static final int    C_MAXVAL_TOO_BIG_FAILURE_PROPORTION    = 100; // per cent(%)


    //==========================================================================
    // Private instance attributes(s).
    //==========================================================================
    /** The <code>PpasProperties</code> object. */
    private PpasProperties    i_ppasProperties          = null;

    /** The <code>Logger</code> object. */
    private Logger            i_logger                  = null;

    /** The session context. */
    private PpasContext       i_ppasContext             = null;

    /** The <code>InstrumentManager</code> object. */
    private InstrumentManager i_instrumentManager       = null;
    
    /** The database user id. */
    private String            i_userid                  = null;

    /** The database password. */
    private String            i_password                = null;

    /** The database service reference. */
    private String            i_dbService               = null;

    /** The database connection string to be used by the Oracle SQL*Loader utility. */
    private String            i_oracleConnectionStr     = null;
    
    /** The SQL Loader control file. */
    private String            i_controlFileStem         = null;
    
    /** The SQL Loader control file extension. */
    private String            i_controlFileExt         = null;

    /** The SQL Loader log directory. */
    private String            i_logDir                  = null;

    /** The SQL Loader bad directory. */
    private String            i_badDir                  = null;

    /** The SQL Loader discard directory. */
    private String            i_discardDir              = null;

    /** The Standard Output directory used by the shell command running on the current OS. */
    private String            i_osLogDir                = null;

    /** The Standard Error directory used by the shell command running on the current OS. */
    private String            i_osErrDir                = null;

    /** The SQL Loader buffer size in bytes. Used as both the read buffer size and bind array size. */
    private int               i_bufferSize              = C_DEFVAL_BUFFER_SIZE;

    /** The SQL Loader log file suppressed messages (header,feedback,errors,discards,partitions). */
    private String            i_suppressMsg             = null;

    /** Counter used to give each file processor a unique id. */
    private int               i_processorInstanceCount  = 0;

    /** Set of DBMS error codes that are recognised as data errors. */
    private HashSet           i_dbmsDataErrors;
    
    /** The too big failure proportion value (percentage). */
    private int               i_tooBigFailureProportion = 0;

    /** Minimum number of rows needed to trigger 'too many errors' exception. */
    private int               i_minRowsTooMany = 10;

    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs a new instance of the <code>ChsFileProcessorPool</code> class.
     * 
     * @param p_ppasContext        The session context.
     * @param p_logger             The logger instance used by the Call History Server process.
     * @param p_instrumentManager  The <code>InstrumentManager</code> used for Admin Instrumentation.
     * @param p_name               The name of the pool.
     * @param p_ppasProperties     The <code>PpasProperties</code> object.
     * 
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    public ChsFileProcessorPool(PpasContext         p_ppasContext,
                                Logger              p_logger,
                                InstrumentManager   p_instrumentManager,
                                String              p_name,
                                PpasProperties      p_ppasProperties)
        throws PpasConfigException
    {
        super(p_instrumentManager, p_logger, p_name, p_ppasProperties);
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        i_ppasContext       = p_ppasContext;
        i_logger            = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_ppasProperties    = p_ppasProperties;

        getConfiguration();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //==========================================================================
    // Public method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_create = "create";
    /** Implements the abstract method of the DynamicPool class.
     * @return Poolable Object.
     */
    public Poolable create()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering " + C_METHOD_create);
        }

        ChsFileProcessor l_chsFileProcessor = null;
        try
        {
            l_chsFileProcessor = new ChsFileProcessor(this,
                                                      i_ppasContext,
                                                      i_logger,
                                                      "" + i_processorInstanceCount++,
                                                      i_instrumentManager);
        }
        catch (PpasConfigException l_Ex)
        {
            // LALU: No specific handling?
            l_Ex = null;
            l_chsFileProcessor = null;
        }

        if (l_chsFileProcessor != null)
        {
            l_chsFileProcessor.start();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving " + C_METHOD_create);
        }
        return l_chsFileProcessor;
    }
    
    /** Method name constant used for middleware calls. Value is {@value}. */
    private static final String C_METHOD_getProcessor = "getProcessor";
    /**
     * Returns a CRP File Processor from the pool.
     * 
     * @param p_timeout  the number of milli-seconds to wait to get a free
     * <code>ChsFileProcessor</code> object from the pool.
     * @return A <code>ChsFileProcessor</code> object.
     * @throws CallHistoryTimeoutException  if the request to the pool returns without a thread.
     */
    public ChsFileProcessor getProcessor(int p_timeout) throws CallHistoryTimeoutException
    {
        ChsFileProcessor            l_processor           = null;
        CallHistoryTimeoutException l_chsTimeoutException = null;

        l_processor = (ChsFileProcessor) super.get(p_timeout);

        if (l_processor == null)
        {
            l_chsTimeoutException = new CallHistoryTimeoutException(C_CLASS_NAME,
                                                                    C_METHOD_getProcessor,
                                                                    10040,
                                                                    this,
                                                                    null,
                                                                    0L,
                                                                    CallHistoryKey.get().poolTimeout(),
                                                                    null);

            i_logger.logMessage(l_chsTimeoutException);
            throw l_chsTimeoutException;
        }

        l_processor.setAssigned(true);
        return l_processor;
    }
    
    /**
     * Returns the given CRP File Processor to the pool.
     * @param p_processor : defines the ChsFileProcessor to put in the Pool.
     */
    public void putProcessor(ChsFileProcessor p_processor)
    {
        super.put(p_processor);
        p_processor.setAssigned(false);
    }

    //--------------------------------------------------------------------------
    // Public manager method(s).
    //--------------------------------------------------------------------------
    /**
     * Manager method that returns the SQL*Loader buffer size.
     * 
     * @return  the new SQL*Loader buffer size.
     */
    public String mngMthGetBufferSize()
    {
        return getBufferSizeAsString();
    }
    
    /**
     * Manager method that sets the SQL*Loader buffer size.
     * 
     * @param p_bufferSize  the new SQL*Loader buffer size.
     */
    public void mngMthSetBufferSize(String p_bufferSize)
    {
        i_bufferSize = Integer.parseInt(p_bufferSize);
    }

    /**
     * Manager method that returns the SQL*Loader log file 'suppress message' parameter.
     * 
     * @return the SQL*Loader log file 'suppress message' parameter.
     */
    public String mngMthGetSuppressMessage()
    {
        return getSuppressMsg();
    }

    /**
     * Manager method that returns the too big failure proportion value (per cent).
     * 
     * @return Proportion of rows that must fail to flag the file as a failure.
     */
    public String mngMthGetTooBigFailureProportion()
    {
        return getTooBigFailureProportionAsString();
    }
    
    /**
     * Manager method that sets the too big failure proportion value (per cent).
     * 
     * @param p_tooBigFailureProportion  the new too big failure proportion value (per cent).
     */
    public void mngMthSetTooBigFailureProportion(String p_tooBigFailureProportion)
    {
        int l_tooBigFailureProportion = 0;

        try
        {
            l_tooBigFailureProportion = Integer.parseInt(p_tooBigFailureProportion);

            // Ignore and use the 'old' (current) value if the given value is out of valid range.
            if (l_tooBigFailureProportion >= C_MINVAL_TOO_BIG_FAILURE_PROPORTION &&
                l_tooBigFailureProportion <= C_MAXVAL_TOO_BIG_FAILURE_PROPORTION)
            {
                i_tooBigFailureProportion = l_tooBigFailureProportion;
            }
        }
        catch (NumberFormatException l_nfEx)
        {
            // A non-numeric value was given. Ignore and use the 'old' (current) value.
            l_nfEx = null;
        }
    }
    
    /**
     * Manager method that sets the minimum number of rows needed to trigger a 'too many errors' exception.
     * 
     * @param p_minRows  the new minimum rows value.
     */
    public void mngMthSetMinRows(String p_minRows)
    {
        try
        {
            int l_minRows = Integer.parseInt(p_minRows);

            // Ignore and use the 'old' (current) value if the given value is out of valid range.
            if (l_minRows >= 0)
            {
                i_minRowsTooMany = l_minRows;
            }
        }
        catch (NumberFormatException l_nfEx)
        {
            // A non-numeric value was given. Ignore and use the 'old' (current) value.
            l_nfEx = null;
        }
    }
    
    /**
     * Manager method that gets the minimum number of rows needed to trigger a 'too many errors' exception.
     * 
     * @return the minimum rows value.
     */
    public String mngMthGetMinRows()
    {
        return "" + i_minRowsTooMany;
    }
    
    /**
     * Manager method that sets the SQL*Loader log file 'suppress message' command-line parameter.
     * 
     * <br>The parameter is a comma separated list of one or more of the following key words for the
     *     messages that should be suppressed in the log file:
     * <br>HEADER     - Suppresses the SQL*Loader header message.
     * <br>FEEDBACK   - Suppresses for instance the "commit point reached" message.
     * <br>ERRORS     - Suppresses data error messages that occur when a record generates an Oracle error
     *                  that causes it to be written to the 'bad file'.
     * <br>DISCARDS   - Suppresses the messages for each record written to the discard file.
     * <br>PARTITIONS - Disables writing the per-partition statistics during a direct load of a partitioned
     *                  table.
     * <br>ALL        - All of the suppression values (HEADER, FEEDBACK, ERRORS, DISCARDS, PARTITIONS).
     * 
     * @param p_suppressMsg  the SQL*Loader log file 'suppress message' parameter.
     */
    public void mngMthSetSuppressMessage(String p_suppressMsg)
    {
        i_suppressMsg = p_suppressMsg;
    }

    //==========================================================================
    // Protected method(s).
    //==========================================================================
    /**
     * @return Returns the oracleConnectionStr.
     */
    protected String getOracleConnectionStr()
    {
        return i_oracleConnectionStr;
    }

    /**
     * Determines which SQL*Loader control file should be returned, by:
     * <ul>
     * <li> acquiring current date
     * <li> then, acquiring the current year and checking if it is an even number
     * <li> if so, then the control file table number value will be made up of the current month + 12
     * <li> else, it will be the value of the current month
     * </ul>
     * 
     * @return Returns the i_controlFileStem.
     */
    protected String getControlFile()
    {
        int l_tableNumber = 0;
        i_controlFileStem = null;
        
        try
        {
            i_controlFileStem = getProperty(C_PROPERTY_CONTROL_FILE_STEM, false, C_DEFVAL_CONTROL_FILE_STEM);
        }
        catch(PpasConfigException e)
        {
            throw new PpasSoftwareException(C_CLASS_NAME, C_CLASS_NAME, 30010, null, null, 0,
                                            SoftwareKey.get().unexpectedException(), e);
        }
        
        Calendar l_currentDate = DatePatch.getCalendar();
        l_currentDate.setTime(DatePatch.getDateTimeNow());
        
        int l_currentYear = l_currentDate.get(Calendar.YEAR) - 1900;
        
        int l_currentMonth = l_currentDate.get(Calendar.MONTH);
        l_currentMonth = l_currentMonth + 1;
        
        if(l_currentYear % 2 == 0)
        {
            l_tableNumber = l_currentMonth + 12;
        }
        else
        {
            l_tableNumber = l_currentMonth;
        }
        
        if(l_tableNumber <= 9)
        {
            i_controlFileStem = i_controlFileStem + "0" + l_tableNumber + i_controlFileExt;
        }
        else
        {
            i_controlFileStem = i_controlFileStem + l_tableNumber + i_controlFileExt;
        }
        
        return i_controlFileStem;
    }

    /**
     * @return Returns the discardDir.
     */
    protected String getDiscardDir()
    {
        return i_discardDir;
    }

    /**
     * @return Returns the logDir.
     */
    protected String getLogDir()
    {
        return i_logDir;
    }

    /**
     * @return Returns the badDir.
     */
    protected String getBadDir()
    {
        return i_badDir;
    }

    /**
     * @return Returns the osErrDir.
     */
    protected String getOsErrDir()
    {
        return i_osErrDir;
    }

    /**
     * @return Returns the osLogDir.
     */
    protected String getOsLogDir()
    {
        return i_osLogDir;
    }

    /**
     * @return Returns the bufferSize as a <code>String</code>.
     */
    protected String getBufferSizeAsString()
    {
        return Integer.toString(getBufferSize());
    }

    /**
     * @return Returns the bufferSize.
     */
    protected int getBufferSize()
    {
        return i_bufferSize;
    }

    /**
     * @return Returns the suppressMsg.
     */
    protected String getSuppressMsg()
    {
        return i_suppressMsg;
    }

    /**
     * Get the set of data errors.
     * 
     * @return Set of Strings that represent DBMS data errors.
     */
    protected HashSet getDbmsDataErrors()
    {
        return i_dbmsDataErrors;
    }

    /**
     * @return Returns the tooBigFailureProportion.
     */
    protected int getTooBigFailureProportion()
    {
        return i_tooBigFailureProportion;
    }

    /**
     * Get minimum nunber of rows to load before a 'too many errors' exception can be raised.
     * @return Returns moinimum number of rows.
     */
    protected int getMinRows()
    {
        return i_minRowsTooMany;
    }

    /**
     * @return Returns the tooBigFailureProportion as a <code>String</code>.
     */
    protected String getTooBigFailureProportionAsString()
    {
        return Integer.toString(getTooBigFailureProportion());
    }

    //==========================================================================
    // Private method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getConfiguration = "getConfiguration";
    /**
     * Gets the necessary configuration from the properties files.
     * 
     * @throws PpasConfigException  if a mandatory property is missing.
     */ 
    private void getConfiguration() throws PpasConfigException
    {
        int          l_poolMinSize          = 0;
        int          l_poolMaxSize          = 0;
        int          l_poolMinFree          = 0;
        int          l_poolMaxFree          = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this,
                            "Entering " + C_METHOD_getConfiguration);
        }

        // Get the JDBC configuration properties.
        i_userid              = getProperty(C_JDBC_PROP_USERNAME,       true,  null);
        i_password            = getProperty(C_JDBC_PROP_PASSWORD,       true,  null);
        i_dbService           = getProperty(C_JDBC_PROP_DB_SERVICE,     true,  null);
        // Set the connection string used by the SQL*Loader utility.
        i_oracleConnectionStr = i_userid + "/" + i_password + "@" + i_dbService;
        
        // Get the processor pool configuration properties.
        l_poolMinSize = getIntProperty(C_PROPERTY_POOL_MIN_SIZE, false, C_DEFVAL_POOL_MIN_SIZE);
        super.setMinSize(l_poolMinSize);
        
        l_poolMaxSize = getIntProperty(C_PROPERTY_POOL_MAX_SIZE, false, C_DEFVAL_POOL_MAX_SIZE);
        super.setMaxSize(l_poolMaxSize);
        
        l_poolMinFree = getIntProperty(C_PROPERTY_POOL_MIN_FREE, false, C_DEFVAL_POOL_MIN_FREE);
        super.setMinFree(l_poolMinFree);
        
        l_poolMaxFree = getIntProperty(C_PROPERTY_POOL_MAX_FREE, false, C_DEFVAL_POOL_MAX_FREE);
        super.setMaxFree(l_poolMaxFree);

        Vector l_dbmsErrors = i_ppasProperties.getListFromProperty(C_PROPERTY_DBMS_DATA_ERRORS);

        i_dbmsDataErrors = l_dbmsErrors == null ? new HashSet() : new HashSet(l_dbmsErrors);

        // Get the SQL*Loader command-line parameters.
        i_controlFileStem   = getProperty(C_PROPERTY_CONTROL_FILE_STEM, false, C_DEFVAL_CONTROL_FILE_STEM);

        i_controlFileExt    = getProperty(C_PROPERTY_CONTROL_FILE_EXT, false, C_DEFVAL_CONTROL_FILE_EXT);
        
        i_logDir        = getProperty(C_PROPERTY_LOG_DIR,         false, C_DEFVAL_LOG_DIR);
        i_logDir        += (!i_logDir.endsWith(File.separator)  ?  File.separator : "");

        i_badDir        = getProperty(C_PROPERTY_BAD_DIR,         false, C_DEFVAL_BAD_DIR);
        i_badDir        += (!i_badDir.endsWith(File.separator)  ?  File.separator : "");

        i_discardDir    = getProperty(C_PROPERTY_DISCARD_DIR,     false, C_DEFVAL_DISCARD_DIR);
        i_discardDir    += (!i_discardDir.endsWith(File.separator)  ?  File.separator : "");

        i_osLogDir      = getProperty(C_PROPERTY_OS_LOG_DIR,      false, C_DEFVAL_OS_LOG_DIR);
        i_osLogDir      += (!i_osLogDir.endsWith(File.separator)  ?  File.separator : "");

        i_osErrDir      = getProperty(C_PROPERTY_OS_ERR_DIR,      false, C_DEFVAL_OS_ERR_DIR);
        i_osErrDir      += (!i_osErrDir.endsWith(File.separator)  ?  File.separator : "");

        i_bufferSize    = getIntProperty(C_PROPERTY_BUFFER_SIZE,
                                         false,
                                         C_DEFVAL_BUFFER_SIZE,
                                         C_MINVAL_BUFFER_SIZE,
                                         C_MAXVAL_BUFFER_SIZE);

        i_suppressMsg   = getProperty(C_PROPERTY_SUPRESS_MESSAGE, false, C_DEFVAL_SUPRESS_MESSAGE);

        i_tooBigFailureProportion = getIntProperty(C_PROPERTY_TOO_BIG_FAILURE_PROPORTION,
                                                   false,
                                                   C_DEFVAL_TOO_BIG_FAILURE_PROPORTION,
                                                   C_MINVAL_TOO_BIG_FAILURE_PROPORTION,
                                                   C_MAXVAL_TOO_BIG_FAILURE_PROPORTION);

        i_minRowsTooMany = i_ppasProperties.getIntProperty(C_SQL_LOADER_PROPERTY_PREFIX + ".minRows", 10);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10060, this,
                            "Entering " + C_METHOD_getConfiguration);
        }
    }

    /**
     * Returns the value, as an <code>int</code>, of the specified property.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value.
     * 
     * @return  The value of the property if defined, otherwise the default value.
     * 
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private int getIntProperty(String  p_property,
                               boolean p_mandatory,
                               int     p_defaultValue) throws PpasConfigException
    {
        return getIntProperty(p_property, p_mandatory, p_defaultValue, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getIntProperty = "getIntProperty";
    /**
     * Returns the value, as an <code>int</code>, of the specified property.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value.
     * @param p_minValue      The min value allowed for this property.
     * @param p_maxValue      The max value allowed for this property.
     * 
     * @return  The value of the property if defined, otherwise the default value.
     * 
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private int getIntProperty(String  p_property,
                               boolean p_mandatory,
                               int     p_defaultValue,
                               int     p_minValue,
                               int     p_maxValue) throws PpasConfigException
    {
        // Local constant(s).
        final String L_VALID_RANGE = "" + p_minValue + " - " + p_maxValue;

        // Local variable(s).
        String l_defaultValue = (p_mandatory  ?  null : Integer.toString(p_defaultValue));
        String l_valueStr     = getProperty(p_property, p_mandatory, l_defaultValue);
        long   l_value        = Long.parseLong(l_valueStr);
        
        if (l_value < p_minValue || l_value > p_maxValue)
        {
            // ***ERROR: The value of this property is not within the range of an integer,
            //           throw a 'PpasConfigException'.
            PpasConfigException l_ppasConfigEx =
                new PpasConfigException(C_CLASS_NAME,
                                        C_METHOD_getIntProperty,
                                        10070,
                                        this,
                                        null,
                                        0,
                                        ConfigKey.get().badConfigValue(p_property,
                                                                       l_valueStr,
                                                                       L_VALID_RANGE));
            i_logger.logMessage(l_ppasConfigEx);
            throw l_ppasConfigEx;
        }

        return (int)l_value;
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getProperty = "getProperty";
    /**
     * Returns the value, as a <code>String</code>, that corresponds to the passed property key.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value (should be <code>null</code> for a mandatory property).
     * @return  The value of the property if defined, otherwise the default value.
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private String getProperty(String  p_property,
                               boolean p_mandatory,
                               String  p_defaultValue) throws PpasConfigException
    {
        String              l_propValue    = null;
        String              l_defaultValue = null;
        PpasConfigException l_ppasConfigEx = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10080, this,
                            "Entering " + C_METHOD_getProperty);
        }

        l_defaultValue = (p_mandatory  ?  null : p_defaultValue);

        if (i_ppasProperties != null)
        {
            l_propValue = i_ppasProperties.getTrimmedProperty(p_property, l_defaultValue);
        }
        else
        {
            l_propValue = l_defaultValue;
        }

        if (p_mandatory && l_propValue == null)
        {
            // ***ERROR: This is a missing mandatory property, throw a 'PpasConfigException'.
            l_ppasConfigEx =
                new PpasConfigException(C_CLASS_NAME,
                                        C_METHOD_getProperty,
                                        10082,
                                        this,
                                        null,
                                        0,
                                        ConfigKey.get().missingConfigData(p_property, C_CLASS_NAME));
            i_logger.logMessage(l_ppasConfigEx);
            throw l_ppasConfigEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10090, this,
                            "Leaving " + C_METHOD_getProperty);
        }
        return l_propValue;
    }
}
