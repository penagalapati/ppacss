////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiSession.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Holds session information for Chsi clients.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.support;

import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.common.support.SessionTimeoutInterface;
import com.slb.sema.ppas.common.web.support.HttpSessionsTable;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.common.web.support.WebSession;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;

/** Holds session information for Chsi clients. */
public class      ChsiSession extends    WebSession
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiSession";
    
    /** Constant representing Chsi message version 2. Value is {@value}. */
    public static final String C_CHSI_VERSION_1 = "1";

    
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Version of Chsi. */
    private String i_chsiVersion = C_CHSI_VERSION_1;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /** Creates a ChsiSession object.
     * @param p_httpSessionsTableToBelongTo Table of HTTP sessions.
     * @param p_httpSession                 HTTP session.
     * @param p_instrumentManager           Instrument manager. 
     * @param p_sessionTimeout              Session timeout manager.
     */
    public ChsiSession(HttpSessionsTable       p_httpSessionsTableToBelongTo,
                       HttpSession             p_httpSession,
                       InstrumentManager       p_instrumentManager,
                       SessionTimeoutInterface p_sessionTimeout)
    {
        super(p_httpSessionsTableToBelongTo,
              p_httpSession,
              p_instrumentManager,
              p_sessionTimeout);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SESSION,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 10000, this,
                           "Constructing ChsiSession(...)");
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SESSION,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10090, this,
                           "Constructed ChsiSession(...)");
        }
    }
    
    /**
     * Set the Chsi message version.
     * @param p_version the message version
     */
    public void setChsiVersion(String p_version)
    {
        i_chsiVersion = p_version;
    }
    
    /**
     * Get the Chsi message version.
     * @return the Chsi message version
     */
    public String getChsiVersion()
    {
        return i_chsiVersion;
    }
    
    /** Get file version of the Chsi definitions.
     * 
     * @param p_chsiVersion Version of Chsi.
     * @return Name of file cntaining Chsi definitions.
     */
    public static String getXsdFileVersion(String p_chsiVersion)
    {
        StringBuffer l_xsdVersion = new StringBuffer("");
        
        String[] l_splitVersion = p_chsiVersion.split("\\.");
        
        if ((l_splitVersion.length < 1) || (l_splitVersion.length > 2))
        {
            // throw PpasRuntimeException
        }
        
        l_xsdVersion = new StringBuffer(l_splitVersion[0]);
        
        if (l_splitVersion.length == 2)
        {
            l_xsdVersion.append("_");
            l_xsdVersion.append(l_splitVersion[1]);
        }
        
        return l_xsdVersion.toString();
    }
}
