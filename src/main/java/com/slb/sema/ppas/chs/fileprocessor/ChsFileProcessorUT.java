////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileProcessorUT.java
//      DATE            :       25-Sep-2006
//      AUTHOR          :       John Lee (k50924d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//                              PpacLon#2653_10017
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit test class for the 'ChsFileProcessor' class.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/10/06 | John Lee   | Minor tidy up of javadoc.       | PpacLon#2653_10124
//----------+------------+---------------------------------+--------------------
// 09/10/06 | John Lee   | Further tidy up.                | PpacLon#2653_10129
//----------+------------+---------------------------------+--------------------
// 20/10/06 | John Lee   | Add more tests.                 | PpacLon#2693_10199
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.fileprocessor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.dataclass.DbRowData;
import com.slb.sema.ppas.common.dataclass.DbRowDataSet;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;

/**
 * This <code>ChsFileProcessorUT</code> class is a  JUnit test class
 * for the 'ChsFileProcessor' class.
 */
public class ChsFileProcessorUT extends CommonTestCaseTT
{   
    /** The temporary directory in the local root that is used for initial processing. */
    private static final String C_TEMP_DIRECTORY = System.getProperty("ascs.localRoot") + "/tmp";
    
    /** The directory that is searched by the Call History Loader process. */
    private static final String C_CHS_DIRECTORY = System.getProperty("ascs.localRoot") + "/chsfiles";
    
    /** Name of the file just before being processed. */
    private ChsFileName i_thisTestFile;
    
    /** Timestamp that will be embedded into the file name.  */
    private PpasDateTime i_runTime;
    
    /** DB Service tool. */
    private static DbServiceTT c_dbServiceTT;
    
    /** File is done (successfully) status. Value is {@value}. */
    private static final String C_DNE = "DNE";
    
    /** File errored. Value is {@value}. */
    private static final String C_ERR = "ERR";
    
    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must
     * implement a constructor that takes a test case name as it's argument.
     *
     * @param p_title The testcase name.
     */
    public ChsFileProcessorUT(String p_title)
    {
        super(p_title);

        if (c_dbServiceTT == null)
        {
            c_dbServiceTT = new DbServiceTT(c_ppasRequest, c_logger);
            
            setDbServicable(c_dbServiceTT);
        } 
    }
    
    //==========================================================================
    // Public test method(s).
    //==========================================================================
    /** @ut.when A file arrives containing valid data.
     *  @ut.then The file is loaded and all data stored.
     */
    public void testValidCdrFile()
    {
        beginOfTest("testValidCdrFile");

        generateTestFile("chs_ldr_test_00001.dat");

        checkFileExists(i_thisTestFile.getDoneFile());
        
        checkCfci(i_thisTestFile, C_DNE, 14, 0, 0);
        
        assertCallHistoryData("083200000000001", "083210000000001", 0);
        assertCallHistoryData("083200000000002", "083210000000002", 1);
        assertCallHistoryData("083200000000004", "083210000000004", 4);
        assertCallHistoryData("083200000000006", "083210000000006", 6);
        assertCallHistoryData("083200000000007", "083210000000007", 7);
        assertCallHistoryData("083200000000008", "083210000000008", 8);
        assertCallHistoryData("083200000000010", "083210000000010", 10);
        assertCallHistoryData("083200000000012", "083210000000012", 20);
        assertCallHistoryData("083200000000014", "083210000000014", 99);
        assertCallHistoryData("083210000000003", "083200000000003", 2);
        assertCallHistoryData("083210000000005", "083200000000005", 5);
        assertCallHistoryData("083210000000009", "083200000000009", 9);
        assertCallHistoryData("083210000000011", "083200000000011", 12);
        assertCallHistoryData("083210000000013", "083200000000013", 21);

        endOfTest();
    }
    
    /** @ut.when A file arrives where optional data is missing.
     *  @ut.then Data loads correctly and valid defaults are used.
     */
    public void testMissingOptionalData()
    {
        beginOfTest("testMissingOptionalData");

        generateTestFile("chs_ldr_test_00002.dat");
        
        checkFileExists(i_thisTestFile.getDoneFile());
        
        // Expect one record to fail - missing traffic case.
        checkCfci(i_thisTestFile, C_DNE, 23, 1, 0);
        
        assertCallHistoryData("083200000000101", null, 0);
        assertCallHistoryData("083210000000101", null, 2);
        assertCallHistoryData("083200000000102", "083210000000102", 1);
        assertCallHistoryData("083210000000103", "083200000000103", 2);
        assertCallHistoryData("083200000000104", "083210000000104", 4);
        assertCallHistoryData("083210000000105", "083200000000105", 5);
        assertCallHistoryData("083200000000106", "083210000000106", 6);
        assertCallHistoryData("083200000000107", "083210000000107", 7);
        assertCallHistoryData("083200000000108", "083210000000108", 8);
        assertCallHistoryData("083210000000109", "083200000000109", 9);
        assertCallHistoryData("083200000000110", "083210000000110", 10);
        assertCallHistoryData("083210000000111", "083200000000111", 12);
        // 083210000000112 has a missing traffic case so it failed to load (checked above)
        assertCallHistoryData("083210000000113", "083200000000113", 21);
        assertCallHistoryData("083200000000114", "083210000000114", 99);
        assertCallHistoryData("083200000000115", "083210000000115", 99);
        assertCallHistoryData("083200000000116", "083210000000116", 99);
        assertCallHistoryData("083200000000117", "083210000000117", 99);
        assertCallHistoryData("083200000000118", "083210000000118", 99);
        assertCallHistoryData("083200000000119", "083210000000119", 99);
        assertCallHistoryData("083200000000120", "083210000000120", 99);
        assertCallHistoryData("083200000000121", "083210000000121", 99);
        assertCallHistoryData("083200000000122", "083210000000122", 99);
        assertCallHistoryData("083200000000123", "083210000000123", 99);

        endOfTest();
    }

    /** @ut.when A file arrives with a small proportion of invalid data.
     *  @ut.then Valid data loads correctly and file is flagged as done.
     */
    public void testInvalidDataSmall()
    {
        beginOfTest("testInvalidDataSmall");

        generateTestFile("chs_ldr_test_00003.dat");
        
        checkFileExists(i_thisTestFile.getDoneFile());
        
        checkCfci(i_thisTestFile, C_DNE, 13, 1, 0);

        endOfTest();
    }

    /** @ut.when A file arrives with a large proportion of invalid data.
     *  @ut.then Valid data loads correctly and file is flagged as errored.
     */
    public void testInvalidDataLarge()
    {
        beginOfTest("testInvalidDataLarge");

        generateTestFile("chs_ldr_test_00004.dat");
        
        checkFileExists(i_thisTestFile.getErrFile());
        
        checkCfci(i_thisTestFile, C_ERR, 6, 8, 0);
        
        endOfTest();
    }

    /** @ut.when A duplicate file arrives.
     *  @ut.then The file is flagged as a duplicate.
     */
    public void testDuplicateCdrFile()
    {
        beginOfTest("testDuplicateCdrFile");

        generateTestFile("chs_ldr_test_00001.dat");
        
        checkFileExists(i_thisTestFile.getDoneFile());
        
        // Create an empty file with the same name
        writeTestFile(C_CHS_DIRECTORY + File.separator + "pre", i_thisTestFile, new String[0]);
        
        checkFileExists(i_thisTestFile.getDupFile());
        
        endOfTest();
    }
    
    /** @ut.when An empty file arrives.
     *  @ut.then ????.
     */
    public void testEmptyFile()
    {
        beginOfTest("testEmptyFile");

        writeTestFile(C_CHS_DIRECTORY + File.separator + "pre", i_thisTestFile, new String[0]);
        
        checkFileExists(i_thisTestFile.getDoneFile());
        
        endOfTest();
    }
    
    /** @ut.when A file with corrupt data arrives.
     *  @ut.then ????.
     */
    public void testCorruptFile()
    {
        beginOfTest("testCorruptFile");

        writeTestFile(C_CHS_DIRECTORY + File.separator + "pre", i_thisTestFile, new String[] {"dodgy"});
        
        checkFileExists(i_thisTestFile.getDoneFile());
        
        endOfTest();
    }
    
    /**
     * Test suite conforming to the JUnit framework allowing tests to be run
     * automatically.
     * 
     * @return a Test case suite.
     */
    public static Test suite()
    {
        return new TestSuite(ChsFileProcessorUT.class);
    }
    
    /**
     * Method that is automatically called prior to a test.
     */
    protected void setUp()
    {
        super.setUp();
        
        DatePatch.changeDate(0, 1);
        i_runTime = getNow();
        
        String l_timeStamp = i_runTime.toString_yyyyMMdd_HHmmss();
        String[] l_parts = l_timeStamp.split("-");
        
        i_thisTestFile = new ChsFileName("chs_ldr", "test", l_parts[0], l_parts[1]);
    }

    /** Generate a test file.
     * 
     * @param p_testFileName Name of file containing test data.
     */
    private void generateTestFile(String p_testFileName)
    {   
        File l_file = new File(C_TEMP_DIRECTORY + File.separator + p_testFileName);
        String [] l_contents = getFileOutput(l_file, 0);
        
        for (int i = 0; i < l_contents.length; i++)
        {
            String l_timeStamp = i_runTime.toString_yyyyMMdd_HHcmmcss();
            l_timeStamp = l_timeStamp.replace(' ', ',');
            l_timeStamp = l_timeStamp.replaceAll(":", "");
            
            l_contents[i] = l_contents[i].replaceAll("<DATE_TIME>", l_timeStamp);
        }
        
        writeTestFile(C_CHS_DIRECTORY + File.separator + "pre", i_thisTestFile, l_contents);
    }
    
    /** Write a file to a specified directory.
     * 
     * @param p_directory  Directory to write file.
     * @param p_name       Name of file.
     * @param p_contents   What to write.
     */
    private void writeTestFile(String      p_directory,
                               ChsFileName p_name,
                               String[]    p_contents)
    {
        String l_file = p_directory + File.separator + p_name.getBaseName() + ".dat";
        String l_tmp  = l_file + "_tmp";
        
        File l_tmpFile = new File(l_tmp);
        
        BufferedWriter l_writer;
        
        try
        {
            l_writer = new BufferedWriter(new FileWriter(l_tmpFile));
            
            for (int i = 0; i < p_contents.length; i++)
            {
                l_writer.write(p_contents[i]);
                l_writer.write(C_NL);
            }
            
            l_writer.close();
            
            l_tmpFile.renameTo(new File(l_file));
        }
        catch(IOException e)
        {
            failedTestException(e);
        }
    }
    
    /** Check a file exists in a given directory.
     * 
     * @param p_file File to check.
     */
    private void checkFileExists(File p_file)
    {
        say("Look for " + p_file);
        for (int i = 0; i < 20; i++)
        {
            if (p_file.exists())
            {
                say("Found file " + p_file);
                return;
            }
            
            snooze(0.5);
        }
        
        fail("Could not find " + p_file);
    }

    /** Dump a given row of the CFCI table.
     * 
     * @param p_testFile Identifier of the CFCI entry.
     * @param p_status   Expected status of the CFCI entry.
     * @param p_loaded   Number of rows successfully loaded.
     * @param p_bad      Number of bad rows.
     * @param p_discard  Number of discarded rows.
     */
    private void checkCfci(ChsFileName p_testFile,
                           String      p_status,
                           int         p_loaded,
                           int         p_bad,
                           int         p_discard)
    {
        SqlString l_sql = new SqlString(500, 5, "SELECT cfci_status, cfci_successful_records, " +
                                        " cfci_bad_records, cfci_discarded_records " +
                                        "FROM cfci_cdr_file_ctl " +
                                        " WHERE cfci_file_info_1 = {0}" +
                                        " AND   cfci_file_info_2 = {1}" +
                                        " AND  cfci_file_info_3 = {2}" +
                                        " AND  cfci_file_info_4 = {3}");
        l_sql.setStringParam(0, p_testFile.getKey1());
        l_sql.setStringParam(1, p_testFile.getKey2());
        l_sql.setStringParam(2, p_testFile.getKey3());
        l_sql.setStringParam(3, p_testFile.getKey4());
        
        DbRowDataSet l_dump = getDbData(l_sql);
        
        say("CFCI for '" + p_testFile.getKey1() + "', '" + p_testFile.getKey2() + "', '" +
            p_testFile.getKey3() + "', '" + p_testFile.getKey4() + "'" + C_NL + l_dump.toFormattedString());
        
        assertEquals("Expect one row", 1, l_dump.size());
        
        DbRowData l_cfci = l_dump.getRow(0);
        
        assertEquals("Incorrect status",       p_status,  l_cfci.getTrimmedColumn(0));
        assertEquals("Incorrect loaded",  "" + p_loaded,  l_cfci.getTrimmedColumn(1));
        assertEquals("Incorrect bad",     "" + p_bad   ,  l_cfci.getTrimmedColumn(2));
        assertEquals("Incorrect discard", "" + p_discard, l_cfci.getTrimmedColumn(3));
    }
    
    /**
     * Asserts the presence of Call History Data.
     *
     * @param p_msisdn  The number corresponding to chdr_msisdn.
     * @param p_otherNumber  The number corresponding to chdr_other_number.
     * @param p_trafficCase  The traffic case.
     */
    private void assertCallHistoryData(String p_msisdn,
                                       String p_otherNumber,
                                       int    p_trafficCase)
    {
        say("Checking CHDR for MSISDN: " + p_msisdn + ", Start: " + i_runTime + ", Other: " + p_otherNumber +
            " Traffic: " + p_trafficCase);

        SqlString l_sql = new SqlString(200, 2, "SELECT chdr_other_number, chdr_traffic_case " +
                                        "FROM chdr_call_history_data_view " +
                                        "WHERE chdr_msisdn = {0} AND chdr_call_start_datetime = {1}");
        
        l_sql.setStringParam(0, p_msisdn);
        l_sql.setDateTimeParam(1, i_runTime);
        
        say(getDbData(l_sql).toFormattedString());
        
        int       l_noOfRows = 0;
        String    l_tables = "chdr_call_history_data_view chdr";
        SqlString l_where;

        l_where = new SqlString(500, 4, "WHERE chdr.chdr_msisdn = {0}" +
                                        " AND  chdr.chdr_other_number " +
                                            (p_otherNumber == null ? "is" : "=") + " {1}" +
                                        " AND  chdr.chdr_call_start_datetime = {2}" +
                                        " AND  chdr.chdr_traffic_case = {3}");
     
        l_where.setStringParam(0, p_msisdn);
        l_where.setStringParam(1, p_otherNumber);
        l_where.setDateTimeParam(2, i_runTime);
        l_where.setIntParam(3, p_trafficCase);

        l_noOfRows = countRowsInTable(l_tables, l_where);
        assertEquals("Incorrect No. of Call History rows:", 1, l_noOfRows);
    }
    
    /**
     * Inner class. 
     */
    private class ChsFileName
    {
        /** The 1st part of the filename. */
        private String i_key1;
        
        /** The 2nd part of the filename. */
        private String i_key2;
        
        /** The 3rd part of the filename. */
        private String i_key3;
        
        /** The 4th part of the filename. */
        private String i_key4;
        
        /**
         * Constructor for the class.
         *
         * @param p_key1 The 1st part of the filename.
         * @param p_key2 The 2nd part of the filename.
         * @param p_key3 The 3rd part of the filename.
         * @param p_key4 The 4th part of the filename.
         */
        public ChsFileName(String p_key1, String p_key2, String p_key3, String p_key4)
        {
            i_key1 = p_key1;
            i_key2 = p_key2;
            i_key3 = p_key3;
            i_key4 = p_key4;
        }
        
        /**
         * Returns the .dne file including it's directory path.
         * 
         * @return a File.
         */
        public File getDoneFile()
        {
            String l_doneDir = C_CHS_DIRECTORY + File.separator + "dne" + File.separator + i_key3 +
                               File.separator + i_key4.substring(0, 2) + File.separator;
            
            return new File(l_doneDir + File.separator + getBaseName() + ".dne");
        }
        
        /**
         * Returns the .dup file including it's directory path.
         * 
         * @return a File.
         */
        public File getDupFile()
        {
            String l_doneDir = C_CHS_DIRECTORY + File.separator + "dup" + File.separator + i_key3 +
                               File.separator;
            
            return new File(l_doneDir + File.separator + getBaseName() + ".dup");
        }
        
        /**
         * Returns the .err file including it's directory path.
         * 
         * @return a File.
         */
        public File getErrFile()
        {
            String l_doneDir = C_CHS_DIRECTORY + File.separator + "err" + File.separator + i_key3 +
                               File.separator;
            
            return new File(l_doneDir + File.separator + getBaseName() + ".err");
        }
        
        /**
         * Returns the base name of a file.
         * 
         * @return The base name of a file.
         */
        public String getBaseName()
        {
            return i_key1 + "_" + i_key2 + "_" + i_key3 + "_" + i_key4;
        }
        
        /**
         * Returns 1st part of a filename.
         * 
         * @return 1st part of a filename.
         */
        public String getKey1()
        {
            return i_key1;
        }
        
        /**
         * Returns 2nd part of a filename.
         * 
         * @return The 2nd part of a filename.
         */
        public String getKey2()
        {
            return i_key2;
        }
        
        /**
         * Returns 3rd part of a filename.
         * 
         * @return The 3rd part of a filename.
         */
        public String getKey3()
        {
            return i_key3;
        }
        
        /**
         * Returns 4th part of a filename.
         * 
         * @return The 4th part of a filename.
         */
        public String getKey4()
        {
            return i_key4;
        }
    }
}
