////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFile.java
//      DATE            :       11-Mars-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A data object representing a Call History file
//                              to be processed.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.dataclass;

import java.io.File;

import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This <code>ChsFile</code> class serves as a data object representing
 * a Call History file to be processed.
 * It will contain details of the key used for storing the file in the database.
 * It will also identify whether the file is being recovered or not.
 */
public class ChsFile
{
    //==========================================================================
    // Public class level constant(s).
    //==========================================================================
    /** The 'undefined' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_UNDEFINED         = "UND";

    /** The 'processing not started' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_NOT_STARTED       = "DAT";

    /** The 'file in progress' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_FILE_IN_PROGRESS  = "IPG";

    /** The 'data loading' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_DATA_LOADING      = "LDG";

    /** The 'invalid file' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_INVALID_FILE      = "INV";

    /** The 'file is successfully processed' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_SUCCESSFULLY_PROC = "DNE";

    /** The 'file is a duplicate' processing status. value is {@value}. */
    public static final String   C_PROCESSING_STATUS_DUPLICATE_FILE    = "DUP";

    /** Indicator that errors occurred loading data - some of the data may have been loaded. Note: This
     * status will only be used if a 'reasonable' percentage of records fail to load. Value is {@value}. */
    public static final String   C_PROCESSING_STATUS_ERROR_LOADING    = "ERR";

    //==========================================================================
    //== Private constants.
    //==========================================================================
    /** The name of this class. */
    private static final String C_CLASS_NAME                   = "ChsFile";

    /** The filename extension delimiter. */
    private static final char   C_DELIMITER_FILENAME_EXTENSION = '.';


    //==========================================================================
    // Private instance attributes(s).
    //==========================================================================
    /** The Call History CDR file. */
    private File           i_cdrFile;

    /** The date and time this instance was created. */
    private PpasDateTime   i_loggedDateTime;

    /** The base filename, that is the filename without the parent path and the extension. */
    private String         i_baseFilename;

    /** The filename part 1. */
    private String         i_cfciFileInfo1;

    /** The filename part 2. */
    private String         i_cfciFileInfo2;

    /** The filename part 3. */
    private String         i_cfciFileInfo3;

    /** The filename part 4. */
    private String         i_cfciFileInfo4;

    /** The filename extension. */
    private String         i_extension;

    /** The status of the file. */
    private String         i_status;

    //==========================================================================
    //== Constructor(s).
    //==========================================================================
    /**
     * Constructs a <code>ChsFile</code> instance using the passed parameters.
     * 
     * @param p_cdrFile         The <code>File</code> object representing the CDR file.
     * @param p_filenamePart1   The filename part 1.
     * @param p_filenamePart2   The filename part 2.
     * @param p_filenamePart3   The filename part 3.
     * @param p_filenamePart4   The filename part 4.
     * @param p_extension       The filename extension.
     * @param p_status          The file status.
     */
    public ChsFile(File   p_cdrFile,
                   String p_filenamePart1,
                   String p_filenamePart2,
                   String p_filenamePart3,
                   String p_filenamePart4,
                   String p_extension,
                   String p_status)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_cdrFile        = p_cdrFile;
        i_loggedDateTime = DatePatch.getDateTimeNow();
        i_cfciFileInfo1  = p_filenamePart1;
        i_cfciFileInfo2  = p_filenamePart2;
        i_cfciFileInfo3  = p_filenamePart3;
        i_cfciFileInfo4  = p_filenamePart4;
        i_extension      = p_extension;
        i_status         = p_status;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //==========================================================================
    // Public method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_rename = "rename";
    /**
     * Renames the CDR file to the given filename.
     * 
     * @param p_newFilename  the new name of the file including the full pathname.
     * @return <code>true</code> if rename was successful, <code>false</code> otherwise.
     */
    public boolean rename(String p_newFilename)
    {
        boolean l_renameSucceeded = false;
        File    l_newFile         = null;
        int     l_extensionIx     = -1;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Entering " + C_METHOD_rename);
        }

        if (i_cdrFile != null)
        {
            l_newFile = new File(p_newFilename);

            // Create directory path if necessary.
            if (!l_newFile.getParentFile().exists())
            {
                l_newFile.getParentFile().mkdirs();
            }

            // Rename the file.
            l_renameSucceeded = i_cdrFile.renameTo(l_newFile);
            if (l_renameSucceeded)
            {
                i_cdrFile = l_newFile;

                // Store the new filename extension.
                l_extensionIx = i_cdrFile.getName().indexOf(C_DELIMITER_FILENAME_EXTENSION);
                i_extension   = i_cdrFile.getName().substring(l_extensionIx + 1);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this,
                            "Leaving " + C_METHOD_rename);
        }
        return l_renameSucceeded;
    }

    /**
     * @return Returns the cdrFile.
     */
    public File getCdrFile()
    {
        return i_cdrFile;
    }

    /**
     * @return Returns the loggedDateTime.
     */
    public PpasDateTime getLoggedDateTime()
    {
        return i_loggedDateTime;
    }

    /**
     * @return Returns the filenamePart1.
     */
    public String getCfciFileInfo1()
    {
        return i_cfciFileInfo1;
    }

    /**
     * @return Returns the filenamePart2.
     */
    public String getCfciFileInfo2()
    {
        return i_cfciFileInfo2;
    }

    /**
     * @return Returns the filenamePart3.
     */
    public String getCfciFileInfo3()
    {
        return i_cfciFileInfo3;
    }

    /**
     * @return Returns the filenamePart4.
     */
    public String getCfciFileInfo4()
    {
        return i_cfciFileInfo4;
    }

    /**
     * @return Returns the extension.
     */
    public String getExtension()
    {
        return i_extension;
    }

    /**
     * @return Returns the status.
     */
    public String getStatus()
    {
        return i_status;
    }

    /**
     * Sets the status.
     * 
     * @param p_status  the new file status.
     */
    public void setStatus(String p_status)
    {
        i_status = p_status;
    }

    /**
     * Returns the base filename, that is the filename without the parent path and the extension.
     * 
     * @return  the baseFilename.
     */
    public String getBaseFilename()
    {
        int l_index = -1;

        if (i_baseFilename == null)
        {
            l_index = i_cdrFile.getName().lastIndexOf(C_DELIMITER_FILENAME_EXTENSION);
            i_baseFilename = i_cdrFile.getName().substring(0, l_index);
        }
        return i_baseFilename;
    }
}
