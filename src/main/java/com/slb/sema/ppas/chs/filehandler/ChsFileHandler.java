////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileHandler.java
//      DATE            :       20-Mars-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class is responsible for parsing a filename
//                              and creating a 'ChsFile' object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.filehandler;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.slb.sema.ppas.chs.dataclass.ChsFile;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * This <code>ChsFileHandler</code> class is responsible for parsing a filename
 * and creating a <code>ChsFile</code> object.
 */
public class ChsFileHandler implements InstrumentedObjectInterface
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                          = "ChsFileHandler";

    /** The property prefix for <code>ChsFileHandler</code> properties. Value is {@value}. */
    private static final String C_CHS_FILE_HANDLER_PROPERTY_PREFIX    =
        "com.slb.sema.ppas.chs.filehandler.ChsFileHandler";

    /** The property name of the 'files in progress' directory. Value is {@value}. */
    private static final String C_PROPERTY_IPG_DIRECTORY              = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".ipgDirectory";

    /** The default value of the 'files in progress' root directory. Value is {@value}. */
    private static final String C_DEFVAL_IPG_DIRECTORY                = "${ASCS_LOCAL_ROOT}/chsfiles/ipg";

    /** The property name of the 'successfully processed files' root directory. Value is {@value}. */
    private static final String C_PROPERTY_DNE_DIRECTORY              = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".dneDirectory";

    /** The default value of the 'successfully processed files' root directory. Value is {@value}. */
    private static final String C_DEFVAL_DNE_DIRECTORY                = "${ASCS_LOCAL_ROOT}/chsfiles/dne";

    /** The property name of the 'invalid filename files' root directory. Value is {@value}. */
    private static final String C_PROPERTY_INV_DIRECTORY              = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".invDirectory";

    /** The default value of the 'invalid filename files' root directory. Value is {@value}. */
    private static final String C_DEFVAL_INV_DIRECTORY                = "${ASCS_LOCAL_ROOT}/chsfiles/inv";

    /** The property name of the 'duplicate filename' root directory. Value is {@value}. */
    private static final String C_PROPERTY_DUP_DIRECTORY              = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".dupDirectory";

    /** The default value of the 'duplicate filename' root directory. Value is {@value}. */
    private static final String C_DEFVAL_DUP_DIRECTORY                = "${ASCS_LOCAL_ROOT}/chsfiles/dup";

    /** The property name of the 'erroneous files' root directory. Value is {@value}. */
    private static final String C_PROPERTY_ERR_DIRECTORY              = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".errDirectory";

    /** The default value of the 'erroneous files' root directory. Value is {@value}. */
    private static final String C_DEFVAL_ERR_DIRECTORY                = "${ASCS_LOCAL_ROOT}/chsfiles/err";

    /** The property name of the 'processing not started' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_PROC_NOT_STARTED_EXTENSION = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".notStartedExt";

    /** The default value of the 'processing not started' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_PROC_NOT_STARTED_EXTENSION   = "dat";

    /** The property name of the 'file in progress' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_FILE_IN_PROGRESS_EXTENSION = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".inProgressExt";

    /** The default value of the 'file in progress' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_FILE_IN_PROGRESS_EXTENSION   = "ipg";

    /** The property name of the 'data loading' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_DATA_LOADING_EXTENSION     = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".loadingExt";

    /** The default value of the 'data loading' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_DATA_LOADING_EXTENSION       = "ldg";

    /** The property name of the 'successfully processed file' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_PROC_DONE_EXTENSION        = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".completedExt";

    /** The default value of the 'successfully processed file' filename extension property.
     * Value is {@value}.*/
    private static final String C_DEFVAL_PROC_DONE_EXTENSION          = "dne";

    /** The property name of the 'invalid filename' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_INVALID_FILENAME_EXTENSION = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".invalidExt";

    /** The default value of the 'invalid filename' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_INVALID_FILENAME_EXTENSION   = ".inv";

    /** The property name of the 'file is a duplicate' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_DUPLICATE_FILE_EXTENSION   = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".duplicateExt";

    /** The default value of the 'file is a duplicate' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_DUPLICATE_FILE_EXTENSION     = "dup";

    /** The property name of the 'erroneous file' filename extension. Value is {@value}. */
    private static final String C_PROPERTY_ERRONEOUS_FILE_EXTENSION   = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".errorExt";

    /** The default value of the 'erroneous file' filename extension property. Value is {@value}. */
    private static final String C_DEFVAL_ERRONEOUS_FILE_EXTENSION     = "err";

    /** The property name of the 'valid filename(s) regular expression' property. Value is {@value}. */
    private static final String C_PROPERTY_FILENAME_REG_EXP           = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".validFilenameRegExp";

    /** The default value of the 'valid filename(s) regular expression' property. Value is {@value}. */
    private static final String C_DEFVAL_FILENAME_REG_EXP             =
        "(.+)_(.+)_(.+)_(.+)\\.(dat|ipg.*|ldg.*)";

    /** The property name of the 'filename parts order' property.
     *  The value of this property is a comma separated list. Value is {@value}. */
    private static final String C_PROPERTY_FILENAME_PARTS_ORDER       = C_CHS_FILE_HANDLER_PROPERTY_PREFIX +
                                                                        ".filenamePartsOrder";

    /** The default value of the 'filename regular expression' property. Value is {@value}. */
    private static final String C_DEFVAL_FILENAME_PARTS_ORDER         = "1, 2, 3, 4";

    /** The filename parts delimiter. */
    private static final String C_DELIMITER_FILENAME_PARTS            = "_";

    /** The filename extension delimiter. */
    private static final String C_DELIMITER_FILENAME_EXTENSION        = ".";


    //==========================================================================
    // Private instance attribute(s).
    //==========================================================================
    /** The Instrument Set for this instrumented object. */
    private InstrumentSet     i_instrumentSet         = null;

    /** The <code>Logger</code> instance used to log events and messages. */
    private Logger            i_logger                = null;

    /** The ASCS properties object. */
    private PpasProperties    i_ppasProperties        = null;

    /** The process instance identifier. */
    private String            i_chsInstance           = null;

    /** The directory used for files in progress. */
    private String            i_ipgDirectory          = null;

    /** The directory used for successfully processed files. */
    private String            i_dneDirectory          = null;

    /** The directory used for files with an invalid filename. */
    private String            i_invDirectory          = null;

    /** The directory used for found duplicate files. */
    private String            i_dupDirectory          = null;

    /** The directory used for erroneous files. */
    private String            i_errDirectory          = null;

    /** The 'processing not started' filename extension. */
    private String            i_procNotStartedExt     = null;

    /** The 'file in progress' filename extension. */
    private String            i_fileInProgressExt     = null;

    /** The 'data loading' filename extension. */
    private String            i_dataLoadingExt        = null;

    /** The 'successfully processed file' filename extension. */
    private String            i_procDoneExt           = null;

    /** The 'invalid filename' filename extension. */
    private String            i_invalidFilenameExt    = null;

    /** The 'file is a duplicate' filename extension. */
    private String            i_duplicateFileExt      = null;

    /** The 'erroneous file' filename extension. */
    private String            i_erroneousFileExt      = null;

    /** The regular expression used to parse the CDR filename. */
    private String            i_filenameRegExp        = null;

    /** The order indexes of the CDR filename parts as an array of integers. */
    private int[]             i_filenamePartsOrderIxs = null;

    /** The 
    /** The <code>Pattern</code> object used to match the CDR filename against. */
    private Pattern           i_filenamePattern       = null;


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs a <code>ChsFileHandler</code> instance using the passed parameters.
     *  
     * @param p_logger               The logger instance used by the Call History Server process.
     * @param p_ppasProperties       Configuration properties.
     * @param p_chsInstance          The CHS instance identifier.
     * @throws PpasConfigException  if any mandatory property is missing.
     */
    public ChsFileHandler(Logger            p_logger,
                          PpasProperties    p_ppasProperties,
                          String            p_chsInstance)
        throws PpasConfigException
    {
        super();
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_logger            = p_logger;
        i_ppasProperties    = p_ppasProperties;
        i_chsInstance       = p_chsInstance;

        init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }


    //==========================================================================
    // Public method(s).
    //==========================================================================
    /**
     * Returns this instrumented object's instrument set.
     * @return This instrumented object's instrument set.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }


    /**
     * Returns this instrumented object's name.
     * @return This instrumented object's name.
     */
    public String getName()
    {
        return C_CLASS_NAME;
    }


    /**
     * Returns the 'file in progress' directory.
     * 
     * @return the 'file in progress' directory.
     */
    public String getFileInProgressDir()
    {
        return i_ipgDirectory;
    }


    /**
     * Returns the 'duplicate CDR files' directory.
     * 
     * @return the 'duplicate CDR files' directory.
     */
    public String getDuplicateCDRFilesDir()
    {
        return i_dupDirectory;
    }


    /**
     * Returns the 'processing not started' filename extension.
     * 
     * @return the 'processing not started' filename extension.
     */
    public String getProcessingNotStartedExtension()
    {
        return i_procNotStartedExt;
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_create = "create";
    /**
     * Creates and returns a <code>ChsFile</code> object from the given <code>File</code> object
     * if it has a valid call history CDR filename, otherwise <code>null</code> is returned.
     * 
     * @param p_file  the call history CDR file.
     * 
     * @return a <code>ChsFile</code> object from the given <code>File</code> object,
     *         or <code>null</code> if the given <code>File</code> has an invalid filename.
     */
    public ChsFile create(File p_file)
    {
        ChsFile l_chsFile = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering " + C_METHOD_create);
        }

        // Validate the filename.
        l_chsFile = validateFilename(p_file);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving " + C_METHOD_create);
        }
        return l_chsFile;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_rename_2 = "rename(ChsFile, String, String)";
    /**
     * Renames the CDR file that is encapsulated by the given <code>ChsFile</code> object to have the given
     * new extension.
     * The CDR file will also be moved to the directory given by 'new directory' parameter.
     * If the 'new directory' parameter is <code>null</code> the CDR file will stay in its current
     * directory but with the given 'new extension'.
     * 
     * @param p_chsFile       the <code>ChsFile</code> object which encapsulates the CDR file to be renamed.
     * @param p_newDir        the new directory to move the CDR file to, or <code>null</code> if the CDR file
     *                        should stay at its current directory.
     * @param p_newExtension  the new extension for the CDR file.
     * 
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    private boolean rename(ChsFile p_chsFile, String p_newDir, String p_newExtension)
    {
        boolean      l_renameSucceeded  = false;
        File         l_cdrFile          = null;
        StringBuffer l_newFilename      = null;
        String       l_newDir           = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10040, this,
                            "Entering " + C_METHOD_rename_2 + 
                            ",  rename the file '" + p_chsFile.getCdrFile().getAbsolutePath() + "'");
        }

        l_cdrFile = p_chsFile.getCdrFile();
        l_newDir  = (p_newDir != null  ?  p_newDir : l_cdrFile.getParent());
        l_newFilename = new StringBuffer(l_newDir);
        l_newFilename.append(File.separatorChar);
        l_newFilename.append(p_chsFile.getBaseFilename());
        l_newFilename.append(C_DELIMITER_FILENAME_EXTENSION);
        l_newFilename.append(p_newExtension);
        l_renameSucceeded = p_chsFile.rename(l_newFilename.toString());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10050, this,
                            "Leaving " + C_METHOD_rename_2);
        }
        return l_renameSucceeded;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_moveToInvFilenameDir = "moveToInvFilenameDir";
    /**
     * Moves (and renames) the given <code>File</code> to the specified directory for 
     * files with an invalid filename.
     * 
     * @param p_file the <code>File</code> to be moved and renamed.
     * 
     * @return <code>true</code> if the move (rename) was successful, <code>false</code> otherwise.
     */
    public boolean moveToInvFilenameDir(File p_file)
    {
        boolean      l_renameSucceeded  = true;
        StringBuffer l_newFilename      = null;
        File         l_newFile          = null;
        File         l_invDateDirectory = null;
        int          l_extensionIx      = -1;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10060, this,
                            "Entering " + C_METHOD_moveToInvFilenameDir);
        }

        l_newFilename = new StringBuffer(i_invDirectory);
        if (!i_invDirectory.endsWith(File.separator))
        {
            l_newFilename.append(File.separator);
        }
        l_newFilename.append(DatePatch.getDateToday().toString_yyyyMMdd());

        l_invDateDirectory = new File(l_newFilename.toString());
        if (!l_invDateDirectory.exists())
        {
            l_renameSucceeded = createDirs(l_invDateDirectory);
        }

        if (l_renameSucceeded)
        {
            l_newFilename.append(File.separator);
            l_extensionIx = p_file.getName().lastIndexOf(C_DELIMITER_FILENAME_EXTENSION);
            l_newFilename.append(p_file.getName().substring(0, l_extensionIx));
            if (!i_invalidFilenameExt.startsWith(C_DELIMITER_FILENAME_EXTENSION))
            {
                l_newFilename.append(C_DELIMITER_FILENAME_EXTENSION);
            }
            l_newFilename.append(i_invalidFilenameExt);
            l_newFile = new File(l_newFilename.toString());
            l_renameSucceeded = p_file.renameTo(l_newFile);
            if (l_renameSucceeded)
            {
                // TODO Why is there no action ?
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10070, this,
                            "Leaving " + C_METHOD_moveToInvFilenameDir +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        
        return l_renameSucceeded;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameToInProgress = "renameToInProgress";
    /**
     * Renames the given <code>ChsFile</code> to the configured 'in progress' extension.
     * 
     * @param p_chsFile the <code>ChsFile</code> to be renamed.
     * @param p_distrThreadId Thread processing this file.
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    public boolean renameToInProgress(ChsFile p_chsFile, int p_distrThreadId)
    {
        boolean      l_renameSucceeded = true;
        StringBuffer l_ipgFilename     = null;
        File         l_ipgFilenameDir  = null;
        String       l_extension       = null;
        String       l_extThreadIdStr  = null;
        String       l_inProgressExt   = null;
        String       l_loadingExt      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10080, this,
                            "Entering " + C_METHOD_renameToInProgress);
        }

        // Create the 'in progress' and 'loading' filename extensions.
        l_extThreadIdStr = "00" + p_distrThreadId;
        l_extThreadIdStr = l_extThreadIdStr.substring(l_extThreadIdStr.length() - 2);
        l_inProgressExt = i_fileInProgressExt +
                          C_DELIMITER_FILENAME_PARTS +
                          i_chsInstance +
                          C_DELIMITER_FILENAME_PARTS +
                          l_extThreadIdStr;

        l_loadingExt = i_dataLoadingExt +
                       C_DELIMITER_FILENAME_PARTS +
                       i_chsInstance +
                       C_DELIMITER_FILENAME_PARTS +
                       l_extThreadIdStr;

        l_extension = p_chsFile.getExtension();

        if (l_extension.equals(l_inProgressExt) || l_extension.equals(l_loadingExt))
        {
            // The CDR file has already been renamed by 'this' thread (at least a thread with the same
            // thread id if this is a recovered file), no need to rename the file but to proceed the process.
            l_renameSucceeded = true;
        }
        else if (l_extension.startsWith(i_fileInProgressExt) || l_extension.startsWith(i_dataLoadingExt))
        {
            // The CDR file has been renamed by a thread with a thread id that differs from the current one.
            // Skip this file and try to process next.
            l_renameSucceeded = false;
        }
        else
        {
            // Do rename the CDR file.
            l_ipgFilename = new StringBuffer(i_ipgDirectory);
            if (!i_ipgDirectory.endsWith(File.separator))
            {
                l_ipgFilename.append(File.separator);
            }
            l_ipgFilenameDir = new File(l_ipgFilename.toString());
            if (!l_ipgFilenameDir.exists())
            {
                l_renameSucceeded = createDirs(l_ipgFilenameDir);
            }

            if (l_renameSucceeded)
            {
                l_renameSucceeded = rename(p_chsFile,
                                           l_ipgFilenameDir.getAbsolutePath(),
                                           l_inProgressExt);
                if (l_renameSucceeded)
                {
                    p_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_FILE_IN_PROGRESS);
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10090, this,
                            "Leaving " + C_METHOD_renameToInProgress +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        return l_renameSucceeded;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameToLoading = "renameToLoading";
    /**
     * Renames the given <code>ChsFile</code> to the configured 'loading data' extension.
     * 
     * @param p_chsFile        the <code>ChsFile</code> to be renamed.
     * @param p_distrThreadId  the current file distributor thread id.
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    public boolean renameToLoading(ChsFile p_chsFile, int p_distrThreadId)
    {
        boolean      l_renameSucceeded = true;
        StringBuffer l_ldrFilename     = null;
        File         l_ldrFilenameDir  = null;
        String       l_extension       = null;
        String       l_extThreadIdStr  = null;
        String       l_loadingExt      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10100, this,
                            "Entering " + C_METHOD_renameToLoading);
        }

        // Create the 'loading' filename extension.
        l_extThreadIdStr = "00" + p_distrThreadId;
        l_extThreadIdStr = l_extThreadIdStr.substring(l_extThreadIdStr.length() - 2);
        l_loadingExt = i_dataLoadingExt +
                       C_DELIMITER_FILENAME_PARTS +
                       i_chsInstance +
                       C_DELIMITER_FILENAME_PARTS +
                       l_extThreadIdStr;

        l_extension = p_chsFile.getExtension();
        if (l_loadingExt.equals(l_extension))
        {
            // The CDR file has already been renamed by 'this' thread (at least a thread with the same
            // thread id if this is a recovered file), no need to rename the file but to proceed the process.
            l_renameSucceeded = true;
        }
        else if (l_extension.startsWith(i_dataLoadingExt))
        {
            // The CDR file has been renamed by a thread with a thread id that differs from the current one.
            // Don't process this file, will probably be processed by another distributor thread.
            l_renameSucceeded = false;
        }
        else
        {
            // Do rename the CDR file.
            l_ldrFilename = new StringBuffer(i_ipgDirectory);
            if (!i_ipgDirectory.endsWith(File.separator))
            {
                l_ldrFilename.append(File.separator);
            }
            l_ldrFilenameDir = new File(l_ldrFilename.toString());

            l_renameSucceeded = rename(p_chsFile,
                                       l_ldrFilenameDir.getAbsolutePath(),
                                       l_loadingExt);
            if (l_renameSucceeded)
            {
                p_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_DATA_LOADING);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10110, this,
                            "Leaving " + C_METHOD_renameToLoading +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        return l_renameSucceeded;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameToSuccessfullyProcessed = "renameToSuccessfullyProcessed";
    /**
     * Renames and moves the given <code>ChsFile</code> to the configured 'successfully processed files'
     * directory and extension.
     * 
     * @param p_chsFile the <code>ChsFile</code> to be renamed and moved.
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    public boolean renameToSuccessfullyProcessed(ChsFile p_chsFile)
    {
        boolean      l_renameSucceeded = true;
        StringBuffer l_doneFilename    = null;
        File         l_doneFilenameDir = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10120, this,
                            "Entering " + C_METHOD_renameToSuccessfullyProcessed);
        }

        l_doneFilename = new StringBuffer(i_dneDirectory);
        if (!i_dneDirectory.endsWith(File.separator))
        {
            l_doneFilename.append(File.separator);
        }
        l_doneFilename.append(p_chsFile.getLoggedDateTime().toString_yyyyMMdd());
        l_doneFilename.append(File.separator);
        l_doneFilename.append(p_chsFile.getLoggedDateTime().toString_yyyyMMdd_HHcmmcss().substring(9, 11));

        l_doneFilenameDir = new File(l_doneFilename.toString());

        if (!l_doneFilenameDir.exists())
        {
            l_renameSucceeded = createDirs(l_doneFilenameDir);
        }

        if (l_renameSucceeded)
        {
            l_renameSucceeded = rename(p_chsFile,
                                       l_doneFilenameDir.getAbsolutePath(),
                                       i_procDoneExt);
            if (l_renameSucceeded)
            {
                p_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_SUCCESSFULLY_PROC);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10130, this,
                            "Leaving " + C_METHOD_renameToSuccessfullyProcessed +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        return l_renameSucceeded;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameToDuplicate = "renameToDuplicate";
    /**
     * Renames and moves the given <code>ChsFile</code> to the configured 'duplicate files'
     * directory and extension.
     * 
     * @param p_chsFile the <code>ChsFile</code> to be renamed.
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    public boolean renameToDuplicate(ChsFile p_chsFile)
    {
        boolean      l_renameSucceeded  = true;
        StringBuffer l_dupFilename      = null;
        File         l_dupDateDirectory = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10140, this,
                            "Entering " + C_METHOD_renameToDuplicate);
        }

        l_dupFilename = new StringBuffer(i_dupDirectory);
        if (!i_dupDirectory.endsWith(File.separator))
        {
            l_dupFilename.append(File.separator);
        }
        l_dupFilename.append(p_chsFile.getLoggedDateTime().toString_yyyyMMdd());

        l_dupDateDirectory = new File(l_dupFilename.toString());
        if (!l_dupDateDirectory.exists())
        {
            l_renameSucceeded = createDirs(l_dupDateDirectory);
        }

        if (l_renameSucceeded)
        {
            l_renameSucceeded = rename(p_chsFile, l_dupDateDirectory.getAbsolutePath(), i_duplicateFileExt);
            if (l_renameSucceeded)
            {
                p_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_DUPLICATE_FILE);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10150, this,
                            "Leaving " + C_METHOD_renameToDuplicate +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        return l_renameSucceeded;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameToErroneous = "renameToErroneous";
    /**
     * Renames and moves the given <code>ChsFile</code> to the configured 'erroneous files'
     * directory and extension.
     * 
     * @param p_chsFile the <code>ChsFile</code> to be renamed.
     * @return <code>true</code> if the rename was successful, <code>false</code> otherwise.
     */
    public boolean renameToErroneous(ChsFile p_chsFile)
    {
        boolean      l_renameSucceeded  = true;
        StringBuffer l_errFilename      = null;
        File         l_errDateDirectory = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10160, this,
                            "Entering " + C_METHOD_renameToErroneous);
        }

        l_errFilename = new StringBuffer(i_errDirectory);
        if (!i_errDirectory.endsWith(File.separator))
        {
            l_errFilename.append(File.separator);
        }
        l_errFilename.append(p_chsFile.getLoggedDateTime().toString_yyyyMMdd());

        l_errDateDirectory = new File(l_errFilename.toString());
        if (!l_errDateDirectory.exists())
        {
            l_renameSucceeded = createDirs(l_errDateDirectory);
        }

        if (l_renameSucceeded)
        {
            l_renameSucceeded = rename(p_chsFile, l_errDateDirectory.getAbsolutePath(), i_erroneousFileExt);
            if (l_renameSucceeded)
            {
                p_chsFile.setStatus(ChsFile.C_PROCESSING_STATUS_ERROR_LOADING);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10170, this,
                            "Leaving " + C_METHOD_renameToErroneous +
                            ",  l_renameSucceeded = " + l_renameSucceeded);
        }
        return l_renameSucceeded;
    }


    //==========================================================================
    // Private static method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_createDirs = "createDirs";
    /**
     * Creates the directory named by the given <code>File</code> object,
     * including any necessary but nonexistent parent directories.
     * Returns <code>true</code> if and only if the directory was created,
     * along with all necessary parent directories, <code>false</code> otherwise.
     *
     * NB: This method is declared both <code>static</code> and <code>synchronized</code> in order
     *     to allow access for only one thread at a time, i.e. to avoid that more than one thread is trying
     *     to create the same directory at the same time which could cause one of them to 'fail' (the
     *     method <code>mkdirs()</code> returns <code>false</code> if the directory already exists!).
     *     
     * NB: If this operation fails it may have succeeded in creating some of the necessary parent directories.
     * 
     * @param l_dir  the directory to be created as a <code>File</code> object.
     * 
     * @return  <code>true</code> if and only if the directory was created,
     *          along with all necessary parent directories, <code>false</code> otherwise.
     */
    private static synchronized boolean createDirs(File l_dir)
    {
        boolean l_successfullyCreated = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10174, null,
                            "Entering " + C_METHOD_createDirs + 
                            ",  directory '" + l_dir.getAbsolutePath() + "' exists: " + l_dir.exists());
        }

        if (!l_dir.exists())
        {
            l_successfullyCreated = l_dir.mkdirs();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10176, null,
                            "Leaving " + C_METHOD_createDirs + 
                            ",  directory '" + l_dir.getAbsolutePath() +
                            "' is created: " + l_successfullyCreated);
        }
        return l_successfullyCreated;
    }


    //==========================================================================
    // Private method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_init = "init";
    /**
     * Initiates this <code>ChsFileDistributor</code> instance.
     * 
     * @throws PpasConfigException  if any mandatory property is missing.
     */
    private void init() throws PpasConfigException
    {
        PpasConfigException l_ppasConfEx            = null;
        String              l_filenamePartsOrder    = null;
        String[]            l_filenamePartsOrderArr = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10180, this,
                            "Entering " + C_METHOD_init);
        }

        // Get the directory to be used for files in progress.
        i_ipgDirectory = getProperty(C_PROPERTY_IPG_DIRECTORY, false, C_DEFVAL_IPG_DIRECTORY);

        // Get the root directory to be used for successfully processed files.
        i_dneDirectory = getProperty(C_PROPERTY_DNE_DIRECTORY, false, C_DEFVAL_DNE_DIRECTORY);

        // Get the root directory to be used for files with an invalid filename.
        i_invDirectory = getProperty(C_PROPERTY_INV_DIRECTORY, false, C_DEFVAL_INV_DIRECTORY);

        // Get the root directory to be used for duplicate files.
        i_dupDirectory = getProperty(C_PROPERTY_DUP_DIRECTORY, false, C_DEFVAL_DUP_DIRECTORY);

        // Get the root directory to be used for erroneous files.
        i_errDirectory = getProperty(C_PROPERTY_ERR_DIRECTORY, false, C_DEFVAL_ERR_DIRECTORY);

        i_filenameRegExp = getProperty(C_PROPERTY_FILENAME_REG_EXP, false, C_DEFVAL_FILENAME_REG_EXP);
        try
        {
            i_filenamePattern = Pattern.compile(i_filenameRegExp);
        }
        catch (PatternSyntaxException  l_patternSyntaxEx)
        {
            // ***ERROR: This is a bad property, throw a 'PpasConfigException'.
            l_ppasConfEx =
                new PpasConfigException(C_CLASS_NAME,
                                        C_METHOD_init,
                                        10182,
                                        this,
                                        null,
                                        0,
                                        ConfigKey.get().badConfigValue(C_PROPERTY_FILENAME_REG_EXP,
                                                                       i_filenameRegExp,
                                                                       "A valid regular expression."));
            i_logger.logMessage(l_ppasConfEx);
            throw l_ppasConfEx;
        }

        l_filenamePartsOrder = getProperty(C_PROPERTY_FILENAME_PARTS_ORDER,
                                           false,
                                           C_DEFVAL_FILENAME_PARTS_ORDER);
        l_filenamePartsOrderArr = l_filenamePartsOrder.split(",");
        i_filenamePartsOrderIxs = new int[l_filenamePartsOrderArr.length];
        for (int l_ix = 0; l_ix < l_filenamePartsOrderArr.length; l_ix++)
        {
            i_filenamePartsOrderIxs[l_ix] = Integer.parseInt(l_filenamePartsOrderArr[l_ix].trim());
        }
        
        i_procNotStartedExt  = getProperty(C_PROPERTY_PROC_NOT_STARTED_EXTENSION, 
                                           false,
                                           C_DEFVAL_PROC_NOT_STARTED_EXTENSION);

        i_fileInProgressExt  = getProperty(C_PROPERTY_FILE_IN_PROGRESS_EXTENSION, 
                                           false,
                                           C_DEFVAL_FILE_IN_PROGRESS_EXTENSION);

        i_dataLoadingExt     = getProperty(C_PROPERTY_DATA_LOADING_EXTENSION, 
                                           false,
                                           C_DEFVAL_DATA_LOADING_EXTENSION);

        i_procDoneExt        = getProperty(C_PROPERTY_PROC_DONE_EXTENSION, 
                                           false,
                                           C_DEFVAL_PROC_DONE_EXTENSION);

        i_duplicateFileExt   = getProperty(C_PROPERTY_DUPLICATE_FILE_EXTENSION, 
                                           false,
                                           C_DEFVAL_DUPLICATE_FILE_EXTENSION);

        i_invalidFilenameExt = getProperty(C_PROPERTY_INVALID_FILENAME_EXTENSION, 
                                           false,
                                           C_DEFVAL_INVALID_FILENAME_EXTENSION);

        i_erroneousFileExt   = getProperty(C_PROPERTY_ERRONEOUS_FILE_EXTENSION, 
                                           false,
                                           C_DEFVAL_ERRONEOUS_FILE_EXTENSION);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10190, this,
                            "Leaving " + C_METHOD_init);
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getProperty = "getProperty";
    /**
     * Returns the value, as a <code>String</code>, of the specified property.
     * 
     * @param p_property      The property.
     * @param p_mandatory     <code>true</code> if it's a mandatory property, <code>false</code> otherwise.
     * @param p_defaultValue  The default value (should be <code>null</code> for a mandatory property).
     * 
     * @return  The value of the property if defined, otherwise the default value.
     * 
     * @throws PpasConfigException  if a mandatory property is missing.
     */
    private String getProperty(String  p_property,
                               boolean p_mandatory,
                               String  p_defaultValue) throws PpasConfigException
    {
        String              l_propValue        = null;
        String              l_defaultValue     = null;
        PpasConfigException l_ppasConfigEx     = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10200, this,
                            "Entering " + C_METHOD_getProperty);
        }

        l_defaultValue = (p_mandatory  ?  null : p_defaultValue);

        if (i_ppasProperties != null)
        {
            l_propValue = i_ppasProperties.getTrimmedProperty(p_property, l_defaultValue);
        }
        else
        {
            l_propValue = l_defaultValue;
        }

        if (p_mandatory && l_propValue == null)
        {
            // ***ERROR: This is a missing mandatory property, throw a 'PpasConfigException'.
            l_ppasConfigEx =
                new PpasConfigException(C_CLASS_NAME,
                                        C_METHOD_getProperty,
                                        10202,
                                        this,
                                        null,
                                        0,
                                        ConfigKey.get().missingConfigValue(p_property, C_CLASS_NAME));
            i_logger.logMessage(l_ppasConfigEx);
            throw l_ppasConfigEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10210, this,
                            "Leaving " + C_METHOD_getProperty);
        }
        return l_propValue;
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_validateFilename = "validateFilename";
    /**
     * Validates the name of the given <code>File</code> object.
     * If the filename is a valid Call History CDR filename then a <code>ChsFile</code>
     * object is created and returned.

     * @param p_file  the file for which the filename is validated.
     * 
     * @return  a <code>ChsFile</code> object.
     */
    private ChsFile validateFilename(File p_file)
    {
        ChsFile l_chsFile       = null;
        Matcher l_matcher       = null;
        int     l_groupCount    = 0;
        String  l_filenamePart1 = null; // NB: Not allowed to be 'null' in the database.
        String  l_filenamePart2 = null; // NB: Not allowed to be 'null' in the database.
        String  l_filenamePart3 = null; // NB: Not allowed to be 'null' in the database.
        String  l_filenamePart4 = null; // NB: Not allowed to be 'null' in the database.
        int     l_groupNumber   = 0;
        String  l_extension     = null;
        int     l_extIndex      = -1;
        String  l_status        = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10220, this,
                            "Entering " + C_METHOD_validateFilename +
                            ",  file = '" + p_file.getAbsolutePath() + "'.");
        }

        l_matcher = i_filenamePattern.matcher(p_file.getName());
        if (l_matcher.matches())
        {
            // Ok, this is a valid filename according to the used regular expression.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10222, this,
                                "It is a valid filename.");
            }

            l_groupCount = l_matcher.groupCount();

            l_groupNumber   = i_filenamePartsOrderIxs[0];
            l_filenamePart1 = (l_groupCount >= l_groupNumber ? l_matcher.group(l_groupNumber) : " ");

            l_groupNumber   = i_filenamePartsOrderIxs[1];
            l_filenamePart2 = (l_groupCount >= l_groupNumber ? l_matcher.group(l_groupNumber) : " ");

            l_groupNumber   = i_filenamePartsOrderIxs[2];
            l_filenamePart3 = (l_groupCount >= l_groupNumber ? l_matcher.group(l_groupNumber) : " ");

            l_groupNumber   = i_filenamePartsOrderIxs[3];
            l_filenamePart4 = (l_groupCount >= l_groupNumber ? l_matcher.group(l_groupNumber) : " ");

            l_extIndex  = p_file.getName().lastIndexOf(C_DELIMITER_FILENAME_EXTENSION);
            l_extension = p_file.getName().substring((l_extIndex + 1));

            if (l_extension.startsWith(i_procNotStartedExt))
            {
                l_status = ChsFile.C_PROCESSING_STATUS_NOT_STARTED;
            }
            else if (l_extension.startsWith(i_fileInProgressExt))
            {
                l_status = ChsFile.C_PROCESSING_STATUS_FILE_IN_PROGRESS;
            }
            else if (l_extension.startsWith(i_dataLoadingExt))
            {
                l_status = ChsFile.C_PROCESSING_STATUS_DATA_LOADING;
            }
            else
            {
                l_status = ChsFile.C_PROCESSING_STATUS_UNDEFINED;
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10224, this,
                                "l_filenamePart1 = " + l_filenamePart1 +
                                ",  l_filenamePart2 = " + l_filenamePart2 +
                                ",  l_filenamePart3 = " + l_filenamePart3 +
                                ",  l_filenamePart4 = " + l_filenamePart4 +
                                ",  l_extension     = " + l_extension     +
                                ",  l_status        = " + l_status);
            }

            // Let's create a 'ChsFile' object.
            l_chsFile = new ChsFile(p_file,
                                    l_filenamePart1,
                                    l_filenamePart2,
                                    l_filenamePart3,
                                    l_filenamePart4,
                                    l_extension,
                                    l_status);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10230, this,
                            "Leaving " + C_METHOD_validateFilename);
        }
        return l_chsFile;
    }
}
