////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiCallHistoryServlet.Java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Servlet to call the ChsiCallHistoryService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of change>   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
 
package com.slb.sema.ppas.chs.chsi.servlet;

import java.text.ParseException;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.service.ChsiCallHistoryResponse;
import com.slb.sema.ppas.chs.chsi.service.ChsiCallHistoryService;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.common.dataclass.CustMsisdnsData;
import com.slb.sema.ppas.common.dataclass.CustMsisdnsDataSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;

/** 
 * This servlet calls methods on the ChsiCallHistoryService class to:
 * - get call history details for a specified customer. This data is passed
 * onto CallHistoryXml.jsp, which returns the information in XML format.
 */
public class ChsiCallHistoryServlet extends ChsiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiCallHistoryServlet";
    
    /** Constant defining the value that will be sent to IS API when param is DATE. Value is {@value}. */
    private static final String C_DATE = "date";
    
    /** Constant defining the value that will be sent to IS API when param is BNUM. Value is {@value}. */
    private static final String C_OTHER_PARTY_NO = "otherPartyNo";
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** The Service to be used by this servlet. */
    private ChsiCallHistoryService i_chsiCallHistoryService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */

    public ChsiCallHistoryServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        }

        return;

    }

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /**
     * Currently does nothing but will construct a ChsiCallHistoryService in
     * the fullness of time.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10900, this,
                "Entered ChsiCallHistoryServlet.doInit()");
        }

        // Should probably be constructing ChsiCallHistoryService here...
        // unfortunately no PpasRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10910, this,
                "Leaving ChsiCallHistoryServlet.doInit()");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Performs request and controls response.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     * @param p_httpSession The session.
     * @param p_chsiRequest The CHSI request.
     * @return The URL to which the request should be forwarded.
      */
    protected String doService(HttpServletRequest  p_request,
                               HttpServletResponse p_response,
                               HttpSession         p_httpSession,
                               ChsiRequest         p_chsiRequest)
    {
        String                    l_command      = null;
        String                    l_forwardUrl   = null;
        ChsiResponse              l_chsiResponse = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_chsiRequest,
                C_CLASS_NAME, 11500, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct ChsiCallHistoryService if it does not already exist.
        if (i_chsiCallHistoryService == null)
        {
            i_chsiCallHistoryService = new ChsiCallHistoryService(p_chsiRequest, i_logger, i_chsiContext);
        }

        try 
        {
            // Get the command from the http request
            l_command = getValidParam( p_chsiRequest,
                                       0,
                                       p_request,
                                       "command",
                                       true, // command is mandatory ...
                                       "",   // so there is no default
                                       new String [] {"GET_CALLS"});
          
            if (l_command.equals("GET_CALLS"))
            {
                l_forwardUrl = getCallHistoryDetails(p_request, p_chsiRequest);
            } // end (if l_command = GET_CALLS)
                        
        } // end try
        catch ( PpasServletException l_servletE)
        {
            // Reset the URL to forward to the general JSP page for handling
            // a response with no service specific data.
            l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
            
            // Handle the case of an invalid or missing parameter in the
            // incoming request
            l_chsiResponse = handleErrorResponse(p_chsiRequest, 0, l_servletE);

            // Store the response in the request
            p_request.setAttribute("p_chsiResponse", l_chsiResponse);

        } // end catch

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 11800, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return(l_forwardUrl);
    }

    //------------------------------------------------------------------------
    // Private Methods
    //------------------------------------------------------------------------
    /**
     * Performs a GET_CALLS request.
     * @param p_request     The servlet request.
     * @param p_chsiRequest The CHSI request.
     * @return The URL to which the request should be forwarded.
     * @throws PpasServletException Invalid CHSI version, no feature licence,
     *                              invalid CallHistory serial number.
     */
    private String getCallHistoryDetails(HttpServletRequest p_request,
                                         ChsiRequest        p_chsiRequest)
        throws PpasServletException
    {
        String       l_forwardUrl       = null;
        String       l_subsList         = null;
        String       l_startDateString  = null;
        String       l_endDateString    = null;
        String       l_maxRecsString    = null;
        String       l_otherPartyNo     = null;
        String       l_sortField        = null;
        String       l_sortOrderString  = null;
        String       l_levelString      = null;
        PpasDate     l_startDate        = null;
        PpasDate     l_endDate          = null;
        //Integer      l_maxRecs          = null;
        int l_maxRecs = 0;
        boolean      l_descendSort      = true;
        boolean      l_subLevel         = true;
        CustMsisdnsDataSet l_custMsisdnsDataSet = null;
        
        ChsiCallHistoryResponse l_chsiCallHistoryResponse;
        
        // Default forward url.
        l_forwardUrl = "/jsp/chsi/callhistoryxml.jsp";
        
        l_subsList = getValidParam(p_chsiRequest,
                                   0,
                                   p_request,
                                   "subscriberList",
                                   true,
                                   null,
                                   PpasServlet.C_TYPE_ANY); 
        
        p_chsiRequest.setChsiSubsList(l_subsList);

        l_startDateString = getValidParam(p_chsiRequest,
                                          PpasServlet.C_FLAG_DATE_ONLY,
                                          p_request,
                                          "startDate",
                                          false,
                                          null,
                                          PpasServlet.C_TYPE_DATE );

        if (l_startDateString != null)
        {
            l_startDate = new PpasDate(l_startDateString);
        }
        
        l_endDateString = getValidParam(p_chsiRequest,
                                        PpasServlet.C_FLAG_DATE_ONLY,
                                        p_request,
                                        "endDate",
                                        false,
                                        null,
                                        PpasServlet.C_TYPE_DATE );

        if (l_endDateString != null)
        {
            l_endDate = new PpasDate(l_endDateString);
        }
        
        l_maxRecsString = getValidParam(p_chsiRequest,
                                        0,
                                        p_request,
                                        "maxRecords",
                                        false,
                                        null,
                                        PpasServlet.C_TYPE_NUMERIC,
                                        10,
                                        C_OPERATOR_LESS_THAN_OR_EQUAL);
      
        if (l_maxRecsString != null)
        {
            l_maxRecs = Integer.parseInt(l_maxRecsString);
        }
        
        l_otherPartyNo = getValidParam(p_chsiRequest,
                                       PpasServlet.C_FLAG_ALLOW_BLANK,
                                       p_request,
                                       "otherPartyNumber",
                                       false,
                                       null,
                                       PpasServlet.C_TYPE_ANY,
                                       15,
                                       C_OPERATOR_LESS_THAN_OR_EQUAL);
               
        if (l_otherPartyNo != null)  
        {
            l_sortField = getValidParam(p_chsiRequest,
                                        0,
                                        p_request,
                                        "sortField",
                                        false,
                                        null,
                                        new String[] {"DATE", "BNUM"},
                                        true);           
        }
        
        l_sortField = l_sortField == null || l_sortField.equalsIgnoreCase("DATE") ? C_DATE : C_OTHER_PARTY_NO;

        if (l_sortField != null) 
        {      
            l_sortOrderString = getValidParam(p_chsiRequest,
                                              0,
                                              p_request,
                                              "sortOrder",
                                              false,
                                              null,
                                              new String[] {"ASC", "DSC"},
                                              true);
        }
        
        l_descendSort = l_sortOrderString == null || l_sortOrderString.equalsIgnoreCase("DSC");

        if (l_sortOrderString != null)
        {
            l_levelString = getValidParam(p_chsiRequest,
                                          0,
                                          p_request,
                                          "level",
                                          false,
                                          null,
                                          new String[] {"SUB", "ACC"},
                                          true);
        }
        
        l_subLevel = l_levelString == null || l_levelString.equalsIgnoreCase("SUB");

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                           C_CLASS_NAME, 11602, this,
                           "Got parameters: " +
                           "startDate = ["    + l_startDate          + "], " +
                           "endDate = ["      + l_endDate            + "], " +
                           "maxRecs = ["      + l_maxRecs            + "], " +
                           "otherPartyNo= ["  + l_otherPartyNo       + "], " +
                           "sortField = ["    + l_sortField          + "], " +
                           "descendSort = ["  + l_descendSort        + "], " +
                           "subLevel= ["      + l_subLevel           + "]");
        }
        
        l_custMsisdnsDataSet = parseSubscriberList(l_subsList, p_chsiRequest);
        
        // Get the call history details for specified selection criterias.
        l_chsiCallHistoryResponse = i_chsiCallHistoryService.getCallHistoryDetails(
                                                                          p_chsiRequest,
                                                                          p_chsiRequest.getSafeTimeToLive(),
                                                                          l_custMsisdnsDataSet,
                                                                          l_maxRecs,
                                                                          l_otherPartyNo,
                                                                          l_sortField,
                                                                          l_descendSort);
      
        
        p_request.setAttribute("p_chsiResponse", l_chsiCallHistoryResponse);

            
        return(l_forwardUrl);
    } // end of doGetHistory

    /**
     * Parses the subscriberList in the request and constructs a CustMsisdnsDataSet object.
     * @param p_list The subscriber list passed in the request.
     * @param p_chsiRequest The CHSI request.
     * @return Set of MSISDN's to query with appropriate start/end date/times.
     * @throws PpasServletException If we cannot parse the data.
     */
    private CustMsisdnsDataSet parseSubscriberList(String p_list, 
                                                   ChsiRequest p_chsiRequest) 
    throws PpasServletException
    {
        CustMsisdnsDataSet l_custMsisdnDataSet = new CustMsisdnsDataSet();
        Msisdn l_msisdn;
        PpasDateTime l_startDateTime;
        PpasDateTime l_endDateTime;
        
        MsisdnFormat l_msisdnFormat = i_chsiContext.getMsisdnFormatInput();
        String l_msisdnStr;
        String l_startDateStr;
        String l_endDateStr;
        
        String[] l_strArr = p_list.split(",",-1);
        int k = l_strArr.length/3;
        int j = 0;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                           C_CLASS_NAME, 17100, this,
                           "Input String: '" + p_list + "' Tokens: " + l_strArr.length + " Number Iterations: " + k);
        }
        
        for (int i = 0; i < k; i++)
        {
            System.out.println("i is "+i);            
            l_msisdnStr = l_strArr[j];
            
            try
            {
                l_msisdn = l_msisdnFormat.parse(l_msisdnStr);
            }
            catch (ParseException e)
            {
                PpasServletException l_pSE = new PpasServletException(C_CLASS_NAME,
                                                                      "parseSubscriberList",
                                                                      11601,
                                                                      this,
                                                                      p_chsiRequest,
                                                                      0,
                                                                      ServletKey.get()
                                                                      .parseMsisdnError(l_msisdnStr,
                                                                                        l_msisdnFormat
                                                                                        .toPattern()),
                                                                      e);

                i_logger.logMessage(l_pSE);

                throw l_pSE;
            }
            
            l_startDateStr = l_strArr[j+1];
            l_endDateStr = l_strArr[j+2];
            j = j+3;

            if (l_startDateStr != null && !l_startDateStr.trim().equals(""))
            {
                l_startDateTime = new PpasDateTime(l_startDateStr);
            }
            else
            {
                l_startDateTime = new PpasDateTime();
            }

            if (l_endDateStr != null && !l_endDateStr.trim().equals(""))
            {
                l_endDateTime = new PpasDateTime(l_endDateStr);
            }
            else
            {
                l_endDateTime = new PpasDateTime();
            }
 
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VHIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                               C_CLASS_NAME, 17100, this,
                               "It: " + i + " Found: " + l_msisdn + " Start: '" + l_startDateTime +
                               "' End: '" + l_endDateTime + "'");
            }
            
            l_custMsisdnDataSet.add(new CustMsisdnsData(l_msisdn, l_startDateTime, l_endDateTime));
        }
        
        return l_custMsisdnDataSet;
    }
}
