////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiForwardingServlet.Java
//      DATE            :       13-Sep-2007
//      AUTHOR          :       Steven James
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Forwards a request to a configured path
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;

/**
 * Servlet to forward requests to a different path
 */
public class ChsiForwardingServlet extends PpasServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiForwardingServlet";
    
    /** Name of initialisation parameter for the target path in web.xml. */
    private static final String C_TARGET_PATH_INIT_PARAM = "targetPath";
    
    /** Name of initialisation parameter for the calls per sub per day in web.xml. */
    private static final String C_CALLS_INIT_PARAM = "callsPerSubPerDay";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */
    public ChsiForwardingServlet()
    {
        super();
    }

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /**
     * Currently does nothing.
     */
    public void doInit()
    {   
        PpasProperties l_configProperties = new PpasProperties(null);

        try
        {
            l_configProperties.loadLayeredProperties();
        }
        catch (PpasConfigException e)
        {
            e.printStackTrace();
        }
      
        WebDebug.setDebug(WebDebug.C_FLAG_SYSTEM_OVERRIDES,
                          l_configProperties);
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           C_CLASS_NAME, 10000, this,
                           "Initializing " + C_CLASS_NAME + ".doInit()");
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END,
                           C_CLASS_NAME, 10000, this,
                           "Initialized " + C_CLASS_NAME + ".doInit()");
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_service = "service";
    /**
     * Service method to control requests and responses to the JSP.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     */
    public void service(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response)
    {
        String                  l_forwardToURL = "";
        String                  l_callsPerSubPerDay = "";

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           C_CLASS_NAME, 11500, this,
                           "ENTERED " + C_METHOD_service);
        }

        try
        {
            RequestDispatcher       l_forwardToRequestDispatcher;

            l_callsPerSubPerDay   = getServletConfig().getInitParameter(C_CALLS_INIT_PARAM);
            l_forwardToURL        = getServletConfig().getInitParameter(C_TARGET_PATH_INIT_PARAM);

            l_forwardToURL = l_forwardToURL + "?" +C_CALLS_INIT_PARAM+"="+l_callsPerSubPerDay;
            
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_APPLICATION_DATA,
                               C_CLASS_NAME, 11500, this,
                               "l_forwardToURL: " + l_forwardToURL);
            }
                
            l_forwardToRequestDispatcher = getServletContext().getRequestDispatcher(l_forwardToURL);
            
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_APPLICATION_DATA,
                               C_CLASS_NAME, 12600, this,
                               "Request dispatcher: " + l_forwardToRequestDispatcher);
            }

            l_forwardToRequestDispatcher.forward(p_request, p_response);

        }
        catch (ServletException l_servletE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_APPLICATION_DATA,
                               C_CLASS_NAME, 12700, this,
                               "Caught ServletException: " + l_servletE);
            }
            // Handle the case of an invalid or missing parameter in the incoming request
            //l_posiResponse = handleErrorResponse(p_posiRequest, 0, l_servletE);
            System.err.println(l_servletE);
            l_servletE.printStackTrace();

        }
        catch (IOException l_ioE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_APPLICATION_DATA,
                               C_CLASS_NAME, 12700, this,
                               "Caught IOException: " + l_ioE);
            }
            System.out.println(l_ioE);
            l_ioE.printStackTrace();
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END,
                           C_CLASS_NAME, 12800, this,
                           "LEAVING " +C_METHOD_service+", forwarding to " + l_forwardToURL);
        }
    }
}
