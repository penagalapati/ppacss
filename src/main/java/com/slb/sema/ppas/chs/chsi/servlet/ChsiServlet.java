////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiServlet.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Servlet Superclass for all ChsiXServlet classes.
//
///////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
///////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------------
// 26/03/07 | K Bond     | Use locale GB not UK.           | PpacLon#1867/11221
//          |            |                                 |      
//////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.servlet;

import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.chs.chsi.support.ChsiSession;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasRuntimeException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.support.LogInOutInterface;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.PpasServletLicenceException;
import com.slb.sema.ppas.common.support.PpasServletMsg;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.support.SessionTimeoutInterface;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.HttpSessionsTable;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.is.isapi.PpasSessionService;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.instrumentation.MultiTimer;
import com.slb.sema.ppas.util.instrumentation.TimerSetInstrument;

/** Servlet Superclass for all ChsiXServlet classes. */
public abstract class ChsiServlet extends PpasServlet
    implements InstrumentedObjectInterface, SessionTimeoutInterface
{
    //------------------------------------------------------------------------
    // Class level constants 
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiServlet";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Indicates whether clients will be forced to login or not. */
    protected boolean             i_verifyLogin;

    /** Time (in milliseconds) to wait:
     *  1. To get a JDBC connection from the pool.
     */
    protected long                i_timeout;

    /** Context containing useful objects like pools and properties. */
    protected ChsiContext         i_chsiContext;

    /** Timer Set for servlet. */
    protected TimerSetInstrument  i_timerSet;

    /** The timers registered name id. */
    protected int                 i_timerSetRegisteredNameId;

    /** Instrument set for servlet. */
    protected InstrumentSet       i_instrumentSet;

    /** Request timer set instrument. */
    protected TimerSetInstrument  i_requestTimerSetInstrument;

    /** The instrument manager. */
    protected InstrumentManager   i_instrumentManager;

    /** Number of requests received by this servlet. */
    protected CounterInstrument     i_requestsReceivedCounterInstrument;

    /** Number of requests processed by this servlet. */
    protected CounterInstrument     i_requestsProcessedCounterInstrument;

    /** Max inactive interval. */
    private int                     i_maxInactiveInterval;
    
    /** Number of 'not logged in' responses received by chsi session servlet. */
    protected CounterInstrument   i_notLoggedInCounterInstrument;   
    
    /** Number of requests failed by chsi session. */
    protected CounterInstrument     i_keepaliveFailedCounterInstrument; 

    //------------------------------------------------------------------------
    // Class Methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Calls doInit and converts any PpasException thrown to a
     * ServletException. Overrides PpasServlet as it does some potentially
     * Chsi specific initialisation.
     * @throws ServletException If an error occurs.
     */
    public void init()
        throws ServletException
    {
        System.out.println("In ChsiServlet init()");
        if (getInitException() == null)
        {
            InstrumentedObjectInterface l_instrumentedObject;
            InstrumentSet          l_instrumentSet;
            
            System.out.println("In ChsiServlet init() inside if getInitException() == null");
            try
            {
                // This init is over-riding so should NOT call super.init()
 
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_VLOW,
                        WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                        WebDebug.C_ST_START,
                        C_CLASS_NAME, 11100, this,
                        "ENTERED init(). Initialising servlet of class "
                        + (this.getClass()).getName());
                }

                i_chsiContext = (ChsiContext)getServletContext().getAttribute(
                             "com.slb.sema.ppas.chs.chsi.support.ChsiContext");

                i_verifyLogin = ((Boolean)i_chsiContext
                    .getAttribute(
                        "com.slb.sema.ppas.chs.chsi.support.ChsiContext.verifyLogin")).
                            booleanValue();

                initLogger();

                //   Names of attributes obtained from context have changed.
                i_timeout =
                    ((Long)i_chsiContext.getAttribute(
                            "com.slb.sema.ppas.support.PpasContext.connResponseTimeout")).
                            longValue();

                // PpaLon#1570/6641 [END]

                i_maxInactiveInterval = 
                    ((Integer)i_chsiContext.getAttribute(
                            "com.slb.sema.ppas.chs.chsi.support.ChsiContext.maxInactiveInterval")).intValue();

                // Instrument set for this servlet
                i_instrumentSet = new InstrumentSet("root");
                i_requestsReceivedCounterInstrument = new CounterInstrument(
                    "Requests Received",
                    "Number of requests received by this servlet."
                    );
                i_instrumentSet.add(i_requestsReceivedCounterInstrument);
                i_requestsProcessedCounterInstrument = new CounterInstrument(
                    "Requests Processed",
                    "Number of requests processed by this servlet."
                    );
                i_instrumentSet.add(i_requestsProcessedCounterInstrument);
                
                i_notLoggedInCounterInstrument = new CounterInstrument("Not Logged in Response Received",
                                                                       "Number of not logged responses received by the posi session servlet.");

                i_instrumentSet.add(i_notLoggedInCounterInstrument);

                i_keepaliveFailedCounterInstrument = new CounterInstrument("KEEPALIVE Requests Failed",
                                                                           "Number of failed requests received by the posi session servlet.");

                i_instrumentSet.add(i_keepaliveFailedCounterInstrument);


                // Register this servlet with the InstrumentManager
                i_instrumentManager =
                    (InstrumentManager)(i_chsiContext.getAttribute(
                        "com.slb.sema.ppas.util.instrumentation.InstrumentManager"
                    ));
                i_instrumentManager.registerObject(this);

                //
                // Get the cross servlet InstrumentSet, get Timer Set from it
                // and register this servlet name in it for use of timing
                // the service method.
                //
                l_instrumentedObject = i_instrumentManager.getManagedObject(
                            "com.slb.sema.ppas.util.instrumentation." +
                                    "InstrumentedObjectImp",
                            "root");
                            
                l_instrumentSet = l_instrumentedObject.getInstrumentSet();
                i_timerSet = (TimerSetInstrument)l_instrumentSet.getInstrument(
                    "request timers");
                    
                i_timerSetRegisteredNameId =
                    i_timerSet.registerName(this.getClass().getName()
                        + "(doService)");

                i_businessConfigCache = (BusinessConfigCache)
                                            i_chsiContext.getAttribute("BusinessConfigCache");
                
                doInit();
            }
            catch (PpasException e)
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_VLOW,
                        WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                        WebDebug.C_ST_TRACE | WebDebug.C_ST_ERROR,
                        C_CLASS_NAME, 11150, this,
                        "init():Converting PpasException : " + e
                        + " to ServletException");
                }

                // Don't throw ServletException here since this will result
                // in Exception being generated instead of a response. Instead,
                // Store the exception that has occur (it should already have
                // been logged) so that a sensible Chsi response can be
                // returned later by the Servlet.
                // throw new ServletException("Error initialising", l_PpasE);

                i_servletInitE = e;
            }
            catch (RuntimeException l_rE)
            {
                l_rE.printStackTrace(System.err);

                // Don't throw ServletException here since this will result
                // in Exception being generated instead of a response. Instead,
                // Store the exception that has occur (it should already have
                // been logged) so that a sensible Chsi response can be
                // returned later by the Servlet.
                // throw(l_rE);
                i_servletInitE = l_rE;
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 11200, this,
                "LEAVING init(). Initialised servlet of class " + (this.getClass()).getName());
        }
        System.out.println("Out ChsiServlet init()");
    }

    //------------------------------------------------------------------------
    // Object Methods
    //------------------------------------------------------------------------
    /** Validate a HTTP session.
     * 
     * @param p_request The servlet request.
     * @return A HTTP session.
     */
    protected HttpSession validateHttpSession(HttpServletRequest p_request)
    {
        HttpSession            l_httpSession = null;

        if ((l_httpSession = p_request.getSession(true)) == null)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET | WebDebug.C_APP_SESSION,
                    WebDebug.C_ST_REQUEST,
                    C_CLASS_NAME, 15100, null,
                    "No current HttpSession");
            }
        }

        return(l_httpSession);
    }


    /** Initialises the logger attribute of the servlet.
     * @throws PpasException if an error occurs.
     */
    protected void initLogger()
        throws PpasException
    {
        PpasLoggerPool         l_loggerPool;
        PpasServletException   l_ppasServletE;

        // Get the LoggerPool object to be used
        l_loggerPool = (PpasLoggerPool) i_chsiContext.getAttribute("LoggerPool");

        if (l_loggerPool == null)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                    WebDebug.C_ST_ERROR,
                    C_CLASS_NAME, 10155, this,
                    "FATAL SERVLET ERROR - Unable to obtain LoggerPool"
                    + " from servlet context.");
            }

            // Raise and output (can't log!) a 'failed to get essential
            // attribute' type exception.
            l_ppasServletE = new PpasServletException(
                    C_CLASS_NAME, C_METHOD_init, 10255, this,
                    null, // PpasRequest not implemented here yet!
                    0, ServletKey.get().essentialAttribFail("LoggerPool"));

            l_ppasServletE.printStackTrace(System.err);
            throw(l_ppasServletE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                WebDebug.C_ST_TRACE,
                C_CLASS_NAME, 10256, this,
                "Obtained LoggerPool to be used by this servlet.");
        }

        // Obtain the logger to be used by this application
        i_logger = l_loggerPool.getLogger();

        if (i_logger == null)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                    WebDebug.C_ST_ERROR,
                    C_CLASS_NAME, 10357, this,
                    "Unable to obtain Logger from LoggerPool.");
            }

            // Raise and output a 'failed to get essential config data' type
            // exception
            l_ppasServletE = new PpasServletException(
                    C_CLASS_NAME, C_METHOD_init, 10260, this,
                    null, // PpasRequest not implemented here yet!
                    0, ServletKey.get().essentialConfigFail("Logger"));

            l_ppasServletE.printStackTrace(System.err);
            throw(l_ppasServletE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET | WebDebug.C_APP_INIT,
                WebDebug.C_ST_ERROR,
                C_CLASS_NAME, 10458, this,
                "Obtained Logger " + i_logger.getName() + " from LoggerPool.");
        }

        return;
    }

     
     /** Returns servlet's name.
     * @return The name of this servlet.
     */
    public String getName()
    {
        return(this.getClass().getName() + "@" + this.hashCode());
    }

    /** Returns servlets instrument set.
     * @return The instrument set associated with this servlet.
     */
    public InstrumentSet getInstrumentSet()
    {
        return(i_instrumentSet);
    }

    /** Handle pages with no specific data.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     * @throws javax.servlet.ServletException If an error occurs in the servlet.
     * @throws java.io.IOException If an I/O error occurs.
     */
    public void service(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response)
        throws
            javax.servlet.ServletException,
            java.io.IOException
    {
        String                 l_forwardUrl = null;
        HttpSession            l_httpSession = null;
        ChsiRequest            l_chsiRequest =  new ChsiRequest(null);
        ChsiResponse           l_chsiResponse;
        ChsiSession            l_chsiSession;
        int                    l_timerId = 0;
        MultiTimer             l_timer = null;
        String                 l_chsiVersion = null;

        ChsiContext            l_chsiContext;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                C_CLASS_NAME, 13200, null,
                "Servlet path is [" + p_request.getServletPath() + "]");
        }
        if (WebDebug.on)
        {
            StringBuffer l_sb = new StringBuffer();
            l_sb.append(p_request.getRequestURL());
            l_sb.append("?");
            l_sb.append(p_request.getParameter("p_command"));
            
            Enumeration l_enum = p_request.getParameterNames();
            
            while (l_enum.hasMoreElements())
            {
                String l_param = (String)l_enum.nextElement();
                
                if (!l_param.equals("p_command"))
                {
                    l_sb.append("&");
                    l_sb.append(l_param);
                    l_sb.append("=");
                    l_sb.append(p_request.getParameter(l_param));
                }
            }
            
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                           C_CLASS_NAME, 25200, null,
                           "Command: " + l_sb);
        }

        Exception l_initE = getInitException();
        if (l_initE != null)
        {
            // Reset the URL to forward to the general JSP page for handling
            // a response with no service specific data.
            l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

            String l_msgText = "";

            if (l_initE instanceof PpasException)
            {
                l_msgText = ((PpasException)l_initE).getMsgText (Locale.UK);
            }
            else if (l_initE instanceof PpasRuntimeException)
            {
                l_msgText = ((PpasRuntimeException)l_initE).toString();
            }
            else
            {
                l_msgText = l_initE.toString();
            }

            l_chsiResponse = new ChsiResponse
                                (l_chsiRequest,
                                 new Locale("en", "GB"),
                                 ChsiResponse.C_KEY_SYS_INIT_FAILURE,
                                 new Object [] {l_msgText},
                                 ChsiResponse.C_SEVERITY_FAILURE);

            p_request.setAttribute("p_chsiRequest", l_chsiRequest);

            l_chsiContext = new ChsiContext (l_chsiRequest,
                                             0,
                                             i_logger);
            p_request.setAttribute("p_chsiContext", l_chsiContext);

            // Store the response in the request
            p_request.setAttribute("p_chsiResponse",
                                   l_chsiResponse);
        }
        else
        {
            try
            {
                l_timer = i_timerSet.getNewTimer();
                l_timerId = l_timer.start(i_timerSetRegisteredNameId);
                p_request.setAttribute ("com.slb.sema.ppas.util.MultiTimer",
                                        l_timer);

                // Returns current session for browser or creates a new one if none
                // currently exists, ie this sesssion has just started.
                l_httpSession = validateHttpSession(p_request);

                // Create a ChsiRequest object and do related processing...
                l_chsiRequest = createChsiRequest(p_request,
                                                  p_response,
                                                  l_timer);

                // Increment requests received counter.
                i_requestsReceivedCounterInstrument.increment();

                getDefaultParams(p_request,
                                 l_chsiRequest);

                l_chsiSession = l_chsiRequest.getChsiSession();

                // Notify context and session that request has been received.
                i_chsiContext.requestReceived();
                l_chsiSession.requestReceived(l_chsiRequest, 0);

                // If the login service is turned on a session must be logged
                // in before using any  other Chsi service.
                if (i_verifyLogin && !l_chsiSession.isLoggedIn())
                {
                    // Create response message indicating that the client must
                    // login before using Chsi services.
                    l_chsiResponse = new ChsiResponse
                            (l_chsiRequest,
                             l_chsiRequest.getChsiSession().getSelectedLocale(),
                             ChsiResponse.C_KEY_LOGIN_REQUIRED,
                             (Object[])null,
                             ChsiResponse.C_SEVERITY_FAILURE);

                    // Append the response to the Http request.
                    p_request.setAttribute("p_chsiResponse", l_chsiResponse);

                    // Forward to the NoServiceDataXML.jsp.
                    l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
                }
                else
                {
                    l_chsiVersion = getValidParam(l_chsiRequest,
                                                  0,
                                                  p_request,
                                                  "version",
                                                  false,
                                                  null,
                                                  ChsiServlet.C_TYPE_ANY);
        
                    if (l_chsiVersion != null)
                    {
                        // version was supplied, check that it is valid
                        if (!l_chsiVersion.equals(ChsiSession.C_CHSI_VERSION_1) )
                        {
                            l_chsiResponse = new ChsiResponse
                                    (l_chsiRequest,
                                     l_chsiRequest.getChsiSession().getSelectedLocale(),
                                     ChsiResponse.C_KEY_INVALID_VERSION,
                                     new String[] {l_chsiVersion},
                                     ChsiResponse.C_SEVERITY_FAILURE);

                            // Append the response to the Http request.
                            p_request.setAttribute("p_chsiResponse", l_chsiResponse);

                            // Forward to the NoServiceDataXML.jsp.
                            l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
                        }
                        else
                        {
                            l_chsiRequest.setChsiVersion(l_chsiVersion);
                        }
                    }
                    else if (!i_verifyLogin)
                    {
                        // logins are disabled, and no version was supplied in the request
                        // this is bad...
                        l_chsiResponse = new ChsiResponse
                                (l_chsiRequest,
                                 l_chsiRequest.getChsiSession().getSelectedLocale(),
                                 ChsiResponse.C_KEY_MESSAGE_VERSION_REQUIRED,
                                 (Object[])null,
                                 ChsiResponse.C_SEVERITY_FAILURE);

                        // Append the response to the Http request.
                        p_request.setAttribute("p_chsiResponse", l_chsiResponse);

                        // Forward to the NoServiceDataXML.jsp.
                        l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";
                    }

                    if (l_forwardUrl == null)
                    {
                        if (l_chsiVersion == null)
                        {
                            // if the version was not set in the message, then use the version from the
                            // session. if login is enabled this version will always be seton the session, 
                            // else if login is not enabled we won't get to this point as the message must
                            // have a version.
                            l_chsiRequest.setChsiVersion(l_chsiRequest.getChsiSession().getChsiVersion());
                        }

                        l_forwardUrl = doService(p_request,
                                                 p_response,
                                                 l_httpSession,
                                                 l_chsiRequest);
                    }
                }

                // Increment requests processed counter.
                i_requestsProcessedCounterInstrument.increment();

                // Notify context and session that request has been processed.
                i_chsiContext.requestProcessed();
                l_chsiSession.requestProcessed(l_chsiRequest, 0);

                l_timer.end(l_timerId);

            } // end try

            catch (PpasServletException l_servletE)
            {
                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = new String("/jsp/chsi/noservicedataxml.jsp");

                // Handle the case of an invalid or missing parameter in the
                // incoming request
                l_chsiResponse = handleErrorResponse(l_chsiRequest, 0, l_servletE);

                // Store the response in the request
                p_request.setAttribute("p_chsiResponse",
                                       l_chsiResponse);
            }

            // Add catch all so that we can generate Chsi response if a runtime
            // exception has occurred
            catch (RuntimeException l_rtE)
            {
                String l_exceptionTxt = "";
                if (!(l_rtE instanceof PpasRuntimeException))
                {
                    // Should generate and log exception descended from
                    // PpasRuntimeException here ... but for the time being,
                    // just output to system console.
                    l_rtE.printStackTrace(System.err);

                    l_exceptionTxt = l_rtE.toString();
                }
                else
                {
                    l_exceptionTxt = ((PpasRuntimeException)l_rtE).
                                                        getMsgText (Locale.UK);
                }

                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = "/jsp/chsi/noservicedataxml.jsp";

                l_chsiResponse = new ChsiResponse
                                    (l_chsiRequest,
                                     new Locale("en", "GB"),
                                     ChsiResponse.C_KEY_RUNTIME_FAILURE,
                                     new Object [] {l_exceptionTxt},
                                     ChsiResponse.C_SEVERITY_FAILURE);

                p_request.setAttribute("p_chsiRequest", l_chsiRequest);

                l_chsiContext = new ChsiContext (l_chsiRequest,
                                                 0,
                                                 i_logger);
                p_request.setAttribute("p_chsiContext", l_chsiContext);

                // Store the response in the request
                p_request.setAttribute("p_chsiResponse",
                                       l_chsiResponse);
            }

        } // end if (! system initialisation failed)

        forwardToUrl(p_request, p_response, l_forwardUrl, l_chsiRequest);

        return;
    }

    /** Create a Chsi request.
     * 
     * @param p_request   The servlet Request.
     * @param p_response  The servlet Response.
     * @param p_timer     Timer.
     * @return A Chsi request.
     * @throws javax.servlet.ServletException If a servlet error occurs.
     * @throws java.io.IOException            If an IO error occurs.
     */
    protected ChsiRequest createChsiRequest(HttpServletRequest  p_request,
                                            HttpServletResponse p_response,
                                            MultiTimer          p_timer)
        throws
            javax.servlet.ServletException,
            java.io.IOException
    {
        HttpSession            l_httpSession = null;
        ChsiSession            l_chsiSession = null;
        ChsiRequest            l_chsiRequest = null;
        Locale                 l_defaultLocale = new Locale("en", "GB");
        String                 l_url = null;
        String                 l_command = null;

        setDefaultHttpHeaders(p_response);

        // Returns current session for browser or creates a new one if none
        // currently exists, ie this sesssion has just started.
        l_httpSession = validateHttpSession(p_request);

        // Check to see if we have already created a Session for this
        // browser...
        l_chsiSession = (ChsiSession)l_httpSession.getAttribute("WebSession");

        // ...and if not we create a new one. //
        if (l_chsiSession == null)
        {
            l_chsiSession = new ChsiSession(
                    (HttpSessionsTable)getServletContext().getAttribute("HttpSessionsTable"),
                    l_httpSession,
                    i_instrumentManager,
                    (SessionTimeoutInterface)this);

            // Set the session timeout. This must be done here instead of validateHttpSession()
            // due to the behaviour of the Chsi on logging out (logging out creates a new session
            // which becomes the session used on logging back in)
            l_httpSession.setMaxInactiveInterval(i_maxInactiveInterval);

            // Set the Locale to "en_GB" - default for Chsi.
            l_chsiSession.setSelectedLocale(l_defaultLocale);
        }

        l_chsiSession.setMultiTimer(p_timer);

        // Added when changing from PpasSession to PpasRequest to use in calls
        // to doService on sub classes
        l_chsiRequest = new ChsiRequest(l_chsiSession);

        l_chsiRequest.setContext(i_chsiContext);

        // Store the URL and the value of the command parameter in the ChsiRequest
        // object so that they can be echoed back in the returned XML page.
        l_url = p_request.getContextPath() + p_request.getServletPath();

        l_chsiRequest.setUrl(l_url);

        l_command = p_request.getParameter("command");

        l_chsiRequest.setCommand(l_command);

        p_request.setAttribute("p_chsiRequest", l_chsiRequest);

        // Store the context in the request to make it available in the JSP
        // scripts
        p_request.setAttribute("p_chsiContext", i_chsiContext);

        // Return the Chsi request object.
        return l_chsiRequest;
    }

    /** Get default parameters.
     * 
     * @param p_request     The servlet request.
     * @param p_chsiRequest The Chsi request.
     * @throws PpasServletException If an error occurs.
     */
    protected void getDefaultParams(HttpServletRequest p_request,
                                    ChsiRequest        p_chsiRequest)
        throws
            PpasServletException
    {
        String                 l_requestOpid = null;
        String                 l_transId     = null;
        // Get the requestOpid and transID out of the Http request and
        // store them in the Chsi request.
        l_requestOpid = getValidParam(p_chsiRequest,
                                      C_FLAG_NULL_IF_ABSENT,
                                      p_request,
                                      "requestOpid",
                                      false,
                                      "",
                                      PpasServlet.C_TYPE_ANY,
                                      8,
                                      PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        p_chsiRequest.setOpid(l_requestOpid);
        
        l_transId = getValidParam(p_chsiRequest,
                                  0,
                                  p_request,
                                  "transId",
                                  false,
                                  "",
                                  PpasServlet.C_TYPE_ANY,
                                  20,
                                  PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        p_chsiRequest.setTransId(l_transId);
    }

    /** Forward the request to another URL.
     * 
     * @param p_request      The servlet request.
     * @param p_response     The servlet response.
     * @param p_forwardUrl   URL to which request should be forwarded.
     * @param p_chsiRequest  The Chsi request.
     * @throws javax.servlet.ServletException If an error occurs in the servlet.
     * @throws java.io.IOException            If an IO error occurs when forwarding. 
     */
    protected void forwardToUrl(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        String                 p_forwardUrl,
        ChsiRequest            p_chsiRequest)
        throws
            javax.servlet.ServletException,
            java.io.IOException
    {

        RequestDispatcher      l_rd;

        if (p_forwardUrl != null)
        {
            // Need to do a forward.

            l_rd = getServletContext().getRequestDispatcher(p_forwardUrl);

            if (l_rd != null)
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_HIGH,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_RESPONSE,
                        p_chsiRequest,
                        C_CLASS_NAME, 11500, null,
                        "Forwarding request to " + p_forwardUrl);
                }

                l_rd.forward(p_request, p_response);

                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_HIGH,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_RESPONSE,
                        C_CLASS_NAME, 11600, null,
                        "Forward returned from " + p_forwardUrl);
                }
            }
            else
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_VLOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_ERROR,
                        p_chsiRequest,
                        C_CLASS_NAME, 11700, null,
                        "Internal software eror - no "
                        + "request dispatcher found for " + p_forwardUrl);
                }
            }
        }
        return;
    }

    /** Set default HTTP headers.
     * 
     * @param p_response The servlet response.
     */
    protected void setDefaultHttpHeaders(HttpServletResponse p_response)
    {
        p_response.setHeader("Expires", "0");
        p_response.setHeader("Cache-Control", "no-cache");

        return;
    }

    /**
     * Service method to control requests and responses to the JSP.
     * @param p_request     The servlet request.
     * @param p_response    The servlet response.
     * @param p_httpSession The sevlet session
     * @param p_chsiRequest The chsi request
     * @return A String
     */
    protected abstract String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        ChsiRequest            p_chsiRequest
        );

    /** Redirect a request to the home page.
     * 
     * @param p_request   The servlet request.
     * @param p_response  The servlet response.
     * @throws java.io.IOException If an IO error occurs accessing the home page.
     */
    protected void redirectToHomePage(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response)
        throws java.io.IOException
    {

        if (WebDebug.on)
        {
            WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET | WebDebug.C_APP_SESSION,
                    WebDebug.C_ST_RESPONSE,
                    C_CLASS_NAME, 11900, null,
                    "Redirect to home page");
        }

        p_response.sendRedirect("Session");
        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_handleErrorResponse = "handleErrorResponse";
    /**
     * Handler for the various type of <code>PpasServletException</code> used to generate failure
     * <code>ChsiResponse</code> objects.
     *  @param p_request   The Chsi request being processed.
     *  @param p_flags     General process flags (not used).
     *  @param p_exception The servlet exception defining an exception
     *                     generated as a result of an invalid parameter being
     *                     found in the http request.
     * @return A Chsi response.
     */
    protected ChsiResponse handleErrorResponse(ChsiRequest          p_request,
                                               long                 p_flags,
                                               PpasServletException p_exception)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request,
                C_CLASS_NAME, 12300, this,
                "Entered " + C_METHOD_handleErrorResponse);
        }

        ChsiResponse  l_response;
        
        if (p_exception instanceof PpasServletLicenceException)
        {
            l_response = handleInvalidLicence(p_request, p_flags, p_exception);
        }
        else
        {
            l_response = handleInvalidParam(p_request, p_flags, p_exception);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_request,
                C_CLASS_NAME, 12390, this,
                "Leaving " + C_METHOD_handleErrorResponse);
        }

        return l_response;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_handleInvalidLicence = "handleInvalidLicence";
    /** Handle a PpasServlet exception arising from an invalid parameter value
     *  being detected in an incoming http request.
     *  @param p_request   The Chsi request being processed.
     *  @param p_flags     General process flags (not used).
     *  @param p_exception The servlet exception defining an exception
     *                     generated as a result of an invalid feature licence.
     * @return A Chsi response.
     */
    private ChsiResponse handleInvalidLicence(ChsiRequest          p_request,
                                              long                 p_flags,
                                              PpasServletException p_exception)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request,
                C_CLASS_NAME, 12200, this,
                "Entered " + C_METHOD_handleInvalidLicence);
        }

        ChsiResponse l_chsiResponse = new ChsiResponse(
                         p_request,
                         p_request.getChsiSession().getSelectedLocale(),
                         ChsiResponse.C_KEY_SERVICE_NOT_CONFIGURED,
                         null,
                         ChsiResponse.C_SEVERITY_FAILURE);
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_request,
                C_CLASS_NAME, 12290, this,
                "Leaving " + C_METHOD_handleInvalidLicence);
        }

        return(l_chsiResponse);
        
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_handleInvalidParam = "handleInvalidParam";
    /** Handle a PpasServlet exception arising from an invalid parameter value
     *  being detected in an incoming http request.
     *  @param p_request   The Chsi request being processed.
     *  @param p_flags     General process flags (not used).
     *  @param p_exception The servlet exception defining an exception
     *                     generated as a result of an invalid parameter being
     *                     found in the chsi request.
     * @return A Chsi response.
     */
    private ChsiResponse handleInvalidParam(ChsiRequest          p_request,
                                            long                 p_flags,
                                            PpasServletException p_exception)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request,
                C_CLASS_NAME, 12100, this,
                "Entered " + C_METHOD_handleInvalidParam);
        }

        ChsiResponse l_chsiResponse;
        Object []    l_params;
        String       l_responseKey;
        String       l_exceptionKey = p_exception.getMsgKey();

        // If we have any of the following keys, then there is a one-to-one
        // correspondence between the parameters in the exception and the
        // parameter required in the Chsi response object to be generated.

        l_params = p_exception.getMsgParams();

        if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_MISSING_MANDATORY_PARAM))
        {
            l_responseKey = ChsiResponse.C_KEY_MISSING_MANDATORY_PARAM;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_NUMERIC_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_NUMERIC_VALUE;
        }
        
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_DECIMAL_NUMERIC_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_DECIMAL_NUMERIC_VALUE;
        }        
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_NEGATIVE_VAL))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_NEGATIVE_VAL;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_PLUS_SIGN))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_PLUS_SIGN;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_BOOLEAN_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_BOOLEAN_VALUE;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_DATE_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_DATE_VALUE;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_DATETIME_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_DATETIME_VALUE;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_PARAM_LENGTH))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_PARAM_LENGTH;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_INVALID_ENUM_VALUE))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_ENUM_VALUE;
        }
        else if (l_exceptionKey.equals(
                   PpasServletMsg.C_KEY_NON_BLANK_PARAM))
        {
            l_responseKey = ChsiResponse.C_KEY_NON_BLANK_PARAM;
        }
        else if (l_exceptionKey.equals(PpasServletMsg.C_KEY_FEATURE_LICENCE_INVALID_PARAM))
        {
            l_responseKey = ChsiResponse.C_KEY_INVALID_FEATURE_LICENCE_PARAM;
        }
        else
        {
            // We have unrecognised exception, so set up key for a general
            // exception.
            l_responseKey = ChsiResponse.C_KEY_SERVICE_FAILURE;
            l_params = new Object [] {};
        }

        // Create the response object from the key/parameters obtained
        l_chsiResponse = new ChsiResponse
                              (p_request,
                               p_request.getChsiSession().getSelectedLocale(),
                               l_responseKey,
                               l_params,
                               ChsiResponse.C_SEVERITY_FAILURE);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_request,
                C_CLASS_NAME, 12190, this,
                "Leaving " + C_METHOD_handleInvalidParam);
        }

        return(l_chsiResponse);
    }

    /** Constant used in calls to middleware. */
    private static final String C_METHOD_getCurrency = "getCurrency";
    
    /**
     * Gets the currency from the request.
     * @param p_request the <code>HttpServletRequest</code> containing the params
     * @param p_chsiRequest the <code>ChsiRequest</code>
     * @param p_msisdn the MSISDN from the request (used for logging)
     * @return a <code>PpasCurrency</code> object
     * @throws PpasServletException if the currency is invalid or missing
     */
    protected PpasCurrency getCurrency(HttpServletRequest p_request,
                                       ChsiRequest        p_chsiRequest,
                                       Msisdn             p_msisdn)
        throws PpasServletException
    {
        PpasServletException l_pSE;
        String               l_currencyAsString;
        PpasCurrency         l_transCurrency;

        // Get the transaction currency from the request.
        l_currencyAsString = getValidParam(p_chsiRequest,
                                           0,
                                           p_request,
                                           "currency",
                                           true, // parameter is mandatory ...
                                           "",   // so there is no default.
                                           PpasServlet.C_TYPE_ANY,
                                           3,
                                           PpasServlet.C_OPERATOR_EQUALS);
        
        l_transCurrency = i_chsiContext.getCurrency(l_currencyAsString);
        
        if (l_transCurrency == null)
        {
            l_pSE = new PpasServletException(C_CLASS_NAME,
                                             C_METHOD_getCurrency,
                                             50000,
                                             this,
                                             p_chsiRequest,
                                             0,
                                             ServletKey.get().invalidValueCurrency(
                                                                    p_msisdn, l_currencyAsString));
        
            i_logger.logMessage(l_pSE);
            throw l_pSE;
        }
        
        return l_transCurrency;
    } // end of method 'handleInvalidParam'

    /**
     * Creates an Invalid Version Response for the Chsi service.
     * @param p_request     The servlet request.
     * @param p_chsiRequest The Chsi request.
     * @return The URL to which the request should be forwarded.
     */
    protected String createInvalidVersionResponse(
        HttpServletRequest     p_request,
        ChsiRequest            p_chsiRequest)
    {
        ChsiResponse l_chsiResponse = new ChsiResponse
                       (p_chsiRequest,
                        p_chsiRequest.getChsiSession().getSelectedLocale(),
                        ChsiResponse.C_KEY_INVALID_VERSION,
                        new Object[] {p_chsiRequest.getChsiVersion()},
                        ChsiResponse.C_SEVERITY_FAILURE);

        p_request.setAttribute("p_chsiResponse", l_chsiResponse);

        return("/jsp/chsi/noservicedataxml.jsp");
    } // end of createInvalidVersionResponse

    /** Set the session as timeout out.
     * @param p_session Session that has timeout out.
     */
    public void setTimedOut(LogInOutInterface p_session)
    {
        (new PpasSessionService(null, i_logger, i_chsiContext)).timeout(p_session);
    }
}

