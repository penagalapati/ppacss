////////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiService.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Super class for ChsiXyzService classes
//
////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                        | REFERENCE
//-----------+---------------+------------------------------------+-----------------
//           |               |                                    |  
//           |               |                                    |
//////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.service;

import java.util.HashMap;

import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Super class for ChsiXyzService classes.
 */
public class ChsiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * Map to be used to convert permitted service exceptions into
     * corresponding CSR response keys.
     */
    protected HashMap i_exceptionConvMap;


    /** Context data for the Chsi application. */
    protected ChsiContext i_chsiContext; 

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates an instance of a PpasCommentService object that
     * can then be used to perform Comment services.
     *
     * @param p_request The request being processed.
     * @param p_flags  General process flags (not used).
     * @param p_logger The Logger to be used to log any exceptions that
     *                  occur while performing this service.
     * @param p_chsiContext Context information to be made available to this
     *                      service.
     */
    public ChsiService(ChsiRequest         p_request,
                       long                p_flags,
                       Logger              p_logger,
                       ChsiContext         p_chsiContext)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_chsiContext = p_chsiContext;
        initKeyMapping(p_request);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }


    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getGeneralFailureParams = "getGeneralFailureParams";
    /**
     * Sets up message parameters for general service failures.
     *
     * @param p_request The request being processed.
     * @param p_flags   General process flags (not used).
     *  @param  p_pSE  The PpasServiceException that is to be mapped to
     *                 a Chsi response.
     *  @return  Array containing parameters for error message
     */
    public Object[] getGeneralFailureParams(ChsiRequest          p_request,
                                            long                 p_flags,
                                            PpasServiceException p_pSE)
    {
        Object[]            l_params     = null;
        String              l_ppasMsgKey = p_pSE.getMsgKey();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request, C_CLASS_NAME, 19730, this,
                "Entered " + C_METHOD_getGeneralFailureParams);
        }

        if (
               // These mappings require the same parameters that
               // were used to create the PpasServiceException

                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_DATE_BEFORE_TODAYS_DATE) ||
                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_END_DATE_BEFORE_START_DATE) ||
                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_DATE_CANNOT_BE_BLANK) ||
                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_INVALID_FEATURE_PARAM) ||
                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_INVALID_FEATURE) ||
                l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_INVALID_FEATURE_LICENCE))
        {
            l_params = p_pSE.getMsgParams();
        }
        else if (l_ppasMsgKey.equals(PpasServiceMsg.C_KEY_INVALID_VALUE_CURRENCY))
        {
            l_params = new Object[] {p_request.getMsisdn(),
                                     (p_pSE.getMsgParams())[2]};
        }
        else
        {
            // No key mapping was found or one of the following keys
            // that require no parameters:-
            //
            // C_KEY_SERVICE_UNAVAILABLE
            // C_KEY_SERVICE_REQUEST_TIMEOUT
            // C_KEY_SERVICE_RESP_TIMEOUT
            
            l_params = new Object[] {};
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME, 14599, this,
                "Leaving " + C_METHOD_getGeneralFailureParams);
        }

        return(l_params);

    }  // end getGeneralFailureParams(...)

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_initKeyMapping = "initKeyMapping";

    /**
     * Initialises the HashMap used to map PpasServiceException keys
     * to Chsi message keys.
     * @param p_request The request being processed.
     */
    private void initKeyMapping (ChsiRequest p_request)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request, C_CLASS_NAME, 20000, this,
                "Entered " + C_METHOD_initKeyMapping);
        }

        // Create and initialise the map to be used to convert anticipated
        // service exceptions into corresponding CSR message keys
        i_exceptionConvMap = new HashMap();

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_GENERAL_FAILURE,
                               ChsiResponse.C_KEY_SERVICE_FAILURE);
        
        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_UNAVAILABLE,
                               ChsiResponse.C_KEY_SERVICE_UNAVAILABLE);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_RESPONSE_UNKNOWN,
                  ChsiResponse.C_KEY_SERVICE_RESPONSE_UNKNOWN);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_REQUEST_TIMEOUT,
                   ChsiResponse.C_KEY_SERVICE_REQUEST_TIMEOUT);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_UNAVAILABLE,
                  ChsiResponse.C_KEY_SERVICE_UNAVAILABLE);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_RESPONSE_TIMEOUT,
                           ChsiResponse.C_KEY_SERVICE_RESPONSE_TIMEOUT);       

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_REQUEST_TIMEOUT,
                               ChsiResponse.C_KEY_SERVICE_REQUEST_TIMEOUT);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_SERVICE_RESPONSE_TIMEOUT,
                               ChsiResponse.C_KEY_SERVICE_RESPONSE_TIMEOUT);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_DATE_BEFORE_TODAYS_DATE,
                               ChsiResponse.C_KEY_DATE_BEFORE_TODAYS_DATE);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_END_DATE_BEFORE_START_DATE,
                               ChsiResponse.C_KEY_END_DATE_BEFORE_START_DATE);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_DATE_CANNOT_BE_BLANK,
                               ChsiResponse.C_KEY_DATE_CANNOT_BE_BLANK);

       
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME, 20010, this,
                "Leaving " + C_METHOD_initKeyMapping);
        }

    }  // end private void initKeyMapping()
}
