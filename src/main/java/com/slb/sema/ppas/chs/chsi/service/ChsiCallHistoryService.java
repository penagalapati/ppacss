////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiCallHistoryService.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Wrappers the PpasCallHistory internal service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-------------+------------+---------------------------------+-----------------
//    dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.service;

import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.common.dataclass.CallHistoryDataSet;
import com.slb.sema.ppas.common.dataclass.CustMsisdnsDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.is.isapi.ChsCallHistoryRetrievalService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform Chsi Call History service.
 * The following services are provided:
 *
 * - Call History Service for a given subscriber
 */
public class ChsiCallHistoryService extends ChsiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiCallHistoryService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Call History reader service. */
    private ChsCallHistoryRetrievalService i_callHistoryService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a ChsiCallHistoryService object that
     * can then be used to perform Accumulator services.
     * @param p_request The request being processed.
     * @param p_logger  The logger to direct messages.
     * @param p_chsiContext General session information.
     */
    public ChsiCallHistoryService(ChsiRequest         p_request,
                                  Logger              p_logger,
                                  ChsiContext         p_chsiContext)
    {
        super(p_request, 0, p_logger, p_chsiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_request, C_CLASS_NAME, 20000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        // Create internal call history service to be used by this service.
        i_callHistoryService = new ChsCallHistoryRetrievalService(p_request, p_logger, p_chsiContext);

        i_exceptionConvMap.put(PpasServiceMsg. C_KEY_INVALID_SEARCH_LEVEL,
                               ChsiResponse.C_KEY_INVALID_SEARCH_LEVEL);
              
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_request, C_CLASS_NAME, 20010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCallHistory = "getCallHistory";
    /**
     * Calls isapi to retrieve the call history records.
     *
     * @param p_chsiRequest The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_custMsisdnsDataSet The msisdn dataset for call history records. 
     * @param p_maxRecordsRetrieve The max number of records to retrive.
     * @param p_otherPartyNumber The other party number.
     * @param p_sortField Sort by call date if "date", else sort by other party number using "otherPartyNo".
     * @param p_descendSort Use descending if true, else ascending.
     * @return ChsiCallHistoryResponse Contains the data returned from doing this service.
     */
    public ChsiCallHistoryResponse getCallHistoryDetails(ChsiRequest        p_chsiRequest,
                                                         long               p_timeoutMillis,
                                                         CustMsisdnsDataSet p_custMsisdnsDataSet,
                                                         int                p_maxRecordsRetrieve,
                                                         String             p_otherPartyNumber,
                                                         String             p_sortField,
                                                         boolean            p_descendSort)
    {
        ChsiCallHistoryResponse l_response;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_chsiRequest, C_CLASS_NAME, 20200, this, 
                           "Entered " + C_METHOD_getCallHistory);
        }
         
        try
        {
            CallHistoryDataSet l_callHistoryResultDataSet =
                                        i_callHistoryService.getCallHistoryDetails(p_chsiRequest,
                                                                                   p_timeoutMillis,
                                                                                   p_custMsisdnsDataSet,
                                                                                   p_maxRecordsRetrieve,
                                                                                   p_otherPartyNumber,
                                                                                   p_sortField,
                                                                                   p_descendSort);
            
            if (l_callHistoryResultDataSet.asArray().length == 0)
            {
                l_response = new ChsiCallHistoryResponse(p_chsiRequest,
                                                         p_chsiRequest.getChsiSession().getSelectedLocale(),
                                                         ChsiResponse.C_KEY_SERVICE_SUCCESS_NO_CALLS,
                                                         (Object[])null,
                                                         ChsiResponse.C_SEVERITY_SUCCESS,
                                                         l_callHistoryResultDataSet);
            }
            else
            {
                l_response = new ChsiCallHistoryResponse(p_chsiRequest,
                                                         p_chsiRequest.getChsiSession().getSelectedLocale(),
                                                         ChsiResponse.C_KEY_SERVICE_SUCCESS,
                                                         (Object[])null,
                                                         ChsiResponse.C_SEVERITY_SUCCESS,
                                                         l_callHistoryResultDataSet);
            }
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_chsiRequest, C_CLASS_NAME, 20220, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            // Attempt to get the ChsiGetCallHistoryResponse message key
            // from the mapping of service exception keys to response keys
            String l_chsiResponseKey = (String)i_exceptionConvMap.get(l_pSE.getMsgKey());
            Object []               l_params;

            if (l_chsiResponseKey == null)
            {
                // Key was not found in mapping, so return a general failure
                // response
                l_chsiResponseKey = ChsiResponse.C_KEY_SERVICE_FAILURE;
                l_params = new Object[] {};
            }
            else if (l_chsiResponseKey.equals(ChsiResponse.C_KEY_INVALID_SEARCH_LEVEL))
            {
                l_chsiResponseKey = ChsiResponse.C_KEY_INVALID_SEARCH_LEVEL;
                l_params = new Object[] {p_chsiRequest.getMsisdn()};
            }
            else
            {
                // Exception is a general exception that could be thrown
                // by any service, so use the following to get the
                // associated message parameters.
                
                l_params = getGeneralFailureParams(p_chsiRequest, 0, l_pSE);
            }

            // Create the response object from the key/parameters obtained
            l_response = new ChsiCallHistoryResponse(p_chsiRequest,
                                                     p_chsiRequest.getChsiSession().getSelectedLocale(),
                                                     l_chsiResponseKey,
                                                     l_params,
                                                     ChsiResponse.C_SEVERITY_FAILURE,
                                                     null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_chsiRequest, C_CLASS_NAME, 20290, this,
                           "Leaving " + C_METHOD_getCallHistory);
        }

        return l_response;
    }
}
