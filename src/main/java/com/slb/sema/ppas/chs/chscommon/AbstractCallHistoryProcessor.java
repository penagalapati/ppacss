////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AbstractCallHistoryProcessor.java
//      DATE            :       19-Mar-2006
//      AUTHOR          :       Lars Lundberg (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Abstract, generic base class for the call history
//                              process class.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY |            | <A brief description>           | PpacLon#<CR>/<Task>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chscommon;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcConnectionPool;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.net.BasicManagerCommandProcessor;
import com.slb.sema.ppas.util.net.BasicManagerControllable;
import com.slb.sema.ppas.util.net.BasicManagerServerEndPoint;

/**
 * This <code>AbstractCallHistoryProcessor</code> class is an abstract
 * generic base class for the call history process class.
 */
public abstract class AbstractCallHistoryProcessor
    implements InstrumentedObjectInterface, BasicManagerControllable
{
    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME            = "AbstractCallHistoryProcessor";

    /** The manager port number property name. */
    private static final String C_PROPERTY_MANAGER_PORT = "com.slb.sema.ppas.managerPortNumber";


    //==========================================================================
    // Protected instance attribute(s).
    //==========================================================================
    /** The Instrument Manager used to register any instruments with. */
    protected InstrumentManager          i_instrumentManager          = null;
    
    /** The Instrument Set for this instrumented object. */
    protected InstrumentSet              i_instrumentSet              = null;
    
    /** The <code>Logger</code> to log to. */
    protected Logger                     i_logger                     = null;

    /** The <code>PpasContext</code>. */
    protected PpasContext                i_ppasContext                = null;

    /** The configuration properties. */
    protected PpasProperties             i_ppasProperties             = null;

    /** The process name. */
    protected String                     i_processName                = null;

    /** Basic Manager Sever End Point which implements basic management such as stopping the Server. */   
    protected BasicManagerServerEndPoint i_basicManagerServerEndPoint = null;

    /** The manager port for the gateway server. */
    protected int                        i_managerPort                = 0;

    /** The <code>PpasRequest</code>. */
    protected PpasRequest                i_ppasRequest                = null;


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an instance of the <code>AbstractCallHistoryProcessor</code> class.
     * NB: Since this is an abstract class this Constructor can only be called from
     *     the Constructor of a sub-class.
     *
     * @throws PpasException  if it fails to construct an instance of this class,
     *                        for instance if a mandatory property is missing.
     */
    public AbstractCallHistoryProcessor() throws PpasException
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        init();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }


    //==========================================================================
    // Public method(s).
    //==========================================================================
    /**
     * Returns this instrumented object's instrument set.
     * @return This instrumented object's instrument set.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }


    /**
     * Returns this instrumented object's name.
     * @return This instrumented object's name.
     */
    public String getName()
    {
        return i_processName;
    }


    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_start = "start";
    /**
     * Starts the call history process.
     * It is declared <code>final</code> to assure that it can't be overridden by derived classes,
     * instead they should override the empty method <code>doStop</code>.
     */
    public final void start()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entered " + C_METHOD_start);
        }

        if (i_basicManagerServerEndPoint != null)
        {
            i_basicManagerServerEndPoint.start();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10022, this,
                            "Calling (usually overriden) method doStart");
        }

        doStart();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10024, this,
                            "Returned from (usually overriden) method doStart");
        }

        i_logger.logMessage(new LoggableEvent("Process startup succeeded for process " + i_processName,
                                              LoggableInterface.C_SEVERITY_SUCCESS));

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving " + C_METHOD_start);
        }
    }


    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_stop = "stop";
    /**
     * Stops the call history process.
     * It is declared <code>final</code> to assure that it can't be overridden by derived classes,
     * instead they should override the empty method <code>doStop</code>.
     */
    public final void stop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10040, this,
                            "Entering " + C_METHOD_stop);
        }

        // Request stop on all running objects.
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10042, this,
                            "Calling (usually overriden) method doStop");
        }

        doStop();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10044, this,
                            "Returned from (usually overriden) method doStop");
        }

        // Stop Basic Manager Server End Point
        try
        {
            if (i_basicManagerServerEndPoint != null)
            {
                i_basicManagerServerEndPoint.stop();
            }
        }
        catch (InterruptedException l_intEx)
        {
            // We tried! Ignore and carry on.
            l_intEx = null;
        }
        finally
        {
            i_basicManagerServerEndPoint = null;
        }

        // Destroy context.
        if (i_ppasContext != null)
        {
            i_ppasContext.destroy();
            i_ppasContext = null;
        }

        i_logger.logMessage(new LoggableEvent("Process stop completed for process " + i_processName,
                                              LoggableInterface.C_SEVERITY_SUCCESS));

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10050, this,
                            "Leaving " + C_METHOD_stop);
        }
    }


    //==========================================================================
    // Protected method(s).
    //==========================================================================
    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_doStart = "doStart";
    /** 
     * Hook for derived classes to perform start activities.
     * This default implementation does nothing.
     */
    protected void doStart()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                C_CLASS_NAME, 10060, this,
                C_METHOD_doStart + ": not overridden by derived class - no class " +
                        "specific processing required.");
        }
    }


    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_doStop = "doStop";
    /** 
     * Hook for derived classes to perform stop activities.
     * This default implementation does nothing.
     */
    protected void doStop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                C_CLASS_NAME, 10070, this,
                C_METHOD_doStop + ": not overridden by derived class - no class " +
                        "specific processing required.");
        }
    }


    //==========================================================================
    // Private method(s).
    //==========================================================================
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialises this process, including creating of a process context,
     * loading of configuration (properties), creation of Instrument Manager,
     * initalisation of the context etc.
     * 
     * @throws PpasException  if it fails to init. this process, for instance if
     *                        a mandatory property is missing.
     */
    private void init() throws PpasException
    {
        PpasLoggerPool      l_loggerPool          = null;
        PpasSession         l_session             = null;
        JdbcConnection      l_jdbcConnection      = null;
        PpasConfigException l_ppasConfigEx        = null;
        String              l_managerPortStr      = null;
        BusinessConfigCache l_businessConfigCache = null;
        JdbcConnectionPool  l_jdbcConnectionPool  = null;
        long                l_jdbcConnTimeout     = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10080, this,
                            "Entered " + C_METHOD_init);
        }

        // Get the name of this process.
        i_processName = System.getProperty("ascs.procName");

        // Create InstrumentManager to allow Std & Remote implementation.
        i_instrumentManager = new InstrumentManager();

        try
        {
            // Create PpasContext and initialise.
            if (i_ppasContext == null)
            {
                // Create & load layered properties files.
                i_ppasProperties = new PpasProperties(i_instrumentManager);
                i_ppasProperties.loadLayeredProperties();

                // Sets debug settings based on the supplied properties.
                PpasDebug.setDebug(PpasDebug.C_FLAG_SYSTEM_OVERRIDES, i_ppasProperties);

                // Sets the default TimeZone on the JVM to that configured in properties file.
                DatePatch.init(C_CLASS_NAME, C_METHOD_init, 10082, this, null, 0, null, i_ppasProperties);
    
                // Create Logger pool
                l_loggerPool = new PpasLoggerPool("LoggerPool", i_ppasProperties, i_processName);
    
                // Get Logger and register with Instrument Manager.
                i_logger = l_loggerPool.getLogger();
    
                if (i_logger == null)
                {
                    // The requested logger could not be found ... create and throw
                    // an appropriate exception
                    l_ppasConfigEx = new PpasConfigException(C_CLASS_NAME,
                                                             C_METHOD_init,
                                                             10084,
                                                             this,
                                                             null,
                                                             0L,
                                                             ConfigKey.get().badLogger("chsLogger",
                                                                                       "LoggerPool"));
    
                    // Can't log (logging not initialised yet), so write to System.err.
                    System.err.println("***FATAL ERROR: FAILED TO START THE CALL HISTORY RECORD PROCESSOR!");
                    l_ppasConfigEx.printStackTrace(System.err);
                    throw (l_ppasConfigEx);
                }
    
                i_logger.setInstrumentManager(i_instrumentManager);
    
                // Create context and set attributes on context
                i_ppasContext = new PpasContext(i_logger, i_instrumentManager);
                i_ppasContext.setAttribute("LoggerPool", l_loggerPool);
                i_ppasContext.init(null, 0, i_ppasProperties, this.getClass().getName());

                // Create a request
                l_session = new PpasSession(i_instrumentManager);
                i_ppasRequest = new PpasRequest(l_session);
                i_ppasRequest.setContext(i_ppasContext);

                // Load the Business Config cache.
                l_businessConfigCache =
                    (BusinessConfigCache) i_ppasContext.getAttribute("BusinessConfigCache");
                l_jdbcConnectionPool = (JdbcConnectionPool) i_ppasContext.getAttribute("JdbcConnectionPool");

                l_jdbcConnTimeout = ((Long)i_ppasContext.getAttribute(
                                   "com.slb.sema.ppas.support.PpasContext.connResponseTimeout")).longValue();
                l_jdbcConnection = l_jdbcConnectionPool.getConnection(C_CLASS_NAME,
                                                                      C_METHOD_init,
                                                                      10086,
                                                                      this,
                                                                      i_ppasRequest,
                                                                      0,
                                                                      l_jdbcConnTimeout);
    
                // Load the business config cache
                l_businessConfigCache.loadAll(i_ppasRequest, l_jdbcConnection);
            }

            // Initialises the instrument set.
            i_instrumentSet = new InstrumentSet("root");

            // Get the manager port number (to listen for manager commands on)
            l_managerPortStr = i_ppasProperties.getPortNumber(C_PROPERTY_MANAGER_PORT);
            i_managerPort    = Integer.parseInt(l_managerPortStr);

            // Construct and start the manager for the gateway server.
            i_basicManagerServerEndPoint =
                new BasicManagerServerEndPoint(i_logger,
                                               i_instrumentManager,
                                               i_managerPort,
                                               5,
                                               0,
                                               false,
                                               new BasicManagerCommandProcessor(this));
        }
        finally
        {
            if (l_jdbcConnection != null)
            {
                l_jdbcConnectionPool.putConnection(0, l_jdbcConnection);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_INIT | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10090, this,
                            "Leaving " + C_METHOD_init);
        }
    }
}
