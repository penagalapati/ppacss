////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiSessionService.java
//      DATE            :       09-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//      DESCRIPTION     :       Wrappers the PpasSessionService internal service
//                              returning a ChsiResponse object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.service;

import com.slb.sema.ppas.common.dataclass.LogInOutOrigin;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.LogInOutInterface;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.is.isapi.PpasSessionService;
import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.chs.chsi.support.ChsiRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform Chsi specific services associated with
 * the client session. The following services are provided:
 *
 * - Login using a the username and password provided.
 */
public class ChsiSessionService extends ChsiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiSessionService";
    
    /** Constant for KeepAlive. Value is {@value}. */
    public static final long C_FLAG_LOGOUT_KEEPALIVE = 0x1; 

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The atomic session service. */
    private PpasSessionService i_sessionService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a PpasSessionService object that
     * can then be used to perform session services.
     * @param p_request The request being processed.
     * @param p_logger  The logger to direct messages.
     * @param p_chsiContext General session information.
     */
    public ChsiSessionService(ChsiRequest         p_request,
                              Logger              p_logger,
                              ChsiContext         p_chsiContext)
    {
        super(p_request, 0, p_logger, p_chsiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing PpasSessionService(...)");
        }

        i_sessionService = new PpasSessionService(p_request, p_logger, p_chsiContext);

        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_LOGIN_FAILURE,
                               ChsiResponse.C_KEY_LOGIN_FAILURE);
        i_exceptionConvMap.put(PpasServiceMsg.C_KEY_INSUFFICIENT_PRIVILEGE,
                               ChsiResponse.C_KEY_INSUFFICIENT_PRIVILEGE);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed PpasSessionService(...)");
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_login = "login";
    /**
     * Logs the account in and generates a ChsiResponse message.
     *
     * @param p_request The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_origin   Where the request came from, for example, the originating IP address.
     * @param p_userName The client's username.
     * @param p_password The client's password.
     *
     * @return A ChsiResponse object.
     */
    public ChsiResponse login(ChsiRequest    p_request,
                              long           p_timeoutMillis,
                              String         p_origin,
                              String         p_userName,
                              String         p_password)
    {
        ChsiResponse l_response = null;
        String       l_chsiResponseKey = null;
        Object[]     l_params;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 11000, this,
                           "Entered " + C_METHOD_login);
        }

        long l_flags = 0;

        if (i_chsiContext.getProperties().getBooleanProperty(
                                          "com.slb.sema.ppas.is.isil.primitiveservice." +
                                          "SessionPrimitiveService.suppressNonInteractiveBarring"))
        {
            l_flags &= PpasService.C_FLAG_SUPPRESS_BARRING;
        }
        
        try
        {
            i_sessionService.login(p_request,
                                   l_flags,
                                   p_timeoutMillis,
                                   (LogInOutInterface)p_request.getSession(),
                                   new LogInOutOrigin(PpasSessionService.C_CHSI_USER,
                                                      i_chsiContext.getProperties().getProcessName(),
                                                      p_origin),
                                   p_userName,
                                   p_password);

            // Generate success ChsiResponse
            l_response = new ChsiResponse(p_request,
                                          p_request.getChsiSession()
                                              .getSelectedLocale(),
                                          ChsiResponse.C_KEY_SERVICE_SUCCESS,
                                          (Object[])null,
                                          ChsiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_request, C_CLASS_NAME, 11010, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            // Get the ChsiResponse message key.
            l_chsiResponseKey = (String)i_exceptionConvMap.get(l_pSE.getMsgKey());

            // If it's null use the general service failure message...
            if (l_chsiResponseKey == null)
            {
                l_chsiResponseKey = ChsiResponse.C_KEY_SERVICE_FAILURE;
                l_params = new Object [] {};
            }
            // ...otherwise construct an appropriate set of parameters.
            else
            {
                if (l_chsiResponseKey.equals(ChsiResponse.C_KEY_LOGIN_FAILURE))
                {
                    l_params = new Object [] {};
                }
                if (l_chsiResponseKey.equals(ChsiResponse.C_KEY_INSUFFICIENT_PRIVILEGE))
                {
                    l_params = new Object [] {};
                }
                else
                {
                    l_params = super.getGeneralFailureParams(p_request, 0, l_pSE);
                }
            }

            // Create the response object from the key/parameters obtained
            l_response = new ChsiResponse(p_request,
                                          p_request.getChsiSession()
                                              .getSelectedLocale(),
                                          l_chsiResponseKey,
                                          l_params,
                                          ChsiResponse.C_SEVERITY_FAILURE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11020, this,
                "Leaving " + C_METHOD_login);
        }

        return l_response;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_logout = "logout";
    /**
     * Logs the account out and generates a ChsiResponse message.
     * @param p_request The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_origin   Where the request came from, for example, the originating IP address.
     * @return A Chsi response.
     */
    public ChsiResponse logout(ChsiRequest p_request,
                               long        p_timeoutMillis,
                               String         p_origin)
    {
        ChsiResponse l_response = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request, C_CLASS_NAME, 12000, this,
                "Entered " + C_METHOD_logout);
        }

        // Currently the service works only on the session object held by
        // the ChsiRequest object. It does not access the database and cannot
        // throw any exceptions. As a consequence we always return a success
        // message.
        i_sessionService.logout(p_request,
                                p_timeoutMillis,
                                (LogInOutInterface)p_request.getSession(),
                                new LogInOutOrigin(PpasSessionService.C_CHSI_USER,
                                                   i_chsiContext.getProperties().getProcessName(),
                                                   p_origin));

        // Generate success ChsiResponse
        l_response = new ChsiResponse(p_request,
                                      p_request.getChsiSession()
                                          .getSelectedLocale(),
                                      ChsiResponse.C_KEY_SERVICE_SUCCESS,
                                      (Object[])null,
                                      ChsiResponse.C_SEVERITY_SUCCESS);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME, 12010, this,
                "Leaving " + C_METHOD_logout);
        }

        return l_response;
    }
    
    
    
    
    /**
     * Comment for <code>C_METHOD_keepAlive</code>
     */
    private static final String C_METHOD_keepAlive = "keepAlive";

    /**
     * @param p_request
     * @return ChsiResponse
     */
    public ChsiResponse keepAlive(ChsiRequest p_request)
    {
        ChsiResponse l_response = null;

        if (WebDebug.on) WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request, C_CLASS_NAME, 13000, this,
                "Entered " + C_METHOD_keepAlive);

        // Generate success ChsiResponse
        l_response = new ChsiResponse(p_request,
                                      p_request.getChsiSession()
                                      .getSelectedLocale(),
                                      ChsiResponse.C_KEY_SERVICE_SUCCESS,
                                      (Object[])null,
                                      ChsiResponse.C_SEVERITY_SUCCESS);

        if (WebDebug.on) WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME,13010, this,
                "Leaving " + C_METHOD_keepAlive);

        return l_response;

    }

}
