////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileDistributorUT.java
//      DATE            :       17-Mars-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit test class for the 'ChsFileDistributor' class.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.filedistributor;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasConfigException;

/**
 * This <code>ChsFileDistributorUT</code> class is a  JUnit test class
 * for the 'ChsFileDistributor' class.
 */
public class ChsFileDistributorUT extends CommonTestCaseTT
{
    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must
     * implement a constructor that takes a test case name as it's argument.
     *
     * @param p_title The testcase name.
     */
    public ChsFileDistributorUT(String p_title)
    {
        super(p_title);
    }


    //==========================================================================
    // Public instance test method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testFileDistributor = "testFileDistributor";
    /**
     * Test.
     */
    public void testFileDistributor()
    {
        ChsFileDistributor l_fileDistributor = null;
        
        super.beginOfTest(C_METHOD_testFileDistributor);

//        super.sayTime("Create a 'ChsFileDistributor' instance.");
//        try
//        {
//            l_fileDistributor = new ChsFileDistributor(super.c_logger,
//                                                       super.c_instManager,
//                                                       super.c_ppasContext,
//                                                       super.c_properties,
//                                                       "A1001",
//                                                       null);
//        }
//        catch (PpasConfigException l_ppasConfigEx)
//        {
//            super.sayTime("***ERROR: Failed to create a 'ChsFileDistributor' instance.");
//            super.failedTestException(l_ppasConfigEx);
//        }
//
//        super.sayTime("Start the file distributor threads.");
//        l_fileDistributor.start();
//        try
//        {
//            Thread.sleep(10000);
//        }
//        catch (InterruptedException l_Ex)
//        {
//        }

        super.endOfTest();
    }


    //==========================================================================
    // Public class level method(s).
    //==========================================================================
    /**
     * Test suite conforming to the JUnit framework allowing tests to be run
     * automatically.
     * 
     * @return a Test case suite.
     */
    public static Test suite()
    {
        return new TestSuite(ChsFileDistributorUT.class);
    }
}
