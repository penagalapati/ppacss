#
# Service Type Exceptions
#
SERVICE_SUCCESS=Service successfully completed.
SERVICE_SUCCESS_NO_CALLS=Service successfully completed but there is no call history data for the MSISDN.
SERVICE_UNAVAILABLE=Service currently unavailable. Please re-try later and if the problem persists, contact your system administrator.
SERVICE_RESPONSE_UNKNOWN=Service response was unknown.
SERVICE_FAILURE=Service was unsuccessful but no specific reason for failure can be given. Please contact your system administrator.
SERVICE_REQUEST_TIMEOUT=Service has timed out trying to perform the request, the request has not been performed.
SERVICE_RESPONSE_TIMEOUT=Service has timed out waiting on the response from an internal server, the state of the request is unknown as no response was received.
NO_SERVICE_SPECIFIED=You must specify the service you wish to use.
INTERNAL_CONFIG_ERROR=The service failed due to an internal data configuration error. Please contact your system administrator.
INTERNAL_DATA_ERROR=The service failed due to an internal data error. Please contact your system administrator.
INVALID_FEATURE_LICENCE=The requested feature (feature code {0}) is not licenced.
INVALID_FEATURE_LICENCE_PARAM=A parameter which activates extra licenced functionality in a normally unlicenced service was supplied, and no licence for this feature {0} is present.
#
# Date exceptions
#
DATE_BEFORE_TODAYS_DATE=The {0} Date entered, {1} , is before todays date {2}.
END_DATE_BEFORE_START_DATE=The end date of the period, {0}, is before the start date of the period {1}.
DATE_CANNOT_BE_BLANK=The {0} Date entered cannot be blank.
#
# Session Service messages
#
LOGIN_REQUIRED=You must log in with a valid username/password before you can use any prepaid service.
NO_LOGIN_REQUIRED=The login service is not turned on. There is no need to log in or log out.
ALREADY_LOGGED_IN=You are already logged in. There is no need to log in again.
NOT_LOGGED_IN=You are not logged in. There is no need to log out.
LOGIN_FAILURE=The username or password supplied was invalid. Please check your username and password and try again.
INSUFFICIENT_PRIVILEGE=You have insufficient privilege to perform this function.
#
# Invalid http input parameter messages
#
MISSING_MANDATORY_PARAM=The mandatory parameter {0} was missing from the URL.
INVALID_NUMERIC_VALUE=The URL parameter {0} is required to be numeric but has the value {1}.
INVALID_DECIMAL_NUMERIC_VALUE=The URL parameter {0} is required to be numeric with maximum one decimal point but has the value {1}. 
INVALID_PARAM_LENGTH=The URL parameter {0} is required to be {1} {2} characters. However, it has value {3} with length {4}.
INVALID_BOOLEAN_VALUE=The URL parameter {0} is required to be boolean (true or false) but has a value of {1}.
INVALID_DATE_VALUE=The URL parameter {0} is required to be a date in the format dd-mmm-yyyy, but had a value of {1}
INVALID_DATETIME_VALUE=The URL parameter {0} is required to be a date in the format dd-mmm-yyyy hh:mm:ss, but had a value of {1}
INVALID_ENUM_VALUE=The URL parameter {0} has value {1} which is not a valid value for this parameter.
NON_BLANK_PARAM=The parameter {0} cannot contain an empty string.
INVALID_NEGATIVE_VAL=The {0} parameter has a negative value, {1}, but only positive values are allowed for this parameter.
INVALID_PLUS_SIGN=The {0} parameter has value {1}, but a plus sign is not allowed as its first character. 
#
# Servlet initialisation errors
#
SYS_INIT_FAILURE=CHS Initialisation failed; contact your system administrator. Please note the following reason for failure and any further data in the CHS output file:\n{0}.
#
# Runtime errors
#
RUNTIME_FAILURE=An unknown runtime error has occurred; contact your system administrator. Please note the following reason for failure and any further data in the CHS output and log files:\n{0}.
#
# Unconfigured Service Response
#
SERVICE_NOT_CONFIGURED=The requested service has not been configured. Please contact your system administrator.

#
# Version exceptions
#
MESSAGE_VERSION_REQUIRED=A message version is required for this operation.
INVALID_VERSION=The message version {0} supplied is invalid.
#
# Call History
#
INVALID_SEARCH_LEVEL =The search level is invalid for MSISDN {0}.