////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiServletContextListener.Java
//      DATE            :       18 May 2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       CHSI servlet context listener.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.servlet;

import javax.servlet.ServletContext;

import com.slb.sema.ppas.chs.chsi.support.ChsiContext;
import com.slb.sema.ppas.common.web.servlet.PpasServletContextListener;
import com.slb.sema.ppas.is.isc.IsClient;

/**
 * CHSI servlet context listener. Servlet context listeners allow applications to listen for and act on start
 * up or shutdown of their context (i.e. start up or shut down of the servlet application).
 */
public class ChsiServletContextListener extends PpasServletContextListener
{

    /**
     * Performs any initialisation for the CHSI context on start up. Currently this is all done by the startup
     * servlet and not here.
     * @param p_servletContext The servlet context which is being initialised.
     */
    public void doContextInitialised(ServletContext p_servletContext)
    {
        // Currently do nothing. In future should move startup code here!

        return;
    }

    /**
     * Performs any tidy up for the CHSI context on shut down.
     * @param p_servletContext The servlet context which is being destroyed.
     */
    public void doContextDestroyed(ServletContext p_servletContext)
    {
        ChsiContext l_chsiContext = null;
        IsClient l_isClient = null;

        //
        // Do shutdown stuff here.
        //

        l_chsiContext = (ChsiContext)p_servletContext
                .getAttribute("com.slb.sema.ppas.chs.chsi.support.ChsiContext");

        // Protected again a null CHSI context (e.g. start up problems).
        if (l_chsiContext != null)
        {
            l_isClient = (IsClient)l_chsiContext.getAttribute("IsClient");
            if (l_isClient != null)
            {
                l_isClient.stop();
            }

            l_chsiContext.destroy();
        }

        return;
    }

} // End of abstract class ChsiServletContextListener

////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////