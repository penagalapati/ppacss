////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryKey.java
//      DATE            :       16-Mar-2006
//      AUTHOR          :       Lars Lundberg (h79023d)
//      REFERENCE       :       PRD_ASCS_DEV_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Maker of Call History exception Keys.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.exceptions;

import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.exceptions.ExceptionKeyMaker;

/**
 * Maker of Call History exception Keys.
 */
public class CallHistoryKey extends ExceptionKeyMaker
{
    //==========================================================================
    // Private class attribute(s).
    //==========================================================================
    /** Instance of the key maker. */
    private static CallHistoryKey i_keyMaker = new CallHistoryKey(); 

    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * No-arg Constructor.
     * Constructs an instance of the <code>CallHistoryKey</code> class.
     * Defined private to avoid unwanted instances.
     */
    private CallHistoryKey()
    {
        // Do nothing - but stop other classes creating an instance.
    }

    //==========================================================================
    // Public class (static) method(s).
    //==========================================================================
    /**
     * Returns an instance (the one and only) of this class.
     * 
     * @return an instance (the one and only) of this class.
     */
    public static CallHistoryKey get()
    {
        return i_keyMaker;
    }

    //==========================================================================
    // Public instance method(s).
    //==========================================================================
    /**
     * Returns the pool timeout <code>CallHistoryExceptionKey</code>.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey poolTimeout()
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_POOL_TIMEOUT, new Object[]{});
    }

    /**
     * Returns the no pool entries <code>CallHistoryExceptionKey</code>.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey noPoolEntries()
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_NO_POOL_ENTRIES, new Object[]{});
    }

    /**
     * Returns the SQL*Loader error <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_cdrFile        the name of the CDR file currently loaded.
     * @param p_stdErrorLines  the Standard Error command output lines.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey sqlLoaderError(String p_cdrFile, String p_stdErrorLines)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_SQL_LOADER_ERROR,
                                           new Object[]{p_cdrFile, p_stdErrorLines});
    }

    /**
     * Returns the CDR file not found <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_cdrFile  the name of the CDR file that couldn't be found.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey cdrFileNotFound(String p_cdrFile)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_CDR_FILE_NOT_FOUND, new Object[]{p_cdrFile});
    }


    /**
     * Returns the CDR file rename failed <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_cdrFile      the name of the CDR file that couldn't be renamed.
     * @param p_cdrStateStr  a CDR file state string, for instance "in progress".
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey cdrFileRenameFailed(String p_cdrFile, String p_cdrStateStr)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_CDR_FILE_RENAME_FAILED,
                                           new Object[]{p_cdrFile, p_cdrStateStr});
    }

    /**
     * Returns the root directory not found <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_rootDir  the name of the root directory that couldn't be found.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey rootDirNotFound(String p_rootDir)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_ROOT_DIR_NOT_FOUND, new Object[]{p_rootDir});
    }

    /**
     * Returns the ignored platform Exception <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_exceptionKey  the exception key for the ignored platform Exception.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey ignoredPlatformException(String p_exceptionKey)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_IGNORED_PLATFORM_EXCEPTION,
                                           new Object[]{p_exceptionKey});
    }

    /**
     * Returns the duplicate CDR file <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_cdrFile  the name of the duplicate CDR file.
     * @param p_dupDir   the configured directory for duplicate CDR files.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey duplicateCDRFile(String p_cdrFile, String p_dupDir)
    {
        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_DUPLICATE_CDR_FILE,
                                           new Object[]{p_cdrFile, p_dupDir});
    }

    /**
     * Returns the 'too many failed records' <code>CallHistoryExceptionKey</code>.
     * 
     * @param p_noOfFailedRecs     the number of failed records.
     * @param p_noOfProcessedRecs  the number of processed records.
     * @param p_cdrFile            the name of the processed CDR file.
     * 
     * @return  the <code>CallHistoryExceptionKey</code> representing this exception.
     */
    public CallHistoryExceptionKey tooManyFailedRecords(int    p_noOfFailedRecs,
                                                        int    p_noOfProcessedRecs,
                                                        String p_cdrFile)
    {
        Integer l_noOfFailedRecsInt = new Integer(p_noOfFailedRecs);
        Integer l_noOfProcRecsInt   = new Integer(p_noOfProcessedRecs);

        return new CallHistoryExceptionKey(CallHistoryMsg.C_KEY_TOO_MANY_FAILED_RECORDS,
                                           new Object[]{l_noOfFailedRecsInt, l_noOfProcRecsInt, p_cdrFile});
    }

    //==========================================================================
    // Inner class(es).
    //==========================================================================
    /**
     * Inner class representing a key of a Call History Exception.
     */ 
    public class CallHistoryExceptionKey extends ExceptionKey
    {
        //=====================================================================
        // Constructor(s).
        //=====================================================================
        /**
         * Constructs an instance of the <code>CallHistoryExceptionKey</code> class
         * using the passed parameters.
         * 
         * @param p_exceptionKey The key of this exception.
         * @param p_parameters   The parameters for this instance of the exception.
         */
        private CallHistoryExceptionKey(String p_exceptionKey, Object[] p_parameters)
        {
            super(p_exceptionKey, p_parameters);
        }
    }
}
