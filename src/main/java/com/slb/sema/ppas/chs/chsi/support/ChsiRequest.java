////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       ChsiRequest.java
//  DATE            :       09-May-2006
//  AUTHOR          :       Kanta Goswami
//  REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       Contains information for a single HTTP
//                          request.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+--------------------
//           |            |                                 | 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chsi.support;

import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains context information for a single HTTP request. */
public class ChsiRequest extends PpasRequest
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** Holds the URL from the client HTTP request. */
    private String              i_url        = "";

    /** Holds the value of the command parameter from the client HTTP request. */
    private String              i_command    = "";

    /** The Chsi version of this request. */
    private String              i_chsiVersion;
    
    /** The Chsi subscriberList of this request. */
    private String              i_chsiSubsList;
    
    /** Time to live. This is used to handle timeouts. */
    private Long i_timeToLive = null;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChsiRequest";

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /**
     * Calls the super class constructor passing p_session. This request constructor does not include the
     * application context and should only be used where the context has not yet been established e.g. the
     * request is an initialisation request from the application itself.
     * @param p_session Session information.
     */
    public ChsiRequest(ChsiSession p_session)
    {
        super(p_session);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SESSION, PpasDebug.C_ST_CONFIN_START, null, // Can't
                                                                                                              // pass
                                                                                                              // half
                                                                                                              // constructed
                                                                                                              // ChsiRequest
                            C_CLASS_NAME, 10000, this, "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SESSION, PpasDebug.C_ST_CONFIN_END, null, // Can't
                                                                                                            // pass
                                                                                                            // half
                                                                                                            // constructed
                                                                                                            // ChsiRequest
                            C_CLASS_NAME, 10010, this, "Constructed " + C_CLASS_NAME);
        }
    }

    /**
     * Constructor with a specified customer.
     * @param p_session Session information.
     * @param p_custId Customer to use in the request.
     */
    public ChsiRequest(ChsiSession p_session, String p_custId)
    {
        super(p_session, "", p_custId);
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /**
     * Stores the URL for the request.
     * @param p_url URLof the request.
     */
    public void setUrl(String p_url)
    {
        if (p_url != null)
        {
            i_url = p_url;
        }
    }

    /**
     * Returns the URL to the calling object.
     * @return URL of the request.
     */
    public String getUrl()
    {
        return i_url;
    }

    /**
     * Stores the command for the request.
     * @param p_command Command for the request.
     */
    public void setCommand(String p_command)
    {
        if (p_command != null)
        {
            i_command = p_command;
        }
    }

    /**
     * Returns the command to the calling object.
     * @return Command associated with this request.
     */
    public String getCommand()
    {
        return i_command;
    }

    // Ppacs#1/18 - [Start]
    /**
     * Returns the PpasSession to the calling object.
     * @return The ASCS session.
     */
    public ChsiSession getChsiSession()
    {
        return (ChsiSession)super.getSession();
    }

    // Ppacs#1/18 - [end]
    /**
     * Allows to set the msisdn.
     * @param p_msisdn The msisdn.
     */
    public void setMsisdn(Msisdn p_msisdn)
    {
        super.setMsisdn(p_msisdn);
    }

    /**
     * Allows to set the operator Id.
     * @param p_opId The operator Id.
     */
    public void setOpid(String p_opId)
    {
        super.setOpid(p_opId);
    }

    /**
     * Get the Chsi version of this request.
     * @return the Chsi version of this request
     */
    public String getChsiVersion()
    {
        return i_chsiVersion == null ? ((ChsiSession)getSession()).getChsiVersion() : i_chsiVersion;
    }

    /**
     * Set the Chsi version of this request.
     * @param p_string the Chsi version of this request
     */
    public void setChsiVersion(String p_string)
    {
        i_chsiVersion = p_string;
    }
    
    /** Get Safe Time To Live.
     * @return Number of milli-seconds this request should live.
     */
    public long getSafeTimeToLive()
    {
        long l_ret = 0;
        if (i_timeToLive == null)
        {
            l_ret = -1;
        }
        else
        {
            l_ret = i_timeToLive.longValue();
        }
        return l_ret;
    }
    /**
     * Get the Chsi subscriber List of this request.
     * @return the Chsi subscriber List of this request
     */
    public String getChsiSubsList()
    {
        return i_chsiSubsList;
    }
    
    /**
     * Set the Chsi subscriber List of this request.
     * @param p_chsiSubsList the Chsi subscriber List of this request
     */
    public void setChsiSubsList(String p_chsiSubsList)
    {
        i_chsiSubsList = p_chsiSubsList;
    }
}