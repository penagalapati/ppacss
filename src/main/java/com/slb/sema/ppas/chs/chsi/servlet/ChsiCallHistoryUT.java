////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsiCallHistoryUT.java
//      DATE            :       11-May-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PRD_ASCS00_DEV_SS_101
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Test the CHSI Call History classes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chsi.servlet;

import java.text.ParseException;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.chs.chsi.localisation.ChsiResponse;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.CallHistoryTestDataTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;

/**
 * Test the CHSI Call History classes.
 * @see com.slb.sema.ppas.chs.chsi.servlet.ChsiCallHistoryServlet
 * @see com.slb.sema.ppas.chs.chsi.service.ChsiCallHistoryService
 */
public class ChsiCallHistoryUT extends ChsiTestTT 
{
    /** CHSI Command to be tested. Value is {@value}. */
    private static final String C_CMD = "chs/chsi/CallHistory?command=";
    
    /** The MSISDN for the master account. */
    private static Msisdn c_master = null;
    
    /** The MSISDN for the second account. */
    private static Msisdn c_second = null;

    /** String representation of the master account MSISDN. */
    private static String c_masterAsString = null;
    
    /** String representation of the second account MSISDN. */
    private static String c_secondAsString = null;

    /** Date/Time the account is installed. */
    private static PpasDate c_start;
    
    /** Date/Time the account is installed. */
    private static PpasDate c_end;
    
    /** Tets tool for generating data. */
    private static final CallHistoryTestDataTT C_DATA_GEN = new CallHistoryTestDataTT();
    
    /** String representation of the subordinate account MSISDN. */
    private static String[] c_subordinateAsString = new String[3];
    
    /** The MSISDN for the subordinate account. */
    private static Msisdn c_subordinate = null;
    
    /** String representation of the Other Party Number. */
    private static String c_otherPartyNumber1 = "123";
    
    /** MSISDN formatter for Call History. */
    private static final MsisdnFormat C_CALL_HISTORY_FORMAT = new MsisdnFormat("44");
    
    /** MSISDN formatter for Call History. */
    MsisdnFormat i_msisdnFormat = new MsisdnFormat("cn", "44");
    
    /** 
     * Standard constructor.
     * @param p_name Name of test.
     */
    public ChsiCallHistoryUT(String p_name)
    {
        super(p_name);
    }
    
    /** Actions to be performed before each test. */
    public void setUp()
    {    
        loginV3();
        
        if (c_master == null)
        {
            // Don't care whether the MSISDN is actually installed on ASCS - this is CHS.

            c_master = getNextMsisdn();
            
            c_masterAsString = i_msisdnFormat.format(c_master);
 
            //Load the testdata for the Master.//Load the testdata for the Master.
            loadMasterCallHistoryTestData(c_master);
            
            //Load the testdata for the Subordinate.
            //loadSubordinatesCallHistoryTestData(c_masterAsString);
        }
    }

    /** Get the next MSISDN.
     * 
     * @return next available MSISDN.
     */
    private Msisdn getNextMsisdn()
    {
        Msisdn l_msisdn = null;
        
        String l_string = "" + System.currentTimeMillis()/1000;
        
        try
        {
            l_msisdn = C_CALL_HISTORY_FORMAT.parse(l_string);
        }
        catch (ParseException e)
        {
            failedTestException(e);
        }
        
        return l_msisdn;
    }
    
    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using all parameters - limit 3.
     * @ut.then A 
     * successful response is returned.
     * @ut.attributes +f
     */
    public void testGetAllParametersMasterLimit3()
    {
        beginOfTest("testGetAllParametersMasterLimit3");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&maxRecords=3" + "&otherPartyNumber=" + c_otherPartyNumber1 + 
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 3);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using all parameters - limit 100.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetAllParametersMasterLimit100()
    {
        beginOfTest("testGetAllParametersMasterLimit100");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&maxRecords=100" + "&otherPartyNumber=" + c_otherPartyNumber1 + 
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 96);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using all parameters - limit 50.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetAllParametersMasterLimit50()
    {
        beginOfTest("testGetAllParametersMasterLimit100");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&maxRecords=50" + "&otherPartyNumber=" + c_otherPartyNumber1 + 
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 50);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using date/other - no limit.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetDateOtherMasterNoLimit()
    {
        beginOfTest("testGetDateOtherMasterNoLimit");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&otherPartyNumber=" + c_otherPartyNumber1 + 
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 96);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using date/other where other does
     * not exist - no limit.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetDateOtherNotExistMasterNoLimit()
    {
        beginOfTest("testGetDateOtherNotExistMasterNoLimit");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&otherPartyNumber=1239876156235" + 
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS_NO_CALLS);
        
        checkFieldCount("ChiCallHistoryItem", 0);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using date (no other) - no limit.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetDateNoOtherMasterNoLimit()
    {
        beginOfTest("testGetDateNoOtherMasterNoLimit");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end +
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 100);  // Uses configured limit (100)
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using date (empty other) - no limit.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetDateEmptyOtherMasterNoLimit()
    {
        beginOfTest("testGetDateEmptyOtherMasterNoLimit");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=" + c_end + "&otherPartyNumber=" +
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 100);   // 280 records but server limited
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using invalid start date.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetInvalidStartDate()
    {
        beginOfTest("testGetInvalidStartDate");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=31-Feb-2006" +
                    "&endDate=" + c_end +
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "FAILURE", ChsiResponse.C_KEY_INVALID_DATE_VALUE);
        
        checkFieldCount("ChiCallHistoryItem", 0);
    }

    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using invalid end date.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetInvalidEndDate()
    {
        beginOfTest("testGetInvalidEndDate");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" +
                    "&startDate=" + c_start +
                    "&endDate=31-Feb-2006" +
                    "&sortField=DATE&sortOrder=DSC&level=SUB&version=1",
                    "FAILURE", ChsiResponse.C_KEY_INVALID_DATE_VALUE);
        
        checkFieldCount("ChiCallHistoryItem", 0);
    }
    
    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using msisdn where the start/end date
     * times exist.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetCallsDateTimeOtherParamsNotSpecified()
    {
        beginOfTest("testGetCallsDateTimeNotSpecified");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59" ,
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 100);
    }
    
    /**
     * @ut.when A call history GET_CLLAS command for CHSI version 1 using 2 msisdns where the start/end date
     * times exist for the msisdns.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetCalls2MsisdnOtherParamsNotSpecified()
    {
        beginOfTest("testGetCalls2MsisdnOtherParamsNotSpecified");

        if (c_second == null)
        {
            // Don't care whether the MSISDN is actually installed on ASCS - this is CHS.

            c_second = getNextMsisdn();
            
            c_secondAsString = i_msisdnFormat.format(c_second);

            //Load the testdata for the second msisdn.
            loadMasterCallHistoryTestData(c_second);
        }
        
        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59," +
                    c_secondAsString + "," +
                    c_start + " 00:00:00," +
                    c_end + " 23:59:59&maxRecords=100" ,
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 100);
    }
    
    /**
     * @ut.when A call history GET_CLLAS command for CHSI version 1 using 2 msisdns where the start/end date
     * times not exist for the first msisdn.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetCalls2MsisdnDateOtherParamsNotSpecified()
    {
        beginOfTest("testGetCalls2MsisdnDateOtherParamsNotSpecified");

        if (c_second == null)
        {
            // Don't care whether the MSISDN is actually installed on ASCS - this is CHS.

            c_second = getNextMsisdn();
            
            c_secondAsString = i_msisdnFormat.format(c_second);

            //Load the testdata for the second msisdn.
            loadMasterCallHistoryTestData(c_second);
        }
        
        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + "," +
                    "," +
                    "," +
                    c_secondAsString + "," +
                    c_start + " 00:00:00," +
                    "&otherPartyNumber="+ c_otherPartyNumber1,
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
        
        checkFieldCount("ChiCallHistoryItem", 100);
    }
    /**
     * @ut.when A call history GET_HISTORY command for CHSI version 1 using msisdn where the start/end date
     * times not exist.
     * @ut.then A successful response is returned.
     * @ut.attributes +f
     */
    public void testGetCallsDateTimeNotSpecified()
    {
        beginOfTest("testGetCallsDateTimeNotSpecified");

        sendRequest(C_CMD + "GET_CALLS&transId=000023&requestOpid=POSIUSER" +
                    "&subscriberList=" + c_masterAsString + ",," +
                    "&maxRecords=20" ,
                    "SUCCESS", ChsiResponse.C_KEY_SERVICE_SUCCESS);
    }
    /** 
     * Convenience method to add call history minimal testdata for a Master MSISDN.
     * @param p_msisdn The msisdn for which records must be inserted 
     */
    private void loadMasterCallHistoryTestData(Msisdn p_msisdn)
    {
        JdbcConnection l_conn = null;
        
        try 
        {
            c_start = getToday();
            PpasDateTime l_now  = new PpasDateTime(getToday() + " 11:00:00");
            l_now.add(PpasDate.C_FIELD_DATE, 1);  // Set to next day - so no problems with time
            
            DatePatch.setDateTime(getNow());
            
            l_conn = getConnection("CallHistoryTestDataTT", "loadMasterCallHistoryTestData", 10100);
            
            //  Load the testdata for the Master.
            for ( int j = 1 ; j < 8; j++ )
            {
                l_now.add(PpasDate.C_FIELD_DATE, j);
                C_DATA_GEN.createCallHistory(l_conn, p_msisdn, l_now, j*10);
            }
            
            DatePatch.setDateTime(l_now);
            setAllDatePatch(getNow());
            c_end = getToday();
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        finally
        {
            putConnection(l_conn);
        }
    }
    
    /** 
     * Convenience method to add call history minimal testdata for a Subordinate MSISDN.
     * @param p_masterAsString Master mobile number.
     */
    private void loadSubordinatesCallHistoryTestData(String p_masterAsString)
    {
        JdbcConnection l_conn = null;
        CallHistoryTestDataTT l_callHistory = null;
        PpasDate     l_start = getToday();
        PpasDateTime l_startTimeSubordinate = new PpasDateTime(l_start.toString() + " 11:30:00");
        Msisdn[]     l_subordinate = new Msisdn[3];
        
        try
        {
            l_callHistory = new CallHistoryTestDataTT();
            l_conn = getConnection("CallHistoryTestDataTT", "loadSubordinatesCallHistoryTestData", 10100);
            
            for ( int l = 0; l < l_subordinate.length; l++)
            {
                say("into Subordinates installing...");
                try
                {
                    l_subordinate[l] = C_CALL_HISTORY_FORMAT.parse(p_masterAsString);
                }
                catch (ParseException e1)
                {
                    failedTestException(e1);
                }
                c_subordinateAsString[l] = c_ppasContext.getMsisdnFormatInput().format(l_subordinate[l]);
                
                
                //  Load the testdata for the Subordinate.
                
                for (int j = 1; j < 8; j++ )
                {
                    say("into Subordinates loading...");
                    l_startTimeSubordinate.add(PpasDate.C_FIELD_DATE, j);
                    l_callHistory.createCallHistory(l_conn, l_subordinate[l], l_startTimeSubordinate, j*10);
                }
            }
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        
        finally 
        {
            putConnection(l_conn);
        }
    }

    /** 
     * Command line interface.
     * @param p_args Not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** 
     * Static method that allows the framework to to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. */
    public static Test suite()
    {
        return new TestSuite(ChsiCallHistoryUT.class);
    }
}
