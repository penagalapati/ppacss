////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChsFileProcessEvent.java
//      DATE            :       23-May-2006
//      AUTHOR          :       Lars L. (h79023d)
//      REFERENCE       :       PRD_ASCS00_SSD_SS_100
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A CDR file process event.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.chs.chscommon;


/**
 * This <code>ChsFileProcessEvent.java</code> class is used to describe a
 * Call History CDR file processin event.
 * An event has a type identifying what event has happened,
 * and a source identifying where/who generated the event.
 */
public class ChsFileProcessEvent
{
    //==========================================================================
    // Public class level constant(s).
    //==========================================================================
    /** The 'process completed' event code. */
    public static final int C_EVENT_TYPE_PROCESS_COMPLETED = 1;
    
    /** The 'fatal error' event code. */
    public static final int C_EVENT_TYPE_FATAL_ERROR       = 2;
    

    //==========================================================================
    // Private instance attributes(s).
    //==========================================================================
    /** The event type. */
    private int    i_type   = -1;
    
    /** The source from which the event is raised. */
    private Object i_source = null;


    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs an instance of the <code>ChsFileProcessEvent.java</code> class
     * from the given parameters.
     * 
     * @param p_type    The event type.
     * @param p_source  The source from which the event is raised.
     */
    public ChsFileProcessEvent(int p_type, Object p_source)
    {
        super();
        i_type   = p_type;
        i_source = p_source;
    }


    //==========================================================================
    // Public method(s).
    //==========================================================================
    /**
     * Returns the source object from which the event is raised.
     * 
     * @return the source object from which the event is raised.
     */
    public Object getSource()
    {
        return i_source;
    }


    /**
     * Returns the event type.
     * 
     * @return the event type.
     */
    public int getType()
    {
        return i_type;
    }
}
