////////////////////////////////////////////////////////////////////////////////////
//
//     FILE NAME :     SqlLoaderLog.java
//     DATE      :     16-March-2006
//     AUTHOR    :     Remi Isaacs
//     REFERENCE :     PRD_ASCS00_DEV_SS_100
//
//     COPYRIGHT :     WM-data 2006
//
//     DESCRIPTION : Data Object representing an SQL Loader log file.
//
////////////////////////////////////////////////////////////////////////////////////
//     CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////
//     DATE     | NAME          | DESCRIPTION                      | REFERENCE
//--------------+---------------+----------------------------------+----------------
//     DD/MM/YY | <name>        | <brief description of problem>   | <reference>
//              | < change>     |                                  |
//--------------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.chs.chscommon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.util.support.UtilDateFormat;

/** This data class contains the attributes defining the SQL loader log file. */
public class SqlLoaderLog
{
    //==========================================================================
    // Public class level constant(s).
    //==========================================================================
    /** The SQL *Loader 'CDR file successfully loaded' status. Value is {@value}. */
    public static final int C_STATUS_SUCCESS         = 0;

    /** The SQL *Loader 'CDR file loaded, but some records failed to load' status. Value is {@value}. */
    public static final int C_STATUS_WARNING         = 1;

    /** The SQL *Loader 'failed to load CDR file' status. Value is {@value}. */
    public static final int C_STATUS_FAILURE         = 2;

    /** The SQL *Loader 'failed to load CDR file and subsequent loads are also likely to fail' status.
     *  Value is {@value}. */
    public static final int C_STATUS_SERIOUS_FAILURE = 3;

    //==========================================================================
    // Private class level constant(s).
    //==========================================================================
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String  C_CLASS_NAME = "SqlLoaderLog";

    /** The 'total logical records read' <code>Pattern</code> object.. */
    private static final Pattern C_PATTERN_TOT_RECORDS_READ           = 
        Pattern.compile("Total logical records read:\\s*(\\d+)");

    /** The 'total logical records rejected' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_TOT_RECORDS_REJECTED       = 
        Pattern.compile("Total logical records rejected:\\s*(\\d+)");

    /** The 'total logical records discarded' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_TOT_RECORDS_DISCARDED      = 
        Pattern.compile("Total logical records discarded:\\s*(\\d+)");

    /** The 'successfully loaded' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_SUCCESSFULLY_LOADED        = 
        Pattern.compile("(\\d+) Rows? successfully loaded\\..*");

    /** The 'rows not loaded due to data errors' regular expression string. Value is {@value}. */
    private static final String  C_REG_EXP_NOT_LOADED_DATA_ERRORS     =
        "(\\d+) Rows? not loaded due to data errors\\..*";

    /** The 'rows not loaded due to data errors' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_NOT_LOADED_DATA_ERRORS     = Pattern.compile(C_REG_EXP_NOT_LOADED_DATA_ERRORS);

    /** The 'rows not loaded because all fields were null' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_NOT_LOADED_ALL_FIELDS_NULL = 
        Pattern.compile("(\\d+) Rows? not loaded because all fields were null\\..*");

    /** The 'elapsed time' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_ELAPSED_TIME               = 
        Pattern.compile("Elapsed time was:\\s*(\\d{2}:\\d{2}:\\d{2}\\.\\d{2})\\s*");

    /** The 'CPU time' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_CPU_TIME                   =
        Pattern.compile("CPU time was:\\s*(\\d{2}:\\d{2}:\\d{2}\\.\\d{2})\\s*");

    /** The 'start time' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_START_TIME                 =
        Pattern.compile("Run began on (\\w{3}) (\\w{3}) (\\d{1,2}) (\\d{2}:\\d{2}:\\d{2}) (\\d{4})\\s*");

    /** The 'end time' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_END_TIME                   =
        Pattern.compile("Run ended on (\\w{3}) (\\w{3}) (\\d{1,2}) (\\d{2}:\\d{2}:\\d{2}) (\\d{4})\\s*");

    /** The 'record error' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_RECORD_ERROR               = Pattern.compile("Record (\\d+): (.*)");

    /** The 'Oracle error' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_ORACLE_ERROR               = Pattern.compile("^(ORA-\\d{1,5}): (.*)");

    /** The 'SQL*Loader error' <code>Pattern</code> object. */
    private static final Pattern C_PATTERN_SQL_LOADER_ERROR           = Pattern.compile("^(SQL\\*Loader-(\\d+)): (.*)");
   
    //==========================================================================
    // Private instance attribute(s).
    //==========================================================================
    /** The CDR file loading status. */
    private int          i_cdrFileLoadStatus             = C_STATUS_SUCCESS;

    /** The name of SQL *Loader log file (the full path is assumed). */
    private String       i_sqlLoaderLogFilename          = null;

    /** The number of rows successfully loaded. */
    private int          i_numOfRowsSuccessfullyLoaded   = 0;

    /** The number of rows not loaded due to data error. */
    private int          i_numRowsNotLoadedDataError     = 0;

    /** The number of rows not loaded because all fields are null. */
    private int          i_numRowsNotLoadedAllFieldsNull = 0;

    /** The total number of logical records read. */
    private int          i_totalLogicalRecordsRead       = 0;

    /** The total number of logical records rejected. */
    private int          i_totalLogicalRecordsRejected   = 0;

    /** The total number of logical records discarded. */
    private int          i_totalLogicalRecordsDiscarded  = 0;

    /** The CDR file loading start time. */
    private PpasDateTime i_startTime                     = null;

    /** The CDR file loading start time. */
    private PpasDateTime i_endTime                       = null;

    /** The elapsed time. */
    private String       i_elapsedTime                   = null;

    /** The CPU Time. */
    private String       i_cpuTime                       = null;

    /** The number of erroneous records counter. */
    private int          i_recordErrorCnt                = 0;

    /** The number of Oracle error counter. */
    private int          i_oracleErrorCnt                = 0;

    /** Set of strings representing DBMS data errors. */
    private HashSet      i_dbmsDataErrors;
    
    /** Exception that caused this file to fail. Could be null even if a failure. This will only be set
     * if there is a problem reading a file to check results.
     */
    private Exception    i_originatingException;

    //==========================================================================
    // Constructor(s).
    //==========================================================================
    /**
     * Constructs a <code>SqlLoaderLog</code> instance using the passed parameters.
     * 
     * @param p_sqlLoaderLogFilename the name of SQL *Loader log file (the full path is assumed).
     * @param p_dbmsDataErrors       set of DBMS data error strings. If a DBMS error is found and it matches
     *                               one of these then it is considered to be a data error. This can be an
     *                               empty set.
     */
    public SqlLoaderLog(String p_sqlLoaderLogFilename, HashSet p_dbmsDataErrors)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_sqlLoaderLogFilename = p_sqlLoaderLogFilename;
        i_dbmsDataErrors       = p_dbmsDataErrors;
        
        parseLogFile(i_sqlLoaderLogFilename);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //==========================================================================
    // Public method(s).
    //==========================================================================
    /**
     * Returns the CDR file load status.
     * 
     * @return the CDR file load status.
     */
    public int getStatus()
    {
        return i_cdrFileLoadStatus;
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_setStatus = "setStatus";
    /**
     * Sets the CDR file load status.
     * 
     * @param p_cdrFileLoadStatus Status of the load.
     */
    public void setStatus(int p_cdrFileLoadStatus)
    {
        switch (p_cdrFileLoadStatus)
        {
            case C_STATUS_SUCCESS:
            case C_STATUS_WARNING:
            case C_STATUS_FAILURE:
            case C_STATUS_SERIOUS_FAILURE:
                i_cdrFileLoadStatus = p_cdrFileLoadStatus;
                break;

            default:
                // An invalid status is passed, don't change the current status.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME, 10025, this,
                                    C_METHOD_setStatus + " -- " +
                                    "***ERROR: An invalid status is passed: " + p_cdrFileLoadStatus);
                }
                break;
        }
    }

    /**
     * Return the number of successful rows loaded.
     * @return int i_numOfRowsSuccessfullyLoaded.
     */
    public int getNumOfSuccRowsLoaded()
    {
        return i_numOfRowsSuccessfullyLoaded;
    }

    /**
     * Return the number of rows not loaded due to data error.
     * @return int i_numRowsNotSuccessfullyLoaded.
     */
    public int getNumRowsNotLoadedDataError()
    {
        return i_numRowsNotLoadedDataError;
    }

    /**
     * Return the number of rows not loaded because all fields are null.
     * @return int i_numRowsLoadedFieldsNull.
     */
    public int getNumRowsNotLoadedAllFieldsNull()
    {
        return i_numRowsNotLoadedAllFieldsNull;
    }

    /**
     * Return the total logical records read.
     * @return int i_totalLogicalRecordsRead.
     */
    public int getTotalLogicalRecordRead()
    {
        return i_totalLogicalRecordsRead;
    }

    /**
     * Return the total logical records rejected.
     * @return int i_totalLogicalRecordsRead.
     */
    public int getTotalLogicalRecordRejected()
    {
        return i_totalLogicalRecordsRejected;
    }

    /**
     * Return the total logical records discarded.
     * @return int i_totalLogicalRecordsDiscarded.
     */
    public int getTotalLogicalRecordDiscarded()
    {
        return i_totalLogicalRecordsDiscarded;
    }
    

    /**
     * Return the start time.
     * @return PpasDateTime i_startTime.
     */
    public PpasDateTime getStartTime()
    {
        return i_startTime;
    }

    /**
     * Return the end time.
     * @return PpasDateTime i_endTime.
     */
    public PpasDateTime getEndTime()
    {
        return i_endTime;
    }

    /**
     * Return the Elapse time.
     * @return String i_elapsedTime.
     */
    public String getElapseTime()
    {
        return i_elapsedTime;
    }

    /**
     * Return the Elapse time.
     * @return String i_cpuTime.
     */
    public String getCpuTime()
    {
        return i_cpuTime;
    }

    /** Get the exception that caused this file to fail. Could be null even if a failure.
     * This will only be set if there is a problem reading a file to check results.
     * 
     * @return Originating exception or <code>null</code>.
     */
    public Exception getOriginatingException()
    {
        return i_originatingException;
    }

    /** Check the proportion of records that have failed and set the status to Failure if it is too high.
     * 
     *  @param p_failurePercent Percentage of records that must fail (bad/discarded/etc) for the file to be
     * flagged as failed rather than warning.
     *  @param p_minRows Minimum number of rows to activate the failure. This ensures that a failure to load
     * one record if the file only contains oe record does not flag a failure.
     *  @return True if this causes the file to be flagged as a failure.
     */
    public boolean checkFailedProportion(int p_failurePercent, int p_minRows)
    {
        boolean l_causesFailure = false;
        
        int l_rowsRead = getTotalLogicalRecordRead();
        
        if (l_rowsRead > 0 && l_rowsRead >= p_minRows && getStatus() < C_STATUS_FAILURE)
        {
            // We have read enough rows to make the check worth while.
            
            int l_failedRecs = getTotalLogicalRecordDiscarded() + getTotalLogicalRecordRejected();
            
            float l_failedPercent = (float)(l_failedRecs * 100.0)/l_rowsRead;
            
            if (l_failedPercent >= p_failurePercent)
            {
                setStatus(C_STATUS_FAILURE);
                l_causesFailure = true;
            }
        }
        
        return l_causesFailure;
    }
    
    /**
     * Returns a String representation of this object's state.
     * 
     * @return String of this class's significant instance variable values.
     */
    public String toString()
    {
        StringBuffer l_toStringBuf = new StringBuffer(C_CLASS_NAME + ": [");
        l_toStringBuf.append("i_sqlLoaderLogFilename="          + i_sqlLoaderLogFilename + ", ");
        l_toStringBuf.append("i_cdrFileLoadStatus="             + i_cdrFileLoadStatus + ", ");
        l_toStringBuf.append("i_numOfRowsSuccessfullyLoaded="   + i_numOfRowsSuccessfullyLoaded + ", ");
        l_toStringBuf.append("i_numRowsNotLoadedDataError="     + i_numRowsNotLoadedDataError + ", ");
        l_toStringBuf.append("i_numRowsNotLoadedAllFieldsNull=" + i_numRowsNotLoadedAllFieldsNull + ", ");
        l_toStringBuf.append("i_totalLogicalRecordsRead="       + i_totalLogicalRecordsRead + ", ");
        l_toStringBuf.append("i_totalLogicalRecordsRejected="   + i_totalLogicalRecordsRejected + ", ");
        l_toStringBuf.append("i_totalLogicalRecordsDiscarded="  + i_totalLogicalRecordsDiscarded + ", ");
        l_toStringBuf.append("i_recordErrorCnt="                + i_recordErrorCnt + ", ");
        l_toStringBuf.append("i_oracleErrorCnt="                + i_oracleErrorCnt + ", ");
        l_toStringBuf.append("i_elapsedTime="                   + i_elapsedTime + ", ");
        l_toStringBuf.append("i_cpuTime="                       + i_cpuTime + ", ");
        l_toStringBuf.append("i_startTime="                     + i_startTime + ", ");
        l_toStringBuf.append("i_endTime="                       + i_endTime + ", ");
        l_toStringBuf.append("Data Errors: [");
        if (i_dbmsDataErrors != null)
        {
            Iterator l_it = i_dbmsDataErrors.iterator();
            
            while (l_it.hasNext())
            {
                l_toStringBuf.append(l_it.next());
                if (l_it.hasNext())
                {
                    l_toStringBuf.append(", ");
                }
            }
        }
        l_toStringBuf.append("]]");

        return(l_toStringBuf.toString()); 
    }

    //==========================================================================
    // Private method(s).
    //==========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_parseLogFile = "parseLogFile";
    /**
     * Parses the passed SQL *Loader log file in order to retrieve CDR file loading info.
     * 
     * @param p_sqlLoaderLogFilename  the name of SQL *Loader log file (the full path is assumed).
     */
    private void parseLogFile(String p_sqlLoaderLogFilename)
    {
        // Local constants.
        final int    L_MATCHER_GROUP_MONTH      = 2;
        final int    L_MATCHER_GROUP_DAY        = 3;
        final int    L_MATCHER_GROUP_TIME       = 4;
        final int    L_MATCHER_GROUP_YEAR       = 5;

        // Local variables.
        BufferedReader l_logFileReader    = null;
        String         l_line             = null;
        Matcher        l_matcher          = null;
        String         l_dateAndTimeStr   = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Entering " + C_METHOD_parseLogFile);
        }

        i_cdrFileLoadStatus = C_STATUS_SUCCESS;

        try
        {
            l_logFileReader = new BufferedReader(new FileReader(new File(p_sqlLoaderLogFilename)));

            while ((l_line = l_logFileReader.readLine()) != null)
            {
                l_line = l_line.trim();
                if (l_line.length() == 0)
                {
                    // This is an empty line, read next line without parsing.
                    continue;
                }

                // Check if a record error (rejected or discarded) has occurred.
                if (C_PATTERN_RECORD_ERROR.matcher(l_line).matches())
                {
                    i_recordErrorCnt++;
                }
                else if ((l_matcher = C_PATTERN_SQL_LOADER_ERROR.matcher(l_line)).matches())
                {
                    // An SQL*Loader error has occurred, always treated as a serious error!
                    i_cdrFileLoadStatus = C_STATUS_SERIOUS_FAILURE;
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10031, this,
                                        "***ERROR: An SQL*Loader error has occurred: " + l_matcher.group(0));
                    }
                }
                else if ((l_matcher = C_PATTERN_ORACLE_ERROR.matcher(l_line)).matches())
                {
                    // Oracle error has occurred.
                    String l_dbmsError = l_matcher.group(1);

                    i_oracleErrorCnt++;
                    if (i_cdrFileLoadStatus == C_STATUS_SUCCESS || i_cdrFileLoadStatus == C_STATUS_WARNING)
                    {
                        i_cdrFileLoadStatus = i_dbmsDataErrors.contains(l_dbmsError) ?
                                C_STATUS_WARNING : C_STATUS_SERIOUS_FAILURE;
                        
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME, 10032, this,
                                            C_METHOD_parseLogFile + " -- Found " + l_dbmsError + 
                                            " status is now " + i_cdrFileLoadStatus);
                        }
                    }
                }
                else if ((l_matcher = C_PATTERN_SUCCESSFULLY_LOADED.matcher(l_line)).matches())
                {
                    i_numOfRowsSuccessfullyLoaded += Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_NOT_LOADED_DATA_ERRORS.matcher(l_line)).matches())
                {
                    i_numRowsNotLoadedDataError += Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_NOT_LOADED_ALL_FIELDS_NULL.matcher(l_line)).matches())
                {
                    i_numRowsNotLoadedAllFieldsNull += Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_TOT_RECORDS_READ.matcher(l_line)).matches())
                {
                    i_totalLogicalRecordsRead = Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_TOT_RECORDS_REJECTED.matcher(l_line)).matches())
                {
                    i_totalLogicalRecordsRejected = Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_TOT_RECORDS_DISCARDED.matcher(l_line)).matches())
                {
                    i_totalLogicalRecordsDiscarded = Integer.parseInt(l_matcher.group(1));
                }
                else if ((l_matcher = C_PATTERN_START_TIME.matcher(l_line)).matches())
                {
                    // Create a time string with the syntax 'yyyyMMdd HH:mm:ss'.
                    l_dateAndTimeStr = l_matcher.group(L_MATCHER_GROUP_YEAR)  +
                                       l_matcher.group(L_MATCHER_GROUP_MONTH) +
                                       l_matcher.group(L_MATCHER_GROUP_DAY)   +
                                       " "                                    +
                                       l_matcher.group(L_MATCHER_GROUP_TIME);
                    i_startTime      = new PpasDateTime(l_dateAndTimeStr,
                                                        new UtilDateFormat("yyyyMMMdd HH:mm:ss"));
                }
                else if ((l_matcher = C_PATTERN_END_TIME.matcher(l_line)).matches())
                {
                    // Create a time string with the syntax 'yyyyMMdd HH:mm:ss'.
                    l_dateAndTimeStr = l_matcher.group(L_MATCHER_GROUP_YEAR)  +
                                       l_matcher.group(L_MATCHER_GROUP_MONTH) +
                                       l_matcher.group(L_MATCHER_GROUP_DAY)   +
                                       " "                                    +
                                       l_matcher.group(L_MATCHER_GROUP_TIME);
                    i_endTime        = new PpasDateTime(l_dateAndTimeStr,
                                                        new UtilDateFormat("yyyyMMMdd HH:mm:ss"));
                }
                else if ((l_matcher = C_PATTERN_ELAPSED_TIME.matcher(l_line)).matches())
                {
                    i_elapsedTime = l_matcher.group(1);
                }
                else if ((l_matcher = C_PATTERN_CPU_TIME.matcher(l_line)).matches())
                {
                    i_cpuTime = l_matcher.group(1);
                }
            } //End of while
        }
        catch (FileNotFoundException e)
        {
            i_cdrFileLoadStatus = C_STATUS_SERIOUS_FAILURE;
            i_originatingException = e;
        }
        catch (IOException e)
        {
            i_cdrFileLoadStatus = C_STATUS_SERIOUS_FAILURE;
            i_originatingException = e;
        }
        finally
        {
            if (l_logFileReader != null)
            {
                try
                {
                    l_logFileReader.close();
                }
                catch (IOException l_ioEx2)
                {
                    // Nothing to do.
                    l_ioEx2 = null;
                }
                finally
                {
                    l_logFileReader = null;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this,
                            "Leaving " + C_METHOD_parseLogFile);
        }
    }
}
