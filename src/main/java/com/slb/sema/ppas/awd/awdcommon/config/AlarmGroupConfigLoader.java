/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmActionGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDestination;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDestinationGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmFormat;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.EmailAlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.FileAlarmDestination;
import com.slb.sema.ppas.awd.awdcommon.alarm.FtpAlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.FtpAlarmDestination;
import com.slb.sema.ppas.awd.awdcommon.alarm.LogAlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.TemplateTextAlarmFormat;
import com.slb.sema.ppas.awd.awdcommon.alarm.TerminalAlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.UserAlarmDestination;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Loader which supports the loading of Alarm Group related configuration.
 * It can instantiate and load an <code>AlarmGroupConfig</code> object from
 * the persisted configuation.
 * It can also instantiate and load a single <code>AlarmGroup</code> object
 * from the persisted configuration.
 */
public class AlarmGroupConfigLoader extends AwdAbstractConfigLoader
{
    /** Name of class. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmGroupConfigLoader";

    /** Alarm group configuration loader.
     * 
     * @param p_logger           Logger for directing loggable events.
     * @param p_configProperties Configuration properties.
     * @param p_awdContext       Session context.
     */
    public AlarmGroupConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext
    )
    {
        super(p_logger, p_configProperties, p_awdContext);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /** Load alarm group configuration.
     * 
     * @param p_configFilename Name of file containing configuration.
     * @return Confoiguration for alarm groups.
     * @throws AwdConfigException If configuration cannot be loaded.
     */
    public AlarmGroupConfig loadAlarmGroupConfig(
        String                  p_configFilename)
    throws AwdConfigException
    {
        AlarmGroupConfig             l_config;
        
        l_config = new AlarmGroupConfig();
        
        // loadTestConfig(l_config);
        loadLayeredConfig(p_configFilename, l_config);
        
        return l_config;
    }

    /** Name of method. Balue is {@value}. */
    private static final String C_METHOD_loadSignleAlarmGroup = "loadSignleAlarmGroup";
    /** Load configuration for a single alarm group.
     * 
     * @param p_configFilename Name of file containing configuration.
     * @param p_alarmGroupName Alarm group for which configuration is needed.
     * @return Configuration.
     * @throws AwdConfigException If configuration cannot be loaded.
     */
    public AlarmGroup loadSingleAlarmGroup(
        String                  p_configFilename,
        String                  p_alarmGroupName)
    throws AwdConfigException
    {
        AlarmGroup              l_alarmGroup;
        AlarmGroupConfig        l_config;

        l_config = new AlarmGroupConfig();
        loadLayeredConfig(p_configFilename, l_config);
        
        l_alarmGroup = l_config.getAlarmGroup(p_alarmGroupName);
        if(l_alarmGroup == null)
        {
            throw new AwdConfigException(
                    C_CLASS_NAME,
                    C_METHOD_loadSignleAlarmGroup,
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().unknownAlarmGroupName(p_alarmGroupName));
        }
        
        return l_alarmGroup;
    }

    /** Name of method. Balue is {@value}. */
    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    /** Load configuration.
     * 
     * @param p_configFilename   Name of file containing configuration.
     * @param p_alarmGroupConfig Configuration object into which data will be inserted.
     * @throws AwdConfigException If configuration cannot be loaded.
     */
    private void loadLayeredConfig(
        String                  p_configFilename,
        AlarmGroupConfig        p_alarmGroupConfig)
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_alarmGroupConfig);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_alarmGroupConfig);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }

    /** Name of method. Balue is {@value}. */
    private static final String C_METHOD_loadConfig = "loadConfig";
    /** Load configuration.
     * 
     * @param p_configFilename   Path of file containing configuration.
     * @param p_alarmGroupConfig Configuration object into which data will be inserted.
     * @throws AwdConfigException If configuration cannot be loaded.
     */
    private void loadConfig(
        String                  p_configFilename,
        AlarmGroupConfig        p_alarmGroupConfig)
    throws AwdConfigException
    {

        ConfigTokenizer         l_tokenizer;
        String                  l_token;
                                
        AlarmDestination        l_alarmDestination = null;
        String                  l_destinationName;
        String                  l_destinationFirstName;
        String                  l_destinationLastName;
        String                  l_destinationType;
        String                  l_destinationNode;
        int                     l_destinationPort;
        String                  l_destinationUsername;
        String                  l_destinationPassword;
        String                  l_destinationFilename;
        String                  l_destinationAppendMode;
        int                     l_destinationAppendModeEnum;
        String                  l_destinationEmailAddress;

        AlarmFormat             l_alarmFormat = null;
        String                  l_formatName;
        String                  l_formatType;
        String                  l_formatTemplateFile;
        FileInputStream         l_fis;
        StringBuffer            l_sb;
        int                     l_readCount;
        byte                    l_bufferARR[] = null;
        String                  l_formatTemplate = null;
        
        AlarmAction             l_alarmAction = null;

        AlarmActionGroup        l_actionGroup;
        String                  l_actionGroupName;
        String                  l_actionType;

        AlarmDestinationGroup   l_destinationGroup;
        String                  l_destinationGroupName;
        
        AlarmGroup              l_alarmGroup;
               
        String                  l_alarmGroupName;
        
        AwdConfigException           l_exception;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        // Read destinations
        l_token = l_tokenizer.nextToken();
        while("alarm.destination.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_destinationName = l_tokenizer.getExpectedNextToken();
            l_tokenizer.getExpectedNextToken("{");
            l_destinationType = l_tokenizer.getExpectedNextParameter("type");
            if("FTP_DESTINATION".equals(l_destinationType))
            {
                l_destinationNode = l_tokenizer.getExpectedNextParameter("node");
                l_destinationPort = l_tokenizer.getOptionalNextParameterInt("port", 21);
                l_destinationFilename = l_tokenizer.getExpectedNextParameter("filename");
                l_destinationAppendModeEnum =FtpAlarmDestination.C_APPEND_MODE_STORE;
                l_destinationAppendMode = l_tokenizer.getOptionalNextParameter("appendMode", "store");
                if("append".equalsIgnoreCase(l_destinationAppendMode))
                {
                    l_destinationAppendModeEnum = FtpAlarmDestination.C_APPEND_MODE_APPEND;
                }
                else if("store".equalsIgnoreCase(l_destinationAppendMode))
                {
                    l_destinationAppendModeEnum = FtpAlarmDestination.C_APPEND_MODE_STORE;
                }
                else
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().unknownFtpAppendModeAtLine(
                                    l_destinationAppendMode, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_destinationUsername = l_tokenizer.getExpectedNextParameter("username");
                l_destinationPassword = l_tokenizer.getExpectedNextParameter("password");
                l_alarmDestination = new FtpAlarmDestination(
                        l_destinationNode, l_destinationPort, l_destinationFilename,
                        l_destinationAppendModeEnum,
                        l_destinationUsername, l_destinationPassword);                    
            }
            else if("USER_DESTINATION".equals(l_destinationType))
            {
                l_destinationUsername = l_tokenizer.getOptionalNextParameter("username", "");
                l_destinationFirstName = l_tokenizer.getOptionalNextParameter("firstName", "");
                l_destinationLastName = l_tokenizer.getOptionalNextParameter("lastName", "");
                l_destinationEmailAddress = l_tokenizer.getOptionalNextParameter("emailAddress", "");
                l_alarmDestination = new UserAlarmDestination(
                        l_destinationFirstName, l_destinationLastName, l_destinationUsername,
                        l_destinationEmailAddress);
            }
            else if("FILE_DESTINATION".equals(l_destinationType))
            {
                l_destinationFilename = l_tokenizer.getExpectedNextParameter("pathAndFilename");
                l_alarmDestination = new FileAlarmDestination(l_destinationFilename);
            }
            else
            {
                handleConfigException(C_METHOD_loadConfig, 10000,
                        AwdKey.get().unknownDestTypeAtLine(
                               l_destinationType, l_tokenizer.getLineNumber(), p_configFilename));
            }
            l_tokenizer.getExpectedNextToken("}");
            p_alarmGroupConfig.addDestination(l_destinationName, l_alarmDestination);
            l_token = l_tokenizer.nextToken();
        }

        // Read formats
        while("alarm.format.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_formatName = l_tokenizer.getExpectedNextToken();
            l_tokenizer.getExpectedNextToken("{");
            l_formatType = l_tokenizer.getExpectedNextParameter("formatType");
            if("TEMPLATE_TEXT_FORMAT".equals(l_formatType))
            {
                l_formatTemplateFile = l_tokenizer.getOptionalNextParameter("formatTemplateFile");
                if(l_formatTemplateFile != null)
                {
                    if(l_bufferARR == null)
                    {
                        l_bufferARR = new byte[8196];
                    }
                    l_sb = new StringBuffer(8196);
                    try
                    {
                        l_fis = new FileInputStream(l_formatTemplateFile);
                        while((l_readCount = l_fis.read(l_bufferARR)) >= 0)
                        {
                            l_sb.append(new String(l_bufferARR, 0 , l_readCount));
                        }
                        l_fis.close();
                        l_formatTemplate = l_sb.toString();
                    }
                    catch(FileNotFoundException l_fnfe)
                    {
                        l_exception = new AwdConfigException(
                                C_CLASS_NAME,
                                C_METHOD_loadConfig,
                                10000,
                                this,
                                (PpasRequest)null,
                                0,
                                AwdKey.get().referredFileNotFoundAtLine(
                                        l_formatTemplateFile, l_tokenizer.getLineNumber(), p_configFilename),
                                l_fnfe);
                        logMessage(l_exception);
                        throw l_exception;
                    }
                    catch(IOException l_ioe)
                    {
                        l_exception = new AwdConfigException(
                                C_CLASS_NAME,
                                C_METHOD_loadConfig,
                                10000,
                                this,
                                (PpasRequest)null,
                                0,
                                AwdKey.get().ioExceptionReadingFromFile(p_configFilename),
                                l_ioe);
                        logMessage(l_exception);
                        throw l_exception;
                    }
                }
                else
                {
                    l_formatTemplate = l_tokenizer.getOptionalNextParameter("formatTemplate");
                    if(l_formatTemplate == null)
                    {
                        handleConfigException(C_METHOD_loadConfig, 10000,
                                AwdKey.get().expectedOneOfAtLine(
                                    "formatTemplateFile or formatTemplate",
                                    l_tokenizer.getLineNumber(),
                                    p_configFilename));
                    }
                }
                l_alarmFormat = new TemplateTextAlarmFormat(l_formatTemplate);
            }
            else
            {
                handleConfigException(C_METHOD_loadConfig, 10000,
                        AwdKey.get().unknownFormatTypeAtLine(
                                l_formatType, l_tokenizer.getLineNumber(), p_configFilename));
            }
            l_tokenizer.getExpectedNextToken("}");
            p_alarmGroupConfig.addFormat(l_formatName, l_alarmFormat);
            l_token = l_tokenizer.nextToken();
        }

        // Read action groups
        while("alarm.action.group.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_actionGroupName = l_tokenizer.getExpectedNextToken();
            l_actionGroup = new AlarmActionGroup();
            l_tokenizer.getExpectedNextToken("{");
            l_token = l_tokenizer.nextToken();
            while("type".equals(l_token))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_actionType = l_tokenizer.getExpectedNextToken();
                if("FTP_ACTION".equals(l_actionType))
                {
                    l_alarmAction = new FtpAlarmAction();
                }
                else if("EMAIL_ACTION".equals(l_actionType))
                {
                    l_alarmAction = new EmailAlarmAction();
                }
                else if("TERMINAL_ACTION".equals(l_actionType))
                {
                    l_alarmAction = new TerminalAlarmAction();
                }
                else if("LOG_ACTION".equals(l_actionType))
                {
                    l_alarmAction = new LogAlarmAction();
                }
                else
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().unknownActionTypeAtLine(
                                    l_actionType, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_actionGroup.addAlarmAction(l_alarmAction);
                l_token = l_tokenizer.nextToken();
            }
            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'type' or '}'");
            }
            p_alarmGroupConfig.addActionGroup(l_actionGroupName, l_actionGroup);
            l_token = l_tokenizer.nextToken();
        }

        // Read destination groups
        while("alarm.destination.group.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_destinationGroupName = l_tokenizer.getExpectedNextToken();
            l_destinationGroup = new AlarmDestinationGroup();
            l_tokenizer.getExpectedNextToken("{");
            l_token = l_tokenizer.nextToken();
            while("destination".equals(l_token))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_destinationName = l_tokenizer.getExpectedNextToken();
                l_alarmDestination = p_alarmGroupConfig.getDestination(l_destinationName);
                if(l_alarmDestination != null)
                {
                    l_destinationGroup.addAlarmDestination(l_alarmDestination);
                }
                else
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().undefinedDestNameAtLine(
                                    l_destinationName, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_token = l_tokenizer.nextToken();
            }
            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'destination' or '}'");
            }
            p_alarmGroupConfig.addDestinationGroup(l_destinationGroupName, l_destinationGroup);
            l_token = l_tokenizer.nextToken();
        }

        // Read alarm groups
        while("alarm.group.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_alarmGroupName = l_tokenizer.getExpectedNextToken();
            l_alarmGroup = new AlarmGroup(l_alarmGroupName);
            l_tokenizer.getExpectedNextToken("{");
            l_token = l_tokenizer.nextToken();
            while("actionGroup".equals(l_token))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_actionGroupName = l_tokenizer.getExpectedNextToken();
                l_formatName = l_tokenizer.getExpectedNextParameter("format");
                l_destinationGroupName = l_tokenizer.getExpectedNextParameter("destinationGroup");
                l_actionGroup = p_alarmGroupConfig.getActionGroup(l_actionGroupName);
                if(l_actionGroup == null)
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().undefinedActionGroupNameAtLine(
                                    l_actionGroupName, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_alarmFormat = p_alarmGroupConfig.getFormat(l_formatName);
                if(l_alarmFormat == null)
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().undefinedFormatNameAtLine(
                                    l_formatName, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_destinationGroup = p_alarmGroupConfig.getDestinationGroup(l_destinationGroupName);
                if(l_destinationGroup == null)
                {
                    handleConfigException(C_METHOD_loadConfig, 10000,
                            AwdKey.get().undefinedDestGroupNameAtLine(
                                    l_destinationGroupName, l_tokenizer.getLineNumber(), p_configFilename));
                }
                l_alarmGroup.add(l_actionGroup, l_destinationGroup, l_alarmFormat);
                l_token = l_tokenizer.nextToken();

            }
            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'}' or 'actionGroup'");
            }
            p_alarmGroupConfig.addAlarmGroup(l_alarmGroup);
            l_token = l_tokenizer.nextToken();
        }
        if(l_token != null)
        {
            handleUnexpectedToken(C_METHOD_loadConfig, 10000,
                    l_token, l_tokenizer.getLineNumber(), 
                    p_configFilename, "'alarm.group.name' or end of file");
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                "Leaving " + C_METHOD_loadConfig);
    }

    /** Handle error in configuration and raise an exception.
     * 
     * @param p_methodName      Name of method.
     * @param p_statementNumber Statement number.
     * @param p_key             Exception key.
     * @throws AwdConfigException Always thrown - this is the desired result.
     */
    private void handleConfigException(
        String                  p_methodName,
        int                     p_statementNumber,
        AwdKey.AwdExceptionKey  p_key)
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                C_CLASS_NAME,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                p_key);
        logMessage(l_exception);
        throw l_exception;
    }

    /** Handle an unexpected token.
     * 
     * @param p_methodName      Name of method.
     * @param p_statementNumber Statement number.
     * @param p_unexpectedToken Actual value.
     * @param p_lineNumber      Line number.
     * @param p_filename        Name of file.
     * @param p_expectedToken   Expected value.
     * @throws AwdConfigException Always thrown - this is the desired result.
     */
    private void handleUnexpectedToken(
        String                  p_methodName,
        int                     p_statementNumber,
        String                  p_unexpectedToken,
        int                     p_lineNumber,
        String                  p_filename,
        String                  p_expectedToken
    )
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                C_CLASS_NAME,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                AwdKey.get().unexpectedTokenAtLine(p_unexpectedToken,
                                                   p_lineNumber,
                                                   p_filename,
                                                   p_expectedToken));
        logMessage(l_exception);
        throw l_exception;
    }

    /** Log a message - either using standard logger if supplied or to standard error..
     * 
     * @param l_loggable Event to be logged.
     */
    private void logMessage(LoggableInterface l_loggable)
    {
        if(i_logger == null)
        {
            System.err.println(l_loggable.getMessageText(Locale.getDefault()));
        }
        else
        {
            i_logger.logMessage(l_loggable);
        }
    }

}
