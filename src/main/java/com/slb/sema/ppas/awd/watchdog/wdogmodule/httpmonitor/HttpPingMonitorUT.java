////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       HttpPingMonitorUT.java
//      DATE            :       08-Feb-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Unit test for HttpPingMonitor.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/05/07 | R.Grimshaw | Replaced sleep with snooze.     | PpacLon#3116
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.httpmonitor;

import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.awd.awdcommon.AwdTestCaseTT;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorListener;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Unit test for HttpPingMonitor.
 */
public class HttpPingMonitorUT extends AwdTestCaseTT
{

    /**
     * Creates a new HttpPingMonitorUT.
     * 
     * @param p_name            name of test.
     * @param p_flags           flags (for context).
     */
    public HttpPingMonitorUT(String p_name, long p_flags)
    {
        super(p_name, p_flags);
    }

    /**
     * Creates a new HttpPingMonitorUT.
     * 
     * @param p_name            name of test.
     */
    public HttpPingMonitorUT(String p_name)
    {
        super(p_name);
    }
    
    /**
     * @ut.dependson  None.
     * @ut.when       A HTTP monitor monitors a working HTTP server, the server
     *                fails, then the server is restarted.
     * @ut.then       Monitor detects the failure and also detects the recovery.
     * @ut.attributes +f
     */
    public void testFailedMonitor()
    {
        HashMap                 l_configMap;
        HttpPingMonitor         l_httpPingMonitor;
        MyCountingML            l_listener;
        
        beginOfTest("Test Failed Monitor");
        
        // Get manager port number
        // TODO 1 Remove hard coded port numbers - either pass in as system property / arg or get programatically
        startProcessAndWaitForItToStart("GUI_SRV_A1001", 6301);
        
        l_configMap = new HashMap();
        l_configMap.put("httpPingServer","localhost");
        // TODO 1 Remove hard coded port number! At least use base and offset based on process name!
        l_configMap.put("httpPingPort","6300");
        l_configMap.put("httpPingRequestLine","GET /ascs/gui/Ping HTTP/1.1");
        l_configMap.put("httpPingResponseRegex",
                "(?s)HTTP.+ 200 OK.+<html>.*<head>.*</head>.*<body>.*OK.*</body>.*</html>.*");
        l_configMap.put("startDelayMillis","1");
        l_configMap.put("monitorIntervalMillis","5000");
        
        l_httpPingMonitor = new HttpPingMonitor(c_logger, "UtHttpPingMonitor");
        
        try
        {
            l_httpPingMonitor.init(l_configMap);
            l_listener = new MyCountingML();
            l_listener.check(0,0);
            l_httpPingMonitor.addMonitorListener(l_listener);
            l_httpPingMonitor.start();
            snooze(5, "Waiting until after next monitor test.");
            l_listener.check(0,0);
            // TODO impl l_httpPingMonitor.getState and check it here and other places
            stopProcessAndWaitForItToStop("GUI_SRV_A1001", 6301);
            snooze(20, "Waiting until after next monitor test.");
            l_listener.check(0,1);
            startProcessAndWaitForItToStart("GUI_SRV_A1001", 6301);
            snooze(20, "Waiting until after next monitor test.");
            l_listener.check(1,1);
        }
        catch(Exception l_e)
        {
            failedTestException(l_e);
        }
        finally
        {
            // Try to ensure tidy up after success or failure.
            try
            {
                if(l_httpPingMonitor != null)
                {
                    l_httpPingMonitor.stop();
                }
                stopProcess("GUI_SRV_A1001");
            }
            catch(Exception l_e)
            {
                l_e.printStackTrace();
            }
        }
        
        endOfTest();
        
    }

    /**
     * Define test suite. This unit test uses a standard JUnit method
     * to derive a list of test cases from 
     * the class.
     * @return Suite of tests to be run.
     */
    public static Test suite()
    {
        return new TestSuite(HttpPingMonitorUT.class);
    }
    
    /**
     * Allow test suite to be invoked from the command line.
     * @param p_argsARR These are not currently used.
     */
    public static void main(String[] p_argsARR)   
    {
        System.out.println("Parameters=" + Debug.arrayToString(p_argsARR));
        System.out.println();
        junit.textui.TestRunner.run(suite());
    }

    /**
     * Private inner class implementing a watchdog monitor listener which
     * counts the number and type of notifications it receives.
     * Used by some tests.
     */
    private class MyCountingML implements WatchdogMonitorListener
    {
       
        /** Number of times passed called. */
        public int              i_passedCounter = 0;

        /** Number of times failed called. */
        public int              i_failedCounter = 0;

        /** Number of times hasEnteredPassedState called. */
        public int              i_enteredPassedStateCounter = 0;

        /** Number of times hasEnteredFailedState called. */
        public int              i_enteredFailedStateCounter = 0;

        /** {@inheritDoc} */
        public void passed()
        {
            i_passedCounter++;
            say(">>>> HTTP monitor passed (count=" +
                    i_passedCounter + ").");            
        }

        /** {@inheritDoc} */
        public void failed()
        {
            i_failedCounter++;
            say(">>>> HTTP monitor failed (count=" +
                    i_failedCounter + ").");            
        }

        /** {@inheritDoc} */
        public void hasEnteredPassedState()
        {
            i_enteredPassedStateCounter++;
            say(">>>> HTTP monitor entered passed state (count=" +
                    i_enteredPassedStateCounter + ").");
        }

        /** {@inheritDoc} */
        public void hasEnteredFailedState()
        {
            i_enteredFailedStateCounter++;
            say(">>>> HTTP monitor entered failed state (count=" +
                    i_enteredFailedStateCounter + ").");
        }
        
        /**
         * Assets (using Junit) that the passed and failed counts match the
         * supplied values (fails if they don't - throws a runtime Junit
         * AssertionFailedError).
         * 
         * @param p_expectedPassedCount
         * @param p_expectedFailedCount
         */
        public void check(
            int                 p_expectedPassedCount,
            int                 p_expectedFailedCount
            )
        {
            assertEquals("Failed count does not equals expected count.",
                    p_expectedFailedCount, i_enteredFailedStateCounter);
            assertEquals("Passed count does not equals expected count.",
                    p_expectedPassedCount, i_enteredPassedStateCounter);
        }

    } // End of private inner class MyCountingML
    
} // End of public class HttpPingMonitorUT.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////