////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmActionListener.java
//      DATE            :       28-Oct-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2004
//
//      DESCRIPTION     :       Interface which an Alarm implementation can optionally
//                              implement to recieve action event notifications
//                              (within the same JVM as the actions occur).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;

/**
 * Interface which an Alarm implementation can optionally
 * implement to recieve action event notifications
 * (within the same JVM as the actions occur).
 */
public interface AlarmActionListener {
    
    /**
     * Called to report the success / failure of an Alarm action.
     * @param p_alarm           the alarm causing the action attempt.
     * @param p_success         success (true) or failure (false) of the action.
     * @param p_alarmAction     the alarm action attempted.
     * @param p_actionSpecificInfo action specific information (if any).
     */
    public void alarmActionStatus(
        Alarm                   p_alarm,
        boolean                 p_success,
        AlarmAction             p_alarmAction,
        Object                  p_actionSpecificInfo
        );

} // End of public interface AlarmDeliveryListener

////////////////////////////////////////////////////////////////////////////////
//End of file
////////////////////////////////////////////////////////////////////////////////