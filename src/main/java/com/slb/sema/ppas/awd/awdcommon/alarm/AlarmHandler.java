////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmHandler.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This class is the abstract super class for all
//                              Alarm Handlers.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * This class is the abstract super class for all Alarm Handlers. Concrete
 * Alarm Handler implementations must implement the two abstract methods
 * doDeliverAlarm and doDeliverAsynchAlarm. The first delivers alarms
 * synchronously and the second should deliver them asnychronously (i.e. in the
 * background). If the concrete class does not support asynchronous delivery
 * it should simply try to deliver the alarm synchronously instead.
 */
public abstract class AlarmHandler
implements InstrumentedObjectInterface
{
    
    /** Logger messages logged to. */
    protected Logger            i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Alarm hander's name (for instrumentation). */
    protected String            i_handlerName = "NotSet";

    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Instrument Set for this instrumented object. */
    protected InstrumentSet     i_instrumentSet;

    /**
     * Macro Substitutor which can be used by derived classes to expand any
     * macros where required.
     */
    protected SimpleMacroSubstitutor i_macroSubstitutor =
            new SimpleMacroSubstitutor();

    /**
     * Creates a new Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     * @param  p_handlerName    name of handler.
     */    
    public AlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext,
        String                  p_handlerName
    )
    {
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
        i_handlerName = p_handlerName;
        
        i_instrumentSet = new InstrumentSet("root");
        
        i_instrumentManager.registerObject(this);
        
        i_macroSubstitutor.addDefaultUtilPropeties(p_configProperties);
    }

    /**
     * Delivers the formatted alarm to the destination. This default
     * implementation delivers the alarm by calling the abstract
     * doDeliverAlarm method which should generally deliver the Alarm
     * synchronously - i.e. the alarm should have been 'delivered' (sent)
     * by the time this method returns.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * 
     */
    public void deliverAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
        )
    {
        doDeliverAlarm(
                p_alarm,
                p_formattedAlarm,
                p_destination
                );
    }

    /**
     * Delivers the formatted alarm to the destination. This default
     * implementation delivers the alarm by calling the abstract
     * doDeliverAsynchAlarm method which should generally deliver the Alarm
     * asynchronously - i.e. the alarm may still be being 'delivered' (sent)
     * in the background when this method returns. Concrete Alarm Handler
     * implementations which do not support asynchronous delivery should
     * attempt to deliver the alarm synchronously instead.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * @param  p_notifyer       notifyer to be notified once alarm 'delivered' (sent).
     * 
     */
    public void deliverAsynchAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifyer
        )
    {
        doDeliverAsynchAlarm(
                p_alarm,
                p_formattedAlarm,
                p_destination,
                p_notifyer
                );
    }

    /**
     * Delivers the formatted alarm to the destination. This default
     * implementation delivers the alarm by calling the abstract
     * doDeliverAsynchAlarm method which should generally deliver the Alarm
     * asynchronously - i.e. the alarm may still be being 'delivered' (sent)
     * in the background when this method returns. Concrete Alarm Handler
     * implementations which do not support asynchronous delivery should
     * attempt to deliver the alarm synchronously instead.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * 
     */
    public void deliverAsynchAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
        )
    {
        doDeliverAsynchAlarm(
                p_alarm,
                p_formattedAlarm,
                p_destination,
                (AsynchAlarmNotifiyer)null
                );
    }
            
    /**
     * Returns this instrumented object's instrument set.
     * @return This instrumented object's instrument set.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }
    
    /**
     * Returns this instrumented object's name.
     * @return This instrumented object's name.
     */
    public String getName()
    {
        return i_handlerName;
    }

    /**
     * Delivers the formatted alarm to the destination synchronously - i.e.
     * the alarm should have been 'delivered' (sent) by the time this method
     * returns.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * 
     */
    protected abstract void doDeliverAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
    );

    /**
     * Delivers the formatted alarm to the destination asynchronously - i.e.
     * the alarm may still be being 'delivered' (sent) in the background when
     * this method returns. Concrete Alarm Handler
     * implementations which do not support asynchronous delivery should
     * attempt to deliver the alarm synchronously instead. If a notifyer 
     * is supplied (is not <code>null</code>), implementations must
     * notify completion of the alarm delivery by calling finished on the
     * notifyer.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * @param  p_notifyer       notifyer to be notified once alarm 'delivered' (sent).
     * 
     */
    protected abstract void doDeliverAsynchAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifyer
        );

} // End of public abstract class AlarmHandler

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////