////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmDestinationGroup.java
//      DATE            :       28-Oct-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A group of alarm destinations.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashSet;
import java.util.Set;

/**
 * A group of alarm destinations.
 */
public class AlarmDestinationGroup
{

    /** Empty Alarm Destination array - used to create an Alarm Destination array. */
    private static final AlarmDestination C_EMPTY_ALARM_DESTINATION_ARRAY[] =
            new AlarmDestination[0];

    /** Name of the Alarm Destination Group. */
    private String              i_name;

    /** Set of Alarm Destinations in this Alarm Destination Group. */    
    private Set                 i_alarmDestinationSet;

    /**
     * Indicates if the cached array view is out of date and needs to be
     * recreated.
     */
    private boolean             i_cacheInvalidated;

    /**
     * Cached array view of Alarm Destinations in this Alarm Destination Group.
     */    
    private AlarmDestination    i_alarmDestinationCachedARR[];

    /** Creates a new Alarm Destination Group. */
    public AlarmDestinationGroup()
    {
        init();
    }

    /** Initializes the Alarm Destination Group. */
    private synchronized void init()
    {
        i_alarmDestinationSet = new HashSet();
        i_cacheInvalidated = true;
    }
    
    /**
     * Returns the name of the Alarm Destination Group.
     * @return The name of the Alarm Destination Group.
     */
    public String getName()
    {
        return(i_name);
    }
    /**
     * Adds the Alarm Destination to the group.
     * @param p_alarmDestination alarm destination to add.
     */
    public synchronized void addAlarmDestination(AlarmDestination p_alarmDestination)
    {
        i_cacheInvalidated = true;
        i_alarmDestinationSet.add(p_alarmDestination);
    }

    /**
     * Returns an array of the Alarm Destinations in the group.
     * @return An array of the Alarm Destinations in the group.
     */
    public synchronized AlarmDestination[] getDestinationArray()
    {

        if(i_cacheInvalidated)
        {
            i_alarmDestinationCachedARR = (AlarmDestination[])i_alarmDestinationSet.
                    toArray(C_EMPTY_ALARM_DESTINATION_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_alarmDestinationCachedARR;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace. 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        AlarmDestination        l_destinationARR[];
        int                     l_loop;
        
        l_sb = new StringBuffer(100);
        
        l_sb.append("AlarmDestinationGroup=[name=");
        l_sb.append(i_name);
        l_sb.append(", destination[]=[\n");
        l_destinationARR = getDestinationArray();
        for(l_loop = 0; l_loop < l_destinationARR.length; l_loop++)
        {
            l_sb.append("    ");
            l_sb.append(l_destinationARR[l_loop].toString());
            l_sb.append("\n");
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

} // End of public class AlarmDestinationGroup.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////

