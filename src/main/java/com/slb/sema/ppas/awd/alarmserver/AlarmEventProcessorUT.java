////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmEventProcessorUT.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Unit test (NON JUNIT CURRENTLY!!!) for Alarm
//                              Event Processor.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarmEvent;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Unit test (NON JUNIT CURRENTLY) for Alarm Event
 * Processor.
 */
public class AlarmEventProcessorUT
{
    /** Alarm Event Processor to UT. */
    private AlarmEventProcessor i_processor;
    
    /** Instrument manager for UT. */
    private InstrumentManager   i_instrumentManager;
    
    /** AWD Context for UR. */
    private AwdContext          i_awdContext;
    
    /** Bootstrap main method for UT.
     * @param  args             command line arguments.
     * @throws Exception        any exception running UT.
     */
    public static void main(String[] args)
    throws Exception
    {
        AlarmEventProcessorUT           l_processorUT;

        Debug.setDebugFullToSystemErr();
        // Debug.setDebugFullToFile();
        
        l_processorUT = new AlarmEventProcessorUT();
        
        l_processorUT.test1();
        
        Thread.sleep(30000);
    }

    /**
     * Test1.
     * @throws Exception        exception that occurred.
     */
    public void test1()
    throws Exception
    {
        AlarmEvent              l_event1;
        AlarmEvent              l_event2;
        AlarmEvent              l_event3;
        PpasProperties          l_configProperties;
        
        i_instrumentManager = new InstrumentManager();
        
        // <<i_awdContext = !!!!>>

        i_processor = new AlarmEventProcessor(
            (Logger)null, //<<????>>
            i_instrumentManager,
            i_awdContext
            );
        l_configProperties = new PpasProperties();
        l_configProperties.load("./bin/conf/alarmserverconfig.properties");
        i_processor.init(l_configProperties);

        System.out.println(i_processor);
        
        l_event1 = new SimpleAlarmEvent("NO_MATCH", "No match",
                "NODE1", "TEST_PROC1");
        i_processor.processEvent(l_event1);

        l_event2 = new SimpleAlarmEvent("EVENT_1_HIGH_SEV", "High sev match",
                "NODE1", "TEST_PROC1");
        i_processor.processEvent(l_event2);

        l_event3 = new SimpleAlarmEvent("EVENT_2_MOD_SEV", "Mod match",
                "NODE2", "TEST_PROC2");
        i_processor.processEvent(l_event3);
    }    

} // End of public class AlarmEventProcessorUT.

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////