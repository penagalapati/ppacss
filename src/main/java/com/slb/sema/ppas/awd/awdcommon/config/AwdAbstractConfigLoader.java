////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdAbstractConfigLoader.java
//      DATE            :       08-Jul-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       Abstract super class for all AWD config loaders.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.config;

import java.util.Locale;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.SimpleMacroSubstitutor;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Abstract super class for all AWD config loaders.
 */
public abstract class AwdAbstractConfigLoader
{

    /** Logger messages logged to. */
    protected Logger            i_logger;

    /**
     * Macro Substitutor which can be used by derived classes to expand any
     * macros where required.
     */
    protected SimpleMacroSubstitutor i_macroSubstitutor =
            new SimpleMacroSubstitutor('^');

    /**
     * Creates a new AwdAbstractConfigLoader.
     *  
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties properties to create loader from.
     * @param  p_awdContext     AWD context.
     */
    public AwdAbstractConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext
    )
    {
        super();

        i_logger = p_logger;
        i_macroSubstitutor.addDefaultUtilPropeties(p_configProperties);
        
    }

    /**
     * Conveniance method to create a new AWD Config Exception from supplied
     * details, log it and then throw it.
     *  
     * @param  p_className      calling class (passed to new exception).
     * @param  p_methodName     calling method (passed to new exception).
     * @param  p_statementNumber calling statement no. (passed to new exception).
     * @param  p_key            exception key (passed to new exception).
     * @throws AwdConfigException the newly created and logged AWD Config Exception.
     */    
    protected void handleConfigException(
        String                  p_className,
        String                  p_methodName,
        int                     p_statementNumber,
        AwdKey.AwdExceptionKey  p_key)
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                p_className,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                p_key);
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Convenieance method to create a new Unexpected Token
     * AWD Config Exception (the most common exception) from supplied
     * details, log it and then throw it.
     *  
     * @param  p_className      calling class (passed to new exception).
     * @param  p_methodName     calling method (passed to new exception).
     * @param  p_statementNumber calling statement no. (passed to new exception).
     * @param  p_unexpectedToken the unexpected token found.
     * @param  p_lineNumber     line number where unexpected token found.
     * @param  p_filename       filename in which unexpected token found.
     * @param  p_expectedToken  the token/tokens which was/were expected.
     * @throws AwdConfigException the newly created and logged AWD Config Exception.
     */    
    protected void handleUnexpectedToken(
        String                  p_className,
        String                  p_methodName,
        int                     p_statementNumber,
        String                  p_unexpectedToken,
        int                     p_lineNumber,
        String                  p_filename,
        String                  p_expectedToken
    )
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                p_className,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                AwdKey.get().unexpectedTokenAtLine(
                        p_unexpectedToken, p_lineNumber, p_filename, p_expectedToken));
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Convenieance method to create a new Previous Configuration Error
     * AWD Config Exception (the most common exception) from supplied
     * details, log it and then throw it.
     *  
     * @param  p_className      calling class (passed to new exception).
     * @param  p_methodName     calling method (passed to new exception).
     * @param  p_statementNumber calling statement no. (passed to new exception).
     * @param  p_lineNumber     line number where unexpected token found.
     * @param  p_filename       filename in which unexpected token found.
     * @param  p_exception      Originating exception.
     * @throws AwdConfigException the newly created and logged AWD Config Exception.
     */    
    protected void handlePreviousConfigErrorException(
        String                  p_className,
        String                  p_methodName,
        int                     p_statementNumber,
        int                     p_lineNumber,
        String                  p_filename,
        Exception               p_exception
    )
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                p_className,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                AwdKey.get().previousConfigErrorAtLine(p_lineNumber, p_filename),
                p_exception);
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Conveniece method which checks if the logger is <code>null</code>
     * , if not it logs the message, else it writes it to
     * </code>System.err</code>.
     * 
     * @param  p_loggable       the loggable message.
     */
    private void logMessage(LoggableInterface p_loggable)
    {
        if(i_logger == null)
        {
            System.err.println(p_loggable.getMessageText(Locale.getDefault()));
        }
        else
        {
            i_logger.logMessage(p_loggable);
        }
    }    

}// End of public abstract class AwdAbstractConfigLoader 

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
