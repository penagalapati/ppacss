////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmDestination.java
//      DATE            :       28-Oct-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Abstract super class for alarm destinations
//                              which hold the details of an alarm destination.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.alarm;

/**
 * Abstract super class for alarm destinations which hold the details of an
 * alarm destination.
 */
public abstract class AlarmDestination
{

    /** User destination. Value is {@value}*/
    protected static final String C_TYPE_USER = "DESTIN_USER";

    /** FTP destination. Value is {@value}*/
    protected static final String C_TYPE_FTP = "DESTIN_FTP";

    /** File destination. Value is {@value}*/
    protected static final String C_TYPE_FILE = "DESTIN_FILE";

    /** Type of destination - one of the C_TYPE constants. */
    private final String        i_typeName;
    
    /**
     * Creates a new Alarm Destination.
     * @param p_typeName        type of destination - one of the C_TYPE
     *                          constants.
     */
    protected AlarmDestination(
        String                  p_typeName
    )
    {
        i_typeName = p_typeName;
    }
     
    /**
     * Returns the type of destination (one of the C_TYPE contants).
     * @return The type of destination (one of the C_TYPE contants).
     */
    public String getDestinationTypeName()
    {
        return i_typeName;
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace. 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return("AlarmDestination=[type=" + i_typeName + "]");
    }

} // End of public class AlarmDestination.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
