////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmActionGroup.java
//      DATE            :       28-Oct-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       An Alarm Action Group.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashSet;
import java.util.Set;

/**
 * An Alarm Action Group defines a named group of Alarm Actions.
 */
public class AlarmActionGroup
{

    /** Empty Alarm Action array - used to create Alarm Action array. */
    private static final AlarmAction C_EMPTY_ALARM_ACTION_ARRAY[] =
            new AlarmAction[0];

    /** Name of Alarm Action Group. */
    private String              i_name;

    /**
     * Set of Alarm Actions in this Alarm Action Group.
     */    
    private Set                 i_alarmActionSet;

    /**
     * Indicates if the cached array view is out of date and needs to be
     * recreated.
     */
    private boolean             i_cacheInvalidated;

    /**
     * Cached array view of Alarm Actions in this Alarm Action Group.
     */    
    private AlarmAction         i_alarmActionCachedARR[];

    /**
     * Creates a new, empty Alarm Action Group.
     */
    public AlarmActionGroup()
    {
        init();
    }
    
    /**
     * Initialises the Alarm Action Group.
     */
    private synchronized void init()
    {
        i_alarmActionSet = new HashSet();
        i_cacheInvalidated = true;
    }

    /**
     * Returns the name of this Alarm Action Group.
     * @return Name of this Alarm Action Group.
     */
    public String getName()
    {
        return(i_name);
    }
    
    /**
     * Adds a new Alarm Action to this Alarm Action Group.
     * @param p_alarmAction     Alarm Action to add.
     */
    public synchronized void addAlarmAction(AlarmAction p_alarmAction)
    {
        i_cacheInvalidated = true;
        i_alarmActionSet.add(p_alarmAction);
    }

    /**
     * Returns an array of the Alarm Actions which make up this Alarm Action Group.
     * @return Array of the Alarm Actions which make up this Alarm Action Group.
     */
    public synchronized AlarmAction[] getActionArray()
    {

        if(i_cacheInvalidated)
        {
            i_alarmActionCachedARR = (AlarmAction[])i_alarmActionSet.
                    toArray(C_EMPTY_ALARM_ACTION_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_alarmActionCachedARR;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        AlarmAction             l_actionARR[];
        int                     l_loop;
        
        l_sb = new StringBuffer(100);
        
        l_sb.append("AlarmActionGroup=[name=");
        l_sb.append(i_name);
        l_sb.append(", action[]=[\n");
        l_actionARR = getActionArray();
        for(l_loop = 0; l_loop < l_actionARR.length; l_loop++)
        {
            l_sb.append("    ");
            l_sb.append(l_actionARR[l_loop].toString());
            l_sb.append("\n");
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

} // End of public class AlarmAction

////////////////////////////////////////////////////////////////////////////////
//End of file
////////////////////////////////////////////////////////////////////////////////