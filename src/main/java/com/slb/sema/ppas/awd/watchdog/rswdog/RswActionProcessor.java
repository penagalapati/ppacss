////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswActionProcessor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       High level processing for possible RSW actions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/07/06 | M.Vonka    | Log error if non-existant alarm | PpaLon#2496/9386
//          |            | or alarm group is attempted to  |
//          |            | be used. Try to default to a    |
//          |            | known alarm or alarm group and  |
//          |            | at least raise that alarm.      |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.alarm.ConfiguredAlarm;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmInstanceConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.OsCommandWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.RaiseAlarmWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroupConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogPlatform;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * High level processing for possible RSW actions. Actual processing for
 * actions will be in lower level classes.
 */
public class RswActionProcessor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RswActionProcessor";

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Alarm Group configuration to use when processing RSW actions.*/
    private AlarmGroupConfig    i_alarmGroupConfig;
    
    /** Alarm configuration to use when processing RSW actions. */
    private AlarmConfig         i_alarmConfig;
    
    /** Watchdog Action Group configuration to use when processing RSW actions. */
    private WatchdogActionGroupConfig i_watchdogActionGroupConfig;
    
    /**
     * Alarm processor used to process delivery of any alarms when
     * processing RSW actions.
     */
    private AlarmProcessor      i_alarmProcessor;
    
    /**
     * Platform with some utility watchdog specific extensions.
     */
    private WatchdogPlatform    i_watchdogPlatform;
    
    /** Process name of running process. */
    private String              i_processName;
    
    /** Node name of running process. */
    private String              i_nodeName;

    /** Configuration properties. */
    private PpasProperties      i_configProperties;
        
    /**
     * Creates a new RswActionProcessor.
     * 
     * @param p_logger          loger to log any messages to.
     * @param p_instrumentManager instrument manager to use.
     * @param p_awdContext      context this component is running under.
     * @param p_configProperties configuration properties.
     * @param p_alarmGroupConfig alarm group config to use.
     * @param p_alarmConfig     alarm config to use.
     * @param p_watchdogActionGroupConfig watchdog action group config to use.
     */
    public RswActionProcessor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext,
        PpasProperties          p_configProperties,
        AlarmGroupConfig        p_alarmGroupConfig,
        AlarmConfig             p_alarmConfig,
        WatchdogActionGroupConfig p_watchdogActionGroupConfig
    )
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
        i_processName = i_awdContext.getProcessName();
        i_nodeName = i_awdContext.getNodeName();
        
        i_configProperties = p_configProperties;
        
        i_alarmGroupConfig = p_alarmGroupConfig;
        i_alarmConfig = p_alarmConfig;
        i_watchdogActionGroupConfig = p_watchdogActionGroupConfig;
        
        i_watchdogPlatform = new WatchdogPlatform(
                i_logger, i_instrumentManager, i_configProperties);
        
        i_alarmProcessor = new AlarmProcessor(
                i_logger, i_instrumentManager, i_awdContext);
        i_alarmProcessor.init(i_configProperties);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_END, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }
    
    /**
     * Performs the actions defined in the supplied Watchdog Action Group.
     * @param p_watchdogActionGroup watchdog action group (actions to perform).
     */
    public void performActions(
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {
        WatchdogAction          l_actionARR[];
        int                     l_loop;
        
        // TODO 2 Handler null or prevent null (check config when loaded!!!!) 
        l_actionARR = p_watchdogActionGroup.getActionArray();
        for(l_loop = 0; l_loop < l_actionARR.length; l_loop++)
        {
            //if(p_processInformation.performAction(l_actionARR[l_loop]))
            //{
                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            "performAction returned true, so action allowed "+
                            "(max retries within interval not exceeded)");
                }
                
                if(l_actionARR[l_loop] instanceof RaiseAlarmWatchdogAction)
                {
                    performRaiseAlarmAction(
                            (RaiseAlarmWatchdogAction)l_actionARR[l_loop]);
    
                }
                else if(l_actionARR[l_loop] instanceof OsCommandWatchdogAction)
                {
                    performOsCommandAction(
                            (OsCommandWatchdogAction)l_actionARR[l_loop]);
    
                }
                /*
                 * Where get process info from?
                else if(l_actionARR[l_loop] instanceof RestartProcessWatchdogAction)
                {
                    performRestartAscsProcessAction(
                            p_processInformation,
                            (RestartProcessWatchdogAction)l_actionARR[l_loop]);
    
                }
                */
                else
                {
                    // TODO : Log
                    System.err.println("WatchdogProcessor: Unexpected watchdog action " +
                    l_actionARR[l_loop].getClass().getName() + ", ignoring.");
                }
            /*}
            else
            {
                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_VLOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            "performAction returned false, so action NOT allowed "+
                            "(max retries within interval exceeded!)");
                }
            }
            */
        }
        
    }
    
    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_performRaiseAlarmAction = "performRaiseAlarmAction";
    /**
     * Performs the supplied Raise Alarm Watchdog Action.
     * @param p_raiseAlarmWatchdogAction the Raise Alarm Watchdog Action.
     */
    private void performRaiseAlarmAction(
        RaiseAlarmWatchdogAction p_raiseAlarmWatchdogAction
    )
    {
        ConfiguredAlarm         l_alarm;
        AlarmInstanceConfig     l_alarmInstanceConfig;
        AlarmGroup              l_alarmGroup;
        AwdException            l_awdException;

        l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(
                p_raiseAlarmWatchdogAction.getAlarmInstanceName());
        l_alarmGroup = i_alarmGroupConfig.getAlarmGroup(
                p_raiseAlarmWatchdogAction.getAlarmGroupName());

        if(l_alarmInstanceConfig == null)
        {
            l_awdException = new AwdException(
                    C_CLASS_NAME,
                    C_METHOD_performRaiseAlarmAction,
                    22386,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().unknownAlarmInstanceName(
                            p_raiseAlarmWatchdogAction.getAlarmInstanceName())
                    );
            i_logger.logMessage(l_awdException);
            l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(
                    "ALARM." + l_awdException.getExceptionId());
        }
        if(l_alarmGroup == null)
        {
            l_awdException = new AwdException(
                    C_CLASS_NAME,
                    C_METHOD_performRaiseAlarmAction,
                    22387,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().unknownAlarmGroupName(
                            p_raiseAlarmWatchdogAction.getAlarmGroupName())
                    );
            i_logger.logMessage(l_awdException);
            l_alarmGroup = i_alarmGroupConfig.getAlarmGroup(
                    "AlarmGroupMain");
        }

        // If we still haven't got an instance or a group, ignore (we've
        // already logged an error.
        if((l_alarmInstanceConfig != null) && (l_alarmGroup != null))
        {
            l_alarm = new ConfiguredAlarm(
                    (String)null,
                    (String)null,
                    (Integer)null,
                    i_nodeName,
                    i_processName,
                    (Map)null,
                    l_alarmInstanceConfig);
            i_alarmProcessor.processAlarm(
                    l_alarm,
                    l_alarmGroup);
        }
    }

    /**
     * Performs the supplied Operating System (OS) Command Action.
     * @param p_osCommandWatchdogAction the Operating System (OS) Command Action.
     */
    private void performOsCommandAction(
        OsCommandWatchdogAction p_osCommandWatchdogAction
    )
    {
        i_watchdogPlatform.performOsCommand(
                p_osCommandWatchdogAction.getOsCommand());
    }
    
    
} // End of public class RswActionProcessor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////