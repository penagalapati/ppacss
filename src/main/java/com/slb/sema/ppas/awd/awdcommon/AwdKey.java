////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdKey.Java
//      DATE            :       24-Jul-2005
//      AUTHOR          :       MAGray
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Maker of AWD exception Keys.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
// 16/02/06 | M.Vonka    | Added errorInitWatchdogMonitor. | PpacLon#1987/7954
//----------+------------+---------------------------------+--------------------
// 11/09/06 | M.Vonka    | Added unexpectedInternal-       | PpaLon#2096/8999
//          |            | ServerError.                    |
//----------+------------+---------------------------------+--------------------
// 20/10/06 | M.Vonka    | Added monitorScriptOrCmdFailed. | PpacLon#2644/10214
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon;

import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.exceptions.ExceptionKeyMaker;

/** Maker of AWD exception Keys. */
public class AwdKey extends ExceptionKeyMaker
{
    /** Instance of the key maker. */
    private static AwdKey i_keyMaker = new AwdKey(); 

    /** Standard constructor - do nothing but stop others doing it. */
    private AwdKey()
    {
        // Do nothing.
    }

    /** Get the instance of this class.
     * 
     * @return Instance of this class.
     */
    public static AwdKey get()
    {
        return i_keyMaker;
    }

    /** Unknown alarm group.
    * 
    * @param p_groupName Name of the alarm group.
    * @return Key representing this exception.
    */
    public AwdExceptionKey unknownAlarmGroupName(String p_groupName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_ALARM_GROUP_NAME,
                                     new Object[]{p_groupName});
    }
    
    /** Unknown watchdog group.
     * 
     * @param p_watchdogGroup Name of the watchdog group.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedWatchDogGroup(String p_watchdogGroup)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_WATCHDOG_GROUP,
                                      new Object[]{p_watchdogGroup});
    }
     
    /** File not found.
     * 
     * @param p_fileName Name of the file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey fileNotFound(String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_FILE_NOT_FOUND, new Object[]{p_fileName});
    }
     
    /** Error reading next token.
     * 
     * @return Key representing this exception.
     */
    public AwdExceptionKey errorReadingNextToken()
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_ERROR_READING_NEXT_TOKEN, new Object[]{});
    }
      
    /** Error receiving alarm or event.
     * 
     * @return Key representing this exception.
     */
    public AwdExceptionKey errorReceivingAlarmOrEvent()
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_ERROR_RECEIVING_ALARM_OR_EVENT, new Object[]{});
    }
       
    /** Problem initialising e-mail client.
     * 
     * @return Key representing this exception.
     */
    public AwdExceptionKey problemInitialisingEmailClient()
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PROBLEM_INITIALISING_EMAIL_CLIENT, new Object[]{});
    }
        
    /** Unknown alarm instance name.
     * 
     * @param p_alarmName Name of an alarm instance that could not be found.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownAlarmInstanceName(String p_alarmName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_ALARM_INSTANCE_NAME, new Object[]{p_alarmName});
    }
        
    /** Unexpected token found.
     * 
     * @param p_actualToken   Actual token found.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @param p_expectedToken Expected token.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unexpectedTokenAtLine(String p_actualToken,
                                                 int    p_lineNumber,
                                                 String p_fileName,
                                                 String p_expectedToken)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNEXPECTED_TOKEN_AT_LINE,
                                   new Object[]{p_actualToken,
                                                new Integer(p_lineNumber),
                                                p_fileName,
                                                p_expectedToken});
    }
        
    /** Unexpected end of stream found.
     * 
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unexpectedEosAtLine(int    p_lineNumber,
                                               String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNEXPECTED_EOS_AT_LINE,
                                   new Object[]{new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown FTP append mode.
     * 
     * @param p_mode          FTP mode.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownFtpAppendModeAtLine(String p_mode,
                                                      int    p_lineNumber,
                                                      String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_FTP_APPEND_MODE_AT_LINE,
                                   new Object[]{p_mode,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown Destination type.
     * 
     * @param p_destType      Destination type.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownDestTypeAtLine(String p_destType,
                                                 int    p_lineNumber,
                                                 String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_DEST_TYPE_AT_LINE,
                                   new Object[]{p_destType,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Referred file not found.
     * 
     * @param p_referredFile  Name of referred file.
     * @param p_lineNumber    Line number in configuration file.
     * @param p_configName    Name of configuration file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey referredFileNotFoundAtLine(String p_referredFile,
                                                      int    p_lineNumber,
                                                      String p_configName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_REFERRED_FILE_NOT_FOUND_AT_LINE,
                                   new Object[]{p_referredFile,
                                                new Integer(p_lineNumber),
                                                p_configName});
    }
        
    /** IO Exception reading from file.
     * 
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey ioExceptionReadingFromFile(String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_IO_EXCEPTION_READING_FROM_FILE,
                                   new Object[]{p_fileName});
    }
        
    /** Expected something that was not there.
     * 
     * @param p_expected      Description of expected item.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey expectedOneOfAtLine(String p_expected,
                                               int    p_lineNumber,
                                               String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_EXPECTED_ONE_OF_AT_LINE,
                                   new Object[]{p_expected,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown format type.
     * 
     * @param p_formatType    Format type.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownFormatTypeAtLine(String p_formatType,
                                                  int    p_lineNumber,
                                                  String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_FORMAT_TYPE_AT_LINE,
                                   new Object[]{p_formatType,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown action type.
     * 
     * @param p_actionType    Action type.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownActionTypeAtLine(String p_actionType,
                                                   int    p_lineNumber,
                                                   String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_ACTION_TYPE_AT_LINE,
                                   new Object[]{p_actionType,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown destination name.
     * 
     * @param p_destName      Destination name.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedDestNameAtLine(String p_destName,
                                                   int    p_lineNumber,
                                                   String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_DEST_NAME_AT_LINE,
                                   new Object[]{p_destName,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown action group name.
     * 
     * @param p_actionGroup   Action group name.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedActionGroupNameAtLine(String p_actionGroup,
                                                          int    p_lineNumber,
                                                          String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_ACTION_GROUP_NAME_AT_LINE,
                                   new Object[]{p_actionGroup,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }

    /** Unknown Watchdog action group name.
     * 
     * @param p_actionGroup   Action group name.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedWdogActionGroupNameAtLine(String p_actionGroup,
                                                              int    p_lineNumber,
                                                              String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_WDOG_ACTION_GROUP_NAME_AT_LINE,
                                   new Object[]{p_actionGroup,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown destination group name.
     * 
     * @param p_destGroup     Destination name.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedDestGroupNameAtLine(String p_destGroup,
                                                        int    p_lineNumber,
                                                        String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_DEST_GROUP_NAME_AT_LINE,
                                   new Object[]{p_destGroup,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Unknown format name.
     * 
     * @param p_format        Format name.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedFormatNameAtLine(String p_format,
                                                     int    p_lineNumber,
                                                     String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_FORMAT_NAME_AT_LINE,
                                   new Object[]{p_format,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Previous configuration error.
     * 
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey previousConfigErrorAtLine(int    p_lineNumber,
                                                     String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PREVIOUS_CONFIG_ERROR_AT_LINE,
                                   new Object[]{new Integer(p_lineNumber),
                                                p_fileName});
    }

    /** Unknown process condition.
     * 
     * @param p_condition     Process Condition.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unknownProcessConditionAtLine(String p_condition,
                                                         int    p_lineNumber,
                                                         String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNKNOWN_PROCESS_CONDITION_AT_LINE,
                                   new Object[]{p_condition,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Undefined process condition.
     * 
     * @param p_condition     Process Condition.
     * @param p_lineNumber    Line number in file.
     * @param p_fileName      Name of file.
     * @return Key representing this exception.
     */
    public AwdExceptionKey undefinedProcessConditionAtLine(String p_condition,
                                                           int    p_lineNumber,
                                                           String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_UNDEFINED_PROCESS_CONDITION_AT_LINE,
                                   new Object[]{p_condition,
                                                new Integer(p_lineNumber),
                                                p_fileName});
    }
        
    /** Process initialisation failed.
     * 
     * @param p_process  Name of process.
     * @param p_reason   Reason for failure.
     * @return Key representing this exception.
     */
    public AwdExceptionKey processInitialisationFailure(String p_process, String p_reason)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PROCESS_INITIALISATION_FAILURE,
                                   new Object[]{p_process, p_reason});
    }
        
    /** Problem delivering an alarm via e-mail.
     * 
     * @param p_alarm       Formatted alarm.
     * @param p_destination Destination address.
     * @param p_port        Post number.
     * @param p_userName    User name.
     * @return Key representing this exception.
     */
    public AwdExceptionKey problemDeliveringAlarmViaEmail(String p_alarm,
                                                          String p_destination,
                                                          String p_port,
                                                          String p_userName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PROBLEM_DELIVERING_ALARM_VIA_EMAIL,
                                   new Object[]{p_alarm, p_destination, p_port, p_userName});
    }
        
    /** Problem delivering an alarm via FTP.
     * 
     * @param p_alarm       Formatted alarm.
     * @param p_node        Destination node.
     * @param p_userName    User name.
     * @return Key representing this exception.
     */
    public AwdExceptionKey problemDeliveringAlarmViaFtp(String p_alarm,
                                                        String p_node,
                                                        String p_userName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PROBLEM_DELIVERING_ALARM_VIA_FTP,
                                   new Object[]{p_alarm, p_node, p_userName});
    }
        
    /** Problem delivering an alarm to file.
     * 
     * @param p_alarm       Formatted alarm.
     * @param p_fileName    File name.
     * @return Key representing this exception.
     */
    public AwdExceptionKey problemDeliveringAlarmToFile(String p_alarm,
                                                        String p_fileName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_PROBLEM_DELIVERING_ALARM_TO_FILE,
                                   new Object[]{p_alarm, p_fileName});
    }
        
    /** Exception issuing a command.
     * 
     * @param p_command Command that was attempted.
     * @return Key representing this exception.
     */
    public AwdExceptionKey watchdogActionCommandException(String p_command)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_WATCHDOG_ACTION_COMMAND_EXCEPTION,
                                   new Object[]{p_command});
    }
        
    /** Error starting process.
     * 
     * @param p_process Process name.
     * @return Key representing this exception.
     */
    public AwdExceptionKey ascsStartProcessError(String p_process)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_ASCS_START_PROCESS_ERROR,
                                   new Object[]{p_process});
    }
    
    /**
     * Error initialising watchdog monitor for service ''{0}'', class ''{1}''.
     * @param p_serviceName     service name.
     * @param p_monitorClassName monitor class name.
     * @return Key representing this exception.
     */
    public AwdExceptionKey errorInitWatchdogMonitor(
            String              p_serviceName,
            String              p_monitorClassName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_ERROR_INIT_WATCHDOG_MONITOR,
                                   new Object[]{p_serviceName, p_monitorClassName});
    }

    /**
     * Error initializing audit logger ''{0}'', using normal logger instead.
     * @param p_loggerName      name of audit logger.
     * @return Key representing this exception.
     */
    public AwdExceptionKey errorInitAuditLogger(
        String                  p_loggerName)
    {
        return new AwdExceptionKey(AwdMsg.C_KEY_ERROR_INIT_AUDIT_LOGGER,
                                   new Object[]{p_loggerName});
    }
    
    /**
     * Monitor failed for service ''{0}'',  to server {1}, on port {2} -
     * reached {3} consecutive failures.
     * @param p_serviceName     name of service being monitored.
     * @param p_monitoredServerIpAddressOrName server IP address or name.
     * @param p_monitoredServerPortNumber port number.
     * @param p_failedAttemptCount number of consecutive attempts which have failed.
     * @return Key representing this exception.
     */
    public AwdExceptionKey monitorFailedForIpService(
        String                  p_serviceName,
        String                  p_monitoredServerIpAddressOrName,
        int                     p_monitoredServerPortNumber,
        int                     p_failedAttemptCount
        )
    {
        return new AwdExceptionKey(
                AwdMsg.C_KEY_MONITOR_FAILED_FOR_IP_SERVICE,
                new Object[]{p_serviceName, p_monitoredServerIpAddressOrName,
                new Integer(p_monitoredServerPortNumber), new Integer(p_failedAttemptCount)
                });
    }

    /**
     * Failed over service ''{0}'',  to server {1}.
     * @param p_serviceName     name of service failed over.
     * @param p_serverName      server name service failed over to.
     * @return Key representing this exception.
     */
    public AwdExceptionKey failedOverService(
        String                  p_serviceName,
        String                  p_serverName
        )
    {
        return new AwdExceptionKey(
                AwdMsg.C_KEY_FAILED_OVER_SERVICE,
                new Object[]{p_serviceName, p_serverName
                });
    }

    /**
     * Unexpected internal server error while ''{0}'', will attempt to continue.
     * @param p_actionBeingPerformedDescription description of the action
     *                          being performed by server at time of error..
     * @return Key representing this exception.
     */
    public AwdExceptionKey unexpectedInternalServerError(
        String                  p_actionBeingPerformedDescription
        )
    {
        return new AwdExceptionKey(
                AwdMsg.C_KEY_UNEXPECTED_INTERNAL_SERVER_ERROR,
                new Object[]{p_actionBeingPerformedDescription});
    }

    /**
     * Monitor ''{0}'' failed, - reached {1} consecutive failures. Script/cmd
     * {2}, cause(s) {3}.
     * @param p_monitorName     name of monitor.
     * @param p_failedAttemptCount number of consecutive attempts which have failed.
     * @param p_scriptOrCommand name of script or command.
     * @param p_causeDescription desciption of cause(s) of failure.
     * @return Key representing this exception.
     */
    public AwdExceptionKey monitorScriptOrCmdFailed(
        String                  p_monitorName,
        int                     p_failedAttemptCount,
        String                  p_scriptOrCommand,
        String                  p_causeDescription
        )
    {
        return new AwdExceptionKey(
                AwdMsg.C_KEY_MONITOR_SCRIPT_OR_CMD_FAILED,
                new Object[]{p_monitorName, new Integer(p_failedAttemptCount),
                p_scriptOrCommand, p_causeDescription
                });
    }


    /**
     * Unexpected action '{0}', ignoring. Could be that the action is not valid in this context.
     * @param p_actionName      name of action.
     * @return Key representing this exception.
     */
    public AwdExceptionKey unexpectedAction(
        String                  p_actionName
        )
    {
        return new AwdExceptionKey(
                AwdMsg.C_KEY_UNEXPECTED_ACTION,
                new Object[]{p_actionName}
                );
    }
    
    /** Inner class representing a key of an AWD Exception. */ 
    public class AwdExceptionKey extends ExceptionKey
    {
        /** Standard constructor.
         * 
         * @param p_exceptionKey Key of this exception.
         * @param p_parameters   Parameters for this instance of the exception.
         */
        private AwdExceptionKey(String p_exceptionKey, Object[] p_parameters)
        {
            super(p_exceptionKey, p_parameters);
        }
    }
    
} // End of public class AwdKey

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
