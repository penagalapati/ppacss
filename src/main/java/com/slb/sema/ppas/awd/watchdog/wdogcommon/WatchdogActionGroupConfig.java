/*
 * Created on 04-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.HashMap;

import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfig;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogActionGroupConfig extends AwdAbstractConfig
{

    private static final String C_CLASS_NAME = "WatchdogActionGroupConfig";

    private WatchdogActionGroup i_watchdogConfigCachedARR[];

    /**
     * Watchdog action group configuration - map of watchdog action group name to
     * <code>WatchdogActionGroup</code>.
     */
    private HashMap             i_watchdogActionGroupMap; 

    public WatchdogActionGroupConfig(
        Logger                       p_logger,
        AlarmConfig                  p_alarmConfig
        )
    {
        super(p_logger);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        init();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        
    }

    
    private void init()
    {
        i_watchdogActionGroupMap = new HashMap();
    }

    private static final String C_METHOD_addWatchdogActionGroup =
            "addWatchdogActionGroup";    
    /* package */ void addWatchdogActionGroup(
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addWatchdogActionGroup + 
                ": Adding mapping of watchdog action group " + p_watchdogActionGroup
                );

        i_watchdogActionGroupMap.put(
                p_watchdogActionGroup.getGroupName(), p_watchdogActionGroup);
    }

    private static final String C_METHOD_getWatchdogActionGroup =
            "getWatchdogActionGroup";    
    public WatchdogActionGroup getWatchdogActionGroup(
        String                  p_watchdogActionName
    )
    {
        WatchdogActionGroup        l_watchdogActionGroup;

        l_watchdogActionGroup = (WatchdogActionGroup)i_watchdogActionGroupMap.get(
                p_watchdogActionName);

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_END | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_getWatchdogActionGroup + 
                ": Returning watchdog action group condition " + l_watchdogActionGroup + " for name " +
                p_watchdogActionName
                );
                
        return l_watchdogActionGroup;
    }

    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("WatchdogActionGroupConfig=[watchdogGroupMap=[\n");
        l_sb.append(i_watchdogActionGroupMap);
        l_sb.append("]]");
        
        return l_sb.toString();
    }
    
}
