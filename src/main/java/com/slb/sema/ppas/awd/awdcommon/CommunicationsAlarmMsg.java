////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CommunicationsAlarmMsg.java
//      DATE            :       01-March-2006
//      AUTHOR          :       Rob O'Brien
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Class required to locate resource bundle for 
//                              OSS communication alarms.
//
////////////////////////////////////////////////////////////////////////////////
//          CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon;

import java.io.IOException;

import com.slb.sema.ppas.common.exceptions.XmlExceptionBundle;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class implements the resource bundle containing the text messages 
 * associated with parse exceptions and contains the keys into that resource 
 * bundle.
 */
public class CommunicationsAlarmMsg extends XmlExceptionBundle
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    /** 
     * Name of the message texts file containing the messages associated with 
     * all parse type exceptions.
     */
    public static final String C_PARSE_MSG_BASENAME = 
        CommunicationsAlarmMsg.class.getName();
    
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Debug methods. */
    private static final String C_CLASS_NAME = "CommunicationsAlarmMsg";

    /**
     * Define the name of the file which will contain all the message texts to be associated with parse 
     * exceptions.
     */
    private static final String C_COMMS_ALARM_MSG_FILE = "communications_alarm_msg.xml";
                                                    
    //------------------------------------------------------------------------
    // Public constructor
    //------------------------------------------------------------------------

    /**
     * Construct a communications alarm messages resource bundle object. 
     * This method opens a stream to the file containing the text of all the messages 
     * to be associated with parse exceptions.
     * 
     * @throws IOException If the message file cannot be read.
     */
    public CommunicationsAlarmMsg() throws IOException
    {
        super(CommunicationsAlarmMsg.class.getResourceAsStream(C_COMMS_ALARM_MSG_FILE));

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                    null, C_CLASS_NAME, 10010, this,
                    hashCode() + " Constructing " + C_CLASS_NAME);
        }

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                    null, C_CLASS_NAME, 10099, this,
                    hashCode() + " Constructed " + C_CLASS_NAME);
        }
    }
    
}  // End of class CommunicationsAlarmMsg

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
