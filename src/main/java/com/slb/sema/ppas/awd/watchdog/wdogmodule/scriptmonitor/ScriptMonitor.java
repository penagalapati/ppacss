////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScriptMonitor.java
//      DATE            :       12-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpacLon#2466
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Monitor which uses a configurable executable
//                              with optional interactive responses and
//                              output matching to perform a monitor.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.scriptmonitor;


import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.AbstractWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorException;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasRuntimeException;
import com.slb.sema.ppas.common.platform.Platform;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.tools.InteractiveResponses;
import com.slb.sema.ppas.util.tools.PasswordResponder;

/**
 * Monitor which uses a configurable executable
 * with optional interactive responses and
 * output matching to perform a monitor.
 * <p/>
 * The PasswordResponder CAN NOT be used by this class as it would be insecure.
 * <p/>
 * Supports the following configuration parameters:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>enabled</td>
 *     <td>Indicates if monitor is enabled.</td>
 *     <td>O</td>
 *     <td>true</td>
 *   </tr>
 *   <tr>
 *     <td>scriptPathAndFilename</td>
 *     <td>Path and filename of script/command to execute.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>response&lt;n&gt;.regex</td>
 *     <td>Regular expression to match aginst for response &lt;n&gt;, where n
 *         starts at 0 and is consecutive. Mandatory for each response.</td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>response&lt;n&gt;.class</td>
 *     <td>Name of class implementing responder for response &lt;n&gt;</td>
 *     <td>O</td>
 *     <td>com.slb.sema.ppas.util.tools.InteractiveResponses.RegexFixedResponder</td>
 *   </tr>
 *   <tr>
 *     <td>response&lt;n&gt;.response</td>
 *     <td>Fixed response to use for the com.slb.sema.ppas.util.tools.InteractiveResponses.RegexFixedResponder
 *         for response &lt;n&gt;</td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>response&lt;n&gt;.param&lt;m&gt;=&lt;paramName&gt;=[&quot;]&lt;paramValue&gt;[&quot;]</td>
 *     <td>Parameter value for parameter &lt;m&gt; for response &lt;n&gt; where m
 *         starts at 0 and is consecutive. For example<p/>
 *         monitorParam=response1.param1="response=connect sys/%password%@liza.ascsdg.site1\n".
 *         <p/>
 *         These will be passed as parameters to the responder instantiated for the
 *         response and will be specific to that class (so look at that classes
 *         Javadoc).
 *     </td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a &quot;ping&quot; fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for for script to complete before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 * </table>
 */
public class ScriptMonitor extends AbstractWatchdogMonitor
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "ScriptMonitor";

    /** Internal buffer size. Value is {@value}. */
    private static final int    C_BUFFER_SIZE = 8196;
    
    private Platform            i_platform;
    
    /** The configured script to execute. */
    private String              i_scriptPathAndFilename;

    /** The responses to use (if any). */
    private InteractiveResponses i_responses = new InteractiveResponses(
            InteractiveResponses.C_MODE_SEQUENTIAL);
    
    /**
     * Creates a new HTTP Ping Monitor.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public ScriptMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        super(p_logger, p_monitoredServiceName);
    }

    /**
     * Initialises the Script Monitor from the configuration parameters.
     * @param p_configParamMap  Map of parameter name (String) to parameter value (String).
     * @throws Exception        exception occurred during initialisation.
     */
    protected void doInit(Map p_configParamMap)
    throws Exception
    {
        String                  l_regexString;
        int                     l_responseCounter = 0;
        String                  l_className;
        String                  l_response;
        String                  l_responseParam;
        HashMap                 l_responseConfigMap;
        int                     l_responseParamCounter;
        int                     l_index;
        Class                   l_class;
        InteractiveResponses.Responder l_responder;
        
        i_platform = Platform.getThisPlatform(i_logger);
        i_scriptPathAndFilename = (String)p_configParamMap.get("scriptPathAndFilename");

        l_responseCounter = 1;
        while((l_regexString = (String)p_configParamMap.get("response" + l_responseCounter + ".regex")) != null)
        {
            l_className = (String)p_configParamMap.get("response" + l_responseCounter + ".class");
            if((l_className == null) || (PasswordResponder.class.getName().equals(l_className)))
            {
                // Default to a fixed responder
                // Do not allow password responder to be used as this would be insecure (e.g.
                // someone could configure the command to be ksh and then use the password
                // responder to echo the password).
                l_response = (String)p_configParamMap.get("response" + l_responseCounter + ".response");
                i_responses.addResponse(l_regexString, l_response, false);
            }
            else
            {
                // Read params for responder, create it and add it to responses
                l_responseConfigMap = new HashMap();
                l_responseParamCounter = 1;
                while((l_responseParam = (String)p_configParamMap.get(
                        "response" + l_responseCounter + ".param" + l_responseParamCounter)) != null)
                {
                    l_index = l_responseParam.indexOf("=");
                    if(l_index > 0)
                    {
                        // Valid param (like abc=def), split on = and store in map
                        l_responseConfigMap.put(
                                l_responseParam.substring(0, l_index),
                                l_responseParam.substring(l_index+1)
                                );                        
                    }
                    l_responseParamCounter++;
                }
                l_responseParamCounter--;
                l_responseConfigMap.put("regex", l_regexString);
                l_class = Class.forName(l_className);
                l_responder = (InteractiveResponses.Responder)l_class.newInstance();
                l_class.getMethod("init", new Class[] {Map.class}).invoke(
                        l_responder, new Object[] {l_responseConfigMap});
                i_responses.addResponse(l_responder);
            }
            l_responseCounter++;
        }
        l_responseCounter--;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performMonitorIteration = "performMonitorIteration";
    /**
     * Performs the actual processing of a single monitoring iteration. Typically Called
     * from within its own thread. 
     */
    public void performMonitorIteration()
    {
        int                     l_retryLimit = i_retryLimit;
        boolean                 l_success = false;
        AwdException            l_awdE;
        Exception               l_lastException = null;
        String                  l_output;
        Throwable               l_causeException;
        String                  l_causeString;
        
        do
        {
            try
            {
                l_retryLimit--;
                l_output = tryScript();
                l_success = true;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                        "SUCCEEDED: Script " + i_scriptPathAndFilename +
                        ", Output=" + l_output);
            }
            catch(Exception l_e)
            {
                l_lastException = l_e;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE | PpasDebug.C_ST_ERROR, C_CLASS_NAME, 12120, this,
                        "FAILED: Script " + i_scriptPathAndFilename + ", Exception=" + l_e);
                if(l_retryLimit > 0)
                {
                    // Retry will be attempted, pause configured interval between retries (if any)
                    if(i_retryMillis > 0)
                    {
                        if(PpasDebug.on) PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12130, this,
                                "Sleeping until next retry (" + i_retryMillis + " ms)");
                        synchronized(this)
                        {
                            try
                            {
                                wait(i_retryMillis);
                            }
                            catch(InterruptedException l_ie)
                            {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }
        while(!l_success && (l_retryLimit > 0) && !isStopping());

        if(!isStopping())
        {
            if(l_success)
            {
                passed(true);
            }
            else
            {
                l_causeException = l_lastException;
                l_causeString = l_causeException.toString();
                do
                {
                    
                    if(l_causeException instanceof PpasException)
                    {
                        l_causeException = ((PpasException)l_causeException).getSourceException();
                    }
                    else if(l_causeException instanceof PpasRuntimeException)
                    {
                        l_causeException = ((PpasRuntimeException)l_causeException).getSourceException();
                    }
                    else
                    {
                        l_causeException = l_causeException.getCause();
                    }
                    if(l_causeException != null)
                    {
                        l_causeString = l_causeString + ", cause= " + l_causeException.toString();
                    }
                } while (l_causeException != null);
                
                l_awdE = new WatchdogMonitorException(
                        C_CLASS_NAME, C_METHOD_performMonitorIteration, 11150,
                        this, (PpasRequest)null, (long)0, AwdKey.get().monitorScriptOrCmdFailed(
                                i_monitoredServiceName, i_retryLimit, i_scriptPathAndFilename,
                                l_causeString
                                ), l_lastException);
                i_logger.logMessage(l_awdE);
                passed(false);
            }
        }
            
    } // End of public method performMonitorIteration

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_tryScript = "tryScript";
    /**
     * Attempts a single actual ping.
     * @return Any output received.
     * @throws Exception        Exception occurred attempting Monitor. Monitor unsuccessful.
     *                          Not logged or output - caller must do this it required.
     */
    private String tryScript()
    throws Exception
    {

        //InteractiveResponses    l_responses;
        String                  l_output;

        i_responses.reset();
        l_output = i_platform.performExecutable(
                i_scriptPathAndFilename,
                i_readTimeoutMillis,
                i_responses);

        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12250, this,
                "Script execution returned " + l_output);

        if(!i_responses.finished())
        {
            throw new Exception("Script failed, did not complete successfully, output received = '"
                    + l_output + "'");
        }
        
        return l_output;
                   
    } // End of private method tryScript

} // End of public class ScriptMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
