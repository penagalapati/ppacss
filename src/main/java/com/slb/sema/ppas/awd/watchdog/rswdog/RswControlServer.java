////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswControlServer.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Rswdog Control Server - server for accepting
//                              and handling RSWP connections.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/06 | M.Vonka    | Added missing stop method so    | PpacLon#2633/9976
//          |            | can be stopped (and made start  |
//          |            | & stop safely recallable).      |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import com.slb.sema.ppas.common.smp.SimpleMessageProtocolMessageHandler;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Rswdog Control Server - server for accepting
 * and handling RSWP connections.
 * Handling is implemented by the supplied SMP Message Handler (
 * RSWP is implemented using SMP).
 * <p/>
 * Supports the following configuration parameters (all prefixed by
 * <code>com.slb.sema.ppas.awd.watchdog.rswdog.RswControlServer.</code>:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>localIpAddress</td>
 *     <td>Server domain name or IP address to listen on. "" (blank) or "*"
 *     to listend on all available IP addresses.</td>
 *     <td>0</td>
 *     <td>"" (blank)</td>
 *   </tr>
 *   <tr>
 *     <td>localIpPort</td>
 *     <td>Server port number to listen on.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>trace</td>
 *     <td>Trace enabled state to set on all newly accepted connections
 *     (true,on,1,y,yes,enabled,enable or anything else which is treated
 *     as false)</td>
 *     <td>O</td>
 *     <td>false</td>
 *   </tr>
 * </table>
 */
public class RswControlServer
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RswControlServer";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdog.rswdog.RswControlServer.";
    
    /** Logger to log any messages to. */
    private Logger              i_logger;
    
    /** Instrument manager to use. */
    private InstrumentManager   i_instrumentManager;
    
    /**
     * The RSWP Server IP address to listen on or <code>null</code> if listen
     * to all available addresses.
     */
    private String              i_localIpAddress;
    
    /** The RSWP Server Port number to listen on. */
    private int                 i_localIpPort;
        
    /** Trace set on each newly accepted connection. */
    private boolean             i_traceSet = false;

    /**
     * The RSWP Control Server End Point which actually implements the
     * listening for and accepting of new connections. It also passes
     * each new connection off to a new
     * {@link com.slb.sema.ppas.common.smp.SimpleMessageProtocolSocketEndPoint}
     * which in turn delegates the processing of received messages to
     * the {@link SimpleMessageProtocolMessageHandler}.
     */
    private RswControlServerEndPoint i_rswControlSEP;
    
    /**
     * The SMP Message Handler which handles received RSWP messages.
     */
    private SimpleMessageProtocolMessageHandler i_smpMessageHandler;

    /**
     * Creates a new RSW Control Server.
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager instrument manager to use.
     * @param p_configProperties config properties.
     * @param p_smpMessageHandler message handler to handle received RSWP messages.
     * @throws PpasConfigException configuration exception occurred.
     */
    public RswControlServer(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        PpasProperties          p_configProperties,
        SimpleMessageProtocolMessageHandler p_smpMessageHandler
        )
    throws PpasConfigException
    {
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_smpMessageHandler = p_smpMessageHandler;

        i_localIpAddress = p_configProperties.getTrimmedProperty(
                C_CFG_PROPERTY_BASE + "localIpAddress");
        if("".equals(i_localIpAddress) || "*".equals(i_localIpAddress))
        {
            i_localIpAddress = null;
        }
        i_localIpPort = Integer.parseInt(p_configProperties.getPortNumber(
                C_CFG_PROPERTY_BASE + "localIpPort"));
        i_traceSet = p_configProperties.getBooleanProperty(
                C_CFG_PROPERTY_BASE + "trace", false);
    }

    /**
     * Starts the RSW Control Server (starts listening for connections).
     */
    public synchronized void start()
    {
        if(i_rswControlSEP == null)
        {
            // TODO 3 Implement support for listening on configured IP address as well as all IP addressed?
            i_rswControlSEP = new RswControlServerEndPoint(
                    i_logger,
                    i_instrumentManager,
                    i_localIpPort,
                    0, // Maximum simultaneous connections. Should never be many so not set.
                    i_traceSet,
                    i_smpMessageHandler
                    );
            i_rswControlSEP.start();
        }
 
    }

    /**
     * Stops the RSW Control Server (stops listening for connections).
     */
    public synchronized void stop()
    {
        if(i_rswControlSEP != null)
        {
            try
            {
                i_rswControlSEP.stop();
            }
            catch(InterruptedException l_e)
            {
                // We tried. Best we can do is output and ignore.
                System.err.println(C_CLASS_NAME + "stop: Stop interrupted, ignoring.");
                l_e.printStackTrace();
            }
            finally
            {
               i_rswControlSEP = null;
            }
        }
    }

    /**
     * @return Returns the localIpAddress (can be <code>null</code> meaning
     * all local IP addresses).
     */
    public String getLocalIpAddress()
    {
        return i_localIpAddress;
    }
    
    /**
     * @return Returns the localIpAddress as a string which will either be
     * a DNS name (unlikely) or dot sepearted IP notation or '*' (meaning
     * all local IP addresses).
     */
    public String getLocalIpAddressLoggableString()
    {
        String                  l_localIpAddressLoggableString;
        
        l_localIpAddressLoggableString = i_localIpAddress;
        if(l_localIpAddressLoggableString == null ||
                l_localIpAddressLoggableString.equals(""))
        {
            l_localIpAddressLoggableString = "*";
        }
        return l_localIpAddressLoggableString;
    }

    /**
     * @return Returns the localIpPort.
     */
    public int getLocalIpPort()
    {
        return i_localIpPort;
    }
    
} // End of public class RswControlServer.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////