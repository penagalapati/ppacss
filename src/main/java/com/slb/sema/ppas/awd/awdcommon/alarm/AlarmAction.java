////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID    :        9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmAction.java
//      DATE            :       28-Oct-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Simple representation of the type of an Alarm Action.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

/**
 * Simple representation of the type of an Alarm Action.
 */
public abstract class AlarmAction
{

    /** FTP Alarm Action. Value is {@value}. */
    public static final String C_TYPE_FTP = "ACTION_FTP";

    /** Terminal Alarm Action. Value is {@value}. */
    public static final String C_TYPE_TERMINAL = "ACTION_TERMINAL";
    
    /** Log to log file Alarm Action. Value is {@value}. */
    public static final String C_TYPE_LOG_FILE = "ACTION_LOG_FILE";
    
    /** Email Alarm Action. Value is {@value}. */
    public static final String C_TYPE_EMAIL = "ACTION_EMAIL";

    /** The type of Alarm Action, one of the <code>C_TYPE_</code> constants. */
    private final String        i_typeName;

    /**
     * Creates an Alarm Action type.
     * @param p_typeName        the Alarm Action type - one of
     *                          the <code>C_TYPE_</code> constants.
     */
    protected AlarmAction(
        String                  p_typeName
    )
    {
        i_typeName = p_typeName;
    }

    /**
     * Returns the Alarm Action type - one of
     * the <code>C_TYPE_</code> constants.
     * @return The Alarm Action type.
     */
    public String getAlarmTypeName()
    {
        return i_typeName;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return("AlarmAction=[type=" + i_typeName + "]");
    }

} // End of public class AlarmAction

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////