/*
 * Created on 06-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdogserver;

import java.util.HashSet;
import java.util.Set;

import com.slb.sema.ppas.awd.watchdog.wdogcommon.ProcessCondition;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogGroup
{

    private static final WatchdogGroup.Entry C_EMPTY_ENTRY_ARRAY[] =
            new WatchdogGroup.Entry[0];
    
    /** Name of WatchdgoGroup */
    private String              i_groupName;

    /**
     * Configuration of group - set of <code>Entry</code>s each mapping
     * a monitored condition to a set of actions.
     */    
    private Set                 i_entrySet;

    private boolean             i_cacheInvalidated = true;
    private WatchdogGroup.Entry i_entryCachedARR[];

    public WatchdogGroup(
        String                  p_groupName
    )
    {
        super();
        init();
        i_groupName = p_groupName;
    }
    
    private void init()
    {
        i_entrySet = new HashSet();
    }
    
    public String getGroupName()
    {
        return i_groupName;
    }

    public void addEntry(
        ProcessCondition        p_condition,
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {
        i_entrySet.add(new Entry(
                p_condition,
                p_watchdogActionGroup));
    }
    
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("WatchdogGroup=[name=" + i_groupName + ",\n");
        l_sb.append(i_entrySet);
        l_sb.append("\n]");
        
        return l_sb.toString();
    }

    public synchronized WatchdogGroup.Entry[] getEntryArray()
    {

        if(i_cacheInvalidated)
        {
            i_entryCachedARR = (WatchdogGroup.Entry[])i_entrySet.
                    toArray(C_EMPTY_ENTRY_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_entryCachedARR;
    }
        
    public class Entry
    {
        private ProcessCondition i_processCondition;
        private WatchdogActionGroup i_watchdogActionGroup;

        public Entry(
            ProcessCondition    p_processCondition,
            WatchdogActionGroup p_watchdogActionGroup
        )
        {
            i_processCondition = p_processCondition;
            i_watchdogActionGroup = p_watchdogActionGroup;
        }
        
        public WatchdogActionGroup getActionGroup()
        {
            return i_watchdogActionGroup;
        }

        public ProcessCondition getProcessCondition()
        {
            return i_processCondition;
        }
        
        public String toString()
        {
            return ("Entry=[procCond=" + i_processCondition +
                    ", actionG=" + i_watchdogActionGroup + "]");
        }
        
    }

}
