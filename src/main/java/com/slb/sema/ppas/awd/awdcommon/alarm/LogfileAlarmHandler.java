////////////////////////////////////////////////////////////////////////////////
//        PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       LogfileAlarmHandler.java
//    DATE            :       08-Jun-2004
//    AUTHOR          :       Marek Vonka
//    REFERENCE       :       PpaLon#246
//
//    COPYRIGHT       :       ATOSORIGIN 2004
//
//    DESCRIPTION     :       This class implements a Logfile
//                            Alarm Handler.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//        |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * This class implements a Log File Alarm Handler which delivers (writes) alarms
 * to a log file.
 */
public class LogfileAlarmHandler
extends AbstractAsynchAlarmHandler
{

    /** Default max number of log file open attempts. Value is {@value}. */
    private static final int    C_DEFAULT_OPEN_RETRY_MAX = 20;

    /** Default millis between log file open attempts. Value is {@value}. */
    private static final int    C_DEFAULT_OPEN_RETRY_INTERVAL = 200;

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "LogfileAlarmHandler";

    /**
     * Maximum number of consecutive times to attempt to open log file before
     * giving up
     */
    private int                 i_openRetryMax = C_DEFAULT_OPEN_RETRY_MAX;
    
    /**
     * Millis between attempts to open log file.
     */
    private long                i_openRetryIntervalMillis = C_DEFAULT_OPEN_RETRY_INTERVAL;
        
    /**
     * Current number of consecutive times to attempt to open log file before
     * giving up
     */
    private int                 i_openRetryCount = 0;

    /**
     * Creates a new Log File Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     */
    public LogfileAlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
        )
    {
        super(  p_logger,
                p_configProperties,
                p_instrumentManager,
                p_awdContext,
                1,
                "LogflAH"
                );
    }

    /** Method name passed to middleware. Value is {@value}. */
    private static final String C_METHOD_doDeliverAsynchAlarm = "doDeliverAsynchAlarm";

    /**
     * {@inheritDoc}
     */
    protected void doDeliverAsynchAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
    )
    throws AlarmDeliveryException
    {

        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12010, this,
                "Delivering alarm " + p_formattedAlarm + " to destination " +
                p_destination);

        deliverLogfileAlarm(p_threadInstanceNum, p_alarm, p_formattedAlarm, p_destination);
    } // End of public method doDeliverAsynchAlarm
    
    /** Method name passed to middleware. Value is {@value}. */
    private static final String C_METHOD_deliverLogfileAlarm = "deliverLogfileAlarm";
    /**
     * Actually performs the alarm delivery to a log file. Synchronized to
     * ensure only one thread can be writing to log files (using this method).
     * This shouldn't really be necessary as should only be one background
     * thread.
     * 
     * @param p_threadInstanceNum the thread number calling this method (not
     *                          used).
     * @param p_alarm           the alarm.
     * @param p_formattedAlarm  the formatted alarm.
     * @param p_destination     the destination to deliver the alarm to.
     * @throws AlarmDeliveryException a problem occurred trying to deliver
     *                          the alarm, the alarm is unlikely to have been
     *                          delivered successfully.
     */
    private synchronized void deliverLogfileAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
    )
    throws AlarmDeliveryException
    {
        FileAlarmDestination    l_fileAlarmDestination;
        String                  l_pathAndFileName;
        FileWriter              l_fileWriter;
        Map                     l_map;
        AlarmDeliveryException  l_alarmE;
        boolean                 l_opened;
        
        l_fileAlarmDestination = (FileAlarmDestination)p_destination;
        
        l_pathAndFileName = l_fileAlarmDestination.getPathAndFilename();

        // Handle macros.
        l_map = p_alarm.getParameterMap();
        l_pathAndFileName = i_macroSubstitutor.expand(l_pathAndFileName, l_map);              

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11010, this,
                "Delivering alarm " + p_formattedAlarm + " to file " + 
                l_pathAndFileName + " (destination " +
                p_destination + ")"
                );

        l_fileWriter = null;        
        try
        {
            // Try to open file. Logic is such that if we hit max retries last time
            // we only try once, otherwise we try upto the maximum. This ensures a
            // permanent error with opening the file will only result in one retry
            // per log message (after 1 lot of maximum retries).
            l_opened = false;
            do
            {
                try
                {
                    l_fileWriter = new FileWriter(l_pathAndFileName, true /* append */);
                    l_opened = true;
                    i_openRetryCount = 0;
                }
                catch (IOException l_e)
                {
                    if(Debug.on) Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE | Debug.C_ST_ERROR,
                            C_CLASS_NAME, 10345, this,
                            "Exception opening log file " + l_pathAndFileName +
                            " : " + l_e
                            );                
                    i_openRetryCount++;
                    if(i_openRetryCount >= i_openRetryMax)
                    {
                        if(Debug.on) Debug.print(
                                Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                                Debug.C_ST_TRACE | Debug.C_ST_ERROR,
                                C_CLASS_NAME, 10347, this,
                                "Exceeded retry count!!! Outputing to System.err."
                                );                
                        throw l_e;
                    }
                }
            } while (!l_opened);
                        
            l_fileWriter.write(p_formattedAlarm);
        }
        catch (IOException l_e)
        {
            System.err.println("[LOG HANDLER] exception opening/writing to log file " +
                    l_pathAndFileName + "(" +
                    l_e + ") Alarm which could not be logged=\n" + p_formattedAlarm);
            l_alarmE = new AlarmDeliveryException(
                    C_CLASS_NAME,
                    C_METHOD_doDeliverAsynchAlarm,
                    10150,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().problemDeliveringAlarmToFile(p_formattedAlarm, l_pathAndFileName),
                    l_e);
            i_logger.logMessage(l_alarmE);
            throw l_alarmE;
        }
        finally
        {
            if(l_fileWriter != null)
            {
                try
                {
                    l_fileWriter.close();
                }
                catch (IOException l_e)
                {
                    l_alarmE = new AlarmDeliveryException(
                            C_CLASS_NAME,
                            C_METHOD_doDeliverAsynchAlarm,
                            10160,
                            this,
                            (PpasRequest)null,
                            0,
                            AwdKey.get().problemDeliveringAlarmToFile(p_formattedAlarm, l_pathAndFileName),
                            l_e);
                    i_logger.logMessage(l_alarmE);
                    throw l_alarmE;
                }                
            }
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_TRACE, C_CLASS_NAME, 10170, this,
                C_METHOD_doDeliverAsynchAlarm + ": Delivered alarm " +
                p_formattedAlarm + " to file " + 
                l_pathAndFileName + " (destination " +
                p_destination + ")"
                );                
        
    } // End of private method deliverLogfileAlarm

} // End of public class LogfileAlarmHandler

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////