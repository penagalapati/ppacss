/*
 * Created on 04-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.io.File;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.ConfigTokenizer;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogActionGroupConfigLoader extends AwdAbstractConfigLoader
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogActionGroupConfigLoader";

    public WatchdogActionGroupConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext
    )
    {
        super(p_logger, p_configProperties, p_awdContext);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    public WatchdogActionGroupConfig loadWatchdogActionGroupConfig(
        String                  p_configFilename,
        AlarmConfig             p_alarmConfig,
        AlarmGroupConfig        p_alarmGroupConfig,
        String                  p_processName
    )
    throws AwdConfigException
    {
        WatchdogActionGroupConfig l_config;
        
        l_config = new WatchdogActionGroupConfig(
                i_logger,
                p_alarmConfig);
        
        //loadTestConfig(l_config);
        loadLayeredConfig(p_configFilename, l_config, p_processName);
        
        return l_config;
    }

    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    private void loadLayeredConfig(
        String                  p_configFilename,
        WatchdogActionGroupConfig p_watchdogConfig,
        String                  p_processName
        )
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_watchdogConfig, p_processName);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_watchdogConfig, p_processName);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }

    private static final String C_METHOD_loadConfig = "loadConfig";
    private void loadConfig(
        String                  p_configFilename,
        WatchdogActionGroupConfig p_watchdogConfig,
        String                  p_processName
        )
    throws AwdConfigException
    {

        ConfigTokenizer         l_tokenizer;
        String                  l_token;        
        String                  l_actionGroupName;
        String                  l_actionType;
        int                     l_maxActionCount;
        String                  l_actionCountLifetime;
        long                    l_actionCountLifetimeMillis;
        String                  l_alarmName;
        String                  l_alarmGroupName;
        WatchdogActionGroup     l_actionGroup;
        WatchdogAction          l_action;
        String                  l_osCommand;
 
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        // Read action groups
        l_token = l_tokenizer.nextToken();
        while("watchdog.action.group.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_actionGroupName = l_tokenizer.getExpectedNextToken();
            l_actionGroup = new WatchdogActionGroup(l_actionGroupName);                    
            l_tokenizer.getExpectedNextToken("{");
            l_token = l_tokenizer.nextToken();
            while("type".equals(l_token))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_actionType = l_tokenizer.getExpectedNextToken();
                l_maxActionCount = l_tokenizer.getOptionalNextParameterInt(
                        "maxActionCount", -1);
                l_actionCountLifetime = l_tokenizer.getOptionalNextParameter(
                        "actionCountLifetime", "-1");
                l_actionCountLifetimeMillis = parseTimePeriod(l_actionCountLifetime);
                if("RAISE_ALARM".equals(l_actionType))
                {
                    l_alarmName = l_tokenizer.getExpectedNextParameter("alarmName");
                    l_alarmGroupName = l_tokenizer.getExpectedNextParameter("alarmGroup");
                    l_action = new RaiseAlarmWatchdogAction(
                            l_alarmGroupName,
                            l_alarmName,
                            l_maxActionCount,
                            l_actionCountLifetimeMillis);
                    l_actionGroup.addWatchdogAction(l_action);                    
                }
                else if("OS_COMMAND".equals(l_actionType))
                {
                    l_osCommand = l_tokenizer.getExpectedNextParameter("osCommand");
                    l_action = new OsCommandWatchdogAction(
                            l_osCommand,
                            l_maxActionCount,
                            l_actionCountLifetimeMillis);
                    l_actionGroup.addWatchdogAction(l_action);                    
                }
                else if("RESTART_ASCS_PROCESS".equals(l_actionType))
                {
                    l_action = new RestartProcessWatchdogAction(
                            l_maxActionCount,
                            l_actionCountLifetimeMillis);
                    l_actionGroup.addWatchdogAction(l_action);                    
                }
                else
                {
                    handleConfigException(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                            AwdKey.get().unknownActionTypeAtLine(
                                    l_actionType,
                                    l_tokenizer.getLineNumber(),
                                    p_configFilename));
                }
                l_token = l_tokenizer.nextToken();
            }
            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'}' or 'actionGroup'");
            }
            p_watchdogConfig.addWatchdogActionGroup(l_actionGroup);
            l_token = l_tokenizer.nextToken();
        }
        
        if(l_token != null)
        {
            handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                    l_token, l_tokenizer.getLineNumber(), 
                    p_configFilename, "'watchdog.action.group.name' or end of file");
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                "Leaving " + C_METHOD_loadConfig);
                
        return;
    }
    
    private long parseTimePeriod(String p_actionCountLifetime)
    {
        long                    l_timePeriodMillis = -1;
        String                  l_actionCountLifetime;
        
        l_actionCountLifetime = p_actionCountLifetime.toLowerCase();
        if(l_actionCountLifetime == null)
        {
            l_timePeriodMillis = -1;
        }
        else if("-1".equals(l_actionCountLifetime))
        {
            l_timePeriodMillis = -1;
        }
        else if(l_actionCountLifetime.endsWith("s"))
        {
            l_timePeriodMillis = Long.parseLong(
                   l_actionCountLifetime.substring(0, l_actionCountLifetime.length()-1)
                   ) * 1000;
        }
        else if(l_actionCountLifetime.endsWith("m"))
        {
            l_timePeriodMillis = Long.parseLong(
                   l_actionCountLifetime.substring(0, l_actionCountLifetime.length()-1)
                   ) * 1000 * 60;
        }
        else if(l_actionCountLifetime.endsWith("h"))
        {
            l_timePeriodMillis = Long.parseLong(
                   l_actionCountLifetime.substring(0, l_actionCountLifetime.length()-1)
                   ) * 1000 * 60 * 60;
        }
        else
        {
            // Assume seconds.
            l_timePeriodMillis = Long.parseLong(
                   l_actionCountLifetime
                   ) * 1000;
        }
        
        return l_timePeriodMillis;
    }

}
