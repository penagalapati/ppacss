////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogMonitorListener.java
//      DATE            :       23-Feb-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Interface for listening for watchdog monitor
//                              events.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

/**
 * Interface for listening for watchdog monitor
 * events.
 */
public interface WatchdogMonitorListener
{
    
    /**
     * Called every time the monitor detects a failure. That is, one or more
     * tests the monitor performs has failed.
     */
    public void failed();

    /**
     * Called every time the monitor detects a pass. That is, each time
     * one of the tests the monitor performs have passed AND monitor is
     * in the passed state - so where one test of a monitor is passing
     * but another test of the monitor is failing, generally passed will not
     * be called (until monitor enters the passed state)).
     */
    public void passed();

    /**
     * Called when the monitor has entered the failed state. That is, changed
     * from passed to failed state.
     */
    public void hasEnteredFailedState();
    
    /**
     * Called when the monitor has entered the passed state. That is, changed
     * from failed to passed state.
     */
    public void hasEnteredPassedState();

} // End of public class WatchdogMonitorListener.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////