/*
 * Created on 12-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class AlarmFormatter
{

    /**
     * Creates a new <code>AlarmFormatter</code>.
     * @param p_properties      properties for formatter.
     */
    public AlarmFormatter(UtilProperties p_properties)
    {
        super();
    }
    
    public abstract String format(
        Alarm                   p_alarm,
        AlarmFormat             p_alarmFormat
    );
     

}
