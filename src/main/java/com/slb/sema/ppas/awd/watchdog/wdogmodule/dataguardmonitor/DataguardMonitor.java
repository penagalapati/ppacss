////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DataguardMonitor.java
//      DATE            :       24-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpacLon#2466
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Datagaurd monitor which uses dgmgrl to
//                              monitor the health of dataguard.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.dataguardmonitor;


import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.AbstractWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorException;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasRuntimeException;
import com.slb.sema.ppas.common.platform.Platform;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.tools.InteractiveResponses;
import com.slb.sema.ppas.util.tools.PasswordResponder;

/**
 * Datagaurd monitor which uses dgmgrl to
 * monitor the health of dataguard.
 * <p/>
 * Supports the following configuration parameters:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>enabled</td>
 *     <td>Indicates if monitor is enabled.</td>
 *     <td>O</td>
 *     <td>true</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a &quot;ping&quot; fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for for script to complete before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 *   <tr>
 *     <td>encryptedPassword</td>
 *     <td>Encrypted password to use to connect. Manadatory if password not set.</td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>password</td>
 *     <td>Password to use to connect - THIS SHOULD ONLY BE USED IN AN EMERGENCY AS IT IS INSECURE.
 *          Mandatory if encryptedPassword not set.</td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>username</td>
 *     <td>Username to use to connect.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>serviceName</td>
 *     <td>Oracle service name to use to connect.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 * </table>
 */
public class DataguardMonitor extends AbstractWatchdogMonitor
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "DataguardMonitor";

    /** Internal buffer size. Value is {@value}. */
    private static final int    C_BUFFER_SIZE = 8196;
    
    private Platform            i_platform;
    
    /** The configured script to execute. */
    private String              i_commandPathAndFilename = "dgmgrl";

    /** The responses to use (if any). */
    private InteractiveResponses i_responses = new InteractiveResponses(
            InteractiveResponses.C_MODE_SEQUENTIAL);
    
    /**
     * Creates a new Dataguard Monitor.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public DataguardMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        super(p_logger, p_monitoredServiceName);
    }

    /**
     * Initialises the Datagaurd Monitor from the configuration parameters.
     * @param p_configParamMap  Map of parameter name (String) to parameter value (String).
     * @throws Exception        exception occurred during initialisation.
     */
    protected void doInit(Map p_configParamMap)
    throws Exception
    {
        PasswordResponder       l_passwordResponder;
        HashMap                 l_passwordResponderConfigMap;
        String                  l_paramValue;
        String                  l_username;
        String                  l_serviceName;
        
        
        l_passwordResponderConfigMap = new HashMap();
        l_paramValue = (String)p_configParamMap.get("password");
        if(l_paramValue != null)
        {
            l_passwordResponderConfigMap.put("password", l_paramValue);
        }
        l_paramValue = (String)p_configParamMap.get("encryptedPassword");
        if(l_paramValue != null)
        {
            l_passwordResponderConfigMap.put("encryptedPassword", l_paramValue);
        }
        l_username = (String)p_configParamMap.get("username");
        l_serviceName = (String)p_configParamMap.get("serviceName");
        l_passwordResponderConfigMap.put("response", "connect " + l_username +
                "/%password%@" + l_serviceName + "\n");
        l_passwordResponderConfigMap.put("regex", "DGMGRL\\>.*");
        
        l_passwordResponder = new PasswordResponder();
        l_passwordResponder.init(l_passwordResponderConfigMap);
        
        i_responses.addResponse(l_passwordResponder);
        i_responses.addResponse("DGMGRL\\>.*", "show configuration\n", false);
        i_responses.addResponse("Current status.*", false);
        i_responses.addResponse("SUCCESS.*", false);
        i_responses.addResponse("DGMGRL\\>.*", "quit\n", false);

        i_platform = Platform.getThisPlatform(i_logger);

    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performMonitorIteration = "performMonitorIteration";
    /**
     * Performs the actual processing of a single monitoring iteration. Typically Called
     * from within its own thread. 
     */
    public void performMonitorIteration()
    {
        int                     l_retryLimit = i_retryLimit;
        boolean                 l_success = false;
        AwdException            l_awdE;
        Exception               l_lastException = null;
        String                  l_output;
        Throwable               l_causeException;
        String                  l_causeString;
        
        do
        {
            try
            {
                l_retryLimit--;
                l_output = tryMonitor();
                l_success = true;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                        "SUCCEEDED: Command " + i_commandPathAndFilename +
                        ", Output=" + l_output);
            }
            catch(Exception l_e)
            {
                l_lastException = l_e;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE | PpasDebug.C_ST_ERROR, C_CLASS_NAME, 12120, this,
                        "FAILED: Command " + i_commandPathAndFilename + ", Exception=" + l_e);
                if(l_retryLimit > 0)
                {
                    // Retry will be attempted, pause configured interval between retries (if any)
                    if(i_retryMillis > 0)
                    {
                        if(PpasDebug.on) PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12130, this,
                                "Sleeping until next retry (" + i_retryMillis + " ms)");
                        synchronized(this)
                        {
                            try
                            {
                                wait(i_retryMillis);
                            }
                            catch(InterruptedException l_ie)
                            {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }
        while(!l_success && (l_retryLimit > 0) && !isStopping());

        if(!isStopping())
        {
            if(l_success)
            {
                passed(true);
            }
            else
            {
                l_causeException = l_lastException;
                l_causeString = l_causeException.toString();
                do
                {
                    
                    if(l_causeException instanceof PpasException)
                    {
                        l_causeException = ((PpasException)l_causeException).getSourceException();
                    }
                    else if(l_causeException instanceof PpasRuntimeException)
                    {
                        l_causeException = ((PpasRuntimeException)l_causeException).getSourceException();
                    }
                    else
                    {
                        l_causeException = l_causeException.getCause();
                    }
                    if(l_causeException != null)
                    {
                        l_causeString = l_causeString + ", cause= " + l_causeException.toString();
                    }
                } while (l_causeException != null);
                
                l_awdE = new WatchdogMonitorException(
                        C_CLASS_NAME, C_METHOD_performMonitorIteration, 11150,
                        this, (PpasRequest)null, (long)0, AwdKey.get().monitorScriptOrCmdFailed(
                                i_monitoredServiceName, i_retryLimit, i_commandPathAndFilename,
                                l_causeString
                                ), l_lastException);
                i_logger.logMessage(l_awdE);
                passed(false);
            }
        }
            
    } // End of public method performMonitorIteration

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_tryMonitor = "tryMonitor";
    /**
     * Attempts a single actual monitor.
     * @return Any output received.
     * @throws Exception        Exception occurred attempting Monitor. Monitor unsuccessful.
     *                          Not logged or output (except for low level
     *                          logging) - caller must do this it required.
     */
    private String tryMonitor()
    throws Exception
    {

        //InteractiveResponses    l_responses;
        String                  l_output;

        i_responses.reset();
        l_output = i_platform.performExecutable(
                i_commandPathAndFilename,
                i_readTimeoutMillis,
                i_responses);

        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12250, this,
                "Datagaurd monitor execution returned " + l_output);

        if(!i_responses.finished())
        {
            throw new Exception("Dataguard monitor failed, did not complete successfully, output received = '"
                    + l_output + "'");
        }
        
        return l_output;
                   
    } // End of private method tryMonitor

} // End of public class DataguardMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
