////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       UserAlarmDestination.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       User destination for alarms.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * User destination for alarms. Defines all attributes for user to be used
 * as a destination for alarms, in particular their username (for alarms to be
 * delivered to their terminal/console and email address for alarms to be
 * delivered by emial).
 */
public class UserAlarmDestination extends AlarmDestination
{
    
    /** User's first name (for info - optional). */
    private String              i_firstName;

    /** User's last name (for info - optional). */
    private String              i_lastName;

    /**
     * User's login user name - alarms can be configured to be sent to this.
     */
    private String              i_userName;
    
    /**
     * Users's email address - alarms can be configured to be sent to this.
     * Format is a simple SMTP string such as "someone@yahoo.co.uk".
     */
    private String              i_emailAddress;


    /**
     * Creates a new FTP Alarm Destination.
     * 
     * @param  p_userName       username of user.
     */    
    public UserAlarmDestination(
        String                  p_userName
    )
    {
        super(AlarmDestination.C_TYPE_USER);
        
        i_userName = p_userName;
    }

    /**
     * Creates a new FTP Alarm Destination.
     * 
     * @param  p_userName       username of user.
     * @param  p_emailAddress   email address of user.
     */
    public UserAlarmDestination(
        String                  p_userName,
        String                  p_emailAddress
    )
    {
        super(AlarmDestination.C_TYPE_USER);
        
        i_userName = p_userName;
        i_emailAddress = p_emailAddress;
    }

    /**
     * Creates a new FTP Alarm Destination.
     * 
     * @param  p_firstName      first name of user (for info - optional).
     * @param  p_lastName       last name of user (for info - optional)
     * @param  p_userName       username of user.
     * @param  p_emailAddress   email address of user.
     */
    public UserAlarmDestination(
        String                  p_firstName,
        String                  p_lastName,
        String                  p_userName,
        String                  p_emailAddress
    )
    {
        super(AlarmDestination.C_TYPE_USER);

        i_firstName = p_firstName;
        i_lastName = p_lastName;        
        i_userName = p_userName;
        i_emailAddress = p_emailAddress;
    }
    
    
    /**
     * Returns the user's user name.
     * @return                  The users's user name.
     */
    public String getUserName()
    {
        return i_userName;
    }

    /**
     * Returns the user's email address.
     * @return                  The user's email address.
     */
    public String getEmailAddress()
    {
        return i_emailAddress;
    }

    /**
     * Returns the user's first name.
     * @return                  The user's first name.
     */
    public String getFirstName()
    {
        return i_firstName;
    }

    /**
     * Returns the user's last name.
     * @return                  The user's last name.
     */
    public String getLastName()
    {
        return i_lastName;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( "UserAlarmDestination=[firstName=" + i_firstName +
                ", lastName=" + i_lastName +
                ", userName=" + i_userName +
                ", email=" + i_emailAddress +
                ", " + super.toString() + "]");
    }


} // End of public class UserAlarmDestination

////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////