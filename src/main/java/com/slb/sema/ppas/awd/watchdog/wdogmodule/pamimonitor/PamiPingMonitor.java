////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PamiPingMonitor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Monitor which uses a configurable "ping" request
//                              and configurable regex matched response over
//                              HTTP.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.pamimonitor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.AbstractWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Monitor which uses a configurable "ping" request
 * and configured regex matched response over
 * HTTP.
 * <p/>
 * Currently only supports HTTP/1.1 and non-persistant connections. A more complex
 * version could be added in future (perhaps using Apache's HttpClient).
 * <p/>
 * Supports the following configuration parameters:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>pamiServer</td>
 *     <td>Server domain name or IP address of PAMI server to monitor</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>pamiPort</td>
 *     <td>Server port number of PAMI server to monitor</td>
 *     <td>O</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>pamiRequestTerminationCharacter</td>
 *     <td>PAMI termination character to use to request &quot;ping&quot; - e.g. &quot;46&quot; (for &quot;.&quot;).
 *         Must be specifed as a decimal ASCII code - e.g. &quot;10&quot; for <LF>.
 *     </td>
 *     <td>O</td>
 *     <td>10</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a &quot;ping&quot; fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>connectTimeoutMillis</td>
 *     <td>Milliseconds to wait for connection to succeed before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for response to be received before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 * </table>
 */
public class PamiPingMonitor extends AbstractWatchdogMonitor
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "PamiPingMonitor";

    /** The default PAMI termination character (as a String). Value is {@value}. */
    private static final String C_PAMI_TERMINATION_CHAR_DEFAULT  = "\n";

    /** Internal buffer size. Value is {@value}. */
    private static final int    C_BUFFER_SIZE = 8196;
            
    /** The IP address or domain name of the HTTP server to ping. */
    private String              i_pamiServer = "localhost";
    
    /** The port number of the HTTP server to ping. */
    private int                 i_pamiPort;
    
    /**
     * The configured HTTP request line (first line) defining the "ping"
     * request to use. For example, "GET /my/ping/url HTTP/1.1"
     */
    private String              i_pamiRequestTerminationCharacter;
           
    /**
     * Creates a new HTTP Ping Monitor.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public PamiPingMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        super(p_logger, p_monitoredServiceName);
    }

    /**
     * Initialises the HTTP Ping Monitor from the configuration parameters.
     * @param p_configParamMap  Map of parameter name (String) to parameter value (String).
     * @throws Exception        exception occurred during initialisation.
     */
    protected void doInit(Map p_configParamMap)
    throws Exception
    {
        String                  l_pamiPortString;
        String                  l_pamiRequestTerminationCharacterString;
        
        i_pamiServer = (String)p_configParamMap.get("pamiServer");
        if(i_pamiServer == null || "".equals(i_pamiServer))
        {
            i_pamiServer = "localhost";
        }
        l_pamiPortString = (String)p_configParamMap.get("pamiPort");
        i_pamiPort = Integer.parseInt(l_pamiPortString);

        l_pamiRequestTerminationCharacterString = (String)p_configParamMap.get("pamiRequestTerminationCharacter");
        if(     l_pamiRequestTerminationCharacterString == null ||
                "".equals(l_pamiRequestTerminationCharacterString))
        {
            i_pamiRequestTerminationCharacter = C_PAMI_TERMINATION_CHAR_DEFAULT;
        }
        else
        {
            i_pamiRequestTerminationCharacter = new String(
                    new char[] {(char)(Integer.parseInt(l_pamiRequestTerminationCharacterString))}
                    );
        }

    }

    // TODO 3 Implement a thread which constantly reads from ping server so can detected dropped connection? But Apache will quickly closes idle connections.
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performMonitorIteration = "performMonitorIteration";
    /**
     * Performs the actual processing of a single monitoring iteration. Typically Called
     * from within its own thread. 
     */
    public void performMonitorIteration()
    {
        int                     l_retryLimit = i_retryLimit;
        boolean                 l_success = false;
        AwdException            l_awdE;
        Exception               l_lastException = null;
        
        do
        {
            try
            {
                l_retryLimit--;
                tryPing();
                l_success = true;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                        "SUCCEEDED: PAMI ping to service " + i_monitoredServiceName +
                        " [server=" + i_pamiServer +
                        ", port=" + i_pamiPort +
                        ", requestTerminationCharacter=" + i_pamiRequestTerminationCharacter +
                        "]");
            }
            catch(IOException l_e)
            {
                l_lastException = l_e;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE | PpasDebug.C_ST_ERROR, C_CLASS_NAME, 12120, this,
                        "FAILED: PAMI ping to service " + i_monitoredServiceName +
                        " [server=" + i_pamiServer +
                        ", port=" + i_pamiPort +
                        ", requestTerminationCharacter=" + i_pamiRequestTerminationCharacter +
                        "], reriesLeft=" + l_retryLimit +
                        ",exception=" + l_e);
                if(l_retryLimit > 0)
                {
                    // Retry will be attempted, pause configured interval between retries (if any)
                    if(i_retryMillis > 0)
                    {
                        if(PpasDebug.on) PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12130, this,
                                "Sleeping until next retry (" + i_retryMillis + " ms)");
                        synchronized(this)
                        {
                            try
                            {
                                wait(i_retryMillis);
                            }
                            catch(InterruptedException l_ie)
                            {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }
        while(!l_success && (l_retryLimit > 0) && !isStopping());

        if(!isStopping())
        {
            if(l_success)
            {
                passed(true);
            }
            else
            {
                l_awdE = new WatchdogMonitorException(
                        C_CLASS_NAME, C_METHOD_performMonitorIteration, 11150,
                        this, (PpasRequest)null, (long)0, AwdKey.get().monitorFailedForIpService(
                                i_monitoredServiceName, i_pamiServer,
                                i_pamiPort, i_retryLimit), l_lastException);
                i_logger.logMessage(l_awdE);
                passed(false);
            }
        }
            
    } // End of public method performMonitorIteration

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_tryPing = "tryPing";
    /**
     * Attempts a single actual ping. 
     * @throws IOException      IO exception occurred attempting Ping. Ping unsuccessful.
     *                          Not logged or output - caller must do this it required.
     */
    private void tryPing()
    throws IOException
    {
        Socket                  l_socket = null;
        InputStream             l_is;
        OutputStream            l_os;
        String                  l_pingRequest;
        byte                    l_pingRequestByteARR[];
        byte                    l_pingResponseByteARR[];
        String                  l_pingResponse;
        int                     l_totalReadCount;
        int                     l_readCount;

        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32100, this,
                "Entered " + C_METHOD_tryPing + ", will attempt ping to " +
                i_pamiServer + " on port " + i_pamiPort + " with timeout " + i_connectTimeoutMillis);
        try
        {
            // TODO 3 Use traceable socket (traceable socket would need updating)
            l_socket = new Socket();
            // Connect with timeout
            l_socket.connect(
                    new InetSocketAddress(i_pamiServer, i_pamiPort),
                    i_connectTimeoutMillis);
            l_socket.setSoTimeout(i_readTimeoutMillis);
            l_is = l_socket.getInputStream();
            l_os = l_socket.getOutputStream();
        
            l_pingRequest = i_pamiRequestTerminationCharacter;
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32120, this,
                    "Ping request=[" + l_pingRequest + "]");
            l_pingRequestByteARR = l_pingRequest.getBytes();
            l_os.write(l_pingRequestByteARR);
            
            l_pingResponseByteARR = new byte[C_BUFFER_SIZE];
            l_totalReadCount = 0;
            boolean l_eos = false;
            // TODO 3 Currently read timeout only works on each individual read, so for example could prevent a slow response from timing out

            while (!l_eos && l_totalReadCount <= 0)
            {
                l_readCount = l_is.read(l_pingResponseByteARR,
                                        l_totalReadCount,
                                        C_BUFFER_SIZE - l_totalReadCount);

                if (l_readCount < 0)
                {
                    l_eos = true;
                }
                else if (l_readCount > 0)
                {
                    l_totalReadCount += l_readCount;
                }
            }
            
            // We've either received something or reached EOS. Either way
            // check Ping response.
    
            l_socket.close();
            l_socket = null;
            l_is = null;
            l_os = null;
    
            l_pingResponse = new String(
                    l_pingResponseByteARR, 0, l_totalReadCount);
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32160, this,
                    "Ping response=[" + l_pingResponse + "]");
            if(!i_pamiRequestTerminationCharacter.equals(l_pingResponse))
            {
                // This is caught above and handled like any other IO exception
                throw new IOException("Failed ping, received [" +
                        l_pingResponse + "], which doesn't match [" +
                        i_pamiRequestTerminationCharacter + "]");
            }
        }
        finally
        {
            // Tidy up
            if(l_socket != null)
            {
                try
                {
                    l_socket.close();
                }
                catch (IOException l_e2)
                {
                    l_e2.printStackTrace();
                    // We tried - ignore and continue
                }
                l_socket = null;
                l_is = null;
                l_os = null;
            }
        }
                   
    } // End of private method tryPing

} // End of public class PamiPingMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
