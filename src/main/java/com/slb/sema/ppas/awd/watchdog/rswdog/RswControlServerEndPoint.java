////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswControlServerEndPoint.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       RSW Control Server End Point - server end point
//                              for RSWP connections which listens for new RSWP
//                              connections and hands them off to new socket end
//                              points to be processed.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import com.slb.sema.ppas.common.smp.SimpleMessageProtocolMessageHandler;
import com.slb.sema.ppas.common.smp.SimpleMessageProtocolServerEndPoint;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Rsw Control Server End Point - server end point
 * for RSWP connections which listens for new RSWP
 * connections and hands them off to new socket end
 * points to be processed.
 */
public class RswControlServerEndPoint extends SimpleMessageProtocolServerEndPoint
{

    /**
     * Creates a new RSW Control Server End Point.
     * 
     * @param p_logger          logger to any log messages to.
     * @param p_instrumentManager instrument manager to register with.
     * @param p_portNumber      port number to listen on for new connections.
     * @param p_maxConnections  maximum simultaneous connections before
     *                          rejecting new connections (by immediately
     *                          closing them). Zero means no maximum.
     * @param p_traceSet        trace state set on newly accepted sockets.
     * @param p_smpMessageHandler message handler to pass read messages to.
     */
    public RswControlServerEndPoint(
            Logger                  p_logger,
            InstrumentManager       p_instrumentManager,
            int                     p_portNumber,
            int                     p_maxConnections,
            boolean                 p_traceSet,
            SimpleMessageProtocolMessageHandler     p_smpMessageHandler
        )
    {
        super(
                p_logger,
                p_instrumentManager,
                p_portNumber,
                p_maxConnections,
                p_traceSet,
                "RSWP",
                p_smpMessageHandler
                );
    }

}  // End of public class RswControlServerEndPoint.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
