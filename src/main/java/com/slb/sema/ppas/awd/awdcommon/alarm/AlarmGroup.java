/*
 * Created on 28-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashSet;
import java.util.Set;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmGroup
{
    private static final Entry  C_EMPTY_ENTRY_ARRAY[] = new Entry[0];
    
    private String              i_name;
    //private AlarmActionGroup    i_alarmActionGroup;
    //private AlarmDestinationGroup i_alarmDestinationGroup;
    private Set                 i_entrySet;
    private Entry               i_entryCachedARR[];
    private boolean             i_cacheInvalidated;

    public AlarmGroup(
        String                  p_name
    )
    {
        i_name = p_name;
        init();
    }
    
    public void init()
    {
        i_entrySet = new HashSet();
        i_cacheInvalidated= true;
    }
    
    public String getName()
    {
        return(i_name);
    }
    
    public void add(
        AlarmActionGroup        p_actionGroup,
        AlarmDestinationGroup   p_destinationGroup,
        AlarmFormat             p_alarmFormat
    )
    {
        i_entrySet.add(new Entry(
                p_destinationGroup, p_actionGroup, p_alarmFormat));
        i_cacheInvalidated = true;
    }
    
    public Entry[] getEntryArray()
    {
        if(i_cacheInvalidated)
        {
            i_entryCachedARR = (Entry [])i_entrySet.toArray(C_EMPTY_ENTRY_ARRAY);
            i_cacheInvalidated = false;
        }
        
        return i_entryCachedARR;
    }
    
    public String toString()
    {
        StringBuffer            l_sb;
        Entry                   l_entryARR[];
        int                     l_loop;
        
        l_sb = new StringBuffer(500);
        
        l_sb.append("AlarmGroup=[name=");
        l_sb.append(i_name);
        l_sb.append(",\nentry[]=[\n");
        l_entryARR = getEntryArray();
        for(l_loop = 0; l_loop < l_entryARR.length; l_loop++)
        {
            l_sb.append("    ");
            l_sb.append(l_entryARR[l_loop].toString());
            l_sb.append("\n");
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

    /**
     * Links one Action Group to one Destination Group. 
     */    
    public class Entry
    {

        public AlarmActionGroup i_alarmActionGroup;
        public AlarmDestinationGroup i_alarmDesintationGroup;
        public AlarmFormat      i_alarmFormat;

        public Entry(
            AlarmDestinationGroup p_desinationGroup,
            AlarmActionGroup    p_actionGroup,
            AlarmFormat         p_alarmFormat
        )
        {
            i_alarmDesintationGroup = p_desinationGroup;
            i_alarmActionGroup = p_actionGroup;
            i_alarmFormat = p_alarmFormat;
        }
        
        public String toString()
        {
            return ("Entry=[actionG=" + i_alarmActionGroup +
                    ", destG=" + i_alarmDesintationGroup);
        }
                
    } // End of public inenr class Entry
    
} // End of public class AlarmGroup
