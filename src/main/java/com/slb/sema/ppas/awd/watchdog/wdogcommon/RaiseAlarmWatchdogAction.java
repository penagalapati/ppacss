////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RaiseAlarmWatchdogAction.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This defines a Watchdog Action to raise an alarm.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.wdogcommon;

/**
 * This defines a Watchdog Action to raise an alarm. It defines :
 * <ul>
 *     <li>Alarm Group Name (defining the actions, format and destinations
 *         for the alarm).</li>
 *     <li>Alarm Instance Name defining the configured details of the alarm.
 *         </li>
 * </ul>
 */
public class RaiseAlarmWatchdogAction extends WatchdogAction
{
    /**
     * The Alarm Group Name defining the actions, format and destinations
     * for the alarm.
     */
    private String              i_alarmGroupName;
    
    /** The Alarm Instance Name defining the configured details of the alarm. */
    private String              i_alarmInstanceName;
    
    /**
     * Creates a new Raise Alarm Watchdog Action.
     * 
     * @param p_alarmGroupName  the Alarm Group Name defining the actions,
     *                          format and destinations for the alarm.
     * @param p_alarmInstanceName the Alarm Instance Name defining the
     *                          configured details of the alarm.
     */
    public RaiseAlarmWatchdogAction(
        String                  p_alarmGroupName,
        String                  p_alarmInstanceName
    )
    {
        super();
        i_alarmGroupName = p_alarmGroupName;
        i_alarmInstanceName = p_alarmInstanceName;
    }

    /**
     * Creates a new Raise Alarm Watchdog Action.
     * 
     * @param p_alarmGroupName  the Alarm Group Name defining the actions,
     *                          format and destinations for the alarm.
     * @param p_alarmInstanceName the Alarm Instance Name defining the
     *                          configured details of the alarm.
     * @param p_maxActionCount  maximum times action will be performed within
     *                          the specified time.
     * @param p_actionCountLifetimeMillis the specified time actions are
     *                          counted for to determin if the maximum action
     *                          count has been reached.
     */
    public RaiseAlarmWatchdogAction(
        String                  p_alarmGroupName,
        String                  p_alarmInstanceName,
        int                     p_maxActionCount,
        long                    p_actionCountLifetimeMillis
    )
    {
        super(p_maxActionCount, p_actionCountLifetimeMillis);
        i_alarmGroupName = p_alarmGroupName;
        i_alarmInstanceName = p_alarmInstanceName;
    }

    /**
     * Returns the Alarm Group Name.
     * 
     * @return The Alarm Group Name.
     */
    public String getAlarmGroupName()
    {
        return i_alarmGroupName;
    }

    /**
     * Returns the Alarm Instance Name.
     * 
     * @return The Alarm Instance Name.
     */
    public String getAlarmInstanceName()
    {
        return i_alarmInstanceName;
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return "RaiseAlarmWatchdogAction=[alarmGroupName=" +
                i_alarmGroupName + ", alarmInstanceName=" +
                i_alarmInstanceName + "," + super.toString() + "]";
    }

} // End of public class RaiseAlarmWatchdogAction

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////