////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdAbstractServer.java
//      DATE            :       16-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Abstract, generic base Server class for all AWD
//                              server processes (such as Alarm Server and
//                              Watchdog Server).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
// 16/02/06 | M.Vonka    | Minor logging and javadoc       | PpacLon#1987/7954
//          |            | improvements.                   |
//----------+------------+---------------------------------+--------------------
// 03/08/07 | E.Clayton  | Added checks on whether objects | PpacLon#3192/11929
//          |            | are null into stop method.      |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon;

import java.util.HashMap;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.lang.DelayedSystemExitDaemon;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.net.BasicManagerCommandProcessor;
import com.slb.sema.ppas.util.net.BasicManagerControllable;
import com.slb.sema.ppas.util.net.BasicManagerServerEndPoint;

/**
 * Abstract, generic base Server class for all AWD server
 * processes (such as Alarm Server and Watchdog Server).
 */
public abstract class AwdAbstractServer
implements InstrumentedObjectInterface, BasicManagerControllable
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AwdAbstractServer";

    /**
     * The base of all configuration property names. As this class is abstract
     * it uses the actual class name (at runtime) as the property base.
     */
    private final String        i_propertyBase;

    /**
     * Basic Manager Sever End Point which implements basic management such as
     * stopping the Server.
     */   
    private BasicManagerServerEndPoint i_basicManagerServerEndPoint;

    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;
    
    /** Instrument Set for this instrumented object. */
    protected InstrumentSet     i_instrumentSet;
    
    
    /** <code>Logger</code> to log to. */
    protected Logger            i_logger;

    /** AWD Context for this Server. */
    protected AwdContext        i_awdContext;

    /** Configuration properties for this Server. */
    protected PpasProperties    i_configProperties;

    /** Process name of this Server. */
    protected String            i_processName;
    
    /**
     * Creates a new AwdAbsteractServer.
     */
    public AwdAbstractServer()
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE, PpasDebug.C_ST_CONFIN_START,
                    C_CLASS_NAME, 10000, this,
                    "Constructing " + C_CLASS_NAME);
        }

        i_propertyBase = this.getClass().getName();
                
        i_processName = System.getProperty("ascs.procName");
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE, PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME,10090, this,
                    "Constructed " + C_CLASS_NAME);
        }

        return;

    }

    /**
     * Performs any tidy up required (and allows debugging trace of objects
     * lifecycle).
     * @throws Throwable        a Throwable occurred.
     */
    public void finalize()
    throws Throwable
    {

        try
        {
            if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_MWARE, PpasDebug.C_ST_CONFIN_START,
                    C_CLASS_NAME, 10020, this,
                    "Finalizing " + C_CLASS_NAME);

            if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_MWARE, PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME, 10025, this,
                    "Finalized " + C_CLASS_NAME);
        }
        finally
        {
            super.finalize();
        }

    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialises this Server, including creating of AWD Context, loading of
     * configuration, creation of Instrument Manager, initalisation of Context etc.
     * @throws PpasException    exception occurred.
     */
    public final void init()
    throws PpasException
    {
        init(new AwdContext());
    }
    
    /**
     * Initialises this Server, including loading of
     * configuration, creation of Instrument Manager, initalisation of Context etc.
     * @param p_context         AWD context object to use (or creates a new one if
     *                          <code>null</code>).
     * @throws PpasException    exception occurred.
     */
    public final void init(
        AwdContext              p_context
        )
    throws PpasException
    {
        AwdException            l_awdE;
        PpasProperties          l_ppasProperties;

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_START,
                                          C_CLASS_NAME, 10030, this,
                                          "Entered " + C_METHOD_init);
        try
        {
    
            if(p_context == null)
            {
                i_awdContext = new AwdContext();
            }
            else
            {
                i_awdContext = p_context;
            }
            i_instrumentManager = i_awdContext.getInstrumentManager();

            i_instrumentSet = new InstrumentSet("root");

            l_ppasProperties = new PpasProperties(i_instrumentManager);
            l_ppasProperties.loadLayeredProperties();
            i_configProperties = l_ppasProperties;
                        
            i_awdContext.init(l_ppasProperties);
            
            i_logger = i_awdContext.getLogger();
            
            init(l_ppasProperties);
            
            if(i_instrumentManager != null)
            {
                i_instrumentManager.registerObject(this);
            }
            else
            {
                i_instrumentSet.disable();
            }
    
            i_logger.logMessage(new LoggableEvent(
                    "Process initialisation successfully completed for process " +
                            i_processName + " at " + DatePatch.getDateTimeNow(),
                    LoggableInterface.C_SEVERITY_SUCCESS));
        }
        catch (Exception l_e)
        {
            l_awdE = new AwdException(
                    C_CLASS_NAME,
                    C_METHOD_init,
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().processInitialisationFailure(i_processName, l_e.getMessage()),
                    l_e);
            if(i_logger != null)
            {
                i_logger.logMessage(l_awdE);
            }
            else
            {
                l_awdE.printStackTrace();
            }
            throw l_awdE;
        }

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_END,
                                          C_CLASS_NAME, 10035, this,
                                          "Leaving " + C_METHOD_init);
        return;
    }
    
    /**
     * Initialises the Server from the supplied Properties.
     * @param p_configProperties configuration properties.
     * @throws PpasException    exception occurred.
     */
    private void init(PpasProperties p_configProperties)
    throws PpasException
    {
        String                  l_propertyName;
        String                  l_configParam;
        PpasException           l_ppasException;
        int                     l_managerPortNumber;
        boolean                 l_managerPortTrace = false;

        
        // Process manager port number (e.g. for shutdown).
        l_propertyName = "com.slb.sema.ppas.managerPortNumber";
        l_configParam = p_configProperties.getPortNumber(l_propertyName);
        if (l_configParam == null)
        {
            l_ppasException = new PpasConfigException(
                    C_CLASS_NAME, C_METHOD_init, 10070, this,
                    null, 0,
                    ConfigKey.get().missingConfigData(l_propertyName, C_CLASS_NAME));

            i_logger.logMessage(l_ppasException);

            throw l_ppasException;
        }

        l_managerPortNumber = Integer.parseInt(l_configParam);

        l_propertyName = i_propertyBase + ".managerPortTrace";
        l_managerPortTrace = p_configProperties.getBooleanProperty(l_propertyName, false);

        i_basicManagerServerEndPoint = new BasicManagerServerEndPoint(
                    i_logger,
                    i_instrumentManager,
                    l_managerPortNumber,
                    0,                          // Timeout for sockets.
                    l_managerPortTrace,
                    new MyBasicManagerCP(this)
                    );
        
        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10030, this,
                                          "Calling (usually overriden) method doInit");
        doInit();
        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10035, this,
                                          "Returned from (usually overriden) method doInit");

        return;
    }

    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_start = "start";
    /**
     * Starts the server. Derived classes can NOT override this method, instead they can
     * override the empty method doStart.
     */
    public final void start()
    {

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_START,
                                          C_CLASS_NAME, 10030, this,
                                          "Entered " + C_METHOD_start);

        i_basicManagerServerEndPoint.start();

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10030, this,
                                          "Calling (usually overriden) method doStart");
        doStart();
        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10030, this,
                                          "Returned from (usually overriden) method doStart");

        i_logger.logMessage(new LoggableEvent(
                "Process startup succeeded for process " + i_processName + " at " +
                        DatePatch.getDateTimeNow(),
                LoggableInterface.C_SEVERITY_SUCCESS));

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_END,
                                          C_CLASS_NAME, 10030, this,
                                          "Leaving " + C_METHOD_start);
    }


    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_stop = "stop";
    /**
     * Stops the server. Derived classes can NOT override this method, instead they can
     * override the empty method doStop.
     */
    public final void stop()
    {
        DelayedSystemExitDaemon l_delayedSystemExitDaemon;

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_START,
                                          C_CLASS_NAME, 10030, this,
                                          "Entering " + C_METHOD_stop);

        //
        // Use a delayed system exit daemon to ensure server stops.
        //
        l_delayedSystemExitDaemon = new DelayedSystemExitDaemon(
                0,        // Flags
                i_logger,       // Logger
                20000           // Delay in millis.
                );
        l_delayedSystemExitDaemon.start();

        //
        // Request stop on all running objects.
        //
        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10030, this,
                                          "Calling (usually overriden) method doStop");
        doStop();
        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10030, this,
                                          "Returned from (usually overriden) method doStop");

        // Stop Basic Manager Server End Point
        try
        {
            if (i_basicManagerServerEndPoint != null) i_basicManagerServerEndPoint.stop();
        }
        catch (InterruptedException l_e)
        {
            // We tried! Ignore and carry on.
        }

        // Destroy context.
        if (i_awdContext != null) i_awdContext.destroy();

        if (i_logger != null) i_logger.logMessage(new LoggableEvent(
                "Process stop completed for process " + i_processName + " at " +
                        DatePatch.getDateTimeNow(),
                LoggableInterface.C_SEVERITY_SUCCESS));

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                          PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                                          PpasDebug.C_ST_END,
                                          C_CLASS_NAME, 10030, this,
                                          "Leaving " + C_METHOD_stop);
        return;
    }
    
    /**
     * Returns this instrumented object's instrument set.
     * @return This instrumented object's instrument set.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }
    
    /**
     * Returns this instrumented object's name.
     * @return This instrumented object's name.
     */
    public String getName()
    {
        return i_processName;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( C_CLASS_NAME + "=[processName=" + i_processName + 
                ", propertyBase=" + i_propertyBase + "]");
    }

    /**
     * Returns a verbose String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A verbose respresentation suitable for debugging/trace.
     */
    public String toVerboseString()
    {
        return( C_CLASS_NAME + "=[name=" + i_processName + ",configProps=" +
                i_configProperties + "]");
    }
        
    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_doInit = "doInit";
    /** 
     * Hook for derived classes to perform initialisation activities. This default implementation
     * does nothing.
     * @throws PpasException    exception occurred.
     */
    protected void doInit()
    throws PpasException
    {

        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_END,
                C_CLASS_NAME, 10030, this,
                C_METHOD_doInit + ": not overridden by derived class - no class " +
                        "specific processing required.");

        return;
    }

    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_doStart = "doStart";
    /** 
     * Hook for derived classes to perform start activities. This default implementation
     * does nothing.
     */
    protected void doStart()
    {
        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_END,
                C_CLASS_NAME, 10030, this,
                C_METHOD_doStart + ": not overridden by derived class - no class " +
                        "specific processing required.");
        return;
    }

    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_doStop = "doStop";
    /** 
     * Hook for derived classes to perform stop activities. This default implementation
     * does nothing.
     */
    protected void doStop()
    {
        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_END,
                C_CLASS_NAME, 10030, this,
                C_METHOD_doStop + ": not overridden by derived class - no class " +
                        "specific processing required.");
        return;
    }

    /** Used in calles to middleware. Value is {@value}.*/
    private static final String C_METHOD_processManagementCommand = "processManagementCommand";
    /**
     * Derived classes can override this to process management commands. This
     * default implementation simply returns <code>null</code>. Derived
     * implementations should return <code>null</code> if they can't process
     * the command.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String). 
     * @return Result string or <code>null</code>.
     */
    protected String processManagementCommand(
        String                  p_command,
        HashMap                 p_parametersHM
        )
    {

        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_END,
                C_CLASS_NAME, 10030, this,
                C_METHOD_processManagementCommand + ": not overridden by derived class - no class " +
                        "specific processing required. Command=" + p_command + ",parameters=" +
                        p_parametersHM);
        return null;
    }
    
    /**
     * Private inner class which extends basic manager command processor so
     * that it calls protected {@link #processManagementCommand(String, HashMap) } to
     * allow any process specific commands to be handled. 
     */
    private class MyBasicManagerCP extends BasicManagerCommandProcessor
    {
        /**
         * Constructor.
         * @param p_controllable an instance of <code>BasicManagerControllable</code>.
         */
        public MyBasicManagerCP(BasicManagerControllable p_controllable)
        {
            super(p_controllable);
        }

        /** 
         * Overridden hook method implementing process-specif commands.
         * Should return null if p_command did not match a specified command.
         * @param p_command command
         * @param p_parametersHM parameters
         * @return text string, or null if no match command was found
         */
        public String processCommand(String p_command, HashMap p_parametersHM)
        {
            return processManagementCommand(p_command, p_parametersHM);
        }

    }
    
} // End of public class AwdAbstractServer

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
