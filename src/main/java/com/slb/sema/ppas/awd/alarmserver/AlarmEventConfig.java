////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmEventConfig.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Alarm event configuration defining which events
//                              should generate an alarm, which alarm and where
//                              to send the alarm.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/08/07 | E Dangoor  | Add methods to remove config    | PpacLon#3319/12021
//          |            | data                            |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmInstanceConfig;
import com.slb.sema.ppas.awd.watchdogserver.WatchdogProcessConfig;
import com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Alarm event configuration defining which events should
 * generate an alarm, which alarm and where to send
 * the alarm.
 */
public class AlarmEventConfig
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AlarmEventConfig";
    
    /** Event field id for event name field. Value is {@value}. */
    public static final int     C_MATCH_FIELD_ID_EVENT_NAME = 1;

    /** Event field id for event text field. Value is {@value}. */
    public static final int     C_MATCH_FIELD_ID_EVENT_TEXT = 2;

    /** Event field id for originating node field. Value is {@value}. */
    public static final int     C_MATCH_FIELD_ID_ORIGNATING_NODE = 3;

    /** Event field id for originating process field. Value is {@value}. */
    public static final int     C_MATCH_FIELD_ID_ORIGINATING_PROCESS = 4;

    /** Map of event field names to field ids (id as Integer). */
    public static final Map     C_EVENT_FIELD_NAME_TO_ID_MAP = new HashMap();

    /** Valid event field names. Value is {@value}. */
    public static final String  C_EVENT_FIELD_VALID_NAMES =
                                        "eventName, eventText, originatingNode" +
                                        "originatingProcess";
    
    static
    {
        C_EVENT_FIELD_NAME_TO_ID_MAP.put(
                "eventName", new Integer(AlarmEventConfig.
                C_MATCH_FIELD_ID_EVENT_NAME));

        C_EVENT_FIELD_NAME_TO_ID_MAP.put(
                "eventText", new Integer(AlarmEventConfig.
                C_MATCH_FIELD_ID_EVENT_TEXT));

        C_EVENT_FIELD_NAME_TO_ID_MAP.put(
                "originatingNode", new Integer(AlarmEventConfig.
                C_MATCH_FIELD_ID_ORIGNATING_NODE));

        C_EVENT_FIELD_NAME_TO_ID_MAP.put(
                "originatingProcess", new Integer(AlarmEventConfig.
                C_MATCH_FIELD_ID_ORIGINATING_PROCESS));
    }

    /** Empty event regex config array. */
    private static final EventRegexConfig C_EMPTY_EVENT_REGEX_CONFIG_ARRAY[] =
            new EventRegexConfig[0];

    /**
     * Alarm group config. Each configured event references one alarm group in
     * this alarm group config. The group defines how and where the alarm
     * for the event is delivered. 
     */
    private AlarmGroupConfig    i_alarmGroupConfig;

    /**
     * Alarm config. Each configured event references one alarm instance
     * configuration in this alarm config. The alarm instance defines the
     * parameters of the alarm (e.g. TXF parameters).
     */
    private AlarmConfig         i_alarmConfig;

    /**
     * Map of event name (<code>String</code>) to
     * {@link AlarmEventConfig.EventNameEntry}.
     */
    private Map                 i_eventNameConfigMap; 

    /**
     * Set of regular expression entries ( 
     * {@link AlarmEventConfig.EventRegexConfig}.
     */
    private Set                 i_eventRegexConfigSet;
    
    /**
     * Cached array view of {@link #i_eventRegexConfigSet}. For iteration
     * efficiency.
     */
    private EventRegexConfig    i_eventRegexConfigCachedARR[];
    
    /**
     * Indicates whether cache (@link #i_eventRegexConfigCachedARR) is out of
     * date and needs to be recreated before being used again. 
     */
    private boolean             i_cacheInvalidated = true;

    /**
     * Creates a new Alarm Event Config. Entries add will have their alarm instance
     * config reference looked up in a
     * @param p_alarmConfig     alarm config to get alarm instance from.
     * @param p_alarmGroupConfig alarm group config to get alarm group from.
     */
    public AlarmEventConfig(
        AlarmConfig                  p_alarmConfig,
        AlarmGroupConfig             p_alarmGroupConfig
        )
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        i_alarmConfig = p_alarmConfig;
        i_alarmGroupConfig = p_alarmGroupConfig;
        init();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        
    }

    /** Initialises the alarm event config. */
    public void init()
    {
        i_eventNameConfigMap = new HashMap();
        i_eventRegexConfigSet = new HashSet();
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_removeEventNameMapping =
            "removeEventNameMapping";    
    /**
     * Removes an event name mapping.
     * @param p_eventName       name of event to unmap.
     */
    public void removeEventNameMapping(
        String                  p_eventName
    )
    {
        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_removeEventNameMapping + 
                ": Removing mapping of event " + p_eventName
                );

        i_eventNameConfigMap.remove(p_eventName);

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW,
                    Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_END,
                    C_CLASS_NAME, 10190, this,
                    "Leaving " + C_METHOD_removeEventNameMapping
                    );
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addEventNameMapping =
            "addEventNameMapping";    
    /**
     * Creates and adds a new event name mapping.
     * @param p_eventName       name of event to map.
     * @param p_alarmName       name of alarm to map event to.
     * @param p_alarmGroupName  name of alarm group to process alarm.
     */
    public void addEventNameMapping(
        String                  p_eventName,
        String                  p_alarmName,
        String                  p_alarmGroupName
    )
    {

        AlarmGroup              l_group;
        AlarmInstanceConfig     l_alarmInstanceConfig;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addEventNameMapping + 
                ": Adding mapping of event " + p_eventName + " to group " +
                        p_alarmGroupName
                );

        l_group = i_alarmGroupConfig.getAlarmGroup(p_alarmGroupName);
        l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(p_alarmName);
        if((l_group != null) && (l_alarmInstanceConfig != null))
        {
            i_eventNameConfigMap.put(p_eventName,
                    new EventNameEntry(p_eventName, l_group, l_alarmInstanceConfig));

            if(Debug.on) Debug.print(
                    Debug.C_LVL_MODERATE, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 11020, this,
                    C_METHOD_addEventNameMapping + ": Added mapping");
        }
        else
        {
            // TODO:medium - <<Throw exception????>>
            if(l_group != null)
            {
                if(Debug.on) Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE | Debug.C_ST_ERROR, C_CLASS_NAME, 11050, this,
                        C_METHOD_addEventNameMapping +
                        ": Failed to add mapping - no group called " + p_alarmGroupName);
            }
            else
            {
                if(Debug.on) Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE | Debug.C_ST_ERROR, C_CLASS_NAME, 11050, this,
                        C_METHOD_addEventNameMapping +
                        ": Failed to add mapping - no alarm called " + p_alarmName);
            }
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_removeEventRegexMapping =
            "removeEventRegexMapping";    
    /**
     * Removes an event name mapping.
     * @param p_fieldIdToMatchAgainst event field id to regex against.
     * @param p_pattern         regex pattern to match event field against.
     */
    public synchronized void removeEventRegexMapping(
        int                     p_fieldIdToMatchAgainst,
        Pattern                 p_pattern
    )
    {
        Iterator    l_entries;
        int         l_entryFieldId;
        String      l_entryPattern;

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_removeEventRegexMapping + 
                ": Removing mapping of event " + p_fieldIdToMatchAgainst +
                " with regex " + p_pattern
                );

        l_entries = i_eventRegexConfigSet.iterator();
        while (l_entries.hasNext())
        {
            l_entryFieldId = ((EventRegexConfig)l_entries.next()).getFieldId();
            l_entryPattern = ((EventRegexConfig)l_entries.next()).getPattern().pattern();
            if (l_entryFieldId == p_fieldIdToMatchAgainst &&
                    p_pattern.pattern().equals(l_entryPattern))
            {
                l_entries.remove();
            }
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW,
                    Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_END,
                    C_CLASS_NAME, 10190, this,
                    "Leaving " + C_METHOD_removeEventRegexMapping
                    );
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addEventRegexMapping =
            "addEventRegexMapping";    
    /**
     * Creates and adds a new event name mapping.
     * @param p_fieldIdToMatchAgainst event field id to regex against.
     * @param p_pattern         regex pattern to match event field against.
     * @param p_alarmName       name of alarm to map matching events to.
     * @param p_alarmGroupName  name of alarm group to process alarm.
     */
    public synchronized void addEventRegexMapping(
        int                     p_fieldIdToMatchAgainst,
        Pattern                 p_pattern,
        String                  p_alarmName,
        String                  p_alarmGroupName
    )
    {

        AlarmGroup              l_group;
        AlarmInstanceConfig     l_alarmInstanceConfig;

        l_group = i_alarmGroupConfig.getAlarmGroup(p_alarmGroupName);
        l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(p_alarmName);
        if((l_group != null) && (l_alarmInstanceConfig != null))
        {
            i_eventRegexConfigSet.add(
                    new EventRegexConfig(
                            p_fieldIdToMatchAgainst, p_pattern,
                            l_group, l_alarmInstanceConfig));
            if(Debug.on) Debug.print(
                    Debug.C_LVL_MODERATE, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 11020, this,
                    C_METHOD_addEventRegexMapping + ": Added mapping");
            if(!i_cacheInvalidated)
            {
                i_cacheInvalidated = true;
     
                if(Debug.on) Debug.print(
                       Debug.C_LVL_MODERATE,
                       Debug.C_APP_INIT | Debug.C_APP_MWARE,
                       Debug.C_ST_TRACE,
                       C_CLASS_NAME, 11190, this,
                       C_METHOD_addEventRegexMapping + 
                       ": Regex Config Cache Array invalidated."
                       );
            }
        }
        else
        {
            // TODO:medium - <<Throw exception????>>
            if(l_group == null)
            {
                if(Debug.on) Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE | Debug.C_ST_ERROR, C_CLASS_NAME, 11050, this,
                        C_METHOD_addEventNameMapping +
                        ": Failed to add regex mapping - no group called " + p_alarmGroupName);
            }
            else
            {
                if(Debug.on) Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE | Debug.C_ST_ERROR, C_CLASS_NAME, 11050, this,
                        C_METHOD_addEventNameMapping +
                        ": Failed to add regex mapping - no alarm called " + p_alarmName);
            }
        }
        
    }

    /**
     * Returns the first mating Entry found. Checks event names first, then regexs.
     * 
     * @param p_alarmEvent      event to find first matching entry against.
     * @return                  The first matching Entry.
     */
    public Entry getMatchingAlarmEntry(
         AlarmEvent             p_alarmEvent
    )
    {
        Entry                   l_entry;
        EventRegexConfig        l_eventRegexConfigCachedARR[];
        int                     l_loop;
        Pattern                 l_pattern;
        Matcher                 l_matcher;
        String                  l_text;
        int                     l_fieldId;
        
        l_entry = (Entry)i_eventNameConfigMap.get(
                p_alarmEvent.getEventName());
        
        if(l_entry == null)
        {
            // Try regexs if any configured. Store local reference to Array
            // incase cached array changes during processing.
            l_eventRegexConfigCachedARR = getEventRegexMappingArray();
            for(l_loop = 0; (l_entry == null) && (l_loop < l_eventRegexConfigCachedARR.length); l_loop++)
            {
                l_pattern = l_eventRegexConfigCachedARR[l_loop].getPattern();
                l_fieldId = l_eventRegexConfigCachedARR[l_loop].getFieldId();
                switch(l_fieldId)
                {
                    case AlarmEventConfig.C_MATCH_FIELD_ID_EVENT_NAME:
                    l_text = p_alarmEvent.getEventName();
                    break;

                    case AlarmEventConfig.C_MATCH_FIELD_ID_EVENT_TEXT:
                    l_text = p_alarmEvent.getEventText();
                    break;

                    case AlarmEventConfig.C_MATCH_FIELD_ID_ORIGNATING_NODE:
                    l_text = p_alarmEvent.getOriginatingNodeName();
                    break;

                    case AlarmEventConfig.C_MATCH_FIELD_ID_ORIGINATING_PROCESS:
                    l_text = p_alarmEvent.getOriginatingProcessName();
                    break;
                    
                    default:
                    // Should never happen.
                    // TODO: Log software error and ignore.
                    l_text = null;
                }
                l_matcher = l_pattern.matcher(l_text);
                if(l_matcher.matches())
                {
                    // Matched!
                    l_entry = l_eventRegexConfigCachedARR[l_loop];
                }
            }
        }
        
        return l_entry;
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("AlarmEventConfig=[eventMap=[\n");
        l_sb.append(i_eventNameConfigMap != null ? i_eventNameConfigMap.toString() : "null");
        l_sb.append("\n]");
        l_sb.append("EventRegexConfigSet=[\n");
        l_sb.append(i_eventRegexConfigSet != null ? i_eventRegexConfigSet.toString() : "null");
        l_sb.append("\n]");
        
        return l_sb.toString();
    }
    
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getEventRegexMappingArray =
            "getEventRegexMappingArray";    

    /**
     * Returns current array of regex mappings. If the cache is invalid, it
     * recreates the cache and returns this.
     * 
     * @return current array of regex mappings.
     */
    private synchronized EventRegexConfig[] getEventRegexMappingArray()
    {
        if(i_cacheInvalidated)
        {
            i_eventRegexConfigCachedARR = 
                (EventRegexConfig[])i_eventRegexConfigSet.toArray(
                    C_EMPTY_EVENT_REGEX_CONFIG_ARRAY);
            i_cacheInvalidated = false;

            if(Debug.on) Debug.print(
                   Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                   Debug.C_ST_TRACE, C_CLASS_NAME, 11190, this,
                   C_METHOD_getEventRegexMappingArray + 
                   ": Regex Config Cache Array recreated and so now valid.");
        }
        return i_eventRegexConfigCachedARR;
    }

/*   
    private static class ConfigSnapshot
    {
        private EventNameConfig i_eventNameConfig; 
        private EventNameRegexConfig i_eventNameRegexConfig; 
        private EventTextRegexConfig i_eventTextRegexConfig; 
    }
*/


    /**
     * Abstract mapping entry inner class, super class for all mapping entries.
     */
    public abstract class Entry
    {
        /** Alarm group entry maps to. */
        private AlarmGroup      i_alarmGroup;
        
        /** Alarm instance entry maps to. */
        private AlarmInstanceConfig i_alarmInstanceConfig;
        
        /**
         * Creates a new entry.
         * @param p_alarmGroup  alarm group entry maps to.
         * @param p_alarmInstanceConfig alarm instance entry maps to.
         */
        private Entry(
            AlarmGroup          p_alarmGroup,
            AlarmInstanceConfig p_alarmInstanceConfig
        )
        {
            i_alarmGroup = p_alarmGroup;
            i_alarmInstanceConfig = p_alarmInstanceConfig;
        }

        /**
         * Returns the alarm group the entry maps to.
         * @return Returns the alarm group the entry maps to.
         */
        public AlarmGroup       getAlarmGroup()
        {
            return i_alarmGroup;
        }

        /**
         * Returns the alarm instance the entry maps to.
         * @return Returns the alarm instance the entry maps to.
         */
        public AlarmInstanceConfig getAlarmInstanceConfig()
        {
            return i_alarmInstanceConfig;
        }
    
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer            l_sb;
            
            l_sb = new StringBuffer(1000);
            l_sb.append("i_alarmGroup=[");
            l_sb.append(i_alarmGroup != null ? i_alarmGroup.toString() : "null");
            l_sb.append("]\n");
            l_sb.append("i_alarmInstanceConfig=[");
            l_sb.append(i_alarmInstanceConfig != null ? i_alarmInstanceConfig.toString() : "null");
            l_sb.append("]\n");
            
            return l_sb.toString();
        }
    }

    /**
     * Event name entry mapping inner class. Maps event name to alarm group
     * and alarm instance.
     */
    private class EventNameEntry
    extends Entry
    {
        /** Event name mapped. */
        private String          i_eventName;
        
        /**
         * Creates a new Event Name Entry.
         * @param p_eventName   event name to map.
         * @param p_alarmGroup  alarm group mapped to.
         * @param p_alarmInstanceConfig alarm instance mapped to.
         */
        private EventNameEntry(
            String              p_eventName,
            AlarmGroup          p_alarmGroup,
            AlarmInstanceConfig p_alarmInstanceConfig
        )
        {
            super(p_alarmGroup, p_alarmInstanceConfig);
            i_eventName = p_eventName;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer            l_sb;
            
            l_sb = new StringBuffer(1000);
            l_sb.append("i_eventName=[");
            l_sb.append(i_eventName);
            l_sb.append("]\n");
            l_sb.append("Entry inner class=[");
            l_sb.append(super.toString());
            l_sb.append("]\n");
            
            return l_sb.toString();
        }
        
    }

    /**
     * Event field regex entry inner class. Defines a regex on an event field
     * and the alarm group and alarm instance to map matching events to.
     */
    private class EventRegexConfig
    extends Entry 
    {
        /** Event field number to match regex against. */
        private int             i_fieldIdToMatchAgainst;
        
        /** Regex pattern to match against field. */
        private Pattern         i_pattern;
        
        /**
         * Creates a new Event Regex Entry.
         * @param p_fieldIdToMatchAgainst event field id to match regex against.
         * @param p_pattern     regex pattern to match against field.
         * @param p_alarmGroup  alarm group mapped to.
         * @param p_alarmInstanceConfig alarm instance mapped to.
         */
        private EventRegexConfig(
            int                 p_fieldIdToMatchAgainst,
            Pattern             p_pattern,
            AlarmGroup          p_alarmGroup,
            AlarmInstanceConfig p_alarmInstanceConfig
        )
        {
            super(p_alarmGroup, p_alarmInstanceConfig);

            i_fieldIdToMatchAgainst = p_fieldIdToMatchAgainst;
            i_pattern = p_pattern;
        }
        
        /**
         * Returns the event field id matched against.
         * @return Returns the event field id matched against.
         */
        public int getFieldId()
        {
            return i_fieldIdToMatchAgainst;
        }
        
        /**
         * Returns the regex pattern matched aginst the field. 
         * @return Returns the regex pattern matched aginst the field. 
         */
        public Pattern getPattern()
        {
            return i_pattern;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer            l_sb;
            
            l_sb = new StringBuffer(1000);
            l_sb.append("i_fieldIdToMatchAgainst=[");
            l_sb.append(i_fieldIdToMatchAgainst);
            l_sb.append("]\n");
            l_sb.append("i_pattern=[");
            l_sb.append(i_pattern != null ? i_pattern.toString() : "null");
            l_sb.append("]\n");
            l_sb.append("Entry inner class=[");
            l_sb.append(super.toString());
            l_sb.append("]\n");
            
            return l_sb.toString();
        }
    }

        
} // End of public class AlarmEventConfig

////////////////////////////////////////////////////////////////////////////////
//End of file
////////////////////////////////////////////////////////////////////////////////