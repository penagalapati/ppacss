////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdException.Java
//      DATE            :       17-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Implements Awd common exceptions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.ExceptionMsgFormat;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasExceptionTypeConstants;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Implements a class of related exceptions all of which are specific to AWD.
 */
public class AwdException extends PpasException
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AwdException";

    /** Unique identifier for this class of exception. Value is {@value}. */
    private static final String C_EXCEPTION_ID = PpasExceptionTypeConstants.C_EXCEPTION_ID_AWD;

    //------------------------------------------------------------------------
    // Public constructors.
    //------------------------------------------------------------------------

    /**
     * Construct an AwdException. Calls the constructor of the parent
     * PpasException class to create this exception providing parameters
     * specific to this type of exception.
     * @param p_callingClass The name of the class invoking this constructor.
     * @param p_callingMethod The name of the method from which this constructor was invoked.
     * @param p_stmtNo Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject The object from which this constructor was 
     *                        invoked ... null if the constructor was invoked from a static method.
     * @param p_request       Session creating this exception.
     * @param p_flags  Bitmask flag. 
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     */
    public AwdException(String       p_callingClass,
                               String       p_callingMethod,
                               int          p_stmtNo,
                               Object       p_callingObject,
                               PpasRequest  p_request,
                               long         p_flags,
                               AwdKey.AwdExceptionKey p_exceptionKey)
    {
        this(   p_callingClass,
                p_callingMethod,
                p_stmtNo,
                p_callingObject,
                p_request,
                p_flags,
                p_exceptionKey,
                null);
    }

    /**
     * Construct an AwdException with support for exception chaining. That is,
     * provide a parameter for the exception which gave rise to this exception.
     * Calls the constructor of the parent PpasException class to create this
     * exception providing parameters specific to this type of exception.
     * @param p_callingClass The name of the class invoking this constructor.
     * @param p_callingMethod The name of the method from which this constructor was invoked.
     * @param p_stmtNo Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject The object from which this constructor was 
     *                        invoked ... null if the constructor was invoked from a static method.
     * @param p_request       Session creating this exception.
     * @param p_flags  Bitmask flag. 
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     * @param p_sourceException An earlier exception which gave rise to this exception.
     */
    public AwdException(String       p_callingClass,
                               String       p_callingMethod,
                               int          p_stmtNo,
                               Object       p_callingObject,
                               PpasRequest  p_request,
                               long         p_flags,
                               AwdKey.AwdExceptionKey p_exceptionKey,
                               Exception    p_sourceException) 
    {
        super(  p_callingClass,
                p_callingMethod,
                p_stmtNo,
                p_callingObject,
                p_request,
                p_flags,
                C_EXCEPTION_ID,
                AwdMsg.C_CONFIG_MSG_BASENAME,
                p_exceptionKey,
                ExceptionMsgFormat.C_CONFIG_MSG_FMT_KEY,
                p_sourceException);

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                    p_request,
                    C_CLASS_NAME, 10010, this,
                    " Constructing " + C_CLASS_NAME + ":" + p_exceptionKey);
        }

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE,
                    p_request,
                    C_CLASS_NAME, 10090, this,
                    " Constructed " + C_CLASS_NAME);
        }
    }
    
} // End of public class AwdException

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////