////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogServer.java
//      DATE            :       04-Dec-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       WatchdogServer including bootstrap for it.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/13/06 | M.Vonka    | Refactored slightly to create   | PpacLon#2644/10214
//          |            | and use new WatchdogContext.    |
//          |            | Added missing Javadoc.          |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdogserver;

import com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogContext;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.util.support.Debug;

/**
 * WatchdogServer including bootstrap for it.
 */
public class WatchdogServer extends AwdAbstractServer
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogServer";

    /** Context running in. */
    private WatchdogContext        i_watchdogContext;

    /** Watchdog processor which performs the watchdog processing. */
    private WatchdogProcessor      i_watchdogProcessor;
    
    /** Watchdog monitor manager which manages configured (pluggable) monitors. */
    private WatchdogMonitorManager i_watchdogMonitorManager;
    
    /**
     * Creates a new WatchdogServer. 
     */
    public WatchdogServer()
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Performs the Watchdog Server specific initialisation. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()} method.
     * @throws PpasException    exception occurred during initialization.
     */    
    public void doInit()
    throws PpasException
    {
        i_watchdogContext = (WatchdogContext)i_awdContext;
        
        i_watchdogProcessor = new WatchdogProcessor(
                i_logger,
                i_instrumentManager,
                i_watchdogContext
                );

        i_watchdogProcessor.init(i_configProperties);
        
        i_watchdogMonitorManager = new WatchdogMonitorManager(
                i_logger,
                i_instrumentManager,
                i_watchdogContext
                );
        i_watchdogMonitorManager.init(i_configProperties);
                
        return;
    }

    /**
     * Starts the Watchdog Server.
     */
    public void doStart()
    {
        i_watchdogProcessor.start();
        i_watchdogMonitorManager.start();
    }

    /**
     * Stops the Watchdog Server.
     */
    public void doStop()
    {
        i_watchdogProcessor.stop();
        i_watchdogMonitorManager.stop();
    }

    /**
     * Main bootstrap method for the Watchdog Server. Instantiates a new
     * <code>WatchdogServer</code>, calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()}
     * and then calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start()} on it.
     * 
     * @param  p_argsARR      command line arguments - none currently.
     */
    public static void main(String[] p_argsARR)
    {
        WatchdogServer          l_watchdogServer;

        // Debug.setDebugFullToSystemErr();
        
        try
        {
            l_watchdogServer = new WatchdogServer();
            l_watchdogServer.init(new WatchdogContext());
            l_watchdogServer.start();
        }
        catch(Exception l_e)
        {
            System.err.println(
                    "Watchdog Server startup exiting due to exception : ");
            l_e.printStackTrace();
            System.exit(1);
        }
    }

}  // End of public class WatchdogServer.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
