/*
 * Created on 12-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DefaultTextAlarmFormat extends AlarmFormat
{


    /**
     */
    public DefaultTextAlarmFormat()
    {
        super(C_TYPE_DEFAULT_TEXT);
    }

    public String toString()
    {
        return("DefaultTextAlarmFormat=[" + super.toString() + "]");
    }

}
