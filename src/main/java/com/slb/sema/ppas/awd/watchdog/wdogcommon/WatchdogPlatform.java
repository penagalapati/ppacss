/*
 * Created on 08-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.platform.AwdPlatform;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.platform.PlatformException;
import com.slb.sema.ppas.common.platform.PlatformProcessInformation;
import com.slb.sema.ppas.common.processcontrol.ProcessController;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogPlatform
extends AwdPlatform
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogPlatform";
    
    private ProcessController   i_processController;


    //private ProcessInformationMap i_runningProcessInformationMap;

    /**
     * Creates a new Watchdog Platform.
     * @param p_logger          loger to log any messages to.
     * @param p_instrumentManager instrument manager to use.
     * @param p_ppasProperties  config properties.
     */
    public WatchdogPlatform(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        PpasProperties          p_ppasProperties
    )
    {
        super(p_logger);
        
        try
        {
            i_processController = new ProcessController(
                    p_logger, p_instrumentManager, p_ppasProperties);
        }
        catch(Exception l_e)
        {
            l_e.printStackTrace();
            // TODO: What to do here?
        }

    }
    
    public ProcessInformationMap getRunningProcessInformation()
    throws PpasException
    {
        PlatformProcessInformation l_platformProcessInfoARR[];
        ProcessInformationMap   l_processInformationMap;
        ProcessInformation      l_processInformation;
        int                     l_loop;
        PlatformProcessInformation l_platformProcessInformation;
        String                  l_processName;
        
        // TODO: Makeconfigurable so can also list (see) all processes.
        l_platformProcessInfoARR = i_platform.listAllProcessesForCurrentUser(
                                                    5000); //timeout of 5 seconds
        
        l_processInformationMap = new ProcessInformationMap();

        for(l_loop = 0; l_loop < l_platformProcessInfoARR.length; l_loop++)
        {
            l_platformProcessInformation = (PlatformProcessInformation)
                    l_platformProcessInfoARR[l_loop];
            l_processName = l_platformProcessInformation.getProcessName();
            l_processInformation = new ProcessInformation(l_processName);
            l_processInformation.setField("STATE", "RUNNING");
            l_processInformationMap.putProcessInformation(
                    l_processName, l_processInformation);
        }
        
        
        // l_processInfoMap = getTestRunningProcessInformation();

        if(Debug.on)
        {
            Debug.print(
                    Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 11010, this,
                    "getRunningProcessInformation: Returning " +
                    l_processInformationMap);
        }        
        
        return l_processInformationMap;
    }

    /** Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performOsCommand = "performOsCommand";
    /**
     * Performs the supplied command, silently discarding any output.
     * 
     * @param p_command         command to perform.
     */
    public void performOsCommand(String p_command)
    {
        WatchdogActionException l_watchdogActionE;
        
        try
        {
            i_platform.performCommandSilently(p_command,
                                              10000); //timeout of 10 secs.
        }
        catch(PlatformException l_e)
        {
            l_watchdogActionE = new WatchdogActionException(
                    C_CLASS_NAME,
                    C_METHOD_performOsCommand,
                    11015,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().watchdogActionCommandException(p_command),
                    l_e);
            i_logger.logMessage(l_watchdogActionE);
            // We have logged it, best we can do now is continue.
        }
    }

    /** Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performOsCommandWithResult = "performOsCommandWithResult";
    /**
     * Performs the supplied command, returning any output.
     * 
     * @param p_command         command to perform.
     * @return Any output or <code>null</code> if error occurred.
     */
    public String performOsCommandWithResult(String p_command)
    {
        String                  l_result = null;
        WatchdogActionException l_watchdogActionE;
        
        try
        {
            l_result = i_platform.performCommand(p_command,
                    10000); //timeout of 10 secs.
        }
        catch(PlatformException l_e)
        {
            l_watchdogActionE = new WatchdogActionException(
                    C_CLASS_NAME,
                    C_METHOD_performOsCommand,
                    11015,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().watchdogActionCommandException(p_command),
                    l_e
                    );
            i_logger.logMessage(l_watchdogActionE);
            // We have logged it, best we can do now is continue.
        }
        
        return l_result;
    }

    /** Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_startAscsProcess = "startAscsProcess";

    /**
     * Tries to start the named ASCS process.
     * 
     * @param p_ascsProcessName process name of ASCS process to start.
     */
    public void startAscsProcess(String p_ascsProcessName)
    {
        WatchdogActionException l_watchdogActionE;
        
        try
        {
            if(i_processController != null)
            {
                i_processController.startAscsProcess(p_ascsProcessName);
            }
        }
        catch(PpasException l_e)
        {
            l_watchdogActionE = new WatchdogActionException(
                    C_CLASS_NAME,
                    C_METHOD_performOsCommand,
                    11015,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().ascsStartProcessError(p_ascsProcessName),
                    l_e);
            i_logger.logMessage(l_watchdogActionE);
            // We have logged it, best we can do now is continue.
        }
    }

}
