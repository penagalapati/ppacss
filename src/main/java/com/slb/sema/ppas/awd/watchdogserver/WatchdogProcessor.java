////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogProcessor.java
//      DATE            :       08-Dec-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       High level processing for watchdog.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/07/06 | M.Vonka    | Log error if non-existant alarm | PpaLon#2496/9386
//          |            | or alarm group is attempted to  |
//          |            | be used. Try to default to a    |
//          |            | known alarm or alarm group and  |
//          |            | at least raise that alarm.      |
//          |            | Added Javadoc.                  |
//----------+------------+---------------------------------+--------------------
// 11/09/06 | M.Vonka    | Log error and continue if       | PpaLon#2096/8999
//          |            | RuntimeException occurs.        |
//----------+------------+---------------------------------+--------------------
// 20/10/06 | M.Vonka    | Refactored slightly to use new  | PpacLon#2644/10214
//          |            | WatchdogContext.                |
//          |            | Added missing Javadoc.          |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdogserver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.alarm.ConfiguredAlarm;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmInstanceConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.CumalativeProcessInformation;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.OsCommandWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.ProcessCondition;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.ProcessInformation;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.ProcessInformationMap;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.RaiseAlarmWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.RestartProcessWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogContext;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogPlatform;
import com.slb.sema.ppas.common.awd.alarmcommon.AlarmParameterConstants;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * [Process] Watchdog processor which handles
 * monitoring of processes. 
 */
public class WatchdogProcessor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogProcessor";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdogserver.WatchdogProcessor.";

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected WatchdogContext   i_watchdogContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Alarm Group Config to use. */
    private AlarmGroupConfig    i_alarmGroupConfig;

    /** Alarm Config to use. */
    private AlarmConfig         i_alarmConfig;
    
    /** Watchdog Process Config to use. */
    private WatchdogProcessConfig i_watchdogProcessConfig;
    
    /**
     * Map of Process Name (String) to {@link CumalativeProcessInformation} holding
     * accumulated process information.
     */
    private Map                 i_cumalativeProcInfoMap;
    
    /** Alarm processor to use. */
    private AlarmProcessor      i_alarmProcessor;
    
    /** Watchdog platform which implements platform specific operations. */
    private WatchdogPlatform    i_watchdogPlatform;
    
    /** The process watchdog thread. */
    private WatchdogThread      i_watchdogThread;
    
    /** Process name running as. */
    private String              i_processName;
    
    /** Node (server) name running on. */
    private String              i_nodeName;
    
    /**
     * Delay after watchdog started before starting to monitor processes.
     * This configurable delay is to allow monitored processes to start before
     * for example starting to raise alarms on them. Current default is 30s.
     */
    private long                i_delayAfterStartMillis = 30 * 1000; // 30s

    /**
     * Configurable Interval between checks of all processes to see if any match
     * configured conditions. Current default is 1m.
     */    
    private long                i_watchdogCheckIntervalMillis = 60 * 1000; // 1m

    /**
     * Creates a new Watchdog Processor.
     * 
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager instrument manager to register any instruments with.
     * @param p_watchdogContext watchdog context running in.
     */
    public WatchdogProcessor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        WatchdogContext         p_watchdogContext
    )
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_watchdogContext = p_watchdogContext;
        i_processName = i_watchdogContext.getProcessName();
        i_nodeName = i_watchdogContext.getNodeName();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_END, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Initialises the Watchdog Processor according to the config.
     * @param p_configProperties config to initialise from.
     * @throws AwdConfigException config exception occurred while trying to
     *                          initialise.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    throws AwdConfigException
    {
        WatchdogProcessConfigLoader l_watchdogProcessConfigLoader;
        String                  l_configValue;
        WatchdogProcessConfig.Entry l_entryARR[];
        int                     l_loop;
        String                  l_configFilename;
        
        i_cumalativeProcInfoMap = new HashMap();

        i_alarmGroupConfig = i_watchdogContext.getAlarmGroupConfig();
        i_alarmConfig = i_watchdogContext.getAlarmConfig();

        l_watchdogProcessConfigLoader = new WatchdogProcessConfigLoader(
                i_logger, p_configProperties, i_watchdogContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "watchdogConfigFilePathAndName");
        i_watchdogProcessConfig =
                l_watchdogProcessConfigLoader.loadWatchdogProcessConfig(
                l_configFilename,
                i_alarmConfig,
                i_alarmGroupConfig,
                i_processName);
                
        l_entryARR = i_watchdogProcessConfig.getEntryArray();
        for(    l_loop = 0;
                l_loop < l_entryARR.length;
                l_loop++)
        {
            i_cumalativeProcInfoMap.put(
                    l_entryARR[l_loop].getProcessName(),
                            new CumalativeProcessInformation(
                    l_entryARR[l_loop].getProcessName())); 
        }
        
        i_alarmProcessor = i_watchdogContext.getAlarmProcessor();

        i_watchdogPlatform = i_watchdogContext.getWatchdogPlatform();
        
        l_configValue = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "delayAfterStartMillis");
        if(l_configValue != null)
        {
            i_delayAfterStartMillis = Integer.parseInt(l_configValue);
        } 

        l_configValue = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "checkIntervalMillis");
        if(l_configValue != null)
        {
            i_watchdogCheckIntervalMillis = Integer.parseInt(l_configValue);
        } 
    }
    
    /** Starts the process Watchdog Processor. */
    public synchronized void start()
    {
        if(i_watchdogThread == null)
        {
            i_watchdogThread = new WatchdogThread();
        }
        i_watchdogThread.start();
    }

    /** Stops the process Watchdog Processor. */
    public synchronized void stop()
    {
        if(i_watchdogThread != null)
        {
            try
            {
                i_watchdogThread.stop();
            }
            catch(InterruptedException l_e)
            {
                // Ignore
            }
                
        }
    }
    
    /**
     * Reads process information from the OS and checks all the configured
     * conditions.
     */
    private void checkAllConfiguredConditions()
    {
        ProcessInformationMap   l_runningProcessMap;
        
        if(Debug.on)
        {
            Debug.print(
                    Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 11010, this,
                    "checkAllConfiguredConditions: Getting running process information.");
        }
        try
        {
            l_runningProcessMap =
                    i_watchdogPlatform.getRunningProcessInformation();
            if(Debug.on)
            {
                Debug.print(
                        Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 11010, this,
                        "checkAllConfiguredConditions: Got running process information.");
            }
            
            checkAllConfiguredConditions(l_runningProcessMap);
        }
        catch(PpasException l_e)
        {
            // TODO: Be more careful??? If too many consecutive errors, terminate?
            // Problem checking conditions, best we can do is ignore (should
            // have already been logged) and carry on.
        }
        
    }

    /**
     * Checks all the configured conditions against the process information supplied.
     * @param p_runningProcessMap the proces information to check.
     */
    private void checkAllConfiguredConditions(
        ProcessInformationMap   p_runningProcessMap
    )
    {
        WatchdogProcessConfig        l_watchdogProcessConfig;

        // Hold reference in local variable and use this incase a reload occurs
        // while processing this event.
        l_watchdogProcessConfig = i_watchdogProcessConfig;
        
        checkAllConfiguredConditions(
                p_runningProcessMap, l_watchdogProcessConfig);
    }
    
    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_checkAllConfiguredConditions = "checkAllConfiguredConditions";    
    /**
     * Checks the Watchdog Process Config against the process information supplied.
     * @param p_runningProcessMap the proces information to check.
     * @param p_watchdogProcessConfig the config (conditions) to check.
     */
    private void checkAllConfiguredConditions(
        ProcessInformationMap   p_runningProcessMap,
        WatchdogProcessConfig   p_watchdogProcessConfig
    )
    {
        WatchdogProcessConfig.Entry l_processConfigEntryARR[];
        int                     l_loop;
        ProcessInformation      l_processInformation;
        CumalativeProcessInformation l_cumalativeProcInfo;
        
        l_processConfigEntryARR = p_watchdogProcessConfig.getEntryArray();

        if(Debug.on)
        {
            Debug.print(
                    Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 12010, this,
                    "checkAllConfiguredConditions: Checking all conditions.");
        }

        for(l_loop = 0; l_loop < l_processConfigEntryARR.length; l_loop++)
        {
            try
            {
                l_processInformation = p_runningProcessMap.getProcessInformation(
                        l_processConfigEntryARR[l_loop].getProcessName());
                l_cumalativeProcInfo = (CumalativeProcessInformation)
                        i_cumalativeProcInfoMap.get(l_processConfigEntryARR[l_loop].
                        getProcessName());
                l_cumalativeProcInfo.update(l_processInformation);
                checkWatchdogConfigEntry(l_cumalativeProcInfo,
                        l_processConfigEntryARR[l_loop]);
            }
            catch(RuntimeException l_e)
            {
                // This should not happen. If it does, best we can do is log and try
                // to continue.
                i_logger.logMessage(new AwdException(
                        C_CLASS_NAME, C_METHOD_checkAllConfiguredConditions,
                        102015, this, (PpasRequest)null, (long)0,
                        AwdKey.get().unexpectedInternalServerError(
                                "checking condition " + l_processConfigEntryARR[l_loop]),
                        l_e
                        ));
            }
        }

        if(Debug.on)
        {
            Debug.print(
                    Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 12020, this,
                    "checkAllConfiguredConditions: Checked all conditions.");
        }

    }

    /**
     * Checks the accumulated process information against the config entry
     * and performs any required actions.
     * @param p_processInformation accumulted process information.
     * @param p_entry           config entry to check.
     */
    private void checkWatchdogConfigEntry(
        CumalativeProcessInformation p_processInformation,
        WatchdogProcessConfig.Entry p_entry
    )
    {        
        checkConditions(
                p_entry.getProcessName(),
                p_processInformation,
                p_entry.getWatchdogGroup());
    }

    /**
     * Checks the watchdog groups conditions against the process information
     * and performs any required actions.
     * @param p_processName     process name of process being checked.
     * @param p_processInformation process information.
     * @param p_watchdogGroup   config definining conditions to check and
     *                          actions to take. 
     */
    public void checkConditions(
        String                  p_processName,
        CumalativeProcessInformation p_processInformation,
        WatchdogGroup           p_watchdogGroup
    )
    {
        boolean                 l_match = false;
        WatchdogGroup.Entry     l_entryARR[];
        int                     l_loop;
        ProcessCondition        l_processCondition;
        
        l_entryARR = p_watchdogGroup.getEntryArray();
        for(l_loop = 0; !l_match && (l_loop < l_entryARR.length); l_loop++)
        {
            l_processCondition = l_entryARR[l_loop].getProcessCondition(); 

            if(Debug.on)
            {
                Debug.print(
                        Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 13010, this,
                        "checkConditions: Checking process " + p_processName +
                        " - checking condition " + l_processCondition  + " against process information " +
                        p_processInformation);
            }

            l_match = l_processCondition.checkCondition(
                   p_processInformation);
            if(l_match)
            {
                // Process condition met - peform actions.

                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 13020, this,
                            "checkConditions: Condition met for process " + p_processName +
                            ". Condition " + l_processCondition  + " matches process information " +
                            p_processInformation);
                }

                performActions(
                        p_processInformation,
                        l_entryARR[l_loop].getActionGroup());
            }
        }
        
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performActions = "performActions";
    /**
     * Performs the configured actions for the process.
     * @param p_processInformation process information.
     * @param p_watchdogActionGroup configured actions.
     */
    private void performActions(
        CumalativeProcessInformation p_processInformation,
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {
        WatchdogAction          l_actionARR[];
        int                     l_loop;
        AwdException            l_awdE;
        
        l_actionARR = p_watchdogActionGroup.getActionArray();
        for(l_loop = 0; l_loop < l_actionARR.length; l_loop++)
        {
            if(p_processInformation.performAction(l_actionARR[l_loop]))
            {
                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 20100, this,
                            "performAction returned true, so action allowed "+
                            "(max retries within interval not exceeded)");
                }
                
                if(l_actionARR[l_loop] instanceof RaiseAlarmWatchdogAction)
                {
                    performRaiseAlarmAction(
                            p_processInformation,
                            (RaiseAlarmWatchdogAction)l_actionARR[l_loop]);
    
                }
                else if(l_actionARR[l_loop] instanceof OsCommandWatchdogAction)
                {
                    performOsCommandAction(
                            p_processInformation,
                            (OsCommandWatchdogAction)l_actionARR[l_loop]);
    
                }
                else if(l_actionARR[l_loop] instanceof RestartProcessWatchdogAction)
                {
                    performRestartAscsProcessAction(
                            p_processInformation,
                            (RestartProcessWatchdogAction)l_actionARR[l_loop]);
    
                }
                else
                {
                    l_awdE = new AwdConfigException(C_CLASS_NAME, C_METHOD_performActions, 20150,
                            this, (PpasRequest)null, (long)0,
                            AwdKey.get().unexpectedAction(l_actionARR[l_loop].getClass().getName())
                            );
                    i_logger.logMessage(l_awdE);
                    // Logged, best we can do now is to continue.
                }
            }
            else
            {
                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_VLOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 20190, this,
                            "performAction returned false, so action NOT allowed "+
                            "(max retries within interval exceeded!)");
                }
            }
        }
        
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_performRaiseAlarmAction = "performRaiseAlarmAction";    
    /**
     * Performs the supplied Raise Alarm Watchdog Action.
     * @param p_processInformation accumulated process information.
     * @param p_raiseAlarmWatchdogAction the Raise Alarm Watchdog Action.
     */
    private void performRaiseAlarmAction(
        CumalativeProcessInformation p_processInformation,
        RaiseAlarmWatchdogAction p_raiseAlarmWatchdogAction
    )
    {
        ConfiguredAlarm         l_alarm;
        AlarmInstanceConfig     l_alarmInstanceConfig;
        AlarmGroup              l_alarmGroup;
        AwdException            l_awdException;
        
        l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(
                p_raiseAlarmWatchdogAction.getAlarmInstanceName());
        l_alarmGroup = i_alarmGroupConfig.getAlarmGroup(
                p_raiseAlarmWatchdogAction.getAlarmGroupName());

        if(l_alarmInstanceConfig == null)
        {
            l_awdException = new AwdException(
                    C_CLASS_NAME,
                    C_METHOD_performRaiseAlarmAction,
                    22386,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().unknownAlarmInstanceName(
                            p_raiseAlarmWatchdogAction.getAlarmInstanceName())
                    );
            i_logger.logMessage(l_awdException);
            l_alarmInstanceConfig = i_alarmConfig.getAlarmInstanceConfig(
                    "ALARM." + l_awdException.getExceptionId());
        }
        if(l_alarmGroup == null)
        {
            l_awdException = new AwdException(
                    C_CLASS_NAME,
                    C_METHOD_performRaiseAlarmAction,
                    22387,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().unknownAlarmGroupName(
                            p_raiseAlarmWatchdogAction.getAlarmGroupName())
                    );
            i_logger.logMessage(l_awdException);
            l_alarmGroup = i_alarmGroupConfig.getAlarmGroup(
                    "AlarmGroupMain");
        }
        

        // If we still haven't got an instance or a group, ignore (we've
        // already logged an error.
        if((l_alarmInstanceConfig != null) && (l_alarmGroup != null))
        {
            l_alarm = new ConfiguredAlarm(
                    (String)null,
                    (String)null,
                    (Integer)null,
                    i_nodeName,
                    i_processName,
                    (Map)null,
                    l_alarmInstanceConfig);
            l_alarm.setParameter(AlarmParameterConstants.C_PARAM_PROCESS_NAME,
                    p_processInformation.getProcessName());
            
            i_alarmProcessor.processAlarm(
                    l_alarm,
                    l_alarmGroup);
        }
    }

    /**
     * Performs an OS Command Action for the process.
     * @param p_processInformation process information.
     * @param p_osCommandWatchdogAction OS command action.
     */
    private void performOsCommandAction(
        CumalativeProcessInformation p_processInformation,
        OsCommandWatchdogAction p_osCommandWatchdogAction
    )
    {
        i_watchdogPlatform.performOsCommand(
                p_osCommandWatchdogAction.getOsCommand());
    }

    /**
     * Performs a Restart ASCS process Action for the process.
     * @param p_processInformation process information.
     * @param p_restartAscsProcessWatchdogAction restart ASCS process action.
     */
    private void performRestartAscsProcessAction(
        CumalativeProcessInformation p_processInformation,
        RestartProcessWatchdogAction p_restartAscsProcessWatchdogAction
    )
    {
        i_watchdogPlatform.startAscsProcess(
                p_processInformation.getProcessName());
    }

    /**
     * Utility method to sleep, with the ability to be woken by being notified
     * (preferred) or interrupted.
     * @param p_sleepMillis     milliseconds to sleep.
     */
    private void sleep(
        long                    p_sleepMillis
    )
    {
        try
        {
            synchronized(this)
            {
                wait(p_sleepMillis);
            }
        }
        catch(InterruptedException l_e)
        {
            // Ignore
        }
    }

    /**
     * Private inner class which implements the Process Watchdog monitor thread.
     */
    private class WatchdogThread extends ThreadObject
    {
        
        /** Debug/trace output date time format. */
        private SimpleDateFormat i_debugDf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");

        /**
         * Top level of the main processing of the Process Watchdog. The main
         * processing is: loop continuously, in each loop wait for the
         * next monitor time, check all processes against configuration and trigger
         * any actions required.
         */
        public void doRun()
        {
            long                l_startTime;
            long                l_endTime;
            long                l_sleepTime;
            
            if(Debug.on)
            {
                Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_START, C_CLASS_NAME, 20100, this,
                        "Watchdog Thread running - doRun entered. Sleeping for " +
                        "delay after start millis " + i_delayAfterStartMillis);
            }

            sleep(i_delayAfterStartMillis);
            
            if(Debug.on)
            {
                Debug.print(
                        Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 20110, this,
                        "doRun: Delay after start up passed, starting checks.");
            }

            while(!isStopping())
            {
                l_startTime = System.currentTimeMillis();

                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 20120, this,
                            "doRun: Starting check of all monitored processes at " +
                            i_debugDf.format(new Date(l_startTime)));
                }
                
                try
                {
                    checkAllConfiguredConditions();
                }
                catch(RuntimeException l_e)
                {
                    // This should not happen. If it does, best we can do is log and try
                    // to continue.
                    i_logger.logMessage(new AwdException(
                            C_CLASS_NAME, "doRun", 20125, this, (PpasRequest)null, (long)0,
                            AwdKey.get().unexpectedInternalServerError(
                                    "checking all configured conditions"),
                            l_e
                            ));
                }
                l_endTime = System.currentTimeMillis();

                // Try to keep the interval accurate, no matter the length of
                // time to perform checks.                
                l_sleepTime = l_startTime + i_watchdogCheckIntervalMillis -
                        System.currentTimeMillis();

                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 20130, this,
                            "doRun: Finished checking of all monitored processes at " +
                            i_debugDf.format(new Date(l_endTime)) +
                            " (took " + (l_endTime - l_startTime) + " millis). Sleeping for " +
                            l_sleepTime + " millis.");
                }

                if(l_sleepTime > 0)
                {
                    sleep(l_sleepTime);
                }
            }
        }
        
        /**
         * Returns the thread's name.
         * @return Returns the thread's name.
         */
        public String getThreadName()
        {
            return ("WDogThd");
        }        
    }

} // End of public class WatchdogProcessor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
