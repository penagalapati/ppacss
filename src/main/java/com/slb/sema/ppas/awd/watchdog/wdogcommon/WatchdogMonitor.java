////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogMonitor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Interface representing a a watchdog monitor.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/13/06 | M.Vonka    | Added isRunning method.         | PpacLon#2644/10214
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.Map;

/**
 * Interface representing a a watchdog monitor.
 */
public interface WatchdogMonitor
{

    /** Monitor is not running (has not been started or has been stopped). */
    public static final int    C_STATE_NOT_RUNNING = 99;    

    /** Monitor is starting up (passed/failed state not know yet). */
    public static final int    C_STATE_STARTING = 1;    

    /** Monitor is in the passed state (last tests passed). */
    public static final int    C_STATE_PASSED = 2;
    
    /** Monitor is in the failed state (last tests inclduing retries failed). */
    public static final int    C_STATE_FAILED = 3;

    /** Start monitoring. */
    public void start();
    
    /** Stop monitoring. */
    public void stop();
    
    /**
     * Add Watchdog Monitor listener to list to be notified when monitor enters the failed
     * or passed states.
     * @param p_listener        listener to be added.
     */
    public void addMonitorListener(WatchdogMonitorListener p_listener);

    /**
     * Remote the Watchdog Monitor listener from list to be notified when monitor enters the failed
     * or passed states.
     * @param p_listener        listener to be removed.
     * @return True if monitor was in list (and was removed).
     */
    public boolean removeMonitorListener(WatchdogMonitorListener p_listener);
    
    /**
     * Initialize the monitor.
     * 
     * @param p_configParams    Map of param name (String) to param value
     *                          (String) for config for monitor.
     * @throws Exception        exception initializing monitor.
     */
    public void init(Map p_configParams)
    throws Exception;
    
    /**
     * Sets the enabled state of the monitor. This does not start or stop the monitor!
     * @param p_enable          enabled state to set.
     */
    public void enable(boolean p_enable);
    
    /**
     * Returns whether the monitor is currently running.
     * @return Returns whether the monitor is currently running.
     */
    public boolean isRunning();

} // End of public class WatchdogMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////