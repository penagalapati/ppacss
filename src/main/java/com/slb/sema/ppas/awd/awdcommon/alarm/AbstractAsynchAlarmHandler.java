////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AbstractAsynchAlarmHandler.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       This class is an abstract super class which
//                              implements a simple mechanism for asynchronous
//                              Alarm Handlers.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
// 28/02/07 | R.Grimshaw | Retrieve max queue size config. | PpacLon#2964
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.ThroughputCounterInstrument;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.Queue;

/**
 * This class is an abstract super class which implements a simple mechanism
 * for asynchronous Alarm Handlers. TODO:<<more detail!!!!>>
 */
public abstract class AbstractAsynchAlarmHandler
extends AlarmHandler
{

    /**
     * Pending alarm queue (queue of {@link Entry}) which need to be
     * processed. New Alarms are added to
     * the end of this queue while background threads remove and deliver
     * Alarms from the front of the queue.
     */    
    private Queue               i_pendingAlarmQueue;

    /**
     * Array of threads which perform the background delivery of
     * Alarms.
     */
    private HandlerDaemon       i_handlerDaemonARR[];
    
    /**
     * Number of background Handler Daemon threads.
     */
    private int                 i_handlerDaemonCount;

    /**
     * Number of alarms queued to be delivered by this handler
     * counter instrument.
     */
    private ThroughputCounterInstrument   i_alarmsQueuedTCI;

    /**
     * Number of alarms successfully delivered by this handler
     * counter instrument.
     */
    private CounterInstrument   i_alarmsDeliveredCI;

    /**
     * Number of alarms unsuccessfully delivered by this handler
     * counter instrument.
     */
    private CounterInstrument   i_alarmsDeliveryFailedCI;

    /**
     * Map of background thread prefix name (<code>String</code>) to last
     * instance number (<code>Integer</code>). Each constructed Abstract
     * Asynch Alarm Handler is passed a thread prefix name to prefix each
     * thread created for that type of Handler. The rest of the thread name is
     * made by appending a "-" and then the next consecutive thread instance
     * number for that prefix. So Handlers with prefix "FtpAH" will have background
     * threads named "FtpAH-1", "FtpAH-2" etc, while handlers with prefix
     * "EmailAH" will have threads named "EmailAH-1", "EmailAH-2" etc.
     */    
    private static Map          c_instanceCountersMap = new HashMap();

    /**
     * Create a new Abstract Asynchronous Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     * @param  p_handlerDaemonCount number of background threads to create.
     * @param  p_threadNamePrefix prefix for name of each background thread.
     */    
    public AbstractAsynchAlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext,
        int                     p_handlerDaemonCount,
        String                  p_threadNamePrefix
    )
    {
        super(  p_logger,
                p_configProperties,
                p_instrumentManager,
                p_awdContext,
                p_threadNamePrefix
                );
        
        int                     l_loop;
        int                     l_maxQueueSize = 0;
        
        i_handlerDaemonCount = p_handlerDaemonCount;

        l_maxQueueSize = p_configProperties.getIntProperty(
                this.getClass().getName() + ".maxQueueSize", 10000);

        i_pendingAlarmQueue = new Queue(
                "AlarmHandlerQ for " + this.getClass().getName() + "-" +
                this.hashCode(),
                i_instrumentManager,
                Queue.C_FLAG_DISCARD_OBJECTS,
                l_maxQueueSize
                );

        i_pendingAlarmQueue.init();
        i_handlerDaemonARR = new HandlerDaemon[i_handlerDaemonCount];
        
        i_alarmsQueuedTCI = new ThroughputCounterInstrument(
                "Queued For Delivery",
                "Number of alarms queued for delivery by this handler.");
        i_instrumentSet.add(i_alarmsQueuedTCI);
        
        i_alarmsDeliveredCI = new CounterInstrument(
                "Delivered",
                "Number of alarms successfully delivered by this handler.");
        i_instrumentSet.add(i_alarmsDeliveredCI);

        i_alarmsDeliveryFailedCI = new CounterInstrument(
                "Delivery failed",
                "Number of alarms unsuccessfully delivered by this handler.");
        i_instrumentSet.add(i_alarmsDeliveryFailedCI);

        for(l_loop = 0; l_loop < i_handlerDaemonCount; l_loop++)
        {
            i_handlerDaemonARR[l_loop] = new HandlerDaemon(p_threadNamePrefix);
            i_handlerDaemonARR[l_loop].start();
        }
        
    }
    
    /**
     * Delivers the formatted alarm to the destination synchronously - i.e.
     * the alarm should have been 'delivered' (sent) by the time this method
     * returns.
     * <p/>
     * Creates a new Notifiyer, queues the alarm (with the notifiyer), waits
     * for the notification that the alarm has been delivered and then returns. 
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     */
    protected final void doDeliverAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
        )
    {
        AsynchAlarmNotifiyer    l_notifier;

        l_notifier = new Notifier();
        queueAlarm(
                p_alarm,
                p_formattedAlarm,
                p_destination,
                l_notifier
                );
                
        l_notifier.waitToFinish();
    }

    /**
     * Delivers the formatted alarm to the destination asynchronously - i.e.
     * the alarm should be 'delivered' (sent) in the background. If a
     * notifiyer is supplied, it will be notified when the alarm has been
     * delivered.
     * <p/>
     * Simply queues the alarm for background processing.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * @param  p_notifier       notifyer to be notified once alarm 'delivered' (sent).
     */
    protected final void doDeliverAsynchAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
        )
    {
        if(p_notifier == null)
        {
            p_notifier = new Notifier();
        }
        queueAlarm(
                p_alarm,
                p_formattedAlarm,
                p_destination,
                p_notifier
                );
    }
    
    /**
     * Actually delivers the formatted alarm to the destination. This abstract
     * method is called by the background threads to actually deliver an
     * alarm. An instance number identifing the calling background thread is passed as
     * some concrecte implementations require this (e.g. to chose which single
     * threaded client/connection object to use to deliver the alarm).
     *
     * @param  p_threadInstanceNum instance number identifing calling background thread.
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * @param  p_notifier       notifyer to be notified once alarm 'delivered' (sent).
     *
     * @throws AlarmDeliveryException a problem occurred trying to deliver an alarm.
     */
    protected abstract void doDeliverAsynchAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
        )
        throws AlarmDeliveryException;

    /**
     * Queues a new Alarm to be delivered in the background.
     * 
     * @param  p_alarm          alarm to send.
     * @param  p_formattedAlarm formatted alarm text.
     * @param  p_destination    destination for alarm.
     * @param  p_notifier       notifyer to be notified once alarm 'delivered' (sent).
     */
    private synchronized void queueAlarm(
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
    )
    {        
        if(true /* TODO: <<i_pendingAlarmQueue.length - implement!!!!>> */)
        {
            i_pendingAlarmQueue.addLast(new Entry(
                    p_alarm,
                    p_formattedAlarm,
                    p_destination,
                    p_notifier
                    ));
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_alarmsQueuedTCI.increment();
                }
            }
        }
    }

    /**
     * This private inner class implements the background threads that
     * performs the main processing of the concreate, derived Handler.
     */    
    private class HandlerDaemon
    extends ThreadObject
    {
        /** Unique (within a thread name prefix) instance number of thread. */
        private int             i_instanceNumber;
        
        /** Unique instance name for thread. */
        private String          i_instanceName;

        /**
         * Creates a new background Handler Daemon thread.
         * @param p_threadNamePrefix thread name prefix for this handler.
         */
        public HandlerDaemon(String p_threadNamePrefix)
        {
            super(C_FLAG_DAEMON);

            Integer             l_instanceCounterI;
            
            synchronized(c_instanceCountersMap)
            {
                l_instanceCounterI = (Integer)c_instanceCountersMap.get(
                        p_threadNamePrefix);
                if(l_instanceCounterI == null)
                {
                    i_instanceNumber = 1;
                }
                else
                {
                    i_instanceNumber = l_instanceCounterI.intValue() + 1;
                }
                i_instanceName = p_threadNamePrefix + "-" + i_instanceNumber;
                
                c_instanceCountersMap.put(
                        p_threadNamePrefix, new Integer(i_instanceNumber));
            }
            
        }

        /**
         * Performs the processing of the background thread. It loops until
         * stopped and on each iteration it removes an
         * {@link AbstractAsynchAlarmHandler.Entry} from the 
         * pending alarms Queue and processes the entry by calling
         * the abstract <code>doDeliverAsynchAlarm</code>.
         */
        public void doRun()
        {
            Entry                   l_entry;
            boolean                 l_successful;

            while(!isStopping())
            {
                l_successful = false;
                l_entry = (Entry)i_pendingAlarmQueue.removeFirst(); // Check notify!!!!
                if(l_entry != null)
                {
                    try
                    {
                        doDeliverAsynchAlarm(
                                i_instanceNumber,
                                l_entry.i_alarm,
                                l_entry.i_formattedAlarm,
                                l_entry.i_alarmDestination,
                                l_entry.i_notifier
                                );
                        if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
                        {
                            if (i_instrumentSet.i_isEnabled)
                            {
                                i_alarmsDeliveredCI.increment();
                            }
                        }
                        l_successful = true;
                    }
                    catch(AlarmException l_e)
                    {
                        // Should have already been logged, so nothing more
                        // we can do except increment failure counter.
                        if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
                        {
                            if (i_instrumentSet.i_isEnabled)
                            {
                                i_alarmsDeliveryFailedCI.increment();
                            }
                        }
                    }
                    finally
                    {
                        // Ensure we call finished even on error.
                        if(l_entry.i_notifier != null)
                        {
                            //TODO: Differentiate between success and failed!
                            l_entry.i_notifier.finished(
                                    l_successful, l_entry.i_alarm);
                        }
                        if(l_entry.i_alarm instanceof AlarmActionListener)
                        {
                            ((AlarmActionListener)l_entry.i_alarm).alarmActionStatus(
                                    l_entry.i_alarm,
                                    l_successful,
                                    (AlarmAction)null,
                                    (Object)null);
                        }
                    }
                }
            }
        }

        /**
         * Returns the name to set this thread's name to. Basically the unquie
         * name is the thread name prefix supplied by the concreate Handler plus
         * a consecutive instance number for the prefix.
         * @return The name to set this thread's name to.
         */
        protected String getThreadName()
        {
            return i_instanceName;
        }

    } // End of private inner class HanderDaemon
    
    /**
     * This private inner class implements a simple notifier used within this
     * class where an <code>AsynchAlarmNotifier<code> is required to wait for
     * completion of an asynchronous delivery of an Alarm.  
     */
    private class Notifier
    implements AsynchAlarmNotifiyer
    {
        /** Indicates if the delivery has completed yet. */
        private boolean         i_finished = false;
        
        /**Indicates if the delivery completed without errors. */
        private boolean         i_wasSuccessful = false;
        
        /**
         * Called when the asynchronous Alarm delivery has completed.
         * @param p_success     <code>true</code> if successfull, <code>false</code> otherwise.
         * @param p_alarm       alarm that was to be delivered.
         */
        public synchronized void finished(boolean p_success, Alarm p_alarm)
        {
            finished(p_success, p_alarm, (Object)null);
        }
        
        /**
         * Called when the asynchronous Alarm delivery has completed.
         * @param p_success     <code>true</code> if successfull, <code>false</code> otherwise.
         * @param p_alarm       alarm that was to be delivered.
         * @param p_result      associated object. 
         */
        public synchronized void finished(boolean p_success, Alarm p_alarm, Object p_result)
        {
            i_wasSuccessful = p_success;
            i_finished = true;
            this.notifyAll();
        }

        /**
         * Returns whether the delivery was successfull.
         * @return Whether the delivery was successfull.
         */
        public boolean wasSuccessful()
        {
            return i_wasSuccessful;
        }

        /** Waits until the asynchronous Alarm delivery has completed. */
        public synchronized void waitToFinish()
        {
            while(!i_finished)
            {
                try
                {
                    this.wait();
                }
                catch(InterruptedException l_e)
                {
                    // Ignore
                }
            }
            
        }
        
        /**
         * Waits until the asynchronous Alarm delivery has completed or timeout
         * is exceeded. After returning, <code>hasFinished</code> can be called
         * to check if it has finished or timeout was exceeded.
         * @param p_timeout     timeout (milliseconds). 
         */
        public void waitToFinish(long p_timeout)
        {
            long                l_endTimeMillis;
            long                l_currentTimeMillis;
            
            l_endTimeMillis = System.currentTimeMillis() + p_timeout;
            
            while(  !i_finished &&
                    ((l_currentTimeMillis = System.currentTimeMillis())
                            <  l_endTimeMillis)
            )
            {
                try
                {
                    this.wait(l_endTimeMillis - l_currentTimeMillis);
                }
                catch(InterruptedException l_e)
                {
                    // Ignore
                }
            }
        }
        
        /**
         * Returns whether Alarm delivery has finished.
         * 
         * @return whether Alarm delivery has finished.
         */
        public boolean hasFinished()
        {
            return i_finished;
        }
        
    } // End of private inner class Notifier
    
    /**
     * This private inner class implements a simple container to hold all
     * required data related to a pending Alarm on the Qeueue.
     */
    private class Entry
    {
        /** The Alarm. */
        private Alarm           i_alarm;
        
        /** The formatted text of the Alarm. */
        private String          i_formattedAlarm;
        
        /** The Alarm Destination for the Alarm. */
        private AlarmDestination i_alarmDestination;
        
        /**
         *  Any notifier which needs to be notified once the Alarm has been
         *  delivered.
         */
        private AsynchAlarmNotifiyer i_notifier;
        
        /**
         * Creates a new Entry.
         * 
         * @param  p_alarm      the Alarm.
         * @param  p_formattedAlarm the formatted text of the Alarm.
         * @param  p_destination the Alarm Destination for the Alarm.
         * @param  p_notifier   notifier to be notified or <code>null</code>.
         */
        private Entry(
            Alarm               p_alarm,
            String              p_formattedAlarm,
            AlarmDestination    p_destination,
            AsynchAlarmNotifiyer p_notifier
        )
        {
            i_alarm = p_alarm;
            i_formattedAlarm = p_formattedAlarm;
            i_alarmDestination = p_destination;
            i_notifier = p_notifier;
        }
    } // End of private inner class Entry

} // End of public abstract class AbstractAsynchAlarmHandler

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
