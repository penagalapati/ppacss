////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       OracleMsg.java
//      DATE            :       12-Oct-2007
//      AUTHOR          :       Eric Dangoor
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Defines the message resource bundle storing the
//                              message strings associated with Oracle alarms.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME        | DESCRIPTION                      | REFERENCE
//----------+-------------+----------------------------------+------------------
// DD/MM/YY | <name>      | <brief description of change>    | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import java.io.IOException;

import com.slb.sema.ppas.common.exceptions.XmlExceptionBundle;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class implements the resource bundle containing the text messages
 * associated with Oracle alarms and contains the keys into that resource
 * bundle.
 */
public class OracleMsg extends XmlExceptionBundle
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Debug methods. */
    private static final String C_CLASS_NAME = "OracleMsg";

    /**
     * Define the name of the file which will contain all the messages
     * to be associated with Oracle alarms.
     */
    private static final String C_ORACLE_MSG_FILE = "oracle_msg.xml";

    //------------------------------------------------------------------------
    // Public constructor
    //------------------------------------------------------------------------

    /**
     * Construct an Oracle messages resource bundle object. This method opens
     * a stream to the file containing the text of all the messages to be
     * associated with Oracle alarms.
     * 
     * @throws IOException If the message file cannot be read.
     */
    public OracleMsg() throws IOException
    {
        super(OracleMsg.class.getResourceAsStream(C_ORACLE_MSG_FILE));

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                    null, C_CLASS_NAME, 10410, this,
                    hashCode() + " Constructing " + C_CLASS_NAME);
        }

        if ( PpasDebug.on )
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                    null, C_CLASS_NAME, 10420, this,
                    hashCode() + " Constructed " + C_CLASS_NAME);
        }
    }
}
