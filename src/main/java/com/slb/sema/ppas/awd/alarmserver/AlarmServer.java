////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmServer.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Alarm Server including bootstrap for it.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/08/07 | E.Clayton  | Added check on whether object   | PpacLon#3192/11929
//          |            | is null into doStop method.     |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.alarmserver;

import com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer;
import com.slb.sema.ppas.common.awd.alarmclient.AlarmClientLogHandler;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Alarm Server including bootstrap for it. The alarm server receives events
 * from other processes, checks them against its configuration and if an alarm
 * has been configured for the event sends the alarm. Alarms can be sent to
 * multiple destinations, in multiple formats using multiple delivery methods -
 * see {@link com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDeliveryManager
 * AlarmDeliveryManager}.
 * <p/>
 * The Alarm Server receives events using a simple proprietary protocol over
 * TCP/IP. This was chosen for performance reasons (e.g. rather than using
 * RMI / Java Serialization). Also it allows non Java components to send events
 * easily if ever required. See {@link AlarmServerEndPoint} and
 * {@link AlarmSocketEndPoint}.
 */
public class AlarmServer extends AwdAbstractServer
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmServer";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.alarmserver.AlarmServer.";

    /**
     * Configuration property name for the interval (millis) between sending
     * heartbeats.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_PORT_NUMBER = C_CFG_PROPERTY_BASE +
                                        "portNumber";

    /**
     * Configuration property name for the initial trace enabled state of
     * new connections accepted by the alarm server.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SOKCET_TRACE_ENABLED = C_CFG_PROPERTY_BASE +
                                        "socketTraceEnabled";

    /**
     * Alarm event processor used to process received events.
     */
    private AlarmEventProcessor i_alarmEventProcessor;
    
    /**
     * Server end point which handles new connections to the Alarm Server.
     */
    private AlarmServerEndPoint i_alarmServerEndPoint;
    
    /**
     * Sever end point port number on which to listen for new connections.
     */
    private int                 i_serverPortNumber;

    /**
     * Create a new <code>AlarmServer</code>.
     */
    public AlarmServer()
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( C_CLASS_NAME + "=[port=" + i_serverPortNumber +
                ", serverEndPoint=" + i_alarmServerEndPoint + ", " +
                ", eventProcessor=" + i_alarmEventProcessor + ", " +
                super.toString() + "]");
    }

    /**
     * Returns a verbose String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A verbose respresentation suitable for debugging/trace.
     */
    public String toVerboseString()
    {
        return( C_CLASS_NAME + "=[port=" + i_serverPortNumber +
                ", serverEndPoint=" + i_alarmServerEndPoint + ", " +
                ", eventProcessor=" + i_alarmEventProcessor.toVerboseString() +
                ", " + super.toVerboseString() + "]");
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /**
     * Performs the Alarm Server specific initialisation. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()} method.
     * @throws PpasException If the initialisation fails.
     */    
    protected void doInit()
    throws PpasException
    {
        String                  l_propertyName;
        String                  l_configParam;
        PpasException           l_ppasException;
        boolean                 l_socketTraceEnabled;

        // Transaction List Manager manager port number (e.g. for shutdown).
        l_propertyName = C_CFG_PARAM_PORT_NUMBER;
        l_configParam = i_configProperties.getPortNumber(l_propertyName);
        if (l_configParam == null)
        {
            l_ppasException = new PpasConfigException(
                    C_CLASS_NAME, C_METHOD_doInit, 10070, this,
                    null, 0,
                    ConfigKey.get().missingConfigData(l_propertyName, C_CLASS_NAME));

            i_logger.logMessage(l_ppasException);

            throw l_ppasException;
        }
        
        i_serverPortNumber = Integer.parseInt(l_configParam);

        l_propertyName = C_CFG_PARAM_SOKCET_TRACE_ENABLED;
        l_socketTraceEnabled = i_configProperties.getBooleanProperty(
               l_propertyName, false);

        i_alarmEventProcessor = new AlarmEventProcessor(
                i_logger,
                i_instrumentManager,
                i_awdContext
                );
        i_alarmEventProcessor.init(i_configProperties);

        i_alarmServerEndPoint = new AlarmServerEndPoint(
                i_logger,
                i_instrumentManager,
                i_serverPortNumber,
                0, // Max cons - 0 means no max set.
                10000, // Timeout for created (accepted) sockets.
                l_socketTraceEnabled, // Trace set default for created (accepted) sockets.
                i_alarmEventProcessor
                );
    }

    /**
     * Performs the Alarm Server specific startup. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start} method.
     */    
    protected void doStart()
    {
        i_alarmServerEndPoint.start();
    }

    /**
     * Performs the Alarm Server specific stop (shutdown) processing. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#stop} method.
     */    
    protected void doStop()
    {
        try
        {
            if (i_alarmServerEndPoint != null) i_alarmServerEndPoint.stop();
        }
        catch(InterruptedException l_e)
        {
            // We tried. Best we can do is output and ignore.
            System.err.println(C_CLASS_NAME + ": Stop interrupted, ignoring.");
            l_e.printStackTrace();
        }
    }

    /// TODO:<<Add support to M/W and here for multiple IP addresses on one server node!!!!>>
    /**
     * Main bootstrap method for the Alarm Server. Instantiates a new
     * <code>AlarmServer</code>, calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()}
     * and then calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start()} on it.
     * 
     * @param  p_argsARR      command line arguments - none currently.
     * @throws PpasException  If initialisation fails.
     */
    public static void main(
        String[]                p_argsARR
    )
    throws PpasException
    {
        AlarmServer             l_alarmServer;

        //Debug.setDebugFullToSystemErr();
        
        //l_configPropertiesFilename = "alarmserver.properties";

        // Disable Alarm Client Log Handler in this JVM as we must prevent
        // infinite circular attempts to raise (log) an event (i.e. prevent
        // error trying
        // to deliver an alarm by this process being sent to this process as
        // an event which again causes an error trying to deliver it etc).         
        AlarmClientLogHandler.setEnabled(false);
        
        l_alarmServer = new AlarmServer();
        //l_configProperties.load(l_configPropertiesFilename); 
        
        try
        {
            l_alarmServer.init();
        }
        catch(PpasException l_ex)
        {
            // Could not initialise, but there might be some threads running,
            // so need to stop everything.
            l_alarmServer.stop();
            throw l_ex;
        }
        l_alarmServer.start();
    }
    
} // End of public class AlarmServer.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////