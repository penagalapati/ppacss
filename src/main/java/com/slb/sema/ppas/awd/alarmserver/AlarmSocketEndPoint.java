////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmSocketEndPoint.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Alarm Socket End Point which handles the ARMP
//                              (AlaRM Protocol - ASCS proprietry) protocol on
//                              a single socket.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Minor fix to attach missing     | PpacLon#1772/7309
//          |            | originating exception. Also     |
//          |            | mssing Javadoc added.           |
//----------+------------+---------------------------------+--------------------
// 06/03/07 | R.Grimshaw | Alarm server running out of     | PpacLon#2971      
//          |            | space in read buffer so tidy    |
//          |            | temporary buffer after parsing  |
//          |            | each message.                   |
//----------+------------+---------------------------------+--------------------
// 12/10/07 | E.Dangoor  | Handle close message            | PpacLon#3405/12253
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdAlarmException;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarm;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarmEvent;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.net.SocketEndPoint;
import com.slb.sema.ppas.util.net.TraceableSocket;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Alarm Socket End Point which handles the ARMP
 * (AlaRM Protocol - ASCS proprietry) protocol on
 * a single socket. A socket it handed off to the constructors of this class
 * for handling by the Alarm Socket End Point.
 */
public class AlarmSocketEndPoint extends SocketEndPoint
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmSocketEndPoint";

    /** Constant for equals (=) character used in ARMP. Value is {@value}. */
    private static final byte C_BYTE_EQUALS = '=';

    /** Constant for double quote (") character used in ARMP. Value is {@value}. */
    private static final byte C_BYTE_DOUBLE_QUOTE = '"';

    /** Constant for ampersand (&amp;) character used in ARMP. Value is {@value}. */
    private static final byte C_BYTE_AMPERSAND = '&';

    /** Constant for percent (%) character used in ARMP. Value is {@value}. */
    private static final byte C_BYTE_PERCENT = '%';

    /** Alarm Event Processor event processing is delegated to. */
    private AlarmEventProcessor i_alarmEventProcessor;

    /** End of input stream flag (input stream of Socket). */
    private boolean             i_eos = false;

    /** Temp buffer fixed length ARMP header is read into. */
    private byte                i_headerBufferARR[] = new byte[4];

    /** Temp buffer ARMP message data is read into. */
    private byte                i_bufferARR[] = new byte[8096];

    /** Amount of temp buffer {@link #i_bufferARR} filled. */    
    private int                 i_bufferFilled = 0;
    
    /** Current read position of temp buffer {@link #i_bufferARR}. */
    private int                 i_bufferPos = 0;
    

    /**
     * Creates a new Alarm Socket End Point to handle the ARMP protocol on the
     * supplied Socket.
     * @param p_logger          logger to log to.
     * @param p_socket          socket for this end point.
     * @param p_timeOut         socket timeout (<code>SO_TIMEOUT</code>) to set
     *                          on the socket.
     * @param p_alarmEventProcessor alarm event processor to pass read Alarm Events to. 
     */
    public AlarmSocketEndPoint(
        Logger                  p_logger,
        TraceableSocket         p_socket,
        int                     p_timeOut,
        AlarmEventProcessor     p_alarmEventProcessor
    )
    {
        this(p_logger, (InstrumentManager)null, p_socket, p_timeOut, p_alarmEventProcessor);
    }

    /**
     * Creates a new Alarm Socket End Point to handle the ARMP protocol on the
     * supplied Socket.
     * @param p_logger          logger to log to.
     * @param p_instrumentManager instrument manager to register any instruments with.
     * @param p_socket          socket for this end point.
     * @param p_timeOut         socket timeout (<code>SO_TIMEOUT</code>) to set
     *                          on the socket.
     * @param p_alarmEventProcessor alarm event processor to pass read Alarm Events to. 
     */
    public AlarmSocketEndPoint(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        TraceableSocket         p_socket,
        int                     p_timeOut,
        AlarmEventProcessor     p_alarmEventProcessor
    )
    {
        super(p_logger, p_instrumentManager, p_socket, p_timeOut,
        1 // Use one thread to read from and write to the socket.
        );
        i_alarmEventProcessor = p_alarmEventProcessor;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doSocketProcessing = "doSocketProcessing";
    
    /** Perform socket processing.
     *  @param p_threadNumber Identifier of the thread performing the work.
     */
    protected void doSocketProcessing(int p_threadNumber)
    {
        // Design of this class is to only use one thread, so can ignore
        // p_threadNumber as it will always be 1.
        InputStream             l_inputStream;
        //OutputStream            l_outputStream;
        Object                  l_alarmMessage;
        AwdException            l_awdE;
        
        while(!i_stopRequested && !i_eos)
        {
            try
            {
                l_inputStream = i_socket.getInputStream();
                //l_outputStream = i_socket.getOutputStream();
                
                l_alarmMessage = readNextMessage(l_inputStream);
                if(l_alarmMessage == null)
                {
                    i_eos = true;
                }
                else if(l_alarmMessage instanceof SimpleAlarmEvent)
                {
                    i_alarmEventProcessor.processEvent((SimpleAlarmEvent)l_alarmMessage);
                }
                else if(l_alarmMessage instanceof SimpleAlarm)
                {
                    i_alarmEventProcessor.processAlarm((SimpleAlarm)l_alarmMessage);
                }
                else
                {
                    throw new IOException("Unexpected class of object read - " +
                            l_alarmMessage.getClass().getName());
                }
            }
            catch(InterruptedIOException l_e)
            {
                // Most likely this is a socket read timeout (or we have been
                // interrupted to shutdown, so ignore and continue).
                if(Debug.on)
                {
                    Debug.print(
                            Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            C_METHOD_doSocketProcessing + ": Interrupted IO, assume timeout or stop called - continue.");
                }
            }
            catch(IOException l_e)
            {
                // Log and then treat as End Of Stream.
                l_awdE = new AwdAlarmException(
                        C_CLASS_NAME, C_METHOD_doSocketProcessing, 10070, this,
                        null, 0,
                        AwdKey.get().errorReceivingAlarmOrEvent(),
                        l_e);

                i_logger.logMessage(l_awdE);

                i_eos = true;
            }
        }
    }
    
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readNextMessage = "readNextMessage";
    
    /**
     * Reads the next ARMP message from the Input Stream and returns it.
     * 
     * @param p_is              Input Stream to read next ARMP message from.
     * @return                  the ARMP message read.
     * @throws IOException      IO Exception trying to read next ARMP message.
     */
    private Object readNextMessage(
        InputStream             p_is
    )
    throws IOException
    {
        Object                  l_alarmMessage;
        SimpleAlarm             l_simpleAlarm;
        SimpleAlarmEvent        l_simpleAlarmEvent;
        String                  l_eventName = "";
        String                  l_eventText = "";
        String                  l_originatingNodeName = "";
        String                  l_originatingProcessName = "";
        String                  l_protocolName;
        String                  l_version;
        Field                   l_field;
        boolean                 l_end = false;
        String                  l_alarmName;
        int                     l_alarmSeverity;
        String                  l_alarmText;
        Map                     l_parameterMap;
        String                  l_armpType;
        long                    l_raisedTime = 0;
        
        // read(p_is, i_headerBufferARR, 0, 4);
        l_protocolName = readString(p_is, 4);
        if(!l_protocolName.equals("ARMP"))
        {
            throw new RuntimeException(
                    "Header error - expected protocol [ARMP] read [" +
                    l_protocolName + "]");
        }

        //read(p_is, i_headerBufferARR, 0, 4);
        //l_version = new String(i_headerBufferARR);
        l_version = readString(p_is, 4);
        if(!l_version.equals("v001"))
        {
            throw new RuntimeException(
                    "Header error - expected version [v001] read [" +
                    l_version + "]");
        }

        l_field = readNextField(p_is);
        l_armpType = l_field.i_value;
        if(l_armpType.equals("event"))
        {
            while(!l_end)
            {
                 l_field = readNextField(p_is);
                 if(l_field == null)
                 {
                     l_end = true;
                 }
                 else
                 {
                     if("eventName".equals(l_field.i_name))
                     {
                         l_eventName = l_field.i_value;
                     }
                     else if("raisedTime".equals(l_field.i_name))
                     {
                         l_raisedTime = Long.parseLong(l_field.i_value);
                     }                 
                     else if("eventText".equals(l_field.i_name))
                     {
                         l_eventText = l_field.i_value;
                     }                 
                     else if("origNodeName".equals(l_field.i_name))
                     {
                         l_originatingNodeName = l_field.i_value;
                     }                 
                     else if("origProcName".equals(l_field.i_name))
                     {
                         l_originatingProcessName = l_field.i_value;
                     }                 
                 }
            }
                
            l_simpleAlarmEvent = new SimpleAlarmEvent(l_eventName, l_eventText,
                    l_originatingNodeName, l_originatingProcessName);
            // Reset raised time as default is object creation time, but here we
            // are reconstituting a simple alarm event created somewhere else.
            l_simpleAlarmEvent.setRaisedTime(l_raisedTime);
            l_alarmMessage = l_simpleAlarmEvent;
            
            
        }
        else if(l_armpType.equals("alarm"))
        {
            l_alarmName = null;
            l_alarmSeverity = 0;
            l_alarmText = null;
            l_parameterMap = new HashMap();
            while(!l_end)
            {
                 l_field = readNextField(p_is);
                 if(l_field == null)
                 {
                     l_end = true;
                 }
                 else
                 {
                     if("alarmName".equals(l_field.i_name))
                     {
                         l_alarmName = l_field.i_value;
                     }
                     else if("alarmSeverity".equals(l_field.i_name))
                     {
                         l_alarmSeverity = Integer.parseInt(l_field.i_value);
                     }                 
                     else if("raisedTime".equals(l_field.i_name))
                     {
                         l_raisedTime = Long.parseLong(l_field.i_value);
                     }                 
                     else if("alarmText".equals(l_field.i_name))
                     {
                         l_alarmText = l_field.i_value;
                     }
                     else if("origNodeName".equals(l_field.i_name))
                     {
                         l_originatingNodeName = l_field.i_value;
                     }                 
                     else if("origProcName".equals(l_field.i_name))
                     {
                         l_originatingProcessName = l_field.i_value;
                     }                 
                     else
                     {
                         // currently just assume its an alarm parameter!
                         l_parameterMap.put(l_field.i_name, l_field.i_value);
                     }                 
                 }
            }
            l_simpleAlarm = new SimpleAlarm(
                    l_alarmName, l_alarmText, l_alarmSeverity,
                    l_originatingNodeName, l_originatingProcessName, l_parameterMap);
            l_simpleAlarm.setRaisedTime(l_raisedTime);
            l_alarmMessage = l_simpleAlarm;
        }
        else if(l_armpType.equals("close"))
        {
            l_alarmMessage = null;
        }
        else
        {
            throw new IOException("Unknown armpType=" + l_armpType);
        }        
        
        // If any of the buffer is consumed, store any remaining buffer at front of buffer
        if(i_bufferPos > 0)
        {
            if(Debug.on) Debug.print(
                Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                C_METHOD_readNextMessage + ": storing any remaining buffer at front of buffer i_bufferPos=" + i_bufferPos);
            
            int l_counter = 0;
            for(int l_loop = i_bufferPos; l_loop < i_bufferFilled; l_loop++)
            {
                i_bufferARR[l_counter] = i_bufferARR[l_loop];
                l_counter++;
            }
            i_bufferFilled = l_counter;
            i_bufferPos = 0;
            
            if(Debug.on) Debug.print(
                Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                Debug.C_ST_TRACE, C_CLASS_NAME, 10010, this,
                C_METHOD_readNextMessage + ": Finished storing any remaining buffer at front of buffer " +
                        "i_bufferFilled=" + i_bufferFilled + " i_bufferPos=" + i_bufferPos);
        }
        

        if(Debug.on) Debug.print(
                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10020, this,
                "Leaving " + C_METHOD_readNextMessage + ", msg=" + l_alarmMessage);

        return l_alarmMessage;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readNextField = "readNextField";

    /**
     * Reads the next ARMP message from the Input Stream and returns it.
     * 
     * @param p_is              Input Stream to read next field of ARMP message from.
     * @return                  the next field of the ARMP message read.
     * @throws IOException      IO Exception trying to read next field of ARMP message.
     */
    private Field readNextField(
        InputStream             p_is
    )
    throws IOException
    {
        Field                   l_field = null;
        StringBuffer            l_valueStringBuffer;
        String                  l_value = null;
        String                  l_name;
        boolean                 l_fieldEnd = false;
        boolean                 l_nameEnd = false;
        boolean                 l_endOfMessage = false;
        boolean                 l_percentInValue = false;
        int                     l_loop;
        int                     l_counter = 0;
        int                     l_nameCounterStart;
        int                     l_nameCounterEnd = 0;
        int                     l_valueCounterStart = 0;
        int                     l_valueCounterEnd = 0;
        int                     l_readCount = 0;
        int                     l_pos;
        int                     l_previousPos;
        int                     l_char;
        //int                     l_index;
        
        l_nameCounterStart = i_bufferPos;
        while(!l_fieldEnd && !l_endOfMessage)
        {
            if(i_bufferPos >= i_bufferFilled)
            {   
                if (i_bufferARR.length - i_bufferFilled == 0)
                {
                    // Already filled buffer so cannot read more - error.
                    throw new RuntimeException("Too much data being sent (>" + i_bufferFilled + " bytes)" + 
                        "ARMP message data temp buffer=[" + new String(i_bufferARR) + "]");
                }
                
                do
                {
                    l_readCount = p_is.read(i_bufferARR, i_bufferFilled,
                            i_bufferARR.length - i_bufferFilled);
                }
                while (l_readCount == 0);
                
                if(l_readCount < 0)
                {
                    // End of stream!!!!
                    throw new RuntimeException("Unexpected end of stream");
                }
                i_bufferFilled += l_readCount;
            }
                        
            while(!l_nameEnd && (i_bufferPos < i_bufferFilled) && !l_endOfMessage)
            {
                if(i_bufferARR[i_bufferPos] == C_BYTE_EQUALS)
                {
                    l_nameEnd = true;
                    l_nameCounterEnd = i_bufferPos;
                    i_bufferPos++; // Skip =
                    l_valueCounterStart = i_bufferPos;
                }
                else if(i_bufferARR[i_bufferPos] == C_BYTE_AMPERSAND)
                {
                    l_endOfMessage = true;
                    i_bufferPos++; // Skip &
                }
                else
                {
                    i_bufferPos++; // Continue to next byte
                }
            }

            while(l_nameEnd && !l_fieldEnd && (i_bufferPos < i_bufferFilled) && !l_endOfMessage)
            {
                if(i_bufferARR[i_bufferPos] == C_BYTE_AMPERSAND)
                {
                    l_fieldEnd = true;
                    l_valueCounterEnd = i_bufferPos;
                    i_bufferPos++; // Skip &
                }
                else if(i_bufferARR[i_bufferPos] == C_BYTE_PERCENT)
                {
                    l_percentInValue = true;
                    i_bufferPos++;
                }
                else
                {
                    i_bufferPos++;
                }
            }
        }

        if(!l_endOfMessage)
        {        
            l_name = new String(i_bufferARR, l_nameCounterStart, l_nameCounterEnd - l_nameCounterStart);
            if(!l_percentInValue)
            {
                l_value = new String(i_bufferARR, l_valueCounterStart,
                        l_valueCounterEnd - l_valueCounterStart); 
            }
            else
            {
                l_valueStringBuffer = new StringBuffer();
                // Handle embedded percents.
                l_pos = l_valueCounterStart;
                l_previousPos = l_pos;
                for(l_pos = l_valueCounterStart; l_pos < l_valueCounterEnd; l_pos++)
                {
                    if(i_bufferARR[l_pos] == '%')
                    {
                        // Append bytes upto %
                        l_valueStringBuffer.append(
                                new String(i_bufferARR, l_previousPos, (l_pos - l_previousPos)));
                        
                        // Convert next two hex digits to actual character and append it.
                        l_char = (hexCharToInt(i_bufferARR[l_pos+1]) * 16);
                        l_char += (hexCharToInt(i_bufferARR[l_pos+2]));
                        l_valueStringBuffer.append((char)l_char);

                        if(Debug.on) Debug.print(
                                Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                                Debug.C_ST_TRACE, C_CLASS_NAME, 10030, this,
                                C_METHOD_readNextField + ": Converted " + new String(i_bufferARR, l_pos, 3) +
                                " to character '" + (char)l_char + "'.");

                        l_pos += 2; // Skip 2, for loop will move on one more.
                        l_previousPos = l_pos + 1; // Skip 3, as not incremented by for loop
                    }
                }
                if(l_previousPos < l_pos)
                {
                    // Append remaining
                    l_valueStringBuffer.append(
                            new String(i_bufferARR, l_previousPos, (l_pos - l_previousPos)));
                    l_previousPos = l_pos;
                }
                l_value = l_valueStringBuffer.toString();
            }
            l_field = new Field(l_name, l_value);
        }
        

        if(Debug.on) Debug.print(
                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10040, this,
                "Leaving " + C_METHOD_readNextField + ", field=" + l_field);
        
        return l_field;
    } // End of private method readNextField(InputStream)
        
    /**
     * Reads exactly the next <code>p_len</code> bytes of the ARMP message
     * from the Input Stream and returns them as a <code>String</code>.
     * 
     * @param p_is              Input Stream to read next ARMP message from.
     * @param p_len             exact length of next string to read.
     * @return                  The <code>String</code> read.
     * @throws IOException      IO Exception trying to read the <code>String</code>.
     */
    private String readString(
        InputStream             p_is,
        int                     p_len
    )
    throws IOException
    {
        int                     l_readCount;
        int                     l_remaining;
        int                     l_startPos = i_bufferPos;
        int                     l_endPos = 0;
        
        l_remaining = p_len;
        
        while((l_remaining > 0) && !i_stopRequested && !i_eos)
        {
            if(i_bufferPos >= i_bufferFilled)
            {
                do
                {
                    l_readCount = p_is.read(i_bufferARR, i_bufferFilled,
                            i_bufferARR.length - i_bufferFilled);
                }
                while (l_readCount == 0);
                if(l_readCount < 0)
                {
                    // End of stream!!!!
                    throw new RuntimeException(
                            "Unexpected end of stream");
                }
                i_bufferFilled += l_readCount;
            }
            if(l_remaining <= (i_bufferFilled - i_bufferPos))
            {
                i_bufferPos += l_remaining;
                l_endPos = i_bufferPos;
                l_remaining = 0;
            }
            else
            {
                l_remaining -= (i_bufferFilled - i_bufferPos);
                i_bufferPos = i_bufferFilled;
            }
        }
        
        String l_string = new String(i_bufferARR, l_startPos, (l_endPos - l_startPos));
        
        if(Debug.on) Debug.print(
            Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
            Debug.C_ST_END, C_CLASS_NAME, 10050, this,
            "Leaving " + C_METHOD_readNextMessage + ", String=" + l_string);
        
        return l_string;
    }

    /**
     * Low level read method which reads exactly <code>p_len</code>
     * bytes from the Input Stream
     * into the buffer starting at <code>p_offset</code>.
     * 
     * @param p_is              Input Stream to read next ARMP message from.
     * @param p_bufferARR       buffer to read bytes into.
     * @param p_offset          offset into buffer.
     * @param p_len             exact number of bytes to read.
     * @throws IOException      IO Exception trying to read the bytes.
     */
    private void read(
        InputStream             p_is,
        byte                    p_bufferARR[],
        int                     p_offset,
        int                     p_len
    )
    throws IOException
    {
        int                     l_readCount;
        int                     l_remaining;
        int                     l_offset = 0;
        
        l_remaining = p_len;
        
        while((l_remaining > 0) && !i_stopRequested && !i_eos)
        {
            l_readCount = p_is.read(p_bufferARR, l_offset, l_remaining);
            if(l_readCount >= 0)
            {
                l_offset += l_readCount;
                l_remaining -= l_readCount;
            }
            else
            {
                i_eos = true;
            }   
        }
    }

    /**
     * Utility method to convert a hex ASCII character ('0'-'F'/'f') byte to
     * its int value.
     * @param p_byte            hex ASCII character byte to convert.
     * @return                  Int value of the hex ASCII character byte.
     */ 
    private int hexCharToInt(byte p_byte)
    {
        int                     l_int;
        
        if((p_byte >= 48) && (p_byte <= 57))
        {
            l_int = p_byte - 48;
        }
        else if((p_byte >= 65) && (p_byte <= 70))
        {
            l_int = p_byte - 65 + 10;
        }
        else if((p_byte >= 97) && (p_byte <= 102))
        {
            l_int = p_byte - 97 + 10;
        }
        else
        {
            throw new RuntimeException("Exception converting hex char - byte " + p_byte +
                    " is not a hex char");
        }
        
        return l_int;
    }

    /**
     * Simple private class holding details of a single ARMP field.
     */
    private static class Field
    {
        /** Name of field. */
        private String          i_name;
        
        /** Value of field. */
        private String          i_value;

        /** Creates a new <code>Field</code>.
         * @param p_name        name of field.
         * @param p_value       value of field.
         */
        private Field(
            String              p_name,
            String              p_value
            )
        {
            i_name = p_name;
            i_value = p_value;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            return "Field=[name=" + i_name + ",value=" + i_value + "]";
        }
    } // End of private class Field.
} // End of public class AlarmSocketEndpoint

