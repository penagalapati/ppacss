////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmServerUT.java
//      DATE            :       07-Sep-2006
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Automated Test for Alarm Server.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.alarmserver;

import java.io.File;

import com.slb.sema.ppas.awd.awdcommon.AwdTestCaseTT;
import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;

/** Automated Test for Alarm Server. */
public class AlarmServerUT extends AwdTestCaseTT
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmServerUT";

    /** Nme of sub-system. Value is {@value}. */
    private static final String C_SUB_SYSTEM = "awd";
    
    /** Regular expression of AWD log file. Value is @{value}. */
    private static final String C_FILE_RE = "^AscsAlarmLogLow_.+$";
    
    /**
     * Standard Constructor.
     * @param p_name Name of test.
     */
    public AlarmServerUT(String p_name)
    {
        super(p_name);
    }

    /** @ut.when A monitored exception is logged that has a short parameter.
     *  @ut.then The exception is recorded in the AWD log.
     */
    public void testLogMonitoredExceptionShort()
    {
        performLoggedException(10);
    }

    /** @ut.when A monitored exception is logged that has a medium parameter.
     *  @ut.then The exception is recorded in the AWD log.
     */
    public void testLogMonitoredExceptionMedium()
    {
        performLoggedException(3000);
    }

    /** @ut.when A monitored exception is logged that has a long parameter.
     *  @ut.then The exception is recorded in the AWD log.
     */
    public void testLogMonitoredExceptionLong()
    {
        performLoggedException(10000);
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performLoggedException = "performLoggedException";
    /** Log an exception with a short text.
     * 
     * @param p_length Length of parameter in the exception text.
     */
    private void performLoggedException(int p_length)
    {
        beginOfTest(C_METHOD_performLoggedException + " with length " + p_length);
        
        File l_file = getLatestLogFile(C_SUB_SYSTEM, C_FILE_RE);

        long l_numRows = l_file == null ? 0 : countNumOfLines(l_file);
        
        StringBuffer l_sb = new StringBuffer();
        
        for (int i = 0; i < p_length; i++)
        {
            l_sb.append("+");
        }
        
        ConfigKey.ConfigExceptionKey l_key = ConfigKey.get().configNotNumeric(l_sb.toString());
        
        PpasConfigException l_except = new PpasConfigException(C_CLASS_NAME,
                                                               C_METHOD_performLoggedException,
                                                               10010, this, null, 0,
                                                               l_key);
        
        c_logger.logMessage(l_except);

        checkAlarmedKey(l_key, l_numRows);
        
        endOfTest();
    }
    
    /** Check a specified exception key has been logged.
     * 
     * @param p_key         Exception key to check.
     * @param p_lastNumRows Number of rows in file as of last check.
     */
    private void checkAlarmedKey(ExceptionKey p_key, long p_lastNumRows)
    {
        long l_lastRows = p_lastNumRows;
        
        for (int i = 0; i < 50; i++)
        {
            File l_file = getLatestLogFile(C_SUB_SYSTEM, C_FILE_RE);
            
            if (l_file != null)
            {
                String[] l_extra = getFileOutput(l_file, l_lastRows);
                
                for (int j = 0; j < l_extra.length; j++)
                {
                    say("Check '" + l_extra[j] + "'");
                    
                    if (l_extra[j].matches("^.+: .*" + p_key.getKey() + ".*$"))
                    {
                        say("Found '" + p_key.getKey() + "' in '" + l_extra[j] + "'");
                        return;
                    }
                }
                
                l_lastRows += l_extra.length;
            }
            
            snooze(0.25);
        }
        
        fail("Could not find '" + p_key.getKey() + "'");
    }
}
