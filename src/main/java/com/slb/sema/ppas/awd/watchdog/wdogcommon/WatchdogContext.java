////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogContext.java
//      DATE            :       13-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#2644
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Watchdog context which extends AwdContext with
//                              watchdog specific items.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfigLoader;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasProperties;


/**
 * Watchdog context which extends AwdContext with
 * watchdog specific items.
 */
public class WatchdogContext extends AwdContext
{
    
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "WatchdogContext";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
            "com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogContext.";

    /**
     * Base part of name for configuration properties for alarm config and
     * alarm group config. For backwards compatibility of existing config, this
     * is still using WatchdogProcessor even after being moved here from
     * {@link com.slb.sema.ppas.awd.watchdogserver.WatchdogProcessor}.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE_WATCHDOG_PROCESSOR =
            "com.slb.sema.ppas.awd.watchdogserver.WatchdogProcessor.";

    /** Watchdog platform to use. */
    private WatchdogPlatform    i_watchdogPlatform;
    
    /** Alarm processor to use. */
    private AlarmProcessor      i_alarmProcessor;

    /** Alarm Group Config to use. */
    private AlarmGroupConfig    i_alarmGroupConfig;
    
    /** Alarm Config to use. */
    private AlarmConfig         i_alarmConfig;
    
    /** Watchdog Action Group Config to use. */
    private WatchdogActionGroupConfig i_watchdogActionGroupConfig;

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialises the context based on the supplied properties.
     * 
     * @param p_configProperties Properties to read configuration from.
     * @throws PpasException If initialisation fails.
     */    
    public void init(PpasProperties p_configProperties)
    throws PpasException
    {
        super.init(p_configProperties);

        AlarmGroupConfigLoader  l_alarmGroupConfigLoader;
        AlarmConfigLoader       l_alarmConfigLoader;
        WatchdogActionGroupConfigLoader l_watchdogActionGroupConfigLoader;
        String                  l_configFilename;

        i_alarmProcessor = new AlarmProcessor(i_logger, i_instrumentManager, this);
        i_alarmProcessor.init(p_configProperties);

        i_watchdogPlatform = new WatchdogPlatform(
                i_logger, i_instrumentManager, p_configProperties);

        l_alarmGroupConfigLoader = new AlarmGroupConfigLoader(
                i_logger, p_configProperties, this);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE_WATCHDOG_PROCESSOR +
                "alarmGroupConfigFilePathAndName");
        i_alarmGroupConfig = l_alarmGroupConfigLoader.loadAlarmGroupConfig(
                l_configFilename);

        l_alarmConfigLoader = new AlarmConfigLoader(
                i_logger, p_configProperties, this);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE_WATCHDOG_PROCESSOR +
                "alarmConfigFilePathAndName");
        i_alarmConfig = l_alarmConfigLoader.loadAlarmConfig(l_configFilename);

        l_watchdogActionGroupConfigLoader = new WatchdogActionGroupConfigLoader(
                i_logger, p_configProperties, this);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "watchdogActionGroupConfigFilePathAndName");
        i_watchdogActionGroupConfig = l_watchdogActionGroupConfigLoader.loadWatchdogActionGroupConfig(
                l_configFilename, i_alarmConfig, i_alarmGroupConfig, getProcessName());

    }
    
    /**
     * Returns the alarmProcessor.
     * @return Returns the alarmProcessor.
     */
    public AlarmProcessor getAlarmProcessor()
    {
        return i_alarmProcessor;
    }
    
    /**
     * Returns the watchdogPlatform.
     * @return Returns the watchdogPlatform.
     */
    public WatchdogPlatform getWatchdogPlatform()
    {
        return i_watchdogPlatform;
    }

    /**
     * Returns the alarmConfig.
     * @return Returns the alarmConfig.
     */
    public AlarmConfig getAlarmConfig()
    {
        return i_alarmConfig;
    }
    
    /**
     * Returns the alarmGroupConfig.
     * @return Returns the alarmGroupConfig.
     */
    public AlarmGroupConfig getAlarmGroupConfig()
    {
        return i_alarmGroupConfig;
    }
    
    /**
     * Returns the watchdogActionGroupConfig.
     * @return Returns the watchdogActionGroupConfig.
     */
    public WatchdogActionGroupConfig getWatchdogActionGroupConfig()
    {
        return i_watchdogActionGroupConfig;
    }
    
} // End of class WatchdogContext

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////