////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdAbstractConfig.java
//      DATE            :       08-Jul-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       Abstract super class for AWD config classes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.config;

import java.util.Locale;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Abstract super class for AWD config classes.
 */
public abstract class AwdAbstractConfig
{

    /** Logger messages logged to. */
    protected Logger            i_logger;

    /**
     * Creates a new AwdAbstractConfig.
     *  
     * @param  p_logger         logger messages logged to.
     */
    public AwdAbstractConfig(
        Logger                  p_logger
    )
    {
        super();
        
        i_logger = p_logger;
    }

    /**
     * Convenieance method to create a new AWD Config Exception from supplied
     * details, log it and then throw it.
     *  
     * @param  p_className      calling class (passed to new exception).
     * @param  p_methodName     calling method (passed to new exception).
     * @param  p_statementNumber calling statement no. (passed to new exception).
     * @param  p_key            exception key (passed to new exception).
     * @throws AwdConfigException the newly created and logged AWD Config Exception.
     */    
    protected void handleConfigException(
        String                  p_className,
        String                  p_methodName,
        int                     p_statementNumber,
        AwdKey.AwdExceptionKey  p_key)
    throws AwdConfigException
    {
        AwdConfigException           l_exception;
        
        l_exception = new AwdConfigException(
                p_className,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                p_key);
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Conveniece method which checks if the logger is <code>null</code>
     * , if not it logs the message, else it writes it to
     * </code>System.err</code>.
     * 
     * @param  p_loggable       the loggable message.
     */
    private void logMessage(LoggableInterface p_loggable)
    {
        if(i_logger == null)
        {
            System.err.println(p_loggable.getMessageText(Locale.getDefault()));
        }
        else
        {
            i_logger.logMessage(p_loggable);
        }
    }    

}// End of public abstract class AwdAbstractConfigLoader 

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
