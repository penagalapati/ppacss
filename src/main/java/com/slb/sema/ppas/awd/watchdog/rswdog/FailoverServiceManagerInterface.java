////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FailoverServiceManagerInterface.java
//      DATE            :       23-Feb-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Interface for a Failover Manager which can perform a
//                              failover.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

/**
 * Interface for a Failover Manager which can perform a
 * failover. The interface hides all other details of what the
 * implementing object does or has to do.
 */
public interface FailoverServiceManagerInterface
{

    /**
     * Called to perform a failover of a service. Can be called on either node
     * regardless of which node is currently supporting the service.
     * 
     * @param p_serviceName
     * @return Textual descriptive result.
     */
    public String failoverService(
        String                  p_serviceName
        );
    
} // End of public interface FailoverServiceManagerInterface.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////