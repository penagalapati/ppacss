////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmServerEndPoint.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Alarm Server End Point which accepts new ARMP
//                              (AlaRM Protocol - ASCS proprietry) connections
//                              to the Alarm Server.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.net.ServerEndPoint;
import com.slb.sema.ppas.util.net.SocketEndPoint;
import com.slb.sema.ppas.util.net.TraceableSocket;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Alarm Server End Point which accepts new ARMP
 * (AlaRM Protocol - ASCS proprietry) connections
 * to the Alarm Server. For accepted connections, creates a new
 * {@link AlarmSocketEndPoint} and hands it the accepted socket to be handled
 * by it.
 */
public class AlarmServerEndPoint extends ServerEndPoint
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmServerEndPoint";

    /**
     * Alarm Event Processor to be used by created Alarm Socket End Points 
     * to process Alarm Events.
     */
    private AlarmEventProcessor i_alarmEventProcessor;

    /**
     * Creates a new Alarm Server End Point.
     * @param p_logger          logger to log to.
     * @param p_portNumber      IP port number to listen for new ARMP connections.
     * @param p_timeOut         read timeout (<code>SO_TIMEOUT</code>) set on newly
     *                          accepted sockets.
     * @param p_traceSet        trace state set on newly accepted sockets.
     * @param p_alarmEventProcessor Alarm Event Processor to use to process Alarm Events.
     */
    public AlarmServerEndPoint(
        Logger                  p_logger,
        int                     p_portNumber,
        int                     p_timeOut,
        boolean                 p_traceSet,
        AlarmEventProcessor     p_alarmEventProcessor
    )
    {
        super(p_logger, p_portNumber, p_timeOut, p_traceSet);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        i_alarmEventProcessor = p_alarmEventProcessor;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Creates a new Alarm Server End Point.
     * @param p_logger          logger to log to.
     * @param p_instrumentManager instrument manager to register any instruments with.
     * @param p_portNumber      IP port number to listen for new ARMP connections.
     * @param p_maxConnections  maximum simultaneous connections (inifinite it <= 0).
     *                          Once maximum reached, newly accepted connections are
     *                          immediately closed.
     * @param p_timeOut         read timeout (<code>SO_TIMEOUT</code>) set on newly
     *                          accepted sockets.
     * @param p_traceSet        trace state set on newly accepted sockets.
     * @param p_alarmEventProcessor Alarm Event Processor to use to process Alarm Events.
     */
    public AlarmServerEndPoint(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        int                     p_portNumber,
        int                     p_maxConnections,
        int                     p_timeOut,
        boolean                 p_traceSet,
        AlarmEventProcessor     p_alarmEventProcessor
    )
    {
        super(  p_logger,
                p_instrumentManager,
                p_portNumber,
                p_maxConnections,
                p_timeOut,
                p_traceSet);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10100, this,
                "Constructing " + C_CLASS_NAME);

        i_alarmEventProcessor = p_alarmEventProcessor;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10190, this,
                "Constructed " + C_CLASS_NAME);

    }

    /**
     * Creates and returns a new Alarm Socket End Point which handles the
     * ARMP protocol on the supplied Socket.
     * 
     * @param p_socket          socket for new Socket End Point.
     * @return New Alarm Socket End Point which handles the supplied Socket.
     */
    protected SocketEndPoint createSocketEndPoint(TraceableSocket p_socket)
    {
        AlarmSocketEndPoint     l_asep;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 10000, this,
                "Creating SocketEndPoint for socket " + p_socket);

        l_asep = new AlarmSocketEndPoint(
                i_logger,
                i_instrumentManager,
                p_socket,
                i_timeOut,
                i_alarmEventProcessor
                );
                
        return l_asep;
    }

} // End of public class AlarmServerEndPoint.

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////