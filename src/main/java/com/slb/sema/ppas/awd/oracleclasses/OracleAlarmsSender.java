////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       OracleAlarmsSender.Java
//    DATE            :       10-Oct-2007
//    AUTHOR          :       Eric Dangoor
//    REFERENCE       :       PRD_ASCS00_GEN_CA_122
//
//    COPYRIGHT       :       WM-data 2007
//
//    DESCRIPTION     :       This procedure is called by a PL/SQL procedure 
//                            that is loaded inside the Oracle database. It
//                            sends an AlarmP message to the ASCS ARM server.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME        | DESCRIPTION                      | REFERENCE
//----------+-------------+----------------------------------+------------------
// DD/MM/YY | <name>      | <brief description of change>    | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.oracleclasses;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;


/**
 * The OracleAlarmsSender class is stored within the Oracle database and its
 * static sendAlert method is published for use within Oracle PL/SQL scripts as
 * a Java stored procedure. This method sends an AlarmP message to the ASCS
 * ARM server.
 */
public class OracleAlarmsSender
{
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // class attributes
    //------------------------------------------------------------------------
    
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "OracleAlarmsSender";

    /** Boolean to check if the configuration is loaded. */
    private static boolean      c_configLoaded = false;
    
    /** List of server:port specifications. */
    private static ArrayList    c_connectionList;
    
    /** Boolean to indicate whether to output debug statements. */
    private static boolean      c_debugEnabled;
    
    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /**
     * This method retrieves values from properties files and sets up the 
     * class attributes.
     *
     * @throws Exception an exception
     */
    public static synchronized void init()
        throws Exception
    {
        Properties      l_properties;
        InputStream     l_inputStream;
        String          l_connectionProperty;
        int             l_connectionNo;
        String          l_connection = null;
        String          l_debugEnabled;

        l_properties = new Properties();

        // Properties files must be loaded into Oracle as a JAVA RESOURCE.
        // They are loaded into Oracle by the makeDb.xml/loadProperties target.
        // ASCS_SYSTEM_ROOT and ASCS_LOCAL_ROOT are not really part of the path, they 
        // are just used as identifiers for where the files originally came from.
        // Note that local area properties override system area properties.
        l_properties.load(OracleAlarmsSender
                          .class.getResourceAsStream("/ASCS_SYSTEM_ROOT/conf/awd/oracle_awd.properties"));
        l_inputStream = OracleAlarmsSender
                          .class.getResourceAsStream("/ASCS_LOCAL_ROOT/conf/awd/oracle_awd.properties");
        if (l_inputStream != null)
        {
            l_properties.load(l_inputStream);
            l_inputStream.close();
        }

        l_debugEnabled = l_properties.getProperty(
                "com.slb.sema.ppas.awd.oracleclasses.OracleAlarmsSender.debugEnabled");
        if (l_debugEnabled.equalsIgnoreCase("true") || l_debugEnabled.equalsIgnoreCase("enabled"))
        {
            c_debugEnabled = true;
        }
        else
        {
            c_debugEnabled = false;
        }

        l_connectionProperty = "com.slb.sema.ppas.awd.oracleclasses.OracleAlarmsSender.alarmServer";
        l_connectionNo = 0;
        c_connectionList = new ArrayList();
        while (l_connection != null || l_connectionNo == 0)
        {
            l_connectionNo++;
            l_connection = l_properties.getProperty(
                    l_connectionProperty.concat(Integer.toString(l_connectionNo)));
            if (l_connection != null)
            {
                c_connectionList.add(l_connection);
            }
        }
        
        c_configLoaded = true;

        return;
    }

    /**
     * Prints a debug message if debugging is switched on
     * 
     * @param p_message the message to print
     */
    private static void debugPrint(String p_message)
    {
        if (c_debugEnabled)
        {
            System.out.println(C_CLASS_NAME + ": " + p_message);
        }
    }

    /**
     * Sends a pre-formatted AlarmP message, including the information passed
     * in as parameters, to the ASCS ARM server.
     * 
     * @param p_host - the hostname
     * @param p_message - the alarm message
     * @param p_severity - the alarm severity
     * @param p_ruleName - the notification event rule name
     * 
     * @throws Exception an exception
     */    
    public static synchronized void sendAlert(
            String  p_host,
            String  p_message,
            String  p_severity,
            String  p_ruleName)
        throws Exception
    {
        boolean             l_connectionFound = false;
        String              l_connection;
        String              l_serverName;
        int                 l_portNumber;
        String              l_ruleName;
        String              l_eventText;
        String              l_message;
        Socket              l_socket = null;
        OutputStream        l_ostream = null;
        StackTraceElement[] l_elements;
        Exception           l_newEx;
        StringBuffer        l_stack;
        
        try
        {
            if (!c_configLoaded)
            {
                init();
            }

            // The rule name must be in upper case and certain special
            // characters in the rule name and event text must be converted
            // so that they will not confuse the receiving ARM process
            l_ruleName = p_ruleName.toUpperCase();
            l_ruleName = l_ruleName.replaceAll(" ", "_");
            l_ruleName = l_ruleName.replaceAll("%", "_");
            l_ruleName = l_ruleName.replaceAll("&", "_");
            
            l_eventText = "Host: " + p_host +
                            ", Rule: " + p_ruleName +
                            ", Severity: " + p_severity +
                            ", Message: " + p_message;
            l_eventText = l_eventText.replaceAll("%", "%25");
            l_eventText = l_eventText.replaceAll("&", "%26");
            
            for (int i = 0; l_connectionFound == false && i < c_connectionList.size(); i++)
            {
                l_connection = (String)c_connectionList.get(i);
                l_serverName = l_connection.replaceFirst(":.*", "");
                l_portNumber = Integer.parseInt(l_connection.replaceFirst(".*:", ""));

                try
                {
                    debugPrint("Connecting to " + l_serverName + ":" + l_portNumber);
                    l_socket = new Socket(l_serverName, l_portNumber);
                    debugPrint("Connected");
                    
                    l_connectionFound = true;
                    
                    // Move this connection to the top of the list so that it
                    // will be the first to be tried next time
                    if (i > 0)
                    {
                        c_connectionList.remove(i);
                        c_connectionList.add(0, l_connection);
                    }
                    
                    l_ostream = l_socket.getOutputStream();

                    l_message = "ARMPv001awpType=event&eventName=EXC.ORA." + l_ruleName +
                                "&raiseTime=" + System.currentTimeMillis() + 
                                "&eventText=" + l_eventText +
                                "&origNodeName=" + p_host +
                                "&&";
                    debugPrint("Sending message:" + l_message);
                    l_ostream.write(l_message.getBytes());
                    l_ostream.flush();
                    debugPrint("Message sent");
        
                    l_message = "ARMPv001awpType=close&&";
                    debugPrint("Sending message:" + l_message);
                    l_ostream.write(l_message.getBytes());
                    l_ostream.flush();
                    debugPrint("Message sent");
                }
                catch (IOException l_e)
                {
                    debugPrint("Connection failed");
                }
                finally
                {
                    if (l_socket != null)
                    {
                        debugPrint("Closing connection");
                        if (l_ostream != null)
                        {
                            l_ostream.close();
                        }
                        l_socket.close();
                        debugPrint("Connection closed");
                    }
                }
            }
            
            if ( ! l_connectionFound)
            {
                l_newEx = new Exception("Cannot find an alarm server to receive alert: "
                                        + l_eventText);
                throw l_newEx;
            }
        }
        catch (Exception l_e)
        {
            l_e.printStackTrace();
            
            // Add the stack trace to the exception message. This makes problems easier
            // to troubleshoot as in Oracle Enterprise Manager you only see the message
            // and not the stack trace.
            l_elements = l_e.getStackTrace();
            l_stack = new StringBuffer();
            for (int i = 0; i < l_elements.length; i++)
            {
                l_stack.append("|");
                l_stack.append(l_elements[i].toString());
            }
            l_newEx = new Exception(l_e.getMessage() + l_stack);
            
            throw l_newEx;
        }
        
        debugPrint("Exiting");
        
        return;
    }

    /**
     * The main method can be used to test the sendAlert method from outside of
     * the Oracle database.
     * 
     * @param argv - Array of arguments
     */
    public static void main(String argv[])
    {
        try
        {
            sendAlert("<HOST_NAME>",
                      "<MESSAGE_%&>",
                      "<SEVERITY_CODE>",
                      "<RULE NAME_%&>");
        }
        catch (Exception l_e)
        {
            l_e.printStackTrace();
        }
    }
}
