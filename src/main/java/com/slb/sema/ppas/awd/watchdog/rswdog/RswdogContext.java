////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswdogContext.java
//      DATE            :       01-Mar-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Rswdog context which extends AWD context.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.io.File;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.ConfigException;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * Rswdog context which extends AWD context. Adds support for an audit logger
 * to which a trace of all important events/actions are logged.
 */
public class RswdogContext extends AwdContext
{
    
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RswdogContext";
    
    /** The audit logger to use. */
    private Logger              i_auditLogger;

    /**
     * Creates a new Rswdog context. 
     */
    public RswdogContext()
    {
        super();
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
   /**
     * Initialises the context based on the supplied properties.
     * 
     * @param p_configProperties Properties to read configuration from.
     * @throws PpasException If initialisation fails.
     */    
    public void init(PpasProperties p_configProperties)
    throws PpasException
    {
        super.init(p_configProperties);
        
        String                  l_auditLoggerName =
                "AwdAuditLog_" + getProcessName();
        UtilProperties          l_auditLoggerProperties;
        String                  l_keyRoot;
        
        i_auditLogger = i_loggerPool.getLogger(l_auditLoggerName);
        if(i_auditLogger == null)
        {
            l_keyRoot = "com.slb.sema.ppas.util.Handler." + l_auditLoggerName;
            l_auditLoggerProperties = new UtilProperties();
            l_auditLoggerProperties.setProperty("com.slb.sema.ppas.util.Logger." +
                    l_auditLoggerName +".handlers",
                    l_auditLoggerName);
            // Extension defaults to .log, filename to handler name, directory to ., behaviour normal
            l_auditLoggerProperties.setProperty(l_keyRoot + ".directory",
                    System.getProperty("ascs.localRoot") + File.separator +
                    "log" + File.separator + "awd");
            // Have to set severities otherwise info and success not logged!
            l_auditLoggerProperties.setProperty(l_keyRoot + ".severities", "511");
            
            try
            {
                i_auditLogger = new Logger(
                        i_instrumentManager,
                        l_auditLoggerName,
                        l_auditLoggerProperties);
            }
            catch (ConfigException l_e)
            {
                // Log to normal logger and make normal logger the audit logger
                i_logger.logMessage(
                        new AwdConfigException(
                                C_CLASS_NAME, C_METHOD_init, 10100, this, (PpasRequest)null, (long)0,
                                AwdKey.get().errorInitAuditLogger(l_auditLoggerName), l_e
                                )
                        );
                i_auditLogger = i_logger;
            }
        }
    }
    
    /**
     * @return The Audit Logger to use.
     */
    public Logger getAuditLogger()
    {
        return i_auditLogger;
    }

} // End of public class RswdogContext.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////