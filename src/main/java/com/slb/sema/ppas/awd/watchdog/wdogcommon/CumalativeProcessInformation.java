////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CumalativeProcessInformation.java
//      DATE            :       06-Dec-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Holds data needed by watch dog about a single
//                              process, some of which is accumulated over time.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/09/06 | M.Vonka    | Fixed action count bugs         | PpaLon#2096/8999
//          |            | (added entryCount-- and changed |
//          |            | <= to <).                       |
//          |            | Also added jaavdoc.             |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.slb.sema.ppas.util.support.Debug;

/**
 * Holds data needed by watch dog about a single
 * process, some of which is accumulated over time. Also maintains a
 * history of actions for any actions which have a maxActionCount
 * configured.
 * <p/>
 * TODO 3 - needs minor updating for further data to be held properly.
 */
public class CumalativeProcessInformation
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "CumalativeProcessInformation";

    /** Empty array used in calls to Collection.toArray */
    private static String C_EMPTY_STRING_ARRAY[] =
            new String[0];

    /** The name of the process (this is a derived name). */
    private String              i_processName;
    
    /** Map of maintained data about process. */
    private HashMap             i_fieldMap;
    
    /** Previous snapshot of process information. */
    private ProcessInformation  i_previousInformation;
    
    /** The derived state of the proces. One of
     * {@link ProcessCondition#C_STATE_ENUM_STARTED},
     * {@link ProcessCondition#C_STATE_ENUM_RUNNING},
     * {@link ProcessCondition#C_STATE_ENUM_STOPPED},
     * {@link ProcessCondition#C_STATE_ENUM_NOT_RUNNING}.
     */
    private int                 i_cumalativeState = 0;
    
    /** Maps WatchdogAction objects to ActionHistory objects. */
    private HashMap             i_actionHistoriesHM = new HashMap();
    
    /**
     * Creates a new Cumalative Process Infromation instance.
     * @param  p_processName   name of process information is for.
     */
    public CumalativeProcessInformation(
        String                  p_processName)
    {
        super();
        
        i_processName = p_processName;
        i_fieldMap = new HashMap();
    }

    /**
     * Updates the process data (including accumulated data) based on a new
     * snapshot of process information.
     * @param p_processInformation 
     */
    public void update(
        ProcessInformation      p_processInformation
    )
    {
        if(p_processInformation == null)
        {
            p_processInformation = new ProcessInformation(i_processName);
            p_processInformation.setField(ProcessCondition.C_FIELD_STATE,
                    ProcessCondition.C_STATE_NOT_RUNNING);
            setField(
                    ProcessCondition.C_FIELD_STATE,
                    ProcessCondition.C_STATE_NOT_RUNNING);
        }
        else
        {
            setField(
                    ProcessCondition.C_FIELD_STATE,
                    ProcessCondition.C_STATE_RUNNING);
        }
        
        if(i_cumalativeState == 0)
        {
                i_cumalativeState = p_processInformation.getStateEnum();
                setField(
                        ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                        ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
        }
        else
        {
            switch(i_cumalativeState)
            {
            case ProcessCondition.C_STATE_ENUM_RUNNING :
                switch(p_processInformation.getStateEnum())
                {
                case ProcessCondition.C_STATE_ENUM_NOT_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_STOPPED;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;
                }
                break;
                           
            case ProcessCondition.C_STATE_ENUM_STARTED :
                switch(p_processInformation.getStateEnum())
                {
                case ProcessCondition.C_STATE_ENUM_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_RUNNING;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;                    
                case ProcessCondition.C_STATE_ENUM_NOT_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_STOPPED;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;                    
                }
                break;
                
            case ProcessCondition.C_STATE_ENUM_STOPPED :
                switch(p_processInformation.getStateEnum())
                {
                case ProcessCondition.C_STATE_ENUM_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_STARTED;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;                    
                case ProcessCondition.C_STATE_ENUM_NOT_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_NOT_RUNNING;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;                    
                }
                break;

            case ProcessCondition.C_STATE_ENUM_NOT_RUNNING :
                switch(p_processInformation.getStateEnum())
                {
                case ProcessCondition.C_STATE_ENUM_RUNNING :
                    i_cumalativeState = ProcessCondition.C_STATE_ENUM_STARTED;
                    setField(
                            ProcessCondition.C_FIELD_CUMALATIVE_STATE,
                            ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
                    break;                    
                }
                break;
                
            }
        }

        i_previousInformation = p_processInformation;
    }

    /**
     * Returns the process name.
     * @return Returns the process name.
     */
    public String getProcessName()
    {
        return i_processName;
    }

    /**
     * Returns the process state as a symbolic name. 
     * @return Returns the process state as a symbolic name.
     */
    public String getState()
    {
        return i_previousInformation.getState();
    }

    /**
     * Returns the process state as an integer (enum).
     * @return Returns the process state as an integer (enum).
     */
    public int getStateEnum()
    {
        return i_previousInformation.getStateEnum();
    }

    /**
     * Returns <bold>true</bold> if the process has (just) started.
     * @return Returns <bold>true</bold> if the process has (just) started.
     */
    public boolean hasStarted()
    {
        boolean                 l_hasStarted = false;
        
        if(i_cumalativeState == ProcessCondition.C_STATE_ENUM_STARTED)
        {
            l_hasStarted = true;
        }
        
        return l_hasStarted;
    }

    /**
     * Returns <bold>true</bold> if the process is (still) running.
     * @return Returns <bold>true</bold> if the process is (still) running.
     */
    public boolean isRunning()
    {
        boolean                 l_isRunning = false;
        
        if(i_cumalativeState == ProcessCondition.C_STATE_ENUM_RUNNING)
        {
            l_isRunning = true;
        }
        
        return l_isRunning;
    }

    /**
     * Returns <bold>true</bold> if the process has (just) stopped
     * @return Returns <bold>true</bold> if the process has (just) stopped.
     */
    public boolean hasStopped()
    {
        boolean                 l_hasStopped = false;
        
        if(i_cumalativeState == ProcessCondition.C_STATE_ENUM_STOPPED)
        {
            l_hasStopped = true;
        }
        
        return l_hasStopped;
    }

    /**
     * Returns <bold>true</bold> if the process is (still) not running.
     * @return Returns <bold>true</bold> if the process is (still) not running.
     */
    public boolean isNotRunning()
    {
        boolean                 l_isNotRunning = false;
        
        if(i_cumalativeState == ProcessCondition.C_STATE_ENUM_NOT_RUNNING)
        {
            l_isNotRunning = true;
        }
        
        return l_isNotRunning;
    }

    /**
     * Returns the requested information about the process.
     * @param p_fieldName       field name of information requested.
     * @return Returns the requested information about the process.
     */
    public Object getField(
        String                  p_fieldName
    )
    {
        return(i_fieldMap.get(p_fieldName));
    }

    /**
     * Sets the specified information about the process.
     * @param p_fieldName       field name of information to set.
     * @param p_fieldValue      value to set to.
     */
    public void setField(
        String                  p_fieldName,
        Object                  p_fieldValue
    )
    {
        i_fieldMap.put(p_fieldName, p_fieldValue);
    }
    
    /**
     * Returns whether the requested action should be performed.
     * @param p_action          action to check.
     * @return whether the requested action should be performed.
     */
    public boolean performAction(WatchdogAction p_action)
    {
        boolean                 l_performAction = true;
        ActionHistory           l_actionHistory;
        
        if(p_action.getMaxActionCount() > -1)
        {
            // Max action count not infinite, so track action count.
            synchronized(i_actionHistoriesHM)
            {
                l_actionHistory = (ActionHistory)i_actionHistoriesHM.get(p_action);
                if(l_actionHistory == null)
                {
                    l_actionHistory = new ActionHistory();
                    i_actionHistoriesHM.put(p_action, l_actionHistory);
                }
                l_performAction = l_actionHistory.performAction(p_action);
            }
        }
        
        return l_performAction;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        int                     l_loop;
        String                  l_fieldNameARR[];
        
        l_sb = new StringBuffer(160);
        
        l_sb.append(C_CLASS_NAME + "=[processName=");
        l_sb.append(i_processName);
        l_sb.append(",cumalativeState=");
        l_sb.append(i_cumalativeState);
        l_sb.append("(=");
        l_sb.append(ProcessCondition.C_STATE_TOSTRING.get(new Integer(i_cumalativeState)));
        l_sb.append(")");
        l_fieldNameARR = (String[])i_fieldMap.keySet().toArray(C_EMPTY_STRING_ARRAY);
        l_sb.append(",fieldMap=[");
        for(l_loop = 0; l_loop < l_fieldNameARR.length; l_loop++)
        {
            if(l_loop > 0)
            {
                l_sb.append(",");
            }
            l_sb.append("name=");
            l_sb.append(l_fieldNameARR[l_loop]);
            l_sb.append(",value=");
            l_sb.append(i_fieldMap.get(l_fieldNameARR[l_loop]));
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

    /**
     * Private inner class implementing a history for when an action was performed.
     */
    private class ActionHistory
    {
        /**
         * Interval over which maximum number of action should not be
         * performed.
         */
        private ArrayList       i_actionTimes;
        
        /** The last time a removal check was performed. */
        private long            i_lastRemoveOldTime = 0;
        
        /** Creates a new Action History. */
        public ActionHistory()
        {
            i_actionTimes = new ArrayList();
        }

        /**
         * Returns whether the requested action should be performed.
         * @param p_action          action to check.
         * @return Whether the requested action should be performed.
         */
        public boolean performAction(WatchdogAction p_action)
        {
            boolean             l_performAction = false;
            int                 l_maxActionCount;
            
            synchronized(i_actionTimes)
            {
                removeOldTimes(p_action);
                l_maxActionCount = p_action.getMaxActionCount();
                if(i_actionTimes.size() < l_maxActionCount)
                {
                    l_performAction = true;
                    i_actionTimes.add(new Long(System.currentTimeMillis()));
                }
            }
            Debug.print(
                    Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 20190, this,
                    "performAction: finished, actionTimes=" + this + " (" + this.hashCode() + ")");
            
            return l_performAction;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            return "ActionHistory=[actionTimes.size=" + i_actionTimes.size() + "]";
        }

        /**
         * Removes any old (no longer relevant) times that the action was
         * performed.
         * @param p_action      the action.
         */
        private void removeOldTimes(WatchdogAction p_action)
        {
            int                l_entryCount;
            int                l_loop;
            Long               l_timeLong;
            long               l_cutOff;
            long               l_currentTime;
            boolean            l_done = false;
            long               l_actionCountLifetimeMillis;

            l_actionCountLifetimeMillis = p_action.getActionCountLifetimeMillis();
            if(l_actionCountLifetimeMillis > -1)
            {
                // Lifetime not infinite, so check
                // TODO: Make more efficient - use simpler count if
                // timeout infinite, rather than ArrayList of times,
                // also sort out 0 lifetime.

                // Prevent excessive removal checking - do max every 5s.
                l_currentTime = System.currentTimeMillis(); 
                if(l_currentTime >  (i_lastRemoveOldTime + 5000))
                {
                    i_lastRemoveOldTime = l_currentTime;
                    l_entryCount = i_actionTimes.size();
                    l_cutOff = l_currentTime -
                            l_actionCountLifetimeMillis;
        
                    if(Debug.on)
                    {
                        Debug.print(
                                Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                                Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                                "Removing old times, before=" + this + ", cutoff=" +
                                new Date(l_cutOff) + " (" + this.hashCode() + ")");
                    }
                    
                    for(l_loop = 0; !l_done && (l_loop < l_entryCount); l_loop++)
                    {
                        l_timeLong = (Long)i_actionTimes.get(l_loop);
                        if(l_timeLong.longValue() < l_cutOff)
                        {
                            i_actionTimes.remove(l_loop);
                            l_loop--;
                            l_entryCount--;
                        }
                        else
                        {
                            l_done = true;
                        }
                    }
        
                    if(Debug.on)
                    {
                        Debug.print(
                                Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                                Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                                "Removed old times, after=" + this  + " (" + this.hashCode() + ")");
                    }
                }
            }
        }
    }

} // End of public class CumalativeProcessInformation.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
