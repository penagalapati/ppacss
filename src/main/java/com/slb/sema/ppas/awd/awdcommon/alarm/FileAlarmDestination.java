////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FileAlarmDestination.java
//      DATE            :       25-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       File destination for alarms.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * File destination for alarms. Defines all attributes of the file to be used
 * as a destination for alarms.
 */
public class FileAlarmDestination
extends AlarmDestination
{

    /**
     * Path and file name of file that this destination represents.
     */
    private String              i_pathAndFilename;

    /**
     * Creates a new File Alarm Destination.
     * 
     * @param  p_pathAndFilename path and file name of the file.
     */
    public FileAlarmDestination(
        String                  p_pathAndFilename
    )
    {
        super(C_TYPE_FILE);
        
        i_pathAndFilename = p_pathAndFilename;
    }

    /**
     * Returns the path and file name of file that this destination represents.
     * 
     * @return Path and file name of the file.
     */
    public String getPathAndFilename()
    {
        return i_pathAndFilename;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( "FileAlarmDestination=[pathAndFilename=" + i_pathAndFilename +
                ", " + super.toString() + "]");
    }


} // End of public class FileAlarmDestination

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////