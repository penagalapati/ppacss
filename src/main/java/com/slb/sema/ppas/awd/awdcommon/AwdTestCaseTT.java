////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmServer.java
//      DATE            :       08-Feb-2006
//      AUTHOR          :       Marek Vonka
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Super class for AWD Test Case classes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/05/07 | R.Grimshaw | Moved methods to start and stop | PpacLon#3116
//          |            | processes to CommonTestCaseTT.  |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon;

import com.slb.sema.ppas.common.support.CommonTestCaseTT;

/**
 * Super class for AWD Test Case classes. Implements some common code and
 * also some useful untility methods.
 */
public class AwdTestCaseTT extends CommonTestCaseTT
{

    /**
     * Creates a new AWD Test Case.
     * @param p_name            name of test.
     * @param p_flags           flags used to initialize the
     *                          <code>PpasConext</code>.
     */
    public AwdTestCaseTT(String p_name, long p_flags)
    {
        super(p_name, p_flags);
    }

    /**
     * Creates a new AWD Test Case.
     * @param p_name            name of test.
     */
    public AwdTestCaseTT(String p_name)
    {
        super(p_name);
    }
    
    /** Performs standard setUp stuff. */
    protected void setUp()
    {
        super.setUp(0);
        
    }

   

} // End of public class AwdTestCaseTT

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
