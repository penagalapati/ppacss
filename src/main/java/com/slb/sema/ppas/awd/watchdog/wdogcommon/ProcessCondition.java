/*
 * Created on 06-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.HashMap;

import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessCondition
{

    private static final String C_CLASS_NAME  = "ProcessCondition";

    public static final int     C_COND_EQUALS = 1;
    
    public static final String  C_FIELD_STATE = "STATE";
    public static final String  C_FIELD_CUMALATIVE_STATE = "CUMALATIVE_STATE";
    public static final String  C_STATE_STARTED = "STARTED";
    public static final String  C_STATE_RUNNING = "RUNNING";
    public static final String  C_STATE_STOPPED = "STOPPED";
    public static final String  C_STATE_NOT_RUNNING = "NOT_RUNNING";

    public static final int     C_STATE_ENUM_STARTED = 1;
    public static final int     C_STATE_ENUM_RUNNING = 2;
    public static final int     C_STATE_ENUM_STOPPED = 3;
    public static final int     C_STATE_ENUM_NOT_RUNNING = 4;
    
    private static final HashMap C_CONDITION_TOSTRING = new HashMap();
 
    private static final HashMap C_CONDITION_TOENUM = new HashMap();

    /* package */ static final HashMap C_STATE_TOENUM = new HashMap();

    /* package */ static final HashMap C_STATE_TOSTRING = new HashMap();
    
    static {
        C_CONDITION_TOSTRING.put(
                new Integer(C_COND_EQUALS), "EQUALS");

        C_CONDITION_TOENUM.put(
                "EQUALS", new Integer(C_COND_EQUALS));

        C_STATE_TOENUM.put(
                C_STATE_STARTED, new Integer(C_STATE_ENUM_STARTED));
        C_STATE_TOENUM.put(
                C_STATE_RUNNING, new Integer(C_STATE_ENUM_RUNNING));
        C_STATE_TOENUM.put(
                C_STATE_STOPPED, new Integer(C_STATE_ENUM_STOPPED));
        C_STATE_TOENUM.put(
                C_STATE_NOT_RUNNING, new Integer(C_STATE_ENUM_NOT_RUNNING));

        C_STATE_TOSTRING.put(
                new Integer(C_STATE_ENUM_STARTED), C_STATE_STARTED);
        C_STATE_TOSTRING.put(
                new Integer(C_STATE_ENUM_RUNNING), C_STATE_RUNNING);
        C_STATE_TOSTRING.put(
                new Integer(C_STATE_ENUM_STOPPED), C_STATE_STOPPED);
        C_STATE_TOSTRING.put(
                new Integer(C_STATE_ENUM_NOT_RUNNING), C_STATE_NOT_RUNNING);
    }

    // e.g. field=STATE, condition=EQUALS and value="NOT_RUNNING"    
    private String              i_fieldName;
    private int                 i_condition;
    private Object              i_fieldValue;

    public ProcessCondition(
        String                  p_fieldName,
        int                     p_condition,
        Object                  p_fieldValue
    )
    {
        super();
        
        i_fieldName = p_fieldName;
        i_condition = p_condition;
        i_fieldValue = p_fieldValue;
    }

    public ProcessCondition(
        String                  p_fieldName,
        String                  p_condition,
        Object                  p_fieldValue
    )
    throws IllegalArgumentException
    {
        super();
        
        Integer                 l_integer;
        
        i_fieldName = p_fieldName;
        l_integer = (Integer)C_CONDITION_TOENUM.get(p_condition);
        if(l_integer == null)
        {
            throw new IllegalArgumentException("Unknown process condition " + p_condition);
        }
        i_condition = l_integer.intValue();
        i_fieldValue = p_fieldValue;
    }
    
    public boolean checkCondition(
        CumalativeProcessInformation p_processInformation
    )
    {
        Object                  l_fieldValue;
        boolean                 l_met = false;

        if(p_processInformation == null)
        {
            if(i_fieldName.equals(C_FIELD_STATE) &&
                    i_fieldValue.equals(C_STATE_NOT_RUNNING))
            {
                l_met = true;
            }
        }
        else
        {        
            l_fieldValue = p_processInformation.getField(i_fieldName);
            switch(i_condition)
            {
            case C_COND_EQUALS:
                l_met = i_fieldValue.equals(l_fieldValue);
                break;
            default:
                Exception l_e = new RuntimeException(
                        "Unexpected condition (field=" + i_fieldName +
                        ", condition=" + i_condition + ", value=" + i_fieldValue +
                        ")");
                l_e.printStackTrace();
                l_met = false; 
            }
        }
        
        if(l_met)
        {
            if(Debug.on) Debug.print(
                    Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE, C_CLASS_NAME, 11000, this,
                    "Process " + ((p_processInformation != null) ? p_processInformation.getProcessName() : "null") +
                    " has met condition " + this);                
        }
                    
        return l_met;
    }
    
    
    public String toString()
    {
        return "ProcessCondition=[fieldName=" + i_fieldName + ", condition=" +
                C_CONDITION_TOSTRING.get(new Integer(i_condition)) +
                ", fieldValue=" + i_fieldValue + "]";
    }

    /**
     * Returns the condition.
     * @return                  The condition.
     */
    public int getCondition()
    {
        return i_condition;
    }

    /**
     * Returns the field name.
     * @return                  The field name.
     */
    public String getFieldName()
    {
        return i_fieldName;
    }

    /**
     * Returns the field value.
     * @return                  The field value.
     */
    public Object getFieldValue()
    {
        return i_fieldValue;
    }

}
