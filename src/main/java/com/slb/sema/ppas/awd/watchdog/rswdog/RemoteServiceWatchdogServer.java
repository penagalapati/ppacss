////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RemoteServiceWatchdogServer.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Remote Service Watchdog Server including
//                              bootstrap for it.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.util.HashMap;

import com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Remote Service Watchdog Server including bootstrap for it.
 * The Remote Service Watchdog Server (rswdog) monitors a number of
 * remote "services".
 * <p/>
 * The services to monitor are configurable, the method of monitoring is
 * configurable (from a set of "monitors") and the actions performed are
 * configurable.
 * <p/>
 * A typical example configuration is to monitor a remote GUI service using
 * a gui monitor (a monitor which sends something to the GUI and checks the
 * response returned). The actions are configured such that if the service fails
 * or times out or hangs a number of times, rswdog raises an alarm, logs an event
 * and runs an OS script to bring up the service's VIP on the local node. It is also
 * (hardcoded) to attempt to send a release request to the rswdog on the remote
 * server to ask it to "release" (bring down) the services VIP.
 * <p/> 
 * Alarms can be sent to
 * multiple destinations, in multiple formats using multiple delivery methods -
 * see {@link com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDeliveryManager
 * AlarmDeliveryManager}.
 * <p/>
 * The alarm server monitors (polls) the local state of sevice VIPs and also
 * polls remote rswdog's for the state of the service VIPs on their server.
 */
public class RemoteServiceWatchdogServer extends AwdAbstractServer
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RemoteServiceWatchdogServer";

    /** RS watchdog processor which performs the RS watchdog processing. */
    private RemoteServiceWatchdogProcessor      i_rswdogProcessor;
    
    /**
     * Creates a new Remote Service Watchdog Server.
     */
    public RemoteServiceWatchdogServer()
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Performs the Remote Service Watchdog Server specific initialisation. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()} method.
     * @throws PpasException exception occurred during initialisation.
     */    
    public void doInit()
    throws PpasException
    {
        
        i_rswdogProcessor = new RemoteServiceWatchdogProcessor(
                i_logger,
                i_instrumentManager,
                (RswdogContext)i_awdContext,
                ((RswdogContext)i_awdContext).getAuditLogger()
                );

        i_rswdogProcessor.init(i_configProperties);
                
        return;
    }

    /**
     * Performs the Remote Service Watchdog Server specific startup. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start} method.
     */
    public void doStart()
    {
        i_rswdogProcessor.start();
    }

    
    /**
     * Performs the Remote Service Watchdog Server specific stop (shutdown) processing. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#stop} method.
     */    
    public void doStop()
    {
        i_rswdogProcessor.stop();
    }

    /**
     * Main bootstrap method for the Remote Service Watchdog Server. Instantiates a new
     * <code>RemoteServiceWatchdogServer</code>, calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()}
     * and then calls
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start()} on it.
     * 
     * @param  p_argsARR      command line arguments - none currently.
     */
    public static void main(String[] p_argsARR)
    {
        RemoteServiceWatchdogServer          l_watchdogServer;

        // Debug.setDebugFullToSystemErr();
        
        try
        {
            l_watchdogServer = new RemoteServiceWatchdogServer();
            l_watchdogServer.init(new RswdogContext());
            l_watchdogServer.start();
        }
        catch(Exception l_e)
        {
            System.err.println(
                    "Watchdog Server startup exiting due to exception : ");
            l_e.printStackTrace();
            System.exit(1);
        }
    }
    
    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_processManagementCommand = "processManagementCommand";
    /**
     * Handles the Remote Service Watchdog Server specific management commands.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String). 
     * @return Result string or <code>null</code>.
     */
    protected final String processManagementCommand(
        String                  p_command,
        HashMap                 p_parametersHM
        )
    {
        String                  l_return = null;

        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 20100, this,
                C_METHOD_processManagementCommand + ": processing management command=[" +
                p_command + ", params=" + p_parametersHM + "]");
        
        if("RELEASE_SERVICE".equalsIgnoreCase(p_command))
        {
            l_return = handleReleaseServiceCommand(p_command, p_parametersHM);
        }
        else if("FAILOVER_SERVICE".equalsIgnoreCase(p_command))
        {
            l_return = handleFailoverServiceCommand(p_command, p_parametersHM);
        }
        else if("FAILOVER_ALL_SERVICES_TO_THIS_SERVER".equalsIgnoreCase(p_command))
        {
            l_return = handleFailoverAllServicesToThisServer(p_command, p_parametersHM, true);
        }
        else if("FAILOVER_ALL_SERVICES_FROM_THIS_SERVER".equalsIgnoreCase(p_command))
        {
            l_return = handleFailoverAllServicesToThisServer(p_command, p_parametersHM, false);
        }
        else if("LIST_SERVICES".equalsIgnoreCase(p_command))
        {
            l_return = handleListServicesCommand(p_command, p_parametersHM);
        }

        if (PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_END | PpasDebug.C_ST_APPLICATION_DATA,
                C_CLASS_NAME, 20190, this,
                C_METHOD_processManagementCommand + ": result=[" +
                l_return + "] for command=[" +
                p_command + ", params=" + p_parametersHM + "]");

        return l_return;
    }
    
    /**
     * Handles (processes) a release service command management.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String).
     *                          Mandatory "service" parameter expected defining
     *                          service to release. 
     * @return Result string or <code>null</code>.
     */
    private String handleReleaseServiceCommand(
        String                  p_command,
        HashMap                 p_parametersHM
        )
    {
        String                  l_serviceName;
        String                  l_return;
        
        l_serviceName = (String)p_parametersHM.get("service");
        if(l_serviceName == null || "".equals(l_serviceName))
        {
            l_return = p_command + " failed - missing 'service' parameter";
        }
        else
        {
            l_return = i_rswdogProcessor.releaseService(l_serviceName);
            if(l_return == null)
            {
                l_return = p_command + " successed for service " + l_serviceName;
            }
        }
        
        return l_return;
    }

    /**
     * Handles (processes) a failover service management command.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String).
     *                          Mandatory "service" parameter expected defining
     *                          service to release. 
     * @return Result string or <code>null</code>.
     */
    private String handleFailoverServiceCommand(
        String                  p_command,
        HashMap                 p_parametersHM
        )
    {
        String                  l_serviceName;
        String                  l_return;
        
        l_serviceName = (String)p_parametersHM.get("service");
        if(l_serviceName == null || "".equals(l_serviceName))
        {
            l_return = p_command + " failed - missing 'service' parameter";
        }
        else
        {
            l_return = i_rswdogProcessor.failoverService(l_serviceName);
            if(l_return == null)
            {
                l_return = p_command + " successed for service " + l_serviceName;
            }
        }
        
        return l_return;
    }

    /**
     * Handles (processes) a failover all services management command.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String).
     *                          No parameters expected. 
     * @param p_toThisServer    if <code>true</code> failover TO this server,
     *                          if <code>false</code> failover FROM this server.
     * @return Result string or <code>null</code>.
     */
    private String handleFailoverAllServicesToThisServer(
        String                  p_command,
        HashMap                 p_parametersHM,
        boolean                 p_toThisServer
        )
    {
        String                  l_return;
        
        if(p_toThisServer)
        {
            l_return = i_rswdogProcessor.failoverAllServicesToThisServer();
        }
        else
        {
            l_return = i_rswdogProcessor.failoverAllServicesFromThisServer();            
        }
        
        return l_return;
    }

    /**
     * Handles (processes) a list services management command.
     * @param  p_command        Management command.
     * @param  p_parametersHM   Map of parameter keys (String) to values (String).
     *                          No parameters expected for this message type. 
     * @return Result string or <code>null</code>.
     */
    private String handleListServicesCommand(
        String                  p_command,
        HashMap                 p_parametersHM
        )
    {
         return i_rswdogProcessor.listServices();
    }

} // End of public class RemoteServiceWatchdogServer.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////