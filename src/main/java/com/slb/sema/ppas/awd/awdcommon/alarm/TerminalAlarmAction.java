/*
 * Created on 28-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TerminalAlarmAction extends AlarmAction
{
    public TerminalAlarmAction()
    {
        super(AlarmAction.C_TYPE_TERMINAL);
    }

    public String toString()
    {
        return("TerminalAlarmAction=[" + super.toString() + "]");
    }

}
