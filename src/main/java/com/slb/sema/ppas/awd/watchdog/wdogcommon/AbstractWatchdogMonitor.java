////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AbstractWatchdogMonitor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Abstract super class for Monitors which
//                              implements some of the
//                              most common requirements for Monitors.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/13/06 | M.Vonka    | Added isRunning method.         | PpacLon#2644/10214
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Abstract super class for Monitors which
 * implements some of the
 * most common requirements for Monitors.
 * <p/>
 * Derieved classes must support being started, then stopped and then started again.
 * <p/>
 * Loads the following configuration parameters (which may or may not be used by derived classes):
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>enabled</td>
 *     <td>Indicates if monitor is enabled.</td>
 *     <td>O</td>
 *     <td>true</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a "ping" fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>connectTimeoutMillis</td>
 *     <td>Milliseconds to wait for connection to succeed before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for response to be received before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 * </table> 
 */
public abstract class AbstractWatchdogMonitor implements WatchdogMonitor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AbstractWatchdogMonitor";

    /** Sequence number for monitor used to make such things as the monitor thread name unique. */
    private static int          c_instanceSequeneceNo = 0;

    /** Logger to log any message to. */
    protected Logger            i_logger;
    
    /** The name of the monitored service. */
    protected String            i_monitoredServiceName;
    
    // TODO 3 Ideally move up a few objects but would need another thread
    /**
     * Delay (millis) before monitor actually starts monitoring (after
     * being started). This can
     * be useful to allow a time at start up for other things to start up
     * (e.g. the monitored services themselves). Must be 0 or a positive
     * number (0 means don't wait). Currently default is
     * 30000 (30s).
     */
    private long                i_startDelayMillis = 30 * 1000;

    /**
     * Interval (millis) between monitor tests. Must be 0 or a positive
     * number (0 means don't wait). Currently default is 10000 (10s).
     */
    private int                 i_monitorIntervalMillis = 10 * 1000;
    
    /**
     * Total number of attempts to perform if failure occurs before deciding
     * HTTP server has failed. Currently default is 3.
     */
    protected int               i_retryLimit = 3;

    /**
     * Interval (millis) between retry attempts. Must be 0 or a positive
     * number (0 means don't wait). Currently default is 5000 (5s).
     */
    protected int               i_retryMillis = 5 * 1000;

    /** Timeout (millis) on waiting to make a connection. Currently default is 10000 (10s). */
    protected int               i_connectTimeoutMillis = 10 * 1000;

    /** Timeout (millis) on waiting to receive a response. Currently default is 10000 (10s).*/
    protected int               i_readTimeoutMillis = 10 * 1000;

    /** The name of the Monitor thread. */
    private String              i_threadName;
    
    /** ArrayList of WatchdogMonitorListener. */
    private ArrayList           i_listenerAL;
    
    /** The monitor thread which performs the monitoring. */
    private MonitorThread       i_monitorThread;
    
    /** Status of the monitor (NOT_RUNNING, STARTING, PASSED, FAILED).*/
    private int                 i_state = C_STATE_NOT_RUNNING;
    
    /** Inidicates if Monitor is enabled. */
    private boolean             i_enabled = true;

    /**
     * Creates a new Abstract Monitor. Monitor listeners can be added later using
     * {@link #addMonitorListener(WatchdogMonitorListener)}.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public AbstractWatchdogMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        this(p_logger, p_monitoredServiceName, (WatchdogMonitorListener)null);
    }

    /**
     * Creates a new Abstract Monitor and adds the supplied monitor listener.
     * Additional monitor listeners can be added later using
     * {@link #addMonitorListener(WatchdogMonitorListener)}.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     * @param p_listener        monitor listener to add.
     */
    public AbstractWatchdogMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName,
        WatchdogMonitorListener p_listener
        )
    {
        i_logger = p_logger;
        i_monitoredServiceName = p_monitoredServiceName;
        synchronized(this)
        {
            c_instanceSequeneceNo++;
            i_threadName = "MonThd-" + c_instanceSequeneceNo;
        }
        i_listenerAL = new ArrayList();
        if(p_listener != null)
        {
            i_listenerAL.add(p_listener);
        }
    }
    
    /**
     * Initialises the monitor from the supplied configuration parameters.
     * @param p_configParamMap  config params - map of param name (String) to param value (String).
     * @throws Exception        exception occurred during initilisation.
     */
    public final void init(Map p_configParamMap)
    throws Exception
    {
        String                  l_param;
        
        l_param = (String)p_configParamMap.get("startDelayMillis");
        if(l_param != null)
        {
            i_startDelayMillis = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("monitorIntervalMillis");
        if(l_param != null)
        {
            i_monitorIntervalMillis = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("retryLimit");
        if(l_param != null)
        {
            i_retryLimit = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("retryMillis");
        if(l_param != null)
        {
            i_retryMillis = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("connectTimeoutMillis");
        if(l_param != null)
        {
            i_connectTimeoutMillis = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("readTimeoutMillis");
        if(l_param != null)
        {
            i_readTimeoutMillis = Integer.parseInt(l_param);
        }
        l_param = (String)p_configParamMap.get("enabled");
        if(     "false".equalsIgnoreCase(l_param) ||
                "off".equalsIgnoreCase(l_param) ||
                "no".equalsIgnoreCase(l_param) ||
                "0".equalsIgnoreCase(l_param) ||
                "n".equalsIgnoreCase(l_param)
                )
        {
            i_enabled = false;
        }

        // Performed derived class initialization
        doInit(p_configParamMap);
    }
    
    /**
     * Performs any Monitor specific initalisation based on the
     * supplied configation parameters.
     * @param p_configParamMap  config params - map of param name (String) to param value (String).
     * @throws Exception        exception occurred during initilisation.
     */
    protected abstract void doInit(Map p_configParamMap)
    throws Exception;


    /**
     * Adds the supplied monitor listener.
     * @param p_listener        monitor listener to add.
     */
    public void addMonitorListener(
        WatchdogMonitorListener p_listener
        )
    {
        i_listenerAL.add(p_listener);
    }

    /**
     * Removes the supplied monitor listener.
     * @param p_listener        monitor listener to remove.
     * @return <code>true</code> if listener was present (and so removed), <code>false</code> otherwise.
     */
    public synchronized boolean removeMonitorListener(
        WatchdogMonitorListener p_listener
        )
    {
        return i_listenerAL.remove(p_listener);
    }

    /**
     * Starts the monitor. A monitor can subsequently be stopped and then started again if required.
     */
    public synchronized void start()
    {
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11100, this,
                "Starting");

        if(i_enabled && (i_monitorThread == null))
        {
            i_state = C_STATE_STARTING;
            i_monitorThread = new MonitorThread();
            i_monitorThread.start();
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11190, this,
                    "Started");
        }
        else
        {
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11195, this,
                    "Not started, either already running or disabled.");
        }

    }

    /**
     * Stops the monitor. The monitor can be started again if required.
     */
    public synchronized void stop()
    {
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11200, this,
                "Stopping");
        
        if(i_monitorThread != null)
        {
            try
            {
                i_monitorThread.stop(30000);
            }
            catch (InterruptedException l_e)
            {
                // We tried - ignore and continue
            }
            i_monitorThread = null;
            i_state = C_STATE_NOT_RUNNING;
        }
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11290, this,
                "Stopped (or was already not running).");

    }
    
    /**
     * Sets the enabled state of the monitor. This does not start or stop the monitor!
     * @param p_enable          enabled state to set.
     */
    public void enable(boolean p_enable)
    {
        i_enabled = p_enable;
    }
    
    /**
     * Returns whether the monitor is currently running.
     * @return Returns whether the monitor is currently running.
     */
    public synchronized boolean isRunning()
    {
        boolean                 l_isRunning = false;
        if(i_monitorThread != null)
        {
            l_isRunning = i_monitorThread.isRunning();
        }
        
        return l_isRunning;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(160);
        l_sb.append(C_CLASS_NAME);
        l_sb.append("=[monitorName=");
        l_sb.append(i_monitoredServiceName);
        l_sb.append(", enabled=");
        l_sb.append(i_enabled);
        l_sb.append(", state=");
        l_sb.append(i_state);
        l_sb.append("]");

        return l_sb.toString();
    }

    /**
     * Perform a monitor. This should either not block (and so perform
     * a single monitoring iteration) or if it can block for a time it
     * should support being "interrupted" (so can be stopped cleanly) by the
     * monitor thread being interrupted. Any looping should check
     * if the monitor is being stopped by calling {@link #isStopping()}.
     */
    abstract protected void performMonitorIteration();

    /**
     * Called by derived classes each time the monitor performs a
     * "test" or monitor detects either passed or failed.
     * <p/>
     * If the monitor has passed:<br/>
     * If the current state is failed, all listeners are notified
     * that the monitor has entered the passed state.<br/>
     * Otherwise no notifications are sent. 
     * <p/>
     * If the monitor has failed:<br/>
     * If the current state is NOT failed, all listeners are notified
     * that the monitor has entered the failed state.<br/>
     * Otherwise no notifications are sent.
     * @param p_passed          boolean indicating if monitor has passed or failed a "test".
     */
    protected void passed(boolean p_passed)
    {
        // On initiailization, passed is viewed as the default, so
        // a change to failed is always notified, whereas a change to passed
        // is only notified if going from a failed state.
        if(p_passed)
        {
            if(i_state == C_STATE_FAILED)
            {
                // Passed - and state changed, notify (ordering is important)
                i_state = C_STATE_PASSED;
                notifyPassedOrFailed(p_passed);
                notifyHasEnteredState(p_passed);
            }
            else
            {
                i_state = C_STATE_PASSED;
                notifyPassedOrFailed(p_passed);
            }
        }
        else
        {
            notifyPassedOrFailed(p_passed);
            if(i_state != C_STATE_FAILED)
            {
                // Failed - and state changed, notify
                notifyHasEnteredState(p_passed);
            }
            i_state = C_STATE_FAILED;            
        }
    }

    /**
     * Indicates if the monitor is stopping.
     * @return <code>true</code> if stopping, <code>false</code> otherwise.
     */
    protected synchronized boolean isStopping()
    {
        boolean                 l_isStopping = false;
        
        if(i_monitorThread != null)
        {
            l_isStopping = i_monitorThread.isStopping();
        }
        return l_isStopping;
    }
    
    /**
     * Notifies all listeners that the monitor has entered the failed or passed state.
     * @param p_passed          <code>true</code> to notify passed, <code>false</code>
     *                          to notify failed.
     */
    private void notifyHasEnteredState(
        boolean                 p_passed
        )
    {
        Iterator                l_listenersI;
        WatchdogMonitorListener l_listener;
        
        l_listenersI = i_listenerAL.iterator();
        while(l_listenersI.hasNext())
        {
            l_listener = (WatchdogMonitorListener)l_listenersI.next();
            if(p_passed)
            {
                l_listener.hasEnteredPassedState();
            }
            else
            {
                l_listener.hasEnteredFailedState();
            }
        }
    }

    /**
     * Notifies all listeners that the monitor has passed or failed. 
     * @param p_passed          <code>true</code> to notify passed, <code>false</code>
     *                          to notify failed.
     */
    private void notifyPassedOrFailed(
        boolean                 p_passed
        )
    {
        Iterator                l_listenersI;
        WatchdogMonitorListener l_listener;
        
        l_listenersI = i_listenerAL.iterator();
        while(l_listenersI.hasNext())
        {
            l_listener = (WatchdogMonitorListener)l_listenersI.next();
            if(p_passed)
            {
                l_listener.passed();
            }
            else
            {
                l_listener.failed();
            }
        }
    }
    
    /**
     * Private inner class implementing the monitoring thread which actually performs the monitoring
     * in the background.
     */
    private class MonitorThread extends ThreadObject
    {
        
        /**
         * Performs the processing of the thread (the monitoring) until stopped.
         */
        public void doRun()
        {
            try
            {
                synchronized (this)
                {
                    if(i_startDelayMillis > 0)
                    {
                        // 0 to Object.wait means wait forever!
                        wait(i_startDelayMillis);
                    }
                }
            }
            catch (InterruptedException l_e)
            {
                // Ignore - continue (might be stopping)
            }
           
            while(!isStopping())
            {
                performMonitorIteration();
                synchronized(this)
                {
                    try
                    {
                        if(i_monitorIntervalMillis > 0)
                        {
                            // 0 to Object.wait means wait forever!
                            wait(i_monitorIntervalMillis);
                        }
                    }
                    catch (InterruptedException l_e)
                    {
                        // Ignore
                    }
                }
            }
        }
       
        /**
         * Returns the unique thread name.
         * @return The unique thread name.
         */
        public String getThreadName()
        {
            return i_threadName;
        }
       
    } // End of private class MonitorThread
    
} // End of public class AbstractWatchdogMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////