////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       EmailAlarmHandler.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       This class implements an Email
//                              Alarm Handler.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.emailclient.EmailClient;
import com.slb.sema.ppas.common.emailclient.emailimplementation.EmailMessage;
import com.slb.sema.ppas.common.emailcommon.EmailException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * This class implements an Email Alarm Handler which delivers alarms via email
 * using a configured SMTP email server. It silently ignores a destination if the
 * destination does not have an email address configured.
 */
public class EmailAlarmHandler extends AbstractAsynchAlarmHandler
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "EmailAlarmHandler";

    /** Default number of background threads to process alarms. */    
    private static final int    C_THREAD_COUNT_DEFAULT = 1;

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.awdcommon.EmailAlarmHandler.";

    /**
     * Configuration property name for the node name or IP address of SMTP
     * server to use.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_NODE_NAME = C_CFG_PROPERTY_BASE +
                                        "nodeName";

    /**
     * Configuration property name for the SMTP port number to use.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_PORT_NUMBER = C_CFG_PROPERTY_BASE +
                                        "portNumber";

    /** Default for the SMTP port number. Value is {@value}. */    
    private static final String C_CFG_PARAM_SMTP_PORT_NUMBER_DEFAULT = "25";


    /**
     * Configuration property name for the user name to use to authenticate
     * with the SMTP server (if required).
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_USER_NAME = C_CFG_PROPERTY_BASE +
                                        "userName";

    /**
     * Default for the user name to use to authenticate with the SMTP
     * server (if required).
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_USER_NAME_DEFAULT = "";

    /**
     * Configuration property name for the password to use to authenticate
     * with the SMTP server (if required).
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_PASSWORD = C_CFG_PROPERTY_BASE +
                                        "password";

    /**
     * Default for the password to use to authenticate with the SMTP
     * server (if required).
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_PASSWORD_DEFAULT = "";

    /**
     * Configuration property name for the from address to send emails from.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_FROM = C_CFG_PROPERTY_BASE +
                                        "from";

    /** Default for the from address to send emails from. Value is {@value}. */
    private static final String C_CFG_PARAM_SMTP_FROM_DEFAULT =
                                        "EmailAlarmHandler@ASCS";

    /**
     * Configuration property name for the subject set on sent emails. The property
     * value can be a template which will be expanded each time an email is sent.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_SMTP_SUBJECT = C_CFG_PROPERTY_BASE +
                                        "subject";

    /** Default for the subject set on sent emails. Value is {@value}. */
    private static final String C_CFG_PARAM_SMTP_SUBJECT_DEFAULT = "%_EmailSubject%";

    /** 
     * Array of Email Clients to send emails using. Each client is single
     * threaded, so one client is created for each background delivery
     * thread.
     */
    private EmailClient         i_emailClientARR[];
    
    /** Node name of SMTP server to use to send emails. */                
    private String              i_nodeName;

    /**
     * Port number on SMTP server to use to send emails. Defaults to the
     * standard SMTP port number ({@link #C_CFG_PARAM_SMTP_PORT_NUMBER_DEFAULT}).
     */                
    private int                 i_portNumber;

    /** Username to use to login to SMTP server. */                
    private String              i_username;

    /** Password to use to login to SMTP server. */                
    private String              i_password;

    /** Address to send emails from. */                
    private String              i_from;

    /**
     * Subject set on emails sent. It can be a template which will be
     * expanded each time an email is sent.
     */                
    private String              i_subject;


    /**
     * Creates a new Email Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     */
    public EmailAlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
        )
    {
        super(  p_logger,
                p_configProperties,
                p_instrumentManager,
                p_awdContext,
                C_THREAD_COUNT_DEFAULT,
                "EmailAH"
                );
                
        init(p_configProperties);
    }

    /** Method name passed to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialises this object based on the supplied configuration.
     * 
     * @param p_configProperties configuration to initialise from.
     */    
    private void init(
        PpasProperties          p_configProperties
        )
    {
        int                     l_loop;
        String                  l_portNumberString;
        AlarmException          l_alarmE;
        UtilProperties          l_utilProperties;
        
        i_emailClientARR = new EmailClient[C_THREAD_COUNT_DEFAULT];

        // TODO: <<Check and handle nulls etc!!!!>>
        l_utilProperties = new UtilProperties();
        l_utilProperties.addMacro("_EmailSubject", "ASCS Alarm (%_AlarmSequenceNumber%)");
        i_macroSubstitutor.addDefaultUtilPropeties(l_utilProperties);
        
        i_nodeName = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_NODE_NAME
                );
                
        // Default is blank - may not be required depending on Email Client
        // implementation and configuration.
        i_username = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_USER_NAME,
                C_CFG_PARAM_SMTP_USER_NAME_DEFAULT
                );

        // Default is blank - may not be required depending on Email Client
        // implementation and configuration.
        i_password = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_PASSWORD,
                C_CFG_PARAM_SMTP_PASSWORD_DEFAULT
                );
                
        i_from = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_FROM,
                C_CFG_PARAM_SMTP_FROM_DEFAULT
                );

        i_subject = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_SUBJECT,
                C_CFG_PARAM_SMTP_SUBJECT_DEFAULT
                );

        l_portNumberString = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_SMTP_PORT_NUMBER,
                C_CFG_PARAM_SMTP_PORT_NUMBER_DEFAULT
                );
        i_portNumber = Integer.parseInt(l_portNumberString);

        try
        {
            for(l_loop = 0; l_loop < C_THREAD_COUNT_DEFAULT; l_loop++)
            {
                i_emailClientARR[l_loop] = new EmailClient();
                i_emailClientARR[l_loop].init(p_configProperties);
            }
        }
        catch(EmailException l_e)
        {
            l_alarmE = new AlarmDeliveryException(
                    C_CLASS_NAME,
                    C_METHOD_doDeliverAsynchAlarm,
                    10150,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().problemInitialisingEmailClient(),
                    l_e);
            i_logger.logMessage(l_alarmE);
            // Logged it - nothing more we can do.
        }
    }

    /** Method name passed to middleware. Value is {@value}. */
    private static final String C_METHOD_doDeliverAsynchAlarm = "doDeliverAsynchAlarm";

    /**
     * {@inheritDoc}
     */
    protected void doDeliverAsynchAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier                
    )
    throws AlarmDeliveryException
    {
        
        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12010, this,
                "Delivering alarm " + p_formattedAlarm + " to destination " +
                p_destination);
                
        deliverEmailAlarm(p_threadInstanceNum, p_alarm, p_formattedAlarm, p_destination);
    }

    /**
     * Actually performs the alarm delivery using an allocated Email client
     * instance. It silently ignores a destination if the
     * destination does not have an email address configured.
     * 
     * @param p_threadInstanceNum the thread number calling this method (used
     *                          to selected the allocated email client).
     * @param p_alarm           the alarm.
     * @param p_formattedAlarm  the formatted alarm.
     * @param p_destination     the destination to deliver the alarm to.
     * @throws AlarmDeliveryException a problem occurred trying to deliver
     *                          the alarm, the alarm is unlikely to have been
     *                          delivered successfully.
     */
    private void deliverEmailAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
    )
    throws AlarmDeliveryException
    {
        UserAlarmDestination    l_alarmDestination;
        EmailMessage            l_emailMessage;
        String                  l_emailAddress;
        EmailClient             l_emailClient;
        AlarmDeliveryException  l_alarmE;
        
        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                "deliverEmailAlarm: Emailing formatted alarm to destination " +
                p_destination);
        
        l_emailClient = i_emailClientARR[p_threadInstanceNum-1]; // Thread no from 1
        l_alarmDestination = (UserAlarmDestination)p_destination;

        // Silently ignore destinations without an email address
        l_emailAddress = l_alarmDestination.getEmailAddress();        
        if((l_emailAddress != null) && (!"".equals(l_emailAddress)))
        {
            try
            {                    
                l_emailMessage = l_emailClient.createNewMessage();
            
                l_emailMessage.addTo(l_alarmDestination.getEmailAddress());
                l_emailMessage.setFrom(i_from);
                l_emailMessage.setSubject(i_macroSubstitutor.expand(
                        i_subject, p_alarm.getParameterMap()));
                l_emailMessage.setMessageText(p_formattedAlarm);
            
                l_emailClient.connect(
                    i_nodeName, i_portNumber, i_username, i_password);
                l_emailClient.sendMessage(l_emailMessage);
                l_emailClient.close();
    
                if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                        Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME,
                        12120, this,
                        "deliverEmailAlarm: Successfully emailed alarm " +
                        p_formattedAlarm + " to destination " + l_emailAddress);
            }
            catch(EmailException l_e)
            {
                l_alarmE = new AlarmDeliveryException(
                        C_CLASS_NAME,
                        C_METHOD_doDeliverAsynchAlarm,
                        10150,
                        this,
                        (PpasRequest)null,
                        0,
                        AwdKey.get().problemDeliveringAlarmViaEmail(
                            p_formattedAlarm,
                            l_alarmDestination.getEmailAddress(),
                            i_nodeName + ":" + i_portNumber,
                            l_alarmDestination.getUserName()),
                        l_e);
                i_logger.logMessage(l_alarmE);
                throw l_alarmE;
            }
        }
    } // End of private method deliverEmailAlarm
} // End of public class EmailAlarmHandler

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////