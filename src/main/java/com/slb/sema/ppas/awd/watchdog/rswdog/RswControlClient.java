////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswControlClient.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Rswdog Control Client - client for connecting
//                              and communicating using RSWP.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/06/06 | M.Vonka    | Fixed to use new method which   | PpaLon#2413/9093
//          |            | ensures when sending an RSWP    | PpaLon#2351/8997
//          |            | request, the return response is |
//          |            | the correct response to the     |
//          |            | request.                        |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.io.InterruptedIOException;

import com.slb.sema.ppas.common.smp.SimpleMessage;
import com.slb.sema.ppas.common.smp.SimpleMessageSocket;
import com.slb.sema.ppas.common.smp.SmpIoException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Rswdog Control Client - client for connecting
 * and communicating using RSWP.
 * <p/>
 * Supports the following configuration parameters (all prefixed by
 * <code>com.slb.sema.ppas.awd.watchdog.rswdog.RswControlClient.</code>:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>remoteIpAddress</td>
 *     <td>Server domain name or IP address to connect to.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>remoteIpPort</td>
 *     <td>Server port number to connect to.</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Read timeout to set on all newly created connections (in millis).</td>
 *     <td>0</td>
 *     <td>10000 (10s)</td>
 *   </tr>
 *   <tr>
 *     <td>connectTimeoutMillis</td>
 *     <td>Connect timeout for connecting new connections (in millis).</td>
 *     <td>0</td>
 *     <td>10000 (10s)</td>
 *   </tr>
 *   <tr>
 *     <td>trace</td>
 *     <td>Trace enabled state to set on all newly created connections
 *     (true,on,1,y,yes,enabled,enable or anything else which is treated
 *     as false)</td>
 *     <td>O</td>
 *     <td>false</td>
 *   </tr>
 * </table>
 */
public class RswControlClient
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RswControlClient";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdog.rswdog.RswControlClient.";

    /** Default read timeout set on newly connected connections. Value is {@value}. */
    private static final int    C_READ_TIMEOUT_MILLIS_DEFAULT = 10000;

    /** Default connect timeout for connecting new connections. Value is {@value}. */
    private static final int    C_CONNECT_TIMEOUT_MILLIS_DEFAULT = 10000;

    /** Logger to log any messages to. */
    private Logger              i_logger;

    /** Instrument manager to use. */
    private InstrumentManager   i_instrumentManager;
    
    /** The SMP Socket client connection to the SMP server. */
    private SimpleMessageSocket i_smpSocket;

    /** The RSWP Server IP address to connect to. */
    private String              i_remoteIpAddress;

    /** The RSWP Server Port to connect to. */
    private int                 i_remoteIpPort;
    
    /**
     * Read timeout set on newly connected connections.
     * Default if not configured is defined by
     * {@link #C_READ_TIMEOUT_MILLIS_DEFAULT}. 
     */
    private int                 i_readTimeoutMillis;

    /**
     * Connect timeout for connecting new connections.
     * Default if not configured is defined by
     * {@link #C_CONNECT_TIMEOUT_MILLIS_DEFAULT}. 
     */
    private int                 i_connectTimeoutMillis;

    /** Trace set on each new connection. */
    private boolean             i_traceSet = false;

    /**
     * Create a new RSWP Control Client.
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager instrument manager to use.
     * @param p_configProperties config properties.
     * @throws PpasConfigException configuration exception occurred.
     */
    public RswControlClient(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        PpasProperties          p_configProperties
        )
    throws PpasConfigException
    {
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;

        i_remoteIpAddress = p_configProperties.getTrimmedProperty(
                C_CFG_PROPERTY_BASE + "remoteIpAddress");
        
        i_remoteIpPort = Integer.parseInt(p_configProperties.getPortNumber(
                C_CFG_PROPERTY_BASE + "remoteIpPort"));

        i_readTimeoutMillis = p_configProperties.getIntProperty(
                C_CFG_PROPERTY_BASE + "readTimeoutMillis", C_READ_TIMEOUT_MILLIS_DEFAULT);

        i_connectTimeoutMillis = p_configProperties.getIntProperty(
                C_CFG_PROPERTY_BASE + "connectTimeoutMillis", C_CONNECT_TIMEOUT_MILLIS_DEFAULT);

        i_traceSet = p_configProperties.getBooleanProperty(
                C_CFG_PROPERTY_BASE + "trace", false);

    }

    /**
     * Starts the RSW Control Client.
     */
    public void start()
    {
        // Currently does nothing.
    }

    /**
     * Sends the supplied RSWP message (RSWP is built on SMP) and returns
     * the response.
     * @param p_message         the message to send.
     * @return The response received.
     */
    public synchronized SimpleMessage sendRequest(
        SimpleMessage           p_message)
    {
        SimpleMessage           l_response = null;
        
        try
        {
            if (i_smpSocket == null)
            {
                connect();
            }
            if(i_smpSocket != null)
            {
                // TOD0 2 Handle 1 retry if just lost connection 
                l_response = i_smpSocket.sendRequest(p_message);
            }
        }
        catch (InterruptedIOException l_iioe)
        {
            l_iioe.printStackTrace();
        }
        catch (SmpIoException l_e)
        {
            // Already logged - try to clean up and continue
            if(i_smpSocket != null)
            {
                try
                {
                    i_smpSocket.close();
                }
                catch(SmpIoException l_e2)
                {
                    l_e2.printStackTrace();
                    // We tried - best we can do is continue
                }
                i_smpSocket = null;
            }
            // Nothing more we can do, continue
        }
        
        return l_response;
    }
    
    /**
     * Sends the supplied RSWP message (RSWP is built on SMP) (without waiting for
     * a response - a response may not be expected).
     * TODO 1 This RSWP and SMP is a bit of a mess - its half asynch and then partly synch as some messages cause new requests from the other server - sort out! 
     * @param p_message         the message to send.
     */
    public synchronized void sendMessage(
        SimpleMessage           p_message)
    {
        
        try
        {
            if (i_smpSocket == null)
            {
                connect();
            }
            if(i_smpSocket != null)
            {
                // TOD0 2 Handle 1 retry if just lost connection 
                i_smpSocket.sendMessage(p_message);
            }
        }
        catch (SmpIoException l_e)
        {
            // Already logged - try to clean up and continue
            if(i_smpSocket != null)
            {
                try
                {
                    i_smpSocket.close();
                }
                catch(SmpIoException l_e2)
                {
                    l_e2.printStackTrace();
                    // We tried - best we can do is continue
                }
                i_smpSocket = null;
            }
            // Nothing more we can do, continue
        }
        
        return;
    }
    
    /**
     * Connects to the remote RSWP server.
     * @throws SmpIoException      exception making connection.
     */
    private void connect()
    throws SmpIoException
    {
        
        i_smpSocket = new SimpleMessageSocket(
                i_logger,
                i_remoteIpAddress,
                i_remoteIpPort,
                i_connectTimeoutMillis,
                i_readTimeoutMillis,
                "RSWP",
                "v001"
                );

    }
    
} // End of public class RswControlClient.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
