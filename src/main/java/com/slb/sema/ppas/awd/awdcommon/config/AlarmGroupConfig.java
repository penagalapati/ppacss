/*
 * Created on 28-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmActionGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDestination;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDestinationGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmFormat;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Configuration container mapping Alarm Group name to Alarm Group object.
 */
public class AlarmGroupConfig
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmGroupConfig";
    
    private Map                 i_alarmGroupMap;
    private Map                 i_actionGroupMap;
    private Map                 i_formatMap;
    private Map                 i_destinationGroupMap;
    private Map                 i_destinationMap;
    //private Map                 i_alarmActionMap;
    //private Map                 i_alarmDestinationMap;

    public AlarmGroupConfig()
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        init();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        
    }
    
    public void init()
    {
        i_alarmGroupMap = new HashMap();
        i_actionGroupMap = new HashMap();
        i_formatMap = new HashMap();
        i_destinationGroupMap = new HashMap();
        i_destinationMap = new HashMap();
        //i_alarmActionMap = new HashMap();
        //i_alarmDestinationMap = new HashMap();
    }

    public void addAlarmGroup(
        AlarmGroup              p_alarmGroup
    )
    {
        i_alarmGroupMap.put(p_alarmGroup.getName(), p_alarmGroup);
    }

    public AlarmGroup getDefaultAlarmGroup()
    {
        // TODO: <<Change to configurable name!!!!>>
        return (AlarmGroup)i_alarmGroupMap.get("DEFAULT");
    }

    public AlarmGroup getAlarmGroup(
        String                  p_alarmGroupName
    )
    {
        return (AlarmGroup)i_alarmGroupMap.get(p_alarmGroupName);
    }
    
    public String toString()
    {
        return(i_alarmGroupMap.toString());
    }
    
    public void addActionGroup(
        String                  p_alarmActionGroupName,
        AlarmActionGroup        p_actionGroup
    )
    {
        i_actionGroupMap.put(p_alarmActionGroupName, p_actionGroup);
    }
    
    public AlarmActionGroup getActionGroup(
        String                  p_alarmActionGroupName
    )
    {
        return((AlarmActionGroup)i_actionGroupMap.get(p_alarmActionGroupName));
    }
        
    public void addFormat(
        String                  p_formatName,
        AlarmFormat             p_alarmFormat
    )
    {
        i_formatMap.put(p_formatName, p_alarmFormat);
    }
    
    public AlarmFormat getFormat(
        String                  p_formatName
    )
    {
        return((AlarmFormat)i_formatMap.get(p_formatName));
    }
    
    public void addDestinationGroup(
        String                  p_destinationGroupName,
        AlarmDestinationGroup   p_destinationGroup
    )
    {
        i_destinationGroupMap.put(p_destinationGroupName, p_destinationGroup);
    }
    
    public AlarmDestinationGroup getDestinationGroup(
        String                  p_destinationGroupName
    )
    {
        return((AlarmDestinationGroup)i_destinationGroupMap.get(p_destinationGroupName));
    }
    
    public void addDestination(
        String                  p_destinationName,
        AlarmDestination        p_alarmDestination
    )
    {
        i_destinationMap.put(p_destinationName, p_alarmDestination);
    }
    
    public AlarmDestination getDestination(
        String                  p_destinationName
    )
    {
        return((AlarmDestination)i_destinationMap.get(p_destinationName));
    }

/*
    public void addActionGroup(
        String                  p_alarmActionGroupName,
        AlarmAction             p_alarmAction
    )
    {
        i_alarmActionMap.put(p_alarmActionGroupName, p_alarmAction);
    }

    public void addDestinationDestination(
        String                  p_alarmDestinationName,
        AlarmDestination              p_alarmDestination
    )
    {
        i_alarmDestinationMap.put(p_alarmDestinationName, p_alarmDestination);
    }
*/
} // End of public class AlarmGroupConfig
