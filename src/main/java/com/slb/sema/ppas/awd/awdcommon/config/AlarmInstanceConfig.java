/*
 * Created on 17-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmInstanceConfig
{
    private String              i_alarmName;
    private String              i_alarmText;
    private int                 i_alarmSeverity;

    private Map                 i_parameterMap;


    public AlarmInstanceConfig(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity
    )
    {
        this(   p_alarmName,
                p_alarmText,
                p_alarmSeverity,
                (Map)null
                );
    }
    

    public AlarmInstanceConfig(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity,
        Map                     p_parameterMap
    )
    {
        super();
        i_alarmName = p_alarmName;
        i_alarmText = p_alarmText;
        i_alarmSeverity = p_alarmSeverity;
        if(p_parameterMap != null)
        {
            i_parameterMap = p_parameterMap;
        }
        else
        {
            i_parameterMap = new HashMap();
        }
    }

    /**
     * Returns the alarm's name.
     * @return                  The alarm's name.
     */
    public String getAlarmName()
    {
        return i_alarmName;
    }

    /**
     * Returns the alarm's severity.
     * @return                  The alarm's severity.
     */
    public int getAlarmSeverity()
    {
        return i_alarmSeverity;
    }

    /**
     * Returns the alarm's text.
     * @return                  The alarm's text.
     */
    public String getAlarmText()
    {
        return i_alarmText;
    }
    /*
     * Returns the alarm's parameter map.
     * @return                  The alarm's parameter map.
     */
    public Map getAlarmParameterMap()
    {
        return i_parameterMap;
    }

}
