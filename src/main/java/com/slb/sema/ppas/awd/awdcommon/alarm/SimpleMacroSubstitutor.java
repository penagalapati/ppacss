/*
 * Created on 30-Jan-04
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SimpleMacroSubstitutor
{
    
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SimpleMacroSubstitutor";

    private static final char   C_DEFAULT_MACRO_DELIMITER = '%';
    private char                i_macroDelimiter = C_DEFAULT_MACRO_DELIMITER;

    /**
     * Default character which within a macro reference, separates the macro
     * name from the macro format. Value = '{@value}'.
     */    
    private static final char   C_DEFAULT_FORMAT_SEPARATOR = ':';

    /**
     * Character which within a macro reference, separates the macro
     * name from the macro format. Default defined by
     * {@link #C_DEFAULT_FORMAT_SEPARATOR C_DEFAULT_FORMAT_SEPARATOR}.
     */    
    private char                i_formatSeparator = C_DEFAULT_FORMAT_SEPARATOR;
    
    /**
     * Default format string for 'inputing' (parsing) dates to be [re]formatted. Value = '{@value}'.
     */    
    private static final String C_DEFAULT_INPUT_DATE_FORMAT_STRING = "dd-MMM-yy HH:mm:ss";

    /**
     * Default format string for outputing (formating) dates to be [re]formatted. Value = '{@value}'.
     */    
    private static final String C_DEFAULT_OUTPUT_DATE_FORMAT_STRING = "dd-MMM-yy HH:mm:ss";

    private static final UtilProperties C_EMPTY_UTIL_PROPERTIES_ARRAY[] =
            new UtilProperties[0];

    /**
     * Set of default <code>UtilProperties</code> to look in for macro substitutions if substituation
     * value not found in supplied macro map.
     */    
    private Set                 i_defaultUtilPropertiesSet = null;

    /**
     * Indicates if the cached array view is out of date and needs to be
     * recreated.
     */
    private boolean             i_cacheInvalidated = true;

    /**
     * Cached array view of default <code>UtilProperties</code> to look in for
     * macro substitutions if substituation value not found in supplied macro
     * map.
     */    
    private UtilProperties      i_defaultUtilPropertiesCachedARR[] = null;
    

    /**
     * 
     */
    public SimpleMacroSubstitutor()
    {
        super();
        
        i_defaultUtilPropertiesSet = new HashSet();
    }

    public SimpleMacroSubstitutor(
        char                    p_macroDelimiter
    )
    {
        super();
        
        i_defaultUtilPropertiesSet = new HashSet();
        i_macroDelimiter = p_macroDelimiter;
    }

    public synchronized void addDefaultUtilPropeties(UtilProperties p_properties)
    {
        i_defaultUtilPropertiesSet.add(p_properties);
        i_cacheInvalidated = true;
    }

    public synchronized void removeDefaultUtilPropeties(UtilProperties p_properties)
    {
        i_defaultUtilPropertiesSet.remove(p_properties);
        i_cacheInvalidated = true;
    }

    /**
     * Expands the supplied string by expanding any macros in the string.
     * It tries to look the macro up in each default <code>Map</code>
     * in the order they were set (if any were set). If still not found, it finally
     * tries to look it up in the <code>System</code> properties. 
     *
     * @param p_originalString  <code>String</code> to expand any macros in.
     * @return                  The expanded <code>String</code>.
     */    
    public String expand(
        String                  p_originalString
    )
    {
        return expand(p_originalString, (Map)null);
    }
    
    
    /**
     * Expands the supplied string by expanding any macros in the string. If
     * the supplied <code>Map</code> is not null, first the macro is looked up
     * in the supplied <code>Map</code>. If the macro substitution value has not
     * been found, it then tries to look it up in each default <code>Map</code>
     * in the order they were set (if any were set). If still not found, it finally
     * tries to look it up in the <code>System</code> properties. 
     *
     * @param p_originalString  <code>String</code> to expand any macros in.
     * @param p_macroMap        map to look for substitution values in first
     *                          or <code>null</code> if only to use defaults
     *                          set.
     * @return                  The expanded <code>String</code>.
     */    
    public String expand(
        String                  p_originalString,
        Map                     p_macroMap
    )
    {
        StringBuffer            l_expandedSB;
        String                  l_expandedString;
        String                  l_expandedMacro;
        boolean                 l_finished;
        UtilProperties          l_defaultUtilPropertiesARR[];
        
        String                  l_originalString;
        int                     l_index;
        int                     l_index2;
        int                     l_previousIndex;
        int                     l_endIndex;
        int                     l_templateLength;
        String                  l_macro;
        int                     l_formatStartIndex;
        String                  l_format;
        
        int                     l_inputDateFormatStartIndex;
        int                     l_outputDateFormatStartIndex;
        String                  l_formatString;
        SimpleDateFormat        l_inputDateFormat;
        SimpleDateFormat        l_outputDateFormat;
        String                  l_value;
        Date                    l_date;

        if(p_originalString == null)
        {
            return null;
        }
        
        // Store temp local copy of array incase instance array changes during
        // processing.
        l_defaultUtilPropertiesARR = getDefaultUtilPropertiesArray();
        
        l_originalString = p_originalString;
        l_expandedString = p_originalString; // Start by assuming nothing to do.        

        l_index = l_originalString.indexOf(i_macroDelimiter);
        if(l_index >= 0)
        {
            // Delimiter found, so process whole string into new string
            // performing any substitutions.
            l_templateLength = l_originalString.length();
            l_expandedSB = new StringBuffer(l_templateLength);
            l_endIndex = l_templateLength - 1;
            l_previousIndex = 0;
            
            l_finished = false;
            while (!l_finished)
            {
                if(l_index < 0)
                {
                    // No more %
                    l_finished = true;
                }
                else if(l_index == l_endIndex)
                {
                    // Single remaining % at end of template
                    l_finished = true;
                }
                else
                {
                    l_index2 = l_originalString.indexOf(i_macroDelimiter, l_index + 1);
                    if(l_index2 < 0)
                    {
                        // No terminating %
                        l_finished = true;
                    }
                    else if(l_index2 == (l_index + 1))
                    {
                        // %%, replace with single, literal %
                        l_expandedSB.append(l_originalString.substring(
                                  l_previousIndex, l_index));
                        l_expandedSB.append(i_macroDelimiter);
                        l_previousIndex = l_index2 + 1;
                    }
                    else
                    {
                        // Substitution parameter found.
                        l_expandedSB.append(l_originalString.substring(
                                  l_previousIndex, l_index));
                        l_macro = l_originalString.substring(l_index + 1, l_index2);
                        if((l_formatStartIndex = l_macro.indexOf(i_formatSeparator)) > 0)
                        {
                            l_format = l_macro.substring(l_formatStartIndex + 1);
                            l_macro = l_macro.substring(0, l_formatStartIndex);
                            if(Debug.on)
                            {
                                Debug.print(
                                        Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                                        Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                                        "Found macro " + l_macro + ", with format " + l_format);
                            }
                            if(l_format.startsWith("DATE"))
                            {
                                l_inputDateFormatStartIndex = l_format.indexOf("__");
                                l_outputDateFormatStartIndex = l_format.indexOf(
                                        "__", l_inputDateFormatStartIndex + 2);
                                l_formatString = l_format.substring(
                                        l_inputDateFormatStartIndex + 2,
                                        l_outputDateFormatStartIndex);
                                if("".equals(l_formatString))
                                {
                                    l_inputDateFormat = new SimpleDateFormat(
                                            C_DEFAULT_INPUT_DATE_FORMAT_STRING);
                                }
                                else
                                {
                                    l_inputDateFormat = new SimpleDateFormat(l_formatString);
                                }
                                l_value = null;
                                try
                                {
                                    if("_DateTimeNow".equals(l_macro))
                                    {
                                        l_date = new Date();
                                    }
                                    else
                                    {
                                        l_value = expand(
                                                getMacro(l_macro, p_macroMap, l_defaultUtilPropertiesARR),
                                                p_macroMap);
                                        l_date = l_inputDateFormat.parse(l_value);
                                    }
                                    l_formatString = l_format.substring(
                                            l_outputDateFormatStartIndex + 2);
                                    if("".equals(l_formatString))
                                    {
                                        l_formatString = C_DEFAULT_OUTPUT_DATE_FORMAT_STRING;
                                    } 
                                    l_outputDateFormat = new SimpleDateFormat(l_formatString);
                                    l_expandedMacro =  l_outputDateFormat.format(l_date);
                                    if(Debug.on)
                                    {
                                        Debug.print(
                                                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                                                Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                                                "Expanded " + l_macro + " to " + l_expandedMacro);
                                    }
                                    l_expandedSB.append(l_expandedMacro);
                                }
                                catch(ParseException l_e)
                                {
                                    System.err.println("EXCEPTION parsing date " + l_value +
                                            "using format " +
                                            l_formatString + ", ignoring.");
                                    l_e.printStackTrace();
                                }
                            }
                        }
                        else
                        {
                            if(Debug.on)
                            {
                                Debug.print(
                                        Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                                        Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                                        "Found macro " + l_macro);
                            }
                            if("_DateTimeNow".equals(l_macro))
                            {
                                l_date = new Date();
                                l_outputDateFormat = new SimpleDateFormat(
                                        C_DEFAULT_OUTPUT_DATE_FORMAT_STRING);
                                l_expandedMacro = l_outputDateFormat.format(l_date);
                                if(Debug.on)
                                {
                                    Debug.print(
                                            Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                                            Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                                            "Expanded " + l_macro + " to " + l_expandedMacro);
                                }
                                l_expandedSB.append(l_expandedMacro);
                            }
                            else if(l_macro.startsWith("__"))
                            {
                                int l_base = 10;
                                int l_startPos = 2;
                                
                                if(l_macro.startsWith("__0x"))
                                {
                                    l_base = 16;
                                    l_startPos = 4;
                                }
                                try
                                {
                                    int l_asciiCharacterCode =
                                            Integer.parseInt(l_macro.substring(l_startPos), l_base);
                                    if(Debug.on)
                                    {
                                        Debug.print(
                                                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                                                Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                                                "Appending char with ascii code " + l_asciiCharacterCode);
                                    }
                                    l_expandedSB.append((char)l_asciiCharacterCode);
                                }
                                catch(RuntimeException l_re)
                                {
                                    // Print error and ignore
                                    l_re.printStackTrace();
                                    l_expandedSB.append(l_macro);
                                }
                            }
                            else
                            {
                                l_expandedSB.append(
                                        expand(getMacro(l_macro, p_macroMap, l_defaultUtilPropertiesARR),
                                                p_macroMap));
                            }
                        }
                        l_previousIndex = l_index2 + 1;
                    }
                }
                
                l_index = l_originalString.indexOf(
                        i_macroDelimiter, l_previousIndex);
                             
            } // End while not finished
            l_expandedSB.append(l_originalString.substring(
                      l_previousIndex, l_templateLength));
            l_expandedString = l_expandedSB.toString();
        }
        
        return l_expandedString;
    }

    /**
     * Gets the substitution value for the supplied macro.
     * If the supplied <code>Map</code> is not null, first the macro is looked up
     * in the supplied <code>Map</code>. If the macro substitution value has not
     * been found, it then tries to look it up in each default <code>Map</code>
     * in the order they were set (if any were set). If still not found, it finally
     * tries to look it up in the <code>System</code> properties.
     *
     * @param p_macroName       name of macro to return substitution for.
     * @param p_map             map to look for substitution values in first
     *                          or <code>null</code> if only to use defaults
     *                          set.
     * @param p_defaultUtilPropertiesARR ordered array of default properties to
     *                          search for substitution value (if not
     *                          found in supplied map).
     * @return                  The first substitution value found for the
     *                          macro or <code>null</code> if no
     *                          substitution found.
     */    
    private String getMacro(
        String                  p_macroName,
        Map                     p_map,
        UtilProperties          p_defaultUtilPropertiesARR[]
        )
    {
        String                  l_value = null;
        int                     l_loop;
        
        if(p_map != null)
        {
            l_value = (String)p_map.get(p_macroName);
        }
        for(l_loop = 0; (l_value == null) && l_loop < p_defaultUtilPropertiesARR.length; l_loop++)
        {
            l_value = p_defaultUtilPropertiesARR[l_loop].getProperty(p_macroName);
        }
        if(l_value == null)
        {
            l_value = System.getProperty(p_macroName);
        }
    
        if(Debug.on)
        {
            Debug.print(
                    Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                    Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11000, this,
                    "Expanded macro " + p_macroName + " to " + l_value);
        }
        
        return l_value;
    }
    
    private synchronized UtilProperties[] getDefaultUtilPropertiesArray()
    {

        if(i_cacheInvalidated)
        {
            i_defaultUtilPropertiesCachedARR =
                    (UtilProperties[])i_defaultUtilPropertiesSet.
                            toArray(C_EMPTY_UTIL_PROPERTIES_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_defaultUtilPropertiesCachedARR;
    }


    public static void main(String p_argsARR[])
    {
        SimpleMacroSubstitutor  l_substitutor;
        java.util.HashMap                 l_map;
        
        l_map = new java.util.HashMap();
        l_map.put("DateTime1", "18-Jun-2004 16:23:45");
        l_map.put("DateTime2", "04/07/19 172446");
        
        l_substitutor = new SimpleMacroSubstitutor();
        
        System.out.println(l_substitutor.expand(
                "DateTime1=%DateTime1%\n" + 
        "DateTime2=%DateTime2%\n" +
        "DateTime1DefInputFormat=%DateTime1:DATE____yyMMddHHmmss%\n" +
        "DateTime2DefOutputFormat=%DateTime2:DATE__yy/MM/dd HHmmss__%\n"
                , l_map));
    }
}
