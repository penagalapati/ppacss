////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RemoteServiceWatchdogConfigLoader.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Loader for Remote Service Watchdog Server
//                              specific config.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/04/06 | M.Vonka    | Fixed to handle multiple        | PpaLon#2185/8522
//          |            | monitors - i.e. grouped servs.  |
//----------+------------+---------------------------------+--------------------
// 30/08/07 | E Dangoor  | Allow config to be found either | PpacLon#3319/12021
//          |            | in the local or the system area |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.ConfigTokenizer;
import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.ServiceConfig;
import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.MonitorConfig;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Loader for Remote Service Watchdog Server
 * specific config.
 */
public class RemoteServiceWatchdogConfigLoader extends AwdAbstractConfigLoader
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RemoteServiceWatchdogConfigLoader";

    /**
     * Creates a new loader.
     * @param p_logger          logger to log any messages to.
     * @param p_configProperties config properties.
     * @param p_awdContext      AWD context.
     */
    public RemoteServiceWatchdogConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext
    )
    {
        super(p_logger, p_configProperties, p_awdContext);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Loads the Remote Service Watchdog Configuation and returns it.
     * @param p_configFilename  filename of file to load config from.
     * @param p_processName     process name (load config relevant to proceess).
     * @return The loaded Remote Service Watchdog Configuation
     * @throws AwdConfigException exception occurred loading the config.
     */
    public RemoteServiceWatchdogConfig loadRemoteServiceWatchdogConfig(
        String                  p_configFilename,
        String                  p_processName
    )
    throws AwdConfigException
    {
        RemoteServiceWatchdogConfig l_config;
        
        l_config = new RemoteServiceWatchdogConfig(i_logger);
        
        //loadTestConfig(l_config);
        loadLayeredConfig(p_configFilename, l_config, p_processName);
        
        return l_config;
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    /**
     * Loads the Remote Service Watchdog Configuation into supplied config object.
     * @param p_configFilename  filename of file to load config from.
     * @param p_watchdogConfig  config Object to load config into.
     * @param p_processName     process name (load config relevant to proceess).
     * @throws AwdConfigException exception occurred loading the config.
     */
    private void loadLayeredConfig(
        String                  p_configFilename,
        RemoteServiceWatchdogConfig p_watchdogConfig,
        String                  p_processName
        )
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_watchdogConfig, p_processName);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_watchdogConfig, p_processName);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_loadConfig = "loadConfig";
    /**
     * Loads the Remote Service Watchdog Configuation into supplied config object.
     * @param p_configFilename  path of file to load config from.
     * @param p_watchdogConfig  config Object to load config into.
     * @param p_processName     process name (load config relevant to proceess).
     * @throws AwdConfigException exception occurred loading the config.
     */
    private void loadConfig(
        String                  p_configFilename,
        RemoteServiceWatchdogConfig p_watchdogConfig,
        String                  p_processName
        )
    throws AwdConfigException
    {

        ConfigTokenizer         l_tokenizer;
        String                  l_token;        
        String                  l_serviceName;
        ServiceConfig           l_serviceConfig;
        String                  l_serviceHomeServer;
        String                  l_serviceStandbyServer;
        String                  l_serviceIpAddress;
        String                  l_enteredFailedStateWatchdogActionGroupName;
        String                  l_enteredPassedStateWatchdogActionGroupName;
        String                  l_releaseServiceWatchdogActionGroupName;
        String                  l_checkLocalServiceVipStateCommand;
        long                    l_minimumMillisBetweenAutoFailovers;
        String                  l_monitorClass;
        Map                     l_monitorParamMap;
        String                  l_monitorParamName;
        String                  l_monitorParamValue;
        MonitorConfig           l_monitorConfig;
         
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        // Read action groups
        l_token = l_tokenizer.nextToken();
        while("rswdog.service.name".equals(l_token))
        {            
            l_tokenizer.getExpectedNextToken("=");
            l_serviceName = l_tokenizer.getExpectedNextToken();
            l_tokenizer.getExpectedNextToken("{");
            l_serviceHomeServer = l_tokenizer.getExpectedNextParameter(
                    "serviceHomeServer");
            l_serviceStandbyServer = l_tokenizer.getExpectedNextParameter(
                    "serviceStandbyServer");
            l_serviceIpAddress = l_tokenizer.getExpectedNextParameter(
                    "serviceIpAddress");
            l_enteredFailedStateWatchdogActionGroupName = l_tokenizer.getExpectedNextParameter(
                    "enteredFailedStateWatchdogActionGroupName");
            l_enteredPassedStateWatchdogActionGroupName = l_tokenizer.getExpectedNextParameter(
                    "enteredPassedStateWatchdogActionGroupName");
            l_releaseServiceWatchdogActionGroupName = l_tokenizer.getExpectedNextParameter(
                    "releaseServiceWatchdogActionGroupName");
            l_checkLocalServiceVipStateCommand = l_tokenizer.getExpectedNextParameter(
                    "checkLocalServiceVipStateCommand");
            l_minimumMillisBetweenAutoFailovers = Long.parseLong(l_tokenizer.getOptionalNextParameter(
                    "minimumMillisBetweenAutoFailovers", "600000"));
            
            l_serviceConfig = new ServiceConfig(
                    l_serviceName,
                    l_serviceHomeServer,
                    l_serviceStandbyServer,
                    l_serviceIpAddress,
                    l_enteredFailedStateWatchdogActionGroupName,
                    l_enteredPassedStateWatchdogActionGroupName,
                    l_releaseServiceWatchdogActionGroupName,
                    l_checkLocalServiceVipStateCommand,
                    l_minimumMillisBetweenAutoFailovers
                    );                    
            
            l_token = l_tokenizer.nextToken();
            while("monitorClass".equals(l_token))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_monitorClass = l_tokenizer.getExpectedNextToken();                
                l_monitorParamMap = new HashMap();
                while("monitorParam".equals(l_token = l_tokenizer.getExpectedNextToken(
                        new String[] {"monitorParam", "monitorClass", "}"} )))
                {
                    l_tokenizer.getExpectedNextToken("=");
                    l_monitorParamName = l_tokenizer.getExpectedNextToken();
                    l_tokenizer.getExpectedNextToken("=");
                    l_monitorParamValue = l_tokenizer.getExpectedNextToken();
                    l_monitorParamMap.put(l_monitorParamName, l_monitorParamValue);
                }
                
                l_monitorConfig = new MonitorConfig(
                        l_monitorClass, l_monitorParamMap);
                l_serviceConfig.addMonitorConfig(l_monitorConfig);
                
            }
            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'}' or 'monitorClass'");
            }
            p_watchdogConfig.addServiceConfig(l_serviceConfig);
            l_token = l_tokenizer.nextToken();
        }
        
        if(l_token != null)
        {
            handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                    l_token, l_tokenizer.getLineNumber(), 
                    p_configFilename, "'rswdog.service.name' or end of file");
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_LOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_APPLICATION_DATA, C_CLASS_NAME, 11080, this,
                C_METHOD_loadConfig + ": Loaded config = " + p_watchdogConfig);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 11090, this,
                "Leaving " + C_METHOD_loadConfig);
                
        return;
    } // End of private method loadConfig
    
} // End of public class RemoteServiceWatchdogConfigLoader.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
