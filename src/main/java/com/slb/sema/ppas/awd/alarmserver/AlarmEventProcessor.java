////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmEventProcessor.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Processes alarm events, raising alarms if
//                              configured.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/07/06 | M.Vonka    | Comment added (and fixed).      | PpaLon#2496/9386
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.alarm.ConfiguredAlarm;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfigLoader;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Processes alarm events, raising alarms if configured. For each event it is
 * passed, it checks its loaded configuration to see if any alarm is configured
 * for it. If an alarm is configured for the event, it creates the alarm and
 * then delegates the processing of the alarm to its AlarmProcessor.
 * <p/>
 * Uses the following configuration:<br/>
 * <table>
 * <tr>
 *   <td>{@link #C_CFG_PARAM_ALARM_CFG_FILE_NAME}</td>
 *           <td>Path and file name to Alarm Configuration file</td>
 * </tr>
 * <tr>
 *   <td>{@link #C_CFG_PARAM_ALARM_EVENT_CFG_FILE_NAME}</td>
 *           <td>Path and file name to Alarm Event Configuration file</td>
 * </tr>
 * <tr>
 *   <td>{@link #C_CFG_PARAM_ALARM_GROUP_CFG_FILE_NAME}</td>
 *           <td>Path and file name to Alarm Group Configuration file</td>
 * </tr>
 * </table>
 */
public class AlarmEventProcessor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmEventProcessor";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.alarmserver." +
                                        "AlarmEventProcessor.";

    /**
     * Configuration property name for the path and filename to the Alarm
     * Group configuration file.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_GROUP_CFG_FILE_NAME = C_CFG_PROPERTY_BASE +
                                        "alarmGroupConfigFilePathAndName";

    /**
     * Configuration property name for the path and filename to the Alarm
     * configuration file.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_CFG_FILE_NAME = C_CFG_PROPERTY_BASE +
                                        "alarmConfigFilePathAndName";

    /**
     * Configuration property name for the path and filename to the Alarm
     * Event configuration file.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_EVENT_CFG_FILE_NAME = C_CFG_PROPERTY_BASE +
                                        "alarmEventConfigFilePathAndName";

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Alarm Event Processor's loaded configuration. */
    private ProcessorConfig     i_processorConfig;
    
    /** Alarm Processor which is used to actually process any required alarms. */
    private AlarmProcessor      i_alarmProcessor;
    
    /**
     * Creates a new AlarmEventProcessor. After creation it must be initialised
     * by calling {@link #init(PpasProperties)}. 
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager instrument manager to register any instruments
     *                          with.
     * @param p_awdContext      context for this component.
     */
    public AlarmEventProcessor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
        )
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
                
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Initialises the AlarmEventProcessor. For details of configuration loaded by
     * this component see the class description {@link AlarmEventProcessor above}.
     * @param p_configProperties configuration to used to initialise with.
     * @throws PpasException    general exception occurred initialisation.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    throws PpasException
    {

        i_alarmProcessor = new AlarmProcessor(
                i_logger,
                i_instrumentManager,
                i_awdContext
                );
        i_alarmProcessor.init(p_configProperties);

        loadConfig(p_configProperties);                
                
    }

    /**
     * Loads required configuration. For details of configuration loaded by
     * this component see the class description {@link AlarmEventProcessor above}.
     * @param p_configProperties configuration to load from.
     * @throws PpasException
     */
    private void loadConfig(
        PpasProperties          p_configProperties
    )
    throws PpasException
    {
        ProcessorConfig         l_processorConfig;
        AlarmGroupConfigLoader  l_alarmGroupConfigLoader;
        AlarmConfigLoader       l_alarmConfigLoader;
        AlarmEventConfigLoader  l_alarmEventConfigLoader;
        String                  l_configFilename;

        l_processorConfig = new ProcessorConfig();        
        l_alarmGroupConfigLoader = new AlarmGroupConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_configFilename = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_GROUP_CFG_FILE_NAME);
        l_processorConfig.i_alarmGroupConfig =
                l_alarmGroupConfigLoader.loadAlarmGroupConfig(l_configFilename);

        l_alarmConfigLoader = new AlarmConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        /** TODO: <<Implement get manadatory config????>>. */
        l_configFilename = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_CFG_FILE_NAME);
        l_processorConfig.i_alarmConfig =
                l_alarmConfigLoader.loadAlarmConfig(l_configFilename);

        l_configFilename = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_EVENT_CFG_FILE_NAME);
        l_alarmEventConfigLoader = new AlarmEventConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_processorConfig.i_alarmEventConfig =
                l_alarmEventConfigLoader.loadAlarmEventConfig(
                        l_configFilename,
                        l_processorConfig.i_alarmConfig,
                        l_processorConfig.i_alarmGroupConfig);
                        
        // Replace processor instance in one atomic step incase this is a reload.
        i_processorConfig = l_processorConfig;
        
    }

    /**
     * Reloads required configuration. For details of configuration loaded by
     * this component see the class description {@link AlarmEventProcessor above}.
     * @param p_configProperties configuration to load from.
     * @throws PpasException
     */
    public void reloadConfig(
        PpasProperties          p_configProperties
    )
    throws PpasException
    {
        loadConfig(p_configProperties);
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_processEvent = "processEvent";
    /**
     * Checks the event against the loaded Alarm Event configuration. If
     * an Alarm is configured for the Event, creates the Alarm (from Alarm
     * Configuration) and deleagates processing of the Alarm to the Alarm
     * Processor.
     * @param p_alarmEvent      Alarm Event to process.
     */
    public void processEvent(
        AlarmEvent              p_alarmEvent
    )
    {
        ProcessorConfig         l_processorConfig;
        AlarmEventConfig.Entry  l_entry;
        ConfiguredAlarm         l_configuredAlarm;
        String                  l_alarmText;

        if(Debug.on) Debug.print(
                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10000, this,
                "Entered " + C_METHOD_processEvent + ", processing event " + p_alarmEvent);

        // Hold reference in local variable and use this incase a reload occurs
        // while processing this event.
        l_processorConfig = i_processorConfig;
                
        // TODO: <<Handle multiple matches????>>
        l_entry = l_processorConfig.i_alarmEventConfig.getMatchingAlarmEntry(
                p_alarmEvent);

        if(l_entry != null)
        {
            // NOTE that if we have an entry, existance of its
            // AlarmInstanceConfig and AlarmGroup will have already been
            // checked so we don't need to check this again here.
            l_configuredAlarm = new ConfiguredAlarm(
                    (String)null, // Name
                    (String)null, // Text
                    (Integer)null, // Severity
                    p_alarmEvent.getOriginatingNodeName(),
                    p_alarmEvent.getOriginatingProcessName(),
                    (Map)null, // Parameter Map
                    l_entry.getAlarmInstanceConfig()
                    );
            l_configuredAlarm.setParameter("_EventText", p_alarmEvent.getEventText());
            l_alarmText = l_configuredAlarm.getAlarmText();
            if((l_alarmText== null) || "".equals(l_alarmText))
            {
                l_configuredAlarm.setAlarmText(p_alarmEvent.getEventText());
            }
            l_configuredAlarm.setParameter("_EventName", p_alarmEvent.getEventName());

            i_alarmProcessor.processAlarm(
                     l_configuredAlarm,
                     l_entry.getAlarmGroup()
                     );
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_processAlarm = "processAlarm";
    /**
     * Delegates actual processing of the alarm (using the configured
     * default alarm group) to
     * {@link AlarmProcessor#processAlarm(Alarm, com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup)}.
     * @param p_alarm           Alarm to process.
     */
    public void processAlarm(
        Alarm                   p_alarm
    )
    {
        ProcessorConfig         l_processorConfig;

        if(Debug.on) Debug.print(
                Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10000, this,
                "Entered " + C_METHOD_processAlarm + ", processing alarm " + p_alarm);                

        // Hold reference in local variable and use this incase a reload occurs
        // while processing this event.
        l_processorConfig = i_processorConfig;

        i_alarmProcessor.processAlarm(p_alarm,
                l_processorConfig.i_alarmGroupConfig.getDefaultAlarmGroup());
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        // Currently nothing more interesting then super.toString().
        return super.toString();
    }

    /**
     * Returns a verbose String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A verbose respresentation suitable for debugging/trace.
     */
    public String toVerboseString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(2000);
        
        l_sb.append("AlarmEventProcessor=[");
        l_sb.append("alarmGroupCfg=[\n");
        l_sb.append(i_processorConfig.i_alarmGroupConfig);
        l_sb.append("\n]");
        l_sb.append(",\nalarmEventCfg=[\n");
        l_sb.append(i_processorConfig.i_alarmEventConfig);
        l_sb.append("\n]");
        l_sb.append(",\nalarmCfg=[\n");
        l_sb.append(i_processorConfig.i_alarmConfig);
        l_sb.append("\n]");
        l_sb.append("]");
        
        return l_sb.toString();
    }

    /** 
     * This private inner class contains all the configuration for the
     * Alarm Event Processor. It allows all the configuration for it to be
     * reloaded atomically (i.e. reference to the config can be changed to the
     * new config in a single atomic assinment). It holds the Alarm Group Config,
     * Alarm Event Config and Alarm Config.
     */
    private class ProcessorConfig
    {
        /** Alarm Group Config. */
        private AlarmGroupConfig i_alarmGroupConfig;
        
        /** Alarm Event Config. */
        private AlarmEventConfig i_alarmEventConfig;
        
        /** Alarm Config. */
        private AlarmConfig     i_alarmConfig;
    }

} // End of public class AlarmEventProcessor.

////////////////////////////////////////////////////////////////////////////////
//End of file
////////////////////////////////////////////////////////////////////////////////