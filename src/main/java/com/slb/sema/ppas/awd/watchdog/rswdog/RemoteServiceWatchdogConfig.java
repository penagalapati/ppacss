////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RemoteServiceWatchdogConfig.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Remote Service Watchdog Server specific config.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfig;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Remote Service Watchdog Server specific config.
 * Namely the config for each monitored service - also see
 * {@link RemoteServiceWatchdogConfig.ServiceConfig}.
 */
public class RemoteServiceWatchdogConfig extends AwdAbstractConfig
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RemoteServiceWatchdogConfig";

    
    /** Utility empty array for use in toArray methods. */
    private static final ServiceConfig C_SERVICE_CONFIG_EMPTY_ARRAY[] = new ServiceConfig[0];

    /** Utility empty array for use in toArray methods. */
    private static final MonitorConfig C_MONITOR_CONFIG_EMPTY_ARRAY[] = new MonitorConfig[0];

    /** Map of service name to {@link ServiceConfig}. */ 
    private Map                 i_serviceConfigMap;
    

    /**
     * Creates a new Remote Service Watchdog Config.
     * @param p_logger          logger to log messages to.
     */
    public RemoteServiceWatchdogConfig(
        Logger                       p_logger
        )
    {
        super(p_logger);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        init();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        
    }

    /** Initialises the config. */
    private void init()
    {
        i_serviceConfigMap = new HashMap();
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_addServiceConfig =
            "addServiceConfig";    

    /**
     * Adds the supplied Service Config.
     * @param p_serviceConfig   service config to add.
     */
    /* package */ void addServiceConfig(
        ServiceConfig           p_serviceConfig
    )
    {

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addServiceConfig + 
                ": Adding mapping of service config " + p_serviceConfig
                );

        i_serviceConfigMap.put(
                p_serviceConfig.getServiceName(), p_serviceConfig);
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_getServiceConfig =
            "getServiceConfig";    

    /**
     * Returns the Service Config for the named service.
     * @param p_serviceName     name of service.
     * @return The Service Config for the named service.
     */
    public ServiceConfig getServiceConfig(
        String                  p_serviceName
    )
    {
        ServiceConfig        l_serviceConfig;

        l_serviceConfig = (ServiceConfig)i_serviceConfigMap.get(
                p_serviceName);

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_END | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_getServiceConfig + 
                ": Returning service config " + l_serviceConfig + " for name " +
                p_serviceName
                );
                
        return l_serviceConfig;
    }
    
    /**
     * Returns an array of all the Service Configs.
     * @return An array of all the Service Configs.
     */
    public ServiceConfig[] getServiceConfigArray()
    {
        // TODO 3 Use a cahced array incase this method called often in future?
        ServiceConfig           l_serviceConfigARR[];
        
        l_serviceConfigARR = (ServiceConfig[])i_serviceConfigMap.values().toArray(
                C_SERVICE_CONFIG_EMPTY_ARRAY);
        
        return l_serviceConfigARR;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("ServiceConfigConfig=[serviceConfigMap=[\n");
        l_sb.append(i_serviceConfigMap);
        l_sb.append("]]");
        
        return l_sb.toString();
    }

    /**
     * Public nested class holding the Service Config for a single service.
     */
    public static class ServiceConfig
    {

        /** Service name. */
        private String          i_serviceName;

        /** Name of home server for the service. */
        private String          i_serviceHomeServer;
        
        /** Name of standby server for the service. */
        private String          i_serviceStandbyServer;
        
        /** VIP Address for this service. */
        private String          i_serviceIpAddress;
        
        /**
         * Watchdog Action Group defining configurable actions to perform
         * when the remote service is detected to have failed. The actions
         * should enable the service on the local node.  
         */
        private String          i_enteredFailedStateWatchdogActionGroupName;

        /**
         * Watchdog Action Group defining configurable actions to perform
         * when the remote service is detected to have passed. Typically
         * this should not happen, as once a remote service fails it
         * should be taken over locally and remote monitoring will stop.
         * So usually does nothing or simply logs an event.  
         */
        private String          i_enteredPassedStateWatchdogActionGroupName;
            
        /**
         * Watchdog Action Group defining configurable actions to perform
         * when requested to release a service. The actions should release
         * any resources required to allow the service to be taken over
         * by a different node (e.g. take down the VIP locally).  
         */
        private String          i_releaseServiceWatchdogActionGroupName;

        /**
         * OS command script to check the VIP status locally for the service.  
         */
        private String          i_checkLocalServiceVipStateCommand;
        
        /**
         * The minimum time between automatic failovers to prevent ping ponging between
         * servers in worst case scenarious. Default currently 10 minutes (see config loader).
         */
        private long            i_mimimumMillisBetweenAutoFailovers;
        
        /**
         * Set of {@link MonitorConfig} which is the set of monitors
         * configured to monitor the service.
         */
        private Set             i_monitorConfigSet;
        
        /**
         * Creates a new Service Config.
         * @param p_serviceName service name.
         * @param p_serviceHomeServer name of home server for service.
         * @param p_serviceStandbyServer name of standby server for service.
         * @param p_serviceIpAddress VIP address for service.
         * @param p_enteredFailedStateWatchdogActionGroupName Watchdog Action Group defining configurable
         * actions to perform when the remote service is detected to have failed.
         * @param p_enteredPassedStateWatchdogActionGroupName Watchdog Action Group defining configurable
         * actions to perform when the remote service is detected to have passed.
         * @param p_releaseServiceWatchdogActionGroupName Watchdog Action Group defining configurable
         * actions to perform when requested to release a service.
         * @param p_checkLocalServiceVipStateCommand OS command script to check the VIP status locally
         * for the service.
         * @param p_minimumMillisBetweenAutoFailovers Minimum time (millis) between automatic failovers
         * to prevent ping ponging in worst case scenarious.
         */
        public ServiceConfig(
            String              p_serviceName,
            String              p_serviceHomeServer,
            String              p_serviceStandbyServer,
            String              p_serviceIpAddress,
            String              p_enteredFailedStateWatchdogActionGroupName,
            String              p_enteredPassedStateWatchdogActionGroupName,
            String              p_releaseServiceWatchdogActionGroupName,
            String              p_checkLocalServiceVipStateCommand,
            long                p_minimumMillisBetweenAutoFailovers
            )
        {
            i_serviceName = p_serviceName;
            i_serviceHomeServer = p_serviceHomeServer;
            i_serviceStandbyServer = p_serviceStandbyServer;
            i_serviceIpAddress = p_serviceIpAddress;
            i_enteredFailedStateWatchdogActionGroupName = p_enteredFailedStateWatchdogActionGroupName;
            i_enteredPassedStateWatchdogActionGroupName = p_enteredPassedStateWatchdogActionGroupName;
            i_releaseServiceWatchdogActionGroupName = p_releaseServiceWatchdogActionGroupName;
            i_checkLocalServiceVipStateCommand = p_checkLocalServiceVipStateCommand;
            i_mimimumMillisBetweenAutoFailovers = p_minimumMillisBetweenAutoFailovers;
            i_monitorConfigSet = new HashSet();
        }

        /**
         * Adds the monitor config.
         * @param p_monitorConfig monitor config to add.
         */
        public void addMonitorConfig(MonitorConfig p_monitorConfig)
        {
            i_monitorConfigSet.add(p_monitorConfig);
        }
        
        /**
         * Returns the service name.
         * @return The service name.
         */
        public String getServiceName()
        {
            return i_serviceName;
        }
        
        /**
         * Returns the serviceHomeServer.
         * @return Returns the serviceHomeServer.
         */
        public String getServiceHomeServer()
        {
            return i_serviceHomeServer;
        }
        /**
         * Returns the serviceStandbyServer.
         * @return Returns the serviceStandbyServer.
         */
        public String getServiceStandbyServer()
        {
            return i_serviceStandbyServer;
        }
        
        /**
         * @return Returns the enteredFailedStateWatchdogActionGroupName.
         */
        public String getEnteredFailedStateWatchdogActionGroupName()
        {
            return i_enteredFailedStateWatchdogActionGroupName;
        }
        
        /**
         * @return Returns the enteredPassedStateWatchdogActionGroupName.
         */
        public String getEnteredPassedStateWatchdogActionGroupName()
        {
            return i_enteredPassedStateWatchdogActionGroupName;
        }
        
        /**
         * @return Returns the releaseServiceWatchdogActionGroupName.
         */
        public String getReleaseServiceWatchdogActionGroupName()
        {
            return i_releaseServiceWatchdogActionGroupName;
        }

        /**
         * Returns the OS command to use to check the local state of the
         * VIP for the service.
         * @return The OS command to use to check the local state of the VIP.
         */
        public String getCheckLocalServiceVipStateCommand()
        {
            return i_checkLocalServiceVipStateCommand;
        }
        
        /**
         * Returns the minimum time (millis) between automatic failovers. 
         * @return The minimum time (millis) between automatic failovers.
         */
        public long getMinimumMillisBetweenAutoFailovers()
        {
            return i_mimimumMillisBetweenAutoFailovers;
        }

        /**
         * Returns an array of the Monitor Configs which defined
         * the monitoring to be performed on the service.
         * @return An array of the Monitor Configs for the service.
         */
        public MonitorConfig[] getMonitorConfigArray()
        {
            // TODO 3 Use a cached array in case called often in future?
            return (MonitorConfig[])i_monitorConfigSet.toArray(
                    C_MONITOR_CONFIG_EMPTY_ARRAY);
        }

        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer        l_sb;
            
            l_sb = new StringBuffer(256);
            l_sb.append(C_CLASS_NAME + ".ServiceConfig=[name=");
            l_sb.append(i_serviceName);
            l_sb.append(", homeNode=");
            l_sb.append(i_serviceHomeServer);
            l_sb.append(", standbyNode=");
            l_sb.append(i_serviceStandbyServer);
            l_sb.append(", serviceIp=");
            l_sb.append(i_serviceIpAddress);
            l_sb.append(", enteredFailStateWdogActionGrpName=");
            l_sb.append(i_enteredFailedStateWatchdogActionGroupName);
            l_sb.append(", enteredPassedStateWdogActionGrpName=");
            l_sb.append(i_enteredPassedStateWatchdogActionGroupName);
            l_sb.append(", releaseServiceWdogActionGrpName=");
            l_sb.append(i_releaseServiceWatchdogActionGroupName);
            l_sb.append("]");
            
            return l_sb.toString();
        }
        
    } // End of public nested class ServiceConfig.

    /**
     * Public nested class holding the config for a service monitor.
     */
    public static class MonitorConfig
    {
        /** Name of class which implements the monitor. */
        private String          i_monitorClass;
        
        /**
         * Map of configuration for the monitor -
         * parameter name (String) to parameter value (String).
         */
        private Map             i_monitorParamMap;

        /**
         * Creates a new Monitor Config.
         * @param p_monitorClass name of class which implements the monitor.
         * @param p_monitorParamMap map of configuration for the monitor -
         * parameter name (String) to parameter value (String).
         */
        public MonitorConfig(
            String              p_monitorClass,
            Map                 p_monitorParamMap)
        {
            i_monitorClass = p_monitorClass;
            i_monitorParamMap = p_monitorParamMap;
        }
        
        
        /**
         * @return Returns the monitorClass.
         */
        public String getMonitorClass()
        {
            return i_monitorClass;
        }
        
        /**
         * @return Returns the monitorParamMap.
         */
        public Map getMonitorParamMap()
        {
            return i_monitorParamMap;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer        l_sb;
            
            l_sb = new StringBuffer(160);
            l_sb.append(C_CLASS_NAME + ".MonitorConfig=[class=");
            l_sb.append(i_monitorClass);
            l_sb.append(", params=");
            l_sb.append(i_monitorParamMap);
            l_sb.append("]");
            
            return l_sb.toString();
        }
        
    } // End of public nested class MonitorConfig
    
} // End of public class RemoteServiceWatchdogConfig.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////