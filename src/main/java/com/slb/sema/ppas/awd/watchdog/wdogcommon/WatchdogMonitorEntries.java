////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogMonitorEntries.java
//      DATE            :       13-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#2644
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Holds a collection of configured watchdog
//                              monitors.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfig;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Holds a collection of configured watchdog
 * monitors.
 */
public class WatchdogMonitorEntries extends AwdAbstractConfig
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "WatchdogMonitorEntries";
    
    /** Utility empty array for use in toArray methods. */
    private static final WdogMonitorEntry C_WATCHDOG_MONITOR_ENTRY_EMPTY_ARRAY[] =
            new WdogMonitorEntry[0];

    /** Watchdog action group config to use. */
    private WatchdogActionGroupConfig i_watchdogActionGroupConfig;

    /**
     * Watchdog monitor entries - map of watchdog monitor name to
     * {@link WdogMonitorEntry}.
     */
    private HashMap             i_watchdogMonitorMap;
    
    /**
     * Indicates whether cache (@link #i_watchdogMonitorEntryCachedARR) is out of
     * date and needs to be recreated before being used again. 
     */
    private boolean             i_cacheInvalidated = true;

    /**
     * Cached array view of {@link #i_watchdogMonitorMap}. For iteration
     * efficiency.
     */
    private WdogMonitorEntry    i_watchdogMonitorEntryCachedARR[];

    /**
     * Creates a new WatchdogMonitorEntries.
     * @param p_logger          logger to log any messages to.
     * @param p_watchdogActionGroupConfig watchdog action group config to use.
     */
    public WatchdogMonitorEntries(
        Logger                       p_logger,
        WatchdogActionGroupConfig    p_watchdogActionGroupConfig
        )
    {
        super(p_logger);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        i_watchdogActionGroupConfig = p_watchdogActionGroupConfig;
        init();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
        
    }

 
    /** Initialises the config. */
    private void init()
    {
        i_watchdogMonitorMap = new HashMap();
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addNewWatchdogMonitorEntry =
            "addNewWatchdogMonitorEntry";
    /**
     * Adds a new Watchdog MonitorEntry.
     * @param p_monitorName     unique name of monitor.
     * @param p_monitorClassName name of monitor class to use.
     * @param p_monitorConfigMap map of configuration for monitor.
     * @param p_enteredFailedStateWatchdogActionGroup watchdog action group to
     *                          use when monitor enteres the failed state.
     * @param p_enteredPassedStateWatchdogActionGroup watchdog action group to
     *                          use when monitor enteres the passed state.
     * @throws AwdConfigException config exception occurred adding monitor entry.
     */
    /* package */ void addNewWatchdogMonitorEntry(
        String                  p_monitorName,
        String                  p_monitorClassName,
        Map                     p_monitorConfigMap,
        String                  p_enteredFailedStateWatchdogActionGroup,
        String                  p_enteredPassedStateWatchdogActionGroup
    )
    throws AwdConfigException
    {
        WdogMonitorEntry        l_entry;
        WatchdogMonitor         l_monitor;
        WatchdogActionGroup     l_enteredFailedStateWatchdogActionGroup;
        WatchdogActionGroup     l_enteredPassedStateWatchdogActionGroup;
        Class                   l_class;
        Constructor             l_constructor;
        AwdConfigException      l_awdE;

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addNewWatchdogMonitorEntry + 
                ": Adding new monitor " + p_monitorName
                );
        
        l_enteredFailedStateWatchdogActionGroup = i_watchdogActionGroupConfig.
               getWatchdogActionGroup(p_enteredFailedStateWatchdogActionGroup);
        l_enteredPassedStateWatchdogActionGroup = i_watchdogActionGroupConfig.
                getWatchdogActionGroup(p_enteredPassedStateWatchdogActionGroup);

        try
        {
            l_class = Class.forName(p_monitorClassName);
            l_constructor = l_class.getConstructor(new Class[] { Logger.class, String.class });
            l_monitor = (WatchdogMonitor)l_constructor.newInstance(
                    new Object[] {i_logger, p_monitorName});
            l_monitor.init(p_monitorConfigMap);
            l_entry = new WdogMonitorEntry(
                    p_monitorName,
                    l_monitor,
                    l_enteredFailedStateWatchdogActionGroup,
                    l_enteredPassedStateWatchdogActionGroup);
        }
        catch(Exception l_e)
        {
            l_awdE =  new AwdConfigException(
                    C_CLASS_NAME,
                    C_METHOD_addNewWatchdogMonitorEntry,
                    10100,
                    this,
                    (PpasRequest)null,
                    (long)0,
                    AwdKey.get().errorInitWatchdogMonitor(
                            p_monitorName,
                            p_monitorClassName),
                    l_e
                    );
            i_logger.logMessage(l_awdE);
            throw l_awdE;
        }

        i_watchdogMonitorMap.put(p_monitorName, l_entry);                

    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getWatchdogMonitorEntry =
            "getWatchdogMonitorEntry";
    /**
     * Returns the named WatchdogMonitorEntry.
     * @param p_watchdogMonitorEntryName name of entry to get.
     * @return Returns the named WatchdogMonitorEntry.
     */
    public WdogMonitorEntry getWatchdogMonitorEntry(
        String                  p_watchdogMonitorEntryName
    )
    {
        WdogMonitorEntry        l_watchdogMonitorEntry;

        l_watchdogMonitorEntry = (WdogMonitorEntry)i_watchdogMonitorMap.get(
                p_watchdogMonitorEntryName);

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_END | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_getWatchdogMonitorEntry + 
                ": Returning watchdog monitor entry " + l_watchdogMonitorEntry + " for name " +
                p_watchdogMonitorEntryName
                );
                
        return l_watchdogMonitorEntry;
    }

    /**
     * Returns an array of all WatchdogMonitorEntries (empty array if no
     * entries).
     * @return Returns an array of all WatchdogMonitorEntries.
     */
    public WdogMonitorEntry[] getWatchdogMonitorEntryArray()
    {
        if(i_cacheInvalidated)
        {
            i_watchdogMonitorEntryCachedARR = (WdogMonitorEntry[])
                    i_watchdogMonitorMap.values().toArray(
                    C_WATCHDOG_MONITOR_ENTRY_EMPTY_ARRAY);
        }
        
        return i_watchdogMonitorEntryCachedARR;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("WatchdogMonitorEntries=[watchdogMonitorEntryMap=[\n");
        l_sb.append(i_watchdogMonitorMap);
        l_sb.append("]]");
        
        return l_sb.toString();
    }

    /**
     * Public inner class implementing a configured entry which holds a
     * WatchdogMonitor and configured details about it.
     */
    public class WdogMonitorEntry
    {
        /** Name of entry. */
        private String          i_name;
        
        /** WatchdogMonitor performing the monitoring for entry. */
        private WatchdogMonitor i_watchdogMonitor;
        
        /** Watchdog action group to use when monitor enteres the failed state.*/
        private WatchdogActionGroup i_enteredFailedStateWatchdogActionGroup;

        /** Watchdog action group to use when monitor enteres the passed state.*/
        private WatchdogActionGroup i_enteredPassedStateWatchdogActionGroup;
        
        /**
         * Creates a new Watchdog Monitor Entry.
         * @param p_name        Name of entry.
         * @param p_watchdogMonitor WatchdogMonitor performing the monitoring for entry.
         * @param p_enteredFailedStateWatchdogActionGroup Watchdog action
         *                      group to use when monitor enteres the failed state.
         * @param p_enteredPassedStateWatchdogActionGroup Watchdog action
         *                      group to use when monitor enteres the passed state.
         */
        public WdogMonitorEntry(
            String              p_name,
            WatchdogMonitor     p_watchdogMonitor,
            WatchdogActionGroup p_enteredFailedStateWatchdogActionGroup,
            WatchdogActionGroup p_enteredPassedStateWatchdogActionGroup
            )
        {
            i_name = p_name;
            i_watchdogMonitor = p_watchdogMonitor;
            i_enteredFailedStateWatchdogActionGroup = p_enteredFailedStateWatchdogActionGroup;
            i_enteredPassedStateWatchdogActionGroup = p_enteredPassedStateWatchdogActionGroup;
        }
        
        /**
         * @return Returns the enteredFailedStateWatchdogActionGroup.
         */
        public WatchdogActionGroup getEnteredFailedStateWatchdogActionGroup()
        {
            return i_enteredFailedStateWatchdogActionGroup;
        }
        /**
         * @return Returns the enteredPssedStateWatchdogActionGroup.
         */
        public WatchdogActionGroup getEnteredPssedStateWatchdogActionGroup()
        {
            return i_enteredPassedStateWatchdogActionGroup;
        }
        /**
         * @return Returns the monitor.
         */
        public WatchdogMonitor getWatchdogMonitor()
        {
            return i_watchdogMonitor;
        }
        /**
         * @return Returns the name.
         */
        public String getName()
        {
            return i_name;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            StringBuffer        l_sb;
            
            l_sb = new StringBuffer(180);
            l_sb.append("WdogMonitorEntry=[name=");
            l_sb.append(i_name);
            l_sb.append(", monitor=");
            l_sb.append(i_watchdogMonitor);
            l_sb.append(", enteredFailedStateWatchdogActionGroup=");
            l_sb.append(i_enteredFailedStateWatchdogActionGroup);
            l_sb.append(", enteredPassedStateWatchdogActionGroup=");
            l_sb.append(i_enteredPassedStateWatchdogActionGroup);
            l_sb.append("]");
            
            return l_sb.toString();
        }
    } // End of public inner class WdogMonitorEntry
    
} // End of public class WatchdogMonitorEntries.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
