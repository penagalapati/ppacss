/*
 * Created on 12-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TemplateTextAlarmFormatter
extends AlarmFormatter
{
            
    /**
     * Macro Substitutor used to expand any macros in alarm template.
     */
    private SimpleMacroSubstitutor i_macroSubstitutor =
            new SimpleMacroSubstitutor();

    /**
     * Creates a new <code>TemplateTextAlarmFormatter</code>.
     * @param p_properties      properties for formatter.
     */
    public TemplateTextAlarmFormatter(UtilProperties p_properties)
    {
        super(p_properties);
        i_macroSubstitutor.addDefaultUtilPropeties(p_properties);
    }

    public String format(
        Alarm                   p_alarm,
        AlarmFormat             p_alarmFormat
    )
    {
        TemplateTextAlarmFormat l_templateTextAlarmFormat;
        
        l_templateTextAlarmFormat = (TemplateTextAlarmFormat)p_alarmFormat;
        return i_macroSubstitutor.expand(
                l_templateTextAlarmFormat.getTemplate(), p_alarm.getParameterMap());
    }

}
