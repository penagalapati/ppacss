////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       OsCommandWatchdogAction.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This defines a Watchdog Action to raise an alarm.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.wdogcommon;


/**
 * This defines a Watchdog Action to execute an Operating System (OS) command.
 * It defines :
 * <ul>
 *     <li>OS command (the OS command to be exexuted).</li>
 * </ul>
 */
public class OsCommandWatchdogAction extends WatchdogAction
{
    /**
     * The OS command to be executed..
     */
    private String              i_osCommand;
        
    /**
     * Creates a new OS Command Watchdog Action.
     * 
     * @param p_osCommand       the OS command to be exexuted.
     */
    public OsCommandWatchdogAction(
        String                  p_osCommand
    )
    {
        super();
        i_osCommand = p_osCommand;
    }

    /**
     * Creates a new OS Command Watchdog Action.
     * 
     * @param p_osCommand       the OS command to be exexuted.
     * @param p_maxActionCount          maximum number of times action should
     *                                  be performed for a process
     *                                  within the action count lifetime. -1
     *                                  means there is no maximum.
     * @param p_actionCountLifetimeMillis lifetime over which maximum number
     *                                  of actions should not be exceeded
     *                                  for a process. -1 means infinite lifetime.
     */
    public OsCommandWatchdogAction(
        String                  p_osCommand,
        int                     p_maxActionCount,
        long                    p_actionCountLifetimeMillis
    )
    {
        super(p_maxActionCount, p_actionCountLifetimeMillis);
        i_osCommand = p_osCommand;
    }

    /**
     * Returns the OS command.
     * 
     * @return The OS command.
     */
    public String getOsCommand()
    {
        return i_osCommand;
    }
   
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return "OsCommandWatchdogAction=[osCommand=" +
                i_osCommand + "," + super.toString() + "]";
    }

} // End of public class OsCommandWatchdogAction

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////