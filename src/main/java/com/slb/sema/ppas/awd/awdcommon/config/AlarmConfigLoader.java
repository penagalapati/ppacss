/*
 * Created on 17-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmConfigLoader extends AwdAbstractConfigLoader
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmConfigLoader";

    /** Loader for alarm configuration.
     * 
     * @param p_logger Logger for directing loggable events.
     * @param p_configProperties Configuration properties.
     * @param p_awdContext Session context.
     * 
     */
    public AlarmConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext)
    {
        super(p_logger, p_configProperties, p_awdContext);

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                        "Constructing " + C_CLASS_NAME);
        }
        
        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                        "Constructed " + C_CLASS_NAME);
        }
    }

    /** Load alarm configuration.
     * 
     * @param p_configFilename Name of file containing configuration.
     * @return Alark configuration.
     * @throws AwdConfigException Ifthe configuration cannot be loaded.
     */
    public AlarmConfig loadAlarmConfig(String p_configFilename)
        throws AwdConfigException
    {
        AlarmConfig             l_config;
        
        l_config = new AlarmConfig();
        
        // loadTestConfig(l_config);
        loadLayeredConfig(p_configFilename, l_config);
        
        return l_config;
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_loadSingleInstanceConfig = "loadSingleInstanceConfig";
    /** Load a single instance of alarm configuration.
     * 
     * @param p_configFilename          Name of file contianing the configuration.
     * @param p_alarmInstanceConfigName Alarm instance for which configuration should be loaded.
     * @return Configuration for this instance.
     * @throws AwdConfigException If the configuration could not be loaded.
     */
    public AlarmInstanceConfig loadSingleInstanceConfig(
        String                  p_configFilename,
        String                  p_alarmInstanceConfigName)
    throws AwdConfigException
    {
        AlarmInstanceConfig     l_alarmInstanceConfig;
        AlarmConfig             l_config;
        AwdConfigException      l_awdConfigE;

        l_config = new AlarmConfig();
        loadLayeredConfig(p_configFilename, l_config);
        
        l_alarmInstanceConfig = l_config.getAlarmInstanceConfig(p_alarmInstanceConfigName);
        if(l_alarmInstanceConfig == null)
        {
            l_awdConfigE = new AwdConfigException(
                    C_CLASS_NAME,
                    C_METHOD_loadSingleInstanceConfig,
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().unknownAlarmInstanceName(p_alarmInstanceConfigName));
            i_logger.logMessage(l_awdConfigE);
            throw l_awdConfigE;
        }
        
        return l_alarmInstanceConfig;

    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    /** Load cconfiguration.
     * 
     * @param p_configFilename  Name of file containing configuration.
     * @param p_alarmConfig     Alarm configuration object to which configuration is added.
     * @throws AwdConfigException If configuration fails.
     */
    private void loadLayeredConfig(
        String                  p_configFilename,
        AlarmConfig             p_alarmConfig)
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_alarmConfig);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_alarmConfig);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }
    
    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_loadConfig = "loadConfig";
    /** Load cconfiguration.
     * 
     * @param p_configFilename  Path of file containing configuration.
     * @param p_alarmConfig     Alarm configuration object to which configuration is added.
     * @throws AwdConfigException If configuration fails.
     */
    private void loadConfig(
        String                  p_configFilename,
        AlarmConfig             p_alarmConfig)
    throws AwdConfigException
    {
        ConfigTokenizer         l_tokenizer;
        String                  l_token;
        boolean                 l_eos = false;
        String                  l_alarmName;
        String                  l_alarmText;
        int                     l_alarmSeverity;
        String                  l_parameterName;
        String                  l_parameterValue;
        Map                     l_parameterMap;
        AlarmInstanceConfig     l_alarmInstanceConfig;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        while(!l_eos)
        {
            l_token = l_tokenizer.getExpectedNextToken(
                    new String[] {null, "alarm.instance.name"});
            if(l_token != null)
            {
                l_tokenizer.getExpectedNextToken("=");
                l_alarmName = l_tokenizer.getExpectedNextToken();
                l_tokenizer.getExpectedNextToken("{");
                l_tokenizer.getExpectedNextToken("text");
                l_tokenizer.getExpectedNextToken("=");
                l_alarmText = l_tokenizer.getExpectedNextToken();
                l_tokenizer.getExpectedNextToken("severity");
                l_tokenizer.getExpectedNextToken("=");
                l_alarmSeverity = l_tokenizer.getExpectedNextInt();
                l_parameterMap = null;
                while(!"}".equals(l_token = l_tokenizer.getExpectedNextToken(
                        new String[] {"parameter", "}"} )))
                {
                    if(l_parameterMap == null)
                    {
                        l_parameterMap = new HashMap();
                    }
                    l_tokenizer.getExpectedNextToken("=");
                    l_parameterName = l_tokenizer.getExpectedNextToken();
                    l_tokenizer.getExpectedNextToken("=");
                    l_parameterValue = l_tokenizer.getExpectedNextToken();
                    l_parameterMap.put(l_parameterName, l_parameterValue);
                }
                
                l_alarmInstanceConfig = new AlarmInstanceConfig(
                        l_alarmName,
                        l_alarmText,
                        l_alarmSeverity,
                        l_parameterMap
                        );
                p_alarmConfig.addAlarmInstanceConfig(l_alarmInstanceConfig);                               
            
            }
            else
            {
                l_eos = true;
            }
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                        "Leaving " + C_METHOD_loadConfig);
        }
    }
}
