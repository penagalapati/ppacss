////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmDeliveryManager.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       An Alarm Delivery Manager delivers a single
//                              Alarm to a single destination (or performs a
//                              single alarm action).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.common.awd.alarmcommon.*;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An Alarm Delivery Manager delivers a single Alarm to a single
 * destination using the appropriate alarm handler (see 
 * {@link com.slb.sema.ppas.awd.awdcommon.alarm.AlarmHandler})
 * to actually deliver the alarm.
 * <p/>
 * The supported actions (one handler for each) are :
 * <ul>
 *     <li>Log Alarm - log alarm to an alarm log file.</li>
 *     <li>FTP Alarm - FTP alarm to a remote host.</li>
 *     <li>Email Alarm - email alarm to an email address.</li>
 *     <li>Terminal Alarm - display alarm on users terminal/console.</li>
 * </ul>
 * <p/>
 * The supported destinations are :
 * <ul>
 *     <li>User - username and/or email address for email/terminal action.</li>
 *     <li>FTP - remote host, username, password and filename for FTP action.</li>
 *     <li>Log file - file to log alarm to for log action.</li>
 * </ul>
 * <p/>
 * The supported formats are :
 * <ul>
 *     <li>Template Text - powerful template / macro driven text formatter.</li>
 * </ul>
 */
public class AlarmDeliveryManager
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmDeliveryManager";

    /**
     * Last sequence number of alarm sent by Delivery Manager.
     * Starts at 0 each time this class is loaded and is incremented before
     * each alarm is sent.
     */    
    private static int          c_lastAlarmSequenceNumber = 0;
    
    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;
    
    /**
     * Map of handler type names (<code>String</code>) to handler objects
     * (subclasses of {@link AlarmHandler}) which will actually peform the
     * alarm action.
     */
    private Map                 i_actionHandlerMap;

    /**
     * Map of format type names (<code>String</code>) to formatter objects
     * (subclasses of {@link AlarmFormat}) which will format the
     * alarm.
     */
    private Map                 i_alarmFormatMap;

    /**
     * Creates a new Alarm Delivery Manager.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_instrumentManager instrument Manager to register any
     *                          instruments with.
     * @param  p_awdContext     context for this component.
     */   
    public AlarmDeliveryManager(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
    )
    {
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
    }    

    /**
     * Initialises the Alarm Delivery Manager based on supplied configuration.
     * Currently no configuration is used by this class, but the configuration
     * is passed on to the alarm handlers and alarm formatters for them to
     * configure themselves.
     * 
     * @param  p_configProperties configuration properties.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    {
        i_actionHandlerMap = new HashMap();
        i_actionHandlerMap.put(AlarmAction.C_TYPE_LOG_FILE,
                new LogfileAlarmHandler(
                        i_logger,
                        p_configProperties,
                        i_instrumentManager,
                        i_awdContext
                        ));
        i_actionHandlerMap.put(AlarmAction.C_TYPE_TERMINAL,
                new TerminalAlarmHandler(
                        i_logger,
                        p_configProperties,
                        i_instrumentManager,
                        i_awdContext
                        ));
        i_actionHandlerMap.put(AlarmAction.C_TYPE_FTP,
                new FtpAlarmHandler(
                        i_logger,
                        p_configProperties,
                        i_instrumentManager,
                        i_awdContext
                        ));
        i_actionHandlerMap.put(AlarmAction.C_TYPE_EMAIL,
                new EmailAlarmHandler(
                        i_logger,
                        p_configProperties,
                        i_instrumentManager,
                        i_awdContext
                        ));

                
        i_alarmFormatMap = new HashMap();
        i_alarmFormatMap.put(AlarmFormat.C_TYPE_DEFAULT_TEXT,
                new DefaultTextAlarmFormatter(p_configProperties));
        i_alarmFormatMap.put(AlarmFormat.C_TYPE_TEMPLATE_TEXT,
                new TemplateTextAlarmFormatter(p_configProperties));
    }

    /**
     * Performs a single action for a single destination to process the Alarm.
     * The alarm is formatted according to the
     * Alarm Format, and then the action is performed for the desintation by
     * calling the appropriate alarm handler.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_destination    destination to send alarm to.
     * @param  p_action         actions to send alarm.
     * @param  p_alarmFormat    format of sent alarm.
     */
    public void deliverAlarm(
        Alarm                   p_alarm,
        AlarmDestination        p_destination,
        AlarmAction             p_action,
        AlarmFormat             p_alarmFormat
    )
    {
        AlarmHandler            l_alarmHandler;
        AlarmFormatter          l_alarmFormatter;
        String                  l_formattedAlarm;
        
        // Set Alarm Sequence Number on alarm.
        // Get alarm formater and format alarm.
        // Get alarm handler and call it to deliver alarm.
        p_alarm.getParameterMap().put(
                AlarmParameterConstants.C_PARAM_ALARM_SEQUENCE_NUMBER,
                Integer.toString((getNextAlarmSequenceNumber()))
                );
        l_alarmFormatter = (AlarmFormatter)i_alarmFormatMap.get(
                p_alarmFormat.getFormatTypeName());
        l_formattedAlarm = l_alarmFormatter.format(p_alarm, p_alarmFormat);
        l_alarmHandler = (AlarmHandler)i_actionHandlerMap.get(
                p_action.getAlarmTypeName());
        l_alarmHandler.deliverAsynchAlarm(p_alarm, l_formattedAlarm, p_destination);
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( C_CLASS_NAME + "=[handlerMap=" + i_actionHandlerMap +
                ", formatterMap=" + i_alarmFormatMap + "]");
    }

    /**
     * Returns a class unique alarm sequence number.
     * @return Returns a class unique sequence number.
     */
    private synchronized static int getNextAlarmSequenceNumber()
    {
        c_lastAlarmSequenceNumber++;
        
        return c_lastAlarmSequenceNumber;
    }
        
} // End of public class AlarmDeliveryManager

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////