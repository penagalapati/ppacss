/*
 * Created on 06-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.HashSet;
import java.util.Set;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogActionGroup
{

    private static final WatchdogAction C_EMPTY_ALARM_ACTION_ARRAY[] =
            new WatchdogAction[0];

    private String              i_groupName;
    private Set                 i_watchdogActionSet;
    private boolean             i_cacheInvalidated;
    private WatchdogAction      i_watchdogActionCachedARR[];

    public WatchdogActionGroup(
        String                  p_groupName
    )
    {
        super();
        i_groupName = p_groupName;
        init();
    }
    
    public synchronized void init()
    {
        i_watchdogActionSet = new HashSet();
        i_cacheInvalidated = true;
    }
    
    public String getGroupName()
    {
        return(i_groupName);
    }
    
    public synchronized void addWatchdogAction(WatchdogAction p_watchdogAction)
    {
        i_cacheInvalidated = true;
        i_watchdogActionSet.add(p_watchdogAction);
    }

    public synchronized WatchdogAction[] getActionArray()
    {

        if(i_cacheInvalidated)
        {
            i_watchdogActionCachedARR = (WatchdogAction[])i_watchdogActionSet.
                    toArray(C_EMPTY_ALARM_ACTION_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_watchdogActionCachedARR;
    }

    public String toString()
    {
        StringBuffer            l_sb;
        WatchdogAction          l_actionARR[];
        int                     l_loop;
        
        l_sb = new StringBuffer(100);
        
        l_sb.append("WatchdogActionGroup=[name=");
        l_sb.append(i_groupName);
        l_sb.append(", actionARR=[\n");
        l_actionARR = getActionArray();
        for(l_loop = 0; l_loop < l_actionARR.length; l_loop++)
        {
            l_sb.append("    ");
            l_sb.append(l_actionARR[l_loop].toString());
            l_sb.append("\n");
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

}
