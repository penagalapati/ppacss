////////////////////////////////////////////////////////////////////////////////
//PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       ConfigTokenizer.java
//DATE            :       28-May-2004
//AUTHOR          :       Marek Vonka
//REFERENCE       :       PpaLon#246
//
//COPYRIGHT       :       ATOSORIGIN 2004
//
//DESCRIPTION     :       Utility class which helps tokenize AWD configuration files.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//25/10/05 | M.Vonka    | Minor fix to attach missing     | PpacLon#1772/7309
//         |            | originating exception. Also     |
//         |            | mssing Javadoc added.           |
//---------+------------+---------------------------------+--------------------
//         |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.SimpleMacroSubstitutor;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Utility class which helps tokenize AWD configuration files. As well as allowing low
 * level token access to the configuration it also supports slightly higher level access
 * such as getting the next parameter (&lt;parameter_name&gt;=&lt;parameter_value&gt;) and helps with
 * handling optional &amp; mandatory tokens &amp; parameters and associated exception generation and
 * logging .
 * <p/>
 * The low level tokenizer behaves as follows:
 * <ul>
 *   <li>Skips comments - starting from hash(#).</li>
 *   <li>Treats control codes as white space (including space, tab etc).</li>
 *   <li>Treats equals(=) and curly braces ({}) as ordinary (i.e. as separate tokens).</li>
 *   <li>Treats single quote(') and double quote(") as delimiting
 *       String literals. String literals are returned as whole tokens,
 *       quotes are not returned.</li>
 * </ul>
 */
public class ConfigTokenizer
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ConfigTokenizer";
    
    /** <code>Logger</code> used by this class. */
    private Logger              i_logger;

    /** Number Format for outputting line numbers. */
    private static final NumberFormat C_LINE_NO_NF = new DecimalFormat("00000");
    
    /**
     * Macro Substitutor which can be used by derived classes to expand any
     * macros where required.
     */
    protected SimpleMacroSubstitutor i_macroSubstitutor;

    /** Reader for reading the configuration file. */
    private Reader              i_reader;
    
    /** Stream Tokenizer used to low level tokenize the configuration file (Reader). */
    private StreamTokenizer     i_tokenizer;
    
    /** Configuration file name. */
    private String              i_filename;

    /**
     * Creates a new ConfigTokenizer.
     * 
     * @param p_logger          Logger to log any exceptions to.
     * @param p_filename        name of configuration file to tokenize.
     * @throws AwdConfigException configuration exception occurred (such as file not found).
     */
    public ConfigTokenizer(
        Logger                  p_logger,
        String                  p_filename
        )
    throws AwdConfigException
    {
        this(p_logger, p_filename, new SimpleMacroSubstitutor('^'));
    }
    
    /**
     * Creates a new ConfigTokenizer.
     * 
     * @param p_logger          Logger to log any exceptions to.
     * @param p_filename        name of configuration file to tokenize.
     * @param p_simpleMacroSubstitutor macro substitutor to use to substitute any macros found.
     * @throws AwdConfigException configuration exception occurred (such as file not found).
     */
    public ConfigTokenizer(
        Logger                  p_logger,
        String                  p_filename,
        SimpleMacroSubstitutor  p_simpleMacroSubstitutor
        )
    throws AwdConfigException
    {
        super();
        
        i_logger = p_logger;
        i_macroSubstitutor = p_simpleMacroSubstitutor;
        
        AwdConfigException      l_exception;
        
        try
        {
            init(new FileReader(p_filename));
            i_filename = p_filename;
        }
        catch(FileNotFoundException l_e)
        {
            l_exception = new AwdConfigException(
                    C_CLASS_NAME,
                    C_CLASS_NAME,
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().fileNotFound(p_filename),
                    l_e);
            logMessage(l_exception);
            throw l_exception;
        }
    }


    /**
     * Creates a new ConfigTokenizer.
     * 
     * @param p_logger          Logger to log any exceptions to.
     * @param p_reader          Reader to configuration to tokenize.
     * @param p_filename        name of configuration "file" (for logging).
     */
    public ConfigTokenizer(
        Logger                  p_logger,
        Reader                  p_reader,
        String                  p_filename
        )
    {
        this(p_logger, p_reader, p_filename, (SimpleMacroSubstitutor)null);
    }

    /**
     * Creates a new ConfigTokenizer.
     * 
     * @param p_logger          Logger to log any exceptions to.
     * @param p_reader          Reader to configuration to tokenize.
     * @param p_filename        name of configuration "file" (for logging).
     * @param p_simpleMacroSubstitutor macro substitutor to use to substitute any macros found.
     */
    public ConfigTokenizer(
        Logger                  p_logger,
        Reader                  p_reader,
        String                  p_filename,
        SimpleMacroSubstitutor  p_simpleMacroSubstitutor
        )
    {
        i_logger = p_logger;
        i_macroSubstitutor = p_simpleMacroSubstitutor;
        
        init(p_reader);
        i_filename = p_filename;
    }

    /**
     * Initialises the Config Tokenizer.
     * 
     * @param p_reader          Reader to configuration to tokenize.
     */
    private void init(Reader p_reader)
    {
        
        i_reader = p_reader;
        i_tokenizer = new StreamTokenizer(i_reader);
        // Set up tokenizer:
        //     - Skip comments - starting  from hash(#).
        //     - Treat control codes as white space (including space, tab etc).
        //     - Treat equals(=) and curly braces ({}) as ordinary (i.e. separte token).
        //     - Treat single quote(') and double quote(") as delimiting
        //       string literals. String literals are returned as whole tokens,
        //       quotes are not returned.
        i_tokenizer.resetSyntax();
        i_tokenizer.whitespaceChars(0,32);
        i_tokenizer.wordChars(33, 126);
        i_tokenizer.ordinaryChar('=');
        i_tokenizer.ordinaryChar('{');
        i_tokenizer.ordinaryChar('}');
        i_tokenizer.commentChar('#');
        i_tokenizer.quoteChar('"');
        i_tokenizer.quoteChar('\'');
    }

    /**
     * Returns the (mandatory) next token as a String. If there is no next
     * token, an exception is thrown. 
     * @return The next token as a String.
     * @throws AwdConfigException configuration exception (such as missing mandatory next token). 
     */
    public String getExpectedNextToken()
    throws AwdConfigException
    {
        String                  l_token;

        l_token = nextToken();
        if(l_token == null)
        {
            handleUnexpectedEos(C_METHOD_getExpectedNextToken, 11060);
        }
      
       return l_token;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getExpectedNextParameter = "getExpectedNextParameter";

    /**
     * Returns the value of the next mandatory parameter. If there is no next parameter or
     * next parameter in the configuration
     * does not match the supplied parameter name an exception is thrown.
     * @param p_expected        the name of the parameter expected next.
     * @return                  The value of the next parameter
     * @throws AwdConfigException configuration exception occurred (such as unexpected next parameter
     *                          where the name of next parameter does not match the supplied name).
     */
    public String getExpectedNextParameter(String p_expected)
    throws AwdConfigException
    {
        String                  l_token;
        String                  l_value = null;
        
        l_token = getExpectedNextToken();
        if(l_token.equals(p_expected))
        {
            getExpectedNextToken("=");
            l_value = getExpectedNextToken();
            l_value = i_macroSubstitutor.expand(l_value);
        }
        else
        {
            handleUnexpectedToken(C_METHOD_getExpectedNextParameter, 11020, l_token, p_expected);
        }
       
       return l_value;
    }

    /**
     * Returns the value of the next optional parameter. If the next parameter in the configuration
     * does not match the supplied parameter name, nothing is consumed and <code>null</code> is
     * returned.
     * @param p_optionalParameterName the name of the optional parameter expected next.
     * @return                  the value of the next parameter or <code>null</code>
     *                          if optional parameter not present.
     * @throws AwdConfigException configuration exception occurred.
     */
    public String getOptionalNextParameter(String p_optionalParameterName)
    throws AwdConfigException
    {
        String                  l_token;
        String                  l_value = null;
        
        l_token = getExpectedNextToken();
        if(l_token.equals(p_optionalParameterName))
        {
            getExpectedNextToken("=");
            l_value = getExpectedNextToken();
            l_value = i_macroSubstitutor.expand(l_value);
        }
        else
        {
            i_tokenizer.pushBack();
        }
       
       return l_value;
    }

    /**
     * Returns the value of the next optional parameter. If the next parameter in the configuration
     * does not match the supplied parameter name, nothing is consumed and <code>p_default</code> is
     * returned.
     * @param p_optionalParameterName the name of the optional parameter expected next.
     * @param p_default         default to return if optional parameter not present.
     * @return                  The value of the next parameter or <code>p_default</code>
     *                          if optional parameter not present.
     * @throws AwdConfigException configuration exception occurred.
     */
    public String getOptionalNextParameter(
         String                 p_optionalParameterName,
         String                 p_default
         )
    throws AwdConfigException
    {
        String                  l_value;

        l_value = getOptionalNextParameter(p_optionalParameterName);
        if(l_value == null)
        {        
            l_value = p_default;
        }
       
        return l_value;
    }

    /**
     * Returns the value of the next optional parameter as an <code>int</code>.
     * If the next parameter in the configuration
     * does not match the supplied parameter name, nothing is consumed and <code>p_default</code> is
     * returned.
     * @param p_optionalParameterName the name of the optional parameter expected next.
     * @param p_default         default to return if optional parameter not present.
     * @return                  The value of the next parameter as an <code>int</code>
     *                          or <code>p_default</code>
     *                          if optional parameter not present.
     * @throws AwdConfigException configuration exception occurred.
     */
    public int getOptionalNextParameterInt(
        String                  p_optionalParameterName,
        int                     p_default
        )
    throws AwdConfigException
    {
        String                  l_token;
        int                     l_value;
        
        l_token = getExpectedNextToken();
        if(l_token.equals(p_optionalParameterName))
        {
            getExpectedNextToken("=");
            l_value = getExpectedNextInt();
        }
        else
        {
            i_tokenizer.pushBack();
            l_value = p_default;
        }
       
       return l_value;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getExpectedNextInt = "getExpectedNextInt";

    /**
     * Returns the (mandatory) next token as an <code>int</code>. If there is no
     * next token, an exception is throw. 
     * @return The next token as an <code>int</code>.
     * @throws AwdConfigException configuration exception (such as missing mandatory next token). 
     */
    public int getExpectedNextInt()
    throws AwdConfigException
    {
        String                  l_token;
        int                     l_int;
        
        l_token = nextToken();
        if(l_token == null)
        {
            handleUnexpectedEos(C_METHOD_getExpectedNextInt, 11050);
        }
        l_token = i_macroSubstitutor.expand(l_token);
        l_int = Integer.parseInt(l_token);
       
        return l_int;
    }

    /**
     * Returns the (mandatory) next token as a String which must match <code>p_expected</code>.
     * If there is no next token or it doesn't match, an exception is thrown.
     * TODO: Check behaviour of this method - it currently returns NULL if no next token! What should it do?
     * @param p_expected        expected next token (<code>null</code> not allowed). 
     * @return The next token as a String.
     * @throws AwdConfigException configuration exception (such as missing mandatory next token). 
     */
    public String getExpectedNextToken(String p_expected)
    throws AwdConfigException
    {
        String                  l_token;
        
        l_token = nextToken();
        if(l_token != null)
        {
            if(!p_expected.equals(l_token))
            {
                handleUnexpectedToken(C_METHOD_getExpectedNextToken, 11040, l_token, p_expected);
            }
       }
       
       return l_token;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getExpectedNextToken = "getExpectedNextToken";
   // Null allowed in array as valid value (EOS)!
    /**
     * Returns the (mandatory) next token as a String which must match one of
     * <code>p_expectedARR</code>.
     * If there is no next token or it doesn't match, an exception is thrown.
     * <code>null</code> is allowed as an entry in <code>p_expectedARR</code> (which matches
     * end of configuration).
     * @param p_expectedARR        array of valid, expected next token (array must not be <code>null</code>
     *                             but <code>null</code> allowed as entry in array). 
     * @return The next token as a String.
     * @throws AwdConfigException configuration exception (such as next token does not match). 
     */
    public String getExpectedNextToken(String p_expectedARR[])
    throws AwdConfigException
    {
        String                  l_token;
        int                     l_loop;
        boolean                 l_matched = false;
        StringBuffer            l_sb;
        
        l_token = nextToken();
        for(   l_loop = 0;
               !l_matched && (l_loop < p_expectedARR.length);
               l_loop++)
        {
            if(p_expectedARR[l_loop] == null)
            {
                if(l_token == null)
                {
                    l_matched = true;
                }
            }
            else if(p_expectedARR[l_loop].equals(l_token))
            {
                l_matched = true;
            }
        }
        if(!l_matched)
        {
            l_sb = new StringBuffer(160);
            
            l_sb.append("one of ");
            for(l_loop = 0; l_loop < p_expectedARR.length; l_loop++)
            {
                if(l_loop > 0)
                {
                    l_sb.append(", ");
                }
                if(p_expectedARR[l_loop] != null)
                {
                    l_sb.append("'");
                    l_sb.append(p_expectedARR[l_loop]);
                    l_sb.append("'");
                }
                else
                {
                    l_sb.append("end of stream");
                }
            }
            handleUnexpectedToken(C_METHOD_getExpectedNextToken, 11030, l_token, l_sb.toString());
        }
       
       return l_token;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_nextToken = "nextToken";

    /**
     * Returns the next token or <code>null</code> if no next token (end of stream).
     * @return The next token or <code>null</code> if no next token (end of stream).
     * @throws AwdConfigException configuration exception occurred.
     */
    public String nextToken()
    throws AwdConfigException
    {
        String                  l_token = null;
        int                     l_tokenType;
        String                  l_lineNumberString;
        AwdConfigException      l_exception;

        try
        {
             l_tokenType = i_tokenizer.nextToken();
        }
        catch(IOException l_e)
        {
            l_exception = new AwdConfigException(
                    C_CLASS_NAME,
                    C_METHOD_nextToken,
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().errorReadingNextToken(),
                    l_e);
            logMessage(l_exception);
            throw l_exception;
        }
        if(l_tokenType != StreamTokenizer.TT_EOF)
        {
            switch(l_tokenType)
            {
            case StreamTokenizer.TT_EOL:

                // NOTE this should not happen as EOL as significant is disabled.

                if(Debug.on)
                {
                    l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                    Debug.print(
                            Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            l_lineNumberString + " TT_EOL");
                }
                l_token = Character.toString((char)l_tokenType);
                break;
                
            case StreamTokenizer.TT_WORD:
            
                if(Debug.on)
                {
                    l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                    Debug.print(
                            Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            l_lineNumberString + " TT_WORD:   [" +
                            i_tokenizer.sval + "]");
                }
                l_token = i_tokenizer.sval;
                break;
                
            case StreamTokenizer.TT_NUMBER:
            
                // NOTE this should not happen as parsing numbers is disabled.

                if(Debug.on)
                {
                    l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                    Debug.print(
                            Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            l_lineNumberString + " TT_NUMBER: [" +
                            i_tokenizer.nval + "]");
                }
                l_token = Double.toString(i_tokenizer.nval);
                break;
            case '"':
            case '\'':
                 if(i_tokenizer.sval != null)
                 {
                     if(Debug.on)
                     {
                         l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                         Debug.print(
                                 Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                                 Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                                 l_lineNumberString + " Quote:     [" +
                                 i_tokenizer.sval + "]");
                     }
                     l_token = i_tokenizer.sval;
                 }
                 else
                 {

                     // NOTE this should not happen as " and ' are set as Quote chars.

                     if(Debug.on)
                     {
                         l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                         Debug.print(
                                 Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                                 Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                                 l_lineNumberString + " TT_CHAR:   [" +
                                 (char)l_tokenType + "]");
                     }
                     l_token = Character.toString((char)l_tokenType);
                 }
                 break;
            default:
                 // Ordinary char
                if(Debug.on)
                {
                    l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
                    Debug.print(
                            Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                            Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                            l_lineNumberString + " Char:      [" +
                            (char)l_tokenType + "]");
                }
                l_token = Character.toString((char)l_tokenType);
                break;
            }
        }

        if(Debug.on)
        {
            l_lineNumberString = C_LINE_NO_NF.format(i_tokenizer.lineno());
            Debug.print(
                    Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                    Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                    "Leaving nextToken, returning token [" + l_token + "] at line number " +
                    l_lineNumberString);
        }
        
        return l_token;
    }
    
    /**
     * Returns the current line number in the configuration being tokenized.  
     * @return Current line number in the configuration being tokenized.
     */
    public int getLineNumber()
    {
        return i_tokenizer.lineno();
    }
    
    /**
     * Creates and logs a configuration exception for an unexpected token.
     * 
     * @param p_methodName      method where exception detected.
     * @param p_statementNumber statement number where exception detected.
     * @param p_unexpectedToken the unexpected token.
     * @param p_expectedToken   the token or tokens that were expected.
     * @throws AwdConfigException the logged configruation exception.
     */
    private void handleUnexpectedToken(
        String                  p_methodName,
        int                     p_statementNumber,
        String                  p_unexpectedToken,
        String                  p_expectedToken
    )
    throws AwdConfigException
    {
        AwdConfigException      l_exception;
        
        l_exception = new AwdConfigException(
                C_CLASS_NAME,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                AwdKey.get().unexpectedTokenAtLine(
                        p_unexpectedToken,getLineNumber(), i_filename, p_expectedToken));
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Creates and logs a configuration exception for an unexpected end of configuration
     * (end of stream).
     * 
     * @param p_methodName      method where exception detected.
     * @param p_statementNumber statement number where exception detected.
     * @throws AwdConfigException the logged configruation exception.
     */
    private void handleUnexpectedEos(
        String                  p_methodName,
        int                     p_statementNumber
    )
    throws AwdConfigException
    {
        AwdConfigException      l_exception;
        
        l_exception = new AwdConfigException(
                C_CLASS_NAME,
                p_methodName,
                p_statementNumber,
                this,
                (PpasRequest)null,
                0,
                AwdKey.get().unexpectedEosAtLine(getLineNumber(), i_filename));
        logMessage(l_exception);
        throw l_exception;
    }

    /**
     * Logs the message to the <code>Logger</code> if one was supplied at construction,
     *  otherwise writes it to <code>System.err</code>.
     * @param l_loggable        Loggable to be logged.
     */
    private void logMessage(LoggableInterface l_loggable)
    {
        if(i_logger == null)
        {
            System.err.println(l_loggable.getMessageText(Locale.getDefault()));
        }
        else
        {
            i_logger.logMessage(l_loggable);
        }
    }
    
} // End of public class ConfigTokenizer.

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////