////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       HttpPingMonitor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Monitor which uses a configurable "ping" request
//                              and configurable regex matched response over
//                              HTTP.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.httpmonitor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Pattern;

import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.AbstractWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Monitor which uses a configurable "ping" request
 * and configured regex matched response over
 * HTTP.
 * <p/>
 * Currently only supports HTTP/1.1 and non-persistant connections. A more complex
 * version could be added in future (perhaps using Apache's HttpClient).
 * <p/>
 * Supports the following configuration parameters:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>httpPingServer</td>
 *     <td>Server domain name or IP address of HTTP server to monitor</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>httpPingPort</td>
 *     <td>Server port number of HTTP server to monitor</td>
 *     <td>O</td>
 *     <td>80</td>
 *   </tr>
 *   <tr>
 *     <td>httpPingRequestLine</td>
 *     <td>HTTP request line to use to request &quot;ping&quot; - e.g.
 *         &quot;GET /ascs/posi/Ping HTTP/1.1&quot;</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>httpPingResponseRegex</td>
 *     <td>Regex to use to check whole response against (inlcuding HTTP repsonse line and headers).
 *         If matches &quot;ping&quot; deemed successful, if doesn't match
 *         &quot;ping&quot; deemed unsuccessful.
 *         e.g. &quot;(?s)HTTP.+ 200 OK.+&lt;html&gt;.*&lt;head&gt;.*
 *         &lt;/head&gt;.*&lt;body&gt;.*OK.*&lt;/body&gt;.*&lt;/html&gt;.*&quot;</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a &quot;ping&quot; fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>connectTimeoutMillis</td>
 *     <td>Milliseconds to wait for connection to succeed before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for response to be received before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 * </table>
 */
public class HttpPingMonitor extends AbstractWatchdogMonitor
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "HttpPingMonitor";

    /** Internal buffer size. Value is {@value}. */
    private static final int    C_BUFFER_SIZE = 8196;
    
    // private static final String C_PING_RESPONSE_REGEX =
    //         "(?s)HTTP.+ 200 OK.+<html>.*<head>.*</head>.*<body>.*OK.*</body>.*</html>.*";
    
    
    /** The configured regex to check response against (match=passed, not-matched=failed). */
    private String              i_pingResponseRegex;
    
    /** The IP address or domain name of the HTTP server to ping. */
    private String              i_httpPingServer = "localhost";
    
    /** The port number of the HTTP server to ping. */
    private int                 i_httpPingPort = 80;
    
    /**
     * The configured HTTP request line (first line) defining the "ping"
     * request to use. For example, "GET /my/ping/url HTTP/1.1"
     */
    private String              i_httpRequestLine;
       
    /**
     * Compiled version of configured response string regex pattern to match
     * successful "ping" response.
     */
    private Pattern             i_responsePattern;
    
    /**
     * Creates a new HTTP Ping Monitor.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public HttpPingMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        super(p_logger, p_monitoredServiceName);
    }

    /**
     * Initialises the HTTP Ping Monitor from the configuration parameters.
     * @param p_configParamMap  Map of parameter name (String) to parameter value (String).
     * @throws Exception        exception occurred during initialisation.
     */
    protected void doInit(Map p_configParamMap)
    throws Exception
    {
        String                  l_httpPingPortString;
        
        i_httpPingServer = (String)p_configParamMap.get("httpPingServer");
        if(i_httpPingServer == null || "".equals(i_httpPingServer))
        {
            i_httpPingServer = "localhost";
        }        
        l_httpPingPortString = (String)p_configParamMap.get("httpPingPort");
        if(l_httpPingPortString == null)
        {
            i_httpPingPort = 80; // Official IANA assigned http port number
        }
        else
        {
            i_httpPingPort = Integer.parseInt(l_httpPingPortString);
        }
        // e.g. GET /ascs/posi/Ping HTTP/1.1
        i_httpRequestLine = (String)p_configParamMap.get("httpPingRequestLine");
        i_pingResponseRegex = (String)p_configParamMap.get("httpPingResponseRegex");
        
        i_responsePattern = Pattern.compile(i_pingResponseRegex);
    }

    // TODO 3 Implement a thread which constantly reads from ping server so can detected dropped connection? But Apache will quickly closes idle connections.
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performMonitorIteration = "performMonitorIteration";
    /**
     * Performs the actual processing of a single monitoring iteration. Typically Called
     * from within its own thread. 
     */
    public void performMonitorIteration()
    {
        int                     l_retryLimit = i_retryLimit;
        boolean                 l_success = false;
        AwdException            l_awdE;
        Exception               l_lastException = null;
        
        do
        {
            try
            {
                l_retryLimit--;
                tryPing();
                l_success = true;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                        "SUCCEEDED: HTTP ping to service " + i_monitoredServiceName +
                        " [server=" + i_httpPingServer +
                        ", port=" + i_httpPingPort +
                        ", requestLine=" + i_httpRequestLine +
                        "]");
            }
            catch(IOException l_e)
            {
                l_lastException = l_e;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE | PpasDebug.C_ST_ERROR, C_CLASS_NAME, 12120, this,
                        "FAILED: HTTP ping to service " + i_monitoredServiceName +
                        " [server=" + i_httpPingServer +
                        ", port=" + i_httpPingPort +
                        ", requestLine=" + i_httpRequestLine +
                        "], reriesLeft=" + l_retryLimit +
                        ",exception=" + l_e);
                if(l_retryLimit > 0)
                {
                    // Retry will be attempted, pause configured interval between retries (if any)
                    if(i_retryMillis > 0)
                    {
                        if(PpasDebug.on) PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE, C_CLASS_NAME, 12130, this,
                                "Sleeping until next retry (" + i_retryMillis + " ms)");
                        synchronized(this)
                        {
                            try
                            {
                                wait(i_retryMillis);
                            }
                            catch(InterruptedException l_ie)
                            {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }
        while(!l_success && (l_retryLimit > 0) && !isStopping());

        if(!isStopping())
        {
            if(l_success)
            {
                passed(true);
            }
            else
            {
                l_awdE = new WatchdogMonitorException(
                        C_CLASS_NAME, C_METHOD_performMonitorIteration, 11150,
                        this, (PpasRequest)null, (long)0, AwdKey.get().monitorFailedForIpService(
                                i_monitoredServiceName, i_httpPingServer,
                                i_httpPingPort, i_retryLimit), l_lastException);
                i_logger.logMessage(l_awdE);
                passed(false);
            }
        }
            
    } // End of public method performMonitorIteration

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_tryPing = "tryPing";
    /**
     * Attempts a single actual ping. 
     * @throws IOException      IO exception occurred attempting Ping. Ping unsuccessful.
     *                          Not logged or output - caller must do this it required.
     */
    private void tryPing()
    throws IOException
    {
        Socket                  l_socket = null;
        InputStream             l_is;
        OutputStream            l_os;
        String                  l_pingRequest;
        byte                    l_pingRequestByteARR[];
        byte                    l_pingResponseByteARR[];
        String                  l_pingResponse;
        int                     l_totalReadCount;
        int                     l_readCount;
        boolean                 l_eos;
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32100, this,
                "Entered " + C_METHOD_tryPing + ", will attempt ping to " +
                i_httpPingServer + " on port " + i_httpPingPort + " with timeout " + i_connectTimeoutMillis);
        try
        {
            // TODO 3 Use traceable socket (traceable socket would need updating)
            l_socket = new Socket();
            // Connect with timeout
            l_socket.connect(
                    new InetSocketAddress(i_httpPingServer, i_httpPingPort),
                    i_connectTimeoutMillis);
            l_socket.setSoTimeout(i_readTimeoutMillis);
            l_is = l_socket.getInputStream();
            l_os = l_socket.getOutputStream();
        
            l_pingRequest = i_httpRequestLine + "\r\n" +
                            "Host: " + i_httpPingServer + ":" + i_httpPingPort + "\r\n" +
                            "Connection: close\r\n" +
                            "\r\n";
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32120, this,
                    "Ping request=[" + l_pingRequest + "]");
            l_pingRequestByteARR = l_pingRequest.getBytes();
            l_os.write(l_pingRequestByteARR);
            
            l_pingResponseByteARR = new byte[C_BUFFER_SIZE];
            l_totalReadCount = 0;
            l_eos = false;
            // TODO 3 Currently read timeout only works on each individual read, so for example could prevent a slow response from timing out
            while(!l_eos && l_totalReadCount < C_BUFFER_SIZE)
            {
                l_readCount = l_is.read(
                        l_pingResponseByteARR,
                        l_totalReadCount,
                        C_BUFFER_SIZE - l_totalReadCount
                        );
                if(l_readCount > 0)
                {
                    l_totalReadCount += l_readCount;
                }
                else if(l_readCount < 0)
                {
                    l_eos = true;
                }
            }
            
            // We've either reached EOS or filled buffer. Either way
            // check Ping response.
    
            l_socket.close();
            l_socket = null;
            l_is = null;
            l_os = null;
    
            l_pingResponse = new String(
                    l_pingResponseByteARR, 0, l_totalReadCount);
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_TRACE, C_CLASS_NAME, 32160, this,
                    "Ping response=[" + l_pingResponse + "]");
            if(!i_responsePattern.matcher(l_pingResponse).matches())
            {
                // This is caught above and handled like anyother IO exception
                throw new IOException("Unmatching ping, got [" +
                        l_pingResponse + "], which doesn't match regex [" +
                        i_pingResponseRegex + "]");
            }
        }
        finally
        {
            // Tidy up
            if(l_socket != null)
            {
                try
                {
                    l_socket.close();
                }
                catch (IOException l_e2)
                {
                    l_e2.printStackTrace();
                    // We tried - ignore and continue
                }
                l_socket = null;
                l_is = null;
                l_os = null;
            }
        }
                   
    } // End of private method tryPing

} // End of public class HttpPingMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
