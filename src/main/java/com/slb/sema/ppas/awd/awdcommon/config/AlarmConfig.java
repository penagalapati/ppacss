/*
 * Created on 17-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import java.util.HashMap;
import java.util.Map;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmConfig
{

    /**
     * Map of alarm instance config name to
     * <code>AlarmInstanceConfig</code>.
     */    
    private Map                 i_alarmConfigMap;

    /**
     * 
     */
    public AlarmConfig()
    {
        super();
        
        i_alarmConfigMap = new HashMap();
    }
    
    public void addAlarmInstanceConfig(
        AlarmInstanceConfig     p_alarmConfigInstance
    )
    {
        i_alarmConfigMap.put(
                p_alarmConfigInstance.getAlarmName(),
                p_alarmConfigInstance);
    }

    public AlarmInstanceConfig getAlarmInstanceConfig(
        String                  p_alarmConfigInstanceName
    )
    {
        return(
                (AlarmInstanceConfig)i_alarmConfigMap.get(
                        p_alarmConfigInstanceName));
    }

    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(100);
        
        l_sb.append("AlarmConfig=[map=\n[\n");
        l_sb.append(i_alarmConfigMap);
        l_sb.append("]");
        
        return l_sb.toString();
    }
}
