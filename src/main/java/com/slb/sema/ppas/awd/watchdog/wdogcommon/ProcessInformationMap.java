/*
 * Created on 12-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessInformationMap
{
    /** Emptry array used in calls to Collection.toArray */
    private static ProcessInformation C_EMPTY_PROCESS_INFO_ARRAY[] =
            new ProcessInformation[0];
    
    /**
     * Map of process names (<code>String</code>) to process information
     * (<code>ProcessInformation</code>).
     */
    private Map                 i_processInformationMap;

    /**
     * 
     */
    public ProcessInformationMap()
    {
        super();
        
        i_processInformationMap = new HashMap();
    }
    
    public void putProcessInformation(
        String                  p_processName,
        ProcessInformation      p_processInformation
    )
    {
        i_processInformationMap.put(p_processName, p_processInformation);
    }

    public ProcessInformation getProcessInformation(
        String                  p_processName
    )
    {
        return((ProcessInformation)i_processInformationMap.get(p_processName));
    }
    
    public String toString()
    {
        StringBuffer            l_sb;
        ProcessInformation      l_processInfoARR[];
        int                     l_loop;
        
        l_sb = new StringBuffer(8196);
        
        l_processInfoARR = (ProcessInformation[])i_processInformationMap.values().toArray(
                C_EMPTY_PROCESS_INFO_ARRAY);
        l_sb.append("ProcessInformationMap=[\n");
        for(l_loop = 0; l_loop < l_processInfoARR.length; l_loop++)
        {
            if(l_loop > 0)
            {
                l_sb.append(",\n");
            }
            l_sb.append(l_processInfoARR[l_loop].toString());
        }
        l_sb.append("\n]");
        
        return l_sb.toString();
    }

}
