/*
 * Created on 12-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class DefaultTextAlarmFormatter
extends AlarmFormatter
{
    
    /**
     * Creates a new <code>DefaultTextAlarmFormatter</code>.
     * @param p_properties      properties for formatter.
     */
    public DefaultTextAlarmFormatter(UtilProperties p_properties)
    {
        super(p_properties);
    }

    public String format(
        Alarm                   p_alarm,
        AlarmFormat             p_alarmFormat
    )
    {
        return p_alarm.toString();
    }

}
