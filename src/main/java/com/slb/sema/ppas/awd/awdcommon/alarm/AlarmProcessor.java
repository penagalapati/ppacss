////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmProcessor.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       An Alarm Processor delivers a single Alarm to
//                              multiple destinations (and/or performs multiple
//                              alarm actions) based on an Alarm Group. 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An Alarm Processor delivers a single Alarm to multiple destinations based
 * on an Alarm Group.
 */
public class AlarmProcessor
{

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /**
     * The Alarm Delivery Manager used to deliver alarms.
     */
    private AlarmDeliveryManager i_alarmDeliveryMgr;
    
    /**
     * Creates a new Alarm Processor.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_instrumentManager instrument Manager to register any
     *                          instruments with.
     * @param  p_awdContext     context for this component.
     */
    public AlarmProcessor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
        )
    {
        super();

        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
    }
    
    /**
     * Initialises the Alarm Processor based on supplied configuration.
     * Currently no configuration is used by this class, but the configuration
     * is passed on to certain intantiated objects for them to configure
     * themselves.
     * 
     * @param  p_configProperties configuration properties.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    {
        i_alarmDeliveryMgr = new AlarmDeliveryManager(
                i_logger,
                i_instrumentManager,
                i_awdContext
                );
        i_alarmDeliveryMgr.init(p_configProperties);
    }
     
    /**
     * Processes an Alarm according to an Alarm Group. The Alarm Group
     * defines the destinations, formats and delivery mechanisms of where
     * and how to send the Alarm.
     * <p/>
     * For each entry in the alarm group, the alarm is formatted according
     * to its Alarm Format, and then each action in the entry is performed for
     * each desintation in the entry.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_alarmGroup     alarm Group of where and how to send Alarm.
     */
    public void processAlarm(
        Alarm                   p_alarm,
        AlarmGroup              p_alarmGroup
    )
    {
        AlarmGroup.Entry        l_entryARR[];
        
        l_entryARR = p_alarmGroup.getEntryArray();
        
        processAlarm(p_alarm, l_entryARR);
    }

    /**
     * Processes an Alarm according to the array of Alarm Group entries.
     * For each Alarm Group entry, the alarm is formatted according
     * to its Alarm Format, and then each action in the entry is performed for
     * each desintation in the entry.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_entry          array of Alarm Group entries.          
     */    
    private void processAlarm(
        Alarm                   p_alarm,
        AlarmGroup.Entry        p_entry[]
    )
    {
        int                     l_entryLoop;
        AlarmDestination        l_alarmDestinationARR[];
        AlarmAction             l_alarmActionARR[];
        
        for(    l_entryLoop = 0;
                l_entryLoop < p_entry.length;
                l_entryLoop++
        )
        {
            l_alarmDestinationARR = p_entry[l_entryLoop].
                    i_alarmDesintationGroup.getDestinationArray();
            l_alarmActionARR = p_entry[l_entryLoop].
                    i_alarmActionGroup.getActionArray();
            performActions(
                    p_alarm,
                    l_alarmDestinationARR,
                    l_alarmActionARR,
                    p_entry[l_entryLoop].i_alarmFormat);
        } // End for all desintations
        
    } // End of public method processAlarm

    /**
     * Performs actions to process the Alarm. The alarm is formatted according
     * to the Alarm Format, and then each action is performed for each
     * desintation.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_destinationARR sestinations to send alarm to.
     * @param  p_actionARR      actions to perform.
     * @param  p_alarmFormat    format of sent alarm.
     */
    private void performActions(
        Alarm                   p_alarm,
        AlarmDestination        p_destinationARR[],
        AlarmAction             p_actionARR[],
        AlarmFormat             p_alarmFormat
    )
    {
        int                     l_destinationLoop;

        for(    l_destinationLoop = 0;
                l_destinationLoop < p_destinationARR.length;
                l_destinationLoop++
        )
        {
            performActions(
                    p_alarm,
                    p_destinationARR[l_destinationLoop],
                    p_actionARR,
                    p_alarmFormat
                    );
        } // End for all actions
    }

    /**
     * Performs actions for a single destination to process the Alarm.
     * The alarm is formatted according to the
     * Alarm Format, and then each action is performed for the desintation.
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_destination    destination to send alarm to.
     * @param  p_actionARR      actions to send alarm.
     * @param  p_alarmFormat    format of sent alarm.
     */
    private void performActions(
        Alarm                   p_alarm,
        AlarmDestination        p_destination,
        AlarmAction             p_actionARR[],
        AlarmFormat             p_alarmFormat
    )
    {
        int                     l_actionLoop;

        for(    l_actionLoop = 0;
                l_actionLoop < p_actionARR.length;
                l_actionLoop++
        )
        {
            performAction(
                    p_alarm,
                    p_destination,
                    p_actionARR[l_actionLoop],
                    p_alarmFormat
                    );
        } // End for all actions
    }

    /**
     * Performs a single action for a single destination to process the Alarm.
     * The alarm is formatted according to the
     * Alarm Format, and then the action is performed for the desintation.
     * <p/>
     * Uses an Alarm Delivery Manager to perform the action. 
     *  
     * @param  p_alarm          alarm to send.
     * @param  p_destination    destination to send alarm to.
     * @param  p_action         actions to send alarm.
     * @param  p_alarmFormat    format of sent alarm.
     */
    private void performAction(
        Alarm                   p_alarm,
        AlarmDestination        p_destination,
        AlarmAction             p_action,
        AlarmFormat             p_alarmFormat
    )
    {
        i_alarmDeliveryMgr.deliverAlarm(
                p_alarm, p_destination, p_action, p_alarmFormat);
    }

} // End of public class AlarmProcessor

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////