////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdContext.java
//      DATE            :       16-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Simple context for AWD subsystem.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+------------------
//24/01/06  | Nick Whalan| Changed the property object     | PpacLon#1864/7793
//          |(h79176d)   | passed to Debug                 |
//----------+------------+---------------------------------+--------------------
// 15/03/07 | R.Grimshaw | Add release version to RMI reg- | PpacLon#2974
//          |            | istration name so that clients  |
//          |            | only connect to correct servers |
//          |            | during ASCS upgrades.           |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon;

import java.util.Locale;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentedRunTime;
import com.slb.sema.ppas.util.instrumentation.RemoteInstrumentManager;
import com.slb.sema.ppas.util.instrumentation.RemoteInstrumentationException;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.rmi.RmiRegistrationsManager;

/**
 * Simple context for AWD subsystem.
 */
public class AwdContext
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AwdContext";
    
    /**
     * Configuration property name for the environment name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ENVIRONMENT_NAME =
                                        "com.slb.sema.ppas.environmentName";
    /**
     * Default for the environment name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ENVIRONMENT_NAME_DEFAULT =
                                        "ascs";
    
    /**
     * Configuration property name for the process name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_PROCESS_NAME =
                                        "ascs.procName";
    /**
     * Default for the process name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_PROCESS_NAME_DEFAULT =
                                        "UnknownProcName";
                                        
    /**
     * Configuration property name for the node name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_NODE_NAME =
                                        "ascs.nodeName";
    /**
     * Default for the node name.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_NODE_NAME_DEFAULT =
                                        "UnknownNodeName";
    
    /** Key for ASCS release version property. */
    private static final String C_CFG_PARAM_RELEASE_VERSION = "ascs.release.version";

    /** Key for ASCS release revision property. */
    private static final String C_CFG_PARAM_RELEASE_REVISION = "ascs.release.revision";

    /** Key for ASCS release patch property. */
    private static final String C_CFG_PARAM_RELEASE_PATCH = "ascs.release.patch";

    /** Process name this context is running in. */
    private String              i_processName;

    /** Node name this context is running on. */
    private String              i_nodeName;

    /** Logger pool which should be used by components in this context. */
    protected PpasLoggerPool    i_loggerPool;

    /** Logger messages should be logged to by components in this context. */
    protected Logger            i_logger;

    /** Instrument Manager for components within this context to register Instrumented Objects with. */
    protected InstrumentManager i_instrumentManager;

    /** Rmi Registrations Manager for components within this context to register any remote objects with. */
    protected RmiRegistrationsManager i_rmiRegistrationsManager;
    
    /** Remote Instrument Manager for the instrument manager for this context.*/
    protected RemoteInstrumentManager i_remoteInstrumentManager;
    
    /**
     * Creates a new AwdAbstractContext.
     */
    public AwdContext()
    {
        super();

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_INIT, PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        i_instrumentManager = new InstrumentManager();
        i_processName = System.getProperty("ascs.procName", "unknownProcName");
        i_nodeName = System.getProperty("ascs.nodeName", "unknownNodeName");

        if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_INIT, PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,10090, this,
                "Constructed " + C_CLASS_NAME);

        return;

    }

    /**
     * Performs any tidy up required (and allows debugging trace of objects
     * lifecycle).
     * @throws Throwable If any error occurs.
     */
    public void finalize()
    throws Throwable
    {

        try
        {
            if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_HIGH,
                    PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                    C_CLASS_NAME, 10020, this,
                    "Finalizing " + C_CLASS_NAME);

            if (PpasDebug.on) PpasDebug.print(PpasDebug.C_LVL_HIGH,
                    PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME, 10025, this,
                    "Finalized " + C_CLASS_NAME);
        }
        finally
        {
            super.finalize();
        }

    }
    
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialises the context based on the supplied properties.
     * 
     * @param p_configProperties Properties to read configuration from.
     * @throws PpasException If initialisation fails.
     */    
    public void init(PpasProperties p_configProperties)
    throws PpasException
    {
        String                  l_environmentName;
        String                  l_processName;
        String                  l_nodeName;
        
        DatePatch.init(C_CLASS_NAME,
                       C_METHOD_init,
                       10031,
                       this,
                       null,
                       0,
                       null, // don't have a logger yet at this point
                       p_configProperties);

        PpasDebug.setDebug(PpasDebug.C_FLAG_SYSTEM_OVERRIDES, p_configProperties);

        //
        // Create the application context wide logger pool.
        //
        try
        {
            i_loggerPool = new PpasLoggerPool("LoggerPool", p_configProperties,
                    i_processName);
        }
        catch (PpasConfigException l_e)
        {
            // Can't log (logging not initialised yet), so write to System.err.
            System.err.println("FATAL ERROR STARTING APPLICATION : ");
            l_e.printStackTrace(System.err);
            throw(l_e);
        }

        i_logger = i_loggerPool.getLogger(); // Gets default logger (process name);

        if (i_logger == null)
        {
            // The requested logger could not be found ... create and throw
            // appropriate exception

            // Create and throw a 'missing configuration' exception
            PpasConfigException l_configE = new PpasConfigException(
                    C_CLASS_NAME,
                    C_METHOD_init,
                    10050,
                    this,
                    (PpasRequest)null,
                    0,
                    ConfigKey.get().badLogger(i_processName, "LoggerPool"));

            // Can't log (logging not initialised yet), so write to System.err.
            System.err.println ("FATAL ERROR STARTING APPLICATION : ");
            l_configE.printStackTrace(System.err);
            throw (l_configE);
        }

        i_logger.setInstrumentManager(i_instrumentManager);

        i_instrumentManager.registerObject(new InstrumentedRunTime(
                "Instrumented Runtime"));

        l_environmentName = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ENVIRONMENT_NAME, C_CFG_PARAM_ENVIRONMENT_NAME_DEFAULT);
        // TODO: <<Change to get from confiProps once PpasProps unpdated to default to System props????>>
        l_processName = System.getProperty(
                C_CFG_PARAM_PROCESS_NAME, C_CFG_PARAM_PROCESS_NAME_DEFAULT);
        l_nodeName = System.getProperty(
                C_CFG_PARAM_NODE_NAME, C_CFG_PARAM_NODE_NAME_DEFAULT);
        
        String l_version  = p_configProperties.getProperty(C_CFG_PARAM_RELEASE_VERSION, "");
        String l_revision = p_configProperties.getProperty(C_CFG_PARAM_RELEASE_REVISION, "");
        String l_patch    = p_configProperties.getProperty(C_CFG_PARAM_RELEASE_PATCH, "");
        
        StringBuffer l_sb = new StringBuffer(l_version);
        if (!l_revision.equals(""))
        {
            l_sb.append("rev" + l_revision);
        }
        
        if (!l_patch.equals(""))
        {
            l_sb.append("patch" + l_patch);
        }
        
        String l_release = l_sb.toString();
        
        i_rmiRegistrationsManager = new RmiRegistrationsManager(
                i_logger, p_configProperties);
        
        i_rmiRegistrationsManager.start();

        try
        {
            i_remoteInstrumentManager = new RemoteInstrumentManager(
                    (Logger)null, p_configProperties, i_instrumentManager, i_rmiRegistrationsManager,
                    (String)null, "/" + l_environmentName + "/" + l_nodeName + "/" +
                    l_processName + "/" + l_release + "/" + 
                    this.getClass().getName() + "/remoteInstrumentManagerServer");
        }
        catch(RemoteInstrumentationException l_e)
        {
            logMessage(new LoggableEvent(l_e.toString(), LoggableInterface.C_SEVERITY_ERROR));
        }
        
    }
    
    /**
     * Returns the logger for this context.
     * @return The logger for this context.
     */
    public Logger getLogger()
    {
        return i_logger;
    }

    /**
     * Returns the process name this context is running in.
     * @return The process name this context is running in.
     */
    public String getProcessName()
    {
        return i_processName;
    }

    /**
     * Returns the node name this context is running on.
     * @return The node name this context is running on.
     */
    public String getNodeName()
    {
        return i_nodeName;
    }

    /**
     * Returns the Rmi Registrations Manager for compoinents within this context
     * to register any remote objects with.
     * @return The Rmi Registrations Manager for this context.
     */
    public RmiRegistrationsManager getRmiRegistrationsManager()
    {
        return i_rmiRegistrationsManager;
    }
    
    /**
     * Returns the Instrument Manager for components within this context to
     * register Instrumented Objects with.
     * @return The Instrument Manager for this context.
     */
    public InstrumentManager getInstrumentManager()
    {
        return i_instrumentManager;
    }

    /**
     * Performs the necessary tidy up for the context.
     */    
    public void destroy()
    {
        i_rmiRegistrationsManager.stop();
    }
 
    /**
     * Utility method to log message.
     * @param p_loggable        Loggable to be logged. 
     */   
    protected void logMessage(LoggableInterface p_loggable)
    {
        if(i_logger == null)
        {
            System.err.println(p_loggable.getMessageText(Locale.getDefault()));
        }
        else
        {
            i_logger.logMessage(p_loggable);
        }
    }

} // End of public class AwdContext

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
