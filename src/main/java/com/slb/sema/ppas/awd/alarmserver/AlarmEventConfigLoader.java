////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmEventConfigLoader.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Loader which supports the loading of Alarm Event
//                              configuration.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/08/07 | E Dangoor  | Allow config to be found either | PpacLon#3319/12021
//          |            | in the local or the system area |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.alarmserver;

import java.io.File;
import java.util.regex.Pattern;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.ConfigTokenizer;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Loader which supports the loading of Alarm Event configuration.
 * It can instantiate and load an
 * {@link com.slb.sema.ppas.awd.alarmserver.AlarmEventConfig} object from
 * the persisted configuation.
 */
public class AlarmEventConfigLoader extends AwdAbstractConfigLoader
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmEventConfigLoader";
    
    /**
     * Creates a new <code>AlarmEventConfigLoader</code>. 
     * @param p_logger          logger to use.
     * @param p_configProperties configuration properties to use to load config.
     * @param p_awdContext      context.
     */
    public AlarmEventConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        AwdContext              p_awdContext
        )
    {
        super(p_logger, p_configProperties, p_awdContext);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Loads the alarm event config into and a new <code>AlarmEventConfig</code>
     * and returns it.
     * @param p_configFilename  path and filename of config to load.
     * @param p_alarmConfig     alarm configuration that loaded config can
     *                          reference.
     * @param p_alarmGroupConfig alarm group config that loaded config can
     *                          reference.
     * @return                  the loaded alarm event config.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    public AlarmEventConfig loadAlarmEventConfig(
        String                  p_configFilename,
        AlarmConfig             p_alarmConfig,
        AlarmGroupConfig        p_alarmGroupConfig
    )
    throws AwdConfigException
    {
        AlarmEventConfig        l_config;
        
        l_config = new AlarmEventConfig(p_alarmConfig, p_alarmGroupConfig);
        
        //loadTestConfig(l_config);
        loadLayeredConfig(p_configFilename, l_config);
        
        return l_config;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    /**
     * Loads the alarm event config into and the <code>AlarmEventConfig</code>.
     * @param p_configFilename  filename of config to load.
     * @param p_alarmEventConfig alarm event config to load into.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    private void loadLayeredConfig(
        String                  p_configFilename,
        AlarmEventConfig        p_alarmEventConfig
        )
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_alarmEventConfig);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_alarmEventConfig);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_loadConfig = "loadConfig";
    /**
     * Loads the alarm event config into and the <code>AlarmEventConfig</code>.
     * @param p_configFilename  path and filename of config to load.
     * @param p_alarmEventConfig alarm event config to load into.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    private void loadConfig(
        String                  p_configFilename,
        AlarmEventConfig        p_alarmEventConfig
        )
    throws AwdConfigException
    {

        ConfigTokenizer         l_tokenizer;
        String                  l_token;
        boolean                 l_eos = false;
        String                  l_eventName;
        String                  l_alarmName;
        String                  l_alarmGroupName;
        boolean                 l_regex = false;
        String                  l_eventRegex = null;
        String                  l_eventRegexField = null;
        Pattern                 l_regexPattern = null;
        Integer                 l_eventRegexFieldIdI = null;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        while(!l_eos)
        {
            l_token = l_tokenizer.getExpectedNextToken(
                    new String[] {null, "event.alarm.mapping"});
            l_regex = false;    /* l_regex reset for each loop */
            
            if(l_token != null)
            {
                // Read next alarm instance configuration.
                l_tokenizer.getExpectedNextToken("{");
                l_eventName = l_tokenizer.getOptionalNextParameter("eventName");
                if(l_eventName == null)
                {
                    l_regex = true;
                    l_eventRegex = l_tokenizer.getExpectedNextParameter(
                            "eventRegex");
                    try
                    {
                        l_regexPattern = Pattern.compile(l_eventRegex);
                    }
                    catch(RuntimeException l_e)
                    {
                        handlePreviousConfigErrorException(
                                C_CLASS_NAME,
                                C_METHOD_loadConfig,
                                11000,
                                l_tokenizer.getLineNumber(),
                                p_configFilename,
                                l_e);
                    }
                    l_eventRegexField = l_tokenizer.getExpectedNextParameter(
                            "eventRegexField");
                    l_eventRegexFieldIdI = (Integer)AlarmEventConfig.
                            C_EVENT_FIELD_NAME_TO_ID_MAP.get(l_eventRegexField);
                    if(l_eventRegexFieldIdI == null)
                    {
                        handleUnexpectedToken(
                                C_CLASS_NAME,
                                C_METHOD_loadConfig,
                                11010,
                                l_eventRegexField,
                                l_tokenizer.getLineNumber(),
                                p_configFilename,
                                AlarmEventConfig.C_EVENT_FIELD_VALID_NAMES);
                    }
                }

                l_alarmName = l_tokenizer.getExpectedNextParameter("alarmName");
                if (l_alarmName.equals("NONE"))
                {
                    if(!l_regex)
                    {
                        p_alarmEventConfig.removeEventNameMapping(l_eventName);
                    }
                    else
                    {
                        p_alarmEventConfig.removeEventRegexMapping(
                                 l_eventRegexFieldIdI.intValue(),
                                 l_regexPattern);                    
                    }
                    
                    // Allow the alarmGroup parameter to be present but ignore it
                    l_tokenizer.getOptionalNextParameter("alarmGroup");
                }
                else
                {
                    l_alarmGroupName = l_tokenizer.getExpectedNextParameter("alarmGroup");
                
                    if(!l_regex)
                    {
                        p_alarmEventConfig.addEventNameMapping(
                                 l_eventName,
                                 l_alarmName,
                                 l_alarmGroupName);
                    }
                    else
                    {
                        p_alarmEventConfig.addEventRegexMapping(
                                 l_eventRegexFieldIdI.intValue(),
                                 l_regexPattern,
                                 l_alarmName,
                                 l_alarmGroupName);                    
                    }
                }

                l_tokenizer.getExpectedNextToken("}");
            }
            else
            {
                l_eos = true;
            }
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                "Leaving " + C_METHOD_loadConfig);
                
        return;
    }

} // End of public class AlarmEventConfigLoader

////////////////////////////////////////////////////////////////////////////////
//End of file
////////////////////////////////////////////////////////////////////////////////