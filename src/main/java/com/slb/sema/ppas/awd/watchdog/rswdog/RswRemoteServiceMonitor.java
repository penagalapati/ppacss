////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RswRemoteServiceMonitor.java
//      DATE            :       09-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Remote Service Monitor which monitors
//                              a single service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;


import java.lang.reflect.Constructor;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.MonitorConfig;
import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.ServiceConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.GroupOrWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorListener;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Remote Service Monitor which monitors
 * a single service.
 */
public class RswRemoteServiceMonitor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RswRemoteServiceMonitor";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdog.rswdog.RswRemoteServiceMonitor.";

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
   
    /** Configuration properties. */
    private PpasProperties      i_configProperties;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Monitored service's name. */
    private String              i_serviceName;
        
    /**
     * The monitor for this "service" - not this could be a monitor
     * which groups multiple monitors togetger.
     */
    private GroupOrWatchdogMonitor i_monitor;

    /**
     * The release service watchdog action group name for the service (actions
     * to release the service on the local node).
     */
    private String              i_releaseServiceWatchdogActionGroupName; 
    

    /**
     * Creates a new RSW Remote Service Monitor.
     * 
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager instrument manager.
     * @param p_awdContext      AWD context.
     * @param p_serviceName     name of service.
     * @param p_monitorListener monitor listener.
     */
    public RswRemoteServiceMonitor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext,
        String                  p_serviceName,
        WatchdogMonitorListener p_monitorListener
        )
    {
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
        i_serviceName = p_serviceName;
        i_monitor = new GroupOrWatchdogMonitor();
        i_monitor.addMonitorListener(p_monitorListener);
    }
    
    
    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_init = "init";
    
    /**
     * Initialize monitor based on config.
     * 
     * @param p_serviceConfig   service config.
     * @param p_configProperties config properties.
     * @throws AwdException     exception occurred while trying to initialize.
     */
    public void init(
        ServiceConfig           p_serviceConfig,
        PpasProperties          p_configProperties
        )
    throws AwdException
    {
        MonitorConfig           l_monitorConfigARR[];
        int                     l_loop;
        MonitorConfig           l_monitorConfig;
        String                  l_className;
        Class                   l_class;
        AwdException            l_awdE;
        WatchdogMonitor         l_monitor;
        Constructor             l_constructor;

        i_configProperties = p_configProperties;
        
        i_releaseServiceWatchdogActionGroupName = p_serviceConfig.
                getReleaseServiceWatchdogActionGroupName();
               
        l_monitorConfigARR = p_serviceConfig.getMonitorConfigArray();
        for(l_loop = 0; l_loop < l_monitorConfigARR.length; l_loop++)
        {
            l_monitorConfig = l_monitorConfigARR[l_loop];
            l_className = l_monitorConfig.getMonitorClass();
            try
            {
                l_class = Class.forName(l_className);
                l_constructor = l_class.getConstructor(new Class[] { Logger.class, String.class });
                l_monitor = (WatchdogMonitor)l_constructor.newInstance(
                        new Object[] {i_logger, p_serviceConfig.getServiceName()});
                l_monitor.init(l_monitorConfigARR[l_loop].getMonitorParamMap());
                i_monitor.addMonitor(l_monitor);                
            }
            catch(Exception l_e)
            {
                l_awdE =  new AwdConfigException(
                        C_CLASS_NAME,
                        C_METHOD_init,
                        10100,
                        this,
                        (PpasRequest)null,
                        (long)0,
                        AwdKey.get().errorInitWatchdogMonitor(
                                p_serviceConfig.getServiceName(),
                                l_className),
                        l_e
                        );
                i_logger.logMessage(l_awdE);
                throw l_awdE;
            }
        }

    }

    /**
     * Adds the supplied monitor listener.
     * 
     * @param p_monitorListener monitor listener to add.
     */
    public void addMonitorListener(WatchdogMonitorListener p_monitorListener)
    {
        i_monitor.addMonitorListener(p_monitorListener);
    }

    /**
     * Removes the supplied monitor listener (if its present).
     * 
     * @param p_monitorListener monitor listener to remove.
     * @return <code>true</code> if was present (and so removed),
     *         <code>false</code> otherwise.
     */
    public boolean removeMonitorListener(WatchdogMonitorListener p_monitorListener)
    {
        return i_monitor.removeMonitorListener(p_monitorListener);
    }

    /** Starts the monitor. */
    public void start()
    {
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11100, this,
                "Starting");

        i_monitor.start();
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11190, this,
                "Ststed");

    }

    /** Stops the monitor. */
    public void stop()
    {
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11200, this,
                "Stopping");
        
        i_monitor.stop();
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11290, this,
                "Stopped");

    }

    /**
     * Returns the name of the Watchdog Action Group which defines actions to
     * perform when requested to release the service.
     * @return The name of the Watchdog Action Group.
     */
    public String getReleaseServiceWatchdogActionGroupName()
    {
        return i_releaseServiceWatchdogActionGroupName;
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return C_CLASS_NAME + "=[monitor=" + i_monitor + "]";
    }
    
} // End of public class RswRemoteServiceMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////