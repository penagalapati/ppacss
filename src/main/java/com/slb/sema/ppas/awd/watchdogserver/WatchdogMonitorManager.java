////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogMonitorManager.java
//      DATE            :       13-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#2644
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Watchdog Monitor Manager which manages
//                              configured watchdog monitors.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdogserver;

import java.io.File;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.alarm.ConfiguredAlarm;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.OsCommandWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.RaiseAlarmWatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogAction;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogContext;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorConfigLoader;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorEntries;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorListener;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogPlatform;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Watchdog Monitor Manager which manages
 * configured watchdog monitors.
 */
public class WatchdogMonitorManager
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogMonitorManager";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdogserver.WatchdogMonitorManager.";

    /** Logger messages logged to. */
    private Logger              i_logger;

    /** Context running in. */
    private WatchdogContext     i_watchdogContext;
    
    /** Instrument Manager to register any instruments with. */
    private InstrumentManager   i_instrumentManager;

    /** Alarm config. */
    private AlarmConfig         i_alarmConfig;

    /** Alarm group config. */
    private AlarmGroupConfig    i_alarmGroupConfig;
 
    /** Alarm processor used to process (perform) any alarm actions. */
    private AlarmProcessor      i_alarmProcessor;
 
    /** Node (Server) name running on. */
    private String              i_nodeName;
   
    /** Process name running as. */
    private String              i_processName;
    
    /** Watchdog platform for handling platform dependant actions. */
    private WatchdogPlatform    i_watchdogPlatform;

    /** The configured watchdog monitors being managed. */
    private WatchdogMonitorEntries i_watchdogMonitorEntries;

    /**
     * Creates a new WatchdogManager.
     * @param p_logger          logger to log any messages to.
     * @param p_instrumentManager Instrument Manager to register any instruments with.
     * @param p_watchdogContext context running in.
     */
    public WatchdogMonitorManager(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        WatchdogContext         p_watchdogContext
        )
    {
        super();
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_watchdogContext = p_watchdogContext;
        i_alarmConfig = i_watchdogContext.getAlarmConfig();
        i_alarmGroupConfig = i_watchdogContext.getAlarmGroupConfig();
        i_alarmProcessor = i_watchdogContext.getAlarmProcessor();
        i_nodeName = i_watchdogContext.getNodeName();
        i_processName = i_watchdogContext.getProcessName();
        i_watchdogPlatform = i_watchdogContext.getWatchdogPlatform();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_END, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Initialises the Watchdog Monitor Manager according to supplied configuration.
     * @param p_configProperties configuration to initialise from.
     * @throws AwdConfigException Exception occurred initialising from supplied configuration.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    throws AwdConfigException
    {
        WatchdogMonitorConfigLoader l_watchdogMonitorConfigLoader;
        String                  l_configFilename;
        int                     l_loop;
        WatchdogMonitorEntries.WdogMonitorEntry l_watchdogMonitorEntryARR[];

        l_watchdogMonitorConfigLoader = new WatchdogMonitorConfigLoader(
                i_logger, p_configProperties, i_watchdogContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "watchdogMonitorConfigFilePathAndName");
        if(l_configFilename != null && (new File(l_configFilename)).exists())
        {
            // This watchdog has some monitors configured, load them.
            i_watchdogMonitorEntries =
                    l_watchdogMonitorConfigLoader.loadWatchdogMonitorEntries(
                    l_configFilename,
                    i_watchdogContext.getWatchdogActionGroupConfig(),
                    i_watchdogContext.getProcessName());
            
            l_watchdogMonitorEntryARR = i_watchdogMonitorEntries.getWatchdogMonitorEntryArray();
            for(l_loop = 0; l_loop < l_watchdogMonitorEntryARR.length; l_loop++)
            {
                l_watchdogMonitorEntryARR[l_loop].getWatchdogMonitor().
                        addMonitorListener(new MgrMonitorListener(
                                l_watchdogMonitorEntryARR[l_loop]));
            }
        }
        else
        {
            // This watchdog does not have any monitors configured, create empty entris object
            i_watchdogMonitorEntries = new WatchdogMonitorEntries(
                    i_logger,
                    i_watchdogContext.getWatchdogActionGroupConfig());

        }

    }

    /**
     * Starts the manager (starts all configured Watchdog Monitors).
     */
    public void start()
    {
        int                     l_loop;
        WatchdogMonitorEntries.WdogMonitorEntry l_watchdogMonitorEntryARR[];
        
        l_watchdogMonitorEntryARR = i_watchdogMonitorEntries.getWatchdogMonitorEntryArray();
        for(l_loop = 0; l_loop < l_watchdogMonitorEntryARR.length; l_loop++)
        {
            l_watchdogMonitorEntryARR[l_loop].getWatchdogMonitor().start();
        }
    }

    /**
     * Stops the manager (stops all configured Watchdog Monitors).
     */
    public void stop()
    {
        int                     l_loop;
        WatchdogMonitorEntries.WdogMonitorEntry l_watchdogMonitorEntryARR[];
        
        l_watchdogMonitorEntryARR = i_watchdogMonitorEntries.getWatchdogMonitorEntryArray();
        for(l_loop = 0; l_loop < l_watchdogMonitorEntryARR.length; l_loop++)
        {
            l_watchdogMonitorEntryARR[l_loop].getWatchdogMonitor().stop();
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performActions = "performActions";
    /**
     * Performs the actions of the supplied Watchdog Action Group.
     * @param p_watchdogActionGroup the watchdog action group.
     */
    private void performActions(
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {
        WatchdogAction          l_actionARR[];
        int                     l_loop;
        AwdException            l_awdE;
        
        l_actionARR = p_watchdogActionGroup.getActionArray();
        for(l_loop = 0; l_loop < l_actionARR.length; l_loop++)
        {
            if(Debug.on)
            {
                Debug.print(
                        Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 10000, this,
                        "performAction returned true, so action allowed "+
                        "(max retries within interval not exceeded)");
            }
            
            if(l_actionARR[l_loop] instanceof RaiseAlarmWatchdogAction)
            {
                performRaiseAlarmAction(
                        (RaiseAlarmWatchdogAction)l_actionARR[l_loop]);

            }
            else if(l_actionARR[l_loop] instanceof OsCommandWatchdogAction)
            {
                performOsCommandAction(
                        (OsCommandWatchdogAction)l_actionARR[l_loop]);

            }
            else
            {
                l_awdE = new AwdConfigException(C_CLASS_NAME, C_METHOD_performActions, 20150,
                        this, (PpasRequest)null, (long)0,
                        AwdKey.get().unexpectedAction(l_actionARR[l_loop].getClass().getName())
                        );
                i_logger.logMessage(l_awdE);
                // Logged, best we can do now is to continue.
            }
        }
        
    }

    /**
     * Performs a Raise Alarm Watchdog Action.
     * @param p_raiseAlarmWatchdogAction the Raise Alarm Watchdog Action to perform.
     */
    private void performRaiseAlarmAction(
        RaiseAlarmWatchdogAction p_raiseAlarmWatchdogAction
    )
    {
        ConfiguredAlarm         l_alarm;
        
        l_alarm = new ConfiguredAlarm(
                (String)null,
                (String)null,
                (Integer)null,
                i_nodeName,
                i_processName,
                (Map)null,
                i_alarmConfig.getAlarmInstanceConfig(
                        p_raiseAlarmWatchdogAction.
                        getAlarmInstanceName()));
        i_alarmProcessor.processAlarm(
                l_alarm,
                i_alarmGroupConfig.getAlarmGroup(
                        p_raiseAlarmWatchdogAction.getAlarmGroupName()));
    }
    
    /**
     * Performs a OS Command Action.
     * @param p_osCommandWatchdogAction the OS Command Action to perform.
     */
    private void performOsCommandAction(
        OsCommandWatchdogAction p_osCommandWatchdogAction
    )
    {
        i_watchdogPlatform.performOsCommand(
                p_osCommandWatchdogAction.getOsCommand());
    }
    
    // NOTE: Class needs to be public so A&I can access its public methods.
    /**
     * Public inner class which implements the listener for the Watchdog
     * Monitors being managed.
     */
    public class MgrMonitorListener
    implements WatchdogMonitorListener, InstrumentedObjectInterface
    {
     
        /** Watchdog Monitor Entry of monitor being listened to. */
        private WatchdogMonitorEntries.WdogMonitorEntry i_watchdogMonitorEntry;
        
        /** Instrument set for instruments of listener. */
        private InstrumentSet   i_instrumentSet;

        /** Total passed counter instrument. */
        private CounterInstrument   i_totalPassedCI;

        /** Total failed counter instrument. */
        private CounterInstrument   i_totalFailedCI;

        /** Total entered passed state counter instrument. */
        private CounterInstrument   i_enteredPassedStateCI;
        
        /** Total entered failed state counter instrument. */
        private CounterInstrument   i_enteredFailedStateCI;

        /**
         * Creates a new Manager Monitor Listener for the supplied Watchdog
         * Monitor Entry.
         * @param p_watchdogMonitorEntry entry to create listener for.
         */
        private MgrMonitorListener(
            WatchdogMonitorEntries.WdogMonitorEntry p_watchdogMonitorEntry
            )
        {
            i_watchdogMonitorEntry = p_watchdogMonitorEntry;
            i_instrumentSet = new InstrumentSet("root");
            i_totalPassedCI = new CounterInstrument(
                    "Total Passed Count",
                    "The total number of times a monitor has detected a pass.");
            i_instrumentSet.add(i_totalPassedCI);
            i_totalFailedCI = new CounterInstrument(
                    "Total Failed Count",
                    "The total number of times a monitor has detected a failure.");
            i_instrumentSet.add(i_totalFailedCI);
            i_enteredPassedStateCI = new CounterInstrument(
                    "Entered Passed State Count",
                    "The total number of times a monitor has entered the passed state.");
            i_instrumentSet.add(i_enteredPassedStateCI);
            i_enteredFailedStateCI = new CounterInstrument(
                    "Entered Failed State Count",
                    "The total number of times a monitor has entered the failed state.");
            i_instrumentSet.add(i_enteredFailedStateCI);
            i_instrumentManager.registerObject(this);
        }

        /**
         * Called every time the monitor detects a failure. i.e one or more
         * tests the monitor performs has failed.
         */
        public void failed()
        {
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_totalFailedCI.increment();
                }
            }
        }

        /**
         * Called every time the monitor detects a pass. i.e. each time
         * one of the tests the monitor performs have passed AND monitor is
         * in the passed state - so where one test of a monitor is passing
         * but another test of the monitor is failing, generally passed will not
         * be called (until monitor enters the passed state)).
         */
        public void passed()
        {
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_totalPassedCI.increment();
                }
            }
        }

        /**
         * Called when the monitor has entered the failed state. i.e. changed
         * from passed to failed state.
         */
        public void hasEnteredFailedState()
        {
            WatchdogActionGroup l_watchdogActionGroup;
            
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_enteredFailedStateCI.increment();
                }
            }
            l_watchdogActionGroup = i_watchdogMonitorEntry.
                    getEnteredFailedStateWatchdogActionGroup(); 
            if(l_watchdogActionGroup != null)
            {
                performActions(l_watchdogActionGroup);
            }
        }
        
        /**
         * Called when the monitor has entered the passed state. i.e. changed
         * from failed to passed state.
         */
        public void hasEnteredPassedState()
        {
            WatchdogActionGroup l_watchdogActionGroup;
            
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_enteredPassedStateCI.increment();
                }
            }
            l_watchdogActionGroup = i_watchdogMonitorEntry.
                    getEnteredPssedStateWatchdogActionGroup(); 
            if(l_watchdogActionGroup != null)
            {
                performActions(l_watchdogActionGroup);
            }
        }

        /**
         * Returns the unique name of this listener.
         * @return Returns the unique name of this listener.
         */
        public String getName()
        {
            return i_watchdogMonitorEntry.getName();
        }
        
        /**
         * Returns the Instrument Set.
         * @return Returns the Instrument Set.
         */
        public InstrumentSet getInstrumentSet()
        {
            return i_instrumentSet;
        }

        /**
         * Manager method to start monitoring (forces it to be enabled
         * first if required).
         * @return Returns result message.
         */
        public String mngMthStartMonitor()
        {
            WatchdogMonitor     l_monitor;
            
            l_monitor = i_watchdogMonitorEntry.getWatchdogMonitor(); 
            l_monitor.enable(true);
            l_monitor.start();
            return "Started monitor " + i_watchdogMonitorEntry.getName();
        }
        
        /**
         * Manager method to stop monitoring.
         * @return Returns result message.
         */
        public String mngMthStopMonitor()
        {
            i_watchdogMonitorEntry.getWatchdogMonitor().stop();
            return "Stopped monitor " + i_watchdogMonitorEntry.getName();
        }
        
        /**
         * Manager method to return the RUNNING or NOT_RUNNING state of the
         * monitor.
         * @return Returns "RUNNING" or "NOT_RUNNING".
         */
        public String mngMthGetStatus()
        {
            String              l_status = "NOT_RUNNING";
            
            if(i_watchdogMonitorEntry.getWatchdogMonitor().isRunning())
            {
                l_status = "RUNNING";
            }
            
            return l_status;
        }
        
        /**
         * Returns a String respresentation of this object suitable for
         * debugging/trace.
         * 
         * @return A respresentation suitable for debugging/trace.
         */
        public String toString()
        {
            return i_watchdogMonitorEntry.toString();
        }
    } // End of public inner class MgrMonitorListener

} // End of public class WatchdogMonitorManager

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
