/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LogAlarmAction extends AlarmAction
{

    /**
     * Creates a new Log Alarm Action.
     */
    public LogAlarmAction()
    {
        super(C_TYPE_LOG_FILE);
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return("LogAlarmAction=[" + super.toString() + "]");
    }

}
