////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdConfigException.Java
//      DATE            :       17-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Implements AWD configuration exceptions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon;

import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Implements AWD configuration exceptions.
 */
public class AwdConfigException extends AwdException
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Debug methods. */
    private static final String C_CLASS_NAME = "AwdConfigException";

    //------------------------------------------------------------------------
    // Public constructors.
    //------------------------------------------------------------------------

    /**
     * Construct an AwdConfigException.
     *
     * @param p_callingClass    The name of the class invoking this constructor.
     * @param p_callingMethod   The name of the method from which this constructor was invoked.
     * @param p_stmtNo          Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject   The object from which this constructor was 
     *                          invoked ... null if the constructor was invoked from a static method.
     * @param p_request         Request being processed when creating this exception or <ocde>null</code>.
     * @param p_flags           For expansion. Not currently used.
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     */
    public AwdConfigException(
        String                  p_callingClass,
        String                  p_callingMethod,
        int                     p_stmtNo,
        Object                  p_callingObject,
        PpasRequest             p_request,
        long                    p_flags,
        AwdKey.AwdExceptionKey  p_exceptionKey) 
    {   
        this(   p_callingClass,
                p_callingMethod,
                p_stmtNo,
                p_callingObject,
                p_request,
                p_flags,
                p_exceptionKey,
                (Exception)null);
    }

    /**
     * Construct an AwdConfigException with exception chaining.
     *
     * @param p_callingClass    The name of the class invoking this constructor.
     * @param p_callingMethod   The name of the method from which this constructor was invoked.
     * @param p_stmtNo          Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject   The object from which this constructor was 
     *                          invoked ... null if the constructor was invoked from a static method.
     * @param p_request         Request being processed when creating this exception or <code>null</code>.
     * @param p_flags           For expansion. Not currently used.
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     * @param p_sourceException An earlier exception which gave rise to this exception.
     */
    public AwdConfigException(
        String                  p_callingClass,
        String                  p_callingMethod,
        int                     p_stmtNo,
        Object                  p_callingObject,
        PpasRequest             p_request,
        long                    p_flags,
        AwdKey.AwdExceptionKey  p_exceptionKey,
        Exception               p_sourceException) 
    {   
        super(  p_callingClass,
                p_callingMethod,
                p_stmtNo,
                p_callingObject,
                p_request,
                p_flags,
                p_exceptionKey,
                p_sourceException);

        if ( Debug.on )
        {
            Debug.print(Debug.C_LVL_VLOW,
                    Debug.C_APP_MWARE,
                    Debug.C_ST_CONFIN_START | Debug.C_ST_TRACE,
                    C_CLASS_NAME, 10030, this,
                    "Constructing " + C_CLASS_NAME + " with key " + p_exceptionKey);
        }

        if ( Debug.on )
        {
            Debug.print(Debug.C_LVL_VLOW,
                    Debug.C_APP_MWARE,
                    Debug.C_ST_CONFIN_START | Debug.C_ST_TRACE,
                    C_CLASS_NAME, 10040, this,
                    "Constructed " + C_CLASS_NAME);
        }
    }
} // End of public class AwdConfigException

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////