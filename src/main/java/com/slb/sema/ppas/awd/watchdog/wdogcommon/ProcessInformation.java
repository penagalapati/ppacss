/*
 * Created on 06-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.HashMap;


/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProcessInformation
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "ProcessInformation";

    /** Emptry array used in calls to Collection.toArray */
    private static String C_EMPTY_STRING_ARRAY[] =
            new String[0];

    private String              i_processName;
    private HashMap             i_fieldMap;
    
    public ProcessInformation(
        String                  p_processName)
    {
        super();
        
        i_processName = p_processName;
        i_fieldMap = new HashMap();
    }

    public String getProcessName()
    {
        return i_processName;
    }

    public String getState()
    {
        return (String)getField(ProcessCondition.C_FIELD_STATE);
    }

    public int getStateEnum()
    {
        String                  l_state;
        int                     l_stateEnum;
        
        l_state = (String)getField(ProcessCondition.C_FIELD_STATE);
        
        l_stateEnum = ((Integer)ProcessCondition.C_STATE_TOENUM.get(
                l_state)).intValue();
        return l_stateEnum;
    }

    public Object getField(
        String                  p_fieldName
    )
    {
        return(i_fieldMap.get(p_fieldName));
    }

    public void setField(
        String                  p_fieldName,
        Object                  p_fieldValue
    )
    {
        i_fieldMap.put(p_fieldName, p_fieldValue);
    }
    
    public String toString()
    {
        StringBuffer            l_sb;
        int                     l_loop;
        String                  l_fieldNameARR[];
        
        l_sb = new StringBuffer(160);
        
        l_sb.append(C_CLASS_NAME + "=[processName=");
        l_sb.append(i_processName);
        l_fieldNameARR = (String[])i_fieldMap.keySet().toArray(C_EMPTY_STRING_ARRAY);
        l_sb.append(",fieldMap=[");
        for(l_loop = 0; l_loop < l_fieldNameARR.length; l_loop++)
        {
            if(l_loop > 0)
            {
                l_sb.append(",");
            }
            l_sb.append("name=");
            l_sb.append(l_fieldNameARR[l_loop]);
            l_sb.append(",value=");
            l_sb.append(i_fieldMap.get(l_fieldNameARR[l_loop]));
        }
        l_sb.append("]");
        
        return l_sb.toString();
    }

}
