////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AwdMsg.Java
//      DATE            :       17-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Defines the message resource bundle storing
//                              the message strings associated with 
//                              AWD application exceptions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 09/12/02 | E Clayton  | Added new key:                  | PpaLon#31/389
//          |            | C_KEY_OPERATION_FAILED_DUE_TO_  |
//          |            | MISSING_PROPERTY                |
//----------+------------+---------------------------------+--------------------
// 25/10/05 | M.Vonka    | Javadoc added / corrected.      | PpacLon#1779/7315
//----------+------------+---------------------------------+--------------------
// 16/02/06 | M.Vonka    | Added                           | PpacLon#1987/7954
//          |            | ERROR_INIT_WATCHDOG_MONITOR.    |
//----------+------------+---------------------------------+--------------------
// 11/09/06 | M.Vonka    | Added UNEXPECTED_INTERNAL_-     | PpaLon#2096/8999
//          |            | SERVER_ERROR.                   |
//----------+------------+---------------------------------+--------------------
// 20/10/06 | M.Vonka    | Added MONITOR_SCRIPT_OR_CMD_-   | PpacLon#2644/10214
//          |            | -FAILED.                        |
//----------+------------+---------------------------------+--------------------

//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon;

import java.io.IOException;

import com.slb.sema.ppas.common.exceptions.XmlExceptionBundle;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.util.support.Debug;

/**
 * This class implements the resource bundle containing the text messages
 * associated with config exceptions and contains the keys into that
 * resource bundle.
 */
public class AwdMsg extends XmlExceptionBundle
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    /**
     * Name of the message texts file containing the messages associated with
     * all config type exceptions.
     */
    public static final String C_CONFIG_MSG_BASENAME =
                              AwdMsg.class.getName();

    /** Unknown alarm group name {0}. */
    public static final String C_KEY_UNKNOWN_ALARM_GROUP_NAME =
            "UNKNOWN_ALARM_GROUP_NAME";

    /** Unknown alarm instance name {0}. */
    public static final String C_KEY_UNKNOWN_ALARM_INSTANCE_NAME =
            "UNKNOWN_ALARM_INSTANCE_NAME";

    /** Unable to find or open a file {0}. */
    public static final String C_KEY_FILE_NOT_FOUND =
            "FILE_NOT_FOUND";

    /** Error reading next token. */
    public static final String C_KEY_ERROR_READING_NEXT_TOKEN =
            "ERROR_READING_NEXT_TOKEN";

    /** Unexpected end of input at or near line {0} in file {1}. */
    public static final String C_KEY_UNEXPECTED_EOS_AT_LINE =
            "UNEXPECTED_EOS_AT_LINE";

    /** Unexpected token {0} at or near line {1} in file {2}, expected {3}. */
    public static final String C_KEY_UNEXPECTED_TOKEN_AT_LINE =
            "UNEXPECTED_TOKEN_AT_LINE";
     
    /** Unknown destination type '{0'} at or near line {1} in file {2}. */
    public static final String C_KEY_UNKNOWN_DEST_TYPE_AT_LINE =
            "UNKNOWN_DEST_TYPE_AT_LINE";

    /** Unable to find or open file {0}, referred to at or near line {1} in file {2}. */
    public static final String C_KEY_REFERRED_FILE_NOT_FOUND_AT_LINE =
            "REFERRED_FILE_NOT_FOUND_AT_LINE";

    /** Unknown format type {0} at line {1} in file {2}. */
    public static final String C_KEY_UNKNOWN_FORMAT_TYPE_AT_LINE =
            "UNKNOWN_FORMAT_TYPE_AT_LINE";
        
    /** Unknown action type {0} at line {1} in file {2}. */
    public static final String C_KEY_UNKNOWN_ACTION_TYPE_AT_LINE =
            "UNKNOWN_ACTION_TYPE_AT_LINE";

    /** Undefined destination name {0} at line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_DEST_NAME_AT_LINE =
            "UNDEFINED_DEST_NAME_AT_LINE";

    /** Undefined action group name {0} at line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_ACTION_GROUP_NAME_AT_LINE =
            "UNDEFINED_ACTION_GROUP_NAME_AT_LINE";

    /** Undefined format name {0} at line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_FORMAT_NAME_AT_LINE =
            "UNDEFINED_FORMAT_NAME_AT_LINE";

    /** Undefined destination group name {0} at line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_DEST_GROUP_NAME_AT_LINE =
            "UNDEFINED_DEST_GROUP_NAME_AT_LINE";

    /** IO exception reading from file {0}. */
    public static final String C_KEY_IO_EXCEPTION_READING_FROM_FILE =
            "IO_EXCEPTION_READING_FROM_FILE";

    /** Expected one of {0} at or near line {1} in file {2}. */
    public static final String C_KEY_EXPECTED_ONE_OF_AT_LINE =
            "EXPECTED_ONE_OF_AT_LINE";

    /** Process initialisation failed for process {0}, due to {1}. */
    public static final String C_KEY_PROCESS_INITIALISATION_FAILURE =
            "PROCESS_INITIALISATION_FAILURE";
    
    /** There was a problem while trying to deliver alarm {0} to file {1}. */
    public static final String C_KEY_PROBLEM_DELIVERING_ALARM_TO_FILE =
            "PROBLEM_DELIVERING_ALARM_TO_FILE";

    /** There was a problem initalising email client. */
    public static final String C_KEY_PROBLEM_INITIALISING_EMAIL_CLIENT =
            "PROBLEM_INITIALISING_EMAIL_CLIENT";

    /** There was a problem while trying to deliver alarm {0} via ftp to node {1}, user {2}. */
    public static final String C_KEY_PROBLEM_DELIVERING_ALARM_VIA_FTP =
            "PROBLEM_DELIVERING_ALARM_VIA_FTP";

    /** There was a problem while trying to deliver alarm {0} via email to email address {1}
     *  using smtp server {2}. */
    public static final String C_KEY_PROBLEM_DELIVERING_ALARM_VIA_EMAIL =
            "PROBLEM_DELIVERING_ALARM_VIA_EMAIL";

    /** Undefined watchdog action group ''{0}'' at or near line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_WDOG_ACTION_GROUP_NAME_AT_LINE =
            "UNDEFINED_WDOG_ACTION_GROUP_NAME_AT_LINE";

    /** Undefined process condition ''{0}'' at or near line {1} in file {2}. */
    public static final String C_KEY_UNDEFINED_PROCESS_CONDITION_AT_LINE =
            "UNDEFINED_PROCESS_CONDITION_AT_LINE";

    /** Error occured while starting ascs process {0}. */
    public static final String C_KEY_ASCS_START_PROCESS_ERROR =
            "ASCS_START_PROCESS_ERROR";

    /** Unknown process condition ''{0}''. Expected one of ''EQUALS'' at or near line {1} in file {2}. */
    public static final String C_KEY_UNKNOWN_PROCESS_CONDITION_AT_LINE =
            "UNKNOWN_PROCESS_CONDITION_AT_LINE";

    /** Undefined Watchdog Group ''{0}''. */
    public static final String C_KEY_UNDEFINED_WATCHDOG_GROUP =
            "UNDEFINED_WATCHDOG_GROUP";
    
    /** Previous configuration error at or near line {0} in file {1}. */
    public static final String C_KEY_PREVIOUS_CONFIG_ERROR_AT_LINE =
            "PREVIOUS_CONFIG_ERROR_AT_LINE";

    /** Exception occurred trying to perform Watchdog Action command {0}. */
    public static final String C_KEY_WATCHDOG_ACTION_COMMAND_EXCEPTION =
            "WATCHDOG_ACTION_COMMAND_EXCEPTION";

    /** Error receiving alarm or event. */
    public static final String C_KEY_ERROR_RECEIVING_ALARM_OR_EVENT =
            "ERROR_RECEIVING_ALARM_OR_EVENT";

    /**
     * Unknown FTP append mode ''{0}'' at or near line {1} in file {2},
     * expected one of 'append' or 'store'.
     */
    public static final String C_KEY_UNKNOWN_FTP_APPEND_MODE_AT_LINE =
            "UNKNOWN_FTP_APPEND_MODE_AT_LINE";

    /**
     * Error initialising watchdog monitor for service ''{0}'', class ''{1}''.
     */
    public static final String C_KEY_ERROR_INIT_WATCHDOG_MONITOR =
            "ERROR_INIT_WATCHDOG_MONITOR";

    /**
     * Error initializing audit logger ''{0}'', using normal logger instead.
     */
    public static final String C_KEY_ERROR_INIT_AUDIT_LOGGER =
            "ERROR_INIT_AUDIT_LOGGER";

    /**
     * Monitor failed for service ''{0}'',  to server {1}, on port {2} - 
     * reached {3} consecutive failures.
     */
    public static final String C_KEY_MONITOR_FAILED_FOR_IP_SERVICE =
            "MONITOR_FAILED_FOR_IP_SERVICE";

    /**
     * Failed over service ''{0}'',  to server {1}.
     */
    public static final String C_KEY_FAILED_OVER_SERVICE =
            "FAILED_OVER_SERVICE";

    /**
     * Unexpected internal server error while ''{0}'', will attempt to continue.
     */
    public static final String C_KEY_UNEXPECTED_INTERNAL_SERVER_ERROR =
            "UNEXPECTED_INTERNAL_SERVER_ERROR";


    /**
     * Monitor ''{0}'' failed, - reached {1} consecutive failures. Script/cmd
     * {2}, cause(s) {3}, output received {4}.
     */
    public static final String C_KEY_MONITOR_SCRIPT_OR_CMD_FAILED =
            "MONITOR_SCRIPT_OR_CMD_FAILED";

    /**
     * Unexpected action '{0}', ignoring. Could be that the action is
     * not valid in this context.
     */
    public static final String C_KEY_UNEXPECTED_ACTION =
            "UNEXPECTED_ACTION";

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Debug methods. */
    private static final String C_CLASS_NAME = "AwdMsg";


    /**
     * Define the name of the file which will contain all the message texts
     * to be associated with config exceptions.
     */
    private static final String C_CONFIG_MSG_FILE = "awd_msg.xml";

    //------------------------------------------------------------------------
    // Public constructor
    //------------------------------------------------------------------------

    /**
     * Construct a config messages resource bundle object. This method opens
     * a stream to the file containing the text of all the messages to be
     * associated with config exceptions.
     * @throws IOException If the message file cannot be read.
     */
    public AwdMsg() throws IOException
    {
        super(AwdMsg.class.getResourceAsStream(C_CONFIG_MSG_FILE));

        if (Debug.on)
        {
            Debug.print(Debug.C_LVL_LOW,
                    WebDebug.C_APP_MWARE,
                    WebDebug.C_ST_CONFIN_START | WebDebug.C_ST_TRACE,
                    C_CLASS_NAME, 10, this,
                    hashCode() + " Constructing " + C_CLASS_NAME);
        }

        if (Debug.on)
        {
            Debug.print(Debug.C_LVL_LOW,
                    WebDebug.C_APP_MWARE,
                    WebDebug.C_ST_CONFIN_END | WebDebug.C_ST_TRACE,
                    C_CLASS_NAME, 20, this,
                    " Constructed " + C_CLASS_NAME);
        }
    }

} // End of public class AwdMsg

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
