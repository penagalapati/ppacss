////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       EchoServiceMonitor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Monitor for an Echo Service (a service which
//                              echos back what is sent to it such as
//                              the unix echo service).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogmodule.echomonitor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.AbstractWatchdogMonitor;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.UtilDateFormat;

/**
 * Monitor for an Echo Service (a service which
 * echos back what is sent to it such as
 * the unix echo service).
 * <p/>
 * Supports the following configuration parameters:
 * <table bgcolor="#EEEEFF" border="1" cellpadding="3" cellspacing="0">
 *   <tr>
 *     <td><b>Parameter</b></td>
 *     <td><b>Description</b></td>
 *     <td><b>Optionality</b></td>
 *     <td><b>Default</b></td>
 *   </tr>
 *   <tr>
 *     <td>echoServiceServer</td>
 *     <td>Server domain name or IP address of server to monitor</td>
 *     <td>M</td>
 *     <td>&lt;none&gt;</td>
 *   </tr>
 *   <tr>
 *     <td>echoServicePort</td>
 *     <td>Port number on server of service to monitor</td>
 *     <td>O</td>
 *     <td>7</td>
 *   </tr>
 *   <tr>
 *     <td>startDelayMillis</td>
 *     <td>Delay (millis) before monitor actually starts monitoring (after
 *         being started). This can
 *         be useful to allow a time at start up for other things to start up
 *         (e.g. the monitored services themselves). Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>30000</td>
 *   </tr>
 *   <tr>
 *     <td>monitorIntervalMillis</td>
 *     <td>Interval (millis) between monitor tests. Must be 0 or a positive
 *         number (0 means don't wait)</td>
 *     <td>O</td>
 *     <td>10000</td>
 *   </tr>
 *   <tr>
 *     <td>retryLimit</td>
 *     <td>When a "ping" fails total number of attempts (including the first failed attempt)
 *         before actually deeming remote service has failed.</td>
 *     <td>O</td>
 *     <td>3</td>
 *   </tr>
 *   <tr>
 *     <td>retryMillis</td>
 *     <td>Milliseconds between retries. Must be 0 or a positive
 *         number (0 means don't wait).</td>
 *     <td>O</td>
 *     <td>5000 (5secs)</td>
 *   </tr>
 *   <tr>
 *     <td>connectTimeoutMillis</td>
 *     <td>Milliseconds to wait for connection to succeed before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 *   <tr>
 *     <td>readTimeoutMillis</td>
 *     <td>Milliseconds to wait for response to be received before timing out.</td>
 *     <td>O</td>
 *     <td>10000 (10secs)</td>
 *   </tr>
 * </table>
 */
public class EchoServiceTcpipMonitor extends AbstractWatchdogMonitor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "EchoServiceTcpipMonitor";

    /** Date format for date time sent in echo mesasage. */
    private UtilDateFormat      i_echoMessageDF =
        new UtilDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
    
    /** The IP address or domain name of the server to monitor. */
    private String              i_echoServiceServer;

    /** The port number of the server to monitor. */
    private int                 i_echoServicePort;
        
    /**
     * Creates a new Echo Service Monitor.
     * @param p_logger          logger to log any messages to.
     * @param p_monitoredServiceName name of monitored service.
     */
    public EchoServiceTcpipMonitor(
        Logger                  p_logger,
        String                  p_monitoredServiceName
        )
    {
        super(p_logger, p_monitoredServiceName);
    }

    /**
     * Initialises the Echo Service Monitor from the configuration parameters.
     * @param p_configParamMap  Map of parameter name (String) to parameter value (String).
     * @throws Exception        exception occurred during initialisation.
     */
    public void doInit(
        Map                     p_configParamMap
        )
    throws Exception
    {
        String                  l_echoServicePortString;
        
        i_echoServiceServer = (String)p_configParamMap.get("echoServiceServer");
        l_echoServicePortString = (String)p_configParamMap.get("echoServicePort");
        if(l_echoServicePortString == null)
        {
            i_echoServicePort = 7; // Official IANA assigned echo service port number
        }
        else
        {
            i_echoServicePort = Integer.parseInt(l_echoServicePortString);
        }
    }

    // TODO 3 Implement a thread which reads from echo server so can detected dropped connection!
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_performMonitorIteration = "performMonitorIteration";
    /**
     * Performs the actual processing of a single monitoring iteration. Typically Called
     * from within its own thread. 
     */
    public void performMonitorIteration()
    {
        int                     l_retryLimit = i_retryLimit;
        boolean                 l_success = false;
        AwdException            l_awdE;
        Exception               l_lastException = null;
       
        do
        {
            try
            {
                l_retryLimit--;
                tryEcho();
                l_success = true;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11110, this,
                        "SUCCEEDED: Echo Service " + i_monitoredServiceName +
                        " [server=" + i_echoServiceServer +
                        ", port=" + i_echoServicePort +
                        "]");
            }
            catch(IOException l_e)
            {
                l_lastException = l_e;
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE | PpasDebug.C_ST_ERROR, C_CLASS_NAME, 11120, this,
                        "FAILED: Echo Service " + i_monitoredServiceName +
                        " [server=" + i_echoServiceServer +
                        ", port=" + i_echoServicePort +
                        "], retriesLeft=" + l_retryLimit +
                        ",exception=" + l_e);
                if(l_retryLimit > 0)
                {
                    // Retry will be attempted, pause configured interval between retries (if any)
                    if(i_retryMillis > 0)
                    {
                        synchronized(this)
                        {
                            try
                            {
                                wait(i_retryMillis);
                            }
                            catch(InterruptedException l_ie)
                            {
                                // Ignore
                            }
                        }
                    }
                }
            }
        }
        while(!l_success && (l_retryLimit > 0) && !isStopping());

        if(!isStopping())
        {
            if(l_success)
            {
                passed(true);
            }
            else
            {
                l_awdE = new WatchdogMonitorException(
                        C_CLASS_NAME, C_METHOD_performMonitorIteration, 11150,
                        this, (PpasRequest)null, (long)0, AwdKey.get().monitorFailedForIpService(
                                i_monitoredServiceName, i_echoServiceServer,
                                i_echoServicePort, i_retryLimit), l_lastException);
                i_logger.logMessage(l_awdE);
                passed(false);
            }
        }
            
    } // End of public method performMonitorIteration
    
    /**
     * Attempts a single actual echo. 
     * @throws IOException      IO exception occurred attempting echo. Echo unsuccessful.
     *                          Not logged or output - caller must do this it required.
     */
    private void tryEcho()
    throws IOException
    {
        Socket                  l_socket = null;
        InputStream             l_is;
        OutputStream            l_os;
        String                  l_lastEchoMessage;
        byte                    l_messageByteARR[];
        int                     l_messageLength;
        byte                    l_echoedMessageByteARR[];
        String                  l_echoedMessage;
        int                     l_totalReadCount;
        int                     l_readCount;
        boolean                 l_eos;
        
        try
        {
            // TODO 3 Use traceable socket (traceable socket would need updating)
            l_socket = new Socket();
            // Connect with timeout
            l_socket.connect(
                    new InetSocketAddress(i_echoServiceServer, i_echoServicePort),
                    i_connectTimeoutMillis);
            l_socket.setSoTimeout(i_readTimeoutMillis);
            l_is = l_socket.getInputStream();
            l_os = l_socket.getOutputStream();
        
            // TODO 2 May need to configure echo service telnet monitor to send line terminators but not expect them to be echoed (needs checking)
            l_lastEchoMessage = "Testing at " + i_echoMessageDF.format(new Date()) + "\n";
            l_messageByteARR = l_lastEchoMessage.getBytes();
            l_os.write(l_messageByteARR);
            
            l_messageLength = l_messageByteARR.length;
            l_echoedMessageByteARR = new byte[l_messageLength];
            l_totalReadCount = 0;
            l_eos = false;
            while(!l_eos && l_totalReadCount < l_messageLength)
            {
                l_readCount = l_is.read(
                        l_echoedMessageByteARR,
                        l_totalReadCount,
                        l_messageLength - l_totalReadCount
                        );
                if(l_readCount > 0)
                {
                    l_totalReadCount += l_readCount;
                }
                else if(l_readCount < 0)
                {
                    l_eos = true;
                }
            }
            l_socket.close();
            l_socket = null;
            l_is = null;
            l_os = null;
            if(!l_eos)
            {
                l_echoedMessage = new String(l_echoedMessageByteARR);
                if(!l_lastEchoMessage.equals(l_echoedMessage))
                {
                    // This is caught above and handled like anyother IO exception
                    throw new IOException("Unmatching echo, got [" +
                            l_echoedMessage + "], expected [" +
                            l_lastEchoMessage + "]");
                }
            }
            
        }
        finally
        {
            // Tidy up
            if(l_socket != null)
            {
                try
                {
                    l_socket.close();
                }
                catch (IOException l_e2)
                {
                    l_e2.printStackTrace();
                    // We tried - ignore and continue
                }
                l_socket = null;
                l_is = null;
                l_os = null;
            }
        }
        
    } // End of private method tryEcho

} // End of public class EchoServiceMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
