/*
 * Created on 15-Jan-04
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AsynchAlarmNotifiyer
{
    
    public void finished(boolean p_success, Alarm p_alarm);
    public void finished(boolean p_success, Alarm p_alarm, Object p_result);
    public boolean wasSuccessful();
    public void waitToFinish();
    public void waitToFinish(long p_timeout);
    public boolean hasFinished();

}
