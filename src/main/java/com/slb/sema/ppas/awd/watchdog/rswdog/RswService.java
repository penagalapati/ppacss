////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RemoteServiceWatchdogProcessor.java
//      DATE            :       16-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Remote service watchdog Service - holds details
//                              about a single service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.ServiceConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorListener;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.support.UtilDateFormat;

/**
 * Remote service watchdog Service - holds details
 * about a single service.
 */
public class RswService implements InstrumentedObjectInterface
{
    
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RswService";
  
    /** Local or remote service VIP is in an unknown state. */
    public static final int    C_STATE_UNKNOWN = 1;

    /** Local or remote service VIP is in the UP state. */
    public static final int    C_STATE_UP = 2;
    
    /** Local or remote service VIP is in the DOWN state. */
    public static final int    C_STATE_DOWN = 3;

    /** Configuration for service. */
    private ServiceConfig       i_serviceConfig;
    
    /** Dateformat including millis. */
    private static UtilDateFormat C_MILLIS_DF = new UtilDateFormat(
            "dd-MMM-yyyy HH:mm:ss.SSS");
    
    /** Mapping of state (Integer) to textual state code. */
    private static Map C_STATE_TO_STRING_MAP = new HashMap();
    static
    {
        C_STATE_TO_STRING_MAP.put(new Integer(C_STATE_UNKNOWN), "UNKOWN"); 
        C_STATE_TO_STRING_MAP.put(new Integer(C_STATE_UP), "UP"); 
        C_STATE_TO_STRING_MAP.put(new Integer(C_STATE_DOWN), "DOWN"); 
    }

    /** Indicates if the monitor is running. */
    private boolean              i_monitorRunning = false;
    
    /** Indicates if the local server is the home for the service. */
    private boolean              i_isHomeForService = false;
    
    /**
     * Monitor for service. Regardless of its home, if Service is
     * running on a remote server, its monitored from here (enabled), else
     * its not monitored (disabled).
     */
    private RswRemoteServiceMonitor i_monitor;

    /** Local server name. */
    private String              i_localNodeName;
    
    /**
     * State of the service on the service's home server - one of the
     * C_STATE_xxx values.
     */
    private int                 i_homeState;
    
    /**
     * State of the service on the service's standby server - one of the
     * C_STATE_xxx values.
     */
    private int                 i_standbyState;
    
    /**
     * Failover service manager which can be used to trigger a failover
     * (for example from A&I).
     */
    private FailoverServiceManagerInterface i_failoverServiceManager;

    /** Instrument set for service. */
    private InstrumentSet       i_instrumentSet;

    /** Total passed count. */
    private int                 i_totalPassedCount;

    /** Total passed counter instrument. */
    private CounterInstrument   i_totalPassedCI;

    /** Total failed count. */
    private int                 i_totalFailedCount;

    /** Total failed counter instrument. */
    private CounterInstrument   i_totalFailedCI;

    /** Total entered passed state count. */
    private int                 i_enteredPassedStateCount;

    /** Total entered passed state counter instrument. */
    private CounterInstrument   i_enteredPassedStateCI;
    
    /** Total entered failed state count */
    private int                 i_enteredFailedStateCount;

    /** Total entered failed state counter instrument. */
    private CounterInstrument   i_enteredFailedStateCI;
    
    /** Total number of failovers performed. */
    private int                 i_failoverPerformedCount;
    
    /** Total number of failovers performed counter instrument. */
    private CounterInstrument   i_failoverPerformedCI;
    
    /** Last time monitor detected a pass. */
    private long                i_lastPassedTime = 0;

    /** Last time monitor detected a failure. */
    private long                i_lastFailedTime = 0;

    /** Last time monitor entered the passed state. */
    private long                i_lastEnteredPassedStateTime = 0;

    /** Last time monitor entered the failed state. */
    private long                i_lastEnteredFailedStateTime = 0;

    /** Last time a failover was performed. */
    private long                i_lastFailoverPerformedTime = 0;
    

    /**
     * Creates a new RSW Service.
     * 
     * @param p_localNodeName   local server name.
     * @param p_serviceConfig   service config.
     */
    public RswService(
        String                  p_localNodeName,
        ServiceConfig           p_serviceConfig
        )
    {
        i_localNodeName = p_localNodeName;
        i_serviceConfig = p_serviceConfig;
        i_homeState = C_STATE_UNKNOWN;
        i_standbyState = C_STATE_UNKNOWN;
        i_instrumentSet = new InstrumentSet("root");

        i_totalPassedCI = new CounterInstrument(
                "Total Passed Count",
                "The total number of times a monitor has detected a pass.");
        i_instrumentSet.add(i_totalPassedCI);
        i_totalFailedCI = new CounterInstrument(
                "Total Failed Count",
                "The total number of times a monitor has detected a failure.");
        i_instrumentSet.add(i_totalFailedCI);
        i_enteredPassedStateCI = new CounterInstrument(
                "Entered Passed State Count",
                "The total number of times a monitor has entered the passed state.");
        i_instrumentSet.add(i_enteredPassedStateCI);
        i_enteredFailedStateCI = new CounterInstrument(
                "Entered Failed State Count",
                "The total number of times a monitor has entered the failed state.");
        i_instrumentSet.add(i_enteredFailedStateCI);
        i_failoverPerformedCI = new CounterInstrument(
                "Failover Performed Count",
                "The total number of times a failover has been performed.");
        i_instrumentSet.add(i_failoverPerformedCI);

        if(i_localNodeName.equals(p_serviceConfig.getServiceHomeServer()))
        {
            i_isHomeForService = true;
        }
    }

    /**
     * Sets the monitor for the Service.
     * @param p_monitor         the monitor which is monitoring the service.
     */
    public void setServiceMonitor(
        RswRemoteServiceMonitor p_monitor
        )
    {
        i_monitor = p_monitor;
        i_monitor.addMonitorListener(new MyMonitorL());
    }

    /**
     * Sets the failover manager for the Service.
     * @param p_failoverServiceManager failover manager for the Service.
     */
    public void setFailoverServiceManager(
        FailoverServiceManagerInterface p_failoverServiceManager
        )
    {
        i_failoverServiceManager = p_failoverServiceManager;
    }

    /** Starts the monitoring of the Service. */
    public synchronized void startMonitoring()
    {
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11100, this,
                "Starting monitoring service " + getServiceName());

        if(!i_monitorRunning)
        {
            i_monitor.start();
            i_monitorRunning = true;
        }
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11190, this,
                "Started monitoring service " + getServiceName());

    }

    /** Stops the monitoring of the Service. */
    public synchronized void stopMonitoring()
    {
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11200, this,
                "Stopping monitoring service " + getServiceName());
        
        if(i_monitorRunning)
        {
            i_monitor.stop();
            i_monitorRunning = false;
        }
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11290, this,
                "Stopped monitoring service " + getServiceName());

    }

    /**
     * Indicates if the monitoring of the service is running.
     * @return <code>true</code> if monitoring, <code>false</code>
     *         otherwise.
     */
    public synchronized boolean isRunning()
    {
        return i_monitorRunning;
    }

    /**
     * Called by the failover manager when a failover has been performed
     * (for example so the service can increment the failover counter).
     */
    public void failoverPerformed()
    {
        i_lastFailoverPerformedTime = System.currentTimeMillis();
        if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
        {
            i_failoverPerformedCount++;
            if (i_instrumentSet.i_isEnabled)
            {
                i_failoverPerformedCI.increment();
            }
        }        
    }

    /**
     * Returns the service name.
     * @return The service name.
     */
    public String getServiceName()
    {
        return i_serviceConfig.getServiceName();
    }

    /**
     * @return Returns the enteredFailedStateWatchdogActionGroupName.
     */
    public String getEnteredFailedStateWatchdogActionGroupName()
    {
        return i_serviceConfig.getEnteredFailedStateWatchdogActionGroupName();
    }
    /**
     * @return Returns the enteredPassedStateWatchdogActionGroupName.
     */
    public String getEnteredPassedStateWatchdogActionGroupName()
    {
        return i_serviceConfig.getEnteredPassedStateWatchdogActionGroupName();
    }
    /**
     * @return Returns the releaseServiceWatchdogActionGroupName.
     */
    public String getReleaseServiceWatchdogActionGroupName()
    {
        return i_serviceConfig.getReleaseServiceWatchdogActionGroupName();
    }
    
    /**
     * @return Returns the getCheckLocalServiceVipStateCommand.
     */
    public String getCheckLocalServiceVipStateCommand()
    {
        return i_serviceConfig.getCheckLocalServiceVipStateCommand();
    }

    /**
     * Sets the state of the service for the supplied server.
     * @param p_server          server name to set state of.
     * @param p_state           state to set to.
     */
    public synchronized void setState(
        String                  p_server,
        int                     p_state)
    {
        if(i_serviceConfig.getServiceHomeServer().equals(p_server))
        {
            i_homeState = p_state;
        }
        else if(i_serviceConfig.getServiceStandbyServer().equals(p_server))
        {
            i_standbyState = p_state;
        }
    }
    
    /**
     * Returns the remote server's name for this Service (which could be
     * either the home server or the standby server name of the service
     * depending on which server this is running on!).
     * @return Remote server's name for this Service
     */
    public String getRemoteServerName()
    {
        String                  l_server;
        
        l_server = getServiceHomeServer();
        if(l_server.equals(i_localNodeName))
        {
            l_server = getServiceStandbyServer();
        }
        
        return l_server;
    }

    /**
     * @return Returns the name of the home server for the Service.
     */
    public String getServiceHomeServer()
    {
        return i_serviceConfig.getServiceHomeServer();
    }
    
    /**
     * @return Returns the name of the standby server for the Service.
     */
    public String getServiceStandbyServer()
    {
        return i_serviceConfig.getServiceStandbyServer();
    }

    /**
     * Indicates if the local server is the home server for the service.
     * @return <code>true</code> if home server for Service, <code>false</code>
     *         otherwise.
     */
    public boolean isHomeForService()
    {
        return i_isHomeForService;
    }
    
    /**
     * @return Returns the homeState.
     */
    public int getServiceHomeState()
    {
        return i_homeState;
    }
    /**
     * @return Returns the standbyState.
     */
    public int getServiceStandbyState()
    {
        return i_standbyState;
    }
    
    /**
     * Returns a textual representation of the home state of the Service.
     * @return A textual representation of the home state of the Service.
     */
    public synchronized String getServiceHomeStateString()
    {
        return (String)C_STATE_TO_STRING_MAP.get(new Integer(i_homeState));
    }

    /**
     * Returns a textual representation of the standby state of the Service.
     * @return A textual representation of the standby state of the Service.
     */
    public String getServiceStandbyStateString()
    {
        return (String)C_STATE_TO_STRING_MAP.get(new Integer(i_standbyState));
    }

    /**
     * Returns the number of passes detected.
     * @return The number of passes detected.
     */
    public int getTotalPassedCount()
    {
        return i_totalPassedCount;
    }

    /**
     * Returns the total number of failures detected.
     * @return The total number of failures detected.
     */
    public int getTotalFailedCount()
    {
        return i_totalFailedCount;
    }

    /**
     * Returns the number of times monitor has entered the passed state.
     * @return The number of times monitor has entered the passed state.
     */
    public int getEnteredPassedStateCount()
    {
        return i_enteredPassedStateCount;
    }

    /**
     * Returns the number of times monitor has entered the failed state.
     * @return The number of times monitor has entered the failed state.
     */
    public int getEnteredFailedStateCount()
    {
        return i_enteredFailedStateCount;
    }

    /**
     * Returns the number of failover performed.
     * @return The number of failover performed.
     */
    public int getFailoverCount()
    {
        return i_failoverPerformedCount;
    }

    /**
     * Returns a textual representation of the local state of the Service
     * (that is, home state if this is its home, otherwise standby state).
     * @return A textual representation of the local state of the Service.
     */
    public synchronized String getLocalStateString()
    {
        int                     l_state;
        
        if(i_serviceConfig.getServiceHomeServer().equals(i_localNodeName))
        {
            l_state = i_homeState;
        }
        else if(i_serviceConfig.getServiceStandbyServer().equals(i_localNodeName))
        {
            l_state = i_standbyState;
        }
        else
        {
            // TODO 2 Log error!
            l_state = C_STATE_UNKNOWN;
        }
            
        return (String)C_STATE_TO_STRING_MAP.get(new Integer(l_state));
    }

    /**
     * Returns the local state of the Service (one of C_STATE_ constants).
     * @return The local state of the Service.
     */
    public synchronized int getLocalState()
    {
        int                     l_state;
        
        if(i_serviceConfig.getServiceHomeServer().equals(i_localNodeName))
        {
            l_state = i_homeState;
        }
        else if(i_serviceConfig.getServiceStandbyServer().equals(i_localNodeName))
        {
            l_state = i_standbyState;
        }
        else
        {
            // Shouldn't be possible
            l_state = C_STATE_UNKNOWN;
        }
            
        return l_state;
    }

    /**
     * Returns the remote state of the Service (one of C_STATE_ constants).
     * @return The remote state of the Service.
     */
    public synchronized int getRemoteState()
    {
        int                     l_state;
        
        if(i_serviceConfig.getServiceHomeServer().equals(i_localNodeName))
        {
            l_state = i_standbyState;
        }
        else if(i_serviceConfig.getServiceStandbyServer().equals(i_localNodeName))
        {
            l_state = i_homeState;
        }
        else
        {
            // Shouldn't be possible
            l_state = C_STATE_UNKNOWN;
        }
            
        return l_state;
    }
    
    /**
     * Returns a textual representation of the remote state of the Service
     * (that is, standby state if this is its home, otherwise home state).
     * @return A textual representation of the remote state of the Service.
     */
    public synchronized String getRemoteStateString()
    {
        int                     l_state;
        
        if(i_serviceConfig.getServiceHomeServer().equals(i_localNodeName))
        {
            l_state = i_standbyState;
        }
        else if(i_serviceConfig.getServiceStandbyServer().equals(i_localNodeName))
        {
            l_state = i_homeState;
        }
        else
        {
            // TODO 2 Log error!
            l_state = C_STATE_UNKNOWN;
        }
            
        return (String)C_STATE_TO_STRING_MAP.get(new Integer(l_state));
    }

    /**
     * Returns the minimum time (millis) between automatic failovers. 
     * @return The minimum time (millis) between automatic failovers.
     */
    public long getMinimumMillisBetweenAutoFailovers()
    {
        return i_serviceConfig.getMinimumMillisBetweenAutoFailovers();
    }

    /**
     * @return Returns the lastEnteredFailedStateTime.
     */
    public long getLastEnteredFailedStateTime()
    {
        return i_lastEnteredFailedStateTime;
    }
    /**
     * @return Returns the lastEnteredPassedStateTime.
     */
    public long getLastEnteredPassedStateTime()
    {
        return i_lastEnteredPassedStateTime;
    }
    /**
     * @return Returns the lastFailedTime.
     */
    public long getLastFailedTime()
    {
        return i_lastFailedTime;
    }
    /**
     * @return Returns the lastFailoverPerformedTime.
     */
    public long getLastFailoverPerformedTime()
    {
        return i_lastFailoverPerformedTime;
    }
    /**
     * @return Returns the lastPassedTime.
     */
    public long getLastPassedTime()
    {
        return i_lastPassedTime;
    }

    /**
     * Returns a formatted string representation of the service with
     * status information.
     * @return Formatted output of the service and its status.
     */
    public String listService()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(80);
        l_sb.append("name: ");
        l_sb.append(getServiceName());
        l_sb.append("  home: ");
        l_sb.append(getServiceHomeServer());
        l_sb.append(" ");
        l_sb.append(getServiceHomeStateString());
        l_sb.append("  standby: ");
        l_sb.append(getServiceStandbyServer());
        l_sb.append(" ");
        l_sb.append(getServiceStandbyStateString());
        l_sb.append(" ");
        if(isRunning())
        {
            l_sb.append("MONITORING");
        }
        else
        {
            l_sb.append("NOT_MONITORING");
        }
     
        return l_sb.toString();
    }

    /**
     * Gets the name of this service (for example for Instrumentation).
     * @return String the name of the object
     */
    public String getName()
    {
        return getServiceName();
    }
    
    /**
     * Gets the the instrument set of objects. These are counters, meters etc. 
     * @return InstrumentSet the set of instruments
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }

    /**
     * Manager method to failover the service (to or from local server).
     * @return A textual result of the failover.
     */
    public String mngMthFailoverService()
    {
        return i_failoverServiceManager.failoverService(getServiceName());
    }
    
    /* *
     * Manager method to get the status of the service.
     * @return The status of the service.
     */
    /*
    public String mngMthGetStatus()
    {
        // TODO 3 Tidy up (use own A&I JSP or use properties to make into pre text box)?
        return "<pre>" + listService() + "</pre>";
    }
    */
    
    /**
     * Manager method to get the last passed time.
     * @return The last passed time (or blank if not yet set).
     */
    public String mngMthGetLastPassedTime()
    {
        String                  l_time = "";
        
        if(i_lastPassedTime > 0)
        {
            l_time = C_MILLIS_DF.format(new Date(i_lastPassedTime));
        }
        
        return l_time;
    }

    /**
     * Manager method to get the last failed time.
     * @return The last failed time (or blank if not yet set).
     */
    public String mngMthGetLastFailedTime()
    {
        String                  l_time = "";
        
        if(i_lastFailedTime > 0)
        {
            l_time = C_MILLIS_DF.format(new Date(i_lastFailedTime));
        }
        
        return l_time;
    }

    /**
     * Manager method to get the last entered passed state time.
     * @return The last entered passed state time (or blank if not yet set).
     */
    public String mngMthGetLastEnteredPassedStateTime()
    {
        String                  l_time = "";
        
        if(i_lastEnteredPassedStateTime > 0)
        {
            l_time = C_MILLIS_DF.format(new Date(i_lastEnteredPassedStateTime));
        }
        
        return l_time;
    }
    
    /**
     * Manager method to get the last entered failed state time.
     * @return The last entered failed state time (or blank if not yet set).
     */
    public String mngMthGetLastEnteredFailedStateTime()
    {
        String                  l_time = "";
        
        if(i_lastEnteredFailedStateTime > 0)
        {
            l_time = C_MILLIS_DF.format(new Date(i_lastEnteredFailedStateTime));
        }
        
        return l_time;
    }

    /**
     * Manager method to get the last failover performed time.
     * @return The last failover performed time (or blank if not yet set).
     */
    public String mngMthGetLastFailoverPerformedTime()
    {
        String                  l_time = "";
        
        if(i_lastFailoverPerformedTime > 0)
        {
            l_time = C_MILLIS_DF.format(new Date(i_lastFailoverPerformedTime));
        }
        
        return l_time;
    }

    /**
     * Manager method to the get the monitoring status (MONITORING or
     * NOT_MONITORING).
     * @return The monitoring status.
     */
    public String mngMthGetMonitoringState()
    {
        String                  l_monitoringState;
        
        if(isRunning())
        {
            l_monitoringState = "MONITORING";
        }
        else
        {
            l_monitoringState = "NOT_MONITORING";
        }
        
        return l_monitoringState;
    }

    /**
     * Manager method to get the Service name.
     * @return The Service name.
     */
    public String mngMthGetServiceName()
    {
        return getServiceName();
    }
    
    /**
     * Manager method to get the local server name.
     * @return The local server name.
     */
    public String mngMthGetLocalServerName()
    {
        return i_localNodeName;
    }
    
    /**
     * Manager method to get the home server name and its VIP state.
     * @return The home server name and its VIP state.
     */
    public String mngMthGetHomeServerAndVipState()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(80);
        l_sb.append(getServiceHomeServer());
        l_sb.append(" - VIP ");
        l_sb.append(getServiceHomeStateString());
        
        return l_sb.toString();
    }

    /**
     * Manager method to get the standby server name and its VIP state.
     * @return The standby server name and its VIP state.
     */
    public String mngMthGetStandbyServerAndVipState()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(80);
        l_sb.append(getServiceStandbyServer());
        l_sb.append(" - VIP ");
        l_sb.append(getServiceStandbyStateString());
        
        return l_sb.toString();
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer();
        
        l_sb.append(C_CLASS_NAME + "=[serviceName=");
        l_sb.append(getServiceName());
        l_sb.append(", homseState=");
        l_sb.append(i_homeState);
        l_sb.append("(");
        l_sb.append(C_STATE_TO_STRING_MAP.get(new Integer(i_homeState)));
        l_sb.append("), standbyState=");
        l_sb.append(i_standbyState);
        l_sb.append("(");
        l_sb.append(C_STATE_TO_STRING_MAP.get(new Integer(i_standbyState)));
        l_sb.append("), monitor=");
        l_sb.append(i_monitor);
        l_sb.append(", config=");
        l_sb.append(i_serviceConfig);
        l_sb.append("]");
        
        return l_sb.toString();
    }

    /**
     * Private inner class implementing the listener which the Service uses
     * to listen to the monitor.
     */
    private class MyMonitorL implements WatchdogMonitorListener
    {

        /**
         * Creates a new MyMonitorL.
         */
        public MyMonitorL()
        {
            // Currently nothing to do.
        }

        /** Called each time the monitor detects a pass. */
        public void passed()
        {
            i_lastPassedTime = System.currentTimeMillis();
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                i_totalPassedCount++;
                if (i_instrumentSet.i_isEnabled)
                {
                    i_totalPassedCI.increment();
                }
            }
        }

        /** Called each time the monitor detects a failure. */
        public void failed()
        {
            i_lastFailedTime = System.currentTimeMillis();
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                i_totalFailedCount++;
                if (i_instrumentSet.i_isEnabled)
                {
                    i_totalFailedCI.increment();
                }
            }
        }

        /** Called each time the monitor enters the passed state. */
        public void hasEnteredPassedState()
        {
            i_lastEnteredPassedStateTime = System.currentTimeMillis();
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                i_enteredPassedStateCount++;
                if (i_instrumentSet.i_isEnabled)
                {
                    i_enteredPassedStateCI.increment();
                }
            }

        }

        /** Called each time the monitor enters the failed state. */
        public void hasEnteredFailedState()
        {
            i_lastEnteredFailedStateTime = System.currentTimeMillis();
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                i_enteredFailedStateCount++;
                if (i_instrumentSet.i_isEnabled)
                {
                    i_enteredFailedStateCI.increment();
                }
            }
            
        }        

    } // End of private inner class MyMonitorL
    
} // End of public class RswService.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////