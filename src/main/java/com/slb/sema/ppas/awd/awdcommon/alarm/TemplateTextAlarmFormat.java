/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;



/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TemplateTextAlarmFormat extends AlarmFormat
{
    private String              i_template;

    public TemplateTextAlarmFormat(String p_template)
    {
        super(C_TYPE_TEMPLATE_TEXT);
        i_template = p_template;
    }
    
    public String getTemplate()
    {
        return i_template;
    }

    public String toString()
    {
        return("TemplateTextAlarmFormat=[" + super.toString() +
                ", template=[" + i_template + "]]");
    }

}
