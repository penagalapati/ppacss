/*
 * Created on 04-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.watchdogserver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.ProcessCondition;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WatchdogProcessConfig extends AwdAbstractConfig
{
    /** Name of class. Value is {@value}. */
    private static final String C_CLASS_NAME = "WatchdogProcessConfig";

    /** Empty instance of Watchdog process configuration array. */
    private static final WatchdogProcessConfig.Entry C_EMPTY_ENTRY_ARRAY[] =
            new WatchdogProcessConfig.Entry[0];

   /**
    * Watchdog configuration - set of <code>Entry</code>s each mapping a
    * monitored process name to a Watchdog Group.
    */
    private Set                 i_watchdogConfigSet; 

    private boolean             i_cacheInvalidated = true;
    private WatchdogProcessConfig.Entry i_watchdogEntryCachedARR[];

    /**
     * Watchdog group configuration - map of watchdog group name to
     * <code>WatchdogGroup</code>.
     */
    private HashMap             i_watchdogGroupMap; 

    /**
     * Watchdog action group configuration - map of watchdog action group name to
     * <code>WatchdogActionGroup</code>.
     */
    private HashMap             i_watchdogActionGroupMap; 

    /**
     * Process condition map - map of process condition name to
     * process condition.
     * <code>WatchdogGroup</code>.
     */
    private HashMap             i_processConditionMap; 

    public WatchdogProcessConfig(
        Logger                       p_logger,
        AlarmConfig                  p_alarmConfig
        )
    {
        super(p_logger);

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                        "Constructing " + C_CLASS_NAME);
        }
            
        init();

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                        "Constructed " + C_CLASS_NAME);
        }
    }

    public synchronized WatchdogProcessConfig.Entry[] getEntryArray()
    {
        if(i_cacheInvalidated)
        {
            i_watchdogEntryCachedARR =
                (WatchdogProcessConfig.Entry[])i_watchdogConfigSet.
                toArray(C_EMPTY_ENTRY_ARRAY);
            i_cacheInvalidated = false;
        }
 
        return i_watchdogEntryCachedARR;
    }
    
    private void init()
    {
        i_cacheInvalidated = true;

        i_watchdogConfigSet = new HashSet();
        i_watchdogGroupMap = new HashMap();
        i_watchdogActionGroupMap = new HashMap();
        i_processConditionMap = new HashMap();
    }
        
    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_addWatchdogGroup = "addWatchdogGroup";    
    public void addWatchdogGroup(
        WatchdogGroup           p_watchdogGroup
    )
    {

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addWatchdogGroup + 
                ": Adding mapping of watchdog group " + p_watchdogGroup
                );

        i_watchdogGroupMap.put(
                p_watchdogGroup.getGroupName(), p_watchdogGroup);
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_addWatchdogActionGroup = "addWatchdogActionGroup";    
    /* package */ void addWatchdogActionGroup(
        WatchdogActionGroup     p_watchdogActionGroup
    )
    {

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addWatchdogGroup + 
                ": Adding mapping of watchdog action group " + p_watchdogActionGroup
                );

        i_watchdogActionGroupMap.put(
                p_watchdogActionGroup.getGroupName(), p_watchdogActionGroup);
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_getWatchdogActionGroup = "getWatchdogActionGroup";    
    /* package */ WatchdogActionGroup getWatchdogActionGroup(
        String                  p_watchdogActionName
    )
    {
        WatchdogActionGroup        l_watchdogActionGroup;

        l_watchdogActionGroup = (WatchdogActionGroup)i_watchdogActionGroupMap.get(
                p_watchdogActionName);

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_END | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_getWatchdogActionGroup + 
                ": Returning process condition " + l_watchdogActionGroup + " for name " +
                p_watchdogActionName
                );
                
        return l_watchdogActionGroup;
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_removeProcessMapping = "removeProcessMapping";    
    public synchronized void removeProcessMapping(
        String                  p_processName
    )
    {
        Iterator    l_entries;
        String      l_entryName;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_removeProcessMapping + 
                ": Removing mapping of process name " + p_processName
                );

        l_entries = i_watchdogConfigSet.iterator();
        while (l_entries.hasNext())
        {
            l_entryName = ((WatchdogProcessConfig.Entry)l_entries.next()).getProcessName();
            if (p_processName.equals(l_entryName))
            {
                l_entries.remove();
            }
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW,
                    Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_END,
                    C_CLASS_NAME, 10190, this,
                    "Leaving " + C_METHOD_removeProcessMapping
                    );
        }
}

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_addProcessMapping = "addProcessMapping";    
    public synchronized void addProcessMapping(
        String                  p_processName,
        String                  p_watchdogGroupName
    )
    throws AwdConfigException
    {

        WatchdogGroup           l_watchdogGroup;

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addProcessMapping + 
                ": Adding mapping of process name " + p_processName +
                " to watchdog group name " + p_watchdogGroupName
                );

        l_watchdogGroup = (WatchdogGroup)i_watchdogGroupMap.get(
                p_watchdogGroupName);
        if(l_watchdogGroup != null)
        {
            // Before adding the entry we must remove any existing ones
            // for this process to avoid duplicates
            removeProcessMapping(p_processName);
            i_watchdogConfigSet.add(new Entry(p_processName, l_watchdogGroup));
        }
        else
        {
            if(Debug.on) Debug.print(
                    Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                    Debug.C_ST_TRACE | Debug.C_ST_ERROR, C_CLASS_NAME, 11050, this,
                    C_METHOD_addProcessMapping +
                    ": Failed to add mapping - no group called " +
                    p_watchdogGroupName);
            handleConfigException(C_CLASS_NAME, C_METHOD_addProcessMapping, 10000,
                    AwdKey.get().undefinedWatchDogGroup(p_watchdogGroupName));
            
        }
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_addProcessCondition = "addProcessCondition";    
    /* package */ void addProcessCondition(
        String                  p_conditionName,
        ProcessCondition        p_processCondition
    )
    {

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_addProcessCondition + 
                ": Adding mapping of process condition " + p_processCondition
                );

        i_processConditionMap.put(
                p_conditionName, p_processCondition);
    }

    /** Name of method. Value is {@value}. */
    private static final String C_METHOD_getProcessCondition = "getProcessCondition";    
    /* package */ ProcessCondition getProcessCondition(
        String                  p_conditionName
    )
    {
        ProcessCondition        l_processCondition;

        l_processCondition = (ProcessCondition)i_processConditionMap.get(
                p_conditionName);

        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_END | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                C_METHOD_getProcessCondition + 
                ": Returning process condition " + l_processCondition + " for name " +
                p_conditionName
                );
                
        return l_processCondition;
    }

    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(1000);
        
        l_sb.append("WatchdogProcessConfig=[entrySet=[\n");
        l_sb.append(i_watchdogConfigSet);
        l_sb.append("]\n]");
        l_sb.append(", watchdogGroupMap=[\n");
        l_sb.append(i_watchdogGroupMap);
        l_sb.append("]]\n]");
        
        return l_sb.toString();
    }

    public class Entry
    {
        private String          i_processName;
        private WatchdogGroup   i_watchdogGroup; // Conditions and actions
        
        private Entry(
            String              p_processName,
            WatchdogGroup       p_watchdogGroup
        )
        {
            i_watchdogGroup = p_watchdogGroup;
            i_processName = p_processName;
        }
        
        public String getProcessName()
        {
            return i_processName;
        }

        public WatchdogGroup getWatchdogGroup()
        {
            return i_watchdogGroup;
        }
        
        public String toString()
        {
            return "Entry=[procName=" + i_processName + ", name=" +
                    i_watchdogGroup.getGroupName() + "]";
        }

    }

/*    
    private class ConditionGroup
    {
        String                  i_groupName;
        int                     i_stateARR[];
        
        private ConditionGroup(
            String              p_groupName
        )
        {
            i_groupName = p_groupName;
            
            return;
        }
        
        private synchronized void addState(
            int                 p_state
        )
        {
            int                 l_newSize = 1;
            int                 l_newStateARR[];
            
            if(i_stateARR == null)
            {
                l_newSize = 1;
            }
            else
            {
                l_newSize = i_stateARR.length + 1;
            }
            l_newStateARR = new int[l_newSize];
            System.arraycopy(
                    i_stateARR, 0, l_newStateARR, 0, (l_newSize -1));
            l_newStateARR[l_newSize - 1] = p_state;
            i_stateARR = l_newStateARR;
            
            return;
        }
    } // End of private inner class ConditionGroup
*/
}
