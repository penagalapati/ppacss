////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FtpAlarmHandler.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This class implements an FTP
//                              Alarm Handler.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.ftpclient.FtpClient;
import com.slb.sema.ppas.common.ftpcommon.FtpException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * This class implements an FTP Alarm Handler which delivers alarms via FTP.
 */
public class FtpAlarmHandler extends AbstractAsynchAlarmHandler
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FtpAlarmHandler";
    
    /** 
     * Array of FTP Clients to FTP using. Each client is single
     * threaded, so one client is created for each background delivery
     * thread.
     */
    private FtpClient           i_ftpClientARR[];


    /**
     * Creates a new FTP Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     */
    public FtpAlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
    )
    {
        super(  p_logger,
                p_configProperties,
                p_instrumentManager,
                p_awdContext,
                3,
                "FtpAH"
                );
        init(p_configProperties);
    }
    
    /**
     * Initialises this object based on the supplied configuration.
     * 
     * @param p_configProperties configuration to initialise from.
     */    
    private void init(
        PpasProperties          p_configProperties
        )
    {
        int                     l_loop;
        
        i_ftpClientARR = new FtpClient[3];
        
        try
        {
            for(l_loop = 0; l_loop < 3; l_loop++)
            {
                i_ftpClientARR[l_loop] = new FtpClient();
                i_ftpClientARR[l_loop].init(new UtilProperties() /*<<!!!!>>*/ );
            }
        }
        catch(FtpException l_e)
        {
            //<<Log - or already logged????>>>
            l_e.printStackTrace();
        }
        
    }
            
    /** Method name passed to middleware. Value is {@value}. */
    private static final String C_METHOD_doDeliverAsynchAlarm = "doDeliverAsynchAlarm";

    /**
     * {@inheritDoc}
     */
    public void doDeliverAsynchAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
    )
    throws AlarmDeliveryException
    {
        
        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12010, this,
                "Delivering alarm " + p_formattedAlarm + " to destination " +
                p_destination);
                
        deliverFtpAlarm(p_threadInstanceNum, p_alarm, p_formattedAlarm, p_destination);
    }

    /**
     * Actually performs the alarm delivery using an allocated FTP client
     * instance.
     * 
     * @param p_threadInstanceNum the thread number calling this method (used
     *                          to selected the allocated email client).
     * @param p_alarm           the alarm.
     * @param p_formattedAlarm  the formatted alarm.
     * @param p_destination     the destination to deliver the alarm to.
     * @throws AlarmDeliveryException a problem occurred trying to deliver
     *                          the alarm, the alarm is unlikely to have been
     *                          delivered successfully.
     */
    private void deliverFtpAlarm(
        int                     p_threadInstanceNum,    
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
    )
    throws AlarmDeliveryException
    {
        FtpAlarmDestination     l_alarmDestination;
        ByteArrayInputStream    l_inputStream = null;
        FtpClient               l_ftpClient;
        String                  l_filename;
        Map                     l_map;
        AlarmDeliveryException  l_alarmE;
        
        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12110, this,
                "deliverFtpAlarm: Ftping formatted alarm to destination " +
                p_destination);
        
        l_ftpClient = i_ftpClientARR[p_threadInstanceNum-1]; // Thread no from 1
        l_alarmDestination = (FtpAlarmDestination)p_destination;

        try
        {        
            l_ftpClient.connect(l_alarmDestination.getRemoteNode(),
                    l_alarmDestination.getRemotePort());
                    
            l_ftpClient.login(l_alarmDestination.getUserName(),
                    l_alarmDestination.getPassword());


            // System.out.println("Byte array len=" + p_formattedAlarm.getBytes().length);
            l_inputStream = new ByteArrayInputStream(
                    p_formattedAlarm.getBytes());
            l_filename = l_alarmDestination.getRemotePathAndFilename();

            // Handle macros.
            l_map = p_alarm.getParameterMap();
            l_filename = i_macroSubstitutor.expand(l_filename, l_map);

            switch(l_alarmDestination.getAppendMode())
            {
                case FtpAlarmDestination.C_APPEND_MODE_APPEND:                          
                l_ftpClient.appendFile(l_inputStream, l_filename);
                break;

                default: // Default to store.                          
                l_ftpClient.storeFile(l_inputStream, l_filename);
                break;                
            }

            if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                    Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME,
                    12120, this,
                    "deliverFtpAlarm: Successfully ftp'ed alarm " +
                    p_formattedAlarm + " to " + l_alarmDestination.getRemoteNode() +
                    "::" + l_alarmDestination.getUserName()
                    );
        }
        catch(FtpException l_e)
        {
            l_alarmE = new AlarmDeliveryException(
                    C_CLASS_NAME,
                    C_METHOD_doDeliverAsynchAlarm,
                    10150,
                    this,
                    (PpasRequest)null,
                    0,
                    AwdKey.get().problemDeliveringAlarmViaFtp(
                        p_formattedAlarm,
                        l_alarmDestination.getRemoteNode() + ":" +
                            l_alarmDestination.getRemotePort(),
                        l_alarmDestination.getUserName()),
                    l_e);
            i_logger.logMessage(l_alarmE);
            throw l_alarmE;
        }
        finally
        {
            // Try to tidy up as best we can.
            if(l_ftpClient.isConnected())
            {
                try
                {
                    l_ftpClient.disconnect();
                }
                catch(FtpException l_ftpE)
                {
                    System.err.println(C_CLASS_NAME + ": Exception disconnecting - ");
                    l_ftpE.printStackTrace();
                }
            }

            if(l_inputStream != null)
            {
                try
                {
                   l_inputStream.close();
                }
                catch(IOException l_e)
                {
                    System.err.println(C_CLASS_NAME + ": Exception closing byte input stream - ");
                    l_e.printStackTrace();
                }
            }
        }

    } // End of private method deliverFtpAlarm

} // End of public class FtpAlarmHandler

////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////