////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RemoteServiceWatchdogProcessor.java
//      DATE            :       08-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Rswdog processor - main high level processing
//                              for the rswdog.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/06/06 | M.Vonka    | Fixed to use new method to send | PpaLon#9093/2413
//          |            | response which ties response    | PpaLon#2351/8997
//          |            | to original request.            |
//----------+------------+---------------------------------+--------------------
// 15/09/06 | M.Vonka    | Also stop control server so     | PpacLon#2633/9976
//          |            | shutsdown properly.             |
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.rswdog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.AwdException;
import com.slb.sema.ppas.awd.awdcommon.AwdKey;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfig;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfigLoader;
import com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogConfig.ServiceConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroup;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroupConfig;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogActionGroupConfigLoader;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogMonitorListener;
import com.slb.sema.ppas.awd.watchdog.wdogcommon.WatchdogPlatform;
import com.slb.sema.ppas.common.smp.SimpleMessage;
import com.slb.sema.ppas.common.smp.SimpleMessageProtocolMessageHandler;
import com.slb.sema.ppas.common.smp.SimpleMessageProtocolSocketEndPoint;
import com.slb.sema.ppas.common.smp.SmpIoException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Rswdog processor - main high level processing
 * for the rswdog.
 */
public class RemoteServiceWatchdogProcessor
implements FailoverServiceManagerInterface
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "RemoteServiceWatchdogProcessor";
    
    /** Utility empty array for use in toArray methods. */
    private static final RswService C_RSW_SERVICE_EMPTY_ARRAY[] = new RswService[0];
    
    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.watchdog.rswdog.RemoteServiceWatchdogProcessor.";

    /** Line separator for this platform (used in output, for example for list services). */
    private static final String c_lineSeparator = System.getProperty("line.separator");

    /** Logger messages logged to. */
    private Logger              i_logger;
    
    /** Logger Audit messages logged to. */
    private Logger              i_auditLogger;

    /** Context for this component. */
    protected AwdContext        i_awdContext;
    
    /** Instrument Manager to register any instruments with. */
    protected InstrumentManager i_instrumentManager;

    /** Alarm Group Config (for any alarms). */
    private AlarmGroupConfig    i_alarmGroupConfig;
    
    /** Alarm Config (for any alarms). */
    private AlarmConfig         i_alarmConfig;
    
    /** Watchdog Action Group Config (for any actions including alarms, running OS scripts etc). */
    private WatchdogActionGroupConfig i_watchdogActionGroupConfig;
    
    /** Watchdog Platform used to perform watchdog actions while hiding platform dependent issues */
    private WatchdogPlatform    i_watchdogPlatform;
    
    /**
     * Thread which polls the local and remote status of all configured services (that this rswdog
     * is the home or standby for).
     */
    private StatusThread        i_statusThread;

    /**
     * Delay between service status polls. Default is 15s.
     */
    private long                i_statusCheckIntervalMillis = 15 * 1000; // 15s

    /** Process name. */
    private String              i_processName;
    
    /** Server node name. */
    private String              i_nodeName;
    
    /** Configuration properties for this processor. */
    private PpasProperties      i_configProperties;
    
    /** Rswdog Action Processor used to perform actions (such as raise alarms, run OS scripts etc). */
    private RswActionProcessor  i_rswActionProcessor;
    
    /**
     * Remote Service Watchdog configuration (such as services to monitor, their home and
     * standby server node names, actions to perform when a service fails).
     */
    private RemoteServiceWatchdogConfig i_remoteServiceWdogConfig;
        
    /** Map of service name to RswService */
    private Map                 i_rswServiceMap = new HashMap();
    
    /** Indicates if the cached array copy of the services is invalid and needs to be recreated. */
    private boolean             i_rswServiceCachedArrayInvalidated = true;

    /** Cached array copy of the services in the service Map. */
    private RswService          i_rswServiceCachedARR[];

    /**
     * SMP Handler for handling RSWP (Remote Service Watchdog Protocol) messages.
     * RSWP messages are messages between the rswdog processes on different servers to
     * share status information and control failover.
     */
    private SimpleMessageProtocolMessageHandler i_smpMessageHandler;
    
    /**
     * The RSWP Control Server for this rswdog. Handles accepting
     * RSWP connectios and managing them.
     */
    private RswControlServer    i_controlServer;
    
    /** Map of server name to Rsw Control Client for that server). */
    private Map                 i_controlClientMap;

    /**
     * Creates a new Remote Service Watchdog Processor.
     * 
     * @param p_logger          logger to log messages to.
     * @param p_instrumentManager instrument manager.
     * @param p_awdContext      context for this processor.
     * @param p_auditLogger     logger to log audit messages to.
     */
    public RemoteServiceWatchdogProcessor(
        Logger                  p_logger,
        InstrumentManager       p_instrumentManager,
        RswdogContext           p_awdContext,
        Logger                  p_auditLogger
    )
    {
        super();

        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        i_logger = p_logger;
        i_instrumentManager = p_instrumentManager;
        i_awdContext = p_awdContext;
        i_auditLogger = p_auditLogger;
        i_processName = i_awdContext.getProcessName();
        i_nodeName = i_awdContext.getNodeName();
        i_controlClientMap = new HashMap();
        
        i_instrumentManager.registerObject(new ProcessorInstrumentor());

        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Initialises the Remote Service Watchdog Processor.
     * @param p_configProperties configuration for processor.
     * @throws AwdConfigException a configuration exception occurred, initialisation failed.
     * @throws PpasConfigException a configuration exception occurred, initialisation failed.
     */
    public void init(
        PpasProperties          p_configProperties
    )
    throws AwdConfigException, PpasConfigException
    {
        AlarmGroupConfigLoader  l_alarmGroupConfigLoader;
        AlarmConfigLoader       l_alarmConfigLoader;
        WatchdogActionGroupConfigLoader l_watchdogActionGroupConfigLoader;
        String                  l_configFilename;
        RemoteServiceWatchdogConfigLoader l_remoteServiceWdogConfigLoader;
                
        i_configProperties = p_configProperties;
        
        l_alarmGroupConfigLoader = new AlarmGroupConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "alarmGroupConfigFilePathAndName");
        i_alarmGroupConfig = l_alarmGroupConfigLoader.loadAlarmGroupConfig(
                l_configFilename);

        l_alarmConfigLoader = new AlarmConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "alarmConfigFilePathAndName");
        i_alarmConfig = l_alarmConfigLoader.loadAlarmConfig(l_configFilename);
        
        l_watchdogActionGroupConfigLoader = new WatchdogActionGroupConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "watchdogActionGroupConfigFilePathAndName");
        i_watchdogActionGroupConfig = l_watchdogActionGroupConfigLoader.loadWatchdogActionGroupConfig(
                l_configFilename, i_alarmConfig, i_alarmGroupConfig, i_processName);
        
        l_remoteServiceWdogConfigLoader = new RemoteServiceWatchdogConfigLoader(
                i_logger, i_configProperties, i_awdContext);
        l_configFilename = p_configProperties.getTrimmedProperty(C_CFG_PROPERTY_BASE +
                "remoteServiceWdogConfigFilePathAndName");
        i_remoteServiceWdogConfig = l_remoteServiceWdogConfigLoader.
                loadRemoteServiceWatchdogConfig(
                        l_configFilename,
                        i_processName);
        System.out.println(">>>>Loaded condfig=" + i_remoteServiceWdogConfig);
        i_auditLogger.logMessage(new LoggableEvent(
                ">>>> Loaded condfig=" + i_remoteServiceWdogConfig,
                LoggableInterface.C_SEVERITY_INFO));
        
        i_rswActionProcessor = new RswActionProcessor(
                i_logger, i_instrumentManager, i_awdContext, i_configProperties,
                i_alarmGroupConfig, i_alarmConfig, i_watchdogActionGroupConfig);

        i_watchdogPlatform = new WatchdogPlatform(
                i_logger, i_instrumentManager, p_configProperties);

        i_smpMessageHandler = new RswpMessageHandler();

        i_controlServer = new RswControlServer(
                i_logger,
                i_instrumentManager,
                i_configProperties,
                i_smpMessageHandler);
        i_controlServer.start();
        i_auditLogger.logMessage(new LoggableEvent(
                ">>>> Started RSWP server listening on local IP address " +
                i_controlServer.getLocalIpAddressLoggableString() + ", port " +
                i_controlServer.getLocalIpPort(),
                LoggableInterface.C_SEVERITY_INFO));
        
        createServices();
                
    }

    // TODO 2 Actually most of the processor is started in init! Sort out.
    /**
     * Starts the processor.
     */
    public synchronized void start()
    {
        if(i_statusThread == null)
        {
            i_statusThread = new StatusThread();
        }
        i_statusThread.start();
    }

    // TODO 2 Actually most of the processor continues to run! Sort out.
    /**
     * Stops the processor.
     */
    public synchronized void stop()
    {
        if(i_controlServer != null)
        {
            i_controlServer.stop();
            i_controlServer = null;
        }
        if(i_statusThread != null)
        {
            try
            {
                i_statusThread.stop();
            }
            catch(InterruptedException l_e)
            {
                // Ignore
            }
            finally
            {
                i_statusThread = null;
            }
        }
    }
    
    /**
     * Releases the named service.
     * @param p_service         name of service to release.
     * @return                  <code>null</code> if successful, error message otherwise.
     */
    public String releaseService(
        String                  p_service
        )
    {
        // TODO 2 Tidy this up!
        return releaseService(p_service, "unknown-ip-address", false);
    }

    /**
     * Lists the services with status information.
     * @return                  Formatted output of services and their status.
     */
    public String listServices()
    {
        StringBuffer            l_sb;
        RswService              l_rswServiceARR[];
        RswService              l_service;
        int                     l_loop;
        
        l_sb = new StringBuffer(160);
        l_sb.append("node: " + i_nodeName + " process " + i_processName + " SERVICES:");
        l_sb.append(c_lineSeparator);
        l_sb.append("----------------------------------------");
        l_sb.append(c_lineSeparator);
        
        l_rswServiceARR = getServiceArray();
        for(l_loop = 0; l_loop < l_rswServiceARR.length; l_loop++)
        {
            //l_sb.append(l_rswServiceARR[l_loop]);
            l_service = l_rswServiceARR[l_loop];
            l_sb.append("name: ");
            l_sb.append(l_service.getServiceName());
            l_sb.append("  home: ");
            l_sb.append(l_service.getServiceHomeServer());
            l_sb.append(" ");
            l_sb.append(l_service.getServiceHomeStateString());
            l_sb.append("  standby: ");
            l_sb.append(l_service.getServiceStandbyServer());
            l_sb.append(" ");
            l_sb.append(l_service.getServiceStandbyStateString());
            l_sb.append(" ");
            if(l_service.isRunning())
            {
                l_sb.append("MONITORING");
            }
            else
            {
                l_sb.append("NOT_MONITORING");
            }
            l_sb.append(c_lineSeparator);
        }
     
        return l_sb.toString();
    }

    /**
     * Lists the services with status information in an HTML format.
     * @return                  Formatted output of services and their status.
     */
    public String listServicesHtml()
    {
        StringBuffer            l_sb;
        RswService              l_rswServiceARR[];
        RswService              l_service;
        int                     l_loop;
        
        l_sb = new StringBuffer(400);
        l_sb.append("<table border=\"1\" cellspacing=\"0\">");
        l_sb.append(c_lineSeparator);
        l_sb.append("<tr><td colspan=\"3\">node: " + i_nodeName +
                "</td><td colspan=\"8\">process: " + i_processName + "</td></tr>");
        l_sb.append(c_lineSeparator);
        
        l_sb.append("<tr><td rowspan=\"2\">service name</td>" +
                "<td colspan=\"2\">home</td><td colspan=\"2\">standby</td>" +
                "<td rowspan=\"2\">monitoring<br/>(from this server)</td>" +
                "<td rowspan=\"2\">total pass</td>" +
                "<td rowspan=\"2\">total fail</td>" +
                "<td rowspan=\"2\">enter pass</td>" +
                "<td rowspan=\"2\">enter fail</td>" +
                "<td rowspan=\"2\">total failovers</td>" +
                "</tr>");
        l_sb.append(c_lineSeparator);
        l_sb.append("<tr><td width=\"400\">server</td>" +
                "<td width=\"200\">VIP state</td><td width=\"400\">server</td>" +
                "<td width=\"200\">VIP state</td></tr>");
        l_sb.append(c_lineSeparator);
        l_rswServiceARR = getServiceArray();
        for(l_loop = 0; l_loop < l_rswServiceARR.length; l_loop++)
        {
            //l_sb.append(l_rswServiceARR[l_loop]);
            l_service = l_rswServiceARR[l_loop];
            l_sb.append("<tr><td>");
            l_sb.append(l_service.getServiceName());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getServiceHomeServer());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getServiceHomeStateString());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getServiceStandbyServer());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getServiceStandbyStateString());
            l_sb.append("</td><td>");
            if(l_service.isRunning())
            {
                l_sb.append("YES");
            }
            else
            {
                l_sb.append("NO");
            }
            l_sb.append("</td><td>");
            l_sb.append(l_service.getTotalPassedCount());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getTotalFailedCount());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getEnteredPassedStateCount());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getEnteredFailedStateCount());
            l_sb.append("</td><td>");
            l_sb.append(l_service.getFailoverCount());
            l_sb.append("</td></tr>");
            l_sb.append(c_lineSeparator);
        }
        l_sb.append("</table>");
        l_sb.append(c_lineSeparator);
     
        return l_sb.toString();
    }

    /**
     * Creates the services from configuration. Only services this server(node) is the home
     * or standby for are created.
     * @throws PpasConfigException a configuration excption occurred.
     */
    private void createServices()
    throws PpasConfigException
    {

        ServiceConfig           l_serviceConfigARR[];
        int                     l_loop;
        ServiceConfig           l_serviceConfig;
        String                  l_serviceHomeServer;
        String                  l_serviceStandbyServer;
        RswService              l_rswService;
        
        String                  l_serviceName;
        RswRemoteServiceMonitor l_serviceMonitor;
        String                  l_remoteServer;
        RswControlClient        l_rswControlClient;
        boolean                 l_initSuccess;
        
        l_serviceConfigARR = i_remoteServiceWdogConfig.getServiceConfigArray();
        for(l_loop = 0; l_loop < l_serviceConfigARR.length; l_loop++)
        {
            l_serviceConfig = l_serviceConfigARR[l_loop];
            l_serviceName = l_serviceConfig.getServiceName();
            l_serviceHomeServer = l_serviceConfig.getServiceHomeServer();
            l_serviceStandbyServer = l_serviceConfig.getServiceStandbyServer();
            if(i_nodeName.equals(l_serviceHomeServer) || i_nodeName.equals(l_serviceStandbyServer))
            {
                l_initSuccess = true;
                // This is the home or standby server for this Service

                // Ensure we have a Control Client to communicate with
                // the Rsw process on the other server
                if(i_nodeName.equals(l_serviceHomeServer))
                {
                    l_remoteServer = l_serviceStandbyServer;
                }
                else
                {
                    l_remoteServer = l_serviceHomeServer;
                }
                l_rswControlClient = (RswControlClient)i_controlClientMap.get(
                        l_remoteServer);
                if(l_rswControlClient == null)
                {
                    // Haven't got a client for this remote server, create one.
                    l_rswControlClient = new RswControlClient(
                            i_logger,
                            i_instrumentManager,
                            i_configProperties
                            );
                    i_controlClientMap.put(l_remoteServer, l_rswControlClient);
                    l_rswControlClient.start();
                }

                l_rswService = new RswService(i_nodeName, l_serviceConfig);
                i_instrumentManager.registerObject(l_rswService);
                l_serviceMonitor = new RswRemoteServiceMonitor(
                        i_logger,
                        i_instrumentManager,
                        i_awdContext,
                        l_serviceName,
                        new MyMonitorL(l_rswService)
                        );
                l_rswService.setServiceMonitor(l_serviceMonitor);
                l_rswService.setFailoverServiceManager(this);
                try
                {
                    l_serviceMonitor.init(l_serviceConfig, i_configProperties);
                }
                catch (AwdException l_awdE)
                {
                    // Skip service
                    l_initSuccess = false;
                }
                if(l_initSuccess)
                {
                    addService(l_rswService);
                    checkLocalServiceStatus(l_rswService);
                    checkRemoteServiceStatus(l_rswService);
                    if("DOWN".equals(l_rswService.getLocalStateString()))
                    {
                        if("DOWN".equals(l_rswService.getRemoteStateString()) &&
                                l_rswService.isHomeForService())
                        {
                            // Down locally and remotely, and we are the home, "failover"
                            // to local
                            i_auditLogger.logMessage(new LoggableEvent(
                                    ">>>> " + l_serviceName + " both local and remote VIP DOWN, " +
                                    "this is service's home, starting service locally " +
                                    "(by failing over to local).",
                                    LoggableInterface.C_SEVERITY_INFO));
                            failoverServiceFromRemoteToLocalServer(l_rswService);
                        }
                        else
                        {
                            // Down locally, start monitoring on remote server
                            i_auditLogger.logMessage(new LoggableEvent(
                                    ">>>> " + l_serviceName +
                                    " local VIP DOWN, remote VIP either UP or unkown, " +
                                    "either way starting to monitor.",
                                    LoggableInterface.C_SEVERITY_INFO));
                            l_rswService.startMonitoring();                        
                        }
                    }
                    //i_instrumentManager.registerObject(
                    //        new ServiceInstrumentor(l_rswService));
                    i_auditLogger.logMessage(new LoggableEvent(
                            ">>>> " + l_serviceName + " - created service entry " +
                            "[" + l_rswService + "]",
                            LoggableInterface.C_SEVERITY_INFO));
                } // End if success initializing service
                        
            }            
        }
        
    }

    /**
     * Checks the status of all services.
     */
    private void checkAllServicesStatus()
    {
        RswService              l_rswServiceARR[];
        RswService              l_rswService;
        int                     l_loop;
        
        l_rswServiceARR = getServiceArray();
        for(l_loop = 0; l_loop < l_rswServiceARR.length; l_loop++)
        {
            l_rswService = l_rswServiceARR[l_loop];
            checkServiceStatus(l_rswService);
            
            // Check for UP, UP which would imply IP address conflict
            // which must be resolved - home should failover service
            // which should get it back to the status quo.
            if(l_rswService.isHomeForService())
            {
                if(     (l_rswService.getServiceHomeState() == RswService.C_STATE_UP) &&
                        (l_rswService.getServiceStandbyState() == RswService.C_STATE_UP))
                {
                    // UP and UP and I'm the home, so try to failover (to here - the home)
                    // TODO 2 Log error and alarm!
                    System.err.println("!!!!\n!!!! SERIOUS - Service " +
                            l_rswService.getServiceName() + " is up on both!!!!" +
                            " Failing it over to the home.\n!!!!");
                    failoverServiceFromRemoteToLocalServer(l_rswService);
                }
            }
        }
    }

    /**
     * Checks the status of the supplied service.
     * @param p_rswService      service to check status of.
     */
    private void checkServiceStatus(RswService p_rswService)
    {
        checkLocalServiceStatus(p_rswService);
        checkRemoteServiceStatus(p_rswService);
    }

    /**
     * Adds the service to the set of services being managed (monitored etc).
     * @param p_rswService      service to manage (monitor etc).
     */
    private void addService(RswService p_rswService)
    {
        i_rswServiceMap.put(p_rswService.getServiceName(), p_rswService);
        i_rswServiceCachedArrayInvalidated = true;
    }

    /**
     * Returns a cached array snapshot of the services being managed.
     * @return                  cached array snapshot of the services being managed.
     */
    private RswService[] getServiceArray()
    {
        if(i_rswServiceCachedArrayInvalidated)
        {
            i_rswServiceCachedARR = (RswService[])i_rswServiceMap.values().
                    toArray(C_RSW_SERVICE_EMPTY_ARRAY);
        }
        
        return i_rswServiceCachedARR;
    }

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_checkLocalServiceStatus  = "checkLocalServiceStatus";
    
    /**
     * Checks the services local status (local as in on this server(node)).
     * @param p_service         service to check status of.
     */
    private void checkLocalServiceStatus(
        RswService              p_service
        )
    {
        String                  l_result;
        
        l_result = i_watchdogPlatform.performOsCommandWithResult(
                p_service.getCheckLocalServiceVipStateCommand());
        // TODO 2 Make VIP status check config REGEX?
        synchronized (this)
        {
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_APPLICATION_DATA | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 30100, this,
                    C_METHOD_checkLocalServiceStatus + ": Check returned=" + l_result);
            if("UP".equals(l_result))
            {
                p_service.setState(i_nodeName, RswService.C_STATE_UP);
                
                // TODO 2 If up and monitoring, stop monitoring as on local node?
            }
            else if("DOWN".equals(l_result))
            {
                p_service.setState(i_nodeName, RswService.C_STATE_DOWN);
            }
            else
            {
                p_service.setState(i_nodeName, RswService.C_STATE_UNKNOWN);
            }
        }
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_APPLICATION_DATA | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 30190, this,
                C_METHOD_checkLocalServiceStatus + ": Afte check, service=" + p_service);
    } // End of private method checkLocalServiceStatus

    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_checkRemoteServiceStatus  = "checkRemoteServiceStatus";

    /**
     * Checks the services remote status (remote as in on remote server(node)). Uses
     * the RSWP protocol to communicate with the remote server's rswdog.
     * @param p_service         service to check status of.
     */
    private void checkRemoteServiceStatus(
        RswService              p_service
        )
    {
        String                  l_result;
        SimpleMessage           l_smpMsg;
        SimpleMessage           l_response;
        String                  l_remoteServer;
        RswControlClient        l_controlClient;
        
        l_remoteServer = p_service.getRemoteServerName();
                
        // TODO 2 Should this be part of the confgured actions? Or 2 sets of conf actions based on success/failure
        // TODO 2 Should this be moved down into the RswControlClient (as a method on it)?
        l_smpMsg = new SimpleMessage("RSWP", "v001");
        l_smpMsg.addField("type", "STATUS_SERVICE_LOCAL_IP_REQ");
        l_smpMsg.addField("service", p_service.getServiceName());
        l_smpMsg.addField("ipAddress", "not-specified");

        l_controlClient = (RswControlClient)i_controlClientMap.get(l_remoteServer);
        if(l_controlClient != null)
        {
            l_response = l_controlClient.sendRequest(l_smpMsg);
            if(l_response != null)
            {
                l_result = l_response.getField("service_status");
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_APPLICATION_DATA | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 30100, this,
                        C_METHOD_checkRemoteServiceStatus + ": Check returned=" + l_result);
                synchronized(this)
                {
                    if("UP".equals(l_result))
                    {
                        p_service.setState(l_remoteServer, RswService.C_STATE_UP);
                    }
                    else if("DOWN".equals(l_result))
                    {
                        p_service.setState(l_remoteServer, RswService.C_STATE_DOWN);
                    }
                    else
                    {
                        p_service.setState(l_remoteServer, RswService.C_STATE_UNKNOWN);
                    }
                }
                if(PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_APPLICATION_DATA | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 30190, this,
                        C_METHOD_checkRemoteServiceStatus + ": Afte check, service=" + p_service);
            }
            else
            {
                // TODO 2 Log error? Need to include reason!
                System.err.println(">>>> ERROR - could not get remote status from " +
                        l_remoteServer + ", setting to unknown");
                p_service.setState(l_remoteServer, RswService.C_STATE_UNKNOWN);                
            }
            
        }
        else
        {
            // TODO 2 Log error
            System.err.println(">>>> ERROR - no control client for service " + l_remoteServer);
        }
 
    } // End of private method checkRemoteServiceStatus

    /**
     * Fails over the service.  Can be called on either node
     * regardless of which node is currently supporting the service.
     * @param p_serviceName     service name to failover.
     * @return Result message of attempting the failover.
     */
    public String failoverService(
        String                  p_serviceName
        )
    {
        String                  l_error = "Failed - unknown reason";
        RswService              l_rswService;

        l_rswService = (RswService)i_rswServiceMap.get(p_serviceName);
        if(l_rswService != null)
        {
            // TODO 2 Handle errors so can return actual result of failover?
            failoverService(l_rswService);
            l_error = "Successfully failed over service " + p_serviceName;
        }
        
        return l_error;
    } // End of public method releaseService
    
    /**
     * Fails over the service. Can be called on either node
     * regardless of which node is currently supporting the service.
     * @param p_rswService      service to failover.
     */
    private void failoverService(
        RswService              p_rswService
        )
    {
        String                  l_serviceName;
        SimpleMessage           l_releaseServiceSmpMsg;
        RswControlClient        l_controlClient;
        
        l_serviceName = p_rswService.getServiceName();
        if("UP".equals(p_rswService.getLocalStateString()))
        {
            // Service VIP is up locally, so send message to remote
            //node to perform failover (i.e. to take on service).
            l_releaseServiceSmpMsg = new SimpleMessage("RSWP", "v001");
            l_releaseServiceSmpMsg.addField("type", "FAILOVER_SERVICE_REQ");
            l_releaseServiceSmpMsg.addField("service", l_serviceName);
            l_releaseServiceSmpMsg.addField("ipAddress", "not-specified");
            l_releaseServiceSmpMsg.addField("master", "false"); // This is the standby, not the master!

            // Try to send failover request to other node.
            l_controlClient = (RswControlClient)i_controlClientMap.get(p_rswService.getRemoteServerName());

            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + l_serviceName + " Sending FAIlOVER_SERVICE_REQ [" +
                    l_releaseServiceSmpMsg + "]",
                    LoggableInterface.C_SEVERITY_INFO));

            l_controlClient.sendMessage(l_releaseServiceSmpMsg);
        }
        else
        {
            failoverServiceFromRemoteToLocalServer(p_rswService);
        }
    }

    /**
     * Fails over all services TO this node.
     * @return Result message of attempting the failover.
     */
    public String failoverAllServicesToThisServer()
    {
        
        return failoverAllServicesToServer(true);
    } // End of public method releaseService

    /**
     * Fails over all services FROM this node.
     * @return Result message of attempting the failover.
     */
    public String failoverAllServicesFromThisServer()
    {
        
        return failoverAllServicesToServer(false);
    } // End of public method releaseService

    /**
     * Fails over all services from / to this server.
     * @param p_toThisServer      if <code>true</code> failover TO this server,
     *                          if <code>false</code> failover FROM this server.
     * @return Message indicating which services failed over.
     */
    private String failoverAllServicesToServer(boolean p_toThisServer)
    {
        StringBuffer            l_messageSB;
        RswService              l_rswServiceARR[];
        RswService              l_rswService;
        int                     l_loop;
        int                     l_failoverCount = 0;
        
        l_messageSB = new StringBuffer(160);
        l_rswServiceARR = getServiceArray();
        l_messageSB.append("Starting.");
        for(l_loop = 0; l_loop < l_rswServiceARR.length; l_loop++)
        {
            l_rswService = l_rswServiceARR[l_loop];
            if(p_toThisServer)
            {
                if(l_rswService.getLocalState() == RswService.C_STATE_DOWN)
                {
                    failoverServiceFromRemoteToLocalServer(l_rswService);
                    l_failoverCount++;
                    l_messageSB.append("Failed service ");
                    l_messageSB.append(l_rswService.getServiceName());
                    l_messageSB.append(" TO this node (" + i_nodeName + ").\n");
                }
            }
            else
            {
                if(l_rswService.getLocalState() == RswService.C_STATE_UP)
                {
                    failoverService(l_rswService);
                    l_failoverCount++;
                    l_messageSB.append("Failed service ");
                    l_messageSB.append(l_rswService.getServiceName());
                    l_messageSB.append(" FROM this node (" + i_nodeName + ").\n");
                }                
            }
        }
        if(l_failoverCount == 0)
        {
            l_messageSB.append("No services needed to be failed over.");
        }
        l_messageSB.append("Finished.");
        
        return l_messageSB.toString();
    }

    /**
     * Attempts to send a release message to remote rswdog (on server where
     * service appears to have failed).
     * Stops monitoring the service (we don't want to monitor services
     * running on the local server - the remote rswdog will take on the monitoring
     * of the service either as a result of the release service RSWP message it
     * was sent or when the rswdog is running again).
     * Then performs configured actions
     * to bring up the service on the local server.
     * @param p_serviceName     service which should be failed over.
     * @return Result message of attempting the failover.
     */
    private String failoverServiceFromRemoteToLocalServer(
        String                  p_serviceName
        )
    {
        String                  l_error = "Failed - unknown reason";
        RswService              l_rswService;

        l_rswService = (RswService)i_rswServiceMap.get(p_serviceName);
        if(l_rswService != null)
        {
            // TODO 2 Handle errors so can return actual result of failover?
            failoverServiceFromRemoteToLocalServer(l_rswService);
            l_error = "Successfully failed over service " + p_serviceName;
        }
        
        return l_error;
        
    }
    
    /** Used in calls to middleware. Value is {@value}.*/
    private static final String C_METHOD_failoverServiceFromRemoteToLocalServer  =
            "failoverServiceFromRemoteToLocalServer";

    /**
     * Attempts to send a release message to remote rswdog (on server where
     * service appears to have failed).
     * Stops monitoring the service (we don't want to monitor services
     * running on the local server - the remote rswdog will take on the monitoring
     * of the service either as a result of the release service RSWP message it
     * was sent or when the rswdog is running again).
     * Then performs configured actions
     * to bring up the service on the local server.
     * @param p_rswService      service which should be failed over.  
     */
    private void failoverServiceFromRemoteToLocalServer(
        RswService              p_rswService
        )
    {
        String                  l_serviceName;
        SimpleMessage           l_releaseServiceSmpMsg;
        SimpleMessage           l_response;
        String                  l_actionGroupName;
        RswControlClient        l_controlClient;

        l_serviceName = p_rswService.getServiceName();
        
        // TODO 2 Should this be part of the confgured actions? Or 2 sets of conf actions based on success/failure
        l_releaseServiceSmpMsg = new SimpleMessage("RSWP", "v001");
        l_releaseServiceSmpMsg.addField("type", "RELEASE_SERVICE_IP_REQ");
        l_releaseServiceSmpMsg.addField("service", l_serviceName);
        l_releaseServiceSmpMsg.addField("ipAddress", "not-specified");
        l_releaseServiceSmpMsg.addField("master", "false"); // This is the standby, not the master!

        i_auditLogger.logMessage(new LoggableEvent(
                ">>>> " + l_serviceName + " Sending RELEASE_SERVICE_IP_REQ [" + l_releaseServiceSmpMsg + "]",
                LoggableInterface.C_SEVERITY_INFO));

        //try
        //{

        // Try to send release to other node.
        l_controlClient = (RswControlClient)i_controlClientMap.get(p_rswService.getRemoteServerName());
        l_response = l_controlClient.sendRequest(l_releaseServiceSmpMsg);
        if(l_response == null)
        {
            // Unkown result, update status to UNKOWN
            p_rswService.setState(p_rswService.getRemoteServerName(), RswService.C_STATE_UNKNOWN);
        }
        else
        {
            if(     "RELEASE_SERVICE_IP_RSP".equals(l_response.getField("type")) &&
                    "SUCCESS".equals(l_response.getField("status")))
            {
                if("DOWN".equals(l_response.getField("service_status")))
                {
                    // Succeeded, update status to DOWN
                    p_rswService.setState(
                            p_rswService.getRemoteServerName(),
                            RswService.C_STATE_DOWN);
                }
                else if("UP".equals(l_response.getField("service_status")))
                {
                    // Failed, update status to UP
                    // TODO 2 Shouldn't happen, don't bring up VIP here!
                    p_rswService.setState(
                            p_rswService.getRemoteServerName(),
                            RswService.C_STATE_UP);                    
                }
                else
                {
                    // Unrecognised status, update status to UNKOWN
                    // TODO 3 Log error and continue!
                    System.out.println("Unknown SMP response state " + l_response.getField("service_status"));
                    p_rswService.setState(
                            p_rswService.getRemoteServerName(),
                            RswService.C_STATE_UNKNOWN);                                                
                }
            }
            else
            {
                // Failed, update status to UNKOWN
                p_rswService.setState(
                        p_rswService.getRemoteServerName(),
                        RswService.C_STATE_UNKNOWN);
            }
        }
        //}
        //catch (IOException l_e)
        //{
        //    l_e.printStackTrace();
        //    // We tried, carry on with actions.
        //}
        
        l_actionGroupName = p_rswService.getEnteredFailedStateWatchdogActionGroupName(); 

        if(l_actionGroupName != null &&
                !"".equals(l_actionGroupName))
        {
            i_rswActionProcessor.performActions(i_watchdogActionGroupConfig.getWatchdogActionGroup(
                    l_actionGroupName));
        }
        
        // Remote service failed, brought up locally, so stop monitoring it.
        // And to be sure upto date, also check the localStatus.
        i_auditLogger.logMessage(new LoggableEvent(
                ">>>> " + l_serviceName + " Stopping monitoring.",
                LoggableInterface.C_SEVERITY_INFO));
        p_rswService.stopMonitoring();
        checkLocalServiceStatus(p_rswService);
        p_rswService.failoverPerformed();

        i_auditLogger.logMessage(new LoggableEvent(
                ">>>> " + l_serviceName + " Failover complete [" + p_rswService + "]",
                LoggableInterface.C_SEVERITY_INFO));
        
        i_logger.logMessage(new AwdException(
                C_CLASS_NAME,
                C_METHOD_failoverServiceFromRemoteToLocalServer,
                31234,
                this,
                (PpasRequest)null,
                (long)0,
                AwdKey.get().failedOverService(l_serviceName, i_nodeName)));
        
        //i_watchdogPlatform.performOsCommand("./hasEnteredFailedState.sh");
        
        // TODO 2 Stop monitoring service (its on this node). Restart if asked
        // to release service (but prevent ping-ponging!).
    }

    /**
     * Releases the named service. This performs all the steps necessary to release the service
     * on this sever, for example so that it can be taken over by another server.
     * 
     * @param p_serviceName     name of service to release.
     * @param p_ipAddress       VIP address of service to release TODO 2 Take out or implement
     * @param p_master          TODO 2 take out or implement and tidy up
     * @return                  <code>null</code> if successfull, error message otherwise.
     */
    private String releaseService(
        String                  p_serviceName,
        String                  p_ipAddress,
        boolean                 p_master
        )
    {
        String                  l_error = "Failed - unknown reason";
        RswService              l_rswService;
        String                  l_releaseServiceWatchdogActionGroupName;
        WatchdogActionGroup     l_watchdogActionGroup;

        l_rswService = (RswService)i_rswServiceMap.get(p_serviceName);
        if(l_rswService != null)
        {
            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + p_serviceName + " Releasing service [" +
                    l_rswService + "]",
                    LoggableInterface.C_SEVERITY_INFO));

            l_releaseServiceWatchdogActionGroupName =
                    l_rswService.getReleaseServiceWatchdogActionGroupName();
            l_watchdogActionGroup = i_watchdogActionGroupConfig.getWatchdogActionGroup(
                    l_releaseServiceWatchdogActionGroupName);
            if(l_watchdogActionGroup != null)
            {
                i_rswActionProcessor.performActions(l_watchdogActionGroup);
                l_error = null;
            }
            else
            {
                // TODO 2 log error
                l_error = "Message error - request to release IP for known sevice " +
                        p_serviceName + ", but no release action group configured.";
                System.err.println(l_error);                
            }
            
            // Service released, so should be starting on other node -
            // to be sure, check (update) status on local node and
            // start monitoring (monitoring will
            // be delayed).
            checkLocalServiceStatus(l_rswService);
            // TODO - handle timing issue and also prevent ping ponging
            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + p_serviceName + " Starting to monitor.",
                    LoggableInterface.C_SEVERITY_INFO));
            l_rswService.startMonitoring();
            l_rswService.failoverPerformed();

            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + p_serviceName + " Released service [" +
                    l_rswService + "]",
                    LoggableInterface.C_SEVERITY_INFO));
            
        }
        else
        {
            // TODO 2 log error
            l_error = "Message error - request to release IP for unknown sevice " +
                p_serviceName;
            System.err.println(l_error);
        }
        
        return l_error;
    } // End of private method releaseService
    
    /**
     * Utility method to sleep a thread in a way that can be woken neatly (using notify if
     * ever necessary).
     * @param p_sleepMillis     milliseconds for thread to sleep.
     */
    private void sleep(
        long                    p_sleepMillis
    )
    {
        try
        {
            synchronized(this)
            {
                wait(p_sleepMillis);
            }
        }
        catch(InterruptedException l_e)
        {
            // Ignore
        }
    }

    /**
     * Private inner class implementing the status checking thread which
     * maintains the view of the status of services by polling the local
     * and remote status of the services periodically.
     */
    private class StatusThread extends ThreadObject
    {

        /** Date format for Debug output. TODO 3 Date is already in debug - take out? */
        private SimpleDateFormat i_debugDf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
        
        /**
         * Performs the main processing of the thread -
         * namely implementing the status checking which
         * maintains the view of the status of services by polling the local
         * and remote status of the services periodically
         */
        public void doRun()
        {
            long                l_startTime;
            long                l_endTime;
            long                l_sleepTime;
            
            if(PpasDebug.on)
            {
                PpasDebug.print(
                        PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START, C_CLASS_NAME, 20100, this,
                        "doRun: Status thread running.");
            }

            while(!isStopping())
            {
                l_startTime = System.currentTimeMillis();

                if(PpasDebug.on)
                {
                    PpasDebug.print(
                            PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_TRACE, C_CLASS_NAME, 20120, this,
                            "doRun: Starting status check of all services " +
                            i_debugDf.format(new Date(l_startTime)));
                }
                
                checkAllServicesStatus();
                l_endTime = System.currentTimeMillis();

                // Try to keep the interval accurate, no matter the length of
                // time to perform checks.                
                l_sleepTime = l_startTime + i_statusCheckIntervalMillis -
                        System.currentTimeMillis();

                if(PpasDebug.on)
                {
                    PpasDebug.print(
                            PpasDebug.C_LVL_LOW, PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_TRACE, C_CLASS_NAME, 20130, this,
                            "doRun: Finished statuc checking of all services at " +
                            i_debugDf.format(new Date(l_endTime)) +
                            " (took " + (l_endTime - l_startTime) + " millis). Sleeping for " +
                            l_sleepTime + " millis.");
                }

                if(l_sleepTime > 0)
                {
                    sleep(l_sleepTime);
                }
            }
        } // End of public method doRun
        
        /**
         * Returns the unique name of the thread.
         * @return The unique name of the thread.
         */
        public String getThreadName()
        {
            return ("WDogThd");
        }
        
    } // End of private inner class StatusThread


    /**
     * Private inner class which implements the monitor listener used to listen
     * for monitors entering the failed state or entering the passed state.
     * There is one listener for each configured service (listening to the
     * service's monitor).
     * <p/>
     * If the monitor enters the failed state, it tries to send a release service RSWP
     * message to the rswdog on the server which currently is running the service
     * which has just been detected as failing. 
     * Stops monitoring the service (we don't want to monitor services
     * running on the local server - the remote rswdog will take on the monitoring
     * of the service either as a result of the release service RSWP message it
     * was sent or when the rswdog is running again).
     * Finally it performs the configured actions
     * which should include the actions to bring up the service on the local server.
     */
    private class MyMonitorL implements WatchdogMonitorListener
    {
       
        /** The service being monitored. */
        private RswService        i_rswService;
        
        /** The name of the service being monitored. Convenience - got from the service. */
        private String            i_monitoredServiceName;

        /**
         * Creates a new monitor listener for the service.
         * @param p_rswService  service to listen to the monitor of.
         */
        public MyMonitorL(
            RswService          p_rswService
            )
        {
            i_rswService = p_rswService;
            i_monitoredServiceName = p_rswService.getServiceName();
        }

        /**
         * Monitor has passed.
         */
        public void passed()
        {
            // Currently nothing to do.
        }

        /**
         * Monitor has failed.
         */
        public void failed()
        {
            long                l_currentTimeMillis;
            String              l_message;

            l_currentTimeMillis = System.currentTimeMillis();
            if(l_currentTimeMillis >
                    (i_rswService.getLastFailoverPerformedTime() +
                            i_rswService.getMinimumMillisBetweenAutoFailovers()))
            {
                l_message = ">>>> " + i_monitoredServiceName +
                        " has FAILED, FAILING OVER to local node (currentTime=" + 
                        l_currentTimeMillis + ", lastFailoverTime=" +
                        i_rswService.getLastFailoverPerformedTime()
                        + ", minTimeAllowedBetween=" + i_rswService.getMinimumMillisBetweenAutoFailovers()
                        + ")";
                System.out.println(l_message);
                i_auditLogger.logMessage(new LoggableEvent(l_message,
                        LoggableInterface.C_SEVERITY_INFO));
                failoverServiceFromRemoteToLocalServer(i_rswService);
            }
            else
            {
                l_message = ">>>> " + i_monitoredServiceName +
                        " has FAILED, NOT FAILING OVER yet as still too soon since last failover (currentTime=" + 
                        l_currentTimeMillis + ", lastFailoverTime=" +
                        i_rswService.getLastFailoverPerformedTime()
                        + ", minTimeAllowedBetween=" + i_rswService.getMinimumMillisBetweenAutoFailovers()
                        + ", millisRemaining=" + (i_rswService.getLastFailoverPerformedTime() +
                                i_rswService.getMinimumMillisBetweenAutoFailovers() - l_currentTimeMillis)
                        + ")";
                System.out.println(l_message);
                i_auditLogger.logMessage(new LoggableEvent(l_message,
                        LoggableInterface.C_SEVERITY_INFO));                

            }
        }

        /**
         * Attempts to send a release message to remote rswdog (on server where
         * service appears to have failed).
         * Stops monitoring the service (we don't want to monitor services
         * running on the local server - the remote rswdog will take on the monitoring
         * of the service either as a result of the release service RSWP message it
         * was sent or when the rswdog is running again).
         * Then performs configured actions
         * to bring up the service on the local server.  
         */
        public void hasEnteredFailedState()
        {
            long                l_currentTimeMillis;
            String              l_message;
            System.out.println(">>>> " + i_monitoredServiceName + " Entered failed state.");

            l_currentTimeMillis = System.currentTimeMillis();
            if(l_currentTimeMillis >
                    (i_rswService.getLastFailoverPerformedTime() +
                            i_rswService.getMinimumMillisBetweenAutoFailovers()))
            {
                l_message = ">>>> " + i_monitoredServiceName +
                        " has entered FAILED state, FAILING OVER to local node (currentTime=" + 
                        l_currentTimeMillis + ", lastFailoverTime=" +
                        i_rswService.getLastFailoverPerformedTime()
                        + ", minTimeAllowedBetween=" + i_rswService.getMinimumMillisBetweenAutoFailovers()
                        + ")";
                System.out.println(l_message);
                i_auditLogger.logMessage(new LoggableEvent(l_message,
                        LoggableInterface.C_SEVERITY_INFO));
                failoverServiceFromRemoteToLocalServer(i_rswService);
            }
            else
            {
                l_message = ">>>> " + i_monitoredServiceName +
                        " has entered FAILED state, NOT FAILING OVER yet as still too soon " +
                        "since last failover, continuing to monitor (currentTime=" + 
                        l_currentTimeMillis + ", lastFailoverTime=" +
                        i_rswService.getLastFailoverPerformedTime()
                        + ", minTimeAllowedBetween=" + i_rswService.getMinimumMillisBetweenAutoFailovers()
                        + ", millisRemaining=" + (i_rswService.getLastFailoverPerformedTime() +
                                i_rswService.getMinimumMillisBetweenAutoFailovers() - l_currentTimeMillis)
                        + ")";
                System.out.println(l_message);
                i_auditLogger.logMessage(new LoggableEvent(l_message,
                        LoggableInterface.C_SEVERITY_INFO));                
            }
        }
        
        /**
         * Performs configured actions - typically none or a simple log or alarm.
         */
        public void hasEnteredPassedState()
        {
            String              l_actionGroupName;
            System.out.println(">>>> " + i_rswService.getServiceName() + " Entered passed state.");

            // TODO 2 Support auto failback (but protect against ping-pong)

            l_actionGroupName = i_rswService.getEnteredPassedStateWatchdogActionGroupName(); 
            if( l_actionGroupName != null &&
                    !"".equals(l_actionGroupName))
            {
                i_rswActionProcessor.performActions(i_watchdogActionGroupConfig.getWatchdogActionGroup(
                        l_actionGroupName));
            }
            //i_watchdogPlatform.performOsCommand("./hasEnteredPassedState.sh");
        }

    }

    /**
     * Private inner class implementing the SMP messgae handler for RSWP.
     */
    private class RswpMessageHandler implements SimpleMessageProtocolMessageHandler
    {
        
        /**
         * Handles an RSWP message (RSWP is built on SMP).
         * @param p_source      source SMP socket end point message was received on.
         * @param p_message     the RSWP (SMP) message.
         */
        public void handleMessage(
            SimpleMessageProtocolSocketEndPoint p_source,
            SimpleMessage       p_message)
        {
            String              l_type;
            SimpleMessage       l_response = null;

            l_type = p_message.getField("type");
            if("RELEASE_SERVICE_IP_REQ".equals(l_type))
            {
                l_response = handleReleaseServiceIpMessage(p_message);
            }
            else if("FAILOVER_SERVICE_REQ".equals(l_type))
            {
                l_response = handleFailoverServiceMessage(p_message);
            }
            else if("STATUS_SERVICE_LOCAL_IP_REQ".equals(l_type))
            {
                l_response = handleStatusServiceLocalIpMessage(p_message);
            }
            else
            {
                // TODO 2 Log something?
            }
            if(l_response != null)
            {
                try
                {
                    p_source.sendResponse(l_response, p_message);
                }
                catch(SmpIoException l_e)
                {
                    l_e.printStackTrace();                    
                    // Can't do any more, continue (return null).
                }
            }
            
        }

        /**
         * Handles a failover service RSWP request message (RSWP is built on SMP).
         * @param p_message     the RSWP (SMP) failover request message.
         * @return              the RSWP (SMP) failover response (result) message to send.
         */
        private SimpleMessage handleFailoverServiceMessage(
            SimpleMessage       p_message)
        {
            String              l_serviceName;
            SimpleMessage       l_response;
           
            l_serviceName = p_message.getField("service");            
            
            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + l_serviceName +
                    " Received FAILOVER_SERVICE_REQ message [" +
                    p_message + "]",
                    LoggableInterface.C_SEVERITY_INFO));

            failoverServiceFromRemoteToLocalServer(l_serviceName);

            //TODO 2 Handle failure and send error
            // TODO 1 This RSWP and SMP is a bit of a mess - its half asynch and then partly synch as some messages cause new requests from the other server - sort out!
            // Currently hacked to hopefully hang together - don't return a response any more.
            l_response = null;
            /*
            l_response = new SimpleMessage("RSWP", "v001");
            l_response.addField("type","FAILOVER_SERVICE_RSP");
            l_response.addField("status","SUCCESS");
            l_response.addField("service", l_serviceName);
            l_response.addField("status_text","Successfully failed over service");
            */
            
            return l_response;
        }
        
        /**
         * Handles a release service IP RSWP request message (RSWP is built on SMP).
         * @param p_message     the RSWP (SMP) release request message.
         * @return              the RSWP (SMP) release response (result) message to send.
         */
        private SimpleMessage handleReleaseServiceIpMessage(
            SimpleMessage       p_message)
        {
            String              l_serviceName;
            String              l_ipAddress;
            String              l_master;
            boolean             l_isMaster = false;
            SimpleMessage       l_response;
           
            l_serviceName = p_message.getField("service");
            l_ipAddress = p_message.getField("ipAddress");
            l_master = p_message.getField("master");
            if("true".equals(l_master))
            {
                l_isMaster = true;
            }
            
            i_auditLogger.logMessage(new LoggableEvent(
                    ">>>> " + l_serviceName +
                    " Received RELEASE_SERVICE_IP_REQ message [" +
                    p_message + "]",
                    LoggableInterface.C_SEVERITY_INFO));
            
            releaseService(l_serviceName, l_ipAddress, l_isMaster);

            //TODO 2 Handle failure and send error
            l_response = new SimpleMessage("RSWP", "v001");
            l_response.addField("type","RELEASE_SERVICE_IP_RSP");
            l_response.addField("status","SUCCESS");
            l_response.addField("service", l_serviceName);
            l_response.addField("service_status",
                    ((RswService)i_rswServiceMap.get(l_serviceName)).getLocalStateString());
            l_response.addField("status_text","Successfully released service");
            
            return l_response;
        }
        
        /**
         * Handles a status service local IP RSWP request message (RSWP is built on SMP).
         * @param p_message     the RSWP (SMP) status request message.
         * @return              the RSWP (SMP) status response (result) message to send.
         */
        private SimpleMessage handleStatusServiceLocalIpMessage(
            SimpleMessage       p_message)
        {
            String              l_serviceName;
            SimpleMessage       l_response;
            RswService          l_service;
           
            l_serviceName = p_message.getField("service");
            
            l_service = (RswService)i_rswServiceMap.get(l_serviceName);
            if(l_service != null)
            {
                l_response = new SimpleMessage("RSWP", "v001");
                l_response.addField("type","STATUS_SERVICE_LOCAL_IP_RSP");
                l_response.addField("status","SUCCESS");
                l_response.addField("status_text","Success");
                l_response.addField("service", l_serviceName);
                l_response.addField("service_status", l_service.getLocalStateString());
                
            }
            else
            {
                l_response = new SimpleMessage("RSWP", "v001");
                l_response.addField("type","STATUS_SERVICE_LOCAL_IP_RSP");
                l_response.addField("status","FAILED");
                l_response.addField("status_text","Unkown service " + l_serviceName);                
                l_response.addField("service", l_serviceName);
            }

            return l_response;
        }
    } // End of private inner class MySmpMessageHandler


    /**
     * Public inner class which implements an instrumented object for the
     * Remote Service Watchdog Processor.
     */
    public class ProcessorInstrumentor implements InstrumentedObjectInterface
    {
        /** Instrument set. */
        private InstrumentSet   i_instrumentSet;

        //------------------------------------------------------------------------
        // Public methods
        //------------------------------------------------------------------------
        
        /**
         * Creates a new Processor Instrumentor.
         */
        public ProcessorInstrumentor()
        {
            i_instrumentSet = new InstrumentSet("root");
        }

        /**
         * Gets the the instrument set of objects. These are counters, meters etc. 
         * @return InstrumentSet the set of instruments
         */
        public InstrumentSet getInstrumentSet()
        {
            return i_instrumentSet;
        }
        
        /**
         * Gets the name of the object (for example for Instrumented Object).
         * @return String the name of the object
         */
        public String getName()
        {
            return i_processName;
        }
        

        /**
         * Returns the status of the processor, namely a summary list of services and their statuses.
         * @return Textual description of the status of the processor.
         */
        public String mngMthGetStatus()
        {
            String             l_listServices;
        
            // TODO 3 Tidy up (use own A&I JSP or use properties to make into pre text box)?
            l_listServices = listServicesHtml();
            //l_listServices = "<pre>" + l_listServices + "</pre>";
            return l_listServices;
        }
        
        /**
         * Fails over all services TO the local server.
         * @return Message describing services failed over.
         */
        public String mngMthFailAllToThisServer()
        {
            return failoverAllServicesToThisServer();
        }

        /**
         * Fails over all services FROM the local server.
         * @return Message describing services failed over.
         */
        public String mngMthFailAllFromThisServer()
        {
            return failoverAllServicesFromThisServer();
        }

    } // End of public class ProcessorInstrumentor

/*
    public class ServiceInstrumentor implements InstrumentedObjectInterface
    {
        private RswService      i_rswService;
        private InstrumentSet   i_instrumentSet;

        //------------------------------------------------------------------------
        // Public methods
        //------------------------------------------------------------------------
        
        public ServiceInstrumentor(RswService p_rswService)
        {
            i_rswService = p_rswService;
            i_instrumentSet = new InstrumentSet("root");
        }

        / ** Gets the the instrument set of objects. These are counters, meters etc. 
         * @return InstrumentSet the set of instruments
         * /
        public InstrumentSet getInstrumentSet()
        {
            return i_instrumentSet;
        }
        
        / ** Gets the name of the object registered.
         * @return String the name of the object
         * /
        public String getName()
        {
            return i_rswService.getServiceName();
        }
             
        public String mngMthGetStatus()
        {
            // TODO 3 Tidy up (use own A&I JSP or use properties to make into pre text box)?

            return "<pre>" + i_rswService.listService() + "</pre>";
        }
        
        public String mngMthFailoverService()
        {
            return failoverService(i_rswService.getServiceName());
        }
        
        public String toString()
        {
            return i_rswService.toString();
        }

    } // End of public class ServiceInstrumentor
*/

} // End of public class RemoteServiceWatchdogProcessor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
