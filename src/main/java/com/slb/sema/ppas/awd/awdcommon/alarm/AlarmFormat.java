////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmDestination.java
//      DATE            :       12-Dec-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Abstract super class for alarm formats
//                              which hold the details of an alarm format.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.awdcommon.alarm;

/**
 * Abstract super class for alarm formats which hold the details of an
 * alarm format.
 */
public abstract class AlarmFormat
{

    /** Default text format. Value is {@value}*/
    public static final String  C_TYPE_DEFAULT_TEXT = "FORMAT_DEFAULT_TEXT";

    /** Template text format. Value is {@value}*/
    public static final String  C_TYPE_TEMPLATE_TEXT = "FORMAT_TEMPLATE_TEXT";

    /** Type of destination - one of the C_TYPE constants. */
    private final String        i_typeName;
    
    /**
     * Creates a new Alarm Destination.
     * @param p_typeName        type of format - one of the C_TYPE
     *                          constants.
     */
    public AlarmFormat(
        String                  p_typeName
    )
    {
        i_typeName = p_typeName;
    }
     
    /**
     * Returns the type of format (one of the C_TYPE contants).
     * @return The type of format (one of the C_TYPE contants).
     */
    public String getFormatTypeName()
    {
        return i_typeName;
    }
        
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace. 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return("AlarmFormat=[type=" + i_typeName + "]");
    }

} // End of public class AlarmFormat.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////

