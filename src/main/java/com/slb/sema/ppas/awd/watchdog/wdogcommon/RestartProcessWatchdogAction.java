////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RestartProcessWatchdogAction.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This defines a Watchdog Action to restart an
//                              ASCS process.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.wdogcommon;

/**
 * This defines a Watchdog Action to restart an ASCS process.
 */
public class RestartProcessWatchdogAction extends WatchdogAction
{
    // Currently no additional details required, so this simply acts as a marker
    // class.

    /**
     * Creates a new Restart Process Watchdog Action.
     */
    public RestartProcessWatchdogAction()
    {
        super();
    }

    /**
     * Creates a new Restart Process Watchdog Action.
     * 
     * @param p_maxActionCount          maximum number of times action should
     *                                  be performed for a process
     *                                  within the action count lifetime. -1
     *                                  means there is no maximum.
     * @param p_actionCountLifetimeMillis lifetime over which maximum number
     *                                  of actions should not be exceeded
     *                                  for a process. -1 means infinite lifetime.
     */
    public RestartProcessWatchdogAction(
        int                     p_maxActionCount,
        long                    p_actionCountLifetimeMillis
    )
    {
        super(p_maxActionCount, p_actionCountLifetimeMillis);
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return "RestartProcessWatchdogAction=[" +
                super.toString() + "]";
    }

} // End of public class RestartProcessWatchdogAction

////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////