/*
 * Filename        : AwdPlatform.java
 * Created on      : 02-Jul-2004
 * Author          : MVonka
 * 
 * Description     :
 * 
 * Copyright       : Copyright Marek Vonka 2004, all rights reserved.
 */
package com.slb.sema.ppas.awd.awdcommon.platform;

import com.slb.sema.ppas.common.platform.Platform;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.TestFactory;

/**
 * @author MVonka
 * This...
 */
public class AwdPlatform
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "HeartbeatServer";
            
    protected Platform          i_platform;
    protected Logger            i_logger;

    public AwdPlatform(Logger p_logger)
    {
        super();

        i_logger = p_logger;
        i_platform = Platform.getThisPlatform(i_logger);
        
    }
    
    public int writeToUsersTerminals(
        String                  p_username,
        String                  p_message
    )
    {
        return i_platform.writeToUsersTerminals(p_username, p_message);
    }

    public static void main(String[] args)
    throws Exception
    {
        AwdPlatform l_platform;
        //Process l_process;
        Logger l_logger;
        
        l_logger = TestFactory.getTestFileLogger();
        
        l_platform = new AwdPlatform(l_logger);
        
        //l_process = l_platform.runCommandPublic(args[0]);
        
        //l_platform.displayProcessOutput(l_process);

        //l_platform.getPlatformFileDir(File.separator + "usr" + File.separator + "usr" + File.separator);            
        l_platform.writeToUsersTerminals(args[0], args[1]);            
        
    }
        
}
