////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GroupOrWatchdogMonitor.java
//      DATE            :       09-Dec-2005
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Composite Watchdog monitor which combines a number
//                              of other monitors using a logical OR.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/13/06 | M.Vonka    | Added isRunning method.         | PpacLon#2644/10214
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Composite watchdog monitor which combines a number
 * of other monitors using a logical OR.
 * i.e. if any of its constituent monitors fails, the OR monitor
 * also fails. It does not pass again until all its constituent
 * monitors pass.
 */
public class GroupOrWatchdogMonitor implements WatchdogMonitor
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "GroupOrWatchdogMonitor";

    /** Overall state of the OR monitor. */
    private int                 i_overallState = C_STATE_NOT_RUNNING;

    /** ArrayList of WatchdogMonitorListener - listeners listening to this monitor. */
    private ArrayList           i_listenerAL;

    /** ArrayList of WatchdogMonitor - the constituent monitors that are OR'd together. */
    private ArrayList           i_monitorAL;

    /**
     * The number of currently failed monitors. That is,
     * the number of consituent monitors which are in the failed state).
     */
    private int                 i_failedMonitorCount = 0;

    /** Number of times the OR monitor has detected the passed state.  */
    public int                  i_overallPassedCounter = 0;

    /** Number of times the OR monitor has detected the failed state.  */
    public int                  i_overallFailedCounter = 0;
    
    /** Inidicates if Monitor is enabled. */
    private boolean             i_enabled = true;
    
    /** Creates a new Group OR Watchdog Monitor. */
    public GroupOrWatchdogMonitor()
    {
        this((WatchdogMonitorListener)null);
    }

    /**
     * Creates a new Group OR Watchdog Monitor.
     * @param p_listener        an initial listener to add (can be
     *                          <code>null</code>.
     */
    public GroupOrWatchdogMonitor(
        WatchdogMonitorListener p_listener
        )
    {
        i_listenerAL = new ArrayList();
        i_monitorAL = new ArrayList();
        
        if(p_listener != null)
        {
            i_listenerAL.add(p_listener);
        }
    }

    /**
     * Initializes the monitor based on the supplied configuration. Currently
     * there is no specific configuration for this monitor.
     * @param p_configParamMap  configuration to initialize from.
     * @throws Exception        exception occurred trying to initialize monitor.
     */
    public void init(Map p_configParamMap)
    throws Exception
    {
        // Nothing to do currently, but required by interface
    }

    /**
     * Adds the supplied monitor to this composite monitor.
     * @param p_monitor         monitor to add.
     */
    public synchronized void addMonitor(WatchdogMonitor p_monitor)
    {
        // TODO 2 Sort out monitor names!
        p_monitor.addMonitorListener(new MyMonitorL("MyMonitor"));
        i_monitorAL.add(p_monitor);
    }
    
    /**
     * Adds the supplied monitor listener to this monitor.
     * @param p_listener        listener to add.
     */
    public synchronized void addMonitorListener(
        WatchdogMonitorListener p_listener
        )
    {
        i_listenerAL.add(p_listener);
    }

    /**
     * Removes the supplied monitor listener from this monitor.
     * @param p_listener        listener to remove.
     * @return <code>true</code> if monitor was present (and so has been
     * removed), <code>false</code> otherwise.
     */
    public synchronized boolean removeMonitorListener(
        WatchdogMonitorListener p_listener
        )
    {
        return i_listenerAL.remove(p_listener);
    }

    /**
     * Starts the monitor (it will start monitoring and notifiying
     * listeners appropriately).
     */
    public synchronized void start()
    {
        Iterator                l_iterator;
        WatchdogMonitor         l_monitor;
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11100, this,
                "Starting");

        // GroupOrMonitor starts in passed state with failed count 0
        // TODO 2 Sort out when first starting - use starting state and more complex logic?
        if(i_enabled)
        {
            i_failedMonitorCount = 0;
            i_overallState = C_STATE_PASSED;
            l_iterator = i_monitorAL.iterator();
            while(l_iterator.hasNext())
            {
                l_monitor = (WatchdogMonitor)l_iterator.next();
                l_monitor.start();
            }
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11190, this,
                    "Started");
        }
        else
        {
            if(PpasDebug.on) PpasDebug.print(
                    PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11195, this,
                    "Not started as disabled.");
        }
        
    }
    
    /**
     * Stops the monitor (it will stop monitoring and stop notifiying
     * listeners).
     */
    public synchronized void stop()
    {
        Iterator                l_iterator;
        WatchdogMonitor         l_monitor;
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11200, this,
                "Stopping");

        l_iterator = i_monitorAL.iterator();
        while(l_iterator.hasNext())
        {
            l_monitor = (WatchdogMonitor)l_iterator.next();
            l_monitor.stop();
        }
        i_overallState = C_STATE_NOT_RUNNING;
        
        if(PpasDebug.on) PpasDebug.print(
                PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_INIT | PpasDebug.C_APP_MWARE,
                PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11290, this,
                "Stopped");

    }

    /**
     * Sets the enabled state of the monitor. This does not start or stop the monitor!
     * @param p_enable          enabled state to set.
     */
    public void enable(boolean p_enable)
    {
        Iterator                l_iterator;
        WatchdogMonitor         l_monitor;

        i_enabled = p_enable;
        l_iterator = i_monitorAL.iterator();
        while(l_iterator.hasNext())
        {
            l_monitor = (WatchdogMonitor)l_iterator.next();
            l_monitor.enable(p_enable);
        }
        
    }

    /**
     * Returns whether the monitor is currently running.
     * @return Returns whether the monitor is currently running.
     */
    public synchronized boolean isRunning()
    {
        boolean                 l_isRunning = true;
        
        if(i_overallState == C_STATE_NOT_RUNNING)
        {
            l_isRunning = false;
        }
        
        return l_isRunning;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        StringBuffer            l_sb;
         
        l_sb = new StringBuffer(160);
        
        l_sb.append(C_CLASS_NAME + "=[");
        l_sb.append("overallState=");
        l_sb.append(i_overallState);
        l_sb.append(",failedMonitorCount=");
        l_sb.append(i_failedMonitorCount);
        l_sb.append(",monitorAL=\n[");
        l_sb.append(i_monitorAL);
        l_sb.append("\n]]");
               
        return l_sb.toString();
    }

    /**
     * Called when the composite Group OR monitor has detected that it has
     * entered the failed state. Changes the internal state and also tiggers
     * notification to all listeners. 
     */
    private synchronized void monitorHasEnteredFailedState()
    {
        i_failedMonitorCount++;
        if(i_failedMonitorCount > 0)
        {
            // At least one monitor in group has failed, so group
            // has failed.
            i_overallState = C_STATE_FAILED;
            notifyHasEnteredState(false);
        }
    }

    /**
     * Called when the composite Group OR monitor has detected that it has
     * entered the passed state. Changes the internal state and also tiggers
     * notification to all listeners. 
     */
    private synchronized void monitorHasEnteredPassedState()
    {
        i_failedMonitorCount--;
        if(i_failedMonitorCount <= 0)
        {
            // All monitors in group have passed, so group
            // has passed.
            i_overallState = C_STATE_PASSED;
            notifyHasEnteredState(true);
        }
    }

    /**
     * Notifies passed (if <code>true</code> supplied) or failed
     * (if <code>false</code> supplied) to all listeners.
     * 
     * @param p_passed          <code>true</code> if passed,
     *                          <code>false</code> if failed.
     */
    private void notifyPassedOrFailed(
        boolean                 p_passed
        )
    {
        Iterator                l_listenersI;
        WatchdogMonitorListener l_listener;
        
        l_listenersI = i_listenerAL.iterator();
        while(l_listenersI.hasNext())
        {
            l_listener = (WatchdogMonitorListener)l_listenersI.next();
            if(p_passed)
            {
                l_listener.passed();
            }
            else
            {
                l_listener.failed();
            }
        }
    }
    
    /**
     * Notifies has entered passed state (if <code>true</code> supplied)
     * or has entered failed state
     * (if <code>false</code> supplied) to all listeners.
     * 
     * @param p_passed          <code>true</code> if has entered passed,
     *                          <code>false</code> if has entered failed.
     */
    private void notifyHasEnteredState(
        boolean                 p_passed
        )
    {
        Iterator                l_listenersI;
        WatchdogMonitorListener l_listener;
        
        l_listenersI = i_listenerAL.iterator();
        while(l_listenersI.hasNext())
        {
            l_listener = (WatchdogMonitorListener)l_listenersI.next();
            if(p_passed)
            {
                l_listener.hasEnteredPassedState();
            }
            else
            {
                l_listener.hasEnteredFailedState();
            }
        }
    }

    /**
     * Called whenever a consituent monitor notifies that it has passed.
     * If this composite Group OR monitor is in the passed state, this
     * Group OR monitor notifies its listeners that a pass has been
     * detected. i.e. if this Group OR monitor is in any other state (e.g.
     * failed) no notification is sent to this Group OR monitor's
     * listeners (thats the meaning of the OR!).
     */
    private synchronized void aMonitorHasPassed()
    {
        // Passes only counted while we are in the passed
        // overall state (as this is an OR monitor and so
        // only if all monitors pass do we count as passed).
        if(i_overallState == C_STATE_PASSED)
        {
            i_overallPassedCounter++;
            notifyPassedOrFailed(true);
        }
    }

    /**
     * Called whenever a consituent monitor notifies that it has failed.
     * This
     * Group OR monitor notifies its listeners that a fail has been
     * detected. i.e. if this Group OR monitor is in any state,
     * a failed notification is always sentto this Group OR monitor's
     * listeners (thats the meaning of the OR!).
     */
    private synchronized void aMonitorHasFailed()
    {
        // Failures always counted (as this is an OR monitor and so
        // any failure is treated as failure of the monitor).
        i_overallFailedCounter++;
        notifyPassedOrFailed(false);
    }

    /**
     * Private inner class implementing the listeners used by the enclosing
     * Group OR monitor to listen to consituent monitors. There is one
     * istance of this class creeted and added to each monitor which
     * which is added to this composite Group OR monitor.
     */
    private class MyMonitorL implements WatchdogMonitorListener
    {

        /** Number of times the listened to monitor has passed. */
        public int              i_individualPassedCounter = 0;

        /** Number of times the listened to monitor has failed. */
        public int              i_individualFailedCounter = 0;

        /** Name of the monitor*/
        private String          i_monitorName;
        
        /** Status of the listened to monitor based on notifications. */
        private int             i_status = C_STATE_STARTING;

        /**
         * Creates a new monitor listener.
         * 
         * @param p_monitorName name of monitor.
         */
        public MyMonitorL(
            String              p_monitorName
            )
        {
            i_monitorName = p_monitorName;
        }

        /** {@inheritDoc} */
        public void passed()
        {
            i_individualPassedCounter++;
            aMonitorHasPassed();
        }

        /** {@inheritDoc} */
        public void failed()
        {
            i_individualFailedCounter++;
            aMonitorHasFailed();
        }
        
        /** {@inheritDoc} */
        public void hasEnteredFailedState()
        {
            // On initiailization, passed is viewed as the default, so
            // a change to failed is always notified, whereas a change to passed
            // is only notified if going from a failed state.
            System.out.println(">>>> " + C_CLASS_NAME + ":" + i_monitorName + " Entered failed state.");
            if(i_status != C_STATE_FAILED)
            {
                    // Gone from passed to failed, notify
                    monitorHasEnteredFailedState();
            }
            i_status = C_STATE_FAILED;
            
        }
        
        /** {@inheritDoc} */
        public void hasEnteredPassedState()
        {
            // On initiailization, passed is viewed as the default, so
            // a change to failed is always notified, whereas a change to passed
            // is only notified if going from a failed state.
            System.out.println(">>>> " + C_CLASS_NAME + ":" + i_monitorName + " Entered passed state.");
            if(i_status == C_STATE_FAILED)
            {
                    // Gone from failed to passed, notify
                    monitorHasEnteredPassedState();
            }
            i_status = C_STATE_PASSED;
        }

    } // End of private inner class MyMonitorL
    
}  // End of public class GroupOrWatchdogMonitor.

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////