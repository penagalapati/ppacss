////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FtpAlarmDestination.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       FTP destination for alarms.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;


/**
 * FTP destination for alarms. Defines all attributes for FTP to be used
 * as a destination for alarms.
 */
public class FtpAlarmDestination
extends AlarmDestination
{
    
    /** Append mode indicating use FTP append.*/
    public static final int     C_APPEND_MODE_APPEND = 1;
    
    /** Append mode indicating use FTP store.*/
    public static final int     C_APPEND_MODE_STORE = 2;

    /**
     * NOT IMPLEMENTED YET - Append mode indicating use FTP append, if
     * error, try once more with FTP store.
     */
    public static final int     C_APPEND_MODE_APPEND_STORE = 3;

    /**
     * Name or IP address of remote node to be used when FTP'ing.
     */
    private String              i_remoteNode;

    /**
     * Port number of remote node to be used when FTP'ing.
     */
    private int                 i_remotePort;

    /**
     * Path and file name of remote file to be used when FTP'ing.
     */
    private String              i_remotePathAndFilename;

    /**
     * Append mode. One of the <code>C_APPEND_MODE_...</code> constants.
     */
    private int                 i_appendMode;

    /**
     * Username of remote user to be used when FTP'ing.
     */
    private String              i_userName;

    /**
     * Password of remote user to be used when FTP'ing.
     */
    private String              i_password;
    
    /**
     * Creates a new FTP Alarm Destination.
     * 
     * @param  p_remoteNode     remote node name or IP address.
     * @param  p_remotePort     remote node's FTP port number.
     * @param  p_remotePathAndFilename remote files path and filename.
     * @param  p_appendMode     append mode, one of the
     *                          <code>C_APPEND_MODE_...</code> constants.
     * @param  p_userName       remote user name.
     * @param  p_password       remote user password.
     */
    public FtpAlarmDestination(
        String                  p_remoteNode,
        int                     p_remotePort,
        String                  p_remotePathAndFilename,
        int                     p_appendMode,
        String                  p_userName,
        String                  p_password
    )
    {
        super(C_TYPE_FTP);
        
        i_remoteNode = p_remoteNode;
        i_remotePort = p_remotePort;
        i_remotePathAndFilename = p_remotePathAndFilename;
        i_appendMode = p_appendMode;
        i_userName = p_userName;
        i_password = p_password;
    }

    /**
     * Returns the remote node name or IP address.
     * 
     * @return The remote node name or IP address
     */
    public String getRemoteNode()
    {
        return i_remoteNode;
    }

    /**
     * Returns the remote FTP port number.
     * 
     * @return The remote FTP port number.
     */
    public int getRemotePort()
    {
        return i_remotePort;
    }

    /**
     * Returns the remote path and filename of the remote file.
     * 
     * @return The remote path and filename of the remote file
     */
    public String getRemotePathAndFilename()
    {
        return i_remotePathAndFilename;
    }

    /**
     * Returns the append mode.
     * One of the <code>C_APPEND_MODE_...</code> constants.
     * 
     * @return The append mode.
     */
    public int getAppendMode()
    {
        return i_appendMode;
    }

    /**
     * Returns the username for the remote user to be used by FTP.
     * 
     * @return The username for the remote user to be used by FTP.
     */
    public String getUserName()
    {
        return i_userName;
    }

    /**
     * Returns the password for the remote user to be used by FTP.
     * 
     * @return The password for the remote user to be used by FTP.
     */
    public String getPassword()
    {
        return i_password;
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( "FtpAlarmDestination=[node=" + i_remoteNode + ", " +
                ", port=" + i_remotePort + ", filename=" + i_remotePathAndFilename +
                ", " + super.toString() + "]");
    }

} // End of public class FtpAlarmDestination

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////