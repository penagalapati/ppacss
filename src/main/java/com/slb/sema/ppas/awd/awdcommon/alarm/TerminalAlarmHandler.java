////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       EmailAlarmHandler.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This class implements a Terminal (Console)
//                              Alarm Handler.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.awdcommon.alarm;

import com.slb.sema.ppas.awd.awdcommon.AwdContext;
import com.slb.sema.ppas.awd.awdcommon.platform.AwdPlatform;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * This class implements a Terminal (Console) Alarm Handler which delivers
 * alarms to user's terminals (consoles). The method of delivery is Operating
 * System dependent, but this is hidden within the
 * {@link com.slb.sema.ppas.awd.awdcommon.platform.AwdPlatform} class.
 */
public class TerminalAlarmHandler
extends AbstractAsynchAlarmHandler
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TerminalAlarmHandler";

    /** The AWD platform used to deliver the alarms. */    
    private AwdPlatform         i_awdPlatform;
    
    /**
     * Creates a new Terminal Alarm Handler.
     * 
     * @param  p_logger         logger messages logged to.
     * @param  p_configProperties configuration properties.
     * @param  p_instrumentManager instrument Manager to register any instruments with.
     * @param  p_awdContext     context for this component.
     */
    public TerminalAlarmHandler(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        InstrumentManager       p_instrumentManager,
        AwdContext              p_awdContext
        )
    {
        super(  p_logger,
                p_configProperties,
                p_instrumentManager,
                p_awdContext,
                1,
                "TermlAH"
                );
        
        // TODO: <<Change to factory method????>>
        i_awdPlatform = new AwdPlatform(i_logger);
    }
    
    /**
     * {@inheritDoc}
     */
    protected synchronized void doDeliverAsynchAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination,
        AsynchAlarmNotifiyer    p_notifier
    )
    {
        if(Debug.on) Debug.print(Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE, C_CLASS_NAME, 12010, this,
                "Delivering alarm " + p_formattedAlarm + " to destination " +
                p_destination);
                
        deliverTerminalAlarm(p_threadInstanceNum, p_alarm, p_formattedAlarm, p_destination);
    }
    
    /**
     * Actually performs the alarm delivery to a log file.
     * 
     * @param p_threadInstanceNum the thread number calling this method (used
     *                          to selected the allocated email client).
     * @param p_alarm           the alarm.
     * @param p_formattedAlarm  the formatted alarm.
     * @param p_destination     the destination to deliver the alarm to.
     */
    private synchronized void deliverTerminalAlarm(
        int                     p_threadInstanceNum,
        Alarm                   p_alarm,
        String                  p_formattedAlarm,
        AlarmDestination        p_destination
    )
    {

        UserAlarmDestination    l_userAlarmDestination;
        
        l_userAlarmDestination = (UserAlarmDestination)p_destination;
        
        if(Debug.on) Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_MWARE,
                Debug.C_ST_START | Debug.C_ST_TRACE,
                C_CLASS_NAME, 11010, this,
                "Delivering alarm " + p_formattedAlarm + " to destination " +
                p_destination
                );
        
        i_awdPlatform.writeToUsersTerminals(
            l_userAlarmDestination.getUserName(),
            p_formattedAlarm
            );

    }
    
} // End of public class TerminalAlarmHandler
