////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogMonitorConfigLoader.java
//      DATE            :       13-Oct-2006
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#2644
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Loader which supports the loading of Watchdog
//                              Monitor configuration.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/08/07 | E Dangoor  | Allow config to be found either | PpacLon#3319/12021
//          |            | in the local or the system area |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.awd.watchdog.wdogcommon;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.config.AwdAbstractConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.ConfigTokenizer;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Loader which supports the loading of Watchdog
 * Monitor configuration.
 */
public class WatchdogMonitorConfigLoader extends AwdAbstractConfigLoader
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "WatchdogMonitorConfigLoader";

    /**
     * Creates a new <code>WatchdogMonitorConfigLoader</code>. 
     * @param p_logger          logger to use.
     * @param p_configProperties configuration properties to use to load config.
     * @param p_watchdogContext context.
     */
    public WatchdogMonitorConfigLoader(
        Logger                  p_logger,
        PpasProperties          p_configProperties,
        WatchdogContext         p_watchdogContext
    )
    {
        super(p_logger, p_configProperties, p_watchdogContext);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Loads the watchdog monitor config into and a new <code>WatchdogMonitorEntries</code>
     * and returns it.
     * @param p_configFilename  path and filename of config to load.
     * @param p_watchdogActionGroupConfig watchdog action group configuration to use.
     * @param p_processName     process name running in.
     * @return                  the loaded alarm event config.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    public WatchdogMonitorEntries loadWatchdogMonitorEntries(
        String                  p_configFilename,
        WatchdogActionGroupConfig p_watchdogActionGroupConfig,
        String                  p_processName
    )
    throws AwdConfigException
    {
        WatchdogMonitorEntries l_entries;
        
        l_entries = new WatchdogMonitorEntries(
                i_logger,
                p_watchdogActionGroupConfig);
        
        loadLayeredConfig(p_configFilename, l_entries, p_processName);
        
        return l_entries;
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_loadLayeredConfig = "loadLayeredConfig";
    
    /**
     * Loads the watchdog monitor config into and the <code>WatchdogMonitorEntries</code>.
     * @param p_configFilename  filename of config to load.
     * @param p_watchdogMonitorEntries atchdog monitor entries to load into.
     * @param p_processName     process name running in.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    private void loadLayeredConfig(
        String                  p_configFilename,
        WatchdogMonitorEntries  p_watchdogMonitorEntries,
        String                  p_processName
        )
    throws AwdConfigException
    {
        String  l_root;
        String  l_configPath;

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_loadLayeredConfig);

        // First look in the system area version of the configuration file
        l_root = System.getProperty("ascs.systemRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        loadConfig(l_configPath, p_watchdogMonitorEntries, p_processName);

        // Then look in the local area version of the configuration file.
        // This will overwrite existing values
        l_root = System.getProperty("ascs.localRoot");
        l_configPath = l_root + "/conf/awd/" + p_configFilename;
        if ((new File(l_configPath)).exists())
        {
            loadConfig(l_configPath, p_watchdogMonitorEntries, p_processName);
        }

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_END, C_CLASS_NAME, 10190, this,
                        "Leaving " + C_METHOD_loadLayeredConfig);
        }
    }

    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_loadConfig = "loadConfig";
    
    /**
     * Loads the watchdog monitor config into and the <code>WatchdogMonitorEntries</code>.
     * @param p_configFilename  path and filename of config to load.
     * @param p_watchdogMonitorEntries atchdog monitor entries to load into.
     * @param p_processName     process name running in.
     * @throws AwdConfigException if a problem occurs loading the
     *                          configuration.
     */
    private void loadConfig(
        String                  p_configFilename,
        WatchdogMonitorEntries  p_watchdogMonitorEntries,
        String                  p_processName
        )
    throws AwdConfigException
    {

        ConfigTokenizer         l_tokenizer;
        String                  l_token;        
        String                  l_monitorName;
        String                  l_monitorClassName;
        String                  l_enteredFailedStateWatchdogActionGroupName;
        String                  l_enteredPassedStateWatchdogActionGroupName;

        Map                     l_monitorParamMap;
        String                  l_monitorParamName;
        String                  l_monitorParamValue;
 
        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_START, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_loadConfig);

        l_tokenizer = new ConfigTokenizer(i_logger, p_configFilename);

        l_token = l_tokenizer.nextToken();
        while("watchdog.monitor.name".equals(l_token))
        {
            l_tokenizer.getExpectedNextToken("=");
            l_monitorName = l_tokenizer.getExpectedNextToken();
            l_tokenizer.getExpectedNextToken("{");

            l_enteredFailedStateWatchdogActionGroupName = l_tokenizer.getExpectedNextParameter(
            "enteredFailedStateWatchdogActionGroupName");
            l_enteredPassedStateWatchdogActionGroupName = l_tokenizer.getExpectedNextParameter(
            "enteredPassedStateWatchdogActionGroupName");            
            
            // Read monitor
            l_monitorClassName = l_tokenizer.getExpectedNextParameter("monitorClass");                
            l_monitorParamMap = new HashMap();
            while("monitorParam".equals(l_token = l_tokenizer.getExpectedNextToken(
                    new String[] {"monitorParam", "monitorClass", "}"} )))
            {
                l_tokenizer.getExpectedNextToken("=");
                l_monitorParamName = l_tokenizer.getExpectedNextToken();
                l_tokenizer.getExpectedNextToken("=");
                l_monitorParamValue = l_tokenizer.getExpectedNextToken();
                l_monitorParamMap.put(l_monitorParamName, l_monitorParamValue);
            }

            if(!"}".equals(l_token))
            {
                handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                        l_token, l_tokenizer.getLineNumber(), p_configFilename, "'}' or 'actionGroup'");
            }
            p_watchdogMonitorEntries.addNewWatchdogMonitorEntry(
                    l_monitorName,
                    l_monitorClassName,
                    l_monitorParamMap,
                    l_enteredFailedStateWatchdogActionGroupName,
                    l_enteredPassedStateWatchdogActionGroupName);
            l_token = l_tokenizer.nextToken();
        }
        
        if(l_token != null)
        {
            handleUnexpectedToken(C_CLASS_NAME, C_METHOD_loadConfig, 10000,
                    l_token, l_tokenizer.getLineNumber(), 
                    p_configFilename, "'watchdog.action.group.name' or end of file");
        }

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_END, C_CLASS_NAME, 10000, this,
                "Leaving " + C_METHOD_loadConfig);
                
        return;
    }

} // End of public class WatchdogMonitorConfigLoader

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////
