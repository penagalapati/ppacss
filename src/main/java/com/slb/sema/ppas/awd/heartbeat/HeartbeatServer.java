////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       HeartbeatServer.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Heartbeat Server including bootstrap for it.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 27/02/06 | R O'Brien  | Alarm instance name changed     | Ppacs#1988/8000
//          |            | since config moved to           |
//          |            | awd_msg.xml file.               |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.heartbeat;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer;
import com.slb.sema.ppas.awd.awdcommon.AwdConfigException;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmActionListener;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor;
import com.slb.sema.ppas.awd.awdcommon.alarm.ConfiguredAlarm;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmGroupConfigLoader;
import com.slb.sema.ppas.awd.awdcommon.config.AlarmInstanceConfig;
import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Heartbeat Server including bootstrap for it. The Heatbeat Server sends an
 * heartbeat "alarm" to one or more destinations based on configuration. It
 * uses an {@link com.slb.sema.ppas.awd.awdcommon.alarm.AlarmProcessor
 * AlarmProcessor} to actually send the heartbeat "alarm".
 * This means mutiple destinations, in multiple formats using multiple
 * delivery methods can be used for the heatbeat "alarm(s)" -
 * see {@link com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDeliveryManager
 * AlarmDeliveryManager}.
 */
public class HeartbeatServer extends AwdAbstractServer
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "HeartbeatServer";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.awd.heartbeat.HeartbeatServer.";

    /**
     * Configuration property name for the interval (millis) between sending
     * heartbeats.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_HEARTBEAT_INTERVAL = C_CFG_PROPERTY_BASE +
                                        "heartbeatIntervalMillis";
    /**
     * Default for the interval (millis) between sending heartbeats.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_HEARTBEAT_INTERVAL_DEFAULT =
                                        "60000";
    
    /**
     * Configuration property name for the name of the alarm
     * instance configuration of the alarm to send as the heartbeat.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_INSTANCE_CFG_NAME = C_CFG_PROPERTY_BASE +
                                        "alarmInstanceConfigName";

    /**
     * Default for the name of the alarm
     * instance configuration of the alarm to send as the heartbeat.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_INSTANCE_CFG_NAME_DEFAULT =
                                        "ALARM.EXTERNAL.HEARTBEAT_ALARM";

    /**
     * Configuration property name for the name of the alarm group
     * configuration of where and how to send the heartbeat.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_GROUP_NAME = C_CFG_PROPERTY_BASE +
                                        "alarmGroupName";

    /**
     * Default for the name of the alarm group
     * configuration of where and how to send the heartbeat.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_GROUP_NAME_DEFAULT =
                                        "HeartbeatDefaultAlarmGroup";

    /**
     * Configuration property name for the filename and path of the alarm
     * configuration file to be load.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_CFG_FILENAME = C_CFG_PROPERTY_BASE +
                                        "alarmConfigFilePathAndName";

    /**
     * Configuration property name for the filename and path of the alarm
     * group configuration file to be load.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_GROUP_CFG_FILENAME = C_CFG_PROPERTY_BASE +
                                        "alarmGroupConfigFilePathAndName";

    /** Simple date format for showing dates as "dd-MMM-yy HH:mm:ss.SSS". */
    private static final SimpleDateFormat C_DATE_FORMAT = new SimpleDateFormat(
                                        "dd-MMM-yy HH:mm:ss.SSS"); 

    /**
     * Background thread which performs the main processing of the Heartbeat
     * Server
     */
    private ServerThread        i_serverThread;
    
    /** Alarm processor used to send the "heartbeat" alarm. */
    private AlarmProcessor      i_alarmProcessor;
    
    /**
     * Heartbeat Server configuration. Current copy of the configuratiob which
     * is swapped in a single atomic action when a reload occurs. 
     */
    private HeartbeatConfig     i_heartbeatConfig;

    /** Number of heartbeats raised counter instrument. */
    private CounterInstrument   i_heartbeatsRaisedCI;
    
    /**
     * Number of heartbeats deliveries counter instrument. Each heartbeat
     * could be delivered to multiple destinations via multiple methods.
     */
    private CounterInstrument   i_heartbeatDeliveriesCI;

    /**
     * Number of successful heartbeat deliveries counter instrument. Each heartbeat
     * could be delivered to multiple destinations via multiple methods.
     */
    private CounterInstrument   i_heartbeatSuccessfulDeliveriesCI;

    /**
     * Number of failed heartbeat deliveries counter instrument. Each heartbeat
     * could be delivered to multiple destinations via multiple methods.
     */
    private CounterInstrument   i_heartbeatFailedDeliveriesCI;

    /** Last time a heartbeat was raised (Java time in millis) */
    private long                i_lastHeartbeatRaisedTimeMillis = 0;

    /** Previous (to last) time a heartbeat was raised (Java time in millis) */
    private long                i_previousHeartbeatRaisedTimeMillis = 0;

    /**
     * Creates a new Heartbeat Server. 
     */
    public HeartbeatServer()
    {
        super();

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);

        if(Debug.on) Debug.print(
                Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME);
    }

    /**
     * Performs the Heartbeat Server specific initialisation. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#init()} method.
     * @throws PpasException if initialisation fails.
     */    
    public void doInit()
    throws PpasException
    {
        
        i_heartbeatsRaisedCI = new CounterInstrument("Heartbeats Raised",
                "Number of heartbeats raised.");
        i_instrumentSet.add(i_heartbeatsRaisedCI);

        i_heartbeatDeliveriesCI = new CounterInstrument("Heartbeat Deliveries",
                "Number of heartbeat deliveries attempted.");
        i_instrumentSet.add(i_heartbeatDeliveriesCI);
                
        i_heartbeatSuccessfulDeliveriesCI = new CounterInstrument(
                "Successful Heartbeat Deliveries",
                "Number of successful heartbeat deliveries.");
        i_instrumentSet.add(i_heartbeatSuccessfulDeliveriesCI);

        i_heartbeatFailedDeliveriesCI = new CounterInstrument(
                "Failed Heartbeat Deliveries",
                "Number of failed heartbeat deliveries.");
        i_instrumentSet.add(i_heartbeatFailedDeliveriesCI);

        i_alarmProcessor = new AlarmProcessor(
                i_logger,
                i_instrumentManager,
                i_awdContext
                );

        i_alarmProcessor.init(i_configProperties);

        loadConfig(i_configProperties);
                
        return;
    }

    /**
     * Loads the Hearbeat Server specific configuration from the supplied
     * properties file.
     * 
     * @param  p_configProperties properties to load config from.
     * @throws AwdConfigException problem loading the config.
     */
    private void loadConfig(
        PpasProperties          p_configProperties
    )
    throws AwdConfigException
    {
        HeartbeatConfig         l_heartbeatConfig;
        AlarmConfigLoader       l_alarmConfigLoader;
        AlarmGroupConfigLoader  l_alarmGroupConfigLoader;
        String                  l_heartbeatAlarmInstanceConfig;
        String                  l_heartbeatAlarmGroupName;
        String                  l_configFilename;
        
        l_alarmConfigLoader = new AlarmConfigLoader(
                i_logger, p_configProperties, i_awdContext);
        l_alarmGroupConfigLoader = new AlarmGroupConfigLoader(
                i_logger, p_configProperties, i_awdContext);

        // Use local reference and new object so if this is a reload,
        // we won't affect existing
        // config until reload is complete and then config swapped in one atomic
        // step.
        l_heartbeatConfig = new HeartbeatConfig();

        // Load heartbeat interval (i.e. how often to perform the heartbeat).
        l_heartbeatConfig.i_heartbeatIntervalMillis = Integer.parseInt(
                p_configProperties.getTrimmedProperty(
                        C_CFG_PARAM_HEARTBEAT_INTERVAL, C_CFG_PARAM_HEARTBEAT_INTERVAL_DEFAULT));
                        
        // Load heartbeat alarm instance config (i.e. details of alarm to use
        // for heartbeat 'alarm' such as text and severity).
        l_heartbeatAlarmInstanceConfig = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_INSTANCE_CFG_NAME, C_CFG_PARAM_ALARM_INSTANCE_CFG_NAME_DEFAULT);
        // TODO: <<Handle null - mandatory parameter!!!!>>
        l_configFilename = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_CFG_FILENAME);
        l_heartbeatConfig.i_heartbeatAlarmInstanceConfig =
                l_alarmConfigLoader.loadSingleInstanceConfig(
                        l_configFilename, l_heartbeatAlarmInstanceConfig);

        // Load heartbeat alarm group (i.e. format, actions and destinations for
        // heartbeat 'alarm').
        l_heartbeatAlarmGroupName = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_GROUP_NAME, C_CFG_PARAM_ALARM_GROUP_NAME_DEFAULT);
        l_configFilename = p_configProperties.getTrimmedProperty(
                C_CFG_PARAM_ALARM_GROUP_CFG_FILENAME);
        l_heartbeatConfig.i_heartbeatAlarmGroup =
                l_alarmGroupConfigLoader.loadSingleAlarmGroup(
                        l_configFilename, l_heartbeatAlarmGroupName);
                        
        // Config successfully loaded, set the config in one atomic step.
        i_heartbeatConfig = l_heartbeatConfig;
                
        return;
    }

    /**
     * Reloads the Hearbeat Server specific configuration from the supplied
     * properties file. NOTE: Not all configuration is reloadable.
     * 
     * @throws  AwdConfigException problem reloading the config.
     */
    public void reloadConfig()
    throws AwdConfigException
    {
        loadConfig(i_configProperties);
    }
        
    /**
     * Performs the Heartbeat Server specific startup. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes
     * {@link com.slb.sema.ppas.awd.awdcommon.AwdAbstractServer#start} method.
     */    
    public synchronized void doStart()
    {
        if(i_serverThread == null)
        {
            i_serverThread = new ServerThread();
            i_serverThread.start();
        }
    }

    /**
     * Performs the Heartbeat Server specific stop (shutdown) processing. Overrides the
     * empty default implementation in the super class. It is called by the
     * super classes <code>stop</code> method.
     */    
    public synchronized void doStop()
    {
        if(i_serverThread != null)
        {
            try
            {
                i_serverThread.stop();
            }
            catch (InterruptedException l_e)
            {
                // We tried, best thing we can do now is ignore this.
            }
            finally
            {
                i_serverThread = null;
            }
        }
    }

    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return( "HeartbeatServer=[alarmProcessor=" + i_alarmProcessor + ", " +
                super.toString() + "]");
    }

    /**
     * Returns a verbose String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A verbose respresentation suitable for debugging/trace.
     */
    public String toVerboseString()
    {
        return( "HeartbeatServer=[alarmProcessor=" + i_alarmProcessor + ", config=" +
                i_heartbeatConfig + ", " + super.toVerboseString() + "]");
    }
    
    /**
     * Main bootstrap method for the Heartbeat Server. Instantiates a new
     * <code>HeartbeatServer</code>, calls <code>init</code> and then calls
     * <code>start</code> on it.
     * 
     * @param  p_argsARR      command line arguments - none currently.
     */
    public static void main(
        String                  p_argsARR[]
    )
    {
        HeartbeatServer         l_heartbeatServer;
        
        try
        {
            // Debug.setDebugFullToSystemErr();

            l_heartbeatServer = new HeartbeatServer();
            l_heartbeatServer.init();
            l_heartbeatServer.start();

            //Thread.sleep(60000);
            
            //System.out.println("Reloading config...");
            /*
            l_configProperties = new UtilProperties();
            // l_configProperties.load("AwdHeartbeatConfig.properties"); <<!!!!>>
            l_configProperties.setProperty(
                    C_CFG_PARAM_HEARTBEAT_INTERVAL, "30000");
                    */
            // TODO: Reload HEARTBEAT _INTERVAL!!!!
            //l_heartbeatServer.reloadConfig();
            //System.out.println("...config reloaded.");
            
        }
        catch(Exception l_e)
        {
            System.err.println(
                    "HearbeatServer startup exiting due to exception : ");
            l_e.printStackTrace();
            System.exit(1);
        }
        
    }
    
    /** Manager method returning the last time a heartbeat was sent.
     * @return Date/time heartbeat was last sent.
     */
    public String mngMthGetLastHeartBeatSentTime()
    {
        return C_DATE_FORMAT.format(new Date(i_lastHeartbeatRaisedTimeMillis));
    }

    /** Manager method returning the previous (to last) time a heartbeat was sent.
     * @return date/time the previous heartbeat was sent. 
     */
    public String mngMthGetPreviousHeartBeatSentTime()
    {
        return C_DATE_FORMAT.format(new Date(i_previousHeartbeatRaisedTimeMillis));
    }
    
    /**
     * Performs the top level main processing of the Heartbeat Server. Called
     * from withing a separate, background thread. Calls
     * <code>sendHeartbeat</code>, then waits for the configured interval between
     * sending heartbeats beform calling <code>sendHeartbeat</code> again.
     *
     */
    private void doHeartbeatProcessing()
    {
        
        sendHeartbeat();
        
        // Wait for the configured time before sending the next heartbeat.
        try
        {
            synchronized(this)
            {
                wait(i_heartbeatConfig.i_heartbeatIntervalMillis);
            }
        }
        catch(InterruptedException l_e)
        {
            // Ignore
        }
        
    }

    /**
     * Performs the top level processing to send all the configured
     * heartbeats. Stored a local reference to the Heartbeat Server config
     * (incase a reload occurs), creates a new "Alarm" for the heartbeat and
     * then calls <code>processAlarm</code> on the Alarm Processor.
     */
    private void sendHeartbeat()
    {
        Alarm                   l_alarm;
        HeartbeatConfig         l_heartbeatConfig;
        
        // Hold local reference to heartbeat config in case a reload occurs
        // during processing.
        l_heartbeatConfig = i_heartbeatConfig;
        
        // Create heartbeat alarm based on configured alarm, then send it
        l_alarm = new HeartbeatAlarm(
                l_heartbeatConfig.i_heartbeatAlarmInstanceConfig);
        i_alarmProcessor.processAlarm(
                l_alarm,
                l_heartbeatConfig.i_heartbeatAlarmGroup);

        if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
        {
            if (i_instrumentSet.i_isEnabled)
            {
                i_heartbeatsRaisedCI.increment();
            }
        }
        i_previousHeartbeatRaisedTimeMillis = i_lastHeartbeatRaisedTimeMillis;
        i_lastHeartbeatRaisedTimeMillis = System.currentTimeMillis();

    }

    /**
     * This private inner class implements the background thread that
     * performs the main processing of the Heartbeat Server.
     */    
    private class ServerThread extends ThreadObject
    {
        /**
         * Performs the processing of the background thread by calling
         * <code>doHeartbeatProcessing</code> until <code>stop</code>
         * is called on the thread.
         */
        public void doRun()
        {
            while(!isStopping())
            {
                doHeartbeatProcessing();
            }
        }

        /**
         * Returns the name to set this thread's name to.
         * @return the name of the thread.
         */
        public String getThreadName()
        {
            return ("HartBServThd");
        }
    }

    /**
     * This private inner class implements a simple container to hold all
     * configuration values and references to the Heartbeat Server
     * configuration so that when
     * configuration is realoaded it can be swapped in in a single atomic
     * statement.
     */
    private class HeartbeatConfig
    {
        /** Interval (millis) between sending heartbeats */
        private int             i_heartbeatIntervalMillis;
        
        /** Alarm instance configuration of the alarm to send as the heartbeat. */
        private AlarmInstanceConfig i_heartbeatAlarmInstanceConfig;
        
        /** Alarm group configuration of where and how to send the heartbeat. */
        private AlarmGroup      i_heartbeatAlarmGroup;
    }

    /** Implements an alarm for heartbeat. */
    private class HeartbeatAlarm extends ConfiguredAlarm
    implements AlarmActionListener
    {
        /**
         * Creates a new alarm, based on the configured alarm instance.
         * @param  p_alarmConfig configured alarm instance.
         */
        public HeartbeatAlarm(
            AlarmInstanceConfig             p_alarmConfig
        )
        {
            super(p_alarmConfig);
        }
        
        public void alarmActionStatus(
            Alarm                   p_alarm,
            boolean                 p_success,
            AlarmAction             p_alarmAction,
            Object                  p_actionSpecificInfo
            )
        {
            if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
            {
                if (i_instrumentSet.i_isEnabled)
                {
                    i_heartbeatDeliveriesCI.increment();
                    if(p_success)
                    {
                        i_heartbeatSuccessfulDeliveriesCI.increment();
                    }
                    else
                    {
                        i_heartbeatFailedDeliveriesCI.increment();
                    }
                }
            }            
        }

    }
    
} // End of public class HeartbeatServer

////////////////////////////////////////////////////////////////////////////////
//                           End of file
////////////////////////////////////////////////////////////////////////////////