////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       WatchdogAction.java
//      DATE            :       08-Jun-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This class is the abstract super class for all
//                              Watchdog Actions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.awd.watchdog.wdogcommon;

/**
 * This class is the abstract super class for all Watchdog Actions.
 */
public abstract class WatchdogAction
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "WatchdogAction";

//TODO: Change defaults.
    /**
     * Maximum number of times action should be performed within the
     * maximum action interval.
     */    
    private int             i_maxActionCount = 2;
    
    /** Interval over which maximum number of action should not be exceeded. */
    private long            i_actionCountLifetimeMillis = 2*60*1000; // 2m


    /**
     * Creates a new Watchdog Action.
     */
    public WatchdogAction()
    {
        this(-1, -1);
    }

    /**
     * Creates a new Watchdog Action.
     * 
     * @param p_maxActionCount          maximum number of times action should
     *                                  be performed for a process
     *                                  within the action count lifetime. -1
     *                                  means there is no maximum.
     * @param p_actionCountLifetimeMillis lifetime over which maximum number
     *                                  of actions should not be exceeded
     *                                  for a process. -1 means infinite lifetime.
     */
    public WatchdogAction(
        int                     p_maxActionCount,
        long                    p_actionCountLifetimeMillis
        )
    {
        super();
        
        i_maxActionCount = p_maxActionCount;
        i_actionCountLifetimeMillis = p_actionCountLifetimeMillis;
    }

    /**
     * @return The maximum number of times action should be performed for a process
     *         within the action count lifetime.
     */
    public int getMaxActionCount()
    {
        return i_maxActionCount;
    }

    /**
     * @return Lifetime over which maximum number of action should not be exceeded
     *         for a process.
     *         -1 means forever, so all action counts are kept (i.e. once
     *         max exceeded the action will not be repeated again by this
     *         JVM.
     */
    public long getActionCountLifetimeMillis()
    {
        return i_actionCountLifetimeMillis;
    }

    /**
     * Sets the maximum number of times action should be performed for a process
     * within the action count lifetime.
     *
     * @param p_maxActionCount the maximum number of times action should be performed.
     */
    public void setMaxActionCount(int p_maxActionCount)
    {
        i_maxActionCount = p_maxActionCount;
    }

    /**
     * Sets the lifetime over which maximum number of action should not be exceeded
     * for a process. Value is in Millis and
     * -1 means forever, so all action counts are kept forever.
     * 
     * @param p_actionCountLifetimeMillis lifetime of counts.
     */
    public void setActionCountLifetimeMillis(long p_actionCountLifetimeMillis)
    {
        i_actionCountLifetimeMillis = p_actionCountLifetimeMillis;
    }
    
    /**
     * Returns a String respresentation of this object suitable for
     * debugging/trace.
     * 
     * @return A respresentation suitable for debugging/trace.
     */
    public String toString()
    {
        return C_CLASS_NAME + "=[maxActionCount=" + i_maxActionCount +
                ",actionCountLifetimeMillis=" + i_actionCountLifetimeMillis +
                "]";
    }

} // End of public abstract class WatchdogAction

////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////