/*
 * Created on 28-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.config;

import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmActionGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmDestinationGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.AlarmGroup;
import com.slb.sema.ppas.awd.awdcommon.alarm.DefaultTextAlarmFormat;
import com.slb.sema.ppas.awd.awdcommon.alarm.TerminalAlarmAction;
import com.slb.sema.ppas.awd.awdcommon.alarm.UserAlarmDestination;
import com.slb.sema.ppas.util.support.Debug;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmConfigUT
{
    private AlarmGroupConfig         i_alarmConfig;

    public static void main(String[] args)
    {
        AlarmConfigUT           l_configUT;

        // Debug.setDebugFullToSystemErr();
        Debug.setDebugFullToFile();
        
        l_configUT = new AlarmConfigUT();
        
        l_configUT.test1();
    }
    
    public AlarmGroupConfig getTest1Config()
    {
        test1();
        return i_alarmConfig;
    }
    
    public void test1()
    {
        AlarmActionGroup        l_actionGroup1;
        AlarmDestinationGroup   l_destinationGroup1;
        AlarmGroup              l_alarmGroup1;
        
        l_actionGroup1 = new AlarmActionGroup();
        l_actionGroup1.addAlarmAction(new TerminalAlarmAction());
        
        l_destinationGroup1 = new AlarmDestinationGroup();
        l_destinationGroup1.addAlarmDestination(
                new UserAlarmDestination("User1"));
        l_destinationGroup1.addAlarmDestination(
                new UserAlarmDestination("User2"));
        l_destinationGroup1.addAlarmDestination(
                new UserAlarmDestination("User3"));
                
        l_alarmGroup1 = new AlarmGroup("AlarmGroup1");
        
        l_alarmGroup1.add(l_actionGroup1, l_destinationGroup1, new DefaultTextAlarmFormat());
        
        i_alarmConfig = new AlarmGroupConfig();
        
        i_alarmConfig.addAlarmGroup(l_alarmGroup1);
        
        System.out.println("test1:AlarmGroupConfig=" + i_alarmConfig.toString());
        
    }
}
