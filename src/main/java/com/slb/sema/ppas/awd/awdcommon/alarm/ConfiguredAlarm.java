/*
 * Created on 17-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.awd.awdcommon.alarm;

import java.util.HashMap;
import java.util.Map;

import com.slb.sema.ppas.awd.awdcommon.config.AlarmInstanceConfig;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarm;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ConfiguredAlarm extends SimpleAlarm
{

    /**
     * @param p_alarmName
     * @param p_alarmText
     * @param p_alarmSeverity
     */
/*    public ConfiguredAlarm(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity
    )
    {
        this(p_alarmName, p_alarmText, p_alarmSeverity, (Map)null);        
    }
*/
    /**
     * @param p_alarmName
     * @param p_alarmText
     * @param p_alarmSeverity
     */
/*    public ConfiguredAlarm(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity,
        Map                     p_alarmParameterMap
    )
    {
        super(p_alarmName, p_alarmText, p_alarmSeverity, p_alarmParameterMap);        
    }
*/    

    /**
     * Creates a new alarm, based simply on the configured alarm instance. 
     * @param p_alarmConfig     alarm configuration.
     */
    public ConfiguredAlarm(
        AlarmInstanceConfig             p_alarmConfig
    )
    {
        super(
                p_alarmConfig.getAlarmName(),
                p_alarmConfig.getAlarmText(),
                p_alarmConfig.getAlarmSeverity(),
                "UndefinedNode", "UndefinedProcessName",
                // Must create a new map from the configured map, as an alarms
                // map can be modified but the configured map must not be!
                new HashMap(p_alarmConfig.getAlarmParameterMap())
        );
    }
    
    /**
     * Creates a new alarm, based simply on the configured alarm instance. 
     * @param p_alarmName       Name of the alarm.
     * @param p_alarmText       Text of the alarm.
     * @param p_alarmSeverity   Severity of the alarm.
     * @param p_originatingNodeName originating server name alarm from.
     * @param p_originatingProcessName originating process name alarm from.
     * @param p_alarmParameterMap alarm parameters.
     * @param p_alarmConfig     alarm configuration.
     */
    public ConfiguredAlarm(
        String                  p_alarmName,
        String                  p_alarmText,
        Integer                 p_alarmSeverity,
        String                  p_originatingNodeName,
        String                  p_originatingProcessName,
        Map                     p_alarmParameterMap,
        AlarmInstanceConfig     p_alarmConfig
    )
    {
        // Default to supplied values.
        super(
                p_alarmName,
                p_alarmText,
                p_alarmSeverity == null ? 0 :  p_alarmSeverity.intValue(),
                p_originatingNodeName,
                p_originatingProcessName,
                p_alarmParameterMap
                );
        Map                     l_map;
        
        // Use configured values if no default supplied.
        if((p_alarmName == null) || "".equals(p_alarmName))
        {
            setAlarmName(p_alarmConfig.getAlarmName());
        }
        if((p_alarmText == null) || "".equals(p_alarmText))
        {
            setAlarmText(p_alarmConfig.getAlarmText());
        }
        if(p_alarmSeverity == null)
        {
            setAlarmSeverity(p_alarmConfig.getAlarmSeverity());
        }
        // Must create a new map from the configured map, as an alarms
        // map can be modified but the configured map must not be!
        l_map = new HashMap(p_alarmConfig.getAlarmParameterMap());
        if(p_alarmParameterMap != null)
        {
            // Merge alarm maps (overwriting any duplicates in configuration map).
            l_map.putAll(p_alarmParameterMap);
        }
        setParameterMap(l_map);
    }

}
