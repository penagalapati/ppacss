////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ServiceOfferingsPane.java
//      DATE            :       15-June-2004
//      AUTHOR          :       Mario Imfeld
//      REFERENCE       :       PpacLon#287/2729
//                              PRD_ASCS00_GEN_CA_16
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to maintain Service Offerings as
//                              part of subscriber segmentation
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.boi.dbservice.ServiceOfferingsDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Service Offerings Screen class.
 */
public class ServiceOfferingPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Private instance attributes
    //-------------------------------------------------------------------------
    /** Panel to hold the service offering selection combo box, description field and buttons. */
    private JPanel i_detailsPanel;
    
    /** Panel to hold the service offerings display table. */
    private JPanel i_codesPanel;
    
    /** Text field to display the selected service offering's description. */
    private ValidatedJTextField i_descriptionField;

    /** Table to display the service offering definitions. */    
    private JTable i_table;
    
    /** String array to hold values for the service offerings table. */
    private String i_data[][];
    
    /** Table model to hold table data and column names. */
    private StringTableModel i_stringTableModel;
    
    /** Vector containing <code>BoiServiceOfferingData</code> objects. */
    private Vector i_definedCodes;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar i_verticalScrollBar = null;
    
    //-------------------------------------------------------------------------
    //  Public constructors
    //-------------------------------------------------------------------------

    /** 
     * Simple constructor
     * @param p_context  Context object for BOI
     */
    public ServiceOfferingPane(
        Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new ServiceOfferingsDbService((BoiContext)i_context);
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------

    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_firstEmptyRowIndex = 0;
        int l_selectedRowIndex   = i_table.getSelectedRow();

        // Account for blank row in i_definedCodes when setting end of table data marker. 
        l_firstEmptyRowIndex = i_definedCodes.size() - 1;

        if (l_selectedRowIndex != -1 && (l_selectedRowIndex < l_firstEmptyRowIndex))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 1);
        }
    }

    //------------------------------------------------------------------------
    // Protected instance methods
    //------------------------------------------------------------------------
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                       BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_SERVICE_OFFERINGS_SCREEN), 
                                       i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createCodesPanel();

        i_mainPanel = WidgetFactory.createMainPanel("Service Offerings", 100, 100, 0, 0);        
        i_mainPanel.add(i_helpComboBox, "serviceOfferingsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,25");
        i_mainPanel.add(i_codesPanel, "codesPanel,1,32,100,68");
    }
    
    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_descriptionField.setText("");
        i_descriptionField.setEnabled(false);
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        i_descriptionField.setEnabled(true);
        i_descriptionField.setText(((ServiceOfferingsDbService)i_dbService).getCurrentServiceOffering().
                                       getInternalSeofData().getServiceOfferingDescription());
        selectRowInTable();
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }
    
    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((ServiceOfferingsDbService)i_dbService).getAllServiceOfferings();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }
    
    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((ServiceOfferingsDbService)i_dbService).getCurrentServiceOffering());
    }
    
    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        // No validation required
        return true;
    }
    
    /**
     * Validates screen data before update.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        // No validation required
        return true;
    }
    
    /**
     * Writes current record fields to screen data object.  Called before update.
     */  
    protected void writeCurrentRecord()
    {
        SeofServiceOfferingData l_serviceOffering;
        int                     l_code;
        
        l_code = ((BoiServiceOfferingData)i_keyDataComboBox.getSelectedItem()).getInternalSeofData().
                                                                                    getServiceOfferingNumber();
        
        l_serviceOffering = new SeofServiceOfferingData(null,
                                                        l_code,
                                                        i_descriptionField.getText(),
                                                        ((BoiContext)i_context).getOperatorUsername(),
                                                        DatePatch.getDateTimeNow());
                                    
        ((ServiceOfferingsDbService)i_dbService).setCurrentServiceOffering(
                                                    new BoiServiceOfferingData(l_serviceOffering));
    }
    
    /**
     * Writes key screen data to screen data object for use in record selection.
     */  
    protected void writeKeyData()
    {
        BoiServiceOfferingData l_serviceOffering;
        int                    l_code;
        
        l_serviceOffering = (BoiServiceOfferingData)i_keyDataComboBox.getSelectedItem();
        l_code = l_serviceOffering.getInternalSeofData().getServiceOfferingNumber();
        
        ((ServiceOfferingsDbService)i_dbService).setCurrentCode(l_code);
    }
    
    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------

    /**
     * Creates the Details Panel and adds components to it.
     */    
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_descriptionLabel = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 25, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Code:");
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesComboBox,17,1,70,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,6,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,6,50,4");
        
        i_detailsPanel.add(i_updateButton, "updateButton,1,15,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,17,15,15,5");
    }

    /**
     * Creates the Codes Panel and adds a scroll pane to it.
     */    
    private void createCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_codesPanel = WidgetFactory.createPanel("Defined Codes", 100, 68, 0, 0);
        
        String[] l_columnNames = { "Code", "Description" };       
        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        i_codesPanel.add(l_scrollPane, "i_table,1,1,100,68");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    /**
     * Populates the service offerings table.
     */
    private void populateCodesTable()
    {
        SeofServiceOfferingData l_chargingData;
        int                     l_numRecords = 0;
        int                     l_arraySize = 0;
        
        // Account for blank row in i_definedCodes when setting l_numRecords. 
        l_numRecords = i_definedCodes.size() - 1;
        l_arraySize = ((l_numRecords > 26) ? l_numRecords : 26);
        i_data = new String[l_arraySize][2];

        // Start with second element of i_definedCodes as we dont want "" element.
        for (int i=1, j=0; i < l_numRecords + 1; i++, j++)
        {
            l_chargingData = ((BoiServiceOfferingData)i_definedCodes.elementAt(i)).getInternalSeofData();
            i_data[j][0] = String.valueOf(l_chargingData.getServiceOfferingNumber());
            i_data[j][1] = l_chargingData.getServiceOfferingDescription();
        }

        i_stringTableModel.setData(i_data);
    }
    
    /**
     * Selects the row in  table that corresponds to the selected
     * item in the combo box.
     * Also adjusts the view port in order to make the selected row visible.
     * @param p_table             The table whose display is being altered.
     * @param p_verticalScrollBar The vertical scrollbar belonging to the JScrollPane in this table.
     */
    private void selectRowInTable()
    {
        int l_selectionIx = -1;
        l_selectionIx = i_keyDataComboBox.getSelectedIndex() - 1;

        if (l_selectionIx >= 0)
        {
            if (l_selectionIx != i_table.getSelectedRow())
            {
                i_table.setRowSelectionInterval(l_selectionIx, l_selectionIx);
                i_verticalScrollBar.setValue(l_selectionIx * i_table.getRowHeight());
            }
        }
        else
        {
            i_verticalScrollBar.setValue(0);
        }
    }
}