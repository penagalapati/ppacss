////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiTestCaseTT.java
//      DATE            :       30-Dec-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5382
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       BOI JUnit Test Tools.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 10/04/06 | M Erskine  | Addition of job submit method   | PpacLon#2006/8373
//----------+------------+---------------------------------+--------------------
// 13/03/07 | S James    | Changes made to support Reports | PpacLon#2995/10671
//          |            | redesign                        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;

import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;
import com.slb.sema.ppas.swing.gui.GuiPane;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** BOI JUnit Test Tools. */
public class BoiTestCaseTT extends UtilTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Constant for BOI operator id. */
    protected static final String C_BOI_OPID = "SUPER";
    
    /** Constant for use in code fields. */
    protected static final String C_CODE = "99";
    
    /** Constant for use in selecting to input a new record. */
    protected static final String C_NEW_RECORD = "NEW RECORD";
    
    /** Constant for use in description fields. */
    protected static final String C_DESCRIPTION = "Dummy Description";
    
    /** Constant for use in description fields. */
    protected static final String C_NEW_DESCRIPTION = "New Dummy Description";
    
    /** Message displayed by dialog box. */
    protected static final String C_MSG_RECORD_UPDATED = "Record has been updated.";
    
    /** Message displayed by dialog box. */
    protected static final String C_MSG_RECORD_INSERTED = "New record inserted.";
    
    /** Message displayed by dialog box. */
    protected static final String C_MSG_RECORD_MADE_AVAIL = "Record has been made available.";
    
    /** Message displayed by dialog box. */
    protected static final String C_CONFIRM_DELETE_RECORD = "Are you sure you wish to delete this record?";
    
    /** Message displayed by dialog box. */
    protected static final String C_CONFIRM_OVERWRITE_RECORD = "A withdrawn record with this key already " +
                                                             "exists. Do you wish to overwrite it?";
    
    /** Message displayed by dialog box. */
    protected static final String C_CONFIRM_MAKE_RECORD_AVAIL = "Do you wish to make the withdrawn record " +
                                                              "available again?";
    
    /** Message displayed by dialog box. */
    private static final String C_CONFIRM_SUBMIT_JOB = "Do you wish to submit this job?";
    
    /** Message displayed by dialog box. */
    private static final String C_MSG_JOB_SUBMITTED = "Job has been submitted";
    
    /** Text of OK button on dialog box. */
    protected static final String C_OK_BUTTON_TEXT = UIManager.getString("OptionPane.okButtonText");
    
    /** Text of Yes button on dialog box. */
    protected static final String C_YES_BUTTON_TEXT = UIManager.getString("OptionPane.yesButtonText");
    
    /** Text of No button on dialog box. */
    protected static final String C_NO_BUTTON_TEXT = UIManager.getString("OptionPane.noButtonText");
    
    //-------------------------------------------------------------------------
    // Class attributes
    //-------------------------------------------------------------------------
    
    /** BOI context object. */
    protected static BoiContext c_context = null;
    
    /** Contains the required properties. */
    protected static PpasProperties c_properties = null; 
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    
    /** Frame in which to display the content pane for the purpose of JUnit testing. */
    protected JFrame i_frame;
    
    /** JPanel containing widgets. */
    protected Container i_contentPane;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public BoiTestCaseTT(String p_title)
    {
        super(p_title);
        
        if (c_context == null)
        {
            initContext();
        }
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Invokes the submission of a job to the Oracle scheduler.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for updates.
     */
    protected void doSubmit(GuiPane p_pane,
                            JButton p_button)
    {
        GuiPane.resetDialogBox();
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_YES_BUTTON_TEXT, 
                                       C_CONFIRM_SUBMIT_JOB);
        
        try
        {
            Thread.sleep(10000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_JOB_SUBMITTED);
    }
    
    /**
     * Invokes a DB update and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for updates.
     */
    protected void doUpdate(GuiPane p_pane,
                            JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        // Don't need to invoke later, since the application ensures that this event is
        // performed after other events in the queue.
        //TODO: Check this change is necessary - shouldn't be.
        //p_button.doClick();
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(4000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_UPDATED);

        SwingTestCaseTT.waitForState(p_pane,
                                     GuiPane.C_STATE_DATA_RETRIEVED,
                                     "State not set to data retrieved after update");
    }
    
    /**
     * Invokes a DB insert and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for inserts.
     */
    protected void doInsert(GuiPane p_pane,
                            JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        // Don't need to invoke later, since the application ensures that this event is
        // performed after other events in the queue.
        p_button.doClick();
        try
        {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_INSERTED);

        SwingTestCaseTT.waitForState(p_pane,
                                     GuiPane.C_STATE_INITIAL,
                                     "State not set to initial after insert");
    }
    
    /**
     * Invokes a DB delete and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for deletes.
     */
    protected void doDelete(GuiPane p_pane,
                            JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_YES_BUTTON_TEXT,
                                       C_CONFIRM_DELETE_RECORD);
        
        SwingTestCaseTT.waitForState(p_pane,
                                     GuiPane.C_STATE_INITIAL,
                                     "State not set to initial after delete");
    }
    
    /**
     * Attempts to make a record available in the DB, and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used to make records available.
     */
    protected void doMakeAvailable(GuiPane p_pane,
                                   JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(1,
                                       C_NO_BUTTON_TEXT,
                                       C_CONFIRM_OVERWRITE_RECORD);
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        

        SwingTestCaseTT.checkDialogBox(0,
                                       C_YES_BUTTON_TEXT,
                                       C_CONFIRM_MAKE_RECORD_AVAIL);

        // Ensure we don't get the previous dialog box again.
        //TODO: Check why this is necessary...
        //GuiPane.resetDialogBox();
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_MADE_AVAIL);

        SwingTestCaseTT.waitForState(p_pane,
                                     GuiPane.C_STATE_INITIAL,
                                     "State not set to initial after makeAvailable");
    }

    /**
     * Initialises data for JUnit testing.
     * @param p_pane BOI screen
     */
    protected void init(GuiPane p_pane)
    {
        i_contentPane = p_pane.getContentPane();
        
        javax.swing.SwingUtilities.invokeLater(new TestRunnable());
    }
    
    /**
     * Updates a config record in the DB,
     * @param p_sqlString SQL to execute.
     * @param p_className Name of calling class.
     * @param p_methodName Name of calling method.
     */
    protected void sqlConfigUpdate(SqlString p_sqlString,
                                   String    p_className,
                                   String    p_methodName)
    {
        JdbcStatement l_statement = null;

        try
        {
            l_statement = c_context.getConnection().createStatement(p_className,
                                                                    p_methodName,
                                                                    10000,
                                                                    this,
                                                                    null);
            
            l_statement.executeUpdate(p_className,
                                      p_methodName,
                                      10010,
                                      this,
                                      null,
                                      p_sqlString);
        }
        catch (PpasSqlException l_pSqlE)
        {
            fail("Database update failed. Exception: " + l_pSqlE);
            l_pSqlE.printStackTrace();
        }
        finally
        {
            if (l_statement != null)
            {
                try
                {
                    l_statement.close(p_className, p_methodName, 10020, this, null);
                }
                catch (PpasSqlException l_pSqlE)
                {
                    // Do nothing
                }
            }            
        }
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Initialises the BOI context.
     */
    private void initContext()
    {
        String l_databaseUsername;
        String l_databasePassword;
        String l_databaseUrl;
        
        try
        {
            c_properties = new PpasProperties();
            c_properties.loadLayeredProperties("BOI_SRV_00001");
            c_properties = c_properties.getPropertiesWithAddedLayers("js");

            
            l_databaseUsername = c_properties.getTrimmedProperty(
            "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.username");
            l_databasePassword = c_properties.getTrimmedProperty(
            "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.password");
            l_databaseUrl = c_properties.getTrimmedProperty(
            "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.dbUrl");
            
            c_context = new BoiContext();
            c_context.setUsername(l_databaseUsername);
            c_context.setPassword(l_databasePassword);
            c_context.setDbString(l_databaseUrl);
            
            c_context.setOperatorUsername(C_BOI_OPID);
            c_context.addObject("LoggedIn", new Boolean(true));

            c_context.createConnection();
            BoiHelpTopics.init();
            c_context.addObject("BoiHelpListener", new BoiHelpListener(c_context));
        }
        catch (PpasConfigException l_e)
        {
            fail(l_e.toString());
        }        
        catch (PpasSqlException l_SqlE)
        {
            fail(l_SqlE.toString());
        }        
    }
    
    /** Get the maximum time (in seconds) this test is allowed to run.
     *
     * @return Maximum number of seconds this test is allowed to run.
     */
    protected int getMaxRuntime()
    {
        return 1800;
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to ensure that all the screen is created on the event dispatching thread. 
     */
    public class TestRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Ensure that the screen components are displayable, otherwise the requestFocus calls
            // in the application will not work.
            i_frame = new JFrame();
            //JFrame.setDefaultLookAndFeelDecorated(true);
            i_frame.getContentPane().add(i_contentPane);
            i_frame.setVisible(true);
        }
    }
}
