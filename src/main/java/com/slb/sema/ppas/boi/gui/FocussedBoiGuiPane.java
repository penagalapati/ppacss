////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       FocussedBoiGuiPane.java
//    DATE            :       12-Oct-2003
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#2590/10182
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Superclass for Focussable BOI panes.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.Exceptionable;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;

/** Superclass for Focussable BOI panes. */
abstract public class FocussedBoiGuiPane extends FocussedGuiPane
{
    /**
     * Standard constructor.
     * @param p_context Context object.
     * @param p_container Panel displaying the data corresponding to the menu selection.
     */
    public FocussedBoiGuiPane(Context   p_context,
                              Container p_container)
    {
        super(p_context, p_container);
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Dumps the exception and all nested exceptions.
     */
    protected void dumpException(Exception p_ex)
    {
        if (p_ex instanceof Exceptionable)
        {
            Exceptionable l_ex = (Exceptionable)p_ex;
            System.err.println("[ASCS EXCEPTION]: " + l_ex.getMessage());
            System.err.println(l_ex.getMessageText());
            
            if (l_ex.getSourceException() != null)
            {
                System.err.println("Originating...");
                dumpException(l_ex.getSourceException());
            }
        }
        else
        {
            System.err.println("[OTHER EXCEPTION]: " + p_ex);
            System.err.println(PpasException.stackTraceToString(p_ex));
        }
    }

}
