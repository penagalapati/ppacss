////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AgentInformationPane.java
//      DATE            :       28-Nov-2005
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#1755/7500
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Agent Information screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 07/11/06 | Yang Liu   | Set selected row in table and   | PpacLon#2079/10278
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don't    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
// 20-feb-07| huy        | Changed code to draw table with | PpacLon#2069/10998
//          |            | defined column widths for table.|
//----------+------------+---------------------------------+--------------------
// 30/03/07 | Andy Harris| Added suspect flag field.       | PpacLon#3011/11241
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import com.slb.sema.ppas.boi.boidata.BoiAgentInfoData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.dbservice.AgentInfoDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Agent Information screen.
 */
public class AgentInformationPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Available markets vector. */
    private Vector              i_markets;
    
    /** Existing Agent codes vector. */
    private Vector              i_definedCodes      = null;

    /** Panel allowing data selection and modification. */
    private JPanel              i_detailsPanel      = null;

    /** Panel containing table of existing records. */
    private JPanel              i_definedCodesPanel = null;

    /** Agents code field. */
    private ValidatedJTextField i_codeField         = null;

    /** Agents description field. */
    private ValidatedJTextField i_descriptionField  = null;

    /** Table containing existing records. */
    private JTable              i_table             = null;

    /** Data array for table of existing records. */
    private String              i_data[][]          = null;

    /** Data model for table of existing records. */
    private StringTableModel    i_stringTableModel  = null;

    /** Column names for table of existing records. */
    private String[]            i_columnNames       = {"Code", "Description", "Suspect"};

    /** Column width sizes for table of existing records. */
    private int[]               i_columnWidths      = {250, 480, 59};

    /** The vertical scroll bar used for the agent records table view port. */
    private JScrollBar          i_verticalScrollBar = null;
    
    private JComboBox           i_suspectCombo = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * AgentInformationPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public AgentInformationPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new AgentInfoDbService((BoiContext)i_context);

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }

    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Agent Information", 100, 100, 0, 0);

        //Change Help later on. For now using another help.
        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_AGENT_INFORMATION), i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();

        i_mainPanel.add(i_helpComboBox, "dedAccountsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Description cannot be blank");
            return false;
        }
        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object. This method is called before checking
     * for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        String l_code = null;

        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        l_code = i_codeField.getText();
        ((AgentInfoDbService)i_dbService).setCurrentCode(l_code);
        return true;
    }

    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        SrvaAgentMstrData l_agentData = null;
        BoiMarket         l_market    = null;
        String l_sus = null;
        
        l_sus = ((String)i_suspectCombo.getSelectedItem());
        

        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();

        l_agentData = new SrvaAgentMstrData(null,
                                            l_market.getMarket(),
                                            i_codeField.getText(),
                                            i_descriptionField.getText(),
                                            null,
                                            null,
                                            ' ',
                                            l_sus.equals("") ? ' ' : l_sus.charAt(0));

        ((AgentInfoDbService)i_dbService).setAgentInfoData(new BoiAgentInfoData(l_agentData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiAgentInfoData l_selectedItem = null;
        String l_code = null;

        if (i_keyDataComboBox.getSelectedItem() instanceof BoiAgentInfoData)
        {
            l_selectedItem = (BoiAgentInfoData)i_keyDataComboBox.getSelectedItem();
            l_code = l_selectedItem.getInternalAgentInfoData().getAgent();
            ((AgentInfoDbService)i_dbService).setCurrentCode(l_code);
        }
    }

    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_suspectCombo.setSelectedItem(" ");
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
        
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_suspectCombo.setEnabled(false);
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        SrvaAgentMstrData l_agentData = null;

        l_agentData = ((AgentInfoDbService)i_dbService).getAgentInfoData().getInternalAgentInfoData();
        i_codeField.setText(l_agentData.getAgent());
        i_descriptionField.setText(l_agentData.getAgentName());
        i_suspectCombo.setSelectedItem("" + l_agentData.getSuspectFlag());
        
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((AgentInfoDbService)i_dbService).getAvailableAgentinfoData();
        
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((AgentInfoDbService)i_dbService).getAvailableMarketData();
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((AgentInfoDbService)i_dbService).getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);

        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((AgentInfoDbService)i_dbService).getAgentInfoData());
    }

    /**
     * Refreshes list data after a new market has been selected from the market combo box.
     */
    protected void refreshDataAfterMarketSelect()
    {
        refreshListData();
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
        i_suspectCombo.setEnabled(true);
    }

    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
        i_suspectCombo.setEnabled(true);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Creates the panel containing the defined codes and the required buttons.
     */
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_marketLabel = null;
        JLabel l_codeLabel = null;
        JLabel l_descriptionLabel = null;
        JLabel l_suspectLabel = null;

        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);

        l_marketLabel = WidgetFactory.createLabel("Market:");

        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");

        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createTextField(8);
        addValueChangedListener(i_codeField);

        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(40);
        addValueChangedListener(i_descriptionField);
        
        l_suspectLabel = WidgetFactory.createLabel("Suspect Flag:");
        Vector l_vector = new Vector();
        l_vector.add(" ");
        l_vector.add("Y");
        l_vector.add("N");
        i_suspectCombo = WidgetFactory.createComboBox(new DefaultComboBoxModel(l_vector));
        addValueChangedListener(i_suspectCombo);

        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketDataComboBox,17,1,35,4");

        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,6,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,6,80,4");

        i_detailsPanel.add(l_codeLabel, "codeLabel,1,11,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,11,15,4");

        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,16,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,16,70,4");
        
        i_detailsPanel.add(l_suspectLabel, "suspectLabel,1,21,15,4");
        i_detailsPanel.add(i_suspectCombo, "suspectCombo,17,21,7,4");

        i_detailsPanel.add(i_updateButton, "updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,27,15,5");
    }

    /**
     * Creates the panel containing the defined codes displayed in a table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;

        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);

        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(i_columnWidths,
                                            150,
                                            200,
                                            i_stringTableModel,
                                            this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        i_definedCodesPanel.add(l_scrollPane, "i_table,1,1,100,60");

        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    /**
     * Populates the table in the defined codes panel.
     */
    private void populateCodesTable()
    {
        SrvaAgentMstrData l_agentData = null;
        int l_numRecords = 0;
        int l_arraySize = 0;

        l_numRecords = i_definedCodes.size() -2;
        l_arraySize = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data = new String[l_arraySize][3];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_definedCodes.size(); i++, j++)
        {
            l_agentData = ((BoiAgentInfoData)i_definedCodes.elementAt(i)).getInternalAgentInfoData();
            i_data[j][0] = l_agentData.getAgent();
            i_data[j][1] = l_agentData.getAgentName();
            i_data[j][2] = "" + l_agentData.getSuspectFlag();
        }
        i_stringTableModel.setData(i_data);

    }
}
