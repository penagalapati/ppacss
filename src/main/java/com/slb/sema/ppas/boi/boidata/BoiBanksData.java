////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiBanksData.java
//      DATE            :       19-Oct-2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PpacLon#1755/7279
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Wrapper for BankBankData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankData;
import com.slb.sema.ppas.common.dataclass.DataObject;


/**
 * Wrapper for bank data class within BOI.
 */
public class BoiBanksData extends DataObject
{
    /** Basic bank data object. */
    private BankBankData i_bankData = null;
    
    /**
     * Simple constructor.
     * @param p_bankData Bank data object to wrapper.
     */
    public BoiBanksData(BankBankData p_bankData)
    {
        i_bankData = p_bankData;
    }

    /**
     * Return wrappered bank data object.
     * @return Wrappered bank data object.
     */
    public BankBankData getInternalBankData()
    {
        return i_bankData;
    }

    /**
     * Compares two bank data objects and returns true if they equate.
     * 
     * @param p_bankData Bank data object to compare the current instance with.
     * 
     * @return <code>true</code> if both instances are equal, otherwise <code>false</code>.
     */
    public boolean equals(Object p_bankData)
    {
        boolean l_return = false;
        
        if ( p_bankData != null &&
             p_bankData instanceof BoiBanksData &&
             ((BoiBanksData)p_bankData).i_bankData.getBankCode().equals(i_bankData.getBankCode()) &&
             ((BoiBanksData)p_bankData).i_bankData.getBankName().equals(i_bankData.getBankName()))
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns bank data object as a String for display in BOI.
     * @return Bank data object as a string.
     */
    public String toString()
    {
        StringBuffer l_displayString = new StringBuffer(i_bankData.getBankCode());
        l_displayString.append(" - "); 
        l_displayString.append(i_bankData.getBankName());
        return l_displayString.toString();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_bankData.getBankCode().length();
    }
}
