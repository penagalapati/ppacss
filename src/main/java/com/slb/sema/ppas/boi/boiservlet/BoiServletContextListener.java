////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiServletContextListener.Java
//      DATE            :       29/09/05
//      AUTHOR          :       Erik Clayton
//
//      COPYRIGHT       :       WM-DATA 2005
//
//      DESCRIPTION     :       BOI servlet context listener.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boiservlet;

import javax.servlet.ServletContext;

import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.web.servlet.PpasServletContextListener;

/**
 * BOI servlet context listener. Servlet context listeners allow applications
 * to listen for and act on start up or shutdown of their context (i.e.
 * start up or shut down of the servlet application).
 */
public class BoiServletContextListener
extends PpasServletContextListener
{

    /**
     * Performs any initialisation for the BOI context on start up. Currently
     * this is all done by the startup servlet and not here.
     *
     * @param  p_servletContext The servlet context which is being initialised.
     */
    public void doContextInitialised(
        ServletContext          p_servletContext
        )
    {
        // Currently do nothing. In future should move startup code here!

        return;
    }

    /**
     * Performs any tidy up for the BOI context on shut down.
     *
     * @param  p_servletContext The servlet context which is being destroyed.
     */
    public void doContextDestroyed(
        ServletContext          p_servletContext
        )
    {
        PpasContext                       l_context = null;

        //
        // Do shutdown stuff here.
        //

        l_context = (PpasContext)p_servletContext.getAttribute(
                                         "com.slb.sema.ppas.common.support.PpasContext");

        // Protected again a null context (e.g. start up problems).
        if(l_context != null)
        {
            l_context.destroy();
        }

        return;
    }

}
