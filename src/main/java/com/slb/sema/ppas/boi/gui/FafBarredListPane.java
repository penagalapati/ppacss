////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME         :       FafBarredListPane.java
//      DATE              :       26-Jan-2005
//      AUTHOR            :       Martin Brister
//      REFERENCE         :       PpacLon#970/5611
//
//      COPYRIGHT         :       WM-data 2005
//
//      DESCRIPTION       :       Family and Friends Barred Numbers screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.slb.sema.ppas.boi.boidata.BoiFafBarredListData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.dbservice.FafBarredListDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Accumulators screen.
 */
public class FafBarredListPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Maximum length of MSISDN. */
    private static final int C_MAX_MSISDN_LENGTH = 15;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Panel allowing data selection and modification. */
    private JPanel i_detailsPanel;
    
    /** Panel containing table of existing records. */
    private JPanel i_definedCodesPanel;
    
    /** Available markets vector. */
    private Vector i_markets;
    
    /** Existing FaF barred numbers vector. */
    private Vector i_definedNumbers;
    
    /** FaF number prefix field. */
    private JFormattedTextField i_numberPrefixField;
    
    /** FaF number length field. */
    private JFormattedTextField i_numberLengthField;
    
    /** Radio button to select all number lengths. */
    private JRadioButton i_allLengthsRadioButton;
    
    /** Radio button to select a specified number length. */
    private JRadioButton i_specifiedLengthRadioButton;
    
    /** FaF number start date field. */
    private JFormattedTextField i_startDateField;
    
    /** FaF number start date field. */
    private JFormattedTextField i_endDateField;
    
    /** Table containing existing records. */
    private JTable i_table;
    
    /** Column names for table of existing records. */
    private String[] i_columnNames = {"Number Prefix", "Number Length", "Start Date", "End Date"};
    
    /** Data array for table of existing records. */
    private String i_data[][];
    
    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar          i_verticalScrollBar = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * FaF Barred Numbers Pane constructor.
     * @param p_context A reference to the BoiContext
     */
    public FafBarredListPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new FafBarredListDbService((BoiContext)i_context);
        
        super.init();
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /**
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {   
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }    
    
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedNumbers.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    /**
     * Handles ActionEvents produced by the FaF barred numbers pane.
     * @param p_event The <code>ActionEvent</code> to handle
     */
    public void actionPerformed(ActionEvent p_event)
    {
        if (p_event.getSource() == i_allLengthsRadioButton)
        {
            i_numberLengthField.setEnabled(false);
            i_numberLengthField.setText("");
        }
        else if (p_event.getSource() == i_specifiedLengthRadioButton)
        {
            i_numberLengthField.setEnabled(true);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Family and Friends Barred Numbers", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                          BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_FAF_SCREEN), 
                                          i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox, "fafHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,40");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,47,100,54");
    }
    
    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        PpasDate              l_startDate;
        PpasDate              l_endDate;
        PpasDate              l_today;
        boolean               l_overlapFound = false;
        FabaFafBarredListData l_fabaData = null;
        String                l_numberPrefix;
        int                   l_numberLength=-1;

        l_numberPrefix = i_numberPrefixField.getText();

        if (i_numberLengthField.getText().equals(""))
        {
            l_numberLength = 0;
        }
        else
        {
            l_numberLength = Integer.parseInt(i_numberLengthField.getText());
        }
        l_startDate = new PpasDate(i_startDateField.getText());
        l_today = DatePatch.getDateToday();

        if (i_keyDataComboBox.getSelectedItem().equals("NEW RECORD"))
        {
            if (l_numberPrefix.equals(""))
            {
                i_numberPrefixField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, "Number Prefix cannot be blank.");
                return false;
            }
            
            if (i_specifiedLengthRadioButton.isSelected() && l_numberLength == 0 )
            {
                i_numberLengthField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Number Length must be set when 'Specified length' is selected.");
                return false;
            }            

            if (i_specifiedLengthRadioButton.isSelected()&& l_numberLength >30)
            {
                i_numberLengthField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Number Length must be less than or equal to 30");
                return false;
            }
            
            if (i_startDateField.getText().trim().equals(""))
            {
                i_startDateField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, "Start Date cannot be blank.");
                return false;
            }

            if (l_startDate.before(l_today))
            {
                i_startDateField.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "Start Date cannot be in the past.");
                return false;
            }
        }
        
        l_endDate = new PpasDate(i_endDateField.getText());
        
        if (l_endDate.before(l_today))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "End Date cannot be in the past.");
            return false;
        }

        if (l_endDate.before(l_startDate))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "End Date cannot be earlier than start date.");
            return false;
        }
        
        // Check for date overlap.
        if (i_keyDataComboBox.getSelectedItem().equals("NEW RECORD"))
        {
            // Ignore first 2 elements of definedNumbers vector as they hold "" and "NEW RECORD".
            // All records in definedNumbers vector are for selected market, so no need to compare.
            for (int i = 2; i < i_definedNumbers.size() && !l_overlapFound; i++)
            {
                l_fabaData = ((BoiFafBarredListData)i_definedNumbers.get(i)).getInternalFafBarredListData();
            
                if(l_numberPrefix.startsWith(l_fabaData.getNumberPrefix()) &&
                   (l_numberLength == l_fabaData.getNumberLength() || l_numberLength == 0 
                           || l_fabaData.getNumberLength() == 0) &&
                   (l_startDate.isBetween(l_fabaData.getStartDate(), l_fabaData.getEndDate()) ||
                           l_endDate.isBetween(l_fabaData.getStartDate(), l_fabaData.getEndDate()) ||
                           (l_startDate.before(l_fabaData.getStartDate()) && 
                                   (l_endDate.after(l_fabaData.getEndDate()) || !l_endDate.isSet()))))
                {
                    l_overlapFound = true;
                }
            }

            if (l_overlapFound)
            {
                if (l_numberLength == l_fabaData.getNumberLength() &&
                    l_startDate.after(l_fabaData.getStartDate()) && 
                    !l_fabaData.getEndDate().isSet())
                {
                    // FafBarredListDbService will check for this and set end date for existing row
                }
                else
                {
                    i_endDateField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane,
                                         "Start and end dates must not overlap an existing record " +
                                         "with the same market, prefix, and length.");
                    return false;
                }
            }
        }
        else
        {
            // We are updating the end date for an existing record. Check for overlap against records
            // with the same prefix and length, but don't check against the record we are updating.
            for (int i = 2; i < i_definedNumbers.size() && !l_overlapFound; i++)
            {
                l_fabaData = ((BoiFafBarredListData)i_definedNumbers.get(i)).getInternalFafBarredListData();
            
                if(l_numberPrefix.startsWith(l_fabaData.getNumberPrefix()) && 
                   (l_numberLength == l_fabaData.getNumberLength() || l_numberLength == 0
                           || l_fabaData.getNumberLength() == 0) &&
                   !l_startDate.equals(l_fabaData.getStartDate()) &&
                   (l_endDate.isBetween(l_fabaData.getStartDate(), l_fabaData.getEndDate()) ||
                           (l_startDate.before(l_fabaData.getStartDate()) && 
                                   (l_endDate.after(l_fabaData.getEndDate()) || !l_endDate.isSet()))))
                {
                    l_overlapFound = true;
                }
            }

            if (l_overlapFound)
            {
                i_endDateField.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "Start and end dates must not overlap an existing record " +
                                     "with the same market, prefix, and length.");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Validates key screen data and writes it to screen data object. This method is called before checking
     * for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_numberLength = -1;

        if (i_numberPrefixField.getText().trim().equals(""))
        {
            i_numberPrefixField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Number Prefix cannot be blank");
            return false;
        }
        
        if (i_specifiedLengthRadioButton.isSelected() &&
            (i_numberLengthField.getText().equals("") || i_numberLengthField.getText().equals("0")))
        {
            i_numberLengthField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Number Length must be set when 'Specified length' is selected.");
            return false;
        }
        
        if (i_startDateField.getText().trim().equals(""))
        {
            i_startDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Start date cannot be blank");
            return false;
        }
        
        if (i_numberLengthField.getText().equals(""))
        {
            l_numberLength = 0;
        }
        else
        {
            l_numberLength = Integer.parseInt(i_numberLengthField.getText());
        }

        ((FafBarredListDbService)i_dbService).setCurrentNumberPrefix(i_numberPrefixField.getText().trim());
        ((FafBarredListDbService)i_dbService).setCurrentNumberLength(l_numberLength);
        ((FafBarredListDbService)i_dbService).setCurrentStartDate(new PpasDate(i_startDateField.getText()));
        
        return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        FabaFafBarredListData l_fafBarredListData;
        BoiMarket             l_market = null;
        int                   l_numberLength = -1;
        
        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();
        
        if (i_numberLengthField.getText().equals(""))
        {
            l_numberLength = 0;
        }
        else
        {
            l_numberLength = Integer.parseInt(i_numberLengthField.getText());
        }
        
        l_fafBarredListData = new FabaFafBarredListData(null,
                                                        l_market.getMarket(),
                                                        i_numberPrefixField.getText().trim(),
                                                        l_numberLength,
                                                        new PpasDate(i_startDateField.getText()),
                                                        new PpasDate(i_endDateField.getText()),
                                                        null,
                                                        null);
        
        ((FafBarredListDbService)i_dbService).setFafBarredListData(
                                                    new BoiFafBarredListData(l_fafBarredListData));
    }
    
    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiFafBarredListData  l_selectedItem = null;
        FabaFafBarredListData l_fabaData = null;
        Object                l_object = null;
        
        l_object = i_keyDataComboBox.getSelectedItem();
        if (l_object instanceof BoiFafBarredListData)
        {
            l_selectedItem = (BoiFafBarredListData)l_object;
            l_fabaData = l_selectedItem.getInternalFafBarredListData();
            
            ((FafBarredListDbService)i_dbService).setCurrentNumberPrefix(l_fabaData.getNumberPrefix());
            ((FafBarredListDbService)i_dbService).setCurrentNumberLength(l_fabaData.getNumberLength());
            ((FafBarredListDbService)i_dbService).setCurrentStartDate(l_fabaData.getStartDate());
        }            
    }
    
    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_numberPrefixField.setText("");
        i_numberPrefixField.setEnabled(false);
        i_allLengthsRadioButton.setEnabled(false);
        i_specifiedLengthRadioButton.setEnabled(false);
        i_numberLengthField.setText("");
        i_numberLengthField.setEnabled(false);
        i_startDateField.setText("");
        i_startDateField.setEnabled(false);
        i_endDateField.setText("");
        i_endDateField.setEnabled(false);
        
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        FabaFafBarredListData l_fabaData = null;
        
        l_fabaData = 
            ((FafBarredListDbService)i_dbService).getFafBarredListData().getInternalFafBarredListData();
        
        i_numberPrefixField.setText(l_fabaData.getNumberPrefix());
        
        if (l_fabaData.getNumberLength() == 0)
        {
            i_allLengthsRadioButton.setSelected(true);
            i_numberLengthField.setText("");
        }
        else
        {
            i_specifiedLengthRadioButton.setSelected(true);
            i_numberLengthField.setText(l_fabaData.getNumberLength() + "");
        }
        
        i_startDateField.setValue(l_fabaData.getStartDate());
        i_endDateField.setValue(l_fabaData.getEndDate());
        selectRowInTable(i_table, i_verticalScrollBar);
    }
    
    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedNumbers = ((FafBarredListDbService)i_dbService).getFafBarredListVector();
        populateCodesTable();
        
        i_keyDataModel = new DefaultComboBoxModel(i_definedNumbers);
        i_keyDataComboBox.setModel(i_keyDataModel);  
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((FafBarredListDbService)i_dbService).getAvailableMarketData();
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(
                               ((FafBarredListDbService)i_dbService).getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);
        
        refreshListData();
    }
    
    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((FafBarredListDbService)i_dbService).getFafBarredListData());
    }
    
    /**
     * Alters screen behaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_numberPrefixField.setEnabled(true);
        i_numberLengthField.setEnabled(true);
        i_allLengthsRadioButton.setEnabled(true);
        i_specifiedLengthRadioButton.setEnabled(true);
        i_startDateField.setEnabled(true);
        i_endDateField.setEnabled(true);
    }
    
    /**
     * Alters screen behaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_numberPrefixField.setEnabled(false);
        i_numberLengthField.setEnabled(true);
        i_allLengthsRadioButton.setEnabled(false);
        i_specifiedLengthRadioButton.setEnabled(false);
        i_numberLengthField.setEnabled(false);
        i_startDateField.setEnabled(false);
        i_endDateField.setEnabled(true);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Creates the panel containing the defined codes and the required buttons.
     */
    private void createDetailsPanel()
    {
        JLabel      l_marketLabel = null;
        JLabel      l_definedNumbersLabel = null;
        JLabel      l_numberPrefixLabel = null;
        JLabel      l_numberLengthLabel = null;
        JLabel      l_startDateLabel = null;
        JLabel      l_endDateLabel = null;
        JLabel      l_allLengthsLabel = null;
        JLabel      l_specifiedLengthLabel = null;
        ButtonGroup l_buttonGroup = null;

        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 40, 0, 0);
        
        l_marketLabel = WidgetFactory.createLabel("Market:");
        
        l_definedNumbersLabel = WidgetFactory.createLabel("Defined Numbers:");
        
        l_numberPrefixLabel = WidgetFactory.createLabel("Number Prefix:");
        i_numberPrefixField = WidgetFactory.createMsisdnField(C_MAX_MSISDN_LENGTH);
        addValueChangedListener(i_numberPrefixField);
        
        i_allLengthsRadioButton = WidgetFactory.createRadioButton("", this);
        l_allLengthsLabel = WidgetFactory.createLabel("All lengths");
        l_allLengthsLabel.setHorizontalAlignment(SwingConstants.LEFT);
        i_specifiedLengthRadioButton = WidgetFactory.createRadioButton("", this);
        l_specifiedLengthLabel = WidgetFactory.createLabel("Specified length");
        l_specifiedLengthLabel.setHorizontalAlignment(SwingConstants.LEFT);
        l_buttonGroup = new ButtonGroup();
        l_buttonGroup.add(i_allLengthsRadioButton);
        l_buttonGroup.add(i_specifiedLengthRadioButton);
        
        l_numberLengthLabel = WidgetFactory.createLabel("Number Length:");
        i_numberLengthField = WidgetFactory.createIntegerField(2, false);
        addValueChangedListener(i_numberLengthField);
        
        l_startDateLabel = WidgetFactory.createLabel("Start Date:");
        i_startDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_startDateField);
        
        l_endDateLabel = WidgetFactory.createLabel("End Date:");
        i_endDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_endDateField);
        
        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketDataComboBox,17,1,35,4");
        i_detailsPanel.add(l_definedNumbersLabel, "definedNumbersLabel,1,6,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,6,40,4");
        i_detailsPanel.add(l_numberPrefixLabel, "numberPrefixLabel,1,11,15,4");
        i_detailsPanel.add(i_numberPrefixField, "numberPrefixField,17,11,20,4");
        i_detailsPanel.add(i_allLengthsRadioButton, "allLengthsRadioButton,17,16,3,4");
        i_detailsPanel.add(l_allLengthsLabel, "allLengthsLabel,20,16,15,4");
        i_detailsPanel.add(i_specifiedLengthRadioButton, "specifiedLengthRadioButton,35,16,3,4");
        i_detailsPanel.add(l_specifiedLengthLabel, "specifiedLengthLabel,38,16,20,4");
        i_detailsPanel.add(l_numberLengthLabel, "numberLengthLabel,1,21,15,4");
        i_detailsPanel.add(i_numberLengthField, "numberLengthField,17,21,10,4");
        i_detailsPanel.add(l_startDateLabel, "startDateLabel,1,26,15,4");
        i_detailsPanel.add(i_startDateField, "startDateField,17,26,15,4");
        i_detailsPanel.add(l_endDateLabel, "endDateLabel,33,26,10,4");
        i_detailsPanel.add(i_endDateField, "endDateField,44,26,15,4");
       
        i_detailsPanel.add(i_updateButton, "updateButton,1,35,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,35,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,35,15,5");
    }
    
    /**
     * Creates the panel containing the defined codes displayed in a table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Numbers", 100, 54, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        i_definedCodesPanel.add(l_scrollPane, "i_table,1,1,100,54");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
    
    /**
     * Populates the table in the defined codes panel.
     */
    private void populateCodesTable()
    {
        FabaFafBarredListData l_fabaData = null;
        int                   l_numRecords = 0;
        int                   l_arraySize = 0;
        
        l_numRecords = i_definedNumbers.size() -2;
        l_arraySize = ((l_numRecords > 18) ? l_numRecords : 19);
        i_data = new String[l_arraySize][4];
        
        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_definedNumbers.size(); i++, j++)
        {
            l_fabaData = ((BoiFafBarredListData)i_definedNumbers.elementAt(i)).getInternalFafBarredListData();

            i_data[j][0] = l_fabaData.getNumberPrefix();
            i_data[j][1] = l_fabaData.getNumberLength() == 0 ? "" : l_fabaData.getNumberLength() + "";
            i_data[j][2] = l_fabaData.getStartDate().toString();
            i_data[j][3] = l_fabaData.getEndDate().toString();
        }
        
        i_stringTableModel.setData(i_data);
    }
}