////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiMarket.java
//      DATE            :       7-Jul-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#338/3075
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for Market class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;

/** Wrapper for Market class within BOI. */
public class BoiMarket extends DataObject
{
    /** Basic Market object */
    private Market i_market;
    
    /**
     * Simple constructor.
     * @param p_market Market object to wrapper.
     */
    public BoiMarket(Market p_market)
    {
        i_market = p_market;
    }

    /**
     * Return wrappered Market object.
     * @return Wrappered Market object.
     */
    public Market getMarket()
    {
        return i_market;
    }

    /**
     * Compares two market instances and retruns true if they equate.
     * @param p_market Market to compare the current Market instance with
     * @return True if both instances are equal
     */
    public boolean equals(Object p_market)
    {
        boolean l_return = false;

        if ( p_market != null &&
             p_market instanceof BoiMarket &&
             i_market.equals(((BoiMarket)p_market).getMarket()) )
        {
            l_return = true;    
        }
        return l_return;
    }

    /**
     * Returns market as a String for display in BOI.
     * @return Market as a string.
     */
    public String toString()
    {
        String l_displayName = null;
        
        if ( (Integer.parseInt(i_market.getSloc()) == 0) || (Integer.parseInt(i_market.getSrva()) == 0) )
        {
            l_displayName = "All Markets";
        }
        else
        {
            l_displayName = i_market.getDisplayName();
        }
        
        return l_displayName;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_market.getSrva().length();
    }
}