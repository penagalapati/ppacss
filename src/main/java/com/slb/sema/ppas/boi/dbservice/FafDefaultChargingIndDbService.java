////////////////////////////////////////////////////////////////////////////////
//    ASCS
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       FafDefaultChargingIndDbService.java
//    DATE            :       15-Nov-2005
//    AUTHOR          :       Kanta Goswami
//    REFERENCE       :       PpacLon#1838/7448
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Database access class for FaF Default Charging 
//                            Indicators screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiFafChargingIndicatorsData;
import com.slb.sema.ppas.boi.boidata.BoiFafDefaultChargingIndData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.DeciDefaultChargingIndSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.FachFafChargingIndSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Family and Friends Default Charging Indicator screen. 
 */
public class FafDefaultChargingIndDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------    
        
    /** Vector of deci_default_charging_ind table records. */
    private Vector                           i_availableDeciDataV  = null;

    /** SQL service for faf default charging indicators details. */
    private DeciDefaultChargingIndSqlService i_fafDeciSqlService   = null;

    /** SQL service for faf charging indicators details. */
    private FachFafChargingIndSqlService     i_fafFachSqlService     = null;

    /** Boi default charging indicators data object. */
    private BoiFafDefaultChargingIndData     i_currentBoiDeciChargIndData = null;

    /** Vector of fach_faf_charging_ind table records. */
    private Vector                           i_availableFachDataV  = null;
 
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public FafDefaultChargingIndDbService(BoiContext p_context)
    {
        super(p_context);
        i_fafDeciSqlService = new DeciDefaultChargingIndSqlService(null);
        i_fafFachSqlService = new FachFafChargingIndSqlService(null);
        i_availableDeciDataV = new Vector(20);
        i_availableFachDataV = new Vector(20);
    }    
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
     
    /** 
     * Set Charg Ind data currently being worked on. 
     * @param p_chargIndData Deci Charg Ind data object.
     */
    public void setDeciChargIndData(BoiFafDefaultChargingIndData p_chargIndData)
    {
        i_currentBoiDeciChargIndData = p_chargIndData;
    }
    
    /**
     * Return Start Numbers of Faf Charging Indicators data.
     * @return Vector of available default faf charging indicators data records.
     */
    public Vector getAvailableDeciDataV()
    {
        return i_availableDeciDataV;
    }    
      
    /**
     * Return faf charging indicators data currently being worked on.
     * @return Faf charging indicators data object.
     */
    public BoiFafDefaultChargingIndData getDeciChargingIndData()
    {
        return i_currentBoiDeciChargIndData;
    }

    /** 
     * Return vector of available charging indicators.
     * @return Vector of available charging indicators.
     */
    public Vector getAvailableFachDataV()
    {
        return i_availableFachDataV;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        // Not required.
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        refreshDeciDataVector(p_connection);
        refreshFachDataVector(p_connection);
    }
    
    /**
     * Updates record in the deci_default_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndData l_deciDefChargIndData = i_currentBoiDeciChargIndData
                .getInternalDeciDefaultChargingIndData();
        
        i_fafDeciSqlService.update(null, 
                                   p_connection, 
                                   i_context.getOperatorUsername(), 
                                   l_deciDefChargIndData.getNumberStart(), 
                                   l_deciDefChargIndData.getNumberEnd(), 
                                   l_deciDefChargIndData.getChargingInd());

        refreshDeciDataVector(p_connection);
    }
    
    /**
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */    
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndData l_deciDefChargIndData = i_currentBoiDeciChargIndData
                .getInternalDeciDefaultChargingIndData();
        
        i_fafDeciSqlService.delete(null, 
                                   p_connection, 
                                   i_context.getOperatorUsername(), 
                                   l_deciDefChargIndData.getNumberStart());

        refreshDeciDataVector(p_connection);
    }
    
    
    /**
     * Marks a previously withdrawn record as available in the deci_default_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndData l_deciDefChargIndData = i_currentBoiDeciChargIndData
                    .getInternalDeciDefaultChargingIndData();
        
        i_fafDeciSqlService.markAsAvailable(null, 
                                            p_connection, 
                                            i_context.getOperatorUsername(),
                                            l_deciDefChargIndData.getNumberStart());

        refreshDeciDataVector(p_connection); 

    }
    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndDataSet l_deciDataSet = null;
        DeciDefaultChargingIndData l_deciData = null;
        boolean[] l_flagsArray = new boolean[2];

        l_deciDataSet = i_fafDeciSqlService.readAll(null, p_connection);
        l_deciData = l_deciDataSet.getRecord(
                         i_currentBoiDeciChargIndData.getInternalDeciDefaultChargingIndData().getNumberStart());
        
        if (l_deciData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_deciData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }

        return l_flagsArray;
    }
    
    /** 
     * Inserts record into the deci_default_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndData l_deciDefChargIndData = i_currentBoiDeciChargIndData
                .getInternalDeciDefaultChargingIndData();
        
        i_fafDeciSqlService.insert(null, 
                                   p_connection, 
                                   i_context.getOperatorUsername(), 
                                   l_deciDefChargIndData.getNumberStart(), 
                                   l_deciDefChargIndData.getNumberEnd(), 
                                   l_deciDefChargIndData.getChargingInd());

        refreshDeciDataVector(p_connection);
    }   
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    /**
     * Refreshes the available deci data vector from the deci table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshDeciDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        DeciDefaultChargingIndDataSet l_deciChargIndDataSet = null;
        DeciDefaultChargingIndData[] l_deciChargIndArray = null;
        BoiFafDefaultChargingIndData temp_Data = null;

        i_availableDeciDataV.removeAllElements();

        l_deciChargIndDataSet = i_fafDeciSqlService.readAll(null, p_connection);
        l_deciChargIndArray = l_deciChargIndDataSet.getAvailableArray();

        i_availableDeciDataV.addElement(new String(""));
        i_availableDeciDataV.addElement(new String("NEW RECORD"));

        for (int i = 0; i < l_deciChargIndArray.length; i++)
        {
            temp_Data = new BoiFafDefaultChargingIndData(l_deciChargIndArray[i]);
            i_availableDeciDataV.addElement(temp_Data);
        }
    }    
    /**
     * Refreshes the available charging indicators data vector from the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */    
    private void refreshFachDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        FachFafChargingIndDataSet l_fachFafChargIndDataSet = null;
        FachFafChargingIndData[] l_fachFafChargIndArray = null;
        BoiFafChargingIndicatorsData l_boiFafCiData = null;

        i_availableFachDataV.removeAllElements();
        i_availableFachDataV.add(0, "");

        l_fachFafChargIndDataSet = i_fafFachSqlService.readAll(null, p_connection);
        l_fachFafChargIndArray = l_fachFafChargIndDataSet.getAvailableArray();

        for (int i = 0; i < l_fachFafChargIndArray.length; i++)
        {
            l_boiFafCiData = new BoiFafChargingIndicatorsData(l_fachFafChargIndArray[i]);
            i_availableFachDataV.addElement(l_boiFafCiData);
        }
    }
}