////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DedicatedAccountsPaneUT.java
//      DATE            :       13-Jan-2005
//      AUTHOR          :       Sally Vonka
//      REFERENCE       :       PpacLon#1067/5471
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for DedicatedAccountsPane.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiDedicatedAccountsData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for AccountGroupsPane. */
public class DedicatedAccountsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "DedicatedAccountsPaneUT";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Dedicated Accounts screen. */
    private DedicatedAccountsPane        i_dedaPane;
    
    /** Dedicated Accounts code field. */
    private JFormattedTextField          i_codeField;
    
    /** Dedicated Accounts description field. */
    private ValidatedJTextField          i_descriptionField;
    
    /** market combo box. */
    private JComboBox                    i_marketDataComboBox;
    
    /** service class combo box. */
    private JComboBox                    i_serviceClassDataComboBox;
   
    /** Constant defining a single dedicated account id. */
    private static final String          C_DEDA_CODE          = "5";
    
    /** Constant defining a single dedicated account description.*/
    private static final String          C_DEDA_DESC          = "5th Account";
    
    /** Constant defining a Market for testing. */
    private static final BoiMarket       C_DEDA_MARKET        = new BoiMarket(new Market(2, 3));
    
    /** Constant defining a Service Class for testing. */
    private static final BoiMiscCodeData C_DEDA_SERVICE_CLASS = new BoiMiscCodeData(
                                                                    new MiscCodeData(
                                                                        C_DEDA_MARKET.getMarket(),
                                                                        "CLS",
                                                                        "1",
                                                                        "Personal (2,3)",
                                                                        " "));
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public DedicatedAccountsPaneUT(String p_title)
    {
        super(p_title);
        
        i_dedaPane = new DedicatedAccountsPane(c_context);
        super.init(i_dedaPane);
        
        i_dedaPane.resetScreenUponEntry();

        i_marketDataComboBox       = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "marketDataComboBox");
        i_serviceClassDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "serviceClassDataComboBox");
        i_keyDataCombo             = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "definedCodesBox");
        i_codeField                = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "codeField");
        i_descriptionField         = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "descriptionField");
        
        i_updateButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(DedicatedAccountsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Dedicated Accounts screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("DedicatedAccountsPane test");
        
        BoiDedicatedAccountsData l_boiDedaData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        SwingTestCaseTT.setMarketComboSelectedItem(i_dedaPane, 
                                                   i_marketDataComboBox, 
                                                   C_DEDA_MARKET);
        
        SwingTestCaseTT.setMarketComboSelectedItem(i_dedaPane, 
                                                   i_serviceClassDataComboBox, 
                                                   C_DEDA_SERVICE_CLASS);

        SwingTestCaseTT.setKeyComboSelectedItem(i_dedaPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_DEDA_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DEDA_DESC);
        doInsert(i_dedaPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiDedaData = createDedicatedAccountsData(C_DEDA_MARKET, 
                                                    C_DEDA_SERVICE_CLASS, 
                                                    C_DEDA_CODE, 
                                                    C_DEDA_DESC);
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_dedaPane, i_keyDataCombo, l_boiDedaData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_dedaPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_dedaPane, i_deleteButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteDedaRecord(C_DEDA_MARKET, C_DEDA_SERVICE_CLASS, C_DEDA_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteDedaRecord(C_DEDA_MARKET, C_DEDA_SERVICE_CLASS, C_DEDA_CODE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiDedicatedAccountsData object with the supplied data.
     * @param p_market Dedicated Accounts market.
     * @param p_serviceClass Dedicated Acounts Service Class.
     * @param p_dedaId Dedicated Account id.
     * @param p_dedaDesc Dedicated Account description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiDedicatedAccountsData createDedicatedAccountsData(
        BoiMarket         p_market,
        BoiMiscCodeData   p_serviceClass,
        String            p_dedaId,
        String            p_dedaDesc)
    {
        DedaDedicatedAccountsData  l_dedaData;
        String                     l_code;
        
        l_code = p_serviceClass.getInternalMiscCodeData().getCode();
        
        l_dedaData = new DedaDedicatedAccountsData(null, // p_request
                                                   p_market.getMarket(),
                                                   new ServiceClass(Integer.parseInt(l_code)),
                                                   Integer.parseInt(p_dedaId),
                                                   p_dedaDesc);
        
        return new BoiDedicatedAccountsData(l_dedaData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteDedaRecord = "deleteDedaRecord";
    
    /**
     * Removes a row from deda_dedicated_accounts.
     * @param p_dedicatedAccountId Dedicated Account id.
     * @param p_market
     * @param p_serviceClass
     */
    private void deleteDedaRecord(
        BoiMarket       p_market,
        BoiMiscCodeData p_serviceClass,
        String       p_dedicatedAccountId)
    {
        String           l_sql;
        SqlString        l_sqlString;
        
        l_sql = "DELETE from deda_dedicated_accounts " +
                           "WHERE deda_srva          = {0} " +
                           "AND   deda_sloc          = {1} " +
                           "AND   deda_service_class = {2} " +
                           "AND   deda_id            = {3} ";
        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setIntParam     (0, p_market.getMarket().getSrva());
        l_sqlString.setIntParam     (1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam  (2, p_serviceClass.getInternalMiscCodeData().getCode());
        l_sqlString.setIntParam     (3, p_dedicatedAccountId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteDedaRecord);
    }
}
