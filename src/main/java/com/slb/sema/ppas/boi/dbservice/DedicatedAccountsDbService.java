////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DedicatedAccountsDbService.java
//      DATE            :       07-Jan-2005
//      AUTHOR          :       Sally J. Vonka
//      REFERENCE       :       PpacLon#1067/5430
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Database access class for Dedicated Accounts screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiDedicatedAccountsData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.DedaDedicatedAccountsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
* Database access class for Dedicated Account screen.
*/
public class DedicatedAccountsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------        
    
    /** Identifies the misc_table column in srva_misc_codes DB table. */
    public static final String C_MISC_TABLE_SERVICE_CLASS = "CLS";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** SQL service for dedicated account details. */
    private DedaDedicatedAccountsSqlService i_dedaSqlService            = null;
    
    /** Vector of dedicated account records. */
    private Vector                    i_availableDedicatedAccountsDataV = null;
    
    /** Boi Dedicated Account data object. */
    private BoiDedicatedAccountsData  i_dedicatedAccountsData           = null;
    
    /** Currently selected dedicated account code. Used as key for read and delete. */
    private long                      i_currentCode                     = -1;
    
    /** Vector of available markets for display. */
    private Vector                    i_availableMarketsV;
    
    /** Vector of miscellaneous code records. */
    private Vector                    i_availableMiscCodeDataV;
    
    /** SQL service for Misc Code. */
    private MiscCodeSqlService        i_miscCodeSqlService;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService  i_operatorSqlService;
    
    /** Operator's default market. */
    private BoiMarket                 i_defaultMarket                   = null;
    
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public DedicatedAccountsDbService(BoiContext p_context)
    {
        super(p_context);
        
        i_dedaSqlService                  = new DedaDedicatedAccountsSqlService(null, null);
        i_miscCodeSqlService              = new MiscCodeSqlService (null, null);
        i_operatorSqlService              = new BicsysUserfileSqlService(null, null);
        i_availableDedicatedAccountsDataV = new Vector(5);
        i_availableMiscCodeDataV          = new Vector(5);
        i_availableMarketsV               = new Vector(5);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }
    
    /** 
     * Return available Misc Code data records. 
     * @return Vector of available Misc Code data records.
     */
    public Vector getAvailableMiscCodeData()
    {
        return i_availableMiscCodeDataV;
    }
    
    
    /**
     * Set current Dedicated Account. Used for record lookup in database.
     * @param p_code Currently selected dedicated account code.
     */
    public void setCurrentCode(long p_code)
    {
        i_currentCode = p_code;
    }
    
    /**
     * Return Dedicated Account data currently being worked on.
     * @return Dedicated Account data object.
     */
    public BoiDedicatedAccountsData getDedicatedAccountsData()
    {
        return i_dedicatedAccountsData;
    }
    
    /**
     * Set Dedicated Account data currently being worked on.
     * @param p_dedicatedAccountsData Dedicated Accounts data object.
     */
    public void setDedicatedAccountsData(BoiDedicatedAccountsData p_dedicatedAccountsData)
    {
        i_dedicatedAccountsData = p_dedicatedAccountsData;
    }
    
    /**
     * Return Dedicated Account data.
     * @return Vector of available Dedicated Account data records.
     */
    public Vector getAvailableDedicatedAccountsData()
    {
        return i_availableDedicatedAccountsDataV;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        DedaDedicatedAccountsDataSet l_dedaDataSet     = null;
        DedaDedicatedAccountsData    l_dedaData        = null;
        String                       l_miscCode        = null;
        ServiceClass                 l_serviceClass    = null;
        String                       l_dedaDescription = null;
        
        l_miscCode        = i_currentServiceClass.getInternalMiscCodeData().getCode();
        l_serviceClass    = new ServiceClass(new Integer(l_miscCode).intValue());
        l_dedaDataSet     = i_dedaSqlService.read(null, p_connection);
        l_dedaDescription = l_dedaDataSet.getDescription(i_currentMarket.getMarket(), 
                                                         l_serviceClass, 
                                                         i_currentCode);
        l_dedaData = new DedaDedicatedAccountsData(null,
                                                   i_currentMarket.getMarket(),
                                                   l_serviceClass,
                                                   i_currentCode,
                                                   l_dedaDescription);
        
        i_dedicatedAccountsData = new BoiDedicatedAccountsData(l_dedaData);
    }
    
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;
        
        //System.out.println("MIE entered DedicatedAccountDbService.readInitial()");
        //System.out.println("    i_currentMarket: " + i_currentMarket);
        //System.out.println("     i_currentAdjustmentType: " + i_currentServiceClass);
        
        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;        
        }
            
        if (i_currentServiceClass == null)
        {
            refreshMiscCodeDataVector(p_connection, i_currentMarket.getMarket());
            if (i_availableMiscCodeDataV.size() != 0)
            {
                i_currentServiceClass = (BoiMiscCodeData)i_availableMiscCodeDataV.firstElement();
            }
        }

        refreshDedicatedAccountsDataVector(p_connection);
    }
    
    /**
     * Inserts record into the Deda_Dedicated_Accounts table.
     * @param  p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        DedaDedicatedAccountsData l_dedicatedAccountsData = null;
        
        l_dedicatedAccountsData = i_dedicatedAccountsData.getInternalDedicatedAccountsData();
        
        i_dedaSqlService.insert(null,
                                p_connection,
                                l_dedicatedAccountsData.getDedaMarket(),
                                l_dedicatedAccountsData.getDedaServiceClass(),
                                l_dedicatedAccountsData.getDedaAccountId(),
                                l_dedicatedAccountsData.getDedaDescription(),
                                i_context.getOperatorUsername());
        
        refreshDedicatedAccountsDataVector(p_connection);
    }
    
    /**
     * Updates record in the DEDA_Dedicated_Account table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        DedaDedicatedAccountsData l_dedicatedAccountsData = null;
        
        l_dedicatedAccountsData = i_dedicatedAccountsData.getInternalDedicatedAccountsData();
        
        i_dedaSqlService.update(null,
                                p_connection,
                                l_dedicatedAccountsData.getDedaMarket(),
                                l_dedicatedAccountsData.getDedaServiceClass(),
                                l_dedicatedAccountsData.getDedaAccountId(),
                                l_dedicatedAccountsData.getDedaDescription(),
                                i_context.getOperatorUsername());
        
        refreshDedicatedAccountsDataVector(p_connection);
    }
    
    /**
     * Deletes record from the DEDA_Dedicated_Account table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        DedaDedicatedAccountsData l_dedicatedAccountsData = null;
        
        l_dedicatedAccountsData = i_dedicatedAccountsData.getInternalDedicatedAccountsData();
        
        i_dedaSqlService.delete(
                                null,
                                p_connection,
                                l_dedicatedAccountsData.getDedaMarket(),
                                l_dedicatedAccountsData.getDedaServiceClass(), 
                                i_currentCode);
        
        refreshDedicatedAccountsDataVector(p_connection);
    }
    
    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) 
        throws PpasSqlException
    {
        DedaDedicatedAccountsDataSet l_dedaDataSet = null;
        boolean[]                    l_flagsArray  = new boolean[2];
        ServiceClass                 l_serviceClass;
        String                       l_code;
        String                       l_description;
        
        l_code         = i_currentServiceClass.getInternalMiscCodeData().getCode();
        l_serviceClass = new ServiceClass(new Integer(l_code).intValue());
        l_dedaDataSet  = i_dedaSqlService.read(null, p_connection);
        l_description  = l_dedaDataSet.getDescription(i_currentMarket.getMarket(),
                                                      l_serviceClass,
                                                      i_currentCode);
        
        if (!l_description.equalsIgnoreCase(""))
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = false;
        }
        else
        {
           l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshMiscCodeDataVector(JdbcConnection p_connection,
                                           Market         p_market)
    throws PpasSqlException
    {
        MiscCodeDataSet   l_miscCodeDataSet;
        MiscCodeDataSet   l_availableMiscCodeDataSet;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;

        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet          = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getRawAvailableMiscCodeData(
                                         p_market, 
                                         C_MISC_TABLE_SERVICE_CLASS);
        
        l_miscCodeV        = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData        = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableMiscCodeDataV.addElement(l_boiMiscCodeArray[j]);
        }
    }
    
    /**
     * Refreshes the available charging data vector from the Deda_Dedicated_Accounts table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshDedicatedAccountsDataVector(JdbcConnection p_connection) 
        throws PpasSqlException
    {
        DedaDedicatedAccountsDataSet l_dedaDataSet  = null;
        DedaDedicatedAccountsData[]  l_dedaArray    = null;
        ServiceClass                 l_serviceClass = null;
        String                       l_code         = null;
        
        i_availableDedicatedAccountsDataV.removeAllElements();
        
        l_dedaDataSet = i_dedaSqlService.read(null, p_connection);

        if (i_currentServiceClass != null)
        {
            l_code         = i_currentServiceClass.getInternalMiscCodeData().getCode();
            l_serviceClass = new ServiceClass(new Integer(l_code).intValue());
            l_dedaArray    = l_dedaDataSet.getDedaArray(i_currentMarket.getMarket(), l_serviceClass);
        }
        
        i_availableDedicatedAccountsDataV.addElement(new String(""));
        i_availableDedicatedAccountsDataV.addElement(new String("NEW RECORD"));
        if (l_dedaArray != null)
        {
            for (int i = 0; i < l_dedaArray.length; i++)
            {
                i_availableDedicatedAccountsDataV.addElement(new BoiDedicatedAccountsData(l_dedaArray[i]));
            }
        }
    }
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
    throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}
