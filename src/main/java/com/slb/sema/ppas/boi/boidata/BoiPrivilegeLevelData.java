////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiPrivilegeLevelData.java
//      DATE            :       16-Sep-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#591/4148
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for PrilPrivilegeLevelData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;

/** Wrapper for privilege level data class within BOI. */
public class BoiPrivilegeLevelData extends DataObject
{
    /** Basic privilege level data object */
    private PrilPrivilegeLevelData i_privilegeLevelData;
    
    /**
     * Simple constructor.
     * @param p_privilegeLevelData Privilege level data object to wrapper.
     */
    public BoiPrivilegeLevelData(PrilPrivilegeLevelData p_privilegeLevelData)
    {
        i_privilegeLevelData = p_privilegeLevelData;
    }

    /**
     * Return wrappered privilege level data object.
     * @return Wrappered privilege level data object.
     */
    public PrilPrivilegeLevelData getInternalPrivilegeLevelData()
    {
        return i_privilegeLevelData;
    }

    /**
     * Compares two privilege level data objects and returns true if they equate.
     * @param p_privilegeLevelData Privilege level data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_privilegeLevelData)
    {
        boolean l_return = false;
        
        if ( p_privilegeLevelData != null &&
             p_privilegeLevelData instanceof BoiPrivilegeLevelData &&
             i_privilegeLevelData.getPrivilegeLevel() == 
                 ((BoiPrivilegeLevelData)p_privilegeLevelData).getInternalPrivilegeLevelData().getPrivilegeLevel())
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns privilege level data object as a String for display in BOI.
     * @return Privilege level data object as a string.
     */
    public String toString()
    {
        return i_privilegeLevelData.getDisplayString();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_privilegeLevelData.getPrivilegeLevel();
    }
}