package com.slb.sema.ppas.boi.gui;

public class GridPlacementSpecifier
{
    private static final int C_BOI_ROW_SPAN = 4;
    
    private String i_componentName;
    
    private int i_xCoord;
    
    private int i_yCoord;
    
    private int i_columnSpan;
    
    private int i_rowSpan;
    
    public GridPlacementSpecifier(String p_componentName,
                                  int    p_xCoord,
                                  int    p_yCoord,
                                  int    p_columnSpan,
                                  int    p_rowSpan)
    {
        i_componentName = p_componentName;
        i_xCoord = p_xCoord;
        i_yCoord = p_yCoord;
        i_columnSpan = p_columnSpan;
        i_rowSpan = p_rowSpan;
    }
    
    public GridPlacementSpecifier(String p_componentName,
                                  int    p_xcoord,
                                  int    p_ycoord,
                                  int    p_columnSpan)
    {
        this(p_componentName, p_xcoord, p_ycoord, p_columnSpan, C_BOI_ROW_SPAN);
    }
    
    public String toString()
    {
        StringBuffer l_sb = new StringBuffer("");
        l_sb.append(i_componentName);
        l_sb.append(",");
        l_sb.append(i_xCoord);
        l_sb.append(",");
        l_sb.append(i_yCoord);
        l_sb.append(",");
        l_sb.append(i_columnSpan);
        l_sb.append(",");
        l_sb.append(i_rowSpan);
        
        return l_sb.toString();
    }
}
