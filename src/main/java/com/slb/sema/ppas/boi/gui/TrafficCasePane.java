////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafficCasePane.java
//      DATE            :       10-Mar-2006
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#2035/8106
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       BOI screen to maintain traffic cases.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiTrafficCaseData;
import com.slb.sema.ppas.boi.dbservice.TrafficCaseDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Traffic Cases Groups Screen class.
 */
public class TrafficCasePane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel i_detailsPanel = null;

    /** Panel containing table of existing records. */
    private JPanel i_definedCodesPanel = null;

    /** Existing Traffic Case codes vector. */
    private Vector i_definedCodes = null;   
    
    /** Traffic Case code field. */
    private JFormattedTextField i_codeField = null;

    /** Traffic Case description field. */
    private ValidatedJTextField i_descriptionField = null;
    
    /** Traffic Case short description field. */
    private ValidatedJTextField i_shortDescriptionField = null;
    
    /** Table containing existing records. */
    private JTable i_table = null;

    /** Column names for table of existing records. */
    private String[] i_columnNames = { "Code", "Short Description", "Description" };

    /** Data array for table of existing records. */
    private String i_data[][] = null;

    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel = null;
    
    /** The vertical scroll bar used for the traffic case table view port. */
    private JScrollBar i_verticalScrollBar = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    
    /** 
     * TrafficCasePane constructor. 
     * @param p_context A reference to the BoiContext
     */        
    public TrafficCasePane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new TrafficCaseDbService((BoiContext)i_context);        
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------    
        
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }
    
    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
        i_shortDescriptionField.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
        i_shortDescriptionField.setEnabled(true);
    }    

    //-------------------------------------------------------------------------
    // Protected methods overriding abstract methods in superclass
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_shortDescriptionField.setText("");
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_shortDescriptionField.setEnabled(false);
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }    
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Traffic Cases", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_TRAFFIC_CASES_SCREEN), 
                                i_helpComboListener);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox, "accountGroupsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        TrafTrafficCaseData l_trafficCaseData;
        
        l_trafficCaseData = ((TrafficCaseDbService)i_dbService).getTrafficCaseData().
                                  getInternalTrafficCaseData();
        i_codeField.setValue(new Integer(l_trafficCaseData.getTrafficCase()));
        i_descriptionField.setText(l_trafficCaseData.getTrafficDescription());
        i_shortDescriptionField.setText(l_trafficCaseData.getTrafficShortDescription());
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((TrafficCaseDbService)i_dbService).getAvailableTrafficCaseData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((TrafficCaseDbService)i_dbService).getTrafficCaseData());
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_code = -1;
        
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }

        l_code = Integer.parseInt(i_codeField.getText());
        ((TrafficCaseDbService)i_dbService).setCurrentCode(l_code);      
        
        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        
        if (i_shortDescriptionField.getText().trim().equals(""))
        {
            i_shortDescriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        TrafTrafficCaseData l_trafficCaseData;
                
        l_trafficCaseData = new TrafTrafficCaseData(null,
                                                    Integer.parseInt(i_codeField.getText()),
                                                    i_descriptionField.getText(),
                                                    i_shortDescriptionField.getText(),
                                                    ' ',
                                                    null,
                                                    null);

        ((TrafficCaseDbService)i_dbService).setTrafficCaseData(new BoiTrafficCaseData(l_trafficCaseData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiTrafficCaseData l_selectedItem = null;
        int l_code = -1;
        
        l_selectedItem = (BoiTrafficCaseData)i_keyDataComboBox.getSelectedItem();
        l_code = l_selectedItem.getInternalTrafficCaseData().getTrafficCase();
        ((TrafficCaseDbService)i_dbService).setCurrentCode(l_code);                
    }
    
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------      
    
    /**
     * Creates the Details Panel.
     */    
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel = null;
        JLabel l_descriptionLabel = null;
        JLabel l_shortDescriptionLabel = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createIntegerField(7, false);
        addValueChangedListener(i_codeField);
        
        l_shortDescriptionLabel = WidgetFactory.createLabel("Short Description:");
        i_shortDescriptionField = WidgetFactory.createTextField(5);
        addValueChangedListener(i_shortDescriptionField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(50);
        addValueChangedListener(i_descriptionField);
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,15,4");
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,6,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,6,10,4");
        i_detailsPanel.add(l_shortDescriptionLabel, "shortDescriptionLabel,1,11,15,4");
        i_detailsPanel.add(i_shortDescriptionField, "shortDescriptionField,17,11,10,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,16,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,16,70,4");

        i_detailsPanel.add(i_updateButton,"updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton,"deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton,"resetButton,33,27,15,5");
    }

    /**
     * Creates the Defined Codes Panel.
     */    
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
                                
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
 
    /**
     * Populates the Codes Table.
     */    
    private void populateCodesTable()
    {
        TrafTrafficCaseData l_trafficCaseData = null;
        int                 l_numRecords = 0;
        int                 l_arraySize = 0;
        
        l_numRecords = i_definedCodes.size() -2;
        l_arraySize = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data = new String[l_arraySize][3];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i=2, j=0; i < i_definedCodes.size(); i++, j++)
        {
            l_trafficCaseData = ((BoiTrafficCaseData)i_definedCodes.elementAt(i)).getInternalTrafficCaseData();
            i_data[j][0] = String.valueOf(l_trafficCaseData.getTrafficCase());
            i_data[j][1] = l_trafficCaseData.getTrafficShortDescription();
            i_data[j][2] = l_trafficCaseData.getTrafficDescription();
        }

        i_stringTableModel.setData(i_data);
    }    
}
