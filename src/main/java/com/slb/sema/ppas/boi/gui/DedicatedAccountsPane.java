////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DedicatedAccountsPane.java
//      DATE            :       07-Jan-2005
//      AUTHOR          :       Sally Vonka
//      REFERENCE       :       PpacLon#1067/5383
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Dedicated Accounts screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 06/03/06 | M Erskine  | Allow 10 digit dedicated account| PpacLon#2005/8047
//          |            | ids.                            |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiDedicatedAccountsData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.dbservice.DedicatedAccountsDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * DedicatedAccounts screen.
 */
public class DedicatedAccountsPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Class constants
    //-------------------------------------------------------------------------
    
    /** The length of the DEDA_ID column in the DEDA_DEDICATED_ACCOUNTS database table. */
    private static final int C_DEDA_ID_LENGTH = 10;
    
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Available markets vector. */
    private Vector              i_markets;
    
    /** Panel allowing data selection and modification. */
    private JPanel              i_detailsPanel             = null;
    
    /** Panel containing table of existing records. */
    private JPanel              i_definedCodesPanel        = null;
    
    /** Existing DedicatedAccounts codes vector. */
    private Vector              i_definedCodes             = null;
    
    /** Existing service classes vector. */
    private Vector              i_serviceClasses           = null;
    
    /** DedicatedAccounts code field. */
    private JFormattedTextField i_codeField                = null;
    
    /** DedicatedAccounts description field. */
    private ValidatedJTextField i_descriptionField         = null;
    
    /** Table containing existing records. */
    private JTable              i_table                    = null;
    
    /** Column names for table of existing records. */
    private String[]            i_columnNames              = {"Code", "Description"};
    
    /** Data array for table of existing records. */
    private String              i_data[][]                 = null;
    
    /** Data model for table of existing records. */
    private StringTableModel    i_stringTableModel         = null;
    
    /** ComboBox model to hold the array of service classes for the selected market. */ 
    private DefaultComboBoxModel i_serviceClassDataModel   = null;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar          i_verticalScrollBar = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * DedicatedAccountsPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public DedicatedAccountsPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new DedicatedAccountsDbService((BoiContext)i_context);
        
        super.init();
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {   
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }    
    
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Dedicated Accounts", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                         BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_DED_ACCOUNTS_SCREEN), 
                                         i_helpComboListener);
        i_helpComboBox.setFocusable(false);
        
        createDetailsPanel();
        createDefinedCodesPanel();

        i_mainPanel.add(i_helpComboBox, "dedAccountsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }
    
    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        long     l_code;
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }
        l_code = Long.parseLong(i_codeField.getText());
        
        if (l_code < 1 || l_code > 2147483647)
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code must be a value between 1 and 2147483647 inclusive");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Description cannot be blank");
            return false;
        }
        return true;
    }
    
    /**
     * Validates key screen data and writes it to screen data object. 
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        long    l_code = -1;
        Object l_serviceClass = null;

        l_serviceClass = i_serviceClassDataComboBox.getSelectedItem();
        if (l_serviceClass == null)
        {
            i_serviceClassDataComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Valid market/service class combination must be selected.");
            return false;
        }
        
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }
        
        l_code = Long.parseLong(i_codeField.getText());
        ((DedicatedAccountsDbService)i_dbService).setCurrentCode(l_code);
        
        return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        DedaDedicatedAccountsData l_DedicatedAccountsData;
        BoiMarket                 l_market              = null;
        ServiceClass              l_serviceClass        = null;
        int                       l_currentCode         = -1;
        BoiMiscCodeData           l_miscCodeDataObject  = null;
        int                       l_serviceClassInteger;
        
        l_market      = (BoiMarket)i_marketDataComboBox.getSelectedItem();
        l_currentCode = Integer.parseInt(i_codeField.getText());
        
        l_miscCodeDataObject  = (BoiMiscCodeData)i_serviceClassDataComboBox.getSelectedItem();
        l_serviceClassInteger = new Integer(l_miscCodeDataObject.getInternalMiscCodeData()
                                                                          .getCode()).intValue();
        l_serviceClass        = new ServiceClass(l_serviceClassInteger);
        ((DedicatedAccountsDbService)i_dbService).setCurrentServiceClass(l_miscCodeDataObject);
        
        l_DedicatedAccountsData = new DedaDedicatedAccountsData(null,
                                                                l_market.getMarket(),
                                                                l_serviceClass,
                                                                l_currentCode,
                                                                i_descriptionField.getText());
        
        ((DedicatedAccountsDbService)i_dbService)
            .setDedicatedAccountsData(new BoiDedicatedAccountsData(l_DedicatedAccountsData));
    }
    
    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiDedicatedAccountsData l_selectedItem         = null;
        long                     l_code                 = -1;
        
        if (i_keyDataComboBox.getSelectedItem() instanceof BoiDedicatedAccountsData)
        {
            l_selectedItem = (BoiDedicatedAccountsData)i_keyDataComboBox.getSelectedItem();
            l_code         = l_selectedItem.getInternalDedicatedAccountsData().getDedaAccountId();
            ((DedicatedAccountsDbService)i_dbService).setCurrentCode(l_code);
        }
        else
        {
            ((DedicatedAccountsDbService)i_dbService).setCurrentCode(-1);
        }
    }
    
    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        DedaDedicatedAccountsData l_DedicatedAccountsData;
        
        l_DedicatedAccountsData = ((DedicatedAccountsDbService)i_dbService).getDedicatedAccountsData()
                                                                         .getInternalDedicatedAccountsData();
        
        i_codeField.setValue(new Long(l_DedicatedAccountsData.getDedaAccountId()));
        i_descriptionField.setText(l_DedicatedAccountsData.getDedaDescription());
        selectRowInTable(i_table, i_verticalScrollBar);
    }
    
    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((DedicatedAccountsDbService)i_dbService).getAvailableDedicatedAccountsData();
        populateCodesTable();
        
        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((DedicatedAccountsDbService)i_dbService).getAvailableMarketData();  
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((DedicatedAccountsDbService)i_dbService).
                                             getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);
        
        i_serviceClasses = ((DedicatedAccountsDbService)i_dbService).getAvailableMiscCodeData();
        i_serviceClassDataModel = new DefaultComboBoxModel(i_serviceClasses);
        i_serviceClassDataComboBox.setModel(i_serviceClassDataModel);
        
        refreshListData();
    }
    
    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(
                                     ((DedicatedAccountsDbService)i_dbService).getDedicatedAccountsData());
    }
    
    /**
     * Refreshes list data after a new market has been selected from the market combo box.
     */   
    protected void refreshDataAfterMarketSelect()
    {
        i_serviceClasses = ((DedicatedAccountsDbService)i_dbService).getAvailableMiscCodeData();
        i_serviceClassDataModel = new DefaultComboBoxModel(i_serviceClasses);
        i_serviceClassDataComboBox.setModel(i_serviceClassDataModel);
        
        refreshListData();
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Creates the panel containing the defined codes and the required buttons.
     */
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_marketLabel       = null;
        JLabel l_serviceClassLabel = null;
        JLabel l_codeLabel         = null;
        JLabel l_descriptionLabel  = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_marketLabel = WidgetFactory.createLabel("Market:");
        
        l_serviceClassLabel = WidgetFactory.createLabel("Service Class:");
        i_serviceClasses    = ((DedicatedAccountsDbService)i_dbService).getAvailableMiscCodeData();
        i_serviceClassDataModel    = new DefaultComboBoxModel(i_serviceClasses);
        i_serviceClassDataComboBox = WidgetFactory.createComboBox(i_serviceClassDataModel);
        i_serviceClassDataComboBox.addItemListener(i_dataFilterComboListener);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createIntegerField(C_DEDA_ID_LENGTH, false);
        addValueChangedListener(i_codeField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        
        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketDataComboBox,17,1,35,4");
        
        i_detailsPanel.add(l_serviceClassLabel, "serviceClassLabel,1,6,15,4");
        i_detailsPanel.add(i_serviceClassDataComboBox, "serviceClassDataComboBox,17,6,50,4");
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,11,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,11,70,4");
        
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,16,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,16,15,4");
        
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,21,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,21,50,4");
        
        i_detailsPanel.add(i_updateButton, "updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,27,15,5");
    }
    
    /**
     * Creates the panel containing the defined codes displayed in a table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table            = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
    
    /**
     * Populates the table in the defined codes panel.
     */
    private void populateCodesTable()
    {
        DedaDedicatedAccountsData   l_dedicatedAccountsData = null;
        int                         l_numRecords            = 0;
        int                         l_arraySize             = 0;
        
        l_numRecords = i_definedCodes.size() -2;
        l_arraySize  = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data       = new String[l_arraySize][3];
        
        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_definedCodes.size(); i++, j++)
        {
            l_dedicatedAccountsData = ((BoiDedicatedAccountsData)i_definedCodes.elementAt(i))
            .getInternalDedicatedAccountsData();
            i_data[j][0] = String.valueOf(l_dedicatedAccountsData.getDedaAccountId());
            i_data[j][1] = l_dedicatedAccountsData.getDedaDescription();
        }
        
        i_stringTableModel.setData(i_data);
    }
}