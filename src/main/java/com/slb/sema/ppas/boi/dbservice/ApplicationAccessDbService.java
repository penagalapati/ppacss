////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ApplicationAccessDbService.java
//    DATE            :       03-Aug-2004
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#112/3485
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Application Access
//                            Privileges screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ApacApplicationAccessSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrilPrivilegeLevelSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Retrieves and maintains the database information for the Application Access Privileges
 * screen.
 */
public class ApplicationAccessDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** SQL service for application access privileges data. */
    private ApacApplicationAccessSqlService i_applicationAccessPrivSqlService = null;

    /** SQL service for operator privileges. */
    private PrilPrivilegeLevelSqlService i_privilegeLevelSqlService = null;

    /** DataSet of privilege level objects */
    private PrilPrivilegeLevelDataSet i_privilegeLevelDataSet = null;

    /** List of available privileges. */
    private PrilPrivilegeLevelData[] i_availablePrivileges = null;

    /** Vector of available privileges. */    
    private Vector i_availablePrivilegesV = null;

    /** Currently selected privilege level data object. */
    private PrilPrivilegeLevelData i_currentPrivilegeData = null;
    
    /** Application Access Level Data. */
    private ApacApplicationAccessData i_applicationAccessData = null;
    
    /** Application Access Level DataSet. */
    private ApacApplicationAccessDataSet i_applicationAccessDataSet = null;
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor to instantiate the SQL services.
     * @param p_context Reference to the BOI context.
     */
    public ApplicationAccessDbService(BoiContext p_context)
    {
        super(p_context);
        i_applicationAccessPrivSqlService = new ApacApplicationAccessSqlService(null, null);
        i_privilegeLevelSqlService = new PrilPrivilegeLevelSqlService(null, null);
        i_availablePrivilegesV = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Returns current privilege level data object. 
     * @return Currently selected privilege level data object. 
     */
    public PrilPrivilegeLevelData getCurrentPrivilegeData()
    {
        return i_currentPrivilegeData;
    }

    /** 
     * Set current privilege level data object. 
     * @param p_privilegeLevelData Currently selected privilege level data object. 
     */
    public void setCurrentPrivilegeData(PrilPrivilegeLevelData p_privilegeLevelData)
    {
        i_currentPrivilegeData = p_privilegeLevelData;
    }

    /** 
     * Returns application access privileges data.
     * @return Application access privileges data.
     */
    public ApacApplicationAccessData getApplicationAccessData()
    {
        return i_applicationAccessData;
    }

    /** 
     * Set application access privileges data.
     * @param p_applicationAccessData Application access privileges data.
     */
    public void setApplicationAccessData(ApacApplicationAccessData p_applicationAccessData)
    {
        i_applicationAccessData = p_applicationAccessData;
    }

    /** 
     * Return array of available privilege levels. 
     * @return Array of available privilege levels. 
     */
    public PrilPrivilegeLevelData[] getAvailablePrivileges()
    {
        return i_availablePrivileges;
    }
    
    /** 
     * Return a vector of available privilege levels. 
     * @return Vector of available privilege levels. 
     */
    public Vector getAvailablePrivilegesV()
    {
        return i_availablePrivilegesV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_applicationAccessDataSet =
            i_applicationAccessPrivSqlService.readAll(null, p_connection);
        
        i_privilegeLevelDataSet = 
            i_privilegeLevelSqlService.readAll(null, p_connection);
            
        i_applicationAccessData =
            i_applicationAccessDataSet.getApplicationAccessProfile(i_currentPrivilegeData.getPrivilegeLevel());
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_privilegeLevelDataSet = i_privilegeLevelSqlService.readAll(null, p_connection);
        i_availablePrivileges = i_privilegeLevelDataSet.getAvailablePrivileges();
        i_availablePrivilegesV.removeAllElements();
        i_availablePrivilegesV.add(0, "");
        
        for (int i = 1; i < i_availablePrivileges.length + 1; i++)
        {
            i_availablePrivilegesV.add(i, i_availablePrivileges[i-1]);
        }          
        
        i_applicationAccessDataSet = i_applicationAccessPrivSqlService.readAll(null, p_connection);
    }
    
    /** 
     * Inserts application access data in the apac_application_access table for the 
     * privilege level held in i_apacApplicationAccessData.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_applicationAccessPrivSqlService.insert(null, 
            p_connection,
            String.valueOf(i_applicationAccessData.getPrivilegeId()),
            i_applicationAccessData.hasBatchApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasBusinessConfigurationApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasPurgeAndArchiveApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasReportsApplicationAccess() ? 'Y' : 'N',
            ' ',
            i_context.getOperatorUsername());

        i_privilegeLevelDataSet = i_privilegeLevelSqlService.readAll(null, p_connection);
    }

    /** 
     * Updates application access data in the apac_application_access table.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_applicationAccessPrivSqlService.update(
            null, 
            p_connection,
            i_applicationAccessData.getPrivilegeId(),
            i_applicationAccessData.hasBatchApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasBusinessConfigurationApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasPurgeAndArchiveApplicationAccess() ? 'Y' : 'N',
            i_applicationAccessData.hasReportsApplicationAccess() ? 'Y' : 'N',
            i_context.getOperatorUsername());

        i_privilegeLevelDataSet = i_privilegeLevelSqlService.readAll(null, p_connection);
    }
    
    /** 
     * Not required - there is no delete functionality for this screen.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required
    }

    /**
     * Checks whether a record about to be inserted into the database already exists.
     * Operators are deleted rather than withdrawn, so the withdrawn flag is always 
     * returned as false.
     * @param p_connection JDBC connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        boolean[] l_flagsArray = new boolean[2];
                               
        l_flagsArray[C_WITHDRAWN] = false;

        return l_flagsArray;
    }
}