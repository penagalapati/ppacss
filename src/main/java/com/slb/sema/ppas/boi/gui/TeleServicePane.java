////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TeleServicePane.java
//      DATE            :       10-Mar-2006
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#2035/8098
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       BOI screen to maintain Teleservices
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiTeleServiceData;
import com.slb.sema.ppas.boi.dbservice.TeleServiceDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Teleservice Codes Screen class.
 */
public class TeleServicePane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel i_detailsPanel = null;

    /** Panel containing table of existing records. */
    private JPanel i_definedCodesPanel = null;

    /** Existing Teleservice codes vector. */
    private Vector i_definedCodes = null;   
    
    /** Teleservice code field. */
    private JFormattedTextField i_codeField = null;

    /** Teleservice description field. */
    private ValidatedJTextField i_descriptionField = null;
    
    /** Table containing existing records. */
    private JTable i_table = null;

    /** Column names for table of existing records. */
    private String[] i_columnNames = { "Code", "Description" };

    /** Data array for table of existing records. */
    private String i_data[][] = null;

    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel = null;
    
    /** The vertical scroll bar used for the teleservice groups table view port. */
    private JScrollBar i_verticalScrollBar = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    
    /** 
     * TeleServicePane constructor. 
     * @param p_context A reference to the BoiContext
     */        
    public TeleServicePane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new TeleServiceDbService((BoiContext)i_context);        
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------    
        
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }
    
    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
    }    

    //-------------------------------------------------------------------------
    // Protected methods overriding abstract methods in superclass
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }    
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Teleservice Codes", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_TELESERVICE_CODES_SCREEN), 
                                i_helpComboListener);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox, "accountGroupsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        TeleTeleserviceData l_teleServiceData;
 
        
        l_teleServiceData = ((TeleServiceDbService)i_dbService).getTeleserviceData().
                                  getInternalTeleserviceData();
        i_codeField.setValue(new Integer(l_teleServiceData.getTeleserviceCode()));
        i_descriptionField.setText(l_teleServiceData.getTeleserviceDescription());
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((TeleServiceDbService)i_dbService).getAvailableTeleserviceData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((TeleServiceDbService)i_dbService).getTeleserviceData());
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_code = -1;
        
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }

        l_code = Integer.parseInt(i_codeField.getText());
        ((TeleServiceDbService)i_dbService).setCurrentCode(l_code);      
        
        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        return true;
    }
    
    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        TeleTeleserviceData l_teleServiceData;
                
        l_teleServiceData = new TeleTeleserviceData(null,
                                                    Integer.parseInt(i_codeField.getText()),
                                                    i_descriptionField.getText(),
                                                    ' ',
                                                    null,
                                                    null);
    
        ((TeleServiceDbService)i_dbService).setTeleserviceData(new BoiTeleServiceData(l_teleServiceData));                                 
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiTeleServiceData l_selectedItem = null;
        int l_code = -1;
        
        l_selectedItem = (BoiTeleServiceData)i_keyDataComboBox.getSelectedItem();
        l_code = l_selectedItem.getInternalTeleserviceData().getTeleserviceCode();
        ((TeleServiceDbService)i_dbService).setCurrentCode(l_code);                
    }
    
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------      
    
    /**
     * Creates the Details Panel.
     */    
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel = null;
        JLabel l_descriptionLabel = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createIntegerField(7, false);
        addValueChangedListener(i_codeField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,55,4");
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,6,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,6,15,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,11,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,11,50,4");

        i_detailsPanel.add(i_updateButton, "updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,27,15,5");
    }

    /**
     * Creates the Defined Codes Panel.
     */    
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
                                
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
 
    /**
     * Populates the Codes Table.
     */    
    private void populateCodesTable()
    {
        TeleTeleserviceData l_teleServiceData = null;
        int                 l_numRecords      = 0;
        int                 l_arraySize       = 0;
        
        l_numRecords = i_definedCodes.size() -2;
        l_arraySize = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data = new String[l_arraySize][2];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i=2, j=0; i < i_definedCodes.size(); i++, j++)
        {
            l_teleServiceData = ((BoiTeleServiceData)i_definedCodes.elementAt(i)).getInternalTeleserviceData();
            i_data[j][0] = String.valueOf(l_teleServiceData.getTeleserviceCode());
            i_data[j][1] = l_teleServiceData.getTeleserviceDescription();
        }

        i_stringTableModel.setData(i_data);
    }    
}
