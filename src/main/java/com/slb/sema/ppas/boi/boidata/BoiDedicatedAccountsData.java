////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiDedicatedAccountsData.java
//      DATE            :       07-Jan-2005
//      AUTHOR          :       Sally Vonka
//      REFERENCE       :       PpacLon#1067/5430
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Wrapper for DedaDedicatedAccountsData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;

/** Wrapper for DedicatedAccount class within BOI. */
public class BoiDedicatedAccountsData extends DataObject
{
    /** Basic DedicatedAccount data object */
    private DedaDedicatedAccountsData i_dedicatedAccountsData;
    
    /**
     * Simple constructor.
     * @param p_dedicatedAccountsData DedicatedAccount data object to wrapper.
     */
    public BoiDedicatedAccountsData(DedaDedicatedAccountsData p_dedicatedAccountsData)
    {
        i_dedicatedAccountsData = p_dedicatedAccountsData;
    }
    
    /**
     * Return wrappered DedicatedAccount data object.
     * @return Wrappered DedicatedAccount data object.
     */
    public DedaDedicatedAccountsData getInternalDedicatedAccountsData()
    {
        return i_dedicatedAccountsData;
    }
    
    /**
     * Compares two DedicatedAccount data objects and returns true if they equate.
     * @param p_dedicatedAccountsData DedicatedAccounts data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_dedicatedAccountsData)
    {
        boolean                   l_return                = false;
        DedaDedicatedAccountsData l_dedicatedAccountsData = null;
        Market                    l_thisMarket            = null;
        Market                    l_compareToMarket       = null;
        ServiceClass              l_thisServiceClass      = null;
        ServiceClass              l_compareToServiceClass = null;
        long                      l_thisDedAccId;
        long                      l_compareToDedAccId;

        if (p_dedicatedAccountsData != null && p_dedicatedAccountsData instanceof BoiDedicatedAccountsData)
        {
            l_dedicatedAccountsData = ((BoiDedicatedAccountsData)p_dedicatedAccountsData)
                                          .getInternalDedicatedAccountsData();
            l_thisMarket            = i_dedicatedAccountsData.getDedaMarket();
            l_thisServiceClass      = i_dedicatedAccountsData.getDedaServiceClass();
            l_thisDedAccId          = i_dedicatedAccountsData.getDedaAccountId();
            l_compareToMarket       = l_dedicatedAccountsData.getDedaMarket();
            l_compareToServiceClass = l_dedicatedAccountsData.getDedaServiceClass();
            l_compareToDedAccId     = l_dedicatedAccountsData.getDedaAccountId();
            
            if (l_thisMarket.equals(l_compareToMarket)
                && l_thisServiceClass.equals(l_compareToServiceClass)
                && l_thisDedAccId == l_compareToDedAccId)
            {
                l_return = true;
            }
        }
        return l_return;
    }
    
    /**
     * Returns DedicatedAccount data object as a String for display in BOI.
     * @return DedicatedAccount data object as a string.
     */
    public String toString()
    {
        String l_displayString = "";
        
        if (i_dedicatedAccountsData != null && i_dedicatedAccountsData.getDedaDescription() != null)
        {
            l_displayString = new String(i_dedicatedAccountsData.getDedaAccountId() + " - "
                                         + i_dedicatedAccountsData.getDedaDescription());
        }
        else
        {
            System.out.println("Problem! - Description is blank in BoiDedicatedAccountData object");
        }
        
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return (int)i_dedicatedAccountsData.getDedaAccountId();
    }
}