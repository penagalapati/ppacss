////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       RegionsDbService.java
//    DATE            :       14-Aug-2005
//    AUTHOR          :       M I Erskine
//    REFERENCE       :       PpacLon#
//                            PRD_ASCS00_GEN_CA_44
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Database access class for Regions screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiRegionData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.RegiRegionSqlService;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Account Groups screen. 
 */
public class RegionsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for region code details. */
    private RegiRegionSqlService i_regiSqlService = null;
    
    /** Vector of region records. */
    private Vector i_availableRegionsDataV = null;
    
    /** Account group data. */
    private BoiRegionData i_regiData = null;

    /** Currently selected region code. Used as key for read and delete. */
    private int i_currentCode = -1;       
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public RegionsDbService(BoiContext p_context)
    {
        super(p_context);
        i_regiSqlService = new RegiRegionSqlService(null, null, null);
        i_availableRegionsDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current region code. Used for record lookup in database. 
     * @param p_code Currently selected region code. 
     */
    public void setCurrentCode(int p_code)   
    {
        i_currentCode = p_code;
    }

    /** 
     * Return region data currently being worked on. 
     * @return Account group data object.
     */
    public BoiRegionData getRegionData()
    {
        return i_regiData;
    }

    /** 
     * Set region data currently being worked on. 
     * @param p_regionData Account group data object.
     */
    public void setRegionData(BoiRegionData p_regionData)
    {
        i_regiData = p_regionData;
    }

    /** 
     * Return region data. 
     * @return Vector of available region data records.
     */
    public Vector getAvailableRegionData()
    {
        return i_availableRegionsDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        RegiRegionDataSet l_regiDataSet = null;
        
        l_regiDataSet = i_regiSqlService.readAll(null, p_connection);
        try
        {
            i_regiData = new BoiRegionData(l_regiDataSet.getRecord(new HomeRegionId((short)i_currentCode)));
        }
        catch (PpasParseException e)
        {
            e.printStackTrace();
        }             
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshRegionsDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the regi_region table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_regiSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_regiData.getInternalRegionData().getHomeRegionId().getValue(),
                                i_regiData.getInternalRegionData().getRegionDescription());        
        
        refreshRegionsDataVector(p_connection);
    }

    /** 
     * Updates record in the regi_region table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_regiSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_regiData.getInternalRegionData().getHomeRegionId().getValue(),                                
                                i_regiData.getInternalRegionData().getRegionDescription());

        refreshRegionsDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the regi_region table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_regiSqlService.delete(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_regiData.getInternalRegionData().getHomeRegionId().getValue());
              
        refreshRegionsDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        RegiRegionDataSet l_regiDataSet        = null;
        RegiRegionData    l_dbRegionData = null;
        boolean[]               l_flagsArray         = new boolean[2];
        
        l_regiDataSet        = i_regiSqlService.readAll(null, p_connection);
        try
        {
            l_dbRegionData = l_regiDataSet.getRecord(new HomeRegionId((short)i_currentCode));
            
            if (l_dbRegionData != null)
            {
                l_flagsArray[C_DUPLICATE] = true;
                if (l_dbRegionData.isDeleted())
                {
                    l_flagsArray[C_WITHDRAWN] = true;
                }
                else
                {
                    l_flagsArray[C_WITHDRAWN] = false;
                }
            }
            else
            {
                l_flagsArray[C_DUPLICATE] = false;
            }      
        }
        catch (PpasParseException e)
        {
            e.printStackTrace();
        }       

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the regi_region table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        try
        {
            HomeRegionId l_homeRegionId = new HomeRegionId((short)i_currentCode);

            i_regiSqlService.markAsAvailable(null,
                                             p_connection,
                                             l_homeRegionId,
                                             i_context.getOperatorUsername());

            refreshRegionsDataVector(p_connection);
        }
        catch (PpasParseException e)
        {
            e.printStackTrace();
        }
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available region data vector from the 
     * acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshRegionsDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        RegiRegionDataSet l_regiDataSet = null;
        RegiRegionData[]  l_regiArray   = null;
        
        i_availableRegionsDataV.removeAllElements();
        
        l_regiDataSet = i_regiSqlService.readAll(null, p_connection);
        l_regiArray   = l_regiDataSet.getAvailableArray();
        
        i_availableRegionsDataV.addElement(new String(""));
        i_availableRegionsDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_regiArray.length; i++)
        {
            i_availableRegionsDataV.addElement(new BoiRegionData(l_regiArray[i]));
        }
    }
}