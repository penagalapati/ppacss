////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiHelpListener.java
//    DATE            :       12-Apr-2005
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PRD_ASCS00_DEV_SS_88
//
//    COPYRIGHT       :       Atos Origin 2005
//
//    DESCRIPTION     :       Listener for BOI Help widgets.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.applet.AppletContext;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JComboBox;

/** Listener for BOI Help widgets. */
public class BoiHelpListener implements ItemListener, ActionListener
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    
    /** Object used by applet to obtain information about its environment. */
    private AppletContext i_appletContext;
    
    /** Static part of URL to locate BOI help info. */
    private String i_partialUrlString;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Standard constructor.
     * @param p_context BOI context object.
     */
    public BoiHelpListener(BoiContext p_context)
    {
        String l_serverHost;
        String l_serverPort;
        
        i_appletContext = (AppletContext)p_context.getObject("ascs.boi.appletContext");
        
        l_serverHost = (String)p_context.getObject("applet.server.host");
        l_serverPort = (String)p_context.getObject("applet.server.login.port");
        i_partialUrlString = "http://" + l_serverHost + ":" + l_serverPort + "/ascs/boi/help/";
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles events from BOI help combo box.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        String    l_selectedItem; 
        URL       l_url = null;
        JComboBox l_source;
        String    l_helpPage;

        if (p_itemEvent.getStateChange() == ItemEvent.SELECTED)
        {
            l_source = ((JComboBox)p_itemEvent.getSource());
            l_selectedItem = l_source.getSelectedItem().toString();

            l_helpPage = BoiHelpTopics.getHelpPage(l_selectedItem);
            
            try
            {
                l_url = new URL(i_partialUrlString + l_helpPage);
                
                i_appletContext.showDocument(l_url, "_blank");
                
                l_source.removeItemListener(this);
                l_source.setSelectedIndex(0);
                l_source.addItemListener(this);
            }
            catch (MalformedURLException l_mURLE)
            {
                l_mURLE.printStackTrace();
            }
        }
    }
    
    /** 
     * Action event handler for general BOI help.
     * @param p_actionEvent Event to handle.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        URL l_url = null;

        try
        {
            l_url = new URL(i_partialUrlString + BoiHelpTopics.C_GENERAL_BOI_HELP_PAGE);
            i_appletContext.showDocument(l_url, "_blank");
        }
        catch (MalformedURLException l_mURLE)
        {
            l_mURLE.printStackTrace();
        }
    }
}