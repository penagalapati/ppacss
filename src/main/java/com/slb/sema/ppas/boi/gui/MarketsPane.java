////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME     :     MarketsPane.java
//    DATE          :     15-August-2005
//    AUTHOR        :     M I Erskine
//    REFERENCE     :     PpacLon#
//                        PRD_ASCS00_GEN_CA_44
// 
//    COPYRIGHT     :     WM-data 2005
//
//    DESCRIPTION   :     BOI screen to maintain Market configuration in ASCS
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//     DATE | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiRegionData;
import com.slb.sema.ppas.boi.dbservice.MarketsDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * BOI Markets Screen class.
 */
public class MarketsPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel              i_detailsPanel      = null;

    /** Panel containing table of existing records. */
    private JPanel              i_definedCodesPanel = null;

    /** Existing Boi Markets vector. */
    private Vector              i_definedCodes      = null;
    
    /** Available Regions vector. */
    private Vector              i_regionsVector     = null;
    
    /** Available Regions vector. */
    private Vector              i_homeRegionsVector = null;  //TODO: Repetition of variable - check and remove?
    
    /** Available Srva Mstr vector. */
    private Vector              i_srvaMstrVector    = null;

    /** Market description field. */
    private ValidatedJTextField i_descriptionField  = null;

    /** Table containing existing records. */
    private JTable              i_table             = null;

    /** Column names for table of existing records. */
    private String[]            i_columnNames       = {"Area", "Location", "Description", "Number Format",
                                                       "Quarantine Period", "Default Home Region"};

    /** Data array for table of existing records. */
    private String              i_data[][]          = null;

    /** Data model for table of existing records. */
    private StringTableModel    i_stringTableModel  = null;

    /** Service Area field. */
    private JFormattedTextField i_areaField         = null;
    
    /** Service Location field. */
    private JFormattedTextField i_locationField     = null;

    /** Number Format field. */
    private ValidatedJTextField i_numberFormatField = null;

    /** Quarantine Period field. */
    private JFormattedTextField i_quarantinePeriodField = null;

    /** Combo-box model to hold available regions. */
    private DefaultComboBoxModel i_defaultHomeRegionComboBoxModel = null;

    /** Combo-box holding the available regions, and to display the currently selected default home region. */
    private JComboBox i_defaultHomeRegionComboBox = null;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar          i_verticalScrollBar = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * MarketsPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public MarketsPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new MarketsDbService((BoiContext)i_context);

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    /**
     * Alters screen behaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_areaField.setEnabled(true);
        i_locationField.setEnabled(true);
        i_descriptionField.setEnabled(true);
        i_numberFormatField.setEnabled(true);
        i_quarantinePeriodField.setEnabled(true);
        i_defaultHomeRegionComboBox.setEnabled(true);
    }

    /**
     * Alters screen behaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_areaField.setEnabled(false);
        i_locationField.setEnabled(false);
        i_descriptionField.setEnabled(true);
        i_numberFormatField.setEnabled(true);
        i_quarantinePeriodField.setEnabled(true);
        i_defaultHomeRegionComboBox.setEnabled(true);
    }

    //-------------------------------------------------------------------------
    // Protected methods overriding abstract methods in superclass
    //-------------------------------------------------------------------------

    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_areaField.setText("");
        i_locationField.setText("");
        i_descriptionField.setText("");
        i_numberFormatField.setText("");
        i_quarantinePeriodField.setText("");
        i_defaultHomeRegionComboBox.setSelectedItem("");
        
        i_areaField.setEnabled(false);
        i_locationField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_numberFormatField.setEnabled(false);
        i_quarantinePeriodField.setEnabled(false);
        i_defaultHomeRegionComboBox.setEnabled(false);
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Markets", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_MARKETS_SCREEN), i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();

        i_mainPanel.add(i_helpComboBox, "marketsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,40");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,47,100,54");
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        BoiMarket     l_boiMarket       = null;
        BoiRegionData l_boiHomeRegion   = null;
        BoiRegionData l_region      = null;
        SrvaMstrData  l_srvaMstrData    = null;
        boolean       l_found           = false;      

        l_boiMarket = ((MarketsDbService)i_dbService).getCurrentMarket();
        l_srvaMstrData = ((MarketsDbService)i_dbService).getCurrentSrvaMstrMarket();
        
        
        i_areaField.setText(l_boiMarket.getMarket().getSrva());
        i_locationField.setText(l_boiMarket.getMarket().getSloc());
        i_descriptionField.setText(l_boiMarket.getMarket().getName());
        i_numberFormatField.setText(l_srvaMstrData.getTeleFormat());
        i_quarantinePeriodField.setText(String.valueOf(l_srvaMstrData.getMsisdnQuarantinePeriod()));
        
        // These are BoiRegionData objects
        // We have a complete list of theses in i_regionsVector
        
        for (int i = 0; i < i_regionsVector.size() && !l_found; i++)
        {
            if (i_regionsVector.elementAt(i) instanceof BoiRegionData)
            {
                l_region = (BoiRegionData)(i_regionsVector.elementAt(i));
                if (l_region.getInternalRegionData().getHomeRegionId().equals(l_srvaMstrData.getDefaultHomeRegion()))
                {
                    l_found = true;
                }
            }
        }
        
        if (l_found)
        {
            l_boiHomeRegion = new BoiRegionData(
                                    new RegiRegionData(null,
                                                     l_srvaMstrData.getDefaultHomeRegion(),
                                                     l_region.getInternalRegionData().getRegionDescription(),
                                                     ' '));
        
            i_defaultHomeRegionComboBox.setSelectedItem(l_boiHomeRegion);
        }
        else
        {
            i_defaultHomeRegionComboBox.setSelectedItem("");
        }
        
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different market selected.
     */
    protected void refreshListData()
    {
        int l_selectedIndex = 0;
        
        l_selectedIndex = i_defaultHomeRegionComboBox.getSelectedIndex();
        
        i_definedCodes = ((MarketsDbService)i_dbService).getAvailableBoiMarketData();
        i_srvaMstrVector = ((MarketsDbService)i_dbService).getAvailableSrvaMstrData();
        i_regionsVector = ((MarketsDbService)i_dbService).getAvailableBoiRegionData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
        
        i_defaultHomeRegionComboBoxModel = new DefaultComboBoxModel(i_regionsVector);
        i_defaultHomeRegionComboBox.setModel(i_defaultHomeRegionComboBoxModel);
        i_defaultHomeRegionComboBox.setSelectedIndex(l_selectedIndex);
    }

    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((MarketsDbService)i_dbService).getAvailableBoiMarketData());
    }

    /**
     * Validates key screen data and writes it to screen data object. This method is called before checking
     * for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_area = -1;
        int l_location = -1;
        BoiMarket l_currentMarket = null;

        if (i_areaField.getText().trim().equals(""))
        {
            i_areaField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Area cannot be blank");
            return false;
        }
        
        if (i_locationField.getText().trim().equals(""))
        {
            i_locationField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Location cannot be blank");
            return false;
        }

        //TODO: This code is probably duplicated in writeCurrentRecord() method...
        l_area = Integer.parseInt(i_areaField.getText());
        l_location = Integer.parseInt(i_locationField.getText());
        l_currentMarket = new BoiMarket(new Market(l_area, l_location, i_descriptionField.getText()));

        ((MarketsDbService)i_dbService).setCurrentMarket(l_currentMarket);

        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_areaField.getText().trim().equals(""))
        {
            i_areaField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Area cannot be blank");
            return false;
        }
        
        if (i_locationField.getText().trim().equals(""))
        {
            i_locationField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Location cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Description cannot be blank");
            return false;
        }
        
        if (i_numberFormatField.getText().trim().equals(""))
        {
            i_locationField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Number Format cannot be blank");
            return false;
        }
        
        if (i_quarantinePeriodField.getText().trim().equals(""))
        {
            i_quarantinePeriodField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Quarantine Period cannot be blank");
            return false;
        }
        
        // Home Region field can be blank
        
        return true;
    }

    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        int l_area = -1;
        int l_location = -1;
        BoiMarket l_currentMarket = null;
        SrvaMstrData l_currentSrvaMstrData = null;
        MsisdnFormat l_msisdnFormat = null;
        
        l_area = Integer.parseInt(i_areaField.getText());
        l_location = Integer.parseInt(i_locationField.getText());
        l_currentMarket = new BoiMarket(new Market(l_area, l_location, i_descriptionField.getText()));
        
        ((MarketsDbService)i_dbService).setCurrentMarket(l_currentMarket);
        
        l_msisdnFormat = new MsisdnFormat(i_numberFormatField.getText(),"");
        
        if (i_defaultHomeRegionComboBox.getSelectedItem() instanceof BoiRegionData)
        {    
            l_currentSrvaMstrData = new SrvaMstrData(null, l_currentMarket.getMarket(),
                                                 l_msisdnFormat,
                                                 Integer.parseInt(i_quarantinePeriodField.getText()),
                                                 null, null, ' ',
                                                 ((BoiRegionData)i_defaultHomeRegionComboBox.getSelectedItem()).getInternalRegionData().getHomeRegionId());
        }
        else
        {
            l_currentSrvaMstrData = new SrvaMstrData(null, l_currentMarket.getMarket(),
                                                     l_msisdnFormat,
                                                     Integer.parseInt(i_quarantinePeriodField.getText()),
                                                     null, null, ' ',
                                                     null);
        }
        
        ((MarketsDbService)i_dbService).setCurrentSrvaMstrData(l_currentSrvaMstrData);
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiMarket     l_selectedItem  = null;

        l_selectedItem  = (BoiMarket)i_keyDataComboBox.getSelectedItem();
        ((MarketsDbService)i_dbService).setCurrentMarket(l_selectedItem);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Creates the Details Panel.
     */
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_areaLabel         = null;
        JLabel l_locationLabel     = null;
        JLabel l_descriptionLabel  = null;
        JLabel l_numberFormatLabel = null;
        JLabel l_quarantinePeriodLabel = null;
        JLabel l_defaultHomeRegionLabel = null;

        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 40, 0, 0);

        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");

        l_areaLabel = WidgetFactory.createLabel("Area:");
        //TODO: Either change the name of this method, or write specific one for SRVA and SLOC
        i_areaField = WidgetFactory.createMsisdnField(Market.C_MAX_NUM_OF_DIGITS_IN_SRVA);
        addValueChangedListener(i_areaField);
        
        l_locationLabel = WidgetFactory.createLabel("Location:");
        //TODO: Either change the name of this method, or write specific one for SRVA and SLOC
        i_locationField = WidgetFactory.createMsisdnField(Market.C_MAX_NUM_OF_DIGITS_IN_SRVA);
        addValueChangedListener(i_locationField);

        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(20);
        addValueChangedListener(i_descriptionField);
        
        l_numberFormatLabel = WidgetFactory.createLabel("Number Format:");
        i_numberFormatField = WidgetFactory.createTextField(21);
        addValueChangedListener(i_numberFormatField);
        
        l_quarantinePeriodLabel = WidgetFactory.createLabel("Quarantine Period:");
        i_quarantinePeriodField = WidgetFactory.createAmountField(5, 0, false);
        addValueChangedListener(i_quarantinePeriodField);
        
        l_defaultHomeRegionLabel = WidgetFactory.createLabel("DefaultHomeRegion");
        //TODO: Probably unnecessary - tidy up.
        i_homeRegionsVector = new Vector();
        i_defaultHomeRegionComboBoxModel = new DefaultComboBoxModel(i_homeRegionsVector);
        i_defaultHomeRegionComboBox = WidgetFactory.createComboBox(i_defaultHomeRegionComboBoxModel);
        addValueChangedListener(i_defaultHomeRegionComboBox);

        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,70,4");
        i_detailsPanel.add(l_areaLabel, "areaLabel,1,6,15,4");
        i_detailsPanel.add(i_areaField, "areaField,17,6,8,4");
        i_detailsPanel.add(l_locationLabel, "locationLabel,26,6,15,4");
        i_detailsPanel.add(i_locationField, "locationField,42,6,8,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,11,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,11,30,4");
        i_detailsPanel.add(l_numberFormatLabel, "numberFormatLabel,1,16,15,4");
        i_detailsPanel.add(i_numberFormatField, "numberFormatField,17,16,30,4");
        i_detailsPanel.add(l_quarantinePeriodLabel, "quarantinePeriodLabel,1,21,15,4");
        i_detailsPanel.add(i_quarantinePeriodField, "quarantinePeriodField,17,21,8,4");
        i_detailsPanel.add(l_defaultHomeRegionLabel, "defaultHomeRegionLabel,1,26,15,4");
        i_detailsPanel.add(i_defaultHomeRegionComboBox, "defaultHomeRegionComboBox,17,26,40,4");
        
        i_detailsPanel.add(i_updateButton,"updateButton,1,35,15,5");
        i_detailsPanel.add(i_deleteButton,"deleteButton,17,35,15,5");
        i_detailsPanel.add(i_resetButton,"resetButton,33,35,15,5");
    }

    /**
     * Creates the Defined Codes Panel.
     */
    private void createDefinedCodesPanel()
    {
        int[]       l_columnWidths = new int[] {85,85,165,165,145,148};
        JScrollPane l_scrollPane   = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 54, 0, 0);

        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(l_columnWidths, 150, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,54");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    /**
     * Populates the Codes Table.
     */
    private void populateCodesTable()
    {
        SrvaMstrData l_srvaMstrData = null;
        int l_numRecords = 0;
        int l_arraySize = 0;
        String l_defaultHomeRegion = null;

        l_numRecords = i_srvaMstrVector.size();
        l_arraySize = ((l_numRecords > 17) ? l_numRecords : 18);
        i_data = new String[l_arraySize][6];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        //TODO: Remove extra j variable - surely not needed here? f40771d
        for (int i = 0, j = 0; i < i_srvaMstrVector.size(); i++, j++)
        {
            l_srvaMstrData = (SrvaMstrData)i_srvaMstrVector.elementAt(i);
            
            l_defaultHomeRegion = (l_srvaMstrData.getDefaultHomeRegion() == null) ? 
                    "" : String.valueOf(l_srvaMstrData.getDefaultHomeRegion());
            
            i_data[j][0] = l_srvaMstrData.getMarket().getSrva();
            i_data[j][1] = l_srvaMstrData.getMarket().getSloc();
            i_data[j][2] = l_srvaMstrData.getMarket().getName();
            i_data[j][3] = l_srvaMstrData.getTeleFormat();
            i_data[j][4] = String.valueOf(l_srvaMstrData.getMsisdnQuarantinePeriod());
            i_data[j][5] = String.valueOf(l_defaultHomeRegion);
        }

        i_stringTableModel.setData(i_data);
    }
}