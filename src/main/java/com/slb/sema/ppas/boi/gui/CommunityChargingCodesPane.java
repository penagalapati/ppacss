////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       CommunityChargingCodesPane.java
//    DATE            :       19-Jul-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#338/3249
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Community Charging Codes screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.util.Vector;

import java.awt.Container;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.boi.boidata.BoiCommunityChargingData;
import com.slb.sema.ppas.boi.dbservice.CommunityChargingDbService;

/**
 * Community Charging Codes screen.
 */
public class CommunityChargingCodesPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel i_detailsPanel = null;

    /** Panel containing table of existing records. */
    private JPanel i_definedCodesPanel = null;

    /** Existing community charging codes vector. */
    private Vector i_definedCodes = null;

    /** Community charging code field. */
    private JFormattedTextField i_codeField = null;

    /** Community charging description field. */
    private ValidatedJTextField i_descriptionField = null;
    
    /** Table containing existing records. */
    private JTable i_table = null;

    /** Column names for table of existing records. */
    private String[] i_columnNames = { "Code", "Description" };

    /** Data array for table of existing records. */
    private String i_data[][] = null;

    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel = null;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar          i_verticalScrollBar = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * CommunityChargingCodesPane constructor. 
     * @param p_context A reference to the BoiContext
     */
    public CommunityChargingCodesPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new CommunityChargingDbService((BoiContext)i_context);

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Community Charging Identities", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                          BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_COMM_CHARGING_SCREEN), 
                                          i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();

        i_mainPanel.add(i_helpComboBox, "commChargingHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_code = -1;
        
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }

        l_code = Integer.parseInt(i_codeField.getText());
        ((CommunityChargingDbService)i_dbService).setCurrentCode(l_code);
        
        return true;
    }

    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        CochCommunityChargingData l_chargingData;
        int                       l_code = -1;
        
        l_code = Integer.parseInt(i_codeField.getText());
        
        l_chargingData = new CochCommunityChargingData(l_code,
                                                       i_descriptionField.getText(),
                                                       ' ');
                                                       
        ((CommunityChargingDbService)i_dbService).setChargingData(
                                                    new BoiCommunityChargingData(l_chargingData));
    }
    
    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiCommunityChargingData l_selectedItem = null;
        int l_code = -1;
        
        l_selectedItem = (BoiCommunityChargingData)i_keyDataComboBox.getSelectedItem();
        l_code = l_selectedItem.getInternalChargingData().getCommunityChgId();
        ((CommunityChargingDbService)i_dbService).setCurrentCode(l_code);
    }

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_verticalScrollBar.setValue(0);
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        CochCommunityChargingData l_chargingData;

        l_chargingData = ((CommunityChargingDbService)i_dbService).getChargingData().getInternalChargingData();
        i_codeField.setValue(new Integer(l_chargingData.getCommunityChgId()));
        i_descriptionField.setText(l_chargingData.getCommunityChgDesc());
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((CommunityChargingDbService)i_dbService).getAvailableChargingData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((CommunityChargingDbService)i_dbService).getChargingData());
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Creates the panel containing the defined codes and the required buttons.
     */
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel = null;
        JLabel l_descriptionLabel = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createIntegerField(7, false);
        addValueChangedListener(i_codeField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,70,4");
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,6,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,6,15,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,11,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,11,50,4");
        
        i_detailsPanel.add(i_updateButton,"updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton,"deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton,"resetButton,33,27,15,5");
    }
    
    /**
     * Creates the panel containing the defined codes displayed in a table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        l_scrollPane = new JScrollPane(
                               i_table,
                               JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                               JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
    
    /**
     * Populates the table in the defined codes panel.
     */
    private void populateCodesTable()
    {
        CochCommunityChargingData l_chargingData = null;
        int                       l_numRecords = 0;
        int                       l_arraySize = 0;
        
        l_numRecords = i_definedCodes.size() -2;
        l_arraySize = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data = new String[l_arraySize][2];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i=2, j=0; i < i_definedCodes.size(); i++, j++)
        {
            l_chargingData = ((BoiCommunityChargingData)i_definedCodes.elementAt(i)).getInternalChargingData();
            i_data[j][0] = String.valueOf(l_chargingData.getCommunityChgId());
            i_data[j][1] = l_chargingData.getCommunityChgDesc();
        }

        i_stringTableModel.setData(i_data);
    }
}