////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ReportsPane.java
//    DATE            :       5-Sep-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#476/3968
//                            PRD_ASCS00_DEV_SS_88
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       BOI Reports submission screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/6/5   | M.Brister  | Add adjustment type/code combo  | PpacLon#1573/6627
//          |            | for adjustments report.         | PRD_ASCS00_GEN_CA_49
//          |            |                                 | BHA-011
//----------+------------+---------------------------------+--------------------
// 14/7/5   | M.Brister  | Added repeat and interval fields| PpacLon#1398/6788
//          |            | to enable repeating jobs to be  | 
//          |            | scheduled.                      | 
//          |            | Changed "Submission date & time"|
//          |            | to "Run date & time".           |
//----------+------------+---------------------------------+--------------------
// 10/11/5  | K Goswami  | Added code to allow user to     | PpacLon#1707/7412
//          |            | specify the report format       | 
//----------+------------+---------------------------------+--------------------
// 05/04/06 | M Erskine  | Properties file changes:        | PpacLon#2036/8373
//          |            | 1. Get the report formats using |
//          |            | the files in the system area.   |
//          |            | 2. Get the JS job type from     |
//          |            | configuration rather than hard- |
//          |            | coding it.                      |
//----------+------------+---------------------------------+--------------------
// 27/04/06 | M Erskine  | New reports for Vodafone Ireland| PpacLon#2152/8590
//----------+------------+---------------------------------+--------------------
// 13/03/07 | S James    | Changes made to support Reports | PpacLon#2995/10671
//          |            | redesign                        |
//----------+------------+---------------------------------+--------------------
// 26/03/07 | S James    | Changed detection of reports to | PpacLon#3020/11213
//          |            | use the Job Type instead of the |
//          |            | report name displayed on screen |
//          |            | so that screen will still work  |
//          |            | if names are ever translated.   |
//          |            | Ensure PPAS Adjustments,        |
//          |            | AccountGroupConfig,             |
//          |            | EnhancedSubscriberLifecycle,    |
//          |            | ServiceOfferingsConfig reports  |
//          |            | are handled correctly.          |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.dbservice.BoiDbService;
import com.slb.sema.ppas.boi.dbservice.ReportsDbService;
import com.slb.sema.ppas.common.dataclass.RoutingData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;

/** BOI Reports submission screen. */
public class ReportsPane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    /** Constant defining the Job Type for Adjustments. */
    private static final String C_JOB_TYPE_ADJUSTMENTS = "reportAdjustments";
    
    /** Constant defining the Job Type for Ppas Adjustments. */
    private static final String C_JOB_TYPE_PPAS_ADJUSTMENTS = "reportPpasAdjustments";
    
    /** Constant defining the Job Type name for MSISDNs. */
    private static final String C_JOB_TYPE_MSISDNS = "reportMsisdns";
    
    /** Constant defining the Job Type name for Account Groups Config. */
    private static final String C_JOB_TYPE_ACCOUNT_GROUPS = "reportAccountGroupsConfig";
    
    /** Constant defining the Job Type name for Enhanced Subscriber Lifecycle. */
    private static final String C_JOB_TYPE_ENHANCED_SUB_LC = "reportEnhancedSubscriberLifecycle";
    
    /** Constant defining the Job Type name for Service Offerings Config. */
    private static final String C_JOB_TYPE_SERVICE_OFFERINGS = "reportServiceOfferingsConfig";
    
    /** Constant defining the Job Type name for VFI Airtime Expiry. */
    private static final String C_JOB_TYPE_VFI_AIRTIME_EXPIRY = "reportVfiAirtimeExpiry";
    
    /** Constant defining the regular expression with which to match Vodafone Ireland Job Types. */
    private static final String C_PATTERN_VFI_REPORT = "reportVfi.*";
    
    /** Compiled <code>Pattern</code> object for Vodafone Ireland reports. */
    private static final Pattern c_pattern = Pattern.compile(C_PATTERN_VFI_REPORT);
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Main panel for the screen. */
    private JPanel i_mainPanel;

    /** Panel for entering submission details. */
    private JPanel i_submissionPanel;

    /** Combo box containing report types. */
    private JComboBox i_reportTypeComboBox;

    /** List of available report types. */
    private Vector i_configuredReportTypes = new Vector();

    /** HashMap with a key of Report display name and value containing JS job type and display name. */
    private HashMap i_reportNamesMap = null;

    /** Run date time field. */
    private JFormattedTextField i_runDateTimeField;
        
    /** Run date time object. */
    private PpasDateTime i_runDateTime;
        
    /** Start date field for report data selection. */
    private JFormattedTextField i_startDateField;

    /** End date field for report data selection. */
    private JFormattedTextField i_endDateField;

    /** Number of days difference between Run Date and Selection Start Date. */
    private int i_startSelectionOffset = 0;
    
    /** Number of days difference between Run Date and Selection End Date. */
    private int i_endSelectionOffset = 0;
    
    /** Screen label for selection start date. */
    private JLabel i_startDateLabel;

    /** Screen label for selection end date. */
    private JLabel i_endDateLabel;

    /** Screen label for MSISDN status combo. */
    private JLabel i_msisdnStatusLabel;

    /** Screen label for SDP id combo. */
    private JLabel i_sdpIdLabel;

    /** Screen label for adjustment type and code combo. */
    private JLabel i_adjTypeCodeLabel;

    /** Combo box containing MSISDN statuses. */  
    private JComboBox i_msisdnStatusComboBox;
    
    /** List of available MSISDN statuses. */
    private Vector i_msisdnStatuses = new Vector(4);

    /** Combo box containing SDP identifiers. */  
    private JComboBox i_sdpIdComboBox;
    
    /** Vector of available SDP ids. */
    private Vector i_sdpIds;

    /** Combo box model for SDP ids. */
    private DefaultComboBoxModel i_sdpIdModel;
    
    /** Combo box containing adjustment type and code combinations. */  
    private JComboBox i_adjTypeCodeComboBox;
    
    /** Vector of adjustment type and code combinations. */
    private Vector i_adjTypeCodes;

    /** Combo box model for adjustment type and code combinations. */
    private DefaultComboBoxModel i_adjTypeCodeModel;
    
    /** Button to submit report. */
    private JButton i_submitButton;

    /** Button to reset screen. */
    private JButton i_resetButton;

    /** Database access class for Reports screen. */
    private ReportsDbService i_dbService;
    
    /** Used to display text next to the frequency combo-box. */
    private JLabel i_frequencyLabel = null;

    /** ComboBox to hold the job submission frequencies. */
    private JComboBox i_frequencyBox = null;
    
    /** Used to display text next to the interval field. */
    private JLabel i_intervalLabel = null;

    /** Text field to hold the job submission interval. */
    private JFormattedTextField i_intervalField = null;
    
    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;
    
    /** Used to display text next to the report format field. */
    private JLabel i_reportformatLabel = null;
    
    /** Combo box containing report format. */
    private JComboBox i_reportFormatComboBox;
    
    /** List of available report formats. */
    private Vector i_reportFormats = new Vector(2);
    
    /** Used to display text next to the expiry combo-box. */
    private JLabel i_daysUntilExpiryLabel = null;
    
    /** Text field to hold the Days Until Expiry value. */
    private JFormattedTextField i_daysUntilExpiryField = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** 
     * ReportsPane constructor. 
     * @param p_context A reference to the BoiContext
     */
    public ReportsPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new ReportsDbService((BoiContext)i_context);
        loadDisplayNameToFormatFileNameHash();
        
        paintScreen();
        populateListData();
        
        i_contentPane = i_mainPanel;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_mainPanel.setFocusCycleRoot(true);        
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles keyboard events for the screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean        l_return = false;
        
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else
            {
                i_submitButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }

    /**
     * Handles button events.
     * @param p_actionEvent Action event.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        Object  l_source = null;
        
        l_source = p_actionEvent.getSource();
        
        if (l_source == i_submitButton)
        {
            // Request focus on submit button such that focus lost processing
            // is performed on previous focus owner.  
            i_submitButton.requestFocusInWindow();
            
            // Perform submit processing after the current focus lost event.
            // i.e. After field validation has occurred for previous focus owner.
            SwingUtilities.invokeLater(new SubmitRunnable());
        }
        else if (l_source == i_resetButton)
        {
            resetScreen();
        }
    }
    
    /**
     * Handles combo box events.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        String l_selectedItem; 

        if (p_itemEvent.getStateChange() == ItemEvent.SELECTED)
        {
            if (p_itemEvent.getSource() == i_reportTypeComboBox)
            {
                i_adjTypeCodeLabel.setVisible(false);
                i_adjTypeCodeComboBox.setVisible(false);
                i_msisdnStatusLabel.setVisible(false);
                i_msisdnStatusComboBox.setVisible(false);
                i_sdpIdLabel.setVisible(false);
                i_sdpIdComboBox.setVisible(false);
                i_startDateLabel.setVisible(true);
                i_startDateField.setVisible(true);
                i_endDateLabel.setVisible(true);
                i_endDateField.setVisible(true);
                i_reportformatLabel.setVisible(true);
                i_reportFormatComboBox.setVisible(true);

                l_selectedItem = getJsJobTypeForDisplayString(
                                     ((JComboBox)p_itemEvent.getSource()).getSelectedItem().toString());
                populateReportFormatTypes();
                
                if (c_pattern.matcher(l_selectedItem).matches() ||
                    l_selectedItem.equals(C_JOB_TYPE_ACCOUNT_GROUPS) ||
                    l_selectedItem.equals(C_JOB_TYPE_ENHANCED_SUB_LC) ||
                    l_selectedItem.equals(C_JOB_TYPE_SERVICE_OFFERINGS))
                {
                    i_startDateLabel.setVisible(false);
                    i_startDateField.setVisible(false);
                    i_endDateLabel.setVisible(false);
                    i_endDateField.setVisible(false);   
                }
                
                if (l_selectedItem.equals(C_JOB_TYPE_VFI_AIRTIME_EXPIRY))
                {
                    i_daysUntilExpiryLabel.setVisible(true);
                    i_daysUntilExpiryField.setVisible(true);
                    i_daysUntilExpiryField.setText("5");
                }
                else
                {
                    i_daysUntilExpiryLabel.setVisible(false);
                    i_daysUntilExpiryField.setVisible(false);
                }

                if (l_selectedItem.equals(C_JOB_TYPE_MSISDNS))
                {
                    i_msisdnStatusComboBox.setSelectedIndex(0);
                    i_sdpIdComboBox.setSelectedIndex(0);
                    
                    i_msisdnStatusLabel.setVisible(true);
                    i_msisdnStatusComboBox.setVisible(true);
                    i_sdpIdLabel.setVisible(true);
                    i_sdpIdComboBox.setVisible(true);
                    i_startDateLabel.setVisible(false);
                    i_startDateField.setVisible(false);
                    i_endDateLabel.setVisible(false);
                    i_endDateField.setVisible(false);
                }
                else if (l_selectedItem.equals(C_JOB_TYPE_ADJUSTMENTS) ||
                         l_selectedItem.equals(C_JOB_TYPE_PPAS_ADJUSTMENTS))
                {
                    i_adjTypeCodeComboBox.setSelectedIndex(0);
                    
                    i_adjTypeCodeLabel.setVisible(true);
                    i_adjTypeCodeComboBox.setVisible(true);
                }
            }
            else if (p_itemEvent.getSource() == i_frequencyBox)
            {
                if (i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE))
                {
                    i_intervalLabel.setVisible(false);
                    i_intervalField.setVisible(false);
                    i_intervalField.setText("");
                }
                else
                {
                    i_intervalLabel.setVisible(true);
                    i_intervalField.setVisible(true);
                }
            }
        }
    }

    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_reportTypeComboBox.requestFocusInWindow())
        {
            System.out.println("Error in requestFocusInWindow!");
        }
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        try
        {
            i_dbService.readInitialData();

            initialiseScreen();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
    }

    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e){}

    /** 
     * Handles GUI events.
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(GuiEvent p_event){}

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Validates screen data before submit.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        PpasDate     l_runDate;
        PpasDate     l_startDate;
        PpasDate     l_endDate;
        
        if (i_runDateTimeField.getText().equals(""))
        {
            i_runDateTimeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Run Date & Time cannot be blank");
            return false;
        }
        
        if ( !i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE) &&
              ( i_intervalField.getText().equals("") || Integer.parseInt(i_intervalField.getText()) == 0 ))
        {
            i_intervalField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Interval must be set for a repeating job");
            return false;
        }
            
        if (i_startDateField.getText().equals(""))
        {
            i_startDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Selection Start Date cannot be blank");
            return false;
        }
        
        if (i_endDateField.getText().equals(""))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Selection End Date cannot be blank");
            return false;
        }

        i_runDateTime = new PpasDateTime(i_runDateTimeField.getText());
        l_runDate = i_runDateTime.getPpasDate();
        l_startDate = new PpasDate(i_startDateField.getText());
        l_endDate = new PpasDate(i_endDateField.getText());

        if (l_startDate.after(l_runDate))
        {
            i_startDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Selection Start Date cannot be later than the Run Date");

            return false;
        }

        if (l_endDate.before(l_startDate))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Selection End Date cannot be earlier than Selection Start Date");

            return false;
        }

        if (l_endDate.after(l_runDate))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Selection End Date cannot be later than the Run Date");

            return false;
        }

        i_startSelectionOffset = l_startDate.getDaysDiff(l_runDate);
        i_endSelectionOffset = l_endDate.getDaysDiff(l_runDate);
        
        if (i_daysUntilExpiryField.isVisible() && i_daysUntilExpiryField.getText().equals(""))
        {
            i_daysUntilExpiryField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Please specify the number of days until airtime expiry");
            return false;
        }

        return true;
    }
    
    /**
     * Gets the Db service belonging to this screen.
     * @return The database service that belongs to this screen.
     */
    protected ReportsDbService getDbService()
    {
        return i_dbService;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    private void paintScreen()
    {
        BoiHelpListener l_helpComboListener;

        i_mainPanel = WidgetFactory.createMainPanel("Reports", 100, 100, 0, 0);
        
        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                            BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_REPORTS_SCREEN), 
                                            l_helpComboListener);
        i_helpComboBox.setFocusable(false);
                      
        createSubmissionPanel();
        
        i_mainPanel.add(i_helpComboBox, "reportsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_submissionPanel, "submissionPanel,1,6,100,95");
    }

    /** 
     * Creates the submission details panel. 
     */
    private void createSubmissionPanel()
    {
        JLabel l_reportTypeLabel;
        JLabel l_runDateTimeLabel;
        
        i_submissionPanel = WidgetFactory.createPanel("Submit Job",100, 100, 0, 0);
        
        l_reportTypeLabel = WidgetFactory.createLabel("Report Type: ");
        i_reportTypeComboBox = WidgetFactory.createComboBox(i_configuredReportTypes, this);
        addValueChangedListener(i_reportTypeComboBox);
        
        l_runDateTimeLabel = WidgetFactory.createLabel("Run Date & Time: ");
        i_runDateTimeField = WidgetFactory.createDateTimeField(this);
        addValueChangedListener(i_runDateTimeField);
        
        i_frequencyLabel = WidgetFactory.createLabel("Repeat: ");
        i_frequencyBox = WidgetFactory.createComboBox(i_dbService.getRepeatFreqs(), this);
        addValueChangedListener(i_frequencyBox);
        
        i_intervalLabel = WidgetFactory.createLabel("Interval: ");
        i_intervalField = WidgetFactory.createIntegerField(2, false);
        addValueChangedListener(i_intervalField);

        i_startDateLabel = WidgetFactory.createLabel("Selection Start Date: ");
        i_startDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_startDateField);
        
        i_endDateLabel = WidgetFactory.createLabel("Selection End Date: ");
        i_endDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_endDateField);
        
        i_reportformatLabel = WidgetFactory.createLabel("Select Report Format: ");
        
        i_reportFormats = new Vector();
        i_reportFormats.addElement("");
        i_reportFormatComboBox = WidgetFactory.createComboBox(i_reportFormats);
        addValueChangedListener(i_reportFormatComboBox);
        
        i_msisdnStatusLabel = WidgetFactory.createLabel("Status: ");
        i_msisdnStatusComboBox = WidgetFactory.createComboBox(i_msisdnStatuses);
        addValueChangedListener(i_msisdnStatusComboBox);

        i_sdpIdLabel = WidgetFactory.createLabel("SDP id: ");
        i_sdpIdModel = new DefaultComboBoxModel();
        i_sdpIdComboBox = WidgetFactory.createComboBox(i_sdpIdModel);
        addValueChangedListener(i_sdpIdComboBox);

        i_adjTypeCodeLabel = WidgetFactory.createLabel("Adjustment Type/Code: ");
        i_adjTypeCodeModel = new DefaultComboBoxModel();
        i_adjTypeCodeComboBox = WidgetFactory.createComboBox(i_adjTypeCodeModel);
        addValueChangedListener(i_adjTypeCodeComboBox);
        
        i_daysUntilExpiryLabel = WidgetFactory.createLabel("Days until expiry: ");
        i_daysUntilExpiryField = WidgetFactory.createIntegerField(4, false);
        addValueChangedListener(i_daysUntilExpiryField);

        i_submitButton = WidgetFactory.createButton("Submit", this, true);
        i_resetButton = WidgetFactory.createButton("Reset", this, false);
        i_resetButton.setVerifyInputWhenFocusTarget(false);
        
        i_submissionPanel.add(l_reportTypeLabel, "reportTypeLabel,1,1,20,4");
        i_submissionPanel.add(i_reportTypeComboBox, "reportTypeComboBox,21,1,50,4");
        i_submissionPanel.add(l_runDateTimeLabel, "runDateTimeLabel,1,6,20,4");
        i_submissionPanel.add(i_runDateTimeField, "runDateTimeField,21,6,30,4");

        i_submissionPanel.add(i_frequencyLabel, "frequencyLabel,53,6,8,4");
        i_submissionPanel.add(i_frequencyBox, "frequencyBox,61,6,14,4");
        i_submissionPanel.add(i_intervalLabel, "intervalLabel,77,6,10,4");
        i_submissionPanel.add(i_intervalField, "intervalField,87,6,6,4");

        i_submissionPanel.add(i_startDateLabel, "startDateLabel,1,11,20,4");
        i_submissionPanel.add(i_startDateField, "startDateField,21,11,20,4");
        i_submissionPanel.add(i_endDateLabel, "endDateLabel,1,16,20,4");
        i_submissionPanel.add(i_endDateField, "endDateField,21,16,20,4");
        i_submissionPanel.add(i_reportformatLabel, "reportformatLabel,1,21,20,4");
        i_submissionPanel.add(i_reportFormatComboBox, "reportFormatComboBox,21,21,20,4");
        
        i_submissionPanel.add(i_msisdnStatusLabel, "msisdnStatusLabel,1,26,20,4");
        i_submissionPanel.add(i_msisdnStatusComboBox, "msisdnStatusComboBox,21,26,20,4");
        i_submissionPanel.add(i_sdpIdLabel, "sdpIdLabel,1,31,20,4");
        i_submissionPanel.add(i_sdpIdComboBox, "sdpIdComboBox,21,31,40,4");

        i_submissionPanel.add(i_adjTypeCodeLabel, "adjTypeCodeLabel,1,36,20,4");
        i_submissionPanel.add(i_adjTypeCodeComboBox, "adjTypeCodeComboBox,21,36,40,4");
        
        i_submissionPanel.add(i_daysUntilExpiryLabel, "daysUntilExpiryLabel,1,41,20,4");
        i_submissionPanel.add(i_daysUntilExpiryField, "daysUntilExpiryField,21,41,6,4");

        i_submissionPanel.add(i_submitButton, "submitButton,1,49,15,5");
        i_submissionPanel.add(i_resetButton, "resetButton,17,49,15,5");
    }

    /** 
     * Initialises the screen data to default values.
     */
    private void initialiseScreen()
    {
        i_reportTypeComboBox.setSelectedIndex(0);

        resetDateTimeFields();
        
        i_frequencyBox.setSelectedIndex(0);
        i_intervalLabel.setVisible(false);
        i_intervalField.setVisible(false);
        i_intervalField.setText("");
        i_daysUntilExpiryField.setText("5");

        i_sdpIds = i_dbService.getAvailableSdpIds();
        i_sdpIdModel = new DefaultComboBoxModel(i_sdpIds);
        i_sdpIdComboBox.setModel(i_sdpIdModel);
        
        i_adjTypeCodes = i_dbService.getAdjCodes();
        i_adjTypeCodeModel = new DefaultComboBoxModel(i_adjTypeCodes);
        i_adjTypeCodeComboBox.setModel(i_adjTypeCodeModel);

        i_sdpIdComboBox.setSelectedIndex(0);
        i_msisdnStatusComboBox.setSelectedIndex(0);
        i_adjTypeCodeComboBox.setSelectedIndex(0);
        
        i_reportFormatComboBox.setSelectedItem(i_dbService.getDefaultReportFormat());
        
        i_state = C_STATE_DATA_RETRIEVED;
    }

    /** 
     * Resets the screen data.
     */
    private void resetScreen()
    {
        resetDateTimeFields();
        
        i_frequencyBox.setSelectedIndex(0);
        i_intervalLabel.setVisible(false);
        i_intervalField.setVisible(false);
        i_intervalField.setText("");
        i_daysUntilExpiryField.setText("5");

        i_sdpIdComboBox.setSelectedIndex(0);
        i_msisdnStatusComboBox.setSelectedIndex(0);
        i_adjTypeCodeComboBox.setSelectedIndex(0);
        
        i_reportFormatComboBox.setSelectedItem(i_dbService.getDefaultReportFormat());
        
        i_state = C_STATE_DATA_RETRIEVED;
    }

    /** 
     * Resets the date and datetime fields.
     */
    private void resetDateTimeFields()
    {
        PpasDateTime l_now;
        PpasDate     l_defaultSelectionDate;

        l_now = DatePatch.getDateTimeNow();
        l_defaultSelectionDate = l_now.getPpasDate();
        l_defaultSelectionDate.add(PpasDate.C_FIELD_DATE, -1);

        i_runDateTimeField.setValue(l_now); 
        i_startDateField.setValue(l_defaultSelectionDate);
        i_endDateField.setValue(l_defaultSelectionDate);
    }

    /**
     * Retrieves the list of reports that are available to the user, and
     * populates the report type combo box accordingly.  Also populates the 
     * report format and MSISDN status dropdowns.
     */
    private void populateListData()
    {
        Object   l_availableReports;
        String[] l_st;
        String   l_info = null;
        int      l_nextToken = 0;

        l_availableReports = ((BoiContext)i_context).getObject("ascs.boi.availableReports");
        
        i_configuredReportTypes.removeAllElements();
        
        if (l_availableReports != null)
        {
            l_st = ((String)l_availableReports).split(";");

            while (l_nextToken < l_st.length)
            {
                l_info = l_st[l_nextToken++].trim();
                i_configuredReportTypes.addElement(l_info.substring(0, l_info.indexOf(',')));
            }
            i_configuredReportTypes.trimToSize();
        }

        i_msisdnStatuses.add("ALL");
        i_msisdnStatuses.add(RoutingData.C_STATUS_ASSIGNED);
        i_msisdnStatuses.add(RoutingData.C_STATUS_AVAILABLE);
        i_msisdnStatuses.add(RoutingData.C_STATUS_REASSIGNABLE);
    }
    
    /**
     * Prepares the report format type dropdown.
     */
    private void populateReportFormatTypes()
    {
    	int                  l_nextToken = 1;
    	String[]             l_st        = null;
    	DefaultComboBoxModel l_model = null;
        String               l_string = null;
    	
    	i_reportFormats.removeAllElements();
    	i_reportFormats.addElement("default");

        l_string = (String)i_reportNamesMap.get(i_reportTypeComboBox.getSelectedItem().toString());
        l_st = l_string.split(",");
        while (l_nextToken < l_st.length)
        {
            if (!l_st[l_nextToken].equalsIgnoreCase("default"))
            {
                i_reportFormats.addElement(l_st[l_nextToken]);
            }

            l_nextToken++;            
        }

        i_reportFormats.trimToSize();
        l_model = new DefaultComboBoxModel(i_reportFormats);
        i_reportFormatComboBox.setModel(l_model);
    }
    
    /**
     * Gets the report name for the supplied report display name.
     * This is used to display the available report formats for the selected report.
     * @param p_displayString The display name of the report.
     * @return The report name.
     */
    private String getReportNameForDisplayString(String p_displayString)
    {
    	String   l_string = null;
    	String[] l_st     = null;
        String   l_reportName   = null;
    	
    	l_string = (String)i_reportNamesMap.get(p_displayString);
        if (l_string != null)
        {
    	    l_st = l_string.split(",");
        }
        
    	l_reportName = (l_st != null) ? l_st[1] : null;
        
        return l_reportName;
    }
    
    /**
     * Gets the JS job type for the supplied report display name.
     * @param p_displayString The String displayed in the Report Type combo-box.
     * @return The JS Job type
     */
    private String getJsJobTypeForDisplayString(String p_displayString)
    {
    	String   l_string = null;
    	String[] l_st     = null;
        String   l_jsJobType = null;
    	
        l_string = (String)i_reportNamesMap.get(p_displayString);
    	l_st = l_string.split(",");
        
        l_jsJobType = (l_st != null) ? l_st[0] : null;
        
    	return l_jsJobType;
    }

    /**
     * Use the BOI context to populate a hash containing reports configuration.
     */
    private void loadDisplayNameToFormatFileNameHash()
    {
    	Object   l_masterReportsInfo = null;
    	int      l_nextToken         = 0;
    	String[] l_st                = null;
    	String   l_detail            = null;
        String   l_reportDisplayName = null;
    	int      l_index             = 0;
    	
        l_masterReportsInfo = ((BoiContext)i_context).getObject("ascs.boi.availableReports");
        
        i_configuredReportTypes.removeAllElements();
        i_reportNamesMap = new HashMap();
        
        if (l_masterReportsInfo != null)
        {
            l_st = ((String)l_masterReportsInfo).split(";");

            while (l_nextToken < l_st.length)
            {
                l_index = l_st[l_nextToken].indexOf(",");
                l_reportDisplayName = l_st[l_nextToken].substring(0,l_index);
                l_detail = l_st[l_nextToken].substring(l_index+1, l_st[l_nextToken].length());
                i_reportNamesMap.put(l_reportDisplayName.trim(), l_detail);
                l_nextToken++;
            }
        }
    	
        i_reportNamesMap.put("","none");
        i_reportNamesMap.put("null", "none");
    }

    /**
     * Handles submit events.
     */
    private void doSubmit()
    {
        boolean         l_valid = false;
        int             l_confirm = 0;
        String          l_selectedReport;
        BoiAdjCodesData l_adjCodesData = null;
        String          l_selectedReportFormat;

        l_valid = validateScreenData();
        if (l_valid)
        {
            l_confirm = displayConfirmDialog(i_contentPane, "Do you wish to submit this job?");
            
            if (l_confirm == JOptionPane.YES_OPTION)
            {
                l_selectedReport = i_reportTypeComboBox.getSelectedItem().toString();
                l_adjCodesData = i_adjTypeCodeComboBox.getSelectedItem().equals("ALL") ? null : 
                                             (BoiAdjCodesData)i_adjTypeCodeComboBox.getSelectedItem();
                l_selectedReportFormat = ((String)i_reportFormatComboBox.getSelectedItem()).trim();

                try
                {
                    i_dbService.submitJob(
                        getJsJobTypeForDisplayString(l_selectedReport),
                        i_runDateTime,
                        i_startSelectionOffset,
                        i_endSelectionOffset,
                        (String)i_msisdnStatusComboBox.getSelectedItem(),
                        (BoiSdpIdData)i_sdpIdComboBox.getSelectedItem(),
                        l_adjCodesData,
                        (String)i_frequencyBox.getSelectedItem(),
                        i_intervalField.getText(),
                        i_daysUntilExpiryField.getText(),
                        l_selectedReportFormat);

                    i_state = C_STATE_DATA_RETRIEVED;
                    displayMessageDialog(i_contentPane, "Job has been submitted");
                }
                catch (PpasServiceFailedException l_pSE)
                {
                    handleException(l_pSE);
                }
            } 
            else
            {
                displayMessageDialog(i_contentPane, "Job submission cancelled");
            }          
        }
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to enable submit processing to be performed in a separate thread.
     */
    public class SubmitRunnable implements Runnable
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------
        
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Only do this if focus was successfully transferred to the submit button.
            // If it is not the focus owner, this would indicate there was a parse
            // error with the previous focus owner.
            if (i_submitButton.isFocusOwner())
            {
                doSubmit();
            }
        }
    }
}
