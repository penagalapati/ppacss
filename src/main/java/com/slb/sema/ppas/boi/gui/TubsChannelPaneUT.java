////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TubsChannelPaneUT.java
//      DATE            :       31-Jan-2007
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PRD_ASCS00_GEN_CA_104
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       JUnit test class for ChannelPane.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 19/02/07 |M.Tornqvist | Group field changed to combobox.| PpacLon#2946/10991
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiChavChannelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for FafChargingIndicatorsPane. */
public class TubsChannelPaneUT extends BoiTestCaseTT
{

  //------------------------------------------------------------------------
  // Constants
  //------------------------------------------------------------------------

  /** Class name used in calls to middleware. */
    private static final String          C_CLASS_NAME          = "channelPaneUT";

    /** Constant used for existing channel. */
    private static final String          C_OLD_CHANNEL         = "OLD";

    /** Constant used for new channel. */
    private static final String          C_NEW_CHANNEL         = "NEW";

    /** Constant used for description for start code. */
    private static final String          C_OLD_DESCRIPTION     = "OLD description";

    /** Constant used for update description. */
    private static final String          C_UPDATE_DESCRIPTION  = "UPDATE description";

    /** Constant used for no group. */
    private static final String          C_GROUP           = "XYZ";

    /** Constant used for starting Channel data in the Defined Codes field. */
    private static final ChavChannelData C_OLD_CHANNEL_DATA    = new ChavChannelData(C_OLD_CHANNEL,
                                                                                     C_OLD_DESCRIPTION,
                                                                                     C_GROUP,
                                                                                     "SUPER",
                                                                                     null,
                                                                                     false);

    /** Constant used for boi Channel data object. */
    private static final BoiChavChannelData C_BOI_CHANNEL_DATA = new BoiChavChannelData(C_OLD_CHANNEL_DATA);

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** TUBS Channel screen. */
    private   TubsChannelPane     i_channelPane;

    /** Text field to hold the code. */
    private   ValidatedJTextField i_codeField;

    /** Text field to hold the channel description. */
    private   ValidatedJTextField i_descriptionField;

    /** Combo box to hold the configured channels. */
    private   JComboBox           i_groupComboBox;

    /** Key data combo box. (Defined Codes) */
    protected JComboBox           i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton             i_updateButton;

    /** Delete button. */
    protected JButton             i_deleteButton;



    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public TubsChannelPaneUT(String p_title)
    {
        super(p_title);

        deleteChavChannelRecord(C_NEW_CHANNEL);

        i_channelPane = new TubsChannelPane(c_context);
        super.init(i_channelPane);

        i_channelPane.resetScreenUponEntry();
        
        i_keyDataCombo     = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField        = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "descriptionField");
        i_groupComboBox    = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "groupComboBox");

        i_updateButton     = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton     = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

  
  
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(TubsChannelPaneUT.class);
    }

  
    /** 
     * @ut.when a user attempts to insert a record through the TUBS Channel screen.
     * @ut.then the operation is successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("TubsChannelPane test");
        BoiChavChannelData l_boiChavChannelData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        if (i_keyDataCombo.getComponentCount() == 0)
        {
            fail("No data configured.");
        }
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_channelPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_OLD_CHANNEL);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_OLD_DESCRIPTION);

        i_groupComboBox.setSelectedItem(C_BOI_CHANNEL_DATA);
        doInsert(i_channelPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiChavChannelData = new BoiChavChannelData( new ChavChannelData(C_OLD_CHANNEL,
                                                                           C_OLD_DESCRIPTION,
                                                                           C_GROUP,
                                                                           "SUPER",
                                                                           null,
                                                                           false));
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_channelPane,
                                                i_keyDataCombo,
                                                l_boiChavChannelData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_UPDATE_DESCRIPTION);
        doUpdate(i_channelPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_channelPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_channelPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_OLD_CHANNEL);
        doMakeAvailable(i_channelPane, i_updateButton);

        endOfTest();

    }

    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteChavChannelRecord(C_OLD_CHANNEL);
    }

  
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
  	  deleteChavChannelRecord(C_OLD_CHANNEL);
        say(":::End Of Test:::");
    }

    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteChavChannelRecord = "deleteChavChannelRecord";
    
    /**
     * Removes a row from CHAV_CHANNEL_VALUE.
     * @param p_channel Channel code.
     */
    protected void deleteChavChannelRecord(String p_channel)
    {        
        String    l_sql = "DELETE FROM chav_channel_values WHERE chav_channel_id = {0}";
        SqlString l_sqlString;
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_channel);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteChavChannelRecord);
    }
    
}