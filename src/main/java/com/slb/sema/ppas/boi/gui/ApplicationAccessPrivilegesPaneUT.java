////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ApplicationAccessPrivilegesPaneUT.java
//      DATE            :       14-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5502
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for 
//                              ApplicationAccessPrivilegesPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ApacApplicationAccessSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrilPrivilegeLevelSqlService;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for ApplicationAccessPrivilegesPane. */
public class ApplicationAccessPrivilegesPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Constant for privilege level id. */
    private static final String C_PRIV_LEVEL = "77";
    
    /** Constant for privilege level description. */
    private static final String C_PRIV_DESCRIPTION = "Privilege level description";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Application Access Privileges screen. */
    private ApplicationAccessPrivilegesPane i_appAccessPrivilegesPane;
    
    /** Check box to hold Business Configuration access privilege. */
    private JCheckBox i_businessConfigurationBox = null;
    
    /** SQL service for Privilege Level data. */
    private PrilPrivilegeLevelSqlService i_prilSqlService = null;
    
    /** SQL service for Application Access data. */
    private ApacApplicationAccessSqlService i_apacSqlService = null;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public ApplicationAccessPrivilegesPaneUT(String p_title)
    {
        super(p_title);
        
        // Insert test data now such that it is picked up by readInitialData
        i_prilSqlService = new PrilPrivilegeLevelSqlService(null, null);
        i_apacSqlService = new ApacApplicationAccessSqlService(null, null);
        insertPrivilegeRecord(C_PRIV_LEVEL, C_PRIV_DESCRIPTION);
        
        i_appAccessPrivilegesPane = new ApplicationAccessPrivilegesPane(c_context);
        super.init(i_appAccessPrivilegesPane);
        
        i_appAccessPrivilegesPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "privilegeBox");
        i_businessConfigurationBox = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "businessConfigurationBox");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(ApplicationAccessPrivilegesPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to select and update a record 
     *     through the Application Access Privileges screen screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        PrilPrivilegeLevelData l_prilData;
        
        beginOfTest("ApplicationAccessPrivilegesPane test");
        
        // *********************************************************************
        say("Selecting record.");
        // *********************************************************************
        
        l_prilData = createPrivilegeLevelData(C_PRIV_LEVEL, C_PRIV_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_appAccessPrivilegesPane, i_keyDataCombo, l_prilData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setCheckBoxValue(i_businessConfigurationBox, true);
        doUpdate(i_appAccessPrivilegesPane, i_updateButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        // Do nothing
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a PrilPrivilegeLevelData object with the supplied data.
     * @param p_privilegeLevelId Privilege level id.
     * @param p_privilegeLevelDesc Privilege level description.
     * @return PrilPrivilegeLevelData object created from the supplied data.
     */
    private PrilPrivilegeLevelData createPrivilegeLevelData(String p_privilegeLevelId,
                                                           String p_privilegeLevelDesc)
    {
        PrilPrivilegeLevelData l_prilData;
        
        l_prilData = new PrilPrivilegeLevelData(null,
                                                Integer.parseInt(p_privilegeLevelId),
                                                p_privilegeLevelDesc,
                                                " ");
        
        return (l_prilData);
    }
    
    /**
     * Inserts a privilege level record into PRIL_PRIVILEGE_LEVEL.
     * @param p_privilegeLevelId Privilege level id.
     * @param p_privilegeLevelDesc Privilege level description.
     */
    private void insertPrivilegeRecord(String p_privilegeLevelId,
                                       String p_privilegeLevelDesc)
    {
        try
        {
            i_prilSqlService.insert(null,
                                    c_context.getConnection(),
                                    p_privilegeLevelId,
                                    p_privilegeLevelDesc,
                                    C_BOI_OPID);
        }
        catch (PpasSqlException l_ex)
        {
            if (l_ex.getMsgKey().equals("DUPLICATE_KEY"))
            {
                // Do nothing
            }
            else
            {
                fail("Failed to insert PRIL record.");
            }
        }
        
        try
        {
            i_apacSqlService.insert(null,
                                    c_context.getConnection(),
                                    p_privilegeLevelId,
                                    'N',
                                    'N',
                                    'N',
                                    'N',
                                    ' ',
                                    C_BOI_OPID);
        }
        catch (PpasSqlException l_ex)
        {
            if (l_ex.getMsgKey().equals("DUPLICATE_KEY"))
            {
                try
                {
                    i_apacSqlService.update(null,
                                            c_context.getConnection(),
                                            Integer.parseInt(p_privilegeLevelId),
                                            'N',
                                            'N',
                                            'N',
                                            'N',
                                            C_BOI_OPID);
                }
                catch (PpasSqlException l_e)
                {
                    fail("Failed to update APAC record.");
                }
            }
            else
            {
                fail("Failed to insert APAC record.");
            }
        }
    }
}
