////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiLoginServlet.java
//      DATE            :       19-Oct-2003
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PRD_ASCS00_DEV_SS_88
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Provides login services for the BOI applet.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 16/09/05 |M I Erskine | Check password isn't expired    | PpacLon#1483/7096
//          |            | when logging into BOI.          |
//          |            | Create Logger correctly.        |
//----------+------------+---------------------------------+--------------------
// 18/01/06 | Paul Rosser| Added instrument manager to     | PpacLon#1923/7813
//          |            | allow Logger to be used in A&I  |
//----------+------------+---------------------------------+------------------
// 24/01/06 | Nick Whalan| Changed the property object     | PpacLon#1864/7793
//          |(h79176d)   | passed to Debug                 |
//----------+------------+---------------------------------+--------------------
// 27/07/06 | K Goswami  | Code changes due to changes in  | PpasLon#2645/9995
//          |            | the BusinessConfigCache class   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boiservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigTables;
import com.slb.sema.ppas.common.dataclass.LogInOutOrigin;
import com.slb.sema.ppas.common.dataclass.OperatorData;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcConnectionPool;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.SimpleLogInOut;
import com.slb.sema.ppas.is.isapi.PpasSessionService;
import com.slb.sema.ppas.util.crypto.Blowfish;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * Servlet class to provide login services for BOI.
 */
public class BoiLoginServlet extends HttpServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "BoiLoginServlet";
    
    /** Timeout for retrieving a JDBC connection. */
    private static final long C_DATABASE_CONNECTION_TIMEOUT_MILLIS = 20000;
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** Logger for use in logging server side info. */
    protected Logger             i_logger = null;
    
    /** Container for objects used in a context. */
    protected PpasContext        i_context = null;
    
    /** Service to provide client session management. */
    protected PpasSessionService i_sessionService = null;
    
    /** Contains the required properties by this servlet. */
    protected PpasProperties     i_properties = null; 
    
    /** Used to hold the encrypted database username. */
    private String             i_encryptedDatabaseUsername;
    
    /** Used to hold the encrypted database password. */
    private String             i_encryptedDatabasePassword;
    
    /** Used to hold the encrypted database URL. */
    private String             i_encryptedDatabaseUrl;    
    
    //-------------------------------------------------------------------------
    // Object methods
    //-------------------------------------------------------------------------

    /** Used in calls to middleware. */
    private static final String C_METHOD_doGet = "doGet";
    /**
     * Ovirrides the method on HttpServlet to handle requests.
     * @param p_request  The http request received.
     * @param p_response The http response to send back.
     */
    protected void doGet(HttpServletRequest  p_request,
                      HttpServletResponse p_response)
    {
        String                  l_userName;
        String                  l_password;
        String                  l_newPassword  = null;
        String                  l_command;
        PrintWriter             l_writer       = null;
        String                  l_forwardUrl;
        OperatorData            l_operatorData = null;
        String                  l_status       = null;
        long                    l_flags        = 0;

        l_userName = p_request.getParameter("userName");
        l_password = p_request.getParameter("password");
        l_command  = p_request.getParameter("command");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_START,
                    null, C_CLASS_NAME, 10400, this,
                    "Entered " + C_METHOD_doGet + ", command = " + l_command);
        }
        
        try
        {
            if (l_command == null)
            {
                // this is a request to get the splash screen
                l_forwardUrl = "/jsp/boisplash.jsp";
            
                // Attempt to forward to the required URL. If this fails due to a runtime
                // exception then forward to the URL of the ccError page.
                try
                {
                    forwardToUrl(p_request, p_response, l_forwardUrl);
                }
                catch (RuntimeException l_rtE)
                {
                    l_rtE.printStackTrace();
                }
            }
            else if (l_command.equals("authenticate"))
            {
                // TODO Should this be stored as part of the session ?
                SimpleLogInOut l_session = new SimpleLogInOut();
                l_operatorData = i_sessionService.login(
                                     null,
                                     0,
                                     C_DATABASE_CONNECTION_TIMEOUT_MILLIS,
                                     l_session,
                                     new LogInOutOrigin(PpasSessionService.C_BOI_USER,
                                                        i_properties.getProcessName(),
                                                        p_request.getRemoteAddr() + " (" +
                                                        p_request.getRemoteHost() + ")"),
                                     l_userName,
                                     l_password);
                
                if (l_operatorData.getPasswordExpiryDate() != null && 
                    l_operatorData.getPasswordExpiryDate().isSet())
                {
                    l_status = l_operatorData.isPasswordExpired() ? "PASSWORD_EXPIRED" : "SUCCESS";
                }
                else
                {
                    // Either the expiry date is null or is an empty date,
                    // so assume password is currently valid.
                    l_status = "SUCCESS";
                }
                    
                p_response.setStatus(HttpServletResponse.SC_OK);
                
                l_writer = p_response.getWriter();

                // If we get this far we've been authenticated - 
                // send the encrypted response.
                l_writer.println("status=" + l_status + "&userName=" + i_encryptedDatabaseUsername +
                                 "&password=" + i_encryptedDatabasePassword +
                                 "&dbUrl=" + i_encryptedDatabaseUrl);

                l_writer.flush();
            }
            else if (l_command.equals("INIT"))
            {
                // Forward to URL where a JSP file retrieves required information from
                // the properties files and displays the login screen 
                
                l_forwardUrl = "/jsp/boilogin.jsp";
            
                // Attempt to forward to the required URL. If this fails due to a runtime
                // exception then forward to the URL of the ccError page.
                try
                {
                    forwardToUrl(p_request, p_response, l_forwardUrl);
                }
                catch (RuntimeException l_rtE)
                {
                    l_rtE.printStackTrace();

                    //l_forwardUrl = new String("/jsp/cc/ccerror.jsp");
                    //forwardToUrl(p_request, p_response, l_forwardUrl);
                }
            }
            else if (l_command.equals("CHANGE_PASSWORD"))
            {
                l_newPassword = p_request.getParameter("newPassword");

                l_flags = PpasSessionService.C_FLAG_USE_CONFIGURED_PASSWORD_VALID_DAYS;
                
                i_sessionService.changePassword(
                                     null,
                                     l_flags,
                                     C_DATABASE_CONNECTION_TIMEOUT_MILLIS,
                                     l_userName,
                                     l_newPassword,
                                     null);
                    
                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                // If we get this far we've successfully change the password - 
                // send the encrypted response.
                l_writer.println("status=SUCCESS");
                l_writer.flush();
            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_END,
                        null, C_CLASS_NAME, 10500, this,
                        "Leaving " + C_METHOD_doGet + ", command = " + l_command);
            }
        }
        catch (Exception l_e)
        {
            i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                                  LoggableInterface.C_SEVERITY_ERROR));
                                
            if (l_writer != null)
            {
                // If we get this far the username/password combo was invalid.
                l_writer.println("status=FALSE");
                l_writer.flush();
            }
        }        
    }
    
    /** Used for calls to middleware. */ 
    private static final String C_METHOD_init = "init";        
    /**
     * Initialises the business config cache, encrypts the database access details, adds objects required 
     * by the <code>BoiFileServicesServlet</code> to the servlet context.
     * @throws ServletException
     */
    public void init()
        throws ServletException
    {
        long                    l_flags;
        BusinessConfigCache     l_cache;
        JdbcConnectionPool      l_pool;
        JdbcConnection          l_connection;
        String                  l_databaseUsername;
        String                  l_databasePassword;
        String                  l_databaseUrl;
        byte[]                  l_databaseUsernameByteArray = null;
        byte[]                  l_databasePasswordByteArray = null;
        byte[]                  l_databaseUrlByteArray = null;
        Blowfish                l_blowfish = null;
        ServletContext          l_servletContext;
        PpasLoggerPool          l_loggerPool = null;
        InstrumentManager       l_instrumentManager  = null;
        
        try
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START,
                        null, C_CLASS_NAME, 10300, this,
                        "Entered " + C_METHOD_init);
            }
            i_properties = new PpasProperties();
            i_properties.loadLayeredProperties();
            
            PpasDebug.setDebug(PpasDebug.C_FLAG_SYSTEM_OVERRIDES, 
                               i_properties);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                        null, C_CLASS_NAME, 10030, this,
                        "Debug turned on in " + C_METHOD_init);
            }

            DatePatch.init(C_CLASS_NAME,
                           C_METHOD_init,
                           10050,
                           this,
                           null,
                           0,
                           null,
                           i_properties);

            try
            {
                l_loggerPool = new PpasLoggerPool("LoggerPool",
                                                  i_properties,
                                                  System.getProperty("ascs.procName"));
            }
            catch (PpasConfigException l_configE)
            {
                //
                // Can't log (logging not initialised yet), so write to System.err.
                //
                System.err.println("FATAL ERROR STARTING BOI APPLICATION : ");
                l_configE.printStackTrace(System.err);

                // If a previous system initialisation error has not already
                // occured, then store this one so that it can later by presented
                // as a sensible response message.
                //if (PpasServlet.i_sysInitE == null)
                //{
                //    PpasServlet.i_sysInitE = l_configE;
                //}

                throw (l_configE);
            }

            i_logger = l_loggerPool.getLogger();

            if (i_logger == null)
            {
                // The requested logger could not be found ... create and throw
                // appropriate exception

                // Create and throw a 'missing configuration' exception
                PpasConfigException l_configE = new PpasConfigException(
                        C_CLASS_NAME,
                        C_METHOD_init,
                        10420,
                        this,
                        null,
                        0L,
                        ConfigKey.get().badLogger(System.getProperty("ascs.procName"), "LoggerPool"));

                // Can't log (logging not initialised yet), so write to System.err.
                System.err.println ("FATAL ERROR STARTING BOI APPLICATION : ");
                l_configE.printStackTrace(System.err);

                // If a previous system initialisation error has not already
                // occured, then store this one so that it can later by presented
                // as a sensible response message.
                //if (PpasServlet.i_sysInitE == null)
                //{
                //    PpasServlet.i_sysInitE = l_configE;
                //}

                throw (l_configE);
            }

            i_context = new PpasContext(i_logger);
            
            
            // Need to sort out flags to exclude various objects from the Context.
            l_flags = 0;
            
            i_context.init(null,
                           l_flags,
                           i_properties,
                           this.getClass().getName());

            l_instrumentManager = (InstrumentManager)
            i_context.getAttribute("com.slb.sema.ppas.util.instrumentation.InstrumentManager");

            i_logger.setInstrumentManager(l_instrumentManager);
            
            // Get a jdbc connection and use it to load up the system
            // configuration data. Unfortunately we need to load the cache to 
            // use the PpasSessionService.
            l_cache = (BusinessConfigCache)i_context.getAttribute("BusinessConfigCache");
                                   
            l_pool = (JdbcConnectionPool)i_context.getAttribute("JdbcConnectionPool");

            l_connection = l_pool.getConnection(C_CLASS_NAME, 
                                                C_METHOD_init,
                                                10800,
                                                null, 
                                                (PpasRequest)null,
                                                0,
                                                20000);

            l_cache.loadSpecified((PpasRequest)null,
                                  l_connection,
                                  BusinessConfigTables.C_SHORT_SYFG_SYSTEM_CONFIG);
                                          
            i_sessionService = new PpasSessionService(null,
                                                      i_logger,
                                                      i_context);

            l_servletContext = getServletContext();
            l_servletContext.setAttribute("com.slb.sema.ppas.is.isapi.PpasSessionService", i_sessionService);
            l_servletContext.setAttribute("com.slb.sema.ppas.common.support.PpasContext", i_context);
            l_servletContext.setAttribute("com.slb.sema.ppas.common.support.PpasProperties", i_properties);
            l_servletContext.setAttribute("com.slb.sema.ppas.util.logging.Logger", i_logger);
            l_servletContext.setAttribute("com.slb.sema.ppas.common.sql.JdbcConnection", l_connection);
            
            l_databaseUsername = i_properties.getTrimmedProperty(
                "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.username");
                    
            l_databasePassword = i_properties.getTrimmedProperty(
                "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.password");
                    
            l_databaseUrl = i_properties.getTrimmedProperty(
                "com.slb.sema.ppas.common.sql.JdbcConnectionPool.JDBC_Pool.dbUrl");
                    
            // Encrypt these properties
                    
            // The information to be encrypted must be in the form of a byte array
            l_databaseUsernameByteArray = l_databaseUsername.getBytes();
            l_databasePasswordByteArray = l_databasePassword.getBytes();
            l_databaseUrlByteArray = l_databaseUrl.getBytes();
                    
            // Set the Blowfish key
            l_blowfish = new Blowfish(null);
            l_blowfish.setSlbBlowfishKey();
                    
            // Blowfish encrypt
            i_encryptedDatabaseUsername = l_blowfish.encryptByteArray(l_databaseUsernameByteArray);
            i_encryptedDatabasePassword = l_blowfish.encryptByteArray(l_databasePasswordByteArray);
            i_encryptedDatabaseUrl = l_blowfish.encryptByteArray(l_databaseUrlByteArray);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_END,
                        null, C_CLASS_NAME, 10410, this,
                        "Leaving " + C_METHOD_init + " successfully.");
            }
        }
        catch (Exception l_e)
        {
            if (i_logger != null)
            {
                i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                                      LoggableInterface.C_SEVERITY_ERROR));
            }
            else
            {
                l_e.printStackTrace();
            }
            
            throw new ServletException("Fatal Error attempting to initialise BOI", l_e);
        }
    }

    /**
     * Forwards the request to the splash screen or the login JSP, depending on the command.
     * @param p_request The http request
     * @param p_response The http response
     * @param p_forwardUrl The URL to attempt to forward to
     * @throws ServletException
     * @throws IOException
     */
    private void forwardToUrl(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        String                 p_forwardUrl
        )
        throws ServletException, IOException
    {

        RequestDispatcher      l_rd;

        if (p_forwardUrl != null)
        {
            // Need to do a forward.
            l_rd = getServletContext().getRequestDispatcher(p_forwardUrl);

            if (l_rd != null)
            {
                if (PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_HIGH,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE,
                        null,
                        C_CLASS_NAME, 11500, null,
                        "Forwarding request to " + p_forwardUrl);

                l_rd.forward(p_request, p_response);

                if (PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_HIGH,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE,
                        null,
                        C_CLASS_NAME, 11600, null,
                        "Forward returned from " + p_forwardUrl);
            }
            else
            {
                if (PpasDebug.on) PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_ERROR,
                        null,
                        C_CLASS_NAME, 11700, null,
                        "Internal software error - no " +
                        "request dispatcher found for " + p_forwardUrl);
            }
        }
    }
}
