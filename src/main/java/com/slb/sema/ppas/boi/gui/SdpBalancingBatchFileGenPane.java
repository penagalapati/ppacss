////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SdpBalancingBatchFileGenPane.java
//    DATE            :       26-July-2005
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#1460/6893
//                            PRD_ASCS00_GEN_CA_56
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       BOI SDP Balancing Batch File Generator screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.dbservice.BalanceSdpsDbService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.util.support.Debug;

/** BOI SDP Balancing Batch File Generation screen. */
public class SdpBalancingBatchFileGenPane extends FocussedBoiGuiPane
   implements PropertyChangeListener, ItemListener
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "SdpBalancingBatchFileGenPane";

    /** Default maximum range of MSISDNs. */
    //private static final int C_DEFAULT_MSISDN_NUMBER_RANGE = 100000;

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Main panel for the screen. */
    private JPanel i_mainPanel;

    /** Panel into which the user specifies the name of the file and the file header. */
    private JPanel i_fileDetailsPanel;
    
    /** Panel where the user specifies the MSISDN range to add to the MSISDN Ranges table. */
    private JPanel i_addMsisdnRangePanel;
    
    /** Panel containing the table of MSISDN ranges to add to the file. */
    private JPanel i_addedRangesPanel;

    /** Field for entering start MSISDN. */
    private JFormattedTextField i_startRangeMsisdnTextField;

    /** Field for entering end MSISDN. */
    private JFormattedTextField i_endRangeMsisdnTextField;

    /** Button to generate file. */
    private JButton i_generateButton;

    /** Button to reset screen. */
    private JButton i_resetButton;

    /** BOI server host. */
    private String i_serverHost;

    /** BOI server port. */
    private String i_loginPort;

    /** Database access class for MSISDN generation screen. */
    private BalanceSdpsDbService i_dbService;

    /** Vector of available old SDP ids. */
    private Vector i_oldAvailableSdpIds;
    
    /** Vector of available new SDP ids. */
    private Vector i_newAvailableSdpIds;

    /** Combo box model for SDP ids. */
    private DefaultComboBoxModel i_sdpIdModel;

    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;

    /** Text field to hold the name of the file that is to be generated on the server. */
    private ValidatedJTextField i_fileNameTextBox;

    /** Combo box to hold the old SDP Id and description. */
    private JComboBox i_oldSdpIdComboBox;
    
    /** Combo box to hold the new SDP Id and description. */
    private JComboBox i_newSdpIdComboBox;

    /** Table model. */
    private StringTableModel i_stringTableModel;

    /** Table object for this screen. */
    private JTable i_table;
    
    /** Array to hold the data for the table. */
    private String[][] i_data;
    
    /** Button used to add number ranges into the table. */
    private GuiButton i_addButton;

    /** Button to delete number ranges from the table. */
    private GuiButton i_deleteButton;

    /** Text box to display the IP address of the old SDP. */
    private ValidatedJTextField i_oldSdpIpAddrTextBox;

    /** Text box to display the IP address of the new SDP. */
    private ValidatedJTextField i_newSdpIpAddrTextBox;
    
    /** Pointer to the next free (empty) row in this screen's Added MSISDN ranges table. */
    private int i_nextFreeRangesRow = 0;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * SdpBalancingBatchFileGenPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public SdpBalancingBatchFileGenPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new BalanceSdpsDbService((BoiContext)i_context);
        i_serverHost = (String)i_context.getMandatoryObject(
                            "applet.server.host");
    
        i_loginPort = (String)i_context.getMandatoryObject(
                             "applet.server.login.port");

//        Object l_maxNumberRange = ((BoiContext)i_context).getObject("ascs.boi.maxNumberRange");

//        try
//        {
//            i_maxNumberRange = 
//                (l_maxNumberRange == null) ? C_DEFAULT_MSISDN_NUMBER_RANGE : 
//                                                 Integer.parseInt((String)l_maxNumberRange);
//        }
//        catch (NumberFormatException l_nfE)
//        {
//            i_maxNumberRange = C_DEFAULT_MSISDN_NUMBER_RANGE;
//            handleException(l_nfE);
//        }
        paintScreen();

        i_contentPane = i_mainPanel;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_mainPanel.setFocusCycleRoot(true);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles keyboard events for the screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;

        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()) && p_keyEvent.getKeyCode() == 10
                && p_keyEvent.getID() == KeyEvent.KEY_PRESSED)
        {
            if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else
            {
                i_generateButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }

    /**
     * Handles button events.
     * @param p_actionEvent Action event.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        Object  l_source  = null;
        int     l_confirm = 0;
        boolean l_valid   = false;
        
        l_source = p_actionEvent.getSource();

        if (l_source == i_addButton)
        {
            l_valid = validateRangeToAdd();
            //System.out.println("MIE Source is i_addButton");
            
            if (l_valid)
            {
                i_data[i_nextFreeRangesRow][0] = i_startRangeMsisdnTextField.getText().trim();
                i_data[i_nextFreeRangesRow++][1] = i_endRangeMsisdnTextField.getText().trim();
                i_stringTableModel.setData(i_data);
                //i_table.setModel(i_stringTableModel);
            
                i_startRangeMsisdnTextField.setText("");
                i_endRangeMsisdnTextField.setText("");
            }
        }
        else if (l_source == i_deleteButton)
        {
            //TODO Should only delete selected row. If no row selected, display error dialog box
            
            i_data[--i_nextFreeRangesRow][0] = "";
            i_data[i_nextFreeRangesRow][1] = "";
            i_stringTableModel.setData(i_data);
        }
        else if (l_source == i_generateButton)
        {
            l_valid = validateScreenData();

            if (l_valid)
            {
                l_confirm = displayConfirmDialog(i_contentPane, "Do you wish to submit this job?");

                if (l_confirm == JOptionPane.YES_OPTION)
                {
                    doGeneration();
                    displayMessageDialog(i_contentPane, "Job has been submitted");
                }
                else
                {
                    displayMessageDialog(i_contentPane, "Job submission cancelled");
                }
            }
        }
        else if (l_source == i_resetButton)
        {
            initialiseScreen();
        }
    }

    /**
     * Handles combo box events.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        String       l_ipAddress    = null;
        
        if (p_itemEvent.getSource() == i_oldSdpIdComboBox)
        {
            if (i_oldSdpIdComboBox.getSelectedItem() instanceof BoiSdpIdData)
            {
                l_ipAddress = ((BoiSdpIdData)i_oldSdpIdComboBox.getSelectedItem()).getInternalSdpIdData().getScpIpAddress();
                i_oldSdpIpAddrTextBox.setText(l_ipAddress);
            }
            else
            {
                i_oldSdpIpAddrTextBox.setText("");
            }
        }
        else if (p_itemEvent.getSource() == i_newSdpIdComboBox)
        {
            if (i_newSdpIdComboBox.getSelectedItem() instanceof BoiSdpIdData)
            {
                l_ipAddress = ((BoiSdpIdData)i_newSdpIdComboBox.getSelectedItem()).getInternalSdpIdData().getScpIpAddress();
                i_newSdpIpAddrTextBox.setText(l_ipAddress);
            }
            else
            {
                i_newSdpIpAddrTextBox.setText("");
            }
        }
        else
        {
            //Some other source
            //System.out.println("MIE Some other source.");
        }
    }

    /**
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_fileNameTextBox.requestFocusInWindow())
        {
            System.out.println("Error in requestFocusInWindow!");
        }
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        try
        {
            i_dbService.readInitialData();
            initialiseScreen();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
    }

    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * Handles GUI events.
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Validates screen data before submit.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        String l_startMsisdnStr = i_startRangeMsisdnTextField.getText().trim();
        String l_endMsisdnStr = i_endRangeMsisdnTextField.getText().trim();
        
        if (i_fileNameTextBox.getText().equals("") || i_fileNameTextBox.getText() == null)
        {
            i_fileNameTextBox.requestFocusInWindow();
            displayMessageDialog(
                i_contentPane,
                "Please enter a file name (in the format SUBSCRIBER_sssss.dat)");
            return false;
        }
        
        if (i_oldSdpIdComboBox.getSelectedIndex() == 0)
        {
            i_oldSdpIdComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please select the old Sdp Id");
            return false;
        }
        
        if (i_newSdpIdComboBox.getSelectedIndex() == 0)
        {
            i_newSdpIdComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please select a new Sdp Id");
            return false;
        }

        if (i_oldSdpIdComboBox.getSelectedIndex() == i_newSdpIdComboBox.getSelectedIndex())
        {
            i_newSdpIdComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Old SDP Id and New SDP Id are the same");
            return false;
        }
        
        if (l_startMsisdnStr.length() != 0)
        {
            i_startRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please submit outstanding MSISDN Range");
            return false; 
        }
        
        if (l_endMsisdnStr.length() != 0)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please submit outstanding MSISDN Range");
            return false; 
        }
        
        //TODO: Maybe make this screen a bit more clever by getting the code below working?
//        if (l_startMsisdnStr.length() != 0) 
//        {
//            if (l_endMsisdnStr.length() != 0)
//            {
//                // Start number not blank, end number not blank
//                i_startRangeMsisdnTextField.requestFocusInWindow();
//                l_dialogueResponse = 
//                    displayConfirmDialog(i_contentPane, "Do you wish to add the outstanding range to the table?");
//            
//                if (l_dialogueResponse == JOptionPane.YES_OPTION)
//                {
//                    i_addButton.doClick();
//                }
//                else
//                {
//                    displayMessageDialog(i_contentPane, "Outstanding range ignored");
//                }
//            }
//            else
//            {
//                // Start number not blank, end number blank 
//                i_endRangeMsisdnTextField.requestFocusInWindow();
//                displayMessageDialog(
//                    i_contentPane,
//                    "Not a valid number range");
//            }
//        }
//        else if (l_endMsisdnStr.length() != 0)
//        {
//            // Start number blank, end number not blank
//            i_startRangeMsisdnTextField.requestFocusInWindow();
//            
//            i_endRangeMsisdnTextField.requestFocusInWindow();
//            displayMessageDialog(
//                i_contentPane,
//                "Not a valid number range");
//        }

//        if (l_endMsisdnStr.length() == 0)
//        {
//            i_endRangeMsisdnTextField.requestFocusInWindow();
//            displayMessageDialog(i_contentPane, "Please enter an end MSISDN for the range");
//            return false;
//        }

        if (l_startMsisdnStr.length() != l_endMsisdnStr.length())
        {
            i_startRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Both Msisdns must be the same length");
            return false;
        }

        return true;
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    private void paintScreen()
    {
        BoiHelpListener l_helpComboListener;

        i_mainPanel = WidgetFactory.createMainPanel("SDP Balancing Batch File Generator", 100, 100, 0, 0);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        
        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_BALANCE_SDPS_SCREEN), l_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createFileDetailsPanel();
        createAddMsisdnRangePanel();
        createAddedRangesPanel();
        i_generateButton = WidgetFactory.createButton("Generate", this, true);
        i_resetButton = WidgetFactory.createButton("Reset", this, false);

        i_mainPanel.add(i_helpComboBox, "sdpBalancingBatchFileGenHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_fileDetailsPanel, "submissionPanel,1,6,100,20");
        i_mainPanel.add(i_addMsisdnRangePanel, "addMsisdnRangePanel,1,27,100,12");
        i_mainPanel.add(i_addedRangesPanel, "addedRangesPanel,1,40,100,55");
        
        i_mainPanel.add(i_generateButton, "generateButton,1,96,15,5");
        i_mainPanel.add(i_resetButton, "resetButton,17,96,15,5");
    }

    /** Creates the file details panel. */
    private void createFileDetailsPanel()
    {
        JLabel l_fileNameLabel;
        JLabel l_oldSdpIdLabel;
        JLabel l_newSdpIdLabel;

        i_fileDetailsPanel = WidgetFactory.createPanel("File Details", 100, 20, 0, 0);

        l_fileNameLabel = WidgetFactory.createLabel("File name: ");
        i_fileNameTextBox = WidgetFactory.createTextField(25);
        addValueChangedListener(i_fileNameTextBox);

        try
        {
            i_dbService.readInitialData();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }

        l_oldSdpIdLabel = WidgetFactory.createLabel("Old SDP Id: ");
        i_oldAvailableSdpIds = i_dbService.getAvailableSdpIds();
        i_oldSdpIdComboBox = WidgetFactory.createComboBox(i_oldAvailableSdpIds, this);
        addValueChangedListener(i_oldSdpIdComboBox);
        i_oldSdpIpAddrTextBox = WidgetFactory.createTextField(30);
        i_oldSdpIpAddrTextBox.setEnabled(false);

        
        l_newSdpIdLabel = WidgetFactory.createLabel("New SDP Id: ");
        i_newAvailableSdpIds = i_dbService.getAvailableSdpIds();
        i_newSdpIdComboBox = WidgetFactory.createComboBox(i_newAvailableSdpIds, this);
        addValueChangedListener(i_newSdpIdComboBox);
        i_newSdpIpAddrTextBox = WidgetFactory.createTextField(30);
        i_newSdpIpAddrTextBox.setEnabled(false);

        i_fileDetailsPanel.add(l_fileNameLabel, "fileNameLabel,1,1,20,4");
        i_fileDetailsPanel.add(i_fileNameTextBox, "fileNameTextBox,21,1,25,4");
        i_fileDetailsPanel.add(l_oldSdpIdLabel, "oldSdpIdLabel,1,6,20,4");
        i_fileDetailsPanel.add(i_oldSdpIdComboBox, "oldSdpIdComboBox,21,6,30,4");
        i_fileDetailsPanel.add(i_oldSdpIpAddrTextBox, "oldSdpIpAddrComboBox,52,6,30,4");
        i_fileDetailsPanel.add(l_newSdpIdLabel, "newSdpIdLabel,1,11,20,4");
        i_fileDetailsPanel.add(i_newSdpIdComboBox, "newSdpIdComboBox,21,11,30,4");
        i_fileDetailsPanel.add(i_newSdpIpAddrTextBox, "newSdpIpAddrComboBox,52,11,30,4");

    }
    
    /** Creates the Add Msisdn Range panel. */
    private void createAddMsisdnRangePanel()
    {
        JLabel l_startRangeLabel;
        JLabel l_endRangeLabel;

        i_addMsisdnRangePanel = WidgetFactory.createPanel("Add MSISDN Range", 100, 10, 0, 0);

        l_startRangeLabel = WidgetFactory.createLabel("Start range MSISDN: ");
        i_startRangeMsisdnTextField = WidgetFactory.createMsisdnField(15);
        addValueChangedListener(i_startRangeMsisdnTextField);

        l_endRangeLabel = WidgetFactory.createLabel("End range MSISDN: ");
        i_endRangeMsisdnTextField = WidgetFactory.createMsisdnField(15);
        addValueChangedListener(i_endRangeMsisdnTextField);


        i_addButton = WidgetFactory.createButton("Add", this, false);

        i_addMsisdnRangePanel.add(l_startRangeLabel, "startRangeLabel,1,1,20,4");
        i_addMsisdnRangePanel.add(i_startRangeMsisdnTextField, "startRangeMsisdnTextField,21,1,20,4");
        i_addMsisdnRangePanel.add(l_endRangeLabel, "endRangeLabel,1,6,20,4");
        i_addMsisdnRangePanel.add(i_endRangeMsisdnTextField, "endRangeMsisdnTextField,21,6,20,4");

        i_addMsisdnRangePanel.add(i_addButton, "addButton,42,6,15,5");
    }
    
    /**
     * Creates the panel to hold the added MSISDN number ranges that will go into the file.
     */
    private void createAddedRangesPanel()
    {
        String[] l_columnNames = {"Start range", "End range"};
        
        i_addedRangesPanel = WidgetFactory.createPanel("Added MSISDN Ranges", 100, 55, 0, 0);
        i_deleteButton = WidgetFactory.createButton("Delete", this, false);

        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table = WidgetFactory.createTable(150, 50, i_stringTableModel, this);

        i_addedRangesPanel.add(new JScrollPane(
                                 i_table,
                                 JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                 JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED),
                                 "i_table,1,1,70,55");
        
        populateDefinedBlocksTable();
        
        i_addedRangesPanel.add(i_deleteButton, "deleteButton,72,51,15,5");
    }

    /**
     * Initialises the screen data to default values.
     */
    private void initialiseScreen()
    {
        i_fileNameTextBox.setText("");
        i_startRangeMsisdnTextField.setValue("");
        i_endRangeMsisdnTextField.setValue("");
        i_oldSdpIdComboBox.setSelectedIndex(0);
        i_newSdpIdComboBox.setSelectedIndex(0);
        
        //TODO: This can be made more efficient because we know that only some of the table rows are populated.
        for (int i = 0; i < i_data.length; i++)
        {
            i_data[i][0] = "";
            i_data[i][1] = "";
        }
        
        i_nextFreeRangesRow = 0;
        
        i_state = C_STATE_DATA_RETRIEVED;
    }
    
    /**
     * Populates the Number Blocks table.
     */
    private void populateDefinedBlocksTable()
    {
        int l_numRecords = 0;
        int l_arraySize = 0;
        
        l_arraySize = ((l_numRecords > 19) ? l_numRecords : 20);
        i_data = new String[l_arraySize][2];

        i_stringTableModel.setData(i_data);
    }
    
    /**
     * Validates the number range to be added to the table.
     * @return True if the Number Range to add is valid, false otherwise.
     */
    private boolean validateRangeToAdd()
    {
        String l_startMsisdnStr = null;
        String l_endMsisdnStr   = null;
        long   l_startMsisdn    = 0;
        long   l_endMsisdn      = 0;
        
        l_startMsisdnStr = i_startRangeMsisdnTextField.getText().trim();
        l_endMsisdnStr   = i_endRangeMsisdnTextField.getText().trim();
        
        //Add the contents of start range MSISDN and end range MSISDN into the table.
        
        if (l_startMsisdnStr.length() == 0)
        {
            i_startRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please enter a start MSISDN for the range");
            return false;
        }

        if (l_endMsisdnStr.length() == 0)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please enter an end MSISDN for the range");
            return false;
        }

        l_startMsisdn = Long.parseLong(l_startMsisdnStr);
        l_endMsisdn = Long.parseLong(l_endMsisdnStr);
        
        if ((l_endMsisdn - l_startMsisdn) < 0)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Start range Msisdn must be less than or equal to End range Msisdn");
            return false;
        }
        
        if (l_startMsisdnStr.length() != l_endMsisdnStr.length())
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Start and End Range must be the same length");
            return false;
        }
        
        //System.out.println("MIE i_nextFreeRangesRow: " + i_nextFreeRangesRow);
        
        for (int i = 0; i < i_nextFreeRangesRow /*i_data.length*/; i++)
        {
            if ( (Long.parseLong(i_data[i][0]) <= l_endMsisdn) &&
                 (Long.parseLong(i_data[i][1]) >= l_startMsisdn) )
            {
                i_startRangeMsisdnTextField.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "Number Ranges must not overlap");
                return false;
            }
        }        

        return true;
    }

    /**
     * Handles Generation events.
     */
    private void doGeneration()
    {
        URLConnection  l_urlConnection  = null;
        URL            l_fileListingUrl = null;
        InputStream    l_is             = null;
        BufferedReader l_bir            = null;
        String         l_line           = "";
        String         l_status         = "";
        String         l_fileListingUrlString = null;
        String l_numberRanges           = "";
        
        for (int i = 0; i < i_nextFreeRangesRow; i++)
        {
            l_numberRanges += "&numberRange" + i + "=" + i_data[i][0] + "-" + i_data[i][1];
        }

        l_fileListingUrlString = "http://" + i_serverHost + ":" + i_loginPort
                + "/ascs/boi/FileServices?command=GENERATEBALANCESDPSFILE" + "&userName="
                + ((BoiContext)i_context).getOperatorUsername() + "&password="
                + ((BoiContext)i_context).getOperatorPassword()
                + "&oldSdpIp=" + i_oldSdpIpAddrTextBox.getText()
                + "&newSdpIp=" + i_newSdpIpAddrTextBox.getText()
                + "&fileName=" + i_fileNameTextBox.getText() + l_numberRanges;

        try
        {
            l_fileListingUrl = new URL(l_fileListingUrlString);

            l_urlConnection = l_fileListingUrl.openConnection();
            l_urlConnection.connect();

            i_state = C_STATE_DATA_RETRIEVED;

            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));
            l_line = l_bir.readLine();

            if (l_line != null)
            {
                if (Debug.on)
                {
                    Debug.print(C_CLASS_NAME, 12010, "Read line " + l_line);
                }
            }
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
        catch (IOException l_e1)
        {
            handleException(l_e1);
        }
        finally
        {
//            i_fileNameTextBox.setText("");
//            i_oldSdpIdComboBox.setSelectedIndex(0);
//            i_newSdpIdComboBox.setSelectedIndex(0);
            initialiseScreen();
        }

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 12070, "Sdp Balancing Batch File Genration status: " + l_status);
        }
    }

    /* (non-Javadoc)
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    public void propertyChange(PropertyChangeEvent p_arg0)
    {
        // TODO Auto-generated method stub
        
    }
}