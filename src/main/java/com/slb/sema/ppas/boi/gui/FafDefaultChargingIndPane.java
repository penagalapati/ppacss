////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FafDefaultChargingIndPane.java
//      DATE            :       15-Nov-2005
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PpacLon#1838/7448
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to maintain FaF Default Charging 
//                              Indicators 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+--------------------
// 07/02/06  | M Erskine  | Move selectRowInTable to super- | PpacLon#1978/7906
//           |            | class.                          |
//-----------+------------+---------------------------------+--------------------
//28-Jun-2006| M T�rnqvist| String comparation of 123 and   | PpacLon#2108/9236
//           |            | 123000 found treated them as if |
//           |            | they were equals.               |
//-----------+------------+---------------------------------+--------------------
//  17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//           |            | display vertical scrollbar      |
//           |            | unnecessarily.                  |
//-----------+------------+---------------------------------+--------------------
// 06/12/06  | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//           |            | not selecting  NEW RECORD.      |
//-----------+------------+---------------------------------+--------------------
//22-Feb-2007| M T�rnqvist| Change size of Default Charging | PpacLon#2106/11023
//           |            | indicator. Now NUMBER(5)+       |
//           |            | VARCHAR(30) + 3 (" - ")         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiFafChargingIndicatorsData;
import com.slb.sema.ppas.boi.boidata.BoiFafDefaultChargingIndData;
import com.slb.sema.ppas.boi.dbservice.FafDefaultChargingIndDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 *Family and Friends Default Charging Indicator Screen class.
 */
/**
 * @author wmmatrq
 *
 */
/**
 * @author wmmatrq
 *
 */
public class FafDefaultChargingIndPane extends BusinessConfigBasePane 
{
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Panel to hold the fields and the buttons. */
    private JPanel               i_detailsPanel;

    /** Text field to hold the start msisdn. */
    private JFormattedTextField  i_startNoField;

    /** Text field to hold the end msisdn. */
    private JFormattedTextField  i_endNoField;

    /** Combo box to hold the configured charging indexes. */
    private JComboBox            i_chargIndComboBox;

    /** Combo box model for charging indicators. */
    private DefaultComboBoxModel i_chargIndModel;

    /** Panel to hold the defined charging indicators table. */
    private JPanel               i_definedBlocksPanel;

    /** Table to hold the charging indicators. */
    private JTable               i_table;

    /** An array of the configured charging indicators for the selected start number. */
    private String[][]           i_data;

    /** Table model to hold table data and column names. */
    private StringTableModel     i_stringTableModel;

    /** Existing deci_default_charging_ind table Charging Indicators vector. */
    private Vector               i_availableDeciDataV = null;

    /** Existing fach_faf_charging_ind table Charging Indicators vector. */
    private Vector               i_availableFachDataV = null;  
    
    /** The vertical scroll bar used for the bank records table view port. */
    private JScrollBar          i_verticalScrollBar   = null;

    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------     
    
    /**
     * FafChargingIndicatorsPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public FafDefaultChargingIndPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new FafDefaultChargingIndDbService((BoiContext)i_context);

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------     
    
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        
        if (l_selectedRowIndex != -1 && (i_availableDeciDataV.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }
    
    //  -------------------------------------------------------------------------
    // Protected methods 
    //-------------------------------------------------------------------------    

    /**
     * Paints the screen
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Family and Friends Default Charging Indicators",
                                                    100,
                                                    100,
                                                    0,
                                                    0);
        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_FAF_DEFAULT_CHARGING_IND_SCREEN), i_helpComboListener);

        createDetailsPanel();
        createDefinedBlocksPanel();

        i_mainPanel.add(i_helpComboBox, "fafchargingindicatorsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,30");
        i_mainPanel.add(i_definedBlocksPanel, "definedBlocksPanel,1,37,100,64");
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        BoiFafDefaultChargingIndData l_boiDeciChargIndData = null;
        DeciDefaultChargingIndData   l_deciChargIndData    = null;
        
        l_boiDeciChargIndData = ((FafDefaultChargingIndDbService)i_dbService).getDeciChargingIndData();
        l_deciChargIndData    = l_boiDeciChargIndData.getInternalDeciDefaultChargingIndData();

        i_startNoField.setText(l_deciChargIndData.getNumberStart());
        i_endNoField.setText(l_deciChargIndData.getNumberEnd());
        i_chargIndComboBox
                .setSelectedItem(new BoiFafChargingIndicatorsData
                                 (new FachFafChargingIndData(l_deciChargIndData.getChargingInd(),
                                                             l_deciChargIndData.getDescription(), 
                                                             false, 
                                                             null, 
                                                             null)));
        
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        String l_newStartNo;
        String l_newEndNo;
        String l_dbStartNo;
        String l_dbEndNo;
        
        l_newStartNo = i_startNoField.getText();
        l_newEndNo   = i_endNoField.getText();
  
        if (l_newStartNo.equals(""))
        {
            i_startNoField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Start Number cannot be blank");
            return false;
        }
  
        if ((l_newStartNo.length() != l_newEndNo.length()) && !l_newEndNo.equals(""))
        {
            i_endNoField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "The Start Number and End Number must be the same length");
            return false;
        }

        if ((l_newStartNo.compareTo(l_newEndNo) > 0) && !l_newEndNo.equals(""))
        {
            i_startNoField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "The Start Number must be smaller than the End Number");
            return false;
        }
  
        if (!l_newStartNo.equals("") && l_newEndNo.equals(""))
        {
            i_endNoField.setText(l_newStartNo);
            l_newEndNo = i_endNoField.getText();
        }
  
        if (i_chargIndComboBox.getSelectedItem().equals(""))
        {
            i_chargIndComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "The Charging Indicator field cannot be blank");
            return false;
        }
  
        // Check msisdn range not already in database
        // Starts from the third value as the 1st and 2nd are "" and "NEW RECORD" respectively
        for (int i = 2; i < i_availableDeciDataV.size(); i++)
        {
            l_dbStartNo = ((BoiFafDefaultChargingIndData)i_availableDeciDataV.get(i))
                             .getInternalDeciDefaultChargingIndData().getNumberStart();
            l_dbEndNo   = ((BoiFafDefaultChargingIndData)i_availableDeciDataV.get(i))
                             .getInternalDeciDefaultChargingIndData().getNumberEnd();
            
            if ( i_keyDataComboBox.getSelectedItem().equals("NEW RECORD") )
            {
                if ( isOverlap( l_dbStartNo, l_dbEndNo, l_newStartNo, l_newEndNo ))
                {
                    // New range overlaps an already defined range
                    displayMessageDialog(i_contentPane, "Msisdn ranges must not overlap");
                    return false;                    
                }
            }
            else
            {
                // Update existing record
                if (l_newStartNo.equals(l_dbStartNo))
                {
                    // Skip to next cycle of loop as this correponds to the selected row on the screen.
                    continue;
                }
                else if (l_newStartNo.equals(l_newEndNo))
                {
                    // No need to update if new end number equals old end number
                    if (l_newEndNo.equals(l_dbEndNo))
                    {
                        displayMessageDialog(i_contentPane, "Number range already exists");
                        return false;
                    }
                }
                else  if ( l_newStartNo.equals(l_dbStartNo) ||
                           l_newStartNo.equals(l_dbEndNo)   ||
                           l_newEndNo.equals(l_dbEndNo)     ||
                           l_newEndNo.equals(l_dbStartNo) )
                {
                    displayMessageDialog(i_contentPane, "Number ranges must not overlap");
                    return false;
                }
            }
                 
        } // End of for loop.
          
        return true;
  
    } // End validateScreenData method 


  
    protected boolean isOverlap(String p_oldStart,
                                String p_oldEnd,
                                String p_newStart,
                                String p_newEnd)
    {
        long    l_oldStart   = Long.parseLong(p_oldStart);
        long    l_oldEnd     = Long.parseLong(p_oldEnd);
        long    l_newStart   = Long.parseLong(p_newStart);
        long    l_newEnd     = Long.parseLong(p_newEnd);
        
        boolean l_returnValue = false;  // assume not overlap
        
        if ( (p_oldStart.length() == p_newStart.length()) &&
             (p_oldEnd.length()   == p_newEnd.length() ) )
        {
            if ( ( (l_newStart >= l_oldStart) && (l_newStart <= l_oldEnd) ) ||   //end inside
                 ( (l_newEnd   >= l_oldStart) && (l_newEnd   <= l_oldEnd) ) ||   //included
                 ( (l_newStart <= l_oldEnd)   && (l_newEnd   >= l_oldEnd) )   )  //start inside
            {
                // New range overlaps an already defined range
                l_returnValue = true; // it overlaps                   
            }
        }
        else if ( p_newStart.equals(p_oldStart) || p_newStart.equals(p_oldEnd) ||
                  p_newEnd.equals(p_oldEnd)     || p_newEnd.equals(p_oldStart) )
        {
            l_returnValue = true;
        }
           
        return l_returnValue;
    }

    

    /**
     * Validates key screen data and writes it to screen data object. This method is called before checking
     * for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        String                     l_chargingInd      = null;
        boolean                    l_key              = true;
        DeciDefaultChargingIndData l_deciChargIndData = null;
        
        Object l_fachObject = i_chargIndComboBox.getSelectedItem();

        if (i_startNoField.getText().trim().equals(""))
        {
            i_startNoField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Start Number cannot be blank");
            l_key = false;
        }

        if (l_fachObject instanceof BoiFafChargingIndicatorsData)
        {
            l_chargingInd = ((BoiFafChargingIndicatorsData)l_fachObject).
                                getInternalFachFafChargingIndData().getChargingInd();
        }
        else
        {
            l_chargingInd = (String)l_fachObject;
        }

        l_deciChargIndData = new DeciDefaultChargingIndData(i_startNoField.getText(),
                                                            i_endNoField.getText(),
                                                            l_chargingInd,
                                                            null, //TODO: There is no DECI description column
                                                            false,
                                                            null,
                                                            null);
        ((FafDefaultChargingIndDbService)i_dbService)
                .setDeciChargIndData(new BoiFafDefaultChargingIndData(l_deciChargIndData));
        
        return l_key;
    }

    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        DeciDefaultChargingIndData l_deciChargIndData = null;
        FachFafChargingIndData     l_fachChargIndData = null;
        
        l_fachChargIndData = ((BoiFafChargingIndicatorsData)i_chargIndComboBox
                                 .getSelectedItem()).getInternalFachFafChargingIndData();

        l_deciChargIndData = new DeciDefaultChargingIndData(i_startNoField.getText(),
                                                            i_endNoField.getText(),
                                                            l_fachChargIndData.getChargingInd(),
                                                            l_fachChargIndData.getDesc(),
                                                            false,
                                                            null,
                                                            null);
        
        ((FafDefaultChargingIndDbService)i_dbService)
                .setDeciChargIndData(new BoiFafDefaultChargingIndData(l_deciChargIndData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiFafDefaultChargingIndData l_selectedItem = null;

        l_selectedItem = (BoiFafDefaultChargingIndData)i_keyDataComboBox.getSelectedItem();
        ((FafDefaultChargingIndDbService)i_dbService).setDeciChargIndData(l_selectedItem);
    }

    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_startNoField.setText("");
        i_endNoField.setText("");
        i_chargIndComboBox.setSelectedItem("");
        i_chargIndComboBox.setEnabled(false);
        i_startNoField.setEnabled(false);
        i_endNoField.setEnabled(false);
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different start no. is selected.
     */
    protected void refreshListData()
    {
        i_availableDeciDataV = ((FafDefaultChargingIndDbService)i_dbService).getAvailableDeciDataV();
        populateCodesTable();
        i_keyDataModel = new DefaultComboBoxModel(i_availableDeciDataV);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }    

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {          
        i_availableFachDataV = ((FafDefaultChargingIndDbService)i_dbService).getAvailableFachDataV();
        i_chargIndModel = new DefaultComboBoxModel(i_availableFachDataV);
        i_chargIndComboBox.setModel(i_chargIndModel);
        
        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((FafDefaultChargingIndDbService)i_dbService)
                .getDeciChargingIndData());
    }
    
    /**
     * Alters screen behaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_startNoField.setEnabled(true);
        i_endNoField.setEnabled(true);
        i_chargIndComboBox.setEnabled(true);
    }

    /**
     * Alters screen behaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_startNoField.setEnabled(false);
        i_endNoField.setEnabled(true);
        i_chargIndComboBox.setEnabled(true);
    }
    
    //-------------------------------------------------------------------------
    // Private methods 
    //-------------------------------------------------------------------------    

    /**
     * Creation of a panel to display the defined Numbers, the Start Number, the End Number field and
     * the Charging Indicator field. 
     */
    private void createDetailsPanel()
    {
        JLabel l_defStartLabel;
        JLabel l_startLabel;
        JLabel l_endLabel;
        JLabel l_chargIndLabel;

        i_detailsPanel  = WidgetFactory.createPanel("Details", 100, 30, 0, 0);

        l_defStartLabel = WidgetFactory.createLabel("Defined Ranges:");
        l_startLabel    = WidgetFactory.createLabel("Start Number:");
        l_endLabel      = WidgetFactory.createLabel("End Number:");
        l_chargIndLabel = WidgetFactory.createLabel("Charging Indicator:");

        i_startNoField  = WidgetFactory.createMsisdnField(15);
        i_endNoField    = WidgetFactory.createMsisdnField(15);

        i_chargIndModel = new DefaultComboBoxModel();
        i_chargIndComboBox = WidgetFactory.createComboBox(i_chargIndModel);

        // Add listeners to editable fields such that state is changed from
        // Initial to New Record whenever a modification is made.
        addValueChangedListener(i_startNoField);
        addValueChangedListener(i_endNoField);
        addValueChangedListener(i_chargIndComboBox);

        i_detailsPanel.add(l_defStartLabel, "l_defStartLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,38,4");
        i_detailsPanel.add(l_startLabel, "startLabel,1,6,15,4");
        i_detailsPanel.add(i_startNoField, "startNumber,17,6,15,4");
        i_detailsPanel.add(l_endLabel, "endLabel,1,11,15,4");
        i_detailsPanel.add(i_endNoField, "endNumber,17,11,15,4");
        i_detailsPanel.add(l_chargIndLabel, "l_chargIndLabel,1,16,15,4");
        i_detailsPanel.add(i_chargIndComboBox, "i_chargIndComboBox,17,16,38,4");

        // Add buttons to screen that were created in superclass. 
        i_detailsPanel.add(i_updateButton, "updateButton,1,25,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,25,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,25,15,5");
    }
    
    /**
     * Method used to perform the basic setup of the screen.
     */
    private void createDefinedBlocksPanel()
    {
        JScrollPane l_scrollPane  = null;
        String[]    l_columnNames = {"Start Number", "End Number", "Charging Indicator"};

        i_definedBlocksPanel = WidgetFactory.createPanel("Defined Ranges", 100, 64, 0, 0);

        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table = WidgetFactory.createTable(150, 350, i_stringTableModel, this);
        
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedBlocksPanel.add(l_scrollPane, "i_table,1,1,70,64");
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    } 
    
    /**
     * Populates the Codes Table.
     */
    private void populateCodesTable()
    {
        DeciDefaultChargingIndData l_deciChargIndData = null;
        int l_numRecords = 0;
        int l_arraySize  = 0;

        l_numRecords = i_availableDeciDataV.size() -2;
        l_arraySize  = ((l_numRecords > 23) ? l_numRecords : 24);
        i_data       = new String[l_arraySize][3];

        for (int i = 2, j = 0; i < i_availableDeciDataV.size(); i++, j++)
        {
            l_deciChargIndData = ((BoiFafDefaultChargingIndData)i_availableDeciDataV.elementAt(i))
                    .getInternalDeciDefaultChargingIndData();
            i_data[j][0] = l_deciChargIndData.getNumberStart();
            i_data[j][1] = l_deciChargIndData.getNumberEnd();
            i_data[j][2] = l_deciChargIndData.getChargingInd();
        }

        i_stringTableModel.setData(i_data);
    }

}
