////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiTrafficCaseData.java
//      DATE            :       10-Mar-2006
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#2035/8106
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Wrapper for TrafTrafficCaseData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseData;
import com.slb.sema.ppas.common.dataclass.DataObject;

/** Wrapper for traffic case class within BOI. */
public class BoiTrafficCaseData extends DataObject
{
    /** Basic traffic case data object. */
    private TrafTrafficCaseData i_trafData;
    
    /**
     * Simple constructor.
     * @param p_trafData traffic case data object to wrapper.
     */
    public BoiTrafficCaseData(TrafTrafficCaseData p_trafData)
    {
        i_trafData = p_trafData;
    }

    /**
     * Return wrappered traffic case data object.
     * @return Wrappered traffic case data object.
     */
    public TrafTrafficCaseData getInternalTrafficCaseData()
    {
        return i_trafData;
    }

    /**
     * Compares two traffic case data objects and returns true if they equate.
     * @param p_trafData traffic case data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_trafData)
    {
        boolean l_return = false;
        
        if ( p_trafData != null &&
             p_trafData instanceof BoiTrafficCaseData &&
             i_trafData.getTrafficCase() ==
                 ((BoiTrafficCaseData)p_trafData).getInternalTrafficCaseData().getTrafficCase())
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns traffic case data object as a String for display in BOI.
     * @return traffic case data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_trafData.getTrafficCase() + 
                                            " - " + i_trafData.getTrafficShortDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_trafData.getTrafficCase();
    }
}