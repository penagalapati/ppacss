////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       AdjustmentCodesPaneUT.java
//    DATE            :       30 November 2005
//    AUTHOR          :       Richard Grimshaw
//    REFERENCE       :       PpacLon#1847
//
//    COPYRIGHT       :       Atos Origin 2005
//
//    DESCRIPTION     :       JUnit test class for AdjustmentCodesPane.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/02/06 | M Erskine  | Use adjustment type that exists | PpacLon#1912/7971
//          |            | in the test data.               |
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for AccountGroupsPane. */
public class AdjustmentCodesPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String          C_CLASS_NAME         = "AdjustmentCodesPaneUT";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Adjustment codes screen. */
    private AdjustmentCodesPane          i_adjCodesPane;

    /** Adjustment codes code field. */
    private ValidatedJTextField          i_codeField;

    /** Adjustment codes description field. */
    private ValidatedJTextField          i_descriptionField;

    /** market combo box. */
    private JComboBox                    i_marketDataComboBox;

    /** service class combo box. */
    private JComboBox                    i_adjTypeDataComboBox;

    /** Constant defining a single dedicated account id. */
    private static final String          C_ADJ_CODE          = "TEST-ADJ";

    /** Constant defining a single dedicated account description. */
    private static final String          C_ADJ_DESC          = "Test Adjustment";

    /** Constant defining a Market for testing. */
    private static final BoiMarket       C_ADJ_MARKET        = new BoiMarket(new Market(1, 2));

    /** Constant defining a Adjustment type for testing. */
    private static final BoiMiscCodeData C_ADJ_ADJUSTMENT_TYPE = new BoiMiscCodeData(new MiscCodeData(
                                                                  C_ADJ_MARKET.getMarket(),
                                                                  "ADT",
                                                                  "XXX",
                                                                  "Administration(1,2) addition",
                                                                  " "));

    /** Key data combo box. */
    protected JComboBox                  i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton                    i_updateButton;

    /** Delete button. */
    protected JButton                    i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AdjustmentCodesPaneUT(String p_title)
    {
        super(p_title);

        i_adjCodesPane = new AdjustmentCodesPane(c_context);
        super.init(i_adjCodesPane);

        i_adjCodesPane.resetScreenUponEntry();

        i_marketDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "marketDataComboBox");
        i_adjTypeDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(
            i_contentPane,
            "adjustmentTypeDataComboBox");
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(
            i_contentPane,
            "descriptionField");

        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AdjustmentCodesPaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again through
     * the Dedicated Accounts screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AdjustmentCodesPane test");

        BoiAdjCodesData l_boiDedaData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        SwingTestCaseTT.setMarketComboSelectedItem(i_adjCodesPane, i_marketDataComboBox, C_ADJ_MARKET);

        SwingTestCaseTT.setMarketComboSelectedItem(
            i_adjCodesPane,
            i_adjTypeDataComboBox,
            C_ADJ_ADJUSTMENT_TYPE);

        SwingTestCaseTT.setKeyComboSelectedItem(i_adjCodesPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_ADJ_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_ADJ_DESC);
        doInsert(i_adjCodesPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiDedaData = createAdjustmentCodesData(
            C_ADJ_MARKET,
            C_ADJ_ADJUSTMENT_TYPE,
            C_ADJ_CODE,
            C_ADJ_DESC);

        SwingTestCaseTT.setKeyComboSelectedItem(i_adjCodesPane, i_keyDataCombo, l_boiDedaData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_adjCodesPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_adjCodesPane, i_deleteButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteAdjCodesRecord(C_ADJ_MARKET, C_ADJ_ADJUSTMENT_TYPE, C_ADJ_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteAdjCodesRecord(C_ADJ_MARKET, C_ADJ_ADJUSTMENT_TYPE, C_ADJ_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /**
     * Creates a BoiAdjCodesData object with the supplied data.
     * @param p_market Dedicated Accounts market.
     * @param p_adjustmentType adjutsment type.
     * @param p_adjCode adjustment code.
     * @param p_adjDesc adjsutment description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiAdjCodesData createAdjustmentCodesData(BoiMarket p_market,
                                                      BoiMiscCodeData p_adjustmentType,
                                                      String p_adjCode,
                                                      String p_adjDesc)
    {
        SrvaAdjCodesData l_adjCodesData;
       
        l_adjCodesData = new SrvaAdjCodesData(
            null, // p_request
            null,
            p_market.getMarket(),
            p_adjustmentType.getInternalMiscCodeData().getCode(),
            p_adjCode,
            p_adjDesc,
            null,
            null,
            ' ');

        return new BoiAdjCodesData(l_adjCodesData);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteAdjCodesRecord = "deleteAdjCodesRecord";

    /**
     * Removes a row from deda_dedicated_accounts.
     * @param p_adjType Adjustment type.
     * @param p_market Market
     * @param p_adjCode Adjustment code 
     */
    private void deleteAdjCodesRecord(BoiMarket p_market,
                                  BoiMiscCodeData p_adjType,
                                  String p_adjCode)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = "DELETE from srva_adj_codes " + "WHERE srva = {0} "
                + "AND sloc = {1} " + "AND adj_type = {2} "
                + "AND adj_code = {3} ";
        
        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setIntParam(0, p_market.getMarket().getSrva());
        l_sqlString.setIntParam(1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam(2, p_adjType.getInternalMiscCodeData().getCode());
        l_sqlString.setStringParam(3, p_adjCode);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteAdjCodesRecord);
    }
}