////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TubsChannelPane.java
//      DATE            :       22-Dec-2006
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PpacLon#2822/10659
//                              PRD_ASCS00_GEN_CA104
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Wrapper for ChavChannelData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/01/07 |M.Tornqvist | Added methods that makes it     | PpacLon#2874/10868
//          |            | possible to filter the choices  |
//          |            | in  the the group combibox, so  |
//          |            | that the hierachy only can be 2 |
//          |            | level                           |
//----------+------------+---------------------------------+--------------------
// 08/02/07 |M.Tornqvist | equals() changed, objects are ==| PpacLon#2922/10937
//          |            | when the channelIds are equals  |
//          |            | because channelId is primaryKey |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;

public class BoiChavChannelData  extends DataObject
{
    /** Basic channel data object. */
    private ChavChannelData i_channelData;
    
    /** Assume it is a master channel. */
    private boolean         i_isMaster    = true;
    
    /** Assume no children. */
    private boolean         i_hasChildren = false;
    
    /**
     * Simple constructor.
     * @param p_channelData Channel data object to wrapper.
     */
    public BoiChavChannelData(ChavChannelData p_channelData)
    {
        i_channelData = p_channelData;
    }

    /**
     * Return wrappered channel data object.
     * @return Wrappered channel data object.
     */
    public ChavChannelData getInternalChannelData()
    {
        return i_channelData;
    }

    /**
     * Compares two channel data objects and returns true if they equate.
     * @param p_channelData Channel data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_channelData)
    {
        boolean         l_return          = false;
        ChavChannelData l_chavChannelData = null;
        
        if ( p_channelData != null &&
             p_channelData instanceof BoiChavChannelData )
        {
        	l_chavChannelData = ((BoiChavChannelData)p_channelData).getInternalChannelData();
        
            if ( i_channelData.getChannelId().equals(l_chavChannelData.getChannelId()) )               
            {
                l_return = true;
            }
        }
        return l_return;
    }

    /**
     * Returns TUBS channel id for display in BOI combo box.
     * @return TUBS channel data object as a string.
     */
    public String toString()
    {
    	return i_channelData.getChannelId() + " - " + i_channelData.getDesc();
    }
    
    /**
     * Returns true if this channel isn't grouped to another channel.
     * @return true if it is a master, false if it belongs to another channel.
     */
    public boolean isMaster()
    {
    	return i_isMaster;
    }
    
    /**
     * Returns true if this channel has other channels refering to it.
     * @return true if it has other channels connected to it, otherwise returns false.
     */
    public boolean hasChildren()
    {
    	return i_hasChildren;
    }
    
    /**
     * Sets a flag to indicate that this channel has other channels that belongs to it.
     */
    public void flagChildren()
    {
    	i_hasChildren = true;
    }
}
