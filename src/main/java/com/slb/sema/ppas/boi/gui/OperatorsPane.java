////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       OperatorsPane.java
//      DATE            :       13-Apr-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#227/2087
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Operator Configuration screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME       | DESCRIPTION                         | REFERENCE
//----------+------------+---------------------------------+--------------------
// 29/08/06 | A Kutthan  | Added New Password validation   | PpacLon#2595/9828
//          |            | Strong and Weak password config |
//-----------+------------+-------------------------------------+---------------
// 13-sep-06 | huy        | Op name input field increased to 30 | PpacLon#2239/9960
//           |            | char to match published specs.      | 
//-----------+------------+-------------------------------------+---------------
// 12-dec-06 | huy        | Changed password confirm field to   | PpacLon#2514/10639
//           |            | match password field length of 20   |
//           |            | characters.                         |
//-----------+------------+-------------------------------------+---------------
// 20-dec-06 | Chris      | Added Pre-expired checkbox          | PpacLon#2002/10678
//           | Harrison   |                                     |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiPrivilegeLevelData;
import com.slb.sema.ppas.boi.dbservice.OperatorsDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.dataclass.OperatorData;
import com.slb.sema.ppas.common.exceptions.PpasSecurityException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.ValidatedJPasswordField;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Operator Configuration Screen. 
 */
public class OperatorsPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** The operator id panel */    
    private JPanel i_operatorPanel;

    /** The operator details panel */    
    private JPanel i_detailsPanel;
    
    /** The operator markets panel */    
    private JPanel  i_marketsPanel;
        
    /** Vector of operators */
    private Vector i_operators;
    
    /** Selected markets on the screen for the current operator. */
    private Vector i_screenSelectedMarkets;
    
    /** Vector of available markets. */
    private Vector i_availableMarkets;
    
    /** Table of markets that are available for selection */
    private JScrollPane i_availableMarketsScrollPane;
    
    /** Table of markets that have need selected for this operator */
    private JScrollPane i_selectedMarketsScrollPane;
    
    /** JList of available markets */
    private JList i_availableMarketsList;

    /** JList of selected markets for the operator */
    private JList i_selectedMarketsList;

    /** Text field to hold the operator id. */
    private ValidatedJTextField i_operatorId;
    
    /** Text field to hold the operator name. */
    private ValidatedJTextField i_operatorName;
    
    /** Text field to hold the new password. */
    private ValidatedJPasswordField i_newPassword;
    
    /** Text field to hold the confirmed password. */
    private ValidatedJPasswordField i_confirmPassword;
    
    /** Text field to hold the password expiry date. */
    private JFormattedTextField i_passwordExpiryDate;

    /** Check box to indicate whether the password should be pre-expired. */
    private JCheckBox i_passwordPreExpired;
    
    /** Box to hold the configured markets. */
    private JComboBox i_defaultMarketBox;
    
    /** Combo box model for default market box. */
    private DefaultComboBoxModel i_defaultMarketModel;
    
    /** Vector of available privileges levels. */
    private Vector i_availablePrivileges;
    
    /** Vector of privileges levels for display in Combo box. Contains available privileges levels plus 
     *  any currently selected privileges level which is no longer available. */
    private Vector i_privilegesForCombo;
    
    /** Box to hold the configured privilege ids. */
    private JComboBox i_privilegeBox;
    
    /** Combo box model for privilege ids. */
    private DefaultComboBoxModel i_privilegesModel;
    
    /** Text field to hold the maximum single adjustment amount. */
    private JFormattedTextField i_maxAdjustAmount;
    
    /** Text field to hold the daily adjustment limit. */
    private JFormattedTextField i_dailyAdjustLimit;

    /** Check box to hold GUI access privilege */
    private JCheckBox i_guiPrivilege;

    /** Check box to hold POSI access privilege */
    private JCheckBox i_posiPrivilege;

    /** Check box to hold PAMI access privilege */
    private JCheckBox i_pamiPrivilege;
    
    /** Button to add market to operator's selected list */
    private GuiButton i_addMarketButton;
    
    /** Button to remove market from operator's selected list */
    private GuiButton i_removeMarketButton;

    /** System base currency precision. */
    private int i_precision = 0;

    /** Check box to hold flag indicating whether user should be unbarred. */
    private JCheckBox i_unbarOperator;
    
    /** Number of failed login attempts since last successful login. */
    private JFormattedTextField i_failedLoginCounter;

    /** Number of failed login attempts since last successful login or unbarring. */
    private JFormattedTextField i_failedLoginBarCounter;

    /** Date/time of last successful login. */
    private JFormattedTextField i_lastLoginDateTime;

    /** Date/time until which time this user is barred. */
    private JFormattedTextField i_barToDateTime;

    /** Maximum number of characters permitted for password. */
    private static final int C_PASSWORD_MAX_STRONG_LENGTH = 20;

    /** Checkbox listener */
    private static CheckBoxListener i_checkBoxListener = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * OperatorsPane constructor. 
     * @param p_context A reference to the BoiContext
     */
    public OperatorsPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new OperatorsDbService((BoiContext)i_context);

        // Need to retrieve base currency precision before painting the screen
        // as the precision is an input to WidgetFactory.createAmountField.
        // NOTE: The called method does not currently manage the query correctly. 
        i_precision = ((BoiContext)i_context).getBaseCurrencyPrecision();

        i_checkBoxListener = new CheckBoxListener();
        
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles button events.
     * @param p_actionEvent Action event.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        Object l_source = null;

        l_source = p_actionEvent.getSource();
        
        if (l_source == i_addMarketButton)
        {
            addSelectedMarkets();
        }
        else if (l_source == i_removeMarketButton)
        {
            removeSelectedMarkets();
        }
    }

    
    /**
     * Handles keyboard events for the screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean  l_return = false;
        PpasDate l_date = null;
        
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_deleteButton)
            {
                i_deleteButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_addMarketButton)
            {
                i_addMarketButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_removeMarketButton)
            {
                i_removeMarketButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_guiPrivilege ||
                     p_keyEvent.getComponent() == i_posiPrivilege ||
                     p_keyEvent.getComponent() == i_pamiPrivilege ||
                     p_keyEvent.getComponent() == i_unbarOperator ||
                     p_keyEvent.getComponent() == i_passwordPreExpired)
            {
                JCheckBox l_checkBox = (JCheckBox)p_keyEvent.getComponent();
                l_checkBox.setSelected(l_checkBox.isSelected() ? false : true);

                if (i_passwordPreExpired.isSelected() == true)
                {
                    l_date = DatePatch.getDateToday();
                    l_date.add(PpasDate.C_FIELD_DAY, -1);
                    i_passwordExpiryDate.setValue(l_date);
                }
                else
                {
                    i_passwordExpiryDate.setValue("");
                }
            }
            else
            {
                i_updateButton.doClick();
            }
            l_return = true;
        }

        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
           (p_keyEvent.getID() == KeyEvent.KEY_PRESSED))
        {
            if (p_keyEvent.getComponent() == i_passwordExpiryDate)
            {
                // If the password expiry date is in the past then set the checkbox
                l_date = DatePatch.getDateToday();
                
                if ((!i_passwordExpiryDate.getText().equals("")) &&
                    (l_date.compareTo(new PpasDate(i_passwordExpiryDate.getText())) > 0))
                {
                    i_passwordPreExpired.setSelected(true);
                }
                else
                {
                    i_passwordPreExpired.setSelected(false);
                }
            }

            else if (p_keyEvent.getComponent() == i_passwordPreExpired)
            {
                if (i_passwordPreExpired.isSelected() == true)
                {
                    l_date = DatePatch.getDateToday();
                    l_date.add(PpasDate.C_FIELD_DAY, -1);
                    i_passwordExpiryDate.setValue(l_date);
                }
            }
        }
        
        return l_return;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Operator Configuration", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                       BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_OPERATORS_SCREEN), 
                                       i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        i_mainPanel.add(i_helpComboBox, "operatorsHelpComboBox,60,1,40,4");
                              
        i_operatorPanel = createOperatorPanel();
        i_mainPanel.add(i_operatorPanel, "operatorPanel,1,6,100,14");

        i_detailsPanel = createDetailsPanel();
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,21,100,37");
        
        i_marketsPanel = createMarketsPanel();
        i_mainPanel.add(i_marketsPanel, "marketsPanel,1,59,100,36");

        //Add buttons to screen that were created in superclass
        i_mainPanel.add(i_updateButton, "updateButton,1,96,15,5");
        
        i_mainPanel.add(i_deleteButton, "deleteButton,17,96,15,5");
        
        i_mainPanel.add(i_resetButton, "resetButton,33,96,15,5");
    }
    
    /**
     * Private method to set the password fields to be empty.
     */
    protected void resetPasswordFields()
    {
        i_newPassword.setText("");
        i_newPassword.requestFocusInWindow();
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        Double   l_dailyAdjustmentLimit = null;
        Double   l_maxAdjustmentAmount = null;
        String   l_password = null;
        String   l_confirmPassword = null;
        boolean  l_strongPasswordConfigured = false;
        
        if (i_operatorId.getText().trim().equals(""))
        {
            i_operatorId.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Operator Id cannot be blank");
            return false;
        }
        
        if (i_operatorName.getText().trim().equals(""))
        {
            i_operatorName.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Operator Name cannot be blank");
            return false;
        }
        
        l_password = new String(i_newPassword.getPassword());
        if (l_password.trim().equals("") &&
            i_keyDataComboBox.getSelectedItem().toString().equals("NEW RECORD"))
        {
            i_newPassword.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "New Password field cannot be empty for a new Operator");
            return false;
        }

        l_confirmPassword = new String(i_confirmPassword.getPassword());
        if (!l_confirmPassword.trim().equals(l_password.trim()))
        {
            i_confirmPassword.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Passwords don't match");
            return false;
        }
        
        if (l_password.trim().equals(""))
        {            
            try
            {
                ((OperatorsDbService)i_dbService).encryptPassword(l_password);
            }
            catch (PpasSecurityException l_ppasSecurityEx)
            {
                handleException(l_ppasSecurityEx);
                return false;
            }
        }
        else
        {       
            l_strongPasswordConfigured = ((OperatorsDbService)i_dbService).isStrongPasswordConfigured();

            if (l_strongPasswordConfigured)
            {
                if (validateStrongPassword(l_password))
                {
                    try
                    {   
                        ((OperatorsDbService)i_dbService).encryptPassword(l_password);
                    }
                    catch (PpasSecurityException l_ppasSecurityEx)
                    {
                        handleException(l_ppasSecurityEx);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    ((OperatorsDbService)i_dbService).encryptPassword(l_password);
                }
                catch (PpasSecurityException l_ppasSecurityEx)
                {
                    handleException(l_ppasSecurityEx);
                    return false;
                }
            }
        }
        
        if (i_passwordExpiryDate.getText().equals(""))
        {
            i_passwordExpiryDate.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Password Expiry Date cannot be blank");
            return false;
        }
                
        if (i_dailyAdjustLimit.getText().trim().equals(""))
        {
            i_dailyAdjustLimit.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Daily Adjustment Limit cannot be blank");
            return false;
        }
        
        if (i_maxAdjustAmount.getText().trim().equals(""))
        {
            i_maxAdjustAmount.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Maximum Adjustment Amount cannot be blank");
            return false;
        }
        
        l_dailyAdjustmentLimit = new Double(i_dailyAdjustLimit.getText());
        l_maxAdjustmentAmount = new Double(i_maxAdjustAmount.getText());
        
        // Max adjust amount must not be greater than daily adjustment limit.
        if (l_maxAdjustmentAmount.compareTo(l_dailyAdjustmentLimit) > 0)
        {
            i_dailyAdjustLimit.requestFocusInWindow();
            i_dailyAdjustLimit.selectAll();
            displayMessageDialog(i_contentPane,
                                 "Maximum Adjustment Amount must not be greater than " +
                                 "Daily Adjustment Limit");
            return false;
        }

        if (i_screenSelectedMarkets.size() == 0)
        {
            i_selectedMarketsScrollPane.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "At least one market must be selected");
            return false;
        }

        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        if (i_operatorId.getText().trim().equals(""))
        {
            i_operatorId.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Operator Id cannot be blank");
            return false;
        }
        
        ((OperatorsDbService)i_dbService).setCurrentOpid(i_operatorId.getText());
        
        return true;
    }

    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        OperatorData           l_operatorData = null;
        BoiMarket              l_defaultMarket = null;
        String                 l_password = null;
        PrilPrivilegeLevelData l_selectedPrivilege;
        
        l_password = new String(i_newPassword.getPassword());

        l_defaultMarket = (BoiMarket)i_defaultMarketBox.getSelectedItem();
        l_selectedPrivilege = ((BoiPrivilegeLevelData)i_privilegeBox.getSelectedItem()).
                                                               getInternalPrivilegeLevelData();

        l_operatorData = new OperatorData(
                                 null,
                                 i_operatorId.getText(),
                                 i_operatorName.getText(),
                                 l_defaultMarket.getMarket(),
                                 l_selectedPrivilege.getPrivilegeLevel(),
                                 new Double(i_maxAdjustAmount.getText()),
                                 new Double(i_dailyAdjustLimit.getText()),
                                 null,
                                 l_password,
                                 new PpasDate(i_passwordExpiryDate.getText()),
                                 i_guiPrivilege.isSelected(),
                                 i_posiPrivilege.isSelected(),
                                 i_pamiPrivilege.isSelected(),
                                 0, null, 0, null);  // Read only fields
                                        
        ((OperatorsDbService)i_dbService).setOperatorData(l_operatorData);
        ((OperatorsDbService)i_dbService).setUnbarOperator(i_unbarOperator.isSelected());
        
        ((OperatorsDbService)i_dbService).setScreenSelectedMarkets(i_screenSelectedMarkets);
    }
    
    /**
     * Writes key screen data to screen data object for use in record selection and deletion.
     */  
    protected void writeKeyData()
    {
        ((OperatorsDbService)i_dbService).setCurrentOpid(i_keyDataComboBox.getSelectedItem().toString());
    }

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_operatorId.setText("");
        i_operatorName.setText("");
        i_newPassword.setText("");
        i_confirmPassword.setText("");
        i_passwordExpiryDate.setValue(null);
        if (i_privilegeBox.getItemCount() != 0)
        {
            i_privilegeBox.setSelectedIndex(0);
        }
        i_passwordPreExpired.setSelected(false);
        i_maxAdjustAmount.setValue(new Double(0.00));
        i_dailyAdjustLimit.setValue(new Double(0.00));
        i_guiPrivilege.setSelected(false);
        i_posiPrivilege.setSelected(false);
        i_pamiPrivilege.setSelected(false);

        i_screenSelectedMarkets.removeAllElements();
        i_selectedMarketsList.setListData(i_screenSelectedMarkets);

        i_defaultMarketModel = new DefaultComboBoxModel(i_screenSelectedMarkets);
        i_defaultMarketBox.setModel(i_defaultMarketModel);
        
        i_unbarOperator.setVisible(false);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        OperatorData           l_operatorData;
        PrilPrivilegeLevelData l_privilegeData;
        BoiMarket              l_defaultMarket;
        PpasDate               l_date = null;
        
        l_operatorData = ((OperatorsDbService)i_dbService).getOperatorData();
        i_operatorId.setText(l_operatorData.getOpid());
        i_operatorName.setText(l_operatorData.getOperatorName());
        i_newPassword.setText("");
        i_confirmPassword.setText("");
        i_passwordExpiryDate.setValue(l_operatorData.getPasswordExpiryDate());
        
        // If the password expiry date is in the past then set the checkbox
        l_date = DatePatch.getDateToday();
        
        if ((!i_passwordExpiryDate.getText().equals("")) &&
            (l_date.compareTo(new PpasDate(i_passwordExpiryDate.getText())) > 0))
        {
            i_passwordPreExpired.setSelected(true);
        }
        else
        {
            i_passwordPreExpired.setSelected(false);
        }

        l_privilegeData = ((OperatorsDbService)i_dbService).getSelectedPrivilege();
        refreshPrivilegeCombo(l_privilegeData);

        i_maxAdjustAmount.setValue(l_operatorData.getMaxAdjustAmountForBoi());
        i_dailyAdjustLimit.setValue(l_operatorData.getDailyAdjustLimitForBoi());
        i_guiPrivilege.setSelected(l_operatorData.hasGuiPrivilege());
        i_posiPrivilege.setSelected(l_operatorData.hasPosiPrivilege());
        i_pamiPrivilege.setSelected(l_operatorData.hasPamiPrivilege());

        i_screenSelectedMarkets = new Vector(((OperatorsDbService)i_dbService).getDbSelectedMarkets());
        i_selectedMarketsList.setListData(i_screenSelectedMarkets);

        i_defaultMarketModel = new DefaultComboBoxModel(i_screenSelectedMarkets);
        i_defaultMarketBox.setModel(i_defaultMarketModel);
        l_defaultMarket = ((OperatorsDbService)i_dbService).getDefaultMarket();
        i_defaultMarketBox.setSelectedItem(l_defaultMarket);

        i_unbarOperator.setSelected(false);
        i_unbarOperator.setVisible(l_operatorData.isBarred());
        i_failedLoginBarCounter.setText("" + l_operatorData.getBarCounter());
        i_failedLoginBarCounter.setEditable(false);
        i_failedLoginCounter.setText("" + l_operatorData.getLoginFailures());
        i_failedLoginCounter.setEditable(false);
        i_lastLoginDateTime.setText("" + l_operatorData.getLastLogin());
        i_lastLoginDateTime.setEditable(false);
        i_barToDateTime.setText("" + l_operatorData.getBarToDateTime());
        i_barToDateTime.setEditable(false);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.
     */
    protected void refreshListData()
    {
        PrilPrivilegeLevelData l_privilegeData;
        String                 l_password;

        i_operators = new Vector(((OperatorsDbService)i_dbService).getOperators());
        i_operators.add(0, "");
        i_operators.add(1, "NEW RECORD");
        i_keyDataModel = new DefaultComboBoxModel(i_operators);
        i_keyDataComboBox.setModel(i_keyDataModel);

        l_privilegeData = ((BoiPrivilegeLevelData)i_privilegeBox.getSelectedItem()).
                                                                           getInternalPrivilegeLevelData();
        refreshPrivilegeCombo(l_privilegeData);
        
        // If the operator has updated their own password, we need to store the new value on the context.
        if (i_operatorId.getText().equals(((BoiContext)i_context).getOperatorUsername()))
        {
            l_password = new String(i_newPassword.getPassword());
            if (!l_password.trim().equals(""))
            {
                ((BoiContext)i_context).setOperatorPassword(l_password);
            }
        }
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_availableMarkets = new Vector(((OperatorsDbService)i_dbService).getAvailableMarkets());
        i_availableMarketsList.setListData(i_availableMarkets);
        
        i_availablePrivileges = ((OperatorsDbService)i_dbService).getAvailablePrivileges();
        i_privilegesModel = new DefaultComboBoxModel(i_availablePrivileges);
        i_privilegeBox.setModel(i_privilegesModel);

        i_precision = ((OperatorsDbService)i_dbService).getBaseCurrencyPrecision();

        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(i_operatorId.getText());
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        PpasDate l_date = null;

        i_operatorId.setEnabled(true);

        // Ensure that any withdrawn records are removed from dropdowns.  They will have been added if
        // an operators record was still assigned a withdrawn value.
        i_availablePrivileges = ((OperatorsDbService)i_dbService).getAvailablePrivileges();
        i_privilegesModel = new DefaultComboBoxModel(i_availablePrivileges);
        i_privilegeBox.setModel(i_privilegesModel);

        // Automatically set the password expiry date
        l_date = DatePatch.getDateToday();
        l_date.add(PpasDate.C_FIELD_DAY, -1);
        i_passwordExpiryDate.setText(l_date.toString());
        i_passwordPreExpired.setSelected(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_operatorId.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Creates the operator id panel.
     * @return Operator ID panel.
     */
    private JPanel createOperatorPanel()
    {
        JPanel       l_operatorPanel;
        
        l_operatorPanel = WidgetFactory.createPanel(100, 14, 0, 0);

        l_operatorPanel.add(WidgetFactory.createLabel("Defined Operators: "), 
                           "definedOperatorsLabel,1,1,20,5");

        l_operatorPanel.add(i_keyDataComboBox, "operatorBox,21,1,27,5");
       
        l_operatorPanel.add(WidgetFactory.createLabel("Operator Id: "),
                           "operatorIdLabel,1,7,20,5");
        i_operatorId = WidgetFactory.createTextField(8);
        addValueChangedListener(i_operatorId);
        i_operatorId.setEnabled(false);
        l_operatorPanel.add(i_operatorId, "operatorId,21,7,27,5");

        l_operatorPanel.add(WidgetFactory.createLabel("Name: "),
                           "operatorNameLabel,41,7,20,5");
        i_operatorName = WidgetFactory.createTextField(30);
        addValueChangedListener(i_operatorName);
        l_operatorPanel.add(i_operatorName, "operatorName,61,7,37,5");

        return l_operatorPanel;
    }
    
    /**
     * Creates the operator details panel.
     * @return Details panel.
     */
    private JPanel createDetailsPanel()
    {
        JPanel  l_detailsPanel;

        l_detailsPanel = WidgetFactory.createPanel("Details", 100, 37, 0, 0);

        l_detailsPanel.add(WidgetFactory.createLabel("New password: "),
                           "newPasswordLabel,1,1,20,4");
        i_newPassword = WidgetFactory.createPasswordField(C_PASSWORD_MAX_STRONG_LENGTH);
        addValueChangedListener(i_newPassword);
        l_detailsPanel.add(i_newPassword, "newPassword,21,1,27,4");
        
        l_detailsPanel.add(WidgetFactory.createLabel("Password Expiry Date: "),
                           "passwordExpiryDateLabel,51,1,20,4");
        i_passwordExpiryDate = WidgetFactory.createDateField(this);
        addValueChangedListener(i_passwordExpiryDate);
        l_detailsPanel.add(i_passwordExpiryDate, "passwordExpiryDate,71,1,27,4");


        l_detailsPanel.add(WidgetFactory.createLabel("Confirm password: "),
                           "newPasswordLabel,1,6,20,4");
        i_confirmPassword = WidgetFactory.createPasswordField(C_PASSWORD_MAX_STRONG_LENGTH);
        addValueChangedListener(i_confirmPassword);
        l_detailsPanel.add(i_confirmPassword, "confirmPassword,21,6,27,4");

        i_passwordPreExpired = WidgetFactory.createCheckBox("Pre-expired", i_checkBoxListener);
        addValueChangedListener(i_passwordPreExpired);
        l_detailsPanel.add(i_passwordPreExpired,  "passwordPreExpired,71,6,27,5");
        
        
        l_detailsPanel.add(WidgetFactory.createLabel("Privilege Id: "), 
                           "privilegeIdLabel,1,11,20,4");
        i_privilegesModel = new DefaultComboBoxModel();
        i_privilegeBox = WidgetFactory.createComboBox(i_privilegesModel);
        addValueChangedListener(i_privilegeBox);
        l_detailsPanel.add(i_privilegeBox, "privilegeBox,21,11,40,4");
       
        l_detailsPanel.add(WidgetFactory.createLabel("Max Adjustment Amt: "),
                           "maxAdjustLabel,1,16,20,4");
        i_maxAdjustAmount = WidgetFactory.createAmountField(12, i_precision, false);
        addValueChangedListener(i_maxAdjustAmount);
        l_detailsPanel.add(i_maxAdjustAmount, "maxAdjustAmount,21,16,27,4");

        l_detailsPanel.add(WidgetFactory.createLabel("Daily Adjustment Limit: "),
                           "dailyAdjustLimitLabel,51,16,20,4");
        i_dailyAdjustLimit = WidgetFactory.createAmountField(12, i_precision, false);
        addValueChangedListener(i_dailyAdjustLimit);
        l_detailsPanel.add(i_dailyAdjustLimit, "dailyAdjustLimit,71,16,27,4");
        
        i_guiPrivilege = WidgetFactory.createCheckBox("GUI Privilege", SwingConstants.LEFT);
        addValueChangedListener(i_guiPrivilege);
        l_detailsPanel.add(i_guiPrivilege,  "guiPrivilege,21,23,20,5");

        i_pamiPrivilege = WidgetFactory.createCheckBox("PAMI Privilege", SwingConstants.LEFT);
        addValueChangedListener(i_pamiPrivilege);
        l_detailsPanel.add(i_pamiPrivilege, "pamiPrivilege,45,23,20,5");

        i_posiPrivilege = WidgetFactory.createCheckBox("POSI Privilege", SwingConstants.LEFT);
        addValueChangedListener(i_posiPrivilege);
        l_detailsPanel.add(i_posiPrivilege, "posiPrivilege,71,23,20,5");

        l_detailsPanel.add(WidgetFactory.createLabel("Failed Login Counter: "),
                           "failedLoginCounterLabel,1,28,20,4");
        i_failedLoginCounter = WidgetFactory.createIntegerField(4, false);
        l_detailsPanel.add(i_failedLoginCounter, "failedLoginCounter,21,28,6,4");
        i_failedLoginCounter.setEditable(false);

        i_unbarOperator = WidgetFactory.createCheckBox("Unbar Operator", SwingConstants.LEFT);
        addValueChangedListener(i_unbarOperator);
        l_detailsPanel.add(i_unbarOperator, "unbarOperator,71,28,20,5");

        l_detailsPanel.add(WidgetFactory.createLabel("Last Login: "),
                           "lastloginLabel,20,28,20,4");
        i_lastLoginDateTime = WidgetFactory.createDateField(this);
        l_detailsPanel.add(i_lastLoginDateTime, "lastLoginDateTime,40,28,27,4");
        i_lastLoginDateTime.setEditable(false);

        l_detailsPanel.add(WidgetFactory.createLabel("Failed Login Bar Counter: "),
                           "failedLoginBarCounterLabel,1,33,20,4");
        i_failedLoginBarCounter = WidgetFactory.createIntegerField(4, false);
        l_detailsPanel.add(i_failedLoginBarCounter, "failedLoginBarCounter,21,33,6,4");
        i_failedLoginBarCounter.setEditable(false);

        l_detailsPanel.add(WidgetFactory.createLabel("Bar To: "),
                           "lastloginLabel,20,33,20,4");
        i_barToDateTime = WidgetFactory.createDateField(this);
        l_detailsPanel.add(i_barToDateTime, "barToDateTime,40,33,27,4");
        i_barToDateTime.setEditable(false);

        return l_detailsPanel;
    }
    
    /**
     * Creates the operator market details panel.
     * @return Markets panel.
     */
    private JPanel createMarketsPanel()
    {
        JPanel   l_marketsPanel;

        l_marketsPanel = WidgetFactory.createPanel("Markets", 100, 36, 0, 0);
        
        l_marketsPanel.add(WidgetFactory.createLabel("Available Markets"),
                           "availableMarketsLabel,3,1,20,4");

        i_availableMarketsScrollPane = WidgetFactory.createScrollPane();
        i_availableMarketsScrollPane.setPreferredSize(new Dimension(35,24));
        i_availableMarkets = new Vector(1);
        i_availableMarketsList = WidgetFactory.createJList(i_availableMarkets);
        i_availableMarketsScrollPane.getViewport().setView(i_availableMarketsList);
        l_marketsPanel.add(i_availableMarketsScrollPane, "availableMarkets,3,5,35,24");


        i_addMarketButton = WidgetFactory.createButton("Add", this, false);
        l_marketsPanel.add(i_addMarketButton, "addMarketButton,43,7,14,5");

        i_removeMarketButton = WidgetFactory.createButton("Remove", this, false);
        l_marketsPanel.add(i_removeMarketButton, "removeMarketButton,43,15,14,5");

        l_marketsPanel.add(WidgetFactory.createLabel("Selected Markets"),
                           "selectedMarketsLabel,63,1,20,4");

        i_selectedMarketsScrollPane = WidgetFactory.createScrollPane();
        i_selectedMarketsScrollPane.setPreferredSize(new Dimension(35,24));
        i_screenSelectedMarkets = new Vector(1);
        i_selectedMarketsList = WidgetFactory.createJList(i_screenSelectedMarkets);
        i_selectedMarketsScrollPane.getViewport().setView(i_selectedMarketsList);
        l_marketsPanel.add(i_selectedMarketsScrollPane, "selectedMarkets,63,5,35,24");

        l_marketsPanel.add(WidgetFactory.createLabel("Default Market :"), 
                           "marketLabel,1,32,20,4");
        i_defaultMarketModel = new DefaultComboBoxModel(i_screenSelectedMarkets);
        i_defaultMarketBox = WidgetFactory.createComboBox(i_defaultMarketModel);
        addValueChangedListener(i_defaultMarketBox);
        l_marketsPanel.add(i_defaultMarketBox, "marketBox,21,32,35,4");

        return l_marketsPanel;
    }
    
    /** 
     * Adds the selected elements from the Available Markets to the Selected Markets list box. 
     */ 
    private void addSelectedMarkets()
    {
        boolean    l_alreadySelected;
        Object[]   l_availableMarkets;
        BoiMarket  l_market;
        
        l_availableMarkets = i_availableMarketsList.getSelectedValues();
        
        if (l_availableMarkets != null && l_availableMarkets.length > 0)
        {
            for (int i = 0; i < l_availableMarkets.length; i++)
            {
                l_alreadySelected = false;
                l_market = (BoiMarket)l_availableMarkets[i];
                
                // If this market is already selected, do not add
                for (int j = 0; j < i_screenSelectedMarkets.size(); j++)
                {
                    if (l_market.equals(i_screenSelectedMarkets.elementAt(j)))
                    {
                        l_alreadySelected = true;
                    }
                }
        
                if (!l_alreadySelected)
                {
                    i_screenSelectedMarkets.add(l_market);
                    i_selectedMarketsList.setListData(i_screenSelectedMarkets);

                    i_defaultMarketModel = new DefaultComboBoxModel(i_screenSelectedMarkets);
                    i_defaultMarketBox.setModel(i_defaultMarketModel);

                    // If this is the first selected market then use it as the default market.
                    if (i_screenSelectedMarkets.size() == 1)
                    {
                        i_defaultMarketBox.setSelectedItem(l_market);
                    }

                    modifyState();
                }
            }
        }
    }
    
    /** 
     * Removes the selected elements from the Selected Markets list box. 
     */ 
    private void removeSelectedMarkets()
    {
        Object[]  l_selectedMarkets;
        BoiMarket l_market;
        BoiMarket l_defaultMarket;
        boolean   l_foundDefault = false;
        
        l_selectedMarkets = i_selectedMarketsList.getSelectedValues();

        if (l_selectedMarkets != null && l_selectedMarkets.length > 0)
        {
            l_defaultMarket = (BoiMarket)i_defaultMarketBox.getSelectedItem();
            
            for (int i=0; i < l_selectedMarkets.length  && !l_foundDefault; i++)
            {
                l_market = (BoiMarket)l_selectedMarkets[i];
                
                // If one of the markets to be removed is the default market, then it can't be removed. 
                if (l_market.equals(l_defaultMarket))
                {
                    l_foundDefault = true;
                    displayMessageDialog(i_contentPane, "Cannot remove default market");
                }
            }

            if (!l_foundDefault)
            {
                for (int i=0; i < l_selectedMarkets.length; i++)
                {
                    i_screenSelectedMarkets.remove(l_selectedMarkets[i]);
                }
                i_selectedMarketsList.setListData(i_screenSelectedMarkets);
                modifyState();
            }
        }
    }

    /**
     * Refreshes the privilege levels combo box.  
     * If the currently selected record holds a privilege level which is withdrawn, 
     * then this privilege level has to be added to the combo box since the combo normally only
     * shows available values.
     * @param p_currentPrivilegeData Currently selected privilege level.
     */
    private void refreshPrivilegeCombo(PrilPrivilegeLevelData p_currentPrivilegeData)
    {
        // Ensure that any withdrawn privilege levels are removed from dropdown.  
        i_availablePrivileges = ((OperatorsDbService)i_dbService).getAvailablePrivileges();
        i_privilegesForCombo = (Vector)i_availablePrivileges.clone();

        // Check whether current privilege level is withdrawn and add to dropdown if this is the case. 
        if ( p_currentPrivilegeData != null &&
             !p_currentPrivilegeData.getIsCurrent())
        {
            i_privilegesForCombo.add(new BoiPrivilegeLevelData(p_currentPrivilegeData));
        }

        i_privilegesModel = new DefaultComboBoxModel(i_privilegesForCombo);
        i_privilegeBox.setModel(i_privilegesModel);

        if (p_currentPrivilegeData != null)
        {
            i_privilegeBox.setSelectedItem(new BoiPrivilegeLevelData(p_currentPrivilegeData));
        }
    }
    
    /**
     * Class to listen on checkbox events.
     */
    private class CheckBoxListener
    implements MouseListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /** @param p_mouseEvent The mouse event */
        public void mouseClicked(MouseEvent p_mouseEvent)
        {
            if (i_passwordPreExpired == (JCheckBox) p_mouseEvent.getSource())
            {
                if (i_passwordPreExpired.isSelected() == true)
                {
                    PpasDate l_date = DatePatch.getDateToday();
                    l_date.add(PpasDate.C_FIELD_DAY, -1);
                    i_passwordExpiryDate.setValue(l_date);
                }
                else
                {
                    i_passwordExpiryDate.setValue("");
                }
            }
        }
        
        /** @param p_mouseEvent The mouse event */
        public void mouseEntered(MouseEvent p_mouseEvent)
        {
        }
        
        /** @param p_mouseEvent The mouse event */
        public void mouseExited(MouseEvent p_mouseEvent)
        {
        }
        
        /** @param p_mouseEvent The mouse event */
        public void mousePressed(MouseEvent p_mouseEvent)
        {
        }
        
        /** @param p_mouseEvent The mouse event */
        public void mouseReleased(MouseEvent p_mouseEvent)
        {
        }
    }
}
