////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CommunityChargingCodesPaneUT.java
//      DATE            :       14-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5502
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for CommunityChargingCodesPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiCommunityChargingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for CommunityChargingCodesPane. */
public class CommunityChargingCodesPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "CommunityChargingCodesPaneUT";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Community Charging screen. */
    private CommunityChargingCodesPane i_cochPane;
    
    /** Community Charging code field. */
    private JFormattedTextField i_codeField;
    
    /** Community Charging description field. */
    private ValidatedJTextField i_descriptionField;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public CommunityChargingCodesPaneUT(String p_title)
    {
        super(p_title);
        
        i_cochPane = new CommunityChargingCodesPane(c_context);
        super.init(i_cochPane);
        
        i_cochPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(CommunityChargingCodesPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Community Charging screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("CommunityChargingCodesPane test");
        
        BoiCommunityChargingData l_boiCochData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_cochPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_cochPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiCochData = createCommunityChargingData(C_CODE, C_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_cochPane, i_keyDataCombo, l_boiCochData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_cochPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_cochPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_cochPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_cochPane, i_updateButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteCochRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteCochRecord(C_CODE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiCommunityChargingData object with the supplied data.
     * @param p_communityChargingId Community Charging id.
     * @param p_communityChargingDesc Community Charging description.
     * @return BoiCommunityChargingData object created from the supplied data.
     */
    private BoiCommunityChargingData createCommunityChargingData(String p_communityChargingId,
                                                                 String p_communityChargingDesc)
    {
        CochCommunityChargingData l_cochData;
        
        l_cochData = new CochCommunityChargingData(Integer.parseInt(p_communityChargingId),
                                                   p_communityChargingDesc,
                                                   ' ');
        
        return new BoiCommunityChargingData(l_cochData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteCochRecord = "deleteCochRecord";
    
    /**
     * Removes a row from COCH_COMMUNITY_CHARGING.
     * @param p_communityChargingId Community Charging id.
     */
    private void deleteCochRecord(String p_communityChargingId)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE from coch_community_charging WHERE coch_community_id = {0}");
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_communityChargingId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteCochRecord);
    }
}
