////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AgentInfoDbService.java
//      DATE            :       28-Nov-2005
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#1755/7500
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Database access class for Agent Information screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 08/11/06 | Yang Liu   | Added argument 'Market' to      |
//          |            | refreshAgentInfoDataVector.     | PpacLon#2079/10278
//----------+------------+---------------------------------+--------------------
// 02/04/07 | Andy Harris| Changes for suspect flag field. | PpacLon#3011/11241
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiAgentInfoData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAgentMstrSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Agent Information screen.
 */
public class AgentInfoDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------        
    
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** SQL service for agent information details. */
    private SrvaAgentMstrSqlService  i_agentSqlService         = null;
    
    /** Vector of agent information records. */
    private Vector                   i_availableAgentInfoDataV = null;
    
    /** Boi Agent Information data object. */
    private BoiAgentInfoData         i_agentInfoData           = null;
    
    /** Currently selected agent code. Used as key for read and delete. */
    private String                   i_currentCode            = null;
    
    /** Vector of available markets for display. */
    private Vector                   i_availableMarketsV       = null;
        
    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService      = null;
    
    /** Operator's default market. */
    private BoiMarket                i_defaultMarket           = null;
    
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public AgentInfoDbService(BoiContext p_context)
    {
        super(p_context);
        
        i_agentSqlService         = new SrvaAgentMstrSqlService(null);
        i_operatorSqlService      = new BicsysUserfileSqlService(null, null);
        i_availableAgentInfoDataV = new Vector(5);
        i_availableMarketsV       = new Vector(5);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }    
    
    /**
     * Set current Agent code. Used for record lookup in database.
     * @param p_code Currently selected agent code.
     */
    public void setCurrentCode(String p_code)
    {
        i_currentCode = p_code;
    }
    
    /**
     * Return Agent data currently being worked on.
     * @return Agent information data object.
     */
    public BoiAgentInfoData getAgentInfoData()
    {
        return i_agentInfoData;
    }
    
    /**
     * Set agent information data object data currently being worked on.
     * @param p_agentInfoData Agent information data object.
     */
    public void setAgentInfoData(BoiAgentInfoData p_agentInfoData)
    {
        i_agentInfoData = p_agentInfoData;
    }
    
    /**
     * Return Agent data.
     * @return Vector of available Agent Information data records.
     */
    public Vector getAvailableAgentinfoData()
    {
        return i_availableAgentInfoDataV;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAgentMstrDataSet l_agentDataSet = null;
        SrvaAgentMstrData    l_agentData    = null;
                
        l_agentDataSet = i_agentSqlService.readAll(null, p_connection);
        
        l_agentData = l_agentDataSet.getAgentDetails(i_currentCode, 
                                                     i_currentMarket.getMarket());
        
        i_agentInfoData = new BoiAgentInfoData(l_agentData);
    }
    
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;
        
        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_availableMarketsV.add(0, new BoiMarket(new Market(0, 0, "Global Market")));
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            i_currentMarket = i_defaultMarket;
        }

        refreshAgentInfoDataVector(p_connection, i_currentMarket.getMarket());
    }
    
    /**
     * Inserts record into the srva_agent_mstr table.
     * @param  p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAgentMstrData l_agentInfoData = null;
        
        l_agentInfoData = i_agentInfoData.getInternalAgentInfoData();
        
        i_agentSqlService.insert(null,
                                 p_connection,
                                 i_context.getOperatorUsername(),
                                 l_agentInfoData.getMarket(),
                                 l_agentInfoData.getAgent(),
                                 l_agentInfoData.getAgentName(),
                                 l_agentInfoData.getSuspectFlag());
        
        refreshAgentInfoDataVector(p_connection, l_agentInfoData.getMarket() );
    }
    
    /**
     * Updates record in the srva_agent_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAgentMstrData l_agentInfoData = null;
        
        l_agentInfoData = i_agentInfoData.getInternalAgentInfoData();
        
        i_agentSqlService.update(null,
                                 p_connection,
                                 i_context.getOperatorUsername(),
                                 l_agentInfoData.getMarket(),
                                 l_agentInfoData.getAgent(),
                                 l_agentInfoData.getAgentName(),
                                 l_agentInfoData.getSuspectFlag());
        
        refreshAgentInfoDataVector(p_connection,
                                   l_agentInfoData.getMarket());
    }
    
    /**
     * Deletes record from the srva_agent_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAgentMstrData l_agentInfoData = null;
        
        l_agentInfoData = i_agentInfoData.getInternalAgentInfoData();
        
        i_agentSqlService.delete(null,
                                 p_connection,
                                 i_context.getOperatorUsername(),
                                 l_agentInfoData.getMarket(),
                                 l_agentInfoData.getAgent());
        
        refreshAgentInfoDataVector(p_connection, i_currentMarket.getMarket());
    }
    
    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) 
        throws PpasSqlException
    {
        
        SrvaAgentMstrDataSet l_agentInfoDataSet = null;
        SrvaAgentMstrData    l_agentInfoData    = null;
        boolean[]            l_flagsArray  = new boolean[2];
        
        l_agentInfoDataSet = i_agentSqlService.readAll(null, p_connection);
        l_agentInfoData    = l_agentInfoDataSet.getAgentDetails(i_currentCode, i_currentMarket.getMarket());

        if (l_agentInfoData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = l_agentInfoData.isDeleted();
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }      

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the srva_agent_mstr table.
     * @param p_connection  The database connection.
     * @throws PpasSqlException SQL-related exception.
     */    
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        i_agentSqlService.markAsAvailable(null, 
                                          p_connection, 
                                          i_context.getOperatorUsername(),
                                          i_agentInfoData.getInternalAgentInfoData().getMarket(),
                                          i_currentCode);
        refreshAgentInfoDataVector(p_connection, 
                                   i_agentInfoData.getInternalAgentInfoData().getMarket());
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    
    /**
     * Refreshes the available charging data vector from the srva_agent_mstr table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshAgentInfoDataVector(JdbcConnection p_connection , Market p_market) 
        throws PpasSqlException
    {
        SrvaAgentMstrDataSet l_agentDataSet   = null;
        SrvaAgentMstrData[]  l_agentDataArray = null;
                     
        i_availableAgentInfoDataV.removeAllElements();
        
        l_agentDataSet = i_agentSqlService.readAll(null, p_connection);
        l_agentDataArray = l_agentDataSet.getRawAvailableArray(p_market); 
        
        i_availableAgentInfoDataV.addElement(new String(""));
        i_availableAgentInfoDataV.addElement(new String("NEW RECORD"));
        if (l_agentDataArray != null)
        {
            for (int i = 0; i < l_agentDataArray.length; i++)
            {
                i_availableAgentInfoDataV.addElement(new BoiAgentInfoData(l_agentDataArray[i]));
            }
        }
    }
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}
