	////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PurgeAndArchivePane.java
//      DATE            :       19-Aug-2004
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/3801
//                              PRD_ASCS00_ANA_FD_19
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to submit File and Database
//                              Purge & Archive jobs.
//                              
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/7/5   | M.Brister  | Added repeat and interval fields| PpacLon#1398/6788
//          |            | to enable repeating jobs to be  | 
//          |            | scheduled.                      | 
//          |            | Changed "Submission date & time"|
//          |            | to "Run date & time".           |
//----------+------------+---------------------------------+--------------------
// 02/08/06 | R.Grimshaw | Add constants for Service Class | PpacLon#2484
//          |            | History purge job.              | PRD_ASCS00_GEN_CA_89 
//----------+------------+---------------------------------+--------------------
// 01/11/06 | R.Grimshaw | Add Purge Voucher Update job to | PpacLon#2730
//          |            | purge job parameters map.       |  
//----------+------------+---------------------------------+--------------------
// 29/11/06 | R.Grimshaw | Allow file systems on ASCSA1 and| PpacLon#2785
//          |            | ASCSA2 to be purged.            |  
//----------+------------+---------------------------------+--------------------
//12/02/07  | K Bond     | Add constants for purge and     | PpacLon#2935/10970
//          |            | archive job.                    |  
//----------+------------+---------------------------------+--------------------
//10/07/07  | K Bond     | Add constants for promo         | PpacLon#3183/11804
//          |            | accumulation.                   |  
//----------+------------+---------------------------------+--------------------
//21/07/07  | S James    | Add constants for promo optin & | PpacLon#3288/11986
//          |            | accumulation.                   |  
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.slb.sema.ppas.boi.dbservice.BoiDbService;
import com.slb.sema.ppas.boi.dbservice.PurgeAndArchiveDbService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.purge.common.PurgeConstants;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Screen to manage Purge and Archive job submission and display the contents of the
 * PUCO_PURGE_CONTROL database table.
 */
public class PurgeAndArchivePane extends FocussedBoiGuiPane
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    
    /** Used to identify the type of job selected on the screen. */
    private static final String C_PA_JOB_TYPE_CUSTOMER_DATA = "Subscriber Data";
    
    /** Used to identify the type of job selected on the screen. */
    private static final String C_PA_JOB_TYPE_APPLICATION_DATA = "Application Data";
    
    /** Used to identify the type of job selected on the screen. */
    private static final String C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1 = "File System on ASCSA1";
    
    /** Used to identify the type of job selected on the screen. */
    private static final String C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2 = "File System on ASCSA2";
    
    //
    // Customer Data Jobs
    //
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_ADJUSTMENT = "Adjustment history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_EVENT = "Event history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_RECHARGE = "Voucher Refill history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_FAILED_RECHARGE = "Failed Voucher Refill history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_PAYMENT_HISTORY = "Account Refill history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_USAGE_PROMO = 
                                   "Usage Based Promotion Credit history and Bonus Adjustments";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_CLEARED_CREDIT = "Cleared Credit history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_PROMO_ALLOC = "Promotion Allocation history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_MEMO = "Memo history";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_CUST_DATA_PURGE_DISCONN_CUST = "Disconnected Subscribers";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String  C_PA_CUST_DATA_PURGE_SERVICE_FEE = "Service Fee Deductions";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String  C_PA_CUST_DATA_PURGE_FAF = "Family and Friends history";
    
    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_SVC = "Service Class Change history";
   
    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_NEVER_TRANSITION_TO_ASCS = "Never Transitioned To ASCS";
   
    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_DUPLICATE_ADJUSTMENTS = "Duplicate Adjustments";
   
    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_DUMMY_CUSTOMER_MSISDN_ALLOCATIONS = "Dummy Customer MSISDN Allocations";

    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_PROMO_ACCUM = "Promotion Accumulation History";
   
    /** Used to identify the name of the job selected on the screen. Value is {@value}. */
    private static final String  C_PA_CUST_DATA_PURGE_PROMO_OPTIN_ACCUM = "Promotion Opt In And Accumulation History";
   
    //
    // Application Data Jobs
    //
       
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_BATCH_CTRL_DATA = "Batch Process Control information";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_PURGE_CTRL_DATA =
                                    "Purge and Archive Process Control information";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_VOUCHER_UPDATE_LOG = "Voucher Update log";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_RDR_IPG_FILE_INFO = "Rdr Ipg File Information";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_RDR_DNE_FILE_INFO = "Rdr Dne File Information";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_RDR_ERR_FILE_INFO = "Rdr Err File Information";
    
    /** Used to identify the name of the job selected on the screen. */
    private static final String C_PA_APP_DATA_PURGE_RDR_INV_FILE_INFO = "Rdr Inv File Information";
    
    
    // Note that File System Job names are configurable and fetched from a purge properties file. 
    
    /** Flag indicating whether a job of the selected type has already been submitted. */
    private boolean i_jobSubmitted = false;
    
    /** Contains a HashMap of the categories of purge job. */
    private static final HashMap c_masterJobNameMap = new HashMap();
    
    /** Contains a HashMap of job names. */
    private static final HashMap c_jobNameMap = new HashMap();
    
    /** 2-D array for initialising the cells displayed in the table. */
    private static final String[][] c_dataInitial = {{"","","",""},{"","","",""},{"","","",""},
                                                     {"","","",""},{"","","",""},{"","","",""},
                                                     {"","","",""},{"","","",""},{"","","",""},
                                                     {"","","",""},{"","","",""},{"","","",""}};
    
    static
    {   
        // NB. Only the Cust Data jobs are submitted by a master job.
        c_masterJobNameMap.put(C_PA_JOB_TYPE_CUSTOMER_DATA, "purgeCustDataMaster");
        
        // All file system purge job types are submitted via the same job
        c_masterJobNameMap.put(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1, "purgeFileSystemAscsA1");   
        
        // All file system purge job types are submitted via the same job
        c_masterJobNameMap.put(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2, "purgeFileSystemAscsA2");   
        
        // Customer Data jobs
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_ADJUSTMENT,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_ADJUSTMENT);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_EVENT,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_COMMENT);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_RECHARGE,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_VOUCHER_REFILL);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_FAILED_RECHARGE,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_FAILED_VOUCHER_REFILL);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_PAYMENT_HISTORY,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_ACCOUNT_REFILL);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_USAGE_PROMO,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_USAGE_PROMO);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_CLEARED_CREDIT,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_CLEARED_CREDIT);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_PROMO_ALLOC,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_ALLOC);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_MEMO,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_MEMO);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_DISCONN_CUST,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_DISCONNECTED_CUST);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_SERVICE_FEE,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_SERVICE_FEE);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_FAF,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_FAF);
        
        c_jobNameMap.put(
                C_PA_CUST_DATA_PURGE_SVC,
                PurgeConstants.C_SUB_JOB_TYPE_PURGE_SVC_CLASS_HISTORY);
            
        c_jobNameMap.put(
        		C_PA_CUST_DATA_PURGE_NEVER_TRANSITION_TO_ASCS,
                PurgeConstants.C_SUB_JOB_TYPE_PURGE_NEVER_TRANSITIONED_TO_ASCS);
            
        c_jobNameMap.put(
        		C_PA_CUST_DATA_PURGE_DUPLICATE_ADJUSTMENTS,
                PurgeConstants.C_SUB_JOB_TYPE_PURGE_DUPLICATE_ADJUSTMENTS);
            
        c_jobNameMap.put(
        		C_PA_CUST_DATA_PURGE_DUMMY_CUSTOMER_MSISDN_ALLOCATIONS,
                PurgeConstants.C_SUB_JOB_TYPE_PURGE_DUMMY_CUSTOMER_MSISDN_ALLOCATIONS);
            
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_PROMO_ACCUM,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_ACCUM);
        
        c_jobNameMap.put(
            C_PA_CUST_DATA_PURGE_PROMO_OPTIN_ACCUM,
            PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_OPTIN_ACCUM);
        // Application Data jobs
        
        // RDR Process Control Information
        // TODO: RDR Process Control Info job integration
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_RDR_IPG_FILE_INFO,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_RDR_CTRL_DATA);
        
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_RDR_DNE_FILE_INFO,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_RDR_CTRL_DATA);
        
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_RDR_ERR_FILE_INFO,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_RDR_CTRL_DATA);
        
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_RDR_INV_FILE_INFO,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_RDR_CTRL_DATA);
        
        
        // Batch Process Control Information
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_BATCH_CTRL_DATA,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_BATCH_CTRL_DATA);
        
        // Purge and Archive Process Control information
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_PURGE_CTRL_DATA,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_PURGE_CTRL_DATA);
        
        // Voucher Update log 
        c_jobNameMap.put(C_PA_APP_DATA_PURGE_VOUCHER_UPDATE_LOG,
                         PurgeConstants.C_SUB_JOB_TYPE_PURGE_VOUCHER_UPDATE);        
    }
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "PurgeAndArchivePane";

    /** ComboBox containing the types of Purge job. */
    private JComboBox i_jobTypeComboBox = null;
    
    /** ComboBox containing the names of the individual Purge jobs. */
    private JComboBox i_jobNameComboBox = null;
    
    /** CheckBox for archive option. */
    private JCheckBox i_archiveCheckBox = null;
    
    /** CheckBox for recovery option. */
    private JCheckBox i_recoveryCheckBox = null;

    /** Collection of purge job types. */
    private Vector    i_availablePurgeJobTypes = null;
    
    /** Collection of purge job names. */
    private Vector    i_configuredJobNames = null;
    
    /** Collection of Customer Data type job names. */
    private Vector    i_configuredCustDataJobNames = null;
    
    /** Collection of Application History type job names. */
    private Vector    i_configuredAppHistJobNames = null;
    
    /** Collection of display names for File System purge jobs. */
    private Vector    i_configuredFileSystemJobNames = null;
    
    /** Collection of types associated with the displayed File System purge job names. */
    private Vector    i_configuredFileSystemJobTypes = null;
    
    /** Table model for table displayed on this screen. */
    private StringTableModel i_stringTableModel = null;
    
    /** The table object that will be displayed on this screen. */
    private JTable i_table = null;
    
    /** The 2-D array that is populated with the data to be displayed in the table. */
    private String[][] i_data = null;
    
    /** Panel to contain the purge job types, names and the data in PUCO_PURGE_CONTROL. */
    private JPanel i_jobsPanel = null;
    
    /** Panel to contain the fields, information and buttons needed to submit a purge job. */
    private JPanel i_fieldsPanel = null;
    
    /** Text field to hold the run date and time. */
    private JFormattedTextField i_runDateTimeField = null;
    
    /** Text field to hold the purge eligibility date. This field should be read-only. */
    private JFormattedTextField i_purgeEligibilityDateField = null;
    
    /** Field to display the job name on the job submission panel. */
    private ValidatedJTextField i_jobNameReadOnlyField = null;
    
    /** Button that, when clicked, repopulates the screen display with the latest data from the database. */
    private JButton i_refreshButton = null;
    
    /** Button that, when clicked, validates and possibly subsequently submits the selected purge job. */
    private JButton i_submitButton = null;
    
    /** Initialises the Submit Job panel. */
    private JButton i_clearButton = null;
    
    /** Radio button to indicate that data for all executed jobs should be displayed. */
    private JRadioButton i_allJobs = null;
    
    /** Radio button to indicate that only data from those jobs in progress should be displayed. */
    private JRadioButton i_jobsInProgress = null;
    
    /** The BOI database servision to be used by this pane. */
    private PurgeAndArchiveDbService i_dbService = null;
    
    /** HashMap containing the retention periods keyed on the display name. */
    private HashMap  i_retentionPeriodsMap = new HashMap();

    /** Used to display text next to the frequency combo-box. */
    private JLabel i_frequencyLabel = null;

    /** ComboBox to hold the job submission frequencies. */
    private JComboBox i_frequencyBox = null;
    
    /** Used to display text next to the interval field. */
    private JLabel i_intervalLabel = null;

    /** Text field to hold the job submission interval. */
    private JFormattedTextField i_intervalField = null;
    
    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Contructs the Purge And Archive Pane.
     * @param p_context
     */
    public PurgeAndArchivePane(Context p_context)
    {
        super(p_context, (Container)null);
        init();
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * Sets the job type combo box to have the focus on screen entry.
     */
    public void defaultFocus()
    {
        i_jobTypeComboBox.requestFocusInWindow(); 
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        i_jobTypeComboBox.setSelectedIndex(0);
        repopulateJobNamesComboBox();
        repopulateScreenTable();
        initialiseJobSubmissionPanel();
        setStateOfFields();
        i_state = C_STATE_INITIAL;
    }
    
    /**
     * Not required.
     * @param p_event The <code>GuiEvent</code> that has occurred
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing.
    }

    /**
     * Handles ActionEvents produced by the Purge and Archive pane.
     * @param p_event The <code>ActionEvent</code> to handle
     */
    public void actionPerformed(ActionEvent p_event)
    {
        if (p_event.getSource() == i_submitButton)
        {   
            // Request focus on submit button such that focus lost processing
            // is performed on previous focus owner.  
            i_submitButton.requestFocusInWindow();
            
            // Perform submit processing after the current focus lost event.
            // i.e. After field validation has occurred for previous focus owner.
            SwingUtilities.invokeLater(new SubmitRunnable());
        }
        else if (p_event.getSource() == i_clearButton)
        {
            initialiseJobSubmissionPanel();
        }
        else if (p_event.getSource() == i_allJobs)
        {
            repopulateScreenTable();
        }
        else if (p_event.getSource() == i_jobsInProgress)
        {
            repopulateScreenTable();
        }
        else if (p_event.getSource() == i_refreshButton)
        {
            repopulateScreenTable();
        }
    }


    /**
     * Handles ItemEvents for the P&A screen.
     * @param p_itemEvent The <code>ItemEvent</code> that has occurred
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        if (p_itemEvent.getSource() == i_jobTypeComboBox)
        {
            repopulateJobNamesComboBox();
            repopulateJobSubmissionFields();
            repopulateScreenTable();
            setStateOfFields();
            i_jobSubmitted = false;
            setScreenState();
        }
        else if (p_itemEvent.getSource() == i_jobNameComboBox)
        {
            repopulateScreenTable();
            repopulateJobSubmissionFields();
            i_jobSubmitted = false;
            setScreenState();
        }
        else if (p_itemEvent.getSource() == i_frequencyBox)
        {
            if (i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE))
            {
                i_intervalLabel.setVisible(false);
                i_intervalField.setVisible(false);
                i_intervalField.setText("");
            }
            else
            {
                i_intervalLabel.setVisible(true);
                i_intervalField.setVisible(true);
            }
        }
    }
    
    /**
     * Handles KeyEvents for the P&A screen.
     * @param p_keyEvent The <code>KeyEvent</code> that is to be dispatched
     * @return True if the event was dispatched, and false otherwise
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean  l_return = false;
            
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_clearButton)
            {
                i_clearButton.doClick();
            }
            else
            {
                i_submitButton.doClick();
            }
            l_return = true;
        }

        return l_return;
    }
    
    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    
    /**
     * Initialise this screen and create necessary services.
     */
    private void init()
    {
        JPanel          l_mainPanel = null;
        String          l_custDataRetentionPeriods = null;
        Object          l_fileRetentionPeriods = null;
        int             l_nextToken = 0;
        String[]        l_st = null;
        BoiHelpListener l_helpComboListener;
        
        // Create the database service for this screen
        i_dbService = new PurgeAndArchiveDbService((BoiContext)i_context);
        
        // Get the Retention Period config
        
        // File retention periods should be read into a Map, keyed on job type
        l_fileRetentionPeriods = ((BoiContext)i_context).getObject("ascs.boi.fileRetentionPeriods");
        l_st = ((String)l_fileRetentionPeriods).split(",");
        l_nextToken++;
        
        while (l_nextToken < l_st.length)
        {
            i_retentionPeriodsMap.put(l_st[l_nextToken++].trim(), l_st[l_nextToken++].trim());
        }
        
        l_custDataRetentionPeriods =
            (String)((BoiContext)i_context).getObject("ascs.boi.custDataRetentionPeriods");
        
        l_st = l_custDataRetentionPeriods.split(",");
        l_nextToken = 0;
        l_nextToken++;
        
        while (l_nextToken < l_st.length)
        {
            i_retentionPeriodsMap.put(l_st[l_nextToken++].trim(), l_st[l_nextToken++].trim());
        }
        
        //System.out.println("MIE i_retentionPeriodsMap: " + i_retentionPeriodsMap);
        
        l_mainPanel = WidgetFactory.createMainPanel("Purge & Archive", 100, 100, 0, 0);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                       BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_PURGE_AND_ARCHIVE_SCREEN), 
                                       l_helpComboListener);
        i_helpComboBox.setFocusable(false);

        i_jobsPanel = createJobsPanel();
        i_fieldsPanel = createSubmitJobPanel();
       
        l_mainPanel.add(i_helpComboBox, "purgeHelpComboBox,60,1,40,4");
        l_mainPanel.add(i_jobsPanel, "jobsPanel,1,6,100,54");
        l_mainPanel.add(i_fieldsPanel, "fieldsPanel,1,61,100,40");
        
        i_contentPane = l_mainPanel;

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        
        // Set this Container to be the root of a focus traversal cycle.
        l_mainPanel.setFocusCycleRoot(true);        
        l_mainPanel.setVisible(true);
    }
    
    /**
     * Create a panel to hold the job types and job progress table.
     * @return A JPanel containing the Job Types and job history.
     */
    private JPanel createJobsPanel()
    {
        JScrollPane l_tableScrollPane = null;
        JPanel      l_jobsPanel = null;
        JLabel      l_jobTypeLabel = null;
        JLabel      l_dataTypeLabel = null;
        ButtonGroup l_buttonGroup = null;
        Object      l_availablePurgeJobTypes = null;
        Object      l_availableCustDataJobs = null;
        Object      l_availableApplDataJobs = null;
        Object      l_fileSystemJobTypes = null;
        String[]    l_st = null;
        int         l_nextToken = 0;
        int[]       l_columnWidths = new int[] {202,202,202,202};

        l_jobsPanel = WidgetFactory.createPanel("Jobs", 100, 54, 0, 0);
        
        // Get the configurion for which purge jobs are configured for submission via BOI 
        
        l_availablePurgeJobTypes  = ((BoiContext)i_context).getObject("ascs.boi.availablePurgeJobTypes");
        l_availableCustDataJobs   = ((BoiContext)i_context).getObject("ascs.boi.availableCustDataJobs");
        l_availableApplDataJobs   = ((BoiContext)i_context).getObject("ascs.boi.availableApplDataJobs");
        l_fileSystemJobTypes      = ((BoiContext)i_context).getObject("ascs.boi.fileSystemJobTypes");
        
        // Split up the list of job types into Vector of Strings objects, that can be passed into a combo-box
        if (l_availablePurgeJobTypes != null)
        {
            i_availablePurgeJobTypes = new Vector(15);
            i_availablePurgeJobTypes.add(0, "");
            l_st         = ((String)l_availablePurgeJobTypes).split(",");
            
            while (l_nextToken < l_st.length)
            {
                i_availablePurgeJobTypes.add(l_st[l_nextToken++].trim());
            }
            i_availablePurgeJobTypes.trimToSize();
        }
        
        if (l_availableCustDataJobs != null)
        {
            l_nextToken = 0;
            i_configuredCustDataJobNames = new Vector(15);
            l_st         = ((String)l_availableCustDataJobs).split(",");
            while (l_nextToken < l_st.length)
            {
                i_configuredCustDataJobNames.add(l_st[l_nextToken++].trim());
            }
            i_configuredCustDataJobNames.add(0, "");
            i_configuredCustDataJobNames.trimToSize();
        }
        
        if (l_availableApplDataJobs != null)
        {
            l_nextToken = 0;
            i_configuredAppHistJobNames = new Vector(15);
            l_st         = ((String)l_availableApplDataJobs).split(",");
            while (l_nextToken < l_st.length)
            {
                i_configuredAppHistJobNames.add(l_st[l_nextToken++].trim());
            }
            i_configuredAppHistJobNames.add(0, "");
            i_configuredAppHistJobNames.trimToSize();
        }
        
        if (l_fileSystemJobTypes != null)
        {
            l_nextToken = 0;
            i_configuredFileSystemJobNames = new Vector(5);
            i_configuredFileSystemJobTypes = new Vector(5);
            l_st         = ((String)l_fileSystemJobTypes).split(",");
            while (l_nextToken < l_st.length)
            {
                i_configuredFileSystemJobTypes.add(l_st[l_nextToken++].trim());
                i_configuredFileSystemJobNames.add(l_st[l_nextToken++].trim());
            }
            i_configuredFileSystemJobNames.add(0, "");
            i_configuredFileSystemJobNames.trimToSize();
        }

        l_jobTypeLabel = WidgetFactory.createLabel("Job Type: ");
        i_jobTypeComboBox = WidgetFactory.createComboBox(i_availablePurgeJobTypes, this);
        
        l_dataTypeLabel = WidgetFactory.createLabel("Job Name: ");
        i_configuredJobNames = new Vector();
        i_jobNameComboBox = WidgetFactory.createComboBox(i_configuredJobNames, this);
        
        i_refreshButton = WidgetFactory.createButton("Refresh", this, false);
        
        i_jobsInProgress = WidgetFactory.createRadioButton("In progress", this);
        i_allJobs = WidgetFactory.createRadioButton("All", this);
        
        l_jobsPanel.add(l_jobTypeLabel, "jobTypeLabel,1,1,20,4");
        l_jobsPanel.add(i_jobTypeComboBox, "jobTypeComboBox,21,1,50,4");
        l_jobsPanel.add(l_dataTypeLabel, "dataTypeLabel,1,6,20,4");
        l_jobsPanel.add(i_jobNameComboBox, "dataTypeComboBox,21,6,50,4");
        
        l_buttonGroup = new ButtonGroup();
        l_buttonGroup.add(i_jobsInProgress);
        l_buttonGroup.add(i_allJobs);
        i_jobsInProgress.setSelected(true);
        
        // The following is for the setup of the Jobs History table
        String[] l_columnNames = { "Execution Date & Time",
                                   "Purge Eligibility Date",
                                   "Archived",
                                   "Status"};

        i_data = c_dataInitial;
        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table =
            WidgetFactory.createTable(l_columnWidths, 150, 100, i_stringTableModel, this);
        l_tableScrollPane = new JScrollPane(
                                    i_table,
                                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
              
        l_jobsPanel.add(l_tableScrollPane, "tableScrollPane,1,12,100,36");
        l_jobsPanel.add(i_refreshButton, "refreshButton,1,49,15,5");
        l_jobsPanel.add(i_jobsInProgress, "jobsInProgress,21,49,20,5");
        l_jobsPanel.add(i_allJobs, "allJobs,41,49,20,5");
        
        return l_jobsPanel;
    }

    /**
     * This panel displays the information necessary for the job to be submitted.
     * @return A job submission panel.
     */
    private JPanel createSubmitJobPanel()
    {
        JPanel l_fieldsPanel = null;
        JLabel l_jobTypeLabel = null;
        JLabel l_runDateTimeLabel = null;
        JLabel l_purgeEligibilityDateLabel = null;
        PpasDateTime l_now = null;
        
        try
        {
            i_dbService.readInitialData();
        }
        catch (PpasServiceFailedException e)
        {
            handleException(e);
        }
        l_fieldsPanel = WidgetFactory.createPanel("Submit Job",100, 40, 0, 0);
        
        l_jobTypeLabel = WidgetFactory.createLabel("Job Name: ");
        i_jobNameReadOnlyField = WidgetFactory.createTextField(60);
        i_jobNameReadOnlyField.setEnabled(false);
        
        l_runDateTimeLabel = WidgetFactory.createLabel("Run Date & Time: ");
        i_runDateTimeField = WidgetFactory.createDateTimeField(this);
        l_now = DatePatch.getDateTimeNow();
        i_runDateTimeField.setValue(l_now);
        
        i_frequencyLabel = WidgetFactory.createLabel("Repeat: ");
        i_frequencyBox = WidgetFactory.createComboBox(i_dbService.getRepeatFreqs(), this);
        
        i_intervalLabel = WidgetFactory.createLabel("Interval: ");
        i_intervalField = WidgetFactory.createIntegerField(2, false);

        l_purgeEligibilityDateLabel = WidgetFactory.createLabel("Purge Eligibility Date: ");
        i_purgeEligibilityDateField = WidgetFactory.createDateField(this);

        i_purgeEligibilityDateField.setValue(new PpasDate(""));
        i_purgeEligibilityDateField.setEnabled(false);
        
        i_archiveCheckBox = WidgetFactory.createCheckBox("Archive", SwingConstants.LEFT);
        i_archiveCheckBox.setVisible(false);
        i_recoveryCheckBox = WidgetFactory.createCheckBox("Recovery", SwingConstants.LEFT);
        i_recoveryCheckBox.setVisible(false);

        i_submitButton = WidgetFactory.createButton("Submit", this, true);
        i_clearButton = WidgetFactory.createButton("Clear", this, false);
        i_clearButton.setVerifyInputWhenFocusTarget(false);
        
        l_fieldsPanel.add(l_jobTypeLabel, "jobTypeLabel,1,1,20,4");
        l_fieldsPanel.add(i_jobNameReadOnlyField, "jobTypeReadOnlyField,21,1,50,4");
        l_fieldsPanel.add(l_runDateTimeLabel, "runDateTimeLabel,1,6,20,4");
        l_fieldsPanel.add(i_runDateTimeField, "runDateTimeField,21,6,30,4");
        l_fieldsPanel.add(i_frequencyLabel, "frequencyLabel,53,6,8,4");
        l_fieldsPanel.add(i_frequencyBox, "frequencyBox,61,6,14,4");
        l_fieldsPanel.add(i_intervalLabel, "intervalLabel,77,6,10,4");
        l_fieldsPanel.add(i_intervalField, "intervalField,87,6,6,4");
        l_fieldsPanel.add(l_purgeEligibilityDateLabel, "purgeEligibilityDateLabel,1,11,20,4");
        l_fieldsPanel.add(i_purgeEligibilityDateField, "purgeEligibilityDateField,21,11,20,4");
        
        l_fieldsPanel.add(i_archiveCheckBox, "archiveCheckBox,21,16,15,5");
        l_fieldsPanel.add(i_recoveryCheckBox, "recoveryCheckBox,21,21,15,5");
        l_fieldsPanel.add(i_submitButton, "submitButton,1,34,15,5");
        l_fieldsPanel.add(i_clearButton, "clearButton,17,34,15,5");
        
        return l_fieldsPanel;
    }
    

    
    /**
     * Clears the Submit Job panel.
     */
    private void initialiseJobSubmissionPanel()
    {
        i_frequencyBox.setSelectedIndex(0);
        i_intervalLabel.setVisible(false);
        i_intervalField.setVisible(false);
        i_intervalField.setText("");

        repopulateJobSubmissionFields();
    }

    /**
     * Repopulates this screen's table with the rows from PUCO_PURGE_CONTROL for the
     * selected job type.
     */    
    private void repopulateScreenTable()
    {
        String l_pucoDatabaseKey = null;
        int    l_jobNameComboBoxIndex = 0;
        
        l_jobNameComboBoxIndex = i_jobNameComboBox.getSelectedIndex();
        try
        {
            i_dbService.readData();
            
            // The PUCO database table key will depend on the job name. The way we  
            // get the job name depends on the job type
            
            if ((i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1) ||
                 i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2))    
                    && 
                l_jobNameComboBoxIndex > 0)
            {
                l_pucoDatabaseKey = 
                    i_configuredFileSystemJobTypes.elementAt(l_jobNameComboBoxIndex - 1).toString();
            
                i_data = i_dbService.getTableData(l_pucoDatabaseKey, i_allJobs.isSelected());
            }
            else if (i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_CUSTOMER_DATA) &&
                     l_jobNameComboBoxIndex != 0)
            {
                l_pucoDatabaseKey = (String)c_jobNameMap.get(i_jobNameReadOnlyField.getText().trim());
                i_data = i_dbService.getTableData(l_pucoDatabaseKey, i_allJobs.isSelected());
            }
            else if (i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_APPLICATION_DATA) &&
                    l_jobNameComboBoxIndex != 0)
            {
                l_pucoDatabaseKey = (String)c_jobNameMap.get(i_jobNameReadOnlyField.getText().trim());
                i_data = i_dbService.getTableData(l_pucoDatabaseKey, i_allJobs.isSelected());
            }
            else
            {
                i_data = c_dataInitial;
            }
              
            
            i_stringTableModel.setData(i_data);
            i_table.setModel(i_stringTableModel);
                 
        }
        catch (PpasServiceFailedException e)
        {
            handleException(e);
        }
    }

    /**
     * 
     *
     */    
    private void repopulateJobNamesComboBox()
    {
        DefaultComboBoxModel l_model = null;
        
        // Repopulate the vector that we give to the combo box
        if (i_jobTypeComboBox.getSelectedItem().toString().equals(C_PA_JOB_TYPE_CUSTOMER_DATA))
        {
            i_configuredJobNames = i_configuredCustDataJobNames;
        }
        else if (i_jobTypeComboBox.getSelectedItem().toString().equals(C_PA_JOB_TYPE_APPLICATION_DATA))
        {
            i_configuredJobNames = i_configuredAppHistJobNames;
        }
        else if (i_jobTypeComboBox.getSelectedItem().toString().equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1) ||
                 i_jobTypeComboBox.getSelectedItem().toString().equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2))
        {
            // Must be file system purge job.
            i_configuredJobNames = i_configuredFileSystemJobNames;   
        }
        else
        {
            i_configuredJobNames = new Vector(1);
            i_configuredJobNames.add(0, "");
        }
        
        // Set the combobox model

        l_model = new DefaultComboBoxModel(i_configuredJobNames);
        i_jobNameComboBox.setModel(l_model); 
    }
    
    /**
     * Adjusts the display of the submission panel
     *
     */
    private void repopulateJobSubmissionFields()
    {
        PpasDateTime l_now = DatePatch.getDateTimeNow();
        PpasDate     l_eligibilityDate = DatePatch.getDateToday();
        String l_jobType = (String)i_jobTypeComboBox.getSelectedItem();
        String l_jobName = (String)i_jobNameComboBox.getSelectedItem();
        
        if (l_jobName != null)
        {   
            i_jobNameReadOnlyField.setText(i_jobNameComboBox.getSelectedItem().toString());

            // Set the run date & time
            i_runDateTimeField.setValue(l_now);
        
            if (!l_jobName.equals(""))
            {
                // Set the purge eligibility date
        
                // If the selected job type is of the type File System, we get the file retention
                // period from a HashMap, otherwise we just get it.
                if (l_jobType.equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1) ||
                    l_jobType.equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2))
                {
                    Object l_fileRetentionPeriod = i_retentionPeriodsMap.get(
                                                       i_jobNameComboBox.getSelectedItem());
                    
                    if (l_fileRetentionPeriod != null && !l_fileRetentionPeriod.equals("null"))
                    {
                        l_eligibilityDate.add(PpasDate.C_FIELD_DATE,
                              0 - Integer.parseInt((String)l_fileRetentionPeriod));
                        i_purgeEligibilityDateField.setValue(l_eligibilityDate);
                        //DatePatch.clearPpasOffset();
                    }
                    else
                    {
                        // TODO: Get the default retention period and set the purge eligibility date
                        
                        // For now, set the eligibilityDate to be approx 6 months ago
                        l_eligibilityDate.add(PpasDate.C_FIELD_DATE,
                                              -180);
                        i_purgeEligibilityDateField.setValue(l_eligibilityDate);
                        
//                        displayMessageDialog(
//                            i_contentPane,
//                            "No file retention period has been configured for " +
//                            "this purge job. Add this configuration and restart the browser");
                    }
                }
                else if (l_jobType.equals(C_PA_JOB_TYPE_CUSTOMER_DATA) ||
                         l_jobType.equals(C_PA_JOB_TYPE_APPLICATION_DATA))
                {
                    //TODO: Set the purge eligibility date
                    Object l_fileRetentionPeriod = i_retentionPeriodsMap.get(
                                                       c_jobNameMap.get(i_jobNameComboBox.getSelectedItem()));
//                    System.out.println("MIE l_fileRetentionPeriod: " + l_fileRetentionPeriod);
                                              
                    if (l_fileRetentionPeriod != null && !l_fileRetentionPeriod.equals("null"))
                    {
                        l_eligibilityDate.add(PpasDate.C_FIELD_DATE,
                              0 - Integer.parseInt((String)l_fileRetentionPeriod));
                        i_purgeEligibilityDateField.setValue(l_eligibilityDate);
                        //DatePatch.clearPpasOffset();
                    }
                    else
                    {
                        // TODO: Get the default retention period and set the purge eligibility date
                        
                        // For now, set the eligibilityDate to be approx 6 months ago
                        l_eligibilityDate.add(PpasDate.C_FIELD_DATE,
                                              -180);
                        i_purgeEligibilityDateField.setValue(l_eligibilityDate);
//                        displayMessageDialog(
//                            i_contentPane,
//                            "No file retention period has been configured for " +
//                            "this purge job. Add this configuration and restart the browser");
                    }
                }
                else if (l_jobType.equals(C_PA_JOB_TYPE_APPLICATION_DATA))
                {
                    //TODO: Set the purge eligibility date
                }
            }
            else
            {
                i_purgeEligibilityDateField.setValue(new PpasDate(""));
            }
        }
        setRecoveryFlags();
    }
    
    /**
     * Enables or disables fields depending on the selected purge job
     */
    private void setStateOfFields()
    {
        // TODO: May be something to do with archive and recovery flags in the future.
    }
    
    /**
     * Validate the screen information to be passed to the Job Scheduler on
     * job submission. 
     * 
     * @return True if the information is valid and false otherwise
     *
     */
    private boolean validateSubmitJobFields()
    {
        String l_jobType = (String)i_jobTypeComboBox.getSelectedItem();
        String l_jobName = i_jobNameReadOnlyField.getText().trim();
        
        if (l_jobType.trim().equals("") || l_jobType == null)
        {
            i_jobTypeComboBox.requestFocusInWindow();
            displayMessageDialog(
                i_contentPane,
                "A job type and job name must be selected before job submission");
            return false;
        }
        if (l_jobName.equals("") || l_jobName == null)
        {
            i_jobNameComboBox.requestFocusInWindow();
            displayMessageDialog(
                i_contentPane,
                "A job name must be selected before job submission");
            
            return false;
        }
        
        // Validate that run date and time are not blank
        if (i_runDateTimeField.getText().trim().equals(""))
        {
            i_runDateTimeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "You must specify a run date & time");
            return false;
        }

        if ( !i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE) &&
             ( i_intervalField.getText().equals("") || Integer.parseInt(i_intervalField.getText()) == 0 ))
        {
            i_intervalField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Interval must be set for a repeating job");
            return false;
        }
        
        return true;
    }
    
    /**
     * Creates and populates a <code>HashMap</code> containing the parameters necessary for job submission. 
     * @return A HashMap containing the necessary parameters for job submission
     * @throws PpasServiceFailedException if method fails to populate job parameters Map.
     */
    private HashMap createJobParametersMap() throws PpasServiceFailedException
    {
        HashMap l_jobParamsHashMap = null;
        String  l_selectedJobType = null;
        String  l_selectedJobName = null;
        String  l_archiveFlag = null;
        String  l_recoveryFlag = null;
        
        l_jobParamsHashMap = new HashMap();
        l_selectedJobType = i_jobTypeComboBox.getSelectedItem().toString();
        l_selectedJobName = i_jobNameReadOnlyField.getText().trim();
        
        l_archiveFlag = i_archiveCheckBox.isSelected()? "Y" : "N";
        l_recoveryFlag = i_recoveryCheckBox.isSelected() ? "Y" : "N";
               
        if (i_archiveCheckBox.isVisible())
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_ARCHIVE_FLAG, l_archiveFlag);
        }
        
        if (i_recoveryCheckBox.isVisible())
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_RECOVERY, l_recoveryFlag);
        }
        
        if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_ADJUSTMENT))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_ADJUSTMENT);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_EVENT))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_COMMENT);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_RECHARGE))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_VOUCHER_REFILL);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_FAILED_RECHARGE))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_FAILED_VOUCHER_REFILL);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_CLEARED_CREDIT))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_CLEARED_CREDIT);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_PROMO_ALLOC))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_ALLOC);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_MEMO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_MEMO);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_PAYMENT_HISTORY))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_ACCOUNT_REFILL);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_DISCONN_CUST))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_DISCONNECTED_CUST);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_USAGE_PROMO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_USAGE_PROMO);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_SERVICE_FEE))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_SERVICE_FEE);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_FAF))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_FAF);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_SVC))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_SVC_CLASS_HISTORY);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_PROMO_ACCUM))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_ACCUM);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_PROMO_OPTIN_ACCUM))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_PROMO_OPTIN_ACCUM);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_NEVER_TRANSITION_TO_ASCS))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_NEVER_TRANSITIONED_TO_ASCS);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_DUPLICATE_ADJUSTMENTS))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_DUPLICATE_ADJUSTMENTS);
        }
        else if (l_selectedJobName.equals(C_PA_CUST_DATA_PURGE_DUMMY_CUSTOMER_MSISDN_ALLOCATIONS))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_DUMMY_CUSTOMER_MSISDN_ALLOCATIONS);
        }
        // Application Data purge jobs
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_RDR_IPG_FILE_INFO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_RDR_FILE_STATUS_IPG);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_RDR_DNE_FILE_INFO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_RDR_FILE_STATUS_DNE);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_RDR_ERR_FILE_INFO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_RDR_FILE_STATUS_ERR);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_RDR_INV_FILE_INFO))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_RDR_FILE_STATUS_INV);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_PURGE_CTRL_DATA))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_PURGE_CTRL_DATA);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_BATCH_CTRL_DATA))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_BATCH_CTRL_DATA);
        }
        else if (l_selectedJobName.equals(C_PA_APP_DATA_PURGE_VOUCHER_UPDATE_LOG))
        {
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   PurgeConstants.C_SUB_JOB_TYPE_PURGE_VOUCHER_UPDATE);
        }
        else if (l_selectedJobType.equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA1) ||
                 l_selectedJobType.equals(C_PA_JOB_TYPE_FILE_SYSTEM_ASCSA2))
        {
            // File System purge jobs
            l_jobParamsHashMap.put(PurgeConstants.C_KEY_PARAM_JOB_TYPE,
                                   i_configuredFileSystemJobTypes.elementAt(
                                       i_jobNameComboBox.getSelectedIndex() - 1));
        }
        else
        {
            PpasServiceFailedException l_psE = 
                new PpasServiceFailedException(
                    C_CLASS_NAME,
                    "createJobParametersHashMap",
                    10285,
                    this,
                    null,
                    0,
                    ServiceKey.get().configMissing(
                        l_selectedJobName, 
                        PurgeConstants.C_KEY_PARAM_JOB_TYPE));

            throw l_psE; 
        }
        
        return l_jobParamsHashMap;
    }

    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * Handles submit events.
     */
    private void doSubmit()
    {
        String  l_jsJobName = null;        
        HashMap l_jobParams = null;
        boolean l_valid = false;
        int l_dialogueResponse = 0;
        
        l_valid = validateSubmitJobFields();
        
        if (l_valid)
        {
            l_dialogueResponse = displayConfirmDialog(
                                     i_contentPane,
                                     "Do you wish to submit this job?");
        
            if (l_dialogueResponse == JOptionPane.YES_OPTION)
            {
                try 
                {
                    l_jobParams = createJobParametersMap();

                    // TODO: Change this to use Debug
//                  System.out.println("MIE submitting purge job with params - ");
//                  System.out.println("    jsContextJobType: " + String.valueOf(c_masterJobNameMap.get(
//                    i_jobTypeComboBox.getSelectedItem().toString())));
//                  System.out.println("    l_nextJobId: " + l_nextJobId);
//                  System.out.println("    l_hashMap: " + l_jobParamsHashMap);
                
                    if (i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_APPLICATION_DATA))
                    {
                        l_jsJobName = (String)c_jobNameMap.get(i_jobNameComboBox.getSelectedItem());
                    }
                    else
                    {
                        //System.out.println("Getting master job name from c_masterJobNameMap");
                        l_jsJobName = (String)c_masterJobNameMap.get(
                            i_jobTypeComboBox.getSelectedItem());
                                              //e.g. "purgeCustDataMaster", "purgeFileSystem"
                    }
                
                    i_dbService.submitJob(
                        l_jsJobName, //e.g. "purgeCustDataMaster" or "purgeFileSystem"
                        new PpasDateTime(i_runDateTimeField.getText()),
                        l_jobParams,
                        false,
                        (String)i_frequencyBox.getSelectedItem(),
                        i_intervalField.getText());
                    
                    displayMessageDialog(i_contentPane, "Job has been submitted");
                    
                    // May have timed out waiting for user to close dialog box, so check still logged in
                    // before accessing database.
                    if (((Boolean)i_context.getObject("LoggedIn")).booleanValue())
                    {
                        repopulateScreenTable();
                    }

                    repopulateJobSubmissionFields();
                    i_jobSubmitted = true;
                    setScreenState();
                }
           
                catch (PpasServiceFailedException l_pSE)
                {
                    handleException(l_pSE);
                }
            }
            else
            {
                displayMessageDialog(i_contentPane, "Job submission cancelled");
            }
        }
    }
    
    /** 
     * Retrieves the archive and recovery properties and sets the screen fields accordingly.
     */ 
    private void setRecoveryFlags()
    {
        Object l_archiveFlag = null;
        Object l_recoveryFlag = null;
    
        if (i_jobTypeComboBox.getSelectedItem().equals(C_PA_JOB_TYPE_CUSTOMER_DATA))
        {
            l_archiveFlag = i_context.getObject("ascs.boi.archiveFlag");
            l_recoveryFlag = i_context.getObject("ascs.boi.recoveryFlag");
        
            // If no value found for archiveFlag, default to "Y"
            if (l_archiveFlag == null || ((String)l_archiveFlag).equals("null") || 
                ((String)l_archiveFlag).equals(""))
            {
                l_archiveFlag = "Y";
            }
            
            // If no value found for recoveryFlag, default to "N"
            if (l_recoveryFlag == null || ((String)l_recoveryFlag).equals("null") || 
                ((String)l_recoveryFlag).equals(""))
            {
                l_recoveryFlag = "N";
            }
            
            i_archiveCheckBox.setSelected(((String)l_archiveFlag).equals("Y"));
            i_archiveCheckBox.setEnabled(true);
            i_archiveCheckBox.setVisible(true);
            i_recoveryCheckBox.setSelected(((String)l_recoveryFlag).equals("Y"));
            i_recoveryCheckBox.setEnabled(true);
            i_recoveryCheckBox.setVisible(true);
        }
        else
        {
            i_archiveCheckBox.setSelected(false);
            i_archiveCheckBox.setVisible(false);
            i_recoveryCheckBox.setSelected(false);
            i_recoveryCheckBox.setVisible(false);
        }
    }
    
    /**
     * Sets the state of the screen based on the selection status of the 
     * Job Name combo boxes, and on whether a job of this type 
     * has been submitted since it was selected.  The state will be used to 
     * determine whether to warn the user when leaving the screen.
     */
    private void setScreenState()
    {
        if (i_jobNameComboBox.getSelectedItem().toString().equals(""))
        {
            i_state = C_STATE_INITIAL;
        }
        else if (i_jobSubmitted)
        {
            i_state = C_STATE_DATA_RETRIEVED;
        }
        else
        {
            // Use NEW RECORD state to indicate that the user should be asked to 
            // confirm screen exit as a job has been selected but not submitted.
            i_state = C_STATE_NEW_RECORD;
        }
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to enable submit processing to be performed in a separate thread.
     */
    public class SubmitRunnable implements Runnable
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------
        
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Only do this if focus was successfully transferred to the submit button.
            // If it is not the focus owner, this would indicate there was a parse
            // error with the previous focus owner.
            if (i_submitButton.isFocusOwner())
            {
                doSubmit();
            }
        }
    }
}
