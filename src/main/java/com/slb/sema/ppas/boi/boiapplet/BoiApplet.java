////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiApplet.java
//    DATE            :       24-Sep-2003
//    AUTHOR          :       M I Erskine
//    REFERENCE       :       PRD_ASCS00_DEV_SS_88
//
//    COPYRIGHT       :       Atos Origin 2003
//
//    DESCRIPTION     :       Java applet for the BOI application.
//                      
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 13/03/07 | S James    | Changes made to support Reports | PpacLon#2995/10671
//          |            | redesign                        |
//----------+------------+---------------------------------+--------------------
// 20/06/07 | E Dangoor  | Add job codes to the context    | PpacLon#3163/11746
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boiapplet;

import java.net.URL;

import javax.swing.SwingUtilities;

import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.boi.gui.BoiPane;
import com.slb.sema.ppas.common.web.applet.AscsApplet;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Java applet for the BOI application.
 */
public class BoiApplet extends AscsApplet
{
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "BoiApplet";

    /** The context object specifically for BOI. */ 
    private BoiContext          i_context;
    
    /** The pane to contain the emtire BOI application display. */
    private BoiPane  i_businessConfigPane;
    
    /**
     * Constructor for BoiApplet.
     */
    public BoiApplet()
    {
        super();
    }
    
    /**
     * Performs BOI applet initialisation.
     */
    public void doInit()
    {
        String              l_param;
        URL                 l_appletUrl;
        String              l_serverHost;

        if(PpasDebug.on) PpasDebug.print
            (C_CLASS_NAME,
             10000,
             "doInit called.");

        i_context = new BoiContext();
        
        l_appletUrl = getCodeBase();
        if(PpasDebug.on) PpasDebug.print
            (C_CLASS_NAME,
             10010,
             "Codebase is " + l_appletUrl);

        l_serverHost = l_appletUrl.getHost();
        if(l_serverHost == null || "".equals(l_serverHost))
        {
            l_serverHost = "localhost";
            if(PpasDebug.on) PpasDebug.print
                (C_CLASS_NAME,
                 10020,
                 "No server host, \"using localhost\"");
        }
        if(PpasDebug.on) PpasDebug.print
            (C_CLASS_NAME,
             10030,
             "Server host is " + l_serverHost);
        i_context.addObject("applet.server.host", l_serverHost);

        l_param = Integer.toString(l_appletUrl.getPort());
        if(PpasDebug.on) PpasDebug.print
            (C_CLASS_NAME,
             10040,
             "Server port is " + l_param);
        i_context.addObject("applet.server.login.port", l_param);
        
        i_context.addObject(
            "ascs.boi.availablescreens", getParameter("ascs.boi.availableBatchScreens"));
        i_context.addObject(
            "ascs.boi.availablePurgeJobTypes", getParameter("ascs.boi.availablePurgeJobTypes"));
        i_context.addObject(
            "ascs.boi.availableCustDataJobs", getParameter("ascs.boi.availableCustDataJobs"));        
        i_context.addObject(
            "ascs.boi.availableApplDataJobs", getParameter("ascs.boi.availableApplDataJobs"));
        i_context.addObject(
            "ascs.boi.fileSystemJobTypes", getParameter("ascs.boi.fileSystemJobTypes"));
        i_context.addObject(
            "ascs.NodeName", getParameter("ascs.NodeName"));
        i_context.addObject(
            "ascs.boi.archiveFlag", getParameter("ascs.boi.archiveFlag"));
        i_context.addObject(
            "ascs.boi.recoveryFlag", getParameter("ascs.boi.recoveryFlag"));
        i_context.addObject(
            "ascs.boi.availableReports", getParameter("ascs.boi.availableReports"));
        i_context.addObject(
            "ascs.boi.jobCodes", getParameter("ascs.boi.jobCodes"));
        i_context.addObject(
            "ascs.boi.custDataRetentionPeriods", getParameter("ascs.boi.custDataRetentionPeriods"));
        i_context.addObject(
            "ascs.boi.fileRetentionPeriods", getParameter("ascs.boi.fileRetentionPeriods"));
        i_context.addObject(
            "ascs.boi.retentionPeriod", getParameter("ascs.boi.retentionPeriod"));
        i_context.addObject(
            "ascs.boi.maxNumberRange", getParameter("ascs.boi.maxNumberRange"));
        i_context.addObject(
            "ascs.boi.inactivityTimeout", getParameter("ascs.boi.inactivityTimeout"));
        i_context.addObject(
            "ascs.boi.tubsElements", getParameter("ascs.boi.tubsElements"));
        
        i_context.addObject("ascs.boi.appletContext", getAppletContext());
        
        i_businessConfigPane = new BoiPane(i_context);
        i_businessConfigPane.getContentPane().setVisible(true);
        
        i_contentPane.add(i_businessConfigPane.getContentPane());

        i_businessConfigPane.resetScreenUponEntry();
        SwingUtilities.invokeLater(new FocusRunnable());
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to enable login screen to be focussed after all other events
     * have been processed.
     */
    public class FocusRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            i_businessConfigPane.defaultFocus();
        }
    }
} 
