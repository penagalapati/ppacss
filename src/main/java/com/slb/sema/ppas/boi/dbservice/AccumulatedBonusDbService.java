////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AccumulatedBonusDbService.java
//  DATE            :       5th July 2007
//  AUTHOR          :       Steven James
//  REFERENCE       :       PpacLon#3183/11754
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       Database service to serve the BOI Accumulated Bonus 
//                          pane.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE    | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
// dd/mm/yy| <name>     | <brief description of change>   | PpacLon#XXXX/YYYYY
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.dbservice;

import com.slb.sema.ppas.boi.boidata.BoiAccumulatedBonusSchemeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BonaBonusAccumParamsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database service to serve the BOI Bonus pane.
 */
public class AccumulatedBonusDbService extends BonusDbService
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------
    /** Used by middleware. */
    private static final String C_CLASS_NAME = "AccumulatedBonusDbService";
    
    //-------------------------------------------------------------------------
    // Class Attributes
    //-------------------------------------------------------------------------
    
    /** Set to true to turn on debug, or false to turn it off. */
    private static boolean c_debug = false;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for accumulated bonus schemes. */
    private BonaBonusAccumParamsSqlService  i_bonaSqlService = null;
    

    /** Current accumulated bonus scheme data being worked on. */
    private BoiAccumulatedBonusSchemeData         i_boiBonaData = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public AccumulatedBonusDbService(BoiContext p_context)
    {
        super(p_context);
      
        i_bonaSqlService                = new BonaBonusAccumParamsSqlService(null);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
    * Return Accumulated Bonus Scheme data currently being worked on. 
    * @return  Accumulated Bonus Scheme data object.
    */
   public BoiAccumulatedBonusSchemeData getAccumulatedBonusSchemeData()
   {
       return i_boiBonaData;
   }

   /** 
    * Set accumulated bonus scheme data currently being worked on.
    * @param p_accumulatedBonusSchemeData Accumulated bonus scheme data object.
    */
   public void setBonusSchemeData(BoiAccumulatedBonusSchemeData p_accumulatedBonusSchemeData)
   {
       i_boiBonaData = p_accumulatedBonusSchemeData;
       if (c_debug)
       {
           System.out.println("Entered setBonusSchemeData: " + 
                              (i_boiBonaData == null ? "null" : i_boiBonaData.getInternalBonusSchemeData().toString()));
       }
   }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusDbService.read()");
        super.read(p_connection);
        
        BonaBonusAccumParamsDataSet l_bonaDataSet = null;
        l_bonaDataSet = i_bonaSqlService.readAll(null, p_connection);

        i_boiBonaData = new BoiAccumulatedBonusSchemeData(l_bonaDataSet.getRecord(i_currentBonusScheme));
        
        if (c_debug) System.out.println("Leaving AccumulatedBonusDbService.read()");
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusDbService.readInitial");
        super.readInitial(p_connection);        
        refreshBonusSchemesDataVector(p_connection, C_ACCUMULATED_SCHEME_TYPE);
        
        if (c_debug) System.out.println("Leaving AccumulatedBonusDbService.readInitial");
    }

    /**
     * Updates bonus schemes in the database.
     * @param p_connection Database connection.
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        super.update(p_connection);
        i_bonaSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_currentBonusScheme,                                
                                i_boiBonaData.getInternalBonusSchemeData().getBonusPercent(),
                                i_boiBonaData.getInternalBonusSchemeData().getAdjCode(),
                                i_boiBonaData.getInternalBonusSchemeData().getAdjType(),
                                i_boiBonaData.getInternalBonusSchemeData().getDedAccId(),
                                i_boiBonaData.getInternalBonusSchemeData().getRefillValueTarget(),
                                i_boiBonaData.getInternalBonusSchemeData().getRefillCountTarget(),
                                i_boiBonaData.getInternalBonusSchemeData().getCountingPeriodDays(),
                                i_boiBonaData.getInternalBonusSchemeData().getBonusAmount(),
                                i_boiBonaData.getInternalBonusSchemeData().getDedAccExpiryDays(),
                                i_boiBonaData.getInternalBonusSchemeData().isSetOfDedAccBalRequired()?'Y':'N'
                                );

        refreshBonusSchemesDataVector(p_connection, C_ACCUMULATED_SCHEME_TYPE);
        refreshServiceOfferingVector(p_connection);
    }
    
    /**
     * Inserts bonus schemes into the database.
     * @param p_connection Database connection.
     * @throws PpasSqlException
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusDbService.insert");
        super.insert(p_connection);
        i_bonaSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_currentBonusScheme,                                
                                i_boiBonaData.getInternalBonusSchemeData().getBonusPercent(),
                                i_boiBonaData.getInternalBonusSchemeData().getAdjCode(),
                                i_boiBonaData.getInternalBonusSchemeData().getAdjType(),
                                i_boiBonaData.getInternalBonusSchemeData().getDedAccId(),
                                i_boiBonaData.getInternalBonusSchemeData().getRefillValueTarget(),
                                i_boiBonaData.getInternalBonusSchemeData().getRefillCountTarget(),
                                i_boiBonaData.getInternalBonusSchemeData().getCountingPeriodDays(),
                                i_boiBonaData.getInternalBonusSchemeData().getBonusAmount(),
                                i_boiBonaData.getInternalBonusSchemeData().getDedAccExpiryDays(),
                                i_boiBonaData.getInternalBonusSchemeData().isSetOfDedAccBalRequired()?'Y':'N'
                                );
        
        refreshBonusSchemesDataVector(p_connection, C_ACCUMULATED_SCHEME_TYPE);
        refreshServiceOfferingVector(p_connection);
        if (c_debug) System.out.println("Leaving AccumulatedBonusDbService.insert");
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

}
