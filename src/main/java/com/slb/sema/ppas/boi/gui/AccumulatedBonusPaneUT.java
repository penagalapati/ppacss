////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatedBonusPaneUT.java
//      DATE            :       10-Aug-2007
//      AUTHOR          :       Steven James
//      REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       JUnit test class for AccumulatedBonusPane.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description of change>         | PpacLon#XXXX/YYYY
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiBonusSchemeData;
import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for AccountGroupsPane. */
public class AccumulatedBonusPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AccumulatedBonusPaneUT";
    
    /** Bonus scheme start date. */
    private static final String C_START_DATE = "07-Jan-2007";
    
    /** Bonus scheme end date. */
    private static final String C_END_DATE = "31-Dec-2010";
    
    /** Used by tests to select a particular service offering. */
    private static final BoiServiceOfferingData C_OPTIN_SERVICEOFFERING = 
                                                    new BoiServiceOfferingData(
                                                            new SeofServiceOfferingData(
                                                                    null,
                                                                    3,
                                                                    "Dummy service offering",
                                                                    C_BOI_OPID,
                                                                    null));

    /** Bonus amount. */
    private static final String C_BONUS_AMOUNT = "10.00";

    /** Dedicated Account zero. */
    private static final String C_ACCOUNT_ONE = "1";

    /** Refill target. */
    private static final String C_REFILL_TARGET = "10.00";
    
    

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Bonus screen. */
    protected AccumulatedBonusPane i_bonsPane;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Key element data combo box. */
    protected JComboBox i_keyElementDataCombo;
    
    /** Bonus scheme id code field. */
    protected ValidatedJTextField i_codeField;
    
    /** Bonus scheme description field. */
    private ValidatedJTextField i_descriptionField;
    
    /** Bonus scheme start date field. */
    private JFormattedTextField i_startDateField;
    
    /** Bonus scheme end date field. */
    private JFormattedTextField i_endDateField;
    
    /** Opt-in service offering combo box. */
    private JComboBox i_serviceOfferingsCombo;
    
    /** Contains the bonus. Note that the same field is used for both amount and percentage. */
    protected JFormattedTextField i_bonusAmountOrPercentField;

    /** Radio button to select refill value target. */
    private JRadioButton i_bonusAmountRadioButton;

    /** Combo-box containing account choice. */
    protected JComboBox i_accountChoiceCombo;
    
    /** Text field containing expiry days for bonus element. */
    protected JFormattedTextField i_expiryDaysField;
    
    /** Combo-box containing adjustment types. */
    protected JComboBox i_adjustmentTypesCombo;
    
    /** Combo-box containing adjustment codes. */
    protected JComboBox i_adjustmentCodesCombo;
    
    /** Contains the refill target. Note that the same field is used for both value and count. */
    protected JFormattedTextField i_refillTargetValueOrCountField;
    
    /** Radio button to select refill value target. */
    private JRadioButton i_refillValueTargetRadioButton;

    /** Contains the counting period in days. */
    protected JFormattedTextField i_countingPeriodField;

    
    /** Button for updates and inserts to schemes. */
    protected JButton i_updateButton;
    
    /** Delete button for bonus schemes. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AccumulatedBonusPaneUT(String p_title)
    {
        super(p_title);
        
        c_context.addObject("ascs.boi.tubsElements",
                            "Channel,STRING,10,null,null,null,TopUpType,STRING,10,Denomination,NUMERIC,12,");
        
        i_bonsPane = new AccumulatedBonusPane(c_context);
        super.init(i_bonsPane);
        
        i_bonsPane.resetScreenUponEntry();
        
        // Bonus schemes panel
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "bonusSchemeId");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        
        i_startDateField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "startDateField");
        i_endDateField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "endDateField");
        i_serviceOfferingsCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "serviceOfferingsCombo");

        i_bonusAmountOrPercentField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                         "bonusAmountOrPercentField");
        
        i_bonusAmountRadioButton = (JRadioButton)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                               "bonusAmountRadioButton");
        
        i_accountChoiceCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "accountChoiceField");
        i_expiryDaysField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "expiryDaysField");
        
        i_adjustmentTypesCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "adjustmentTypesCombo");
        i_adjustmentCodesCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "adjustmentCodesCombo");
        
        i_refillTargetValueOrCountField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                             "refillTargetValueOrCountField");
        i_refillValueTargetRadioButton = (JRadioButton)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                     "refillValueTargetRadioButton");
        i_countingPeriodField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                   "countingPeriodField");
        
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
        
        // Bonus elements panel
        i_keyElementDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "keyElementDataCombo");
            
        
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AccumulatedBonusPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Account Groups screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AccumulatedBonusPane test");
        
        BoiBonusSchemeData l_boiBonusSchemeData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_bonsPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        SwingTestCaseTT.setTextComponentValue(i_startDateField, C_START_DATE);
        SwingTestCaseTT.setTextComponentValue(i_endDateField, C_END_DATE);
        SwingTestCaseTT.setComboSelectedItem(i_bonsPane,
                                             i_serviceOfferingsCombo,
                                             C_OPTIN_SERVICEOFFERING);
        SwingTestCaseTT.setTextComponentValue(i_bonusAmountOrPercentField, C_BONUS_AMOUNT);
        SwingTestCaseTT.setRadioButtonValue(i_bonusAmountRadioButton, true);
        SwingTestCaseTT.setComboSelectedItem(i_bonsPane,
                                             i_accountChoiceCombo,
                                             C_ACCOUNT_ONE);
        SwingTestCaseTT.setTextComponentValue(i_expiryDaysField, "99");
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_adjustmentTypesCombo, 1);
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_adjustmentCodesCombo, 1);
        SwingTestCaseTT.setTextComponentValue(i_refillTargetValueOrCountField, C_REFILL_TARGET);
        SwingTestCaseTT.setRadioButtonValue(i_refillValueTargetRadioButton, true);
        SwingTestCaseTT.setTextComponentValue(i_countingPeriodField, "30");
        
        doInsert(i_bonsPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiBonusSchemeData = createBonusSchemeData(C_CODE,
                                                     C_DESCRIPTION,
                                                     new PpasDate(C_START_DATE),
                                                     new PpasDate(C_END_DATE), new Long(1), true);
        SwingTestCaseTT.setKeyComboSelectedItem(i_bonsPane, i_keyDataCombo, l_boiBonusSchemeData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        SwingTestCaseTT.setTextComponentValue(i_countingPeriodField, "40");

        doUpdate(i_bonsPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_bonsPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_bonsPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_bonsPane, i_updateButton);
        
        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteBonsRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteBonsRecord(C_CODE);
        say(":::End Of Test:::");
    }
    
    /**
     * Creates a BoiBonusSchemeData object with the supplied data.
     * @param p_schemeId The bonus scheme represented by this object.
     * @param p_schemeDesc Description of this channel.
     * @param p_startDate Start date of this scheme.
     * @param p_endDate End date of this scheme.
     * @param p_optInServOff The Bonus Opt-in Service Offering.
     * @param p_active boolean to indicate if scheme is active.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    protected BoiBonusSchemeData createBonusSchemeData(String   p_schemeId,
                                                       String   p_schemeDesc,
                                                       PpasDate p_startDate,
                                                       PpasDate p_endDate,
                                                       Long     p_optInServOff,
                                                       boolean  p_active)
    {
        BonsBonusSchemeData l_bonsData = new BonsBonusSchemeData(p_schemeId,
                                                                 'A', // Accumulated scheme type
                                                                 p_schemeDesc,
                                                                 p_startDate,
                                                                 p_endDate,
                                                                 p_optInServOff,
                                                                 p_active,
                                                                 false,
                                                                 C_BOI_OPID,
                                                                 null);
        return new BoiBonusSchemeData(l_bonsData);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteBonsRecord = "deleteBonsRecord";
    
    /**
     * Removes a row from BONS.
     * @param p_bonusSchemeId Account group id.
     */
    protected void deleteBonsRecord(String p_bonusSchemeId)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE from bons_bonus_scheme " +
                           "WHERE bons_scheme_id = {0}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_bonusSchemeId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteBonsRecord);
    }
}
