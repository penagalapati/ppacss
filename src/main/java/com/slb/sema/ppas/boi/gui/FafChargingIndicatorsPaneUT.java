////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       FafChargingIndicatorsPaneUT.java
//    DATE            :       03-Nov-2005
//    AUTHOR          :       Yang Liu
//    REFERENCE       :       PpacLon#1755/7375
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       JUnit test class for FafChargingIndicatorsPane.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction      | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiFafChargingIndicatorsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for FafChargingIndicatorsPane. */
public class FafChargingIndicatorsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String       C_CLASS_NAME = "FafChargingIndicatorsPaneUT";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** FaF Charging Indicators screen. */
    private FafChargingIndicatorsPane i_fafChargingIndPane;

    /** FaF Charging Indicators code field. */
    private JFormattedTextField       i_codeField;

    /** FaF Charging Indicators description field. */
    private ValidatedJTextField       i_descriptionField;

    /** Key data combo box. */
    protected JComboBox               i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton                 i_updateButton;

    /** Delete button. */
    protected JButton                 i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public FafChargingIndicatorsPaneUT(String p_title)
    {
        super(p_title);

        i_fafChargingIndPane = new FafChargingIndicatorsPane(c_context);
        super.init(i_fafChargingIndPane);
//
//        // Set the size of the frame to be a quarter of the total screen.
//
//        super.i_frame.setSize((Toolkit.getDefaultToolkit().getScreenSize().width / 2),
//                              (Toolkit.getDefaultToolkit().getScreenSize().height / 2));

        i_fafChargingIndPane.resetScreenUponEntry();

        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(FafChargingIndicatorsPaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again through
     * the FaF Charging Indicators screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("FafChargingIndicatorsPane test");

        BoiFafChargingIndicatorsData l_boiFafChargingIndData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_fafChargingIndPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_fafChargingIndPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiFafChargingIndData = new BoiFafChargingIndicatorsData(
                                      new FachFafChargingIndData(C_CODE,
                                                                 C_DESCRIPTION,
                                                                 false,
                                                                 C_BOI_OPID,
                                                                 null));     
        SwingTestCaseTT
                .setKeyComboSelectedItem(i_fafChargingIndPane, i_keyDataCombo, l_boiFafChargingIndData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_fafChargingIndPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_fafChargingIndPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_fafChargingIndPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_fafChargingIndPane, i_updateButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteFafChargingIndRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteFafChargingIndRecord(C_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteFafChargingIndRecord = "deleteFafCharingIndRecord";

    /**
     * Removes a row from FACH_FAF_CHARGING_IND.
     * @param p_fafChargingInd FaF Charging Indicators id.
     */
    private void deleteFafChargingIndRecord(String p_fafChargingInd)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = new String("DELETE from fach_faf_charging_ind " + "WHERE fach_charging_ind = {0}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_fafChargingInd);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteFafChargingIndRecord);
    }
}

