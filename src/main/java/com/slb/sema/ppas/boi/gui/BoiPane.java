////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiPane.java
//    DATE            :       24-Sep-2003
//    AUTHOR          :       Marek Vonka
//    REFERENCE       :       PpacLon#104/1162
//                            PRD_ASCS00_DEV_SS_088
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       The main pane for the BOI application.  
//                            Contains the menu pane, config pane and button 
//                            pane.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 11/10/06 | M Erskine  | Dump nested exceptions when     | PpacLon#2590/10164
//          |            | handling exception in ASCS BOI. |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.BorderFactory;

import com.slb.sema.ppas.swing.components.GridLayoutManager;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.ActivityListener;
import com.slb.sema.ppas.swing.events.ApplicationGuiEventListener;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.swing.gui.GuiPane;
import com.slb.sema.ppas.swing.components.LogoutButton;

/**
 * The main pane for the BOI application.
 */
public class BoiPane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    /** Polling period for the timer checking for user timeout. Set to 30 seconds. */
    private static final int C_TIMEOUT_POLL_PERIOD = 30000;
    
    /** Polling period for the timer checking for user timeout. Set to 30 minutes. */
    private static final int C_DEFAULT_TIMEOUT_PERIOD = 1800000;
    
    /** Text to be displayed at the top of each BOI screen. */
    private static final String C_BOI_PANE_TITLE = "ASCS developed by WM-data for Ericsson";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Pane containing button pane and main pane. */
    private JPanel i_topPane;
    
    /** Button pane. */
    private JPanel i_buttonPane;
    
    /** Pane containing menu pane and config pane. */
    private JPanel i_mainPane;

    /** Menu scroll pane. */
    private JScrollPane i_menuSP;
    
    /** Config scroll pane. */
    private JScrollPane i_configSP;

    /** Menu pane. */
    private BoiMenuPane i_bcMenu;
    
    /** Login pane. */
    private BoiLoginPane i_bcLogin;
    
    /** Change password pane. */
    private ChangePasswordPane i_bcChangePassword;

    /** Company label. */
    private JLabel i_companyLabel;
    
    /** Operator label. */
    private JLabel i_operator;
    
    /** Logout button. */
    private LogoutButton i_logoutButton;
    
    /** Logout button. */
    private GuiButton i_helpButton;
    
    /** Spacer for the button panel. */
    private JLabel i_spacer1;
    
    /** Spacer for the button panel. */
    private JLabel i_spacer2;
    
    /** Spacer for the button panel. */
    private JLabel i_spacer3;
    
    /** Spacer for the button panel. */
    private JLabel i_spacer4;
    
    /** Box within button pane.*/
    private Box i_box;
    
    /** Menu border. */
    private Border i_menuBorder;
    
    /** GUI event listener. */
    private ApplicationGuiEventListener i_applicationGEL;
    
    /** Timer for use in timing out after period of inactivity. */
    private Timer i_timeoutTimer;
    
    /** Listener for events from timeout timer. */
    private BoiTimeoutListener i_timeoutListener;
    
    /** Period aof user inactivity after which the BOI client will time out. */
    private long i_timeoutMillis = 0;

    /** Listener for user activity. */
    private ActivityListener i_activityListener;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * Constructs the main pane for the whole BOI application.
     * @param p_context  Context information specific to BOI.
     */
    public BoiPane(BoiContext p_context)
    {
        super(p_context, (Container)null);
        init();
    }
    
    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------

    /**
     * Sets the defaultFocus.
     */
    public void defaultFocus()
    {
        i_bcLogin.defaultFocus();
        i_bcChangePassword.defaultFocus();
    }
    
    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        i_bcLogin.resetScreenUponEntry();
        i_bcChangePassword.resetScreenUponEntry();
    }
    
    /**
     * Returns a reference to the Login button.
     * @return A reference to the Login button.
     */
    public JButton getDefaultButton()
    {
        return(i_bcLogin.getLoginButton());
    }

    /**
     * Method to implement an action if an event has occurred.
     * @param p_event A GuiEvent object.
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        if("LOGGED_IN".equals(p_event.getName()))
        {
            setConfigPane((Container)null);
            i_bcMenu.setCurrentPane(null);
            setButtonPane(true);

            i_context.addObject("LoggedIn", new Boolean(true));
            i_timeoutTimer.start();
        }
        else if("LOGGED_OUT".equals(p_event.getName()))
        {
            i_timeoutTimer.stop();
            ((BoiContext)i_context).closeConnection();
            i_context.addObject("LoggedIn", new Boolean(false));

            if (i_dialogBox != null)
            {
                i_dialogBox.setValue(new Integer(JOptionPane.CLOSED_OPTION));
            }

            setConfigPane(i_bcLogin.getContentPane());
            setButtonPane(false);
            SwingUtilities.invokeLater(new FocusRunnable());
        }
        else if ("PASSWORD_EXPIRED".equals(p_event.getName()))
        {
            i_bcChangePassword = new ChangePasswordPane((BoiContext)i_context);
            i_bcChangePassword.addGuiEventListener(i_applicationGEL);
            setConfigPane(i_bcChangePassword.getContentPane());
            setButtonPane(false);
            i_context.addObject("LoggedIn", new Boolean(false));
            SwingUtilities.invokeLater(new FocusPasswordRunnable());
        }
    }

    /** 
     * Action event handler.
     * @param p_actionEvent Event to handle.
     */
    public void actionPerformed(ActionEvent p_actionEvent){}
    
    /** 
     * Item event handler.
     * @param p_itemEvent Event to handle.
     */
    public void itemStateChanged(ItemEvent p_itemEvent){}
    
    /** 
     * Key event handler.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        return false;
    }
    
    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event){}
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
 
    /**
     * Sets up the various screen areas for the whole BOI application in
     * addition to creating the various components to occupy these areas
     */
    private void init()
    {
        Container l_configParentContainer;
        String    l_inactivityTimeout;
        BoiHelpListener l_helpListener;
        
        WidgetFactory.init(i_context);
        
        i_activityListener = new ActivityListener(i_context);
        i_timeoutListener = new BoiTimeoutListener();
        i_timeoutTimer = new Timer(C_TIMEOUT_POLL_PERIOD, i_timeoutListener);
        i_timeoutTimer.setRepeats(true);

        l_inactivityTimeout = (String)i_context.getObject("ascs.boi.inactivityTimeout");
        if (l_inactivityTimeout != null && !l_inactivityTimeout.equals("null"))
        {
            i_timeoutMillis = Long.parseLong(l_inactivityTimeout) * 1000;
        }
        else
        {
            i_timeoutMillis = C_DEFAULT_TIMEOUT_PERIOD;
        }
        
        BoiHelpTopics.init();
        l_helpListener = new BoiHelpListener((BoiContext)i_context);
        i_context.addObject("BoiHelpListener", l_helpListener);
        
        i_spacer1 = WidgetFactory.createLabel("  ");
        i_spacer2 = WidgetFactory.createLabel(" ");
        i_spacer3 = WidgetFactory.createLabel("  ");
        i_spacer4 = WidgetFactory.createLabel("  ");
        i_operator = WidgetFactory.createLabel("             ");
        i_companyLabel = WidgetFactory.createLabel(C_BOI_PANE_TITLE,
                                                   WidgetFactory.C_FONT_PLAIN_TEXT_13);

        i_logoutButton = new LogoutButton();
        i_logoutButton.setEnabled(false);
        i_logoutButton.setVisible(false);
        i_logoutButton.addActionListener(new LogoutListener());
        i_logoutButton.setFocusable(false);
        i_logoutButton.setRequestFocusEnabled(false);
        
        i_helpButton = WidgetFactory.createButton("Help", l_helpListener, false);
        i_helpButton.setFocusable(false);

        i_box = Box.createHorizontalBox();
        i_box.setFocusable(false);
        i_box.setRequestFocusEnabled(false);
        i_box.add(i_spacer1);
        i_box.add(i_companyLabel);
        i_box.add(Box.createGlue());
        i_box.add(i_operator);
        i_box.add(i_spacer2);
        i_box.add(i_logoutButton);
        i_box.add(i_spacer3);
        i_box.add(i_helpButton);
        i_box.add(i_spacer4);

        i_buttonPane = new JPanel();
        i_buttonPane.setBackground(WidgetFactory.C_COLOUR_BUTTONPANEL_BACKGROUND);
        i_buttonPane.setLayout(new BoxLayout(i_buttonPane, BoxLayout.Y_AXIS));
        i_buttonPane.setFocusable(false);
        i_buttonPane.setRequestFocusEnabled(false);        
        i_buttonPane.add(Box.createRigidArea(new Dimension(0, 5)));
        i_buttonPane.add(i_box);
        i_buttonPane.add(Box.createRigidArea(new Dimension(0, 5)));

        i_bcMenu = new BoiMenuPane((BoiContext)i_context);
        i_bcLogin = new BoiLoginPane((BoiContext)i_context);
        i_bcChangePassword = new ChangePasswordPane((BoiContext)i_context);

        i_menuSP = new JScrollPane(i_bcMenu.getContentPane());
        i_menuBorder = BorderFactory.createEmptyBorder(0,0,0,0);
        i_menuSP.setBorder(i_menuBorder);
        i_menuSP.setFocusable(false);
        i_menuSP.setRequestFocusEnabled(false);
        i_menuSP.getHorizontalScrollBar().setFocusable(false);
        i_menuSP.getHorizontalScrollBar().setRequestFocusEnabled(false);
        i_menuSP.getVerticalScrollBar().setFocusable(false);
        i_menuSP.getVerticalScrollBar().setRequestFocusEnabled(false);

        i_configSP = new JScrollPane(i_bcLogin.getContentPane());
        i_configSP.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        i_configSP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        i_mainPane = new JPanel();
        i_mainPane.setBackground(WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        i_mainPane.setPreferredSize(new Dimension(1016, 675));
        i_mainPane.setLayout(new GridLayoutManager(11, 60, 0, 0));
        i_mainPane.add(i_menuSP, "menuSP,1,1,2,60");
        i_mainPane.add(i_configSP, "configSP,3,2,9,59");
        
        i_topPane = new JPanel();
        i_topPane.setBackground(WidgetFactory.C_COLOUR_BACKGROUND);
        i_topPane.setLayout(new BoxLayout(i_topPane, BoxLayout.Y_AXIS));
        i_topPane.add(i_buttonPane);
        i_topPane.add(i_mainPane);
        
        i_contentPane = i_topPane;
        
        i_context.addObject("ConfigParentContainer", i_configSP.getViewport());
        
        i_applicationGEL = new ApplicationGuiEventListener();
        i_applicationGEL.addGuiEventListener(i_bcMenu);
        i_applicationGEL.addGuiEventListener(i_bcLogin);
        i_applicationGEL.addGuiEventListener(i_bcChangePassword);
        i_applicationGEL.addGuiEventListener(this);

        i_bcMenu.addGuiEventListener(i_applicationGEL);
        i_bcLogin.addGuiEventListener(i_applicationGEL);
        this.addGuiEventListener(i_applicationGEL);
        
        i_bcMenu.getContentPane().addMouseListener(i_activityListener);
        l_configParentContainer = (Container)i_context.getObject("ConfigParentContainer");
        l_configParentContainer.addMouseListener(i_activityListener);
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(i_activityListener);

        try
        {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            SwingUtilities.updateComponentTreeUI(i_contentPane);
        }
        catch (ClassNotFoundException e)
        {
            handleException(e);
        }
        catch (InstantiationException e)
        {
            handleException(e);
        }
        catch (IllegalAccessException e)
        {
            handleException(e);
        }
        catch (UnsupportedLookAndFeelException e)
        {
            handleException(e);
        }
    }
    
    /** 
     * Refreshes the config pane with the latest objects and repaints it.
     * @param p_container The Container object that is to be displayed in the Config pane.
     */
    private void setConfigPane(Container p_container)
    {
        Container l_configParentContainer;

        l_configParentContainer = (Container)i_context.getObject("ConfigParentContainer");
        l_configParentContainer.removeAll();
        l_configParentContainer.add(p_container);
        l_configParentContainer.repaint();
    }
    
    /**
     * Sets the layout of the button panel. If we are logging into BOI, then 
     * the logout <code>GuiButton</code> should be enabled and the operator's username should be 
     * displayed. If logging out, then the logout button should be disabled and the
     * operator's username set to be blank.
     * @param p_login True if this is being called as a result of logging in,
     *                and false if this is for logging out.
     */
    private void setButtonPane(boolean p_login)
    {
        String l_operatorName = null;

        if (p_login)
        {
            l_operatorName = ((BoiContext)i_context).getOperatorUsername();
            i_logoutButton.setEnabled(true);
            i_logoutButton.setVisible(true);
        }
        else
        {
            l_operatorName = "             ";
            i_logoutButton.setEnabled(false);
            i_logoutButton.setVisible(false);
        }
            
        i_operator = WidgetFactory.createLabel(l_operatorName,
                                               WidgetFactory.C_FONT_PLAIN_TEXT_13);
        i_box.removeAll();
        i_box.add(i_spacer1);
        i_box.add(i_companyLabel);
        i_box.add(Box.createGlue());
        i_box.add(i_operator);
        i_box.add(i_spacer2);
        i_box.add(i_logoutButton);
        i_box.add(i_spacer3);
        i_box.add(i_helpButton);
        i_box.add(i_spacer4);
        i_box.validate();

        i_topPane.repaint();
    }
    
    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Logout button listener.
     */
    private class LogoutListener implements ActionListener
    {
        /**
         * Handles events associated with the Log Out button. 
         * @param p_event Event to handle.
         */
        public void actionPerformed(ActionEvent p_event)
        {
            if(p_event.getSource() == i_logoutButton)
            {
                // Ensure focus lost events are processed, and hence screen state is updated,
                // before performing log out processing.
                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

                SwingUtilities.invokeLater(new LogoutRunnable());
            }
        }
    }

    /**
     * Listener for timer events.
     */
    private class BoiTimeoutListener implements ActionListener
    {
        /**
         * Handles timer events for BOI.
         * @param p_event Event to handle.
         */
        public void actionPerformed(ActionEvent p_event)
        {
            Long l_lastEventTime;
            
            if(p_event.getSource() == i_timeoutTimer)
            {
                // Code to read the last event time could be synchronized with the code to 
                // write it, but we don't need to be that accurate. 
                l_lastEventTime = (Long)i_context.getObject("LastEventTime");

                if (l_lastEventTime != null &&
                    System.currentTimeMillis() - l_lastEventTime.longValue() > i_timeoutMillis)
                {
                    displayMessageDialog(i_contentPane,
                                         "BOI session has timed out.  Please log in again.",
                                         false,
                                         false);
                    
                    notifyEvent(new GuiEvent("LOGGED_OUT", this));
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to enable screen to be focussed after other events
     * have been processed.
     */
    public class FocusRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            i_bcLogin.defaultFocus();
        }
    }
    
    /**
     * Class to enable screen to be focussed after other events
     * have been processed.
     */
    public class FocusPasswordRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            i_bcChangePassword.defaultFocus();
        }
    }

    /**
     * Class to enable logout event handler to check current screen status after all
     * focus lost events have been processed.
     */
    private class LogoutRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            FocussedGuiPane l_currentPane;
            int             l_screenState;
            int             l_response;
            boolean         l_confirmLogout = true;
            String          l_confirmMsg;

            l_currentPane = i_bcMenu.getCurrentPane();
            
            if (l_currentPane != null)
            {
                l_screenState = l_currentPane.getState();
                
                if ( l_screenState == GuiPane.C_STATE_NEW_RECORD ||
                     l_screenState == GuiPane.C_STATE_DATA_MODIFIED )
                {
                    if (l_currentPane instanceof BusinessConfigBasePane)
                    {
                        l_confirmMsg = "Unsaved modifications will be lost. " +
                                       "Are you sure you wish to log out?";
                    }
                    else
                    {
                        l_confirmMsg = "The selected job has not been submitted. " +
                                       "Are you sure you wish to log out?";
                    }
                    
                    l_response = displayConfirmDialog(l_currentPane.getContentPane(),
                                                      l_confirmMsg);
                    
                    if (l_response != JOptionPane.YES_OPTION)
                    {
                        l_confirmLogout = false;
                    }
                }
            }
            
            if (l_confirmLogout)
            {
                notifyEvent(new GuiEvent("LOGGED_OUT", this));
            }
        }
    }
}
