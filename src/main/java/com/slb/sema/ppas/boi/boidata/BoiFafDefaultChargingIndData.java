////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiFafDefaultChargingIndData.java
//      DATE            :       16-Nov-05
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PpacLon#1838/7448
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Wrapper for DeciDefaultChargingIndData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndData;
import com.slb.sema.ppas.common.dataclass.DataObject;

/** Wrapper for faf charging indicators class within BOI. */
public class BoiFafDefaultChargingIndData extends DataObject
{
    //-------------------------------------------------------------------------
    // Private Instance Attributes
    //-------------------------------------------------------------------------    
        
    /** Faf Charging Indicators data object */
    private DeciDefaultChargingIndData i_deciChargIndData;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_deciChargIndData faf Charging Indicators data object to wrapper.
     */
    public BoiFafDefaultChargingIndData(DeciDefaultChargingIndData p_deciChargIndData)
    {
        i_deciChargIndData = p_deciChargIndData;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Return Faf Charging Indicators wrappered data object.
     * @return Wrappered Faf Charging Indicators data object.
     */
    public DeciDefaultChargingIndData getInternalDeciDefaultChargingIndData()
    {
        return i_deciChargIndData;
    }

    /**
     * Compares two fach faf charging indicators data objects and returns true if they equate.
     * @param p_deciChargIndData faf charging Indicators data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_deciChargIndData)
    {
        boolean l_return = false;

        if (p_deciChargIndData != null
                && p_deciChargIndData instanceof BoiFafDefaultChargingIndData
                && i_deciChargIndData.getNumberStart().equals
                                (((BoiFafDefaultChargingIndData)p_deciChargIndData)
                                .getInternalDeciDefaultChargingIndData().getNumberStart()))
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns faf charging indicators data object as a String for display in BOI.
     * @return Faf Charging Indicators data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_deciChargIndData.getNumberStart() + " - "
                                            + i_deciChargIndData.getNumberEnd());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_deciChargIndData.getChargingInd().length();
    }
}