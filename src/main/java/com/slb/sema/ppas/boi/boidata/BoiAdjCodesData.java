////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiAdjCodesData.java
//      DATE            :       17-Jun-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1573/6627
//                              PRD_ASCS00_GEN_CA_49
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Wrapper for SrvaAdjCodesData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;

/** Wrapper for SrvaAdjCodesData class within BOI. */
public class BoiAdjCodesData extends DataObject
{
    /** Wrappered data object */
    private SrvaAdjCodesData i_adjCodesData;
    
    /**
     * Simple constructor.
     * @param p_adjCodesData Adjustment code data object to wrapper.
     */
    public BoiAdjCodesData(SrvaAdjCodesData p_adjCodesData)
    {
        i_adjCodesData = p_adjCodesData;
    }
    
    /**
     * Return wrappered adjustment code data object.
     * @return Wrappered adjustment code data object.
     */
    public SrvaAdjCodesData getInternalAdjCodesData()
    {
        return i_adjCodesData;
    }
    
    /**
     * Compares two adjustment code data objects and returns true if they equate.
     * @param p_adjCodesData Adjustment code data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_adjCodesData)
    {
        boolean          l_return = false;
        SrvaAdjCodesData l_adjCodesData = null;
        
        if (p_adjCodesData != null &&
            p_adjCodesData instanceof BoiAdjCodesData)
        {
            l_adjCodesData = ((BoiAdjCodesData)p_adjCodesData).getInternalAdjCodesData();
            
            if (i_adjCodesData.getAdjType().equals(l_adjCodesData.getAdjType()) &&
                i_adjCodesData.getAdjCode().equals(l_adjCodesData.getAdjCode()))
            {
                l_return = true;
            }
        }
        return l_return;
    }
    
    /**
     * Returns adjustment code data object as a String for display in BOI.
     * @return Adjustment code data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_adjCodesData.getAdjType() + 
                                            " - " + i_adjCodesData.getAdjCode());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_adjCodesData.getAdjCode().length();
    }
}