////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatorsPaneUT.java
//      DATE            :       14-Jan-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#1148/5382
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       JUnit test class for AccumulatorsPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiAccumulatorData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for AccumulatorsPane. */
public class AccumulatorsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AccumulatorsPaneUT";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Accumulators screen. */
    private AccumulatorsPane             i_accfPane;
    
    /** Accumulators code field. */
    private JFormattedTextField          i_codeField;
    
    /** Accumulators description field. */
    private ValidatedJTextField          i_descriptionField;
    
    /** Accumulators Units field. */
    private ValidatedJTextField          i_unitsField;
    
    /** market combo box. */
    private JComboBox                    i_marketDataComboBox;
    
    /** service class combo box. */
    private JComboBox                    i_serviceClassDataComboBox;
   
    /** Constant defining a single accumulator id. */
    private static final String          C_ACCF_CODE          = "5";
    
    /** Constant defining a single accumulator description. */
    private static final String          C_ACCF_DESC          = "5th Accumulator";
    
    /** Constant defining a single accumulator's units. */
    private static final String          C_ACCF_UNITS         = "Units";
    
    /** Constant defining a Market for testing. */
    private static final BoiMarket       C_ACCF_MARKET        = new BoiMarket(new Market(2, 3));
    
    /** Constant defining a Service Class for testing. */
    private static final BoiMiscCodeData C_ACCF_SERVICE_CLASS = new BoiMiscCodeData(
                                                                    new MiscCodeData(
                                                                        C_ACCF_MARKET.getMarket(),
                                                                        "CLS",
                                                                        "1",
                                                                        "Personal (2,3)",
                                                                        " "));
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AccumulatorsPaneUT(String p_title)
    {
        super(p_title);
        
        i_accfPane = new AccumulatorsPane(c_context);
        super.init(i_accfPane);
        
        i_accfPane.resetScreenUponEntry();

        i_marketDataComboBox       = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "marketDataComboBox");
        i_serviceClassDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "serviceClassDataComboBox");
        i_keyDataCombo             = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "definedCodesBox");
        i_codeField                = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "codeField");
        i_descriptionField         = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "descriptionField");
        
        i_unitsField               = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                        "unitsField");
        
        i_updateButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AccumulatorsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Accumulators screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AccumulatorsPane test");
        
        BoiAccumulatorData l_boiAccfData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        SwingTestCaseTT.setMarketComboSelectedItem(i_accfPane, 
                                                   i_marketDataComboBox, 
                                                   C_ACCF_MARKET);
        
        SwingTestCaseTT.setMarketComboSelectedItem(i_accfPane, 
                                                   i_serviceClassDataComboBox, 
                                                   C_ACCF_SERVICE_CLASS);

        SwingTestCaseTT.setKeyComboSelectedItem(i_accfPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_ACCF_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_ACCF_DESC);
        SwingTestCaseTT.setTextComponentValue(i_unitsField, C_ACCF_UNITS);
        doInsert(i_accfPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiAccfData = createAccumulatorData(C_ACCF_MARKET, 
                                              C_ACCF_SERVICE_CLASS, 
                                              C_ACCF_CODE, 
                                              C_ACCF_DESC,
                                              C_ACCF_UNITS);
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_accfPane, i_keyDataCombo, l_boiAccfData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_accfPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_accfPane, i_deleteButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteAccfRecord(C_ACCF_MARKET, C_ACCF_SERVICE_CLASS, C_ACCF_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteAccfRecord(C_ACCF_MARKET, C_ACCF_SERVICE_CLASS, C_ACCF_CODE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiAccumulatorData object with the supplied data.
     * @param p_market Accumulators market.
     * @param p_serviceClass Dedicated Acounts Service Class.
     * @param p_accfId Accumulator id.
     * @param p_accfDesc Accumulator description.
     * @param p_unitsDesc Units description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiAccumulatorData createAccumulatorData(
        BoiMarket         p_market,
        BoiMiscCodeData   p_serviceClass,
        String            p_accfId,
        String            p_accfDesc,
        String            p_unitsDesc)
    {
        AccfAccumulatorData  l_accfData;
        String               l_code;
        
        l_code = p_serviceClass.getInternalMiscCodeData().getCode();
        
        l_accfData = new AccfAccumulatorData(null, // p_request
                                             p_market.getMarket(),
                                             new ServiceClass(Integer.parseInt(l_code)),
                                             Integer.parseInt(p_accfId),
                                             p_accfDesc,
                                             p_unitsDesc);
        
        return new BoiAccumulatorData(l_accfData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteAccfRecord = "deleteAccfRecord";
    
    /**
     * Removes a row from accf_accumulator_configuration.
     * @param p_market
     * @param p_serviceClass
     * @param p_accumulatorId Accumulator id.
     */
    private void deleteAccfRecord(
        BoiMarket       p_market,
        BoiMiscCodeData p_serviceClass,
        String          p_accumulatorId)
    {
        String           l_sql;
        SqlString        l_sqlString;
        
        l_sql = "DELETE from accf_accumulator_configuration " +
                           "WHERE accf_srva          = {0} " +
                           "AND   accf_sloc          = {1} " +
                           "AND   accf_service_class = {2} " +
                           "AND   accf_id            = {3} ";
        
        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setIntParam     (0, p_market.getMarket().getSrva());
        l_sqlString.setIntParam     (1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam  (2, p_serviceClass.getInternalMiscCodeData().getCode());
        l_sqlString.setIntParam     (3, p_accumulatorId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteAccfRecord);
    }
}
