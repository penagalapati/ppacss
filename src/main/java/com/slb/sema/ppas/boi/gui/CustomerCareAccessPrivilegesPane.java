////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CustomerCareAccessPrivilegesPane.java
//      DATE            :       27-May-2004
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/2508
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Screen to allow a BMO to create, amend or delete
//                              privilege IDs and assign or remove permission for
//                              different types of CSR to access individual
//                              Customer Care screens or specific functions within
//                              those screens.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/10/05 | K Goswami  | Added checkbox for Temporary    | PpacLon#463
//          |            | Blocking Privilege              |
//----------+------------+---------------------------------+--------------------
// 12/12/05 | Paul Rosser| Added checkbox for voucher      | PpacLon#1888/7595
//          |            | history enquiry and break screen| PRD_ASCS00_GEN_CA_039
//----------+------------+---------------------------------+---------------------
// 15/12/05 |K Goswami   | Changed code to allow voucher   | PpaLon#1892/7603
//          |            | history enquiry privilege       | CA#39
//----------+------------+---------------------------------+--------------------
// 12/12/05 | M Erskine  | Add a function access check box | PpacLon#1882/7556
//          |            | for a CSO to override the Msisdn| PRD_ASCS00_GEN_CA_65
//          |            | quarantine period. Note that    |
//          |            | the functionality this enables  |
//          |            | is feature licensed.            |
//----------+------------+---------------------------------+--------------------
// 20/03/06 | Ian James  | Operator access privilege for   | PpacLon#2055/8226
//          |            | Call History screen.            | PRD_ASCS00_GEN_CA_66
//----------+------------+---------------------------------+--------------------
// 11/07/06 | Chris      | Added All & None buttons.       | PpacLon#1898/9347
//          | Harrison   | Grouped checkboxes, removed,    |
//          |            | renamed some. Added Function    |
//          |            |'highlighting'.                  |
//----------+------------+---------------------------------+-------------------
// 24/04/07 | S James    | Add Single Step Install changes | PpacLon#3072/11370
//          |            | & Account Reconnection changes  |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import java.util.Vector;
import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.slb.sema.ppas.boi.dbservice.CustCareAccessDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Service Class Details Screen class.
 */
public class CustomerCareAccessPrivilegesPane extends BusinessConfigBasePane
{
    /** Contains fields to hold the privilege id and the associated description. */
    private JPanel i_topPanel = null;
    
    /** Contains check boxes indicating the screen access privileges for the selected privilege level. */
    private JPanel i_screenAccessPanel = null;
    
    /** Contains check boxes indicating the function access privileges for the selected privilege level. */
    private JPanel i_functionAccessPanel = null;
    
    /** Vector containing the available privilege level data objects. */
    private Vector i_availablePrivilegeDataV = null;

    /** Privilege level code field. */
    private JFormattedTextField i_privilegeLevelField = null;

    /** Privilege level description field. */
    private ValidatedJTextField i_descriptionField = null;

    private SelectionButtonListener i_selectionButtonListener = null;
    
    private CheckBoxListener i_checkBoxListener = null;

    /** Button to set All Screen Access checkboxes ON */
    private GuiButton i_screenAccessAllButton = null;

    /** Button to set All Screen Access checkboxes OFF */
    private GuiButton i_screenAccessNoneButton = null;

    /** Button to set All Function Access checkboxes ON */
    private GuiButton i_functionAccessAllButton = null;

    /** Button to set All Function Access checkboxes OFF */
    private GuiButton i_functionAccessNoneButton = null;
    
    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_accountDetailsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_newSubscriptionBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_subscriberDetailsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_vouchersBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_paymentsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_adjustmentsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_adjustmentApprovalBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_promotionsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_disconnectionBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_subordinatesBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_additionalInformationBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_writeMemoBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_dedicatedAccountsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_accumulatorsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_familyAndFriendsBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_subscriberSegmentationBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_communityChargingBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_routingBox = null;
   
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_chargedEventCountersBox = null;
   
    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_serviceFeeDeductionsBox = null;
   
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateExpiryDatesBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_changeServiceClassBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_changeLanguageBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateAgentOrSubAgentBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_refillUnbarBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateSubscriberDefaultsBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_standardVoucherRechargeBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_voucherUpdateBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_voucherEnquiryBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_makePaymentBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_makeAdjustmentBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_modifyPromotionPlanAllocsBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_addSubordinatesBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateAdditionalInformationBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_makeDedicatedAccountAdjustmentBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_modifyFamilyAndFriendsNumbersBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateAccumulatorsBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateSubscriberSegmentationBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateCommunityChargingBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateUssdEocnIdBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateIvrPinBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateEventCountersBox = null;

    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_updateHomeRegionBox = null;
    
    /** Check box for operator temporary block update privilege. */
    private JCheckBox i_updateTempBlockBox = null;
    
    /** Check box to allow operator to view  voucher history. */
    private JCheckBox i_voucherHistoryEnquiryBox = null;
    
    /** Check box for operator GUI access update privilege. */
    private JCheckBox i_overrideMsisdnQuarantinePeriodBox = null;

    /** Check box to allow operator to perform single step install. */
    private JCheckBox i_singleStepInstallBox = null;

    /** Check box for operator GUI access screen privilege. */
    private JCheckBox i_callHistoryBox = null;
    
    /** Check box to allow operator to perform Account Reconnection. */
    private JCheckBox i_accountReconnectionBox = null;
    
    /** Each Screen Access JCheckBox may have a related array of
     * Function Access JCheckBoxes. This HashMap stores these relations. 
     */
    private HashMap i_screenFunctionsHashMap = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * Simple Constructor.
     * @param p_context A reference to the BoiContext
     */
    public CustomerCareAccessPrivilegesPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new CustCareAccessDbService((BoiContext)i_context);

        i_selectionButtonListener = new SelectionButtonListener();
        i_checkBoxListener = new CheckBoxListener();

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles keyboard events for this screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;
        boolean l_selected = false;
        boolean l_setSelected = false;
        int     l_keyEvent = p_keyEvent.getKeyCode();
        Object  l_source = p_keyEvent.getSource();
                    
        if ( (i_contentPane.isAncestorOf(p_keyEvent.getComponent())) &&
             (p_keyEvent.getID() == KeyEvent.KEY_PRESSED) &&
             ((l_keyEvent == KeyEvent.VK_ENTER) || 
              (l_keyEvent == KeyEvent.VK_SPACE)))
        {
            if (l_source == i_deleteButton)
            {
                i_deleteButton.doClick();
            }
            else if (l_source == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else if ( (l_source == i_updateButton) ||
                      (l_source == i_keyDataComboBox) ||
                      (l_source == i_privilegeLevelField) ||
                      (l_source == i_descriptionField) )
            {
                i_updateButton.doClick();
            }
            else if (l_source == i_screenAccessAllButton)
            {
                i_screenAccessAllButton.doClick();
            }
            else if (l_source == i_screenAccessNoneButton)
            {
                i_screenAccessNoneButton.doClick();
            }
            else if (l_source == i_functionAccessAllButton)
            {
                i_functionAccessAllButton.doClick();
            }
            else if (l_source == i_functionAccessNoneButton)
            {
                i_functionAccessNoneButton.doClick();
            }
            else
            {
                // Source must be a JCheckBox
                JCheckBox l_checkBox = (JCheckBox)p_keyEvent.getSource();

                if (l_checkBox == i_accountDetailsBox)
                {
                    // Force the Account Details Screen to be always ON
                    i_accountDetailsBox.setSelected(true);
                }

                else
                {
                    l_selected = l_checkBox.isSelected();
                    l_setSelected = (l_selected ? false : true); 
                         
                    ((JCheckBox)l_source).setSelected(l_setSelected);
                }
            }
            l_return = true;
        }
        return l_return;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Customer Care Access Privileges", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                         BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_CC_ACCESS_PRIV_SCREEN), 
                                         i_helpComboListener);
        i_helpComboBox.setFocusable(false);
                      
        createTopPanel();
        createScreenAccessPanel();
        createFunctionAccessPanel();

        i_mainPanel.add(i_helpComboBox, "ccAccessPrivHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_topPanel, "topPanel,1,6,100,15");
        i_mainPanel.add(i_screenAccessPanel, "screenAccess,1,22,49,73");
        i_mainPanel.add(i_functionAccessPanel, "functionAccess,51,22,50,73");

        //Add buttons to screen that were created in superclass
        i_mainPanel.add(i_updateButton, "updateButton,1,96,15,5");
        i_mainPanel.add(i_deleteButton, "deleteButton,17,96,15,5");
        i_mainPanel.add(i_resetButton, "resetButton,33,96,15,5");
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_privilegeLevelField.getText().trim().equals(""))
        {
            i_privilegeLevelField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Privilege ID cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }

        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        int l_code = -1;
        
        if (i_privilegeLevelField.getText().trim().equals(""))
        {
            i_privilegeLevelField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Privilege ID cannot be blank");
            return false;
        }
        
        l_code = Integer.parseInt(i_privilegeLevelField.getText());
        
        ((CustCareAccessDbService)i_dbService).setCurrentPrivilegeCode(l_code);
        
        return true;
    }

    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        PrilPrivilegeLevelData    l_privilegeLevelData;
        int                       l_code = -1;
        GopaGuiOperatorAccessData l_gopaData;
        
        l_code = Integer.parseInt(i_privilegeLevelField.getText());
        
        l_privilegeLevelData = new PrilPrivilegeLevelData(null,
                                                          l_code,
                                                          i_descriptionField.getText(),
                                                          "");
                                                       
        ((CustCareAccessDbService)i_dbService).setPrivilegeLevelData(l_privilegeLevelData);

        l_gopaData = new GopaGuiOperatorAccessData(
            null,
            l_code,
            i_newSubscriptionBox.isSelected() ? 'Y' : 'N',
            i_subscriberDetailsBox.isSelected() ? 'Y' : 'N',
            i_vouchersBox.isSelected() ? 'Y' : 'N',
            i_paymentsBox.isSelected() ? 'Y' : 'N',
            i_adjustmentsBox.isSelected() ? 'Y' : 'N',
            i_promotionsBox.isSelected() ? 'Y' : 'N',
            i_disconnectionBox.isSelected() ? 'Y' : 'N',
            i_subordinatesBox.isSelected() ? 'Y' : 'N',
            i_additionalInformationBox.isSelected() ? 'Y' : 'N',
            i_dedicatedAccountsBox.isSelected() ? 'Y' : 'N',
            i_accumulatorsBox.isSelected() ? 'Y' : 'N',
            i_writeMemoBox.isSelected() ? 'Y' : 'N',
            i_familyAndFriendsBox.isSelected() ? 'Y' : 'N',
            i_adjustmentApprovalBox.isSelected() ? 'Y' : 'N',
            i_subscriberSegmentationBox.isSelected() ? 'Y' : 'N',
            i_communityChargingBox.isSelected() ? 'Y' : 'N',
            i_routingBox.isSelected() ? 'Y' : 'N',
            i_chargedEventCountersBox.isSelected() ? 'Y' : 'N',
            i_serviceFeeDeductionsBox.isSelected() ? 'Y' : 'N',
            i_updateExpiryDatesBox.isSelected() ? 'Y' : 'N',
            i_changeServiceClassBox.isSelected() ? 'Y' : 'N',
            i_changeLanguageBox.isSelected() ? 'Y' : 'N',
            i_updateAgentOrSubAgentBox.isSelected() ? 'Y' : 'N',
            i_refillUnbarBox.isSelected() ? 'Y' : 'N',
            'N', // "IN for New Subscriber" is not displayed anymore
            i_updateSubscriberDefaultsBox.isSelected() ? 'Y' : 'N',
            i_standardVoucherRechargeBox.isSelected() ? 'Y' : 'N',
            i_voucherEnquiryBox.isSelected() ? 'Y' : 'N',
            i_makePaymentBox.isSelected() ? 'Y' : 'N',
            i_makeAdjustmentBox.isSelected() ? 'Y' : 'N',
            i_modifyPromotionPlanAllocsBox.isSelected() ? 'Y' : 'N',
            i_addSubordinatesBox.isSelected() ? 'Y' : 'N',
            i_updateAdditionalInformationBox.isSelected() ? 'Y' : 'N',
            i_makeDedicatedAccountAdjustmentBox.isSelected() ? 'Y' : 'N',
            i_modifyFamilyAndFriendsNumbersBox.isSelected() ? 'Y' : 'N',
            i_updateAccumulatorsBox.isSelected() ? 'Y' : 'N',
            i_voucherUpdateBox.isSelected() ? 'Y' : 'N',
            i_updateSubscriberSegmentationBox.isSelected() ? 'Y' : 'N',
            i_updateCommunityChargingBox.isSelected() ? 'Y' : 'N',
            i_updateUssdEocnIdBox.isSelected() ? 'Y' : 'N',
            i_updateIvrPinBox.isSelected() ? 'Y' : 'N',
            i_updateEventCountersBox.isSelected() ? 'Y' : 'N',
            i_updateHomeRegionBox.isSelected() ? 'Y' : 'N',
            i_updateTempBlockBox.isSelected() ? 'Y' : 'N',
            i_voucherHistoryEnquiryBox.isSelected() ? 'Y' : 'N',
            i_overrideMsisdnQuarantinePeriodBox.isSelected() ? 'Y' : 'N',
            i_singleStepInstallBox.isSelected() ? 'Y' : 'N',
            i_callHistoryBox.isSelected() ? 'Y' : 'N',
            i_accountReconnectionBox.isSelected() ? 'Y' : 'N',
            null, // opid
            null ); // last modified

        ((CustCareAccessDbService)i_dbService).setGopaData(l_gopaData);
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        PrilPrivilegeLevelData l_selectedItem = null;
        int l_code = -1;
        
        l_selectedItem = (PrilPrivilegeLevelData)i_keyDataComboBox.getSelectedItem();
        l_code = l_selectedItem.getPrivilegeLevel();
        ((CustCareAccessDbService)i_dbService).setCurrentPrivilegeCode(l_code);
    }

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_privilegeLevelField.setText("");
        i_descriptionField.setText("");
        
        setScreenAccessBoxes(false);
        setFunctionAccessBoxes(false);
    }

    /** 
     * Set all the Screen Access boxes to a given value.
     * @param p_value The value to set.
     */
    protected void setScreenAccessBoxes(boolean p_value)
    {
        i_accountDetailsBox.setSelected(true);      // Always ON
        i_newSubscriptionBox.setSelected(p_value);
        i_subscriberDetailsBox.setSelected(p_value);
        i_vouchersBox.setSelected(p_value);
        i_paymentsBox.setSelected(p_value);
        i_adjustmentsBox.setSelected(p_value);
        i_adjustmentApprovalBox.setSelected(p_value);
        i_promotionsBox.setSelected(p_value);
        i_disconnectionBox.setSelected(p_value);
        i_subordinatesBox.setSelected(p_value);
        i_additionalInformationBox.setSelected(p_value);
        i_writeMemoBox.setSelected(p_value);
        i_dedicatedAccountsBox.setSelected(p_value);
        i_accumulatorsBox.setSelected(p_value);
        i_familyAndFriendsBox.setSelected(p_value);
        i_subscriberSegmentationBox.setSelected(p_value);
        i_communityChargingBox.setSelected(p_value);
        i_routingBox.setSelected(p_value);
        i_chargedEventCountersBox.setSelected(p_value);
        i_serviceFeeDeductionsBox.setSelected(p_value);
        i_callHistoryBox.setSelected(p_value);
    }
    
    /** 
     * Set all the Function Access boxes to a given value.
     * @param p_value The value to set.
     */
    protected void setFunctionAccessBoxes(boolean p_value)
    {
        i_updateExpiryDatesBox.setSelected(p_value);
        i_changeServiceClassBox.setSelected(p_value);
        i_changeLanguageBox.setSelected(p_value);
        i_updateAgentOrSubAgentBox.setSelected(p_value);
        i_refillUnbarBox.setSelected(p_value);
        i_updateSubscriberDefaultsBox.setSelected(p_value);
        i_standardVoucherRechargeBox.setSelected(p_value);
        i_voucherUpdateBox.setSelected(p_value);
        i_voucherEnquiryBox.setSelected(p_value);
        i_makePaymentBox.setSelected(p_value);
        i_makeAdjustmentBox.setSelected(p_value);
        i_modifyPromotionPlanAllocsBox.setSelected(p_value);
        i_addSubordinatesBox.setSelected(p_value);
        i_updateAdditionalInformationBox.setSelected(p_value);
        i_makeDedicatedAccountAdjustmentBox.setSelected(p_value);
        i_modifyFamilyAndFriendsNumbersBox.setSelected(p_value);
        i_updateAccumulatorsBox.setSelected(p_value);
        i_updateSubscriberSegmentationBox.setSelected(p_value);
        i_updateCommunityChargingBox.setSelected(p_value);
        i_updateUssdEocnIdBox.setSelected(p_value);
        i_updateIvrPinBox.setSelected(p_value);
        i_updateEventCountersBox.setSelected(p_value);
        i_updateHomeRegionBox.setSelected(p_value);
        i_updateTempBlockBox.setSelected(p_value);
        i_voucherHistoryEnquiryBox.setSelected(p_value);
        i_overrideMsisdnQuarantinePeriodBox.setSelected(p_value);
        i_singleStepInstallBox.setSelected(p_value);
        i_accountReconnectionBox.setSelected(p_value);
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        GopaGuiOperatorAccessData l_gopaData;
        PrilPrivilegeLevelData    l_privilegeLevelData;
                                                       
        l_privilegeLevelData = ((CustCareAccessDbService)i_dbService).getPrivilegeLevelData();

        i_privilegeLevelField.setValue(new Integer(l_privilegeLevelData.getPrivilegeLevel()));
        i_descriptionField.setText(l_privilegeLevelData.getDescription());

        l_gopaData = ((CustCareAccessDbService)i_dbService).getGopaData();
        
        i_newSubscriptionBox.setSelected(l_gopaData.hasNewSubsScreenAccess());
        i_subscriberDetailsBox.setSelected(l_gopaData.hasSubsDetailsScreenAccess());
        i_vouchersBox.setSelected(l_gopaData.hasVouchersScreenAccess());
        i_paymentsBox.setSelected(l_gopaData.hasPaymentsScreenAccess());
        i_adjustmentsBox.setSelected(l_gopaData.hasAdjustmentsScreenAccess());
        i_adjustmentApprovalBox.setSelected(l_gopaData.hasAdjApprovalScreenAccess());
        i_promotionsBox.setSelected(l_gopaData.hasPromotionsScreenAccess());
        i_disconnectionBox.setSelected(l_gopaData.hasDisconnectScreenAccess());
        i_subordinatesBox.setSelected(l_gopaData.hasSubordinatesScreenAccess());
        i_additionalInformationBox.setSelected(l_gopaData.hasAddInfoScreenAccess());
        i_writeMemoBox.setSelected(l_gopaData.hasCommentsScreenAccess());
        i_dedicatedAccountsBox.setSelected(l_gopaData.hasDedAccountsScreenAccess());
        i_accumulatorsBox.setSelected(l_gopaData.hasAccumulatorsScreenAccess());
        i_familyAndFriendsBox.setSelected(l_gopaData.hasFafScreenAccess());
        i_subscriberSegmentationBox.setSelected(l_gopaData.hasSubscriberSegmentationScreenAccess());
        i_communityChargingBox.setSelected(l_gopaData.hasCommunityChargingScreenAccess());
        i_routingBox.setSelected(l_gopaData.hasRoutingScreenAccess());
        i_chargedEventCountersBox.setSelected(l_gopaData.hasEventCountersScreenAccess());
        i_serviceFeeDeductionsBox.setSelected(l_gopaData.hasServiceFeeScreenAccess());
        i_callHistoryBox.setSelected(l_gopaData.hasCallHistoryScreenAccess());
        
        i_updateExpiryDatesBox.setSelected(l_gopaData.expDatesUpdateIsPermitted());
        i_changeServiceClassBox.setSelected(l_gopaData.servClassChangeIsPermitted());
        i_changeLanguageBox.setSelected(l_gopaData.languageChangeIsPermitted());
        i_updateAgentOrSubAgentBox.setSelected(l_gopaData.agentUpdateIsPermitted());
        i_refillUnbarBox.setSelected(l_gopaData.ivrUnbarIsPermitted());
        i_updateSubscriberDefaultsBox.setSelected(l_gopaData.subsDetailsUpdateIsPermitted());
        i_standardVoucherRechargeBox.setSelected(l_gopaData.accountRechargeIsPermitted());
        i_voucherUpdateBox.setSelected(l_gopaData.voucherUpdateIsPermitted());
        i_voucherEnquiryBox.setSelected(l_gopaData.voucherEnquiryIsPermitted());
        i_makePaymentBox.setSelected(l_gopaData.makePaymentIsPermitted());
        i_makeAdjustmentBox.setSelected(l_gopaData.makeAdjustmentIsPermitted());
        i_modifyPromotionPlanAllocsBox.setSelected(l_gopaData.promotionsUpdateIsPermitted());
        i_addSubordinatesBox.setSelected(l_gopaData.addSubordinateIsPermitted());
        i_updateAdditionalInformationBox.setSelected(l_gopaData.addInfoUpdateIsPermitted());
        i_makeDedicatedAccountAdjustmentBox.setSelected(l_gopaData.makeDedAccAdjustIsPermitted());
        i_modifyFamilyAndFriendsNumbersBox.setSelected(l_gopaData.updateFafListPermitted());
        i_updateAccumulatorsBox.setSelected(l_gopaData.updateAccumIsPermitted());
        i_updateSubscriberSegmentationBox.setSelected(l_gopaData.subscriberSegmentationUpdateIsPermitted());
        i_updateCommunityChargingBox.setSelected(l_gopaData.communityChargingUpdateIsPermitted());
        i_updateUssdEocnIdBox.setSelected(l_gopaData.ussdEocnIdUpdateIsPermitted());
        i_updateIvrPinBox.setSelected(l_gopaData.subscriberPinUpdateIsPermitted());
        i_updateEventCountersBox.setSelected(l_gopaData.eventCountersUpdateIsPermitted());
        i_updateHomeRegionBox.setSelected(l_gopaData.homeRegionUpdateIsPermitted());
        i_updateTempBlockBox.setSelected(l_gopaData.tempBlockUpdateIsPermitted());
        i_voucherHistoryEnquiryBox.setSelected(l_gopaData.voucherHistoryIsPermitted());
        i_overrideMsisdnQuarantinePeriodBox.setSelected(
                                         l_gopaData.overrideMsisdnQuarantinePeriodIsPermitted());
        i_singleStepInstallBox.setSelected(l_gopaData.singleStepInstallIsPermitted());
        i_accountReconnectionBox.setSelected(l_gopaData.isAccountReconnectionPermitted());
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates.
     */
    protected void refreshListData()
    {
        i_availablePrivilegeDataV = ((CustCareAccessDbService)i_dbService).getAvailablePrivileges();
        i_keyDataModel = new DefaultComboBoxModel(i_availablePrivilegeDataV);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((CustCareAccessDbService)i_dbService).getPrivilegeLevelData());
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_privilegeLevelField.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_privilegeLevelField.setEnabled(false);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Paints the privilege levels panel.
     */
    private void createTopPanel()
    {
        JLabel l_definedPrivilegesLabel = null;
        JLabel l_privilegeLabel = null;
        JLabel l_descriptionLabel = null;
        
        i_topPanel = WidgetFactory.createPanel(100, 15, 0, 0);
        
        l_definedPrivilegesLabel = WidgetFactory.createLabel("Defined Privileges:");
        
        l_privilegeLabel = WidgetFactory.createLabel("Privilege:");
        i_privilegeLevelField = WidgetFactory.createIntegerField(3, false);
        addValueChangedListener(i_privilegeLevelField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        i_topPanel.add(l_definedPrivilegesLabel, "definedPrivilegesLabel,1,1,15,5");
        i_topPanel.add(i_keyDataComboBox, "definedPrivilegesComboBox,16,1,50,5");
        
        i_topPanel.add(l_privilegeLabel, "privilegeLabel,1,7,15,5");
        i_topPanel.add(i_privilegeLevelField, "privilegeLevelField,16,7,5,5");
        
        i_topPanel.add(l_descriptionLabel, "descriptionLabel,32,7,10,5");
        i_topPanel.add(i_descriptionField, "descriptionField,43,7,47,5");
    }
    
    /**
     * Paints the screen access privileges panel.
     */
    private void createScreenAccessPanel()
    {
        i_screenAccessPanel = WidgetFactory.createPanel("Screen Access", 44, 73, 0, 0);

        i_screenAccessAllButton = WidgetFactory.createButton("All", i_selectionButtonListener, false);
        i_screenAccessNoneButton = WidgetFactory.createButton("None", i_selectionButtonListener, false);

        i_screenAccessPanel.add(i_screenAccessAllButton, "screenAccessAllButton,1,67,10,5");
        i_screenAccessPanel.add(i_screenAccessNoneButton, "screenAccessNoneButton,14,67,10,5");

        i_accountDetailsBox = WidgetFactory.createCheckBox("Account Details", i_checkBoxListener);
        i_accountDetailsBox.setSelected(true);
        i_accountDetailsBox.setToolTipText("Always enabled");

        i_newSubscriptionBox = WidgetFactory.createCheckBox("New Subscriber", i_checkBoxListener);
        i_subscriberDetailsBox = WidgetFactory.createCheckBox("Subscriber Details", i_checkBoxListener);
        i_vouchersBox = WidgetFactory.createCheckBox("Vouchers", i_checkBoxListener);
        i_paymentsBox = WidgetFactory.createCheckBox("Account Refills", i_checkBoxListener);
        i_adjustmentsBox = WidgetFactory.createCheckBox("Adjustments", i_checkBoxListener);
        i_adjustmentApprovalBox = WidgetFactory.createCheckBox("Adjustment Approval", i_checkBoxListener);
        i_promotionsBox = WidgetFactory.createCheckBox("Promotions", i_checkBoxListener);
        i_disconnectionBox = WidgetFactory.createCheckBox("Disconnection", i_checkBoxListener);
        i_subordinatesBox = WidgetFactory.createCheckBox("Subordinates", i_checkBoxListener);
        i_additionalInformationBox = WidgetFactory.createCheckBox("Additional Information", i_checkBoxListener);
        i_writeMemoBox = WidgetFactory.createCheckBox("Write Memo", i_checkBoxListener);
        i_dedicatedAccountsBox = WidgetFactory.createCheckBox("Dedicated Accounts", i_checkBoxListener);
        i_accumulatorsBox = WidgetFactory.createCheckBox("Accumulators", i_checkBoxListener);
        i_familyAndFriendsBox = WidgetFactory.createCheckBox("Family and Friends", i_checkBoxListener);
        i_subscriberSegmentationBox = WidgetFactory.createCheckBox("Subscriber Segmentation", i_checkBoxListener);
        i_communityChargingBox = WidgetFactory.createCheckBox("Community Charging", i_checkBoxListener);
        i_routingBox = WidgetFactory.createCheckBox("MSISDN Routing", i_checkBoxListener);
        i_chargedEventCountersBox = WidgetFactory.createCheckBox("Charged Event Counters", i_checkBoxListener);
        i_serviceFeeDeductionsBox = WidgetFactory.createCheckBox("Service Fee Deductions", i_checkBoxListener);
        i_callHistoryBox = WidgetFactory.createCheckBox("Call History / Statement", i_checkBoxListener);
        
        addValueChangedListener(i_newSubscriptionBox);
        addValueChangedListener(i_subscriberDetailsBox);
        addValueChangedListener(i_vouchersBox);
        addValueChangedListener(i_paymentsBox);
        addValueChangedListener(i_adjustmentsBox);
        addValueChangedListener(i_adjustmentApprovalBox);
        addValueChangedListener(i_promotionsBox);
        addValueChangedListener(i_disconnectionBox);
        addValueChangedListener(i_subordinatesBox);
        addValueChangedListener(i_additionalInformationBox);
        addValueChangedListener(i_writeMemoBox);
        addValueChangedListener(i_dedicatedAccountsBox);
        addValueChangedListener(i_accumulatorsBox);
        addValueChangedListener(i_familyAndFriendsBox);
        addValueChangedListener(i_subscriberSegmentationBox);
        addValueChangedListener(i_communityChargingBox);
        addValueChangedListener(i_routingBox);
        addValueChangedListener(i_chargedEventCountersBox);
        addValueChangedListener(i_serviceFeeDeductionsBox);
        addValueChangedListener(i_callHistoryBox);

        i_screenAccessPanel.add(i_accountDetailsBox, "accountDetailsBox,1,1,25,5");        
        i_screenAccessPanel.add(i_subscriberDetailsBox, "subscriberDetailsBox,1,5,25,5");        
        i_screenAccessPanel.add(i_vouchersBox, "vouchersBox,1,9,25,5");
        i_screenAccessPanel.add(i_paymentsBox, "paymentsBox,1,13,25,5");
        i_screenAccessPanel.add(i_adjustmentsBox, "adjustmentsBox,1,17,25,5");
        i_screenAccessPanel.add(i_promotionsBox, "promotionsBox,1,21,25,5");
        i_screenAccessPanel.add(i_disconnectionBox, "disconnectionBox,1,25,25,5");
        i_screenAccessPanel.add(i_subordinatesBox, "subordinatesBox,1,29,25,5");
        i_screenAccessPanel.add(i_additionalInformationBox, "additionalInformationBox,1,33,25,5");
        i_screenAccessPanel.add(i_dedicatedAccountsBox, "dedicatedAccountsBox,1,37,25,5");
        i_screenAccessPanel.add(i_accumulatorsBox, "accumulatorsBox,1,41,25,5");
        i_screenAccessPanel.add(i_chargedEventCountersBox, "chargedEventCountersBox,1,45,25,5");
        i_screenAccessPanel.add(i_familyAndFriendsBox, "familyAndFriendsBox,1,49,25,5");

        i_screenAccessPanel.add(i_subscriberSegmentationBox, "subsSegmentationBox,26,1,25,5");
        i_screenAccessPanel.add(i_communityChargingBox, "commChargingBox,26,5,25,5");
        i_screenAccessPanel.add(i_serviceFeeDeductionsBox, "serviceFeeDeductionsBox,26,9,25,5");
        i_screenAccessPanel.add(i_callHistoryBox, "callHistoryBox,26,13,25,5");
        i_screenAccessPanel.add(i_writeMemoBox, "writeMemoBox,26,17,25,5");
        
        i_screenAccessPanel.add(i_newSubscriptionBox, "newSubscriptionBox,26,41,25,5");        
        i_screenAccessPanel.add(i_adjustmentApprovalBox, "adjustmentApprovalBox,26,45,25,5");
        i_screenAccessPanel.add(i_routingBox, "routingBox,26,49,25,5");
    }
    
    /**
     * Paints the update access privileges panel.
     */
    private void createFunctionAccessPanel()
    {
        i_functionAccessPanel = WidgetFactory.createPanel("Function Access", 55, 73, 0, 0);

        i_functionAccessAllButton = WidgetFactory.createButton("All", i_selectionButtonListener, false);
        i_functionAccessNoneButton = WidgetFactory.createButton("None", i_selectionButtonListener, false);

        i_functionAccessPanel.add(i_functionAccessAllButton, "functionAccessAllButton,1,67,10,5");
        i_functionAccessPanel.add(i_functionAccessNoneButton, "functionAccessNoneButton,14,67,10,5");

        i_updateExpiryDatesBox = WidgetFactory.createCheckBox("Update Expiry Dates");
        i_updateUssdEocnIdBox = WidgetFactory.createCheckBox("Update USSD EoCN ID");
        i_changeLanguageBox = WidgetFactory.createCheckBox("Change Language");
        i_updateAgentOrSubAgentBox = WidgetFactory.createCheckBox("Update Agent / Sub Agent");
        i_refillUnbarBox = WidgetFactory.createCheckBox("Refill Unbar");
        i_updateSubscriberDefaultsBox = WidgetFactory.createCheckBox("Update Subscriber Details");
        i_standardVoucherRechargeBox = WidgetFactory.createCheckBox("Standard Voucher Refill");
        i_voucherUpdateBox = WidgetFactory.createCheckBox("Voucher Update");
        i_voucherEnquiryBox = WidgetFactory.createCheckBox("Voucher Enquiry");
        i_makePaymentBox = WidgetFactory.createCheckBox("Account Refill");
        i_makeAdjustmentBox = WidgetFactory.createCheckBox("Make Adjustment");
        i_modifyPromotionPlanAllocsBox = WidgetFactory.createCheckBox("Promotion Plan Allocs");
        i_addSubordinatesBox = WidgetFactory.createCheckBox("Add Subordinates");
        i_updateAdditionalInformationBox = WidgetFactory.createCheckBox("Update Additional Info");
        i_makeDedicatedAccountAdjustmentBox = WidgetFactory.createCheckBox("Dedicated Account Adjust");
        i_modifyFamilyAndFriendsNumbersBox = WidgetFactory.createCheckBox("Modify Family And Friends");
        i_updateAccumulatorsBox = WidgetFactory.createCheckBox("Update Accumulators");
        i_updateSubscriberSegmentationBox = WidgetFactory.createCheckBox("Update Subs Segmentation");
        i_updateCommunityChargingBox = WidgetFactory.createCheckBox("Update Community Charging");
        i_updateIvrPinBox = WidgetFactory.createCheckBox("Update IVR Access Pin");
        i_updateEventCountersBox = WidgetFactory.createCheckBox("Update Event Counters");
        i_updateHomeRegionBox = WidgetFactory.createCheckBox("Update Home Region");
        i_changeServiceClassBox = WidgetFactory.createCheckBox("Update Service Class / V V Refill");
        i_updateTempBlockBox = WidgetFactory.createCheckBox("Update Temporary Blocking");
        i_voucherHistoryEnquiryBox = WidgetFactory.createCheckBox("Voucher History Enquiry");
        i_overrideMsisdnQuarantinePeriodBox = WidgetFactory.createCheckBox("Reset Quarantine Period");
        i_singleStepInstallBox = WidgetFactory.createCheckBox("Single Step Install");
        i_accountReconnectionBox = WidgetFactory.createCheckBox("Reconnection");

        
        addValueChangedListener(i_updateExpiryDatesBox);
        addValueChangedListener(i_changeServiceClassBox);
        addValueChangedListener(i_changeLanguageBox);
        addValueChangedListener(i_updateAgentOrSubAgentBox);
        addValueChangedListener(i_refillUnbarBox);
        addValueChangedListener(i_updateSubscriberDefaultsBox);
        addValueChangedListener(i_standardVoucherRechargeBox);
        addValueChangedListener(i_voucherUpdateBox);
        addValueChangedListener(i_voucherEnquiryBox);
        addValueChangedListener(i_makePaymentBox);
        addValueChangedListener(i_makeAdjustmentBox);
        addValueChangedListener(i_modifyPromotionPlanAllocsBox);
        addValueChangedListener(i_addSubordinatesBox);
        addValueChangedListener(i_updateAdditionalInformationBox);
        addValueChangedListener(i_makeDedicatedAccountAdjustmentBox);
        addValueChangedListener(i_modifyFamilyAndFriendsNumbersBox);
        addValueChangedListener(i_updateAccumulatorsBox);
        addValueChangedListener(i_updateSubscriberSegmentationBox);
        addValueChangedListener(i_updateCommunityChargingBox);
        addValueChangedListener(i_updateUssdEocnIdBox);
        addValueChangedListener(i_updateIvrPinBox);
        addValueChangedListener(i_updateEventCountersBox);
        addValueChangedListener(i_updateHomeRegionBox);
        addValueChangedListener(i_updateTempBlockBox);
        addValueChangedListener(i_voucherHistoryEnquiryBox);
        addValueChangedListener(i_overrideMsisdnQuarantinePeriodBox);
        addValueChangedListener(i_singleStepInstallBox);
        addValueChangedListener(i_accountReconnectionBox);

        i_functionAccessPanel.add(i_updateExpiryDatesBox, "updateExpiryDatesBox,1,1,28,5");
        i_functionAccessPanel.add(i_updateUssdEocnIdBox, "i_updateUssdEocnIdBox,1,5,28,5");
        i_functionAccessPanel.add(i_changeLanguageBox, "changeLanguageBox,1,9,28,5");
        i_functionAccessPanel.add(i_updateAgentOrSubAgentBox, "updateAgentOrSubAgentBox,1,13,28,5");
        i_functionAccessPanel.add(i_refillUnbarBox, "refillUnbarBox,1,17,28,5");
        i_functionAccessPanel.add(i_updateIvrPinBox, "updateIvrPinBox,1,21,28,5");
        i_functionAccessPanel.add(i_changeServiceClassBox , "changeServiceClassBox,1,25,28,5");
        i_functionAccessPanel.add(i_updateHomeRegionBox, "updateHomeRegionBox,1,29,28,5");
        i_functionAccessPanel.add(i_updateTempBlockBox, "updateTempBlockBox,1,33,28,5");
        i_functionAccessPanel.add(i_updateSubscriberDefaultsBox, "updateSubscriberDefaultsBox,1,37,28,5");
        i_functionAccessPanel.add(i_standardVoucherRechargeBox, "standardVoucherRechargeBox,1,41,28,5");
        i_functionAccessPanel.add(i_voucherUpdateBox, "voucherUpdateBox,1,45,28,5");
        i_functionAccessPanel.add(i_voucherEnquiryBox, "voucherEnquiryBox,1,49,28,5");
        i_functionAccessPanel.add(i_voucherHistoryEnquiryBox, "voucherHistoryEnquiryBox,1,53,28,5");
        i_functionAccessPanel.add(i_makePaymentBox, "makePaymentBox,1,57,28,5");
        
        i_functionAccessPanel.add(i_makeAdjustmentBox, "makeAdjustmentBox,30,1,26,5");
        i_functionAccessPanel.add(i_modifyPromotionPlanAllocsBox, "modifyPromotionPlanAllocsBox,30,5,26,5");
        i_functionAccessPanel.add(i_addSubordinatesBox, "addSubordinatesBox,30,9,26,5");
        i_functionAccessPanel.add(i_updateAdditionalInformationBox, "updateAdditionalInformationBox,30,13,26,5");
        i_functionAccessPanel.add(i_makeDedicatedAccountAdjustmentBox, "makeDedicatedAccountAdjustmentBox,30,17,26,5");
        i_functionAccessPanel.add(i_updateAccumulatorsBox, "updateAccumulatorsBox,30,21,26,5");
        i_functionAccessPanel.add(i_updateEventCountersBox, "updateEventCountersBox,30,25,26,5");
        i_functionAccessPanel.add(i_modifyFamilyAndFriendsNumbersBox, "modifyFamilyAndFriendsNumbersBox,30,29,26,5");
        i_functionAccessPanel.add(i_updateSubscriberSegmentationBox, "updateSubsSegmentationBox,30,33,26,5");
        i_functionAccessPanel.add(i_updateCommunityChargingBox, "updateCommunityChargingBox,30,37,26,5");
        i_functionAccessPanel.add(i_overrideMsisdnQuarantinePeriodBox,"overrideMsisdnQuarantinePeriodBox,30,41,26,5");
        i_functionAccessPanel.add(i_singleStepInstallBox,"singleStepInstallBox,30,45,26,5");
        i_functionAccessPanel.add(i_accountReconnectionBox, "accountReconnectionBox,30,49,25,5");
        
        /* Construct an array of arrays of Field Access JCheckBoxes...
         * Each sub-array lists available Functions for a Screen.
         * When a Screen checkbox is mouse entered then any related Function
         * checkboxes will be visually highlighted. 
         */
        JCheckBox l_screenFunctionsArray[][] =
        {
            {   /* Account Details Screen */
                i_updateExpiryDatesBox,
                i_changeServiceClassBox,
                i_changeLanguageBox,
                i_updateAgentOrSubAgentBox,
                i_refillUnbarBox,
                i_updateUssdEocnIdBox,
                i_updateIvrPinBox,
                i_updateHomeRegionBox,
                i_updateTempBlockBox
            },
            {   /* Subscriber Details Screen */
                i_updateSubscriberDefaultsBox
            },
            {   /* Vouchers Screen */
                i_standardVoucherRechargeBox,
                i_voucherUpdateBox,
                i_voucherEnquiryBox,
                i_voucherHistoryEnquiryBox,
                i_changeServiceClassBox
            },
            {   /* Account Refills Screen */
                i_makePaymentBox
            },
            {   /* Adjustments Screen */
                i_makeAdjustmentBox
            },
            {   /* Promotions Screen */
                i_modifyPromotionPlanAllocsBox
            },
            {   /* Subordinates Screen */
                i_addSubordinatesBox
            },
            {   /* Additional Info Screen */
                i_updateAdditionalInformationBox
            },
            {   /* New Subscriber Screen */
                i_overrideMsisdnQuarantinePeriodBox,
                i_singleStepInstallBox
            },
            {   /* Accumulators Screen */
                i_updateAccumulatorsBox
            },
            {   /* Dedicated Accounts Screen */
                i_makeDedicatedAccountAdjustmentBox
            },
            {   /* Family & Friends Screen */
                i_modifyFamilyAndFriendsNumbersBox
            },
            {   /* Community Charging Details Screen */
                i_updateCommunityChargingBox
            },
            {   /* Subscriber Segmentation Details Screen */
                i_updateSubscriberSegmentationBox
            },
            {   /* Charged End User Event Counters Screen */
                i_updateEventCountersBox
            },
            {
                // Disconnection screen
                i_accountReconnectionBox
            }
        };

        // Contruct a HashMap to relate each Function array to a Screen...
        i_screenFunctionsHashMap = new HashMap();
        i_screenFunctionsHashMap.put(i_accountDetailsBox, l_screenFunctionsArray[0]);
        i_screenFunctionsHashMap.put(i_subscriberDetailsBox, l_screenFunctionsArray[1]);
        i_screenFunctionsHashMap.put(i_vouchersBox, l_screenFunctionsArray[2]);
        i_screenFunctionsHashMap.put(i_paymentsBox, l_screenFunctionsArray[3]);
        i_screenFunctionsHashMap.put(i_adjustmentsBox, l_screenFunctionsArray[4]);
        i_screenFunctionsHashMap.put(i_promotionsBox, l_screenFunctionsArray[5]);
        i_screenFunctionsHashMap.put(i_subordinatesBox, l_screenFunctionsArray[6]);
        i_screenFunctionsHashMap.put(i_additionalInformationBox, l_screenFunctionsArray[7]);
        i_screenFunctionsHashMap.put(i_newSubscriptionBox, l_screenFunctionsArray[8]);
        i_screenFunctionsHashMap.put(i_accumulatorsBox, l_screenFunctionsArray[9]);
        i_screenFunctionsHashMap.put(i_dedicatedAccountsBox, l_screenFunctionsArray[10]);
        i_screenFunctionsHashMap.put(i_familyAndFriendsBox, l_screenFunctionsArray[11]);
        i_screenFunctionsHashMap.put(i_communityChargingBox, l_screenFunctionsArray[12]);
        i_screenFunctionsHashMap.put(i_subscriberSegmentationBox, l_screenFunctionsArray[13]);
        i_screenFunctionsHashMap.put(i_chargedEventCountersBox, l_screenFunctionsArray[14]);
        i_screenFunctionsHashMap.put(i_disconnectionBox, l_screenFunctionsArray[15]);
        
        // Tell the Screen Access checkBoxListener to use this HashMap of
        // related Function Access JCheckBoxes... 
        i_checkBoxListener.setRelatedCheckBoxHashMap(i_screenFunctionsHashMap);
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to listen on All and None buttons.
     */
    private class SelectionButtonListener
    implements ActionListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Event handler for the All & None buttons.
         * @param p_actionEvent Event to handle.
         */
        public void actionPerformed(ActionEvent p_actionEvent)
        {
            Object    l_source = p_actionEvent.getSource();
        
            if (l_source == i_screenAccessAllButton)
            {
                setScreenAccessBoxes(true);
            }

            else if (l_source == i_screenAccessNoneButton)
            {
                setScreenAccessBoxes(false);
            }

            else if (l_source == i_functionAccessAllButton)
            {
                setFunctionAccessBoxes(true);
            }

            else if (l_source == i_functionAccessNoneButton)
            {
                setFunctionAccessBoxes(false);
            }
        }
    }

    /**
     * Class to listen on checkbox events. Currently handles events
     * for checkbox highlighting and the Account Details checkbox
     * which always has to be on. 
     */
    private class CheckBoxListener
    implements MouseListener
    {
        /* This HashMap contains key,value pairs where the keys
         * are JCheckBoxes and the values are references to arrays
         * of related JCheckBoxes. So for a given JCheckBox it
         * is possible to find out if there are any JCheckBoxes
         * related to it.
         */
        private HashMap i_relatedCheckBoxHashMap = null;

        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        void setRelatedCheckBoxHashMap(HashMap p_relatedCheckBoxHashMap)
        {
            i_relatedCheckBoxHashMap = p_relatedCheckBoxHashMap;
        }
        
        public void mouseClicked(MouseEvent p_mouseEvent)
        {
            if (i_accountDetailsBox == (JCheckBox) p_mouseEvent.getSource())
            {
                // Force the Account Details Screen to be always ON
                i_accountDetailsBox.setSelected(true);
            }
        }
        
        public void mouseEntered(MouseEvent p_mouseEvent)
        {
            JCheckBox l_checkBox = (JCheckBox) p_mouseEvent.getSource();
        
            l_checkBox.setFont(WidgetFactory.C_FONT_BOLD_TEXT);
            setFontForRelatedCheckBoxes(l_checkBox, WidgetFactory.C_FONT_BOLD_TEXT);
        }
        
        public void mouseExited(MouseEvent p_mouseEvent)
        {
            JCheckBox l_checkBox = (JCheckBox) p_mouseEvent.getSource();
        
            l_checkBox.setFont(WidgetFactory.C_FONT_PLAIN_TEXT);
            setFontForRelatedCheckBoxes(l_checkBox, WidgetFactory.C_FONT_PLAIN_TEXT);
        }
        
        public void mousePressed(MouseEvent p_mouseEvent)
        {
        }
        
        public void mouseReleased(MouseEvent p_mouseEvent)
        {
        }

        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /* As the mouse moves into & out-of a given JCheckBox
         * set the font of any related JCheckBoxes.
         * @param p_checkBox - the JCheckBox that caused the mouse event
         * @param p_font - the font the related JCheckBoxes must now be set to
         */
        private void setFontForRelatedCheckBoxes(JCheckBox p_checkBox, Font p_font)
        {
            if (i_relatedCheckBoxHashMap != null)
            {
                JCheckBox l_functionsArray[] = (JCheckBox[]) i_relatedCheckBoxHashMap.get(p_checkBox);
                
                if (l_functionsArray != null)
                {
                    for (int l_count = 0; l_count < l_functionsArray.length; ++l_count)
                    {
                        l_functionsArray[l_count].setFont(p_font);
                    }
                }
            }
        }
    }
}
