////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       AccumulatorsDbService.java
//    DATE            :       05-Jan-2005
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#1067/5383
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Accumulators screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy  | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiAccumulatorData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.AccfAccumulatorSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Accumulators screen.
 */
public class AccumulatorsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------        
    
    /** Identifies the misc_table column in srva_misc_codes DB table. */
    public static final String C_MISC_TABLE_SERVICE_CLASS = "CLS";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for accumulator code details. */
    private AccfAccumulatorSqlService i_accfSqlService            = null;

    /** Vector of accumulator records. */
    private Vector                    i_availableAccumulatorDataV = null;

    /** Boi Accumulator data objetc. */
    private BoiAccumulatorData        i_accumulatorData           = null;
    
    /** Currently selected accumulator code. Used as key for read and delete. */
    private int                       i_currentCode               = -1;
    
    /** Vector of available markets for display. */
    private Vector                    i_availableMarketsV         = null;
    
    /** Vector of miscellaneous code records. */
    private Vector                    i_availableMiscCodeDataV    = null;
    
    /** SQL service for Misc Code. */
    private MiscCodeSqlService        i_miscCodeSqlService        = null;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService         = null;
    
    /** Operator's default market. */
    private BoiMarket                 i_defaultMarket             = null;
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public AccumulatorsDbService(BoiContext p_context)
    {
        super(p_context);
        i_accfSqlService = new AccfAccumulatorSqlService(null);
        i_miscCodeSqlService = new MiscCodeSqlService (null, null);
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);
        i_availableAccumulatorDataV = new Vector(5);
        i_availableMiscCodeDataV = new Vector(5);
        i_availableMarketsV = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }
    
    /** 
     * Return available Misc Code data records. 
     * @return Vector of available Misc Code data records.
     */
    public Vector getAvailableMiscCodeData()
    {
        return i_availableMiscCodeDataV;
    }
    
    
    /**
     * Set current accumulator code. Used for record lookup in database.
     * @param p_code Currently selected accumulator code.
     */
    public void setCurrentCode(int p_code)
    {
        i_currentCode = p_code;
    }
    
    /**
     * Return Accumulator data object currently being worked on.
     * @return Accumulator data object currently being worked on.
     */
    public BoiAccumulatorData getAccumulatorData()
    {
        return i_accumulatorData;
    }

    /**
     * Sets the Accumulator data object currently being worked on.
     * @param p_accumulatorData Accumulator data object.
     */
    public void setAccumulatorData(BoiAccumulatorData p_accumulatorData)
    {
        i_accumulatorData = p_accumulatorData;
    }

    /**
     * Returns the available Accumulator data.
     * @return Vector of available Accumulator data records.
     */
    public Vector getAvailableAccumulatorData()
    {
        return i_availableAccumulatorDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        AccfAccumulatorDataSet l_accfDataSet = null;

        l_accfDataSet = i_accfSqlService.read(null, p_connection);
        
        i_accumulatorData = 
            new BoiAccumulatorData(
                    l_accfDataSet.getAccumulatorData(
                        i_currentMarket.getMarket(),
                        new ServiceClass(new Integer(
                                i_currentServiceClass.getInternalMiscCodeData().getCode()).intValue()), 
                        i_currentCode));
    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;

        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;     
        }
        
        if (i_currentServiceClass == null)
        {
            refreshMiscCodeDataVector(p_connection, i_currentMarket.getMarket());
            if (i_availableMiscCodeDataV.size() != 0)
            {
                i_currentServiceClass = (BoiMiscCodeData)i_availableMiscCodeDataV.firstElement();
            }
        }

        refreshAccumulatorDataVector(p_connection);
    }

    /**
     * Inserts record into the accf_accumulator_configuration table.
     * @param  p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        AccfAccumulatorData l_accumulatorData = null;
        
        l_accumulatorData = i_accumulatorData.getInternalAccumulatorData();
        
        i_accfSqlService.insert(null,
                                p_connection,
                                l_accumulatorData.getMarket(),
                                l_accumulatorData.getServiceClass(),
                                l_accumulatorData.getId(),
                                l_accumulatorData.getDescription(),
                                l_accumulatorData.getUnitsDescription(),
                                i_context.getOperatorUsername());

        refreshAccumulatorDataVector(p_connection);
    }

    /**
     * Updates record in the accf_accumulator_configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        AccfAccumulatorData l_accumulatorData = null;
        
        l_accumulatorData = i_accumulatorData.getInternalAccumulatorData();
        
        i_accfSqlService.update(null,
                                p_connection,
                                l_accumulatorData.getMarket(),
                                l_accumulatorData.getServiceClass(),
                                l_accumulatorData.getId(),
                                l_accumulatorData.getDescription(),
                                l_accumulatorData.getUnitsDescription(),
                                i_context.getOperatorUsername());

        refreshAccumulatorDataVector(p_connection);
    }

    /**
     * Deletes record from the accf_accumulator_configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        AccfAccumulatorData l_accumulatorData = null;
        
        l_accumulatorData = i_accumulatorData.getInternalAccumulatorData();
        
        i_accfSqlService.delete(
            null,
            p_connection,
            l_accumulatorData.getMarket(),
            l_accumulatorData.getServiceClass(), 
            i_currentCode);

        refreshAccumulatorDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection The database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        AccfAccumulatorDataSet l_accfDataSet = null;
        AccfAccumulatorData    l_dbAccumulatorData = null;
        boolean[]              l_flagsArray = new boolean[2];
        
        l_accfDataSet = i_accfSqlService.read(null, p_connection);
        l_dbAccumulatorData = 
            l_accfDataSet.getAccumulatorData(
                i_currentMarket.getMarket(),
                new ServiceClass(new Integer(
                        i_currentServiceClass.getInternalMiscCodeData().getCode()).intValue()), 
                i_currentCode);
                               
        if (l_dbAccumulatorData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = false;
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshMiscCodeDataVector(JdbcConnection p_connection,
                                           Market         p_market)
        throws PpasSqlException
    {
        MiscCodeDataSet   l_miscCodeDataSet;
        MiscCodeDataSet   l_availableMiscCodeDataSet;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;
        
        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getRawAvailableMiscCodeData(
                                         p_market, 
                                         C_MISC_TABLE_SERVICE_CLASS);
        
        l_miscCodeV = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableMiscCodeDataV.addElement(l_boiMiscCodeArray[j]);
        }
    }
    
    /**
     * Refreshes the available charging data vector from the accf_accumulator_configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshAccumulatorDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        AccfAccumulatorDataSet l_accfDataSet = null;
        AccfAccumulatorData[]  l_accfArray   = null;

        i_availableAccumulatorDataV.removeAllElements();

        l_accfDataSet = i_accfSqlService.read(null, p_connection);
        
        if (i_currentServiceClass != null)
        {
            l_accfArray =
                l_accfDataSet.getAccfArray(
                    i_currentMarket.getMarket(),
                    new ServiceClass(
                            new Integer(
                                    i_currentServiceClass.getInternalMiscCodeData().getCode()).intValue()));
        
        }

        i_availableAccumulatorDataV.addElement(new String(""));
        i_availableAccumulatorDataV.addElement(new String("NEW RECORD"));
        if (l_accfArray != null)
        {
            for (int i = 0; i < l_accfArray.length; i++)
            {
                i_availableAccumulatorDataV.addElement(new BoiAccumulatorData(l_accfArray[i]));
            }
        }
    }
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}
