////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       PurgeAndArchiveDbService.java
//    DATE            :       20-Aug-2004
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#112/3801
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Operators screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/7/5   | M.Brister  | Added repeat scheduling         | PpacLon#1398/6788
//          |            | parameters to submitJob.        | 
//          |            |                                 | 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.HashMap;

import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.PucoPurgeControlData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PucoPurgeControlDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PucoPurgeControlSqlService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDateTime;

/**
 * Class to provide PUCO_PURGE_CONTROL table information and job submission methods.  
 */
public class PurgeAndArchiveDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** The initial number of rows to display in the table on the Purge and Archive screen. */
    private static final int C_INITIAL_NUMBER_OF_TABLE_ROWS = 12;
    
    /** Database service to perform operations on the PUCO_PURGE_CONTROL_TABLE. */
    private PucoPurgeControlSqlService i_purgeControlSqlService = null;
    
    /** DataSet object tailed specifically to hold rows from the PUCO_PURGE_CONTROL table. */
    private PucoPurgeControlDataSet i_purgeControlDataSet = null;
    
    /** Data array of Strings to contain a subset of data from PUCO_PURGE_CONTROL. */
    private String[][] i_tableData = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructs an instance of this class.
     * @param p_context A <code>BoiContext</code> object
     */
    public PurgeAndArchiveDbService(BoiContext p_context)
    {
        super(p_context);
        i_purgeControlSqlService = new PucoPurgeControlSqlService(null);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Fetches rows from the PUCO_PURGE_CONTROL table and reads them into a data array 
     * for display on the Purge and Archive screen.
     * @param p_jobType Used to match with PUCO_JOB_TYPE to return the required rows
     * @param p_allJobs A boolean value which is true if all rows should be returned regardless of
     *                  the status, and false if only rows representing those jobs in progress should 
     *                  be returned
     * @return A data array of Strings for display on the Purge and Archive screen table.
     * 
     */
    public String[][] getTableData(String p_jobType, boolean p_allJobs)
    {
        PucoPurgeControlDataSet l_purgeControlDataSubSet = null;
        PucoPurgeControlData    l_purgeControlData = null;
        int                     l_numberOfFilledRows = 0;
        int                     l_numberOfRows       = 0; // Includes empty rows
        int                     i                    = 0;
        
        if (p_allJobs)
        {
            l_purgeControlDataSubSet = i_purgeControlDataSet.getJobInfo(null, p_jobType);
        }
        else
        {  
            l_purgeControlDataSubSet = i_purgeControlDataSet.getJobInfo(null, p_jobType, 'I');
        }
            
        l_numberOfFilledRows = l_purgeControlDataSubSet.getDataV().size();
        l_numberOfRows = 
                (l_numberOfFilledRows < C_INITIAL_NUMBER_OF_TABLE_ROWS) ?
                 C_INITIAL_NUMBER_OF_TABLE_ROWS : l_numberOfFilledRows;
             
        i_tableData = new String[l_numberOfRows][4];
            
            
        for (i = 0; i < l_numberOfFilledRows; i++)
        {
            l_purgeControlData = (PucoPurgeControlData)l_purgeControlDataSubSet.getDataV().elementAt(i);

            if (l_purgeControlData.getJobExecutionDateTime() != null)
            {
                i_tableData[i][0] = l_purgeControlData.getJobExecutionDateTime().toString_yyyyMMdd_HHmmss();
            }
            
            if (l_purgeControlData.getEligibilityDate() != null)
            {
                i_tableData[i][1] = l_purgeControlData.getEligibilityDate().toString();
            }

            i_tableData[i][2] = String.valueOf(l_purgeControlData.getArchiveOption());
                    
            i_tableData[i][3] = String.valueOf(l_purgeControlData.getStatus());
        }
            
        if (i < C_INITIAL_NUMBER_OF_TABLE_ROWS)
        {
            for (int k = i; k < C_INITIAL_NUMBER_OF_TABLE_ROWS; k++)
            {
                for (int l = 0; l < 4; l++)
                {
                    i_tableData[k][l] = "";
                }
            }
        }
        
        return i_tableData;
    }

    /**
     * Submits the job to the job scheduler.
     * @param p_jobType
     * @param p_submitDateTime
     * @param p_jobParams
     * @param p_scheduleImmediate
     * @param p_frequency Frequency of job submission.
     * @param p_interval Interval of job submission.  eg. If frequency = DAILY and
     *                   interval = 2, job will run once every 2 days.
     * @throws PpasServiceFailedException Requested service failed exception.
     */
    public void submitJob(String       p_jobType,
                          PpasDateTime p_submitDateTime,
                          HashMap      p_jobParams,
                          boolean      p_scheduleImmediate,
                          String       p_frequency,
                          String       p_interval)
        throws PpasServiceFailedException
    {
        super.submitJob(p_jobType, 
                        p_submitDateTime, 
                        p_jobParams, 
                        p_scheduleImmediate,
                        p_frequency,
                        p_interval.equals("") ? 0 : Integer.parseInt(p_interval));
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection The <code>JdbcConnection</code> to the database.
     * @throws PpasSqlException Any SQL exception when attempting to read from the database.
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_purgeControlDataSet = i_purgeControlSqlService.readAll(null, p_connection);
    }
   
    /** 
     * Must implement superclass method, but does nothing.
     * @param p_connection A database connection
     * @throws PpasSqlException
     */   
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException {}
    
    /** 
     * Must implement superclass method, but does nothing.
     * @param p_connection A database connection
     * @throws PpasSqlException
     */    
    protected void insert(JdbcConnection p_connection) throws PpasSqlException {}

    /** 
     * Must implement superclass method, but does nothing.
     * @param p_connection A database connection
     * @throws PpasSqlException
     */  
    protected void update(JdbcConnection p_connection) throws PpasSqlException {}
    
    /** 
     * Must implement superclass method, but does nothing.
     * @param p_connection A database connection
     * @throws PpasSqlException
     */  
    protected void delete(JdbcConnection p_connection) throws PpasSqlException {}
    
    /** 
     * Implement inherited abstract method.
     * @param p_connection A database connection 
     * @return null
     * @throws PpasSqlException 
     */ 
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        return null;
    }

    
}