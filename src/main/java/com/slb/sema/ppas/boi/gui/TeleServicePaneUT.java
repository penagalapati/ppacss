////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TeleServicePaneUT.java
//      DATE            :       30 March 2006
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#2124/8365
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit test class for the Tele Service pane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiTeleServiceData;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/**
 * This <code>TeleServicePaneUT</code> class is a JUnit test class for the
 * <code>TeleServicePane</code> object.
 */
public class TeleServicePaneUT extends BoiTestCaseTT
{
    //--------------------------------------------------------------------------
    // Constants.                                                             --
    //--------------------------------------------------------------------------
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME     = "TeleServicePaneUT";

    //--------------------------------------------------------------------------
    // Instance attributes.                                                   --
    //--------------------------------------------------------------------------
    /** Table containing existing records. */
    private TeleServicePane     i_teleServicePane  = null;

    /** Tele Service Codes code field. */
    private JFormattedTextField i_codeField        = null;

    /** Tele Service description field. */
    private ValidatedJTextField i_descriptionField = null;

    /** Key data combo box. */
    private JComboBox           i_keyDataCombo     = null;
    
    /** Button for updates and inserts. */
    private JButton             i_updateButton     = null;
    
    /** Delete button. */
    private JButton             i_deleteButton     = null;

    //--------------------------------------------------------------------------
    // Constructors.                                                          --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public TeleServicePaneUT(String p_title)
    {
        super(p_title);

        i_teleServicePane = new TeleServicePane(c_context);
        super.init(i_teleServicePane);
        i_teleServicePane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField    = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //--------------------------------------------------------------------------
    // Public class methods.                                                  --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Static method that allows the framework to automatically run all the
     * tests in the class.
     * A program provided by the JUnit framework can traverse a list of TestCase
     * classes calling this suite method and get an instance of the TestCase
     * which it then executes.
     * 
     * @return an instance of the <code>junit.framework.Test</code> class.
     */
    public static Test suite()
    {
        return new TestSuite(TeleServicePaneUT.class);
    }

    //--------------------------------------------------------------------------
    // Public instance methods.                                               --
    //--------------------------------------------------------------------------
    /*************************************************************************** 
     * @ut.when a user attempts to insert, select, update, delete, and makes a
     * record available again through the Teleservice screen.
     * 
     * @ut.then the operations are successfully performed.
     * 
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("TeleServicePane test");

        BoiTeleServiceData l_boiTeleServiceData = null;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_teleServicePane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_teleServicePane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiTeleServiceData = new BoiTeleServiceData(new TeleTeleserviceData(null, 
                                                                              Integer.parseInt(C_CODE), 
                                                                              C_DESCRIPTION,
                                                                              ' ', 
                                                                              C_BOI_OPID, 
                                                                              null));
        SwingTestCaseTT.setKeyComboSelectedItem(i_teleServicePane, i_keyDataCombo, l_boiTeleServiceData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_teleServicePane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_teleServicePane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_teleServicePane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_teleServicePane, i_updateButton);

        // *********************************************************************
        say("End of test.");
        // *********************************************************************
        endOfTest();
    }

    //--------------------------------------------------------------------------
    // Protected instance methods.                                            --
    //--------------------------------------------------------------------------
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteTeleServiceRecord(C_CODE);
    }

    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteTeleServiceRecord(C_CODE);
        say(":::End Of Test:::");
    }
    
    //--------------------------------------------------------------------------
    // Private instance methods.                                              --
    //--------------------------------------------------------------------------
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteTeleServiceRecord = "deleteTeleServiceRecord";
    /**
     * Removes a row from the TELE_TELESERVICE table.
     * 
     * @param p_teleServiceCode the tele service code.
     */
    private void deleteTeleServiceRecord(String p_teleServiceCode)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE FROM tele_teleservice " +
                           "WHERE tele_teleservice_code = {0}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_teleServiceCode);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteTeleServiceRecord);
    }
}