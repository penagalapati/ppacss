////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiBonusSchemeData.java
//      DATE            :       19-Dec-2006
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#
//                              PRD_ASCS00_GEN_CA_104
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Wrapper for BonsBonusSchemeData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// yy/mm/dd | <Name>     | <Description>                   | PpacLon#XXXX/YYYY
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeData;

/** Wrapper for BonsBonusSchemeData class within BOI. */
public class BoiBonusSchemeData extends DataObject
{
    /** Wrappered data object */
    private BonsBonusSchemeData i_bonusSchemeData;
    
    /**
     * Simple constructor.
     * @param p_bonusSchemeData Bonus scheme data object to wrapper.
     */
    public BoiBonusSchemeData(BonsBonusSchemeData p_bonusSchemeData)
    {
        i_bonusSchemeData = p_bonusSchemeData;
    }
    
    /**
     * Return wrappered bonus scheme data object.
     * @return Wrappered bonus scheme data object.
     */
    public BonsBonusSchemeData getInternalBonusSchemeData()
    {
        return i_bonusSchemeData;
    }
    
    /**
     * Compares two bonus scheme data objects and returns true if they equate.
     * @param p_bonusSchemeData Bonus scheme data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_bonusSchemeData)
    {
        boolean          l_return = false;
        BonsBonusSchemeData l_bonusSchemeData = null;
        
        if (p_bonusSchemeData != null &&
            p_bonusSchemeData instanceof BoiBonusSchemeData)
        {
            l_bonusSchemeData = ((BoiBonusSchemeData)p_bonusSchemeData).getInternalBonusSchemeData();
            
            if (i_bonusSchemeData.getBonusSchemeId().equals(l_bonusSchemeData.getBonusSchemeId()))
            {
                l_return = true;
            }
        }
        return l_return;
    }
    
    /**
     * Returns bonus scheme data object as a String for display in BOI.
     * @return Bonus scheme data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_bonusSchemeData.getBonusSchemeId() + 
                                            " - " + i_bonusSchemeData.getDesc());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_bonusSchemeData.getBonusSchemeId().length();
    }
}