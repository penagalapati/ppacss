////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ServiceClassDetailsPane.java
//      DATE            :       12-Jan-2004
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/1305
//
//      COPYRIGHT       :       SchlumbergerSema 2003
//
//      DESCRIPTION     :       BOI screen to maintain the default subscriber
//                              installation data for each service class.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10586
//          |            | selecting empty row.            |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ItemEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.slb.sema.ppas.boi.boidata.BoiCurrencyFormatData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.boidata.BoiPromoPlanData;
import com.slb.sema.ppas.boi.boidata.BoiValidLanguageData;
import com.slb.sema.ppas.boi.dbservice.ServiceClassDetailsDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Service Class Details screen.
 */
public class ServiceClassDetailsPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------        

    /** Available markets vector. */
    private Vector i_markets;    
    
    /** Existing Service Class Details codes vector. */
    private Vector i_definedCodes; 
    
    /** Vector of available promotion plans. */
    private Vector i_availablePromoPlans;
    
    /** Vector of promotion plans for display in Combo box. Contains available plans plus any currently 
     *  selected plan which is no longer available. */
    private Vector i_promoPlansForCombo;
    
    /** Vector of available currencies. */
    private Vector i_availableCurrencies;

    /** Vector of currencies for display in Combo box. Contains available currencies plus any currently 
     *  selected currency which is no longer available. */
    private Vector i_currenciesForCombo;
    
    /** Vector of available valid languages. */
    private Vector i_availableValidLanguages;

    /** Vector of languages for display in Combo box. Contains available languages plus any currently 
     *  selected language which is no longer available. */
    private Vector i_languagesForCombo;
    
    /** Panel to hold the defined markets and service classes. */
    private JPanel i_topPanel;
    
    /** Panel to hold the service class defaults. */
    private JPanel i_detailsPanel;
    
    /** Miscellaneous code field. */
    private JFormattedTextField i_codeField;

    /** Miscellaneous code description field. */
    private ValidatedJTextField i_descriptionField;    
    
    /** Combo-box to hold the defined languages. */
    private JComboBox i_languageBox;
    
    /** Combo-box to hold the defined currencies. */
    private JComboBox i_currencyBox;
    
    /** Combo-box to hold the defined promotions. */
    private JComboBox i_promotionBox;
    
    /** Warning message to alert user to obsolete promotion plan. */
    private JLabel i_promoWarningLabel;
    
    /** Combo box model for currency formates. */
    private DefaultComboBoxModel i_currencyModel;
    
    /** Combo box model for promotion plans. */
    private DefaultComboBoxModel i_promotionModel;
    
    /** Combo box model for valid languages. */
    private DefaultComboBoxModel i_languageModel;        

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------     
    
    /** 
     * ServiceClassDetailsPane constructor. 
     * @param p_context A reference to the BoiContext
     */     
    public ServiceClassDetailsPane(
        Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new ServiceClassDetailsDbService((BoiContext)i_context);        
        
        super.init();        
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------        
    
    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {   
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }    
        
    /**
     * Handles an <code>ItemEvent</code> for this screen.
     * @param p_itemEvent The event to be handled
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        PrplPromoPlanData l_promoPlan = null;

        if (p_itemEvent.getStateChange() == ItemEvent.SELECTED &&
            p_itemEvent.getSource() == i_promotionBox)
        {
            if (!i_promotionBox.getSelectedItem().toString().equals(""))
            {
                l_promoPlan = ((BoiPromoPlanData)i_promotionBox.getSelectedItem()).getInternalPromoPlanData();
            }
            
            if ( l_promoPlan != null &&
                 (l_promoPlan.isDeleted() || l_promoPlan.isObsolete()) )
            {
                i_promoWarningLabel.setVisible(true);
            }
            else
            {
                i_promoWarningLabel.setVisible(false);
            }
        }
    }
    
    //-------------------------------------------------------------------------
    // Protected methods 
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        if (i_languageBox.getItemCount() != 0)
        {
            i_languageBox.setSelectedIndex(0);
        }
        if (i_promotionBox.getItemCount() != 0)
        {
            i_promotionBox.setSelectedIndex(0);
        }
        if (i_currencyBox.getItemCount() != 0)
        {
            i_currencyBox.setSelectedIndex(0);
        }
    }   
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Service Class Details", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                          BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_SERVICE_CLASS_SCREEN), 
                                          i_helpComboListener);
        i_helpComboBox.setFocusable(false);
        
        createTopPanel();
        createDetailsPanel();
        
        i_mainPanel.add(i_helpComboBox, "serviceClassHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_topPanel, "topPanel,1,6,100,41");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,48,100,47");
        
        // Add buttons to screen that were created in superclass
        i_mainPanel.add(i_updateButton, "updateButton,1,96,15,5");
        i_mainPanel.add(i_deleteButton, "deleteButton,17,96,15,5");
        i_mainPanel.add(i_resetButton, "resetButton,33,96,15,5");         
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        MiscCodeData             l_miscCodeData;
        ValaValidLanguageData    l_currentLanguageData;
        PrplPromoPlanData        l_currentPromoPlanData;
        CufmCurrencyFormatsData  l_currentCurrency;

        l_miscCodeData = ((ServiceClassDetailsDbService)i_dbService).getMiscCodeData().getInternalMiscCodeData();
        
        i_codeField.setValue(new Integer(l_miscCodeData.getCode()));
        i_descriptionField.setText(l_miscCodeData.getDescription());
        
        l_currentLanguageData = ((ServiceClassDetailsDbService)i_dbService).getCurrentLanguageSelection();
        refreshLanguageCombo(l_currentLanguageData);

        l_currentCurrency = ((ServiceClassDetailsDbService)i_dbService).getCurrentCurrencySelection();
        refreshCurrencyCombo(l_currentCurrency);

        l_currentPromoPlanData = ((ServiceClassDetailsDbService)i_dbService).getCurrentPromoSelection();
        refreshPromoPlanCombo(l_currentPromoPlanData);
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((ServiceClassDetailsDbService)i_dbService).getAvailableMarketData();  
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((ServiceClassDetailsDbService)i_dbService).
                                                 getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);         
               
        i_definedCodes = ((ServiceClassDetailsDbService)i_dbService).getAvailableMiscCodeData();
        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);

        i_availableValidLanguages = ((ServiceClassDetailsDbService)i_dbService).getAvailableValidLanguageData();
        i_languageModel = new DefaultComboBoxModel(i_availableValidLanguages);
        i_languageBox.setModel(i_languageModel);
        
        i_availablePromoPlans = ((ServiceClassDetailsDbService)i_dbService).getAvailablePromoPlanData();
        i_promotionModel = new DefaultComboBoxModel(i_availablePromoPlans);
        i_promotionBox.setModel(i_promotionModel);
        
        i_promoWarningLabel.setVisible(false);

        i_availableCurrencies = ((ServiceClassDetailsDbService)i_dbService).getAvailableCurrencyFormatsData();
        i_currencyModel = new DefaultComboBoxModel(i_availableCurrencies);
        i_currencyBox.setModel(i_currencyModel);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        PrplPromoPlanData       l_promoPlan = null;
        ValaValidLanguageData   l_language = null;
        CufmCurrencyFormatsData l_currency = null;
        
        i_definedCodes = ((ServiceClassDetailsDbService)i_dbService).getAvailableMiscCodeData();        
        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);

        if (!i_promotionBox.getSelectedItem().toString().equals(""))
        {
            l_promoPlan = ((BoiPromoPlanData)i_promotionBox.getSelectedItem()).getInternalPromoPlanData();
        }
        refreshPromoPlanCombo(l_promoPlan);

        l_language = ((BoiValidLanguageData)i_languageBox.getSelectedItem()).getInternalValidLanguageData();
        refreshLanguageCombo(l_language);
        
        l_currency = ((BoiCurrencyFormatData)i_currencyBox.getSelectedItem()).getInternalCurrencyFormatData();
        refreshCurrencyCombo(l_currency);
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((ServiceClassDetailsDbService)i_dbService).getMiscCodeData());
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {   
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        ((ServiceClassDetailsDbService)i_dbService).setCurrentCode(i_codeField.getText());
        
        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {       
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */  
    protected void writeCurrentRecord()
    { 
        String                   l_language;
        String                   l_promoPlan = null;
        Object                   l_promoPlanData;
        String                   l_currentCode;        
        MiscCodeData             l_miscCodeData;
        CmdfCustMastDefaultsData l_cmdfData;
        BoiMarket                l_market;
        ServiceClass             l_serviceClass;
        PpasCurrency             l_currency;

        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();        
        
        l_currentCode  = i_codeField.getText();
        l_serviceClass = new ServiceClass(new Integer(l_currentCode).intValue());

        l_language = ((BoiValidLanguageData)i_languageBox.getSelectedItem())
                                                  .getInternalValidLanguageData().getCode();

        l_currency = ((BoiCurrencyFormatData)i_currencyBox.getSelectedItem())
                                                  .getInternalCurrencyFormatData().getCurrency();

        l_promoPlanData = i_promotionBox.getSelectedItem().toString();
        if (!l_promoPlanData.equals(""))
        {
            l_promoPlan = ((BoiPromoPlanData)i_promotionBox.getSelectedItem()).
                                                 getInternalPromoPlanData().getPromoPlan();
        }

        l_cmdfData = new CmdfCustMastDefaultsData(null,
                                                  l_market.getMarket(),
                                                  l_serviceClass,
                                                  l_language,
                                                  null,
                                                  null,  
                                                  ' ', 
                                                  l_currency,
                                                  l_promoPlan);
        
        ((ServiceClassDetailsDbService)i_dbService).setServiceClassDetailsData(l_cmdfData);

        l_miscCodeData = new MiscCodeData(l_market.getMarket(),
                                          ServiceClassDetailsDbService.C_MISC_TABLE_SERVICE_CLASS,
                                          i_codeField.getText(),
                                          i_descriptionField.getText(),
                                          " ");
                                                       
        ((ServiceClassDetailsDbService)i_dbService).setMiscCodeData(new BoiMiscCodeData(l_miscCodeData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {        
        String          l_code;
        BoiMiscCodeData l_selectedItem;
        Object          l_object;
        
        l_object = i_keyDataComboBox.getSelectedItem();
        if (l_object instanceof BoiMiscCodeData)
        {
            l_selectedItem = (BoiMiscCodeData)l_object;
            l_code = l_selectedItem.getInternalMiscCodeData().getCode();
            ((ServiceClassDetailsDbService)i_dbService).setCurrentCode(l_code);
        }            
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_currencyBox.setEnabled(true);
        i_descriptionField.setEnabled(true);

        // Ensure that any withdrawn records are removed from dropdowns.  They will have been added if
        // a service class defaults record was still assigned a withdrawn value.
        i_availablePromoPlans = ((ServiceClassDetailsDbService)i_dbService).getAvailablePromoPlanData();
        i_promotionModel = new DefaultComboBoxModel(i_availablePromoPlans);
        i_promotionBox.setModel(i_promotionModel);
        i_promoWarningLabel.setVisible(false);

        i_availableValidLanguages = ((ServiceClassDetailsDbService)i_dbService).getAvailableValidLanguageData();        
        i_languageModel = new DefaultComboBoxModel(i_availableValidLanguages);
        i_languageBox.setModel(i_languageModel);
        
        i_availableCurrencies = ((ServiceClassDetailsDbService)i_dbService).getAvailableCurrencyFormatsData();                
        i_currencyModel = new DefaultComboBoxModel(i_availableCurrencies);
        i_currencyBox.setModel(i_currencyModel);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_currencyBox.setEnabled(false);
        i_descriptionField.setEnabled(true);
        
    }    
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------      
    
    /**
     * Creates <code>JPanel</code> to hold the defined markets and Service Classes.  
     */
    private void createTopPanel()
    {
        JLabel l_marketLabel;
        JLabel l_serviceClassLabel;
        JLabel l_codeLabel;
        JLabel l_descriptionLabel;        
        
        i_topPanel = WidgetFactory.createPanel(100, 41, 0, 0);
        l_marketLabel = WidgetFactory.createLabel("Market :");
        l_serviceClassLabel = WidgetFactory.createLabel("Service Class :");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");

        i_codeField = WidgetFactory.createIntegerField(4, false);
        addValueChangedListener(i_codeField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);        
        
        i_topPanel.add(l_marketLabel, "marketLabel,1,1,14,4");
        i_topPanel.add(i_marketDataComboBox, "marketDataComboBox,16,1,35,4");                
        i_topPanel.add(l_serviceClassLabel, "serviceClassLabel,1,6,14,4");
        i_topPanel.add(i_keyDataComboBox, "keyDataComboBox,16,6,50,4");
        i_topPanel.add(l_codeLabel, "codeLabel,1,11,14,4");
        i_topPanel.add(i_codeField, "codeField,16,11,10,4");
        i_topPanel.add(l_descriptionLabel, "descriptionLabel,1,16,14,4");
        i_topPanel.add(i_descriptionField, "descriptionField,16,16,40,4");
    }

    /**
     * Creates the JPanel to hold the default configuration for the selected service class.
     */
    private void createDetailsPanel()
    {
        JLabel l_languageLabel;
        JLabel l_currencyLabel;
        JLabel l_promotionLabel;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 46, 0, 0);
        
        l_languageLabel = WidgetFactory.createLabel("Language :");    
        i_detailsPanel.add(l_languageLabel, "languageLabel,1,10,14,4");
  
        i_languageModel = new DefaultComboBoxModel();
        i_languageBox = WidgetFactory.createComboBox(i_languageModel);
        addValueChangedListener(i_languageBox);        
        i_detailsPanel.add(i_languageBox, "languageBox,16,10,35,4");

        l_promotionLabel = WidgetFactory.createLabel("Promotion Plan :");
        i_detailsPanel.add(l_promotionLabel, "promotionLabel,1,20,14,4");

        i_promotionModel = new DefaultComboBoxModel();        
        i_promotionBox = WidgetFactory.createComboBox(i_promotionModel);
        addValueChangedListener(i_promotionBox);
        i_promotionBox.addItemListener(this);
        i_detailsPanel.add(i_promotionBox, "promotionBox,16,20,40,4");

        i_promoWarningLabel = WidgetFactory.createWarningLabel(
                                                       "Warning: This promotion plan is no longer valid.");
        i_detailsPanel.add(i_promoWarningLabel, "promoWarningLabel,58,20,40,4");
        i_promoWarningLabel.setVisible(false);
                
        l_currencyLabel = WidgetFactory.createLabel("Currency :");
        i_detailsPanel.add(l_currencyLabel, "currencyLabel,1,30,14,4");

        i_currencyModel = new DefaultComboBoxModel();         
        i_currencyBox = WidgetFactory.createComboBox(i_currencyModel);
        addValueChangedListener(i_currencyBox);
        i_detailsPanel.add(i_currencyBox, "currencyBox,16,30,8,4");
        i_currencyBox.setEnabled(false);
    }
    
    /**
     * Refreshes the promotion plan combo box.  
     * If the currently selected record holds a promotion plan which is withdrawn or obsolete, 
     * then this promo plan has to be added to the combo box since the combo normally only
     * shows available values.
     * @param p_currentPromoPlanData Currently selected promotion plan.
     */
    private void refreshPromoPlanCombo(PrplPromoPlanData p_currentPromoPlanData)
    {
        // Ensure that any withdrawn promo plans are removed from dropdown.  
        i_availablePromoPlans = ((ServiceClassDetailsDbService)i_dbService).getAvailablePromoPlanData();
        i_promoPlansForCombo = (Vector)i_availablePromoPlans.clone();

        // Check whether current promo plan is withdrawn or obsolete and add to dropdown if this is the case. 
        if ( p_currentPromoPlanData != null &&
             (p_currentPromoPlanData.isDeleted() || p_currentPromoPlanData.isObsolete()) )
        {
            i_promoPlansForCombo.add(new BoiPromoPlanData(p_currentPromoPlanData));
        }

        i_promotionModel = new DefaultComboBoxModel(i_promoPlansForCombo);
        i_promotionBox.setModel(i_promotionModel);

        if (p_currentPromoPlanData == null)
        {
            i_promotionBox.setSelectedItem("");
            i_promoWarningLabel.setVisible(false);
        }
        else
        {
            i_promotionBox.setSelectedItem(new BoiPromoPlanData(p_currentPromoPlanData));
        }
    }

    /**
     * Refreshes the language combo box.  
     * If the currently selected record holds a language which is withdrawn, 
     * then this language has to be added to the combo box since the combo normally only
     * shows available values.
     * @param p_currentLanguageData Currently selected language.
     */
    private void refreshLanguageCombo(ValaValidLanguageData p_currentLanguageData)
    {
        // Ensure that any withdrawn languages are removed from dropdown.  
        i_availableValidLanguages = ((ServiceClassDetailsDbService)i_dbService).getAvailableValidLanguageData();
        i_languagesForCombo = (Vector)i_availableValidLanguages.clone();

        // Check whether current language is withdrawn and add to dropdown if this is the case. 
        if ( p_currentLanguageData != null &&
             p_currentLanguageData.isDeleted())
        {
            i_languagesForCombo.add(new BoiValidLanguageData(p_currentLanguageData));
        }

        i_languageModel = new DefaultComboBoxModel(i_languagesForCombo);
        i_languageBox.setModel(i_languageModel);

        if (p_currentLanguageData != null)
        {
            i_languageBox.setSelectedItem(new BoiValidLanguageData(p_currentLanguageData));
        }
    }

    /**
     * Refreshes the currency combo box.  
     * If the currently selected record holds a currency which is withdrawn, 
     * then this currency has to be added to the combo box since the combo normally only
     * shows available values.
     * @param p_currentCurrencyData Currently selected currency.
     */
    private void refreshCurrencyCombo(CufmCurrencyFormatsData p_currentCurrencyData)
    {
        // Ensure that any withdrawn currencies are removed from dropdown.  
        i_availableCurrencies = ((ServiceClassDetailsDbService)i_dbService).getAvailableCurrencyFormatsData();
        i_currenciesForCombo = (Vector)i_availableCurrencies.clone();

        // Check whether current currency is withdrawn and add to dropdown if this is the case. 
        if ( p_currentCurrencyData != null &&
             p_currentCurrencyData.isDeleted())
        {
            i_currenciesForCombo.add(new BoiCurrencyFormatData(p_currentCurrencyData));
        }

        i_currencyModel = new DefaultComboBoxModel(i_currenciesForCombo);
        i_currencyBox.setModel(i_currencyModel);

        if (p_currentCurrencyData != null)
        {
            i_currencyBox.setSelectedItem(new BoiCurrencyFormatData(p_currentCurrencyData));
        }
    }
}
