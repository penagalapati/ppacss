////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiAccumulatorData.java
//    DATE            :       04-Jan-2005
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#1067/5383
//
//    COPYRIGHT       :       Atos Origin 2005
//
//    DESCRIPTION     :       Wrapper for AccfAccumulatorData class 
//                            within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorData;

/** Wrapper for accumulator class within BOI. */
public class BoiAccumulatorData extends DataObject
{
    /** Basic accumulator data object */
    private AccfAccumulatorData i_accumulatorData;

    /**
     * Simple constructor.
     * @param p_accumulatorData Accumulator data object to wrapper.
     */
    public BoiAccumulatorData(AccfAccumulatorData p_accumulatorData)
    {
        i_accumulatorData = p_accumulatorData;
    }

    /**
     * Return wrappered accumulator data object.
     * @return Wrappered accumulator data object.
     */
    public AccfAccumulatorData getInternalAccumulatorData()
    {
        return i_accumulatorData;
    }

    /**
     * Compares two accumulator data objects and returns true if they equate.
     * @param p_accumulatorData Accumulator data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_accumulatorData)
    {
        boolean             l_return = false;
        AccfAccumulatorData l_accumulatorData = null;
        
        if (p_accumulatorData != null && p_accumulatorData instanceof BoiAccumulatorData)
        {
            l_accumulatorData = ((BoiAccumulatorData)p_accumulatorData).getInternalAccumulatorData();

            if (i_accumulatorData.getMarket().equals(l_accumulatorData.getMarket())
                && i_accumulatorData.getServiceClass().equals(l_accumulatorData.getServiceClass())
                && i_accumulatorData.getId() == l_accumulatorData.getId())
            {
                l_return = true;
            }
        }
        return l_return;
    }

    /**
     * Returns accumulator data object as a String for display in BOI.
     * @return Accumulator data object as a String.
     */
    public String toString()
    {
        String l_displayString = null;
        
        if (i_accumulatorData != null)
        {
            l_displayString = new String(i_accumulatorData.getId() + " - "
                                      + i_accumulatorData.getDescription());
        }
        
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_accumulatorData.getId();
    }
}