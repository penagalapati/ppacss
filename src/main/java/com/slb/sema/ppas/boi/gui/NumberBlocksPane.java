////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NumberBlocksPane.java
//      DATE            :       28-Oct-2003
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpacLon#104/1162
//
//      COPYRIGHT       :       SchlumbergerSema 2003
//
//      DESCRIPTION     :       Number Block Maintenance screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
///16/02/06 | M Erskine  | Get routing method and validate | PpacLon#1879/7933
//          |            | new range for all of nubl       |
//          |            | rather than for current market  |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.dbservice.NumberBlocksDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.NublNumberBlockData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Number Block Maintenance Screen. 
 */
public class NumberBlocksPane extends BusinessConfigBasePane implements PropertyChangeListener, ItemListener
{
    /** Available markets vector. */
    private Vector i_markets;    
    
    /** Array of number block data objects for the currently selected market. */
    private NublNumberBlockData[] i_definedNumberBlocksCurrentMarket;
    
    /** Text field to hold the number block start msisdn. */
    private JFormattedTextField i_startNumber;
    
    /** Text field to hold the number block end msisdn. */
    private JFormattedTextField i_endNumber;
    
    /** Combo box to hold the configured SDP Ids. */
    private JComboBox i_sdpIdComboBox;

    /** Vector of available SDP ids. */
    private Vector i_availableSdpIds;

    /** Combo box model for SDP ids. */
    private DefaultComboBoxModel i_sdpIdModel;
    
    /** Panel to hold the market, new number block, and the buttons. */
    private JPanel i_detailsPanel;
    
    /** Panel to hold the defined number blocks table. */
    private JPanel i_definedBlocksPanel;
    
    /** Table to hold the number blocks. */
    private JTable i_table;
    
    /** An array of the configured number blocks for the selected market. */
    private String[][] i_data;
    
    /** Table model to hold table data and column names. */
    private StringTableModel i_stringTableModel;
    
    /** Routing method: Number Range OR SDP Id. */
    private String i_routingMethod;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------     
    
    /** 
     * Simple constructor. 
     * @param p_context A reference to the <code>BoiContext</code> object
     */
    public NumberBlocksPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new NumberBlocksDbService((BoiContext)i_context);        
        
        super.init();        
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------        
    
    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {   
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }    
        
    /**
     * Listens for modifications to the start and end number fields.
     * @param p_evt Property change event.
     */
    public void propertyChange(PropertyChangeEvent p_evt)
    {
        if (p_evt.getNewValue() != null)
        {
            i_state = C_STATE_NEW_RECORD;
        }
    }

    /**
     * Listens for modifications to the SDP id combo.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        i_state = C_STATE_NEW_RECORD;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods 
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_startNumber.setValue(null);
        i_endNumber.setValue(null);
        i_routingMethod = ((NumberBlocksDbService)i_dbService).getRoutingMethod();
        setSdpIdComboState();
        
        // Set state to Initial such that screen will be in initial state after reset action.
        i_state = C_STATE_INITIAL;
    }   
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Number Blocks", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                          BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_NUMBER_BLOCKS_SCREEN), 
                                          i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedBlocksPanel();
        setSdpIdComboState();
        
        i_mainPanel.add(i_helpComboBox, "numberBlocksHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,30");
        i_mainPanel.add(i_definedBlocksPanel, "definedBlocksPanel,1,37,100,64");
    }

    /**
     * Not required - there is no refresh functionality for this screen.
     */
    protected void refreshCurrentRecord()
    {
        // Not required.
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((NumberBlocksDbService)i_dbService).getAvailableMarketData();  
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((NumberBlocksDbService)i_dbService).
                                                 getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);         
               
        i_availableSdpIds = ((NumberBlocksDbService)i_dbService).getAvailableSdpIds();
        i_sdpIdModel = new DefaultComboBoxModel(i_availableSdpIds);
        i_sdpIdComboBox.setModel(i_sdpIdModel);
        
        refreshListData();
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedNumberBlocksCurrentMarket = ((NumberBlocksDbService)i_dbService).getAllNumberBlocksForMarket();
        populateDefinedBlocksTable();
    }

    /**
     * Not required - there is no key combo box for this screen.
     */
    protected void resetSelectionForKeyCombo()
    {
        // Not required.
    }

    /**
     * Not required
     * @return true.
     */
    protected boolean validateKeyData()
    {   
        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {       
        String  l_newStartNumberString;
        String  l_newEndNumberString;
        long    l_newStartNumberAsLong = 0;
        long    l_newEndNumberAsLong = 0;
        String  l_nublStartNumber;
        String  l_nublEndNumber;
        String  l_messageString = null;
        NublNumberBlockData[] l_allDefinedNumberBlocks = null;

        l_newStartNumberString = i_startNumber.getText();
        l_newEndNumberString = i_endNumber.getText();
        
        if (l_newStartNumberString.equals("")) 
        {
            i_startNumber.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Start Number cannot be blank");
            return false;
        }
        
        if (l_newEndNumberString.equals(""))
        {
            i_endNumber.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "End Number cannot be blank");
            return false;
        }
        
        if (l_newStartNumberString.length() != l_newEndNumberString.length())
        {
            i_startNumber.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "The Start Number must be the same length as the End Number");
            return false;
        }
        
        if (l_newStartNumberString.compareTo(l_newEndNumberString) > 0)
        {
            i_startNumber.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "The Start Number must not be greater than the End Number");
            return false;
        }

        l_newStartNumberAsLong = Long.parseLong(l_newStartNumberString);
        l_newEndNumberAsLong = Long.parseLong(l_newEndNumberString);
        l_allDefinedNumberBlocks = ((NumberBlocksDbService)i_dbService).getAllNumberBlocks();
                    
        // Check number blocks do not overlap.
        // Note: It is possible that records will overlap if more than one operator is
        // inserting records on the same market at the same time.
        for (int i=0; i < l_allDefinedNumberBlocks.length; i++)
        {
            l_nublStartNumber = l_allDefinedNumberBlocks[i].getNublStartNumberString();
            l_nublEndNumber = l_allDefinedNumberBlocks[i].getNublEndNumberString();

            if ( l_nublStartNumber.length() == l_newStartNumberString.length() &&
                 (Long.parseLong(l_nublStartNumber) <= l_newEndNumberAsLong) &&
                 (Long.parseLong(l_nublEndNumber) >= l_newStartNumberAsLong) )
            {
                i_startNumber.requestFocusInWindow();
                
                if (l_allDefinedNumberBlocks[i].getMarket().equals(
                       ((BoiMarket)i_marketDataComboBox.getSelectedItem()).getMarket()))
                {
                    l_messageString = "Numbers blocks must not overlap";
                }
                else
                {
                    l_messageString = "Overlap with number block in Market: " + 
                                          l_allDefinedNumberBlocks[i].getMarket().getDisplayName();
                }
                
                displayMessageDialog(
                   i_contentPane,
                   l_messageString);
                
                return false;
            }
        }

        return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */  
    protected void writeCurrentRecord()
    { 
        BoiMarket           l_market;
        NublNumberBlockData l_nublData;
        String              l_sdpId = "";

        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();
        
        if (!i_sdpIdComboBox.getSelectedItem().toString().equals(""))
        {
            l_sdpId = ((BoiSdpIdData)i_sdpIdComboBox.getSelectedItem()).getInternalSdpIdData().getScpId();
        }

        l_nublData = new NublNumberBlockData(null,
                                             l_market.getMarket(),
                                             i_startNumber.getText(),
                                             i_endNumber.getText(),
                                             l_sdpId,
                                             null,  
                                             null);
        
        ((NumberBlocksDbService)i_dbService).setCurrentNumberBlock(l_nublData);
    }

    /**
     * Writes key screen data to screen data object.
     */  
    protected void writeKeyData()
    {        
        // Nothing to do
    }

    //-------------------------------------------------------------------------
    // Private methods 
    //-------------------------------------------------------------------------    

    /**
     * Creation of a panel to display the selected Market, the Start Mobile number field and
     * the End Mobile number field. 
     */
    private void createDetailsPanel()
    {
        JLabel l_marketLabel;
        JLabel l_startLabel;
        JLabel l_endLabel;
        JLabel l_sdpIdLabel;

        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 30, 0, 0);

        l_marketLabel = WidgetFactory.createLabel("Market:");
        l_startLabel = WidgetFactory.createLabel("Start Number:");
        l_endLabel = WidgetFactory.createLabel("End Number:");
        l_sdpIdLabel = WidgetFactory.createLabel("SDP Id:");

        i_startNumber = WidgetFactory.createMsisdnField(15);
        i_endNumber = WidgetFactory.createMsisdnField(15);
        i_sdpIdModel = new DefaultComboBoxModel();
        i_sdpIdComboBox = WidgetFactory.createComboBox(i_sdpIdModel);

        // Add listeners to editable fields such that state is changed from 
        // Initial to New Record whenever a modification is made. 
        i_startNumber.addPropertyChangeListener("value", this);
        i_endNumber.addPropertyChangeListener("value", this);
        i_sdpIdComboBox.addItemListener(this);

        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketBox,17,1,35,4");
        i_detailsPanel.add(l_startLabel, "startLabel,1,6,15,4");
        i_detailsPanel.add(i_startNumber, "startNumber,17,6,20,4");
        i_detailsPanel.add(l_endLabel, "endLabel,1,11,15,4");
        i_detailsPanel.add(i_endNumber, "endNumber,17,11,20,4");
        i_detailsPanel.add(l_sdpIdLabel, "sdpIdLabel,1,16,15,4");
        i_detailsPanel.add(i_sdpIdComboBox, "sdpIdComboBox,17,16,40,4");

        // Add buttons to screen that were created in superclass. Rename update button first.
        i_updateButton.setText("AddBlock");
        i_detailsPanel.add(i_updateButton, "addBlockButton,1,25,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,17,25,15,5");
    }

    /**
     * Method used to perform the basic setup of the screen.
     */
    private void createDefinedBlocksPanel()
    {
        String[] l_columnNames = {"Start Number", "End Number", "SDP Id"};
        
        i_definedBlocksPanel = WidgetFactory.createPanel("Configured Number Blocks", 100, 64, 0, 0);

        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table = WidgetFactory.createTable(150, 350, i_stringTableModel, this);

        i_definedBlocksPanel.add(new JScrollPane(
                                 i_table,
                                 JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                 JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED),
                                 "i_table,1,1,70,64");
    } 
   
    /**
     * Populates the Number Blocks table.
     */
    private void populateDefinedBlocksTable()
    {
        int l_numRecords = 0;
        int l_arraySize = 0;
        
        l_numRecords = i_definedNumberBlocksCurrentMarket.length -2;
        l_arraySize = ((l_numRecords > 23) ? l_numRecords : 24);
        i_data = new String[l_arraySize][3];

        for (int i = 0; i < i_definedNumberBlocksCurrentMarket.length; i++)
        {
            i_data[i][0] = i_definedNumberBlocksCurrentMarket[i].getNublStartNumberString();
            i_data[i][1] = i_definedNumberBlocksCurrentMarket[i].getNublEndNumberString();
            i_data[i][2] = i_definedNumberBlocksCurrentMarket[i].getSdpId();
        }

        i_stringTableModel.setData(i_data);
    }
    
    /**
     * Method to disable the SDP Id combo box depending on
     * whether number range routing is configured.
     */
    private void setSdpIdComboState()
    {
        i_sdpIdComboBox.setSelectedItem("");
        
        if (i_routingMethod != null)
        {
            if (i_routingMethod.equals("SDP"))
            {
                // Disable Sdp Id Combo Box, since Number Range Routing is not configured.
                
                i_sdpIdComboBox.setEnabled(false);
            }
        }
    }
}
