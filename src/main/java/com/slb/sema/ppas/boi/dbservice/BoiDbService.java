////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       BoiDbService.java
//  DATE            :       2-Jul-2004
//  AUTHOR          :       Martin Brister
//  REFERENCE       :       PpacLon#338/3075
//
//  COPYRIGHT       :       WM-data 2005
//
//  DESCRIPTION     :       Database access superclass for BOI screens.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE    | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
// 14/7/5  | M.Brister  | Added repeat scheduling         | PpacLon#1398/6788
//         |            | functionality to submitJob.     | 
//         |            |                                 | 
//---------+------------+---------------------------------+--------------------
// 20/06/07| E Dangoor  | Use job code in job name        | PpacLon#3163/11746
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ConfCompanyConfigSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CufmCurrencyFormatsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaMstrSqlService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.idgenerator.PpasIdGeneratorException;
import com.slb.sema.ppas.util.support.Debug;

/** Database access superclass for BOI screens. */
public abstract class BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Constant for duplicate record. */
    public static final int C_DUPLICATE = 0;

    /** Constant for withdrawn record. */
    public static final int C_WITHDRAWN = 1;
    
    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_ONCE = "Do Not Repeat";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_YEARLY = "By Years";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_MONTHLY = "By Months";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_WEEKLY = "By Weeks";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_DAILY = "By Days";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_HOURLY = "By Hours";

    /** Constant for displaying frequency of job submission. */
    public static final String C_JOB_FREQ_DISPLAY_MINUTELY = "By Minutes";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_ONCE = "ONCE";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_YEARLY = "YEARLY";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_MONTHLY = "MONTHLY";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_WEEKLY = "WEEKLY";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_DAILY = "DAILY";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_HOURLY = "HOURLY";

    /** Constant for frequency of job submission. */
    public static final String C_JOB_FREQ_SUBMIT_MINUTELY = "MINUTELY";

    /** Used in calls to middleware. */
    private static final String C_CLASS_NAME = "BoiDbService";
    
    //-------------------------------------------------------------------------
    // Class attributes
    //-------------------------------------------------------------------------
    
    /** Map of job repeat frequencies held against display values. */
    private static HashMap c_repeatMap = new HashMap();

    /** Vector to hold frequencies for job submission. */
    private static Vector c_frequencies = new Vector(7);  
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** Context object for BOI */
    protected BoiContext i_context;

    /** SQL service for market data. */
    protected SrvaMstrSqlService i_marketSqlService = null;
    
    /** SQL service for currency data. */
    protected CufmCurrencyFormatsSqlService i_currencySqlService = null;

    /** SQL service for company configuration data. */
    protected ConfCompanyConfigSqlService i_companyConfigSqlService = null;
    
    /** Currently selected market. Used as key for read and delete. Not applicable to every screen. */
    protected BoiMarket i_currentMarket;
    
    /** Currently selected service class. Used as key for read and delete. Not applicable to every screen. */
    protected BoiMiscCodeData i_currentServiceClass;
    
    /** Currently selected adjustment type. Used as key for read and delete. Not applicable to every screen. */
    protected BoiMiscCodeData i_currentAdjustmentType;
    
    /** Operator id. */
    private String i_opid;
    
    /** User. */
    private String i_user;
    
    /** Server node. */
    private String i_node;
    
    // Class initialisation.
    static
    {
        c_frequencies.add(C_JOB_FREQ_DISPLAY_ONCE);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_MINUTELY);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_HOURLY);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_DAILY);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_WEEKLY);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_MONTHLY);
        c_frequencies.add(C_JOB_FREQ_DISPLAY_YEARLY);
        
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_ONCE, C_JOB_FREQ_SUBMIT_ONCE);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_YEARLY, C_JOB_FREQ_SUBMIT_YEARLY);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_MONTHLY, C_JOB_FREQ_SUBMIT_MONTHLY);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_WEEKLY, C_JOB_FREQ_SUBMIT_WEEKLY);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_DAILY, C_JOB_FREQ_SUBMIT_DAILY);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_HOURLY, C_JOB_FREQ_SUBMIT_HOURLY);
        c_repeatMap.put(C_JOB_FREQ_DISPLAY_MINUTELY, C_JOB_FREQ_SUBMIT_MINUTELY);
    }
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructs the database service for BOI.
     * @param p_context Context
     */
    public BoiDbService(BoiContext p_context)
    {
        i_context = p_context;
        
        i_node  = (String)(i_context.getObject("ascs.NodeName"));
        i_user  = i_context.getUsername();
        i_opid  = i_context.getOperatorUsername();
        
        i_marketSqlService = new SrvaMstrSqlService();
        i_currencySqlService = new CufmCurrencyFormatsSqlService(null);
        i_companyConfigSqlService = new ConfCompanyConfigSqlService(null);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current market. Used for record lookup in database. 
     * @param p_market Currently selected market. 
     */
    public void setCurrentMarket(BoiMarket p_market)
    {
        i_currentMarket = p_market;
    }
    
    /** 
     * Return current market. 
     * @return Currently selected market. 
     */
    public BoiMarket getCurrentMarket()
    {
        return(i_currentMarket);
    }

    /** 
     * Set current service class. Used for record lookup in database. 
     * @param p_serviceClass Currently selected service class.
     */
    public void setCurrentServiceClass(BoiMiscCodeData p_serviceClass)
    {
        i_currentServiceClass = p_serviceClass;
    }
    
    /** 
     * Return current service class.
     * @return Currently selected service class.
     */
    public BoiMiscCodeData getCurrentServiceClass()
    {
        return(i_currentServiceClass);
    }
    
    /**
     * Set current adjustment type. Used for record lookup in database.
     * @param p_adjType Currently selected adjustment type.
     */
    public void setCurrentAdjustmentType(BoiMiscCodeData p_adjType)
    {
        i_currentAdjustmentType = p_adjType;
    }
    
    /**
     * Retrieves list of frequencies for repeated job submission.
     * @return List of frequencies for repeated job submission.
     */
    public Vector getRepeatFreqs()
    {
        return c_frequencies;
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_insertData = "insertData";
    
    /**
     * Handles connection and transaction for record insertion.
     * @throws PpasServiceFailedException
     */
    public void insertData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_insertData,
                                          10000,
                                          this,
                                          null);
            insert(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_insertData,
                                              10010,
                                              this,
                                              null);
            
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_insertData,
                                        10020,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_insertData,
                               10030,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_updateData = "updateData";
    
    /**
     * Handles connection and transaction for record update.
     * @throws PpasServiceFailedException
     */
    public void updateData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_updateData,
                                          10040,
                                          this,
                                          null);
            update(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_updateData,
                                              10050,
                                              this,
                                              null);
            
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_updateData,
                                        10060,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_updateData,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_deleteData = "deleteData";
    
    /**
     * Handles connection and transaction for record deletion.
     * @throws PpasServiceFailedException
     */
    public void deleteData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_deleteData,
                                          10080,
                                          this,
                                          null);
            delete(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_deleteData,
                                              10090,
                                              this,
                                              null);
            
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_deleteData,
                                        10100,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_deleteData,
                               10110,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_readData = "readData";
    
    /**
     * Handles connection for record retrieval.
     * @throws PpasServiceFailedException
     */
    public void readData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();
            read(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_readData,
                                              10120,
                                              this,
                                              null);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_readData,
                               10130,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_readInitialData = "readInitialData";
    
    /**
     * Handles connection for initial screen data retrieval.
     * @throws PpasServiceFailedException
     */
    public void readInitialData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();
            readInitial(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_readInitialData,
                                              10140,
                                              this,
                                              null);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_readInitialData,
                               10150,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_checkForDuplicateKey = "checkForDuplicateKey";
    
    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @return boolean array: element[0] indicates if the record is a duplicate,
     *                        element[1] indicates if the record is withdrawn.
     * @throws PpasServiceFailedException
     */
    public boolean[] checkForDuplicateKey()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        boolean[]       l_flagsArray = null;
        
        try
        {
            l_connection = i_context.getConnection();
            l_flagsArray = checkForDuplicate(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_checkForDuplicateKey,
                                              10160,
                                              this,
                                              null);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_checkForDuplicateKey,
                               10170,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
        return l_flagsArray;
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_markAsAvailableData = "markAsAvailableData";
    
    /**
     * Handles connection and transaction for marking withdrawn records as available.
     * @throws PpasServiceFailedException
     */
    public void markAsAvailableData()
        throws PpasServiceFailedException
    {
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_markAsAvailableData,
                                          10180,
                                          this,
                                          null);
            markAsAvailable(l_connection);

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_markAsAvailableData,
                                              10190,
                                              this,
                                              null);
            
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_markAsAvailableData,
                                        10200,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_markAsAvailableData,
                               10210,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /** Used in calls to middleware. */
    private static final String C_METHOD_submitJob = "submitJob";
    /**
     * Submits job to the job scheduler.
     * @param p_jobType Type of job.
     * @param p_runDateTime Time at which the job is to be run.
     * @param p_jobParams Job parameter hashmap.
     * @param p_scheduleImmediate True if job is to be scheduled immediately.
     * @param p_frequency Frequency of job submission.
     * @param p_interval Interval of job submission.  eg. If frequency = DAILY and
     *                   interval = 2, job will run once every 2 days.
     * @throws PpasServiceFailedException Requested service failed exception.
     */
    protected void submitJob(String        p_jobType,
                             PpasDateTime  p_runDateTime,
                             HashMap       p_jobParams,
                             boolean       p_scheduleImmediate,
                             String        p_frequency,
                             int           p_interval)
        throws PpasServiceFailedException
    {       
        String         l_sql;
        String         l_key;
        String         l_startDate = "";
        boolean        l_statementClosed = false;
        SqlString      l_sqlString;
        int            l_indexIterator = 0;
        Set            l_paramSet;
        Iterator       l_iterator;
        JdbcConnection l_connection = null;
        JdbcStatement  l_statement;
        String         l_jobName;
        String         l_jobType = null;
        String         l_jobCode = null;
        String         l_jobId;
        String         l_repeatInterval = "";
        int            l_nextToken = 0;
        int            l_index = 0;
        Object         l_jobCodes;
        String[]       l_st;
        PpasServiceFailedException l_psE;
        
        if (Debug.on)
        {
            Debug.print( C_CLASS_NAME, 
                         10220,
                         "Entered " + C_METHOD_submitJob + "with" +
                         "\n Job type = " + p_jobType +
                         "\n Run datetime = " + p_runDateTime.toString() +
                         "\n Schedule immediate = " + p_scheduleImmediate +
                         "\n Frequency = " + p_frequency +
                         "\n Interval = " + p_interval);
        }

        try
        {
            l_jobId = i_context.getJobIdGenerator().getNextAvailableId().toString();
        }
        catch (PpasIdGeneratorException l_pIdGenE)
        {
            l_psE = new PpasServiceFailedException(
                         C_CLASS_NAME,
                         C_METHOD_submitJob,
                         10285,
                         this,
                         null,
                         0,
                         ServiceKey.get().generalFailure(),
                         l_pIdGenE);

            throw l_psE; 
        }

        l_jobCodes = (String)(i_context.getObject("ascs.boi.jobCodes"));
        l_st = ((String)l_jobCodes).split(";");
        while (( ! p_jobType.equals(l_jobType)) && (l_nextToken < l_st.length))
        {
            l_index = l_st[l_nextToken].indexOf(",");
            l_jobType = l_st[l_nextToken].substring(0,l_index);
            l_jobCode = l_st[l_nextToken].substring(l_index+1, l_st[l_nextToken].length());
            l_nextToken++;
        }

        l_jobName = l_jobCode + "_" + i_opid + "_" + l_jobId;

        if (!p_scheduleImmediate)
        {
            SqlString l_tmp = new SqlString(100, 1, "start_date => {0}, ");
            l_tmp.setOracleDateTimeWithTimeZoneParam(0, p_runDateTime);
            l_startDate = l_tmp.toString();    
        }

        if (p_interval > 0 && p_frequency != C_JOB_FREQ_DISPLAY_ONCE)
        {
            l_repeatInterval = "repeat_interval => '";
            l_repeatInterval += "FREQ=" + c_repeatMap.get(p_frequency);
            l_repeatInterval += "; INTERVAL=" + p_interval;
            l_repeatInterval += "', ";
        }

        l_sql = new String( "BEGIN " +
                            "sys.dbms_scheduler.create_job("    +
                            "job_name => '" + l_jobName + "', " +
                            "job_type => 'STORED_PROCEDURE', "  +
                            "job_action => 'submitJob', "       +
                            l_startDate                         +
                            l_repeatInterval                    +
                            "number_of_arguments => 15, "       +
                            "auto_drop => TRUE, "               +
                            "enabled => FALSE); "               +
                            "END;" );

        l_sqlString = new SqlString(200, 0, l_sql);

        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_submitJob,
                                          10240, 
                                          this,
                                          null);

            l_statement = l_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_submitJob,
                                                       10250, 
                                                       this,
                                                       null);

            l_statement.executeUpdate(C_CLASS_NAME,
                                      C_METHOD_submitJob,
                                      10260, 
                                      this,
                                      null,
                                      l_sqlString);
                                       
            addSubmitJobArgument( l_statement, l_jobName, 1, i_node   );
            addSubmitJobArgument( l_statement, l_jobName, 2, i_user   );
            addSubmitJobArgument( l_statement, l_jobName, 3, i_opid   );
            addSubmitJobArgument( l_statement, l_jobName, 4, p_jobType);
            addSubmitJobArgument( l_statement, l_jobName, 5, l_jobId  );
            
            // Populate the other arguments with values from the parameter hashmap.
            l_paramSet  = p_jobParams.keySet();
            l_iterator  = l_paramSet.iterator();
            l_index = 0;
            while (l_iterator.hasNext())
            {
                l_key = (String) l_iterator.next();
                addSubmitJobArgument(l_statement, 
                                     l_jobName, 
                                     (l_index + 6), 
                                     (l_key + "=" + p_jobParams.get(l_key)));
                l_indexIterator++;
                l_index++;
            }

            // Any arguments not populated should be created and made blank.
            for(int i = l_indexIterator; i < 10; i++)
            {
                addSubmitJobArgument(l_statement, 
                                     l_jobName, 
                                     (6 + l_index++), 
                                     "");
            }

            l_sqlString = new SqlString(50, 
                                        0,
                                        ("BEGIN DBMS_SCHEDULER.ENABLE ('" + l_jobName + "');END;"));
            
            l_statement.executeUpdate(C_CLASS_NAME,
                                      C_METHOD_submitJob,
                                      10270, 
                                      this,
                                      null,
                                      l_sqlString); 
            
            l_statementClosed = true;

            l_connection.endTransaction( C_CLASS_NAME,
                                         C_METHOD_submitJob,
                                         10280, 
                                         this,
                                         null,
                                         JdbcConnection.C_FLAG_COMMIT );
        }
        catch (PpasSqlException l_pSE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_submitJob,
                               10290,
                               this,
                               l_pSE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /** 
     * Reads a record from the relevant configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    abstract protected void read(JdbcConnection p_connection)
        throws PpasSqlException;
    
    /** 
     * Reads the first record from the relevant configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    abstract protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException;
    
    /** 
     * Updates record in the relevant configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    abstract protected void update(JdbcConnection p_connection)
        throws PpasSqlException;
    
    /** 
     * Inserts record in the relevant configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    abstract protected void insert(JdbcConnection p_connection)
        throws PpasSqlException;
    
    /** 
     * Deletes a record from the relevant configuration table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    abstract protected void delete(JdbcConnection p_connection)
        throws PpasSqlException;
        
    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection JDBC connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    abstract protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException;
    
    /**
     * Hook method which must be over-ridden in any subclass which allows records to
     * be withdrawn and re-instated, rather than physically removing the record from
     * the table.
     * @param p_connection JDBC connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // No default processing
    }
    
    /**
     * Handles SQL exceptions.
     * @param p_class Calling class.
     * @param p_method Calling method.
     * @param p_statement Statement number.
     * @param p_instance Calling object.
     * @param p_psqlE SQL exception.
     * @param p_connection JDBC connection.
     * @param p_statementClosed True if JDBC statement already closed.
     * @throws PpasServiceFailedException Requested service failed exception.
     */
    protected void handleSqlException(String           p_class,
                                      String           p_method,
                                      int              p_statement,
                                      Object           p_instance,
                                      PpasSqlException p_psqlE,
                                      JdbcConnection   p_connection,
                                      boolean          p_statementClosed)
        throws PpasServiceFailedException
    {
        PpasServiceFailedException l_psE;
        
        try
        {
            if (p_statementClosed == false)
            {
                p_connection.getStatement().close(p_class,
                                                  p_method,
                                                  p_statement,
                                                  this,
                                                  null);
            }

            p_connection.endTransaction(p_class,
                                        p_method,
                                        p_statement,
                                        p_instance,
                                        null,
                                        JdbcConnection.C_FLAG_ROLLBACK);
        }
        catch (PpasSqlException l_pSE)
        {
            // Discard this exception - we�re only interested in the first one generated.
        }

        l_psE = new PpasServiceFailedException(
                     p_class,
                     p_method,
                     p_statement,
                     p_instance,
                     null,
                     0,
                     ServiceKey.get().generalFailure(),
                     p_psqlE);

        throw l_psE; 
    }
        
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** Constant defining the method name, used in debug. */
    private static final String C_METHOD_addSubmitJobArgument = "addSubmitJobArgument";
    /**
     * Adds arguments to the database schedule job method.
     * @param p_statement JDBC statement.
     * @param p_jobName Job name in format: node_user_timestamp
     * @param p_argPostion Argument position.
     * @param p_argumentValue Value of the argument.
     * @throws PpasSqlException SQL exception.
     */
    private void addSubmitJobArgument(JdbcStatement p_statement,
                                      String        p_jobName,
                                      int           p_argPostion,
                                      String        p_argumentValue) 
        throws PpasSqlException
    {
        SqlString l_sqlString;
        String    l_addArg;
        
        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 
                        10300,
                        "Entered " + C_METHOD_addSubmitJobArgument + "with " +
                        "\n Job name = " + p_jobName + 
                        "\n Argument postition = " + p_argPostion +
                        "\n Argument value = " + p_argumentValue);
        }

        l_addArg =
            "BEGIN " +
              "DBMS_SCHEDULER.SET_JOB_ARGUMENT_VALUE ("   +
                "job_name => '"         + p_jobName       + "', "  +
                "argument_position => " + p_argPostion    + ", "   +
                "argument_value => '"   + p_argumentValue + "'); " +
            "END;";

        l_sqlString = new SqlString(200, 0, l_addArg);
        
        // TODO - handle number of rows returned?
        p_statement.executeUpdate( C_CLASS_NAME,
                                   C_METHOD_addSubmitJobArgument,
                                   10310,
                                   this,
                                   null,
                                   l_sqlString );

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 
                        10320,
                        "Leaving " + C_METHOD_addSubmitJobArgument);
        }                           
    }

}
