////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiAccumulatedBonusSchemeData.java
//      DATE            :       5-July-2007
//      AUTHOR          :       Steven James
//      REFERENCE       :       PpacLon#
//                              PRD_ASCS00_GEN_CA_129
//      COPYRIGHT       :       LogicaCMG 2007
//
//      DESCRIPTION     :       Wrapper for BonaBonusAccumParamsData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// yy/mm/dd | <Name>     | <Description>                   | PpacLon#XXXX/YYYY
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;

/** Wrapper for BonaBonusAccumParamsData class within BOI. */
public class BoiAccumulatedBonusSchemeData extends DataObject
{
    /** Wrappered data object */
    private BonaBonusAccumParamsData i_accumBonusSchemeData;
    
    /**
     * Simple constructor.
     * @param p_accumBonusSchemeData Bonus scheme data object to wrapper.
     */
    public BoiAccumulatedBonusSchemeData(BonaBonusAccumParamsData p_accumBonusSchemeData)
    {
        i_accumBonusSchemeData = p_accumBonusSchemeData;
    }
    
    /**
     * Return wrappered accumulated bonus scheme data object.
     * @return Wrappered accumulated bonus scheme data object.
     */
    public BonaBonusAccumParamsData getInternalBonusSchemeData()
    {
        return i_accumBonusSchemeData;
    }
    
    /**
     * Compares two accumulated bonus scheme data objects and returns true if they equate.
     * @param p_accumBonusSchemeData Bonus scheme data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_accumBonusSchemeData)
    {
        boolean          l_return = false;
        BonaBonusAccumParamsData l_accumBonusSchemeData = null;
        
        if (p_accumBonusSchemeData != null &&
            p_accumBonusSchemeData instanceof BoiAccumulatedBonusSchemeData)
        {
            l_accumBonusSchemeData =
                ((BoiAccumulatedBonusSchemeData)p_accumBonusSchemeData).getInternalBonusSchemeData();
            
            if (i_accumBonusSchemeData.getBonusSchemeId().equals(l_accumBonusSchemeData.getBonusSchemeId()))
            {
                l_return = true;
            }
        }
        return l_return;
    }
    
    /**
     * Returns bonus scheme data object as a String for display in BOI.
     * @return Bonus scheme data object as a string.
     */
    public String toString()
    {
        return i_accumBonusSchemeData.getBonusSchemeId();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_accumBonusSchemeData.getBonusSchemeId().length();
    }
}