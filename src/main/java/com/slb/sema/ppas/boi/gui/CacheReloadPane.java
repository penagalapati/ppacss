////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       CacheReloadPane.java
//    DATE            :       7-Dec-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#825/5227
//                            PRD_ASCS00_ANA_FD_15
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       BOI Business Configuration Cache Reload screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Don�t display vertical scollbar | PpacLon#2066/10439
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 22/03/07 | R.Grimshaw | Add releoad feature licence     | PpacLon#2066/10439
//          |            | cache button.                   |
//----------+------------+---------------------------------+--------------------
// 19/04/07 | R.Grimshaw | Rename "Cache Reload" button to | PpacLon#3065        
//          |            | "Reload Business Config Cache". |
//          |            | Tidied up screen layout and made|
//          |            | node and process columns the    | 
//          |            | same size.                      |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.util.support.Debug;

/** BOI Business Configuration Cache Reload screen. */
public class CacheReloadPane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    /** Used in calls to middleware. */
    private static final String C_CLASS_NAME = "CacheReloadPane";
    
    /** Used to find the node name in the process URL String. */
    private static final int C_PROCESS_URL_TOKENIZER_NODE_ELEMENT = 4;
    
    /** Used to find the process name in the process URL String. */
    private static final int C_PROCESS_URL_TOKENIZER_PROCESS_ELEMENT = 5;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Main panel for the screen. */
    private JPanel i_mainPanel;
    
    /** Panel for displaying active processes. */
    private JPanel i_processPanel;
    
    /** Button to reload business config cache. */
    private JButton i_reloadButton;
    
    /** Button to reload feature licence config cache. */
    private JButton i_reloadFeatureButton;
    
    /** Button to refresh process list. */
    private JButton i_refreshButton;
    
    /** Table containing processes with business config cache. */
    private JTable i_table = null;

    /** Column names for table of active processes. */
    private String[] i_columnNames = {"Node", "Process"};

    /** Data array for table of active processes. */
    private String[][] i_data = null;

    /** Data model for table of active processes. */
    private StringTableModel i_stringTableModel = null;
    
    /** BOI server host.*/
    private String i_serverHost = null;
    
    /** BOI server port.*/
    private String i_serverPort = null;
    
    /** URL string to retrieve process listing. */
    private String i_processListingUrlString;

    /** URL to retrieve process listing. */
    private URL i_processListingUrl = null;

    /** URL string to reload cache. */
    private String i_cacheReloadUrlString;
    
    /** URL string to reload feature licence cache. */
    private String i_featureCacheReloadUrlString;

    /** URL to reload cache. */
    private URL i_cacheReloadUrl = null;
    
    /** URL to reload feature licence cache. */
    private URL i_featureCacheReloadUrl = null;

    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** 
     * Standard constructor. 
     * @param p_context A reference to the BoiContext
     */
    public CacheReloadPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        paintScreen();

        i_contentPane = i_mainPanel;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_mainPanel.setFocusCycleRoot(true);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /**
     * Handles keyboard events for the screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;
        
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_refreshButton)
            {
                i_refreshButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_reloadFeatureButton)
            {
                i_reloadFeatureButton.doClick();
            }
            else
            {
                i_reloadButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }
    
    /**
     * Handles button events.
     * @param p_actionEvent Action event.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        Object l_source = null;
        
        l_source = p_actionEvent.getSource();
        
        if (l_source == i_reloadButton)
        {
            reloadCache(false);
        }
        else if (l_source == i_reloadFeatureButton)
        {
            reloadCache(true);
        }
        else if (l_source == i_refreshButton)
        {
            refreshTable();
        }
    }
    
    /**
     * Handles combo box events.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent) {}
    
    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_reloadButton.requestFocusInWindow())
        {
            System.out.println("Error in requestFocusInWindow!");
        }
    }
    
    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        initialiseUrls();
        refreshTable();
    }
    
    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event){}
    
    /** 
     * Handles GUI events.
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(GuiEvent p_event){}
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Paints the screen.
     */
    private void paintScreen()
    {
        BoiHelpListener l_helpComboListener;

        i_mainPanel = WidgetFactory.createMainPanel("Cache Reload", 100, 100, 0, 0);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createComboBox(
                                            BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_CACHE_RELOAD_SCREEN), 
                                            l_helpComboListener);
        i_helpComboBox.setFocusable(false);
        i_mainPanel.add(i_helpComboBox, "cacheReloadHelpComboBox,60,1,40,4");
                              
        createProcessPanel();
        i_mainPanel.add(i_processPanel, "processPanel,1,6,100,89");

        i_reloadButton = WidgetFactory.createButton("Reload Config Cache", this, true);
        i_reloadFeatureButton = WidgetFactory.createButton("Reload Feature Cache", this, true);
        i_refreshButton = WidgetFactory.createButton("Refresh", this, false);
        
        i_mainPanel.add(i_reloadButton, "reloadButton,1,96,20,5");
        i_mainPanel.add(i_reloadFeatureButton, "reloadFeatureButton,22,96,20,5");
        i_mainPanel.add(i_refreshButton, "refreshButton,43,96,20,5");
    }
    
    /** Creates the process listing panel. */
    private void createProcessPanel()
    {   
        i_processPanel = WidgetFactory.createPanel(100, 100, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        i_table.setFocusable(false);
        
        // Diable the table component in order to make the rows unselectable.
        i_table.setEnabled(false);

        i_processPanel.add(new JScrollPane(i_table,
                                           JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                           JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED),
                                           "i_table,1,1,60,100");
    }
    
    /**
     * Refreshes the table containing processes with business config cache. 
     */
    private void refreshTable()
    {
        ArrayList l_processes = null;
        int       l_numRecords = 0;
        int       l_arraySize = 0;
        String    l_urlString;
        String[]  l_tokenizer = null;
        
        l_processes = getCacheReloadRemoteObjs();

        l_numRecords = l_processes.size();
        l_arraySize = ((l_numRecords > 33) ? l_numRecords : 34);
        i_data = new String[l_arraySize][2];

        for (int i = 0; i < l_numRecords; i++)
        {
            l_urlString = (String)l_processes.get(i);
            l_tokenizer = l_urlString.split("[/]");
            
            i_data[i][0] = l_tokenizer[C_PROCESS_URL_TOKENIZER_NODE_ELEMENT];
            i_data[i][1] = l_tokenizer[C_PROCESS_URL_TOKENIZER_PROCESS_ELEMENT];
        }

        i_stringTableModel.setData(i_data);
    }

    /**
     * Accesses the RMI services servlet to retrieve a list of remote objects which
     * can be used to reload business config data.
     * @return List of remote objects which can be used to reload business config data.
     */
    private ArrayList getCacheReloadRemoteObjs()
    {
        URLConnection  l_urlConnection;
        InputStream    l_inputStream;
        BufferedReader l_bufferedReader;
        String[]       l_tokenizer = null;
        int            l_nextToken = 0;
        String         l_line = "";
        String         l_param;
        String         l_status = "";
        ArrayList      l_cacheReloadRemoteObjs = new ArrayList();
        
        try
        {
            l_urlConnection = i_processListingUrl.openConnection();
            l_urlConnection.connect();
            l_inputStream = l_urlConnection.getInputStream();
            l_bufferedReader = new BufferedReader(new InputStreamReader(l_inputStream));             
            l_line = l_bufferedReader.readLine();

            if (Debug.on)
            {
                Debug.print(C_CLASS_NAME, 10010, "Read line:" + l_line + ".");
            }
            
            if (l_line != null && l_line != "")
            {
                // Expecting response of the form "status=SUCCESS&process1=Xxx&process2=Yyy..."
                // or "status=FAILEDLOOKUP".
                l_tokenizer = l_line.split("[=& ]");
                
                while (l_nextToken < l_tokenizer.length)
                {
                    l_param = l_tokenizer[l_nextToken++];
                    if (l_param.equals("status"))
                    {
                        l_status = l_tokenizer[l_nextToken++];
                    }
                    else if (l_param.substring(0,7).equals("process"))
                    {
                        l_cacheReloadRemoteObjs.add(l_tokenizer[l_nextToken++]);
                    }
                }
            }

            if (!l_status.equals("SUCCESS"))
            {
                displayMessageDialog(i_contentPane,
                                     "Process lookup failed.\nContact system administrator.");
            }
        }
        catch (IOException l_e)
        {
            handleException(l_e, "Process lookup failed.\nContact system administrator.");
        }
        
        if (Debug.on)
        {
            if (l_status.equals("SUCCESS"))
            {
                Debug.print(C_CLASS_NAME, 10020, "Process lookup successful");
            }
            else if (l_status.equals("FAILEDLOOKUP"))
            {
                Debug.print(C_CLASS_NAME, 10030, "Process lookup failed");
            }
            else
            {
                Debug.print(C_CLASS_NAME, 10040, "Failed before lookup");
            }
        }
        
        return l_cacheReloadRemoteObjs;
    }
    
    /**
     * Sends a request to the server to reload the business config cache for all processes
     * which use it.
     * @param p_featureCache true if feature licence cache should be reloaded otherwise business
     * config cache is reloaded.
     */
    private void reloadCache(boolean p_featureCache)
    {
        URLConnection  l_urlConnection;
        InputStream    l_inputStream;
        BufferedReader l_bufferedReader;
        String[]       l_tokenizer = null;
        int            l_nextToken = 0;
        String         l_line = "";
        String         l_param;
        String         l_status = "";
        int            l_response;
        ArrayList      l_failedReloads = new ArrayList();
        StringBuffer   l_failedReloadsMsg;
        
        l_response = displayConfirmDialog(
                          i_contentPane,
                          "Do you wish to reload the " + (p_featureCache ? "feature licence" : "business configuration") + " cache?");
        
        if (l_response == JOptionPane.YES_OPTION)
        {
            try
            {
                if (p_featureCache)
                {
                    l_urlConnection = i_featureCacheReloadUrl.openConnection();
                }
                else
                {
                    l_urlConnection = i_cacheReloadUrl.openConnection();
                }
                
                l_urlConnection.connect();
                l_inputStream = l_urlConnection.getInputStream();
                l_bufferedReader = new BufferedReader(new InputStreamReader(l_inputStream));             
                l_line = l_bufferedReader.readLine();
                
                if (Debug.on)
                {
                    Debug.print(C_CLASS_NAME, 10040, "Read line:" + l_line + ".");
                }
                
                if (l_line != null && l_line != "")
                {
                    // Expecting response of the form "status=SUCCESS" 
                    // or "status=RELOADFAILED&process1=Xxx&process2=Yyy...".
                    l_tokenizer = l_line.split("[=& ]");
                    
                    while (l_nextToken < l_tokenizer.length)
                    {
                        l_param = l_tokenizer[l_nextToken++];
                        if (l_param.equals("status"))
                        {
                            l_status = l_tokenizer[l_nextToken++];
                        }
                        else if (l_param.substring(0,7).equals("process"))
                        {
                            l_failedReloads.add(l_tokenizer[l_nextToken++]);
                        }
                    }
                }
                
                if (l_status.equals("SUCCESS"))
                {
                    displayMessageDialog(i_contentPane,
                    "Cache reload successful");
                }
                else if (l_status.equals("FAILEDRELOAD"))
                {
                    l_failedReloadsMsg = new StringBuffer();
                    for (int i=0; i<l_failedReloads.size(); i++)
                    {
                        l_failedReloadsMsg.append(l_failedReloads.get(i) + "\n");
                    }
                    
                    displayMessageDialog(i_contentPane,
                                         "Cache reload failed for the following processes:\n" +
                                         l_failedReloadsMsg +
                                         "\nContact system administrator.");
                }
                else if (l_status.equals("FAILEDLOOKUP"))
                {
                    displayMessageDialog(i_contentPane,
                    "Cache reload unavailable. \nContact system administrator.");
                }
                else
                {
                    displayMessageDialog(i_contentPane,
                    "Cache reload failed.\nContact system administrator.");
                }
            }
            catch (IOException l_e)
            {
                handleException(l_e, "Cache reload failed.\nContact system administrator.");
            }
        }
    }
    
    /**
     * Initialises the URLs required to retrieve a process listing and 
     * request a cache reload.
     */
    private void initialiseUrls()
    {
        i_serverHost = (String)i_context.getMandatoryObject("applet.server.host");
        i_serverPort = (String)i_context.getMandatoryObject("applet.server.login.port");
        
        i_processListingUrlString = "http://" + i_serverHost + ":" + i_serverPort +
        "/ascs/boi/RmiServices?command=GET_PROCESSES" +
        "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
        "&password=" + ((BoiContext)i_context).getOperatorPassword();
        
        i_cacheReloadUrlString = "http://" + i_serverHost + ":" + i_serverPort +
        "/ascs/boi/RmiServices?command=RELOAD_CACHE" +
        "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
        "&password=" + ((BoiContext)i_context).getOperatorPassword();
        
        i_featureCacheReloadUrlString = "http://" + i_serverHost + ":" + i_serverPort +
        "/ascs/boi/RmiServices?command=RELOAD_FEATURE_CACHE" +
        "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
        "&password=" + ((BoiContext)i_context).getOperatorPassword();
        
        try
        {
            i_processListingUrl = new URL(i_processListingUrlString);
            i_cacheReloadUrl = new URL(i_cacheReloadUrlString);
            i_featureCacheReloadUrl = new URL(i_featureCacheReloadUrlString);
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
    }
}