////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ServiceOfferingPaneUT.java
//      DATE            :       21-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1216/5578
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for ServiceOfferingPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SeofServiceOfferingSqlService;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for ServiceOfferingPane. */
public class ServiceOfferingPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Constant for service offering number. */
    private static final String C_SEOF_NUMBER = "31";
    
    /** Constant for service offering description. */
    private static final String C_SEOF_DESCRIPTION = "Service offering description";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Service Offering screen. */
    private ServiceOfferingPane i_serviceOfferingPane;
    
    /** SQL service for Service Offering data. */
    private SeofServiceOfferingSqlService i_seofSqlService = null;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Text field to display the selected service offering's description. */
    private ValidatedJTextField i_descriptionField;

    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public ServiceOfferingPaneUT(String p_title)
    {
        super(p_title);
        
        // Update test data now such that it is picked up by readInitialData
        i_seofSqlService = new SeofServiceOfferingSqlService(null, null);
        updateServiceOfferingRecord(C_SEOF_NUMBER, C_SEOF_DESCRIPTION);
        
        i_serviceOfferingPane = new ServiceOfferingPane(c_context);
        super.init(i_serviceOfferingPane);
        
        i_serviceOfferingPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesComboBox");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(ServiceOfferingPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to select and update a record 
     *     through the Service Offering screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        BoiServiceOfferingData l_seofData;
        
        beginOfTest("ServiceOfferingPane test");
        
        // *********************************************************************
        say("Selecting record.");
        // *********************************************************************
        
        l_seofData = createServiceOfferingData(C_SEOF_NUMBER, C_SEOF_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_serviceOfferingPane, i_keyDataCombo, l_seofData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, "New description");
        doUpdate(i_serviceOfferingPane, i_updateButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to set up anything required by each test. 
     */
    protected void setUp()
    {
        // Do nothing
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiServiceOfferingData object with the supplied data.
     * @param p_serviceOfferingNumber Service Offering Number.
     * @param p_serviceOfferingDesc Service Offering Description.
     * @return BoiServiceOfferingData object created from the supplied data.
     */
    private BoiServiceOfferingData createServiceOfferingData(String p_serviceOfferingNumber,
                                                             String p_serviceOfferingDesc)
    {
        SeofServiceOfferingData l_seofData;
        
        l_seofData = new SeofServiceOfferingData(null,
                                                 Integer.parseInt(p_serviceOfferingNumber),
                                                 p_serviceOfferingDesc,
                                                 C_BOI_OPID,
                                                 null);
        
        return (new BoiServiceOfferingData(l_seofData));
    }
    
    /**
     * Updates a record in SEOF_SERVICE_OFFERING.
     * @param p_serviceOfferingNumber Service Offering Number.
     * @param p_serviceOfferingDesc Service Offering Description.
     */
    private void updateServiceOfferingRecord(String p_serviceOfferingNumber,
                                             String p_serviceOfferingDesc)
    {
        try
        {
            i_seofSqlService.update(null,
                                    c_context.getConnection(),
                                    C_BOI_OPID,
                                    Integer.parseInt(p_serviceOfferingNumber),
                                    p_serviceOfferingDesc);
        }
        catch (PpasSqlException l_e)
        {
            fail("Failed to update SEOF record.");
        }
    }
}
