////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       MiscCodesDbService.java
//    DATE            :       26-Jul-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#338/3340
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Miscellaneous Codes 
//                            screens.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for miscellaneous codes screens. 
 */
public class MiscCodesDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for miscellaneous code details. */
    private MiscCodeSqlService i_miscCodeSqlService = null;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService = null;

    /** Vector of miscellaneous code records. */
    private Vector i_availableMiscCodeDataV = null;
    
    /** List of available markets for display. */
    private Vector i_availableMarketsV = null;

    /** Miscellaneous code data. */
    private BoiMiscCodeData i_miscCodeData = null;

    /** Operator's default market. */
    private BoiMarket i_defaultMarket = null;
    
    /** Currently selected miscellaneous code. Used as key for read and delete. */
    private String i_currentCode = null;
    
    /** Identifies type of data to be maintained. Part of key for srva_misc_codes table.*/
    private String i_miscTable = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     * @param p_miscTable Identifies type of data to be maintained. 
     *                    Part of key for srva_misc_codes table. 
     */
    public MiscCodesDbService(BoiContext p_context, String p_miscTable)
    {
        super(p_context);
        i_miscTable = p_miscTable;
        i_miscCodeSqlService = new MiscCodeSqlService(null, null);
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);
        i_availableMarketsV = new Vector(5);
        i_availableMiscCodeDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current miscellaneous code. Used for record lookup in database. 
     * @param p_code Currently selected miscellaneous code. 
     */
    public void setCurrentCode(String p_code)
    {
        i_currentCode = p_code;
    }

    /** 
     * Return miscellaneous code data currently being worked on. 
     * @return Miscellaneous code data object.
     */
    public BoiMiscCodeData getMiscCodeData()
    {
        return i_miscCodeData;
    }

    /** 
     * Set miscellaneous code data currently being worked on. 
     * @param p_miscCodeData Miscellaneous code data object.
     */
    public void setMiscCodeData(BoiMiscCodeData p_miscCodeData)
    {
        i_miscCodeData = p_miscCodeData;
    }

    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }

    /** 
     * Return available markets vector. 
     * @return Vector of available markets.
     */
    public Vector getAvailableMarkets()
    {
        return i_availableMarketsV;
    }

    /** 
     * Return miscellaneous code data vector. 
     * @return Vector of available miscellaneous code data records.
     */
    public Vector getAvailableMiscCodeData()
    {
        return i_availableMiscCodeDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        MiscCodeDataSet l_miscCodeDataSet = null;
        MiscCodeData    l_miscCodeData = null;
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_miscCodeData = l_miscCodeDataSet.getRawDataFromKey(i_miscTable,
                                                             i_currentMarket.getMarket(),
                                                             i_currentCode);
        i_miscCodeData = new BoiMiscCodeData(l_miscCodeData);
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        Market[]        l_availableMarketArray = null;
        Market          l_globalMarket = null;
        SrvaMstrDataSet l_srvaMstrDataSet = null;

        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            l_globalMarket = new Market(0, 0, "Global Market");
            i_availableMarketsV.add(0, new BoiMarket(l_globalMarket));
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;
        }
        
        refreshMiscCodeDataVector(p_connection,
                                  i_currentMarket.getMarket());
    }
    
    /** 
     * Inserts record into the srva_misc_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.insert(null,
                                    p_connection,
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSrva(),
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSloc(),
                                    i_miscCodeData.getInternalMiscCodeData().getCode(),
                                    i_miscCodeData.getInternalMiscCodeData().getDescription(),
                                    i_context.getOperatorUsername(),
                                    i_miscTable);

        refreshMiscCodeDataVector(p_connection,
                                  i_miscCodeData.getInternalMiscCodeData().getMarket());
    }

    /** 
     * Updates record in the srva_misc_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.update(null,
                                    p_connection,
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSrva(),
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSloc(),
                                    i_miscCodeData.getInternalMiscCodeData().getCode(),
                                    i_miscCodeData.getInternalMiscCodeData().getDescription(),
                                    i_context.getOperatorUsername(),
                                    i_miscTable);

        refreshMiscCodeDataVector(p_connection,
                                  i_miscCodeData.getInternalMiscCodeData().getMarket());
    }
    
    /** 
     * Deletes record from the srva_misc_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.delete(null,
                                    p_connection,
                                    i_currentMarket.getMarket().getSrva(),
                                    i_currentMarket.getMarket().getSloc(),
                                    i_currentCode,
                                    i_context.getOperatorUsername(),
                                    i_miscTable);

        refreshMiscCodeDataVector(p_connection,
                                  i_currentMarket.getMarket());
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception. 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        MiscCodeDataSet l_miscCodeDataSet = null;
        MiscCodeData    l_dbMiscCodeData = null;
        boolean[]       l_flagsArray = new boolean[2];
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_dbMiscCodeData = l_miscCodeDataSet.getRawDataFromKey(i_miscTable,
                                                               i_currentMarket.getMarket(),
                                                               i_currentCode); 
                               
        if (l_dbMiscCodeData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbMiscCodeData.getIsCurrent())
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the srva_misc_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception. 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.markAsAvailable(null,
                                             p_connection,
                                             i_miscCodeData.getInternalMiscCodeData().getMarket().getSrva(),
                                             i_miscCodeData.getInternalMiscCodeData().getMarket().getSloc(),
                                             i_miscCodeData.getInternalMiscCodeData().getCode(),
                                             i_context.getOperatorUsername(),
                                             i_miscTable);

        refreshMiscCodeDataVector(p_connection,
                                  i_miscCodeData.getInternalMiscCodeData().getMarket());
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception.
     */    
    private void refreshMiscCodeDataVector(JdbcConnection p_connection,
                                           Market         p_market)
        throws PpasSqlException
    {
        MiscCodeDataSet l_miscCodeDataSet = null;
        MiscCodeDataSet l_availableMiscCodeDataSet = null;
        Vector          l_miscCodeV = null;
        MiscCodeData    l_miscCodeData = null;
        
        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getRawAvailableMiscCodeData(p_market, i_miscTable);
        l_miscCodeV = l_availableMiscCodeDataSet.getDataV();
        
        i_availableMiscCodeDataV.addElement(new String(""));
        i_availableMiscCodeDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData = (MiscCodeData)l_miscCodeV.get(i);
            i_availableMiscCodeDataV.addElement(new BoiMiscCodeData(l_miscCodeData));
        }
    }

    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @param p_connection Database connection.
     * @return The default market for the current Operator.
     * @throws PpasSqlException SQL-related exception. 
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}
