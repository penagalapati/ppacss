////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChavChannelDbService.java
//      DATE            :       22-Dec-2006
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PpacLon#2822/10659
//                              PRD_ASCS00_GEN_CA104
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Database access class for Channel definition screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/01/07 |M.Tornqvist | Filter the possible choices in  | PpacLon#2874/10868
//          |            | the the group combibox, so that |
//          |            | the hierachy only can be 2 level|
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiChavChannelData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ChavChannelSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

public class ChavChannelDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for channel code details. */
    private ChavChannelSqlService i_channelSqlService     = null;
    
    /** Vector of Tubs channel records. */
    private Vector                i_availableChannelDataV = null;

    /** Vector of Defined groups records. */
    private Vector                i_availableGroupsV      = null;

    /** TUBS Channel data. */
    private BoiChavChannelData    i_channelData           = null;

    /** Currently selected channel code. Used as key for read and delete. */
    private String                i_currentCode           = "";
    
    private Vector                i_masterChannels        = null;
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public ChavChannelDbService(BoiContext p_context)
    {
        super(p_context);
        i_channelSqlService     = new ChavChannelSqlService(null);
        i_availableChannelDataV = new Vector(5);
        i_availableGroupsV      = new Vector(5);
        i_masterChannels        = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current channel code. Used for record lookup in database. 
     * @param p_code Currently selected channel code. 
     */
    public void setCurrentCode(String p_code)   
    {
        i_currentCode = p_code;
    }

    /** 
     * Return channel data currently being worked on. 
     * @return Channel data object.
     */
    public BoiChavChannelData getChannelData()
    {
        return i_channelData;
    }

    /** 
     * Set channel data currently being worked on. 
     * @param p_channelData Channel data object.
     */
    public void setChannelData(BoiChavChannelData p_channelData)
    {
        i_channelData = p_channelData;
    }

    /** 
     * Return Channel data. 
     * @return Vector of available Channel data records.
     */
    public Vector getAvailableChannelData()
    {
        return i_availableChannelDataV;
    }
    
    /**
     * Return valid Group data.
     * @return Vector of available group data.
     */
    public Vector getAvailableGroups( String p_channel )
    {
        boolean            l_isMaster    = false;
        boolean            l_hasChildren = false;
        BoiChavChannelData l_tmpChannel  = null;
        
        i_availableGroupsV.removeAllElements();
        
    	// Find out type of channel, master or child
        for ( int i= 0; i < i_masterChannels.size(); i++)
    	{
    		l_tmpChannel = (BoiChavChannelData)i_masterChannels.get(i);
    		if ( p_channel != null && p_channel.equals(l_tmpChannel.getInternalChannelData().getChannelId()) )
    		{
    			l_isMaster = true;
    			if (l_tmpChannel.hasChildren())
    			{
    				l_hasChildren = true;
    			}
        		break;
    		}
    	}
        
        i_availableGroupsV.addElement("");
        for ( int i=0; i < i_masterChannels.size(); i++)
        {
        	l_tmpChannel = (BoiChavChannelData)i_masterChannels.get(i);
        	if (p_channel == null )
        	{
          		i_availableGroupsV.addElement(l_tmpChannel.getInternalChannelData().getChannelId());  		
        	}
        	else if ( l_isMaster )
        	{
        		if (l_hasChildren )
        		{
        	    	// Has children - cannot be grouped to another channel
        		    continue;
        	    }
        	    else if ( !l_tmpChannel.getInternalChannelData().getChannelId().equals(p_channel))
        	    {
        		    // Shall not be possible to choose itself
        		    i_availableGroupsV.addElement(l_tmpChannel.getInternalChannelData().getChannelId());
        		    continue;
        	    }
        	}
        	else
        	{
        		// Not a master - can move to another group
        		i_availableGroupsV.addElement(l_tmpChannel.getInternalChannelData().getChannelId());
        		continue;
        	}
        }
        
    	return i_availableGroupsV;
    }
    
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        ChavChannelDataSet l_channelDataSet = i_channelSqlService.readAll(null, p_connection);

        i_channelData = new BoiChavChannelData(l_channelDataSet.getRecord(i_currentCode));            
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshTubsChannelDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {    	    	
        i_channelSqlService.insert(null,
                                   p_connection,
                                   i_channelData.getInternalChannelData().getChannelId(),
                                   i_channelData.getInternalChannelData().getDesc(),
                                   i_channelData.getInternalChannelData().getChannelGroup(),
                                   i_context.getOperatorUsername());        
        
        refreshTubsChannelDataVector(p_connection);
    }

    /** 
     * Updates record in the CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_channelSqlService.update(null,
                                   p_connection,
                                   i_context.getOperatorUsername(),                                
                                   i_channelData.getInternalChannelData().getChannelId(),                               
                                   i_channelData.getInternalChannelData().getChannelGroup(),
                                   i_channelData.getInternalChannelData().getDesc());

        refreshTubsChannelDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_channelSqlService.delete(null,
                                   p_connection,
                                   i_context.getOperatorUsername(),
                                   i_channelData.getInternalChannelData().getChannelId());
              
        refreshTubsChannelDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        ChavChannelDataSet l_channelDataSet = null;
        ChavChannelData    l_dbChannelData  = null;
        boolean[]          l_flagsArray     = new boolean[2];
        
        l_channelDataSet = i_channelSqlService.readAll(null, p_connection);
        l_dbChannelData  = l_channelDataSet.getRecord(i_currentCode);
            
        if (l_dbChannelData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = l_dbChannelData.isDeleted();
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_channelSqlService.markAsAvailable(null,
                                            p_connection,
                                            i_currentCode,
                                            i_context.getOperatorUsername());

        refreshTubsChannelDataVector(p_connection);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Refreshes the available channel data vector from the 
     * CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshTubsChannelDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        ChavChannelDataSet l_dataSet = null;
        ChavChannelData[]  l_array   = null;
        BoiChavChannelData l_channel = null;
        
        i_availableChannelDataV.removeAllElements();
        i_availableGroupsV.removeAllElements();
        i_masterChannels.removeAllElements();
        
        l_dataSet = i_channelSqlService.readAll(null, p_connection);
        l_array   = l_dataSet.getAvailableArray();
        
        i_availableChannelDataV.addElement("");
        i_availableChannelDataV.addElement("NEW RECORD");
        for (int i=0; i < l_array.length; i++)
        {
        	l_channel = new BoiChavChannelData(l_array[i]);
            i_availableChannelDataV.addElement(l_channel);
            
            // Save master channels
           	if ( (l_array[i].getChannelGroup() == null) ||
           		 ((l_array[i].getChannelGroup() != null) && (l_array[i].getChannelGroup().equals(""))) )
           	{
           		// It is a master
           	    i_masterChannels.addElement(l_channel);
           	}
        }
        
        // Update number of children for the masters
        for ( int i=0; i < l_array.length; i++)
        {
        	if ( (l_array[i].getChannelGroup() != null) && !(l_array[i].getChannelGroup().equals("")) )
        	{
        		// It is a child - find the master channel and increase the number of children
        		for ( int j=0; j < i_masterChannels.size(); j++ )
        		{
        			BoiChavChannelData l_tmp = (BoiChavChannelData)i_masterChannels.get(j);
        			if ( (l_array[i].getChannelGroup().equals(l_tmp.getInternalChannelData().getChannelId())) )
        			{
         				l_tmp.flagChildren();
        				break;
        			}
        		}        	    
        	}
        }
    }

}
