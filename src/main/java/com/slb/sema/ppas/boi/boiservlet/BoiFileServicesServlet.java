////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiFileServicesServlet.java
//      DATE            :       12-August-2004
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PRD_ASCS00_DEV_SS_88
//                              PpacLon#112/3568
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Servlet to provide file services for BOI
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 19/05/06 |L. Lundberg | The command 'BATCHINDATADIR' is | PpacLon#2291/8841
//          |            | added in the 'doGet()' method in|
//          |            | order to obtain the configured  |
//          |            | batch indata directory (used by |
//          |            | the Fraud Returns batch).       |
//----------+------------+---------------------------------+--------------------
// 18/07/06 | M Erskine  | Addition of Refill batch job    | PpacLon#2479/9401
//----------+------------+---------------------------------+--------------------
// 18/07/07 | Ian James  | Subscriber Segmentation Change  | PpacLon#3183/11848
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boiservlet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.LogInOutOrigin;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.SimpleLogInOut;
import com.slb.sema.ppas.is.isapi.PpasSessionService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides file services for BOI. A request is received from the BOI applet, the appropriate
 * operation is attempted, and a http response is sent with the relevant information (including
 * whether or not it was successful). 
 * <p>One service provides a listing of the available Batch files for a certain job type, another 
 * changes a filename extention from .DAT to .SCH (used by BOI on sheduling a job), and there is 
 * one to generate a file of MSISDNs over a given range.
 */
public class BoiFileServicesServlet extends HttpServlet
{
    // N.B. We validate the username and password of the operator for each request.
    
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------
    
    /** Used in calls to middleware. */
    private static final String C_CLASS_NAME = "BoiFileServicesServlet";
    
    /** Name of request parameter for old Sdp Ip address. */
    private static final String C_PARAM_OLD_SDP_IP = "oldSdpIp";
    
    /** Name of request parameter for new Sdp Ip address. */
    private static final String C_PARAM_NEW_SDP_IP = "newSdpIp";
    
    /** Name of request parameter for file names. */
    private static final String C_PARAM_FILE_NAME = "fileName";
    
    /** Name of request parameter for number ranges. */
    private static final String C_PARAM_NUMBER_RANGE = "numb";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
        
    /** Container for objects used in a Ppas context. */
    protected PpasContext        i_context = null;
    
    /** File system path to where the batch files are. */
    private String             i_pathToBatchFileDir = null;
    
    /** Reference to a PpasSessionService object. */
    private PpasSessionService i_ppasSessionService = null;
    
    /** Reference to a PpasProperties object. */
    private PpasProperties     i_properties = null;
    
    /** References a logger. */
    private Logger             i_logger = null;
    
    /** References to a JdbcConnection.  */
    private JdbcConnection          i_jdbcConn = null ;


    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** Used for calls to middleware. */ 
    private static final String C_METHOD_init = "init";    
    /**
     * Fetches required items from properties files and the servlet context.
     * @throws ServletException
     */
    public void init()
        throws ServletException
    {   
        ServletContext          l_servletContext = null;
        
        try
        {               
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                        null, C_CLASS_NAME, 10030, this,
                        "Entered " + C_METHOD_init);
            }
                                                      
            i_pathToBatchFileDir = 
                System.getProperty("ascs.localRoot",
                        System.getProperty("user.home").concat(File.separator + "ascs"))
                        + File.separator + "batchfiles" + File.separator + "data" + File.separator;
            
            l_servletContext = getServletContext();
            
            i_ppasSessionService =
                (PpasSessionService)l_servletContext.
                    getAttribute("com.slb.sema.ppas.is.isapi.PpasSessionService");
            
            i_properties =
                (PpasProperties)l_servletContext.
                    getAttribute("com.slb.sema.ppas.common.support.PpasProperties");

            i_logger =
                (Logger)l_servletContext.
                    getAttribute("com.slb.sema.ppas.util.logging.Logger"); 
         
            i_jdbcConn = (JdbcConnection)l_servletContext.
                    getAttribute("com.slb.sema.ppas.common.sql.JdbcConnection");        
            
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                        null, C_CLASS_NAME, 10400, this,
                        "Leaving " + C_METHOD_init + " successfully.");
            }
        }
        catch (Exception l_e)
        {
            if (i_logger != null)
            {
                i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                                      LoggableInterface.C_SEVERITY_ERROR));                
            }
            else
            {            
                l_e.printStackTrace();
            }
            
            throw new ServletException("Fatal Error attempting to initialise BOI File Services Servlet",
                                       l_e);
        }
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
        
    /** Used in calls to middleware. */
    private static final String C_METHOD_doGet = "doGet";
    /**
     * Overrides the method on HttpServlet to handle requests.
     * @param p_request  The http request received.
     * @param p_response The http response to send back.
     * @throws ServletException If the SDP Balancing file cannot be generated.
     */
    protected void doGet(HttpServletRequest  p_request,
                         HttpServletResponse p_response)
        throws ServletException
    {
        String                  l_userName = null;
        String                  l_password = null;
        String                  l_command  = null;
        PrintWriter             l_writer   = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_MWARE,
                    PpasDebug.C_ST_START,
                    null, C_CLASS_NAME, 10400, this,
                    "Entered " + C_METHOD_doGet);
        }
        
        l_userName = p_request.getParameter("userName");
        l_password = p_request.getParameter("password");
        l_command  = p_request.getParameter("command");
        
        SimpleLogInOut l_logInOut = new SimpleLogInOut();
        LogInOutOrigin l_origin   = new LogInOutOrigin(PpasSessionService.C_BOI_USER,
                                                       i_properties.getProcessName(),
                                                       "Unknown");
        
        try
        {
            // Login to ASCS. This ensures the user taht is logged into a BOI client is really authorised to
            // use ASCS in the server (otherwise naughty people could send a sneaky request).
            i_ppasSessionService.login(null, 0, 20000, l_logInOut, l_origin, l_userName, l_password);
            
            // If we get this far we've been authenticated 
            if (PpasDebug.on)
            {
                PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE,
                        null, C_CLASS_NAME, 10400, this,
                        "Authenticated in method " + C_METHOD_doGet + " successfully.");
            }
            
            if (l_command.equals("RENAME"))
            {
                String l_oldFileName = null;
                File   l_fileToRename = null;
                String l_newFileName = null;
                
                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                
                l_oldFileName = p_request.getParameter("fileName");
                l_fileToRename = new File(i_pathToBatchFileDir + l_oldFileName);
                
                if (l_fileToRename.isFile())
                {   
                    if (l_oldFileName.endsWith("DAT") || l_oldFileName.endsWith("XML"))
                    {
                        l_newFileName = l_oldFileName.replaceFirst("\\.[A-Z]{3}", ".SCH");
                        
                        l_fileToRename.renameTo(new File(i_pathToBatchFileDir + l_newFileName));
                    }

                    l_writer.println("status=SUCCESS"); 
                    
                    // TODO: Include the latest listing of files? Or should
                    // the BOI screen just remove this file from the list it has?
                }
                else
                {
                    // File doesn't exist or isn't a normal file
                    //System.out.println("File doesn't exist or isn't a normal file");
                    l_writer.println("status=FAILURE&reason=File doesn't exist");
                }
                
                l_writer.flush();
            }
            else if (l_command.equals("FILELOOKUP"))
            {
                String   l_partialFileName = null;
                File     l_dir = null;
                String   l_fileListing[] = null; 
                
                l_partialFileName = p_request.getParameter("fileName");
                l_dir = new File(i_pathToBatchFileDir);
                l_fileListing = l_dir.list();
                
//                if (l_dir.isDirectory())
//                {
//                    for ( int i = 0; i < l_fileListing.length; i++)
//                    {
//                        //Iterate through files
//                        System.out.println("    " + l_fileListing[i]);
//                    }
//                }
                
                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                // Build the response String containing the file listing
                StringBuffer l_fileListingResponse = null;
                
                l_fileListingResponse = new StringBuffer();
                l_fileListingResponse.append("status=SUCCESS");
                
                for (int i = 0; i < l_fileListing.length; i++)
                {
                    if ((l_fileListing[i].length() >= l_partialFileName.length()) &&
                        l_fileListing[i].substring(0,l_partialFileName.length()).equals(l_partialFileName))
                    {
                        l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                    }
                }
                l_writer.println(l_fileListingResponse.toString());
                
                l_writer.flush();               
            }
            else if (l_command.equals("BATCHINDATADIR"))
            {
                String         l_batchIndataDir   = null;
                String         l_addedPropLayer   = null;
                PpasProperties l_props            = null;

                // Get the configured batch indata directory.
                l_addedPropLayer = p_request.getParameter("propLayer");
                try
                {
                    l_props = i_properties.getPropertiesWithAddedLayers(l_addedPropLayer);
                }
                catch (PpasConfigException l_ppasConfigEx)
                {
                    throw new ServletException("Error attempting to read one of the properties files " +
                                               "added by the layer '" + l_addedPropLayer + "'",
                                               l_ppasConfigEx);
                }
                l_batchIndataDir = l_props.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY,
                                                              i_pathToBatchFileDir);

                // Create the response.
                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                // Build the response String containing the batch indata directory.
                StringBuffer l_batchIndataDirResponse = new StringBuffer();
                l_batchIndataDirResponse.append("status=SUCCESS");
                l_batchIndataDirResponse.append("&indataDir=" + l_batchIndataDir);

                // Print the response.
                l_writer.println(l_batchIndataDirResponse.toString());
                l_writer.flush();               
            }
            else if (l_command.equals("DIRLIST"))
            {
                String          l_jobType        = null;
                File            l_dir            = null;
                BatchFileFilter l_filenameFilter = null;
                String[]        l_fileListing    = null;
                
                l_jobType = p_request.getParameter("jobType");

                // Create File Object. 
                // NB. This can be a file or a directory. 
                l_dir = new File(i_pathToBatchFileDir);

                // Once you have the appropriate path, you can iterate through its contents:
                // List directory
                l_filenameFilter = new BatchFileFilter();
                l_fileListing = l_dir.list(l_filenameFilter);
                
                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                // Build the response String containing the file listing
                StringBuffer l_fileListingResponse = null;
                
                l_fileListingResponse = new StringBuffer();
                l_fileListingResponse.append("status=SUCCESS");
                
                if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_DAT) ||
                            l_fileListing[i].matches(BatchConstants.C_PATTERN_SUBINST_LONG_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_SDP_BALANCING))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_PROMOTION_PLAN_CHANGE))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_SYNCH))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_SYNCH_LONG_FILENAME))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_MISC))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_UPDATE_MISC_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_CCI))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "="+ l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(
                                              BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "=" + l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_DELETION))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(
                                              BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "=" + l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_REFILL))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(
                                              BatchConstants.C_PATTERN_BATCH_REFILL_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "=" + l_fileListing[i]);
                        }
                    }
                }
                else if (l_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION))
                {
                    for (int i = 0; i < l_fileListing.length; i++)
                    {
                        if (l_fileListing[i].matches(
                                              BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_DAT))
                        {
                            l_fileListingResponse.append("&file" + i + "=" + l_fileListing[i]);
                        }
                    }
                }
                else
                {
                    // Could not find job type
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                                null, C_CLASS_NAME, 10300, this,
                                "Could not find Batch Job Type supplied in client request");
                    }
                    
                    i_logger.logMessage(
                        new LoggableEvent(
                                C_CLASS_NAME + ":" + C_METHOD_doGet 
                                    + ": Could not find Batch Job Type supplied in client request",
                                LoggableInterface.C_SEVERITY_INFO)
                        );
                }

                //System.out.println("Sending response: " + l_fileListingResponse.toString());
                l_writer.println(l_fileListingResponse.toString());
                
                l_writer.flush();
            }
            else if (l_command.equals("GENERATEFILE"))
            {
                BufferedWriter     l_tmpFile        = null;
                String             l_startMsisdnStr = null;
                long               l_startMsisdn    = 0;
                String             l_endMsisdnStr   = null;
                long               l_endMsisdn      = 0;
                int                l_msisdnLength   = 0;
                String             l_sdpId          = null;
                String             l_filePath       = null;
                String             l_fileName       = null;
               
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_TRACE,
                            null, C_CLASS_NAME, 12050, this,
                            "Attempting to generate MSISDN file");
                }

                p_response.setStatus(HttpServletResponse.SC_OK);
                l_writer = p_response.getWriter();

                // If we get this far we've been authenticated 
                l_startMsisdnStr = p_request.getParameter("startMsisdn");
                l_endMsisdnStr   = p_request.getParameter("endMsisdn");
                l_sdpId = p_request.getParameter("sdpId");
                
                l_msisdnLength   = l_startMsisdnStr.length();
                
                l_startMsisdn = Long.parseLong(l_startMsisdnStr);
                l_endMsisdn = Long.parseLong(l_endMsisdnStr);
                               
                l_filePath = i_properties.getTrimmedProperty(
                                 "com.slb.sema.ppas.boi.gui.MsisdnGeneration.directory");
                
                l_fileName = "MSISDN_LOAD_" + DatePatch.getDateTimeNow().toString_yyyyMMddHHmmss() + ".DAT";
                l_fileName = l_fileName.replaceAll(":", "");
                
                l_tmpFile = new BufferedWriter(new FileWriter(l_filePath + File.separator + l_fileName));
                
                for (long i = l_startMsisdn; i <= l_endMsisdn; i++)
                {
                    String l_msisdn = String.valueOf(i);
                    
                    while (l_msisdn.length() < l_msisdnLength)
                    {
                        l_msisdn = "0" + l_msisdn;
                    }
                    
                    try
                    {
                        if ( validateMsisdn(l_msisdn)== 0)                            
                        {                           
                            while (l_msisdn.length() < 15)
                            {
                                l_msisdn = " " + l_msisdn;                                
                            }     
                                l_tmpFile.write(l_msisdn + l_sdpId + "\n");                                                  
                        }
                    }
                    catch (PpasSqlException e)
                    {
                        i_logger.logMessage(e);
                    }
                }
                try 
                
                {
                    l_tmpFile.flush();
                    l_tmpFile.close();
                }  
                catch (IOException l_e) 
                {
                    i_logger.logMessage(
                                        new LoggableEvent(
                                                          C_CLASS_NAME + ":" + C_METHOD_doGet 
                                                          + ": Exception : Cannot close stream writer to the MSISDN generation file: " 
                                                          + " !\n" 
                                                          + l_e.getMessage(),
                                                          LoggableInterface.C_SEVERITY_ERROR)
                    );
                }
                
                l_writer.println("status=SUCCESS"); 
                l_writer.flush();
                
            }
            else if (l_command.equals("GENERATEBALANCESDPSFILE"))
            {
                generateBalanceSdpsFile(p_request, p_response);
            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_START,
                        null, C_CLASS_NAME, 10400, this,
                        "Leaving " + C_METHOD_doGet);
            }
        }
        catch (PpasServiceException l_e)
        {
            i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                    LoggableInterface.C_SEVERITY_ERROR));
                                
            if (l_writer != null)
            {
                // If we get this far the username/password combo was invalid.
                l_writer.println("status=FALSE");
                l_writer.flush();
            }
        }
        catch (IOException l_e)
        {
            i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                        LoggableInterface.C_SEVERITY_ERROR));
                                
            if (l_writer != null)
            {
                // If we get this far the username/password combo was invalid.
                l_writer.println("status=FALSE");
                l_writer.flush();
            }
        }
        finally
        {
            if (l_logInOut.isLoggedIn())
            {
                // Make sure they log out.
                i_ppasSessionService.logout(null, 20000, l_logInOut, l_origin);
            }
        }
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Generates a file for input into the SDP according to a specification referenced in 
     * PRD_ASCS00_GEN_CA_56.
     * 
     * @param p_request  The HTTP request.
     * @param p_response The HTTP response.
     * @throws PpasServiceException
     * @throws IOException
     * @throws ServletException
     * 
     */
    private void generateBalanceSdpsFile(HttpServletRequest  p_request,
                                         HttpServletResponse p_response)
        throws PpasServiceException, IOException, ServletException
    {
            BufferedWriter     l_tmpFile        = null;
            long               l_startMsisdn    = 0;
            long               l_endMsisdn      = 0;
            int                l_msisdnLength   = 0;
            String             l_filePath       = null;
            String             l_fileName       = null;
            PrintWriter        l_writer         = null;
            String             l_oldSdpIp       = null;
            String             l_newSdpIp       = null;
            Vector             l_numberRanges   = null;
            String             l_line           = null;
            String[]           l_tokenizer      = null;
            int                l_nextToken      = 0;
            String             l_param          = null;
            long               l_numBodyRows    = 0;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_MWARE,
                        PpasDebug.C_ST_TRACE,
                        null, C_CLASS_NAME, 12050, this,
                        "Attempting to generate Balance SDPs file");
            }

            try
            {
                p_response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                l_writer = p_response.getWriter();

                l_fileName = p_request.getParameter(C_PARAM_FILE_NAME);
                l_oldSdpIp = p_request.getParameter(C_PARAM_OLD_SDP_IP);
                l_newSdpIp = p_request.getParameter(C_PARAM_NEW_SDP_IP);
                l_line = p_request.getQueryString();
            
                l_filePath = i_properties.getTrimmedProperty(
                             "com.slb.sema.ppas.boi.gui.BalanceSdps.directory");
            
                l_tmpFile = new BufferedWriter(new FileWriter(l_filePath + File.separator + l_fileName));

            
                l_tokenizer = l_line.split("[=& ]");
                l_numberRanges = new Vector();
                
                while (l_nextToken < l_tokenizer.length)
                {
                    l_param = l_tokenizer[l_nextToken++];
                    if (C_PARAM_NUMBER_RANGE.equals(l_param.substring(0,4)))
                    {
                        l_numberRanges.add(l_tokenizer[l_nextToken++]);
                    }
                }
            
                l_tmpFile.write("0," + l_oldSdpIp + "," + l_newSdpIp + "\n");
                
                for (int j = 0; j < l_numberRanges.size(); j++)
                {
                    String[] l_msisdns = ((String)l_numberRanges.elementAt(j)).split("-");
                    l_msisdnLength   = l_msisdns[0].length();
                    l_startMsisdn = Long.parseLong(l_msisdns[0]);
                    l_endMsisdn = Long.parseLong(l_msisdns[1]);
            
                    for (long i = l_startMsisdn; i <= l_endMsisdn; i++)
                    {
                        String l_msisdn = String.valueOf(i);
                
                        while (l_msisdn.length() < l_msisdnLength)
                        {
                            l_msisdn = "0" + l_msisdn;
                        }

                        l_tmpFile.write("1," + l_msisdn + "\n");
                    }
                    l_numBodyRows += (l_endMsisdn - l_startMsisdn + 1);
                }

                // Add 1 for the header
                l_tmpFile.write("100," + (l_numBodyRows + 1));
                p_response.setStatus(HttpServletResponse.SC_OK);

            }
            catch (RuntimeException l_e)
            {
                if (i_logger != null)
                {
                    i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                                          LoggableInterface.C_SEVERITY_ERROR));                
                }
                else
                {            
                    l_e.printStackTrace();
                }
                
                throw new ServletException("Error attempting to generate Balance SDPs input file",
                                           l_e);
            }
            finally
            {
                try 
                {
                    l_tmpFile.flush();
                    l_tmpFile.close();
                } 
                catch (IOException l_e) 
                {
                    i_logger.logMessage(
                        new LoggableEvent(
                            C_CLASS_NAME + ":" + C_METHOD_doGet 
                                + ": Exception : Cannot close stream writer to the Balance SDPs file: " 
                                + " !\n" 
                                + l_e.getMessage(),
                            LoggableInterface.C_SEVERITY_ERROR)
                     );
                }
                l_writer.println("status=SUCCESS"); 
                l_writer.flush();
            }
    }

    /**
     * Check for the existence of each MSISDN in table MSIS_MSISDN.
     * 
     * @param p_msisdn The mobile number to be validated.
     * @throws PpasSqlException
     * @return 0 if the mobile number is not exist in MSIS_MSISDN table.
     */
    private int validateMsisdn (String       p_msisdn)
    throws PpasSqlException
    {
        SqlString       l_sql           = null;
        JdbcResultSet   l_resultSet      = null;
        JdbcStatement   l_statement      = null;
        int             l_count = 0;
        
        l_sql = new SqlString( 500, 
                               1, 
                               "SELECT msis_mobile_number from msis_msisdn " +
                               "where msis_mobile_number = {0}" );
        
        l_sql.setStringParam(0, p_msisdn);
        
        try
        {              
            l_statement = i_jdbcConn.createStatement( C_CLASS_NAME,
                                                      "validateMsisdn",
                                                      12052,
                                                      this,
                                                      null );
            
            l_resultSet = l_statement.executeQuery( C_CLASS_NAME,
                                                    "validateMsisdn",
                                                    12054,
                                                    this,
                                                    null,
                                                    l_sql);    
            if (l_resultSet.next(12056))
            {
                l_count = 1;
            }
            
        }
        
        finally
        {
            if (l_statement != null)
            {
                if(l_resultSet != null)
                {
                    l_resultSet.close(12056);                    
                }
                l_statement.close( C_CLASS_NAME, "validateMsisdn", 22052, this, null);
            }
            
        }
        return l_count;
    }
    
    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------
    
    /**
     * Inner class to filter the directory listing of available batch input files. This will filter out
     * all files not ending with a valid extension, before a detailed check is done on the filename format.
     */
    public class BatchFileFilter implements FilenameFilter
    {   
        public boolean accept(File p_dir, String p_name)
        {
            return (p_name.toUpperCase().endsWith(BatchConstants.C_EXTENSION_INDATA_FILE) || 
                    p_name.toUpperCase().endsWith(BatchConstants.C_EXTENSION_XML_FILE));
        }

    }
}
