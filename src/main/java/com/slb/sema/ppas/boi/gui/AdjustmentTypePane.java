////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentTypePane.java
//      DATE            :       21 November 2005
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PpacLon#1755/7279
//                              PRD_ASCS00_GEN_CA_xx
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to maintain new Adjustment Types.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;


import javax.swing.JTextField;

import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/*******************************************************************************
* BOI screen to maintain the Bank Codes.
*/
public class AdjustmentTypePane extends GenericMiscCodesPane
{
    /** Name of the Misc code. Value is {@value}. */
    private static String C_ADJUSTMENT_TYPE = "ADT";

    /** Title of the BOI screen. Value is {@value}. */
    private static String C_MAIN_TITLE      = "Adjustment Types";

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    /** 
    * AdjustmentTypePane constructor. 
    * @param p_context A reference to the BoiContext.
    */        
    public AdjustmentTypePane(Context p_context)
    {
        super(p_context, C_ADJUSTMENT_TYPE, C_MAIN_TITLE, BoiHelpTopics.C_ADJUSTMENT_TYPE_SCREEN);
    }

    

    /**
     * Creates the code field - it can be of different types: text, integer
     * @return a text field for the details pane
     */
    protected JTextField createCodeField()
    {
        return WidgetFactory.createTextField(3);
    }    
    
 }
