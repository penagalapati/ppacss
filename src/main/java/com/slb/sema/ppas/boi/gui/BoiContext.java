////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiContext.java
//    DATE            :       24-Sep-2003
//    AUTHOR          :       Marek Vonka
//    REFERENCE       :       PpacLon#104/1162
//                            PRD_ASCS00_DEV_SS_088
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Created on start up of the BOI application to hold 
//                            objects and configuration values required throughout
//                            the application.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.sql.DriverManager;

import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ConfCompanyConfigData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ApacApplicationAccessSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ConfCompanyConfigSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CufmCurrencyFormatsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.idgenerator.PpasIdGeneratorException;
import com.slb.sema.ppas.common.support.idgenerator.SequentialIdManager;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * Created on start up of the BOI application to hold objects and configuration values
 * required throughout the application.
 */
public class BoiContext extends Context
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Used for calls to middleware. */
    public static final String C_CLASS_NAME = "BoiContext";
    
    /** Database type Oracle. */
    public static final int C_DBTYPE_ORACLE = 1;
    
    /** Database type Ingres. */
    public static final int C_DBTYPE_INGRES = 2;

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The database type. Has one of two possible values, C_DBTYPE_ORACLE or C_DBTYPE_INGRES. */
    private int i_dbType = C_DBTYPE_ORACLE;

    /** The database username. */
    private String i_username = "";
    
    /** The database password. */
    private String i_password = "";
    
    /** The database URL to be used to connect to the database. */
    private String i_dbString = "";
    
    /** The Operator login username. */
    private String i_operatorUsername = "";
    
    /** The Operator login password. */
    private String i_operatorPassword = "";
    
    /** The Jdbc database connection. */
    private JdbcConnection i_connection;

    /** Id generator used for job submission id. */
    private SequentialIdManager i_jobIdGenerator;
    
    /** Precision of system base currency. */
    private int i_baseCurrencyPrecision = -1;
    
    /** Flag indicating whether database driver has been registered. */
    private boolean i_driverRegistered = false;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructs an instance of this class. */
    public BoiContext()
    {
        super();        
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Sets the database type to Oracle or Ingres. The default is Oracle.
     * @param p_dbType The database type.
     */
    public void setDbType(int p_dbType)
    {
        i_dbType = p_dbType;
    }
    
    /**
     * Sets the username for logging into the database.
     * @param p_username The database login username.
     */    
    public void setUsername(String p_username)
    {
        i_username = p_username;
    }
    
    /** 
     * Get the database username.
     * @return The database username
     */
    public String getUsername()
    {
        return i_username;
    }

    /**
     * Sets the password for logging into the database.
     * @param p_password The database login password
     */
    public void setPassword(String p_password)
    {
        i_password = p_password;
    }

    /** 
     * Sets the URL for logging into the database.
     * @param p_dbString The database URL as a String
     */
    public void setDbString(String p_dbString)
    {
        i_dbString = p_dbString;
    }
    
    /**
     * Sets the value of the Operator Username that was used to log into 
     * the BOI application.
     * @param p_operatorUsername The login username.  
     */
    public void setOperatorUsername(String p_operatorUsername)
    {
        i_operatorUsername = p_operatorUsername;
    }

    /** 
     * Gets the Operator Password used to log into the BOI application.
     * @return The Operator Password as a String.
     */    
    public String getOperatorPassword()
    {
        return i_operatorPassword;
    }
    
    /**
     * Sets the value of the Operator's password that was used to log into 
     * the BOI application.
     * @param p_operatorPassword The login username.  
     */
    public void setOperatorPassword(String p_operatorPassword)
    {
        i_operatorPassword = p_operatorPassword;
    }

    /** 
     * Gets the Operator Username used to log into the BOI application.
     * @return The Operator Username as a String.
     */    
    public String getOperatorUsername()
    {
        return i_operatorUsername;
    }
    
    /** 
     * Return id generator used to obtain job submission id. 
     * @return Job id generator.
     */
    public SequentialIdManager getJobIdGenerator()
    {
        if (i_jobIdGenerator == null)
        {
            try
            {
                i_jobIdGenerator = new SequentialIdManager("js_jobid",
                                                           null,
                                                           getConnection());
            }
            catch (PpasConfigException l_ex)
            {
                l_ex.printStackTrace();
            }
            catch (PpasIdGeneratorException l_ex)
            {
                l_ex.printStackTrace();
            }
        }
        return i_jobIdGenerator;
    }

    /**
     * Returns JdbcConnection to the calling class. 
     * @return A JdbcConnection
     */
    public JdbcConnection getConnection()
    {
        return i_connection;
    }
    
    /** Used in calls to middleware. */
    private static final String C_METHOD_createConnection = "createConnection";
    /**
     * Creates a JdbcConnection.
     * @throws PpasSqlException An Exception that is thrown as a result of an error occurring in 
     *                          the execution of an SQL statement.
     */
    public void createConnection()
        throws PpasSqlException
    {
        UtilProperties l_dbInfo = new UtilProperties();
        
        if (!i_driverRegistered)
        {
            try
            {
                DriverManager.registerDriver(
                        (java.sql.Driver)(Class.forName("oracle.jdbc.OracleDriver").newInstance()));
                
                i_driverRegistered = true;
            }
            catch (Exception l_e)
            {
                PpasSqlException l_ppasSqlE = new PpasSqlException(
                        C_CLASS_NAME,
                        C_METHOD_createConnection,
                        10371,
                        this,
                        null,
                        0,
                        SqlKey.get().errorRegisteringJdbcDriver(),
                        l_e);

                throw(l_ppasSqlE);
            }
        }

        l_dbInfo.put("user", i_username);
        l_dbInfo.put("password", i_password);
        l_dbInfo.put("dbType", "oracle");
            
        i_connection = new JdbcConnection(C_CLASS_NAME,
                                          C_METHOD_createConnection,
                                          30000,
                                          this,
                                          (PpasRequest)null,
                                          (Logger)null,
                                          new InstrumentManager(),
                                          l_dbInfo,
                                          i_dbString);
    }
    
    /**
     * Closes the JDBC connection.
     */
    public void closeConnection()
    {
        if (i_connection != null)
        {
            try
            {
                i_connection.close();
            }
            catch(PpasSqlException l_pSqlE)
            {
                l_pSqlE.printStackTrace();
            }
            finally
            {
                i_connection.destroy();
                i_connection = null;
                // Ensure job id generator is recreated with new connection when user next logs in.
                i_jobIdGenerator = null;
            }
        }
    }

    /**
     * Returns the application access info for the current operator.
     * @return An <code>ApacApplicationAccessData</code> object for the current operator
     * @throws PpasSqlException An Exception that is thrown as a result of an error occurring in 
     *                          the execution of an SQL statement.
     */
    public ApacApplicationAccessData getOpidPrivilege()
        throws PpasSqlException
    {
        ApacApplicationAccessDataSet    l_apacDataSet;
        ApacApplicationAccessSqlService l_apacSqlService = new ApacApplicationAccessSqlService(null, null);
        BicsysUserfileSqlService        l_operatorSqlService = new BicsysUserfileSqlService(null, null);
        int                             l_privilege = 0;
        ApacApplicationAccessData       l_applicationAccessData = null;
        
        l_privilege = l_operatorSqlService.getOperatorData(null, getConnection(), getOperatorUsername(),
                                                           null).getPrivilegeId();
        
        l_apacDataSet = l_apacSqlService.readAll(null, getConnection());
        l_applicationAccessData = l_apacDataSet.getApplicationAccessProfile(l_privilege);
        
        return l_applicationAccessData;
    } 
    
    /**
     * Returns the Base Currency precision.
     * @return The base currency precision as an int.
     */
    public int getBaseCurrencyPrecision()
    {
        ConfCompanyConfigData         l_confCompanyConfigData;
        CufmCurrencyFormatsDataSet    l_cufmDataSet;
        CufmCurrencyFormatsData       l_cufmData;
        String                        l_baseCurrency;
        ConfCompanyConfigSqlService   l_confSqlService = new ConfCompanyConfigSqlService(null);
        CufmCurrencyFormatsSqlService l_cufmSqlService = new CufmCurrencyFormatsSqlService(null);
        
        if (i_baseCurrencyPrecision == -1)
        {
            try
            {
                l_confCompanyConfigData = l_confSqlService.readConfCompanyConfig(null, getConnection());
                l_baseCurrency = l_confCompanyConfigData.getBaseCurrency().getCurrencyCode();
                
                l_cufmDataSet = l_cufmSqlService.readCurrencyFormats(null, getConnection());
                l_cufmData = l_cufmDataSet.getCurrencyFormatData(l_baseCurrency);
                
                i_baseCurrencyPrecision = l_cufmData.getPrecision();
            }
            catch (PpasSqlException l_e)
            {    
                l_e.printStackTrace();
            }
        }
        return i_baseCurrencyPrecision;
    }
}
