////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GenericMiscCodesPane.java
//      DATE            :       10-June-2003
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/2644
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Class to be used to create BOI screens for 
//                              the maintenance of miscellaneous codes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
//28/10/05  |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
//22/11/05  |M.Toernqvist| Changed this class to abstract  | PpasLon#1755/7473
//          |            | and added an abstract method:   |
//          |            | createCodeField().              |
//          |            | Constructor requires one more   |
//          |            | parameter: "main screen title". |
//          |            | Removed hardcoded CLS-stuff.    |
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 25/09/06 | Chris      | Make Defined Codes a scrollable | PpacLon#2061/9054
//          | Harrison   | table to stop large data values |
//          |            | messing up the screen layout.   |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.util.Vector;

import java.awt.Container;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.dbservice.MiscCodesDbService;

import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;

/**
 *  A class used to instantiate any screen required for maintaining srva_misc_codes.
 */
abstract public class GenericMiscCodesPane extends BusinessConfigBasePane
{   
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel i_detailsPanel                  = null;

    /** Panel containing table of existing records. */
    private JPanel i_definedCodesPanel             = null;

    /** Existing miscellaneous codes vector. */
    private Vector i_definedCodes                  = null;

    /** Available markets vector. */
    private Vector i_markets                       = null;

    /** Miscellaneous code field. */
    private JTextField i_codeField                 = null;

    /** Miscellaneous code description field. */
    private ValidatedJTextField i_descriptionField = null;
    
    /** Table containing existing records. */
    private JTable i_table                         = null;
    
    /** Main title */
    private String i_mainTitle                     = null;

    /** Column names for table of existing records. */
    private String[] i_columnNames                 = { "Code", "Description" };

    /** Data array for table of existing records. */
    private String i_data[][]                      = null;

    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel    = null;
    
    /** Identifies type of data to be maintained. Part of key for srva_misc_codes table.*/
    private String i_miscTable                     = null;
    
    /** Help screen identifier. */
    private int i_helpScreenIdentifier             = -1;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar i_verticalScrollBar         = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * Simple constructor. 
     * @param p_context A reference to the BoiContext
     * @param p_miscTable Identifies type of data to be maintained. 
     *                    Part of key for srva_misc_codes table. 
     * @param p_mainTitle Main Pane title.
     * @param p_helpScreenIdentifier Help screen identifier.
     */
    public GenericMiscCodesPane(Context p_context,
                                String  p_miscTable,
                                String  p_mainTitle,
                                int     p_helpScreenIdentifier )
    {
        super(p_context, (Container)null);

        i_miscTable = p_miscTable;
        i_mainTitle = p_mainTitle;
        i_helpScreenIdentifier = p_helpScreenIdentifier;
        i_dbService = new MiscCodesDbService((BoiContext)i_context, i_miscTable);
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    
    /** 
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }        
    }
 
    
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        String l_helpComboBox = i_mainTitle.replaceAll(" ","") + "HelpCompoBox,60,1,40,4";
        
        i_mainPanel    = WidgetFactory.createMainPanel(i_mainTitle, 100, 100, 0, 0);
        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics.getHelpTopics(i_helpScreenIdentifier), 
                                                          i_helpComboListener);
        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox, l_helpComboBox);
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");       
    }

    
    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            return false;
        }
        return true;
    }

    
    /**
     * Validates key screen data before checking for already existing records.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            return false;
        }
        
        ((MiscCodesDbService)i_dbService).setCurrentCode(i_codeField.getText());
        
        return true;
    }

    
    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        BoiMarket    l_market       = null;
        MiscCodeData l_miscCodeData = null;
        
        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();
        
        l_miscCodeData = new MiscCodeData(l_market.getMarket(),
                                          i_miscTable,
                                          i_codeField.getText(),
                                          i_descriptionField.getText(),
                                          " ");
        ((MiscCodesDbService)i_dbService).setMiscCodeData(new BoiMiscCodeData(l_miscCodeData));
    }

    
    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiMiscCodeData l_selectedItem = null;
        String          l_code         = null;
        Object          l_object       = null;
        
        l_object = i_keyDataComboBox.getSelectedItem();
        if (l_object instanceof BoiMiscCodeData)
        {
            l_selectedItem = (BoiMiscCodeData)l_object;
            l_code = l_selectedItem.getInternalMiscCodeData().getCode();
            ((MiscCodesDbService)i_dbService).setCurrentCode(l_code);
        }
    }

    
    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_verticalScrollBar.setValue(0);
    }

    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        MiscCodeData l_miscCodeData;

        l_miscCodeData = ((MiscCodesDbService)i_dbService).getMiscCodeData().getInternalMiscCodeData();
        i_codeField.setText(l_miscCodeData.getCode());

        i_descriptionField.setText(l_miscCodeData.getDescription());
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    
    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((MiscCodesDbService)i_dbService).getAvailableMiscCodeData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((MiscCodesDbService)i_dbService).getAvailableMarkets();  
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((MiscCodesDbService)i_dbService).getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);

        refreshListData();
    }

    
    /**
     * Resets the selected value for the combo box holding the key data.
     * Called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((MiscCodesDbService)i_dbService).getMiscCodeData());
    }

    
    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
    }

    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
    }

    
    /**
     * Creates a code field. It can be of different types depening upon which Misc code it
     * shall handle.
     * @return the field to be added to the detailed pane.
     */
    abstract protected JTextField createCodeField();

    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Paint the data entry panel.
     */
    private void createDetailsPanel()
    {
        JLabel l_marketLabel       = null;
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel         = null;
        JLabel l_descriptionLabel  = null;
        
        i_detailsPanel      = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_marketLabel       = WidgetFactory.createLabel("Market:");
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");       
        l_codeLabel         = WidgetFactory.createLabel("Code:");
        
        i_codeField = createCodeField();
        addValueChangedListener(i_codeField);
                
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(40);
        addValueChangedListener(i_descriptionField);
        
        
        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketBox,17,1,30,4");
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,6,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,6,70,4");
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,11,15,4");
        
        i_detailsPanel.add(i_codeField, "codeField,17,11,15,4");
        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,16,15,4");
        i_detailsPanel.add(i_descriptionField, "descriptionField,17,16,70,4");
        
        i_detailsPanel.add(i_updateButton,"updateButton,1,25,15,5");
        i_detailsPanel.add(i_deleteButton,"deleteButton,17,25,15,5");
        i_detailsPanel.add(i_resetButton,"resetButton,33,25,15,5");                
    }
 
    
    /**
     * Paint the panel containing the defined codes table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        int[]       l_columnWidths = new int[] { 250, 544 };
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(l_columnWidths, 150, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    
    /**
     * Populate the table containing previously defined codes.
     */
    private void populateCodesTable()
    {
        MiscCodeData l_miscCodeData = null;
        int          l_numRecords   = 0;
        int          l_arraySize    = 0;
        
        l_numRecords = i_definedCodes.size() -2;
        l_arraySize  = ((l_numRecords > 20) ? l_numRecords : 21);
        i_data       = new String[l_arraySize][2];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i=2, j=0; i < i_definedCodes.size(); i++, j++)
        {
            l_miscCodeData = ((BoiMiscCodeData)i_definedCodes.elementAt(i)).getInternalMiscCodeData();
            i_data[j][0]   = String.valueOf(l_miscCodeData.getCode());
            i_data[j][1]   = l_miscCodeData.getDescription();
        }

        i_stringTableModel.setData(i_data);
    }
}