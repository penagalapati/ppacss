////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       TeleServiceDbService.java
//    DATE            :       10-Mar-2006
//    AUTHOR          :       Mikael Alm
//    REFERENCE       :       PpacLon#2035/8098
//                            PRD_ASCS00_GEN_CA66
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Tele Service screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiTeleServiceData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceData;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.TeleTeleserviceSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Teleservice screen. 
 */
public class TeleServiceDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for teleservice code details. */
    private TeleTeleserviceSqlService i_teleSqlService = null;
    
    /** Vector of teleservice records. */
    private Vector i_availableTeleServicepDataV = null; // MIAM
    
    /** Teleservice data. */
    private BoiTeleServiceData i_teleData = null; // MIAM

    /** Currently selected teleservice code. Used as key for read and delete. */
    private int i_currentCode = -1;       
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public TeleServiceDbService(BoiContext p_context)
    {
        super(p_context);
        i_teleSqlService = new TeleTeleserviceSqlService(null, null, null);
        i_availableTeleServicepDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current teleservice code. Used for record lookup in database. 
     * @param p_code Currently selected teleservice code. 
     */
    public void setCurrentCode(int p_code)   
    {
        i_currentCode = p_code;
    }

    /** 
     * Return Teleservice data currently being worked on. 
     * @return Teleservice data object.
     */
    public BoiTeleServiceData getTeleserviceData()
    {
        return i_teleData;
    }

    /** 
     * Set teleservice data currently being worked on. 
     * @param p_teleserviceData Teleservice data object.
     */
    public void setTeleserviceData(BoiTeleServiceData p_teleserviceData)
    {
        i_teleData = p_teleserviceData;
    }

    /** 
     * Return teleservice data. 
     * @return Vector of available teleservice data records.
     */
    public Vector getAvailableTeleserviceData()
    {
        return i_availableTeleServicepDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TeleTeleserviceDataSet l_teleDataSet = null;
        
        l_teleDataSet = i_teleSqlService.readAll(null, p_connection);
        
        i_teleData = new BoiTeleServiceData(l_teleDataSet.getRecord(i_currentCode));
            
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshTeleserviceDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the tele_teleservice table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_teleSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_teleData.getInternalTeleserviceData().getTeleserviceCode(),
                                i_teleData.getInternalTeleserviceData().getTeleserviceDescription());        
        
        refreshTeleserviceDataVector(p_connection);
    }

    /** 
     * Updates record in the tele_teleservice table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_teleSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_teleData.getInternalTeleserviceData().getTeleserviceCode(),
                                i_teleData.getInternalTeleserviceData().getTeleserviceDescription());

        refreshTeleserviceDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the tele_teleservice table table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_teleSqlService.delete(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_teleData.getInternalTeleserviceData().getTeleserviceCode());
              
        refreshTeleserviceDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TeleTeleserviceDataSet l_teleDataSet = null;
        TeleTeleserviceData    l_dbTeleData  = null;
        boolean[]              l_flagsArray  = new boolean[2];
        
        l_teleDataSet        = i_teleSqlService.readAll(null, p_connection);

        l_dbTeleData = l_teleDataSet.getRecord(i_currentCode);
              
        if (l_dbTeleData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = l_dbTeleData.isDeleted();
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }        

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the tele_teleservice table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_teleSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_currentCode,
                                         i_context.getOperatorUsername());

        refreshTeleserviceDataVector(p_connection);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available teleservice data vector from the 
     * tele_teleservice table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshTeleserviceDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TeleTeleserviceDataSet l_teleDataSet = null;
        TeleTeleserviceData[]  l_teleArray   = null;
        
        i_availableTeleServicepDataV.removeAllElements();
        
        l_teleDataSet = i_teleSqlService.readAll(null, p_connection);
        l_teleArray   = l_teleDataSet.getAvailableArray();
        
        i_availableTeleServicepDataV.addElement(new String(""));
        i_availableTeleServicepDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_teleArray.length; i++)
        {
            i_availableTeleServicepDataV.addElement(new BoiTeleServiceData(l_teleArray[i]));
        }
    }
}