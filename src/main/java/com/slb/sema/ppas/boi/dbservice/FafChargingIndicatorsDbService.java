////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       FafChargingIndicatorsDbService.java
//    DATE            :       28-Oct-2005
//    AUTHOR          :       Yang Liu
//    REFERENCE       :       PpacLon#1755/7311
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Database access class for FaF Charging Indicators screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction.     | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.FachFafChargingIndSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.boi.boidata.BoiFafChargingIndicatorsData;
import com.slb.sema.ppas.boi.gui.BoiContext;

/**
 * Database access class for Faf Charging Indicators screen.
 */
public class FafChargingIndicatorsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for faf charging indicators code details. */
    private FachFafChargingIndSqlService i_fafciSqlService              = null;

    /** Vector of faf charging indicators records. */
    private Vector                       i_availableFafChargingIndDataV = null;

    /** Faf charging indicators data. */
    private BoiFafChargingIndicatorsData i_fafciData                    = null;

    /** Currently selected faf charging indicators code. Used as key for read and delete. */
    private int                          i_currentCode                  = -1;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public FafChargingIndicatorsDbService(BoiContext p_context)
    {
        super(p_context);
        i_fafciSqlService = new FachFafChargingIndSqlService(null);
        i_availableFafChargingIndDataV = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Set current faf charging indicators code. Used for record lookup in database.
     * @param p_code Currently selected faf charging indicator code.
     */
    public void setCurrentCode(int p_code)
    {
        i_currentCode = p_code;
    }

    /**
     * Return faf charging indicators data currently being worked on.
     * @return Faf charging indicators data object.
     */
    public BoiFafChargingIndicatorsData getFafChargingIndData()
    {
        return i_fafciData;
    }

    /**
     * Set Faf Charging Indicators data currently being worked on.
     * @param p_fafchargingIndData Faf Charging Indicators data object.
     */
    public void setFafChargingIndData(BoiFafChargingIndicatorsData p_fafchargingIndData)
    {
        i_fafciData = p_fafchargingIndData;
    }

    /**
     * Return Faf Charging Indicators data.
     * @return Vector of available faf charging indicators data records.
     */
    public Vector getAvailableFafChargingIndData()
    {
        return i_availableFafChargingIndDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        FachFafChargingIndDataSet l_fachfafciDataSet = null;

        l_fachfafciDataSet = i_fafciSqlService.readAll(null, p_connection);

        i_fafciData = new BoiFafChargingIndicatorsData(l_fachfafciDataSet.getRecord("" + i_currentCode));

    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        refreshFafChargingIndDataVector(p_connection);
    }

    /**
     * Inserts record into the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_fafciSqlService.insert(
                           null, 
                           p_connection, 
                           i_context.getOperatorUsername(), 
                           Integer.parseInt(i_fafciData.getInternalFachFafChargingIndData().getChargingInd()), 
                           i_fafciData.getInternalFachFafChargingIndData().getDesc());

        refreshFafChargingIndDataVector(p_connection);
    }

    /**
     * Updates record in the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        i_fafciSqlService.update(
                           null, 
                           p_connection, 
                           i_context.getOperatorUsername(), 
                           Integer.parseInt(i_fafciData.getInternalFachFafChargingIndData().getChargingInd()), 
                           i_fafciData.getInternalFachFafChargingIndData().getDesc());

        refreshFafChargingIndDataVector(p_connection);
    }

    /**
     * Deletes record from the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        i_fafciSqlService.delete(
                           null, 
                           p_connection, 
                           i_context.getOperatorUsername(), 
                           Integer.parseInt(i_fafciData.getInternalFachFafChargingIndData().getChargingInd()));

        refreshFafChargingIndDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        FachFafChargingIndDataSet l_fachfafciDataSet = null;
        FachFafChargingIndData l_dbFachfafciData = null;
        boolean[] l_flagsArray = new boolean[2];

        l_fachfafciDataSet = i_fafciSqlService.readAll(null, p_connection);
        l_dbFachfafciData = l_fachfafciDataSet.getRecord("" + i_currentCode);

        if (l_dbFachfafciData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbFachfafciData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }

        return l_flagsArray;
    }

    /**
     * Marks a previously withdrawn record as available in the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        i_fafciSqlService.markAsAvailable(
                           null, 
                           p_connection, 
                           Integer.parseInt(i_fafciData.getInternalFachFafChargingIndData().getChargingInd()), 
                           i_context.getOperatorUsername());

        refreshFafChargingIndDataVector(p_connection);

    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Refreshes the available faf charging indicators data vector from the fach_faf_charging_ind table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshFafChargingIndDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        FachFafChargingIndDataSet l_fachfafciDataSet = null;
        FachFafChargingIndData[] l_fachfafciArray = null;

        i_availableFafChargingIndDataV.removeAllElements();

        l_fachfafciDataSet = i_fafciSqlService.readAll(null, p_connection);
        l_fachfafciArray = l_fachfafciDataSet.getAvailableArray();

        i_availableFafChargingIndDataV.addElement(new String(""));
        i_availableFafChargingIndDataV.addElement(new String("NEW RECORD"));
        for (int i = 0; i < l_fachfafciArray.length; i++)
        {
            i_availableFafChargingIndDataV.addElement(new BoiFafChargingIndicatorsData(l_fachfafciArray[i]));
        }
    }
}