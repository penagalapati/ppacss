////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BusinessConfigBasePane.java
//    DATE            :       2-Jul-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#338/3075
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Superclass for all BOI panes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
// 22/12/05 | Lars. L.   | A new hook method is added:     | PpacLon#1755/7585
//          |            | restoreSelections(..).          |
//          |            | Added functionality when the    |
//          |            | RESET button is pressed and the |
//          |            | current state is "NEW RECORD".  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | With C_STATE_INITIAL state, it  | PpacLon#2086/10520
//          |            | should call initialiseScreen()  |
//          |            | instead of calling              |
//          |            | updateScreenForNewRecord()      |
//----------+------------+---------------------------------+--------------------
// 22/12/06 | M.T�rnqvist| Added hook method               | PpacLon#2822/10710
//          |            | validateDeletion()              |
//----------+------------+---------------------------------+--------------------
// 29/01/07 | M Erskine  | Support of secondary state.     | PpacLon#2898/10896
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.KeyboardFocusManager;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;

import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.dbservice.BoiDbService;

/**
 * Superclass for all BOI panes.
 */
abstract public class BusinessConfigBasePane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Main panel for the screen. */
    protected JPanel i_mainPanel;

    /** Button to update or insert data record. */
    protected GuiButton i_updateButton;
    
    /** Button to delete data record. Not applicable for every screen. */
    protected GuiButton i_deleteButton;
    
    /** Button to reset screen data. */
    protected GuiButton i_resetButton;
    
    /** Combo box model for selecting database records. */
    protected DefaultComboBoxModel i_keyDataModel;

    /** Combo box model for markets. */
    protected DefaultComboBoxModel i_marketDataModel;

    /** Combo box for selecting database records. */
    protected JComboBox i_keyDataComboBox;
    
    /** 
     * Market combo box used to filter records for the key data combo.
     * Created in this class, but not applicable for every screen.
     */
    protected JComboBox i_marketDataComboBox;
    
    /** 
     * Available service class combo box used to filter records for the key data combo.
     * Only applicable for a couple of screens, so created in subclasses.
     */
    protected JComboBox i_serviceClassDataComboBox = null;

    /** Action listener for common BOI buttons (update, delete, reset). */
    protected CommonButtonListener i_commonButtonListener;
    
    /** Item listener for combo box used to select records from the database. */
    protected KeyDataComboListener i_keyDataComboListener;
    
    /** Item listener for market and service class combo boxes. */ 
    protected DataFilterComboListener i_dataFilterComboListener;

    /** Database service object for screen. */
    protected BoiDbService i_dbService;
    
    /** Listener for help dropdowns. */
    protected BoiHelpListener i_helpComboListener;

    /** Combo box containing help topics for the screen. */
    protected JComboBox i_helpComboBox;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor.
     * @param p_context Context object.
     * @param p_container Panel displaying the data corresponding to the menu selection.
     */
    public BusinessConfigBasePane(
        Context    p_context,
        Container  p_container)
    {
        super(p_context, p_container);

        i_commonButtonListener = new CommonButtonListener();
        i_updateButton = WidgetFactory.createButton("Update", i_commonButtonListener, true);
        i_deleteButton = WidgetFactory.createButton("Delete", i_commonButtonListener, false);
        i_deleteButton.setVerifyInputWhenFocusTarget(false);
        i_resetButton = WidgetFactory.createButton("Reset", i_commonButtonListener, false);
        i_resetButton.setVerifyInputWhenFocusTarget(false);

        i_keyDataComboListener = new KeyDataComboListener();
        i_dataFilterComboListener = new DataFilterComboListener();

        i_keyDataModel = new DefaultComboBoxModel();
        i_keyDataComboBox = WidgetFactory.createComboBox(i_keyDataModel);
        i_keyDataComboBox.addItemListener(i_keyDataComboListener);

        i_marketDataModel = new DefaultComboBoxModel();
        i_marketDataComboBox = WidgetFactory.createComboBox(i_marketDataModel);
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);
        
        i_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Sets the key data combo to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if(!i_keyDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        readInitialDataForPane();
        refreshDataUponEntry();
        initialiseScreen();
        i_state = C_STATE_INITIAL;
        initialiseElementsPanel();
    }

    /**
     * Handles keyboard events for the screen.
     * Must be overridden in subclass if there are extra buttons in addition to
     * Update, Delete, and Reset.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean  l_return = false;
            
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_deleteButton)
            {
                i_deleteButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else
            {
                i_updateButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }
    
    /**
     * Invoked when an item has been selected or deselected by the user.
     * @param p_itemEvent Event to be handled.
     */    
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        // Do nothing
    }

    /**
     * Handles action events.
     * @param p_actionEvent Event to be handled.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        // Do nothing.
    }
    
    /** 
     * Handles GUI events
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing.
    }

    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event){}

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Performs the initial set-up of the screen.
     */
    protected void init()
    {
        paintScreen();

        i_contentPane = i_mainPanel;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_mainPanel.setFocusCycleRoot(true);        
    }
    
    /**
     * Selects the row in  table that corresponds to the selected
     * item in the combo box.
     * Also adjusts the view port in order to make the selected row visible.
     * @param p_table             The table whose display is being altered.
     * @param p_verticalScrollBar The vertical scrollbar belonging to the JScrollPane in this table.
     */
    protected void selectRowInTable(JTable p_table, JScrollBar p_verticalScrollBar)
    {
        int l_selectionIx = -1;
        l_selectionIx = i_keyDataComboBox.getSelectedIndex() - 2;

        if (l_selectionIx >= 0)
        {
            if (l_selectionIx != p_table.getSelectedRow())
            {
                p_table.setRowSelectionInterval(l_selectionIx, l_selectionIx);
                p_verticalScrollBar.setValue(l_selectionIx * p_table.getRowHeight());
            }
        }
        else
        {
            p_verticalScrollBar.setValue(0);
        }
    }

    /**
     * Paints the screen.
     */
    abstract protected void paintScreen();

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    abstract protected void refreshCurrentRecord();

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */  
    abstract protected boolean validateScreenData();

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */  
    abstract protected boolean validateKeyData();

    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    abstract protected void writeCurrentRecord();

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    abstract protected void writeKeyData();

    /**
     * Initialises screen data fields to values when no record is selected.
     */
    abstract protected void initialiseScreen();

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data after inserts, 
     * deletes, and updates, and also after selections from market and service class combos.
     * Implementations of this method should NOT refresh market and service class combo boxes.
     */
    abstract protected void refreshListData();

    /**
     * Refreshes data upon screen entry.
     */
    abstract protected void refreshDataUponEntry();

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    abstract protected void resetSelectionForKeyCombo();

    /**
     * Refreshes list data after a new market has been selected from the market combo box.
     * Needs to be over-ridden in panes where key record filtering is done by both 
     * market and service class, such that service class combo can also be refreshed.
     */   
    protected void refreshDataAfterMarketSelect()
    {
        refreshListData();
    }
    
    /**
     * Hook method to allow subclass to alter screen bevaviour when "NEW RECORD" 
     * is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        // Do nothing
    }
    
    /**
     * Hook method to allow subclass to alter screen bevaviour when an existing record, 
     * as opposed to "NEW RECORD", is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        // Do nothing
    }
    
    /** Hook method to allow subclasses to add checks before deleting an 
     * existing row.
     * @return TRUE if it is OK to delete
     */
    protected boolean validateDeletion()
    {
    	// Do nothing
    	return true;
    }
    
    protected void refreshAfterSelection(ItemEvent p_itemEvent) throws PpasServiceFailedException
    {
        // Do nothing
    }

    /**
     * Hook method to allow subclass to alter screen behaviour when the user
     * regrets a selection that would lead to an aborted NEW RECORD process.
     */
    protected void restoreSelections()
    {
        // Do nothing
    }
    
    /**
     * Hook method to allow subclass with 2nd key data combo-box to initialise
     * the containing panel
     */
    protected void initialiseElementsPanel()
    {
        // Do nothing
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Reads data required by the pane when it is selected from the menu.
     */
    private void readInitialDataForPane()
    {
        // Ensure that any market filter dropdown will be refreshed, and the default market will be selected.
        i_dbService.setCurrentMarket(null);
        
        // Ensure that any service class filter dropdown will be refreshed. 
        i_dbService.setCurrentServiceClass(null);
        
        // Ensure that any adjustment type dropdown will be refreshed.
        i_dbService.setCurrentAdjustmentType(null);

        try
        {
            i_dbService.readInitialData();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
    }

    /**
     * Handles user requests to update the database with the screen data. 
     * @throws PpasServiceFailedException
     */
    private void doUpdate()
        throws PpasServiceFailedException
    {
        int       l_response = 0;
        boolean   l_valid = false;
        int       l_selectedIndex = -1;
        boolean[] l_flagsArray = null;
        
        switch (i_state)
        {
            case C_STATE_INITIAL:
            {
                displayMessageDialog(i_contentPane,
                                     "Select record and modify data before updating.");
                break;
            }
            case C_STATE_DATA_RETRIEVED:
            {
                displayMessageDialog(i_contentPane,
                                     "Modify data before updating.");
                break;
            }
            case C_STATE_NEW_RECORD:
            {
                l_valid = validateKeyData();
                if (l_valid)
                {
                    l_flagsArray = i_dbService.checkForDuplicateKey();
                    if (l_flagsArray[BoiDbService.C_WITHDRAWN])
                    {
                        l_response = displayConfirmDialog(
                                                i_contentPane,
                                                "A withdrawn record with this key already exists. " +
                                                "Do you wish to overwrite it?");
                        
                        if (l_response == JOptionPane.YES_OPTION)
                        {
                            l_valid = validateScreenData();
                            if (l_valid)
                            {
                                writeCurrentRecord();
                                i_dbService.updateData();
                                displayMessageDialog(i_contentPane,
                                                     "Record has been overwritten.");
                                refreshListData();
                                initialiseScreen();
                                i_state = C_STATE_INITIAL;
                            }
                        }
                        else
                        {
                            l_response = displayConfirmDialog(
                                            i_contentPane,
                                            "Do you wish to make the withdrawn record available again?");
                            
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                i_dbService.markAsAvailableData();
                                displayMessageDialog(i_contentPane,
                                                     "Record has been made available.");
                                refreshListData();
                                initialiseScreen();
                                i_state = C_STATE_INITIAL;
                            }
                        }
                    }
                    else if (l_flagsArray[BoiDbService.C_DUPLICATE])
                    {
                        displayMessageDialog(i_contentPane,
                                             "Record with this key already exists.");
                    }
                    else
                    {
                        l_valid = validateScreenData();
                        if (l_valid)
                        {
                            writeCurrentRecord();
                            i_dbService.insertData();
                            displayMessageDialog(i_contentPane,
                                                 "New record inserted.");
                            refreshListData();
                            initialiseScreen();
                            i_state = C_STATE_INITIAL;
                        }
                    }
                }
                break;
            }
            case C_STATE_DATA_MODIFIED:
            {
                l_valid = validateScreenData();
                if (l_valid)
                {
                    l_selectedIndex = i_keyDataComboBox.getSelectedIndex();
                    writeCurrentRecord();
                    i_dbService.updateData();
                    displayMessageDialog(i_contentPane,
                                         "Record has been updated.");
                    refreshListData();
                    i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                    i_keyDataComboBox.setSelectedIndex(l_selectedIndex);
                    i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                    i_state = C_STATE_DATA_RETRIEVED;
                }
                break;
            }
        }
    }

    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to listen on Update, Delete, and Reset buttons.
     */
    protected class CommonButtonListener
    implements ActionListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Event handler for the common business config buttons: Update, Delete,
         * and Reset.
         * @param p_actionEvent Event to handle.
         */
        public void actionPerformed(ActionEvent p_actionEvent)
        {
            Object    l_source = null;
            int       l_response = 0;
        
            l_source = p_actionEvent.getSource();
        
            try
            {
                if (l_source == i_updateButton)
                {
                    // Request focus on update button such that focus lost processing
                    // is performed on previous focus owner.  
                    i_updateButton.requestFocusInWindow();
                    
                    // Perform update processing after the current focus lost event.
                    // i.e. After field validation has occurred for previous focus owner.
                    SwingUtilities.invokeLater(new UpdateRunnable());
                }
                else if (l_source == i_deleteButton)
                {
                    switch (i_state)
                    {
                        case C_STATE_INITIAL:
                        {
                            displayMessageDialog(
                                       i_contentPane,
                                       "Retrieve data before deleting.");
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            displayMessageDialog(
                                       i_contentPane,
                                       "Select existing record before hitting delete.");
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        case C_STATE_DATA_MODIFIED:
                        {
                        	if ( !validateDeletion() )
                        	{
                        		// Cannot delete this row
                        		break;
                        	}
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Are you sure you wish to delete this record?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                writeKeyData();
                                i_dbService.deleteData();
                                refreshListData();  
                                initialiseScreen();
                                i_state = C_STATE_INITIAL;
                                initialiseElementsPanel();
                            }
                            break;
                        }
                    }
                }
                else if (l_source == i_resetButton)
                {
                    switch (i_state)
                    {
                        case C_STATE_INITIAL:
                        case C_STATE_DATA_RETRIEVED:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Record not saved. Are you sure you wish to reset?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseScreen();
                                if (i_keyDataComboBox.getItemCount() > 0)
                                {
                                    i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                                    i_keyDataComboBox.setSelectedIndex(0);
                                    i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                                }
                                i_state = C_STATE_INITIAL;
                            }
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Data has been modified. Are you sure you wish to reset?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                writeKeyData();
                                i_dbService.readData();
                                refreshCurrentRecord();
                                i_state = C_STATE_DATA_RETRIEVED;
                            }
                            break;
                        }
                    }
                }
            }
            catch (PpasServiceFailedException l_e)
            {
                handleException(l_e);
            }
        }
    }
    
    /**
     * Class to listen on key data selection combo box.  Handles selection of NEW RECORD,
     * existing record, or blank.
     */
    protected class KeyDataComboListener
    implements ItemListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Combo box event handler for the key data combo.
         * @param p_itemEvent Event to handle.
         */
        public void itemStateChanged(ItemEvent p_itemEvent)
        {
            String l_selectedItem; 
            int    l_response = 0;

            if (p_itemEvent.getStateChange() == ItemEvent.SELECTED)
            {
                l_selectedItem = ((JComboBox)p_itemEvent.getSource()).getSelectedItem().toString();
                if (i_state2 == C_STATE_DATA_MODIFIED)
                {
                    l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                    if (l_response != JOptionPane.YES_OPTION)
                    {
                        i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                        resetSelectionForKeyCombo();
                        i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                        return;
                    }
                }

                if (l_selectedItem.equals("NEW RECORD"))
                {
                    switch (i_state)
                    {
                        case C_STATE_INITIAL:
                        {
                            i_state = C_STATE_NEW_RECORD;
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        {
                            initialiseScreen();
                            i_state = C_STATE_NEW_RECORD;
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseScreen();
                                i_state = C_STATE_NEW_RECORD;
                            }
                            else
                            {
                                i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                                resetSelectionForKeyCombo();
                                i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                            }
                            break;
                        }
                    }
                    initialiseElementsPanel();
                }
                else if (l_selectedItem.equals(""))
                {
                    switch (i_state)
                    {
                        case C_STATE_INITIAL:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        {
                            initialiseScreen();
                            i_state = C_STATE_INITIAL;
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseScreen();
                                i_state = C_STATE_INITIAL;
                            }
                            else
                            {
                                // Reset to NEW_RECORD. Should always be element 1 in dropdown.
                                i_keyDataComboBox.setSelectedIndex(1);
                            }
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseScreen();
                                i_state = C_STATE_INITIAL;
                            }
                            else
                            {
                                i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                                resetSelectionForKeyCombo();
                                i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                            }
                            break;
                        }
                    }
                    initialiseElementsPanel();
                }
                else
                {
                    try
                    {
                        switch (i_state)
                        {
                            case C_STATE_INITIAL:
                            case C_STATE_DATA_RETRIEVED:
                            {
                                writeKeyData();
                                i_dbService.readData();
                                refreshCurrentRecord();
                                i_state = C_STATE_DATA_RETRIEVED;
                                break;
                            }
                            case C_STATE_NEW_RECORD:
                            {
                                l_response = displayConfirmDialog(
                                             i_contentPane,
                                             "Data modifications will be lost. Do you wish to continue?");
        
                                if (l_response == JOptionPane.YES_OPTION)
                                {
                                    writeKeyData();
                                    i_dbService.readData();
                                    refreshCurrentRecord();
                                    i_state = C_STATE_DATA_RETRIEVED;
                                }
                                else
                                {
                                    // Reset to NEW_RECORD. Should always be element 1 in dropdown.
                                    i_keyDataComboBox.setSelectedIndex(1);
                                }
                                break;
                            }
                            case C_STATE_DATA_MODIFIED:
                            {
                                l_response = displayConfirmDialog(
                                             i_contentPane,
                                             "Data modifications will be lost. Do you wish to continue?");
        
                                if (l_response == JOptionPane.YES_OPTION)
                                {
                                    writeKeyData();
                                    i_dbService.readData();
                                    refreshCurrentRecord();
                                    i_state = C_STATE_DATA_RETRIEVED;
                                }
                                else
                                {
                                    i_keyDataComboBox.removeItemListener(i_keyDataComboListener);
                                    resetSelectionForKeyCombo();
                                    i_keyDataComboBox.addItemListener(i_keyDataComboListener);
                                }
                                break;
                            }
                        }
                    }
                    catch (PpasServiceFailedException l_e)
                    {
                        handleException(l_e);
                    }
                }

                if (i_state == C_STATE_NEW_RECORD) 
                {
                    updateScreenForNewRecord();
                }
                else if (i_state == C_STATE_INITIAL) 
                {
                    initialiseScreen();
                }
                else
                {
                    updateScreenForExistingRecord();
                }
            }
        }
    }

    /**
     * Listener for combo boxes used to filter the records for the key data dropdown. 
     */
    protected class DataFilterComboListener
    implements ItemListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Event handler for the market and service class combo boxes.
         * @param p_itemEvent Event to handle.
         */
        public void itemStateChanged(ItemEvent p_itemEvent)
        {
            int    l_response = 0;

            if (p_itemEvent.getStateChange() == ItemEvent.SELECTED)
            {
                try
                {
                    switch (i_state)
                    {
                        case C_STATE_INITIAL:
                        case C_STATE_DATA_RETRIEVED:
                        {
                            refreshScreenAfterSelection(p_itemEvent);
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                             i_contentPane,
                                             "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                refreshScreenAfterSelection(p_itemEvent);
                            }
                            else
                            {
                                if (p_itemEvent.getSource() == i_marketDataComboBox)
                                {
                                    i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
                                    i_marketDataComboBox.setSelectedItem(i_dbService.getCurrentMarket());
                                    i_marketDataComboBox.addItemListener(i_dataFilterComboListener);
                                }
                                else if (p_itemEvent.getSource() == i_serviceClassDataComboBox)
                                {
                                    i_serviceClassDataComboBox.removeItemListener(i_dataFilterComboListener);
                                    i_serviceClassDataComboBox.setSelectedItem(
                                                                     i_dbService.getCurrentServiceClass());
                                    i_serviceClassDataComboBox.addItemListener(i_dataFilterComboListener);
                                }
                                else
                                {
                                    restoreSelections();
                                }
                            }
                            break;
                        }
                    }
                }
                catch (PpasServiceFailedException l_e)
                {
                    handleException(l_e);
                }
            }
        }
        
        /** 
         * Refreshes the screen after a successful selection from market or service class combo boxes.
         * @param p_itemEvent Combo selection event.
         * @throws PpasServiceFailedException PPAS service exception.
         */
        private void refreshScreenAfterSelection(ItemEvent p_itemEvent)
            throws PpasServiceFailedException
        {
            if (p_itemEvent.getSource() == i_marketDataComboBox)
            {
                i_dbService.setCurrentMarket((BoiMarket)i_marketDataComboBox.getSelectedItem());
                // Ensure that service class vector is refreshed.
                i_dbService.setCurrentServiceClass(null);
                i_dbService.setCurrentAdjustmentType(null);
                i_dbService.readInitialData();
                refreshDataAfterMarketSelect();
            }
            else if (p_itemEvent.getSource() == i_serviceClassDataComboBox)
            {
                i_dbService.setCurrentServiceClass(
                                     (BoiMiscCodeData)i_serviceClassDataComboBox.getSelectedItem());
                i_dbService.readInitialData();
                refreshListData();
            }
            else
            {
                refreshAfterSelection(p_itemEvent);
            }
            
            initialiseScreen();
            i_state = C_STATE_INITIAL;
        }
    }

    /**
     * Class to enable update processing to be performed in a separate thread.
     */
    public class UpdateRunnable implements Runnable
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------
        
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Only do this if focus was successfully transferred to the update button.
            // If it is not the focus owner, this would indicate there was a parse
            // error with the previous focus owner.
            if (i_updateButton.isFocusOwner())
            {
                try
                {
                    doUpdate();
                }
                catch (PpasServiceFailedException l_e)
                {
                    handleException(l_e);
                }
            }
        }
    }
}  