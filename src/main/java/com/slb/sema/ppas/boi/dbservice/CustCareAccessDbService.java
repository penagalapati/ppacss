////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       CustCareAccessDbService.java
//    DATE            :       12-Aug-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#432/3643
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Customer Care Access 
//                            Privileges screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//17/10/05 | K Goswami  | Changed code to allow temp      | PpacLon#463
//         |            | blocking privilege.             |
//---------+------------+---------------------------------+---------------------
//15/12/05 | K Goswami  | Changed code to allow voucher   | PpaLon#1892/7603
//         |            | history enquiry privilege       | CA#39
//---------+------------+---------------------------------+---------------------
//12/12/05 | M Erskine  | Add a function access privilege.| PpacLon#1882/7556
//         |            |                                 | PRD_ASCS00_GEN_CA_65
//---------+------------+---------------------------------+---------------------
//20/03/06 | Ian James  | Add a function access privilege.| PpacLon#2055/8226
//         |            |                                 | PRD_ASCS00_GEN_CA_66
//---------+------------+---------------------------------+---------------------
//24/04/07 | S James    | Add Single Step Install changes | PpacLon#3072/11370
//         |            | & Account Reconnection changes  |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ApacApplicationAccessSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.GopaGuiOperatorAccessSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrilPrivilegeLevelSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

import com.slb.sema.ppas.boi.gui.BoiContext;

/**
 * Database service class for Customer Care Access Privileges screen. 
 */
public class CustCareAccessDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for operator privilege level data. */
    private PrilPrivilegeLevelSqlService i_prilSqlService;
    
    /** SQL service for operator GUI access data. */
    private GopaGuiOperatorAccessSqlService i_gopaSqlService;
    
    /** SQL service for operator application access data. */
    private ApacApplicationAccessSqlService i_apacSqlService;
    
    /** Vector of available privileges. */
    private Vector i_availablePrivilegesV;

    /** Currently selected privilege code. */
    private int i_currentPrivilegeCode;

    /** Privilege level data object currently being worked on. */ 
    private PrilPrivilegeLevelData i_privilegeLevelData;
    
    /** GUI access privileges data object currently being worked on. */
    private GopaGuiOperatorAccessData i_gopaData;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor.
     * @param p_context BOI context object.
     */
    public CustCareAccessDbService(BoiContext p_context)
    {
        super(p_context);
        i_prilSqlService = new PrilPrivilegeLevelSqlService(null, null);
        i_gopaSqlService = new GopaGuiOperatorAccessSqlService(null, null);
        i_apacSqlService = new ApacApplicationAccessSqlService(null, null);
        i_availablePrivilegesV = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Return vector of available privileges. 
     * @return Vector of available privileges. 
     */
    public Vector getAvailablePrivileges()
    {
        return i_availablePrivilegesV;
    }

    /** 
     * Set currently selected privilege code. Used for record lookup in database. 
     * @param p_currentPrivilegeCode Currently selected privilege code.
     */
    public void setCurrentPrivilegeCode(int p_currentPrivilegeCode)
    {
        i_currentPrivilegeCode = p_currentPrivilegeCode;
    }

    /** 
     * Returns privilege level data object currently being worked on. 
     * @return Privilege level data object currently being worked on. 
     */
    public PrilPrivilegeLevelData getPrivilegeLevelData()
    {
        return i_privilegeLevelData;
    }

    /** 
     * Set privilege level data object currently being worked on. 
     * @param p_privilegeLevelData Current privilege level data.
     */
    public void setPrivilegeLevelData(PrilPrivilegeLevelData p_privilegeLevelData)
    {
        i_privilegeLevelData = p_privilegeLevelData;
    }

    /** 
     * Return GUI access privileges data object currently being worked on.
     * @return GUI access privileges data object currently being worked on.
     */
    public GopaGuiOperatorAccessData getGopaData()
    {
        return i_gopaData;
    }
    
    /** 
     * Set GUI access privileges data object currently being worked on.
     * @param p_gopaData GUI access privileges data object currently being worked on.
     */
    public void setGopaData(GopaGuiOperatorAccessData p_gopaData)
    {
        i_gopaData = p_gopaData;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        GopaGuiOperatorAccessDataSet l_gopaDataSet;
        PrilPrivilegeLevelDataSet    l_privilegeLevelDataSet;
        
        l_privilegeLevelDataSet = i_prilSqlService.readAll(null, p_connection);
        i_privilegeLevelData = l_privilegeLevelDataSet.getPrilPrivilegeLevelData(i_currentPrivilegeCode);

        l_gopaDataSet = i_gopaSqlService.readAll(null, p_connection);
        i_gopaData = l_gopaDataSet.getGuiOperatorAccessProfile(i_currentPrivilegeCode);
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshPrivilegeLevelVector(p_connection);
    }
    
    /** 
     * Inserts new privilege data records in the pril_privilege_level and gopa_gui_operator_access 
     * tables. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_prilSqlService.insert(
            null,
            p_connection,
            i_privilegeLevelData.getPrivilegeLevel() + "",
            i_privilegeLevelData.getDescription(),
            i_context.getOperatorUsername());
            
        i_gopaSqlService.insert(
            null,
            p_connection,
            i_gopaData.getPrivilegeId() + "",
            i_gopaData.hasNewSubsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasSubsDetailsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasVouchersScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasPaymentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAdjustmentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasPromotionsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasDisconnectScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasSubordinatesScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAddInfoScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasDedAccountsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAccumulatorsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasCommentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasFafScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAdjApprovalScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasRoutingScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasEventCountersScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasServiceFeeScreenAccess() ? 'Y' : 'N',
            i_gopaData.expDatesUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.servClassChangeIsPermitted() ? 'Y' : 'N',
            i_gopaData.languageChangeIsPermitted() ? 'Y' : 'N',
            i_gopaData.agentUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.ivrUnbarIsPermitted() ? 'Y' : 'N',
            i_gopaData.installInIdIsPermitted() ? 'Y' : 'N',
            i_gopaData.subsDetailsUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.accountRechargeIsPermitted() ? 'Y' : 'N',
            i_gopaData.voucherEnquiryIsPermitted() ? 'Y' : 'N',
            i_gopaData.makePaymentIsPermitted() ? 'Y' : 'N',
            i_gopaData.makeAdjustmentIsPermitted() ? 'Y' : 'N',
            i_gopaData.promotionsUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.addSubordinateIsPermitted() ? 'Y' : 'N',
            i_gopaData.addInfoUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.makeDedAccAdjustIsPermitted() ? 'Y' : 'N',
            i_gopaData.updateFafListPermitted() ? 'Y' : 'N',
            i_gopaData.updateAccumIsPermitted() ? 'Y' : 'N',
            i_gopaData.voucherUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasSubscriberSegmentationScreenAccess() ? 'Y' : 'N',
            i_gopaData.subscriberSegmentationUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasCommunityChargingScreenAccess() ? 'Y' : 'N',
            i_gopaData.communityChargingUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.ussdEocnIdUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.subscriberPinUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.eventCountersUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.homeRegionUpdateIsPermitted()  ? 'Y' : 'N',
            i_gopaData.tempBlockUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.voucherHistoryIsPermitted() ? 'Y' : 'N',
            i_gopaData.overrideMsisdnQuarantinePeriodIsPermitted() ? 'Y' : 'N',
            i_gopaData.singleStepInstallIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasCallHistoryScreenAccess() ? 'Y' : 'N',
            i_gopaData.isAccountReconnectionPermitted() ? 'Y' : 'N',
            i_context.getOperatorUsername());
                
        i_apacSqlService.insert(
            null,
            p_connection,
            i_privilegeLevelData.getPrivilegeLevel() + "",
            'N', 
            'N', 
            'N', 
            'N', 
            ' ',
            i_context.getOperatorUsername());

        refreshPrivilegeLevelVector(p_connection);
    }

    /** 
     * Updates privilege data records in the pril_privilege_level and gopa_gui_operator_access 
     * tables. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_prilSqlService.update(
            null,
            p_connection,
            i_privilegeLevelData.getPrivilegeLevel() + "",
            i_privilegeLevelData.getDescription(),
            i_context.getOperatorUsername());
                            
        i_gopaSqlService.update(
            null,
            p_connection,
            i_gopaData.getPrivilegeId() + "",
            i_gopaData.hasNewSubsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasSubsDetailsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasVouchersScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasPaymentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAdjustmentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasPromotionsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasDisconnectScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasSubordinatesScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAddInfoScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasDedAccountsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAccumulatorsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasCommentsScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasFafScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasAdjApprovalScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasRoutingScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasEventCountersScreenAccess() ? 'Y' : 'N',
            i_gopaData.hasServiceFeeScreenAccess() ? 'Y' : 'N',
            i_gopaData.expDatesUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.servClassChangeIsPermitted() ? 'Y' : 'N',
            i_gopaData.languageChangeIsPermitted() ? 'Y' : 'N',
            i_gopaData.agentUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.ivrUnbarIsPermitted() ? 'Y' : 'N',
            i_gopaData.installInIdIsPermitted() ? 'Y' : 'N',
            i_gopaData.subsDetailsUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.accountRechargeIsPermitted() ? 'Y' : 'N',
            i_gopaData.voucherEnquiryIsPermitted() ? 'Y' : 'N',
            i_gopaData.makePaymentIsPermitted() ? 'Y' : 'N',
            i_gopaData.makeAdjustmentIsPermitted() ? 'Y' : 'N',
            i_gopaData.promotionsUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.addSubordinateIsPermitted() ? 'Y' : 'N',
            i_gopaData.addInfoUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.makeDedAccAdjustIsPermitted() ? 'Y' : 'N',
            i_gopaData.updateFafListPermitted() ? 'Y' : 'N',
            i_gopaData.updateAccumIsPermitted() ? 'Y' : 'N',
            i_gopaData.voucherUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasSubscriberSegmentationScreenAccess() ? 'Y' : 'N',
            i_gopaData.subscriberSegmentationUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasCommunityChargingScreenAccess() ? 'Y' : 'N',
            i_gopaData.communityChargingUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.ussdEocnIdUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.subscriberPinUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.eventCountersUpdateIsPermitted() ? 'Y' : 'N',
            i_gopaData.homeRegionUpdateIsPermitted()  ? 'Y' : 'N',
            i_gopaData.tempBlockUpdateIsPermitted()  ? 'Y' : 'N',
            i_gopaData.voucherHistoryIsPermitted() ? 'Y' : 'N',   
            i_gopaData.overrideMsisdnQuarantinePeriodIsPermitted() ? 'Y' : 'N',
            i_gopaData.singleStepInstallIsPermitted() ? 'Y' : 'N',
            i_gopaData.hasCallHistoryScreenAccess() ? 'Y' : 'N',
            i_gopaData.isAccountReconnectionPermitted() ? 'Y' : 'N',
            i_context.getOperatorUsername());

        refreshPrivilegeLevelVector(p_connection);
    }
    
    /** 
     * Deletes privilege data records from the pril_privilege_level and gopa_gui_operator_access 
     * tables. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_prilSqlService.delete(
            null,
            p_connection,
            i_privilegeLevelData.getPrivilegeLevel() + "",
            i_context.getOperatorUsername());

        refreshPrivilegeLevelVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        boolean[]                 l_flagsArray = new boolean[2];
        PrilPrivilegeLevelDataSet l_privilegeLevelDataSet = null;
        PrilPrivilegeLevelData    l_privilegeLevelData = null;
        
        l_privilegeLevelDataSet = i_prilSqlService.readAll(null, p_connection);
        l_privilegeLevelData = l_privilegeLevelDataSet.getPrilPrivilegeLevelData(i_currentPrivilegeCode);

        if (l_privilegeLevelData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_privilegeLevelData.getIsCurrent())
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the pril_privilege_level table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_prilSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_privilegeLevelData.getPrivilegeLevel() + "",
                                         i_context.getOperatorUsername());

        refreshPrivilegeLevelVector(p_connection);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available privilege level data vector from the 
     * pril_privilege_level table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshPrivilegeLevelVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        PrilPrivilegeLevelDataSet l_privilegeLevelDataSet = null;
        PrilPrivilegeLevelData[]  l_availablePrivileges = null;
        
        l_privilegeLevelDataSet = i_prilSqlService.readAll(null, p_connection);
        l_availablePrivileges = l_privilegeLevelDataSet.getAvailablePrivileges();

        i_availablePrivilegesV.removeAllElements();
        i_availablePrivilegesV.addElement(new String(""));
        i_availablePrivilegesV.addElement(new String("NEW RECORD"));
        for (int i = 0; i < l_availablePrivileges.length; i++)
        {
            i_availablePrivilegesV.addElement(l_availablePrivileges[i]);
        }
    }
}
