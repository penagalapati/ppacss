////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiFastAdjustmentCodesData.java
//      DATE            :       12-December-2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PpacLon#1755/7585
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Wrapper for ScapServClassAdjData class within BOI.
//                              Supports toString and equals methods for use
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjData;
import com.slb.sema.ppas.common.dataclass.DataObject;

/*******************************************************************************
 * This <code>BoiFastAdjustmentCodesData</code> class serves as a wrapper class
 * for the <code>ScapServClassAdjData</code>.
 * It provides <code>toString()</code> and <code>equals(Object p_obj)</code>
 * methods for use within combo boxes.
 */
public class BoiFastAdjustmentCodesData extends DataObject
{
    //==========================================================================
    // Private attribute(s).                                                  ==
    //==========================================================================
    /** The wrapped data object. */
    private ScapServClassAdjData i_scapServClassAdjData = null;


    //==========================================================================
    // Constructor(s).                                                        ==
    //==========================================================================
    /***************************************************************************
     * Constructs an instance of this <code>BoiFastAdjustmentCodesData</code>
     * class using the passed <code>ScapServClassAdjData</code> object.
     * 
     * @param p_scapServClassAdjData  the <code>ScapServClassAdjData</code> object.
     */
    public BoiFastAdjustmentCodesData(ScapServClassAdjData p_scapServClassAdjData)
    {
        i_scapServClassAdjData = p_scapServClassAdjData;
    }


    //==========================================================================
    // Public method(s).                                                      ==
    //==========================================================================
    /***************************************************************************
     * Returns the (wrapped) fast adjustment code data object.
     * 
     * @return the (wrapped) fast adjustment code data object.
     */
    public ScapServClassAdjData getInternalFastAdjCodesData()
    {
        return i_scapServClassAdjData;
    }


    /***************************************************************************
     * Compares two fast adjustment code data objects and returns true if they
     * equate.
     * 
     * @param p_fastAdjCodesData  the fast adjustment code data object to
     *                            compare the current instance with.
     *                            
     * @return <code>true</code> if both instances are equal, <code>false</code>
     *                           otherwise.
     */
    public boolean equals(Object p_fastAdjCodesData)
    {
        boolean                    l_equal                = false;
        BoiFastAdjustmentCodesData l_fastAdjCodesData     = null;
        ScapServClassAdjData       l_scapServClassAdjData = null;

        if (p_fastAdjCodesData != null  && (p_fastAdjCodesData instanceof BoiFastAdjustmentCodesData))
        {
            l_fastAdjCodesData = (BoiFastAdjustmentCodesData)p_fastAdjCodesData;
            l_scapServClassAdjData = l_fastAdjCodesData.getInternalFastAdjCodesData();
            if (l_scapServClassAdjData.getAdjType().equals(i_scapServClassAdjData.getAdjType()) &&
                l_scapServClassAdjData.getAdjCode().equals(i_scapServClassAdjData.getAdjCode()))
            {
                l_equal = true;
            }
        }
        return l_equal;
    }


    /***************************************************************************
     * Returns the (wrapped) fast adjustment code data object as a
     * <code>String</code> for display in BOI.
     * 
     * @return the (wrapped) fast adjustment code data object as a <code>String</code>.
     */
    public String toString()
    {
        StringBuffer l_toStringBuf = new StringBuffer();
        l_toStringBuf.append(i_scapServClassAdjData.getAdjType());
        l_toStringBuf.append(" - ");
        l_toStringBuf.append(i_scapServClassAdjData.getAdjCode());

        return l_toStringBuf.toString();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_scapServClassAdjData.getAdjCode().length();
    }
}
