////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME     : MarketsDbService.java
//    DATE          : 15-Aug-2005
//    AUTHOR        : M I Erskine
//    REFERENCE     : PRD_ASCS00_GEN_CA_44
//
//    COPYRIGHT     : WM-data 2005
//
//    DESCRIPTION   : Database access class for Account Groups screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy  | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiRegionData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.RegiRegionSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaMstrSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Account Groups screen.
 */
public class MarketsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for srva mstr market details. */
    private SrvaMstrSqlService i_srvaMstrSqlService     = null;
    
    /** SQL service for REGI region details. */
    private RegiRegionSqlService i_regiRegionSqlService = null;

    /** Vector of BoiMarket data objects. */
    private Vector               i_availableBoiMarketV  = null;
    
    /** Vector of available Srva Market Data objects. */
    private Vector               i_availableSrvaMarketV = null;
    
    /** Vector of available Home Regions. */
    private Vector               i_availableRegiRegionV = null;
    
    /** Srva Mstr data object holding the current data selected on the screen. */
    private SrvaMstrData         i_currentSrvaMstrData  = null;


    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public MarketsDbService(BoiContext p_context)
    {
        super(p_context);
        i_srvaMstrSqlService = new SrvaMstrSqlService();
        i_regiRegionSqlService = new RegiRegionSqlService(null, null, null);
        i_availableBoiMarketV = new Vector(5);
        i_availableSrvaMarketV = new Vector(5);
        i_availableRegiRegionV = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Set Market currently being worked on. Used for record lookup in database.
     * @param p_market Currently selected Market.
     */
    public void setCurrentMarket(BoiMarket p_market)
    {
        i_currentMarket = p_market;
    }
    
    /**
     * Set srva_mstr Market. Used for record lookup in database.
     * @param p_srvaMstrData Currently selected Market.
     */
    public void setCurrentSrvaMstrData(SrvaMstrData p_srvaMstrData)
    {
        i_currentSrvaMstrData = p_srvaMstrData;
    }
    

    /**
     * Return account group data currently being worked on.
     * @return Account group data object.
     */
    public BoiMarket getCurrentMarket()
    {
        return i_currentMarket;
    }
    

    /**
     * Return available Boi Market data.
     * @return Vector of available BoiMarket data records.
     */
    public Vector getAvailableBoiMarketData()
    {
        return i_availableBoiMarketV;
    }
    
    /**
     * Return available SrvaMstrData.
     * @return Vector of available SrvaMstr data records.
     */
    public Vector getAvailableSrvaMstrData()
    {
        return i_availableSrvaMarketV;
    }
    
    /**
     * Return available Boi Region data.
     * @return Vector of available BoiMarket data records.
     */
    public Vector getAvailableBoiRegionData()
    {
        return i_availableRegiRegionV;
    }
    
    /**
     * Return srva_mstr data currently being worked on.
     * @return The srva_mstr data currently being worked on
     */
    public SrvaMstrData getCurrentSrvaMstrMarket()
    {
        return i_currentSrvaMstrData;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaMstrDataSet   l_srvaMstrDataSet = null;
        int               l_srva = 0;
        int               l_sloc = 0;

        l_srva = Integer.parseInt(i_currentMarket.getMarket().getSrva());
        l_sloc = Integer.parseInt(i_currentMarket.getMarket().getSloc());
        l_srvaMstrDataSet = i_srvaMstrSqlService.readAllForBoi(null, p_connection);
        
        i_currentSrvaMstrData = l_srvaMstrDataSet.getRecord(l_srva, l_sloc);
    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        refreshSrvaMstrAndRegionsDataVector(p_connection);
    }

    /**
     * Inserts record into the srva_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_srvaMstrSqlService.insert(null,
                                    p_connection,
                                    i_context.getOperatorUsername(),
                                    i_currentMarket.getMarket().getSrva(),
                                    i_currentMarket.getMarket().getSloc(),
                                    i_currentMarket.getMarket().getName(),
                                    i_currentSrvaMstrData.getTeleFormat(),
                                    i_currentSrvaMstrData.getMsisdnQuarantinePeriod(),
                                    i_currentSrvaMstrData.getDefaultHomeRegion());

        refreshSrvaMstrAndRegionsDataVector(p_connection);
    }

    /**
     * Updates record in the srva_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        i_srvaMstrSqlService.update(null,
                                    p_connection,
                                    i_context.getOperatorUsername(),
                                    i_currentMarket.getMarket().getSrva(),
                                    i_currentMarket.getMarket().getSloc(),
                                    i_currentMarket.getMarket().getName(),
                                    i_currentSrvaMstrData.getTeleFormat(),
                                    i_currentSrvaMstrData.getMsisdnQuarantinePeriod(),
                                    i_currentSrvaMstrData.getDefaultHomeRegion());

        refreshSrvaMstrAndRegionsDataVector(p_connection);
    }

    /**
     * Deletes record from the srva_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        i_srvaMstrSqlService.delete(null, p_connection,
                                    i_context.getOperatorUsername(),
                                    i_currentMarket.getMarket().getSrva(),
                                    i_currentMarket.getMarket().getSloc());

        refreshSrvaMstrAndRegionsDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaMstrDataSet l_srvaMstrDataSet = null;
        SrvaMstrData l_srvaMstrData = null;
        boolean[] l_flagsArray = new boolean[2];
        
        l_srvaMstrDataSet = i_srvaMstrSqlService.readAll(null, p_connection);
        l_srvaMstrData = l_srvaMstrDataSet.getRecordIncludeWithdrawn(i_currentMarket.getMarket());
        
        if (l_srvaMstrData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_srvaMstrData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }

        return l_flagsArray;
    }

    /**
     * Marks a previously withdrawn record as available in the srva_mstr table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        i_srvaMstrSqlService.markAsAvailable(null,
                                             p_connection,
                                             i_currentMarket.getMarket().getSrva(),
                                             i_currentMarket.getMarket().getSloc(),
                                             i_context.getOperatorUsername());

        refreshSrvaMstrAndRegionsDataVector(p_connection);
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Refreshes the available Market data vector from the srva_mstr table. Also refreshes the available 
     * home region data.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshSrvaMstrAndRegionsDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaMstrDataSet l_srvaMstrDataSet = null;
        SrvaMstrData[]  l_srvaMstrArray   = null;
        RegiRegionDataSet l_regiDataSet   = null;
        RegiRegionData[]  l_regiDataArray = null;

        i_availableBoiMarketV.removeAllElements();
        i_availableSrvaMarketV.removeAllElements();

        l_srvaMstrDataSet = i_srvaMstrSqlService.readAllForBoi(null, p_connection);
        l_srvaMstrArray = l_srvaMstrDataSet.getAvailableArray();

        i_availableBoiMarketV.addElement(new String(""));
        i_availableBoiMarketV.addElement(new String("NEW RECORD"));
        for (int i = 0; i < l_srvaMstrArray.length; i++)
        {
            i_availableBoiMarketV.addElement(new BoiMarket(l_srvaMstrArray[i].getMarket()));
            i_availableSrvaMarketV.addElement(l_srvaMstrArray[i]);
        }
        
        i_availableRegiRegionV.removeAllElements();
        
        l_regiDataSet = i_regiRegionSqlService.readAll(null, p_connection);
        l_regiDataArray = l_regiDataSet.getAvailableArray();
        
        i_availableRegiRegionV.addElement(new String(""));
        for (int i = 0; i < l_regiDataArray.length; i++)
        {
            i_availableRegiRegionV.addElement(new BoiRegionData(l_regiDataArray[i]));
        }
    }
}