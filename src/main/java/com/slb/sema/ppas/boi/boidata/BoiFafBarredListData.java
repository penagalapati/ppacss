////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiFafBarredListData.java
//    DATE            :       25-Jan-2005
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#970/5611
//
//    COPYRIGHT       :       Atos Origin 2005
//
//    DESCRIPTION     :       Wrapper for FabaFafBarredListData class 
//                            within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListData;

/** Wrapper for Friends and Family barred list data class within BOI. */
public class BoiFafBarredListData extends DataObject
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Basic Friends and Family barred list data object */
    private FabaFafBarredListData i_barredListData;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_fafBarredListData Friends and Family barred list data object to wrapper.
     */
    public BoiFafBarredListData(FabaFafBarredListData p_fafBarredListData)
    {
        i_barredListData = p_fafBarredListData;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Return wrappered Friends and Family barred list data object.
     * @return Wrappered Friends and Family barred list data object.
     */
    public FabaFafBarredListData getInternalFafBarredListData()
    {
        return i_barredListData;
    }
    
    /**
     * Compares two Friends and Family barred list data objects and returns true if they equate.
     * @param p_fafBarredListData Friends and Family barred list data object to compare the current 
     *                            instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_fafBarredListData)
    {
        boolean               l_return = false;
        FabaFafBarredListData l_fafBarredListData = null;
        
        if ( p_fafBarredListData != null &&
             p_fafBarredListData instanceof BoiFafBarredListData)
        {
            l_fafBarredListData = ((BoiFafBarredListData)p_fafBarredListData).getInternalFafBarredListData();

            if (i_barredListData.getMarket().equals(l_fafBarredListData.getMarket()) &&
                i_barredListData.getNumberPrefix().equals(l_fafBarredListData.getNumberPrefix()) &&
                i_barredListData.getNumberLength() == l_fafBarredListData.getNumberLength() &&
                i_barredListData.getStartDate().equals(l_fafBarredListData.getStartDate()))
            {
                l_return = true;
            }
        }

        return l_return;
    }
    
    /**
     * Returns Friends and Family barred list data object as a String for display in BOI.
     * @return Friends and Family barred list data object as a string.
     */
    public String toString()
    {
        String l_displayString;
        String l_length;
        
        if (i_barredListData.getNumberLength() == 0)
        {
            l_length = "All";
        }
        else
        {
            l_length = i_barredListData.getNumberLength() + "";
        }
        
        l_displayString = new String(i_barredListData.getNumberPrefix() + 
                                     "  :  " + l_length +
                                     "  :  " + i_barredListData.getStartDate());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_barredListData.getNumberPrefix().length();
    }
}