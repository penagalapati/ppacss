////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ServiceOfferingsDbService.java
//      DATE            :       20-Aug-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PpacLon#495/3784
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Database access class for Service Offerings
//                              screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SeofServiceOfferingSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Service Offerings screen.
 */
public class ServiceOfferingsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Number of service offerings. */
    private static final int C_NUM_SERVICE_OFFERINGS = 31;

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for service offering details. */
    private SeofServiceOfferingSqlService i_seofSqlService;
    
    /** Set of service offering data objects. */
    private SeofServiceOfferingDataSet i_seofDataSet;

    /** Vector of service offering data objects. */
    private Vector i_allServiceOfferingsV;

    /** Service offering data object currently being worked on. */
    private BoiServiceOfferingData i_currentServiceOffering;

    /** Currently selected service offering code. Used as key for read and delete. */
    private int i_currentCode = -1;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * @param p_context BOI context.
     */
    public ServiceOfferingsDbService(BoiContext p_context)
    {
        super(p_context);
        i_seofSqlService = new SeofServiceOfferingSqlService(null, null);
        i_allServiceOfferingsV = new Vector(C_NUM_SERVICE_OFFERINGS + 1); 
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current service offering code. Used for record lookup in database. 
     * @param p_code Currently selected service offering code. 
     */
    public void setCurrentCode(int p_code)
    {
        i_currentCode = p_code;
    }

    /** 
     * Set service offering data object currently being worked on.
     * @param p_currentServiceOffering Service offering data object currently being worked on.
     */
    public void setCurrentServiceOffering(BoiServiceOfferingData p_currentServiceOffering)
    {
        i_currentServiceOffering = p_currentServiceOffering;
    }
    
    /** 
     * Return service offering data object currently being worked on.
     * @return Service offering data object currently being worked on.
     */
    public BoiServiceOfferingData getCurrentServiceOffering()
    {
        return i_currentServiceOffering;
    }
    
    /** 
     * Return service offering data. 
     * @return Vector of all service offering data records.
     */
    public Vector getAllServiceOfferings()
    {
        return i_allServiceOfferingsV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_currentServiceOffering = new BoiServiceOfferingData(i_seofDataSet.getRecord(i_currentCode));
    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshServiceOfferingVector(p_connection);
    }

    /** 
     * Updates record in the seof_service_offerings table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_seofSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_currentServiceOffering.getInternalSeofData().getServiceOfferingNumber(),
                                i_currentServiceOffering.getInternalSeofData().getServiceOfferingDescription());
    
        refreshServiceOfferingVector(p_connection);
    }

    /** 
     * Not required - there is no insert functionality for this screen.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }

    /**
     * Not required - there is no delete functionality for this screen.
     * @param p_connection a <code>JdbcConnection</code>
     * @throws PpasSqlException if an error occurs reading from the database
     */
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }

    /** 
     * Not required since there is no insert functionality for this screen.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) 
        throws PpasSqlException
    {
        return null;
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the service offerings vector from the 
     * seof_service_offerings table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshServiceOfferingVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        SeofServiceOfferingData[]  l_seofArray;
        
        i_allServiceOfferingsV.removeAllElements();
        
        i_seofDataSet = i_seofSqlService.readAll(null, p_connection);
        l_seofArray = i_seofDataSet.getAllArray();

        i_allServiceOfferingsV.addElement(new String(""));
        for (int i=0; i < l_seofArray.length; i++)
        {
            i_allServiceOfferingsV.addElement(new BoiServiceOfferingData(l_seofArray[i]));
        }
    }
}
