////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BanksPane.java
//      DATE            :       19 October 2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PpacLon#1755/7279
//                              PRD_ASCS00_GEN_CA_xx
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to maintain the Bank Codes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 18/11/05 | Lars L.    | Review comments corrections.    |PpacLon#1755/7469
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiBanksData;
import com.slb.sema.ppas.boi.dbservice.BanksDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/*******************************************************************************
 * BOI screen to maintain the Bank Codes.
 */
public class BanksPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Private variables (instance attributes).
    //-------------------------------------------------------------------------    
    /** Panel allowing data selection and modification. */
    private JPanel              i_detailsPanel      = null;

    /** Panel containing a table of existing records. */
    private JPanel              i_definedCodesPanel = null;

    /** Table containing existing records. */
    private JTable              i_table             = null;

    /** Existing Banks data vector. */
    private Vector              i_definedBanks      = null;   

    /** Bank Codes code field. */
    private ValidatedJTextField i_codeField         = null;

    /** Bank Codes name field. */
    private ValidatedJTextField i_nameField         = null;
    
    /** Data array for the table of existing records. */
    private String i_data[][]                       = null;

    /** Data model for table of existing records. */
    private StringTableModel    i_stringTableModel  = null;    

    /** Column names for the table of existing records. */
    private String[]            i_columnNames       = { "Code", "Name" };

    /** The vertical scroll bar used for the bank records table view port. */
    private JScrollBar          i_verticalScrollBar = null;


    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    /** 
     * BanksPane constructor. 
     * @param p_context A reference to the BoiContext.
     */        
    public BanksPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new BanksDbService((BoiContext)i_context);
        
        super.init();
    }


    /**
     * Handles mouse released event on the bank codes table.
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedBanks.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }


    //-------------------------------------------------------------------------
    // Protected methods ( overriding abstract methods in superclass).
    //-------------------------------------------------------------------------    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Bank Codes", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_BANK_CODES_SCREEN), 
                                i_helpComboListener);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox, "bankCodesHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        BankBankData l_bankData = null;
        l_bankData = ((BanksDbService)i_dbService).getBanksData().getInternalBankData();
        i_codeField.setText(l_bankData.getBankCode());
        i_nameField.setText(l_bankData.getBankName());

        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * 
     * @return <code>true</code> if valid, <code>false</code> otherwise.
     */
    protected boolean validateKeyData()
    {
        boolean l_validData = true;

        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            l_validData = false;
        }
        else
        {
            ((BanksDbService)i_dbService).setCurrentCode(i_codeField.getText());
            l_validData = true;
        }
        
        return l_validData;
    }

    /**
     * Validates screen data before update or insert.
     * 
     * @return  <code>true</code> if valid, <code>false</code> otherwise.
     */
    protected boolean validateScreenData()
    {
        boolean l_validData = true;

        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Code cannot be blank");
            l_validData = false;
        }
        
        if (i_nameField.getText().trim().equals(""))
        {
            i_nameField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Description cannot be blank");
            l_validData = false;
        }

        return l_validData;
    }

    /**
     * Writes current record fields to screen data object.
     * Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        BankBankData l_bankData = new BankBankData(null, i_codeField.getText(), i_nameField.getText(), " ");
        ((BanksDbService)i_dbService).setBanksData(new BoiBanksData(l_bankData));            
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiBanksData l_selectedItem = null;
        String       l_code         = null;
        
        l_selectedItem = (BoiBanksData)i_keyDataComboBox.getSelectedItem();
        l_code = l_selectedItem.getInternalBankData().getBankCode();
        ((BanksDbService)i_dbService).setCurrentCode(l_code);                
    }

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_nameField.setText("");
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
        
        i_codeField.setEnabled(false);
        i_nameField.setEnabled(false);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedBanks = ((BanksDbService)i_dbService).getAvailableBanksData();
        populateBanksTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedBanks);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((BanksDbService)i_dbService).getBanksData());
    }

    /**
     * Alters screen behaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_nameField.setEnabled(true);
    }
    
    /**
     * Alters screen behaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_nameField.setEnabled(true);
    }    

    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------
    /**************************************************************************
     * Creates the Details Panel.
     */    
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel         = null;
        JLabel l_nameLabel         = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createTextField(11);
        addValueChangedListener(i_codeField);
        
        l_nameLabel = WidgetFactory.createLabel("Name:");
        i_nameField = WidgetFactory.createTextField(40);
        addValueChangedListener(i_nameField);
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,1,70,4");
        i_detailsPanel.add(l_codeLabel, "codeLabel,1,6,15,4");
        i_detailsPanel.add(i_codeField, "codeField,17,6,15,4");
        i_detailsPanel.add(l_nameLabel, "nameLabel,1,11,15,4");
        i_detailsPanel.add(i_nameField, "nameField,17,11,50,4");

        i_detailsPanel.add(i_updateButton, "updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton,  "resetButton,33,27,15,5");
    }


    /**
     * Creates the Defined Codes Panel.
     */    
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(150, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    
    /**
     * Populates the Banks table.
     */    
    private void populateBanksTable()
    {
        BankBankData l_bankData = null;
        int          l_numRecords = 0;
        int          l_arraySize = 0;
        
        l_numRecords = i_definedBanks.size() -2;
        l_arraySize = ((l_numRecords > 21) ? l_numRecords : 22);
        i_data = new String[l_arraySize][2];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_definedBanks.size(); i++, j++)
        {
            l_bankData = ((BoiBanksData)i_definedBanks.elementAt(i)).getInternalBankData();
            i_data[j][0] = l_bankData.getBankCode();
            i_data[j][1] = l_bankData.getBankName();
        }

        i_stringTableModel.setData(i_data);
    }    
}
