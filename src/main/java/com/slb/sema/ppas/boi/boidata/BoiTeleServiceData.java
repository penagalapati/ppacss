// //////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME : BoiTeleServiceData.java
//      DATE : 10-Mar-2006
//      AUTHOR : Mikael Alm
//      REFERENCE : PpacLon#2035/8098
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT : WM-data 2006
//
//      DESCRIPTION : Wrapper for TeleTeleserviceData class
//                              within BOI.
//                              Supports toString and equals methods for use
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE | NAME | DESCRIPTION | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name> | <brief description of | <reference>
//          | | change> |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceData;
import com.slb.sema.ppas.common.dataclass.DataObject;

/** Wrapper for teleservice class within BOI. */
public class BoiTeleServiceData extends DataObject
{
    /** Basic teleservice data object. */
    private TeleTeleserviceData i_teleData;

    /**
     * Simple constructor.
     * @param p_teleData Teleservice data object to wrapper.
     */
    public BoiTeleServiceData(TeleTeleserviceData p_teleData)
    {
        i_teleData = p_teleData;
    }

    /**
     * Return wrappered teleservice data object.
     * @return Wrappered teleservice data object.
     */
    public TeleTeleserviceData getInternalTeleserviceData()
    {
        return i_teleData;
    }

    /**
     * Compares two teleservice data objects and returns true if they equate.
     * @param p_teleData Teleservice data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_teleData)
    {
        boolean l_return = false;

        if (p_teleData != null
                && p_teleData instanceof BoiTeleServiceData
                && i_teleData.getTeleserviceCode() == ((BoiTeleServiceData)p_teleData)
                        .getInternalTeleserviceData().getTeleserviceCode())
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns teleservice data object as a String for display in BOI.
     * @return teleservice data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_teleData.getTeleserviceCode() + " - "
                + i_teleData.getTeleserviceDescription());
        return l_displayString;
    }

    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object.
     */
    public int hashCode()
    {
        return i_teleData.getTeleserviceCode();
    }
}