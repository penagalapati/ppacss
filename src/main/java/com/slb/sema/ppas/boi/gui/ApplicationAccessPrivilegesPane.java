////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ApplicationAccessPrivilegesPane.java
//    DATE            :       03-Jun-2004
//    AUTHOR          :       Mario Imfeld
//    REFERENCE       :       PpacLon#287/2622
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Screen to allow a BMO to assign or remove permission for
//                            different types of CSR to access individual
//                            Application screens.

////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.slb.sema.ppas.boi.dbservice.ApplicationAccessDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * Application Access Configuration Screen. 
 */
public class ApplicationAccessPrivilegesPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Private instance attributes
    //-------------------------------------------------------------------------       

    /** Panel to hold the application access privilege selection combobox. */    
    private JPanel                          i_topPanel                  = null;

    /** Panel to hold the checkboxes for the application access privileges. */
    private JPanel                          i_bottomPanel               = null;
    
    /** Check box to hold Batch access privilege. */
    private JCheckBox                       i_batchBox                  = null;

    /** Check box to hold Business Configuratino access privilege. */
    private JCheckBox                       i_businessConfigurationBox  = null;
    
    /** Check box to hold Purge and Archive access privilege. */
    private JCheckBox                       i_purgeAndArchiveBox        = null;

    /** Check box to hold Reports access privilege. */
    private JCheckBox                       i_reportsBox                = null; 
    
    /** 
     * Vector containing the available privilege data objects and an
     * empty String to be displayed in the key data combo box.
     */
    private Vector                          i_availablePrivilegeDataV   = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** 
     * ApplicationAccessPane constructor. 
     * @param p_context A reference to the BoiContext
     * 
     */
    public ApplicationAccessPrivilegesPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new ApplicationAccessDbService((BoiContext)i_context);
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------    

    /** 
     * Must implement abstract method from <code>GuiPane</code>.
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(
        GuiEvent                p_event)
    {
        
    } // End of public method guiEventOccurred().
    
    /**
     * Method that is invoked if an <code>ActionEvent</code> has occurred.
     * @param p_event The ActionEvent that has occurred
     */
    public void actionPerformed(ActionEvent p_event)
    {
        // Not used in this pane.
        
    } // End of public method actionPerformed().

    /**
     * Handles keyboard events for this screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean             l_return = false;
        
        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED)
        {
            if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else if ((p_keyEvent.getComponent() == i_updateButton) ||
                     (p_keyEvent.getComponent() == i_keyDataComboBox))
            {
                i_updateButton.doClick();
            }
            else
            {
                // Source must be a JCheckBox
                JCheckBox l_checkBox = (JCheckBox)p_keyEvent.getSource();
                boolean l_selected = l_checkBox.isSelected();
                boolean l_setSelected = (l_selected ? false : true); 
                     
                ((JCheckBox)p_keyEvent.getSource()).setSelected(l_setSelected);
            }
            l_return = true;
        }

        return l_return;
        
    } // End of public method dispatchKeyEvent().
    
    //-------------------------------------------------------------------------
    // Protected Methods
    //-------------------------------------------------------------------------  
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Application Access Privileges", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                         BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_APP_ACCESS_PRIV_SCREEN), 
                                         i_helpComboListener);
        i_helpComboBox.setFocusable(false);
        i_mainPanel.add(i_helpComboBox, "appAccessPrivHelpComboBox,60,1,40,4");
                              
        i_topPanel = createTopPanel();
        i_mainPanel.add(i_topPanel, "topPanel,1,6,100,15");

        i_bottomPanel = createBottomPanel();
        i_mainPanel.add(i_bottomPanel, "bottomPanel,1,22,100,73");
        
        // Add Buttons that were created in superclass to screen.
        // N.B: No delete button on this screen
        i_mainPanel.add(i_updateButton, "updateButton,1,96,15,5");
        i_mainPanel.add(i_resetButton, "resetButton,17,96,15,5");
    }    

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        // No validation required
        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        // No validation required
        return true;
    }

    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {   
        ApacApplicationAccessData l_applicationAccessData = null;
        
        l_applicationAccessData = 
            new ApacApplicationAccessData(
                    null,
                    ((PrilPrivilegeLevelData)i_keyDataComboBox.getSelectedItem()).getPrivilegeLevel(),
                    i_batchBox.isSelected() ? 'Y' : 'N',
                    i_businessConfigurationBox.isSelected() ? 'Y' : 'N',
                    i_purgeAndArchiveBox.isSelected() ? 'Y' : 'N',
                    i_reportsBox.isSelected() ? 'Y' : 'N',
                    ((BoiContext)i_context).getOperatorUsername(),
                    null);

        ((ApplicationAccessDbService)i_dbService).setApplicationAccessData(l_applicationAccessData);
    }

    /**
     * Writes key screen data to screen data object for use in record selection and deletion.
     */  
    protected void writeKeyData()
    {
        PrilPrivilegeLevelData    l_currentPrivilegeData;

        l_currentPrivilegeData = (PrilPrivilegeLevelData)i_keyDataComboBox.getSelectedItem();
        ((ApplicationAccessDbService)i_dbService).setCurrentPrivilegeData(l_currentPrivilegeData);
    }

    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_batchBox.setSelected(false);
        i_purgeAndArchiveBox.setSelected(false);
        i_businessConfigurationBox.setSelected(false);
        i_reportsBox.setSelected(false);
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        ApacApplicationAccessData l_applicationAccessData;
        
        l_applicationAccessData = ((ApplicationAccessDbService)i_dbService).getApplicationAccessData();

        i_purgeAndArchiveBox.setSelected(l_applicationAccessData.hasPurgeAndArchiveApplicationAccess());
        i_batchBox.setSelected(l_applicationAccessData.hasBatchApplicationAccess());
        i_reportsBox.setSelected(l_applicationAccessData.hasReportsApplicationAccess());
        i_businessConfigurationBox.setSelected(l_applicationAccessData.hasBusinessConfigurationApplicationAccess());
    }    

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_availablePrivilegeDataV = ((ApplicationAccessDbService)i_dbService).getAvailablePrivilegesV();
        i_keyDataModel = new DefaultComboBoxModel(i_availablePrivilegeDataV);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((ApplicationAccessDbService)i_dbService).getCurrentPrivilegeData());
    }

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------   

    /**
     * Private method to create the Top Panel and add components to it.
     * @return A <code>JPanel</code> object.
     */
    private JPanel createTopPanel()
    {
        JLabel l_definedPrivilegesLabel = null;
                        
        i_topPanel = WidgetFactory.createPanel(100, 15, 0, 0);
        l_definedPrivilegesLabel = WidgetFactory.createLabel("Privilege:");
        i_topPanel.add(l_definedPrivilegesLabel, "definedPrivilegesLabel,1,1,8,5");
        i_topPanel.add(i_keyDataComboBox, "privilegeBox,10,1,50,5");
       
        return i_topPanel;
        
    } // End of private method createTopPanel().

    /**
     * Private method to create the Bottom Panel and add checkboxes to it.
     * @return A <code>JPanel</code> object.
     */
    private JPanel createBottomPanel()
    {
        i_bottomPanel = WidgetFactory.createPanel(100, 73, 0, 0);

        i_batchBox = WidgetFactory.createCheckBox("Batch");
        addValueChangedListener(i_batchBox);
        i_businessConfigurationBox = WidgetFactory.createCheckBox("Business Configuration");
        addValueChangedListener(i_businessConfigurationBox);
        i_purgeAndArchiveBox = WidgetFactory.createCheckBox("Purge and Archive");
        addValueChangedListener(i_purgeAndArchiveBox);       
        i_reportsBox = WidgetFactory.createCheckBox("Reports");
        addValueChangedListener(i_reportsBox);
                
        i_bottomPanel.add(i_batchBox, "batchBox,1,1,25,5");
        i_bottomPanel.add(i_businessConfigurationBox, 
                          "businessConfigurationBox,1,7,25,5");                          
        i_bottomPanel.add(i_purgeAndArchiveBox, "purgeAndArchiveBox,1,13,25,5");                           
        i_bottomPanel.add(i_reportsBox, "reportsBox,1,19,25,5");
             
        return i_bottomPanel;
        
    } // End of private method createBottomPanel().

} // end public class