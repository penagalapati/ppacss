////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiAgentInfoData.java
//      DATE            :       28-Nov-2005
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#1755/7500
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Wrapper for SrvaAgentMstrData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.dataclass.DataObject;

/** Wrapper for Agent class within BOI. */
public class BoiAgentInfoData extends DataObject
{
    /** Basic Agent data object. */
    private SrvaAgentMstrData i_agentInfoData;
    
    /**
     * Simple constructor.
     * @param p_agentInfoData Agent data object to wrapper.
     */
    public BoiAgentInfoData(SrvaAgentMstrData p_agentInfoData)
    {
        i_agentInfoData = p_agentInfoData;
    }
    
    /**
     * Return wrappered agentInfo data object.
     * @return Wrappered agentInfo data object.
     */
    public SrvaAgentMstrData getInternalAgentInfoData()
    {
        return i_agentInfoData;
    }
    
    /**
     * Compares two agentInfoData data objects and returns true if they equate.
     * @param p_agentInfoData AgentInfoData data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_agentInfoData)
    {
        boolean l_return = false;
        SrvaAgentMstrData l_agentInfoData = null;
        
        if (p_agentInfoData != null && p_agentInfoData instanceof BoiAgentInfoData)
        {
            l_agentInfoData = ((BoiAgentInfoData)p_agentInfoData).getInternalAgentInfoData();
            
            if (i_agentInfoData.getMarket().equals(l_agentInfoData.getMarket()) && 
                i_agentInfoData.getAgent().equals(l_agentInfoData.getAgent()))
            {               
                l_return = true;
            }
        } 
        return l_return;
    }    
    /**
     * Returns AgentInfo data object as a String for display in BOI.
     * @return AgentInfo data object as a string.
     */
    public String toString()
    {
        StringBuffer l_displayString = new StringBuffer(i_agentInfoData.getAgent());
        l_displayString.append(" - "); 
        l_displayString.append(i_agentInfoData.getAgentName());
        return l_displayString.toString();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_agentInfoData.getAgent().length();
    }
}