////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CustomerCareAccessPrivilegesPaneUT.java
//      DATE            :       14-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5502
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for 
//                              CustomerCareAccessPrivilegesPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for CustomerCareAccessPrivilegesPane. */
public class CustomerCareAccessPrivilegesPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "CustomerCareAccessPrivilegesPaneUT";
    
    /** Constant for privilege level id. */
    private static final String C_PRIV_LEVEL = "88";
    
    /** Constant for privilege level description. */
    private static final String C_PRIV_DESCRIPTION = "Privilege level description";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Customer Care Access Privileges screen. */
    private CustomerCareAccessPrivilegesPane i_ccAccessPrivilegesPane;
    
    /** Privilege level code field. */
    private JFormattedTextField i_privilegeLevelField = null;
    
    /** Privilege level description field. */
    private ValidatedJTextField i_descriptionField = null;
    
    /** Check box for new subscription screen privilege. */
    private JCheckBox i_newSubscriptionBox = null;
    
    /** Check box for subscriber details screen privilege. */
    private JCheckBox i_subscriberDetailsBox = null;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public CustomerCareAccessPrivilegesPaneUT(String p_title)
    {
        super(p_title);
        
        i_ccAccessPrivilegesPane = new CustomerCareAccessPrivilegesPane(c_context);
        super.init(i_ccAccessPrivilegesPane);
        
        i_ccAccessPrivilegesPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedPrivilegesComboBox");
        i_privilegeLevelField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                   "privilegeLevelField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_newSubscriptionBox = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, "newSubscriptionBox");
        i_subscriberDetailsBox = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                          "subscriberDetailsBox");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(CustomerCareAccessPrivilegesPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record 
     *     available again through the Customer Care Access Privileges screen screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        PrilPrivilegeLevelData l_prilData;
        
        beginOfTest("CustomerCareAccessPrivilegesPane test");
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_ccAccessPrivilegesPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_privilegeLevelField, C_PRIV_LEVEL);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_PRIV_DESCRIPTION);
        SwingTestCaseTT.setCheckBoxValue(i_newSubscriptionBox, true);
        SwingTestCaseTT.setCheckBoxValue(i_subscriberDetailsBox, false);
        doInsert(i_ccAccessPrivilegesPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_prilData = createPrivilegeLevelData(C_PRIV_LEVEL, C_PRIV_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_ccAccessPrivilegesPane, i_keyDataCombo, l_prilData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, "New priv level description");
        SwingTestCaseTT.setCheckBoxValue(i_subscriberDetailsBox, true);
        doUpdate(i_ccAccessPrivilegesPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_ccAccessPrivilegesPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_ccAccessPrivilegesPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_privilegeLevelField, C_PRIV_LEVEL);
        doMakeAvailable(i_ccAccessPrivilegesPane, i_updateButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deletePrivilegeRecord(C_PRIV_LEVEL);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deletePrivilegeRecord(C_PRIV_LEVEL);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a PrilPrivilegeLevelData object with the supplied data.
     * @param p_privilegeLevelId Privilege level id.
     * @param p_privilegeLevelDesc Privilege level description.
     * @return PrilPrivilegeLevelData object created from the supplied data.
     */
    private PrilPrivilegeLevelData createPrivilegeLevelData(String p_privilegeLevelId,
                                                            String p_privilegeLevelDesc)
    {
        PrilPrivilegeLevelData l_prilData;
        
        l_prilData = new PrilPrivilegeLevelData(null,
                                                Integer.parseInt(p_privilegeLevelId),
                                                p_privilegeLevelDesc,
                                                " ");
        return l_prilData;
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deletePrivilegeRecord = "deletePrivilegeRecord";
    
    /**
     * Removes rows from GOPA_GUI_OPERATOR_ACCESS, APAC_APPLICATION_ACCESS and PRIL_PRIVILEGE_LEVEL
     * for given privilege level id.
     * @param p_privilegeId Privilege level id.
     */
    private void deletePrivilegeRecord(String p_privilegeId)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = "DELETE from gopa_gui_operator_access WHERE gopa_privilege_id = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_privilegeId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deletePrivilegeRecord);
        
        l_sql = "DELETE from apac_application_access WHERE apac_privilege_id = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_privilegeId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deletePrivilegeRecord);
        
        l_sql = "DELETE from pril_privilege_level WHERE pril_level = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_privilegeId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deletePrivilegeRecord);
    }
}
