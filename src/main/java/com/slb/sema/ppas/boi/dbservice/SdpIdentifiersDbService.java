////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SdpIdentifiersDbService.java
//    DATE            :       01-Nov-2005
//    AUTHOR          :       Yang Liu
//    REFERENCE       :       PpacLon#1755/7463
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Database access class for SDP Identifiers screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction.     | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScpiScpInfoSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.gui.BoiContext;

/**
 * Database access class for SDP Identifiers screen.
 */
public class SdpIdentifiersDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    //Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for SDP identifiers code details. */
    private ScpiScpInfoSqlService i_scpiInfoSqlService           = null;

    /** Vector of SDP identifiers records. */
    private Vector                i_availableSDPIdentifiersDataV = null;

    /** SDP identifiers data. */
    private BoiSdpIdData          i_sdpIdData                    = null;

    /** Currently selected SDP identifiers code. Used as key for read and delete. */
    private String                i_currentCode                  = null;

    //-------------------------------------------------------------------------
    //Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public SdpIdentifiersDbService(BoiContext p_context)
    {
        super(p_context);
        i_scpiInfoSqlService = new ScpiScpInfoSqlService(null);
        i_availableSDPIdentifiersDataV = new Vector(7);
    }

    //-------------------------------------------------------------------------
    //Public methods
    //-------------------------------------------------------------------------

    /**
     * Set current SDP identifiers code. Used for record lookup in database.
     * @param p_code Currently selected SDP identifiers code.
     */
    public void setCurrentCode(String p_code)
    {
        i_currentCode = p_code;
    }

    /**
     * Return SDP identifiers data currently being worked on.
     * @return SDP identifiers data object.
     */
    public BoiSdpIdData getSdpIdData()
    {
        return i_sdpIdData;
    }

    /**
     * Set SDP identifiers data currently being worked on.
     * @param p_sdpIdData SDP identifiers data object.
     */
    public void setSdpIdData(BoiSdpIdData p_sdpIdData)
    {
        i_sdpIdData = p_sdpIdData;
    }

    /**
     * Return SDP identifiers data.
     * @return Vector of available SDP identifiers data records.
     */
    public Vector getAvailableSdpIdentifiersData()
    {
        return i_availableSDPIdentifiersDataV;
    }

    //-------------------------------------------------------------------------
    //Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        ScpiScpInfoDataSet l_scpiscpInfoDataSet = null;

        l_scpiscpInfoDataSet = i_scpiInfoSqlService.readAll(null, p_connection);

        i_sdpIdData = new BoiSdpIdData(l_scpiscpInfoDataSet.getRecordIncludeDeleted(i_currentCode));

    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        refreshSDPIdentifiersDataVector(p_connection);
    }

    /**
     * Inserts record into the scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_scpiInfoSqlService.insert(null,
                                    p_connection,
                                    i_sdpIdData.getInternalSdpIdData().getScpId(),
                                    i_sdpIdData.getInternalSdpIdData().getDescription(),
                                    i_context.getOperatorUsername(),
                                    i_sdpIdData.getInternalSdpIdData().getScpIpAddress());

        refreshSDPIdentifiersDataVector(p_connection);
    }

    /**
     * Updates record in the scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        i_scpiInfoSqlService.update(null,
                                    p_connection,
                                    i_sdpIdData.getInternalSdpIdData().getScpId(),
                                    i_sdpIdData.getInternalSdpIdData().getDescription(),
                                    i_context.getOperatorUsername());

        refreshSDPIdentifiersDataVector(p_connection);
    }

    /**
     * Deletes record from the scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        i_scpiInfoSqlService.delete(null, p_connection, i_context.getOperatorUsername(), i_sdpIdData
                .getInternalSdpIdData().getScpId());

        refreshSDPIdentifiersDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        ScpiScpInfoDataSet l_scpiscpInfoDataSet = null;
        ScpiScpInfoData l_dbScpiscpInfoData = null;
        boolean[] l_flagsArray = new boolean[2];

        l_scpiscpInfoDataSet = i_scpiInfoSqlService.readAll(null, p_connection);

        l_dbScpiscpInfoData = l_scpiscpInfoDataSet.getRecordIncludeDeleted(i_currentCode);
        if (l_dbScpiscpInfoData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbScpiscpInfoData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        return l_flagsArray;
    }

    /**
     * Marks a previously withdrawn record as available in the scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        i_scpiInfoSqlService.markAsAvailable(null, p_connection, i_currentCode, i_context
                .getOperatorUsername());

        refreshSDPIdentifiersDataVector(p_connection);
    }

    //-------------------------------------------------------------------------
    //Private methods
    //-------------------------------------------------------------------------

    /**
     * Refreshes the available SDP identifiers data vector from the scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshSDPIdentifiersDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        ScpiScpInfoDataSet l_scpiscpInfoDataSet = null;
        ScpiScpInfoData[] l_scpiscpInfoArray = null;

        i_availableSDPIdentifiersDataV.removeAllElements();

        l_scpiscpInfoDataSet = i_scpiInfoSqlService.readAll(null, p_connection);
        l_scpiscpInfoArray = l_scpiscpInfoDataSet.getAvailableArray();

        i_availableSDPIdentifiersDataV.addElement(new String(""));
        i_availableSDPIdentifiersDataV.addElement(new String("NEW RECORD"));
        for (int i = 0; i < l_scpiscpInfoArray.length; i++)
        {
            i_availableSDPIdentifiersDataV.addElement(new BoiSdpIdData(l_scpiscpInfoArray[i]));
        }
    }
}