////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentTypePaneUT.java
//      DATE            :       23 November 2005
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PpacLon#1755/7483
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       JUnit test class for the Adjustment Types pane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;



/** JUnit test class for AccountGroupsPane. */
public class AdjustmentTypePaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AdjustmentTypePaneUT";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Dedicated Accounts screen. */
    private AdjustmentTypePane           i_adjustmentTypesPane;

    /** Dedicated Accounts code field. */
    private JTextField                   i_codeField;

    /** Dedicated Accounts description field. */
    private ValidatedJTextField          i_descriptionField;

    /** market combo box. */
    private JComboBox                    i_marketDataComboBox;
    
    /** Constant defining the type of misc codes. */
    private static final String          C_TABLE             = "ADT";

    /** Constant defining a Adjustment type code. */
    private static final String          C_ADT_CODE          = "GOY";

    /** Constant defining a Adjustment type description.*/
    private static final String          C_ADT_DESCRIPTION   = "Test description";

    /** Constant defining a Market for testing. */
    private static final BoiMarket       C_ADT_MARKET        = new BoiMarket(new Market(2, 3));

    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton   i_updateButton;

    /** Delete button. */
    protected JButton   i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
    * Required constructor for JUnit testcase.
    * Any subclass of TestCase must implement a constructor
    * that takes a test case name as its argument
    * @param p_title The testcase name.
    */
    public AdjustmentTypePaneUT(String p_title)
    {
        super(p_title);

        i_adjustmentTypesPane = new AdjustmentTypePane(c_context);
        super.init(i_adjustmentTypesPane);

//        super.i_frame.setSize((Toolkit.getDefaultToolkit().getScreenSize().width/2),
//                              (Toolkit.getDefaultToolkit().getScreenSize().height/2));


        i_adjustmentTypesPane.resetScreenUponEntry();
        i_marketDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                        "marketBox");
        i_keyDataCombo       = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                        "definedCodesBox");
        i_codeField          = (JTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                         "codeField");
        i_descriptionField   = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                  "descriptionField");
        i_updateButton       = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton       = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /** 
    * Static method that allows the framework to automatically run all the tests in the class. A program
    * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
    * get an instance of the TestCase which it then executes.
    * @return an instance of <code>junit.framework.Test</code>
    */
    public static Test suite()
    {
        return new TestSuite(AdjustmentTypePaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** 
    * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
    *     through the Adjustment Types screen.
    * @ut.then the operations are successfully performed.
    * @ut.attributes +f
    */
    public void testDatabaseUpdates()
    {
        beginOfTest("AdjustmentTypesPane test");

        BoiMiscCodeData l_boiAdjustmentTypesData;


        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        SwingTestCaseTT.setMarketComboSelectedItem(i_adjustmentTypesPane, 
                                                   i_marketDataComboBox, 
                                                   C_ADT_MARKET);
        SwingTestCaseTT.setKeyComboSelectedItem(i_adjustmentTypesPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_ADT_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_ADT_DESCRIPTION);
        doInsert(i_adjustmentTypesPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiAdjustmentTypesData = createAdjustmentTypesData(C_ADT_MARKET, 
                                                             C_ADT_CODE, 
                                                             C_ADT_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_adjustmentTypesPane,
                                                i_keyDataCombo,
                                                l_boiAdjustmentTypesData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_adjustmentTypesPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_adjustmentTypesPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_adjustmentTypesPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_ADT_CODE);
        doMakeAvailable(i_adjustmentTypesPane, i_updateButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
    * This method is used to setup anything required by each test. 
    */
    protected void setUp()
    {
        deleteAdjustmentTypeRecord(C_ADT_MARKET, C_ADT_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteAdjustmentTypeRecord(C_ADT_MARKET, C_ADT_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /**
    * Creates a BoiDedicatedAccountsData object with the supplied data.
    * @param p_market Dedicated Accounts market.
    * @param p_code Adjustment Type code.
    * @param p_description Adjustment Type description.
    * @return MiscCodeData object created from the supplied data.
    */
    private BoiMiscCodeData createAdjustmentTypesData(BoiMarket p_market,
                                                      String    p_code,
                                                      String    p_description)
    {
        
        MiscCodeData l_adjustmentTypesData;

        l_adjustmentTypesData = new MiscCodeData(p_market.getMarket(),
                                                 "ADT",
                                                 p_code,
                                                 p_description,
                                                 " ");

        return new BoiMiscCodeData(l_adjustmentTypesData);
    }

    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteAdjustmentTypeRecord = "deleteAdjustmentTypeRecord";

    /**
    * Removes a row from deda_dedicated_accounts.
    * @param p_dedicatedAccountId Dedicated Account id.
    * @param p_market
    * @param p_serviceClass
    */
    private void deleteAdjustmentTypeRecord( BoiMarket p_market,
                                             String    p_miscCode)
    {
        String    l_sql;
        SqlString l_sqlString;
        l_sql = new String("DELETE FROM srva_misc_codes " +
                           "WHERE srva  = {0} " +
                           "and sloc = {1} " +
                           "and misc_table = {2} " +
                           "and misc_code  = {3}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_market.getMarket().getSrva());
        l_sqlString.setStringParam(1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam(2, C_TABLE);
        l_sqlString.setStringParam(3, p_miscCode);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteAdjustmentTypeRecord);
    }
}