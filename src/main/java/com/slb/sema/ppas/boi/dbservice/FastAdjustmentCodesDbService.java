////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       FastAdjustmentCodesDbService.java
//DATE            :       12-Dec-2005
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PpacLon#1755/7585
//
//COPYRIGHT       :       WM-data 2005
//
//DESCRIPTION     :       Database access class for Fast Adjustment Codes screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiFastAdjustmentCodesData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CufmCurrencyFormatsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScapServClassAdjParamSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAdjCodesSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;


/*******************************************************************************
 * This <code>FastAdjustmentCodesDbService</code> class serves as the database
 * access class for the Fast Adjustment Codes screen.
 */
public class FastAdjustmentCodesDbService extends BoiDbService
{
    //==========================================================================
    // Private attribute(s).                                                  ==
    //==========================================================================
    /** The wrapped data object. */
    private ScapServClassAdjParamSqlService i_scapServClassAdjParamSqlService = null;

    /** The SQL service for adjustment code details. */
    private SrvaAdjCodesSqlService          i_srvaAjCodesSqlService           = null;

    /** The SQL service for adjustment code details. */
    private CufmCurrencyFormatsSqlService   i_currencyFormatSqlService        = null;

    /** The available Adjustment Types. */
    private Vector                          i_availableAdjustmentTypesV       = null;

    /** Currently selected Adjustment Type. */
    private String                          i_currentAdjustmentType           = null;

    /** The available Adjustment Codes. */
    private Vector                          i_availableAdjustmentCodesV       = null;

    /** The Fast Adjustment Codes. */
    private Vector                          i_availableFastAdjustmentCodesV   = null;

    /** The available currency codes. */
    private Vector                          i_availableCurrencyCodesV         = null;

    /** The defined currency formats (in the CUFM_CURRENCY_FORMATS table). */
    private CufmCurrencyFormatsDataSet      i_currencyFormatsDataSet          = null;

    /** The currently used fast adjustment code data object. */
    private BoiFastAdjustmentCodesData      i_fastAdjustmentCodeData          = null;

    /** The currently selected adjustment code. */
    private String                          i_currentAdjustmentCode           = null;

    /** The currently used adjustment code data object. */
    private BoiAdjCodesData                 i_adjustmentCodeData              = null;

    
    //==========================================================================
    // Constructor(s).                                                        ==
    //==========================================================================
    /***************************************************************************
     * Constructs an instance of this <code>FastAdjustmentCodesDbService</code>
     * class using the passed <code>BoiContext</code> object.
     * 
     * @param p_context  the <code>BoiContext</code> object.
     */
    public FastAdjustmentCodesDbService(BoiContext p_context)
    {
        super(p_context);
        i_scapServClassAdjParamSqlService = new ScapServClassAdjParamSqlService(null);
        i_srvaAjCodesSqlService           = new SrvaAdjCodesSqlService(null, null);
        i_currencyFormatSqlService        = new CufmCurrencyFormatsSqlService(null);
        i_availableAdjustmentTypesV       = new Vector(10);
        i_availableAdjustmentCodesV       = new Vector(10);
        i_availableFastAdjustmentCodesV   = new Vector(10);
        i_availableCurrencyCodesV         = new Vector(10);
    }


    //==========================================================================
    // Public method(s).                                                      ==
    //==========================================================================
    /***************************************************************************
     * Returns the available adjustment types as a <code>Vector</code> of
     * <code>String</code>s.
     * 
     * @return the available adjustment types as a <code>Vector</code> of
     *         <code>String</code>s.
     */
    public Vector getAvailableAdjustmentTypes()
    {
        return i_availableAdjustmentTypesV;
    }


    /***************************************************************************
     * Sets the currently used adjustment type.
     * 
     * @param p_adjType  the adjustment type.
     */
    public void setCurrentAdjustmentType(String p_adjType)
    {
        i_currentAdjustmentType = p_adjType;
    }


    /***************************************************************************
     * Returns the currently used adjustment type.
     * 
     * @return the currently used adjustment type.
     */
    public String getCurrentAdjustmentType()
    {
        return i_currentAdjustmentType;
    }


    /***************************************************************************
     * Sets the currently selected adjustment code.
     * 
     * @param p_adjCode  the currentAdjustmentCode to set.
     */
    public void setCurrentAdjustmentCode(String p_adjCode)
    {
        i_currentAdjustmentCode = p_adjCode;
    }


    /***************************************************************************
     * Returns the available adjustment types as a <code>Vector</code> of
     * <code>BoiAdjCodesData</code> objects.
     * 
     * @return the available adjustment types as a <code>Vector</code> of
     *         <code>BoiAdjCodesData</code> objects.
     */
    public Vector getAvailableAdjustmentCodes()
    {
        return i_availableAdjustmentCodesV;
    }


    /***************************************************************************
     * Returns the available fast adjustment codes as a <code>Vector</code> of
     * <code>BoiFastAdjustmentCodesData</code> objects.
     * 
     * @return the available fast adjustment codes as a <code>Vector</code> of
     *         <code>BoiFastAdjustmentCodesData</code> objects.
     */
    public Vector getAvailableFastAdjCodes()
    {
        return i_availableFastAdjustmentCodesV;
    }


    /***************************************************************************
     * Returns the available currency codes as a <code>Vector</code> of
     * <code>String</code>s.
     * 
     * @return the available currency codes as a <code>Vector</code> of
     *         <code>String</code>s.
     */
    public Vector getAvailableCurrencyCodes()
    {
        return i_availableCurrencyCodesV;
    }


    /***************************************************************************
     * Returns the precision for the given currency code.
     * A value of -1 is returned if the currency has not been configured. 
     * 
     * @param p_currencyCode  the currency code.
     * @return the precision for the given currency code.
     */
    public int getPrecision(String p_currencyCode)
    {
        int l_precision = -1;

        if (i_currencyFormatsDataSet != null)
        {
            l_precision = i_currencyFormatsDataSet.getPrecision(p_currencyCode);
        }

        return l_precision;
    }


    /***************************************************************************
     * Returns the currently used fast adjustment code data object.
     * 
     * @return the currently used fast adjustment code data object.
     */
    public BoiFastAdjustmentCodesData getFastAdjustmentCodeData()
    {
        return i_fastAdjustmentCodeData;
    }


    /***************************************************************************
     * Sets the used fast adjustment code data object.
     * 
     * @param p_fastAdjustmentCodeData  the fast adjustment code data object.
     */
    public void setFastAdjustmentCodeData(BoiFastAdjustmentCodesData p_fastAdjustmentCodeData)
    {
        i_fastAdjustmentCodeData = p_fastAdjustmentCodeData;
    }


    /***************************************************************************
     * Returns the currently used adjustment code data object.
     * 
     * @return the currently used adjustment code data object.
     */
    public BoiAdjCodesData getAdjustmentCodeData()
    {
        return i_adjustmentCodeData;
    }


    //==========================================================================
    // Protected method(s) (overriding abstract methods in the superclass).   ==
    //==========================================================================
    /***************************************************************************
     * Populates this data object with data from the database when an existing
     * record is selected.
     * 
     * @param p_connection  the database connection.
     * 
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        ScapServClassAdjDataSet l_scapServClassAdjDataSet = null;
        ScapServClassAdjData    l_scapServClassAdjData    = null;
        SrvaAdjCodesDataSet     l_srvaAdjCodesDataSet     = null;
        SrvaAdjCodesData        l_srvaAdjCodesData        = null;

        // Update the the currently used fast adjustment code data object.
        l_scapServClassAdjDataSet = i_scapServClassAdjParamSqlService.readAll(null, p_connection);
        l_scapServClassAdjData    =
            l_scapServClassAdjDataSet.getScapServClassAdjData(i_currentAdjustmentCode,
                                                              i_currentAdjustmentType);
        i_fastAdjustmentCodeData = new BoiFastAdjustmentCodesData(l_scapServClassAdjData);

        // Update the the currently used adjustment code data object.
        l_srvaAdjCodesDataSet = i_srvaAjCodesSqlService.readDistinctAdjTypesAndCodes(null, p_connection);
        l_srvaAdjCodesData    = l_srvaAdjCodesDataSet.getAdjustmentData(i_currentAdjustmentType,
                                                                        i_currentAdjustmentCode);
        i_adjustmentCodeData  = new BoiAdjCodesData(l_srvaAdjCodesData);
    }


    /***************************************************************************
     * Reads data required by the pane when it is in its initial state.
     * 
     * @param p_connection  The database connection.
     * 
     * @throws PpasSqlException SQL-related exception.
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        if (i_currentAdjustmentType == null)
        {
            refreshAvailableAdjTypesVec(p_connection);
        }

        refreshAvailableFastAdjCodesVec(p_connection);
        refreshAvailableAdjCodesVec(p_connection);
        refreshAvailableCurrencyCodesVec(p_connection);
    }


    /***************************************************************************
     * Updates a record in the SCAP_SERV_CLASS_ADJ_PARAM table.
     * 
     * @param p_connection  The database connection.
     * 
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        ScapServClassAdjData l_internalFastAdjCodesData = null;
        char                 l_writeInd                 = 'Y';

        l_internalFastAdjCodesData = i_fastAdjustmentCodeData.getInternalFastAdjCodesData();
        l_writeInd = (l_internalFastAdjCodesData.isWriteIndicator()  ?  'Y' : 'N');
        i_scapServClassAdjParamSqlService.update(null,
                                                 p_connection,
                                                 i_context.getOperatorUsername(),
                                                 l_internalFastAdjCodesData.getAdjType(),
                                                 l_internalFastAdjCodesData.getAdjCode(),
                                                 l_internalFastAdjCodesData.getDefaultAmount(),
                                                 l_writeInd);

        refreshAvailableFastAdjCodesVec(p_connection);
        refreshAvailableAdjCodesVec(p_connection);
        refreshAvailableCurrencyCodesVec(p_connection);
    }


    /***************************************************************************
     * Inserts a record in the SCAP_SERV_CLASS_ADJ_PARAM table.
     * 
     * @param p_connection  The database connection.
     * 
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        ScapServClassAdjData l_internalFastAdjCodesData = null;
        char                 l_writeInd                 = 0;

        l_internalFastAdjCodesData = i_fastAdjustmentCodeData.getInternalFastAdjCodesData();
        l_writeInd = (l_internalFastAdjCodesData.isWriteIndicator()  ?  'Y' : 'N');
        i_scapServClassAdjParamSqlService.insert(null,
                                                 p_connection,
                                                 i_context.getOperatorUsername(),
                                                 l_internalFastAdjCodesData.getAdjType(),
                                                 l_internalFastAdjCodesData.getAdjCode(),
                                                 l_internalFastAdjCodesData.getDefaultAmount(),
                                                 l_writeInd);

        refreshAvailableFastAdjCodesVec(p_connection);
        refreshAvailableAdjCodesVec(p_connection);
        refreshAvailableCurrencyCodesVec(p_connection);
    }



    /***************************************************************************
     * Deletes one record in the SCAP_SERV_CLASS_ADJ_PARAM table.
     * 
     * @param p_connection  The database connection.
     * 
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        ScapServClassAdjData l_internalFastAdjCodesData = null;

        l_internalFastAdjCodesData = i_fastAdjustmentCodeData.getInternalFastAdjCodesData();
        i_scapServClassAdjParamSqlService.delete(null,
                                                 p_connection,
                                                 l_internalFastAdjCodesData.getAdjType(),
                                                 l_internalFastAdjCodesData.getAdjCode());

        refreshAvailableFastAdjCodesVec(p_connection);
        refreshAvailableAdjCodesVec(p_connection);
        refreshAvailableCurrencyCodesVec(p_connection);
    }


    /***************************************************************************
     * Checks whether a record about to be inserted into the database already
     * exists, and if so, whether it was previously withdrawn.
     * 
     * @param p_connection The database connection.
     * 
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     *                        
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        boolean[]               l_flagsArray              = new boolean[2];
        ScapServClassAdjDataSet l_scapServClassAdjDataSet = null;
        ScapServClassAdjData    l_scapServClassAdjData    = null;

        l_scapServClassAdjDataSet = i_scapServClassAdjParamSqlService.readAll(null, p_connection);
        l_scapServClassAdjData    =
            l_scapServClassAdjDataSet.getScapServClassAdjData(i_currentAdjustmentCode,
                                                              i_currentAdjustmentType);
        l_flagsArray[C_DUPLICATE] = (l_scapServClassAdjData != null);
        l_flagsArray[C_WITHDRAWN] = false;

        return l_flagsArray;
    }


    //==========================================================================
    // Private method(s).                                                     ==
    //==========================================================================
    /*************************************************************************** 
     * Refreshes the internal available adjustment types vector.
     * Obtains the data from the SRVA_ADJ_CODES table.
     * 
     * @param p_connection The database connection.
     *
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshAvailableAdjTypesVec(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_srvaAdjCodesDataSet  = null;
        String[]            l_availableAdjTypesArr = null;

        i_availableAdjustmentTypesV.removeAllElements();

        l_srvaAdjCodesDataSet = i_srvaAjCodesSqlService.readDistinctAdjTypesAndCodes(null, p_connection);
        l_availableAdjTypesArr = l_srvaAdjCodesDataSet.getAvailableAdjTypesArray();
        i_availableAdjustmentTypesV = new Vector(l_availableAdjTypesArr.length);
        for (int l_ix = 0; l_ix < l_availableAdjTypesArr.length; l_ix++)
        {
            i_availableAdjustmentTypesV.add(l_availableAdjTypesArr[l_ix]);
        }
        i_currentAdjustmentType = (String)i_availableAdjustmentTypesV.firstElement();

//        // Also refresh the available adjustment codes vector.
//        refreshAvailableAdjCodesVec(l_srvaAdjCodesDataSet);
    }


    /*************************************************************************** 
     * Refreshes the internal available adjustment codes vector.
     * Obtains the data from the SRVA_ADJ_CODES table.
     * 
     * @param p_connection Database connection.
     *
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshAvailableAdjCodesVec(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_srvaAdjCodesDataSet = null;
        SrvaAdjCodesData[]  l_srvaAdjCodesDataArr = null;

        i_availableAdjustmentCodesV.removeAllElements();
        i_availableAdjustmentCodesV.addElement("");

        l_srvaAdjCodesDataSet = i_srvaAjCodesSqlService.readDistinctAdjTypesAndCodes(null, p_connection);
        l_srvaAdjCodesDataArr = l_srvaAdjCodesDataSet.getAvailableAdjCodesArray(i_currentAdjustmentType);
        if (l_srvaAdjCodesDataArr != null)
        {
            for (int l_ix = 0; l_ix < l_srvaAdjCodesDataArr.length; l_ix++)
            {
                i_availableAdjustmentCodesV.add(new BoiAdjCodesData(l_srvaAdjCodesDataArr[l_ix]));
            }
        }
    }


    /*************************************************************************** 
     * Refreshes the internal available fast adjustment codes vector.
     * Obtains the data from the SCAP_SERV_CLASS_ADJ_PARAM table.
     * 
     * @param p_connection Database connection.
     *
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshAvailableFastAdjCodesVec(JdbcConnection p_connection) throws PpasSqlException
    {
        ScapServClassAdjDataSet    l_scapServClassAdjDataSet = null;
        ScapServClassAdjData[]     l_scapServClassAdjDataArr = null;
        BoiFastAdjustmentCodesData l_fastAdjCodesData        = null;

        i_availableFastAdjustmentCodesV.removeAllElements();
        i_availableFastAdjustmentCodesV.addElement("");
        i_availableFastAdjustmentCodesV.addElement("NEW RECORD");

        l_scapServClassAdjDataSet = i_scapServClassAdjParamSqlService.readAll(null, p_connection);
        l_scapServClassAdjDataArr =
            l_scapServClassAdjDataSet.getScapServClassAdjDataArr(i_currentAdjustmentType);
        
        for (int l_ix = 0; l_ix < l_scapServClassAdjDataArr.length; l_ix++)
        {
            l_fastAdjCodesData = new BoiFastAdjustmentCodesData(l_scapServClassAdjDataArr[l_ix]);
            i_availableFastAdjustmentCodesV.add(l_fastAdjCodesData);
        }
    }


    /*************************************************************************** 
     * Refreshes the available currency codes vector.
     * Obtains the data from the CUFM_CURRENCY_FORMATS table.
     * 
     * @param p_connection Database connection.
     *
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshAvailableCurrencyCodesVec(JdbcConnection p_connection) throws PpasSqlException
    {
        String[]                   l_currencyCodesArr       = null;

        i_availableCurrencyCodesV.removeAllElements();
        i_availableCurrencyCodesV.addElement("");

        i_currencyFormatsDataSet = i_currencyFormatSqlService.readCurrencyFormats(null, p_connection);
        l_currencyCodesArr       = i_currencyFormatsDataSet.getAvailableCodes();

        // Sort the currency codes in ascending order and add them into the available vector.
        Arrays.sort(l_currencyCodesArr);
        
        for (int l_ix = 0; l_ix < l_currencyCodesArr.length; l_ix++)
        {
            i_availableCurrencyCodesV.add(l_currencyCodesArr[l_ix]);
        }
    }
}
