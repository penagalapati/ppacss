////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FastAdjustmentCodesPane.java
//      DATE            :       08-December-2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PpacLon#1755/7585
//                              PRD_ASCS00_GEN_CA_16
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to maintain Fast Adjustment Codes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/04/06 |M.Toernqvist| Restrict number of characters   | CR2112/8501
//          |            | to 12 figurs + 3 decimals       |
//----------+------------+---------------------------------+--------------------
// 20/10/06 |M.Toernqvist| Control that the number of      | CR2112/10234
//          |            | figures before decimal point is |
//          |            | le 12.                          |
//----------+------------+---------------------------------++-------------------
// 19-dec-06| huy        | Fixed it so that the check on 12 | PpacLon#2112/10686
//          |            | digit before decimal actually let|
//          |            | through 12 digits.               |
//          |            | Reset the format of default amnt |
//          |            | field to 3 digit precision after |
//          |            | a record has been updated.       |
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10586
//          |            | selecting empty row.            |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.text.DefaultFormatterFactory;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiFastAdjustmentCodesData;
import com.slb.sema.ppas.boi.dbservice.FastAdjustmentCodesDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.swing.components.FixedSizeNumberFormatter;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.GuiPane;


/*******************************************************************************
 * The purpose of this <code>FastAdjustmentCodesPane</code> class is to present
 * a GUI in order to maintain the Fast Adjustment Codes.
 */
public class FastAdjustmentCodesPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Private class constants.
    //-------------------------------------------------------------------------
    /** The column names used by the defined codes table. */
    private static final String[]    C_COLUMN_NAMES    = {"Type",
                                                          "Code",
                                                          "Default Amount",
                                                          "Default Currency",
                                                          "Write Adj. Hist."};

    /** The number of columns in the defined codes table. */
    private static final int         C_NO_OF_COLUMNS   = C_COLUMN_NAMES.length;

    /** The standard <code>MoneyFormat</code> object for SQL access. */
    private static final MoneyFormat C_MONEY_FORMATTER = new MoneyFormat("0.000");
    
    /** Max number of digits before the decimal point in the amount field. */
    private static final int         C_AMOUNT_SIZE     = 12;


    //-------------------------------------------------------------------------
    // Private variables (instance attributes).
    //-------------------------------------------------------------------------    
    /** The panel allowing data selection and modification. */
    private JPanel               i_detailsPanel                 = null;

    /** The panel containing a table of existing fast adjustment data records. */
    private JPanel               i_definedCodesPanel            = null;

    /** The available adjustment types vector. */
    private Vector               i_availableAdjTypesV           = null;

    /** The adjustment types combo box model. */
    private DefaultComboBoxModel i_adjTypesDataModel            = null;

    /** The adjustment types combo box. */
    private JComboBox            i_adjTypesComboBox             = null;

    /** The available adjustment codes vector. */
    private Vector               i_availableAdjCodesV           = null;

    /** The adjustment codes combo box model. */
    private DefaultComboBoxModel i_adjCodesDataModel            = null;

    /** The adjustment codes combo box. */
    private JComboBox            i_adjCodesComboBox             = null;

    /** Existing Fast Adjustment codes vector. */
    private Vector               i_fastAdjCodesV                = null;

    /** The available currency codes vector (defined in the CUFM_CURRENCY_FORMATS table). */
    private Vector               i_currencyCodesV               = null;

    /** The default adjustment amount field. */
    private JFormattedTextField  i_defaultAdjAmountField        = null;

    /** The default currency codes combo box model. */
    private DefaultComboBoxModel i_defaultCurrencyCodeDataModel = null;

    /** The default currency codes combo box. */
    private JComboBox            i_defaultCurrencyCodeComboBox  = null;

    /** The default currency codes combo box <code>ItemListener</code>. */
    private CurrencyCodeItemListener i_currencyCodeItemListener = null;

    /** The write adjustment "yes" indicator radio button. */
    private JRadioButton         i_doWriteAdjIndicatorRB        = null;

    /** The write adjustment "no" indicator radio button. */
    private JRadioButton         i_doNotWriteAdjIndicatorRB     = null;

    /** The existing fast adjustment records table. */
    private JTable               i_table                        = null;

    /** The vertical scroll bar used for the table's view port. */
    private JScrollBar          i_verticalScrollBar             = null;

    /** The data array to be displayed in the existing records table. */
    private String[][]          i_recordData                    = null;

    /** The data model used by the existing records table. */
    private StringTableModel    i_stringTableModel              = null;

    /** The trace printout flag. */
    private boolean             i_tracePrintout                 = false;

    //--------------------------------------------------------------------------
    // Constructor(s).
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Constructs a <code>FastAdjustmentCodesPane</code> instance using the
     * passed <code>Context</code> object.
     * The passed <code>Context</code> object is assumed to be a
     * <code>BoiContext</code> object.
     * 
     * @param p_context  The <code>BoiContext</code> object.
     */        
    public FastAdjustmentCodesPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService          = new FastAdjustmentCodesDbService((BoiContext)i_context);
        i_availableAdjTypesV = new Vector();
        i_availableAdjCodesV = new Vector();
        i_currencyCodesV     = new Vector();

        super.init();
    }


    //--------------------------------------------------------------------------
    // Public method(s).
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Handles mouse released event on the defined codes table.
     * If the mouse is released on a valid row then the fast adjusments combo box,
     * the adjustment code combo box, the default amount and currency fields and
     * the write indicator radio buttons should be updated to reflect the values
     * of the selected object.
     * 
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_fastAdjCodesV.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }


    /***************************************************************************
     * Handles action events for instance when the state of a radio button is
     * changed.
     * 
     * @param p_actionEvent Event to be handled.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        trace("actionPerformed", "Entering.");

        if (p_actionEvent.getSource() == i_doWriteAdjIndicatorRB  ||
            p_actionEvent.getSource() == i_doNotWriteAdjIndicatorRB)
        {
            // Change state to 'modified' if the present state is 'retrieved'.
            trace("actionPerformed", "state before = " + i_state);
            super.modifyState();
            trace("actionPerformed", "state after = " + i_state);
        }
        
        trace("actionPerformed", "Leaving.");
    }


    //--------------------------------------------------------------------------
    // Protected methods (overriding abstract methods in the superclass).
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Builds-up (paints) the Fast Adjustment Codes screen.
     * 
     * @see com.slb.sema.ppas.boi.gui.BusinessConfigBasePane#paintScreen()
     */
    protected void paintScreen()
    {
        trace("paintScreen", "Entering.");

        i_mainPanel    = WidgetFactory.createMainPanel("Fast Adjustments Codes", 100, 100, 0, 0);

        i_helpComboBox =
            WidgetFactory.createHelpComboBox(BoiHelpTopics.getHelpTopics(
                                                              BoiHelpTopics.C_FAST_ADJUSTMENT_CODES_SCREEN), 
                                                              i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox,      "fastAdjustmentCodesHelpComboBox,60,  1,  40,  4");
        i_mainPanel.add(i_detailsPanel,      "detailsPanel,                    1,  6, 100, 33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,               1, 40, 100, 60");

        trace("paintScreen", "Leaving.");
    }


    /***************************************************************************
     * Refreshes the screen when a new selection is made in the Adjustment Types
     * combo box.
     * 
     * @param p_itemEvent  The object on which the selection event initially occurred.
     */
    protected void refreshAfterSelection(ItemEvent p_itemEvent)
    {
        trace("refreshAfterSelection", "Entering,  p_source: " + p_itemEvent.getSource());
        String l_selectedAdjType = null;

        if (p_itemEvent.getSource() == i_adjTypesComboBox)
        {
            l_selectedAdjType = (String)i_adjTypesComboBox.getSelectedItem();

            // Read the initial data from the db service in order to refresh the adjustment codes data.
            try
            {
                ((FastAdjustmentCodesDbService)i_dbService).setCurrentAdjustmentType(l_selectedAdjType);
                i_dbService.readInitialData();

                // Refresh the defined codes combo box.
                i_fastAdjCodesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableFastAdjCodes();
                i_keyDataModel  = new DefaultComboBoxModel(i_fastAdjCodesV);
                i_keyDataComboBox.setModel(i_keyDataModel);

                // Refresh the adjustment code combo box.
                i_availableAdjCodesV =
                    ((FastAdjustmentCodesDbService)i_dbService).getAvailableAdjustmentCodes();
                i_adjCodesDataModel  = new DefaultComboBoxModel(i_availableAdjCodesV);
                i_adjCodesComboBox.setModel(i_adjCodesDataModel);
                i_adjCodesComboBox.setEnabled(true);

                // Refresh the defined codes table.
                refreshListData();
                i_table.clearSelection();
                i_verticalScrollBar.setValue(0);
            }
            catch (PpasServiceFailedException l_ppasServFailedEx)
            {
                handleException(l_ppasServFailedEx);
            }
        }

        trace("refreshAfterSelection", "Leaving.");
    }


    /***************************************************************************
     * Sets the Adjustment Type combo box selection to the item currently in use.
     */
    protected void restoreSelections()
    {
        String l_currentAdjType = null;

        l_currentAdjType = ((FastAdjustmentCodesDbService)i_dbService).getCurrentAdjustmentType();
        selectItemByObject(i_adjTypesComboBox, l_currentAdjType, true);
    }


    /***************************************************************************
     * Refreshes the current record fields with values held in the screen data
     * object.
     * The components that needs to be refreshed are the adjustment code combo
     * box, the default amount and currency fields, the write indicator radio
     * buttons and the defined codes table.
     */
    protected void refreshCurrentRecord()
    {
        trace("refreshCurrentRecord", "Entering.");

        BoiFastAdjustmentCodesData l_boiFastAdjCodesData = null;
        BoiAdjCodesData            l_boiAdjCodesData     = null;
        Money                      l_defaultAmount       = null;
        String                     l_defaultAmountValStr = null;
        String                     l_currencyCode        = null;
        boolean                    l_writeAdjIndicator   = false;

        // Update the adjustment code combo box for the currently selected fast adjustment code.
        l_boiAdjCodesData      = ((FastAdjustmentCodesDbService)i_dbService).getAdjustmentCodeData();
        selectItemByObject(i_adjCodesComboBox, l_boiAdjCodesData, true);
        i_adjCodesComboBox.setEnabled(false);

        // Get the currently selected fast adjustment code data object's internal object.
        l_boiFastAdjCodesData = ((FastAdjustmentCodesDbService)i_dbService).getFastAdjustmentCodeData();

        // Update the default amount and currency code field and combo box for the currently selected 
        // fast adjustment code.
        l_defaultAmount = l_boiFastAdjCodesData.getInternalFastAdjCodesData().getDefaultAmount();
        l_currencyCode = l_defaultAmount.getCurrency().getCurrencyCode();
        selectItemByObject(i_defaultCurrencyCodeComboBox, l_currencyCode, false);
        if (i_state == GuiPane.C_STATE_INITIAL)
        {
            // When the state is equal to C_STATE_INITIAL the amount field formatter needs to be set here,
            // otherwise it will be set by the currency code combo box item listener.
            setAmountFieldNumberFormatter(l_currencyCode);
        }
        l_defaultAmountValStr = getAmountValueAsString(l_defaultAmount);
        i_defaultAdjAmountField.setValue(Double.valueOf(l_defaultAmountValStr));

        // Update the write adj. indicator for the currently selected fast adjustment code.
        l_writeAdjIndicator = l_boiFastAdjCodesData.getInternalFastAdjCodesData().isWriteIndicator();
        selectRadioButton(i_doWriteAdjIndicatorRB,     l_writeAdjIndicator, true);
        selectRadioButton(i_doNotWriteAdjIndicatorRB, !l_writeAdjIndicator, true);

        // Selects the row in the defined codes table that corresponds to the currently selected
        // fast adjustment code.
        selectRowInTable();

        trace("refreshCurrentRecord", "Leaving.");
    }


    /***************************************************************************
     * Validates screen data before update or insert.
     * Returns <code>true</code> if the screen data is valid, <code>false</code>
     * otherwise.
     * 
     * @return  <code>true</code> if the screen data is valid, <code>false</code>
     *          otherwise.
     */
    protected boolean validateScreenData()
    {
    	final int L_MAX_LENGTH_BEFORE_DECIMAL_POINT = 12;
        trace("validateScreenData", "Entering.");
        
    	int     l_defaultAdjAmountFieldLength = i_defaultAdjAmountField.getText().trim().length();
    	int     l_decimalPointIndex           = i_defaultAdjAmountField.getText().trim().indexOf(".");
        boolean l_valid                       = true;

        
        trace("validateScreenData", "defaultAmountLen=" + l_defaultAdjAmountFieldLength);
     	trace("validateScreenData", "dpIndex=" + l_decimalPointIndex);
        if (l_defaultAdjAmountFieldLength == 0)
        {
            i_defaultAdjAmountField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Default amount value cannot be blank");
            l_valid = false;
        }
        else
        {
            // Check the decimal point's index - must be < 12
            // Check if currency does not allow decimalsthat the maxlength is <= 12
            if ((l_decimalPointIndex > L_MAX_LENGTH_BEFORE_DECIMAL_POINT) ||
                (l_decimalPointIndex == -1 && l_defaultAdjAmountFieldLength > L_MAX_LENGTH_BEFORE_DECIMAL_POINT))
            {
                i_defaultAdjAmountField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Default amount is too big, maximum number of figures before decimal point is " + 
                                     L_MAX_LENGTH_BEFORE_DECIMAL_POINT);
                l_valid = false;       		
            }
        }

        if (((String)i_defaultCurrencyCodeComboBox.getSelectedItem()).trim().length() == 0)
        {
            i_defaultCurrencyCodeComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Default amount currency code cannot be blank");
            l_valid = false;
        }

        if (!i_doWriteAdjIndicatorRB.isSelected() && !i_doNotWriteAdjIndicatorRB.isSelected())
        {
            i_doWriteAdjIndicatorRB.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "One of the Write Adj. History 'Yes' or 'No' radio button " +
                                 "has to be selected");
            l_valid = false;
        }

        trace("validateScreenData", "Leaving.");
        return l_valid;
    }


    /***************************************************************************
     * Validates key screen data and writes it to screen data object.
     * Returns <code>true</code> if the key screen data is valid, <code>false</code>
     * otherwise.
     * This method is called before checking for existing records with this key.
     * 
     * @return  <code>true</code> if the key screen data is valid, <code>false</code>
     *          otherwise.
     */
    protected boolean validateKeyData()
    {
        trace("validateKeyData", "Entering.");

        boolean         l_valid           = true;
        String          l_adjCode         = null;
        BoiAdjCodesData l_boiAdjCodesData = null;

        if (i_adjCodesComboBox.getSelectedItem() instanceof BoiAdjCodesData)
        {
            l_boiAdjCodesData = (BoiAdjCodesData)i_adjCodesComboBox.getSelectedItem();
            l_adjCode         = l_boiAdjCodesData.getInternalAdjCodesData().getAdjCode();
            ((FastAdjustmentCodesDbService)i_dbService).setCurrentAdjustmentCode(l_adjCode);
        }
        else
        {
            i_adjCodesComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Adjustment Code cannot be blank");
            l_valid = false;
        }

        trace("validateKeyData", "Leaving.");
        return l_valid;
    }


    /***************************************************************************
     * Writes current record fields to screen data object.
     * This method is called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        trace("writeCurrentRecord", "Entering.");

        ScapServClassAdjData l_newServClassAdjData = null;
        BoiAdjCodesData      l_adjCodesData        = null;
        String               l_adjType             = null;
        String               l_adjCode             = null;
        String               l_defaultAmountValStr = null;
        String               l_currencyCode        = null;
        PpasCurrency         l_ppasCurrency        = null;
        Money                l_defaultAmount       = null;
        int                  l_precision           = -1;


        // Get the adjustment type and code.
        l_adjCodesData = (BoiAdjCodesData)i_adjCodesComboBox.getSelectedItem();
        l_adjType      = l_adjCodesData.getInternalAdjCodesData().getAdjType();
        l_adjCode      = l_adjCodesData.getInternalAdjCodesData().getAdjCode();

        // Create a new 'Money' object.
        l_defaultAmountValStr = i_defaultAdjAmountField.getText().trim();
        l_currencyCode        = (String)i_defaultCurrencyCodeComboBox.getSelectedItem();
        l_precision           = ((FastAdjustmentCodesDbService)i_dbService).getPrecision(l_currencyCode);
        l_ppasCurrency        = new PpasCurrency(l_currencyCode, l_precision);

        trace("writeCurrentRecord", "l_defaultAmountValStr = " + l_defaultAmountValStr);
        trace("writeCurrentRecord", "l_precision           = " + l_precision);
        trace("writeCurrentRecord", "l_ppasCurrency        = " + l_ppasCurrency);

        try
        {
            l_defaultAmount = C_MONEY_FORMATTER.parse(l_defaultAmountValStr, l_ppasCurrency);
        }
        catch (ParseException l_parseEx)
        {
            handleException(l_parseEx);
        }

        // Create the new 'internal' fast adjustment codes data object.
        l_newServClassAdjData = new ScapServClassAdjData(l_adjType,
                                                         l_adjCode,
                                                         l_defaultAmount,
                                                         i_doWriteAdjIndicatorRB.isSelected(),
                                                         "",
                                                         null);

        // Write the new 'internal' fast adjustment codes data object to this screen's data object.
        ((FastAdjustmentCodesDbService)i_dbService).
                             setFastAdjustmentCodeData(new BoiFastAdjustmentCodesData(l_newServClassAdjData));
        
        trace("writeCurrentRecord", "Leaving.");
    }


    /***************************************************************************
     * Writes key screen data to screen data object for use in record selection
     * or deletion.
     */
    protected void writeKeyData()
    {
        trace("writeKeyData", "Entering.");
        BoiFastAdjustmentCodesData l_selectedFastAdjCodesData = null;
        String                     l_adjCode                  = null;

        if (i_keyDataComboBox.getSelectedItem() instanceof BoiFastAdjustmentCodesData)
        {
            l_selectedFastAdjCodesData = (BoiFastAdjustmentCodesData)i_keyDataComboBox.getSelectedItem();
            l_adjCode = l_selectedFastAdjCodesData.getInternalFastAdjCodesData().getAdjCode();
            ((FastAdjustmentCodesDbService)i_dbService).setCurrentAdjustmentCode(l_adjCode);
        }

        trace("writeKeyData", "Leaving.");
    }


    /***************************************************************************
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        trace("initialiseScreen", "Entering.");

        selectItemByIndex(i_adjCodesComboBox,            0, true);
        selectItemByIndex(i_defaultCurrencyCodeComboBox, 0, true);
        i_adjCodesComboBox.setEnabled(false);
        i_defaultAdjAmountField.setText("");
        i_defaultAdjAmountField.setEnabled(false);
        selectRadioButton(i_doWriteAdjIndicatorRB,    false, true);
        selectRadioButton(i_doNotWriteAdjIndicatorRB, false, true);
        i_doNotWriteAdjIndicatorRB.setEnabled(false);
        i_doWriteAdjIndicatorRB.setEnabled(false);
        i_defaultCurrencyCodeComboBox.setEnabled(false);
        
        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);

        trace("initialiseScreen", "Leaving.");
    }


    /***************************************************************************
     * Refreshes data in combo boxes, jlists, or tables.
     * Used to refresh data after inserts, deletes, and updates.
     */
    protected void refreshListData()
    {
        trace("refreshListData", "Entering.");

        i_fastAdjCodesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableFastAdjCodes();
        populateDefinedCodesTable();

        i_keyDataModel  = new DefaultComboBoxModel(i_fastAdjCodesV);
        i_keyDataComboBox.setModel(i_keyDataModel);

        /* Reset the currency precision back to three digits. */
        setAmountFieldNumberFormatter(3);

        trace("refreshListData", "Leaving.");
    }

    /***************************************************************************
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        trace("refreshDataUponEntry", "Entering.");

        try
        {
            ((FastAdjustmentCodesDbService)i_dbService).setCurrentAdjustmentType((String)null);
            i_dbService.readInitialData();

            // Refresh the adjustment type combo box.
            i_availableAdjTypesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableAdjustmentTypes();
            i_adjTypesDataModel = new DefaultComboBoxModel(i_availableAdjTypesV);
            i_adjTypesComboBox.setModel(i_adjTypesDataModel);

            // Refresh the defined codes combo box.
            i_fastAdjCodesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableFastAdjCodes();
            i_keyDataModel  = new DefaultComboBoxModel(i_fastAdjCodesV);
            i_keyDataComboBox.setModel(i_keyDataModel);

            // Refresh the adjustment code combo box.
            i_availableAdjCodesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableAdjustmentCodes();
            i_adjCodesDataModel  = new DefaultComboBoxModel(i_availableAdjCodesV);
            i_adjCodesComboBox.setModel(i_adjCodesDataModel);

            // Refresh the existing currency codes combo box.
            i_currencyCodesV = ((FastAdjustmentCodesDbService)i_dbService).getAvailableCurrencyCodes();
            i_defaultCurrencyCodeDataModel = new DefaultComboBoxModel(i_currencyCodesV);
            i_defaultCurrencyCodeComboBox.setModel(i_defaultCurrencyCodeDataModel);

            refreshListData();
        }
        catch (PpasServiceFailedException l_ppasServFailedEx)
        {
            handleException(l_ppasServFailedEx);
        }

        trace("refreshDataUponEntry", "Leaving.");
    }


    /***************************************************************************
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        BoiFastAdjustmentCodesData l_keyDataItem = null;
        l_keyDataItem = ((FastAdjustmentCodesDbService)i_dbService).getFastAdjustmentCodeData();
        i_keyDataComboBox.setSelectedItem(l_keyDataItem);
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_adjCodesComboBox.setEnabled(true);
        i_defaultAdjAmountField.setEnabled(true);
        i_doNotWriteAdjIndicatorRB.setEnabled(true);
        i_doWriteAdjIndicatorRB.setEnabled(true);
        i_defaultCurrencyCodeComboBox.setEnabled(true);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_defaultAdjAmountField.setEnabled(true);
        i_doNotWriteAdjIndicatorRB.setEnabled(true);
        i_doWriteAdjIndicatorRB.setEnabled(true);
        i_defaultCurrencyCodeComboBox.setEnabled(true);
        i_adjCodesComboBox.setEnabled(false);
    }

    //--------------------------------------------------------------------------
    // Private methods.
    //--------------------------------------------------------------------------
    /**************************************************************************
     * Creates the Details Panel.
     * <br>The Details panel should contain: 
     * <br>Adjustment Type combo box containing <code>BoiMiscCodeData</code> objects.
     * <br>Adjustment Codes combo box (this is the KeyDataComboBox for the screen),
     *     and should display the Adjustment Codes defined for the selected 
     *     Adjustment Type and the NEW RECORD and blank items.
     * <br>Default Adjustment Amount field.
     *     The precision should be 3 decimal places (max of 12 digits before the
     *     decimal point). The field should be >=0.
     * <br>Default Currency Code combo box holding the defined currencies from
     *     the EMCR table.
     * <br>Write Adjustment Indicator combo box or radio buttons.
     * <br>Update, Delete and Reset buttons.
     */    
    private void createDetailsPanel()
    {
        trace("createDetailsPanel", "Entering.");

        JLabel      l_adjTypesLabel            = null;
        JLabel      l_adjCodesLabel            = null;
        JLabel      l_definedCodesLabel        = null;
        JLabel      l_defaultAdjAmountLabel    = null;
        JLabel      l_defaultCurrencyCodeLabel = null;
        JLabel      l_writeAdjIndicatorLabel   = null;
        ButtonGroup l_writeAdjIndicatorBG      = null;

        // Create the details panel.
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);

        // Create the adjustment types combo box.
        l_adjTypesLabel     = WidgetFactory.createLabel("Adjustment Type:");
        i_adjTypesDataModel = new DefaultComboBoxModel();
        i_adjTypesComboBox  = WidgetFactory.createComboBox(i_adjTypesDataModel);
        i_adjTypesComboBox.addItemListener(i_dataFilterComboListener);

        // Create the defined codes label.
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");

        // Create the adjustment codes combo box.
        l_adjCodesLabel     = WidgetFactory.createLabel("Adjustment Code:");
        i_adjCodesDataModel = new DefaultComboBoxModel();
        i_adjCodesComboBox  = WidgetFactory.createComboBox(i_adjTypesDataModel);

        // Create the default amount and currency fields.
        l_defaultAdjAmountLabel = WidgetFactory.createLabel("Default Amount:");
        i_defaultAdjAmountField = WidgetFactory.createAmountField(C_AMOUNT_SIZE, 3, false); 
        addValueChangedListener(i_defaultAdjAmountField);

        l_defaultCurrencyCodeLabel     = WidgetFactory.createLabel("Currency:");
        i_defaultCurrencyCodeDataModel = new DefaultComboBoxModel(i_currencyCodesV);
        i_defaultCurrencyCodeComboBox  = WidgetFactory.createComboBox(i_defaultCurrencyCodeDataModel);
        addValueChangedListener(i_defaultCurrencyCodeComboBox);
        i_currencyCodeItemListener = new CurrencyCodeItemListener();
        i_defaultCurrencyCodeComboBox.addItemListener(i_currencyCodeItemListener);

        // Create the write adjustment indicator radio buttons and add them to a button group.
        l_writeAdjIndicatorLabel   = WidgetFactory.createLabel("Write Adj. History:");
        i_doWriteAdjIndicatorRB    = WidgetFactory.createRadioButton("Yes", this);
        i_doWriteAdjIndicatorRB.addActionListener(this);
        i_doNotWriteAdjIndicatorRB = WidgetFactory.createRadioButton("No", this);
        i_doNotWriteAdjIndicatorRB.addActionListener(this);
        l_writeAdjIndicatorBG    = new ButtonGroup();
        l_writeAdjIndicatorBG.add(i_doWriteAdjIndicatorRB);
        l_writeAdjIndicatorBG.add(i_doNotWriteAdjIndicatorRB);


        // Add the components to the details panel:       name                           x,  y,  w, h
        i_detailsPanel.add(l_adjTypesLabel,               "adjTypesLabel,                1,  1, 15, 4");
        i_detailsPanel.add(i_adjTypesComboBox,            "adjTypesComboBox,            17,  1, 20, 4");

        i_detailsPanel.add(l_definedCodesLabel,           "definedCodesLabel,            1,  6, 15, 4");
        i_detailsPanel.add(i_keyDataComboBox,             "keyDataComboBox,             17,  6, 35, 4");

        i_detailsPanel.add(l_adjCodesLabel,               "adjCodesLabel,                1, 11, 15, 4");
        i_detailsPanel.add(i_adjCodesComboBox,            "adjCodesComboBox,            17, 11, 20, 4");

        i_detailsPanel.add(l_defaultAdjAmountLabel,       "defaultAdjAmountLabel,        1, 16, 15, 4");
        i_detailsPanel.add(i_defaultAdjAmountField,       "defaultAdjAmountField,       17, 16, 20, 4");

        i_detailsPanel.add(l_defaultCurrencyCodeLabel,    "defaultCurrencyCodeLabel,    36, 16,  8, 4");
        i_detailsPanel.add(i_defaultCurrencyCodeComboBox, "defaultCurrencyCodeComboBox, 44, 16,  8, 4");

        i_detailsPanel.add(l_writeAdjIndicatorLabel,      "writeAdjIndicatorLabel,       1, 21, 15, 4");
        i_detailsPanel.add(i_doWriteAdjIndicatorRB,       "doWriteAdjIndicatorRB,       17, 21,  7, 4");
        i_detailsPanel.add(i_doNotWriteAdjIndicatorRB,    "doNotWriteAdjIndicatorRB,    24, 21,  7, 4");

        i_detailsPanel.add(i_updateButton,                "updateButton,                 1, 27, 15, 5");
        i_detailsPanel.add(i_deleteButton,                "deleteButton,                17, 27, 15, 5");
        i_detailsPanel.add(i_resetButton,                 "resetButton,                 33, 27, 15, 5");


        trace("createDetailsPanel", "Leaving.");
    }


    /***************************************************************************
     * Creates the Defined Codes Panel.
     */    
    private void createDefinedCodesPanel()
    {
        trace("createDefinedCodesPanel", "Entering.");

        JScrollPane l_scrollPane = null;
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);
        
        i_stringTableModel = new StringTableModel(i_recordData, C_COLUMN_NAMES);
        i_table = WidgetFactory.createTable(90, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();

        trace("createDefinedCodesPanel", "Entering.");
    }


    /***************************************************************************
     * Populates the defined codes table.
     */    
    private void populateDefinedCodesTable()
    {
        int                        l_numRecords       = 0;
        int                        l_arraySize        = 20;
        BoiFastAdjustmentCodesData l_fastAdjCodesData = null;
        Money                      l_defaultAmount    = null;
        boolean                    l_writeAdjRecord   = false;

        l_numRecords = i_fastAdjCodesV.size() - 2;
        l_arraySize = ((l_numRecords > 22) ? l_numRecords : 23);
        i_recordData = new String[l_arraySize][C_NO_OF_COLUMNS];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_fastAdjCodesV.size(); i++, j++)
        {
            l_fastAdjCodesData = (BoiFastAdjustmentCodesData)i_fastAdjCodesV.elementAt(i);
            l_defaultAmount    = l_fastAdjCodesData.getInternalFastAdjCodesData().getDefaultAmount();
            l_writeAdjRecord   = l_fastAdjCodesData.getInternalFastAdjCodesData().isWriteIndicator();
            i_recordData[j][0] = l_fastAdjCodesData.getInternalFastAdjCodesData().getAdjType();
            i_recordData[j][1] = l_fastAdjCodesData.getInternalFastAdjCodesData().getAdjCode();
            i_recordData[j][2] = getAmountValueAsString(l_defaultAmount);
            i_recordData[j][3] = l_defaultAmount.getCurrency().getCurrencyCode();
            i_recordData[j][4] = (l_writeAdjRecord  ?  "Yes" : "No");
        }

        i_stringTableModel.setData(i_recordData);
    }    


    /***************************************************************************
     * Selects the row in the defined codes table that corresponds to the
     * selected item in the fast adjustment codes combo box.
     * The table's view port will also be adjusted in order to make the selected
     *  row visible.
     */
    protected void selectRowInTable()
    {
        int l_selectionIx = -1;
        l_selectionIx = i_keyDataComboBox.getSelectedIndex() - 2;
        if (l_selectionIx >= 0  &&  l_selectionIx != i_table.getSelectedRow())
        {
            i_table.setRowSelectionInterval(l_selectionIx, l_selectionIx);
            i_verticalScrollBar.setValue(l_selectionIx * i_table.getRowHeight());
        }
    }


    /***************************************************************************
     * Selects the passed object in the given combo box.
     * 
     * @param p_comboBox             the combo box.
     * @param p_obj                  the object to be selected.
     * @param p_removeItemListeners  if <code>true</code> all <code>ItemListener</code>s
     *                               added to the given combo box will be removed
     *                               before the selection is made and added again
     *                               afterwards.
     */
    private void selectItemByObject(JComboBox p_comboBox, Object p_obj, boolean p_removeItemListeners)
    {
        ItemListener[] l_itemListenerArr = null;

        if (p_removeItemListeners)
        {
            l_itemListenerArr = p_comboBox.getItemListeners();
            for (int l_ix = 0; l_ix < l_itemListenerArr.length; l_ix++)
            {
                p_comboBox.removeItemListener(l_itemListenerArr[l_ix]);
            }
        }

        p_comboBox.setSelectedItem(p_obj);

        if (l_itemListenerArr != null)
        {
            for (int l_ix = 0; l_ix < l_itemListenerArr.length; l_ix++)
            {
                p_comboBox.addItemListener(l_itemListenerArr[l_ix]);
            }
        }
    }


    /***************************************************************************
     * Selects the item at the given index in the given combo box.
     * 
     * @param p_comboBox             the combo box.
     * @param p_index                the index for the item to be selected.
     * @param p_removeItemListeners  if <code>true</code> all <code>ItemListener</code>s
     *                               added to the given combo box will be removed
     *                               before the selection is made and added again
     *                               afterwards.
     */
    private void selectItemByIndex(JComboBox p_comboBox, int p_index, boolean p_removeItemListeners)
    {
        ItemListener[] l_itemListenerArr = null;

        if (p_index < p_comboBox.getItemCount())
        {
            if (p_removeItemListeners)
            {
                l_itemListenerArr = p_comboBox.getItemListeners();
                for (int l_ix = 0; l_ix < l_itemListenerArr.length; l_ix++)
                {
                    p_comboBox.removeItemListener(l_itemListenerArr[l_ix]);
                }
            }
    
            p_comboBox.setSelectedIndex(p_index);
    
            if (l_itemListenerArr != null)
            {
                for (int l_ix = 0; l_ix < l_itemListenerArr.length; l_ix++)
                {
                    p_comboBox.addItemListener(l_itemListenerArr[l_ix]);
                }
            }
        }
    }


    /***************************************************************************
     * Sets the select status for the passed radio button.
     * 
     * @param p_radioButton            the radio button.
     * @param p_select                 <code>true</code> if the radio button should be selected,
     *                                 <code>false</code> otherwise.
     * @param p_removeActionListeners  if <code>true</code> all <code>ActionListener</code>s
     *                                 added to the given combo box will be removed
     *                                 before the selection is made and added again
     *                                 afterwards.
     */
    private void selectRadioButton(JRadioButton  p_radioButton,
                                   boolean       p_select,
                                   boolean       p_removeActionListeners)
    {
        ActionListener[] l_actionListenerArr = null;

        if (p_removeActionListeners)
        {
            l_actionListenerArr = p_radioButton.getActionListeners();
            for (int l_ix = 0; l_ix < l_actionListenerArr.length; l_ix++)
            {
                p_radioButton.removeActionListener(l_actionListenerArr[l_ix]);
            }
        }

        p_radioButton.setSelected(p_select);

        if (l_actionListenerArr != null)
        {
            for (int l_ix = 0; l_ix < l_actionListenerArr.length; l_ix++)
            {
                p_radioButton.addActionListener(l_actionListenerArr[l_ix]);
            }
        }
    }


    /***************************************************************************
     * Returns the amount value from the passed <code>Money</code> object as a
     * <code>String</code>.
     * 
     * @param p_money  the <code>Money</code> object.
     * 
     * @return the amount value from the passed <code>Money</code> object.
     */
    String getAmountValueAsString(Money p_money)
    {
        return C_MONEY_FORMATTER.format(p_money);
    }


    /***************************************************************************
     * Creates and sets a new number formatter on the amount value field for
     * the passed currency code.
     * 
     * @param p_currencyCode  the currency code.
     */
    private void setAmountFieldNumberFormatter(String p_currencyCode)
    {
        int                      l_precision        = 0;
        StringBuffer             l_formatBuffer     = null;
        DecimalFormat            l_decimalFormat    = null;
        FixedSizeNumberFormatter l_numberFormatter  = null;
        DefaultFormatterFactory  l_formatterFactory = null;

        trace("setAmountFieldNumberFormatter(String)", "Entering. Currency code = " + p_currencyCode);

        if (p_currencyCode.length() > 0)
        {
            l_precision = ((FastAdjustmentCodesDbService)i_dbService).getPrecision(p_currencyCode);
            if (l_precision >= 0)
            {
                l_formatBuffer = new StringBuffer("0");
                for (int l_ix = 0; l_ix < l_precision; l_ix++)
                {
                    if (l_ix == 0)
                    {
                        l_formatBuffer.append(".");
                    }
                    l_formatBuffer.append("0");
                }
                trace("setAmountFieldNumberFormatter", "p_formatBuffer = '" + l_formatBuffer + "'");
                l_decimalFormat = new DecimalFormat(l_formatBuffer.toString());
                l_numberFormatter = new FixedSizeNumberFormatter(l_decimalFormat,
                                                                 C_AMOUNT_SIZE,
                                                                 l_precision,
                                                                 false);
                l_numberFormatter.setAllowsInvalid(false);
                l_numberFormatter.setCommitsOnValidEdit(true);

                trace("setAmountFieldNumberFormatter", "Set formatter factory.");
                l_formatterFactory = new DefaultFormatterFactory(l_numberFormatter,
                                                                 l_numberFormatter,
                                                                 l_numberFormatter,
                                                                 l_numberFormatter);
                i_defaultAdjAmountField.setFormatterFactory(l_formatterFactory);
            }
        }

        trace("setAmountFieldNumberFormatter(String)", "Leaving.");
    }


    /***************************************************************************
     * Creates and sets a new number formatter on the amount value field for
     * the currentcy precision.
     *
     * @param p_currencyPrecision the number of digits for the currency's precision.
     */
    private void setAmountFieldNumberFormatter(int p_currencyPrecision)
    {
        StringBuffer             l_formatBuffer     = null;
        DecimalFormat            l_decimalFormat    = null;
        FixedSizeNumberFormatter l_numberFormatter  = null;
        DefaultFormatterFactory  l_formatterFactory = null;

        if (p_currencyPrecision >= 0)
        {
            l_formatBuffer = new StringBuffer("0");
            for (int l_ix = 0; l_ix < p_currencyPrecision; l_ix ++)
            {
                if (l_ix == 0)
                {
                    l_formatBuffer.append(".");
                }
                l_formatBuffer.append("0");
            }

            trace("setAmountFieldNumberFormatter(int)", "p_formatBuffer = '" + l_formatBuffer + "'");
            l_decimalFormat = new DecimalFormat(l_formatBuffer.toString());
            l_numberFormatter = new FixedSizeNumberFormatter(l_decimalFormat,
                                                             C_AMOUNT_SIZE,
                                                             p_currencyPrecision,
                                                             false);
            l_numberFormatter.setAllowsInvalid(false);
            l_numberFormatter.setCommitsOnValidEdit(true);

            trace("setAmountFieldNumberFormatter", "Set formatter factory.");
            l_formatterFactory = new DefaultFormatterFactory(l_numberFormatter,
                                                             l_numberFormatter,
                                                             l_numberFormatter,
                                                             l_numberFormatter);
            i_defaultAdjAmountField.setFormatterFactory(l_formatterFactory);
        }

        trace("setAmountFieldNumberFormatter(int)", "Leaving.");
    }


    /***************************************************************************
     * Prints a trace message.
     * 
     * @param p_method  the calling method.
     * @param p_msg     the message to be printed.
     */
    private void trace(String p_method, String p_msg)
    {
        if (i_tracePrintout)
        {
            System.out.println("FastAdjustmentCodesPane." + p_method + " -- " + p_msg);
        }
    }


    //--------------------------------------------------------------------------
    // Inner class(es).
    //--------------------------------------------------------------------------
    /***************************************************************************
     * The purpose of this <code>CurrencyCodeItemListener</code> inner class is
     * to be used as an <code>ItemListener</code> to handle the item state
     * change on the currency code combo box.
     */
    private class CurrencyCodeItemListener implements ItemListener
    {
        /***********************************************************************
         * Handles the item state change events when an item in the currency code
         * combo box is selected or deselected.
         * 
         * @param p_itemEvent  the <code>ItemEvent</code> object to be processed.
         */
        public void itemStateChanged(ItemEvent p_itemEvent)
        {
            trace("itemStateChanged", "Entering.");

            if (p_itemEvent.getStateChange() == ItemEvent.SELECTED  &&  i_state != GuiPane.C_STATE_INITIAL)
            {
                trace("itemStateChanged", "Create new formatter.");
                setAmountFieldNumberFormatter((String)i_defaultCurrencyCodeComboBox.getSelectedItem());
            }

            trace("itemStateChanged", "Leaving.");
        }
    }
}
