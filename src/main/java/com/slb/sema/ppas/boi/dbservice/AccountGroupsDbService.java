////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       AccountGroupsDbService.java
//    DATE            :       18-Aug-2004
//    AUTHOR          :       Mario Imfeld
//    REFERENCE       :       PpacLon#470/3661
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Account Groups screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.AcgrAccountGroupSqlService;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.boi.boidata.BoiAccountGroupsData;
import com.slb.sema.ppas.boi.gui.BoiContext;

/**
 * Database access class for Account Groups screen. 
 */
public class AccountGroupsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for account group code details. */
    private AcgrAccountGroupSqlService i_acgrSqlService = null;
    
    /** Vector of account group records. */
    private Vector i_availableAccountGroupDataV = null;
    
    /** Account group data. */
    private BoiAccountGroupsData i_acgrData = null;

    /** Currently selected account group code. Used as key for read and delete. */
    private int i_currentCode = -1;       
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public AccountGroupsDbService(BoiContext p_context)
    {
        super(p_context);
        i_acgrSqlService = new AcgrAccountGroupSqlService(null, null, null);
        i_availableAccountGroupDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current account group code. Used for record lookup in database. 
     * @param p_code Currently selected account group code. 
     */
    public void setCurrentCode(int p_code)   
    {
        i_currentCode = p_code;
    }

    /** 
     * Return account group data currently being worked on. 
     * @return Account group data object.
     */
    public BoiAccountGroupsData getAccountGroupData()
    {
        return i_acgrData;
    }

    /** 
     * Set account group data currently being worked on. 
     * @param p_accountGroupsData Account group data object.
     */
    public void setAccountGroupData(BoiAccountGroupsData p_accountGroupsData)
    {
        i_acgrData = p_accountGroupsData;
    }

    /** 
     * Return account group data. 
     * @return Vector of available account group data records.
     */
    public Vector getAvailableAccountGroupData()
    {
        return i_availableAccountGroupDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        AcgrAccountGroupDataSet l_acgrDataSet = null;
        
        l_acgrDataSet = i_acgrSqlService.readAll(null, p_connection);
        try
        {
            i_acgrData = new BoiAccountGroupsData(l_acgrDataSet.getRecord(new AccountGroupId(i_currentCode)));
        }
        catch (PpasServiceFailedException e)
        {
            e.printStackTrace();
        }             
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshAccountGroupDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_acgrSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                (int)i_acgrData.getInternalAccountGroupData().getAccountID().getValue(),
                                i_acgrData.getInternalAccountGroupData().getAccountDescription());        
        
        refreshAccountGroupDataVector(p_connection);
    }

    /** 
     * Updates record in the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_acgrSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                (int)i_acgrData.getInternalAccountGroupData().getAccountID().getValue(),                                
                                i_acgrData.getInternalAccountGroupData().getAccountDescription());

        refreshAccountGroupDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_acgrSqlService.delete(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                (int)i_acgrData.getInternalAccountGroupData().getAccountID().getValue());
              
        refreshAccountGroupDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        AcgrAccountGroupDataSet l_acgrDataSet        = null;
        AcgrAccountGroupData    l_dbAccountGroupData = null;
        boolean[]               l_flagsArray         = new boolean[2];
        
        l_acgrDataSet        = i_acgrSqlService.readAll(null, p_connection);
        try
        {
            l_dbAccountGroupData = l_acgrDataSet.getRecord(new AccountGroupId(i_currentCode));
            
            if (l_dbAccountGroupData != null)
            {
                l_flagsArray[C_DUPLICATE] = true;
                if (l_dbAccountGroupData.isDeleted())
                {
                    l_flagsArray[C_WITHDRAWN] = true;
                }
                else
                {
                    l_flagsArray[C_WITHDRAWN] = false;
                }
            }
            else
            {
                l_flagsArray[C_DUPLICATE] = false;
            }      
        }
        catch (PpasServiceFailedException e)
        {
            e.printStackTrace();
        }       

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        try
        {
            AccountGroupId accountGroupId = new AccountGroupId(i_currentCode);

            i_acgrSqlService.markAsAvailable(null,
                                             p_connection,
                                             accountGroupId,
                                             i_context.getOperatorUsername());

            refreshAccountGroupDataVector(p_connection);
        }
        catch (PpasServiceFailedException e)
        {
            e.printStackTrace();
        }
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available account group data vector from the 
     * acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshAccountGroupDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        AcgrAccountGroupDataSet l_acgrDataSet = null;
        AcgrAccountGroupData[]  l_acgrArray   = null;
        
        i_availableAccountGroupDataV.removeAllElements();
        
        l_acgrDataSet = i_acgrSqlService.readAll(null, p_connection);
        l_acgrArray   = l_acgrDataSet.getAvailableArray();
        
        i_availableAccountGroupDataV.addElement(new String(""));
        i_availableAccountGroupDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_acgrArray.length; i++)
        {
            i_availableAccountGroupDataV.addElement(new BoiAccountGroupsData(l_acgrArray[i]));
        }
   }
}