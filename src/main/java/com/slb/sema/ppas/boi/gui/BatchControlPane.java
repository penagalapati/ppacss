////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchControlPane.java
//      DATE            :       14-June-2004
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/3125
//                              PRD_ASCS00_ANA_FD_13
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       BOI screen to monitor and submit batch jobs.
//                              
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//  14/7/5  | M.Brister  | Added repeat and interval fields| PpacLon#1398/6788
//          |            | to enable repeating jobs to be  | 
//          |            | scheduled.                      |
//          |            | Changed "Submission date & time"|
//          |            | to "Run date & time", and "Run  |
//          |            | date" to "Disconnection date".  |
//----------+------------+---------------------------------+--------------------
// 28/10/05 |L. Lundberg | The mouse clicked event handling| PpacLon#1759/7306
//          |            | is moved to the mouse release   |
//          |            | event, i.e. the 'mouseClicked'  |
//          |            | method is renamed to            |
//          |            | 'mouseReleased'.                |
//----------+------------+---------------------------------+--------------------
// 19/05/06 |L. Lundberg | The Fraud Returns batch is      | PpacLon#2291/8841
//          |            | added and several methods have  |
//          |            | modified.                       |
//----------+------------+---------------------------------+--------------------
// 22/06/06 |L. Lundberg | Method: mouseReleased(...)      | PpacLon#2345/9174
//          |            | Don't clear the "Directory"     |
//          |            | ("File") field if the current   |
//          |            | batch job is the Fraud Returns  |
//          |            | batch job.                      |
//----------+------------+---------------------------------+--------------------
// 17/07/06 | M Erskine  | Addition of Batch Refill.       | PpacLon#2479/9401
//          |            |                                 | PRD_ASCS00_GEN_CA_93
//----------+------------+---------------------------------+--------------------
// 02/08/06 | Ian James  | Addition of Automated MSISDN    | PpacLon#2541/9518
//          |            | Routing Deletion.               | PRD_ASCS00_GEN_CA_98
//----------+------------+---------------------------------+--------------------
// 17/07/07 | Ian James  | Subscriber Segmentation Change. | PpacLon#3183/11848
//          |            |                                 | PRD_ASCS00_GEN_CA_129
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.boi.boidata.BoiBatchJobType;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.dbservice.BatchControlDbService;
import com.slb.sema.ppas.boi.dbservice.BoiDbService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.util.support.Debug;

/**
 * BOI Screen to monitor and submit batch jobs.
 */
public class BatchControlPane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------
    
    /** Display name of the subscriber installation batch job. */
    private static final String C_BC_TYPE_SUBSINSTALL = "Batch Subscriber Installation";
    
    /** Display name of the subscriber status change batch job. */
    private static final String C_BC_TYPE_SUBSSTATCHANGE = "Batch Subscriber Status Change";
    
    /** Display name of the SDP balancing batch job. */
    private static final String C_BC_TYPE_SDPBALANCING = "Batch SDP Balancing";
    
    /** Display name of the subscriber disconnection batch job. */
    private static final String C_BC_TYPE_DISCONNECT = "Batch Disconnection";
    
    /** Display name of the number plan change batch job. */
    private static final String C_BC_TYPE_NUMBERPLANCHANGE = "Batch Number Plan Change";
    
    /** Display name of the number plan cutover batch job. */
    private static final String C_BC_TYPE_NUMBERPLANCUTOVER = "Batch Number Plan Cutover";
    
    /** Display name of the promotion plan change batch job. */
    private static final String C_BC_TYPE_PROMOTIONPLANCHANGE = "Batch Promotion Plan Change";
    
    /** Display name of the refill batch job. */
    private static final String C_BC_TYPE_REFILL = "Batch Refill";

    /** Display name of the subscriber segmentation change batch job. */
    private static final String C_BC_TYPE_SUBSCRIBERSEGMENTATIONCHANGE = "Batch Subscriber Segmentation Change";

    /** Display name of the Automated MSISDN Routing Deletion batch job. */
    private static final String C_BC_TYPE_AUTO_MSISDN_ROUTING_DELETION = "Automated MSISDN Routing Deletion";
    
    /** Display name of the promotion allocation provisioning batch job. */
    private static final String C_BC_TYPE_PROMOALLOCPROVISIONING = "Batch Promotion Allocation Provisioning";
    
    /** Display name of the bulk load of account finder batch job. */
    private static final String C_BC_TYPE_BULKLOADOFACCFINDER = "Bulk Load of Account Finder";
    
    /** Display name of the service class change batch job. */
    private static final String C_BC_TYPE_SERVICECLASSCHANGE = "Batch Service Class Change";
    
    /** Display name of the synchronisation of promotion recharge data batch job. */
    private static final String C_BC_TYPE_SYNCOFPROMRECHARGEDATA 
                                   = "Batch Synchronisation of Promotion/Refill Data";
    
    /** Display name of the miscellaneous data upload batch job. */
    private static final String C_BC_TYPE_MISCDATAUPLOAD = "Batch Miscellaneous Data Upload";
    
    /** Display name of the community charging batch job. */
    private static final String C_BC_TYPE_COMMUNITYCHARGING = "Batch Community Charging";
    
    /** Display name of the promotion allocation synchronisation batch job. */
    private static final String C_BC_TYPE_PROMO_ALLOC_SYNCH = "Batch Promotion Allocation Synchronisation";
    
    /** Display name of the MSISDN Routing Provisioning batch job. */
    private static final String C_BC_TYPE_MSISDN_ROUTING_PROV = "MSISDN Routing Provisioning";
    
    /** Display name of the MSISDN Routing Deletion batch job. */
    private static final String C_BC_TYPE_MSISDN_ROUTING_DELETION = "MSISDN Routing Deletion";
    
    /** Display name of the Fraud Returns batch job. */
    private static final String C_BC_TYPE_FRAUD_RETURNS = "Fraud Returns";
    
    /** Constant for date length. */ 
    private static final int C_DATE_LENGTH_DD_MMM_YYYY = 11;
    
    /** Use for calls to middleware. */
    private static final String C_CLASS_NAME = "BatchControlPane";
    
    /** Initial number of rows that will displayed in the table. */
    private static final int C_INITIAL_DISPLAY_ROWS = 12;
    
    /** Initial number of columns that will displayed in the table. */    
    private static final int C_INITIAL_DISPLAY_COLS = 10;

    /** The 'Directory' file field label text. */
    private static final String C_LABEL_TEXT_DIRECTORY = "Directory: ";
    
    /** The 'File' file field label text. */
    private static final String C_LABEL_TEXT_FILE      = "File: ";
    
    //-------------------------------------------------------------------------
    // Class variables.
    //-------------------------------------------------------------------------
    
    /** The initialised data to be displayed in the table. */
    private static String[][] c_dataInitial = null;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** ComboBox to hold the batch job type. */
    private JComboBox i_jobTypeComboBox = null;
    
    /** List of configured job types loaded from a properties file. */
    private Vector i_configuredJobTypes = null;
    
    /** List of available input files on the server. */
    private Vector i_files = null;
    
    /** The model for this screen's table. */
    private StringTableModel i_stringTableModel = null;
    
    /** Table object to be displayed on the screen. */
    private JTable i_table = null;
    
    /** Data to be displayed in the table. */
    private String[][] i_data = null;

    /** Panel to display information about the configured batch jobs. */ 
    private JPanel i_jobsPanel = null;
    
    /** Panel containing fields related to the selected job in the jobs panel. */
    private JPanel i_fieldsPanel = null;
    
    /** Refresh button. */
    private JButton i_refreshButton = null;
    
    /** Submit button. */
    private JButton i_submitButton = null;
    
    /** Clear button. */
    private JButton i_clearButton = null;
    
    /** Text field to hold the run date and time. */
    private JFormattedTextField i_runDateTimeField = null;
    
    /** Text field to hold the disconnection date used in batch disconnection. */
    private JFormattedTextField i_discDateField = null;
    
    /** Text field to hold the selected file for submission. */
    private ValidatedJTextField i_fileField = null;
    
    /** Text field to display the job type in the submission panel. */
    private ValidatedJTextField i_jobTypeReadOnlyField = null;
    
    /** Radio button to specify that all jobs should be displayed whatever the status. */
    private JRadioButton i_allJobs = null;
    
    /** Radio button to specify that only jobs in progress should be displayed. */
    private JRadioButton i_jobsInProgress = null;
    
    /** ComboBox to hold the available markets. */
    private JComboBox i_marketBox = null;
    
    /** ComboBox to hold the available service classes. */
    private JComboBox i_serviceClassBox = null;
    
    /** Interface to the database for this screen. */
    private BatchControlDbService i_dbService = null;
    
    /** Host name of server. */
    private String i_serverHost = null;
    
    /** Port number to connect to. */
    private String i_loginPort = null;
    
    /** Model to hold the files available for submission for the selected job type. */
    private DefaultComboBoxModel i_filesDefaultComboBoxModel = null;
    
    /** Combo box to display the files model. */
    private JComboBox i_filesComboBox = null;
    
    /** Flag indicating whether a job of the selected type has already been submitted. */
    private boolean i_jobSubmitted = false;
    
    /** Flag indicating whether the selected job is to be resubmitted or completely new. */
    private boolean i_resubmissionMode = false;
    
    /** Used to display a description of the disconnection date field. */
    private JLabel i_discDateLabel = null;
    
    /** Used to display text next to the market combo-box. */
    private JLabel i_marketLabel = null;
    
    /** Used to display text next to the service class combo-box. */
    private JLabel i_serviceClassLabel = null;
    
    /** Used to display text next to the frequency combo-box. */
    private JLabel i_frequencyLabel = null;

    /** ComboBox to hold the job submission frequencies. */
    private JComboBox i_frequencyBox = null;
    
    /** Used to display text next to the interval field. */
    private JLabel i_intervalLabel = null;

    /** Text field to hold the job submission interval. */
    private JFormattedTextField i_intervalField = null;
    
    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;

    private JLabel    i_fileFieldLabel = null;

    // Class initialisation.
    static
    {
        c_dataInitial = new String[C_INITIAL_DISPLAY_ROWS][C_INITIAL_DISPLAY_COLS];
        
        for (int i = 0; i < C_INITIAL_DISPLAY_ROWS; i++)
        {
            for (int j = 0; j < C_INITIAL_DISPLAY_COLS; j++)
            {    
                c_dataInitial[i][j] = "";
            }
        }
    }
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor for the Batch Control pane.
     * @param p_context Reference to a context object containing BOI specific config.
     */
    public BatchControlPane(Context p_context)
    {
        super(p_context, (Container)null);
        i_serverHost = (String)i_context.getMandatoryObject(
            "applet.server.host");
                
        i_loginPort = (String)i_context.getMandatoryObject(
            "applet.server.login.port");
        init();
    }
    
    /**
     *  Initialises the screen.
     */
    private void init()
    {
        JPanel          l_mainPanel = null;
        BoiHelpListener l_helpComboListener;

        i_dbService = new BatchControlDbService((BoiContext)i_context);
         
        l_mainPanel = WidgetFactory.createMainPanel("Batch Control", 100, 100, 0, 0);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                           BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_BATCH_CONTROL_SCREEN), 
                                           l_helpComboListener);
        i_helpComboBox.setFocusable(false);

        i_jobsPanel = createJobsPanel();
        i_fieldsPanel = createSubmitJobPanel();
       
        l_mainPanel.add(i_helpComboBox, "batchControlHelpComboBox,60,1,40,4");
        l_mainPanel.add(i_jobsPanel, "jobsPanel,1,6,100,54");
        l_mainPanel.add(i_fieldsPanel, "fieldsPanel,1,61,100,40");
        
        i_contentPane = l_mainPanel;

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        
        l_mainPanel.setFocusCycleRoot(true);        
    }
    
    /**
     * Create a panel to hold the job types and job progress table.
     * @return A JPanel containing the Job Types and job history.
     */
    private JPanel createJobsPanel()
    {
        JScrollPane     l_tableScrollPane = null;
        JPanel          l_jobsPanel = null;
        JLabel          l_jobTypeLabel = null;
        JLabel          l_filesLabel = null;
        ButtonGroup     l_buttonGroup = null;
        Object          l_availableBatchScreens = null;
        String          l_displayName = null;
        String[]        l_st = null;
        BoiBatchJobType l_jobType = null;
        int             l_nextToken = 0;
        int[]           l_columnWidths = new int[] {170,103,86,103,86,103,103,170,115};
                
        l_jobsPanel = WidgetFactory.createPanel("Jobs", 100, 54, 0, 0);
        
        // Get the configurion for available batch screens
        l_availableBatchScreens = ((BoiContext)i_context).getObject("ascs.boi.availablescreens");
        
        i_configuredJobTypes = new Vector();
        
        if (l_availableBatchScreens != null)
        {
            l_st         = ((String)l_availableBatchScreens).split(",");
            
            while (l_nextToken < l_st.length)
            {
                l_displayName = l_st[l_nextToken++].trim();
                
                if (l_displayName.equals(C_BC_TYPE_AUTO_MSISDN_ROUTING_DELETION))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_AUTO_MSISDN_ROUTING_DELETION,
                                        BatchConstants.C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION,
                                        "batchMaster", true);
                }
                else if (l_displayName.equals(C_BC_TYPE_COMMUNITYCHARGING))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_COMMUNITYCHARGING,
                                        BatchConstants.C_JOB_TYPE_BATCH_CCI,
                                        "batchCCIChange", true, "CHANGE_SDP");
                }
                else if (l_displayName.equals(C_BC_TYPE_DISCONNECT))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_DISCONNECT,
                                        BatchConstants.C_JOB_TYPE_BATCH_DISCONNECTION,
                                        "batchMaster", true);
                }
                else if (l_displayName.equals(C_BC_TYPE_MISCDATAUPLOAD))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_MISCDATAUPLOAD,
                                        BatchConstants.C_JOB_TYPE_BATCH_MISC,
                                        "batchMiscDataUpload", true, "UPDATE_MISC_");
                }
                else if (l_displayName.equals(C_BC_TYPE_NUMBERPLANCHANGE))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_NUMBERPLANCHANGE,
                                        BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE,
                                        "batchNPChange", true, "NUMBER_CHANGE_");
                }
                else if (l_displayName.equals(C_BC_TYPE_NUMBERPLANCUTOVER))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_NUMBERPLANCUTOVER,
                                        BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER,
                                        "batchNPCutover", true, "NUMBER_CUTOVER_");
                }
                else if (l_displayName.equals(C_BC_TYPE_PROMOALLOCPROVISIONING))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_PROMOALLOCPROVISIONING,
                                        BatchConstants.C_JOB_TYPE_PROMO_ALLOC_PROVISIONING,
                                        "batchMaster", true);
                }
                else if (l_displayName.equals(C_BC_TYPE_PROMO_ALLOC_SYNCH))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_PROMO_ALLOC_SYNCH,
                                        BatchConstants.C_JOB_TYPE_BATCH_PROMO_ALLOC_SYNCH,
                                        "batchMaster", true);
                }
                else if (l_displayName.equals(C_BC_TYPE_PROMOTIONPLANCHANGE))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_PROMOTIONPLANCHANGE,
                                        BatchConstants.C_JOB_TYPE_BATCH_PROMOTION_PLAN_CHANGE,
                                        "batchPromotionPlanChange", true, "PALLOAD_");
                }
                else if (l_displayName.equals(C_BC_TYPE_REFILL))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_REFILL,
                                        BatchConstants.C_JOB_TYPE_BATCH_REFILL,
                                        "batchRefill", true, "BATCH_REFILL_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SDPBALANCING))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SDPBALANCING,
                                        BatchConstants.C_JOB_TYPE_BATCH_SDP_BALANCING,
                                        "batchSDPBalancing", true, "CHANGE_SDP_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SERVICECLASSCHANGE))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SERVICECLASSCHANGE,
                                        BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE,
                                        "batchServiceClassChange", true, "BATCH_CHG_SERV_CLASS_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SUBSCRIBERSEGMENTATIONCHANGE))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SUBSCRIBERSEGMENTATIONCHANGE,
                                        BatchConstants.C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION,
                                        "batchSubSegmentationChange", true, "BATCH_CHG_SUB_SEG_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SUBSINSTALL))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SUBSINSTALL,
                                        BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION,
                                        "batchInstall", true, "INSTALL_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SUBSSTATCHANGE))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SUBSSTATCHANGE,
                                        BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                        "batchStatusChange", true, "INSTALL_STATUS_");
                }
                else if (l_displayName.equals(C_BC_TYPE_SYNCOFPROMRECHARGEDATA))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_SYNCOFPROMRECHARGEDATA,
                                        BatchConstants.C_JOB_TYPE_BATCH_SYNCH,
                                        "batchSynchronisation", true, "AIRDATASYNC_");
                }
                else if (l_displayName.equals(C_BC_TYPE_BULKLOADOFACCFINDER))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_BULKLOADOFACCFINDER,
                                        BatchConstants.C_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER,
                                        "batchMaster", false);
                }
                else if (l_displayName.equals(C_BC_TYPE_MSISDN_ROUTING_DELETION))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_MSISDN_ROUTING_DELETION,
                                        BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_DELETION,
                                        "batchMsisdnRoutingDeletion", true, "MSISDN_DELETE_");
                }
                else if (l_displayName.equals(C_BC_TYPE_MSISDN_ROUTING_PROV))
                {
                    l_jobType = new BoiBatchJobType(
                                        C_BC_TYPE_MSISDN_ROUTING_PROV,
                                        BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING,
                                        "batchMsisdnRoutingProvisioning", true, "MSISDN_LOAD_");
                }
                else if (l_displayName.equals(C_BC_TYPE_FRAUD_RETURNS))
                {
                    l_jobType = new BoiBatchJobType(C_BC_TYPE_FRAUD_RETURNS,
                                                    BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS,
                                                    BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS,
                                                    true,
                                                    "");
                }
                else
                {
                    displayMessageDialog(
                        i_contentPane,
                        l_displayName + " from the properties file is not a recognised batch job type");
                    l_jobType = new BoiBatchJobType();
                }
              
                i_configuredJobTypes.addElement(l_jobType);
            }
            i_configuredJobTypes.trimToSize();
        }
        
        i_configuredJobTypes.add(0, new BoiBatchJobType());
        l_jobTypeLabel = WidgetFactory.createLabel("Job Type: ");
        i_jobTypeComboBox = WidgetFactory.createComboBox(i_configuredJobTypes, this);
        
        l_filesLabel = WidgetFactory.createLabel("Files: ");
        i_files = new Vector();
        i_filesDefaultComboBoxModel = new DefaultComboBoxModel(i_files);
        i_filesComboBox = WidgetFactory.createComboBox(i_filesDefaultComboBoxModel);
        i_filesComboBox.addItemListener(this);
        
        i_refreshButton = WidgetFactory.createButton("Refresh", this, false);
        
        i_jobsInProgress = WidgetFactory.createRadioButton("In progress", this);
        i_allJobs = WidgetFactory.createRadioButton("All", this);
        
        l_jobsPanel.add(l_jobTypeLabel, "jobTypeLabel,1,1,20,4");
        l_jobsPanel.add(i_jobTypeComboBox, "jobTypeComboBox,21,1,50,4");
        l_jobsPanel.add(l_filesLabel, "filesLabel,1,6,20,4");
        l_jobsPanel.add(i_filesComboBox, "filesComboBox,21,6,50,4");
        
        l_buttonGroup = new ButtonGroup();
        l_buttonGroup.add(i_jobsInProgress);
        l_buttonGroup.add(i_allJobs);
        i_jobsInProgress.setSelected(true);
        
        // The following is for the setup of the Jobs History table
        String[] l_columnNames = { "Execution Date & Time",
                                   "File Date",
                                   "Seq No.",
                                   "Sub Seq No",
                                   "Status",
                                   "No. Success",
                                   "No. Reject",
                                   "Market",
                                   "Serv Class",
                                   "Run Date"};

        i_data = c_dataInitial;
        i_stringTableModel = new StringTableModel(i_data, l_columnNames);
        i_table = 
            WidgetFactory.createTable(l_columnWidths, 150, 100, i_stringTableModel, this);
        
        l_tableScrollPane = new JScrollPane(
                                    i_table,
                                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
              
        l_jobsPanel.add(l_tableScrollPane, "tableScrollPane,1,12,100,36");
        l_jobsPanel.add(i_refreshButton, "refreshButton,1,49,15,5");
        l_jobsPanel.add(i_jobsInProgress, "jobsInProgress,21,49,20,5");
        l_jobsPanel.add(i_allJobs, "allJobs,41,49,20,5");
        
        return l_jobsPanel;
    }
    
    /**
     * Creates the submission panel for jobs.
     * @return The job submission panel.
     */
    private JPanel createSubmitJobPanel()
    {
        JPanel               l_fieldsPanel = null;
        JLabel               l_jobTypeLabel = null;
        JLabel               l_runDateTimeLabel = null;
        Vector               l_serviceClasses = null;
        Vector               l_marketVector = null;
        PpasDateTime         l_now = null;
        DefaultComboBoxModel l_serviceClassModel = null;

        l_fieldsPanel = WidgetFactory.createPanel("Submit Job",100, 40, 0, 0);
        
        l_jobTypeLabel = WidgetFactory.createLabel("Job Type: ");
        i_jobTypeReadOnlyField = WidgetFactory.createTextField(50);
        i_jobTypeReadOnlyField.setEnabled(false);
        
        i_fileFieldLabel = WidgetFactory.createLabel(C_LABEL_TEXT_FILE);
        i_fileField = WidgetFactory.createTextField(40);
        
        l_runDateTimeLabel = WidgetFactory.createLabel("Run Date & Time: ");
        i_runDateTimeField = WidgetFactory.createDateTimeField(this);
        l_now = DatePatch.getDateTimeNow();
        
        i_runDateTimeField.setValue(l_now);
        
        i_frequencyLabel = WidgetFactory.createLabel("Repeat: ");
        i_frequencyBox = WidgetFactory.createComboBox(i_dbService.getRepeatFreqs(), this);
        
        i_intervalLabel = WidgetFactory.createLabel("Interval: ");
        i_intervalField = WidgetFactory.createIntegerField(2, false);

        i_discDateLabel = WidgetFactory.createLabel("Disconnection Date: ");
        i_discDateField = WidgetFactory.createDateField(this);
        
        i_marketLabel = WidgetFactory.createLabel("Market: ");
        l_marketVector = new Vector();
        i_marketBox = WidgetFactory.createComboBox(l_marketVector, this);

        i_serviceClassLabel = WidgetFactory.createLabel("Service Class");
        l_serviceClasses = new Vector();
        
        l_serviceClassModel = new DefaultComboBoxModel(l_serviceClasses);
        i_serviceClassBox = WidgetFactory.createComboBox(l_serviceClassModel);
        i_serviceClassBox.setSelectedItem("");
        i_marketBox.setSelectedItem("");
            
        i_submitButton = WidgetFactory.createButton("Submit", this, true);
        i_clearButton = WidgetFactory.createButton("Clear", this, false);
        i_clearButton.setVerifyInputWhenFocusTarget(false);
        
        l_fieldsPanel.add(l_jobTypeLabel, "jobTypeLabel,1,1,20,4");
        l_fieldsPanel.add(i_jobTypeReadOnlyField, "jobTypeReadOnlyField,21,1,50,4");
//        l_fieldsPanel.add(l_fileLabel, "fileLabel,1,6,20,4");
        l_fieldsPanel.add(i_fileFieldLabel, "fileLabel,1,6,20,4");
        l_fieldsPanel.add(i_fileField, "fileField,21,6,60,4");
        l_fieldsPanel.add(l_runDateTimeLabel, "runDateTimeLabel,1,11,20,4");
        l_fieldsPanel.add(i_runDateTimeField, "runDateTimeField,21,11,30,4");
        l_fieldsPanel.add(i_frequencyLabel, "frequencyLabel,53,11,8,4");
        l_fieldsPanel.add(i_frequencyBox, "frequencyBox,61,11,14,4");
        l_fieldsPanel.add(i_intervalLabel, "intervalLabel,77,11,10,4");
        l_fieldsPanel.add(i_intervalField, "intervalField,87,11,6,4");
        l_fieldsPanel.add(i_discDateLabel, "discDateLabel,1,16,20,4");
        l_fieldsPanel.add(i_discDateField, "discDateField,21,16,30,4");
        l_fieldsPanel.add(i_marketLabel, "marketLabel,1,21,20,4");
        l_fieldsPanel.add(i_marketBox, "marketBox,21,21,35,4");
        l_fieldsPanel.add(i_serviceClassLabel, "serviceClassLabel,57,21,15,4");
        l_fieldsPanel.add(i_serviceClassBox, "serviceClassBox,73,21,22,4");
        l_fieldsPanel.add(i_submitButton, "submitButton,1,34,15,5");
        l_fieldsPanel.add(i_clearButton, "clearButton,17,34,15,5");
        
        return l_fieldsPanel;
    }

    /**
     * Sets the default focus on entry to the screen.
     */
    public void defaultFocus()
    {
        i_jobTypeComboBox.requestFocusInWindow(); 
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        try
        {
            i_dbService.readInitialData();
            i_jobTypeComboBox.setSelectedIndex(0);
            repopulateTableOnPane();
            initialiseJobSubmissionPanel();
            setStateOfFields();
            i_state = C_STATE_INITIAL;
            i_resubmissionMode = false;
        }
        catch (PpasServiceFailedException e)
        {
            handleException(e);
        }
    }
    
    /**
     * 
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        
    }

    /**
     * Handles action events.
     * @param p_event The ActionEvent that has occured on this pane.
     */
    public void actionPerformed(ActionEvent p_event)
    {        
        if (p_event.getSource() == i_submitButton)
        {
            // Request focus on submit button such that focus lost processing
            // is performed on previous focus owner.  
            i_submitButton.requestFocusInWindow();
        
            // Perform submit processing after the current focus lost event.
            // i.e. After field validation has occurred for previous focus owner.
            SwingUtilities.invokeLater(new SubmitRunnable());
        }
        else if (p_event.getSource() == i_clearButton)
        {
            i_filesComboBox.setSelectedItem("<Please select input file>");
            initialiseJobSubmissionPanel();
            setScreenState();
        }
        else if (p_event.getSource() == i_allJobs)
        {
            repopulateTableOnPane();
        }
        else if (p_event.getSource() == i_jobsInProgress)
        {
            repopulateTableOnPane();
        }
        else if (p_event.getSource() == i_refreshButton)
        {
            repopulateTableOnPane();
            getBatchDatFiles();
            setScreenState();
        }
        
    }


    /**
     * Method to handle the itemEvents generated by changing the value
     * in a combobox.
     * 
     * @param p_itemEvent The <code>ItemEvent</code> that has occurred.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        DefaultComboBoxModel l_serviceClassModel = null;
        Vector               l_availableServiceClasses = null;
        
        if (p_itemEvent.getSource() == i_jobTypeComboBox)
        {
            repopulateSubmitFields();
            repopulateTableOnPane();
            getBatchDatFiles();
            setStateOfFields();
            i_jobSubmitted = false;
            setScreenState();
        }
        else if (p_itemEvent.getSource() == i_filesComboBox)
        {
            if (i_filesComboBox.getSelectedItem().toString().equals("<Please select input file>"))
            {
                i_fileField.setText("");
            }
            else
            {
                i_fileField.setText(i_filesComboBox.getSelectedItem().toString());
            }
            setStateOfFields();
            setScreenState();
        }
        else if (p_itemEvent.getSource() == i_marketBox)
        {   
            l_availableServiceClasses = new Vector();
            if (i_marketBox.isVisible() && i_marketBox.getSelectedItem() != null &&
                    !i_marketBox.getSelectedItem().equals("") && !i_marketBox.getSelectedItem().equals("N/A"))
            {
                l_availableServiceClasses = 
                    i_dbService.getAvailableServiceClasses(
                        ((BoiMarket)i_marketBox.getSelectedItem()).getMarket());
            }

            l_serviceClassModel = new DefaultComboBoxModel(l_availableServiceClasses);
            i_serviceClassBox.setModel(l_serviceClassModel);
            i_serviceClassBox.setSelectedItem("");
        }
        else if (p_itemEvent.getSource() == i_frequencyBox)
        {
            if (i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE))
            {
                i_intervalLabel.setVisible(false);
                i_intervalField.setVisible(false);
                i_intervalField.setText("");
            }
            else
            {
                i_intervalLabel.setVisible(true);
                i_intervalField.setVisible(true);
            }
        }
    }
    
    /**
     * Initialises the job submission panel.
     */
    private void initialiseJobSubmissionPanel()
    {
        repopulateSubmitFields();

        i_frequencyBox.setSelectedIndex(0);
        i_intervalField.setText("");
        i_intervalLabel.setVisible(false);
        i_intervalField.setVisible(false);
    }
    
    /**
     * Repopulates fields in the job submission panel.
     */
    private void repopulateSubmitFields()
    {
        if (i_fileField.isEnabled())
        {
            if (i_fileFieldLabel.getText().equals(C_LABEL_TEXT_FILE))
            {
                i_fileField.setText("");
            }
            
            i_frequencyLabel.setVisible(false);
            i_frequencyBox.setVisible(false);
            i_intervalLabel.setVisible(false);
            i_intervalField.setVisible(false);
            i_frequencyBox.setSelectedIndex(0);
            i_intervalField.setText("");
        }
        else
        {
            i_frequencyLabel.setVisible(true);
            i_frequencyBox.setVisible(true);
        }
        
        if (i_runDateTimeField.isEnabled())
        {
            i_runDateTimeField.setValue(DatePatch.getDateTimeNow());
        }
        
        i_resubmissionMode = false;
    }

    /**
     * Repopulate the table displayed on the screen with the specified data from the baco_batch_control 
     * table. The data displayed depends on the selected job type and job status.
     */    
    private void repopulateTableOnPane()
    {
        String l_batchJobSymbolicMapName = null;
        
         // Get the Job Type from the drop down list and use it to get the symbolic batch name
        l_batchJobSymbolicMapName = getSelectedBatchJobType().getBatchJobType();
        
        try
        {
            i_dbService.readData();
            
            if (l_batchJobSymbolicMapName != null)
            {
                i_data = i_dbService.getTableData(
                                         l_batchJobSymbolicMapName,
                                         i_allJobs.isSelected());
            }
            else
            {
                i_data = c_dataInitial;
            }
            
            i_stringTableModel.setData(i_data);
            i_table.setModel(i_stringTableModel);
            i_jobTypeReadOnlyField.setText(i_jobTypeComboBox.getSelectedItem().toString());
                 
        }
        catch (PpasServiceFailedException e)
        {
            handleException(e);
        }
    }
    
    /**
     * Gets a reference to the selected batch job type object from the Job Type combo box.
     * @return A reference to the <code>BoiBatchJobType</code> object representing the selected batch job.
     */
    private BoiBatchJobType getSelectedBatchJobType()
    {
        Object l_jobTypeComboBoxSelection = null; 
        BoiBatchJobType l_selectedBatchJobType = null;
        
        l_jobTypeComboBoxSelection = i_jobTypeComboBox.getSelectedItem();
        
        if ((l_jobTypeComboBoxSelection instanceof BoiBatchJobType) )
        {
            l_selectedBatchJobType = (BoiBatchJobType)l_jobTypeComboBoxSelection;
        }
        else
        {
            System.out.println("Error - Batch job selected is not of type BoiBatchJobType");
        }
        
        return l_selectedBatchJobType;
    }
    
    
    /**
     * Sets the state of the fields depending on the selected Job Type. For file based
     * batch jobs, the format of the file is also considered.
     */
    private void setStateOfFields()
    {
        BoiBatchJobType l_selectedBatchJobType = null;
        Vector l_marketVector = null;
        Vector l_serviceClasses = null;
        DefaultComboBoxModel l_marketModel = null;
        DefaultComboBoxModel l_serviceClassModel = null;
        
        l_selectedBatchJobType = getSelectedBatchJobType();
        
        if (l_selectedBatchJobType.getDisplayName().equals(C_BC_TYPE_SUBSINSTALL) &&
            (i_filesComboBox.getSelectedItem().toString().matches(
                 BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_DAT)))
        {
            l_marketVector = i_dbService.getAvailableMarkets();
            l_marketModel = new DefaultComboBoxModel(l_marketVector);
            i_marketBox.setModel(l_marketModel);
            
            l_serviceClasses = new Vector();
            l_serviceClasses.add("");
            l_serviceClassModel = new DefaultComboBoxModel(l_serviceClasses);
            i_serviceClassBox.setModel(l_serviceClassModel);
            i_serviceClassBox.setSelectedItem("");
            
            i_marketLabel.setVisible(true);
            i_marketBox.setSelectedItem("");
            i_marketBox.setVisible(true);
            
            i_serviceClassLabel.setVisible(true);
            i_serviceClassBox.setSelectedItem("");
            i_serviceClassBox.setVisible(true);
        }
        else
        {
            i_marketLabel.setVisible(false);
            i_marketBox.setVisible(false);

            i_serviceClassLabel.setVisible(false);
            i_serviceClassBox.setVisible(false);
        }
        
        if (l_selectedBatchJobType.isDbJobType())
        {
            i_fileField.setText("N/A");
            i_fileField.setEnabled(false);
            i_filesComboBox.setEnabled(false);
        }
        else
        {
            // Set the file field label text to 'Directory' if the Fraud Returns batch is choosen.
            if (l_selectedBatchJobType.getDisplayName().equals(C_BC_TYPE_FRAUD_RETURNS))
            {
                i_fileFieldLabel.setText(C_LABEL_TEXT_DIRECTORY);
                i_filesComboBox.setEnabled(false);
                
                // Get the configured (given as a property) batch indata directory,
                // to be used as a proposal in the 'file' field.
                i_fileField.setText(getBatchIndataDir("batch_vfr"));
            }
            else
            {
                i_fileFieldLabel.setText(C_LABEL_TEXT_FILE);
                i_filesComboBox.setEnabled(true);
            }
            i_fileField.setEnabled(true);
        }
        
        if (l_selectedBatchJobType.toString().equals(C_BC_TYPE_DISCONNECT))
        {
            i_discDateLabel.setVisible(true);
            i_discDateField.setVisible(true);
            i_discDateField.setValue(DatePatch.getDateToday());
        }
        else
        {
            i_discDateLabel.setVisible(false);
            i_discDateField.setVisible(false);
        }
    }
    
    /**
     * Validate the screen information to be passed to the Job Scheduler on
     * job submission. 
     * 
     * @return true if the information is valid and false otherwise
     */
    private boolean validateSubmitJobFields()
    {
        String l_jobType = i_jobTypeReadOnlyField.getText();
        
        if (l_jobType.trim().equals(""))
        {
            i_jobTypeComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "A job type must be selected before job submission");
            return false;
        }
        
        // Validate that run date and time are not blank
        if (i_runDateTimeField.getText().trim().equals(""))
        {
            i_runDateTimeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "You must specify a run date & time");
            return false;
        }
        
        if (!(l_jobType.equals(C_BC_TYPE_DISCONNECT) ||
              l_jobType.equals(C_BC_TYPE_PROMOALLOCPROVISIONING) ||
              l_jobType.equals(C_BC_TYPE_BULKLOADOFACCFINDER) ||
              l_jobType.equals(C_BC_TYPE_AUTO_MSISDN_ROUTING_DELETION) ||
              l_jobType.equals(C_BC_TYPE_PROMO_ALLOC_SYNCH)) && 
            i_fileField.getText().toString().trim().equals(""))
        {
            i_fileField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "You must specify an input file");
            return false;
        }
        
        if (l_jobType.equals(C_BC_TYPE_SUBSINSTALL) && i_marketBox.isVisible())
        {
            String l_selectedMarket = i_marketBox.getSelectedItem().toString().trim();
            String l_selectedServiceClass = i_serviceClassBox.getSelectedItem().toString().trim();
            if (l_selectedMarket.equals("N/A") || l_selectedMarket.equals(""))
            {
                i_marketBox.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "Market cannot be blank");
                return false;
            }
            
            if (l_selectedServiceClass.equals("N/A") || l_selectedServiceClass.equals(""))
            {
                i_serviceClassBox.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "Service Class cannot be blank");
                return false;
            }
        }
        
        if (l_jobType.equals(C_BC_TYPE_DISCONNECT))
        {
            if (i_discDateField.getText().trim().equals(""))
            {
                i_discDateField.requestFocusInWindow();
                displayMessageDialog(i_contentPane,
                                     "You must specify a run date");
                return false;
            }
        }
        
        if ( !i_frequencyBox.getSelectedItem().equals(BoiDbService.C_JOB_FREQ_DISPLAY_ONCE) &&
            ( i_intervalField.getText().equals("") || Integer.parseInt(i_intervalField.getText()) == 0 ))
        {
            i_intervalField.requestFocusInWindow();
            displayMessageDialog(i_contentPane,
                                 "Interval must be set for a repeating job");
            return false;
        }
        
        return true;
    }
    
    /**
     * Gets data from the relevant fields on the screen depending on the selected job type and puts them
     * into a HashMap with the keys required by the batch subsystem. 
     * @return A hashMap containing the necessary parameters for job submission
     */
    private HashMap createJobParametersHashMap()
    {
        HashMap         l_jobParamsHashMap = null;
        String          l_fileName = null;
        Market          l_market = null;
        String          l_serviceClass = null;
        BoiBatchJobType l_selectedJobType = null;
        PpasDate        l_discDate = null;
        PpasDateTime    l_runDateTime = null;
        Integer         l_discOffset = null;
        
        l_jobParamsHashMap = new HashMap();
        l_selectedJobType = getSelectedBatchJobType();
        
        if (l_selectedJobType.isDbJobType())
        {
            if (i_resubmissionMode)
            {
                l_jobParamsHashMap.put(BatchConstants.C_KEY_RECOVERY_FLAG, "yes");
            }
            else
            {
                l_jobParamsHashMap.put(BatchConstants.C_KEY_RECOVERY_FLAG, "no");
            }
            l_discDate = new PpasDate(i_discDateField.getText());
            l_runDateTime = new PpasDateTime(i_runDateTimeField.getText());
            l_discOffset = new Integer(l_discDate.getDaysDiff(l_runDateTime.getPpasDate()));
            
            l_jobParamsHashMap.put(
                BatchConstants.C_KEY_BATCH_JOB_TYPE,
                l_selectedJobType.getBatchJobType());
            l_jobParamsHashMap.put(BatchConstants.C_KEY_DISCONNECTION_OFFSET, l_discOffset);
        }
        else 
        {
            // It's a file type job, so we need to include the file name (renamed to a .SCH to signify it's
            // been scheduled for submission.
            // If this is the Fraud Returns batch, set the directory parameter instead.
            if (l_selectedJobType.getJsSubmissionName().equals(BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS))
            {
                // Set the input directory parameter for the Fraud Returns batch.
                l_jobParamsHashMap.put(BatchConstants.C_KEY_INPUT_DIRECTORY_NAME,
                                       i_fileField.getText().trim());
            }
            else
            {
                if (!l_selectedJobType.getDisplayName().equals(C_BC_TYPE_SYNCOFPROMRECHARGEDATA))
                {
                    l_fileName = i_fileField.getText().trim().replaceFirst("[.\\.]DAT", ".SCH");
                }
                else
                {
                    l_fileName = i_fileField.getText().trim().replaceFirst("[.\\.]XML", ".SCH");
                }
                l_jobParamsHashMap.put(BatchConstants.C_KEY_INPUT_FILENAME, l_fileName);
            }
            
            if (i_marketBox.isVisible() && i_serviceClassBox.isVisible())
            {
                l_market = ((BoiMarket)(i_marketBox.getSelectedItem())).getMarket();
                l_serviceClass =
                    ((BoiMiscCodeData)
                            (i_serviceClassBox.getSelectedItem())).getInternalMiscCodeData().getCode();
                l_jobParamsHashMap.put(BatchConstants.C_KEY_SERVICE_AREA, l_market.getSrva());
                l_jobParamsHashMap.put(BatchConstants.C_KEY_SERVICE_LOCATION, l_market.getSloc());
                l_jobParamsHashMap.put(BatchConstants.C_KEY_SERVICE_CLASS, l_serviceClass);
            }
        }
        
        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10010,
                        "Job Parameters HashMap to be submitted: " + l_jobParamsHashMap);
        }
        
        return l_jobParamsHashMap;
    }
    
    /** Used for calls to middleware. */
    private static final String C_METHOD_getBatchDatFiles = "getBatchDatFiles";
    /** 
     * Gets the Batch files available for batch job submission from the server.
     */
    private void getBatchDatFiles()
    {
        URLConnection       l_urlConnection;
        URL                 l_fileListingUrl = null;
        InputStream         l_is;
        BufferedReader      l_bir;
        String[]            l_tokenizer = null;
        int                 l_nextToken = 0;
        String              l_line           = "";
        String              l_param;
        String              l_status         = "";
        String              l_fileListingUrlString;
        
        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                11010, 
                "Entered  " + C_METHOD_getBatchDatFiles + ", file listing URL is: " + "http://" + 
                i_serverHost + ":" + i_loginPort + "/ascs/boi/FileServices?command=DIRLIST&jobType=" +
                getSelectedBatchJobType().getBatchJobType() +
                "&userName=<username>&password=<password>");
        }
        
        l_fileListingUrlString = "http://" + i_serverHost + ":" + i_loginPort +
                    "/ascs/boi/FileServices?command=DIRLIST&jobType=" +
                    getSelectedBatchJobType().getBatchJobType() +
                    "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
                    "&password=" + ((BoiContext)i_context).getOperatorPassword();
                    
        try
        {
            l_fileListingUrl = new URL(l_fileListingUrlString);
            l_urlConnection = l_fileListingUrl.openConnection();
            l_urlConnection.connect();
            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));             
            l_line = l_bir.readLine();

            if (l_line == null)
            {
                l_line = "";
            }
            
            if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12010, 
                    "Read line " + l_line);
            }
            
            i_files.removeAllElements();
            i_files.add("<Please select input file>");
            
            l_tokenizer = l_line.split("[=& ]");
            
            while (l_nextToken < l_tokenizer.length)
            {
                l_param = l_tokenizer[l_nextToken++];
                if ("status".equals(l_param))
                {
                    l_status = l_tokenizer[l_nextToken++];
                }
                else if ("file".equals(l_param.substring(0,4)))
                {
                    i_files.add(l_tokenizer[l_nextToken++]);
                }
            }
            
            i_filesDefaultComboBoxModel = new DefaultComboBoxModel(i_files);
            i_filesComboBox.setModel(i_filesDefaultComboBoxModel);                  
            
            if (!"SUCCESS".equals(l_status))
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12080, 
                        "Batch File Lookup failed");
                }
              
                displayMessageDialog(i_contentPane, 
                                     "A failure occurred when attempting to retrieve a list of available " +
                                     "batch files from the server. Please contact your system administrator.",
                                     false,
                                     false);
              
              
          
               if (Debug.on)
               {
                   Debug.print(
                       C_CLASS_NAME,
                       12180, 
                       "Batch File Lookup successful");
               }
            }
            else if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12250, 
                    "Batch File Lookup successful");
            }
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
        catch (IOException l_e)
        {
            handleException(l_e);
        }
    }

    /** Used in calls to middleware. */
    private static final String C_METHOD_renameBatchDatFile = "renameBatchDatFile";
    
    /**
     * Sends a request to rename the batch file submitted to the Job Scheduler.
     */
    private void renameBatchDatFile()
    {
        URLConnection       l_urlConnection       = null;
        URL                 l_fileRenameUrl       = null;
        InputStream         l_is                  = null;
        BufferedReader      l_bir                 = null;
        String[]            l_tokenizer           = null;
        int                 l_nextToken           = 0;
        String              l_line                = "";
        String              l_status              = "";
        String              l_fileRenameUrlString = "";
        String              l_param               = "";
        
        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                12300, 
                "Entered " + C_METHOD_renameBatchDatFile +
                "Connecting to " + "http://" + i_serverHost + ":" + i_loginPort +
                "/ascs/boi/FileServices?command=RENAME&userName=OperatorUsername&password=OperatorPassword" + 
                "&fileName=" + i_fileField.getText());
        }
        
        l_fileRenameUrlString = "http://" + i_serverHost + ":" + i_loginPort +
                "/ascs/boi/FileServices?command=RENAME&userName=" +
                ((BoiContext)i_context).getOperatorUsername() +
                "&password=" + ((BoiContext)i_context).getOperatorPassword() + 
                "&fileName=" + i_fileField.getText();
            
        try
        {
            l_fileRenameUrl = new URL(l_fileRenameUrlString);

            l_urlConnection = l_fileRenameUrl.openConnection();
            l_urlConnection.connect();
            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));             
            l_line = l_bir.readLine();

            if (l_line == null)
            {
                l_line = "";
            }
            
            if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12350, 
                    "Read line " + l_line);
            }
            
            l_tokenizer = l_line.split("[=& ]");
            
            while (l_nextToken < l_tokenizer.length)
            {
                l_param = l_tokenizer[l_nextToken++];
                if ("status".equals(l_param))
                {
                    l_status = l_tokenizer[l_nextToken++];
                }
            }
            
            if (!"SUCCESS".equals(l_status))
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12400, 
                        "Batch File Rename failed");
                }
                // TODO: Should maybe throw an Exception here. We definitely shouldn't continue submitting
                //       the job.
                displayMessageDialog(i_contentPane, 
                                     "A failure occurred when attempting to rename the submitted batch " +
                                     "file on the server. Please contact your system administrator.",
                                     false,
                                     false);                
            }
            else if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12070, 
                    "Batch File Rename successful");
            }
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
        catch (IOException l_e1)
        {
            handleException(l_e1);
        }
    }
    
    /**
     * Retrieves the name of a batch file from the server based on a partial file name. The partial file
     * name will have been created from information we have on the screen and will be unique. However, the 
     * file name extension is not known, so we get it from the server.
     * @param p_partialFileName The fragment of the file name to match against.
     * @return The full name of the file that matches the fragment passed in.
     */
    private String getBatchFileFromPartialFileName(String p_partialFileName)
    {
        URLConnection       l_urlConnection        = null;
        URL                 l_fileListingUrl       = null;
        InputStream         l_is                   = null;
        BufferedReader      l_bir                  = null;
        String[]            l_tokenizer            = null;
        int                 l_nextToken            = 0;
        String              l_line                 = "";
        String              l_param                = null;
        String              l_status               = "";
        String              l_fileListingUrlString = null;
        String              l_fileForResubmission  = null;
        
        //TODO: Needs rewriting, but check that this method is actually required first. May not be required 
        //      as it has now been confirmed by the batch team that they should all be .IPG if the job has 
        //      failed.
        
        l_fileListingUrlString = "http://" + i_serverHost + ":" + i_loginPort +
            "/ascs/boi/FileServices?command=FILELOOKUP&fileName=" +
            p_partialFileName +
            "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
            "&password=" + ((BoiContext)i_context).getOperatorPassword();
                    
        try
        {
            l_fileListingUrl = new URL(l_fileListingUrlString);
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
        
        try
        {
            l_urlConnection = l_fileListingUrl.openConnection();
            l_urlConnection.connect();
            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));             
            l_line = l_bir.readLine();
        }
        catch (IOException l_e1)
        {
            handleException(l_e1);
        }

        if (l_line != null)
        {
            if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12010, 
                    "Read line " + l_line);
            }
        }
        else
        {
            l_line = "";
        }
            
        l_tokenizer = l_line.split("[=& ]");
            
        // TODO: What if we retrieve more than one file?
        while (l_nextToken < l_tokenizer.length)
        {
            l_param = l_tokenizer[l_nextToken++];
            if ("status".equals(l_param))
            {
                l_status = l_tokenizer[l_nextToken++];
            }
            else if ("file".equals(l_param.substring(0,4)))
            {
                l_fileForResubmission = l_tokenizer[l_nextToken++];
            }
        }
            
        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                12070, 
                "Batch File Lookup: " + l_status);
        }

        return l_fileForResubmission;
    }


    /**
     * Retrieves the batch indata directory as configured by a property included in the given additional
     * properties layer.
     * 
     * @param p_additionalPropLayer The additional properties layer.
     * 
     * @return the batch indata directory.
     */
    private String getBatchIndataDir(String p_additionalPropLayer)
    {
        String         l_batchIndataDir = null;
        URLConnection  l_urlConn        = null;
        URL            l_url            = null;
        String         l_urlString      = null;
        InputStream    l_inputStream    = null;
        BufferedReader l_reader         = null;
        String         l_line           = null;
        String[]       l_lineParts      = null;

        // Create the URL connection string.
        l_urlString = "http://" + i_serverHost + ":" + i_loginPort +
                      "/ascs/boi/FileServices?command=BATCHINDATADIR" +
                      "&propLayer=" + p_additionalPropLayer +
                      "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
                      "&password=" + ((BoiContext)i_context).getOperatorPassword();

        // Create the URL.
        try
        {
            l_url = new URL(l_urlString);
        }
        catch (MalformedURLException l_malformedURLEx)
        {
            handleException(l_malformedURLEx);
            return "";
        }

        // Open the connection and read the response.
        try
        {
            l_urlConn = l_url.openConnection();
            l_urlConn.connect();
            l_inputStream = l_urlConn.getInputStream();
            l_reader = new BufferedReader(new InputStreamReader(l_inputStream));
            l_line = l_reader.readLine();

            // Process the response.
            l_lineParts = l_line.split("[=& ]");
            for (int l_ix = 0; l_ix < l_lineParts.length; l_ix++)
            {
                if (l_lineParts[l_ix].equals("indataDir"))
                {
                    // The indata dir parameter is found, save the parameter value and stop looping.
                    l_batchIndataDir = l_lineParts[(l_ix + 1)];
                    break;
                }
            }
        }
        catch (IOException l_ioEx)
        {
            handleException(l_ioEx);
        }
        finally
        {
            if (l_reader != null)
            {
                try
                {
                    l_reader.close();
                }
                catch (IOException l_ioEx2)
                {
                    // No specific handling.
                    l_ioEx2 = null;
                }
                l_reader = null;
            }
        }

        return l_batchIndataDir;
    }


    /**
     * Handle mouseClicked MouseEvents.
     * @param p_me The Mouse event to handle.
     */
    public void mouseClicked(MouseEvent p_me) {}

    /**
     * Handle mouseReleased MouseEvents.
     * @param p_me The Mouse event to handle.
     */
    public void mouseReleased(MouseEvent p_me)
    {
        int             l_selectedRow                 = 0;
        StringBuffer    l_partialResubmissionFileName = null;
        String          l_resubmissionFileName        = null;
        String          l_seqNum                      = null;
        PpasDate        l_fileDate                    = null;
        String          l_dateStr                     = null;
        String          l_jobType                     = null;
        BoiBatchJobType l_batchJobType                = null;
        
        //System.out.println("MIE Entered mouseClicked, p_me.getSource(): " + p_me.getSource());
        if (p_me.getSource() == i_table)
        {
            //System.out.println("MIE Source is i_table");
            l_batchJobType = getSelectedBatchJobType();
            l_selectedRow = i_table.getSelectedRow();

            if (!i_stringTableModel.getValueAt(
                    l_selectedRow,
                    BatchControlDbService.C_COLUMN_STATUS).equals("C") &&
                !i_stringTableModel.getValueAt(
                    l_selectedRow,
                    BatchControlDbService.C_COLUMN_STATUS).equals("") && 
                l_batchJobType.isResubmittable())
            {
                l_jobType = l_batchJobType.toString();

                // If this is a database type job
                if (l_batchJobType.isDbJobType())
                {
                    if (!l_batchJobType.getDisplayName().equals(C_BC_TYPE_BULKLOADOFACCFINDER))
                    {
                        i_resubmissionMode = true;
                    }
                    
                    if (i_discDateField.isEnabled())
                    {
                        i_discDateField.setValue(
                            new PpasDate((String)i_stringTableModel.getValueAt(
                                            l_selectedRow, BatchControlDbService.C_COLUMN_RUN_DATE),
                                         PpasDate.C_DF_yyyyMMdd));
                    }
                
                    i_runDateTimeField.setValue(DatePatch.getDateTimeNow());
                }
                else
                {
                    // Else it must be a file type batch job.
                    // Build the first part of the filename and then look it up on the server.
                    l_dateStr = (String)i_stringTableModel.getValueAt(
                                            l_selectedRow, BatchControlDbService.C_COLUMN_FILE_DATE);
                    
                    if (l_dateStr.length() > C_DATE_LENGTH_DD_MMM_YYYY)
                    {
                        l_fileDate = new PpasDateTime(l_dateStr);
                    }
                    else
                    {
                        l_fileDate = new PpasDate(l_dateStr);
                    }
                    
                    l_seqNum = (String)i_stringTableModel.getValueAt(
                                           l_selectedRow, BatchControlDbService.C_COLUMN_SEQ_NUM);
                    
                    if (l_seqNum != null)
                    {
                        while (l_seqNum.length() < 5)
                        {
                            l_seqNum = "0" + l_seqNum;
                        }
                    }
                    
                    l_partialResubmissionFileName = new StringBuffer(l_batchJobType.getFilenamePrefix());
                    
                    if (l_jobType.equals(C_BC_TYPE_SDPBALANCING))
                    {
                        l_partialResubmissionFileName.append(l_seqNum);
                    }
                    else if (l_jobType.equals(C_BC_TYPE_MSISDN_ROUTING_PROV) ||
                                 l_jobType.equals(C_BC_TYPE_MSISDN_ROUTING_DELETION))
                    {
                        l_partialResubmissionFileName.append(
                            ((PpasDateTime)l_fileDate).toString_yyyyMMddHHmmss());                      
                    }
                    else
                    {
                        l_partialResubmissionFileName.append(l_fileDate.toString_yyyyMMdd() + "_" + l_seqNum);
                    }

                    if (l_jobType.equals(C_BC_TYPE_SUBSINSTALL))
                    {   
                        setMarketAndServiceClassComboValuesFromTable(l_selectedRow);
                    }

                    l_resubmissionFileName =
                        getBatchFileFromPartialFileName(l_partialResubmissionFileName.toString());
                    i_fileField.setText(l_resubmissionFileName);
                    i_runDateTimeField.setValue(DatePatch.getDateTimeNow());
                }
            }
            else
            {
                // Job has completed successfully, so no need to go to resubmission scenario
                i_resubmissionMode = false;

                // Don't clear the "Directory" ("File") field if the Fraud Returns batch job is selected.
                if (!l_batchJobType.getDisplayName().equals(C_BC_TYPE_FRAUD_RETURNS))
                {
                    i_fileField.setText("");
                }

                i_marketLabel.setVisible(false);
                i_marketBox.setVisible(false);
                i_serviceClassLabel.setVisible(false);
                i_serviceClassBox.setVisible(false);
            }
        }
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e)
    {
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e)
    {
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
        
    }

    /**
     * Handle the user pressing the return key.
     * @param p_keyEvent The <code>KeyEvent</code> that occurred. 
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean  l_return = false;
            
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_clearButton)
            {
                i_clearButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_refreshButton)
            {
                i_refreshButton.doClick();
            }
            else
            {
                i_submitButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }

    /**
     * Handles submit events.
     */
    private void doSubmit()
    {
        HashMap l_jobParamHashMap = null;
        boolean l_valid = false;
        int l_dialogueResponse = 0;
        
        l_valid = validateSubmitJobFields();
        
        if (l_valid)
        {
            l_dialogueResponse = displayConfirmDialog(i_contentPane, "Do you wish to submit this job?");
        
            if (l_dialogueResponse == JOptionPane.YES_OPTION)
            {
                l_jobParamHashMap = createJobParametersHashMap();
            
                // Don't rename the file if this is the Fraud Returns batch.
                if (!getSelectedBatchJobType().isDbJobType() &&
                    !getSelectedBatchJobType().getJsSubmissionName().equals(
                                                              BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS))
                {
                    renameBatchDatFile();
                }
            
                try
                {
                    i_dbService.submitJob(
                                    getSelectedBatchJobType().getJsSubmissionName(),
                                    new PpasDateTime(i_runDateTimeField.getText()),
                                    l_jobParamHashMap,
                                    false,
                                    (String)i_frequencyBox.getSelectedItem(),
                                    i_intervalField.getText());
                    
                    displayMessageDialog(i_contentPane, "Job has been submitted");

                    // May have timed out waiting for user to close dialog box, so check still logged in
                    // before accessing database.
                    if (((Boolean)i_context.getObject("LoggedIn")).booleanValue())
                    {
                        repopulateTableOnPane();
                        if (!getSelectedBatchJobType().isDbJobType())
                        {
                            getBatchDatFiles();
                        }
                    }
                    repopulateSubmitFields();
                    i_jobSubmitted = true;
                    setScreenState();
                }
                catch (PpasServiceException e)
                {
                    handleException(e);
                }
            }
            else
            {
                displayMessageDialog(i_contentPane, "Job submission cancelled");
            }
        }
    }
    
    /**
     * Sets the state of the screen based on the selection status of the 
     * Job Types and Files combo boxes, and on whether a job of this type 
     * has been submitted since it was selected.  The state will be used to 
     * determine whether to warn the user when leaving the screen.
     */
    private void setScreenState()
    {
        if (i_jobTypeComboBox.getSelectedItem().toString().equals(""))
        {
            i_state = C_STATE_INITIAL;
        }
        else if ( (i_filesComboBox.isEnabled() || i_jobSubmitted) &&
                  i_filesComboBox.getSelectedItem().toString().equals("<Please select input file>") )
        {
            // Either we have a job type requiring a file, where the file hasn't been specified, or
            // we have a job type not requiring a file which has already been submitted.
            i_state = C_STATE_DATA_RETRIEVED;
        }
        else
        {
            // Use NEW RECORD state to indicate that the user should be asked to 
            // confirm screen exit as a job has been selected but not submitted.
            i_state = C_STATE_NEW_RECORD;
        }
    }
    
    /**
     * 
     * @param p_selectedRow The currently selected row in the table on the screen.
     */
    private void setMarketAndServiceClassComboValuesFromTable(int p_selectedRow)
    {
        String               l_marketFromTable = null;
        Vector               l_marketVector = null;
        Vector               l_serviceClasses = null;
        DefaultComboBoxModel l_marketModel = null;
        DefaultComboBoxModel l_serviceClassModel = null;
        boolean              l_notFound = true;
        Object               l_boiMarket = null;
        String               l_serviceClassFromTable = null;
        Object               l_boiServiceClass = null;
        
        l_marketVector = i_dbService.getAvailableMarkets();
        l_marketModel = new DefaultComboBoxModel(l_marketVector);
        i_marketBox.setModel(l_marketModel);
        i_marketLabel.setVisible(true);
        i_marketBox.setVisible(true);
        
        l_marketFromTable = 
            (String)i_stringTableModel.getValueAt(
                        p_selectedRow, BatchControlDbService.C_COLUMN_MARKET);
        l_notFound = true;
        
        // Set the market combobox to display what is in the table
        for (int i=0; l_notFound && (i < i_marketBox.getItemCount()); i++)
        {
            l_boiMarket = i_marketBox.getItemAt(i);
            
            if (l_boiMarket != null && !l_boiMarket.equals("") && !l_boiMarket.equals("N/A"))
            {
                if (((BoiMarket)l_boiMarket).toString().equals(l_marketFromTable))
                {
                    i_marketBox.setSelectedIndex(i);
                    l_notFound = false;
                }
            }
        }
        
        if (l_notFound == true)
        {
            // Market from table is nolonger a valid market on ASCS - problem...?
            i_marketBox.setSelectedItem("");
        }
        
        l_serviceClasses = i_dbService.getAvailableServiceClasses(
                               ((BoiMarket)(i_marketBox.getSelectedItem())).getMarket());
        
        l_serviceClassModel = new DefaultComboBoxModel(l_serviceClasses);
        i_serviceClassBox.setModel(l_serviceClassModel);  
        
        // Do the same for service class.
        l_notFound = true;
        l_serviceClassFromTable =
            (String)i_stringTableModel.getValueAt(
                        p_selectedRow, BatchControlDbService.C_COLUMN_SERVICE_CLASS);
        // Set the service class combobox to display what is in the table
        for (int i=0; l_notFound && (i < i_serviceClassBox.getItemCount()); i++)
        {
            l_boiServiceClass = i_serviceClassBox.getItemAt(i);
            
            if (l_boiServiceClass != null && !l_boiServiceClass.equals("") && !l_boiServiceClass.equals("N/A"))
            {
                if (((BoiMiscCodeData)l_boiServiceClass).getInternalMiscCodeData().getCode().equals(l_serviceClassFromTable))
                {
                    i_serviceClassBox.setSelectedIndex(i);
                    l_notFound = false;
                }
            }
        }
        
        if (l_notFound == true)
        {
            // Service Class no longer present in ASCS - problem...?
            i_serviceClassBox.setSelectedItem("");
        }
        
        i_serviceClassLabel.setVisible(true);
        i_serviceClassBox.setVisible(true);
    }
    
    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Class to enable submit processing to be performed in a separate thread.
     */
    public class SubmitRunnable implements Runnable
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------
        
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Only do this if focus was successfully transferred to the submit button.
            // If it is not the focus owner, this would indicate there was a parse
            // error with the previous focus owner.
            if (i_submitButton.isFocusOwner())
            {
                doSubmit();
            }
        }
    }
}
