////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiAccountGroupData.java
//      DATE            :       19-Aug-2004
//      AUTHOR          :       Mario Imfeld
//      REFERENCE       :       PpacLon#470/3661
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for AcgrAccountGroupsData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;

/** Wrapper for account groups class within BOI. */
public class BoiAccountGroupsData extends DataObject
{
    /** Basic account group data object */
    private AcgrAccountGroupData i_acgrData;
    
    /**
     * Simple constructor.
     * @param p_acgrData Account group data object to wrapper.
     */
    public BoiAccountGroupsData(AcgrAccountGroupData p_acgrData)
    {
        i_acgrData = p_acgrData;
    }

    /**
     * Return wrappered account group data object.
     * @return Wrappered account group data object.
     */
    public AcgrAccountGroupData getInternalAccountGroupData()
    {
        return i_acgrData;
    }

    /**
     * Compares two account group data objects and returns true if they equate.
     * @param p_acgrData Account groupo data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_acgrData)
    {
        boolean l_return = false;
        
        if ( p_acgrData != null &&
             p_acgrData instanceof BoiAccountGroupsData &&
             i_acgrData.getAccountID().getValue() ==
                 ((BoiAccountGroupsData)p_acgrData).getInternalAccountGroupData().getAccountID().getValue() )
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns account group data object as a String for display in BOI.
     * @return Account group data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_acgrData.getAccountID() + 
                                            " - " + i_acgrData.getAccountDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return (int)(i_acgrData.getAccountID().getValue());
    }
}