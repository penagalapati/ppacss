////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ServiceClassDetailsPaneUT.java
//      DATE            :       14-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5502
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for ServiceClassDetailsPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for ServiceClassDetailsPane. */
public class ServiceClassDetailsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "ServiceClassDetailsPaneUT";
    
    /** Misc table constant for service classes. */
    private static final String C_MISC_TABLE = "CLS";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Service Class Details screen. */
    private ServiceClassDetailsPane i_serviceClassPane;
    
    /** Service Class code field. */
    private JFormattedTextField i_codeField;
    
    /** Service Class description field. */
    private ValidatedJTextField i_descriptionField;
    
    /** Combo box to hold the defined languages. */
    private JComboBox i_languageBox;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public ServiceClassDetailsPaneUT(String p_title)
    {
        super(p_title);
        
        i_serviceClassPane = new ServiceClassDetailsPane(c_context);
        super.init(i_serviceClassPane);
        
        i_serviceClassPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "keyDataComboBox");
        i_codeField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_languageBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "languageBox");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(ServiceClassDetailsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Service Class Details screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("ServiceClassDetailsPane test");
        
        BoiMiscCodeData l_boiMiscCodeData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_serviceClassPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_serviceClassPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiMiscCodeData = createMiscCodeData(C_CODE, C_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_serviceClassPane, i_keyDataCombo, l_boiMiscCodeData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        
        if(i_languageBox.getComponentCount() > 1)
        {
            i_languageBox.setSelectedIndex(1);
        }
        doUpdate(i_serviceClassPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_serviceClassPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_serviceClassPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_serviceClassPane, i_updateButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteServiceClassRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteServiceClassRecord(C_CODE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiMiscCodeData object with the supplied data.
     * @param p_serviceClassId Service Class id.
     * @param p_serviceClassDesc Service Class description.
     * @return BoiMiscCodeData object created from the supplied data.
     */
    private BoiMiscCodeData createMiscCodeData(String p_serviceClassId,
                                               String p_serviceClassDesc)
    {
        MiscCodeData l_miscCodeData;
        
        l_miscCodeData = new MiscCodeData(null,  // Don't need a market for our purposes
                                          C_MISC_TABLE,
                                          p_serviceClassId,
                                          p_serviceClassDesc,
                                          " ");
        
        return new BoiMiscCodeData(l_miscCodeData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteServiceClassRecord = "deleteServiceClassRecord";
    
    /**
     * Removes rows from SRVA_MISC_CODES and CMDF_CUST_MAST_DEFAULTS.
     * @param p_serviceClass Service Class id.
     */
    private void deleteServiceClassRecord(String p_serviceClass)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = "DELETE from cmdf_cust_mast_defaults WHERE cmdf_cust_class = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_serviceClass);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteServiceClassRecord);
        
        l_sql = "DELETE from srva_misc_codes WHERE misc_code = {0} AND misc_table = {1}";
        l_sqlString = new SqlString(100, 2, l_sql);
        l_sqlString.setStringParam(0, p_serviceClass);
        l_sqlString.setStringParam(1, C_MISC_TABLE);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteServiceClassRecord);
    }
}
