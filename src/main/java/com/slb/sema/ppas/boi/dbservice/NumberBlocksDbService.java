////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       NumberBlocksDbService.java
//    DATE            :       21-Sep-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#470/4216
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Number Blocks screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//16/02/06 | M Erskine  | Get routing method and all of   | PpacLon#1879/7933
//         |            | nubl rather than just for       |
//         |            | current market.                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.NublNumberBlockData;
import com.slb.sema.ppas.common.businessconfig.dataclass.NublNumberBlockDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.NublNumberBlockSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScpiScpInfoSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasConfigException;

/**
 * Database access class for Number Blocks screen. 
 */
public class NumberBlocksDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Used to retrieve routing method from system config. */
    private static final String C_DESTINATION = "BUS_SRV";
    
    /** Used to retrieve routing method from system config. */
    private static final String C_PARAM_NAME = "ROUTING_METHOD";
    
    /** Default routing method. */
    private static final String C_DEFAULT_FORMAT = "SDP";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Vector of available markets for display. */
    private Vector i_availableMarketsV;
    
    /** Operator's default market. */
    private BoiMarket i_defaultMarket;    

    /** Array of number block data objects for the current market. */
    private NublNumberBlockData[] i_numberBlocksArrayForMarket;
    
    /** Array of all <code>NublNumberBlockData</code> from the nubl_number_blocks table. */
    private NublNumberBlockData[] i_numberBlocksArrayAll;

    /** Number block object currently being worked on. */
    private NublNumberBlockData i_currentNumberBlock;

    /** Vector of SDP id strings. */
    private Vector i_availableSdpIdV;
    
    /** Set of SDP id data objects. */
    private ScpiScpInfoDataSet i_scpiDataSet;

    /** SQL service for number block data. */
    private NublNumberBlockSqlService i_nublSqlService;    

    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService;    

    /** SQL service for SDP id data. */
    private ScpiScpInfoSqlService i_scpiSqlService;
    
    /** System-wide configuration SQL service. */
    private SyfgSystemConfigSqlService i_syfgSqlService;
    
    /** Routing method: Number Range OR sdp ID. */
    private String i_routingMethod = null;
 
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public NumberBlocksDbService(BoiContext p_context)
    {
        super(p_context);
        i_nublSqlService = new NublNumberBlockSqlService(null, null, null);
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);
        i_syfgSqlService = new SyfgSystemConfigSqlService(null);
        i_scpiSqlService = new ScpiScpInfoSqlService(null);
        i_availableSdpIdV = new Vector(2);
        i_availableMarketsV = new Vector(5);
    }    
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }    
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }    
    
    /** 
     * Return all number block data records for the current market. 
     * @return Vector of number block data records for the current market.
     */
    public NublNumberBlockData[] getAllNumberBlocksForMarket()
    {
        return i_numberBlocksArrayForMarket;
    }
    
    /**
     * Return all number block data records.
     * @return Vector of number block data records
     */
    public NublNumberBlockData[] getAllNumberBlocks()
    {
        return i_numberBlocksArrayAll;
    }

    /** 
     * Set number block data record currently being worked on. 
     * @param p_numberBlock Number block data record currently being worked on. 
     */
    public void setCurrentNumberBlock(NublNumberBlockData p_numberBlock)
    {
        i_currentNumberBlock = p_numberBlock;
    }        

    /** 
     * Return vector of available SDP id strings.
     * @return Vector of available SDP id strings.
     */
    public Vector getAvailableSdpIds()
    {
        return i_availableSdpIdV;
    }
    
    /** 
     * Returns the routing method, SDP or number range.
     * @return Routing method - SDP or number range.
     */
    public String getRoutingMethod()
    {
        return i_routingMethod;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        Market[]               l_availableMarketArray = null;
        SrvaMstrDataSet        l_srvaMstrDataSet      = null;
        NublNumberBlockDataSet l_nublDataSet          = null;
        SyfgSystemConfigData   l_syfgData             = null;

        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;
        }

        l_nublDataSet = i_nublSqlService.readAllForBoi(null, p_connection);
        i_numberBlocksArrayAll = l_nublDataSet.getAllArray();
        i_numberBlocksArrayForMarket = l_nublDataSet.getRecords(i_currentMarket.getMarket());
        
        l_syfgData = i_syfgSqlService.read(null, p_connection);
        
        try
        {
            i_routingMethod = l_syfgData.get(C_DESTINATION, C_PARAM_NAME);
        }
        catch (PpasConfigException l_pCE)
        {
            i_routingMethod = C_DEFAULT_FORMAT;
        }
        
        refreshSdpIdDataVector(p_connection);
    }
    
    /** 
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }
    
    /** 
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }

    /**
     * Not required.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        boolean[] l_flagsArray = {false, false};
        
        return l_flagsArray;
    }
    
    /** 
     * Inserts record into the NUBL_NUMBER_BLOCKS table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        NublNumberBlockDataSet l_nublDataSet;
        long                   l_quantity = 0;
        String                 l_startNumberString;
        String                 l_endNumberString;
        Market                 l_market;

        l_market = i_currentNumberBlock.getMarket();

        l_startNumberString = i_currentNumberBlock.getNublStartNumberString();
        l_endNumberString = i_currentNumberBlock.getNublEndNumberString();

        l_quantity = Long.parseLong(l_endNumberString) - Long.parseLong(l_startNumberString) + 1;
                
        i_nublSqlService.insert(
            null,
            p_connection,
            i_context.getOperatorUsername(),
            Integer.parseInt(l_market.getSrva()),
            Integer.parseInt(l_market.getSloc()),
            l_startNumberString,
            l_endNumberString,
            l_quantity,
            i_currentNumberBlock.getSdpId());

        l_nublDataSet = i_nublSqlService.readAllForBoi(null, p_connection);
        i_numberBlocksArrayAll = l_nublDataSet.getAllArray();
        i_numberBlocksArrayForMarket = l_nublDataSet.getRecords(l_market);
    }
    
    /** 
     * Not required.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not required.
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available SDP id data vector from the 
     * scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshSdpIdDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {        
        ScpiScpInfoData[] l_sdpArray;        
       
        i_availableSdpIdV.removeAllElements();
        i_availableSdpIdV.add(0, "");
        
        i_scpiDataSet = i_scpiSqlService.readAll(null, p_connection);
        l_sdpArray = i_scpiDataSet.getAvailableArray();
        
        for (int i=0; i < l_sdpArray.length; i++)
        {
            i_availableSdpIdV.addElement(new BoiSdpIdData(l_sdpArray[i]));
        }
    }
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}

