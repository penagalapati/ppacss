////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       AdjustmentCodesDbService.java
//DATE            :       17-Nov-2005
//AUTHOR          :       R.Grimshaw
//REFERENCE       :       PpacLon#1847
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Database access class for Adjustment Codes screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAdjCodesSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
* Database access class for Adjustment Codes screen. 
*/
public class AdjustmentCodesDbService extends BoiDbService
{
    /** Identifies the misc_table column in srva_misc_codes DB table. */
    public static final String C_MISC_TABLE_ADJUSTMENT_TYPE = "ADT";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for account group code details. */
    private SrvaAdjCodesSqlService     i_adjCodesSqlService             = null;

    /** Vector of Adjustment Codes records. */
    private Vector                     i_availableAdjustmentCodesDataV = null;
    
    /** Vector of miscellaneous code records. */
    private Vector                     i_availableMiscCodeDataV;
    
    /** SQL service for Misc Code. */
    private MiscCodeSqlService        i_miscCodeSqlService;

    /** Account group data. */
    private BoiAdjCodesData            i_adjCodesData                   = null;
    
    /** Currently selected adjusyment code. Used as key for read and delete. */
    private String                     i_currentCode               = "";
    
    /** Vector of available markets for display. */
    private Vector                    i_availableMarketsV;
    
    /** Operator's default market. */
    private BoiMarket                 i_defaultMarket                   = null;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService  i_operatorSqlService;
    
    /** Currently selected service class. Used as key for read and delete. Not applicable to every screen. */
    //protected BoiMiscCodeData i_currentAdjustmentType;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public AdjustmentCodesDbService(BoiContext p_context)
    {
        super(p_context);
        i_adjCodesSqlService = new SrvaAdjCodesSqlService(null, null);
        i_availableAdjustmentCodesDataV = new Vector(5);
        i_miscCodeSqlService              = new MiscCodeSqlService (null, null);
        i_availableMiscCodeDataV          = new Vector(5);
        i_availableMarketsV               = new Vector(5);
        i_operatorSqlService              = new BicsysUserfileSqlService(null, null);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Set current account group code. Used for record lookup in database.
     * @param p_adjCode Currently selected adjustment code.
     */
    public void setCurrentAdjustmentCode(String p_adjCode)
    {
        i_currentCode = p_adjCode;
    }

    /**
     * Return account group data currently being worked on.
     * @return Account group data object.
     */
    public BoiAdjCodesData getAdjustmentCodesData()
    {
        return i_adjCodesData;
    }

    /**
     * Set account group data currently being worked on.
     * @param p_adjustmentCodesData Adustment codes data object.
     */
    public void setAdjustmentCodesData(BoiAdjCodesData p_adjustmentCodesData)
    {
        i_adjCodesData = p_adjustmentCodesData;
    }

    /**
     * Return account group data.
     * @return Vector of available account group data records.
     */
    public Vector getAvailableAdjustmentCodesData()
    {
        return i_availableAdjustmentCodesDataV;
    }
    
    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }

    /** 
     * Return available Misc Code data records. 
     * @return Vector of available Misc Code data records.
     */
    public Vector getAvailableAdjustmentTypes()
    {
        return i_availableMiscCodeDataV;
    }
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_adjCodesDataSet;
        String              l_adjustmentType;
        
        l_adjustmentType = i_currentAdjustmentType.getInternalMiscCodeData().getCode();
        
        l_adjCodesDataSet = i_adjCodesSqlService.readAll(null, p_connection);
        
        i_adjCodesData = new BoiAdjCodesData(
                                l_adjCodesDataSet.getAdjustmentData(l_adjustmentType, i_currentCode));
       
    }

    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void readInitial(JdbcConnection p_connection) throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;
        
        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            i_availableMarketsV.addElement(new BoiMarket(new Market(0, 0, "Global Market")));
            
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;        
        }
            
        if (i_currentAdjustmentType == null)
        {
            refreshMiscCodeDataVector(p_connection, i_currentMarket.getMarket());
            if (i_availableMiscCodeDataV.size() != 0)
            {
                i_currentAdjustmentType = (BoiMiscCodeData)i_availableMiscCodeDataV.firstElement();
            }
        }
        
        refreshAdjustmentCodesDataVector(p_connection);
    }

    /**
     * Inserts record into the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_adjCodesSqlService.
            insert(
                null, 
                p_connection, 
                i_context.getOperatorUsername(), 
                i_adjCodesData.getInternalAdjCodesData().getMarket(), 
                i_adjCodesData.getInternalAdjCodesData().getAdjType(),
                i_adjCodesData.getInternalAdjCodesData().getAdjCode(),
                i_adjCodesData.getInternalAdjCodesData().getAdjDesc()
                );

        refreshAdjustmentCodesDataVector(p_connection);
    }

    /**
     * Updates record in the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void update(JdbcConnection p_connection) throws PpasSqlException
    {
        i_adjCodesSqlService.
            update(
                null, 
                p_connection, 
                i_context.getOperatorUsername(),
                i_adjCodesData.getInternalAdjCodesData().getMarket(),
                i_adjCodesData.getInternalAdjCodesData().getAdjType(),
                i_adjCodesData.getInternalAdjCodesData().getAdjCode(),
                i_adjCodesData.getInternalAdjCodesData().getAdjDesc());

        refreshAdjustmentCodesDataVector(p_connection);
    }

    /**
     * Deletes record from the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesData l_adjCodesData = null;
        
        l_adjCodesData = i_adjCodesData.getInternalAdjCodesData();
        
        i_adjCodesSqlService.
            delete(
                null, 
                p_connection, 
                i_context.getOperatorUsername(), 
                l_adjCodesData.getAdjType(),
                i_currentCode);
                
        refreshAdjustmentCodesDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists, and if so, whether it
     * was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     * element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_adjCodesDataSet = null;
        SrvaAdjCodesData l_dbAdjCodesData = null;
        boolean[] l_flagsArray = new boolean[2];
        
        String l_adjustmentType = i_currentAdjustmentType.getInternalMiscCodeData().getCode(); 
        l_adjCodesDataSet = i_adjCodesSqlService.readAll(null, p_connection);
        l_dbAdjCodesData = l_adjCodesDataSet.getAdjustmentData(l_adjustmentType, i_currentCode);

        if (l_dbAdjCodesData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbAdjCodesData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        

        return l_flagsArray;
    }

    /**
     * Marks a previously withdrawn record as available in the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        
        String l_adjustmentType = i_currentAdjustmentType.getInternalMiscCodeData().getCode(); 
        
        try
        {
            i_adjCodesSqlService.
                markAsAvailable(
                    null, 
                    p_connection, 
                    l_adjustmentType, 
                    i_currentCode, 
                    i_context.getOperatorUsername());

            refreshAdjustmentCodesDataVector(p_connection);
        }
        catch (PpasSqlException e)
        {
            e.printStackTrace();
        }
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Refreshes the available account group data vector from the srva_adj_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshAdjustmentCodesDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_adjCodesDataSet = null;
        SrvaAdjCodesData[] l_adjCodesArray = null;
 
        i_availableAdjustmentCodesDataV.removeAllElements();
        
        if ( i_currentAdjustmentType != null )
        {
            String l_adjustmentType = i_currentAdjustmentType.getInternalMiscCodeData().getCode(); 

            l_adjCodesDataSet = i_adjCodesSqlService.readAll(null, p_connection);
            
            // get available only?
            l_adjCodesArray = l_adjCodesDataSet.getAvailableAdjCodesArray(i_currentMarket.getMarket(),
                                                                          l_adjustmentType);
        }
        
        i_availableAdjustmentCodesDataV.addElement(new String(""));
        i_availableAdjustmentCodesDataV.addElement(new String("NEW RECORD"));

        for (int i = 0; (l_adjCodesArray != null && i < l_adjCodesArray.length); i++)
        {
            i_availableAdjustmentCodesDataV.addElement(new BoiAdjCodesData(l_adjCodesArray[i]));
        }

    }
    
    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshMiscCodeDataVector(JdbcConnection p_connection,
                                           Market         p_market)
    throws PpasSqlException
    {
        MiscCodeDataSet   l_miscCodeDataSet;
        MiscCodeDataSet   l_availableMiscCodeDataSet;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;

        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet          = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getRawAvailableMiscCodeData(
                                         p_market, 
                                         C_MISC_TABLE_ADJUSTMENT_TYPE);
        
        l_miscCodeV        = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData        = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableMiscCodeDataV.addElement(l_boiMiscCodeArray[j]);
        }
    }
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
    throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}