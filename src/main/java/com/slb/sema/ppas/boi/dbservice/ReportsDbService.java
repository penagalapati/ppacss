////////////////////////////////////////////////////////////////////////////////
//  ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       ReportsDbService.java
//  DATE            :       7-Sep-2004
//  AUTHOR          :       Martin Brister
//  REFERENCE       :       PpacLon#476/3968
//
//  COPYRIGHT       :       Atos Origin 2004
//
//  DESCRIPTION     :       Database access class for BOI Reports screen.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/6/5   | M.Brister  | Retrieve and store adjustment   | PpacLon#1573/6627
//          |            | type/codes.  Pass selected type | PRD_ASCS00_GEN_CA_49
//          |            | and code in submitJob.          | BHA-011
//----------+------------+---------------------------------+--------------------
// 20/6/5   | M.Brister  | Add constants for following     | PpacLon#1578/6643
//          |            | reports:                        | PRD_ASCS00_GEN_CA_48
//          |            | Service Fee Debts Paid          | BHA-015
//          |            | Service Fee Debts Unpaid        |
//----------+------------+---------------------------------+--------------------
// 14/7/5   | M.Brister  | Added repeat scheduling         | PpacLon#1398/6788
//          |            | parameters to submitJob.        | 
//----------+------------+---------------------------------+--------------------
// 10/11/5  | K Goswami  | Added code to allow user to     | PpacLon#1707/7412
//          |            | specify the report format       |
//----------+------------+---------------------------------+--------------------
// 03/01/05 | R.Grimshaw | Add constants for following     | PpacLon#1893/7692
//          |            | reports:                        | PRD_ASCS00_GEN_CA_??
//          |            | PPAS Adjustments                |         
//          |            | PPAS Cleared Customer Credit    |
//          |            | PPAS Dates of Activation        |
//          |            | PPAS Division Card Recharges    |
//          |            | PPAS Payments                   |
//----------+------------+---------------------------------+--------------------
// 07/04/06 | M Erskine  | Remove the mapping of display   | PpacLon#2036/8373
//          |            | name to JS job type. We get this|
//          |            | in the Reports pane using       |
//          |            | configuration.                  |
//----------+------------+---------------------------------+--------------------
// 27/04/06 | M Erskine  | New reports for Vodafone Ireland| PpacLon#2152/8590
//----------+------------+---------------------------------+--------------------
// 13/06/06 | M Erskine  | Retrofit PpacLon#1707 - Enable  | PpacLon#2407/9086
//          |            | user to specify report format.  |
//----------+------------+---------------------------------+--------------------
// 13/03/07 | S James    | Changes made to support Reports | PpacLon#2995/10671
//          |            | redesign                        |
//----------+------------+---------------------------------+--------------------
// 26/03/07 | S James    | Ensure PPAS Adjustments,        | PpacLon#3020/11213
//          |            | AccountGroupConfig,             |
//          |            | EnhancedSubscriberLifecycle,    |
//          |            | ServiceOfferingsConfig reports  |
//          |            | are handled correctly.          |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.HashMap;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScpiScpInfoSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAdjCodesSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.util.support.Debug;

/** Database access class for BOI Reports screen. */
public class ReportsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    /** Constant defining the class name. */
    private static final String C_CLASS_NAME = "ReportsDbService";

    /** Used to retrieve format from system config. */
    private static final String C_DESTINATION = "REPORTS";
    
    /** Used to retrieve format from system config. */
    private static final String C_PARAM_NAME = "FORMAT";
    
    /** JS request hashmap constant. */
    private static final String C_FORMAT = "format";

    /** JS request hashmap constant. */
    private static final String C_START_DATE_OFFSET = "startDateOffset";

    /** JS request hashmap constant. */
    private static final String C_END_DATE_OFFSET = "endDateOffset";

    /** JS request hashmap constant. */
    private static final String C_ADJ_TYPE = "adjType";

    /** JS request hashmap constant. */
    private static final String C_ADJ_CODE = "adjCode";

    /** JS request hashmap constant. */
    private static final String C_STATUS = "status";

    /** JS request hashmap constant. */
    private static final String C_SDP_ID = "sdpId";

    /** JS request hashmap constant. */
    private static final String C_EXP_DATE_OFFSET = "expiryDateOffset";

    /** JS job type for adjustments. */
    private static final String C_SUBMIT_ADJUSTMENTS = "reportAdjustments";

    /** JS job type for PPAS adjustments. */
    private static final String C_SUBMIT_PPAS_ADJUSTMENTS = "reportPpasAdjustments";

    /** JS job type for MSISDNs. */
    private static final String C_SUBMIT_MSISDNS = "reportMsisdns";

    /** JS job type for Account Groups Config. */
    private static final String C_SUBMIT_ACCOUNT_GROUPS = "reportAccountGroupsConfig";
    
    /** JS job type for Enhanced Subscriber Lifecycle. */
    private static final String C_SUBMIT_ENHANCED_SUB_LC = "reportEnhancedSubscriberLifecycle";
    
    /** JS job type for Service Offerings Config. */
    private static final String C_SUBMIT_SERVICE_OFFERINGS = "reportServiceOfferingsConfig";
    
    /** JS job type for VFI Airtime Expiry. */
    private static final String C_SUBMIT_VFI_AIRTIME_EXPIRY = "reportVfiAirtimeExpiry";
    
    /** Pattern for matching JS job types specifically for Vodafone Ireland. */
    private static final String C_PATTERN_SUBMIT_VFI = "reportVfi.*";
    
    /** Compiled <code>Pattern</code> object for Vodafone Ireland reports. */
    private static final Pattern c_pattern = Pattern.compile(C_PATTERN_SUBMIT_VFI);
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** Default Report format. */
    private String i_defaultReportFormat;
    
    /** System-wide configuration SQL service. */
    private SyfgSystemConfigSqlService i_syfgSqlService;

    /** Job scheduler request parameter hashmap. */
    private HashMap i_submitParamsMap = new HashMap();
    
    /** The JS report type of the last submitted report. */
    private String i_submitReportType = null;
    
    /** SQL service for SDP id data. */
    private ScpiScpInfoSqlService i_scpiSqlService;

    /** Vector of SDP id strings. */
    private Vector i_availableSdpIdV;
    
    /** Set of SDP id data objects. */
    private ScpiScpInfoDataSet i_scpiDataSet;

    /** SQL service for adjustment code data. */
    private SrvaAdjCodesSqlService i_srvaAdjCodesSqlService;

    /** Vector of BOI adjustment code data objects. */
    private Vector i_adjCodesV;
    
    /** Set of adjustment code data objects. */
    private SrvaAdjCodesDataSet i_adjCodesDataSet;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor.
     * @param p_context BOI context object.
     */
    public ReportsDbService(BoiContext p_context)
    {
        super(p_context);
        i_syfgSqlService = new SyfgSystemConfigSqlService(null);
        i_scpiSqlService = new ScpiScpInfoSqlService(null);
        i_availableSdpIdV = new Vector(10,5);
        i_srvaAdjCodesSqlService = new SrvaAdjCodesSqlService(null, null);
        i_adjCodesV = new Vector(20,5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /**
     * Submits job to the job scheduler.
     * @param p_reportType Type of report.
     * @param p_runDateTime Time at which the job is to be run.
     * @param p_startSelectionOffset Difference in days between start selection date and run date.
     * @param p_endSelectionOffset Difference in days between end selection date and run date.
     * @param p_msisdnStatus MSISDN status to query on for reportMsisdns.
     * @param p_sdpId SDP id to query on for reportMsisdns.
     * @param p_adjCodesData Adjustment type and code for adjustments report.
     * @param p_frequency Frequency of job submission.
     * @param p_interval Interval of job submission.  eg. If frequency = DAILY and
     *                   interval = 2, job will run once every 2 days.
     * @param p_daysUntilExpiry Number of days in the future that the report is looking for expiries.
     * @param p_reportFormat Report Format type, whether long or short.
     * @throws PpasServiceFailedException Requested service failed exception.
     */
    public void submitJob(String          p_reportType,
                          PpasDateTime    p_runDateTime,
                          int             p_startSelectionOffset,
                          int             p_endSelectionOffset,
                          String          p_msisdnStatus,
                          BoiSdpIdData    p_sdpId,
                          BoiAdjCodesData p_adjCodesData,
                          String          p_frequency,
                          String          p_interval,
                          String          p_daysUntilExpiry,
                          String          p_reportFormat)
        throws PpasServiceFailedException
    {
        i_submitReportType = p_reportType;

        i_submitParamsMap.clear();
        
        i_submitParamsMap.put(C_FORMAT, p_reportFormat + "");
        
        if(Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10000,
                        "Report Format: " + p_reportFormat );
        }
        
        if ((p_reportType.equals(C_SUBMIT_ADJUSTMENTS) || p_reportType.equals(C_SUBMIT_PPAS_ADJUSTMENTS))
                && p_adjCodesData != null)
        {
            i_submitParamsMap.put(C_ADJ_TYPE, p_adjCodesData.getInternalAdjCodesData().getAdjType());
            i_submitParamsMap.put(C_ADJ_CODE, p_adjCodesData.getInternalAdjCodesData().getAdjCode());
        }

        if (p_reportType.equals(C_SUBMIT_MSISDNS))
        {
            if (!(p_msisdnStatus.equals("ALL")))
            {
                i_submitParamsMap.put(C_STATUS, p_msisdnStatus);
            }
            i_submitParamsMap.put(C_SDP_ID, p_sdpId.getInternalSdpIdData().getScpId());
        }
        else if (c_pattern.matcher(p_reportType).matches() ||
                 p_reportType.equals(C_SUBMIT_ACCOUNT_GROUPS) ||
                 p_reportType.equals(C_SUBMIT_ENHANCED_SUB_LC) ||
                 p_reportType.equals(C_SUBMIT_SERVICE_OFFERINGS))
        {
            if (p_reportType.equals(C_SUBMIT_VFI_AIRTIME_EXPIRY))
            {
                i_submitParamsMap.put(C_EXP_DATE_OFFSET, p_daysUntilExpiry);
            }
        }
        else
        {
            i_submitParamsMap.put(C_START_DATE_OFFSET, p_startSelectionOffset + "");
            i_submitParamsMap.put(C_END_DATE_OFFSET, p_endSelectionOffset + "");
        }
                
        submitJob(p_reportType,
                  p_runDateTime,
                  i_submitParamsMap,
                  false,
                  p_frequency,
                  p_interval.equals("") ? 0 : Integer.parseInt(p_interval));
    }
    
    /** 
     * Return vector of available SDP id strings.
     * @return Vector of available SDP id strings.
     */
    public Vector getAvailableSdpIds()
    {
        return i_availableSdpIdV;
    }
    
    /** 
     * Return vector of BOI adjustment code data objects.
     * @return Vector of BOI adjustment code data objects.
     */
    public Vector getAdjCodes()
    {
        return i_adjCodesV;
    }
    
    /** 
     * Return the default report format from the database
     * @return int value of default report format.
     */
    public String getDefaultReportFormat()
    {
        return i_defaultReportFormat;
    }

    /**
     * Returns the report type of the last submitted report.
     * @return The JS report type.
     */
    public String getLastSubmittedReportType()
    {   
        return i_submitReportType;
    }
    
    /**
     * Returns the job parameters that have just been set. Used for automated testing.
     * @return The job parameter keys.
     */
    public HashMap getSubmitParams()
    {
        return i_submitParamsMap;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException 
    {
        SyfgSystemConfigData l_syfgData;
        
        l_syfgData = i_syfgSqlService.read(null, p_connection);
        try
        {
            i_defaultReportFormat = l_syfgData.get(C_DESTINATION, C_PARAM_NAME);
        }
        catch (PpasConfigException l_pCE)
        {
            // Do nothing, we don't care if this row doesn't exist in the database.
        }
        
        refreshSdpIdDataVector(p_connection);
        refreshAdjCodesDataVector(p_connection);
    }
    
    /** 
     * Method not required for ReportsDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
        // Not required
    }
    
    /** 
     * Method not required for ReportsDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException  
    { 
        // Not required
    }
    
    /** 
     * Method not required for ReportsDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
            // Not required
    }
    
    /** 
     * Method not required for ReportsDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
            // Not required
    }
        
    /** 
     * Method not required for ReportsDbService. 
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException 
    {
        return null;
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /** 
     * Refreshes the available SDP id data vector from the 
     * scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshSdpIdDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {        
        ScpiScpInfoData[] l_sdpArray;        
       
        i_availableSdpIdV.removeAllElements();
        
        i_scpiDataSet = i_scpiSqlService.readAll(null, p_connection);
        l_sdpArray = i_scpiDataSet.getAvailableArray();
        
        for (int i=0; i < l_sdpArray.length; i++)
        {
            i_availableSdpIdV.addElement(new BoiSdpIdData(l_sdpArray[i]));
        }
    }
    
    /** 
     * Refreshes the adjustment codes data vector from the 
     * srva_adj_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshAdjCodesDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {        
        SrvaAdjCodesData[] l_adjCodesArray;        
       
        i_adjCodesV.removeAllElements();
        i_adjCodesV.addElement("ALL");
        
        i_adjCodesDataSet = i_srvaAdjCodesSqlService.readDistinctAdjTypesAndCodes(null, p_connection);
        l_adjCodesArray = i_adjCodesDataSet.getAllArray();
        
        for (int i=0; i < l_adjCodesArray.length; i++)
        {
            i_adjCodesV.addElement(new BoiAdjCodesData(l_adjCodesArray[i]));
        }
    }
}
