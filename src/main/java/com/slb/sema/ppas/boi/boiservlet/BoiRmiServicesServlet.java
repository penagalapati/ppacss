////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiRmiServicesServlet.java
//      DATE            :       7-Dec-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#825/5227
//                              PRD_ASCS00_ANA_FD_15
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Servlet to provide RMI services for BOI.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 22/03/07 | R.Grimshaw | Reload feature licence cache.   | PpacLon#2843
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boiservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slb.sema.ppas.common.dataclass.LogInOutOrigin;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.instrumentation.RemoteInstrumentationClient;
import com.slb.sema.ppas.common.instrumentation.RemoteMngMthRequest;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.SimpleLogInOut;
import com.slb.sema.ppas.is.isapi.PpasSessionService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Servlet to provide RMI services for BOI
 */
public class BoiRmiServicesServlet extends HttpServlet
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    /** Used in calls to middleware. */
    private static final String C_CLASS_NAME = "BoiRmiServicesServlet";
    
    /** Timeout for getting a DB connection. */
    private static final int C_JDBC_TIMEOUT = 20000;
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    
    /** Reference to a PpasSessionService object. */
    private PpasSessionService i_ppasSessionService = null;
    
    /** Reference to a PpasProperties object. */
    private PpasProperties i_properties = null;
    
    /** References a logger. */
    private Logger i_logger = null;
    
    /** Cache reloader object. */
    private RemoteInstrumentationClient i_remoteInstrClient = null;

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** Used for calls to middleware. */ 
    private static final String C_METHOD_init = "init";    

    /**
     * Fetches required items from properties files and the servlet context.
     * @throws ServletException
     */
    public void init()
        throws ServletException
    {   
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                            null, 
                            C_CLASS_NAME, 
                            10000, 
                            this,
                            "Entered " + C_METHOD_init);
        }
        
        ServletContext l_servletContext = null;
        
        l_servletContext = getServletContext();
        
        i_ppasSessionService = (PpasSessionService)l_servletContext.getAttribute(
                                      "com.slb.sema.ppas.is.isapi.PpasSessionService");
        i_properties = (PpasProperties)l_servletContext.getAttribute(
                                      "com.slb.sema.ppas.common.support.PpasProperties");
        i_logger = (Logger)l_servletContext.getAttribute(
                                      "com.slb.sema.ppas.util.logging.Logger");
        
        try
        {
            i_remoteInstrClient = new RemoteInstrumentationClient(i_logger, i_properties);
        }
        catch (PpasConfigException l_pCE)
        {
            throw new ServletException("Error initialising", l_pCE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_END | PpasDebug.C_ST_TRACE,
                            null, 
                            C_CLASS_NAME, 
                            10010, 
                            this,
                            "Leaving " + C_METHOD_init);
        }
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /** Used in calls to middleware. */
    private static final String C_METHOD_doGet = "doGet";

    /**
     * Overrides the method on HttpServlet to handle requests.
     * @param p_request  The http request received.
     * @param p_response The http response to send back.
     */
    protected void doGet(HttpServletRequest  p_request,
                         HttpServletResponse p_response)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_START,
                            null, 
                            C_CLASS_NAME, 
                            10020, 
                            this,
                            "Entered " + C_METHOD_doGet);
        }
        
        ArrayList     l_remoteRefs = null;
        String        l_userName = null;
        String        l_password = null;
        String        l_command = null;
        PrintWriter   l_writer = null;
        StringBuffer  l_processListingResponse = null;
        ArrayList     l_reloadResults = null;
        StringBuffer  l_failedReloadResponse = null;
        RemoteMngMthRequest l_remoteRequest;
        boolean       l_someFailures;
        
        l_userName = p_request.getParameter("userName");
        l_password = p_request.getParameter("password");
        
        SimpleLogInOut l_logInOut = new SimpleLogInOut();
        LogInOutOrigin l_origin   = new LogInOutOrigin(PpasSessionService.C_BOI_USER,
                                                       i_properties.getProcessName(),
                                                       "Unknown");
        
        try
        {
            l_writer = p_response.getWriter();

            // Login to ASCS. This ensures the user taht is logged into a BOI client is really authorised to
            // use ASCS in the server (otherwise naughty people could send a sneaky request).
            i_ppasSessionService.login(null, 0, C_JDBC_TIMEOUT, l_logInOut, l_origin, l_userName, l_password);
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE,
                                null, 
                                C_CLASS_NAME, 
                                10030, 
                                this,
                                "Authenticated in method " + C_METHOD_doGet + " successfully.");
            }
            
            l_command = p_request.getParameter("command");

            if (l_command.equals("GET_PROCESSES"))
            {
                l_processListingResponse = new StringBuffer();

                l_remoteRefs =
                    i_remoteInstrClient.getReferencesForBusinessConfigReload();
                
                p_response.setStatus(HttpServletResponse.SC_OK);
                
                l_processListingResponse.append("status=SUCCESS");
                if (l_remoteRefs != null)
                {
                    for (int i = 0; i < l_remoteRefs.size(); i++)
                    {
                        l_remoteRequest = (RemoteMngMthRequest)l_remoteRefs.get(i);
                        l_processListingResponse.append(
                            "&process" + i + "=" + l_remoteRequest.getBindingName());
                    }
                }

                l_writer.println(l_processListingResponse.toString());
                l_writer.flush(); 
            }
            else if (l_command.equals("RELOAD_CACHE"))
            {
                l_reloadResults =
                    i_remoteInstrClient.doRemoteBusinessConfigReload();

                p_response.setStatus(HttpServletResponse.SC_OK);

                l_someFailures = false;
                l_failedReloadResponse = new StringBuffer();
                for (int i = 0; i < l_reloadResults.size(); i++)
                {
                    l_remoteRequest = (RemoteMngMthRequest)l_reloadResults.get(i);
                    if (!l_remoteRequest.isSuccess())
                    {
                        l_failedReloadResponse.append("&process" + i + "=" + l_remoteRequest.getBindingName());
                        l_someFailures = true;
                    }
                }
                
                if (l_someFailures)
                {
                    l_failedReloadResponse.insert(0, "status=FAILEDRELOAD");
                    l_writer.println(l_failedReloadResponse);
                }
                else
                {
                    l_writer.println("status=SUCCESS");
                }
                
                l_writer.flush(); 
            }
            else if (l_command.equals("RELOAD_FEATURE_CACHE"))
            {
                l_reloadResults =
                    i_remoteInstrClient.doRemoteFeatureLicenceReload();

                p_response.setStatus(HttpServletResponse.SC_OK);

                l_someFailures = false;
                l_failedReloadResponse = new StringBuffer();
                for (int i = 0; i < l_reloadResults.size(); i++)
                {
                    l_remoteRequest = (RemoteMngMthRequest)l_reloadResults.get(i);
                    if (!l_remoteRequest.isSuccess())
                    {
                        l_failedReloadResponse.append("&process" + i + "=" + l_remoteRequest.getBindingName());
                        l_someFailures = true;
                    }
                }
                
                if (l_someFailures)
                {
                    l_failedReloadResponse.insert(0, "status=FAILEDRELOAD");
                    l_writer.println(l_failedReloadResponse);
                }
                else
                {
                    l_writer.println("status=SUCCESS");
                }
                
                l_writer.flush(); 
            }
        }
        catch (PpasServiceException l_e)
        {
            // An exception of this type should already have been logged, so don't relog here.
            
            if (l_writer != null)
            {
                if (l_e.getMsgKey() == PpasServiceMsg.C_KEY_SERVICE_UNAVAILABLE)
                {
                    l_writer.println("status=FAILEDLOOKUP");
                }
                else
                {
                    l_writer.println("status=FAILED");
                }
                l_writer.flush();
            }
        }
        catch (IOException l_e)
        {
            i_logger.logMessage(new LoggableEvent(l_e.getMessage(),
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (l_writer != null)
            {
                l_writer.println("status=FAILED");
                l_writer.flush();
            }
        }
        finally
        {
            if (l_logInOut.isLoggedIn())
            {
                // Make sure they log out.
                i_ppasSessionService.logout(null, C_JDBC_TIMEOUT, l_logInOut, l_origin);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_MWARE,
                            PpasDebug.C_ST_END,
                            null, 
                            C_CLASS_NAME, 
                            10040, 
                            this,
                            "Leaving " + C_METHOD_doGet);
        }
    }
}
