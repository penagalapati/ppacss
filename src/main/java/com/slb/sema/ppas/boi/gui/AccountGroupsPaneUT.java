////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountGroupsPaneUT.java
//      DATE            :       30-Dec-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5382
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       JUnit test class for AccountGroupsPane.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiAccountGroupsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for AccountGroupsPane. */
public class AccountGroupsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AccountGroupsPaneUT";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Account Groups screen. */
    private AccountGroupsPane i_acgrPane;
    
    /** Account Group code field. */
    private JFormattedTextField i_codeField;
    
    /** Account Group description field. */
    private ValidatedJTextField i_descriptionField;

    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AccountGroupsPaneUT(String p_title)
    {
        super(p_title);
        
        i_acgrPane = new AccountGroupsPane(c_context);
        super.init(i_acgrPane);
        
        i_acgrPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AccountGroupsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Account Groups screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AccountGroupsPane test");
        
        BoiAccountGroupsData l_boiAcgrData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_acgrPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_acgrPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiAcgrData = createAccountGroupsData(C_CODE, C_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_acgrPane, i_keyDataCombo, l_boiAcgrData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_acgrPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_acgrPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_acgrPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_acgrPane, i_updateButton);
        
        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteAcgrRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteAcgrRecord(C_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiAccountGroupsData object with the supplied data.
     * @param p_accountGroupId Account group id.
     * @param p_accountGroupDesc Account group description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiAccountGroupsData createAccountGroupsData(String p_accountGroupId,
                                                         String p_accountGroupDesc)
    {
        AcgrAccountGroupData l_acgrData;
        AccountGroupId       l_acgrId = null;

        try
        {
            l_acgrId = new AccountGroupId(p_accountGroupId);
        }
        catch (PpasServiceFailedException l_pSFE)
        {
            fail("Invalid account group id supplied");
        }
        
        l_acgrData = new AcgrAccountGroupData(null,
                                              l_acgrId,
                                              p_accountGroupDesc,
                                              ' ',
                                              C_BOI_OPID,
                                              null);
        
        return new BoiAccountGroupsData(l_acgrData);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteAcgrRecord = "deleteAcgrRecord";
    
    /**
     * Removes a row from ACGR.
     * @param p_accountGroupId Account group id.
     */
    private void deleteAcgrRecord(String p_accountGroupId)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE from acgr_account_group " +
                           "WHERE acgr_account_id = {0}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_accountGroupId);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteAcgrRecord);
    }
}
