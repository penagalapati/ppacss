////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafficCasePaneUT.java
//      DATE            :       30 March 2006
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#2124/8366
//                              PRD_ASCS00_GEN_CA66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit test class for the Traffic Case pane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiTrafficCaseData;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/**
 * This <code>TrafficCasePaneUT</code> class is a JUnit test class for the
 * <code>TrafficCasePane</code> object.
 */
public class TrafficCasePaneUT extends BoiTestCaseTT
{
    //--------------------------------------------------------------------------
    // Constants.                                                             --
    //--------------------------------------------------------------------------
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME            = "TrafficCasePaneUT";
    
    /** Constant for use in short description fields. */
    private static final String C_SHORT_DESCRIPTION     = "test1";
    
    /** Constant for use in short description fields. */
    private static final String C_NEW_SHORT_DESCRIPTION = "test2";


    //--------------------------------------------------------------------------
    // Instance attributes.                                                   --
    //--------------------------------------------------------------------------
    /** Table containing existing records. */
    private TrafficCasePane     i_trafficCasePane       = null;

    /** Traffic Case code field. */
    private JFormattedTextField i_codeField             = null;

    /** Traffic Case short description field. */
    private ValidatedJTextField i_shortDescriptionField = null;
    
    /** Traffic Case description field. */
    private ValidatedJTextField i_descriptionField      = null;

    /** Key data combo box. */
    private JComboBox           i_keyDataCombo          = null;
    
    /** Button for updates and inserts. */
    private JButton             i_updateButton          = null;
    
    /** Delete button. */
    private JButton             i_deleteButton          = null;

    //--------------------------------------------------------------------------
    // Constructors.                                                          --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public TrafficCasePaneUT(String p_title)
    {
        super(p_title);

        i_trafficCasePane = new TrafficCasePane(c_context);
        super.init(i_trafficCasePane);
        i_trafficCasePane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField    = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_shortDescriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                     "shortDescriptionField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //--------------------------------------------------------------------------
    // Public class methods.                                                  --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Static method that allows the framework to automatically run all the
     * tests in the class.
     * A program provided by the JUnit framework can traverse a list of TestCase
     * classes calling this suite method and get an instance of the TestCase
     * which it then executes.
     * 
     * @return an instance of the <code>junit.framework.Test</code> class.
     */
    public static Test suite()
    {
        return new TestSuite(TrafficCasePaneUT.class);
    }

    //--------------------------------------------------------------------------
    // Public instance methods.                                               --
    //--------------------------------------------------------------------------
    /*************************************************************************** 
     * @ut.when a user attempts to insert, select, update, delete, and makes a
     * record available again through the Traffic Case screen.
     * 
     * @ut.then the operations are successfully performed.
     * 
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("TrafficCasePane test");

        BoiTrafficCaseData l_boiTrafficCaseData = null;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_trafficCasePane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_shortDescriptionField, C_SHORT_DESCRIPTION);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_trafficCasePane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiTrafficCaseData = new BoiTrafficCaseData(new TrafTrafficCaseData(null, 
                                                                              Integer.parseInt(C_CODE), 
                                                                              C_DESCRIPTION,
                                                                              "C_SHORT_DESCRIPTION",
                                                                              ' ', 
                                                                              C_BOI_OPID, 
                                                                              null));
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_trafficCasePane, i_keyDataCombo, l_boiTrafficCaseData);


        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_shortDescriptionField, C_NEW_SHORT_DESCRIPTION);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_trafficCasePane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_trafficCasePane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_trafficCasePane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_trafficCasePane, i_updateButton);

        // *********************************************************************
        say("End of test.");
        // *********************************************************************
        endOfTest();
    }

    //--------------------------------------------------------------------------
    // Protected instance methods.                                            --
    //--------------------------------------------------------------------------
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteTrafficCaseRecord(C_CODE);
    }

    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteTrafficCaseRecord(C_CODE);
        say(":::End Of Test:::");
    }
    
    //--------------------------------------------------------------------------
    // Private instance methods.                                              --
    //--------------------------------------------------------------------------
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteTrafficCaseRecord = "deleteTrafficCaseRecord";
    /**
     * Removes a row from the TRAF_TRAFFIC_CASE table.
     * 
     * @param p_trafficCaseCode the traffic case code.
     */
    private void deleteTrafficCaseRecord(String p_trafficCaseCode)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE FROM traf_traffic_case " +
                           "WHERE traf_traffic_case = {0}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_trafficCaseCode);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteTrafficCaseRecord);
    }
}