////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ReportsPaneUT.java
//      DATE            :       06-Apr-2006
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#2036/8373
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit test class for ReportsPane.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 27/04/06 | M Erskine  | New reports for Vodafone Ireland| PpacLon#2152/8590
//----------+------------+---------------------------------+--------------------
// 27/09/06 | John Cheung| Change order of reports to      | PpacLon#2662/10047
//          |            | investigate VFIAirtimeExpiry    |
//          |            | report submission problem.      |
//----------+------------+---------------------------------+--------------------
// 13/03/07 | S James    | Changes made to support Reports | PpacLon#2995/10671
//          |            | redesign                        |
//----------+------------+---------------------------------+--------------------
//21/06/07  | E Dangoor  | Get Job Codes from              | PpacLon#3163/11752
//          |            | js.properties                   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.util.HashMap;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiAccountGroupsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for ReportsPane. */
public class ReportsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "ReportsPaneUT";
    
    // Note that all the constants defined below will eventually be used, 
    // so please do not delete them.
    
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_ACTIVATION_DATES = "reportActivationDates";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_ADJUSTMENTS = "reportAdjustments";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_ACCOUNT_REFILLS = "reportAccountRefills";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_VOUCHER_REFILLS = "reportVoucherRefills";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_EXTENDED_REFILLS = "reportExtendedRefills";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_FAILED_VCH_REFILLS = "reportFailedVoucherRefills";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_RENEWAL_PERIODS = "reportRenewalPeriods";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_CLEARED_CREDIT = "reportClearedCredit";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_DEACTIVATIONS = "reportDeactivations";
    /** Job type constant for submitting to job scheduler. */
    private static final String C_SUBMIT_ENHANCED_SUBS_LIFECYCLE = "reportEnhancedSubscriberLifecycle";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_NEG_BAL_BARRINGS = "reportNegativeBalanceBarred";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_SERVICE_UPDATES = "reportServiceUpdates";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_MSISDNS = "reportMsisdns";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_SFEE_DEBTS_PAID = "reportServiceFeeDebtsPaid";
    /** Job type constant for submitting to job scheduler. */ 
    private static final String C_SUBMIT_SFEE_DEBTS_UNPAID = "reportServiceFeeDebtsUnpaid";
    /** Job type constant for submitting to job scheduler. */ 
    public static final String C_SUBMIT_PPAS_ADJSUTMENTS = "reportPpasAdjustments";
    /** Job type constant for submitting to job scheduler. */ 
    public static final String C_SUBMIT_PPAS_CLEARED_CUSTOMER_CREDIT = "reportPpasClearedCustomerCredit";
    /** Job type constant for submitting to job scheduler. */ 
    public static final String C_SUBMIT_PPAS_DATES_OF_ACTIVATION = "reportPpasDatesOfActivation";
    /** Job type constant for submitting to job scheduler. */ 
    public static final String C_SUBMIT_PPAS_DIVISION_CARD_RECHARGES = "reportPpasDivisionCardRecharges";
    /** Job type constant for submitting to job scheduler. */ 
    public static final String C_SUBMIT_PPAS_PAYMENTS = "reportPpasPayments";
    
    /** Report type constant for display. */
    private static final String C_DISPLAY_ACTIVATION_DATES = "Activation Dates";
    /** Report type constant for display. */
    public static final String C_DISPLAY_ADJUSTMENTS = "Adjustments";
    /** Report type constant for display. */
    private static final String C_DISPLAY_ACCOUNT_REFILLS = "Account Refills";
    /** Report type constant for display. */
    private static final String C_DISPLAY_VOUCHER_REFILLS = "Voucher Refills";
    /** Report type constant for display. */
    private static final String C_DISPLAY_EXTENDED_REFILLS = "Extended Refills";
    /** Report type constant for display. */
    private static final String C_DISPLAY_FAILED_VCH_REFILLS = "Failed Voucher Refills";
    /** Report type constant for display. */
    private static final String C_DISPLAY_RENEWAL_PERIODS = "Renewal Periods";
    /** Report type constant for display. */
    private static final String C_DISPLAY_CLEARED_CREDIT = "Cleared Credit";
    /** Report type constant for display. */
    private static final String C_DISPLAY_DEACTIVATIONS = "Deactivations";
    /** Report type constant for display. */
    private static final String C_DISPLAY_ENHANCED_SUBS_LIFECYCLE = "Enhanced Subscriber Lifecycle";
    /** Report type constant for display. */
    private static final String C_DISPLAY_NEG_BAL_BARRINGS = "Negative Balance Barrings";
    /** Report type constant for display. */
    private static final String C_DISPLAY_SERVICE_UPDATES = "Service Updates";
    /** Report type constant for display. */
    public static final String C_DISPLAY_MSISDNS = "MSISDNs";
    /** Report type constant for display. */
    public static final String C_DISPLAY_SFEE_DEBTS_PAID = "Service Fee Debts Paid";
    /** Report type constant for display. */
    public static final String C_DISPLAY_SFEE_DEBTS_UNPAID = "Service Fee Debts Unpaid";
    /** Report type constant for display. */
    public static final String C_DISPLAY_PPAS_ADJSUTMENTS = "PPAS Adjustments";
    /** Report type constant for display. */
    public static final String C_DISPLAY_PPAS_CLEARED_CUSTOMER_CREDIT = "PPAS Cleared Customer Credit";
    /** Report type constant for display. */
    public static final String C_DISPLAY_PPAS_DATES_OF_ACTIVATION = "PPAS Dates of Activation";
    /** Report type constant for display. */
    public static final String C_DISPLAY_PPAS_DIVISION_CARD_RECHARGES = "PPAS Division Card Recharges";
    /** Report type constant for display. */
    public static final String C_DISPLAY_PPAS_PAYMENTS = "PPAS Payments";
    /** Report type constant for display. */
    public static final String C_DISPLAY_VFI_AIRTIME_EXPIRY = "VFI Airtime Expiry";
    
    final String C_JS_JOB_DETAIL_PREFIX = "com.slb.sema.ppas.js.jscommon.JsContext.jobDetail.";

    
    /** Expected standard set of job submission parameters. */
    public static HashMap c_submitParamsStandardExpected;
    
    /** Expected set of job submission parameters in an MSISDNs report. */
    public static HashMap c_submitParamsAdjustmentsExpected;
    
    /** Expected set of job submission parameters in an MSISDNs report. */
    public static HashMap c_submitParamsMsisdnsExpected;
    
    /** Expected set of job submission parameters in a VFI Airtime Expiry report. */
    public static HashMap c_submitParamsVfiAirtimeExpiryExpected;

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Reports screen. */
    private static ReportsPane i_reportsPane;
    
    /** Combo-box containing the report types. */
    protected JComboBox i_reportTypeCombo;
    
    /** Combo-box containing the available repetition intervals. */
    protected JComboBox i_repeatCombo;
    
    /** Field for the repetition interval. */
    protected JFormattedTextField i_intervalField;
    
    /** Combo-box containing the available report formats for the selected report. */
    protected JComboBox i_formatCombo;
    
    /** Button for submission of jobs. */
    protected JButton i_submitButton;
    
    /** Reset button. */
    protected JButton i_resetButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public ReportsPaneUT(String p_title)
    {
        super(p_title);

        HashMap                    l_reportClassDisplayNameMap    = null;
        String                     l_availableJobTypes            = null;
        String[]                   l_st                           = null;

        l_reportClassDisplayNameMap = c_properties.getHashFromProperty(
                                          "com.slb.sema.ppas.boi.gui.Reports.reportDisplayNames",
                                          (String)null);
        l_availableJobTypes = c_properties.getTrimmedProperty("com.slb.sema.ppas.js.jscommon.JsContext.jobTypes");

        // Available Reports screen to job type mapping 
        l_st = l_availableJobTypes.split(",");
        
        StringBuffer l_jobCodesBuffer = new StringBuffer("");
        for (int i = 0; i < l_st.length; i++)
        {
            String   l_jobName          = l_st[i].toString();
            String   l_jsPropertyKey    = C_JS_JOB_DETAIL_PREFIX + l_jobName + ".code";
            String   l_jobCode          = c_properties.getTrimmedProperty(l_jsPropertyKey);

            // add this info to a string buffer which will be passed to the applet
            l_jobCodesBuffer.append(l_jobName + "," + l_jobCode);

            l_jobCodesBuffer.append(";");
        }

        c_context.addObject("ascs.boi.jobCodes",l_jobCodesBuffer.toString());

        StringBuffer l_reportsMasterInfoBuffer = new StringBuffer("");
        for (int i = 0; i < l_st.length; i++)
        {
            if (l_st[i].substring(0,6).equalsIgnoreCase("report"))
            {
                String l_jsPropertyKey      = C_JS_JOB_DETAIL_PREFIX + l_st[i].toString() + ".class";
                String l_reportJobClassName = null;
                String l_reportDisplayName  = null;
                String l_reportJobName      = null;
                String[] l_reportFormatArray = null;
                Class  l_reportClass        = null;
                
                l_reportJobClassName = c_properties.getTrimmedProperty(l_jsPropertyKey);
                l_reportDisplayName = (String)l_reportClassDisplayNameMap.get(
                                          l_reportJobClassName.substring(l_reportJobClassName.lastIndexOf(".")+1,
                                                                     l_reportJobClassName.length()));
            
                try
                {
                    l_reportClass = Class.forName(l_reportJobClassName);
                    l_reportJobName = l_reportClass.getField("C_JOB_NAME").get(null).toString();
                    l_reportFormatArray = (String[])l_reportClass.getField("C_FORMAT_LIST").get(null);
                }
                catch (Exception l_e)
                {
                    fail(l_e.toString());
                }

                l_reportsMasterInfoBuffer.append(l_reportDisplayName +"," + l_reportJobName);
                for (int l_index = 0; l_index < l_reportFormatArray.length; l_index++)
                {
                    l_reportsMasterInfoBuffer.append(","+l_reportFormatArray[l_index]);
                }
                l_reportsMasterInfoBuffer.append(";");
            }
        }

        c_context.addObject("ascs.boi.availableReports",l_reportsMasterInfoBuffer.toString());

        c_context.addObject(
            "ascs.NodeName",
            "borgdev1");
        
        i_reportsPane = new ReportsPane(c_context);
        super.init(i_reportsPane);
        
        i_reportsPane.resetScreenUponEntry();
        
        i_reportTypeCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "reportTypeComboBox");
        i_repeatCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "frequencyBox");
        i_intervalField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "intervalField");
        i_formatCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "reportFormatComboBox");
        i_submitButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "submitButton");
        i_resetButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "resetButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(ReportsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** 
     * @ut.when a user submits Account Refills, Adjustments and Msisdns reports from the BOI
     *          Reports screen.
     * @ut.then the combo-boxes are populated correctly and a job is successfully submitted with the 
     *          correct parameters and report type.
     * @ut.attributes +f
     */
    public void testSubmitReports()
    {   
        beginOfTest("SubmitAccountRefillsReport");
        HashMap l_hashMap = null;
        
        SwingTestCaseTT.setJobTypeComboSelectedItem(
            i_reportsPane, i_reportTypeCombo, C_DISPLAY_ACCOUNT_REFILLS);
        SwingTestCaseTT.setReportFormatComboSelectedItem(i_reportsPane, i_formatCombo, "long");
        doSubmit(i_reportsPane, i_submitButton);
        
        // *********************************************************************
        say("Submitted report type: " + i_reportsPane.getDbService().getLastSubmittedReportType());
        // *********************************************************************
        
        assertTrue(
            "Wrong JS report type submitted",
            i_reportsPane.getDbService().getLastSubmittedReportType().equals(C_SUBMIT_ACCOUNT_REFILLS));
        
        l_hashMap = i_reportsPane.getDbService().getSubmitParams();

        assertTrue(
            "Wrong job paramaters submitted, expected: " + c_submitParamsStandardExpected.keySet() + 
                ", but was: " + l_hashMap.keySet(),
            l_hashMap.keySet().equals(c_submitParamsStandardExpected.keySet()));
        endOfTest();

        beginOfTest("SubmitAdjustmentsReport");
        
        SwingTestCaseTT.setJobTypeComboSelectedItem(
            i_reportsPane, i_reportTypeCombo, C_DISPLAY_ADJUSTMENTS);
        SwingTestCaseTT.setReportFormatComboSelectedItem(i_reportsPane, i_formatCombo, "default");
        doSubmit(i_reportsPane, i_submitButton);
        
        // *********************************************************************
        say("Submitted report type: " + i_reportsPane.getDbService().getLastSubmittedReportType());
        // *********************************************************************
        
        assertTrue(
            "Wrong JS report type submitted",
            i_reportsPane.getDbService().getLastSubmittedReportType().equals(C_SUBMIT_ADJUSTMENTS));
        
        l_hashMap = i_reportsPane.getDbService().getSubmitParams();

        assertTrue(
            "Wrong job paramaters submitted, expected: " + c_submitParamsAdjustmentsExpected.keySet() + 
                ", but was: " + l_hashMap.keySet(),
            l_hashMap.keySet().equals(c_submitParamsAdjustmentsExpected.keySet()));
        endOfTest();
        
        beginOfTest("SubmitVFIAirtimeExpiryReport");
        SwingTestCaseTT.setJobTypeComboSelectedItem(
            i_reportsPane, i_reportTypeCombo, C_DISPLAY_VFI_AIRTIME_EXPIRY);
        SwingTestCaseTT.setReportFormatComboSelectedItem(i_reportsPane, i_formatCombo, "default");
        doSubmit(i_reportsPane, i_submitButton);
        
        // *********************************************************************
        say("Submitted report type: " + i_reportsPane.getDbService().getLastSubmittedReportType());
        // *********************************************************************
        
        assertTrue(
            "Wrong JS report type submitted",
            i_reportsPane.getDbService().getLastSubmittedReportType().equals("reportVfiAirtimeExpiry"));
        
        l_hashMap = i_reportsPane.getDbService().getSubmitParams();

        assertTrue(
            "Wrong job paramaters submitted, expected: " + c_submitParamsVfiAirtimeExpiryExpected.keySet() + 
                ", but was: " + l_hashMap.keySet(),
            l_hashMap.keySet().equals(c_submitParamsVfiAirtimeExpiryExpected.keySet()));
        endOfTest();

        beginOfTest("SubmitMsisdnsReport");
        SwingTestCaseTT.setJobTypeComboSelectedItem(
            i_reportsPane, i_reportTypeCombo, C_DISPLAY_MSISDNS);
        SwingTestCaseTT.setReportFormatComboSelectedItem(i_reportsPane, i_formatCombo, "default");
        doSubmit(i_reportsPane, i_submitButton);
        
        // *********************************************************************
        say("Submitted report type: " + i_reportsPane.getDbService().getLastSubmittedReportType());
        // *********************************************************************
        
        assertTrue(
            "Wrong JS report type submitted",
            i_reportsPane.getDbService().getLastSubmittedReportType().equals(C_SUBMIT_MSISDNS));
        
        l_hashMap = i_reportsPane.getDbService().getSubmitParams();

        assertTrue(
            "Wrong job paramaters submitted, expected: " + c_submitParamsMsisdnsExpected.keySet() + 
                ", but was: " + l_hashMap.keySet(),
            l_hashMap.keySet().equals(c_submitParamsMsisdnsExpected.keySet()));
        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        
        c_submitParamsStandardExpected = new HashMap();
        c_submitParamsStandardExpected.put("startDateOffset", "");
        c_submitParamsStandardExpected.put("endDateOffset", "");
        c_submitParamsStandardExpected.put("format", "");
        
        c_submitParamsAdjustmentsExpected = new HashMap();
        c_submitParamsAdjustmentsExpected.put("format", "");
        //TODO: param3 and param4 only present if selected Adjustment Type/Code is not "ALL".
        //c_submitParamsAdjustmentsExpected.put("param3", "");
        //c_submitParamsAdjustmentsExpected.put("param4", "");
        c_submitParamsAdjustmentsExpected.put("startDateOffset", "");
        c_submitParamsAdjustmentsExpected.put("endDateOffset", "");
        
        c_submitParamsMsisdnsExpected = new HashMap();
        c_submitParamsMsisdnsExpected.put("format", "");
        
        //Only present if not set to "ALL".
        //c_submitParamsMsisdnsExpected.put("param3", "");
        c_submitParamsMsisdnsExpected.put(getReportParameterList(
            c_properties.getTrimmedProperty(C_JS_JOB_DETAIL_PREFIX + "reportMsisdns.class"))[1][0], "");
        
        c_submitParamsVfiAirtimeExpiryExpected = new HashMap();
        c_submitParamsVfiAirtimeExpiryExpected.put("format", "");
        c_submitParamsVfiAirtimeExpiryExpected.put(getReportParameterList(
            c_properties.getTrimmedProperty(C_JS_JOB_DETAIL_PREFIX + "reportVfiAirtimeExpiry.class"))[0][0], "");
    }

    /**
     * Gets the Parameter list for the specified class
     * @param p_className  the class name
     * @return the parameter list
     */
    private String[][] getReportParameterList(String p_className)
    {
        String [][] l_reportParamsArray = null;

        try
        {
            Class l_reportClass = Class.forName(p_className);
            l_reportParamsArray = (String[][])l_reportClass.getField("C_PARAMETER_LIST").get(null);
        }
        catch (Exception l_e)
        {
            fail(l_e.toString());
        }
        return l_reportParamsArray;   
    }
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
}
