////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatedBonusElementPaneUT.java
//      DATE            :       13-Aug-2007
//      AUTHOR          :       Steven James
//      REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       JUnit test class for AccumulatedBonusPane 
//                              (Elements part).
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description of change>         | PpacLon#XXXX/YYYY
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiBonusElementData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.gui.GuiPane;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for bonus elements. */
public class AccumulatedBonusElementPaneUT extends AccumulatedBonusPaneUT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AccumulatedBonusElementPaneUT";
    
    /** Constant for use in selecting to input a new element. */
    protected static final String C_NEW_ELEMENT = "NEW ELEMENT";
    
    /** Bonus scheme id to use. */
    protected static final String C_BONUS_SCHEME = "A1A";
    
    /** Comma separated String of key elements used to look up record. */
    protected static final String C_KEY_ELEMENTS = "CHAN2,,1";
    
    /** Comma separated String of non-key elements used to look up record. */
    protected static final String C_NONKEY_ELEMENTS = "0";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Field contained in screen being tested. */
    protected JComboBox i_element1Field;
    
    /** Field contained in screen being tested. */
    protected ValidatedJTextField i_element2Field;
    
    /** Field contained in screen being tested. */
    protected JComboBox i_element3Field;
    
    /** Field contained in screen being tested. */
    protected JCheckBox i_activeElementIndCheckbox;
    
    /** Button for updates and inserts for bonus elements. */
    protected JButton i_updateButtonEl;
    
    /** Delete button for bonus elements. */
    protected JButton i_deleteButtonEl;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AccumulatedBonusElementPaneUT(String p_title)
    {
        super(p_title);
        System.out.println("MIE entered BonusElementPaneUT constructor");
        
        i_element1Field = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "element1Field");
        i_element2Field = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "element2Field");
        i_element3Field = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "element3Field");
        i_activeElementIndCheckbox = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, "activeElementIndCheckbox");
        
        i_updateButtonEl = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButtonEl");
        i_deleteButtonEl = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButtonEl");
        
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AccumulatedBonusElementPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Bonus screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AccumulatedBonusElementPane test");
        
        BoiBonusElementData l_boiBonusElementData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        i_keyDataCombo.setSelectedIndex(2); // Set to the first valid bonus scheme in the list.

        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_keyElementDataCombo, 1); // NEW ELEMENT
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_element1Field, 3);
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_element3Field, 3);
        SwingTestCaseTT.setCheckBoxValue(i_activeElementIndCheckbox, true);
        
        doInsertEl(i_bonsPane, i_updateButtonEl);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiBonusElementData = createBoiBoneData();
        
        SwingTestCaseTT.setComboSelectedItem(i_bonsPane, i_keyElementDataCombo, l_boiBonusElementData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setCheckBoxValue(i_activeElementIndCheckbox, false);
        doUpdateEl(i_bonsPane, i_updateButtonEl);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDeleteEl(i_bonsPane, i_deleteButtonEl);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_keyElementDataCombo, 1); // NEW ELEMENT
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_element1Field, 3);
        SwingTestCaseTT.setComboSelectedIndex(i_bonsPane, i_element3Field, 3);
        doMakeAvailableEl(i_bonsPane, i_updateButtonEl);
        
        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteBoneRecord("A1A");
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteBoneRecord("A1A");
        say(":::End Of Test:::");
    }
    
    /**
     * Invokes a DB update and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for updates.
     */
    protected void doUpdateEl(GuiPane p_pane,
                              JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        // Don't need to invoke later, since the application ensures that this event is
        // performed after other events in the queue.
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(4000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_UPDATED);
        System.out.println("p_pane.getState(): " + p_pane.getState());

    }
    
    /**
     * Invokes a DB insert and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for inserts.
     */
    protected void doInsertEl(GuiPane p_pane,
                              JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        // Don't need to invoke later, since the application ensures that this event is
        // performed after other events in the queue.
        p_button.doClick();
        try
        {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_INSERTED);

        SwingTestCaseTT.waitForState(p_pane,
                                     GuiPane.C_STATE_DATA_RETRIEVED,
                                     "Problem with GuiPane state.");
    }
    
    /**
     * Invokes a DB delete and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used for deletes.
     */
    protected void doDeleteEl(GuiPane p_pane,
                              JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(2000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_YES_BUTTON_TEXT,
                                       C_CONFIRM_DELETE_RECORD);
        
    }
    
    /**
     * Attempts to make a record available in the DB, and checks that a confirmation popup is displayed.
     * @param p_pane GUI pane being tested.
     * @param p_button Button used to make records available.
     */
    protected void doMakeAvailableEl(GuiPane p_pane,
                                     JButton p_button)
    {
        GuiPane.resetDialogBox();
        
        SwingTestCaseTT.invokeButtonClickLater(p_button);
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(1,
                                       C_NO_BUTTON_TEXT,
                                       C_CONFIRM_OVERWRITE_RECORD);
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        

        SwingTestCaseTT.checkDialogBox(0,
                                       C_YES_BUTTON_TEXT,
                                       C_CONFIRM_MAKE_RECORD_AVAIL);

        // Ensure we don't get the previous dialog box again.
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        SwingTestCaseTT.checkDialogBox(0,
                                       C_OK_BUTTON_TEXT, 
                                       C_MSG_RECORD_MADE_AVAIL);

    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiBonusElementData object for this test/.
     * @return BoiBonusElementData object created.
     */
    private BoiBonusElementData createBoiBoneData()
    {
        BoneBonusElementData l_boneData;
        
        l_boneData = new BoneBonusElementData(C_BONUS_SCHEME,
                                              C_KEY_ELEMENTS,
                                              C_NONKEY_ELEMENTS,
                                              null,
                                              null,
                                              null,
                                              null,
                                              null,
                                              null,
                                              true,
                                              false,
                                              C_BOI_OPID,
                                              null);          
        
        return new BoiBonusElementData(l_boneData);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteBoneRecord = "deleteBoneRecord";
    
    /**
     * Removes a row from BONE.
     * @param p_bonusSchemeId Account group id.
     */
    private void deleteBoneRecord(String p_bonusSchemeId)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE from bone_bonus_elements " +
                           "WHERE bone_scheme_id = {0} " +
                           "AND bone_key_elements = {1} " + 
                           "AND bone_nonkey_elements = {2}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_bonusSchemeId);
        l_sqlString.setStringParam(1, C_KEY_ELEMENTS);
        l_sqlString.setStringParam(2, C_NONKEY_ELEMENTS);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteBoneRecord);
    }
}
