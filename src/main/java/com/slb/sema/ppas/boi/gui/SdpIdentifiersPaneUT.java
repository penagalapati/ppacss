////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SdpIdentifiersPaneUT.java
//    DATE            :       09-Nov-2005
//    AUTHOR          :       Yang Liu
//    REFERENCE       :       PpacLon#1755/7463
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       JUnit test class for SdpIdentifiersPane.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//  24/11/05| Yang L.    | Review comments correction.     | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for SdpIdentifiersPane. */
public class SdpIdentifiersPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "SdpIdentifiersPaneUT";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** SDP Identifiers screen. */
    private SdpIdentifiersPane  i_sdpIdentifiersPane;

    /** SDP Identifiers code field. */
    private ValidatedJTextField i_codeField;

    /** SDP Identifiers description field. */
    private ValidatedJTextField i_descriptionField;

    /** Key data combo box. */
    protected JComboBox         i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton           i_updateButton;

    /** Delete button. */
    protected JButton           i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public SdpIdentifiersPaneUT(String p_title)
    {
        super(p_title);

        i_sdpIdentifiersPane = new SdpIdentifiersPane(c_context);
        super.init(i_sdpIdentifiersPane);

        i_sdpIdentifiersPane.resetScreenUponEntry();

        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(SdpIdentifiersPaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again through
     * the SDP Identifiers screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("SdpIdentifiersPane test");

        BoiSdpIdData l_boiSdpIdData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_sdpIdentifiersPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_sdpIdentifiersPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiSdpIdData = new BoiSdpIdData(new ScpiScpInfoData(null,
                                                              C_CODE,
                                                              "",
                                                              C_DESCRIPTION,
                                                              null,
                                                              C_BOI_OPID,
                                                              ' ',
                                                              'G',
                                                              false));
        SwingTestCaseTT.setKeyComboSelectedItem(i_sdpIdentifiersPane, i_keyDataCombo, l_boiSdpIdData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_sdpIdentifiersPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_sdpIdentifiersPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_sdpIdentifiersPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_sdpIdentifiersPane, i_updateButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteSdpIdRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteSdpIdRecord(C_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteSdpIdRecord = "deleteSdpIdRecord";

    /**
     * Removes a row from SCPI_SCP_INFO.
     * @param p_scpId SDP Identifiers id.
     */
    private void deleteSdpIdRecord(String p_scpId)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = new String("DELETE from SCPI_SCP_INFO " + "WHERE SCPI_SCP_ID = {0}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_scpId);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteSdpIdRecord);
    }
}

