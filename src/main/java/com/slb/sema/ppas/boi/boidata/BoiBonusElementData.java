////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiBonusElementData.java
//      DATE            :       19-Dec-2006
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#XXX/YYYY
//                              PRD_ASCS00_GEN_CA_104
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Wrapper for BoneBonusElementData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;

/** Wrapper for BoneBonusElementData class within BOI. */
public class BoiBonusElementData extends DataObject
{
    /** Wrappered data object */
    private BoneBonusElementData i_boneElementData;
    
    /**
     * Simple constructor.
     * @param p_bonusElementData Bonus element data object to wrapper.
     */
    public BoiBonusElementData(BoneBonusElementData p_bonusElementData)
    {
        i_boneElementData = p_bonusElementData;
    }
    
    /**
     * Return wrappered adjustment code data object.
     * @return Wrappered adjustment code data object.
     */
    public BoneBonusElementData getInternalBonusElementData()
    {
        return i_boneElementData;
    }
    
    /**
     * Compares two adjustment code data objects and returns true if they equate.
     * @param p_bonusElementData Bonus element data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_bonusElementData)
    {
        boolean          l_return = false;
        BoneBonusElementData l_adjCodesData = null;
        
        if (p_bonusElementData != null &&
            p_bonusElementData instanceof BoiBonusElementData)
        {
            l_adjCodesData = ((BoiBonusElementData)p_bonusElementData).getInternalBonusElementData();
            
            if (i_boneElementData.getBonusSchemeId().equals(l_adjCodesData.getBonusSchemeId()) &&
                i_boneElementData.getKeyElements().equals(l_adjCodesData.getKeyElements()) &&
                i_boneElementData.getNonkeyElements().equals(l_adjCodesData.getNonkeyElements()))
            {
                l_return = true;
            }
        }
        return l_return;
    }
    
    /**
     * Returns adjustment code data object as a String for display in BOI.
     * @return Bonus element data object as a string.
     */
    public String toString()
    {
        StringBuffer l_displaySB = new StringBuffer(i_boneElementData.getBonusSchemeId() +
                                            " - " + i_boneElementData.getKeyElements());
        if(!i_boneElementData.getNonkeyElements().equals("0"))
        {
            l_displaySB.append(" - " + i_boneElementData.getNonkeyElements());
        }
        return l_displaySB.toString();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_boneElementData.getBonusSchemeId().length();
    }
}