////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BanksPaneUT.java
//      DATE            :       12 November 2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PpacLon#1755/7428
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       JUnit test class for the Bank Codes pane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 18/11/05 | Lars L.    | Review comments corrections.    |PpacLon#1755/7469
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiBanksData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/**
 * This <code>BanksPaneUT</code> class is a JUnit test class for the
 * <code>BanksPane</code> object.
 */
public class BanksPaneUT extends BoiTestCaseTT
{
    //--------------------------------------------------------------------------
    // Constants.                                                             --
    //--------------------------------------------------------------------------
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME     = "BanksPaneUT";

    /** The test bank code. */
    private static final String C_BANK_CODE      = "99999";

    /** The test bank name. */
    private static final String C_BANK_NAME      = "ASCS Dummy Bank";

    /** The new test bank name. */
    private static final String C_NEW_BANK_NAME  = "The New ASCS Dummy Bank";


    //--------------------------------------------------------------------------
    // Instance attributes.                                                   --
    //--------------------------------------------------------------------------
    /** Table containing existing records. */
    private BanksPane           i_banksPane    = null;

    /** Bank Codes code field. */
    private ValidatedJTextField i_codeField    = null;

    /** Bank Codes name field. */
    private ValidatedJTextField i_nameField    = null;

    /** Key data combo box. */
    private JComboBox           i_keyDataCombo = null;
    
    /** Button for updates and inserts. */
    private JButton             i_updateButton = null;
    
    /** Delete button. */
    private JButton             i_deleteButton = null;


    //--------------------------------------------------------------------------
    // Constructors.                                                          --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public BanksPaneUT(String p_title)
    {
        super(p_title);

        i_banksPane = new BanksPane(c_context);
        super.init(i_banksPane);
        i_banksPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField    = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_nameField    = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "nameField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }


    //--------------------------------------------------------------------------
    // Public class methods.                                                  --
    //--------------------------------------------------------------------------
    /***************************************************************************
     * Static method that allows the framework to automatically run all the
     * tests in the class.
     * A program provided by the JUnit framework can traverse a list of TestCase
     * classes calling this suite method and get an instance of the TestCase
     * which it then executes.
     * 
     * @return an instance of the <code>junit.framework.Test</code> class.
     */
    public static Test suite()
    {
        return new TestSuite(BanksPaneUT.class);
    }


    //--------------------------------------------------------------------------
    // Public instance methods.                                               --
    //--------------------------------------------------------------------------
    /*************************************************************************** 
     * @ut.when a user attempts to insert, select, update, delete, and makes a
     * record available again through the Bank Codes screen.
     * 
     * @ut.then the operations are successfully performed.
     * 
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("BanksPane test");

        BoiBanksData l_boiBanksData = null;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_banksPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_BANK_CODE);
        SwingTestCaseTT.setTextComponentValue(i_nameField, C_BANK_NAME);
        doInsert(i_banksPane, i_updateButton);
        

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiBanksData = new BoiBanksData(new BankBankData(null, C_BANK_CODE, C_BANK_NAME, " "));
        SwingTestCaseTT.setKeyComboSelectedItem(i_banksPane, i_keyDataCombo, l_boiBanksData);


        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_nameField, C_NEW_BANK_NAME);
        doUpdate(i_banksPane, i_updateButton);
        

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_banksPane, i_deleteButton);
        

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_banksPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_BANK_CODE);
        doMakeAvailable(i_banksPane, i_updateButton);


        // *********************************************************************
        say("End of test.");
        // *********************************************************************
        endOfTest();
    }


    //--------------------------------------------------------------------------
    // Protected instance methods.                                            --
    //--------------------------------------------------------------------------
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteBankRecord(C_BANK_CODE);
    }

    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteBankRecord(C_BANK_CODE);
        say(":::End Of Test:::");
    }
    
    //--------------------------------------------------------------------------
    // Private instance methods.                                              --
    //--------------------------------------------------------------------------
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteBankRecord = "deleteBankRecord";
    /**
     * Removes a row from the BANK_BANK table.
     * 
     * @param p_bankCode the bank code.
     */
    private void deleteBankRecord(String p_bankCode)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = new String("DELETE FROM bank_bank " +
                           "WHERE bank_code = {0}");
        
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_bankCode);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteBankRecord);
    }
}
