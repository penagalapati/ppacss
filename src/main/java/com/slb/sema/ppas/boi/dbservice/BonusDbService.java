////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       BonusDbService.java
//  DATE            :       4th January 2007
//  AUTHOR          :       Michael Erskine
//  REFERENCE       :       PpacLon#2827/10751
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       Database service to serve the BOI Bonus pane.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE    | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
// dd/mm/yy| <name>     | <brief description of change>   | PpacLon#XXXX/YYYYY
//---------+------------+---------------------------------+--------------------
// 21/06/07| SJ Vonka   | Add wildcard elements to        | PpacLon#3184/11759
//         |            | promotion server.               | prd_ascs00_gen_ca_120
//---------+------------+---------------------------------+--------------------
// 21/07/07| S James    | Changes made to restrict service| PpacLon#3183/11851
//         |            | offerings drop down list to     | prd_ascs00_gen_ca_129
//         |            | contain available items         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiAccountGroupsData;
import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiBonusElementData;
import com.slb.sema.ppas.boi.boidata.BoiBonusSchemeData;
import com.slb.sema.ppas.boi.boidata.BoiChavChannelData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.AcgrAccountGroupSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BoneBonusElementSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BonsBonusSchemeSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ChavChannelSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SeofServiceOfferingSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAdjCodesSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database service to serve the BOI Bonus pane.
 */
public class BonusDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------
    /** Used by middleware. */
    private static final String C_CLASS_NAME = "BonusDbService";
    
    /** Identifies the misc_table column in srva_misc_codes DB table. */
    public static final String C_MISC_TABLE_ADJUSTMENT_TYPE = "ADT";

    /** Standard scheme type value */
    public static final char C_STANDARD_SCHEME_TYPE = 'S';

    /** Accumulated scheme type value */
    public static final char C_ACCUMULATED_SCHEME_TYPE = 'A';
    
    //-------------------------------------------------------------------------
    // Class Attributes
    //-------------------------------------------------------------------------
    
    /** Set to true to turn on debug, or false to turn it off. */
    private static boolean c_debug = false;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for account group code details. */
    private SrvaAdjCodesSqlService     i_adjCodesSqlService             = null;

    /** Vector of Adjustment Codes records. */
    private Vector                     i_availableAdjustmentCodesDataV = null;
    
    /** Vector of miscellaneous code records. */
    private Vector                     i_availableMiscCodeDataV;
    
    /** Vector of all available service offerings. */
    private Vector                     i_allServiceOfferingsV;
    
    /** SQL service for Misc Codes. */
    private MiscCodeSqlService         i_miscCodeSqlService;

    /** Operator's default market. */
    private BoiMarket                  i_defaultMarket = null;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService   i_operatorSqlService;
    
    /** SQL service for service offerings. */
    private SeofServiceOfferingSqlService i_seofServiceOfferingSqlService;
    
    /** SQL service for bonus schemes. */
    private BonsBonusSchemeSqlService  i_bonsSqlService = null;
    
    /** SQL service for bonus elements. */
    private BoneBonusElementSqlService i_boneSqlService = null;
    
    /** Vector of available bonus schemes. */
    private Vector                     i_availableBonusSchemesDataV = null;
    
    /** Vector of available bonus elements. */
    private Vector                     i_availableBonusElementsDataV = null;
    
    /** Current bonus scheme data being worked on. */
    private BoiBonusSchemeData         i_boiBonsData = null;
    
    /** Current bonus element data being worked on. */
    private BoiBonusElementData        i_boiBoneData = null;
    
    /** Currently selected adjustment code. */
    private BoiAdjCodesData            i_adjCodesData = null;

    /** Currently selected  code. Used as key for read and delete. */
    protected String                   i_currentBonusScheme = null;
    
    /** Current key elements being worked on. */
    private String                     i_currentKeyElements = null;
    
    /** Current non-key elements being worked on. */
    private String                     i_currentNonKeyElements = null;
    
    /** Currently selected adjustment type. */
    private String                     i_currentAdjustmentType = null;
    
    /** SQL service for channel code details. */
    private ChavChannelSqlService      i_channelSqlService = null;
    
    /** Vector of Tubs channel case records. */
    private Vector                     i_availableChannelDataV = null;
    
    /** TUBS Channel data. */
    private BoiChavChannelData         i_channelData = null;
    
    /** SQL service for account group code details. */
    private AcgrAccountGroupSqlService i_acgrSqlService = null;
    
    /** Vector of account group records. */
    private Vector                     i_availableAccountGroupDataV = null;

    /** The dataset of bons_bonus_scheme records */
    private BonsBonusSchemeDataSet     i_bonsDataSet = null;

    /** Vector of assigned service offering bits */
    private Vector                     i_assignedSeofBits = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public BonusDbService(BoiContext p_context)
    {
        super(p_context);
      
        i_bonsSqlService                = new BonsBonusSchemeSqlService(null);
        i_boneSqlService                = new BoneBonusElementSqlService(null);
        i_adjCodesSqlService            = new SrvaAdjCodesSqlService(null, null);
        i_channelSqlService             = new ChavChannelSqlService(null);
        i_availableChannelDataV         = new Vector(5);
        i_availableAdjustmentCodesDataV = new Vector(5);
        i_miscCodeSqlService            = new MiscCodeSqlService (null, null);
        i_availableMiscCodeDataV        = new Vector(5);
        i_operatorSqlService            = new BicsysUserfileSqlService(null, null);
        i_seofServiceOfferingSqlService = new SeofServiceOfferingSqlService(null, null);
        i_allServiceOfferingsV          = new Vector(5);
        
        i_availableBonusSchemesDataV    = new Vector(5);
        i_availableBonusElementsDataV   = new Vector(5);
        
        i_acgrSqlService                = new AcgrAccountGroupSqlService(null, null, null);
        i_availableAccountGroupDataV    = new Vector(5);
        i_assignedSeofBits              = new Vector(5);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current bonus scheme. Used for record lookup in database. 
     * @param p_bonusScheme Currently selected account group code. 
     */
    public void setCurrentBonusScheme(String p_bonusScheme)
    {
        i_currentBonusScheme = p_bonusScheme;
    }
    
    /**
     * Set current bonus key elements. Used for record lookup in database. 
     * @param p_elements Key elements in a comma separated String.
     */
    public void setCurrentKeyElements(String p_elements)
    {
        i_currentKeyElements = p_elements;
    }
    
    /**
     * Set current bonus non-key elements. Used for record lookup in database. 
     * @param p_elements Non-key elements in a comma separated String.
     */
    public void setCurrentNonKeyElements(String p_elements)
    {
        i_currentNonKeyElements = p_elements;
    }

    /** 
     * Return Bonus Scheme data currently being worked on. 
     * @return Account group data object.
     */
    public BoiBonusSchemeData getBonusSchemeData()
    {
        return i_boiBonsData;
    }
    
    /** 
     * Return Bonus Element data currently being worked on. 
     * @return Account group data object.
     */
    public BoiBonusElementData getBonusElementData()
    {
        if (c_debug) System.out.println("Entered getBonusElementData(): " + i_boiBoneData);
        return i_boiBoneData;
    }

    /** 
     * Set bonus scheme data currently being worked on.
     * @param p_bonusSchemeData Bonus scheme data object.
     */
    public void setBonusSchemeData(BoiBonusSchemeData p_bonusSchemeData)
    {
        i_boiBonsData = p_bonusSchemeData;
        
        if (c_debug)
        {
            System.out.println("Entered setBonusSchemeData.getDesc: " +
                               (i_boiBonsData == null ? "null" : i_boiBonsData.getInternalBonusSchemeData().getDesc()));
        }
    }
    
    /** 
     * Set bonus element data currently being worked on.
     * @param p_bonusElementData Bonus element data object.
     */
    public void setBonusElementData(BoiBonusElementData p_bonusElementData)
    {
        if (c_debug) System.out.println("Entered setBonusElementData(): " + p_bonusElementData);
        i_boiBoneData = p_bonusElementData;
    }

    /** 
     * Returns available bonus scheme data. 
     * @return Vector of available bonus scheme data records.
     */
    public Vector getAvailableBonusSchemeData()
    {
        return i_availableBonusSchemesDataV;
    }
    
    /** 
     * Returns available channel value data. 
     * @return Vector of available bonus scheme data records.
     */
    public Vector getAvailableChannelData()
    {
        return i_availableChannelDataV;
    }
    
    /** 
     * Returns available channel value data. 
     * @return Vector of available bonus scheme data records.
     */
    public Vector getAvailableAccountGroupData()
    {
        return i_availableAccountGroupDataV;
    }
    
    /** 
     * Returns available bonus scheme data. 
     * @return Vector of available bonus scheme data records.
     */
    public Vector getAvailableBonusElementData()
    {
        return i_availableBonusElementsDataV;
    }
    
    /**
     * Return account group data currently being worked on.
     * @return Account group data object.
     */
    public BoiAdjCodesData getAdjustmentCodesData()
    {
        return i_adjCodesData;
    }

    /**
     * Set account group data currently being worked on.
     * @param p_adjustmentCodesData Adustment codes data object.
     */
    public void setAdjustmentCodesData(BoiAdjCodesData p_adjustmentCodesData)
    {
        i_adjCodesData = p_adjustmentCodesData;
    }

    /**
     * Return account group data.
     * @return Vector of available account group data records.
     */
    public Vector getAvailableAdjustmentCodesDataV()
    {
        return i_availableAdjustmentCodesDataV;
    }
    
    /**
     * Gets the available adjustment types.
     * @return Available adjustment types.
     */
    public Vector getAvailableAdjTypesDataV()
    {
        return i_availableMiscCodeDataV;
    }
    
    /**
     * Sets the adjustment type currently being worked on.
     * @param p_miscCodeData The value to set.
     */
    public void setCurrentAdjType(String p_miscCodeData)
    {
        i_currentAdjustmentType = p_miscCodeData;
    }
    
    /**
     * Gets all the available service offerings.
     * @return All available service offerings.
     */
    public Vector getAllServiceOfferingsV()
    {
        return i_allServiceOfferingsV;
    }
    

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        if (c_debug) System.out.println("Entered BonusDbService.read()");
        BonsBonusSchemeDataSet l_bonsDataSet = null;
        l_bonsDataSet = i_bonsSqlService.readAll(null, p_connection);

        i_boiBonsData = new BoiBonusSchemeData(l_bonsDataSet.getRecord(i_currentBonusScheme));
        refreshServiceOfferingVector(p_connection);
        refreshBonusElementsDataVector(p_connection);
        
        SrvaAdjCodesDataSet l_adjCodesDataSet;
        String              l_adjustmentType;
        
        //TODO: These are null when selecting from the bonus scheme id combo box. 
        //l_adjustmentType = i_currentAdjustmentType.getInternalMiscCodeData().getCode();
        //
        //l_adjCodesDataSet = i_adjCodesSqlService.readAll(null, p_connection);
        //
        //i_adjCodesData = new BoiAdjCodesData(
        //                        l_adjCodesDataSet.getAdjustmentData(l_adjustmentType, i_currentAdjustmentCode));
        
        if (c_debug) System.out.println("Leaving BonusDbService.read()");
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        if (c_debug) System.out.println("Entered BonusDbService.readInitial");
        
        refreshBonusSchemesDataVector(p_connection, C_STANDARD_SCHEME_TYPE);
        refreshBonusElementsDataVector(p_connection);
        
        i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
        refreshAdjTypesDataVector(p_connection, i_defaultMarket.getMarket());
        refreshAdjustmentCodesDataVector(p_connection);
        refreshServiceOfferingVector(p_connection);
        refreshTubsChannelDataVector(p_connection);
        refreshAccountGroupDataVector(p_connection);
        
        if (c_debug) System.out.println("Leaving BonusDbService.readInitial");
    }

    /**
     * Updates bonus schemes in the database.
     * @param p_connection Database connection.
     */
	protected void update(JdbcConnection p_connection) throws PpasSqlException
    {   
        i_bonsSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_currentBonusScheme,                                
                                i_boiBonsData.getInternalBonusSchemeData().getDesc(),
                                i_boiBonsData.getInternalBonusSchemeData().getStartDate(),
                                i_boiBonsData.getInternalBonusSchemeData().getEndDate(),
                                i_boiBonsData.getInternalBonusSchemeData().isBonusActive()? 'Y':'N',
                                i_boiBonsData.getInternalBonusSchemeData().getOptinServOff()
                                );

        if(i_boiBonsData.getInternalBonusSchemeData().getSchemeType() == C_STANDARD_SCHEME_TYPE)
        {
            refreshBonusSchemesDataVector(p_connection, C_STANDARD_SCHEME_TYPE);
            refreshServiceOfferingVector(p_connection);
        }
        
	}
    
    private static final String C_METHOD_updateElementData = "updateElementData";
    /**
     * 
     * @param p_connection
     * @throws PpasSqlException
     */
    public void updateElementData() throws PpasServiceFailedException
    {   
        JdbcConnection  l_connection = null;
        boolean         l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();

            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_updateElementData,
                                          10040,
                                          this,
                                          null);
        
            BoneBonusElementData l_boneData = i_boiBoneData.getInternalBonusElementData();
            i_boneSqlService.update(null, l_connection, i_currentBonusScheme,
                                    l_boneData.getKeyElements(),
                                    l_boneData.getNonkeyElements(),
                                    l_boneData.getBonusAmount(),
                                    l_boneData.getBonusPercent(),
                                    l_boneData.getBonusAdjCode(),
                                    l_boneData.getBonusAdjType(),
                                    l_boneData.getDedAccId(),
                                    l_boneData.getDedAccExpDays(),
                                    l_boneData.isBonusActive()? 'Y':'N',
                                    i_context.getOperatorUsername());
        
        
            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_updateElementData,
                                              10050,
                                              this,
                                              null);
        
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_updateElementData,
                                        10060,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
            
            refreshBonusElementsDataVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_updateElementData,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /**
     * Inserts bonus schemes into the database.
     * @param p_connection Database connection.
     * @throws PpasSqlException
     */
	protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_bonsSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_currentBonusScheme,                                
                                i_boiBonsData.getInternalBonusSchemeData().getDesc(),
                                i_boiBonsData.getInternalBonusSchemeData().getStartDate(),
                                i_boiBonsData.getInternalBonusSchemeData().getEndDate(),
                                i_boiBonsData.getInternalBonusSchemeData().isBonusActive()? 'Y':'N',
                                i_boiBonsData.getInternalBonusSchemeData().getOptinServOff(),
                                i_boiBonsData.getInternalBonusSchemeData().getSchemeType()
                                );
        
        if(i_boiBonsData.getInternalBonusSchemeData().getSchemeType() == C_STANDARD_SCHEME_TYPE)
        {
            refreshBonusSchemesDataVector(p_connection, C_STANDARD_SCHEME_TYPE);
            refreshServiceOfferingVector(p_connection);
        }
        
	}
    
    /**
     * 
     * @throws PpasServiceFailedException
     */
    public void insertElementData() throws PpasServiceFailedException
    {
        JdbcConnection l_connection = null;
        boolean        l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();
            
            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_updateElementData,
                                          10040,
                                          this,
                                          null);
            
            BoneBonusElementData l_boneData = i_boiBoneData.getInternalBonusElementData();
            
            i_boneSqlService.insert(null, l_connection, i_currentBonusScheme,
                                    l_boneData.getKeyElements(),
                                    l_boneData.getNonkeyElements(),
                                    l_boneData.getBonusAmount(),
                                    l_boneData.getBonusPercent(),
                                    l_boneData.getBonusAdjCode(),
                                    l_boneData.getBonusAdjType(),
                                    l_boneData.getDedAccId(),
                                    l_boneData.getDedAccExpDays(),
                                    l_boneData.isBonusActive()? 'Y':'N',
                                    i_context.getOperatorUsername());
            
            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_updateElementData,
                                              10050,
                                              this,
                                              null);
        
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_updateElementData,
                                        10060,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
            
            refreshBonusElementsDataVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_updateElementData,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }

    /**
     * Deletes bonus schemes from the database.
     * @param p_connection Database connection.
     * @throws PpasSqlException
     */
	protected void delete(JdbcConnection p_connection) throws PpasSqlException
    {
        i_bonsSqlService.delete(null, p_connection, i_context.getOperatorUsername(), i_currentBonusScheme);
        
        refreshBonusSchemesDataVector(p_connection,
                                      i_boiBonsData.getInternalBonusSchemeData().getSchemeType());

        refreshServiceOfferingVector(p_connection);
	}
    
    /**
     * 
     * @param p_keyElements
     * @param p_nonKeyElements
     * @throws PpasServiceFailedException
     */
    public void deleteElementData(String p_keyElements,
                                  String p_nonKeyElements)
        throws PpasServiceFailedException
    {
        JdbcConnection l_connection = null;
        boolean        l_statementClosed = false;
        
        try
        {
            l_connection = i_context.getConnection();
            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_updateElementData,
                                          10040,
                                          this,
                                          null);
            i_boneSqlService.delete(null, l_connection, i_context.getOperatorUsername(),
                                    i_currentBonusScheme,
                                    p_keyElements,
                                    p_nonKeyElements);
            
            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_updateElementData,
                                              10050,
                                              this,
                                              null);
        
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_updateElementData,
                                        10060,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
            
            refreshBonusElementsDataVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_updateElementData,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
    }
    
    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        BonsBonusSchemeDataSet l_bonsDataSet        = null;
        BonsBonusSchemeData    l_dbBonusSchemeData = null;
        boolean[]               l_flagsArray         = new boolean[2];
        
        if (c_debug) System.out.println("Entered BonusDbService.checkForDuplicate()");
        
        l_bonsDataSet        = i_bonsSqlService.readAll(null, p_connection);

        l_dbBonusSchemeData = l_bonsDataSet.getRecord(i_currentBonusScheme);
            
        if (l_dbBonusSchemeData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbBonusSchemeData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }      
        
        if (c_debug) System.out.println("Leaving BonusDbService.checkForDuplicate()");

        return l_flagsArray;
    }
    
    /** Used in calls to middleware. */
    private static final String C_METHOD_checkForDuplicateElementRow = "checkForDuplicateElementRow";
    
    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @return boolean array: element[0] indicates if the record is a duplicate,
     *                        element[1] indicates if the record is withdrawn.
     * @throws PpasServiceFailedException
     */
    public boolean[] checkForDuplicateElementRow()
        throws PpasServiceFailedException
    {
        boolean         l_statementClosed = false;
        boolean[]       l_flagsArray = new boolean[2];
        JdbcConnection  l_connection = null;
        BoneBonusElementDataSet l_boneDataSet = null;
        BoneBonusElementData    l_dbBonusElementData = null;
        
        try
        {
            l_connection = i_context.getConnection();
            
            l_boneDataSet = i_boneSqlService.readAll(null, l_connection);

            l_dbBonusElementData = l_boneDataSet.getRecord(
                                       i_currentBonusScheme,
                                       i_currentKeyElements,
                                       i_currentNonKeyElements);
                
            if (l_dbBonusElementData != null)
            {
                l_flagsArray[C_DUPLICATE] = true;
                if (l_dbBonusElementData.isDeleted())
                {
                    l_flagsArray[C_WITHDRAWN] = true;
                }
                else
                {
                    l_flagsArray[C_WITHDRAWN] = false;
                }
            }
            else
            {
                l_flagsArray[C_DUPLICATE] = false;
            }      

            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_checkForDuplicateElementRow,
                                              10160,
                                              this,
                                              null);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_checkForDuplicateElementRow,
                               10170,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_bonsSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_currentBonusScheme,
                                         i_context.getOperatorUsername());

        // do a read to populate i_boiBonsData
        read(p_connection);
        
        refreshBonusSchemesDataVector(p_connection,
                                      i_boiBonsData.getInternalBonusSchemeData().getSchemeType());

    }
    
    /**
     * Marks BONE data as available.
     * @param p_keyElements
     * @param p_nonKeyElements
     * @throws PpasServiceFailedException
     */
    public void markAsAvailableElementData(String p_keyElements,
                                           String p_nonKeyElements)
        throws PpasServiceFailedException
    {
        JdbcConnection l_connection = null;
        boolean        l_statementClosed = false;
        if (c_debug) System.out.println("Entered BonusDbService.markAsAvailableElementData()");
        try
        {
            l_connection = i_context.getConnection();
            l_connection.startTransaction(C_CLASS_NAME,
                                          C_METHOD_updateElementData,
                                          10040,
                                          this,
                                          null);
            
            i_boneSqlService.markAsAvailable(null, l_connection,
                                             i_currentBonusScheme,
                                             i_context.getOperatorUsername(),
                                             p_keyElements,
                                             p_nonKeyElements);
            
            l_statementClosed = true;
            l_connection.getStatement().close(C_CLASS_NAME,
                                              C_METHOD_updateElementData,
                                              10050,
                                              this,
                                              null);
        
            l_connection.endTransaction(C_CLASS_NAME,
                                        C_METHOD_updateElementData,
                                        10060,
                                        this,
                                        null,
                                        JdbcConnection.C_FLAG_COMMIT);
            
            refreshBonusElementsDataVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_updateElementData,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               l_statementClosed);
        }
        if (c_debug) System.out.println("MIE Leaving BonusDbService.markAsAvailableElementData()");
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available bonus schemes data vector from the 
     * BONS_BONUS_SCHEME table.
     * @param p_connection Database connection.
     * @param p_schemeType Only populate the data vector with record of this scheme type 
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void refreshBonusSchemesDataVector(JdbcConnection p_connection, char p_schemeType)
        throws PpasSqlException
    {   
        BonsBonusSchemeData[]  l_bonsArray   = null;
        
        i_availableBonusSchemesDataV.removeAllElements();
        
        i_bonsDataSet = i_bonsSqlService.readAll(null, p_connection);
        l_bonsArray = i_bonsDataSet.getAvailableArrayForType(p_schemeType);
        
        i_availableBonusSchemesDataV.addElement(new String(""));
        i_availableBonusSchemesDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_bonsArray.length; i++)
        {
            i_availableBonusSchemesDataV.addElement(new BoiBonusSchemeData(l_bonsArray[i]));
        }
    }
    
    /** 
     * Refreshes the available bonus elements data vector from the 
     * BONE_BONUS_ELEMENTS table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshBonusElementsDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {   
        BoneBonusElementDataSet l_boneDataSet = null;
        BoneBonusElementData[]  l_boneArray   = null;
        if (c_debug) System.out.println("Entered refreshBonusElementsDataVector()");
        
        i_availableBonusElementsDataV.removeAllElements();
        
        l_boneDataSet = i_boneSqlService.readAll(null, p_connection);
        
        if (i_currentBonusScheme == null)
        {
            l_boneArray = l_boneDataSet.getAvailableArray();
        }
        else
        {
            l_boneArray = l_boneDataSet.getAvailableArrayWithId(i_currentBonusScheme);
        }
        
        i_availableBonusElementsDataV.addElement(new String(""));
        i_availableBonusElementsDataV.addElement(new String("NEW ELEMENT"));
        for (int i=0; i < l_boneArray.length; i++)
        {
            i_availableBonusElementsDataV.addElement(new BoiBonusElementData(l_boneArray[i]));
        }
        
        if (c_debug) System.out.println("Leaving refreshBonusElementsDataVector()");
    }
    
    /** Used for calls to middleware. */
    private static final String C_METHOD_refreshAdjCodesDataVector = "refreshAdjCodesDataVector";
    /**
     * 
     * @throws PpasServiceFailedException
     */
    public void refreshAdjustmentCodesDataVector() throws PpasServiceFailedException
    {
        JdbcConnection l_connection = i_context.getConnection();
        
        try
        {
            refreshAdjustmentCodesDataVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_refreshAdjCodesDataVector,
                               10070,
                               this,
                               l_psqlE,
                               l_connection,
                               true);
        }
    }
    
    /**
     * Refreshes the available account group data vector from the srva_adj_codes table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception
     */
    private void refreshAdjustmentCodesDataVector(JdbcConnection p_connection) throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_adjCodesDataSet = null;
        Vector l_adjCodesArray = null;
        if (c_debug) System.out.println("Entered BonusDbService.refreshAdjustmentCodesVector()");
 
        i_availableAdjustmentCodesDataV.removeAllElements();
        
        if (i_currentAdjustmentType != null)
        { 
            l_adjCodesDataSet = i_adjCodesSqlService.readAll(null, p_connection);
            
            l_adjCodesArray = l_adjCodesDataSet.getAdjCodeData(i_defaultMarket.getMarket(),
                                                               i_currentAdjustmentType,
                                                               true);
        }

        i_availableAdjustmentCodesDataV.addElement("");
        
        for (int i = 0; (l_adjCodesArray != null && i < l_adjCodesArray.size()); i++)
        {
            i_availableAdjustmentCodesDataV.addElement(
                new BoiAdjCodesData((SrvaAdjCodesData)l_adjCodesArray.elementAt(i)));
        }
        if (c_debug) System.out.println("Leaving BonusDbService.refreshAdjustmentCodesVector()");
    }
    
    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshAdjTypesDataVector(JdbcConnection p_connection,
                                           Market         p_market)
    throws PpasSqlException
    {
        MiscCodeDataSet   l_miscCodeDataSet;
        MiscCodeDataSet   l_availableMiscCodeDataSet;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;

        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet          = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getAvailableMiscCodeData(
                                         p_market,
                                         C_MISC_TABLE_ADJUSTMENT_TYPE);
        
        l_miscCodeV        = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData        = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        i_availableMiscCodeDataV.addElement("");
        
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableMiscCodeDataV.addElement(l_boiMiscCodeArray[j]);
        }
    }
    
    /**
     * Refreshes the available channel data vector from the 
     * CHAV_CHANNEL_VALUES table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshTubsChannelDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        ChavChannelDataSet l_DataSet = null;
        ChavChannelData[]  l_array   = null;
        
        i_availableChannelDataV.removeAllElements();
        
        l_DataSet = i_channelSqlService.readAll(null, p_connection);
        l_array   = l_DataSet.getAvailableArray();
        
        i_availableChannelDataV.addElement("");
        // Sally
        i_availableChannelDataV.addElement("*");    // wildcard
        
        for (int i=0; i < l_array.length; i++)
        {
            i_availableChannelDataV.addElement(new BoiChavChannelData(l_array[i]));
        }
    }
    
    /** 
     * Refreshes the available account group data vector from the 
     * acgr_account_group table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshAccountGroupDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        AcgrAccountGroupDataSet l_acgrDataSet = null;
        AcgrAccountGroupData[]  l_acgrArray   = null;
        
        i_availableAccountGroupDataV.removeAllElements();
        
        l_acgrDataSet = i_acgrSqlService.readAll(null, p_connection);
        l_acgrArray   = l_acgrDataSet.getAvailableArray();
        
        i_availableAccountGroupDataV.addElement("");
        // Sally
        i_availableAccountGroupDataV.addElement("*");    // wildcard
        
        for (int i=0; i < l_acgrArray.length; i++)
        {
            i_availableAccountGroupDataV.addElement(new BoiAccountGroupsData(l_acgrArray[i]));
        }
    }
    
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
    
    /** 
     * Stores a Vector of Service Offerings currently assigned to all available Bonus Schemes. 
     * This is used to determine which Service Offerings are available to load into the drop down list
     * on the Bonus screens.
     */
    protected void refreshAssignedSeofBits()
    {
        BonsBonusSchemeData[] l_bonsArray   = i_bonsDataSet.getAvailableArray();
        Long                  l_serviceOfferingBit = null;
        
        i_assignedSeofBits.removeAllElements();
        // store the currently assigned service offering bits
        for(int i=0; i < l_bonsArray.length; i++)
        {
            l_serviceOfferingBit = l_bonsArray[i].getOptinServOff();

            if (l_serviceOfferingBit != null)
            {
                i_assignedSeofBits.addElement(l_serviceOfferingBit);
            }
        }
        if (c_debug) System.out.println("assigned bits vector is "+i_assignedSeofBits.toString());
        return;
    }

    /** Used for calls to middleware. */
    private static final String C_METHOD_refreshServiceOfferingVector = "refreshServiceOfferingVector";
    /**
     * Updates the Service Offerings Vector with Service Offerings that are currently available for
     * selection by the user for assigning to Bonus Schemes.
     * @throws PpasServiceFailedException
     */
    public void refreshServiceOfferingVector() throws PpasServiceFailedException
    {
        JdbcConnection l_connection = i_context.getConnection();
        
        try
        {
            refreshServiceOfferingVector(l_connection);
        }
        catch (PpasSqlException l_psqlE)
        {
            handleSqlException(C_CLASS_NAME,
                               C_METHOD_refreshServiceOfferingVector,
                               10075,
                               this,
                               l_psqlE,
                               l_connection,
                               true);
        }
        return;
    }

    /**
     * Updates the Service Offerings Vector with Service Offerings that are currently available for
     * selection by the user for assigning to Bonus Schemes.
     * @param p_connection The database connection
     * @throws PpasSqlException
     */
    protected void refreshServiceOfferingVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        SeofServiceOfferingDataSet l_seofDataSet    = null;
        SeofServiceOfferingData[]  l_seofDataArray  = null;
        int                        l_seofNumber;
        Long                       l_seofBit        = null;
        Long                       l_currentSeofBit = null;

        refreshAssignedSeofBits();
        
        // get the currently selected bonus scheme's Service Offering if possible
        if(i_boiBonsData != null)
        {
            l_currentSeofBit = i_boiBonsData.getInternalBonusSchemeData().getOptinServOff();
        }
        l_seofDataSet = i_seofServiceOfferingSqlService.readAll(null, p_connection);
        l_seofDataArray = l_seofDataSet.getAllArray();
        
        i_allServiceOfferingsV.removeAllElements();
        i_allServiceOfferingsV.addElement("");
        
        for (int i=0; i<l_seofDataArray.length; i++)
        {
            l_seofNumber = l_seofDataArray[i].getServiceOfferingNumber();
            l_seofBit = Long.valueOf(String.valueOf(1 << (l_seofNumber - 1)));
            // ensure that available service offerings and the scheme's current service offering
            // are added to the vector
            if((l_currentSeofBit != null && l_currentSeofBit.equals(l_seofBit)) ||
                    !i_assignedSeofBits.contains(l_seofBit))
            {
                i_allServiceOfferingsV.add(new BoiServiceOfferingData(l_seofDataArray[i]));
            }
        }
        
        return;

    }
}
