////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiHelpTopics.java
//    DATE            :       12-Apr-2005
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PRD_ASCS00_DEV_SS_88
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Container for topics for BOI Help widgets.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | The basic help topics for the   |PpacLon#1755/7279
//          |            | Bank Codes screen are added.    |
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | The basic help topics for the   |PpacLon#1755/7425
//          |            | Faf Charging Indicators screen  |
//          |            | are added.                      |
//----------+------------+---------------------------------+--------------------
// 17/11/05 | Yang L.    | The basic help topics for the   |PpacLon#1755/7463
//          |            | SDP Identifiers screen are added|
//----------+------------+---------------------------------+--------------------
// 21/11/05 | Marianne T.| The basic help topics for the   |PpacLon#1755/7473
//          |            | AdjustmentType screen are added |
//----------+------------+---------------------------------+--------------------
// 22/12/05 | Lars L.    | The basic help topics for the   |PpacLon#1755/7279
//          |            | Fast Adjustment Codes screen    |
//          |            | are added.                      |
//----------+------------+---------------------------------+--------------------
// 17/01/05 | David B.   | Add screen specific help        | PpacLon#1911/7785
//          |            | topics for:                     |
//          |            | * Adjustment Codes              |
//          |            | * Adjustment Types              |
//          |            | * Bank Codes                    |
//          |            | * Fast Adjustment Codes         |
//          |            | * Agent Info         |
//          |            | * FaF Charging Indicators       |
//          |            | * Faf Default CIs               |
//          |            | * SDP ID's                      |
//          |            | Also, change help files from    |
//          |            | mixed to all lower case.        |
//----------+------------+---------------------------------+--------------------
// 01/02/06 | M Erskine  | Fix broken links and reload all | PpacLon#1969/7887
//          |            | help topics.                    |
//----------+------------+---------------------------------+--------------------
// 22/12/06 | M.T�rnqvist| Added TUBS Channel constant     | PpacLon#2822/10659
//----------+------------+---------------------------------+--------------------
// 14/12/06 | M Erskine  | Add Bonus help topics.          | PpacLon#2827/10668
//----------+------------+---------------------------------+--------------------
// 14/12/06 | M Erskine  | Add Bonus and Channels help     | PpacLon#2827/10775
//          |            | topic files.                    |
//----------+------------+---------------------------------+--------------------
// 26/11/07 | J E Aberg  | Fixed links for "About Acc.     | PpacLon#3489/12508
//          |            | Bonus Schemes" & "Change a Bonus|
//	    |            | Scheme"                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.util.HashMap;

/** Container for topics for BOI Help widgets. */
public class BoiHelpTopics
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------
    
    //
    // Constants for help topics
    //
    
    /** Constant for BOI help topics. */
    public final static String C_HOW_DO_I = "How do I ......";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_ADJUSTMENT_CODES = "About Adjustment Codes";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_ADJUSTMENT_CODES = "View Adjustment Codes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_AN_ADJUSTMENT_CODE = "Add an Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_AN_ADJUSTMENT_CODE = "Change an Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_AN_ADJUSTMENT_CODE = "Delete an Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_AN_ADJUSTMENT_CODE = "Reinstate an Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_ADJUSTMENT_TYPES = "About Adjustment Types";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_ADJUSTMENT_TYPES = "View Adjustment Types";
    /** Constant for BOI help topics. */
    public final static String C_ADD_AN_ADJUSTMENT_TYPE = "Add an Adjustment Type";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_AN_ADJUSTMENT_TYPE = "Change an Adjustment Type";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_AN_ADJUSTMENT_TYPE = "Delete an Adjustment Type";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_AN_ADJUSTMENT_TYPE = "Reinstate an Adjustment Type";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_BANKS = "About Bank Codes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_BANK_CODE = "Add a Bank Code";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_BANK_CODE = "Change a Bank Code";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_BANK_CODE = "Delete a Bank Code";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_A_BANK_CODE = "Reinstate a Bank Code";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_FAST_ADJUSTMENT_CODES = "About Fast Adjustment Codes";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_FAST_ADJUSTMENT_CODES = "View Fast Adjustment Codes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_FAST_ADJUSTMENT_CODE = "Add a Fast Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_FAST_ADJUSTMENT_CODE = "Change a Fast Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_FAST_ADJUSTMENT_CODE = "Delete a Fast Adjustment Code";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_AGENT_INFORMATION = "About Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_AGENT_INFORMATION = "View Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_ADD_AGENT_INFORMATION = "Add Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_AGENT_INFORMATION = "Change Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_AGENT_INFORMATION = "Delete Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_AGENT_INFORMATION = "Reinstate Agent Information";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_SDP_IDENTIFIERS = "About SDP Identifies";
    /** Constant for BOI help topics. */
    public final static String C_ADD_AN_SDP_IDENTIFIER = "Add an SDP Identifier";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_AN_SDP_IDENTIFIER = "Change an SDP Identifier";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_AN_SDP_IDENTIFIER = "Delete an SDP Identifier";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_AN_SDP_IDENTIFIER = "Reinstate an SDP Identifier";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_FAF_CHARGING_INDICATORS = 
                                                               "About Family and Friends Charging Indicators";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_CHARGING_INDICATOR = "Add a Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_CHARGING_INDICATOR = "Change a Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_CHARGING_INDICATOR = "Delete a Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_A_CHARGING_INDICATOR = "Reinstate a Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_FAF_DEFAULT_CHARGING_INDICATORS = 
                                                       "About Family and Friends Default Charging Indicators";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_DEFAULT_CHARGING_INDICATOR = "Add a Default Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_DEFAULT_CHARGING_INDICATOR = "Change a Default Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_DEFAULT_CHARGING_INDICATOR = "Delete a Default Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_A_DEFAULT_CHARGING_INDICATOR = 
                                                                     "Reinstate a Default Charging Indicator";
    /** Constant for BOI help topics. */
    public final static String C_USER_GUIDE_REFERENCES = "User Guide References";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_ASCS = "About ASCS";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_THE_BOI = "About the Business Operations Interface";
    /** Constant for BOI help topics. */
    public final static String C_LOG_IN_TO_BOI = "Log in to BOI";
    /** Constant for BOI help topics. */
    public final static String C_LOG_OUT_OF_BOI = "Log out of BOI";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_PASSWORDS = "About Passwords";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_MY_PASSWORD = "Change my password";
    /** Constant for BOI help topics. */
    public final static String C_SET_THE_BOI_PASSWORD_STRENGTH = "Set the BOI password strength";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_HELP = "About Help";
    /** Constant for BOI help topics. */
    public final static String C_USE_FULL_HELP = "Use full help";
    /** Constant for BOI help topics. */
    public final static String C_USE_CONTEXT_SENSITIVE_HELP = "Use context-sensitive help";
    /** Constant for BOI help topics. */
    public final static String C_USE_THE_HELP_INDEX = "Use the help index";
    /** Constant for BOI help topics. */
    public final static String C_SEARCH_THE_HELP_SYSTEM = "Search the help system";
    /** Constant for BOI help topics. */
    public final static String C_PRINT_HELP = "Print help";
    /** Constant for BOI help topics. */
    public final static String C_BUSINESS_CONFIGURATION = "Business Configuration";
    /** Constant for BOI help topics. */
    public final static String C_USE_BC_SCREENS = "Use the Business Configuration screens";
    /** Constant for BOI help topics. */
    public final static String C_OPERATOR_CONFIGURATION = "About Operator Configuration";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_SET_OF_OPERATOR_DETAILS = "View a set of Operator details";
    /** Constant for BOI help topics. */
    public final static String C_ADD_SET_OF_OPERATOR_DETAILS = "Add a set of Operator details";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_SET_OF_OPERATOR_DETAILS = "Change a set of Operator details";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_SET_OF_OPERATOR_DETAILS = "Delete a set of Operator details";
    /** Constant for BOI help topics. */
    public final static String C_UNBAR_AN_OPERATOR = "Unbar an Operator";
    /** Constant for BOI help topics. */
    public final static String C_CUSTOMER_CARE_ACCESS = "About Customer Care Access";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_SET_OF_PRIVILEGES = "View a set of Privileges";
    /** Constant for BOI help topics. */
    public final static String C_ADD_SET_OF_PRIVILEGES = "Add a set of Privileges";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_SET_OF_PRIVILEGES = "Change a set of Privileges";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_SET_OF_PRIVILEGES = "Delete a set of Privileges";
    /** Constant for BOI help topics. */
    public final static String C_APPLICATION_ACCESS = "About Application Access";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_APPLICATION_ACCESS = "View Application Access privileges";
    /** Constant for BOI help topics. */
    public final static String C_SET_APPLICATION_ACCESS = "Set Application Access privileges";
    /** Constant for BOI help topics. */
    public final static String C_ACCOUNT_GROUP_IDENTITIES = "Account Group Identities";
    /** Constant for BOI help topics. */
    public final static String C_ADD_ACCOUNT_GROUP = "Add an Account Group";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_ACCOUNT_GROUP = "Change an Account Group";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_ACCOUNT_GROUP = "Delete an Account Group";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_ACCOUNT_GROUP = "Reinstate an Account Group";
    /** Constant for BOI help topics. */
    public final static String C_ACCUMULATORS = "Accumulators";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_ACCUMULATOR_DETAILS = "View Accumulator details";
    /** Constant for BOI help topics. */
    public final static String C_ADD_ACCUMULATOR_DETAILS = "Add Accumulator details";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_ACCUMULATOR_DETAILS = "Change Accumulator details";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_ACCUMULATOR_DETAILS = "Delete Accumulator details";
    /** Constant for BOI help topics. */
    public final static String C_COMMUNITY_CHARGING = "About Community Charging";
    /** Constant for BOI help topics. */
    public final static String C_ADD_COMM_CHARGING_IDENTITY = "Add a Community Charging Identity";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_COMM_CHARGING_IDENTITY = "Change a Community Charging Identity";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_COMM_CHARGING_IDENTITY = "Delete a Community Charging Identity";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_COMM_CHARGING_IDENTITY = "Reinstate a Community Charging Identity";
    /** Constant for BOI help topics. */
    public final static String C_DEDICATED_ACCOUNTS = "About Dedicated Accounts";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_DED_ACCOUNT_DETAILS = "View Dedicated Account details";
    /** Constant for BOI help topics. */
    public final static String C_ADD_DED_ACCOUNT_DETAILS = "Add Dedicated Account details";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_DED_ACCOUNT_DETAILS = "Change Dedicated Account details";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_DED_ACCOUNT_DETAILS = "Delete Dedicated Account details";
    /** Constant for BOI help topics. */
    public final static String C_FAF_BARRED_NUMBERS = "About Family and Friends Barred Numbers";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_BARRED_NUMBER_DEFN = "View a Barred Number definition";
    /** Constant for BOI help topics. */
    public final static String C_ADD_BARRED_NUMBER_DEFN = "Add a Barred Number definition";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_BARRED_NUMBER_DEFN = "Change a Barred Number definition";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_BARRED_NUMBER_DEFN = "Delete a Barred Number definition";
    /** Constant for BOI help topics. */
    public final static String C_SERVICE_CLASS_DETAILS = "Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_SERVICE_CLASS_DETAILS = "View Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_ADD_SERVICE_CLASS_DETAILS = "Add Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_SERVICE_CLASS_DETAILS = "Change Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_SERVICE_CLASS_DETAILS = "Delete Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_SERVICE_CLASS_DETAILS = "Reinstate Service Class Details";
    /** Constant for BOI help topics. */
    public final static String C_SERVICE_OFFERINGS = "About Service Offerings";
    /** Constant for BOI help topics. */
    public final static String C_DEFINE_SERVICE_OFFERING = "Define a Service Offering";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_SERVICE_OFFERING = "Change a Service Offering";
    /** Constant for BOI help topics. */
    public final static String C_NUMBER_BLOCKS = "About Number Blocks";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_NUMBER_BLOCKS = "View Number Blocks";
    /** Constant for BOI help topics. */
    public final static String C_ADD_NUMBER_BLOCK = "Add a Number Block";
    /** Constant for BOI help topics. */
    public final static String C_CACHE_RELOAD = "About Cache Reload";
    /** Constant for BOI help topics. */
    public final static String C_INITIATE_CONFIG_CACHE_RELOAD = "Initiate Config Cache Reload";
    /** Constant for BOI help topics. */
    public final static String C_INITIATE_FEATURE_CACHE_RELOAD = "Initiate Feature Cache Reload";
    /** Constant for BOI help topics. */
    public final static String C_BATCH_JOBS = "Batch Jobs";
    /** Constant for BOI help topics. */
    public final static String C_BATCH_CONTROL = "About Batch Control";
    /** Constant for BOI help topics. */
    public final static String C_SUBMIT_BATCH_JOB = "Submit a batch job";
    /** Constant for BOI help topics. */
    public final static String C_REVIEW_SUBMITTED_BATCH_JOBS = "Review previously submitted batch jobs";
    /** Constant for BOI help topics. */
    public final static String C_STOP_PAUSE_RESUME_BATCH_JOB = "Stop, pause or resume a batch job";
    /** Constant for BOI help topics. */
    public final static String C_RECOVER_BATCH_JOB = "Recover a failed or stopped batch job";
    /** Constant for BOI help topics. */
    public final static String C_CHECK_STATUS_OF_BATCH_JOB = "Check the status of a completed batch job";
    /** Constant for BOI help topics. */
    public final static String C_RESCHEDULE_BATCH_JOB = "Reschedule a batch job";
    /** Constant for BOI help topics. */
    public final static String C_MSISDN_GENERATION = "About MSISDN Generation";
    /** Constant for BOI help topics. */
    public final static String C_GENERATE_MSISDNS = "Generate MSISDNs";
    /** Constant for BOI help topics. */
    public final static String C_PURGE_AND_ARCHIVE = "About Purge & Archive";
    /** Constant for BOI help topics. */
    public final static String C_SUBMIT_PURGE_JOB = "Submit a purge job";
    /** Constant for BOI help topics. */
    public final static String C_REVIEW_SUBMITTED_PURGE_JOBS = "Review previously submitted purge jobs";
    /** Constant for BOI help topics. */
    public final static String C_RECOVER_PURGE_JOB = "Recover a failed or stopped purge job";
    /** Constant for BOI help topics. */
    public final static String C_STOP_PAUSE_RESUME_PURGE_JOB = "Stop, pause or resume a purge job";
    /** Constant for BOI help topics. */
    public final static String C_RESCHEDULE_PURGE_JOB = "Reschedule a purge job";
    /** Constant for BOI help topics. */
    public final static String C_REPORTS = "About Reports";
    /** Constant for BOI help topics. */
    public final static String C_SUBMIT_A_REPORT = "Submit a report";
    /** Constant for BOI help topics. */
    public final static String C_VIEW_REPORT = "View a report";
    /** Constant for BOI help topics. */
    public final static String C_RESCHEDULE_REPORT = "Reschedule a report";
    /** Constant for BOI help topics. */
    public final static String C_BALANCE_SDPS = "About Balance SDPs";
    /** Constant for BOI help topics. */
    public final static String C_GENERATE_AN_SDP_BALANCING_FILE = "Generate an SDP Balancing File";
    /** Constant for BOI help topics. */
    public final static String C_REGION_IDENTITIES = "About Regions";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_REGION = "Add a Region";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_REGION = "Change a Region";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_REGION = "Delete a Region";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_A_REGION = "Reinstate a Region";
    /** Constant for BOI help topics. */
    public final static String C_MARKETS = "About Markets";
    /** Constant for BOI help topics. */
    public final static String C_ADD_A_MARKET = "Add a Market";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_A_MARKET = "Change a Market";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_A_MARKET = "Delete a Market";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_A_MARKET = "Reinstate a Market";
    /** Constant for BOI help topics. */
    public final static String C_MAKE_BATCH_JOB_PERIODIC = "Make a batch job periodic";
    /** Constant for BOI help topics. */
    public final static String C_USE_ORACLE_ENTERPRISE_MANAGER = "Use Oracle Enterprise Manager";
    /** Constant for BOI help topics. */
    public final static String C_MAKE_PURGE_AND_ARCHIVE_JOB_PERIODIC = "Make a purge and archive job periodic";
    /** Constant for BOI help topics. */
    public final static String C_MAKE_REPORT_PERIODIC = "Make a report periodic";
    /** Constant for BOI help topics. */
    public final static String C_STOP_PAUSE_RESUME_REPORT_JOB = "Stop, pause or resume a report job";
    /** Constant for BOI help topics. */
    public final static String C_ADD_TELESERVICE_CODES = "Add Teleservice Codes";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_TELESERVICE_CODES = "Change Teleservice Codes";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_TELESERVICE_CODES = "Delete Teleservice Codes";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_TELESERVICE_CODES = "Reinstate Teleservice Codes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_TRAFFIC_CASES = "Add Traffic Cases";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_TRAFFIC_CASES = "Change Traffic Cases";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_TRAFFIC_CASES = "Delete Traffic Cases";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_TRAFFIC_CASES = "Reinstate Traffic Cases";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_BONUS_SCHEMES = "About Bonus Schemes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_BONUS_SCHEME = "Add a Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_BONUS_SCHEME = "Change a Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_BONUS_SCHEME = "Delete a Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_BONUS_SCHEME = "Reinstate a Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_ADD_BONUS_ELEMENT = "Add a Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_BONUS_ELEMENT = "Change a Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_BONUS_ELEMENT = "Delete a Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_BONUS_ELEMENT = "Reinstate a Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_CHANNELS = "About Channels";
    /** Constant for BOI help topics. */
    public final static String C_ADD_CHANNEL = "Add a Channel";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_CHANNEL = "Change a Channel";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_CHANNEL = "Delete a Channel";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_CHANNEL = "Reinstate a Channel";
    /** Constant for BOI help topics. */
    public final static String C_ABOUT_ACCUMULATED_BONUS_SCHEMES = "About Accumulated Bonus Schemes";
    /** Constant for BOI help topics. */
    public final static String C_ADD_ACCUMULATED_BONUS_SCHEME = "Add an Accumulated Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_ACCUMULATED_BONUS_SCHEME = "Change an Accumulated Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_ACCUMULATED_BONUS_SCHEME = "Delete an Accumulated Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_ACCUMULATED_BONUS_SCHEME = "Reinstate an Accumulated Bonus Scheme";
    /** Constant for BOI help topics. */
    public final static String C_ADD_ACCUMULATED_BONUS_ELEMENT = "Add an Accumulated Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_CHANGE_ACCUMULATED_BONUS_ELEMENT = "Change an Accumulated Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_DELETE_ACCUMULATED_BONUS_ELEMENT = "Delete an Accumulated Bonus Element";
    /** Constant for BOI help topics. */
    public final static String C_REINSTATE_ACCUMULATED_BONUS_ELEMENT = "Reinstate an Accumulated Bonus Element";
    
    //
    // Constants for BOI screens
    //
    
    /** Constant to identify Account Groups screen. */
    public final static int C_ACCOUNT_GROUPS_SCREEN = 1;
    
    /** Constant to identify Accumulators screen. */
    public final static int C_ACCUMULATORS_SCREEN = 2;
    
    /** Constant to identify Application Access Privileges screen. */
    public final static int C_APP_ACCESS_PRIV_SCREEN = 3;
    
    /** Constant to identify Batch Control screen. */
    public final static int C_BATCH_CONTROL_SCREEN = 4;
    
    /** Constant to identify Cache Reload screen. */
    public final static int C_CACHE_RELOAD_SCREEN = 5;
    
    /** Constant to identify Customer Care Access Privileges screen. */
    public final static int C_CC_ACCESS_PRIV_SCREEN = 6;
    
    /** Constant to identify Community Charging screen. */
    public final static int C_COMM_CHARGING_SCREEN = 7;
    
    /** Constant to identify Dedicated Accounts screen. */
    public final static int C_DED_ACCOUNTS_SCREEN = 8;
    
    /** Constant to identify Friends and Family screen. */
    public final static int C_FAF_SCREEN = 9;
    
    /** Constant to identify Login screen. */
    public final static int C_LOGIN_SCREEN = 10;
    
    /** Constant to identify MSISDN Generation screen. */
    public final static int C_MSISDN_GENERATION_SCREEN = 11;
    
    /** Constant to identify Number Blocks screen. */
    public final static int C_NUMBER_BLOCKS_SCREEN = 12;
    
    /** Constant to identify Operators screen. */
    public final static int C_OPERATORS_SCREEN = 13;
    
    /** Constant to identify Purge and Archive screen. */
    public final static int C_PURGE_AND_ARCHIVE_SCREEN = 14;
    
    /** Constant to identify Reports screen. */
    public final static int C_REPORTS_SCREEN = 15;
    
    /** Constant to identify Service Class Details screen. */
    public final static int C_SERVICE_CLASS_SCREEN = 16;
    
    /** Constant to identify Service Offerings screen. */
    public final static int C_SERVICE_OFFERINGS_SCREEN = 17;
    
    /** Constant to identify Regions screen. */
    public final static int C_REGIONS_SCREEN = 18;
    
    /** Constant to identify Markets screen. */
    public final static int C_MARKETS_SCREEN = 19;
    
    /** Constant to identify Balance SDPs screen. */
    public final static int C_BALANCE_SDPS_SCREEN = 20;
    
    /** Constant to identify Bank Codes screen. */
    public final static int C_BANK_CODES_SCREEN = 21;

    /** Constant to identify Family and Friends Charging Indicators screen. */
    public final static int C_FAF_CHARGING_IND_SCREEN = 22;
    
    /** Constant to identify Family and Friends Default Charging Indicators screen. */
    public final static int C_FAF_DEFAULT_CHARGING_IND_SCREEN = 23;
    
    /** Constant to identify SDP Identifiers screen. */
    public final static int C_SDP_IDENTIFIERS_SCREEN = 24;
    
    /** Constant to identify Change Password screen. */
    public final static int C_CHANGE_PASSWORD_SCREEN = 25;

    /** Constant to identify Adjustment Type screen. */
    public final static int C_ADJUSTMENT_TYPE_SCREEN = 26;

    /** Constant to identify Adjsutment Codes screen. */
    public final static int C_ADJUSTMENT_CODES_SCREEN = 27;
    
    /** Constant to identify Fast Adjustment Codes screen. */
    public final static int C_FAST_ADJUSTMENT_CODES_SCREEN = 28;
    
    /** Constant to identify Fast Adjustment Codes screen. */
    public final static int C_AGENT_INFORMATION = 29;
    
    /** Constant to identify Teleservice Codes Details screen. */
    public final static int C_TELESERVICE_CODES_SCREEN = 30;
    
    /** Constant to identify Traffic Cases Details screen. */
    public final static int C_TRAFFIC_CASES_SCREEN = 31;
    
    /** Constant to identify TUBS Channel Codes screen. */
    public final static int C_TUBS_CHANNEL_GROUPS_SCREEN = 32;
    
    /** Constant to identify Bonus screen. */
    public final static int C_BONUS_SCREEN = 33;
    
    /** Constant to identify Accumulated Bonus screen. */
    public final static int C_ACCUMULATED_BONUS_SCREEN = 34;

    //
    // Constants for help pages
    //
    
    /** Constant for general BOI help page. */
    public final static String C_GENERAL_BOI_HELP_PAGE = "boihelp.htm";
    
    /** Constant for default BOI help page. */
    public final static String C_DEFAULT_BOI_HELP_PAGE = "_blank.htm";
        
    //-------------------------------------------------------------------------
    // Class attributes
    //-------------------------------------------------------------------------
    
    /** Hashmap holding help topic to help page mapping. */
    private static HashMap i_helpPagesMap;
    
    /** Default list of help topics. */
    private static Object[] i_defaultHelpTopics;
    
    /** List of help topics for Account Groups screen. */
    private static Object[] i_accountGroupsHelpTopics;
    
    /** List of help topics for Accumulators screen. */
    private static Object[] i_accumulatorsHelpTopics;
    
    /** List of help topics for Application Access Privileges screen. */
    private static Object[] i_appAccessPrivHelpTopics;
    
    /** List of help topics for Batch Control screen. */
    private static Object[] i_batchControlHelpTopics;
    
    /** List of help topics for Cache Reload screen. */
    private static Object[] i_cacheReloadHelpTopics;
    
    /** List of help topics for Customer Care Access Privileges screen. */
    private static Object[] i_ccAccessPrivHelpTopics;
    
    /** List of help topics for Community Charging screen. */
    private static Object[] i_commChargingHelpTopics;
    
    /** List of help topics for Dedicated Accounts screen. */
    private static Object[] i_dedAccountsHelpTopics;
    
    /** List of help topics for Friends and Family screen. */
    private static Object[] i_fafHelpTopics;
    
    /** List of help topics for Login screen. */
    private static Object[] i_loginHelpTopics;
    
    /** List of help topics for MSISDN Generation screen. */
    private static Object[] i_msisdnGenerationHelpTopics;
    
    /** List of help topics for Number Blocks screen. */
    private static Object[] i_numberBlocksHelpTopics;
    
    /** List of help topics for Operators screen. */
    private static Object[] i_operatorsHelpTopics;
    
    /** List of help topics for Purge and Archive screen. */
    private static Object[] i_purgeAndArchiveHelpTopics;
    
    /** List of help topics for Reports screen. */
    private static Object[] i_reportsHelpTopics;
    
    /** List of help topics for Service Class Details screen. */
    private static Object[] i_serviceClassHelpTopics;
    
    /** List of help topics for Service Offerings screen. */
    private static Object[] i_serviceOfferingsHelpTopics;
    
    /** List of help topics for Regions screen. */
    private static Object[] i_regionsHelpTopics;
    
    /** List of help topics for Markets screen. */
    private static Object[] i_marketsHelpTopics;
    
    /** List of help topics for Balance SDPs screen. */
    private static Object[] i_balanceSdpsHelpTopics;

    /** List of help topics for Bank Codes screen. */
    private static Object[] i_banksHelpTopics;

    /** List of help topics for Faf Charging Indicators screen. */
    private static Object[] i_fafChargingIndicatorsHelpTopics;
    
    /** List of help topics for Faf Charging Indicators screen. */
    private static Object[] i_fafDefaultChargingIndicatorsHelpTopics;
    
    /** List of help topics for SDP Identifiers screen. */
    private static Object[] i_sdpidentifiersHelpTopics;
    
    /** List of help topics for Change Password screen. */
    private static Object[] i_changePasswordHelpTopics;
    
    /** List of help topics for Adjustment Type screen. */
    private static Object[] i_adjustmentTypeHelpTopics;

    /** List of help topics for Adjustment Type screen. */
    private static Object[] i_adjustmentCodesHelpTopics;

    /** List of help topics for Fast Adjustment Codes screen. */
    private static Object[] i_fastAdjustmentCodesHelpTopics;
    
    /** List of help topics for Agent Information Codes screen. */
    private static Object[] i_agentInformationHelpTopics;

    /** List of help topics for Teleservice Codes Details screen. */
    private static Object[] i_teleserviceCodesHelpTopics;
    
    /** List of help topics for Traffic Cases Details screen. */
    private static Object[] i_trafficCasesHelpTopics;
    
    /** List of help topics for Channels screen. */
    private static Object[] i_channelsHelpTopics;
    
    /** List of help topics for Bonus screen. */
    private static Object[] i_bonusHelpTopics;
    
    /** List of help topics for Accumulated Bonus screen. */
    private static Object[] i_accumulatedBonusHelpTopics;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Standard constructor.
     */
    private BoiHelpTopics()
    {
        // Prevent class from being instantiated
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Initialises BOI help data.
     */
    public static void init()
    {
        initHelpTopicsPerScreen();
        initHelpTopicToHtmlMapping();
    }
    
    /**
     * Returns the help page for the specified help topic.
     * @param p_helpTopic Help topic selected from dropdown
     * @return HTML help page.
     */
    public static String getHelpPage(String p_helpTopic)
    {
        Object l_helpPage;
        
        l_helpPage = i_helpPagesMap.get(p_helpTopic);
        
        if (l_helpPage == null)
        {
            l_helpPage = C_DEFAULT_BOI_HELP_PAGE;
        }
        
        return (String)l_helpPage;
    }
    
    /**
     * Returns the list of help topics for the specified BOI screen.
     * @param p_screen Identifies screen to return help topics for.
     * @return List of help topics for the specified BOI screen.
     */
    public static Object[] getHelpTopics(int p_screen)
    {
        Object[] l_helpTopics = null;
        
        switch (p_screen)
        {
            case C_ACCOUNT_GROUPS_SCREEN:
            {
                l_helpTopics = i_accountGroupsHelpTopics;
                break;
            }
            case C_ACCUMULATORS_SCREEN:
            {
                l_helpTopics = i_accumulatorsHelpTopics;
                break;
            }
            case C_APP_ACCESS_PRIV_SCREEN:
            {
                l_helpTopics = i_appAccessPrivHelpTopics;
                break;
            }
            case C_BATCH_CONTROL_SCREEN:
            {
                l_helpTopics = i_batchControlHelpTopics;
                break;
            }
            case C_CACHE_RELOAD_SCREEN:
            {
                l_helpTopics = i_cacheReloadHelpTopics;
                break;
            }
            case C_CC_ACCESS_PRIV_SCREEN:
            {
                l_helpTopics = i_ccAccessPrivHelpTopics;
                break;
            }
            case C_COMM_CHARGING_SCREEN:
            {
                l_helpTopics = i_commChargingHelpTopics;
                break;
            }
            case C_DED_ACCOUNTS_SCREEN:
            {
                l_helpTopics = i_dedAccountsHelpTopics;
                break;
            }
            case C_FAF_SCREEN:
            {
                l_helpTopics = i_fafHelpTopics;
                break;
            }
            case C_LOGIN_SCREEN:
            {
                l_helpTopics = i_loginHelpTopics;
                break;
            }
            case C_MSISDN_GENERATION_SCREEN:
            {
                l_helpTopics = i_msisdnGenerationHelpTopics;
                break;
            }
            case C_NUMBER_BLOCKS_SCREEN:
            {
                l_helpTopics = i_numberBlocksHelpTopics;
                break;
            }
            case C_OPERATORS_SCREEN:
            {
                l_helpTopics = i_operatorsHelpTopics;
                break;
            }
            case C_PURGE_AND_ARCHIVE_SCREEN:
            {
                l_helpTopics = i_purgeAndArchiveHelpTopics;
                break;
            }
            case C_REPORTS_SCREEN:
            {
                l_helpTopics = i_reportsHelpTopics;
                break;
            }
            case C_SERVICE_CLASS_SCREEN:
            {
                l_helpTopics = i_serviceClassHelpTopics;
                break;
            }
            case C_SERVICE_OFFERINGS_SCREEN:
            {
                l_helpTopics = i_serviceOfferingsHelpTopics;
                break;
            }
            case C_REGIONS_SCREEN:
            {
                l_helpTopics = i_regionsHelpTopics;
                break;
            }
            case C_MARKETS_SCREEN:
            {
                l_helpTopics = i_marketsHelpTopics;
                break;
            }
            case C_BALANCE_SDPS_SCREEN:
            {
                l_helpTopics = i_balanceSdpsHelpTopics;
                break;
            }
            case C_BANK_CODES_SCREEN:
            {
                l_helpTopics = i_banksHelpTopics;
                break;
            }
            case C_FAF_CHARGING_IND_SCREEN:
            {
                l_helpTopics = i_fafChargingIndicatorsHelpTopics;
                break;
            }
            case C_FAF_DEFAULT_CHARGING_IND_SCREEN:
            {
                l_helpTopics = i_fafDefaultChargingIndicatorsHelpTopics;
                break;
            }
            case C_SDP_IDENTIFIERS_SCREEN:
            {
                l_helpTopics = i_sdpidentifiersHelpTopics;
                break;
            }
            case C_CHANGE_PASSWORD_SCREEN:
            {
                l_helpTopics = i_changePasswordHelpTopics;
                break;
            }
            case C_ADJUSTMENT_TYPE_SCREEN:
            {
                l_helpTopics = i_adjustmentTypeHelpTopics;
                break;
            }
            case C_ADJUSTMENT_CODES_SCREEN:
            {
                l_helpTopics = i_adjustmentCodesHelpTopics;
                break;
            }
            case C_FAST_ADJUSTMENT_CODES_SCREEN:
            {
                l_helpTopics = i_fastAdjustmentCodesHelpTopics;
                break;
            }
            case C_AGENT_INFORMATION:
            {
                l_helpTopics = i_agentInformationHelpTopics;
                break;
            }
            case C_TELESERVICE_CODES_SCREEN:
            {
                l_helpTopics = i_teleserviceCodesHelpTopics;
                break;
            }
            case C_TRAFFIC_CASES_SCREEN:
            {
                l_helpTopics = i_trafficCasesHelpTopics;
                break;
            }
            case C_TUBS_CHANNEL_GROUPS_SCREEN:
            {
                l_helpTopics = i_channelsHelpTopics;
                break;
            }
            case C_BONUS_SCREEN:
            {
                l_helpTopics = i_bonusHelpTopics;
                break;
            }

            case C_ACCUMULATED_BONUS_SCREEN:
            {
                l_helpTopics = i_accumulatedBonusHelpTopics;
                break;
            }
            default:
            {
                l_helpTopics = i_defaultHelpTopics;
            }
        }
        return l_helpTopics;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /**
     * Initialises the lists of help topics for each screen.
     */
    private static void initHelpTopicsPerScreen()
    {
        i_defaultHelpTopics = new Object[] {C_HOW_DO_I};

        i_accountGroupsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ACCOUNT_GROUP_IDENTITIES,
                                   C_ADD_ACCOUNT_GROUP,
                                   C_CHANGE_ACCOUNT_GROUP,
                                   C_DELETE_ACCOUNT_GROUP,
                                   C_REINSTATE_ACCOUNT_GROUP};

        i_accumulatorsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ACCUMULATORS,
                                   C_VIEW_ACCUMULATOR_DETAILS,
                                   C_ADD_ACCUMULATOR_DETAILS,
                                   C_CHANGE_ACCUMULATOR_DETAILS,
                                   C_DELETE_ACCUMULATOR_DETAILS};
        
        i_appAccessPrivHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_APPLICATION_ACCESS,
                                   C_VIEW_APPLICATION_ACCESS,
                                   C_SET_APPLICATION_ACCESS};
        
        i_batchControlHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BATCH_CONTROL,
                                   C_SUBMIT_BATCH_JOB,
                                   C_MAKE_BATCH_JOB_PERIODIC,
                                   C_REVIEW_SUBMITTED_BATCH_JOBS,
                                   C_STOP_PAUSE_RESUME_BATCH_JOB,
                                   C_RECOVER_BATCH_JOB,
                                   C_CHECK_STATUS_OF_BATCH_JOB,
                                   C_RESCHEDULE_BATCH_JOB,
                                   C_USE_ORACLE_ENTERPRISE_MANAGER};
        
        i_cacheReloadHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_CACHE_RELOAD,
                                   C_INITIATE_CONFIG_CACHE_RELOAD,
                                   C_INITIATE_FEATURE_CACHE_RELOAD};
        
        i_ccAccessPrivHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_CUSTOMER_CARE_ACCESS,
                                   C_VIEW_SET_OF_PRIVILEGES,
                                   C_ADD_SET_OF_PRIVILEGES,
                                   C_CHANGE_SET_OF_PRIVILEGES,
                                   C_DELETE_SET_OF_PRIVILEGES};
        
        i_commChargingHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_COMMUNITY_CHARGING,
                                   C_ADD_COMM_CHARGING_IDENTITY,
                                   C_CHANGE_COMM_CHARGING_IDENTITY,
                                   C_DELETE_COMM_CHARGING_IDENTITY,
                                   C_REINSTATE_COMM_CHARGING_IDENTITY};
        
        i_dedAccountsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_DEDICATED_ACCOUNTS,
                                   C_VIEW_DED_ACCOUNT_DETAILS,
                                   C_ADD_DED_ACCOUNT_DETAILS,
                                   C_CHANGE_DED_ACCOUNT_DETAILS,
                                   C_DELETE_DED_ACCOUNT_DETAILS};
        
        i_fafHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_FAF_BARRED_NUMBERS,
                                   C_VIEW_BARRED_NUMBER_DEFN,
                                   C_ADD_BARRED_NUMBER_DEFN,
                                   C_CHANGE_BARRED_NUMBER_DEFN,
                                   C_DELETE_A_BARRED_NUMBER_DEFN};
        
        i_loginHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_ABOUT_ASCS,
                                   C_ABOUT_THE_BOI,
                                   C_LOG_IN_TO_BOI,
                                   C_LOG_OUT_OF_BOI,
                                   C_ABOUT_PASSWORDS,
                                   C_ABOUT_HELP,
                                   C_USE_FULL_HELP,
                                   C_USE_CONTEXT_SENSITIVE_HELP,
                                   C_USE_THE_HELP_INDEX,
                                   C_SEARCH_THE_HELP_SYSTEM,
                                   C_PRINT_HELP,
                                   C_USER_GUIDE_REFERENCES};
        
        i_msisdnGenerationHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_MSISDN_GENERATION,
                                   C_GENERATE_MSISDNS};
        
        i_numberBlocksHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_NUMBER_BLOCKS,
                                   C_VIEW_NUMBER_BLOCKS,
                                   C_ADD_NUMBER_BLOCK};
        
        i_operatorsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_OPERATOR_CONFIGURATION,
                                   C_VIEW_SET_OF_OPERATOR_DETAILS,
                                   C_ADD_SET_OF_OPERATOR_DETAILS,
                                   C_CHANGE_SET_OF_OPERATOR_DETAILS,
                                   C_DELETE_SET_OF_OPERATOR_DETAILS,
                                   C_UNBAR_AN_OPERATOR};
        
        i_purgeAndArchiveHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_PURGE_AND_ARCHIVE,
                                   C_SUBMIT_PURGE_JOB,
                                   C_MAKE_PURGE_AND_ARCHIVE_JOB_PERIODIC,
                                   C_REVIEW_SUBMITTED_PURGE_JOBS,
                                   C_RECOVER_PURGE_JOB,
                                   C_STOP_PAUSE_RESUME_PURGE_JOB,
                                   C_RESCHEDULE_PURGE_JOB,
                                   C_USE_ORACLE_ENTERPRISE_MANAGER};
        
        i_reportsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_REPORTS,
                                   C_SUBMIT_A_REPORT,
                                   C_MAKE_REPORT_PERIODIC,
                                   C_VIEW_REPORT,
                                   C_RESCHEDULE_REPORT,
                                   C_STOP_PAUSE_RESUME_REPORT_JOB,
                                   C_USE_ORACLE_ENTERPRISE_MANAGER};
        
        i_serviceClassHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_SERVICE_CLASS_DETAILS,
                                   C_VIEW_SERVICE_CLASS_DETAILS,
                                   C_ADD_SERVICE_CLASS_DETAILS,
                                   C_CHANGE_SERVICE_CLASS_DETAILS,
                                   C_DELETE_SERVICE_CLASS_DETAILS,
                                   C_REINSTATE_SERVICE_CLASS_DETAILS};
        
        i_serviceOfferingsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_SERVICE_OFFERINGS,
                                   C_DEFINE_SERVICE_OFFERING,
                                   C_CHANGE_SERVICE_OFFERING};
        
        i_regionsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_REGION_IDENTITIES,
                                   C_ADD_A_REGION,
                                   C_CHANGE_A_REGION,
                                   C_DELETE_A_REGION,
                                   C_REINSTATE_A_REGION};
        
        i_marketsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_MARKETS,
                                   C_ADD_A_MARKET,
                                   C_CHANGE_A_MARKET,
                                   C_DELETE_A_MARKET,
                                   C_REINSTATE_A_MARKET};

        i_balanceSdpsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BALANCE_SDPS,
                                   C_GENERATE_AN_SDP_BALANCING_FILE};

        i_banksHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_BANKS,
                                   C_ADD_A_BANK_CODE,
                                   C_CHANGE_A_BANK_CODE,
                                   C_DELETE_A_BANK_CODE,
                                   C_REINSTATE_A_BANK_CODE};

        i_fafChargingIndicatorsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_FAF_CHARGING_INDICATORS,
                                   C_ADD_A_CHARGING_INDICATOR,
                                   C_CHANGE_A_CHARGING_INDICATOR,
                                   C_DELETE_A_CHARGING_INDICATOR,
                                   C_REINSTATE_A_CHARGING_INDICATOR};
        
        i_fafDefaultChargingIndicatorsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_FAF_DEFAULT_CHARGING_INDICATORS,
                                   C_ADD_A_DEFAULT_CHARGING_INDICATOR,
                                   C_CHANGE_A_DEFAULT_CHARGING_INDICATOR,
                                   C_DELETE_A_DEFAULT_CHARGING_INDICATOR,
                                   C_REINSTATE_A_DEFAULT_CHARGING_INDICATOR};
        
        i_sdpidentifiersHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_SDP_IDENTIFIERS,
                                   C_ADD_AN_SDP_IDENTIFIER,
                                   C_CHANGE_AN_SDP_IDENTIFIER,
                                   C_DELETE_AN_SDP_IDENTIFIER,
                                   C_REINSTATE_AN_SDP_IDENTIFIER};
        
        i_changePasswordHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_ABOUT_PASSWORDS,
                                   C_CHANGE_MY_PASSWORD,
                                   C_SET_THE_BOI_PASSWORD_STRENGTH};
        
        i_adjustmentTypeHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_ADJUSTMENT_TYPES,
                                   C_VIEW_ADJUSTMENT_TYPES,
                                   C_ADD_AN_ADJUSTMENT_TYPE,
                                   C_CHANGE_AN_ADJUSTMENT_TYPE,
                                   C_DELETE_AN_ADJUSTMENT_TYPE,
                                   C_REINSTATE_AN_ADJUSTMENT_TYPE};

        i_fastAdjustmentCodesHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_FAST_ADJUSTMENT_CODES,
                                   C_VIEW_FAST_ADJUSTMENT_CODES,
                                   C_ADD_A_FAST_ADJUSTMENT_CODE,
                                   C_CHANGE_A_FAST_ADJUSTMENT_CODE,
                                   C_DELETE_A_FAST_ADJUSTMENT_CODE};
        
        i_agentInformationHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_AGENT_INFORMATION,
                                   C_VIEW_AGENT_INFORMATION,
                                   C_ADD_AGENT_INFORMATION,
                                   C_CHANGE_AGENT_INFORMATION,
                                   C_DELETE_AGENT_INFORMATION,
                                   C_REINSTATE_AGENT_INFORMATION};
        
        i_adjustmentCodesHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_ADJUSTMENT_CODES,
                                   C_VIEW_ADJUSTMENT_CODES,
                                   C_ADD_AN_ADJUSTMENT_CODE,
                                   C_CHANGE_AN_ADJUSTMENT_CODE,
                                   C_DELETE_AN_ADJUSTMENT_CODE,
                                   C_REINSTATE_AN_ADJUSTMENT_CODE};

        i_teleserviceCodesHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ADD_TELESERVICE_CODES,
                                   C_CHANGE_TELESERVICE_CODES,
                                   C_DELETE_TELESERVICE_CODES,
                                   C_REINSTATE_TELESERVICE_CODES};

        i_trafficCasesHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ADD_TRAFFIC_CASES,
                                   C_CHANGE_TRAFFIC_CASES,
                                   C_DELETE_TRAFFIC_CASES,
                                   C_REINSTATE_TRAFFIC_CASES};
        
        i_channelsHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_CHANNELS,
                                   C_ADD_CHANNEL,
                                   C_CHANGE_CHANNEL,
                                   C_DELETE_CHANNEL,
                                   C_REINSTATE_CHANNEL};
        
        i_bonusHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
                                   C_ABOUT_BONUS_SCHEMES,
                                   C_ADD_BONUS_SCHEME,
                                   C_CHANGE_BONUS_SCHEME,
                                   C_DELETE_BONUS_SCHEME,
                                   C_REINSTATE_BONUS_SCHEME,
                                   C_ADD_BONUS_ELEMENT,
                                   C_CHANGE_BONUS_ELEMENT,
                                   C_DELETE_BONUS_ELEMENT,
                                   C_REINSTATE_BONUS_ELEMENT};
        
        i_accumulatedBonusHelpTopics = new Object[] {
                                   C_HOW_DO_I,
                                   C_BUSINESS_CONFIGURATION,
                                   C_USE_BC_SCREENS,
				   C_ABOUT_ACCUMULATED_BONUS_SCHEMES,
                                   C_ADD_ACCUMULATED_BONUS_SCHEME,
                                   C_CHANGE_ACCUMULATED_BONUS_SCHEME,
                                   C_DELETE_ACCUMULATED_BONUS_SCHEME,
                                   C_REINSTATE_ACCUMULATED_BONUS_SCHEME,
                                   C_ADD_ACCUMULATED_BONUS_ELEMENT,
                                   C_CHANGE_ACCUMULATED_BONUS_ELEMENT,
                                   C_DELETE_ACCUMULATED_BONUS_ELEMENT,
                                   C_REINSTATE_ACCUMULATED_BONUS_ELEMENT};
    }

    /**
     * Initialises the hashmap containing the mapping of help topic to help page.
     */
    private static void initHelpTopicToHtmlMapping()
    {
        i_helpPagesMap = new HashMap();
        
        i_helpPagesMap.put(C_ABOUT_BANKS, "banks.htm");
        i_helpPagesMap.put(C_ADD_A_BANK_CODE, "add_a_bank_code.htm");
        i_helpPagesMap.put(C_CHANGE_A_BANK_CODE, "change_a_bank_code.htm");
        i_helpPagesMap.put(C_DELETE_A_BANK_CODE, "delete_a_bank_code.htm");
        i_helpPagesMap.put(C_REINSTATE_A_BANK_CODE, "reinstate_a_bank_code.htm");
        i_helpPagesMap.put(C_ABOUT_FAF_CHARGING_INDICATORS, "family_&_friends_charging_indicators.htm");
        i_helpPagesMap.put(C_ADD_A_CHARGING_INDICATOR, "add_a_charging_indicator.htm");
        i_helpPagesMap.put(C_CHANGE_A_CHARGING_INDICATOR, "change_a_charging_indicator.htm");
        i_helpPagesMap.put(C_DELETE_A_CHARGING_INDICATOR, "delete_a_charging_indicator.htm");
        i_helpPagesMap.put(C_REINSTATE_A_CHARGING_INDICATOR, "reinstate_a_charging_indicator.htm");
        i_helpPagesMap.put(C_ABOUT_FAF_DEFAULT_CHARGING_INDICATORS, "family_&_friends_default_ch_inds.htm");
        i_helpPagesMap.put(C_ADD_A_DEFAULT_CHARGING_INDICATOR, "add_a_faf_default_ch_ind.htm");
        i_helpPagesMap.put(C_CHANGE_A_DEFAULT_CHARGING_INDICATOR, "change_a_faf_default_ch_ind.htm");
        i_helpPagesMap.put(C_DELETE_A_DEFAULT_CHARGING_INDICATOR, "delete_a_faf_default_ch_ind.htm");
        i_helpPagesMap.put(C_REINSTATE_A_DEFAULT_CHARGING_INDICATOR, "reinstate_a_faf_default_ch_ind.htm");
        i_helpPagesMap.put(C_ABOUT_SDP_IDENTIFIERS, "sdp_identifiers.htm");
        i_helpPagesMap.put(C_ADD_AN_SDP_IDENTIFIER, "add_an_sdp_identifier.htm");
        i_helpPagesMap.put(C_CHANGE_AN_SDP_IDENTIFIER, "change_an_sdp_identifier.htm");
        i_helpPagesMap.put(C_DELETE_AN_SDP_IDENTIFIER, "delete_an_sdp_identifier.htm");
        i_helpPagesMap.put(C_REINSTATE_AN_SDP_IDENTIFIER, "reinstate_an_sdp_identifier.htm");
        i_helpPagesMap.put(C_ABOUT_ADJUSTMENT_TYPES, "adjustment_types.htm");
        i_helpPagesMap.put(C_VIEW_ADJUSTMENT_TYPES, "view_adjustment_types.htm");
        i_helpPagesMap.put(C_ADD_AN_ADJUSTMENT_TYPE, "add_an_adjustment_type.htm");
        i_helpPagesMap.put(C_CHANGE_AN_ADJUSTMENT_TYPE, "change_an_adjustment_type.htm");
        i_helpPagesMap.put(C_DELETE_AN_ADJUSTMENT_TYPE, "delete_an_adjustment_type.htm");
        i_helpPagesMap.put(C_REINSTATE_AN_ADJUSTMENT_TYPE, "reinstate_an_adjustment_type.htm");  
        i_helpPagesMap.put(C_ABOUT_FAST_ADJUSTMENT_CODES, "fast_adjustment_codes.htm");
        i_helpPagesMap.put(C_VIEW_FAST_ADJUSTMENT_CODES, "view_fast_adjustment_codes.htm");
        i_helpPagesMap.put(C_ADD_A_FAST_ADJUSTMENT_CODE, "add_a_fast_adjustment_code.htm");
        i_helpPagesMap.put(C_CHANGE_A_FAST_ADJUSTMENT_CODE, "change_a_fast_adjustment_code.htm");
        i_helpPagesMap.put(C_DELETE_A_FAST_ADJUSTMENT_CODE, "delete_a_fast_adjustment_code.htm");
        i_helpPagesMap.put(C_ABOUT_AGENT_INFORMATION, "agent_information.htm");
        i_helpPagesMap.put(C_VIEW_AGENT_INFORMATION, "view_agent_information.htm");
        i_helpPagesMap.put(C_ADD_AGENT_INFORMATION, "add_agent_information.htm");
        i_helpPagesMap.put(C_CHANGE_AGENT_INFORMATION, "change_agent_information.htm");
        i_helpPagesMap.put(C_DELETE_AGENT_INFORMATION, "delete_agent_information.htm");
        i_helpPagesMap.put(C_REINSTATE_AGENT_INFORMATION, "reinstate_agent_information.htm");
        i_helpPagesMap.put(C_ABOUT_ADJUSTMENT_CODES, "adjustment_codes.htm");
        i_helpPagesMap.put(C_VIEW_ADJUSTMENT_CODES, "view_adjustment_codes.htm");
        i_helpPagesMap.put(C_ADD_AN_ADJUSTMENT_CODE, "add_an_adjustment_code.htm");
        i_helpPagesMap.put(C_CHANGE_AN_ADJUSTMENT_CODE, "change_an_adjustment_code.htm");
        i_helpPagesMap.put(C_DELETE_AN_ADJUSTMENT_CODE, "delete_an_adjustment_code.htm");
        i_helpPagesMap.put(C_REINSTATE_AN_ADJUSTMENT_CODE, "reinstate_an_adjustment_code.htm");
        i_helpPagesMap.put(C_USER_GUIDE_REFERENCES, "references.htm");
        i_helpPagesMap.put(C_ABOUT_ASCS, "about_ascs.htm");
        i_helpPagesMap.put(C_ABOUT_THE_BOI, "about_the_business_operations_interface.htm");
        i_helpPagesMap.put(C_LOG_IN_TO_BOI, "log_in_to_boi.htm");
        i_helpPagesMap.put(C_LOG_OUT_OF_BOI, "log_out_of_boi.htm");
        i_helpPagesMap.put(C_ABOUT_PASSWORDS, "about_passwords.htm");
        i_helpPagesMap.put(C_ABOUT_HELP, "about_help.htm");
        i_helpPagesMap.put(C_USE_FULL_HELP, "use_full_help.htm");
        i_helpPagesMap.put(C_USE_CONTEXT_SENSITIVE_HELP, "use_context_sensitive_help.htm");
        i_helpPagesMap.put(C_USE_THE_HELP_INDEX, "use_the_help_index.htm");
        i_helpPagesMap.put(C_SEARCH_THE_HELP_SYSTEM, "search_the_help_system.htm");
        i_helpPagesMap.put(C_PRINT_HELP, "print_help.htm");
        i_helpPagesMap.put(C_CHANGE_MY_PASSWORD, "change_my_password.htm");
        i_helpPagesMap.put(C_SET_THE_BOI_PASSWORD_STRENGTH, "set_the_boi_password_strength.htm");
        i_helpPagesMap.put(C_BUSINESS_CONFIGURATION, "business_configuration.htm");
        i_helpPagesMap.put(C_USE_BC_SCREENS, "use_the_business_configuration_screens.htm");
        i_helpPagesMap.put(C_OPERATOR_CONFIGURATION, "operator_configuration.htm");
        i_helpPagesMap.put(C_VIEW_SET_OF_OPERATOR_DETAILS, "view_a_set_of_operator_details.htm");
        i_helpPagesMap.put(C_ADD_SET_OF_OPERATOR_DETAILS, "add_a_set_of_operator_details.htm");
        i_helpPagesMap.put(C_CHANGE_SET_OF_OPERATOR_DETAILS, "change_a_set_of_operator_details.htm");
        i_helpPagesMap.put(C_DELETE_SET_OF_OPERATOR_DETAILS, "delete_a_set_of_operator_details.htm");
        i_helpPagesMap.put(C_UNBAR_AN_OPERATOR, "unbar_an_operator.htm");
        i_helpPagesMap.put(C_CUSTOMER_CARE_ACCESS, "customer_care_access.htm");
        i_helpPagesMap.put(C_VIEW_SET_OF_PRIVILEGES, "view_a_set_of_privileges.htm");
        i_helpPagesMap.put(C_ADD_SET_OF_PRIVILEGES, "add_a_set_of_privileges.htm");
        i_helpPagesMap.put(C_CHANGE_SET_OF_PRIVILEGES, "change_a_set_of_privileges.htm");
        i_helpPagesMap.put(C_DELETE_SET_OF_PRIVILEGES, "delete_a_set_of_privileges.htm");
        i_helpPagesMap.put(C_APPLICATION_ACCESS, "application_access.htm");
        i_helpPagesMap.put(C_VIEW_APPLICATION_ACCESS, "view_application_access_privileges.htm");
        i_helpPagesMap.put(C_SET_APPLICATION_ACCESS, "set_application_access_privileges.htm");
        i_helpPagesMap.put(C_ACCOUNT_GROUP_IDENTITIES, "account_group_identities.htm");
        i_helpPagesMap.put(C_ADD_ACCOUNT_GROUP, "add_an_account_group.htm");
        i_helpPagesMap.put(C_CHANGE_ACCOUNT_GROUP, "change_an_account_group.htm");
        i_helpPagesMap.put(C_DELETE_ACCOUNT_GROUP, "delete_an_account_group.htm");
        i_helpPagesMap.put(C_REINSTATE_ACCOUNT_GROUP, "reinstate_an_account_group.htm");
        i_helpPagesMap.put(C_ACCUMULATORS, "accumulators.htm");
        i_helpPagesMap.put(C_VIEW_ACCUMULATOR_DETAILS, "view_accumulator_details.htm");
        i_helpPagesMap.put(C_ADD_ACCUMULATOR_DETAILS, "add_accumulator_details.htm");
        i_helpPagesMap.put(C_CHANGE_ACCUMULATOR_DETAILS, "change_accumulator_details.htm");
        i_helpPagesMap.put(C_DELETE_ACCUMULATOR_DETAILS, "delete_accumulator_details.htm");
        i_helpPagesMap.put(C_COMMUNITY_CHARGING, "community_charging.htm");
        i_helpPagesMap.put(C_ADD_COMM_CHARGING_IDENTITY, "add_a_community_charging_identity.htm");
        i_helpPagesMap.put(C_CHANGE_COMM_CHARGING_IDENTITY, "change_a_community_charging_identity.htm");
        i_helpPagesMap.put(C_DELETE_COMM_CHARGING_IDENTITY, "delete_a_community_charging_identity.htm");
        i_helpPagesMap.put(C_REINSTATE_COMM_CHARGING_IDENTITY, "reinstate_a_community_charging_identity.htm");
        i_helpPagesMap.put(C_DEDICATED_ACCOUNTS, "dedicated_accounts.htm");
        i_helpPagesMap.put(C_VIEW_DED_ACCOUNT_DETAILS, "view_dedicated_account_details.htm");
        i_helpPagesMap.put(C_ADD_DED_ACCOUNT_DETAILS, "add_dedicated_account_details.htm");
        i_helpPagesMap.put(C_CHANGE_DED_ACCOUNT_DETAILS, "change_dedicated_account_details.htm");
        i_helpPagesMap.put(C_DELETE_DED_ACCOUNT_DETAILS, "delete_dedicated_account_details.htm");
        i_helpPagesMap.put(C_FAF_BARRED_NUMBERS, "family_&_friends_barred_numbers.htm");
        i_helpPagesMap.put(C_VIEW_BARRED_NUMBER_DEFN, "view_a_barred_number_definition.htm");
        i_helpPagesMap.put(C_ADD_BARRED_NUMBER_DEFN, "add_a_barred_number_definition.htm");
        i_helpPagesMap.put(C_CHANGE_BARRED_NUMBER_DEFN, "change_a_barred_number_definition.htm");
        i_helpPagesMap.put(C_DELETE_A_BARRED_NUMBER_DEFN, "delete_a_barred_number_definition.htm");
        i_helpPagesMap.put(C_SERVICE_CLASS_DETAILS, "service_class_details.htm");
        i_helpPagesMap.put(C_VIEW_SERVICE_CLASS_DETAILS, "view_service_class_details.htm");
        i_helpPagesMap.put(C_ADD_SERVICE_CLASS_DETAILS, "add_service_class_details.htm");
        i_helpPagesMap.put(C_CHANGE_SERVICE_CLASS_DETAILS, "change_service_class_details.htm");
        i_helpPagesMap.put(C_DELETE_SERVICE_CLASS_DETAILS, "delete_service_class_details.htm");
        i_helpPagesMap.put(C_REINSTATE_SERVICE_CLASS_DETAILS, "reinstate_service_class_details.htm");
        i_helpPagesMap.put(C_SERVICE_OFFERINGS, "service_offerings.htm");
        i_helpPagesMap.put(C_DEFINE_SERVICE_OFFERING, "define_a_service_offering.htm");
        i_helpPagesMap.put(C_CHANGE_SERVICE_OFFERING, "change_a_service_offering.htm");
        i_helpPagesMap.put(C_NUMBER_BLOCKS, "number_blocks.htm");
        i_helpPagesMap.put(C_VIEW_NUMBER_BLOCKS, "view_number_blocks.htm");
        i_helpPagesMap.put(C_ADD_NUMBER_BLOCK, "add_a_number_block.htm");
        i_helpPagesMap.put(C_CACHE_RELOAD, "cache_reload.htm");
        i_helpPagesMap.put(C_INITIATE_CONFIG_CACHE_RELOAD, "initiate_config_cache_reload.htm");
        i_helpPagesMap.put(C_INITIATE_FEATURE_CACHE_RELOAD, "initiate_feature_cache_reload.htm");
        i_helpPagesMap.put(C_BATCH_JOBS, "batch_jobs.htm");
        i_helpPagesMap.put(C_BATCH_CONTROL, "batch_control.htm");
        i_helpPagesMap.put(C_MSISDN_GENERATION, "msisdn_generation.htm");
        i_helpPagesMap.put(C_BATCH_CONTROL, "batch_control.htm");
        i_helpPagesMap.put(C_SUBMIT_BATCH_JOB, "submit_a_batch_job.htm");
        i_helpPagesMap.put(C_REVIEW_SUBMITTED_BATCH_JOBS, "review_previously_submitted_batch_jobs.htm");
        i_helpPagesMap.put(C_STOP_PAUSE_RESUME_BATCH_JOB, "stop_pause_or_resume_a_batch_job.htm");
        i_helpPagesMap.put(C_RECOVER_BATCH_JOB, "recover_a_failed_or_stopped_batch_job.htm");
        i_helpPagesMap.put(C_CHECK_STATUS_OF_BATCH_JOB, "check_the_status_of_a_batch_job.htm");
        i_helpPagesMap.put(C_RESCHEDULE_BATCH_JOB, "reschedule_a_batch_job.htm");
        i_helpPagesMap.put(C_GENERATE_MSISDNS, "generate_msisdns.htm");
        i_helpPagesMap.put(C_PURGE_AND_ARCHIVE, "purge_and_archive.htm");
        i_helpPagesMap.put(C_SUBMIT_PURGE_JOB, "submit_a_purge_job.htm");
        i_helpPagesMap.put(C_REVIEW_SUBMITTED_PURGE_JOBS, "review_previously_submitted_purge_jobs.htm");
        i_helpPagesMap.put(C_RECOVER_PURGE_JOB, "recover_a_failed_or_stopped_purge_job.htm");
        i_helpPagesMap.put(C_STOP_PAUSE_RESUME_PURGE_JOB, "stop_pause_or_resume_a_purge_job.htm");
        i_helpPagesMap.put(C_RESCHEDULE_PURGE_JOB, "reschedule_a_purge_job.htm");
        i_helpPagesMap.put(C_REPORTS, "reports.htm");
        i_helpPagesMap.put(C_SUBMIT_A_REPORT, "submit_a_report.htm");
        i_helpPagesMap.put(C_VIEW_REPORT, "view_a_report.htm");
        i_helpPagesMap.put(C_RESCHEDULE_REPORT, "reschedule_a_report.htm");
        i_helpPagesMap.put(C_BALANCE_SDPS, "balance_sdps.htm");
        i_helpPagesMap.put(C_GENERATE_AN_SDP_BALANCING_FILE, "generate_an_sdp_balancing_file.htm");
        i_helpPagesMap.put(C_REGION_IDENTITIES, "region_identities.htm");
        i_helpPagesMap.put(C_ADD_A_REGION, "add_a_region.htm");
        i_helpPagesMap.put(C_CHANGE_A_REGION, "change_a_region.htm");
        i_helpPagesMap.put(C_DELETE_A_REGION, "delete_a_region.htm");
        i_helpPagesMap.put(C_REINSTATE_A_REGION, "reinstate_a_region.htm");
        i_helpPagesMap.put(C_MARKETS, "markets.htm");
        i_helpPagesMap.put(C_ADD_A_MARKET, "add_a_market.htm");
        i_helpPagesMap.put(C_CHANGE_A_MARKET, "change_a_market.htm");
        i_helpPagesMap.put(C_DELETE_A_MARKET, "delete_a_market.htm");
        i_helpPagesMap.put(C_REINSTATE_A_MARKET, "reinstate_a_market.htm");
        i_helpPagesMap.put(C_MAKE_BATCH_JOB_PERIODIC, "make_a_batch_job_periodic.htm");
        i_helpPagesMap.put(C_USE_ORACLE_ENTERPRISE_MANAGER, "oracle_enterprise_manager.htm");
        i_helpPagesMap.put(C_MAKE_PURGE_AND_ARCHIVE_JOB_PERIODIC, "make_a_purge_and_archive_job_periodic.htm");
        i_helpPagesMap.put(C_MAKE_REPORT_PERIODIC, "make_a_report_periodic.htm");
        i_helpPagesMap.put(C_STOP_PAUSE_RESUME_REPORT_JOB, "stop_pause_or_resume_a_report_job.htm");
        i_helpPagesMap.put(C_ADD_TELESERVICE_CODES, "add_a_teleservice_code.htm");
        i_helpPagesMap.put(C_CHANGE_TELESERVICE_CODES, "change_a_teleservice_code.htm");
        i_helpPagesMap.put(C_DELETE_TELESERVICE_CODES, "delete_a_teleservice_code.htm");
        i_helpPagesMap.put(C_REINSTATE_TELESERVICE_CODES, "reinstate_a_teleservice_code.htm");
        i_helpPagesMap.put(C_ADD_TRAFFIC_CASES, "add_a_traffic_case.htm");
        i_helpPagesMap.put(C_CHANGE_TRAFFIC_CASES, "change_a_traffic_case.htm");
        i_helpPagesMap.put(C_DELETE_TRAFFIC_CASES, "delete_a_traffic_case.htm");
        i_helpPagesMap.put(C_REINSTATE_TRAFFIC_CASES, "reinstate_a_traffic_case.htm");
        i_helpPagesMap.put(C_ABOUT_CHANNELS, "channels.htm");
        i_helpPagesMap.put(C_ADD_CHANNEL, "add_a_channel.htm");
        i_helpPagesMap.put(C_CHANGE_CHANNEL, "change_a_channel.htm");
        i_helpPagesMap.put(C_DELETE_CHANNEL, "delete_a_channel.htm");
        i_helpPagesMap.put(C_REINSTATE_CHANNEL, "reinstate_a_channel.htm");
        i_helpPagesMap.put(C_ABOUT_BONUS_SCHEMES, "bonus_schemes.htm");
        i_helpPagesMap.put(C_ADD_BONUS_SCHEME, "add_a_bonus_scheme.htm");
        i_helpPagesMap.put(C_CHANGE_BONUS_SCHEME, "change_a_bonus_scheme.htm");
        i_helpPagesMap.put(C_DELETE_BONUS_SCHEME, "delete_a_bonus_scheme.htm");
        i_helpPagesMap.put(C_REINSTATE_BONUS_SCHEME, "reinstate_a_bonus_scheme.htm");
        i_helpPagesMap.put(C_ADD_BONUS_ELEMENT, "add_a_bonus_element.htm");
        i_helpPagesMap.put(C_CHANGE_BONUS_ELEMENT, "change_a_bonus_element.htm");
        i_helpPagesMap.put(C_DELETE_BONUS_ELEMENT, "delete_a_bonus_element.htm");
        i_helpPagesMap.put(C_REINSTATE_BONUS_ELEMENT, "reinstate_a_bonus_element.htm");
        i_helpPagesMap.put(C_ABOUT_ACCUMULATED_BONUS_SCHEMES, "accumulated_bonus_schemes.htm");	
        i_helpPagesMap.put(C_ADD_ACCUMULATED_BONUS_SCHEME, "add_an_accumulated_bonus_scheme.htm");
        i_helpPagesMap.put(C_CHANGE_ACCUMULATED_BONUS_SCHEME, "change_an_accumulated_bonus_scheme.htm");
        i_helpPagesMap.put(C_DELETE_ACCUMULATED_BONUS_SCHEME, "delete_an_accumulated_bonus_scheme.htm");
        i_helpPagesMap.put(C_REINSTATE_ACCUMULATED_BONUS_SCHEME, "reinstate_an_accumulated_bonus_scheme.htm");
        i_helpPagesMap.put(C_ADD_ACCUMULATED_BONUS_ELEMENT, "add_an_accumulated_bonus_element.htm");
        i_helpPagesMap.put(C_CHANGE_ACCUMULATED_BONUS_ELEMENT, "change_an_accumulated_bonus_element.htm");
        i_helpPagesMap.put(C_DELETE_ACCUMULATED_BONUS_ELEMENT, "delete_an_accumulated_bonus_element.htm");
        i_helpPagesMap.put(C_REINSTATE_ACCUMULATED_BONUS_ELEMENT, "reinstate_an_accumulated_bonus_element.htm");
    }
}
