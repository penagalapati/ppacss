////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiRegionData.java
//      DATE            :       15-Aug-2005
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#
//                              PRD_ASCS00_GEN_CA_44
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Wrapper for RegiRegionData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;

/** Wrapper for regions class within BOI. */
public class BoiRegionData extends DataObject
{
    /** Basic region data object */
    private RegiRegionData i_regiData;
    
    /**
     * Simple constructor.
     * @param p_acgrData region data object to wrapper.
     */
    public BoiRegionData(RegiRegionData p_acgrData)
    {
        i_regiData = p_acgrData;
    }

    /**
     * Return wrappered region data object.
     * @return Wrappered region data object.
     */
    public RegiRegionData getInternalRegionData()
    {
        return i_regiData;
    }

    /**
     * Compares two region data objects and returns true if they equate.
     * @param p_regiData region data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_regiData)
    {
        boolean l_return = false;
        
        if ( p_regiData != null &&
             p_regiData instanceof BoiRegionData &&
             i_regiData.getHomeRegionId().getValue() ==
                 ((BoiRegionData)p_regiData).getInternalRegionData().getHomeRegionId().getValue() )
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns region data object as a String for display in BOI.
     * @return region data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_regiData.getHomeRegionId() + 
                                            " - " + i_regiData.getRegionDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_regiData.getHomeRegionId().getValue();
    }
}