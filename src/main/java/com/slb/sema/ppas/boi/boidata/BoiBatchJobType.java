////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiBatchJobType.java
//    DATE            :       22-March-2005
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#1346/5912
//
//    COPYRIGHT       :       Atos Origin 2005
//
//    DESCRIPTION     :       Encapsulates a batch job type.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boidata;

/**
 * @author 40771f
 * Encapsulates the information BOI needs about a batch job.
 */
public class BoiBatchJobType
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------
    
    
    /** The display name of the batch job. */
    private String i_displayName = null;

    /** The name of the batch job type submitted to job scheduler. */
    private String i_jobSchedulerSubmissionName = null;
    
    /** The batch job type. Also used to identify the batch job in the baco_batch_control table. */
    private String i_batchJobType = null;

    /** Flag to signify whether this is a database or file driven batch job. */
    private boolean i_isDbJobType = false;

    /** Flag to signify whether or not this job type is resubmittable. */
    private boolean i_isResubmittable = false;
    
    /** Filename prefix to use when building the filename. */ 
    private String i_filenamePrefix = null;
    
    /**
     * No argument constructor.
     */
    public BoiBatchJobType()
    {
        this("", "", "", false, "");
    }

    /**
     * Constructor for database driven batch jobs.
     * @param p_displayName      The name of the batch job as it is to appear on the Batch Control screen.
     * @param p_jsSubmissionName Batch job type identifier to JS subsystem.
     * @param p_batchJobType     The batch job type known to the Batch subsystem.
     * @param p_isResubmittable  True if job is resubmittable, false otherwise.
     */
    public BoiBatchJobType(String  p_displayName,
                           String  p_batchJobType,
                           String  p_jsSubmissionName,
                           //boolean p_isDbJobType,
                           boolean p_isResubmittable)
    {
        i_displayName = p_displayName;
        i_batchJobType = p_batchJobType;
        i_jobSchedulerSubmissionName = p_jsSubmissionName;
        i_isDbJobType = true;
        i_isResubmittable = p_isResubmittable;
    }
    
    
    /**
     * Constructor for file driven batch jobs.
     * @param p_displayName      The name of the batch job as it is to appear on the Batch Control screen.
     * @param p_jsSubmissionName Batch job type identifier to JS subsystem.
     * @param p_batchJobType     The batch job type known to the Batch subsystem.
     * @param p_isResubmittable  True if job is resubmittable, false otherwise.
     * @param p_filenamePrefix   Prefix of the filename to submit for this job type.
     */
    public BoiBatchJobType(String  p_displayName,
                           String  p_batchJobType,
                           String  p_jsSubmissionName,
                           //boolean p_isDbJobType,
                           boolean p_isResubmittable,
                           String  p_filenamePrefix)
    {
        i_displayName = p_displayName;
        i_batchJobType = p_batchJobType;
        i_jobSchedulerSubmissionName = p_jsSubmissionName;
        i_isDbJobType = false;
        i_isResubmittable = p_isResubmittable;
        i_filenamePrefix = p_filenamePrefix;
    }

    /**
     * Gets the BOI display name of the batch job.
     * @return Display name of the batch job on the BOI Batch COntrol screen.
     */
    public String getDisplayName()
    {
        return i_displayName;
    }

    /** 
     * Gets the batch job identifier name for the Job Scheduler subsystem.
     * @return The name of this batch job type for Job Scheduler.
     */
    public String getJsSubmissionName()
    {
        return i_jobSchedulerSubmissionName;
    }
    
    /**
     * Gets the batch job type (this is the name of the batch job as known to the batch subsystem).
     * @return The batch job type.
     */
    public String getBatchJobType()
    {
        return i_batchJobType;
    }
    
    /**
     * Gets the filename prefix for this job type.
     * @return The prefix of the filename for this job type.
     */
    public String getFilenamePrefix()
    {
        return i_filenamePrefix;
    }

    /**
     * Returns true if this is a database-driven job and false otherwise.
     * @return true if this is a database driven job and false otherwise.
     */
    public boolean isDbJobType()
    {
        return i_isDbJobType;
    }

    /**
     * Returns true if this job is resubmittable and false otherwise.
     * @return true or false, depending on whether or not the job is resubmittable.
     */
    public boolean isResubmittable()
    {
        return i_isResubmittable;
    }

    /** 
     * Sets the display name to the specified value for this job type.
     * @param p_displayName The display name for the job type. 
     */
    public void setDisplayName(String p_displayName)
    {
        i_displayName = p_displayName;
    }

    /**
     * Sets the name of the job type for use when submitting to the Job Scheduler.
     * @param p_jsSubmissionName The name of the job type for submission to Job Scheduler.
     */
    public void setJsSubmissionName(String p_jsSubmissionName)
    {
        i_jobSchedulerSubmissionName = p_jsSubmissionName;
    }

    /**
     * Sets the job type to be file driven or database driven.
     * @param p_isDbJobType true if the job type is database driven and false otherwise.
     */
    public void setDbJobType(boolean p_isDbJobType)
    {
        i_isDbJobType = p_isDbJobType;
    }

    /**
     * Sets whether or not this batch job is resubmittable.
     * @param p_resubmittable true if the batch job is resubmittable and false otherwise.
     */
    public void setResubmittable(boolean p_resubmittable)
    {
        i_isResubmittable = p_resubmittable;
    }
    
    /**
     * Returns the display name, for use in displaying batch jobs on BOI screen using a <code>JComboBox<code>.
     * @return The display name of this batch job type, intended for display in a BOI screen.
     */
    public String toString()
    {
        return i_displayName;   
    }

}