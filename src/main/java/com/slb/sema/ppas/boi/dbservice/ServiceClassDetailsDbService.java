////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ServiceClassDetailsDbService.java
//    DATE            :       31-Aug-2004
//    AUTHOR          :       Mario Imfeld
//    REFERENCE       :       PpacLon#470/3860
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Service Class Details screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiCurrencyFormatData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.boidata.BoiPromoPlanData;
import com.slb.sema.ppas.boi.boidata.BoiValidLanguageData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CmdfCustMastDefaultsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CufmCurrencyFormatsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrplPromoPlanSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ValaValidLanguageSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Service Class Details screen. 
 */
public class ServiceClassDetailsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------        

    /** Identifies the misc_table column in srva_misc_codes DB table. */
    public static final String C_MISC_TABLE_SERVICE_CLASS = "CLS";

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Vector of available markets for display. */
    private Vector i_availableMarketsV;
    
    /** Vector of miscellaneous code records. */
    private Vector i_availableMiscCodeDataV;    
    
    /** Miscellaneous code record currently being worked on. */
    private BoiMiscCodeData i_miscCodeData;

    /** Service Class Details record currently being worked on. */
    private CmdfCustMastDefaultsData i_cmdfData ;

    /** Currently selected Service Class Details code. Used as key for read and delete. */
    private String i_currentCode;    
    
    /** Vector of Currency Format objects. */
    private Vector i_availableCurrencyFormatDataV;    

    /** DataSet of Currency Format objects */
    CufmCurrencyFormatsDataSet i_currencyFormatDataSet;

    /** Currently selected Currency Format object. */
    private CufmCurrencyFormatsData i_currentCurrencySelection;        
    
    /** Vector of Promotion Plan records. */
    private Vector i_availablePromoPlanDataV;
    
    /** DataSet of Promotion Plan objects */
    private PrplPromoPlanDataSet i_promotionPlanDataSet;         

    /** Currently selected promotion plan object. */
    private PrplPromoPlanData i_currentPromoSelection;        
    
    /** Vector of Valid Language records. */
    private Vector i_availableValidLanguageDataV;

    /** DataSet of Language objects */
    private ValaValidLanguageDataSet i_validLanguageDataSet;         

    /** Currently selected language object. */
    private ValaValidLanguageData i_currentLanguageSelection;

    /** SQL service for Misc Code. */
    private MiscCodeSqlService i_miscCodeSqlService;    
    
    /** SQL service for Service Class Details. */
    private CmdfCustMastDefaultsSqlService i_cmdfSqlService;
    
    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService;    
    
    /** SQL service for Currency Formats data. */
    private CufmCurrencyFormatsSqlService i_cufmSqlService;

    /** SQL service for Promotion Plan data. */
    private PrplPromoPlanSqlService i_prplSqlService;    
    
    /** SQL service for Valid Language data. */
    private ValaValidLanguageSqlService i_valaSqlService;        
 
    /** Operator's default market. */
    private BoiMarket i_defaultMarket = null;    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public ServiceClassDetailsDbService(BoiContext p_context)
    {
        super(p_context);
        i_cmdfSqlService = new CmdfCustMastDefaultsSqlService (null, null);
        i_miscCodeSqlService = new MiscCodeSqlService (null, null);
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);        
        i_cufmSqlService = new CufmCurrencyFormatsSqlService (null);
        i_prplSqlService = new PrplPromoPlanSqlService (null, null);
        i_valaSqlService = new ValaValidLanguageSqlService (null);        
        i_availableMiscCodeDataV = new Vector(5);   
        i_availableCurrencyFormatDataV = new Vector(5);
        i_availablePromoPlanDataV = new Vector(5);
        i_availableValidLanguageDataV = new Vector(5);
        i_availableMarketsV = new Vector(5);
    }    
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }    
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }    
    
    /** 
     * Return available Misc Code data records. 
     * @return Vector of available Misc Code data records.
     */
    public Vector getAvailableMiscCodeData()
    {
        return i_availableMiscCodeDataV;
    }        

    /** 
     * Return Misc code data currently being worked on. 
     * @return Misc Code data object.
     */
    public BoiMiscCodeData getMiscCodeData()
    {
        return i_miscCodeData;
    }    
    
    /** 
     * Set MiscCode data currently being worked on. 
     * @param p_miscCodeData MiscCode data object.
     */
    public void setMiscCodeData(BoiMiscCodeData p_miscCodeData)
    {
        i_miscCodeData = p_miscCodeData;
    }

    /** 
     * Set current Service Class code. Used for record lookup in database. 
     * @param p_code Currently selected Service Class code. 
     */
    public void setCurrentCode(String p_code)
    {
        i_currentCode = p_code;
    }
    
    /** 
     * Return Service Class Details data currently being worked on. 
     * @return Service Class Details data object.
     */
    public CmdfCustMastDefaultsData getServiceClassDetailsData()
    {
        return i_cmdfData;
    }

    /** 
     * Set ServiceClassDetails data currently being worked on. 
     * @param p_serviceClassDetailsData Service Class Details data object.
     */
    public void setServiceClassDetailsData(CmdfCustMastDefaultsData p_serviceClassDetailsData)
    {
        i_cmdfData = p_serviceClassDetailsData;
    }

    /** 
     * Return available Currency Formats. 
     * @return Available Currency Format data records.
     */
    public Vector getAvailableCurrencyFormatsData()
    {
        return i_availableCurrencyFormatDataV;
    }
    
    /** 
     * Return Promotion Plan data. 
     * @return Vector of available Promotion Plan data records.
     */
    public Vector getAvailablePromoPlanData()
    {
        return i_availablePromoPlanDataV;
    }
    
    /** 
     * Return Valid Lanugage data. 
     * @return Vector of available Valid Language data records.
     */
    public Vector getAvailableValidLanguageData()
    {
        return i_availableValidLanguageDataV;
    }    
    
    /** 
     * Return currently selected promotion plan object. 
     * @return Currently selected promotion plan object.
     */
    public PrplPromoPlanData getCurrentPromoSelection()
    {
        return i_currentPromoSelection;
    }
    
    /** 
     * Return currently selected language object. 
     * @return Currently selected language object. 
     */
    public ValaValidLanguageData getCurrentLanguageSelection()
    {
        return i_currentLanguageSelection;
    }
    
    /** 
     * Return currently selected currency object. 
     * @return Currently selected currency object. 
     */
    public CufmCurrencyFormatsData getCurrentCurrencySelection()
    {
        return i_currentCurrencySelection;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        MiscCodeData                l_miscCodeData;        
        MiscCodeDataSet             l_miscCodeDataSet;
        CmdfCustMastDefaultsDataSet l_cmdfDataSet;        
        ServiceClass                l_serviceClass;        
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_miscCodeData = l_miscCodeDataSet.getRawDataFromKey(C_MISC_TABLE_SERVICE_CLASS,
                                                             i_currentMarket.getMarket(),
                                                             i_currentCode);
        i_miscCodeData = new BoiMiscCodeData(l_miscCodeData);
        
        l_cmdfDataSet = i_cmdfSqlService.read(null, p_connection);        
        l_serviceClass = new ServiceClass(new Integer(i_currentCode).intValue());
        i_cmdfData = l_cmdfDataSet.getData(i_currentMarket.getMarket(), l_serviceClass);

        if (i_cmdfData.getPromotionPlan() == null)
        {
            i_currentPromoSelection = null;
        }
        else
        {
            i_currentPromoSelection = i_promotionPlanDataSet.getWithCode(i_cmdfData.getPromotionPlan());
        }
        
        i_currentLanguageSelection = i_validLanguageDataSet.getWithCode(i_cmdfData.getDefaultLanguage());

        i_currentCurrencySelection = i_currencyFormatDataSet.getCurrencyFormatData(
                                                      i_cmdfData.getCmdfCurrency().getCurrencyCode());
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;

        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;        
        }
        
        refreshMiscCodeDataVector(p_connection, i_currentMarket.getMarket());        
        refreshCurrencyFormatDataVector(p_connection);
        refreshPromoPlanDataVector(p_connection);
        refreshValidLanguageDataVector(p_connection);        
    }
    
    /** 
     * Updates record in the SRVA_MISC_CODES and CMDF_CUST_MAST_DEFAULTS tables.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.update (null,
                                     p_connection,
                                     i_miscCodeData.getInternalMiscCodeData().getMarket().getSrva(),
                                     i_miscCodeData.getInternalMiscCodeData().getMarket().getSloc(),
                                     i_miscCodeData.getInternalMiscCodeData().getCode(),
                                     i_miscCodeData.getInternalMiscCodeData().getDescription(),
                                     i_context.getOperatorUsername(),
                                     C_MISC_TABLE_SERVICE_CLASS);
        
        refreshMiscCodeDataVector(p_connection, i_miscCodeData.getInternalMiscCodeData().getMarket());
        
        i_cmdfSqlService.update (null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_cmdfData.getDefaultLanguage(),
                                i_cmdfData.getCmdfCurrency().getCurrencyCode(),
                                i_cmdfData.getPromotionPlan(),
                                i_cmdfData.getCmdfMarket().getSrva(),
                                i_cmdfData.getCmdfMarket().getSloc(),
                                i_cmdfData.getCmdfCustClass().getValue() + "");
    }
    
    /** 
     * Marks records as deleted in the SRVA_MISC_CODES and CDFM_CUST_MAST_DEFAULTS tables.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.delete(null,
                                    p_connection,
                                    i_currentMarket.getMarket().getSrva(),
                                    i_currentMarket.getMarket().getSloc(),
                                    i_currentCode,
                                    i_context.getOperatorUsername(),
                                    C_MISC_TABLE_SERVICE_CLASS);        

        refreshMiscCodeDataVector(p_connection, i_currentMarket.getMarket());       
        
        i_cmdfSqlService.delete(null,
                                p_connection,
                                i_currentMarket.getMarket().getSrva(),
                                i_currentMarket.getMarket().getSloc(),
                                i_currentCode,
                                i_context.getOperatorUsername());
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        CmdfCustMastDefaultsDataSet l_cmdfDataSet;
        CmdfCustMastDefaultsData    l_cmdfData;
        ServiceClass                l_serviceClass;
        boolean[]                   l_flagsArray = new boolean[2];
        
        l_cmdfDataSet = i_cmdfSqlService.read(null, p_connection);
        l_serviceClass = new ServiceClass(new Integer(i_currentCode).intValue());        
        l_cmdfData = l_cmdfDataSet.getData(i_currentMarket.getMarket(), l_serviceClass);
        
        if (l_cmdfData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_cmdfData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;   
    }
    
    /** 
     * Inserts record into the SRVA_MISC_CODES and CDFM_CUST_MAST_DEFAULTS tables.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.insert(null,
                                    p_connection,
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSrva(),
                                    i_miscCodeData.getInternalMiscCodeData().getMarket().getSloc(),
                                    i_miscCodeData.getInternalMiscCodeData().getCode(),
                                    i_miscCodeData.getInternalMiscCodeData().getDescription(),
                                    i_context.getOperatorUsername(),
                                    C_MISC_TABLE_SERVICE_CLASS);

        refreshMiscCodeDataVector(p_connection,
                                  i_miscCodeData.getInternalMiscCodeData().getMarket());
        
        i_cmdfSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_cmdfData.getDefaultLanguage(),
                                i_cmdfData.getCmdfCurrency().getCurrencyCode(),
                                i_cmdfData.getPromotionPlan(),
                                i_cmdfData.getCmdfMarket().getSrva(),
                                i_cmdfData.getCmdfMarket().getSloc(),
                                i_cmdfData.getCmdfCustClass().getValue() + "");
    }
    
    /** 
     * Marks a previously withdrawn record as available in the SRVA_MISC_CODES and 
     * CDFM_CUST_MAST_DEFAULTS tables.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_miscCodeSqlService.markAsAvailable(null,
                                             p_connection,
                                             i_currentMarket.getMarket().getSrva(),
                                             i_currentMarket.getMarket().getSloc(),
                                             i_currentCode,
                                             i_context.getOperatorUsername(),
                                             C_MISC_TABLE_SERVICE_CLASS);

        refreshMiscCodeDataVector(p_connection,
                                  i_currentMarket.getMarket());
        
        i_cmdfSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_currentMarket.getMarket().getSrva(),
                                         i_currentMarket.getMarket().getSloc(),
                                         i_currentCode,
                                         i_context.getOperatorUsername());
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available miscellaneous code data vector from the 
     * srva_misc_codes table.
     * @param p_connection Database connection.
     * @param p_market Currently selected market.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshMiscCodeDataVector(JdbcConnection p_connection,
                                           Market         p_market)
        throws PpasSqlException
    {
        MiscCodeDataSet   l_miscCodeDataSet;
        MiscCodeDataSet   l_availableMiscCodeDataSet;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;
        
        i_availableMiscCodeDataV.removeAllElements();
        
        l_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        l_availableMiscCodeDataSet = l_miscCodeDataSet.getRawAvailableMiscCodeData(
                                                                  p_market, C_MISC_TABLE_SERVICE_CLASS);
        l_miscCodeV = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        
        i_availableMiscCodeDataV.addElement(new String(""));
        i_availableMiscCodeDataV.addElement(new String("NEW RECORD"));
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableMiscCodeDataV.addElement(l_boiMiscCodeArray[j]);
        }
    }    
    
    /** 
     * Refreshes the available Currency Format data vector from the 
     * cufm_currency_formats table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshCurrencyFormatDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        Vector                     l_cufmV;        
       
        i_availableCurrencyFormatDataV.removeAllElements();
        
        i_currencyFormatDataSet = i_cufmSqlService.readCurrencyFormats(null, p_connection);
        l_cufmV = i_currencyFormatDataSet.getAvailable();

        for (int i=0; i < l_cufmV.size(); i++)
        {
            i_availableCurrencyFormatDataV.addElement(
                             new BoiCurrencyFormatData((CufmCurrencyFormatsData)l_cufmV.get(i)));
        }
    }
    
    /** 
     * Refreshes the available Promotion Plan data vector from the 
     * prom_promotion table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshPromoPlanDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {        
        PrplPromoPlanData[]  l_prplArray;        
       
        i_availablePromoPlanDataV.removeAllElements();
        i_availablePromoPlanDataV.add(0, "");
        
        i_promotionPlanDataSet = i_prplSqlService.readAll(null, p_connection);
        l_prplArray = i_promotionPlanDataSet.getAvailableArray();
        
        for (int i=0; i < l_prplArray.length; i++)
        {
            i_availablePromoPlanDataV.addElement(new BoiPromoPlanData(l_prplArray[i]));
        }
    }
    
    /** 
     * Refreshes the available Valid Language data vector from the 
     * vala_valid_language table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshValidLanguageDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        ValaValidLanguageData[]  l_valaArray;
        
        i_availableValidLanguageDataV.removeAllElements();
        
        i_validLanguageDataSet = i_valaSqlService.readAll(null, p_connection);
        l_valaArray   = i_validLanguageDataSet.getAvailableArray();
        
        for (int i=0; i < l_valaArray.length; i++)
        {
            i_availableValidLanguageDataV.addElement(new BoiValidLanguageData(l_valaArray[i]));
        }
    }
    
    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}