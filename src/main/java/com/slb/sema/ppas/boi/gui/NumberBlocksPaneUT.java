////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NumberBlocksPaneUT.java
//      DATE            :       21-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1216/5578
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for NumberBlocksPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for NumberBlocksPane. */
public class NumberBlocksPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "NumberBlocksPaneUT";
    
    /** Constant used for MSISDN start number. */
    private static final String C_START_NUMBER = "1234500000"; 
    
    /** Constant used for MSISDN end number. */
    private static final String C_END_NUMBER = "1234599999"; 
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Number Blocks screen. */
    private NumberBlocksPane i_nublPane;
    
    /** Market combo box. */
    private JComboBox i_marketDataComboBox;
    
    /** Text field to hold the number block start msisdn. */
    private JFormattedTextField i_startNumber;
    
    /** Text field to hold the number block end msisdn. */
    private JFormattedTextField i_endNumber;

    /** Button for adding a number block. */
    private JButton i_addBlockButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public NumberBlocksPaneUT(String p_title)
    {
        super(p_title);

        i_nublPane = new NumberBlocksPane(c_context);
        super.init(i_nublPane);
        
        i_nublPane.resetScreenUponEntry();
        
        i_marketDataComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "marketBox");
        i_startNumber = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "startNumber");
        i_endNumber = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "endNumber");
        i_addBlockButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "addBlockButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(NumberBlocksPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert a record through the Number Blocks screen.
     * @ut.then the operation is successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("NumberBlocksPane test");
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        if (i_marketDataComboBox.getComponentCount() == 0)
        {
            fail("No markets configured.");
        }
        
        SwingTestCaseTT.setTextComponentValue(i_startNumber, C_START_NUMBER);
        SwingTestCaseTT.setTextComponentValue(i_endNumber, C_END_NUMBER);
        doInsert(i_nublPane, i_addBlockButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteNublRecord(C_START_NUMBER, C_END_NUMBER);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteNublRecord(C_START_NUMBER, C_END_NUMBER);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
        
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteNublRecord = "deleteNublRecord";
    
    /**
     * Removes a row from NUBL_NUMBER_BLOCK.
     * @param p_startNumber Number of the first MSISDN in the range.
     * @param p_endNumber Number of the last MSISDN in the range.
     */
    private void deleteNublRecord(String p_startNumber,
                                  String p_endNumber)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = "DELETE from nubl_number_block " +
                "WHERE nubl_start_number = {0} " +
                "AND   nubl_end_number = {1} ";
        
        l_sqlString = new SqlString(200, 2, l_sql);
        
        l_sqlString.setStringParam(0, p_startNumber);
        l_sqlString.setStringParam(1, p_endNumber);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteNublRecord);
    }
}
