////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiLoginPane.java
//      DATE            :       14-Jan-2003
//      AUTHOR          :       M I Erskine
//      REFERENCE       :       PpacLon#112/1305
//
//      COPYRIGHT       :       SchlumbergerSema 2003
//
//      DESCRIPTION     :       BOI Login screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.swing.components.GridLayoutManager;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.ValidatedJPasswordField;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.util.crypto.Blowfish;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Boi Login screen class.
 */
public class BoiLoginPane extends FocussedGuiPane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "BoiLogin";

    /** Panel to contain the fields panel. */
    private JPanel                   i_loginPanel;
    
    /** Panel to contain the login fields. */
    private JPanel                   i_fieldsPanel;
    
    /** Field to contain the username. */
    private ValidatedJTextField      i_username;
    
    /** Field to contain the password. */
    private ValidatedJPasswordField  i_password;
    
    /** Button to attempt login to BOI. */
    private GuiButton                i_loginButton;
    
    /** Button to reset the username and password text fields. */
    private GuiButton                i_resetButton;
    
    /** The name of the host server. */
    private String                   i_serverHost   = "";
    
    /** The port on which to log in. */
    private String                   i_loginPort    = "";

    /** The main panel for this screen. */
    private JPanel                   i_mainPanel;
    
    /** Combo box containing help topics for the screen. */
    private JComboBox                i_helpComboBox;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor to create a new instance of BoiLogin.
     * @param p_context  Context object for BOI
     */
    public BoiLoginPane(BoiContext p_context)
    {
        super(p_context, (Container)null);

        i_serverHost = (String)i_context.getMandatoryObject(
                "applet.server.host");

        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                10000, 
                "Server host from context = " + i_serverHost);
        }
                
        i_loginPort = (String)i_context.getMandatoryObject(
            "applet.server.login.port");
                
        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                10010, 
                "Login Port from Context = " + i_loginPort);
        }
            
        init();
    }

    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------

    /**
     * Event handler method for GUI events.
     * @param p_event Event to handle.
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing.
    }
    
    /**
     * Returns Login button.
     * @return Login button.
     */
    public JButton getLoginButton()
    {
        return(i_loginButton);
    }
    
    /**
     * Sets the field that is to have the default focus on entering the screen.
     */
    public void defaultFocus()
    {
        i_username.setVerifyInputWhenFocusTarget(false);
        i_username.requestFocusInWindow();
        i_username.setVerifyInputWhenFocusTarget(true);
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        getContentPane().setVisible(true);
        i_username.getParent().getParent().setVisible(false);
        i_username.getParent().getParent().setVisible(true);
        i_resetButton.doClick();
    }
    
    /**
     * Method to perform an action depending on an action event supplied.
     * @param p_event The ActionEvent object that caused this action
     */
    public void actionPerformed(ActionEvent p_event)
    {
        Object l_source;

        l_source = p_event.getSource();

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 11000, "Got action:" + p_event.getActionCommand());
        }
        
        if (l_source == i_loginButton)
        {
            if (Debug.on)
            {
                Debug.print(C_CLASS_NAME, 11010, "Login selected");
            }

            if (i_username.getText().equals("") || new String(i_password.getPassword()).equals(""))
            {
                displayMessageDialog(
                    i_contentPane,
                    "You must enter both a username and a " + "password before pressing [LOGIN]",
                    false,
                    false);

                if (i_username.getText().equals(""))
                {
                    i_username.requestFocusInWindow();
                }
                else
                {
                    i_password.requestFocusInWindow();
                }
            }
            else
            {
                doLogin();
            }
        }
        else if (l_source == i_resetButton)
        {
            if (Debug.on)
                Debug.print(C_CLASS_NAME, 11020, "Reset selected");

            reset();
        }
        else
        {
            if (Debug.on)
                Debug.print(C_CLASS_NAME, 11030, "Unknown action performed");
        }
    }
    
    /** 
     * Must implement this inherited method. 
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        // Do nothing.
    }

    /** 
     * Key event handler.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;

        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()))
        {
            if (p_keyEvent.getKeyCode() == 10)
            {
                if (p_keyEvent.getID() == KeyEvent.KEY_PRESSED)
                {
                    l_return = true;
                    i_loginButton.doClick();
                }
            }
        }

        return l_return;
    }

    /** 
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event){}
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event){}

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Method used to perform the basic setup of the screen, etc.
     */
    private void init()
    {
        JLabel          l_usernameLabel;
        JLabel          l_passwordLabel;
        BoiHelpListener l_helpComboListener;
        
        i_mainPanel = new JPanel();
        i_mainPanel.setLayout(new GridLayoutManager(100, 100, 0, 0));
        
        i_mainPanel.setBackground(WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        i_mainPanel.setVisible(true);
        i_mainPanel.setFocusable(false);
        i_mainPanel.setRequestFocusEnabled(false);
                
        i_loginPanel = WidgetFactory.createMainPanel("Log In", 100, 35, 0, 0);
        i_loginPanel.setVisible(true);
        i_loginPanel.setFocusable(false);
        i_loginPanel.setRequestFocusEnabled(false);
        
        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                            BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_LOGIN_SCREEN), 
                                            l_helpComboListener);
        i_helpComboBox.setFocusable(false);
        
        i_fieldsPanel = WidgetFactory.createPanel(50, 30, 0, 0);
        
        l_usernameLabel = WidgetFactory.createLabel("User ID :");
        l_passwordLabel = WidgetFactory.createLabel("Password :");
        l_passwordLabel.setForeground(WidgetFactory.C_COLOUR_FIELDS);
            
        i_username = WidgetFactory.createTextField(8);
        i_username.setVisible(true);
        i_username.setEnabled(true);
        i_username.setFocusable(true);
        i_username.setFocusAccelerator('u');
        i_username.setRequestFocusEnabled(true);

        i_password = WidgetFactory.createPasswordField(21);
        i_password.setVisible(true);
        i_password.setEnabled(true);
        i_password.setFocusable(true);
        i_password.setFocusAccelerator('p');
        i_password.setRequestFocusEnabled(true);

        i_loginButton = WidgetFactory.createButton("Login", this, true);
        i_resetButton = WidgetFactory.createButton("Reset", this, false);

        i_fieldsPanel.add(l_usernameLabel, "usernameLabel,1,5,15,5");
        i_fieldsPanel.add(l_passwordLabel, "passwordLabel,1,11,15,5");
        i_fieldsPanel.add(i_username, "username,17,5,8,5");
        i_fieldsPanel.add(i_password, "password,17,11,14,5");
        i_fieldsPanel.add(i_loginButton, "loginButton,1,26,15,5");
        i_fieldsPanel.add(i_resetButton, "resetButton,17,26,15,5");
        i_fieldsPanel.setFocusCycleRoot(true);
        
        i_loginPanel.add(i_helpComboBox, "loginHelpComboBox,60,1,40,4");
        i_loginPanel.add(i_fieldsPanel, "fieldsPanel,1,6,50,30");
        i_mainPanel.add(i_loginPanel, "loginPanel,1,1,100,35");
        i_mainPanel.setVisible(true);

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_contentPane = i_mainPanel;
    }

    /** Attempts to login the Operator. */
    private void doLogin()
    {
        URLConnection       l_urlConnection;
        URL                 l_loginUrl;
        InputStream         l_is;
        BufferedReader      l_bir;
        String[]            l_tokenizer = null;
        int                 l_nextToken = 0;

        String              l_line           = "";
        String              l_param;
        String              l_oracleUserName = "";
        String              l_oraclePassword = "";
        String              l_status         = "";
        String              l_loginUrlString;
        String              l_oracleDbUrl    = "";
        
        byte[]              l_decryptedOracleDbUrlByteArray = null;
        byte[]              l_decryptedOracleUserNameArray = null;
        byte[]              l_decryptedOraclePasswordArray = null;
        
        String              l_decryptedOracleDbUrl    = "";
        String              l_decryptedOracleUserName = "";
        String              l_decryptedOraclePassword = ""; 
        Blowfish            l_blowfish;
        
        try
        {
            l_loginUrlString = "http://" + i_serverHost + ":" + i_loginPort +
                    "/ascs/boi/Login?command=authenticate&userName=" +
                    i_username.getText() + "&password=" +
                    new String(i_password.getPassword());
                    
            l_loginUrl = new URL(l_loginUrlString);
            l_urlConnection = l_loginUrl.openConnection();
            l_urlConnection.connect();
            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));
             
            l_line = l_bir.readLine();
            
            if (l_line != null)
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12010, 
                        "Read line " + l_line);
                }
            }
            else
            {
                l_line = "";
            }
            
            l_tokenizer = l_line.split("[=& ]");
            while (l_nextToken < l_tokenizer.length)
            {
                l_param = l_tokenizer[l_nextToken++];
                if ("status".equals(l_param))
                {
                    l_status = l_tokenizer[l_nextToken++];
                }
                else if ("userName".equals(l_param))
                {
                    l_oracleUserName = l_tokenizer[l_nextToken++];
                }
                else if ("password".equals(l_param))
                {
                    l_oraclePassword = l_tokenizer[l_nextToken++];
                }
                else if ("dbUrl".equals(l_param))
                {
                    l_oracleDbUrl = l_tokenizer[l_nextToken++]; 
                }
            }
            
            if (Debug.on)
            {
                Debug.print(
                    C_CLASS_NAME,
                    12030,
                    "Now Blowfish decrypt");
            }
            
            l_blowfish = new Blowfish(null);
            
            l_blowfish.setSlbBlowfishKey();
            
            l_decryptedOracleUserNameArray = l_blowfish.decryptString(l_oracleUserName);
            l_decryptedOracleUserName = (new String(l_decryptedOracleUserNameArray)).trim();
            
            l_decryptedOraclePasswordArray = l_blowfish.decryptString(l_oraclePassword);
            l_decryptedOraclePassword = (new String(l_decryptedOraclePasswordArray)).trim();
            
            l_decryptedOracleDbUrlByteArray = l_blowfish.decryptString(l_oracleDbUrl);
            l_decryptedOracleDbUrl = (new String(l_decryptedOracleDbUrlByteArray)).trim();
            
            if ("SUCCESS".equals(l_status))
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12070, 
                        "Login successful");
                }

                ((BoiContext)i_context).setUsername(l_decryptedOracleUserName);
                ((BoiContext)i_context).setPassword(l_decryptedOraclePassword);
                ((BoiContext)i_context).setDbString(l_decryptedOracleDbUrl);
                
                ((BoiContext)i_context).setOperatorUsername(i_username.getText());
                ((BoiContext)i_context).setOperatorPassword(String.valueOf(i_password.getPassword()));
                
                ((BoiContext)i_context).createConnection();
                
                notifyEvent(new GuiEvent("LOGGED_IN",
                                         this,
                                         ((BoiContext)i_context).getOpidPrivilege()));
            }
            else if ("PASSWORD_EXPIRED".equals(l_status))
            {
                ((BoiContext)i_context).setUsername(l_decryptedOracleUserName);
                ((BoiContext)i_context).setPassword(l_decryptedOraclePassword);
                ((BoiContext)i_context).setDbString(l_decryptedOracleDbUrl);
                
                ((BoiContext)i_context).setOperatorUsername(i_username.getText());
                ((BoiContext)i_context).setOperatorPassword(String.valueOf(i_password.getPassword()));
                
                ((BoiContext)i_context).createConnection();
                
                displayMessageDialog(i_contentPane,
                                     "Your password has expired. Please enter a new one.",
                                     false,
                                     false);
                
                notifyEvent(new GuiEvent("PASSWORD_EXPIRED",
                                         this,
                                         ((BoiContext)i_context).getOpidPrivilege()));
            }
            else
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12080, 
                        "Login failed");
                }
                     
                displayMessageDialog(i_contentPane,
                                     "Login failed - invalid username and/or password",
                                     false,
                                     false);
            }
            
            reset();
        }
        catch (PpasSqlException l_e)
        {
            handleException(l_e, 
                            "Could not establish a connection with the database.\n" +
                            "Contact system administrator.");
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e, 
                            "Could not log into BOI.\nContact system administrator.");
        }
        catch (IOException l_e)
        {
            handleException(l_e, 
                            "Could not log into BOI.\nContact system administrator.");
        }
    }

    /**
     * Private method to set the username and password fields to
     * be empty.
     */
    private void reset()
    {
        i_username.setText("");
        i_password.setText("");
        i_username.requestFocusInWindow();
    }
}
