///////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AccumulatedBonusPane.java
//  DATE            :       15th July 2007
//  AUTHOR          :       Steve James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       A BOI screen to configure accumulated bonus schemes and elements
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE    | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
// 03/08/07| M Erskine  | Add table displaying bonus      | PpacLon#3256/11931
//         |            | element matching criteria.      |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;

import com.slb.sema.ppas.boi.boidata.BoiAccountGroupsData;
import com.slb.sema.ppas.boi.boidata.BoiAccumulatedBonusSchemeData;
import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiBonusElementData;
import com.slb.sema.ppas.boi.boidata.BoiBonusSchemeData;
import com.slb.sema.ppas.boi.boidata.BoiChavChannelData;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.boidata.BoiServiceOfferingData;
import com.slb.sema.ppas.boi.dbservice.AccumulatedBonusDbService;
import com.slb.sema.ppas.boi.dbservice.BoiDbService;
import com.slb.sema.ppas.boi.dbservice.BonusDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.swing.components.FixedSizeNumberFormatter;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJPasswordField;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

/**
 * A BOI screen to configure bonus schemes and elements
 */
public class AccumulatedBonusPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Class attributes
    //-------------------------------------------------------------------------
    
    /** Index of channel for ordering of key elements. */
    private static int c_channelIndex = -1;
    
    /** Index of top-up type for ordering of key elements. */
    private static int c_topUpTypeIndex = -1;
    
    /** True if debug is to be switched on. */
    private static boolean c_debug = false;
        
    /** Default Non Key Elements value for Accumulated Bonus schemes */
    private static final String C_NON_KEY_ELEMENTS_VALUE = "0";
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Property change listener for JFormattedTextField objects. */
    protected GuiPropertyChangeListenerEl i_guiPropertyChangeListenerEl;
    
    /** Document listener for JTextField objects. */
    protected GuiDocumentListenerEl i_guiDocumentListenerEl;
    
    /** Item listener for buttons, check boxes, and combo boxes. */
    protected GuiItemListenerEl i_guiItemListenerEl;

    /** Panel to display current bonus schemes. */
    private JPanel i_bonusSchemesPanel = null;
    
    /** Panel to display current bonus elements. */
    private JPanel i_definedBonusElementsPanel = null;    

    /** Vector of existing bonus schemes. */
    private Vector i_definedBonusSchemes = null;
    
    /** Vector of existing bonus elements. */
    private Vector i_definedBonusElements = null;
    
    /** Vector of current channel values. */
    private Vector i_definedChannelValues = new Vector(5);
    
    /** Vector of current account groups. */
    private Vector i_definedAccountGroups = new Vector(5);
    
    /** Combo box for selecting database records. */
    protected JComboBox i_keyElementDataComboBox;
    
    /** Combo box model for selecting bonus element database records. */
    protected DefaultComboBoxModel i_keyDataModelElements;
    
    /** Combo box model for selecting service offerings. */
    protected DefaultComboBoxModel i_serviceOfferingsModel;
    
    //
    // Fields for bonus scheme id panel
    //
    
    /** Text field for bonus scheme id. */
    protected ValidatedJTextField i_bonusSchemeId      = null;
    
    /** Text field for bonus scheme description. */
    protected ValidatedJTextField i_bonSchemeDescField = null;
    
    /** Text field for bonus scheme start date. */
    protected JFormattedTextField i_startDateField     = null;
    
    /** Text field for bonus scheme end date. */
    protected JFormattedTextField i_endDateField       = null;
    
    /** Text field for bonus scheme opt-in service offering. */
    protected JComboBox           i_serviceOfferingsCombo = null;
    
    /** Vector of available service offerings. */
    protected Vector              i_serviceOfferings   = new Vector();
    
    /** Check box to indicate whether scheme is active. */
    protected JCheckBox           i_activeSchemeIndCheckBox  = null;

    /** Panel to hold the radio buttons for refill target type selection */
    protected JPanel i_refillTargetRadioPanel = null;

    /** Panel to hold the radio button panel for refill target type selection */
    protected JPanel i_refillTargetPanel = null;
    
    /**
     * Vector to hold available adjustment types. These will be the adjustment types applicable for 
     * the operator's default market.
     */
    protected Vector   i_adjustmentTypes = null;
    
    /** Vector to hold available adjustment codes. */ 
    protected Vector   i_adjustmentCodes = null;

    /** Panel to hold the radio buttons for bonus type selection */
    protected JPanel i_bonusRadioPanel = null;

    /** Panel to hold the radio button panel for bonus type selection */
    protected JPanel i_bonusPanel = null;
    
    /** Contains the bonus. Note that the same field is used for both amount and percentage. */
    protected JFormattedTextField i_bonusAmountOrPercentField = null;

    /** Radio button to select refill value target. */
    private JRadioButton i_bonusAmountRadioButton;
    
    /** Radio button to select refill count target. */
    private JRadioButton i_bonusPercentageRadioButton;
    
    /** Check box to indicate whether element is active. */
    protected JCheckBox i_activeElementIndCheckbox = null;
    
    /** Combo-box containing account choice. */
    protected JComboBox i_accountChoiceCombo = null;
    
    /** Text field containing expiry days for bonus element. */
    protected JFormattedTextField i_expiryDaysField = null;
    
    /** Combo-box containing adjustment types. */
    protected JComboBox i_adjustmentTypesCombo = null;
    
    /** Combo-box containing adjustment codes. */
    protected JComboBox i_adjustmentCodesCombo = null;
    
    /** Contains the refill target. Note that the same field is used for both value and count. */
    protected JFormattedTextField i_refillTargetValueOrCountField = null;
    
    /** Radio button to select refill value target. */
    private JRadioButton i_refillValueTargetRadioButton;
    
    /** Radio button to select refill count target. */
    private JRadioButton i_refillCountTargetRadioButton;
    
    /** Contains the counting period in days. */
    protected JFormattedTextField i_countingPeriodField = null;
    
    /** Check box to indicate whether bonuses to ded. account are applied
     *  as an adjustment to the existing balance or a 'set' to set the
     *  balance to the bonus account.
     */
    protected JCheckBox i_isSetDedicatedAccount = null;

    //
    // Fields for bonus elements panel
    //
    
    /** Combo-box of channel values. */
    protected JComboBox           i_element1Field = null;
    
    /** Contains nothing in initial implementation. */
    protected ValidatedJTextField i_element2Field = null;
    
    /** Combo-box of top-up types. */
    protected JComboBox           i_element3Field = null;
    
    /** Listener class for events occurring in the key element data combo-box. */ 
    protected KeyElementDataComboListener i_keyElementDataComboListener = null;
    
    /** Listener class for events occurring to (UPDATE,DELETE,RESET) buttons in the bonus element panel. */ 
    protected BonusElementButtonListener i_bonusElementButtonListener = null;
    
    /** Update button for Elements panel. */
    protected GuiButton i_updateButtonEl;
    
    /** Delete button for Elements panel. */
    protected GuiButton i_deleteButtonEl;
    
    /** Reset button for Elements panel. */
    protected GuiButton i_resetButtonEl;
    
    /** Elements config from server. */
    protected Object i_elements;
    
    /** Data model for table of existing records. */
    private StringTableModel i_stringTableModel;
    
    /** The vertical scroll bar used for the bonus elements table view port. */
    private JScrollBar i_verticalScrollBar;
    
    /** Table containing existing element records. */
    private JTable i_table = null;

    /** Column names for table of existing records. */
    private String[] i_columnNames = { "Channel",
                                       "",
                                       "TopUpType",
                                       "Active?"};
    
    /** Data array for table of existing bonus element records. */
    private String i_data[][] = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    
    /** 
     * BonusPane constructor. 
     * @param p_context A reference to the BoiContext
     */        
    public AccumulatedBonusPane(Context p_context)
    {
        super(p_context, (Container)null);
        i_guiPropertyChangeListenerEl = new GuiPropertyChangeListenerEl();
        i_guiDocumentListenerEl = new GuiDocumentListenerEl();
        i_guiItemListenerEl = new GuiItemListenerEl();
        
        i_dbService = new AccumulatedBonusDbService((BoiContext)i_context);        
        
        i_bonusElementButtonListener = new BonusElementButtonListener();
        
        i_updateButtonEl = WidgetFactory.createButton("Update", i_bonusElementButtonListener, false);
        i_deleteButtonEl = WidgetFactory.createButton("Delete", i_bonusElementButtonListener, false);
        i_resetButtonEl = WidgetFactory.createButton("Reset", i_bonusElementButtonListener, false);
        
        // Override creation in super class => don't want a default button on this screen.
        i_updateButton = WidgetFactory.createButton("Update", i_commonButtonListener, false);
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------    

    /**
     * Handles keyboard events for this screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean  l_return = false;
            
        if( i_contentPane.isAncestorOf(p_keyEvent.getComponent()) &&
            p_keyEvent.getKeyCode() == 10 &&
            p_keyEvent.getID() == KeyEvent.KEY_PRESSED )
        {
            if (p_keyEvent.getComponent() == i_deleteButton)
            {
                i_deleteButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_updateButton)
            {
                i_updateButton.doClick();
            }
            else if (p_keyEvent.getComponent() == i_deleteButtonEl)
            {
                i_deleteButtonEl.doClick();
            }
            else if (p_keyEvent.getComponent() == i_resetButtonEl)
            {
                i_resetButtonEl.doClick();
            }
            else if (p_keyEvent.getComponent() == i_updateButtonEl)
            {
                i_updateButtonEl.doClick();
            }
            l_return = true;
        }
        return l_return;
    }
    
    /**
     * Handles mouse released event on bonus elements table.
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedBonusElements.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyElementDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_bonusSchemeId.setEnabled(true);
    }
    
    /**
     * Alters screen behaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_bonusSchemeId.setEnabled(false);
    }
    
    /**
     * Alters screen bevaviour when NEW ELEMENT is selected from the key element data dropdown.
     */
    protected void updateScreenForNewElementRecord()
    {
        i_element1Field.setEnabled(true);
        i_element2Field.setEnabled(true);
        i_element3Field.setEnabled(true);
    }
    
    /**
     * Alters screen appearance when an existing record is selected from the key element data dropdown.
     */
    protected void updateScreenForExistingElementRecord()
    {
        i_element1Field.setEnabled(false);
        i_element2Field.setEnabled(false);
        i_element3Field.setEnabled(false);
    }
    
    /**
     * Invoked when an item has been selected or deselected by the user.
     * @param p_itemEvent Event to be handled.
     */    
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        if (c_debug) System.out.println("Entered BonusPane.itemStateChanged");
        if (p_itemEvent.getSource() == i_adjustmentTypesCombo)
        {
            Object l_currentAdjType = i_adjustmentTypesCombo.getSelectedItem();
            if (l_currentAdjType instanceof BoiMiscCodeData)
            {            
                ((BonusDbService)i_dbService).setCurrentAdjType(
                    ((BoiMiscCodeData)l_currentAdjType).getInternalMiscCodeData().getCode());
            }
            else
            {
                ((BonusDbService)i_dbService).setCurrentAdjType(null);
            }
            
            try
            {
                ((BonusDbService)i_dbService).refreshAdjustmentCodesDataVector();
            }
            catch (PpasServiceFailedException l_e)
            {
                handleException(l_e);
            }
            i_adjustmentCodesCombo.setModel(
                new DefaultComboBoxModel(((BonusDbService)i_dbService).getAvailableAdjustmentCodesDataV()));
        }
        else if (p_itemEvent.getSource() == i_accountChoiceCombo)
        {
             int l_accountNumber = Integer.parseInt(i_accountChoiceCombo.getSelectedItem().toString());
             if(l_accountNumber == 0)
             {
                 i_expiryDaysField.setValue(null);
                 i_expiryDaysField.setEnabled(false);
                 i_isSetDedicatedAccount.setSelected(false);
                 i_isSetDedicatedAccount.setEnabled(false);

             }
             else
             {
                 i_expiryDaysField.setEnabled(true);
                 i_isSetDedicatedAccount.setEnabled(true);

             }
        }
        if (c_debug) System.out.println("Leaving BonusPane.itemStateChanged");
    }

    /**
     * Handles ActionEvents produced by the Schemes pane.
     * @param p_event The <code>ActionEvent</code> to handle
     */
    public void actionPerformed(ActionEvent p_event)
    {
        if (c_debug) System.out.println("Entered BonusPane.actionPerformed");
        if (p_event.getSource() == i_refillValueTargetRadioButton)
        {
            FixedSizeNumberFormatter l_formatter = getNewFormatter(new DecimalFormat("0.000"),
                                                                   12,
                                                                   3);

            i_refillTargetValueOrCountField.setFormatterFactory(
                                                new DefaultFormatterFactory(l_formatter));
        }
        else if (p_event.getSource() == i_refillCountTargetRadioButton)
        {
            FixedSizeNumberFormatter l_formatter = getNewFormatter(new DecimalFormat("0"),
                                                                   3,
                                                                   0);
            
            i_refillTargetValueOrCountField.setFormatterFactory(
                                                new DefaultFormatterFactory(l_formatter));
        }
        else if (p_event.getSource() == i_bonusAmountRadioButton)
        {
            FixedSizeNumberFormatter l_formatter = getNewFormatter(new DecimalFormat("0.000"),
                                                                   12,
                                                                   3);

            i_bonusAmountOrPercentField.setFormatterFactory(
                                                new DefaultFormatterFactory(l_formatter));
        }
        else if (p_event.getSource() == i_bonusPercentageRadioButton)
        {
            FixedSizeNumberFormatter l_formatter = getNewFormatter(new DecimalFormat("0.00"),
                                                                   3,
                                                                   2);
            i_bonusAmountOrPercentField.setFormatterFactory(
                                                new DefaultFormatterFactory(l_formatter));
        }
        if (c_debug) System.out.println("Leaving BonusPane.actionPerformed");
    }

    //-------------------------------------------------------------------------
    // Protected methods (mostly overriding abstract methods in superclass)
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        if (c_debug) System.out.println("Entered initialiseScreen()");
        i_bonusSchemeId.setText("");
        i_bonSchemeDescField.setText("");
        i_startDateField.setText("");
        i_endDateField.setText("");
        i_serviceOfferingsCombo.setSelectedIndex(0);
        i_activeSchemeIndCheckBox.setSelected(false);
        i_bonusAmountOrPercentField.setValue(null);
        i_bonusAmountRadioButton.setSelected(true);
        
        i_accountChoiceCombo.setSelectedIndex(0);
        i_expiryDaysField.setValue(null);
        i_expiryDaysField.setEnabled(false);
        i_isSetDedicatedAccount.setEnabled(false);
        i_activeElementIndCheckbox.setSelected(false);
        i_adjustmentTypesCombo.setSelectedIndex(0);
        i_adjustmentCodesCombo.setSelectedIndex(0);
        i_refillTargetValueOrCountField.setValue(null);
        i_refillValueTargetRadioButton.setSelected(true);
        i_countingPeriodField.setValue(null);
        
        ((BonusDbService)i_dbService).setBonusSchemeData((BoiBonusSchemeData)null);
        ((AccumulatedBonusDbService)i_dbService).setBonusSchemeData((BoiAccumulatedBonusSchemeData)null);
        try
        {
            ((BonusDbService)i_dbService).refreshServiceOfferingVector();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
        if (c_debug) System.out.println("Leaving initialiseScreen()");
    }
    
    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseElementsPanel()
    {
        if (c_debug) System.out.println("Entered initialiseElementsPanel()");
        
        if ((i_keyDataComboBox.getSelectedIndex() == 0) || (i_keyDataComboBox.getSelectedIndex() == 1))
        {
            i_keyElementDataComboBox.setSelectedIndex(0);
        }
        
        i_element1Field.setSelectedIndex(0);
        i_element2Field.setText("");
        i_element3Field.setSelectedIndex(0);

        i_activeElementIndCheckbox.setSelected(false);
        i_table.clearSelection();

        if (i_keyDataComboBox.getSelectedIndex() == 0 || i_keyDataComboBox.getSelectedIndex() == 1)
        {
            setElementsPanelFieldsEnabledStatus(false);
        }
        else
        {
            setElementsPanelFieldsEnabledStatus(true);
        }
        
        populateCodesTable();
        i_verticalScrollBar.setValue(0);
        ((BonusDbService)i_dbService).setBonusElementData(null);
        
        i_state2 = C_STATE_INITIAL;
        if (c_debug) System.out.println("Leaving initialiseElementsPanel()");
    }
    
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Accumulated Bonus Schemes", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_ACCUMULATED_BONUS_SCREEN), 
                                i_helpComboListener);

        createBonusSchemesPanel();
        createBonusElementModificationPanel();
        
        i_mainPanel.add(i_helpComboBox, "bonusHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_bonusSchemesPanel, "bonusSchemesPanel,1,6,100,50");
        i_mainPanel.add(i_definedBonusElementsPanel, "definedBonusElementsPanel,1,57,100,44");
        
        Object l_tubsElements = ((BoiContext)i_context).getObject("ascs.boi.tubsElements");
        
        if (l_tubsElements == null || ((String)l_tubsElements).trim().equals(""))
        {
            displayMessageDialog(i_contentPane, 
                "Necessary configuration is missing from the server. " + 
                "Please contact System Administrator.");
        }
    }
    
    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {   
        BonsBonusSchemeData l_bonusSchemeData;
        BonaBonusAccumParamsData l_accumBonusSchemeData;
        String l_bonsId; 
        PpasDate l_endDate;

        if (c_debug) System.out.println("Entered refreshCurrentRecord()");
      
        l_bonusSchemeData = ((BonusDbService)i_dbService).getBonusSchemeData().getInternalBonusSchemeData();
        l_bonsId    = l_bonusSchemeData.getBonusSchemeId();
        i_bonusSchemeId.setText(l_bonsId);
        i_bonSchemeDescField.setText(l_bonusSchemeData.getDesc());
        if (c_debug) System.out.println("    l_bonusSchemeData.getDesc: " + l_bonusSchemeData.getDesc());
        i_startDateField.setText(l_bonusSchemeData.getStartDate().toString());
        
        l_endDate = l_bonusSchemeData.getEndDate() != null ? l_bonusSchemeData.getEndDate() : new PpasDate();
        i_endDateField.setText(l_endDate.toString());
        i_activeSchemeIndCheckBox.setSelected(l_bonusSchemeData.isBonusActive());
        for (int i=1; i< i_serviceOfferings.size(); i++)
        {
            Long l_seof = l_bonusSchemeData.getOptinServOff();
            
            if (l_seof != null && (i_serviceOfferings.elementAt(i) instanceof BoiServiceOfferingData))
            {
                if (convertIntToSeofBit(l_seof.intValue()) ==
                       ((BoiServiceOfferingData)i_serviceOfferings.elementAt(i)).
                           getInternalSeofData().getServiceOfferingNumber())
                {
                    i_serviceOfferingsCombo.setSelectedItem(i_serviceOfferings.elementAt(i));
                }
            }
            else
            {
                i_serviceOfferingsCombo.setSelectedItem("");
            }
        }

        // get Accumulated Bonus params
        l_accumBonusSchemeData = ((AccumulatedBonusDbService)i_dbService).getAccumulatedBonusSchemeData().getInternalBonusSchemeData();
        
        Object l_amPer = null;
        
        if(l_accumBonusSchemeData.getBonusAmount() != null)
        {
            l_amPer = l_accumBonusSchemeData.getBonusAmount();
        }
        else
        {
            l_amPer = l_accumBonusSchemeData.getBonusPercent();
        }
        i_bonusAmountOrPercentField.setValue(l_amPer);

        
        if(l_accumBonusSchemeData.getBonusAmount() != null)
        {
            i_bonusAmountRadioButton.doClick();
        }
        else
        {
            i_bonusPercentageRadioButton.doClick();
        }

        i_accountChoiceCombo.setSelectedItem(String.valueOf(l_accumBonusSchemeData.getDedAccId()));

        i_expiryDaysField.setValue(l_accumBonusSchemeData.getDedAccExpiryDays());
 
        for (int i=1; i< i_adjustmentTypes.size(); i++)
        {
            if (l_accumBonusSchemeData.getAdjType().equals(
                    ((BoiMiscCodeData)i_adjustmentTypes.elementAt(i)).getInternalMiscCodeData().getCode()))
            {
                i_adjustmentTypesCombo.setSelectedItem(i_adjustmentTypes.elementAt(i));
            }
        }
        
        for (int i=1; i<i_adjustmentCodes.size(); i++)
        {
            if (l_accumBonusSchemeData.getAdjCode().equals(
                    ((BoiAdjCodesData)i_adjustmentCodes.elementAt(i)).
                        getInternalAdjCodesData().getAdjCode()))
            {
                i_adjustmentCodesCombo.setSelectedItem(i_adjustmentCodes.elementAt(i));
            }
        }
        
        Object l_valCount = null;
        if (l_accumBonusSchemeData.getRefillValueTarget() != null)
        {
            l_valCount = l_accumBonusSchemeData.getRefillValueTarget();
        }
        else
        {
            l_valCount = l_accumBonusSchemeData.getRefillCountTarget();
        }
        
        if (c_debug) System.out.println("    l_valCount " + l_valCount);
        i_refillTargetValueOrCountField.setValue(l_valCount);
        
        if(l_accumBonusSchemeData.getRefillValueTarget() != null)
        {
            i_refillValueTargetRadioButton.doClick();
        }
        else
        {
            i_refillCountTargetRadioButton.doClick();
        }
        
        i_countingPeriodField.setValue(new Integer(l_accumBonusSchemeData.getCountingPeriodDays()));

        i_isSetDedicatedAccount.setSelected(l_accumBonusSchemeData.isSetOfDedAccBalRequired());

        // get bonus elements
        refreshCurrentElementRecord();

        if (c_debug) System.out.println("Leaving refreshCurrentRecord()");
    }

    
    /**
     * 
     *
     */
    protected void refreshCurrentElementRecord()
    {
        BoiBonusElementData l_boiBoneData;
        BoneBonusElementData l_boneData;
        
        i_definedBonusElements = ((BonusDbService)i_dbService).getAvailableBonusElementData();
        i_keyElementDataComboBox.setModel(new DefaultComboBoxModel(i_definedBonusElements));
        
        l_boiBoneData = ((BonusDbService)i_dbService).getBonusElementData();
        if (c_debug) System.out.println("Entered refreshCurrentElementRecord(), l_boiBoneData: " + l_boiBoneData);
       
        if (l_boiBoneData != null)
        {
            i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
            i_keyElementDataComboBox.setSelectedItem(l_boiBoneData);
            i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
            l_boneData = l_boiBoneData.getInternalBonusElementData();
            
            String[] l_kE = l_boneData.getKeyElements().split(",", 3);
            
            boolean l_found = false;
            for (int i=2; i< i_definedChannelValues.size() && !l_found; i++)
            {                
                if (l_kE.length < 3)
                {
                    if ((l_kE.length == 1) && (c_channelIndex == 0))
                    {
                        if (l_kE[c_channelIndex].equals(
                            ((BoiChavChannelData)i_definedChannelValues.elementAt(i)).getInternalChannelData().getChannelId()))
                        {
                            i_element1Field.setSelectedItem(i_definedChannelValues.elementAt(i));
                        }
                        else if (l_kE[c_channelIndex].equals("*"))
                        {
                            i_element1Field.setSelectedIndex(1);
                        }
                    }
                    else
                    {
                        i_element1Field.setSelectedIndex(0); // Assume blank element
                    }                    
                    l_found = true;
                    continue;
                }
                
                if (l_kE[c_channelIndex].equals("*"))
                {
                    i_element1Field.setSelectedIndex(1);
                    l_found = true;
                    continue;
                    
                }
                
                if (l_kE[c_channelIndex].equals(
                        ((BoiChavChannelData)i_definedChannelValues.elementAt(i)).getInternalChannelData().getChannelId()))
                {
                    i_element1Field.setSelectedItem(i_definedChannelValues.elementAt(i));
                    l_found = true;
                }
            }
            
            if (!l_found) i_element1Field.setSelectedIndex(0);
            
            //TODO: What to do with i_element2Field? (Which is normally configured as blank).
            
            l_found = false;
            for (int i=2; i< i_definedAccountGroups.size() && !l_found; i++)
            {
                if (l_kE.length < 3)
                {
                    i_element3Field.setSelectedIndex(0); // Assume blank element
                    l_found = true;
                }
                else if (l_kE[c_topUpTypeIndex].equals("*"))
                {
                    i_element3Field.setSelectedIndex(1);
                    l_found = true;
                }
                else if (l_kE[c_topUpTypeIndex].equals(
                            String.valueOf(
                                ((BoiAccountGroupsData)i_definedAccountGroups.elementAt(i)).
                                    getInternalAccountGroupData().getAccountID().getValue())))
                {
                    i_element3Field.setSelectedItem(i_definedAccountGroups.elementAt(i));
                    l_found = true;
                }
            }
            if (!l_found) i_element3Field.setSelectedIndex(0);
                                    
            i_activeElementIndCheckbox.setSelected(l_boneData.isBonusActive());
            populateCodesTable();
        }
        else
        {
            initialiseElementsPanel();
        }
        
        selectRowInTable(i_table, i_verticalScrollBar);
        if (c_debug) System.out.println("Leaving refreshCurrentElementRecord()");
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        if (c_debug) System.out.println("Entered refreshDataUponEntry");
        refreshListData();
        refreshElementListData();
        if (c_debug) System.out.println("Leaving refreshDataUponEntry");
    }
    
    protected void refreshListData()
    {
        if (c_debug) System.out.println("Entered refreshListData");

        Object l_selectedAdjType = null;
        Object l_selectedAdjCode = null;

        i_definedBonusSchemes = ((BonusDbService)i_dbService).getAvailableBonusSchemeData();

        i_keyDataModel = new DefaultComboBoxModel(i_definedBonusSchemes);
        i_keyDataComboBox.setModel(i_keyDataModel);
        
        Object l_seofData = i_serviceOfferingsCombo.getSelectedItem();
        
        i_serviceOfferings = ((BonusDbService)i_dbService).getAllServiceOfferingsV();
        
        i_serviceOfferingsModel = new DefaultComboBoxModel(i_serviceOfferings);
        i_serviceOfferingsCombo.setModel(i_serviceOfferingsModel);
        i_serviceOfferingsCombo.setSelectedItem(l_seofData);

        l_selectedAdjType = i_adjustmentTypesCombo.getSelectedItem();
        l_selectedAdjCode = i_adjustmentCodesCombo.getSelectedItem();

        i_adjustmentTypes = ((BonusDbService)i_dbService).getAvailableAdjTypesDataV();
        i_adjustmentTypesCombo.setModel(new DefaultComboBoxModel(i_adjustmentTypes));
        i_adjustmentTypesCombo.setSelectedItem(l_selectedAdjType);
        
        i_adjustmentCodes = ((BonusDbService)i_dbService).getAvailableAdjustmentCodesDataV();
        i_adjustmentCodesCombo.setModel(new DefaultComboBoxModel(i_adjustmentCodes));
        i_adjustmentCodesCombo.setSelectedItem(l_selectedAdjCode);

        if (c_debug) System.out.println("Leaving refreshListData");
    }
    

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshElementListData()
    {
        if (c_debug) System.out.println("Entered refreshElementListData()");
        
        //Only elements 1 and 3 can be combo-box lists
        Object l_selectedElement1 = null;
        Object l_selectedElement3 = null;
       
        
        i_definedBonusElements = ((BonusDbService)i_dbService).getAvailableBonusElementData();
        i_keyDataModelElements = new DefaultComboBoxModel(i_definedBonusElements);
        i_keyElementDataComboBox.setModel(i_keyDataModelElements);
        
        l_selectedElement1 = i_element1Field.getSelectedItem();
        i_definedChannelValues = ((BonusDbService)i_dbService).getAvailableChannelData();
        i_element1Field.setModel(new DefaultComboBoxModel(i_definedChannelValues));
        
        l_selectedElement3 = i_element3Field.getSelectedItem();
        i_definedAccountGroups = ((BonusDbService)i_dbService).getAvailableAccountGroupData();
        i_element3Field.setModel(new DefaultComboBoxModel(i_definedAccountGroups));
        
        i_element1Field.setSelectedItem(l_selectedElement1);
        i_element3Field.setSelectedItem(l_selectedElement3);
        
        populateCodesTable();
        
        if (c_debug) System.out.println("Leaving refreshElementListData()");
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        if (c_debug) System.out.println("Entered BonusPane.resetSelectionForKeyCombo()");
        i_keyDataComboBox.setSelectedItem(((BonusDbService)i_dbService).getBonusSchemeData());
        if (c_debug) System.out.println("Leaving BonusPane.resetSelectionForKeyCombo()");
    }
    
    /**
     * Resets the selected value for the combo box holding the key element data.
     * Typically called when user does not confirm new selection via key element data combo.
     */
    protected void resetSelectionForKeyElementCombo()
    {
        if (c_debug) System.out.println("Entered resetSelectionForKeyElementCombo");
        i_keyElementDataComboBox.setSelectedItem(((BonusDbService)i_dbService).getBonusElementData());
        if (c_debug) System.out.println("Leaving resetSelectionForKeyElementCombo");
    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.validateKeyData()");
        String l_bonusSchemeId = null;
        
        if (i_bonusSchemeId.getText().trim().equals(""))
        {
            i_bonusSchemeId.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Bonus scheme id cannot be blank");
            return false;
        }

        l_bonusSchemeId = i_bonusSchemeId.getText();
        ((BonusDbService)i_dbService).setCurrentBonusScheme(l_bonusSchemeId);
        ((BonusDbService)i_dbService).setCurrentKeyElements(buildKeyElementsString());
        ((BonusDbService)i_dbService).setCurrentNonKeyElements(C_NON_KEY_ELEMENTS_VALUE);
        
        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.validateKeyData()");
        
        return true;
    }
    
    /**
     * Validates key element screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyElementData()
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.validateKeyElementData()");
        String l_bonusSchemeId = null;
        
        if (i_bonusSchemeId.getText().trim().equals(""))
        {
            i_bonusSchemeId.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Bonus scheme id cannot be blank");
            return false;
        }
        
        if ((i_element1Field.getSelectedIndex() == 0) && (i_element3Field.getSelectedIndex() == 0))
        {
            i_element1Field.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Channel value and Top Up Type cannot both be blank");
            return false;
        }
        
        l_bonusSchemeId = i_bonusSchemeId.getText();
        ((BonusDbService)i_dbService).setCurrentBonusScheme(l_bonusSchemeId);
        ((BonusDbService)i_dbService).setCurrentKeyElements(buildKeyElementsString());
        ((BonusDbService)i_dbService).setCurrentNonKeyElements(C_NON_KEY_ELEMENTS_VALUE);
        
        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.validateKeyElementData()");
        
        return true;
    }

    /**
     * Validates bonus scheme screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.validateScreenData()");
        
        if (i_bonSchemeDescField.getText().trim().equals(""))
        {
            i_bonSchemeDescField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter a description");
            return false;
        }
                
        if (i_startDateField.getText().trim().equals(""))
        {
            i_startDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter a start date");
            return false;
        }
        
        if (i_endDateField.getText().trim().equals(""))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter an end date");
            return false;
        }
        
        if (new PpasDate(i_endDateField.getText()).before(DatePatch.getDateTimeNow().getPpasDate()))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Bonus scheme end date is in the past");
            return false;
        }
        
        if (new PpasDate(i_startDateField.getText()).after(new PpasDate(i_endDateField.getText())))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Bonus scheme end date must be after start date");
            return false;
        }
        
        if (new PpasDate(i_startDateField.getText()).after(DatePatch.getDateTimeNow().getPpasDate()))
        {
            i_endDateField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "INFO: Bonus scheme start date is in the future");
        }

        if (i_bonusAmountOrPercentField.getText().trim().equals(""))
        {
            i_bonusAmountOrPercentField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter a bonus");
            return false;
        }
        
        if (i_bonusPercentageRadioButton.isSelected())
        {
            float l_percentage = 0;
            
            try
            {
                l_percentage = Float.valueOf(i_bonusAmountOrPercentField.getText().trim()).floatValue();
                if (l_percentage < 0.00f || l_percentage > 999.99f)
                {
                    i_bonusAmountOrPercentField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane, 
                                         "Invalid bonus percentage");
                    return false;
                }
            }
            catch (NumberFormatException l_e)
            {
                i_bonusAmountOrPercentField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid bonus percentage");
                return false;
            }
        }
        else
        {
            double l_amount = 0.0d;
            try
            {
                l_amount = Double.valueOf(i_bonusAmountOrPercentField.getText().trim()).doubleValue();
                if (l_amount < 0.000d || l_amount > 999999999999.999d)
                {
                    i_bonusAmountOrPercentField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane, 
                                         "Invalid bonus amount");
                    return false;
                    
                }
            }
            catch (NumberFormatException l_e)
            {
                i_bonusAmountOrPercentField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid bonus amount");
                return false;
            }
        }

        if(i_accountChoiceCombo.getSelectedIndex() != 0)
        {
            // if an account has been selected validate the Expiry days entry
            if(i_expiryDaysField.getText().trim().equals(""))
            {
                i_expiryDaysField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Please enter the expiry days");
                return false;
            }
            
            try
            {
                int l_expiryDays = Integer.valueOf(i_expiryDaysField.getText().trim()).intValue();
                if(l_expiryDays < 1 || l_expiryDays > 999)
                {
                    i_expiryDaysField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane, 
                                         "Invalid expiry days value");
                    return false;
                }
            }
            catch (NumberFormatException l_e)
            {
                i_expiryDaysField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid expiry days value");
                return false;

            }
            
        }
        
        if (i_adjustmentTypesCombo.getSelectedIndex() == 0)
        {
            i_adjustmentTypesCombo.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please select an adjustment type");
            return false;
        }
        
        if (i_adjustmentCodesCombo.getSelectedIndex() == 0)
        {
            i_adjustmentCodesCombo.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please select an adjustment code");
            return false;
        }

        if (i_refillTargetValueOrCountField.getText().trim().equals(""))
        {
            i_refillTargetValueOrCountField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter a refill target");
            return false;
        }
        
        if (i_refillValueTargetRadioButton.isSelected())
        {
            double l_value = 1.0d;
            try
            {
                l_value = Double.valueOf(i_refillTargetValueOrCountField.getText().trim()).doubleValue();
                if (l_value <= 0.000d || l_value > 999999999999.999d)
                {
                    i_refillTargetValueOrCountField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane, 
                                         "Invalid refill target value");
                    return false;
                    
                }
            }
            catch (NumberFormatException l_e)
            {
                i_refillTargetValueOrCountField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid refill target value");
                return false;
            }
        }
        else
        {
            try
            {
                int l_count = Integer.valueOf(i_refillTargetValueOrCountField.getText().trim()).intValue();
                if(l_count < 1 || l_count > 999)
                {
                    i_refillTargetValueOrCountField.requestFocusInWindow();
                    displayMessageDialog(i_contentPane, 
                                         "Invalid refill count value");
                    return false;
                }
            }
            catch (NumberFormatException l_e)
            {
                i_refillTargetValueOrCountField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid refill count value");
                return false;

            }
        }

        if(i_countingPeriodField.getText().trim().equals(""))
        {
            i_countingPeriodField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Please enter the counting period");
            return false;
        }
        
        try
        {
            int l_countingPeriod = Integer.valueOf(i_countingPeriodField.getText().trim()).intValue();
            if(l_countingPeriod < 1 || l_countingPeriod > 999)
            {
                i_countingPeriodField.requestFocusInWindow();
                displayMessageDialog(i_contentPane, 
                                     "Invalid counting period value");
                return false;
            }
        }
        catch (NumberFormatException l_e)
        {
            i_countingPeriodField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Invalid counting period value");
            return false;

        }

        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.validateScreenData()");
        return true;
    }
    
    /**
     * Validates bonus element screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateElementScreenData()
    {
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.validateElementScreenData()");
        // don't think there's anything to do here!        
        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.validateElementScreenData()");
        return true;
    }
    
    /**
     * Writes current record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {   
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.writeCurrentRecord()");
        BonsBonusSchemeData l_bonusSchemeData = null;
        BonaBonusAccumParamsData l_accumulatedBonusSchemeData = null;
        
        String l_bonusSchemeId = i_bonusSchemeId.getText();

        l_bonusSchemeData = new BonsBonusSchemeData(l_bonusSchemeId,
                                                    BonusDbService.C_ACCUMULATED_SCHEME_TYPE,
                                                    i_bonSchemeDescField.getText(),
                                                    new PpasDate(i_startDateField.getText()),
                                                    new PpasDate(i_endDateField.getText()),
                                                    convertSeofBitToInt(i_serviceOfferingsCombo.getSelectedItem()),
                                                    i_activeSchemeIndCheckBox.isSelected(),
                                                    false,
                                                    "SUPER",
                                                    null);
                                                        
        ((BonusDbService)i_dbService).setBonusSchemeData(new BoiBonusSchemeData(l_bonusSchemeData));

        l_accumulatedBonusSchemeData = 
            new BonaBonusAccumParamsData(l_bonusSchemeId,
                                         i_bonusPercentageRadioButton.isSelected() ? 
                                             Float.valueOf(i_bonusAmountOrPercentField.getText().trim()) : null,
                                         ((BoiAdjCodesData)i_adjustmentCodesCombo.getSelectedItem()).
                                             getInternalAdjCodesData().getAdjCode(),
                                         ((BoiMiscCodeData)i_adjustmentTypesCombo.getSelectedItem()).
                                             getInternalMiscCodeData().getCode(),
                                         Integer.parseInt(i_accountChoiceCombo.getSelectedItem().toString()),
                                         i_refillValueTargetRadioButton.isSelected() ?
                                             Double.valueOf(i_refillTargetValueOrCountField.getText().trim()) : null,
                                         i_refillCountTargetRadioButton.isSelected() ?
                                             Integer.valueOf(i_refillTargetValueOrCountField.getText().trim()) : null,
                                         Integer.valueOf(i_countingPeriodField.getText().trim()).intValue(),
                                         i_bonusAmountRadioButton.isSelected() ? 
                                             Double.valueOf(i_bonusAmountOrPercentField.getText().trim()) : null,
                                         i_accountChoiceCombo.getSelectedIndex() != 0 ?
                                             Integer.valueOf(i_expiryDaysField.getText().trim()) : null,
                                         i_isSetDedicatedAccount.isSelected()); 
                                         
        ((AccumulatedBonusDbService)i_dbService).
            setBonusSchemeData(new BoiAccumulatedBonusSchemeData(l_accumulatedBonusSchemeData));

        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.writeCurrentRecord()");
    }
    
    /**
     * Writes current element record fields to screen data object.  Called before update or insert.
     */  
    protected void writeCurrentElementRecord()
    {   
        if (c_debug) System.out.println("Entered AccumulatedBonusPane.writeCurrentElementRecord()");

        String l_bonusSchemeId = i_bonusSchemeId.getText();
        
        BoneBonusElementData l_boneData = null;
        
        l_boneData = 
            new BoneBonusElementData(
                           l_bonusSchemeId,
                           buildKeyElementsString(),
                           C_NON_KEY_ELEMENTS_VALUE,
                           null,
                           null,
                           null,
                           null,
                           null,
                           null,
                           i_activeElementIndCheckbox.isSelected(),
                           false,
                           "",
                           null);
        
        ((BonusDbService)i_dbService).setBonusElementData(new BoiBonusElementData(l_boneData));
        
        if (c_debug) System.out.println("Leaving AccumulatedBonusPane.writeCurrentElementRecord()");
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiBonusSchemeData l_selectedScheme = null;
        BoiBonusElementData l_selectedElementItem = null;
        String l_bonusSchemeId = null;
        
        if (c_debug) System.out.println("Entered writeKeyData()");
        
        if (i_keyDataComboBox.getSelectedItem() instanceof BoiBonusSchemeData)
        {
            l_selectedScheme = (BoiBonusSchemeData)i_keyDataComboBox.getSelectedItem();
            l_bonusSchemeId = l_selectedScheme.getInternalBonusSchemeData().getBonusSchemeId();
            ((BonusDbService)i_dbService).setCurrentBonusScheme(l_bonusSchemeId);
        }
        else
        {
            ((BonusDbService)i_dbService).setCurrentBonusScheme(null);
        }
        
        if (l_selectedScheme != null && 
                (i_keyElementDataComboBox.getSelectedItem() instanceof BoiBonusElementData))
        {
            l_selectedElementItem = (BoiBonusElementData)i_keyElementDataComboBox.getSelectedItem();
            
            // If a new scheme id has been chosen, initialise the current element info.
            if (l_selectedScheme.getInternalBonusSchemeData().getBonusSchemeId().equals(
                    l_selectedElementItem.getInternalBonusElementData().getBonusSchemeId()))
            {
                ((BonusDbService)i_dbService).setBonusElementData(l_selectedElementItem);
            }
            else
            {
                ((BonusDbService)i_dbService).setBonusElementData(null);
            }
        }
        else
        {
            ((BonusDbService)i_dbService).setBonusElementData(null);
        }
        
        if (c_debug) System.out.println("Leaving writeKeyData()");
    }
    
    /**
     * Selects the row in  table that corresponds to the selected
     * item in the element combo box.
     * Also adjusts the view port in order to make the selected row visible.
     * @param p_table             The table whose display is being altered.
     * @param p_verticalScrollBar The vertical scrollbar belonging to the JScrollPane in this table.
     */
    protected void selectRowInTable(JTable p_table, JScrollBar p_verticalScrollBar)
    {
        int l_selectionIx = -1;
        l_selectionIx = i_keyElementDataComboBox.getSelectedIndex() - 2;

        if (l_selectionIx >= 0)
        {
            if (l_selectionIx != p_table.getSelectedRow())
            {
                p_table.setRowSelectionInterval(l_selectionIx, l_selectionIx);
                p_verticalScrollBar.setValue(l_selectionIx * p_table.getRowHeight());
            }
        }
        else
        {
            p_verticalScrollBar.setValue(0);
        }
    }
    
    /**
     * Creates the bonus schemes panel.
     */
    private void createBonusSchemesPanel()
    {
        JLabel l_bonusSchemesLabel     = null;
        JLabel l_bonusSchemeIdLabel    = null;
        JLabel l_bonusSchemeDescLabel  = null;
        JLabel l_startDateLabel        = null;
        JLabel l_endDateLabel          = null;
        JLabel l_serviceOfferingsLabel = null;
        JLabel l_bonusAmountOrPercentLabel = null;
        JLabel l_adjustmentCodeLabel       = null;
        JLabel l_adjustmentTypeLabel       = null;
        JLabel l_accountChoiceLabel        = null;
        JLabel l_expiryDaysLabel           = null;
        JLabel l_refillTargetLabel         = null;
        JLabel l_countingPeriodLabel       = null;
        ButtonGroup l_bonusButtonGroup = null;
        ButtonGroup l_refillTargetButtonGroup = null;

        
        l_bonusSchemesLabel = WidgetFactory.createLabel("Defined Schemes: ");
        l_bonusSchemeIdLabel = WidgetFactory.createLabel("Scheme Id: ");
        l_bonusSchemeDescLabel = WidgetFactory.createLabel("Description: ");
        l_startDateLabel = WidgetFactory.createLabel("Start Date: ");
        l_endDateLabel = WidgetFactory.createLabel("End Date: ");
        l_serviceOfferingsLabel = WidgetFactory.createLabel("Service Offering: ");

        l_adjustmentCodeLabel = WidgetFactory.createLabel("Adjustment Code: ");
        l_adjustmentTypeLabel = WidgetFactory.createLabel("Adjustment Type: ");
        l_bonusAmountOrPercentLabel = WidgetFactory.createLabel("Bonus: ");
        l_accountChoiceLabel = WidgetFactory.createLabel("Account: ");
        l_expiryDaysLabel = WidgetFactory.createLabel("Expiry Days: ");
        l_refillTargetLabel = WidgetFactory.createLabel("Refill Target: ");
        l_countingPeriodLabel = WidgetFactory.createLabel("Counting Period: ");

        i_activeSchemeIndCheckBox = WidgetFactory.createCheckBoxWithSize("Active?");       
        addValueChangedListener(i_activeSchemeIndCheckBox);
        i_bonusSchemeId = WidgetFactory.createTextField(4);
        addValueChangedListener(i_bonusSchemeId);
        i_bonSchemeDescField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_bonSchemeDescField);
        i_startDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_startDateField);
        i_endDateField = WidgetFactory.createDateField(this);
        addValueChangedListener(i_endDateField);
        
        i_serviceOfferings.add("");
        
        i_serviceOfferingsCombo = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_serviceOfferings));
        addValueChangedListener(i_serviceOfferingsCombo);

        Vector l_dedaAccounts = null; // TODO: Retrieve a list of valid dedicated accounts from
        // configuration???
        l_dedaAccounts = new Vector();
        for (int i = 0; i <= 10; i++)
        {
            l_dedaAccounts.add(String.valueOf(i));
        }

        // Create a panel that contains the Bonus label, the Bonus amount/percent field and
        // another panel which contains the amount & percent radio buttons
        i_bonusPanel = WidgetFactory.createPanel(null, 36, 8, 0, 0);
        i_bonusPanel.setBorder(BorderFactory.createLineBorder(new Color(0xded6b5)));

        // Create the panel to contain the radio buttons
        i_bonusRadioPanel = WidgetFactory.createPanel(null, 11, 8, 0, 0);
        i_bonusRadioPanel.setBorder(BorderFactory.createEmptyBorder());

        i_bonusAmountOrPercentField = WidgetFactory.createAmountField(12, 3, false); 
        addValueChangedListener(i_bonusAmountOrPercentField);

        i_bonusAmountRadioButton = WidgetFactory.createRadioButton("Amount", this);
        addValueChangedListener(i_bonusAmountRadioButton);
        i_bonusAmountRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
        i_bonusAmountRadioButton.setBorder(BorderFactory.createEmptyBorder());
        i_bonusAmountRadioButton.setBorderPainted(true);

        i_bonusPercentageRadioButton = WidgetFactory.createRadioButton("Percentage", this);
        addValueChangedListener(i_bonusPercentageRadioButton);
        i_bonusPercentageRadioButton.setHorizontalAlignment(SwingConstants.LEFT);
        i_bonusPercentageRadioButton.setBorder(BorderFactory.createEmptyBorder());
        i_bonusPercentageRadioButton.setBorderPainted(true);

        l_bonusButtonGroup = new ButtonGroup();
        l_bonusButtonGroup.add(i_bonusAmountRadioButton);
        l_bonusButtonGroup.add(i_bonusPercentageRadioButton);

        // add the radio buttons to the radio panel
        i_bonusRadioPanel.add(i_bonusAmountRadioButton,
                new GridPlacementSpecifier("bonusAmountRadioButton", 1, 1, 9, 4).toString());
        i_bonusRadioPanel.add(i_bonusPercentageRadioButton,
                new GridPlacementSpecifier("bonusPercentageRadioButton", 1, 5, 11, 4).toString());

        // add the Bonus label, the Bonus amount/percent field and the radio panel to the bonus panel
        i_bonusPanel.add(l_bonusAmountOrPercentLabel,
                new GridPlacementSpecifier("bonusAmountOrPercentLabel", 1, 3, 7).toString());
        i_bonusPanel.add(i_bonusAmountOrPercentField,
                new GridPlacementSpecifier("bonusAmountOrPercentField", 9, 3, 15).toString());
        i_bonusPanel.add(i_bonusRadioPanel,
                new GridPlacementSpecifier("bonusRadioPanel", 25, 1, 11, 8).toString());

        
        i_accountChoiceCombo = WidgetFactory.createComboBox(l_dedaAccounts, this);
        addValueChangedListener(i_accountChoiceCombo);

        i_expiryDaysField = WidgetFactory.createIntegerField(3, false);
        addValueChangedListener(i_expiryDaysField);

        i_adjustmentTypes = ((BonusDbService)i_dbService).getAvailableAdjTypesDataV();
        i_adjustmentCodes = ((BonusDbService)i_dbService).getAvailableAdjustmentCodesDataV();
        i_adjustmentTypesCombo = WidgetFactory.createComboBox(i_adjustmentTypes, this);
        addValueChangedListener(i_adjustmentTypesCombo);
        i_adjustmentCodesCombo = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_adjustmentCodes));
        addValueChangedListener(i_adjustmentCodesCombo);

        // Create a panel that contains the Refill Target label, the Refill Target Value/Count field and
        // another panel which contains the value and count radio buttons
        i_refillTargetPanel = WidgetFactory.createPanel(null, 36, 8, 0, 0);
        i_refillTargetPanel.setBorder(BorderFactory.createLineBorder(new Color(0xded6b5)));

        // Create the panel to contain the radio buttons
        i_refillTargetRadioPanel = WidgetFactory.createPanel(null, 8, 8, 0, 0);
        i_refillTargetRadioPanel.setBorder(BorderFactory.createEmptyBorder());

        i_refillValueTargetRadioButton = WidgetFactory.createRadioButton("Value", this);
        addValueChangedListener(i_refillValueTargetRadioButton);
        i_refillValueTargetRadioButton.setBorder(BorderFactory.createEmptyBorder());
        i_refillValueTargetRadioButton.setBorderPainted(true);
        i_refillValueTargetRadioButton.setHorizontalAlignment(SwingConstants.LEFT);

        i_refillCountTargetRadioButton = WidgetFactory.createRadioButton("Count", this);
        addValueChangedListener(i_refillCountTargetRadioButton);
        i_refillCountTargetRadioButton.setBorder(BorderFactory.createEmptyBorder());
        i_refillCountTargetRadioButton.setBorderPainted(true);
        i_refillCountTargetRadioButton.setHorizontalAlignment(SwingConstants.LEFT);

        l_refillTargetButtonGroup = new ButtonGroup();
        l_refillTargetButtonGroup.add(i_refillValueTargetRadioButton);
        l_refillTargetButtonGroup.add(i_refillCountTargetRadioButton);

        // add the radio buttons to the radio panel
        i_refillTargetRadioPanel.add(i_refillValueTargetRadioButton,
                new GridPlacementSpecifier("refillValueTargetRadioButton", 1, 1, 8, 4).toString());
        i_refillTargetRadioPanel.add(i_refillCountTargetRadioButton,
                new GridPlacementSpecifier("refillCountTargetRadioButton", 1, 5, 8, 4).toString());

        // add the Refill Target label, the Refill Target Value/Count field and radio panel to the
        // Refill Target panel
        i_refillTargetPanel.add(l_refillTargetLabel,
                new GridPlacementSpecifier("refillTargetLabel", 1, 3, 11).toString());
        i_refillTargetValueOrCountField = WidgetFactory.createAmountField(12, 3, false);
        addValueChangedListener(i_refillTargetValueOrCountField);
        i_refillTargetPanel.add(i_refillTargetValueOrCountField,
                new GridPlacementSpecifier("refillTargetValueOrCountField", 13, 3, 15).toString());
        i_refillTargetPanel.add(i_refillTargetRadioPanel,
                new GridPlacementSpecifier("refillTargetRadioPanel", 29, 1, 8, 8).toString());

        i_countingPeriodField = WidgetFactory.createIntegerField(3, false);
        addValueChangedListener(i_countingPeriodField);
        
        i_isSetDedicatedAccount = WidgetFactory.createCheckBox("Set Dedicated Account?");
        i_isSetDedicatedAccount.setBorder(BorderFactory.createEmptyBorder());
        addValueChangedListener(i_isSetDedicatedAccount);

        // create the Bonus Schemes panel and add all the items
        i_bonusSchemesPanel = WidgetFactory.createPanel("Schemes", 100, 50, 0, 0);

        i_bonusSchemesPanel.add(l_bonusSchemesLabel, 
                    new GridPlacementSpecifier("bonusSchemesLabel", 1, 1, 15).toString());
        i_bonusSchemesPanel.add(i_keyDataComboBox,
                new GridPlacementSpecifier("definedCodesBox", 17, 1, 45).toString());

        i_bonusSchemesPanel.add(i_activeSchemeIndCheckBox,
                                new GridPlacementSpecifier("activeCheckbox", 63, 1, 20, 4).toString());

        i_bonusSchemesPanel.add(l_bonusSchemeIdLabel,
                new GridPlacementSpecifier("bonusSchemeIdLabel", 1, 6, 15).toString());
        i_bonusSchemesPanel.add(i_bonusSchemeId,
                new GridPlacementSpecifier("bonusSchemeId", 17, 6, 10).toString());

        i_bonusSchemesPanel.add(l_bonusSchemeDescLabel,
                new GridPlacementSpecifier("descLabel", 33, 6, 15).toString());
        i_bonusSchemesPanel.add(i_bonSchemeDescField,
                new GridPlacementSpecifier("descriptionField", 49, 6, 30).toString());

        i_bonusSchemesPanel.add(l_startDateLabel,
                new GridPlacementSpecifier("startDateLabel", 1, 11, 15).toString());
        i_bonusSchemesPanel.add(i_startDateField,
                new GridPlacementSpecifier("startDateField", 17, 11, 15).toString());
        i_bonusSchemesPanel.add(l_endDateLabel,
                new GridPlacementSpecifier("endDateLabel", 33, 11, 15).toString());
        i_bonusSchemesPanel.add(i_endDateField,
                new GridPlacementSpecifier("endDateField", 49, 11, 15).toString());

        i_bonusSchemesPanel.add(l_serviceOfferingsLabel,
                new GridPlacementSpecifier("serviceOfferingsLabel", 1, 16, 15).toString());
        i_bonusSchemesPanel.add(i_serviceOfferingsCombo,
                new GridPlacementSpecifier("serviceOfferingsCombo", 17, 16, 35).toString());

        i_bonusSchemesPanel.add(i_bonusPanel,
                  new GridPlacementSpecifier("bonusPanel", 9, 21, 36, 8).toString());
        
        i_bonusSchemesPanel.add(l_accountChoiceLabel,
                new GridPlacementSpecifier("accountChoiceLabel", 45, 23, 9).toString());
        i_bonusSchemesPanel.add(i_accountChoiceCombo,
                new GridPlacementSpecifier("accountChoiceField", 55, 23, 6).toString());
    
        i_bonusSchemesPanel.add(l_expiryDaysLabel,
                new GridPlacementSpecifier("expiryDaysLabel", 62, 23, 10).toString());
        i_bonusSchemesPanel.add(i_expiryDaysField,
                new GridPlacementSpecifier("expiryDaysField", 73, 23, 5).toString());

        i_bonusSchemesPanel.add(i_isSetDedicatedAccount,
                new GridPlacementSpecifier("isSetDedicatedAccount", 81, 23, 20, 4).toString());

        i_bonusSchemesPanel.add(l_adjustmentTypeLabel,
                new GridPlacementSpecifier("adjustmentTypeLabel", 1, 30, 15).toString());
        i_bonusSchemesPanel.add(i_adjustmentTypesCombo,
                new GridPlacementSpecifier("adjustmentTypesCombo", 17, 30, 30).toString());

        i_bonusSchemesPanel.add(l_adjustmentCodeLabel,
                new GridPlacementSpecifier("adjustmentCodeLabel", 47, 30, 15).toString());
        i_bonusSchemesPanel.add(i_adjustmentCodesCombo,
                new GridPlacementSpecifier("adjustmentCodesCombo", 63, 30, 30).toString());
        
        i_bonusSchemesPanel.add(i_refillTargetPanel,
                new GridPlacementSpecifier("refillTargetPanel", 5, 35, 36, 8).toString());
                
        i_bonusSchemesPanel.add(l_countingPeriodLabel,
                new GridPlacementSpecifier("countingPeriodLabel", 47, 37, 15).toString());
        i_bonusSchemesPanel.add(i_countingPeriodField,
                new GridPlacementSpecifier("countingPeriodField", 63, 37, 5).toString());
        
        i_bonusSchemesPanel.add(i_updateButton,
                new GridPlacementSpecifier("updateButton", 1, 46, 15, 5).toString());
        i_bonusSchemesPanel.add(i_deleteButton,
                new GridPlacementSpecifier("deleteButton", 17, 46, 15, 5).toString());
        i_bonusSchemesPanel.add(i_resetButton,
                new GridPlacementSpecifier("resetButton", 33, 46, 15, 5).toString());

    }

    /**
     * Creates the bonus elements modification panel.
     */
    private void createBonusElementModificationPanel()
    {
        String[] l_st                        = null;
        JLabel   l_definedElementsLabel      = null;
        JLabel   l_element1Label             = null;
        JLabel   l_element2Label             = null;
        JLabel   l_element3Label             = null;

        i_keyElementDataComboListener = new KeyElementDataComboListener();
        i_keyElementDataComboBox = WidgetFactory.createComboBox(new DefaultComboBoxModel());
        i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
        
        i_elements = ((BoiContext)i_context).getObject("ascs.boi.tubsElements");
        l_st = ((String)i_elements).split(",");
        
        for (int i=0; i<l_st.length; i=i+3)
        {
            if (l_st[i].equals("Channel"))
            {
                c_channelIndex = (i/3);
            }
            else if (l_st[i].equals("TopUpType"))
            {
                c_topUpTypeIndex = (i/3);
            }
        }
        
        if (c_debug)
        {
            System.out.println("c_channelIndex: " + c_channelIndex);
            System.out.println("c_topUpTypeIndex: " + c_topUpTypeIndex);
        }
        
        if ((c_channelIndex == -1) || (c_topUpTypeIndex == -1))
        {
            displayMessageDialog(
                i_contentPane, "Erroneous key elements config detected. Please contact system administrator");
        }
        
        l_definedElementsLabel = WidgetFactory.createLabel("Elements:");
        
        l_element1Label = WidgetFactory.createLabel("Channel:");
        l_element2Label = WidgetFactory.createLabel("null");
        l_element2Label.setVisible(false);
        l_element3Label = WidgetFactory.createLabel("TopUpType:");

        i_element1Field = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_definedChannelValues));
        addValueChangedListenerEl(i_element1Field);
        
        i_element2Field = WidgetFactory.createTextField(1);
        i_element2Field.setVisible(false);
        addValueChangedListenerEl(i_element2Field);
        
        i_element3Field = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_definedAccountGroups));
        addValueChangedListenerEl(i_element3Field);
        
        i_activeElementIndCheckbox = WidgetFactory.createCheckBoxWithSize("Active? ");
        addValueChangedListenerEl(i_activeElementIndCheckbox);

        i_definedBonusElementsPanel = WidgetFactory.createPanel("Bonus Element Modification", 100, 44, 0, 0);

        i_definedBonusElementsPanel.add(l_definedElementsLabel,
            new GridPlacementSpecifier("definedElementsLabel", 1, 1, 15).toString());
        i_definedBonusElementsPanel.add(i_keyElementDataComboBox,
            new GridPlacementSpecifier("keyElementDataCombo", 17, 1, 60).toString());

        i_definedBonusElementsPanel.add(l_element1Label,
            new GridPlacementSpecifier("element1Label", 1, 6, 15).toString());
        i_definedBonusElementsPanel.add(i_element1Field,
            new GridPlacementSpecifier("element1Field", 17, 6, 45).toString());

        i_definedBonusElementsPanel.add(l_element3Label,
            new GridPlacementSpecifier("element3Label", 1, 11, 15).toString());
        i_definedBonusElementsPanel.add(i_element3Field,
            new GridPlacementSpecifier("element3Field", 17, 11, 45).toString());

        i_definedBonusElementsPanel.add(l_element2Label,
            new GridPlacementSpecifier("element2Label", 63, 6, 10).toString());
        i_definedBonusElementsPanel.add(i_element2Field,
            new GridPlacementSpecifier("element2Field", 75, 6, 29).toString());

        i_definedBonusElementsPanel.add(i_activeElementIndCheckbox,
            new GridPlacementSpecifier("activeElementIndCheckbox", 78, 1, 15).toString());

        i_definedBonusElementsPanel.add(i_updateButtonEl,
            new GridPlacementSpecifier("updateButtonEl", 1, 18, 15, 5).toString());

        i_definedBonusElementsPanel.add(i_deleteButtonEl,
            new GridPlacementSpecifier("deleteButtonEl", 17, 18, 15, 5).toString());

        i_definedBonusElementsPanel.add(i_resetButtonEl,
            new GridPlacementSpecifier("resetButtonEl", 33, 18, 15, 5).toString());

        createBonElemTable();
    }
    
    /**
     * 
     * @param p_index
     * @param p_st
     * @return A label for a dynamic tubs element.
     */
    private JLabel createDynamicElementLabel(int p_index, String[] p_st)
    {
        JLabel l_label = null;
        
        p_index = ((p_index -1) * 3) + 1;
        l_label = WidgetFactory.createLabel(p_st[p_index-1].trim() + ":");
        if (l_label.getText().equals("null:")) l_label.setVisible(false);
        
        return l_label;
    }
    
    /**
     * Possibly for future use...
     * @param p_index
     * @param p_st
     * @return A JComboBox for a dynamic tubs element.
     */
    private JComboBox createDynamicElementCombo(int p_index, String[] p_st)
    {
        //System.out.println("Entered createDynamicElementCombo, p_index: " + p_index);
        JComboBox l_comboBox = null;
        
        p_index = ((p_index - 1) * 3);
        
        if (p_st[p_index].equals("Channel"))
        {
            //System.out.println("     Creating channel combo");
            l_comboBox = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_definedChannelValues));
        }
        else
        {
            //System.out.println("     Creating account group combo");
            l_comboBox = WidgetFactory.createComboBox(new DefaultComboBoxModel(i_definedAccountGroups));
        }
        
        return l_comboBox;
    }

    /**
     * 
     * @param p_index
     * @param p_st
     * @return A dynamic element text field.
     */
    private JFormattedTextField createDynamicElementField(int p_index, String[] p_st)
    {
        JFormattedTextField l_field = null;
        if (c_debug) System.out.println("Entered createDynamicElementField, p_index: " + p_index);
        p_index = ((p_index -1) * 3) + 1;

        if (!p_st[p_index-1].equals("null"))
        {
            if (p_st[p_index].equals("STRING"))
            {
                //l_field = WidgetFactory.createTextField(Integer.parseInt(p_st[p_index+1]));
            }
            else if (p_st[p_index].equals("NUMERIC"))
            {
                l_field = WidgetFactory.createAmountField(Integer.parseInt(p_st[p_index+1]), 2, false);
            }
            else
            {
                //l_field = WidgetFactory.createTextField(Integer.parseInt(p_st[p_index+1]));
                l_field = WidgetFactory.createAmountField(Integer.parseInt(p_st[p_index+1]), 2, false);
            }
        }
        else
        {
            // Create a dummy element that will be invisible and never used
            l_field = WidgetFactory.createAmountField(Integer.parseInt("1"), 2, false);
            l_field.setVisible(false);
        }
        if (c_debug) System.out.println("Leaving createDynamicElementField");
        
        return l_field;
    }

    /**
     * 
     * @return A comma separated String containing the key elements in the configured order. 
     */
    private String buildKeyElementsString()
    {
        StringBuffer l_keyElements = new StringBuffer("");
        
        String[] l_st = ((String)i_elements).split(",");
        
        for (int i=0; i<l_st.length; i = i+3)
        {
            // Get the key element.       
            if (l_st[i].equals("Channel"))
            {
                if (i_element1Field.isVisible())
                {
                    Object l_selectedItem = i_element1Field.getSelectedItem();
                    if (l_selectedItem instanceof BoiChavChannelData)
                    {
                        l_keyElements.append(
                            ((BoiChavChannelData)l_selectedItem).getInternalChannelData().getChannelId());
                    }
                    else if (((String)l_selectedItem).equals("*"))
                    {
                        l_keyElements.append("*");
                    }
                }
                //TODO: Move this to bottom of method.
                if (i < 6) l_keyElements.append(",");
            }
            else if (l_st[i].equals("TopUpType"))
            {
                if (i_element3Field.isVisible())
                {
                    Object l_selectedItem = i_element3Field.getSelectedItem();
                    if (l_selectedItem instanceof BoiAccountGroupsData)
                    {
                        l_keyElements.append(
                            ((BoiAccountGroupsData)l_selectedItem).getInternalAccountGroupData().getAccountID());
                    }
                    else if (((String)l_selectedItem).equals("*"))
                    {
                        l_keyElements.append("*");
                    }
                }
                if (i < 6) l_keyElements.append(",");
            }
            else
            {
                // Do not insert a comma at the end of the String.
                if (i < 6)
                {
                    l_keyElements.append(",");
                }
            }
            
        }   
        
        if (c_debug) System.out.println("buildKeyElementsString: " + l_keyElements.toString());
        return l_keyElements.toString();
    }
    
    /**
     * Handles user requests to update the database with the screen data. 
     * @throws PpasServiceFailedException
     */
    private void doUpdateElement()
        throws PpasServiceFailedException
    {
        int       l_response = 0;
        boolean   l_valid = false;
        int       l_selectedIndex = -1;
        boolean[] l_flagsArray = null;
        
        switch (i_state2)
        {
            case C_STATE_INITIAL:
            {
                displayMessageDialog(i_contentPane,
                                     "Select record and modify data before updating.");
                break;
            }
            case C_STATE_DATA_RETRIEVED:
            {
                displayMessageDialog(i_contentPane,
                                     "Modify data before updating.");
                break;
            }
            case C_STATE_NEW_RECORD:
            {
                l_valid = validateKeyElementData();
                if (l_valid)
                {
                    l_flagsArray = ((BonusDbService)i_dbService).checkForDuplicateElementRow();
                    if (l_flagsArray[BoiDbService.C_WITHDRAWN])
                    {
                        l_response = displayConfirmDialog(
                                                i_contentPane,
                                                "A withdrawn record with this key already exists. " +
                                                "Do you wish to overwrite it?");
                        
                        if (l_response == JOptionPane.YES_OPTION)
                        {
                            l_valid = validateElementScreenData();
                            if (l_valid)
                            {
                                writeCurrentElementRecord();
                                ((BonusDbService)i_dbService).updateElementData();
                                displayMessageDialog(i_contentPane,
                                                     "Record has been overwritten.");
                                refreshElementListData();
                                initialiseElementsPanel();
                                i_state2 = C_STATE_INITIAL;
                            }
                        }
                        else
                        {
                            l_response = displayConfirmDialog(
                                            i_contentPane,
                                            "Do you wish to make the withdrawn record available again?");
                            
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                //i_dbService.markAsAvailableData();
                                ((BonusDbService)i_dbService).markAsAvailableElementData(
                                                                  buildKeyElementsString(),
                                                                  C_NON_KEY_ELEMENTS_VALUE);
                                displayMessageDialog(i_contentPane,
                                                     "Record has been made available.");
                                refreshElementListData();
                                initialiseElementsPanel();
                                i_state2 = C_STATE_INITIAL;
                            }
                        }
                    }
                    else if (l_flagsArray[BoiDbService.C_DUPLICATE])
                    {
                        displayMessageDialog(i_contentPane,
                                             "Record with this key already exists.");
                    }
                    else
                    {
                        l_valid = validateElementScreenData();
                        if (l_valid)
                        {
                            writeCurrentElementRecord();
                            ((BonusDbService)i_dbService).insertElementData();
                            displayMessageDialog(i_contentPane,
                                                 "New record inserted.");
                            refreshElementListData();
                            initialiseElementsPanel();
                            i_state2 = C_STATE_INITIAL;
                        }
                    }
                }
                break;
            }
            case C_STATE_DATA_MODIFIED:
            {
                l_valid = validateElementScreenData();
                if (l_valid)
                {
                    l_selectedIndex = i_keyElementDataComboBox.getSelectedIndex();
                    writeCurrentElementRecord();
                    //i_dbService.updateData();
                    ((BonusDbService)i_dbService).updateElementData();
                    displayMessageDialog(i_contentPane,
                                         "Record has been updated.");
                    refreshElementListData();
                    i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
                    i_keyElementDataComboBox.setSelectedIndex(l_selectedIndex);
                    i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
                    i_state2 = C_STATE_DATA_RETRIEVED;
                }
                break;
            }
        }
    }    

    /**
     * Converts an integer to the highest order bit that has been set. Note that its expected that 
     * only one bit is set in the integer.
     * @param p_servOfferingsInt The supplied integer.
     * @return The highest order bit that has been set in the supplied integer.
     */
    private int convertIntToSeofBit(int p_servOfferingsInt)
    {
        String l_binString = Integer.toBinaryString(p_servOfferingsInt);
        return l_binString.length();
    }
    
    /**
     * Extracts the selected service offering and returns a Long representing the decimal value.
     * @param p_servOfferingsBit
     * @return Decimal value corresponding to 2^(selected service offering)
     */
    private Long convertSeofBitToInt(Object p_servOfferingsBit)
    {
        Long l_return = null;
      
        if (p_servOfferingsBit instanceof BoiServiceOfferingData)
        {
            int l_servOff;
            l_servOff = ((BoiServiceOfferingData)p_servOfferingsBit).
                                getInternalSeofData().getServiceOfferingNumber();
            
            int l_int = (1 << (l_servOff - 1));
                 
            l_return = Long.valueOf(String.valueOf(l_int));
        }
         
        return l_return;
    }
    
    /**
     * Sets all the fields in the Elements panel to the specified status. 
     * @param p_enabledStatus
     */
    private void setElementsPanelFieldsEnabledStatus(boolean p_enabledStatus)
    {
        i_keyElementDataComboBox.setEnabled(p_enabledStatus);
        i_element1Field.setEnabled(p_enabledStatus);
        i_element2Field.setEnabled(p_enabledStatus);
        i_element3Field.setEnabled(p_enabledStatus);
    }
    
    /**
     * Create table to display bonus elements.
     */
    private void createBonElemTable()
    {
        JScrollPane l_scrollPane = null;
        int[]       l_columnWidths = new int[] {100,0,100,40};

        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(l_columnWidths, 150, 50, i_stringTableModel, this);
        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedBonusElementsPanel.add(l_scrollPane, "i_table,17,24,39,21");

        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
    
    /**
     * Adds a listener to listen for data modifications.  Listener is dependent
     * upon the class.
     * @param p_object Object to add listener for.
     */
    protected void addValueChangedListenerEl(Object p_object)
    {
        if (p_object instanceof ValidatedJTextField)
        {
            ((ValidatedJTextField)p_object).getDocument().addDocumentListener(i_guiDocumentListenerEl);
        }
        else if (p_object instanceof JFormattedTextField)
        {
            ((JFormattedTextField)p_object).addPropertyChangeListener("value", i_guiPropertyChangeListenerEl);
        }
        else if (p_object instanceof JComboBox)
        {
            ((JComboBox)p_object).addItemListener(i_guiItemListenerEl);
        }
        else if (p_object instanceof JCheckBox)
        {
            ((JCheckBox)p_object).addItemListener(i_guiItemListenerEl);
        }
        else if (p_object instanceof ValidatedJPasswordField)
        {
            ((ValidatedJPasswordField)p_object).getDocument().addDocumentListener(i_guiDocumentListener);
        }
        else
        {
            System.out.println("Failed to add a listener for this type of object");
        }
    }
    
    /**
     * Populates the Codes Table.
     */
    private void populateCodesTable()
    {
        BoneBonusElementData l_bonusElementData = null;
        int l_numRecords = 0;
        int l_arraySize = 0;
        if (c_debug) System.out.println("Entered populateCodesTable(), i_state: " + i_state);
        l_numRecords = i_definedBonusElements.size();
        
        l_arraySize = (i_keyDataComboBox.getSelectedIndex() == 0 || 
                          i_keyDataComboBox.getSelectedIndex() == 1) ? 7 :
                              (l_numRecords > 6) ? l_numRecords : 7;
        
        i_data = new String[l_arraySize][4];

        // Blank the table if no bonus scheme is selected.
        if ((i_keyDataComboBox.getSelectedIndex() == 0) || (i_keyDataComboBox.getSelectedIndex() == 1))
        {
            for (int i = 0; i < 7; i++)
            {
                for (int j=0; j < 4; j++)
                {
                    i_data[i][j] = "";
                }
            }
        }
        else
        {
            // Start with third element of i_definedBonusElements as we dont want "" or "NEW RECORD" elements.
            for (int i = 2, j = 0; i < i_definedBonusElements.size(); i++, j++)
            {
                String[] l_bonusDataKE = null;
                l_bonusElementData = ((BoiBonusElementData)i_definedBonusElements
                                         .elementAt(i)).getInternalBonusElementData();
            
                if (l_bonusElementData.getKeyElements() != null)
                {
                    l_bonusDataKE = l_bonusElementData.getKeyElements().split(",", -1);
                    i_data[j][0] = l_bonusDataKE[c_channelIndex];
                    i_data[j][1] = (l_bonusDataKE.length > 1) ? l_bonusDataKE[1] : "";
                    i_data[j][2] = (l_bonusDataKE.length > 2) ? l_bonusDataKE[c_topUpTypeIndex] : "";
                }
                else
                {
                    i_data[j][0] = "";
                    i_data[j][1] = "";
                    i_data[j][2] = "";
                }

                i_data[j][3] = l_bonusElementData.isBonusActive() ? "Y" : "N";
            }
         }

         i_stringTableModel.setData(i_data);
         if (c_debug) System.out.println("Leaving populateCodesTable()");
    }

    /**
     * Convenience method to get a new formatter
     * @param p_format     NumberFormat object to associate with formatter.
     * @param p_length     Maximum number of digits before decimal place.
     * @param p_precision  Maximum number of digits after decimal place.
     * @return The number formatter
     */
    private FixedSizeNumberFormatter getNewFormatter(NumberFormat p_format,
                                                     int          p_length,
                                                     int          p_precision)
    {
        FixedSizeNumberFormatter l_formatter = new FixedSizeNumberFormatter(p_format,
                                                                            p_length,
                                                                            p_precision,
                                                                            false);
        l_formatter.setAllowsInvalid(false);
        l_formatter.setCommitsOnValidEdit(true);
        
        return l_formatter;
    }
  
    //-----------------------------------------------------------------------------
    // Inner classes
    //-----------------------------------------------------------------------------
    
    /**
     * Class to listen on key element data selection combo box.  Handles selection of NEW ELEMENT,
     * existing record, or blank.
     */
    protected class KeyElementDataComboListener
    implements ItemListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Combo box event handler for the key element data combo.
         * @param p_itemEvent Event to handle.
         */
        public void itemStateChanged(ItemEvent p_itemEvent)
        {
            String l_selectedItem; 
            int    l_response = 0;
            if (c_debug)
            {
                System.out.println("Entered KeyElementDataComboListener.itemStateChanged()");
                System.out.println("    i_state2: " + i_state2);
                System.out.println("    p_itemEvent.getStateChange(): " + p_itemEvent.getStateChange());
            }

            if (p_itemEvent.getStateChange() == ItemEvent.SELECTED)
            {
                l_selectedItem = ((JComboBox)p_itemEvent.getSource()).getSelectedItem().toString();
                if (c_debug) System.out.println("    l_selectedItem: " + l_selectedItem);

                if (l_selectedItem.equals("NEW ELEMENT"))
                {
                    switch (i_state2)
                    {
                        case C_STATE_INITIAL:
                        {
                            i_state2 = C_STATE_NEW_RECORD;
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        {
                            initialiseElementsPanel();
                            i_state2 = C_STATE_NEW_RECORD;
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseElementsPanel();
                                i_state2 = C_STATE_NEW_RECORD;
                            }
                            else
                            {
                                i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
                                resetSelectionForKeyElementCombo();
                                i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
                            }
                            break;
                        }
                    }
                }
                else if (l_selectedItem.equals(""))
                {
                    switch (i_state2)
                    {
                        case C_STATE_INITIAL:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        {
                            initialiseElementsPanel();
                            i_state2 = C_STATE_INITIAL;
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseElementsPanel();
                                i_state2= C_STATE_INITIAL;
                            }
                            else
                            {
                                // Reset to NEW_RECORD. Should always be element 1 in dropdown.
                                i_keyElementDataComboBox.setSelectedIndex(1);
                            }
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                         i_contentPane,
                                         "Data modifications will be lost. Do you wish to continue?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseElementsPanel();
                                i_state2 = C_STATE_INITIAL;
                            }
                            else
                            {
                                i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
                                resetSelectionForKeyElementCombo();
                                i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
                            }
                            break;
                        }
                    }
                }
                else
                {
                    try
                    {
                        switch (i_state2)
                        {
                            case C_STATE_INITIAL:
                            case C_STATE_DATA_RETRIEVED:
                            {
                                writeKeyData();
                                i_dbService.readData();
                                refreshCurrentElementRecord();
                                i_state2 = C_STATE_DATA_RETRIEVED;
                                break;
                            }
                            case C_STATE_NEW_RECORD:
                            {
                                l_response = displayConfirmDialog(
                                             i_contentPane,
                                             "Data modifications will be lost. Do you wish to continue?");
        
                                if (l_response == JOptionPane.YES_OPTION)
                                {
                                    writeKeyData();
                                    i_dbService.readData();
                                    refreshCurrentElementRecord();
                                    i_state2 = C_STATE_DATA_RETRIEVED;
                                }
                                else
                                {
                                    // Reset to NEW_RECORD. Should always be element 1 in dropdown.
                                    i_keyElementDataComboBox.setSelectedIndex(1);
                                }
                                break;
                            }
                            case C_STATE_DATA_MODIFIED:
                            {
                                l_response = displayConfirmDialog(
                                             i_contentPane,
                                             "Data modifications will be lost. Do you wish to continue?");
        
                                if (l_response == JOptionPane.YES_OPTION)
                                {
                                    writeKeyData();
                                    i_dbService.readData();
                                    refreshCurrentElementRecord();
                                    i_state2 = C_STATE_DATA_RETRIEVED;
                                }
                                else
                                {
                                    i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
                                    resetSelectionForKeyElementCombo();
                                    i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
                                }
                                break;
                            }
                        }
                    }
                    catch (PpasServiceFailedException l_e)
                    {
                        handleException(l_e);
                    }
                }

                if ( (i_state2 == C_STATE_NEW_RECORD) ||
                     (i_state2 == C_STATE_INITIAL) )
                {
                    updateScreenForNewElementRecord();
                }
                else
                {
                    updateScreenForExistingElementRecord();
                }
            }
            
            if (c_debug) System.out.println("Leaving KeyElementDataComboListener.itemStateChanged()");
        }
    }
    
    /**
     * Class to listen on Update, Delete, and Reset buttons.
     */
    protected class BonusElementButtonListener
        implements ActionListener
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------

        /**
         * Event handler for the common business config buttons: Update, Delete,
         * and Reset.
         * @param p_actionEvent Event to handle.
         */
        public void actionPerformed(ActionEvent p_actionEvent)
        {
            Object    l_source = null;
            int       l_response = 0;
        
            l_source = p_actionEvent.getSource();
            if (c_debug)
            {
                System.out.println("Entered BonusElementButtonListener.actionPerformed()");
                System.out.println("    l_source: " + l_source);
            }
            
            try
            {
                if (l_source == i_updateButtonEl)
                {
                    // Request focus on update button such that focus lost processing
                    // is performed on previous focus owner.  
                    i_updateButtonEl.requestFocusInWindow();
                    
                    // Perform update processing after the current focus lost event.
                    // i.e. After field validation has occurred for previous focus owner.
                    SwingUtilities.invokeLater(new UpdateElementRunnable());
                }
                else if (l_source == i_deleteButtonEl)
                {
                    switch (i_state2)
                    {
                        case C_STATE_INITIAL:
                        {
                            displayMessageDialog(
                                       i_contentPane,
                                       "Retrieve data before deleting.");
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            displayMessageDialog(
                                       i_contentPane,
                                       "Select existing record before hitting delete.");
                            break;
                        }
                        case C_STATE_DATA_RETRIEVED:
                        case C_STATE_DATA_MODIFIED:
                        {
                            if ( !validateDeletion() )
                            {
                                // Cannot delete this row
                                break;
                            }
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Are you sure you wish to delete this record?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                writeKeyData();
                                ((BonusDbService)i_dbService).deleteElementData(
                                                                  buildKeyElementsString(),
                                                                  C_NON_KEY_ELEMENTS_VALUE);
                                refreshElementListData();  
                                initialiseElementsPanel();
                                i_state2 = C_STATE_INITIAL;
                            }
                            break;
                        }
                    }
                }
                else if (l_source == i_resetButtonEl)
                {
                    switch (i_state2)
                    {
                        case C_STATE_INITIAL:
                        case C_STATE_DATA_RETRIEVED:
                        {
                            // No action
                            break;
                        }
                        case C_STATE_NEW_RECORD:
                        {
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Record not saved. Are you sure you wish to reset?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                initialiseElementsPanel();
                                if (i_keyElementDataComboBox.getItemCount() > 0)
                                {
                                    i_keyElementDataComboBox.removeItemListener(i_keyElementDataComboListener);
                                    i_keyElementDataComboBox.setSelectedIndex(0);
                                    i_keyElementDataComboBox.addItemListener(i_keyElementDataComboListener);
                                }
                                i_state2 = C_STATE_INITIAL;
                            }
                            break;
                        }
                        case C_STATE_DATA_MODIFIED:
                        {
                            l_response = displayConfirmDialog(
                                       i_contentPane,
                                       "Data has been modified. Are you sure you wish to reset?");
        
                            if (l_response == JOptionPane.YES_OPTION)
                            {
                                writeKeyData();
                                i_dbService.readData();
                                refreshCurrentElementRecord();
                                i_state2 = C_STATE_DATA_RETRIEVED;
                            }
                            break;
                        }
                    }
                }
                if (c_debug) System.out.println("Leaving BonusElementButtonListener.actionPerformed()");
            }
            catch (PpasServiceFailedException l_e)
            {
                handleException(l_e);
            }
        }
    }
    
    /**
     * DocumentListener class.
     */
    private class GuiDocumentListenerEl
    implements DocumentListener
    {
        /**
         * Gives notification that there was an insert into the document. 
         * @param p_event the document event
         */
        public void insertUpdate(DocumentEvent p_event) 
        {        
            modifyState2();
        }

        /**
         * Gives notification that a portion of the document has been 
         * removed.
         * @param p_event the document event
         */
        public void removeUpdate(DocumentEvent p_event) 
        {
            modifyState2();
        }

        /**
         * Gives notification that an attribute or set of attributes changed.
         * @param p_event the document event
         */
        public void changedUpdate(DocumentEvent p_event) 
        {
            modifyState2();
        }
    }

    /**
     * ItemListener class.
     */
    private class GuiItemListenerEl implements ItemListener
    {
        /**
         * Invoked when an item has been selected or deselected by the user.
         * @param p_itemEvent Event to be handled.
         */    
        public void itemStateChanged(ItemEvent p_itemEvent)
        {
            modifyState2();
        }
    }

    /**
     * PropertyChangeListener class.
     */
    private class GuiPropertyChangeListenerEl implements PropertyChangeListener
    {
        /**
         * This method gets called when a bound property is changed.
         * @param p_event A PropertyChangeEvent object describing the event source 
         *                and the property that has changed.
         */
        public void propertyChange(PropertyChangeEvent p_event)
        {
            // Check whether the value has changed, since this event seems to get fired
            // during field initialisation when both old and new values are null.
            if (p_event.getNewValue() != p_event.getOldValue())
            {
                modifyState2();
            }
        }
    }
    
    /**
     * Class to enable update processing to be performed in a separate thread.
     */
    public class UpdateElementRunnable implements Runnable
    {
        //-------------------------------------------------------------------------
        // Public methods
        //-------------------------------------------------------------------------
        
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            // Only do this if focus was successfully transferred to the update button.
            // If it is not the focus owner, this would indicate there was a parse
            // error with the previous focus owner.
            if (i_updateButtonEl.isFocusOwner())
            {
                try
                {
                    doUpdateElement();
                }
                catch (PpasServiceFailedException l_e)
                {
                    handleException(l_e);
                }
            }
        }
    }
}
