////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiServiceOfferingData.java
//      DATE            :       20-Aug-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PpacLon#495/3784
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for SeofServiceOfferingData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;

/** Wrapper for service offering class within BOI. */
public class BoiServiceOfferingData extends DataObject
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** Wrappered service offering data object */
    private SeofServiceOfferingData i_serviceOfferingData;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_serviceOfferingData Service offering data object to wrapper.
     */
    public BoiServiceOfferingData(SeofServiceOfferingData p_serviceOfferingData)
    {
        i_serviceOfferingData = p_serviceOfferingData;
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Return wrappered service offering data object.
     * @return Wrappered service offering data object.
     */
    public SeofServiceOfferingData getInternalSeofData()
    {
        return i_serviceOfferingData;
    }

    /**
     * Compares two service offering data objects and returns true if they equate.
     * @param p_serviceOfferingData Service offering data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_serviceOfferingData)
    {
        boolean l_return = false;
        
        if ( p_serviceOfferingData != null &&
             p_serviceOfferingData instanceof BoiServiceOfferingData &&
             i_serviceOfferingData.getServiceOfferingNumber() == ((BoiServiceOfferingData)p_serviceOfferingData).
                                                        getInternalSeofData().getServiceOfferingNumber())
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns service offering data object as a String for display in BOI.
     * @return Service offering data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_serviceOfferingData.getServiceOfferingNumber() + 
                                            " - " + i_serviceOfferingData.getServiceOfferingDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_serviceOfferingData.getServiceOfferingNumber();
    }
}
