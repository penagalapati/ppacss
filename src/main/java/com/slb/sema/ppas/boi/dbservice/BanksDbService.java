////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BankCodesDbService.java
//    DATE            :       19-Oct-2005
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PpacLon#1755/7279
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Database access class for Bank Codes screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy  | <name>     | <brief description>             | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiBanksData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BankBankSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;


/**
 * Database access class for Bank Codes screen.
 */
public class BanksDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Private variables (instance attributes).
    //-------------------------------------------------------------------------
    /** SQL service for bank data details. */
    private BankBankSqlService i_bankSqlService     = null;

    /** Vector of bank data records. */
    private Vector             i_availableBankDataV = null;
    
    /** Bank data. */
    private BoiBanksData       i_banksData          = null;

    /** Currently selected bank code. Used as key for read and delete. */
    private String             i_currentCode        = null;       
    

    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public BanksDbService(BoiContext p_context)
    {
        super(p_context);
        i_bankSqlService = new BankBankSqlService(null);
        i_availableBankDataV = new Vector(25); 
    }

    //-------------------------------------------------------------------------
    // Public methods.
    //-------------------------------------------------------------------------
    /** 
     * Set current bank code. Used for record lookup in database. 
     * @param p_code Currently selected bank code. 
     */
    public void setCurrentCode(String p_code)   
    {
        BoiBanksData l_banksData = null;
        if (!p_code.equals(i_currentCode))
        {
            for (int l_ix = 2; l_ix < i_availableBankDataV.size(); l_ix++)
            {
                l_banksData = (BoiBanksData)i_availableBankDataV.elementAt(l_ix);
                if (l_banksData.getInternalBankData().getBankCode().equals(p_code))
                {
                    i_banksData = l_banksData;
                    break;
                }
            }
        }
        i_currentCode = p_code;
    }

    /** 
     * Return the bank data currently being worked on. 
     * @return Bank data object.
     */
    public BoiBanksData getBanksData()
    {
        return i_banksData;
    }

    /** 
     * Set tha bank data currently being worked on.
     *  
     * @param p_banksData  the bank data object.
     */
    public void setBanksData(BoiBanksData p_banksData)
    {
        i_banksData = p_banksData;
    }

    /** 
     * Return available bank data records. 
     * @return Vector of available bank data records.
     */
    public Vector getAvailableBanksData()
    {
        return i_availableBankDataV;
    }


    //-------------------------------------------------------------------------
    // Protected methods.
    //-------------------------------------------------------------------------
    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection) throws PpasSqlException
    {
        BankBankDataSet l_bankDataSet = null;
        BankBankData    l_bankData    = null;
        
        l_bankDataSet = i_bankSqlService.readBankDetails(null, p_connection);
        l_bankData = l_bankDataSet.getBankBankData(i_currentCode);
        i_banksData = new BoiBanksData(l_bankData);
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshBankDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the BANK_BANK table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection) throws PpasSqlException
    {
        i_bankSqlService.insertBankDetails(null,
                                           p_connection,
                                           i_context.getOperatorUsername(),                                
                                           i_banksData.getInternalBankData().getBankCode(),
                                           i_banksData.getInternalBankData().getBankName());        
        
        refreshBankDataVector(p_connection);
    }

    /** 
     * Updates record in the BANK_BANK table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_bankSqlService.updateBankDetails(null,
                                           p_connection,
                                           i_context.getOperatorUsername(),                                
                                           i_banksData.getInternalBankData().getBankCode(),
                                           i_banksData.getInternalBankData().getBankName());        

        refreshBankDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the BANK_BANK table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_bankSqlService.deleteBankDetails(null,
                                           p_connection,
                                           i_context.getOperatorUsername(),                                
                                           i_banksData.getInternalBankData().getBankCode());        
              
        refreshBankDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * 
     * @param p_connection  The database connection.
     * 
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     *                        
     * @throws PpasSqlException SQL-related exception.
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        BankBankDataSet l_bankDataSet = null;
        BankBankData    l_bankData    = null;
        boolean[]       l_flagsArray  = new boolean[2];
        
        l_bankDataSet = i_bankSqlService.readBankDetails(null, p_connection);
        l_bankData    = l_bankDataSet.getBankBankData(i_currentCode);

        if (l_bankData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = l_bankData.isWithdrawn();
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }      

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the BANK_BANK table.
     * 
     * @param p_connection  The database connection.
     * 
     * @throws PpasSqlException SQL-related exception.
     */    
    protected void markAsAvailable(JdbcConnection p_connection) throws PpasSqlException
    {
        i_bankSqlService.markAsAvailable(null, p_connection, i_currentCode, i_context.getOperatorUsername());
        refreshBankDataVector(p_connection);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available bank data vector from the BANK_BANK table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshBankDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        BankBankDataSet l_bankDataSet = null;
        Vector          l_bankDataVec = null;
        BankBankData    l_bankData    = null;
        
        i_availableBankDataV.removeAllElements();
        
        l_bankDataSet = i_bankSqlService.readBankDetails(null, p_connection);
        l_bankDataVec = l_bankDataSet.getAvailableBankBankSet();
        i_availableBankDataV.addElement(new String(""));
        i_availableBankDataV.addElement(new String("NEW RECORD"));
        for (int i = 0; i < l_bankDataVec.size(); i++)
        {
            l_bankData = (BankBankData)l_bankDataVec.elementAt(i);
            i_availableBankDataV.addElement(new BoiBanksData(l_bankData));
        }
    }
}
