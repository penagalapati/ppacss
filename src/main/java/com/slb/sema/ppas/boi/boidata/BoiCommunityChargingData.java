////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiCommunityChargingData.java
//      DATE            :       19-Jul-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#338/3249
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for CochCommunityChargingData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;

/** Wrapper for community charging class within BOI. */
public class BoiCommunityChargingData extends DataObject
{
    /** Basic community charging data object */
    private CochCommunityChargingData i_chargingData;
    
    /**
     * Simple constructor.
     * @param p_chargingData Community charging data object to wrapper.
     */
    public BoiCommunityChargingData(CochCommunityChargingData p_chargingData)
    {
        i_chargingData = p_chargingData;
    }

    /**
     * Return wrappered community charging data object.
     * @return Wrappered community charging data object.
     */
    public CochCommunityChargingData getInternalChargingData()
    {
        return i_chargingData;
    }

    /**
     * Compares two community charging data objects and returns true if they equate.
     * @param p_chargingData Community charging data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_chargingData)
    {
        boolean l_return = false;
        
        if ( p_chargingData != null &&
             p_chargingData instanceof BoiCommunityChargingData &&
             i_chargingData.getCommunityChgId() == 
                 ((BoiCommunityChargingData)p_chargingData).getInternalChargingData().getCommunityChgId() )
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns community charging data object as a String for display in BOI.
     * @return Community charging data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_chargingData.getCommunityChgId() + 
                                            " - " + i_chargingData.getCommunityChgDesc());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_chargingData.getCommunityChgId();
    }
}