////////////////////////////////////////////////////////////////////////////////
//        ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME   : ChangePasswordPane.java
//        DATE        : 13-Sep-2005
//        AUTHOR      : M I Erskine
//        REFERENCE   : PpacLon#1483/7096
//
//        COPYRIGHT   : SchlumbergerSema 2005
//
//        DESCRIPTION : BOI Change Password screen.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 29/08/06 | A Kutthan  | Changed Password field length   | PpacLon#2595/9828
//          |            | within init() method            |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.swing.components.GridLayoutManager;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.ValidatedJPasswordField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.util.support.Debug;

/**
 * Boi Login screen class.
 */
public class ChangePasswordPane extends FocussedBoiGuiPane
{
    /** Maximum (inclusive) number of characters permitted in new strong password. */
    private static final int C_PASSWORD_MAX_STRONG_LENGTH = 20;
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Used for calls to middleware. */
    private static final String     C_CLASS_NAME = "ChangePasswordPane";

    /** Panel to contain the fields panel. */
    private JPanel                  i_loginPanel;

    /** Panel to contain the login fields. */
    private JPanel                  i_fieldsPanel;

    /** Field to contain the username. */
    private JLabel                  i_username;

    /** Field to contain the password. */
    private ValidatedJPasswordField i_password;
    
    /** Field to contain the confirmed password. */
    private ValidatedJPasswordField i_confirmPasswordField;

    /** Button to attempt login to BOI. */
    private GuiButton               i_changePasswordButton;

    /** Button to reset the username and password text fields. */
    private GuiButton               i_resetButton;

    /** The main panel for this screen. */
    private JPanel                  i_mainPanel;

    /** Combo box containing help topics for the screen. */
    private JComboBox               i_helpComboBox;
    
    /** The name of the host server. */
    private String                   i_serverHost   = "";
    
    /** The port on which to log in. */
    private String                   i_loginPort    = "";


    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor to create a new instance of BoiLogin.
     * @param p_context Context object for BOI
     */
    public ChangePasswordPane(BoiContext p_context)
    {
        super(p_context, (Container)null);
        i_serverHost = (String)i_context.getMandatoryObject("applet.server.host");

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 10000, "Server host from context = " + i_serverHost);
        }

        i_loginPort = (String)i_context.getMandatoryObject("applet.server.login.port");

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 10010, "Login Port from Context = " + i_loginPort);
        }
        
        init();
    }

    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------

    /**
     * Event handler method for GUI events.
     * @param p_event Event to handle.
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing.
    }

    /**
     * Returns Login button.
     * @return Login button.
     */
    public JButton getLoginButton()
    {
        return (i_changePasswordButton);
    }

    /**
     * Sets the field that is to have the default focus on entering the screen.
     */
    public void defaultFocus()
    {   
        i_password.setVerifyInputWhenFocusTarget(false);
        i_password.requestFocusInWindow();
        i_password.setVerifyInputWhenFocusTarget(true);
    }
    
    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        getContentPane().setVisible(true);
        i_password.getParent().getParent().setVisible(false);
        i_password.getParent().getParent().setVisible(true);
        i_resetButton.doClick();
    }

    /**
     * Method to perform an action depending on an action event supplied.
     * @param p_event The ActionEvent object that caused this action
     */
    public void actionPerformed(ActionEvent p_event)
    {
        Object l_source;

        l_source = p_event.getSource();

        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME, 11000, "Got action:" + p_event.getActionCommand());
        }

        if (l_source == i_changePasswordButton)
        {
            if (Debug.on)
            {
                Debug.print(C_CLASS_NAME, 11010, "Change Password selected");
            }

            if (//i_username.getText().equals("") || 
                new String(i_password.getPassword()).equals("") ||
                new String(i_confirmPasswordField.getPassword()).equals(""))
            {
                displayMessageDialog(i_contentPane, "Password cannot be blank", false, false);

                if (i_password.getText().equals(""))
                {
                    i_password.requestFocusInWindow();
                }
                else if (i_confirmPasswordField.getText().equals(""))
                {
                    i_confirmPasswordField.requestFocusInWindow();
                }
            }
            else
            {
                changePassword();
            }
        }
        else if (l_source == i_resetButton)
        {
            if (Debug.on)
                Debug.print(C_CLASS_NAME, 11020, "Reset selected");

            reset();
        }
        else
        {
            if (Debug.on)
                Debug.print(C_CLASS_NAME, 11030, "Unknown action performed");
        }
    }

    /**
     * Must implement this inherited method.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        // Do nothing.
    }

    /**
     * Key event handler.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;

        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()))
        {
            if (p_keyEvent.getKeyCode() == 10)
            {
                if (p_keyEvent.getID() == KeyEvent.KEY_PRESSED)
                {
                    l_return = true;
                    i_changePasswordButton.doClick();
                }
            }
        }

        return l_return;
    }

    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent p_event)
    {
    }

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent p_event)
    {
    }

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent p_event)
    {
    }

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent p_event)
    {
    }

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent p_event)
    {
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Method used to perform the basic setup of the screen, etc.
     */
    private void init()
    {
        JLabel l_usernameLabel;
        JLabel l_passwordLabel;
        JLabel l_confirmPasswordLabel;
        BoiHelpListener l_helpComboListener;

        i_mainPanel = new JPanel();
        i_mainPanel.setLayout(new GridLayoutManager(100, 100, 0, 0));

        i_mainPanel.setBackground(WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        i_mainPanel.setVisible(true);
        i_mainPanel.setFocusable(false);
        i_mainPanel.setRequestFocusEnabled(false);

        i_loginPanel = WidgetFactory.createMainPanel("Change Password", 100, 35, 0, 0);
        i_loginPanel.setVisible(true);
        i_loginPanel.setFocusable(false);
        i_loginPanel.setRequestFocusEnabled(false);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_CHANGE_PASSWORD_SCREEN), l_helpComboListener);
        i_helpComboBox.setFocusable(false);

        i_fieldsPanel = WidgetFactory.createPanel(50, 30, 0, 0);

        l_usernameLabel = WidgetFactory.createLabel("User ID :");
        l_passwordLabel = WidgetFactory.createLabel("Password :");
        l_confirmPasswordLabel = WidgetFactory.createLabel("Confirm Password :");
        l_passwordLabel.setForeground(WidgetFactory.C_COLOUR_FIELDS);

        i_username = WidgetFactory.createLabel(((BoiContext)i_context).getOperatorUsername());
        i_username.setHorizontalAlignment(SwingConstants.LEFT);

        i_password = WidgetFactory.createPasswordField(C_PASSWORD_MAX_STRONG_LENGTH);
        
        i_confirmPasswordField = WidgetFactory.createPasswordField(C_PASSWORD_MAX_STRONG_LENGTH);

        i_changePasswordButton = WidgetFactory.createButton("Change Password", this, true);
        i_resetButton = WidgetFactory.createButton("Reset", this, false);

        i_fieldsPanel.add(l_usernameLabel, "usernameLabel,1,5,15,5");
        i_fieldsPanel.add(l_passwordLabel, "passwordLabel,1,11,15,5");
        i_fieldsPanel.add(l_confirmPasswordLabel, "confirmPassworkLabel,1,17,15,5");
        i_fieldsPanel.add(i_username, "username,17,5,8,5");
        i_fieldsPanel.add(i_password, "password,17,11,14,5");
        i_fieldsPanel.add(i_confirmPasswordField, "confirmPasswordField,17,17,14,5");
        i_fieldsPanel.add(i_changePasswordButton, "changePasswordButton,1,26,20,5");
        i_fieldsPanel.add(i_resetButton, "resetButton,22,26,15,5");
        i_fieldsPanel.setFocusCycleRoot(true);

        i_loginPanel.add(i_helpComboBox, "loginHelpComboBox,60,1,40,4");
        i_loginPanel.add(i_fieldsPanel, "fieldsPanel,1,6,50,30");
        i_mainPanel.add(i_loginPanel, "loginPanel,1,1,100,35");

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_contentPane = i_mainPanel;
    }
    
    /** Used in call to middleware. */
    private static final String C_METHOD_changePassword = "changePassword1";
    /**
     * Attempts to update the password details for the user.
     */
    private void changePassword()
    {
        JdbcConnection      l_connection     = null;
        InputStream         l_is             = null;
        BufferedReader      l_bir            = null;
        String[]            l_tokenizer      = null;
        int                 l_nextToken      = 0;
        boolean             l_valid          = false;
        String              l_loginUrlString = null;
        URL                 l_loginUrl       = null;
        URLConnection       l_urlConnection  = null;
        String              l_line           = null;
        String              l_param          = null;
        String              l_status         = null;
        
        // Validate the password
        l_valid = validateNewPasswords();
        
        if (l_valid)
        {
            try
            {
                // Password gets hashed on the server side.
                l_loginUrlString = "http://" + i_serverHost + ":" + i_loginPort
                        + "/ascs/boi/Login?command=CHANGE_PASSWORD&userName=" + i_username.getText()
                        + "&password=" + new String(i_password.getPassword()) + "&newPassword="
                        + i_password.getText();

                l_loginUrl = new URL(l_loginUrlString);
                l_urlConnection = l_loginUrl.openConnection();
                l_urlConnection.connect();
                l_is = l_urlConnection.getInputStream();
                l_bir = new BufferedReader(new InputStreamReader(l_is));

                l_line = l_bir.readLine();

                if (l_line != null)
                {
                    if (Debug.on)
                    {
                        Debug.print(C_CLASS_NAME, 12010, "Read line " + l_line);
                    }
                }
                else
                {
                    l_line = "";
                }

                l_tokenizer = l_line.split("[=& ]");
                while (l_nextToken < l_tokenizer.length)
                {
                    l_param = l_tokenizer[l_nextToken++];
                    if ("status".equals(l_param))
                    {
                        l_status = l_tokenizer[l_nextToken++];
                    }
                }

                if ("SUCCESS".equals(l_status))
                {
                    displayMessageDialog(i_contentPane, "Your password has been changed.", false, false);
                    notifyEvent(new GuiEvent("LOGGED_IN", this, ((BoiContext)i_context).getOpidPrivilege()));
                }
                else
                {
                    displayMessageDialog(i_contentPane, "An error occured.\nContact system administrator.", false, false);
                }
                    
            }
            catch (PpasSqlException l_sqlE)
            {
                try
                {
                    l_connection.getStatement().close(C_CLASS_NAME,
                                                      C_METHOD_changePassword,
                                                      10120,
                                                      this,
                                                      null);
                }
                catch (PpasSqlException l_pSqlE)
                {
                    // Ignore this time
                }
                handleException(l_sqlE);
            }
            catch (MalformedURLException l_e)
            {
                handleException(l_e, 
                                "Could not log into BOI.\nContact system administrator.");
            }
            catch (IOException l_e)
            {
                handleException(l_e, 
                                "Could not log into BOI.\nContact system administrator.");
            }
        }
    }
    
    /**
     * Checks the password configuration in syfg_system_config table and validates accordingly.
     * @return True if the password is valid and false otherwise.
     */
    private boolean validateNewPasswords()
    {
        String                     l_password        = null;
        String                     l_confirmPassword = null;
        boolean                    l_strongPassword  = false;
        SyfgSystemConfigSqlService l_syfgSqlService  = null;
        SyfgSystemConfigData       l_syfgData        = null;
        
        l_password = i_password.getText();
        l_confirmPassword = i_confirmPasswordField.getText();
        
        // Validate the new password
        if (!l_password.equals(l_confirmPassword))
        {
            displayMessageDialog(i_contentPane,
                                 "The password did not match the password confirmation field. " + 
                                 "Please enter password and password confirmation again.",
                                 false,
                                 false);
            i_password.setText("");
            i_confirmPasswordField.setText("");
            i_password.requestFocusInWindow();
            return false;
        }
        
        // Check whether this password is Strong and validate accordingly.
        l_syfgSqlService = new SyfgSystemConfigSqlService(null);
        
        try
        {
            l_syfgData = l_syfgSqlService.read(null, ((BoiContext)i_context).getConnection());
        }
        catch (PpasSqlException e)
        {
            handleException(e);
        }
        
        l_strongPassword = l_syfgData.getBoolean("PASSWORD", "PASSWORD_STRONG", "TRUE");
        
        if (l_strongPassword)
        {
            return validateStrongPassword(l_password);
        }
        
        return true;
    }

    
    /**
     * Private method to set the username and password fields to
     * be empty.
     */
    private void reset()
    {
        i_password.setText("");
        i_confirmPasswordField.setText("");
        i_password.requestFocusInWindow();
    }
}