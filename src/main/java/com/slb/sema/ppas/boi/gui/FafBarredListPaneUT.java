////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FafBarredListPaneUT.java
//      DATE            :       3-Feb-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#970/5761
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for FafBarredListPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiFafBarredListData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for FafBarredListPane. */
public class FafBarredListPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "FafBarredListPaneUT";
    
    /** Constant for use in number prefix field. */
    private static final String C_NUMBER_PREFIX = "5555";
    
    /** Constant for use in start date field. */
    private static final String C_START_DATE = "21-Aug-2030";
    
    /** Constant for use in end date field. */
    private static final String C_END_DATE = "21-Dec-2030";
    
    /** Constant for use in end date field. */
    private static final String C_NEW_END_DATE = "21-Jan-2031";
    
    /** Constant for use in Market field. */
    private static final BoiMarket C_MARKET = new BoiMarket(new Market(1, 2));
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Faf Barred Numbers screen. */
    private FafBarredListPane i_fabaPane;
    
    /** FaF number prefix field. */
    private JFormattedTextField i_numberPrefixField;
    
    /** FaF number start date field. */
    private JFormattedTextField i_startDateField;
    
    /** FaF number start date field. */
    private JFormattedTextField i_endDateField;
    
    /** Market combo box. */
    private JComboBox i_marketDataCombo;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public FafBarredListPaneUT(String p_title)
    {
        super(p_title);
        
        i_fabaPane = new FafBarredListPane(c_context);
        super.init(i_fabaPane);
        
        i_fabaPane.resetScreenUponEntry();
        
        i_marketDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "marketDataComboBox");
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_numberPrefixField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                 "numberPrefixField");
        i_startDateField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "startDateField");
        i_endDateField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "endDateField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(FafBarredListPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, and delete a record through the 
     *     FaF Barred Numbers screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("FafBarredListPane test");
        
        BoiFafBarredListData l_boiFabaData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setMarketComboSelectedItem(i_fabaPane, i_marketDataCombo, C_MARKET);
        SwingTestCaseTT.setKeyComboSelectedItem(i_fabaPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_numberPrefixField, C_NUMBER_PREFIX);
        SwingTestCaseTT.setTextComponentValue(i_startDateField, C_START_DATE);
        SwingTestCaseTT.setTextComponentValue(i_endDateField, C_END_DATE);
        doInsert(i_fabaPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiFabaData = createFafBarredListData(C_MARKET, C_NUMBER_PREFIX, 0, C_START_DATE);
        SwingTestCaseTT.setKeyComboSelectedItem(i_fabaPane, i_keyDataCombo, l_boiFabaData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_endDateField, C_NEW_END_DATE);
        doUpdate(i_fabaPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_fabaPane, i_deleteButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteFabaRecord(C_MARKET, C_NUMBER_PREFIX, 0, C_START_DATE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteFabaRecord(C_MARKET, C_NUMBER_PREFIX, 0, C_START_DATE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiFafBarredListData object with the supplied data.
     * @param p_market           Subscriber's market.
     * @param p_fabaNumberPrefix Number prefix.
     * @param p_fabaNumberLength Number length.
     * @param p_fabaStartDate    Start date time for the Barred Number.
     * @return BoiFafBarredListData object created from the supplied data.
     */
    private BoiFafBarredListData createFafBarredListData(BoiMarket p_market,
                                                         String    p_fabaNumberPrefix,
                                                         int       p_fabaNumberLength,
                                                         String    p_fabaStartDate)
    {
        FabaFafBarredListData l_fafBarredListData;
        
        l_fafBarredListData = new FabaFafBarredListData(null,
                                                        p_market.getMarket(),
                                                        p_fabaNumberPrefix,
                                                        p_fabaNumberLength,
                                                        new PpasDate(p_fabaStartDate),
                                                        null,
                                                        null,
                                                        null);
        
        return new BoiFafBarredListData(l_fafBarredListData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteFabaRecord = "deleteFabaRecord";
    
    /**
     * Removes a row from FABA_FAF_BARRED_LIST.
     * @param p_market           Subscriber's market.
     * @param p_fabaNumberPrefix Number prefix.
     * @param p_fabaNumberLength Number length.
     * @param p_fabaStartDate    Start date time for the Barred Number.
     */
    private void deleteFabaRecord(BoiMarket p_market,
                                  String    p_fabaNumberPrefix,
                                  int       p_fabaNumberLength,
                                  String    p_fabaStartDate)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = "DELETE from faba_faf_barred_list " +
                "WHERE faba_srva          = {0} " +
                "AND   faba_sloc          = {1} " +
                "AND   faba_number_prefix = {2} " +
                "AND   faba_number_length = {3} " +
                "AND   faba_start_date    = {4}";
        
        l_sqlString = new SqlString(200, 5, l_sql);
        
        l_sqlString.setIntParam   (0, p_market.getMarket().getSrva());
        l_sqlString.setIntParam   (1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam(2, p_fabaNumberPrefix);
        l_sqlString.setIntParam   (3, p_fabaNumberLength);
        l_sqlString.setDateParam  (4, new PpasDate(p_fabaStartDate));
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteFabaRecord);
    }
}
