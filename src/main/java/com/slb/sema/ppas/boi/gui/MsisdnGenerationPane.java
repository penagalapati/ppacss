////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MsisdnGenerationPane.java
//      DATE            :       26-Oct-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#882/4696
//                              PRD_ASCS00_GEN_CA_37
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       BOI Business Config Msisdn Generation screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.KeyboardFocusManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.slb.sema.ppas.boi.dbservice.MsisdnGenerationDbService;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;
import com.slb.sema.ppas.util.support.Debug;

/** BOI Reports submission screen. */
public class MsisdnGenerationPane extends FocussedBoiGuiPane
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "MsisdnGenerationPane";
    
    /** Default maximum range of MSISDNs. */
    private static final int C_DEFAULT_MSISDN_NUMBER_RANGE = 100000;

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Main panel for the screen. */
    private JPanel i_mainPanel;

    /** Panel for entering file generation details. */
    private JPanel i_generationPanel;

    /** Field for entering start MSISDN. */
    private JFormattedTextField i_startRangeMsisdnTextField;

    /** Field for entering end MSISDN. */
    private JFormattedTextField i_endRangeMsisdnTextField;

    /** SDP id combo box. */
    private JComboBox i_sdpIdComboBox;

    /** Button to generate file. */
    private JButton i_generateButton;

    /** Button to reset screen. */
    private JButton i_resetButton;
    
    /** BOI server host. */
    private String i_serverHost;
    
    /** BOI server port. */
    private String i_loginPort;
    
    /** Routing method: Number rANGE OR sdp ID. */
    private String i_routingMethod;
    
    /** Maximum range of MSISDNs. */
    private int i_maxNumberRange;

    /** Database access class for MSISDN generation screen. */
    private MsisdnGenerationDbService i_dbService;
    
    /** Vector of available SDP ids. */
    private Vector i_availableSdpIds;
    
    /** Combo box model for SDP ids. */
    private DefaultComboBoxModel i_sdpIdModel;
    
    /** Combo box containing help topics for the screen. */
    private JComboBox i_helpComboBox;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * MsisdnGenerationPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public MsisdnGenerationPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new MsisdnGenerationDbService((BoiContext)i_context);
        i_serverHost = (String)i_context.getMandatoryObject(
                           "applet.server.host");
            
        i_loginPort = (String)i_context.getMandatoryObject(
                          "applet.server.login.port");
        
        Object l_maxNumberRange = ((BoiContext)i_context).getObject("ascs.boi.maxNumberRange");
        
        try
        {
            i_maxNumberRange = 
                (l_maxNumberRange == null) ? C_DEFAULT_MSISDN_NUMBER_RANGE : 
                                                 Integer.parseInt((String)l_maxNumberRange);
        }
        catch (NumberFormatException l_nfE)
        {
            i_maxNumberRange = C_DEFAULT_MSISDN_NUMBER_RANGE;
            handleException(l_nfE);
        }
        paintScreen();

        i_contentPane = i_mainPanel;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
        i_mainPanel.setFocusCycleRoot(true);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Handles keyboard events for the screen.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        boolean l_return = false;

        if (i_contentPane.isAncestorOf(p_keyEvent.getComponent()) && p_keyEvent.getKeyCode() == 10
                && p_keyEvent.getID() == KeyEvent.KEY_PRESSED)
        {
            if (p_keyEvent.getComponent() == i_resetButton)
            {
                i_resetButton.doClick();
            }
            else
            {
                i_generateButton.doClick();
            }
            l_return = true;
        }
        return l_return;
    }

    /**
     * Handles button events.
     * @param p_actionEvent Action event.
     */
    public void actionPerformed(ActionEvent p_actionEvent)
    {
        Object  l_source = null;
        boolean l_valid = false;
        int     l_confirm = 0;

        l_source = p_actionEvent.getSource();

        if (l_source == i_generateButton)
        {
            l_valid = validateScreenData();
            
            if (l_valid)
            {
                l_confirm = displayConfirmDialog(i_contentPane, "Do you wish to submit this job?");
                
                if (l_confirm == JOptionPane.YES_OPTION)
                {
                    doGeneration();
                    displayMessageDialog(i_contentPane, "Job has been submitted");
                }
                else
                {
                    displayMessageDialog(i_contentPane, "Job submission cancelled");
                }
            }
        }
        else if (l_source == i_resetButton)
        {
            initialiseScreen();
        }
    }

    /**
     * Handles combo box events.
     * @param p_itemEvent Item event.
     */
    public void itemStateChanged(ItemEvent p_itemEvent)
    {
        // Do nothing
    }

    /**
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_startRangeMsisdnTextField.requestFocusInWindow())
        {
            System.out.println("Error in requestFocusInWindow!");
        }
    }

    /**
     * Resets the screen when the user selects it from the menu.
     */
    public void resetScreenUponEntry()
    {
        try
        {
            i_dbService.readInitialData();
            i_routingMethod = i_dbService.getRoutingMethod();
            setSdpIdComboState();
            initialiseScreen();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e)
    {
        // Do nothing
    }

    /**
     * Handles GUI events.
     * @param p_event The <code>GuiEvent</code> object
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        // Do nothing
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Validates screen data before submit.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        String l_startMsisdnStr = i_startRangeMsisdnTextField.getText().trim();
        String l_endMsisdnStr = i_endRangeMsisdnTextField.getText().trim();
        long l_startMsisdn = 0;
        long l_endMsisdn = 0;
        
        if (l_startMsisdnStr.length() == 0)
        {
            i_startRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please enter a start MSISDN");
            return false;
        }
        
        if (l_endMsisdnStr.length() == 0)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please enter an end MSISDN");
            return false;
        }
        
        if (l_startMsisdnStr.length() != l_endMsisdnStr.length())
        {
            i_startRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Both Msisdns must be the same length");
            return false;
        }
        
        l_startMsisdn = Long.parseLong(l_startMsisdnStr);
        l_endMsisdn =  Long.parseLong(l_endMsisdnStr);
        
        if ((l_endMsisdn - l_startMsisdn) < 0)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(
                i_contentPane,
                "Start range Msisdn must be less than End range Msisdn");
            return false;
        }
        
        if ((l_endMsisdn - l_startMsisdn + 1) > i_maxNumberRange)
        {
            i_endRangeMsisdnTextField.requestFocusInWindow();
            displayMessageDialog(
                i_contentPane, 
                "Maximum number range of " + i_maxNumberRange + " exceeded");
            return false;
        }
        
        if (i_routingMethod.equals("SDP") && i_sdpIdComboBox.getSelectedItem().toString().equals(""))
        {
            i_sdpIdComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Please supply an SDP ID");
            return false;
            
        }
        
        return true;
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    private void paintScreen()
    {
        BoiHelpListener l_helpComboListener;

        i_mainPanel = WidgetFactory.createMainPanel("MsisdnGeneration", 100, 100, 0, 0);

        l_helpComboListener = (BoiHelpListener)i_context.getObject("BoiHelpListener");
        i_helpComboBox = WidgetFactory.createComboBox(
                                       BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_MSISDN_GENERATION_SCREEN), 
                                       l_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createGenerationPanel();
        setSdpIdComboState();
        
        i_mainPanel.add(i_helpComboBox, "msisdnGenHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_generationPanel, "submissionPanel,1,6,100,95");
    }

    /** Creates the submission details panel. */
    private void createGenerationPanel()
    {
        JLabel l_startRangeLabel;
        JLabel l_endRangeLabel;
        JLabel l_sdpIdLabel;

        i_generationPanel = WidgetFactory.createPanel("Generate File", 100, 100, 0, 0);

        l_startRangeLabel = WidgetFactory.createLabel("Start range MSISDN: ");
        i_startRangeMsisdnTextField = WidgetFactory.createMsisdnField(15);
        addValueChangedListener(i_startRangeMsisdnTextField);
        
        l_endRangeLabel = WidgetFactory.createLabel("End range MSISDN: ");
        i_endRangeMsisdnTextField = WidgetFactory.createMsisdnField(15);
        addValueChangedListener(i_endRangeMsisdnTextField);

        l_sdpIdLabel = WidgetFactory.createLabel("SDP Id: ");
        
        try
        {
            i_dbService.readInitialData();
        }
        catch (PpasServiceFailedException l_e)
        {
            handleException(l_e);
        }
        
        i_availableSdpIds = i_dbService.getAvailableSdpIds();
        i_sdpIdComboBox = WidgetFactory.createComboBox(i_availableSdpIds);
        //i_sdpIdModel = new DefaultComboBoxModel(i_availableSdpIds);
        //i_sdpIdComboBox.setModel(i_sdpIdModel);
        addValueChangedListener(i_sdpIdComboBox);

        i_generateButton = WidgetFactory.createButton("Generate", this, true);
        i_resetButton = WidgetFactory.createButton("Reset", this, false);
        i_resetButton.setVerifyInputWhenFocusTarget(false);

        i_generationPanel.add(l_startRangeLabel, "startRangeLabel,1,1,20,4");
        i_generationPanel.add(i_startRangeMsisdnTextField, "startRangeMsisdnTextField,21,1,20,4");
        i_generationPanel.add(l_endRangeLabel, "endRangeLabel,1,6,20,4");
        i_generationPanel.add(i_endRangeMsisdnTextField, "endRangeMsisdnTextField,21,6,20,4");
        i_generationPanel.add(l_sdpIdLabel, "sdpIdLabel,1,11,20,4");
        i_generationPanel.add(i_sdpIdComboBox, "sdpIdComboBox,21,11,40,4");

        i_generationPanel.add(i_generateButton, "submitButton,1,34,15,5");
        i_generationPanel.add(i_resetButton, "resetButton,17,34,15,5");
    }

    /**
     * Initialises the screen data to default values.
     */
    private void initialiseScreen()
    {
        i_startRangeMsisdnTextField.setValue(null);
        i_endRangeMsisdnTextField.setValue(null);
        i_sdpIdComboBox.setSelectedItem("");

        i_state = C_STATE_DATA_RETRIEVED;
    }

    /**
     * Handles submit events.
     */
    private void doGeneration()
    {
        URLConnection       l_urlConnection;
        URL                 l_fileListingUrl = null;
        InputStream         l_is;
        BufferedReader      l_bir;
        String              l_line           = "";
        String              l_status         = "";
        String              l_fileListingUrlString;
            
        l_fileListingUrlString = "http://" + i_serverHost + ":" + i_loginPort +
            "/ascs/boi/FileServices?command=GENERATEFILE" +
            "&userName=" + ((BoiContext)i_context).getOperatorUsername() + 
            "&password=" + ((BoiContext)i_context).getOperatorPassword() +
            "&startMsisdn=" + i_startRangeMsisdnTextField.getText().trim() +
            "&endMsisdn=" + i_endRangeMsisdnTextField.getText().trim() +
            "&sdpId=" + i_sdpIdComboBox.getSelectedItem();
                                     
        try
        {
            l_fileListingUrl = new URL(l_fileListingUrlString);

            l_urlConnection = l_fileListingUrl.openConnection();
            l_urlConnection.connect();
            
            i_state = C_STATE_DATA_RETRIEVED;

            l_is = l_urlConnection.getInputStream();
            l_bir = new BufferedReader(new InputStreamReader(l_is));             
            l_line = l_bir.readLine();

            if (l_line != null)
            {
                if (Debug.on)
                {
                    Debug.print(
                        C_CLASS_NAME,
                        12010, 
                        "Read line " + l_line);
                }
            }
        }
        catch (MalformedURLException l_e)
        {
            handleException(l_e);
        }
        catch (IOException l_e1)
        {
            handleException(l_e1);
        }
                
        if (Debug.on)
        {
            Debug.print(
                C_CLASS_NAME,
                12070, 
                "Msisdn File Generation status: " + l_status);
        }
    }
    
    /**
     * Method to disable the SDP Id combo box depending on
     * whether number range routing is configured.
     */
    private void setSdpIdComboState()
    {
        if (i_routingMethod != null)
        {
            if (i_routingMethod.equals("NBR"))
            {
                // Disable Sdp Id Combo Box, since Number Range Routing is configured.
                i_sdpIdComboBox.setSelectedItem("");
                i_sdpIdComboBox.setEnabled(false);
            }
        }
    }
}