////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiFafChargingIndicatorsData.java
//    DATE            :       26-oct-2005
//    AUTHOR          :       Yang Liu
//    REFERENCE       :       PpacLon#1755/7311
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Wrapper for FachFafChargingIndData class 
//                            within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction.     | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;

/** Wrapper for faf charging indicators class within BOI. */
public class BoiFafChargingIndicatorsData extends DataObject
{
    /** Faf Charging Indicators data object */
    private FachFafChargingIndData i_fachfafciData;

    /**
     * Simple constructor.
     * @param p_fachfafciData faf Charging Indicators data object to wrapper.
     */
    public BoiFafChargingIndicatorsData(FachFafChargingIndData p_fachfafciData)
    {
        i_fachfafciData = p_fachfafciData;
    }

    /**
     * Return Faf Charging Indicators wrappered data object.
     * @return Wrappered Faf Charging Indicators data object.
     */
    public FachFafChargingIndData getInternalFachFafChargingIndData()
    {
        return i_fachfafciData;
    }

    /**
     * Compares two fach faf charging indicators data objects and returns true if they equals.
     * @param p_fachfafciData faf charging Indicators data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_fachfafciData)
    {
        boolean l_return = false;

        if (p_fachfafciData != null
                && p_fachfafciData instanceof BoiFafChargingIndicatorsData
                && i_fachfafciData.getChargingInd().equals(((BoiFafChargingIndicatorsData)p_fachfafciData)
                        .getInternalFachFafChargingIndData().getChargingInd()))
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns faf charging indicators data object as a String for display in BOI.
     * @return Faf Charging Indicators data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_fachfafciData.getChargingInd() + " - "
                + i_fachfafciData.getDesc());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_fachfafciData.getChargingInd().length();
    }
}