////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :     9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :     RegionsPaneUT.java
//      DATE            :     06-10-2005
//      AUTHOR          :     Michael Erskine
//      REFERENCE       :     PpacLon#7204/1709
//
//      COPYRIGHT       :     WM-data 2005
//
//      DESCRIPTION     :     JUnit test class for RegionsPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//    DATE  | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiRegionData;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for RegionsPane. */
public class RegionsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "RegionsPaneUT";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Regions screen. */
    private RegionsPane   i_regiPane;

    /** Region code field. */
    private JFormattedTextField i_codeField;

    /** Region description field. */
    private ValidatedJTextField i_descriptionField;

    /** Key data combo box. */
    protected JComboBox         i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton           i_updateButton;

    /** Delete button. */
    protected JButton           i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public RegionsPaneUT(String p_title)
    {
        super(p_title);

        i_regiPane = new RegionsPane(c_context);
        super.init(i_regiPane);

        i_regiPane.resetScreenUponEntry();

        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_codeField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "codeField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane,
                                                                                "descriptionField");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(RegionsPaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again through
     * the Regions screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("RegionsPane test");

        BoiRegionData l_boiRegiData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_regiPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        doInsert(i_regiPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiRegiData = createRegionData(C_CODE, C_DESCRIPTION);
        SwingTestCaseTT.setKeyComboSelectedItem(i_regiPane, i_keyDataCombo, l_boiRegiData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_regiPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_regiPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_regiPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_CODE);
        doMakeAvailable(i_regiPane, i_updateButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteRegiRecord(C_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteRegiRecord(C_CODE);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /**
     * Creates a BoiAccountGroupsData object with the supplied data.
     * @param p_regionId Account group id.
     * @param p_regionDesc Account group description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiRegionData createRegionData(String p_regionId, String p_regionDesc)
    {
        RegiRegionData l_regiData;
        HomeRegionId l_regiId = null;

        try
        {
            l_regiId = new HomeRegionId(p_regionId);
        }
        catch (PpasParseException e)
        {
            fail("Invalid region id supplied");
        }

        l_regiData = new RegiRegionData(null, l_regiId, p_regionDesc, ' ');

        return new BoiRegionData(l_regiData);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteRegiRecord = "deleteRegiRecord";

    /**
     * Removes a row from REGI.
     * @param p_regionId Region id.
     */
    private void deleteRegiRecord(String p_regionId)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = new String("DELETE from regi_region " + "WHERE regi_region_id = {0}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_regionId);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteRegiRecord);
    }
}