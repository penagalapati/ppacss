////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       FafBarredListDbService.java
//    DATE            :       25-Jan-2005
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#970/5611
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Friends and Family
//                            Barred Numbers screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiFafBarredListData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.FabaFafBarredListSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDate;

/**
 * Database access class for Friends and Family Barred List screen. 
 */
public class FafBarredListDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------
    
    /** Barred list data sql service. */
    private FabaFafBarredListSqlService i_fabaSqlService = null;
    
    /** Operator details sql service. */
    private BicsysUserfileSqlService i_operatorSqlService;    
    
    /** Vector of available markets for display. */
    private Vector i_availableMarketsV = null;
    
    /** Operator's default market. */
    private BoiMarket i_defaultMarket = null;
    
    /** Vector of Friends and Family Barred List records. */
    private Vector i_fafBarredListDataV = null;
    
    /** Set of all Friends and Family Barred List records. */
    private FabaFafBarredListDataSet i_fabaDataSet = null;
    
    /** Friends and Family Barred List data currently being worked on. */
    private BoiFafBarredListData i_fafBarredListData = null;
    
    /** FaF barred number list prefix, used as part of key for read and delete. */
    private String i_numberPrefix;
    
    /** FaF barred number list length, used as part of key for read and delete. */
    private int i_numberLength = -1;
    
    /** FaF barred number list start date, used as part of key for read and delete. */
    private PpasDate i_startDate;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public FafBarredListDbService(BoiContext p_context)
    {
        super(p_context);
        i_fabaSqlService = new FabaFafBarredListSqlService(null, null);
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);        
        i_fafBarredListDataV = new Vector(5); 
        i_availableMarketsV = new Vector(5);
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Return available markets. 
     * @return Vector of available market data records.
     */
    public Vector getAvailableMarketData()
    {
        return i_availableMarketsV;
    }
    
    /** 
     * Return operator's default market. 
     * @return Operator's default market.
     */
    public BoiMarket getOperatorDefaultMarket()
    {
        return i_defaultMarket;
    }
    
    /** 
     * Returns vector of Friends and Family Barred List records. 
     * @return Vector of Friends and Family Barred List records. 
     */
    public Vector getFafBarredListVector()
    {
        return i_fafBarredListDataV;
    }
    
    /** 
     * Returns FaF barred list data currently being worked on. 
     * @return FaF barred list data object.
     */
    public BoiFafBarredListData getFafBarredListData()
    {
        return i_fafBarredListData;
    }
    
    /** 
     * Sets FaF barred list data currently being worked on. 
     * @param p_fafBarredListData FaF barred list data object.
     */
    public void setFafBarredListData(BoiFafBarredListData p_fafBarredListData)
    {
        i_fafBarredListData = p_fafBarredListData;
    }
    
    /** 
     * Sets FaF barred number list prefix used for table lookup. 
     * @param p_numberPrefix FaF barred number list prefix.
     */
    public void setCurrentNumberPrefix(String p_numberPrefix)
    {
        i_numberPrefix = p_numberPrefix;
    }
    
    /** 
     * Sets FaF barred number list length used for table lookup. 
     * @param p_numberLength FaF barred number list length. 
     */
    public void setCurrentNumberLength(int p_numberLength)
    {
        i_numberLength = p_numberLength;
    }
    
    /** 
     * Sets FaF barred number list start date used for table lookup. 
     * @param p_startDate FaF barred number list start date.
     */
    public void setCurrentStartDate(PpasDate p_startDate)
    {
        i_startDate = p_startDate;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
    throws PpasSqlException
    {
        i_fabaDataSet = i_fabaSqlService.readAll(null, p_connection);
        i_fafBarredListData = new BoiFafBarredListData(i_fabaDataSet.getFafBarredListData(
                                                                              i_currentMarket.getMarket(),
                                                                              i_numberPrefix,
                                                                              i_numberLength,
                                                                              i_startDate));
    }
    
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
    throws PpasSqlException
    {
        Market[]        l_availableMarketArray;
        SrvaMstrDataSet l_srvaMstrDataSet;

        if (i_currentMarket == null)
        {
            i_availableMarketsV.removeAllElements();
            
            l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
            l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
            
            for (int i = 0; i < l_availableMarketArray.length; i++)
            {
                i_availableMarketsV.addElement(new BoiMarket(l_availableMarketArray[i]));
            }
            
            i_defaultMarket = new BoiMarket(getDefaultMarket(p_connection));
            
            i_currentMarket = i_defaultMarket;        
        }

        refreshFafBarredListVector(p_connection);
    }
    
    /** 
     * Inserts record into the faba_faf_barred_list table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
    throws PpasSqlException
    {
        FabaFafBarredListData   l_existingFafBarredData = null;
        FabaFafBarredListData   l_newFafBarredData = null;
        PpasDate                l_updatedEndDate;
        FabaFafBarredListData[] l_fabaArray = null;
        boolean                 l_found = false;
        
        l_newFafBarredData = i_fafBarredListData.getInternalFafBarredListData();
        
        l_fabaArray = i_fabaDataSet.getArray(l_newFafBarredData.getMarket());
        
        for(int i = 0; i < l_fabaArray.length && !l_found; i++)
        {
            if (l_fabaArray[i].getNumberPrefix().equals(l_newFafBarredData.getNumberPrefix()) &&
                l_fabaArray[i].getNumberLength() == l_newFafBarredData.getNumberLength() &&
                !l_fabaArray[i].getEndDate().isSet() &&
                l_fabaArray[i].getStartDate().before(l_newFafBarredData.getStartDate()))
            {
                l_existingFafBarredData = l_fabaArray[i];
                l_found = true;
            }
        }
       
        if (l_existingFafBarredData != null)
        {
            // There is an existing earlier record with the same market, number prefix and number length, 
            // and a null end date. We need to set the end date for this record to the day before the start 
            // date of the new record.
            l_updatedEndDate = new PpasDate(l_newFafBarredData.getStartDate());
            l_updatedEndDate.add(PpasDate.C_FIELD_DATE, -1);  
            
            i_fabaSqlService.update(null,
                                    p_connection,
                                    l_existingFafBarredData.getMarket(),
                                    l_existingFafBarredData.getNumberPrefix(),
                                    l_existingFafBarredData.getNumberLength(),
                                    l_existingFafBarredData.getStartDate(),
                                    l_updatedEndDate,
                                    i_context.getOperatorUsername());
        }

        i_fabaSqlService.insert(null,
                                p_connection,
                                l_newFafBarredData.getMarket(),
                                l_newFafBarredData.getNumberPrefix(),
                                l_newFafBarredData.getNumberLength(),
                                l_newFafBarredData.getStartDate(),
                                l_newFafBarredData.getEndDate(),
                                i_context.getOperatorUsername());
        
        refreshFafBarredListVector(p_connection);
    }
    
    /** 
     * Updates record in the faba_faf_barred_list table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
    throws PpasSqlException
    {
        i_fabaSqlService.update(null,
                                p_connection,
                                i_fafBarredListData.getInternalFafBarredListData().getMarket(),
                                i_fafBarredListData.getInternalFafBarredListData().getNumberPrefix(),
                                i_fafBarredListData.getInternalFafBarredListData().getNumberLength(),
                                i_fafBarredListData.getInternalFafBarredListData().getStartDate(),
                                i_fafBarredListData.getInternalFafBarredListData().getEndDate(),
                                i_context.getOperatorUsername());
        
        refreshFafBarredListVector(p_connection);
    }
    
    /** 
     * Deletes record from the faba_faf_barred_list table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
    throws PpasSqlException
    {
        i_fabaSqlService.delete(null,
                                p_connection,
                                i_currentMarket.getMarket(),
                                i_numberPrefix,
                                i_numberLength,
                                i_startDate);
        
        refreshFafBarredListVector(p_connection);
    }
    
    /**
     * Checks whether a record about to be inserted into the database already exists.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
    throws PpasSqlException
    {
        FabaFafBarredListData    l_fafBarredListData = null;
        boolean[]                l_flagsArray = new boolean[2];
        
        i_fabaDataSet = i_fabaSqlService.readAll(null, p_connection);
        l_fafBarredListData = i_fabaDataSet.getFafBarredListData(i_currentMarket.getMarket(),
                                                                 i_numberPrefix,
                                                                 i_numberLength,
                                                                 i_startDate);
        
        if (l_fafBarredListData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        l_flagsArray[C_WITHDRAWN] = false;
        
        return l_flagsArray;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    
    /** 
     * Refreshes the FaF barred numbers vector from the faba_faf_barred_list table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshFafBarredListVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        FabaFafBarredListData[] l_fabaArray = null;
        
        i_fafBarredListDataV.removeAllElements();
        
        i_fabaDataSet = i_fabaSqlService.readAll(null, p_connection);
        l_fabaArray = i_fabaDataSet.getArray(i_currentMarket.getMarket());  
        
        i_fafBarredListDataV.addElement(new String(""));
        i_fafBarredListDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_fabaArray.length; i++)
        {
            i_fafBarredListDataV.addElement(new BoiFafBarredListData(l_fabaArray[i]));
        }
    }

    /**
     * Gets the default <code>Market</code> for the current Operator.
     * @return The default market for the current Operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception  
     */
    private Market getDefaultMarket(JdbcConnection p_connection)
        throws PpasSqlException
    {
        return i_operatorSqlService.getOperatorData(null, p_connection, i_context.getOperatorUsername(),
                                                    null).getDefaultMarket();
    }
}
