////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiMenuPane.java
//    DATE            :       24-Sep-2003
//    AUTHOR          :       Marek Vonka
//    REFERENCE       :       PRD_ASCS00_DEV_SS_088
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Displays BOI menu options available to an   
//                            operator after successful login. 
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | The Bank Codes screen is added  |PpacLon#1755/7279
//          |            | as a Codes Config screen.       |
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | The FaF Cahrging Indicators     |PpacLon#1755/7425
//          |            | screen is added as a User       |
//          |            | Config screen.                  |
//----------+------------+---------------------------------+--------------------
// 17/11/05 | Yang L.    | The SDP Identifiers screen is   |PpacLon#1755/7463
//          |            | added as a User Config screen.  |
//----------+------------+---------------------------------+--------------------
// 18/11/05 | Lars L.    | Review comments corrections.    |PpacLon#1755/7469
//----------+------------+---------------------------------+--------------------
// 18/11/05 | Yang L.    | Review comments corrections.    |PpacLon#1755/7474
//----------+------------+---------------------------------+--------------------
// 15/11/05 | R.Grimshaw | The Adjustment codes screen to  |PpacLon#1847
//          |            | User Config screen.             |
//----------+------------+---------------------------------+--------------------
// 20/11/05 | K Goswami  | The Faf Default Charging Ind    |PpacLon#1838/7448
//          |            | screen is added as a Codes      |
//          |            | Config screen                   |
//----------+------------+---------------------------------+--------------------
// 29/11/05 | M Alm      | The Agent Information  screen   |PpacLon#1755/7500
//          |            | is added as a Codes Config      |
//          |            | screen.                         |
//----------+------------+---------------------------------+--------------------
// 22/03/06 | M Alm      | The Traffic Cases screen        |PpacLon#2035/8106
//          |            | is added as a Codes Config      |
//          |            | screen.                         |
//----------+------------+---------------------------------+--------------------
// 22/03/06  | M Alm     | The Teleservice screen          |PpacLon#2035/8098
//          |            | is added as a Codes Config      |
//          |            | screen.                         |
//----------+------------+---------------------------------+--------------------
// 22/12/06 | M.T�rnqvist| The TUBS Channel screen is added| PpacLon#2822/10659
//          |            | as a Codes Config screen.       |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessData;
import com.slb.sema.ppas.swing.components.MenuItem;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.events.GuiEvent;
import com.slb.sema.ppas.swing.gui.GuiPane;
import com.slb.sema.ppas.swing.gui.FocussedGuiPane;

/**
 * Displays BOI menu options available to an operator after successful login.
 */
public class BoiMenuPane extends GuiPane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Menu tree for BOI panes. */
    private JTree i_menu;
    
    /** Root node in the menu tree. */
    private DefaultMutableTreeNode i_rootNode;
    
    /** Tree data model for the menu tree. */
    private DefaultTreeModel i_treeModel;

    /** Currently selected pane from the menu. */
    private FocussedGuiPane i_currentPane;
    
    /** Path to the currently selected node. */
    private TreePath i_currentSelectionPath;
    
    /** Listener for events on the menu tree. */
    private MenuListener i_menuListener;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor.
     * @param p_context BOI context object.
     */
    public BoiMenuPane(BoiContext p_context)
    {   
        super(p_context, (Container)null);
        init();
    } 

    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------

    /**
     * Sets the currently selected pane in the menu.
     * @param p_currentPane Currently selected pane.
     */
    public void setCurrentPane(FocussedGuiPane p_currentPane)
    {
        i_currentPane = p_currentPane;
    }

    /**
     * Retrieves the currently selected pane in the menu.
     * @return Currently selected pane.
     */
    public FocussedGuiPane getCurrentPane()
    {
        return i_currentPane;
    }

    /**
     * Event handler for GUI events.
     * @param p_event GUI event object.
     */
    public void guiEventOccurred(GuiEvent p_event)
    {
        if("LOGGED_IN".equals(p_event.getName()))
        {
            configureMenuTree((ApacApplicationAccessData)(p_event.getAssociatedObject()));
            i_menu.expandRow(0);
            i_menu.setEnabled(true);
            i_menu.repaint();
        }
        else if("LOGGED_OUT".equals(p_event.getName()))
        {
            i_menu.setEnabled(false);
            i_menu.clearSelection();
            i_rootNode.removeAllChildren();
            i_treeModel.reload(i_rootNode);
            i_menu.repaint();
        }
    }

    /** 
     * Action event handler.
     * @param p_actionEvent Event to handle.
     */
    public void actionPerformed(ActionEvent p_actionEvent){}
    
    /** 
     * Item event handler.
     * @param p_itemEvent Event to handle.
     */
    public void itemStateChanged(ItemEvent p_itemEvent){}
    
    /** 
     * Key event handler.
     * @param p_keyEvent The keyboard event to be handled
     * @return True if the <code>KeyEvent</code> has been dispatched and
     *         false otherwise.
     */
    public boolean dispatchKeyEvent(KeyEvent p_keyEvent)
    {
        return false;
    }
        
    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e){}

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e){} 

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Initialises the JTree used to display the menu options.
     */
    private void init()
    {
        DefaultTreeCellRenderer l_treeCellRenderer;

        i_rootNode = new DefaultMutableTreeNode("Operator Menu");
        
        i_treeModel = new DefaultTreeModel(i_rootNode);
        i_menu = new JTree(i_treeModel);
        i_menuListener = new MenuListener();
        i_menu.addTreeSelectionListener(i_menuListener);
        i_menu.setVerifyInputWhenFocusTarget(false);
        l_treeCellRenderer = new DefaultTreeCellRenderer();
        l_treeCellRenderer.setBackground(WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        l_treeCellRenderer.setBackgroundNonSelectionColor(
                WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        i_menu.setCellRenderer(l_treeCellRenderer);
        
        i_contentPane = i_menu;
        i_contentPane.setBackground(WidgetFactory.C_COLOUR_BLUE_BACKGROUND);
        
        i_menu.setEnabled(false);
    }

    /**
     * Constructs the JTree used to display the menu options.
     * @param p_privileges Operator application access privileges.
     */
    private void configureMenuTree(ApacApplicationAccessData p_privileges)
    {
        DefaultMutableTreeNode  l_node1;
        DefaultMutableTreeNode  l_node2;
        DefaultMutableTreeNode  l_node3;
        MenuItem                l_menuItem;
        
        if(p_privileges.hasBusinessConfigurationApplicationAccess())
        {
            l_node1 = new DefaultMutableTreeNode("Business Config");
            
            l_node2 = new DefaultMutableTreeNode("User Config");
            
            l_menuItem = new MenuItem(i_context,
                                      "Operators",
                                      "com.slb.sema.ppas.boi.gui.OperatorsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Customer Care Access Privileges",
                                      "com.slb.sema.ppas.boi.gui.CustomerCareAccessPrivilegesPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Application Access Privileges",
                                      "com.slb.sema.ppas.boi.gui.ApplicationAccessPrivilegesPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_node1.add(l_node2);
            
            l_node2 = new DefaultMutableTreeNode("Codes Config");
            
            l_menuItem = new MenuItem(i_context,
                                      "Account Groups",
                                      "com.slb.sema.ppas.boi.gui.AccountGroupsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
                       
            l_menuItem = new MenuItem(i_context,
                                      "Accumulated Bonus Schemes",
                                      "com.slb.sema.ppas.boi.gui.AccumulatedBonusPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Accumulators",
                                      "com.slb.sema.ppas.boi.gui.AccumulatorsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Adjustment Codes",
                                      "com.slb.sema.ppas.boi.gui.AdjustmentCodesPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));

            l_menuItem = new MenuItem(i_context,
                                      "Adjustment Types",
                                      "com.slb.sema.ppas.boi.gui.AdjustmentTypePane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));             
            
            
            l_menuItem = new MenuItem(i_context,
                                      "Agent Information",
                                      "com.slb.sema.ppas.boi.gui.AgentInformationPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Banks",
                                      "com.slb.sema.ppas.boi.gui.BanksPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem)); 
            
            l_menuItem = new MenuItem(i_context,
                                      "Bonus Schemes",
                                      "com.slb.sema.ppas.boi.gui.BonusPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Channels",
                                      "com.slb.sema.ppas.boi.gui.TubsChannelPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Community Charging Identities",
                                      "com.slb.sema.ppas.boi.gui.CommunityChargingCodesPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Dedicated Accounts",
                                      "com.slb.sema.ppas.boi.gui.DedicatedAccountsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Family & Friends Barred Numbers",
                                      "com.slb.sema.ppas.boi.gui.FafBarredListPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Family & Friends Charging Indicators",
                                      "com.slb.sema.ppas.boi.gui.FafChargingIndicatorsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));             
            
            l_menuItem = new MenuItem(i_context,
                                      "Family & Friends Default Charging Indicators",
                                      "com.slb.sema.ppas.boi.gui.FafDefaultChargingIndPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Fast Adjustment Codes",
                                      "com.slb.sema.ppas.boi.gui.FastAdjustmentCodesPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));

            l_menuItem = new MenuItem(i_context,
                                      "Markets",
                                      "com.slb.sema.ppas.boi.gui.MarketsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Number Blocks",
                                      "com.slb.sema.ppas.boi.gui.NumberBlocksPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Regions",
                                      "com.slb.sema.ppas.boi.gui.RegionsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "SDP Identifiers",
                                      "com.slb.sema.ppas.boi.gui.SdpIdentifiersPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
                                   
            /* 
             * Commented out since not needed for current release.
             * Replace "<misc> Codes" with screen title when needed.
             *
             *           l_menuItem = new MenuItem(
             *                   (BoiContext)i_context,
             *                   "<misc> Codes",
             *                   "com.slb.sema.ppas.boi.gui.GenericMiscCodesPane");
             *           l_node2.add(new DefaultMutableTreeNode(l_menuItem));
             */
            
            l_menuItem = new MenuItem(i_context,
                                      "Service Class Details",
                                      "com.slb.sema.ppas.boi.gui.ServiceClassDetailsPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
           
            l_menuItem = new MenuItem(i_context,
                                      "Service Offerings",
                                      "com.slb.sema.ppas.boi.gui.ServiceOfferingPane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Teleservice Codes",
                                      "com.slb.sema.ppas.boi.gui.TeleServicePane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Traffic Cases",
                                      "com.slb.sema.ppas.boi.gui.TrafficCasePane");
            l_node2.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_node1.add(l_node2);
            
            l_menuItem = new MenuItem(i_context,
                                      "Cache Reload",
                                      "com.slb.sema.ppas.boi.gui.CacheReloadPane");
            l_node1.add(new DefaultMutableTreeNode(l_menuItem));
            
            i_rootNode.add(l_node1);
        }
        
        if (p_privileges.hasBatchApplicationAccess())
        {
            l_node3 = new DefaultMutableTreeNode("Batch");
            
            l_menuItem = new MenuItem(i_context,
                                      "Batch Control",
                                      "com.slb.sema.ppas.boi.gui.BatchControlPane");
            l_node3.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Msisdn Generation",
                                      "com.slb.sema.ppas.boi.gui.MsisdnGenerationPane");
            l_node3.add(new DefaultMutableTreeNode(l_menuItem));
            
            l_menuItem = new MenuItem(i_context,
                                      "Balance SDPs",
                                      "com.slb.sema.ppas.boi.gui.SdpBalancingBatchFileGenPane");
            l_node3.add(new DefaultMutableTreeNode(l_menuItem));
            
            i_rootNode.add(l_node3);
        }
        
        if (p_privileges.hasPurgeAndArchiveApplicationAccess())
        {
            l_menuItem = new MenuItem(i_context,
                                      "Purge & Archive",
                                      "com.slb.sema.ppas.boi.gui.PurgeAndArchivePane");
            i_rootNode.add(new DefaultMutableTreeNode(l_menuItem));
        }
        
        if (p_privileges.hasReportsApplicationAccess())
        {
            l_menuItem = new MenuItem(i_context,
                                      "Reports",
                                      "com.slb.sema.ppas.boi.gui.ReportsPane");
            i_rootNode.add(new DefaultMutableTreeNode(l_menuItem));
        }
        
        i_treeModel.reload(i_rootNode);
    }
                               
    //-------------------------------------------------------------------------
    // Inner classes
    //-------------------------------------------------------------------------

    /**
     * Listens for events on the menu tree.
     */
    private class MenuListener implements TreeSelectionListener
    {
        /**
         * Handles events on the menu tree.
         * @param p_event Menu selection event.
         */
        public void valueChanged(TreeSelectionEvent p_event)
        {
            DefaultMutableTreeNode l_node;
            Container              l_configParentContainer;
            
            l_node = (DefaultMutableTreeNode)i_menu.getLastSelectedPathComponent();
        
            if (l_node != null)
            {
                if (l_node.isLeaf()) 
                {
                    if(l_node.getUserObject() instanceof MenuItem)
                    {
                        SwingUtilities.invokeLater(new ScreenChangeRunnable());
                    }
                    else
                    {
                        // <<TEMP!!!!>>
                        l_configParentContainer = (Container)i_context.getObject(
                                "ConfigParentContainer");
                        l_configParentContainer.removeAll();
                        l_configParentContainer.repaint();
                    }
                } 
            }
        }
    }

    /**
     * Class to enable selected screen to be focussed and painted after other events
     * have been processed.
     */
    private class FocusRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            i_currentPane.defaultFocus();
            ((Container)i_context.getObject("ConfigParentContainer")).repaint();
        }
    }

    /**
     * Class to enable menu pane event handler to check current screen status after 
     * focus lost event has been processed for current focus owner.
     */
    private class ScreenChangeRunnable implements Runnable
    {
        /**
         * Performs processing to be executed in thread. 
         */
        public void run()
        {
            int                    l_paneState;
            int                    l_response;
            String                 l_confirmMsg;
            Container              l_configParentContainer;
            Object                 l_nodeInfo; 
            DefaultMutableTreeNode l_node;
            boolean                l_confirmChangeScreen = true;
            
            if (i_currentPane != null)
            {
                l_paneState = i_currentPane.getState();
                
                if (l_paneState == GuiPane.C_STATE_NEW_RECORD ||
                    l_paneState == GuiPane.C_STATE_DATA_MODIFIED)
                {
                    if (i_currentPane instanceof BusinessConfigBasePane)
                    {
                        l_confirmMsg = "Unsaved modifications will be lost. " +
                                       "Are you sure you wish to leave the screen?";
                    }
                    else
                    {
                        l_confirmMsg = "The selected job has not been submitted. " +
                                       "Are you sure you wish to leave the screen?";
                    }
                    
                    l_response = displayConfirmDialog(i_currentPane.getContentPane(),
                                                      l_confirmMsg);
                    
                    if (l_response != JOptionPane.YES_OPTION)
                    {
                        l_confirmChangeScreen = false;
                    }
                }
            }
            
            if (l_confirmChangeScreen)
            {
                l_node = (DefaultMutableTreeNode)i_menu.getLastSelectedPathComponent();
                l_nodeInfo = l_node.getUserObject();

                l_configParentContainer = (Container)i_context.getObject("ConfigParentContainer");
                l_configParentContainer.removeAll();
                l_configParentContainer.add(((MenuItem)l_nodeInfo).getMenuItem().getContentPane());
            
                i_currentPane = ((MenuItem)l_nodeInfo).getMenuItem();
                i_currentSelectionPath = i_menu.getSelectionPath();
            
                i_currentPane.resetScreenUponEntry();
                SwingUtilities.invokeLater(new FocusRunnable());
            }
            else
            {
                i_menu.removeTreeSelectionListener(i_menuListener);
                i_menu.setSelectionPath(i_currentSelectionPath);
                i_menu.addTreeSelectionListener(i_menuListener);
            }
        }
    }
}
