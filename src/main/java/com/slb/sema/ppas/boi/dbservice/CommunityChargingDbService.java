////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       CommunityChargingDbService.java
//    DATE            :       19-Jul-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#338/3249
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Community Charging screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CochCommunityChargingSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.boi.boidata.BoiCommunityChargingData;
import com.slb.sema.ppas.boi.gui.BoiContext;

/**
 * Database access class for Community Charging screen. 
 */
public class CommunityChargingDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for community charging code details. */
    private CochCommunityChargingSqlService i_cochSqlService = null;
    
    /** Vector of community charging records. */
    private Vector i_availableChargingDataV = null;
    
    /** Community charging data. */
    private BoiCommunityChargingData i_chargingData = null;

    /** Currently selected community charging code. Used as key for read and delete. */
    private int i_currentCode = -1;
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor
     * @param p_context BOI context.
     */
    public CommunityChargingDbService(BoiContext p_context)
    {
        super(p_context);
        i_cochSqlService = new CochCommunityChargingSqlService(null, null);
        i_availableChargingDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current community charging code. Used for record lookup in database. 
     * @param p_code Currently selected community charging code. 
     */
    public void setCurrentCode(int p_code)
    {
        i_currentCode = p_code;
    }

    /** 
     * Return community charging data currently being worked on. 
     * @return Community charging data object.
     */
    public BoiCommunityChargingData getChargingData()
    {
        return i_chargingData;
    }

    /** 
     * Set community charging data currently being worked on. 
     * @param p_chargingData Community charging data object.
     */
    public void setChargingData(BoiCommunityChargingData p_chargingData)
    {
        i_chargingData = p_chargingData;
    }

    /** 
     * Return community charging data. 
     * @return Vector of available community charging data records.
     */
    public Vector getAvailableChargingData()
    {
        return i_availableChargingDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        CochCommunityChargingDataSet l_cochDataSet = null;
        
        l_cochDataSet = i_cochSqlService.readAll(null, p_connection);
        i_chargingData = new BoiCommunityChargingData(l_cochDataSet.getCommunityChargingData(i_currentCode));
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshChargingDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the coch_community_charging table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_cochSqlService.insert(null,
                                p_connection,
                                i_chargingData.getInternalChargingData().getCommunityChgId(),
                                i_chargingData.getInternalChargingData().getCommunityChgDesc(),
                                i_context.getOperatorUsername());
        
        refreshChargingDataVector(p_connection);
    }

    /** 
     * Updates record in the coch_community_charging table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_cochSqlService.update(null,
                                p_connection,
                                i_chargingData.getInternalChargingData().getCommunityChgId(),
                                i_chargingData.getInternalChargingData().getCommunityChgDesc(),
                                i_context.getOperatorUsername());

        refreshChargingDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the coch_community_charging table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_cochSqlService.delete(null,
                                p_connection,
                                i_currentCode,
                                i_context.getOperatorUsername());

        refreshChargingDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        CochCommunityChargingDataSet l_cochDataSet = null;
        CochCommunityChargingData    l_dbChargingData = null;
        boolean[]                    l_flagsArray = new boolean[2];
        
        l_cochDataSet = i_cochSqlService.readAll(null, p_connection);
        l_dbChargingData = l_cochDataSet.getCommunityChargingData(i_currentCode);
                               
        if (l_dbChargingData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            if (l_dbChargingData.isDeleted())
            {
                l_flagsArray[C_WITHDRAWN] = true;
            }
            else
            {
                l_flagsArray[C_WITHDRAWN] = false;
            }
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }
        
        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the coch_community_charging table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_cochSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_chargingData.getInternalChargingData().getCommunityChgId(),
                                         i_context.getOperatorUsername());

        refreshChargingDataVector(p_connection);
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available charging data vector from the 
     * coch_community_charging table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshChargingDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        CochCommunityChargingDataSet l_cochDataSet = null;
        CochCommunityChargingData[] l_cochArray = null;
        
        i_availableChargingDataV.removeAllElements();
        
        l_cochDataSet = i_cochSqlService.readAll(null, p_connection);
        l_cochArray = l_cochDataSet.getAvailableArray();
        
        i_availableChargingDataV.addElement(new String(""));
        i_availableChargingDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_cochArray.length; i++)
        {
            i_availableChargingDataV.addElement(new BoiCommunityChargingData(l_cochArray[i]));
        }
   }
}