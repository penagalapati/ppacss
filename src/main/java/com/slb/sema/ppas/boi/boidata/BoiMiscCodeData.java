////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiMiscCodeData.java
//      DATE            :       26-Jul-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#338/3340
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for MiscCodeData class within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;

/** Wrapper for Miscellaneous code class within BOI. */
public class BoiMiscCodeData extends DataObject implements Comparable
{
    /** Constant to identify service class records from the SRVA_MISC_CODES table. */
    private static final String C_SERV_CLASS_MISC_TABLE = "CLS";
    
    /** Basic miscellaneous code data object */
    private MiscCodeData i_miscCodeData;
    
    /**
     * Simple constructor.
     * @param p_miscCodeData Miscellaneous code data object to wrapper.
     */
    public BoiMiscCodeData(MiscCodeData p_miscCodeData)
    {
        i_miscCodeData = p_miscCodeData;
    }

    /**
     * Return wrappered miscellaneous code data object.
     * @return Wrappered miscellaneous code data object.
     */
    public MiscCodeData getInternalMiscCodeData()
    {
        return i_miscCodeData;
    }

    /**
     * Compares two miscellaneous code data objects and returns true if they equate.
     * @param p_miscCodeData Miscellaneous code data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_miscCodeData)
    {
        boolean l_return = false;
        
        if ( p_miscCodeData != null &&
             p_miscCodeData instanceof BoiMiscCodeData &&
             i_miscCodeData.getCode().equals(
                 ((BoiMiscCodeData)p_miscCodeData).getInternalMiscCodeData().getCode()) )
        {
            l_return = true;
        }
        return l_return;
    }

    /** 
     * Method used to sort objects of this class.  Currently, this is only used to sort 
     * service class code records. i.e. Objects populated from SRVA_MISC_CODES with a type 
     * of CLS.
     * @param p_otherObject An <code>Object</code> to compare with.
     * @return -1, 0 or 1 depending on whether the comparision object is greater than, 
     * equal to, or less than this object. 
     */
    public int compareTo (Object p_otherObject)
    {
        int             l_retVal = 0;
        Integer         l_thisMiscCode;
        Integer         l_otherMiscCode;
        BoiMiscCodeData l_otherMiscCodeData;
        
        if (p_otherObject == null || !(p_otherObject instanceof BoiMiscCodeData))
        {
            l_retVal = 1;
        }
        else
        {
            l_otherMiscCodeData = (BoiMiscCodeData)p_otherObject;

            if (!l_otherMiscCodeData.getInternalMiscCodeData().getCodeType().equals(C_SERV_CLASS_MISC_TABLE))
            {
                l_retVal = 1;
            }
            else
            {
                l_otherMiscCode = Integer.valueOf(l_otherMiscCodeData.getInternalMiscCodeData().getCode());
                l_thisMiscCode = Integer.valueOf(i_miscCodeData.getCode());
                
                l_retVal = l_thisMiscCode.compareTo(l_otherMiscCode);
            }
        }

        return (l_retVal);
    }
    
    /**
     * Returns miscellaneous code data object as a String for display in BOI.
     * @return Miscellaneous code data object as a string.
     */
    public String toString()
    {
        String l_displayString = new String(i_miscCodeData.getCode() + 
                                            " - " + i_miscCodeData.getDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_miscCodeData.getCode().length();
    }
}