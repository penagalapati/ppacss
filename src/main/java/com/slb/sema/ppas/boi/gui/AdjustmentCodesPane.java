////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       AdjustmentCodesPane.java
//DATE            :       17 November 2005
//AUTHOR          :       R.Grimshaw
//REFERENCE       :       PpacLon#1847
//
//COPYRIGHT       :       WM-data 2005
//
//DESCRIPTION     :       Dedicated Accounts screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 07/02/06 | M Erskine  | Set selected row in table and   | PpacLon#1978/7906
//          |            | adjust viewport accordingly.    |
//----------+------------+---------------------------------+--------------------
// 16/06/06 | Chris      | Adjust fields and table to allow| PpacLon#2064/9334
//          | Harrison   | for maximum data lengths.       |
//----------+------------+---------------------------------+--------------------
// 17/11/06 | Yang Liu   | Tables resized in BOI. Don�t    | PpacLon#2066/10439
//          |            | display vertical scrollbar      |
//          |            | unnecessarily.                  |
//----------+------------+---------------------------------+--------------------
// 06/12/06 | Yang Liu   | Disable the input fields when   | PpacLon#2086/10520
//          |            | not selecting  NEW RECORD.      |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiAdjCodesData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.dbservice.AdjustmentCodesDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;
import com.slb.sema.ppas.util.support.Debug;

/**
* Adjustments Codes screen.
*/
public class AdjustmentCodesPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AdjustmentCodesPane";
    
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Available markets vector. */
    private Vector               i_markets;

    /** Panel allowing data selection and modification. */
    private JPanel               i_detailsPanel          = null;

    /** Panel containing table of existing records. */
    private JPanel               i_definedCodesPanel     = null;

    /** Existing Adjustment codes vector. */
    private Vector               i_definedCodes          = null;

    /** Existing service classes vector. */
    private Vector               i_adjustmentTypes       = null;

    /** Adjustment Codes code field. */
    private ValidatedJTextField  i_codeField             = null;
    
    /** DedicatedAccounts description field. */
    private ValidatedJTextField  i_descriptionField      = null;

    /** Table containing existing records. */
    private JTable               i_table                 = null;

    /** Column names for table of existing records. */
    private String[]             i_columnNames           = {"Code", "Description"};

    /** Data array for table of existing records. */
    private String               i_data[][]              = null;

    /** Data model for table of existing records. */
    private StringTableModel     i_stringTableModel      = null;

    /** ComboBox model to hold the array of service classes for the selected market. */
    private DefaultComboBoxModel i_adjustmentTypeDataModel = null;
    
    /** 
     * Available service class combo box used to filter records for the key data combo.
     * Only applicable for a couple of screens, so created in subclasses.
     */
    protected JComboBox i_adjustmentTypeDataComboBox = null;
    
    /** The vertical scroll bar used for the table view port. */
    private JScrollBar           i_verticalScrollBar = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * DedicatedAccountsPane constructor.
     * @param p_context A reference to the BoiContext
     */
    public AdjustmentCodesPane(Context p_context)
    {
        super(p_context, (Container)null);

        i_dbService = new AdjustmentCodesDbService((BoiContext)i_context);

        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Sets the component that is to have the focus on initialising this screen.
     */
    public void defaultFocus()
    {
        if (!i_marketDataComboBox.requestFocusInWindow())
        {
            System.out.println("Unable to set default focus");
        }
    }

    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if (l_selectedRowIndex != -1 && (i_definedCodes.size() > (l_selectedRowIndex + 2)))
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Adjustment Codes", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(BoiHelpTopics
                .getHelpTopics(BoiHelpTopics.C_ADJUSTMENT_CODES_SCREEN), i_helpComboListener);
        i_helpComboBox.setFocusable(false);

        createDetailsPanel();
        createDefinedCodesPanel();

        i_mainPanel.add(i_helpComboBox, "adjustmentCodesHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel, "detailsPanel,1,6,100,33");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,40,100,60");
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Description cannot be blank");
            return false;
        }
        return true;
    }

    /**
     * Validates key screen data and writes it to screen data object. This method is called before checking
     * for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        String l_adjCode = null;
        Object l_adjType = null;

        l_adjType = i_adjustmentTypeDataComboBox.getSelectedItem();
        if (l_adjType == null)
        {
            i_adjustmentTypeDataComboBox.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Valid market/adjustment type combination must be selected.");
            return false;
        }

        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        l_adjCode = i_codeField.getText();
        ((AdjustmentCodesDbService)i_dbService).setCurrentAdjustmentCode(l_adjCode);

        return true;
    }

    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */
    protected void writeCurrentRecord()
    {
        SrvaAdjCodesData l_adjCodesData;
        BoiMarket l_market = null;
        String l_adjsutmentType = null;
        String l_currentCode;
        BoiMiscCodeData l_miscCodeDataObject = null;
        
        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10030,
                        "Entering writeCurrentRecord");
        }

        l_market = (BoiMarket)i_marketDataComboBox.getSelectedItem();
        l_currentCode = i_codeField.getText();

        l_miscCodeDataObject = (BoiMiscCodeData)i_adjustmentTypeDataComboBox.getSelectedItem();
        l_adjsutmentType = l_miscCodeDataObject.getInternalMiscCodeData().getCode();
        
       // ((AdjustmentCodesDbService)i_dbService).setCurrentServiceClass(l_miscCodeDataObject);
        
        l_adjCodesData = new SrvaAdjCodesData(
                            null,
                            null,
                            l_market.getMarket(),
                            l_adjsutmentType,
                            l_currentCode,
                            i_descriptionField.getText(),
                            null, // datetime
                            "", //opdi
                            ' ' // delfalg
                            );


        ((AdjustmentCodesDbService)i_dbService).setAdjustmentCodesData(new BoiAdjCodesData(
            l_adjCodesData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */
    protected void writeKeyData()
    {
        BoiAdjCodesData l_selectedItem = null;
        String l_code = null;

        if(Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10030,
                        "Entering writeKeyData()");
        }
        
        if (i_keyDataComboBox.getSelectedItem() instanceof BoiAdjCodesData)
        {
            l_selectedItem = (BoiAdjCodesData)i_keyDataComboBox.getSelectedItem();
            l_code = l_selectedItem.getInternalAdjCodesData().getAdjCode();
            ((AdjustmentCodesDbService)i_dbService).setCurrentAdjustmentCode(l_code);
        }
        else
        {
            ((AdjustmentCodesDbService)i_dbService).setCurrentAdjustmentCode(null);
        }
    }

    /**
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        if(Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10030,
                        "Entering initialiseScreen...");
        }
        
        i_codeField.setText("");
        i_descriptionField.setText("");
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(false);
        i_verticalScrollBar.setValue(0);
        
        if(Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10030,
                        "Leaving initialiseScreen...");
        }
        
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        SrvaAdjCodesData l_adjustmentCodesData;

        l_adjustmentCodesData = ((AdjustmentCodesDbService)i_dbService).getAdjustmentCodesData()
                .getInternalAdjCodesData();
        
        i_codeField.setText(l_adjustmentCodesData.getAdjCode());
        i_descriptionField.setText(l_adjustmentCodesData.getAdjDesc());
        selectRowInTable(i_table, i_verticalScrollBar);
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables. Used to refresh data after inserts, deletes, and
     * updates, and when different market selected.
     */
    protected void refreshListData()
    {
        i_definedCodes = ((AdjustmentCodesDbService)i_dbService).getAvailableAdjustmentCodesData();
        populateCodesTable();

        i_keyDataModel = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
    }

    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {
        i_markets = ((AdjustmentCodesDbService)i_dbService).getAvailableMarketData();
        i_marketDataModel = new DefaultComboBoxModel(i_markets);
        i_marketDataComboBox.setModel(i_marketDataModel);
        i_marketDataComboBox.removeItemListener(i_dataFilterComboListener);
        i_marketDataComboBox.setSelectedItem(((AdjustmentCodesDbService)i_dbService)
                .getOperatorDefaultMarket());
        i_marketDataComboBox.addItemListener(i_dataFilterComboListener);

        i_adjustmentTypes = ((AdjustmentCodesDbService)i_dbService).getAvailableAdjustmentTypes();
        i_adjustmentTypeDataModel = new DefaultComboBoxModel(i_adjustmentTypes);
        i_adjustmentTypeDataComboBox.setModel(i_adjustmentTypeDataModel);

        refreshListData();
    }

    /**
     * Resets the selected value for the combo box holding the key data. Typically called when user does not
     * confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((AdjustmentCodesDbService)i_dbService)
                .getAdjustmentCodesData());
    }

    /**
     * Refreshes list data after a new market has been selected from the market combo box.
     */
    protected void refreshDataAfterMarketSelect()
    {
        if (Debug.on)
        {
            Debug.print(C_CLASS_NAME,
                        10030,
                        "Entering refreshDataAfterMarketSelect");
        }
        
        i_adjustmentTypes = ((AdjustmentCodesDbService)i_dbService).getAvailableAdjustmentTypes();
        i_adjustmentTypeDataModel = new DefaultComboBoxModel(i_adjustmentTypes);
        i_adjustmentTypeDataComboBox.setModel(i_adjustmentTypeDataModel);

        refreshListData();
    }

    /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
    }

    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
        i_descriptionField.setEnabled(true);
    }
    
    /**
     * Handles refreshing the Adjustment Types combo-box for this screen.
     * @param p_itemEvent The <code>ItemEvent</code> associated with the call to this method
     */
    protected void refreshAfterSelection(ItemEvent p_itemEvent)
    {
        ((AdjustmentCodesDbService)i_dbService).setCurrentAdjustmentType(
                             (BoiMiscCodeData)i_adjustmentTypeDataComboBox.getSelectedItem());
        try
        {
            i_dbService.readInitialData();
        }
        catch (PpasServiceFailedException l_pSE)
        {
            handleException(l_pSE);
        }
        refreshListData();
    }

    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Creates the panel containing the defined codes and the required buttons.
     */
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_marketLabel = null;
        JLabel l_adjustmentTypeLabel = null;
        JLabel l_codeLabel = null;
        JLabel l_descriptionLabel = null;

        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 33, 0, 0);

        l_marketLabel = WidgetFactory.createLabel("Market:")    ;

        l_adjustmentTypeLabel = WidgetFactory.createLabel("Adjustment Type:");
        i_adjustmentTypes = ((AdjustmentCodesDbService)i_dbService).getAvailableAdjustmentTypes();
        i_adjustmentTypeDataModel = new DefaultComboBoxModel(i_adjustmentTypes);
        i_adjustmentTypeDataComboBox = WidgetFactory.createComboBox(i_adjustmentTypeDataModel);
        i_adjustmentTypeDataComboBox.addItemListener(i_dataFilterComboListener);

        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");

        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createTextField(10);
        addValueChangedListener(i_codeField);

        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(40);
        addValueChangedListener(i_descriptionField);

        i_detailsPanel.add(l_marketLabel, "marketLabel,1,1,15,4");
        i_detailsPanel.add(i_marketDataComboBox, "marketDataComboBox,17,1,35,4");

        i_detailsPanel.add(l_adjustmentTypeLabel, "adjustmentTypesLabel,1,6,15,4");
        i_detailsPanel.add(i_adjustmentTypeDataComboBox, "adjustmentTypeDataComboBox,17,6,80,4");

        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,11,15,4");
        i_detailsPanel.add(i_keyDataComboBox, "definedCodesBox,17,11,70,4");

        i_detailsPanel.add(l_codeLabel, "codeLabel,1,16,15,4");

        i_detailsPanel.add(i_codeField, "codeField,17,16,18,4");

        i_detailsPanel.add(l_descriptionLabel, "descriptionLabel,1,21,15,4");

        i_detailsPanel.add(i_descriptionField, "descriptionField,17,21,65,4");

        i_detailsPanel.add(i_updateButton, "updateButton,1,27,15,5");
        i_detailsPanel.add(i_deleteButton, "deleteButton,17,27,15,5");
        i_detailsPanel.add(i_resetButton, "resetButton,33,27,15,5");
    }

    /**
     * Creates the panel containing the defined codes displayed in a table.
     */
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        int[]       l_columnWidths = new int[] { 250, 544 };
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 60, 0, 0);

        i_stringTableModel = new StringTableModel(i_data, i_columnNames);
        i_table = WidgetFactory.createTable(l_columnWidths, 150, 200, i_stringTableModel, this);

        l_scrollPane = new JScrollPane(i_table,
                                       JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        i_definedCodesPanel.add(l_scrollPane, "i_table,1,1,100,60");
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }

    /**
     * Populates the table in the defined codes panel.
     */
    private void populateCodesTable()
    {
        SrvaAdjCodesData l_adjustCodesData = null;
        int l_numRecords = 0;
        int l_arraySize = 0;

        l_numRecords = i_definedCodes.size() -2;
        l_arraySize = ((l_numRecords > 20) ? l_numRecords : 21);
        i_data = new String[l_arraySize][2];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i = 2, j = 0; i < i_definedCodes.size(); i++, j++)
        {
            l_adjustCodesData = ((BoiAdjCodesData)i_definedCodes.elementAt(i))
                    .getInternalAdjCodesData();
            i_data[j][0] = l_adjustCodesData.getAdjCode();
            i_data[j][1] = l_adjustCodesData.getAdjDesc();
        }

        i_stringTableModel.setData(i_data);
    }
}