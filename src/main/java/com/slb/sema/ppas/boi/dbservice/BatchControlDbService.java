////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchControlDbService.java
//    DATE            :       21-Jul-2004
//    AUTHOR          :       Michael Erskine
//    REFERENCE       :       PpacLon#112/3274
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Database access class for Batch Control screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/7/5   | M.Brister  | Added repeat scheduling         | PpacLon#1398/6788
//          |            | parameters to submitJob.        | 
//----------+------------+---------------------------------+--------------------
// 07/08/06 | M. Erskine | Display the most recently       | PpacLon#2479/9483
//          |            | executed jobs at the top of the |
//          |            | table on Batch Control screen.  |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiMiscCodeData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.BacoBatchControlData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BacoBatchControlDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BacoBatchControlSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDateTime;

/**
 *  Database access class for the Batch Control screen.
 */
public class BatchControlDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Class Constants
    //-------------------------------------------------------------------------

    /** Position of file date in table data array. */
    public static final int C_COLUMN_EXECUTION_DATETIME = 0;
    
    /** Position of file date in table data array. */
    public static final int C_COLUMN_FILE_DATE   = 1;

    /** Position of sequence number in table data array. */
    public static final int C_COLUMN_SEQ_NUM     = 2;
    
    /** Position of subsequence number in table data array. */
    public static final int C_COLUMN_SUB_SEQ_NUM = 3;
    
    /** Position of Job Status in table data array. */
    public static final int C_COLUMN_STATUS      = 4;
    
    /** Position of Number of successful records in table data array. */
    public static final int C_COLUMN_NUM_SUCCESS = 5;
    
    /** Position of Number of rejected records in table data array. */
    public static final int C_COLUMN_NUM_REJECT  = 6;
    
    /** Position of Market in table data array. */
    public static final int C_COLUMN_MARKET      = 7;
    
    /** Position of Service Class in table data array. */
    public static final int C_COLUMN_SERVICE_CLASS = 8;
    
    /** Position of Run Date in table data array. */
    public static final int C_COLUMN_RUN_DATE    = 9;
    
    
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Initial number of rows in the table. */
    private static final int C_INITIAL_NUMBER_OF_TABLE_ROWS = 12;
    
    /** Number of columns in the table. */
    private static final int C_NUMBER_OF_TABLE_COLUMNS = 10;

    /** Batch control SQL Service. */
    private BacoBatchControlSqlService i_batchControlSqlService = null;

    /** Misc Codes SQL Service. */
    private MiscCodeSqlService i_miscCodeSqlService = null;

    /** Batch control data set. */
    private BacoBatchControlDataSet i_batchControlDataSet = null;
    
    /** Misc Codes data set. */
    private MiscCodeDataSet i_miscCodeDataSet = null;
    
    /** Data for batch control history table. */
    private String[][] i_tableData = null;

    /** List of available markets for display. */
    private Vector i_availableMarkets = null;
    
    /** List of available markets for display. */
    private Vector i_availableServiceClasses = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Standard constructor.
     * @param p_context Session related information.
     */
    public BatchControlDbService(BoiContext p_context)
    {
        super(p_context);
        i_availableMarkets = new Vector(5);
        i_availableServiceClasses = new Vector(5);
        i_batchControlSqlService = new BacoBatchControlSqlService(null);
        i_miscCodeSqlService = new MiscCodeSqlService(null, null);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** Return list of available markets.
     * @return A <code>Vector</code> of markets that can be used.
     */
    public Vector getAvailableMarkets()
    {       
        return i_availableMarkets;
    }
    
    /** Return list of available service classes.
     * @param p_market Market associated with the service classes.
     * @return A <code>Vector</code> of service classes that can be used for the specified market..
     */
    public Vector getAvailableServiceClasses(Market p_market)
    {
        MiscCodeDataSet   l_availableMiscCodeDataSet = null;
        Vector            l_miscCodeV;
        MiscCodeData      l_miscCodeData;
        BoiMiscCodeData[] l_boiMiscCodeArray;

        l_availableMiscCodeDataSet = i_miscCodeDataSet.getRawAvailableMiscCodeData(
                                        p_market,
                                        "CLS");
        
        i_availableServiceClasses.removeAllElements();

        l_miscCodeV = l_availableMiscCodeDataSet.getDataV();
        l_boiMiscCodeArray = new BoiMiscCodeData[l_miscCodeV.size()];
        for (int i=0; i < l_miscCodeV.size(); i++)
        {
            l_miscCodeData = (MiscCodeData)l_miscCodeV.get(i);
            l_boiMiscCodeArray[i] = new BoiMiscCodeData(l_miscCodeData);
        }
        
        // Sort misc codes into numerical order of service class.
        Arrays.sort(l_boiMiscCodeArray);
        
        for (int j=0; j < l_boiMiscCodeArray.length; j++)
        {
            i_availableServiceClasses.addElement(l_boiMiscCodeArray[j]);
        }
        i_availableServiceClasses.addElement("");
//        i_availableServiceClasses.addElement("N/A");
        
        return i_availableServiceClasses;
    }
    
    /**
     * Retrieves the data used to populate the batch control history table.
     * @param p_jobType Batch job type.
     * @param p_allJobs True if data for all jobs is to be returned, false for in-progress jobs only.
     * @return 2-D array of batch control table data.
     */
    public String[][] getTableData(String p_jobType, boolean p_allJobs)
    {
        BacoBatchControlDataSet l_batchControlDataSubSet = null;
        BacoBatchControlData    l_batchControlData = null;
        int                     l_numberOfFilledRows = 0;
        int                     l_numberOfRows = 0; // Includes empty rows
        int                     l_row = 0;
        
        if (p_allJobs)
        {
            l_batchControlDataSubSet = i_batchControlDataSet.getJobInfo(null, p_jobType);
        }
        else
        {  
            l_batchControlDataSubSet = i_batchControlDataSet.getJobInfo(null, p_jobType, 'I');
        }
            
        l_numberOfFilledRows = l_batchControlDataSubSet.getDataV().size();
        l_numberOfRows =
            (l_numberOfFilledRows < C_INITIAL_NUMBER_OF_TABLE_ROWS) ? 
                C_INITIAL_NUMBER_OF_TABLE_ROWS : l_numberOfFilledRows;
             
        i_tableData = new String[l_numberOfRows][C_NUMBER_OF_TABLE_COLUMNS];
            
        for (l_row = 0; l_row < l_numberOfFilledRows; l_row++)
        {
            l_batchControlData = (BacoBatchControlData)l_batchControlDataSubSet.getDataV().elementAt(l_row);
            Arrays.fill(i_tableData[l_row], "");
            i_tableData[l_row][C_COLUMN_EXECUTION_DATETIME] =
                l_batchControlData.getJobExecutionDateTime().toString_ddMMMyyyy_HHmmss();
            
            if (p_jobType.equals(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING) ||
                p_jobType.equals(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_DELETION))
            {
                i_tableData[l_row][C_COLUMN_FILE_DATE] = l_batchControlData.getFileDate().toString();
            }
            else
            {
                i_tableData[l_row][C_COLUMN_FILE_DATE] = l_batchControlData.getFileDate().getPpasDate().toString();
            }
            if (l_batchControlData.getSequenceNumber() != null)
            {
                i_tableData[l_row][C_COLUMN_SEQ_NUM] = l_batchControlData.getSequenceNumber().toString();
            }
            if (l_batchControlData.getSubSequenceNumber() != null)
            {
                i_tableData[l_row][C_COLUMN_SUB_SEQ_NUM] = l_batchControlData.getSubSequenceNumber().toString();
            }
            i_tableData[l_row][C_COLUMN_STATUS] = String.valueOf(l_batchControlData.getStatus());
            if (l_batchControlData.getSuccess() != null)
            {
                i_tableData[l_row][C_COLUMN_NUM_SUCCESS] = String.valueOf(l_batchControlData.getSuccess());
            }
            if (l_batchControlData.getFailure() != null)
            {
                i_tableData[l_row][C_COLUMN_NUM_REJECT] = String.valueOf(l_batchControlData.getFailure());
            }
            if (p_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION))
            {
                if (l_batchControlData.getExtraData1() != null)
                {
                    i_tableData[l_row][C_COLUMN_SERVICE_CLASS] = l_batchControlData.getExtraData1();
                }
                 
                Market l_market = null;
                boolean l_found = false;
                    
                if (l_batchControlData.getExtraData2() != null && 
                    !l_batchControlData.getExtraData2().equals("") &&
                    l_batchControlData.getExtraData3() != null && 
                    !l_batchControlData.getExtraData3().equals(""))
                {
                    l_market = new Market(l_batchControlData.getExtraData2(), l_batchControlData.getExtraData3());
                   
                    // If this market is one of the available markets then we can get the display name
                    for (int j = 0; j < i_availableMarkets.size() && !l_found ; j++)
                    {
                        if (((BoiMarket)(i_availableMarkets.elementAt(j))).getMarket().equals(l_market))
                        {
                            l_market = ((BoiMarket)(i_availableMarkets.elementAt(j))).getMarket();
                            l_found = true;
                        }   
                    }
                    
                    if (l_found && (l_market.getDisplayName() != null))
                    {
                        i_tableData[l_row][C_COLUMN_MARKET] = l_market.getDisplayName();
                    }
                }
            }
            else if (p_jobType.equals(BatchConstants.C_JOB_TYPE_BATCH_DISCONNECTION) && 
                     l_batchControlData.getExtraData1() != null)
            {
                i_tableData[l_row][C_COLUMN_RUN_DATE] = l_batchControlData.getExtraData1();
            }
        }
            
        if (l_row < C_INITIAL_NUMBER_OF_TABLE_ROWS)
        {
            for (int k = l_row; k < C_INITIAL_NUMBER_OF_TABLE_ROWS; k++)
            {
                for (int l = C_COLUMN_EXECUTION_DATETIME; l <= C_COLUMN_RUN_DATE; l++)
                {
                    i_tableData[k][l] = "";
                }
            }
        }
        
        //TODO: Convert this to a debug statement.
        //for (int i = 0; i < i_tableData.length; i++)
        //{
        //    for (int j = 0; j < i_tableData[i].length; j++)
        //    {
        //        System.out.println("MIE i_tableData[" + i + "][" + j + "]: " + i_tableData[i][j]);
        //    }
        //}
        
        return i_tableData;
    }
    
    /**
     * Submits the job to the job scheduler.
     * @param p_jobType Type of job.
     * @param p_runDateTime Time at which the job is to be run.
     * @param p_jobParams Job parameter hashmap.
     * @param p_scheduleImmediate True if job is to be scheduled immediately.
     * @param p_frequency Frequency of job submission.
     * @param p_interval Interval of job submission.  eg. If frequency = DAILY and
     *                   interval = 2, job will run once every 2 days.
     * @throws PpasServiceFailedException Requested service failed exception.
     */
    public void submitJob(String       p_jobType,
                          PpasDateTime p_runDateTime,
                          HashMap      p_jobParams,
                          boolean      p_scheduleImmediate,
                          String       p_frequency,
                          String       p_interval)
        throws PpasServiceFailedException
    {
        super.submitJob(p_jobType, 
                        p_runDateTime, 
                        p_jobParams, 
                        p_scheduleImmediate,
                        p_frequency,
                        p_interval.equals("") ? 0 : Integer.parseInt(p_interval));
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException If an SQL error occurs when reading the data.
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_batchControlDataSet = i_batchControlSqlService.readAll(null, p_connection);
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException If an SQL error occurs when reading the data.
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        Market[] l_availableMarketArray = null;
        SrvaMstrDataSet       l_srvaMstrDataSet = null;
        
        l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
        l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
        i_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(null, p_connection);
        
        i_availableMarkets.removeAllElements();
        
        for (int i = 0; i < l_availableMarketArray.length; i++)
        {
            i_availableMarkets.addElement(new BoiMarket(l_availableMarketArray[i]));
        }
        
        i_availableMarkets.addElement("");
        
//        i_availableServiceClasses = i_miscCodeDataSet.getAvailableMiscCodeData(
//                                        null,
//                                        ((BoiMarket)i_availableMarkets.elementAt(0)).getMarket(),
//                                        "CLS");
    }
    
    /** 
     * Insert method is not needed.
     * @param p_connection Database connection.
     * @throws PpasSqlException If an SQL error occurs when reading the data.
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not needed in this class.
    }

    /** 
     * Update method is not needed.
     * @param p_connection Database connection.
     * @throws PpasSqlException If an SQL error occurs when reading the data.
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not needed in this class.
    }
    
    /**
     * Delete method is not needed.
     * @param p_connection Database connection.
     * @throws PpasSqlException If an SQL error occurs when reading the data.
     */ 
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        // Not needed in this class.
    }

    /** Implement inherited abstract method.
    * @param p_connection Database connection.
    * @return Array of booleans.
    * @throws PpasSqlException If an SQL error occurs when reading the data.
    */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection) throws PpasSqlException
    {
        return null;
    }
}
