////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiSdpIdData.java
//    DATE            :       21-Sep-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#470/4216
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Wrapper for ScpiScpInfoData class within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE    | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
// 17/02/06| M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;

/** Wrapper for SDP id class within BOI. */
public class BoiSdpIdData extends DataObject
{
    /** Basic SDP id data object */
    private ScpiScpInfoData i_sdpData;

    /**
    * Simple constructor.
    * @param p_sdpData SDP id data object to wrapper.
    */
    public BoiSdpIdData(ScpiScpInfoData p_sdpData)
    {
        i_sdpData = p_sdpData;
    }

    /**
    * Return wrappered SDP id data object.
    * @return Wrappered SDP id data object.
    */
    public ScpiScpInfoData getInternalSdpIdData()
    {
        return i_sdpData;
    }

    /**
    * Compares two SDP id data objects and returns true if they equate.
    * @param p_sdpData SDP id data object to compare the current instance with. 
    * @return True if both instances are equal
    */
    public boolean equals(Object p_sdpData)
    {
        boolean l_return = false;
       
        if ( p_sdpData != null &&
        p_sdpData instanceof BoiSdpIdData &&
             i_sdpData.getScpId().equals(
                 ((BoiSdpIdData)p_sdpData).getInternalSdpIdData().getScpId()))
        {
            l_return = true;
        }
        return l_return;
    }

    /**
    * Returns SDP id data object as a String for display in BOI.
    * @return SDP id data object as a string.
    */
    public String toString()
    {
        return i_sdpData.getScpId() + " - " + i_sdpData.getDescription();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_sdpData.getScpId().length();
    }
}