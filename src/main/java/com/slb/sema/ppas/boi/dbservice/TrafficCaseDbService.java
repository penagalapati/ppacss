////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       TrafficCaseDbService.java
//    DATE            :       10-Mar-2006
//    AUTHOR          :       Mikael Alm
//    REFERENCE       :       PpacLon#2035/8106
//                            PRD_ASCS00_GEN_CA66
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Traffic Case screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiTrafficCaseData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseData;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.TrafTrafficCaseSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;

/**
 * Database access class for Traffic Case screen. 
 */
public class TrafficCaseDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for traffic case code details. */
    private TrafTrafficCaseSqlService i_trafSqlService = null;
    
    /** Vector of traffic case records. */
    private Vector i_availableTrafficCaseDataV = null;
    
    /** Traffic case data. */
    private BoiTrafficCaseData i_trafData = null;

    /** Currently selected traffic case code. Used as key for read and delete. */
    private int i_currentCode = -1;       
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_context BOI context.
     */
    public TrafficCaseDbService(BoiContext p_context)
    {
        super(p_context);
        i_trafSqlService = new TrafTrafficCaseSqlService(null, null, null);
        i_availableTrafficCaseDataV = new Vector(5); 
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Set current traffic case code. Used for record lookup in database. 
     * @param p_code Currently selected traffic case code. 
     */
    public void setCurrentCode(int p_code)   
    {
        i_currentCode = p_code;
    }

    /** 
     * Return traffic case data currently being worked on. 
     * @return Traffic case data object.
     */
    public BoiTrafficCaseData getTrafficCaseData()
    {
        return i_trafData;
    }

    /** 
     * Set traffic case data currently being worked on. 
     * @param p_trafficCaseData Traffic case data object.
     */
    public void setTrafficCaseData(BoiTrafficCaseData p_trafficCaseData)
    {
        i_trafData = p_trafficCaseData;
    }

    /** 
     * Return Traffic case data. 
     * @return Vector of available traffic case data records.
     */
    public Vector getAvailableTrafficCaseData()
    {
        return i_availableTrafficCaseDataV;
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TrafTrafficCaseDataSet l_trafDataSet = null;
        
        l_trafDataSet = i_trafSqlService.readAll(null, p_connection);

        i_trafData = new BoiTrafficCaseData(l_trafDataSet.getRecord(i_currentCode));
             
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {
        refreshTrafficCaseDataVector(p_connection);
    }
    
    /** 
     * Inserts record into the traf_traffic_case table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_trafSqlService.insert(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_trafData.getInternalTrafficCaseData().getTrafficCase(),
                                i_trafData.getInternalTrafficCaseData().getTrafficDescription(),
                                i_trafData.getInternalTrafficCaseData().getTrafficShortDescription());        
        
        refreshTrafficCaseDataVector(p_connection);
    }

    /** 
     * Updates record in the traf_traffic_case table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_trafSqlService.update(null,
                                p_connection,
                                i_context.getOperatorUsername(),                                
                                i_trafData.getInternalTrafficCaseData().getTrafficCase(),
                                i_trafData.getInternalTrafficCaseData().getTrafficDescription(),
                                i_trafData.getInternalTrafficCaseData().getTrafficShortDescription());

        refreshTrafficCaseDataVector(p_connection);
    }
    
    /** 
     * Deletes record from the traf_traffic_case table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_trafSqlService.delete(null,
                                p_connection,
                                i_context.getOperatorUsername(),
                                i_trafData.getInternalTrafficCaseData().getTrafficCase());
              
        refreshTrafficCaseDataVector(p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists,
     * and if so, whether it was previously withdrawn.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TrafTrafficCaseDataSet l_trafDataSet        = null;
        TrafTrafficCaseData    l_dbTrafData = null;
        boolean[]               l_flagsArray         = new boolean[2];
        
        l_trafDataSet        = i_trafSqlService.readAll(null, p_connection);

        l_dbTrafData = l_trafDataSet.getRecord(i_currentCode);
            
        if (l_dbTrafData != null)
        {
            l_flagsArray[C_DUPLICATE] = true;
            l_flagsArray[C_WITHDRAWN] = l_dbTrafData.isDeleted();
        }
        else
        {
            l_flagsArray[C_DUPLICATE] = false;
        }

        return l_flagsArray;
    }
    
    /** 
     * Marks a previously withdrawn record as available in the traf_traffic_case table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void markAsAvailable(JdbcConnection p_connection)
        throws PpasSqlException
    {

        i_trafSqlService.markAsAvailable(null,
                                         p_connection,
                                         i_currentCode,
                                         i_context.getOperatorUsername());

        refreshTrafficCaseDataVector(p_connection);


    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /** 
     * Refreshes the available traffic case data vector from the 
     * traf_traffic_case table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshTrafficCaseDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        TrafTrafficCaseDataSet l_trafDataSet = null;
        TrafTrafficCaseData[]  l_trafArray   = null;
        
        i_availableTrafficCaseDataV.removeAllElements();
        
        l_trafDataSet = i_trafSqlService.readAll(null, p_connection);
        l_trafArray   = l_trafDataSet.getAvailableArray();
        
        i_availableTrafficCaseDataV.addElement(new String(""));
        i_availableTrafficCaseDataV.addElement(new String("NEW RECORD"));
        for (int i=0; i < l_trafArray.length; i++)
        {
            i_availableTrafficCaseDataV.addElement(new BoiTrafficCaseData(l_trafArray[i]));
        }
    }

}