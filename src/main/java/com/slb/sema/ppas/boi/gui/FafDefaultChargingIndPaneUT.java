////////////////////////////////////////////////////////////////////////////////
//   ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//   FILE NAME       :       FafDefaultChargingIndPaneUT.java
//   DATE            :       23-Nov-2005
//   AUTHOR          :       Kanta Goswami
//   REFERENCE       :       PpacLon#1838/7448
//
//   COPYRIGHT       :       WM-data 2005
//
//   DESCRIPTION     :       JUnit test class for FafDefaultChargingIndPane.
//
////////////////////////////////////////////////////////////////////////////////
//   CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//   DATE     | NAME       | DESCRIPTION                     | REFERENCE
//------------+------------+---------------------------------+--------------------
// 17/02/06   | K Goswami  | Added setUp() method            | PpacLon#1951/7968
//------------+------------+---------------------------------+--------------------
// 23/02/06   | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiFafChargingIndicatorsData;
import com.slb.sema.ppas.boi.boidata.BoiFafDefaultChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for FafChargingIndicatorsPane. */
public class FafDefaultChargingIndPaneUT extends BoiTestCaseTT
{

  //------------------------------------------------------------------------
  // Constants
  //------------------------------------------------------------------------

  /** Class name used in calls to middleware. */
    private static final String                       C_CLASS_NAME         = "FafDefaultChargindIndPaneUT";

    /** Constant used for MSISDN start number. */
    private static final String                       C_START_NO           = "01000";

    /** Constant used for MSISDN end number. */
    private static final String                       C_END_NO             = "01100";

    /** Constant used for MSISDN new end number. */
    private static final String                       C_NEW_END_NO         = "01110";

    /** Constant used for Charging Ind. */
    private static final String                       C_CHARG_IND          = "3";

    /** Constant used for Descrpription. */
    private static final String                       C_DESC               = "fach 3";

    /** Constant used for charging indicator data object. */
    private static final FachFafChargingIndData       C_CHARG_IND_DATA     = new FachFafChargingIndData
                                                                            ("3",
                                                                             "fach 3",
                                                                             false,
                                                                             "SUPER",
                                                                             null);


    /** Constant used for boi charging indicator data object. */
    private static final BoiFafChargingIndicatorsData C_BOI_CHARG_IND_DATA = new BoiFafChargingIndicatorsData
                                                                             (C_CHARG_IND_DATA);

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** FaF default Charging Indicators screen. */
    private FafDefaultChargingIndPane                 i_fafDefaultChargIndPane;

    /** Text field to hold the start msisdn. */
    private JFormattedTextField                       i_startNoField;

    /** Text field to hold the end msisdn. */
    private JFormattedTextField                       i_endNoField;

    /** Combo box to hold the configured charging indexes. */
    private JComboBox                                 i_chargIndComboBox;

    /** Key data combo box. */
    protected JComboBox                               i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton                                 i_updateButton;

    /** Delete button. */
    protected JButton                                 i_deleteButton;



  //------------------------------------------------------------------------
  // Constructors
  //------------------------------------------------------------------------

  /**
   * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
   * takes a test case name as its argument
   * @param p_title The testcase name.
   */
  public FafDefaultChargingIndPaneUT(String p_title)
    {
        super(p_title);

        deleteDeciChargingIndRecord(C_START_NO);

        i_fafDefaultChargIndPane = new FafDefaultChargingIndPane(c_context);
        super.init(i_fafDefaultChargIndPane);

        i_fafDefaultChargIndPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_startNoField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "startNumber");
        i_endNoField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "endNumber");
        i_chargIndComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "i_chargIndComboBox");

        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

  //------------------------------------------------------------------------
  // Public class methods
  //------------------------------------------------------------------------

  /**
   * Static method that allows the framework to automatically run all the tests in the class. A program
   * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
   * get an instance of the TestCase which it then executes.
   * @return an instance of <code>junit.framework.Test</code>
   */
  public static Test suite()
  {
      return new TestSuite(FafDefaultChargingIndPaneUT.class);
  }

  /** 
   * @ut.when a user attempts to insert a record through the Number Blocks screen.
   * @ut.then the operation is successfully performed.
   * @ut.attributes +f
   */
  public void testDatabaseUpdates()
    {
        beginOfTest("FafDefaultChargingIndPane test");
        BoiFafDefaultChargingIndData l_boiFafDefChargIndData;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        if (i_keyDataCombo.getComponentCount() == 0)
        {
            fail("No data configured.");
        }
        SwingTestCaseTT.setKeyComboSelectedItem(i_fafDefaultChargIndPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_startNoField, C_START_NO);
        SwingTestCaseTT.setTextComponentValue(i_endNoField, C_END_NO);

        i_chargIndComboBox.setSelectedItem(C_BOI_CHARG_IND_DATA);
        doInsert(i_fafDefaultChargIndPane, i_updateButton);
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiFafDefChargIndData = new BoiFafDefaultChargingIndData(
                                                                   new DeciDefaultChargingIndData(C_START_NO,
                                                                                                  C_END_NO,
                                                                                                  C_CHARG_IND,
                                                                                                  C_DESC,
                                                                                                  false,
                                                                                                  "SUPER",
                                                                                                  null));
        SwingTestCaseTT.setKeyComboSelectedItem(i_fafDefaultChargIndPane,
                                                i_keyDataCombo,
                                                l_boiFafDefChargIndData);

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_endNoField, C_NEW_END_NO);
        doUpdate(i_fafDefaultChargIndPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_fafDefaultChargIndPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_fafDefaultChargIndPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_startNoField, C_START_NO);
        doMakeAvailable(i_fafDefaultChargIndPane, i_updateButton);

        endOfTest();

    }
  
  //------------------------------------------------------------------------
  // Protected methods
  //------------------------------------------------------------------------

  /**
   * This method is used to setup anything required by each test.
   */
  protected void setUp()
  {
      deleteDeciChargingIndRecord(C_START_NO);
  }
  
  /**
   * Performs standard clean up activities at the end of a test.
   */
  protected void tearDown()
  {
      deleteDeciChargingIndRecord(C_CODE);
      say(":::End Of Test:::");
  }

  //------------------------------------------------------------------------
  // Private methods
  //------------------------------------------------------------------------

  /** Method name used in calls to middleware. */
  private static final String C_METHOD_deleteFafChargingIndRecord = "deleteDeciChargingIndRecord";

  /**
   * Removes a row from deci_default_charging_ind.
   * @param p_deciStartNo FaF default Charging Indicators id.
   */
  private void deleteDeciChargingIndRecord(String p_deciStartNo)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = new String("DELETE from deci_default_charging_ind " 
                           + "WHERE deci_number_series_start = {0}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_deciStartNo);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteFafChargingIndRecord);
    }
}

