////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AgentInformationPaneUT.java
//      DATE            :       28-Nov-2005
//      AUTHOR          :       Mikael Alm
//      REFERENCE       :       PpacLon#1755/7500
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       JUnit test class for AgentInformationPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
//----------+------------+---------------------------------+--------------------
// 02/04/07 | Andy Harris| Change for suspect flag field.  | PpacLon#3011/11241
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiAgentInfoData;
import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for AgentInformationPane. */
public class AgentInformationPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "AgentInformationPaneUT";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Agent Information screen. */
    private AgentInformationPane         i_agentPane;
    
    /** Agent Information code field. */
    private ValidatedJTextField          i_codeField;
    
    /** Agent Information description field. */
    private ValidatedJTextField          i_descriptionField;
    
    /** Market combo box. */
    private JComboBox                    i_marketDataComboBox;
   
    /** Constant defining a single agent id. */
    private static final String          C_AGENT_CODE          = "99998888";
    
    /** Constant defining a single agent description.*/
    private static final String          C_AGENT_DESC          = "UT test";
    
    /** Constant defining a Market for testing. */
    private static final BoiMarket       C_AGENT_MARKET        = new BoiMarket(new Market(2, 3));
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public AgentInformationPaneUT(String p_title)
    {
        super(p_title);
        
        i_agentPane = new AgentInformationPane(c_context);
        super.init(i_agentPane);
        
        i_agentPane.resetScreenUponEntry();

        i_marketDataComboBox       = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "marketDataComboBox");
        i_keyDataCombo             = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                              "definedCodesBox");
        i_codeField                = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "codeField");
        i_descriptionField         = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                        "descriptionField");
        
        i_updateButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton             = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(AgentInformationPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again 
     *     through the Agent Information screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("AgentInformationPane test");
        
        BoiAgentInfoData l_boiAgentInfoData;
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        SwingTestCaseTT.setMarketComboSelectedItem(i_agentPane, i_marketDataComboBox, C_AGENT_MARKET);        
        SwingTestCaseTT.setKeyComboSelectedItem(i_agentPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_AGENT_CODE);
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_AGENT_DESC);
        doInsert(i_agentPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        l_boiAgentInfoData = createAgentInfoData(C_AGENT_MARKET, C_AGENT_CODE, C_AGENT_DESC);        
        SwingTestCaseTT.setKeyComboSelectedItem(i_agentPane, i_keyDataCombo, l_boiAgentInfoData);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_NEW_DESCRIPTION);
        doUpdate(i_agentPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_agentPane, i_deleteButton);
        
        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_agentPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_codeField, C_AGENT_CODE);
        doMakeAvailable(i_agentPane, i_updateButton);
        
        // *********************************************************************
        say("End of test.");
        // *********************************************************************
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteAgentRecord(C_AGENT_MARKET, C_AGENT_CODE);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteAgentRecord(C_AGENT_MARKET, C_AGENT_CODE);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /**
     * Creates a BoiAgentInfoData object with the supplied data.
     * @param p_market    Agents market.
     * @param p_agent     Agent id.
     * @param p_agentDesc Agent description.
     * @return BoiAgentInfoData object created from the supplied data.
     */
    private BoiAgentInfoData createAgentInfoData(BoiMarket p_market, String p_agent, String p_agentDesc)
    {
        SrvaAgentMstrData  l_agentData;
        
        l_agentData = new SrvaAgentMstrData(null, 
                                            p_market.getMarket(), 
                                            p_agent, 
                                            p_agentDesc, 
                                            null, 
                                            null, 
                                            ' ',
                                            ' ');
        
        return new BoiAgentInfoData(l_agentData);
    }
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteAgentRecord = "deleteAgentRecord";
    
    /**
     * Removes a row from srva_agent_mstr.
     * @param p_market Agents market     
     * @param p_agent  Agent id.
     */
    private void deleteAgentRecord(BoiMarket p_market, String p_agent)
    {
        String           l_sql;
        SqlString        l_sqlString;
        
        l_sql = "DELETE from srva_agent_mstr " +
                           "WHERE srva  = {0} " +
                           "AND   sloc  = {1} " +
                           "AND   agent = {2} ";
        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setIntParam   (0, p_market.getMarket().getSrva());
        l_sqlString.setIntParam   (1, p_market.getMarket().getSloc());
        l_sqlString.setStringParam(2, p_agent);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteAgentRecord);
    }
}
