////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiPromoPlanData.java
//    DATE            :       06-Sep-2004
//    AUTHOR          :       Mario Imfeld
//    REFERENCE       :       PpacLon#470/3661
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Wrapper for PrplPromoPlanData class 
//                            within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;

/** Wrapper for promotion plan class within BOI. */
public class BoiPromoPlanData extends DataObject
{
    
    /** Basic promotion plan data object */
    private PrplPromoPlanData i_prplData;

    /**
    * Simple constructor.
    * @param p_prplData Promotion plan data object to wrapper.
    */
    public BoiPromoPlanData(PrplPromoPlanData p_prplData)
    {
        i_prplData = p_prplData;
    }

    /**
    * Return wrappered promotion plan data object.
    * @return Wrappered promotion plan data object.
    */
    public PrplPromoPlanData getInternalPromoPlanData()
    {
        return i_prplData;
    }

    /**
    * Compares two promotion plan data objects and returns true if they equate.
    * @param p_prplData Promotion plan data object to compare the current instance with.
    * @return True if both instances are equal
    */
    public boolean equals(Object p_prplData)
    {
        boolean l_return = false;

        if ( p_prplData != null &&
             p_prplData instanceof BoiPromoPlanData &&
             i_prplData.getPromoPlan().equals(
                 ((BoiPromoPlanData)p_prplData).getInternalPromoPlanData().getPromoPlan()) )
        {
            l_return = true;
        }
        return l_return;
    }

    /**
    * Returns promotion plan data object as a String for display in BOI.
    * @return Promotion plan data object as a string.
    */
    public String toString()
    {
        String l_displayString = new String(i_prplData.getPromoPlan() + 
                                     " - " + i_prplData.getPromoDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object . 
     */
    public int hashCode()
    {
        return i_prplData.getPromoPlan().length();
    }
}