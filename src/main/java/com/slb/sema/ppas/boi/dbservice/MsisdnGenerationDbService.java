////////////////////////////////////////////////////////////////////////////////
//  ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       MsisdnGenerationDbService.java
//  DATE            :       08-Nov-2004
//  AUTHOR          :       Michael Erksine
//  REFERENCE       :       PpacLon#882/4807
//
//  COPYRIGHT       :       Atos Origin 2004
//
//  DESCRIPTION     :       Database access class for BOI MSISDN Generation screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiSdpIdData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScpiScpInfoSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasConfigException;

/** Database access class for BOI MSISDN Generation screen. */
public class MsisdnGenerationDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------

    /** Used to retrieve routing method from system config. */
    private static final String C_DESTINATION = "BUS_SRV";
    
    /** Used to retrieve routing method from system config. */
    private static final String C_PARAM_NAME = "ROUTING_METHOD";
    
    /** Default routing method. */
    private static final String C_DEFAULT_FORMAT = "SDP";
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** Routing method (SDP or number range). */
    private String i_routingMethod;
    
    /** Set of SDP id data objects. */
    private ScpiScpInfoDataSet i_scpiDataSet;
    
    /** System-wide configuration SQL service. */
    private SyfgSystemConfigSqlService i_syfgSqlService;
    
    /** Service to read from the SCPI table. */
    private ScpiScpInfoSqlService i_scpiSqlService;
    
    /** Vector of available SDP ids. */
    private Vector i_availableSdpIdV;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor
     * @param p_context BOI context object.
     */
    public MsisdnGenerationDbService(BoiContext p_context)
    {
        super(p_context);
        i_syfgSqlService = new SyfgSystemConfigSqlService(null);
        i_scpiSqlService = new ScpiScpInfoSqlService(null);
        i_availableSdpIdV = new Vector();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Returns the routing method, SDP or number range.
     * @return Routing method - SDP or number range.
     */
    public String getRoutingMethod()
    {
        return i_routingMethod;
    }
    
    /** 
     * Return vector of available SDP id strings.
     * @return Vector of available SDP id strings.
     */
    public Vector getAvailableSdpIds()
    {
        return i_availableSdpIdV;
    }
    
    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------
    
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException 
    {
        SyfgSystemConfigData l_syfgData;
        
        l_syfgData = i_syfgSqlService.read(null, p_connection);
        
        try
        {
            i_routingMethod = l_syfgData.get(C_DESTINATION, C_PARAM_NAME);
        }
        catch (PpasConfigException l_pCE)
        {
            i_routingMethod = C_DEFAULT_FORMAT;
        }
        refreshSdpIdDataVector(p_connection);
    }
    
    /** 
     * Method not required for MsisdnGenerationDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
        // Not required
    }
    
    /** 
     * Method not required for MsisdnGenerationDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException  
    { 
        // Not required
    }
    
    /** 
     * Method not required for MsisdnGenerationDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
            // Not required
    }
    
    /** 
     * Method not required for MsisdnGenerationDbService. 
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException 
    { 
            // Not required
    }
        
    /** 
     * Method not required for MsisdnGenerationDbService. 
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException 
    {
        return null;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    /** 
     * Refreshes the available SDP id data vector from the 
     * scpi_scp_info table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshSdpIdDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {        
        ScpiScpInfoData[] l_sdpArray;        
       
        i_availableSdpIdV.removeAllElements();
        i_availableSdpIdV.add(0, "");
        
        i_scpiDataSet = i_scpiSqlService.readAll(null, p_connection);
        l_sdpArray = i_scpiDataSet.getAvailableArray();
        
        for (int i=0; i < l_sdpArray.length; i++)
        {
            i_availableSdpIdV.addElement(new BoiSdpIdData(l_sdpArray[i]));
        }
    }
}