////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       OperatorsDbService.java
//    DATE            :       2-Jul-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#338/3075
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Database access class for Operators screen.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//--------- +------------+---------------------------------+--------------------
// 29/08/06 | A Kutthan  | Made numerous changes within    | PpacLon#2595/9828
//          |            | Constructor, added              |
//          |            | setStrongPasswordConfigured(..) |
//          |            | and isStrongPasswordConfigured()|
//          |            | methods, and made some additions|
//          |            | to the readInitial(..) method   |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.dbservice;

import java.util.Vector;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.boi.boidata.BoiPrivilegeLevelData;
import com.slb.sema.ppas.boi.gui.BoiContext;
import com.slb.sema.ppas.common.businessconfig.dataclass.BicsysUsersrvasDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ConfCompanyConfigData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUserfileSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BicsysUsersrvasSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrilPrivilegeLevelSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.OperatorData;
import com.slb.sema.ppas.common.exceptions.PpasSecurityException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.sql.PpasSqlMsg;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasSecurity;

/**
 * Database access class for Operators screen.
 */
public class OperatorsDbService extends BoiDbService
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** SQL service for operator details. */
    private BicsysUserfileSqlService i_operatorSqlService = null;

    /** SQL service for operator privilege level data. */
    private PrilPrivilegeLevelSqlService i_privilegeLevelSqlService = null;
    
    /** SQL service for BicsysUsersrvas data. */
    private BicsysUsersrvasSqlService i_bicsysUsersrvasSqlService = null;
    
    /** SQL service for Syfg_system_config data. */
    private SyfgSystemConfigSqlService i_syfgSystemConfigSqlService = null;

    /** Currently selected operator id. */
    private String i_currentOpid = null;
    
    /** Operator config data. */
    private OperatorData i_operatorData = null;

    /** Encrypted operator password. */
    private String i_hashedPassword = "";

    /** List of available markets for display. */
    private Vector i_availableMarkets = null;

    /** Set of markets the operator has access to as configured in the database. */
    private Vector i_dbSelectedMarkets = null;

    /** Set of markets the operator has access to as configured on the screen. */
    private Vector i_screenSelectedMarkets = null;

    /** List of configured operators. */
    private Vector i_operators = null;

    /** List of available privileges. */
    private Vector i_availablePrivileges = null;
    
    /** DataSet of privilege level objects */
    private PrilPrivilegeLevelDataSet i_privilegeLevelDataSet = null;

    /** Privilege data object corresponding to operator's selected privilege. */
    private PrilPrivilegeLevelData i_selectedPrivilege = null;

    /** System base currency */
    private PpasCurrency i_baseCurrency;

    /** Flag to indicate this user should be unbarred. */
    private boolean i_unbarFlag;
    
    /** Currently configured Strong Password value. */
    private boolean i_strongPasswordConfigured = false;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor
     * @param p_context BOI context object.
     */
    public OperatorsDbService(BoiContext p_context)
    {
        super(p_context);
        
        i_operatorSqlService = new BicsysUserfileSqlService(null, null);
        i_privilegeLevelSqlService = new PrilPrivilegeLevelSqlService(null, null);
        i_bicsysUsersrvasSqlService = new BicsysUsersrvasSqlService(null, null, null);
        i_syfgSystemConfigSqlService = new SyfgSystemConfigSqlService(null);
        
        i_availableMarkets = new Vector(5);
        i_dbSelectedMarkets = new Vector(5);
        i_screenSelectedMarkets = new Vector(5);
        i_availablePrivileges = new Vector(5);
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Sets currently configured Strong Password value
     * @param p_strongPasswordConfigured Currently configured Strong Password value 
     */
    public void setStrongPasswordConfigured(boolean p_strongPasswordConfigured)
    {
        i_strongPasswordConfigured = p_strongPasswordConfigured;
    }

    /**
     * Return currently configured Strong Password value
     * @return currently configured Strong Password value
     */
    public boolean isStrongPasswordConfigured()
    {
        return i_strongPasswordConfigured;
    }
    
    /** 
     * Set current operator. Used for record lookup in database. 
     * @param p_opid Currently selected operator id. 
     */
    public void setCurrentOpid(String p_opid)
    {
        i_currentOpid = p_opid;
    }

    /** 
     * Set flag to indicate this user should be unbarred.
     * @param p_unbarFlag True if this operator should be unbarred. 
     */
    public void setUnbarOperator(boolean p_unbarFlag)
    {
        i_unbarFlag = p_unbarFlag;
    }

    /** 
     * Return operator config data. 
     * @return Operator config data. 
     */
    public OperatorData getOperatorData()
    {
        return i_operatorData;
    }

    /** 
     * Return operator's default market. 
     * @return Operator's default market. 
     */
    public BoiMarket getDefaultMarket()
    {
        return new BoiMarket(i_operatorData.getDefaultMarket());
    }

    /** 
     * Set operator config data. 
     * @param p_operatorData Operator configuration data. 
     */
    public void setOperatorData(OperatorData p_operatorData)
    {
        i_operatorData = p_operatorData;
    }

    /** 
     * Return set of markets the operator has access to. 
     * @return Set of markets the operator has access to.
     */
    public Vector getDbSelectedMarkets()
    {
        return i_dbSelectedMarkets;
    }

    /** 
     * Set the markets which the operator has access to. 
     * @param p_markets Markets to which the operator has access. 
     * */
    public void setScreenSelectedMarkets(Vector p_markets)
    {
        i_screenSelectedMarkets = p_markets;
    }

    /** 
     * Return list of configured operators. 
     * @return List of configured operators. 
     */
    public Vector getOperators()
    {
        return i_operators;
    }

    /** 
     * Return list of available privileges. 
     * @return List of available privileges. 
     */
    public Vector getAvailablePrivileges()
    {
        return i_availablePrivileges;
    }

    /** 
     * Return privilege data object corresponding to operator's selected privilege. 
     * @return Privilege data object corresponding to operator's selected privilege. 
     */
    public PrilPrivilegeLevelData getSelectedPrivilege()
    {
        return i_selectedPrivilege;
    }

    /** 
     * Return list of available markets. 
     * @return List of available markets. 
     */
    public Vector getAvailableMarkets()
    {
        return i_availableMarkets;
    }

    /** 
     * Return precision of system base currency.
     * @return Precision of system base currency. 
     */
    public int getBaseCurrencyPrecision()
    {
        return i_baseCurrency.getPrecision();
    }

    /**
     * Encrypts the operator password.
     * @param p_password Operator password.
     * @throws PpasSecurityException Exception related to security.
     */
    public void encryptPassword(String p_password)
        throws PpasSecurityException
    {
        if (p_password.equals(""))
        {
            i_hashedPassword = "";
        }
        else
        {
            i_hashedPassword = PpasSecurity.hashPassword(p_password);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods
    //-------------------------------------------------------------------------

    /**
     * Populates this data object with data from the database when an existing record is selected.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void read(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_operatorData = i_operatorSqlService.getOperatorData(null, 
                                                              p_connection,
                                                              i_currentOpid,
                                                              i_baseCurrency);

        i_operators = i_operatorSqlService.getOperators(null, p_connection);

        i_selectedPrivilege = i_privilegeLevelDataSet.getPrilPrivilegeLevelData(
                                                                 i_operatorData.getPrivilegeId());
        
        refreshDbSelectedMarkets(p_connection);
    }
   
    /**
     * Reads data required by the pane when it is in its initial state.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    protected void readInitial(JdbcConnection p_connection)
        throws PpasSqlException
    {   
        Market[]                  l_availableMarketArray = null;
        SrvaMstrDataSet           l_srvaMstrDataSet = null;
        ConfCompanyConfigData     l_confCompanyConfigData = null;
        SyfgSystemConfigData      l_syfgSystemConfigData = null;
        boolean                   l_strongPasswordConfigured = false;
        
        i_operators = i_operatorSqlService.getOperators(null, p_connection);

        refreshPrivilegeLevelDataVector(p_connection);
        
        l_srvaMstrDataSet = i_marketSqlService.readAll(null, p_connection);
        l_availableMarketArray = l_srvaMstrDataSet.getAvailableMarkets();
        i_availableMarkets.removeAllElements();
        
        for (int i = 0; i < l_availableMarketArray.length; i++)
        {
            i_availableMarkets.addElement(new BoiMarket(l_availableMarketArray[i]));
        }

        l_confCompanyConfigData = i_companyConfigSqlService.readConfCompanyConfig(null, p_connection);
        i_baseCurrency = l_confCompanyConfigData.getBaseCurrency();
        
        l_syfgSystemConfigData = i_syfgSystemConfigSqlService.read(null, p_connection);
        
        l_strongPasswordConfigured = l_syfgSystemConfigData.getBoolean("PASSWORD", "PASSWORD_STRONG", "TRUE");
       
        setStrongPasswordConfigured(l_strongPasswordConfigured);  
    }
    
    /** 
     * Inserts operator data in the bicsys_userfile and bicsys_usersrvas tables for the 
     * operator held in i_operatorData.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void insert(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_operatorSqlService.insert(null, 
                                    p_connection,
                                    i_operatorData.getOpid(),
                                    i_operatorData.getOperatorName(),
                                    i_operatorData.getDefaultMarket(),
                                    i_operatorData.getPrivilegeId(),
                                    i_operatorData.getMaxAdjustAmountForBoi(),
                                    i_operatorData.getDailyAdjustLimitForBoi(),
                                    i_hashedPassword,
                                    i_operatorData.getPasswordExpiryDate(),
                                    i_operatorData.hasGuiPrivilege(),
                                    i_operatorData.hasPosiPrivilege(),
                                    i_operatorData.hasPamiPrivilege(),
                                    i_context.getOperatorUsername());
                                    
        insertSelectedMarketData(p_connection);

        i_operators = i_operatorSqlService.getOperators(null, p_connection);
        refreshDbSelectedMarkets(p_connection);
    }

    /** 
     * Updates operator data in the bicsys_userfile and bicsys_usersrvas tables for the 
     * operator held in i_operatorData.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void update(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_operatorSqlService.update(null, 
                                    p_connection,
                                    i_operatorData.getOpid(),
                                    i_operatorData.getOperatorName(),
                                    i_operatorData.getDefaultMarket(),
                                    i_operatorData.getPrivilegeId(),
                                    i_operatorData.getMaxAdjustAmountForBoi(),
                                    i_operatorData.getDailyAdjustLimitForBoi(),
                                    i_hashedPassword,
                                    i_operatorData.getPasswordExpiryDate(),
                                    i_operatorData.hasGuiPrivilege(),
                                    i_operatorData.hasPosiPrivilege(),
                                    i_operatorData.hasPamiPrivilege(),
                                    i_context.getOperatorUsername(),
                                    i_unbarFlag);
                                    
        updateSelectedMarketData(p_connection);

        i_operators = i_operatorSqlService.getOperators(null, p_connection);
        refreshDbSelectedMarkets(p_connection);
    }
    
    /** 
     * Deletes operator data from the bicsys_userfile and bicsys_usersrvas tables for the 
     * operator held in i_operatorData.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    protected void delete(JdbcConnection p_connection)
        throws PpasSqlException
    {
        i_operatorSqlService.delete(null, 
                                    p_connection,
                                    i_currentOpid);
                                    
        i_bicsysUsersrvasSqlService.deleteForOpid(null, 
                                                  p_connection,
                                                  i_currentOpid);

        i_operators = i_operatorSqlService.getOperators(null, p_connection);
    }

    /**
     * Checks whether a record about to be inserted into the database already exists.
     * Operators are deleted rather than withdrawn, so the withdrawn flag is always 
     * returned as false.
     * @param p_connection Database connection.
     * @return boolean array: element[C_DUPLICATE] indicates if the record is a duplicate,
     *                        element[C_WITHDRAWN] indicates if the record is withdrawn.
     * @throws PpasSqlException SQL-related exception 
     */
    protected boolean[] checkForDuplicate(JdbcConnection p_connection)
        throws PpasSqlException
    {
        boolean[] l_flagsArray = new boolean[2];
        
        try
        {
            i_operatorSqlService.getOperatorData(null, 
                                                 p_connection,
                                                 i_currentOpid,
                                                 i_baseCurrency);
            l_flagsArray[C_DUPLICATE] = true;
        }
        catch (PpasSqlException l_pSqlE)
        {
            if (l_pSqlE.getMsgKey().equals(PpasSqlMsg.C_KEY_DATABASE_INCONSISTENCY))
            {
                l_flagsArray[C_DUPLICATE] = false;
            }
            else
            {
                throw l_pSqlE;
            }
        }
                               
        l_flagsArray[C_WITHDRAWN] = false;

        return l_flagsArray;
    }
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------

    /**
     * Updates the bicsys_usersrvas table with the selected markets for the updated operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    private void updateSelectedMarketData(JdbcConnection p_connection)
        throws PpasSqlException
    {
        boolean   l_found = false;
        BoiMarket l_boiScreenMarket = null;
        BoiMarket l_boiDbMarket = null;
        
        // Loop through the markets selected on the screen for this operator, and insert any 
        // that are not already stored in the database. 
        for (int i = 0; i < i_screenSelectedMarkets.size(); i++)
        {
            l_boiScreenMarket = (BoiMarket)i_screenSelectedMarkets.elementAt(i);
                
            l_found = false;

            for (int j = 0; j < i_dbSelectedMarkets.size() && !l_found; j++)
            {
                if ( l_boiScreenMarket.equals(i_dbSelectedMarkets.elementAt(j)) )
                {
                    l_found = true;
                }
            }
            
            if (!l_found)
            {
                i_bicsysUsersrvasSqlService.insert(
                            null,
                            p_connection,
                            i_operatorData.getOpid(),
                            l_boiScreenMarket.getMarket());
            }
        }
        
        // Loop through the markets already stored in the database for this operator, and delete any 
        // that are not selected on the screen. 
        for (int i = 0; i < i_dbSelectedMarkets.size(); i++)
        {
            l_boiDbMarket = (BoiMarket)i_dbSelectedMarkets.elementAt(i);
                
            l_found = false;

            for (int j = 0; j < i_screenSelectedMarkets.size() && !l_found; j++)
            {
                if ( l_boiDbMarket.equals(i_screenSelectedMarkets.elementAt(j)) )
                {
                    l_found = true;  
                }
            }
            
            if (!l_found)
            {
                i_bicsysUsersrvasSqlService.delete(
                        null,
                        p_connection,
                        i_operatorData.getOpid(),
                        l_boiDbMarket.getMarket());
            }
        }
    }

    /**
     * Updates the bicsys_usersrvas table with the selected markets for the new operator.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    private void insertSelectedMarketData(JdbcConnection p_connection)
        throws PpasSqlException
    {
        BoiMarket l_boiScreenMarket = null;

        for (int i = 0; i < i_screenSelectedMarkets.size(); i++)
        {
            l_boiScreenMarket = (BoiMarket)i_screenSelectedMarkets.elementAt(i);
                
            i_bicsysUsersrvasSqlService.insert(
                            null,
                            p_connection,
                            i_operatorData.getOpid(),
                            l_boiScreenMarket.getMarket());
        }
    }
    
    /**
     * Refreshes the selected markets area from the bicsys_usersrvas table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */
    private void refreshDbSelectedMarkets(JdbcConnection p_connection)
        throws PpasSqlException
    {
        BicsysUsersrvasDataSet    l_bicsysUsersrvasDataSet = null;
        Vector                    l_selectedMarkets = null;
        
        l_bicsysUsersrvasDataSet = i_bicsysUsersrvasSqlService.readAll(null, p_connection);
        l_selectedMarkets = l_bicsysUsersrvasDataSet.getSelectedMarkets(i_currentOpid);
        i_dbSelectedMarkets.removeAllElements();
        for (int i = 0; i < l_selectedMarkets.size(); i++)
        {
            i_dbSelectedMarkets.addElement(new BoiMarket((Market)l_selectedMarkets.get(i)));
        }
    }

    /** 
     * Refreshes the available privilege level data vector from the 
     * pril_privilege_level table.
     * @param p_connection Database connection.
     * @throws PpasSqlException SQL-related exception 
     */    
    private void refreshPrivilegeLevelDataVector(JdbcConnection p_connection)
        throws PpasSqlException
    {
        PrilPrivilegeLevelData[]  l_prilArray;
        
        i_availablePrivileges.removeAllElements();
        
        i_privilegeLevelDataSet = i_privilegeLevelSqlService.readAll(null, p_connection);
        l_prilArray = i_privilegeLevelDataSet.getAvailablePrivileges();
        
        for (int i=0; i < l_prilArray.length; i++)
        {
            i_availablePrivileges.addElement(new BoiPrivilegeLevelData(l_prilArray[i]));
        }
    }
}