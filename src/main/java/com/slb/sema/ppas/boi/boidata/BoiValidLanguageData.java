////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BoiValidLanguageData.java
//    DATE            :       06-Sep-2004
//    AUTHOR          :       Mario Imfeld
//    REFERENCE       :       PpacLon#470/3661
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       Wrapper for ValaValidLanguageData class 
//                            within BOI.  
//                            Supports toString and equals methods for use 
//                            within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageData;

/** Wrapper for valid language class within BOI. */
public class BoiValidLanguageData extends DataObject
{
    
    /** Basic valid language data object */
    private ValaValidLanguageData i_valaData;

    /**
    * Simple constructor.
    * @param p_valaData Valid language data object to wrapper.
    */
    public BoiValidLanguageData(ValaValidLanguageData p_valaData)
    {
        i_valaData = p_valaData;
    }

    /**
    * Return wrappered valid language data object.
    * @return Wrappered valid language data object.
    */
    public ValaValidLanguageData getInternalValidLanguageData()
    {
        return i_valaData;
    }

    /**
    * Compares two valid language data objects and returns true if they equate.
    * @param p_valaData Valid language data object to compare the current instance with. 
    * @return True if both instances are equal
    */
    public boolean equals(Object p_valaData)
    {
        boolean l_return = false;
       
        if ( p_valaData != null &&
             p_valaData instanceof BoiValidLanguageData &&
             i_valaData.getCode().equals(
                 ((BoiValidLanguageData)p_valaData).getInternalValidLanguageData().getCode()))
        {
            l_return = true;
        }
        return l_return;
    }

    /**
    * Returns valid language data object as a String for display in BOI.
    * @return Valid language data object as a string.
    */
    public String toString()
    {
        String l_displayString = new String(i_valaData.getCode() + 
                                  " - " + i_valaData.getDescription());
        return l_displayString;
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_valaData.getValaAnnouncementLanguageNumber();
    }
}