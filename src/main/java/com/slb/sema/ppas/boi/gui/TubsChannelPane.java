////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TubsChannelPane.java
//      DATE            :       22-Dec-2006
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       PpacLon#2822/10659
//                              PRD_ASCS00_GEN_CA104
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       BOI screen to maintain TUBS channel definitions.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/01/07 |M.Tornqvist | Group field changed to combobox.| PpacLon#2841/10735
//----------+------------+---------------------------------+--------------------
// 11/01/07 |M.Tornqvist | Stop the possibility to create  | PpacLon#2874/10819
//          |            | a "never ending hierachy" og    |
//          |            | channels.                       |
//----------+------------+---------------------------------+--------------------
// 11/01/07 |M.Tornqvist | Clean the Group field after     | PpacLon#2867/10817
//          |            | "NEW RECORD" has been selected  |
//----------+------------+---------------------------------+--------------------
// 11/01/07 |M.Tornqvist | Don't clean the Group field when| PpacLon#2868/10830
//          |            | pressing the "Update button".   |
//----------+------------+---------------------------------+--------------------
// 23/01/07 |M.Tornqvist | Filter the possible choices in  | PpacLon#2874/10868
//          |            | the the group combibox, so that |
//          |            | the hierachy only can be 2 level|
//----------+------------+---------------------------------+--------------------
// 22/06/07 | SJ Vonka   | Validate the use of wildcards.  | PpacLon#3184/11759
//          |            |                                 | prd_ascs00_gen_ca_120
//----------+------------+---------------------------------+--------------------
// 11/09/07 | Ian James  |                                 | PpacLon#3139/12076
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.boi.gui;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.slb.sema.ppas.boi.boidata.BoiChavChannelData;
import com.slb.sema.ppas.boi.dbservice.ChavChannelDbService;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;
import com.slb.sema.ppas.swing.components.StringTableModel;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.components.WidgetFactory;
import com.slb.sema.ppas.swing.gui.Context;

public class TubsChannelPane extends BusinessConfigBasePane
{
    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** Panel allowing data selection and modification. */
    private JPanel               i_detailsPanel      = null;

    /** Panel containing table of existing records. */
    private JPanel               i_definedCodesPanel = null;

    /** Existing Channel codes vector. */
    private Vector               i_definedCodes      = null;   
    
    /** Channel code field. */
    private ValidatedJTextField  i_codeField         = null;

    /** TUBS Channel description field. */
    private ValidatedJTextField  i_descriptionField  = null;
    
    /** ComboBox model to hold the array of already defined "master groups". */
    private DefaultComboBoxModel i_groupModel        = null;

    /** 
     * Available service class combo box used to filter records for the key data combo.
     * Only applicable for a couple of screens, so created in subclasses.
     */
    protected JComboBox          i_groupComboBox     = null;

    /** Defined channels vector. */
    private Vector               i_definedGroups     = new Vector();

    /** Table containing existing records. */
    private JTable               i_table             = null;

    /** Column names for table of existing records. */
    private String[]             i_columnNames       = {"Channel ID",
                                                        "Description",
                                                        "Group"};

    /** Data array for table of existing records. */
    private String               i_data[][]          = null;

    /** Data model for table of existing records. */
    private StringTableModel     i_stringTableModel  = null;
    
    /** The vertical scroll bar used for the Channel table view port. */
    private JScrollBar           i_verticalScrollBar = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------    
    
    /** 
     * TubsChannelPane constructor. 
     * @param p_context A reference to the BoiContext
     */        
    public TubsChannelPane(Context p_context)
    {
        super(p_context, (Container)null);
        
        i_dbService = new ChavChannelDbService((BoiContext)i_context);        
        
        super.init();
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------    
        
    /**
     * Handles mouse released event on codes table.
     * @param p_mouseEvent The event to be handled.
     */
    public void mouseReleased(MouseEvent p_mouseEvent)
    {
        int l_selectedRowIndex = i_table.getSelectedRow();
        if ( (l_selectedRowIndex != -1) && (i_definedCodes.size() > (l_selectedRowIndex + 2)) )
        {
            i_keyDataComboBox.setSelectedIndex(l_selectedRowIndex + 2);
        }
    }

    //-------------------------------------------------------------------------
    // Protected methods overriding hook methods in superclass
    //-------------------------------------------------------------------------    
   /**
     * Alters screen bevaviour when NEW RECORD is selected from the key data dropdown.
     */
    protected void updateScreenForNewRecord()
    {
        i_codeField.setEnabled(true);
        i_descriptionField.setEnabled(true);
        i_groupComboBox.setSelectedIndex(0);
    }
    
    /**
     * Alters screen bevaviour when an existing record is selected from the key data dropdown.
     */
    protected void updateScreenForExistingRecord()
    {
        i_codeField.setEnabled(false);
    }    

    
    //-------------------------------------------------------------------------
    // Protected methods overriding abstract methods in superclass
    //-------------------------------------------------------------------------    

    /** 
     * Initialises the screen data to default values.
     */
    protected void initialiseScreen()
    {
        i_codeField.setText("");
        i_descriptionField.setText("");        
        i_groupComboBox.setSelectedIndex(0);

        i_table.clearSelection();
        i_verticalScrollBar.setValue(0);
    }    
   
    /**
     * Paints the screen.
     */
    protected void paintScreen()
    {
        i_mainPanel = WidgetFactory.createMainPanel("Channels", 100, 100, 0, 0);

        i_helpComboBox = WidgetFactory.createHelpComboBox(
                                BoiHelpTopics.getHelpTopics(BoiHelpTopics.C_TUBS_CHANNEL_GROUPS_SCREEN), 
                                i_helpComboListener);

        createDetailsPanel();
        createDefinedCodesPanel();
        
        i_mainPanel.add(i_helpComboBox,      "channelGroupsHelpComboBox,60,1,40,4");
        i_mainPanel.add(i_detailsPanel,      "detailsPanel,1,6,100,48");
        i_mainPanel.add(i_definedCodesPanel, "definedCodesPanel,1,55,100,46");
        
    }

    /**
     * Refreshes the current record fields with values held in the screen data object.
     */
    protected void refreshCurrentRecord()
    {
        ChavChannelData l_channelData;
        l_channelData = ((ChavChannelDbService)i_dbService).getChannelData().getInternalChannelData();
        i_codeField.setText(l_channelData.getChannelId());
        i_descriptionField.setText(l_channelData.getDesc());
        i_groupComboBox.setSelectedItem(l_channelData.getChannelGroup());
        
        selectRowInTable(i_table, i_verticalScrollBar);
    }
    
    /**
     * Refreshes data upon screen entry.
     */
    protected void refreshDataUponEntry()
    {  
    	i_definedGroups = ((ChavChannelDbService)i_dbService).getAvailableGroups(null);
        i_groupModel    = new DefaultComboBoxModel(i_definedGroups);
        i_groupComboBox.setModel(i_groupModel);

        refreshListData();
    }

    /**
     * Refreshes data in combo boxes, jlists, or tables.  Used to refresh data 
     * after inserts, deletes, and updates, and when different market selected.
     */
    protected void refreshListData()
    {
        Object l_object       = null;
        String l_selectedItem = null;

        i_definedCodes  = ((ChavChannelDbService)i_dbService).getAvailableChannelData();
        i_keyDataModel  = new DefaultComboBoxModel(i_definedCodes);
        i_keyDataComboBox.setModel(i_keyDataModel);
       
        l_object        = i_groupComboBox.getSelectedItem();
        l_selectedItem  = (String)i_keyDataComboBox.getSelectedItem();
    	
        i_definedGroups = ((ChavChannelDbService)i_dbService).getAvailableGroups(l_selectedItem);
        i_groupModel = new DefaultComboBoxModel(i_definedGroups);
        i_groupComboBox.setModel(i_groupModel);
        i_groupComboBox.setSelectedItem(l_object);
        
        populateCodesTable();
    }

    /**
     * Resets the selected value for the combo box holding the key data.
     * Typically called when user does not confirm new selection via key data combo.
     */
    protected void resetSelectionForKeyCombo()
    {
        i_keyDataComboBox.setSelectedItem(((ChavChannelDbService)i_dbService).getChannelData());
        i_groupModel = new DefaultComboBoxModel(i_definedGroups);
        i_groupComboBox.setModel(i_groupModel);

    }

    /**
     * Validates key screen data and writes it to screen data object.
     * This method is called before checking for existing records with this key.
     * @return True if valid, false otherwise.
     */
    protected boolean validateKeyData()
    {
        String l_code = "";
        
        if (i_codeField.getText().trim().equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        // Wildcard is not allowed as a valid entry.
        else if (i_codeField.getText().trim().indexOf("*") != -1 )
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Wildcard [*] is not a valid value");
            return false;
        }

        l_code = i_codeField.getText();
        ((ChavChannelDbService)i_dbService).setCurrentCode(l_code);
        
        return true;
    }

    /**
     * Validates screen data before update or insert.
     * @return True if valid, false otherwise.
     */
    protected boolean validateScreenData()
    {
    	String l_codeFieldText = i_codeField.getText().trim();
    	
    	// Code field is mandatory
        if (l_codeFieldText.equals(""))
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Code cannot be blank");
            return false;
        }

        // Wildcard is not allowed as a valid entry.
        else if (i_codeField.getText().trim().indexOf("*") != -1)
        {
            i_codeField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, 
                                 "Wildcard [*] is not a valid value");
            return false;
        }
        
        // Description field is mandatory
        if (i_descriptionField.getText().trim().equals(""))
        {
            i_descriptionField.requestFocusInWindow();
            displayMessageDialog(i_contentPane, "Description cannot be blank");
            return false;
        }
                
        return true;
    }
    
    /**
     * Hook method, that will prevent a channel that has been defined as a "master channel" from being
     * removed before its "children" are removed.
     * @return Returns false if the channel can't be deleted.
     */
    protected boolean validateDeletion()
    {
    	String l_codeFieldText = i_codeField.getText().trim();
    	String l_tmp           = null;
    	
		for ( int i = 2; i < i_definedCodes.size(); i++ )
		{
			l_tmp = ((BoiChavChannelData)(i_definedCodes.elementAt(i))).getInternalChannelData().getChannelGroup();
			if ( (l_codeFieldText.equals(l_tmp)) )
			{
				// The row that is to be deleted - is used as an master channel for another channel
	      		displayMessageDialog(i_contentPane,
                                     "Cannot be deleted, it is used as a 'master channel'");
	      	    return false;
			}
		}
		return true;
    }
    
    /**
     * Writes current record fields to screen data object. Called before update or insert.
     */  
    protected void writeCurrentRecord()
    {
        ChavChannelData l_tubsChannelData;
        String l_group = (String)i_groupComboBox.getSelectedItem();

        l_tubsChannelData = new ChavChannelData(i_codeField.getText(),
        		                        i_descriptionField.getText(),
        		                        l_group,
                                                null,
                                                null,
                                                false);

        ((ChavChannelDbService)i_dbService).setChannelData(new BoiChavChannelData(l_tubsChannelData));
    }

    /**
     * Writes key screen data to screen data object for use in record selection or deletion.
     */  
    protected void writeKeyData()
    {
        BoiChavChannelData l_selectedItem = null;
        String             l_code         = null;
        
        l_selectedItem = (BoiChavChannelData)i_keyDataComboBox.getSelectedItem();
        l_code         = l_selectedItem.getInternalChannelData().getChannelId();
        ((ChavChannelDbService)i_dbService).setCurrentCode(l_code);         
    }
    
    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------      
    
    /**
     * Creates the Details Panel.
     */    
    private void createDetailsPanel()
    {
        JLabel l_definedCodesLabel = null;
        JLabel l_codeLabel         = null;
        JLabel l_descriptionLabel  = null;
        JLabel l_groupLabel        = null;
        
        i_detailsPanel = WidgetFactory.createPanel("Details", 100, 48, 0, 0);
        
        l_definedCodesLabel = WidgetFactory.createLabel("Defined Codes:");
        
        l_codeLabel = WidgetFactory.createLabel("Code:");
        i_codeField = WidgetFactory.createTextField(10);
        addValueChangedListener(i_codeField);
        
        l_descriptionLabel = WidgetFactory.createLabel("Description:");
        i_descriptionField = WidgetFactory.createTextField(30);
        addValueChangedListener(i_descriptionField);
        
        l_groupLabel    = WidgetFactory.createLabel("Group:");
        i_definedGroups = ((ChavChannelDbService)i_dbService).getAvailableChannelData();
        i_groupModel    = new DefaultComboBoxModel(i_definedGroups);
        i_groupComboBox = WidgetFactory.createComboBox(i_groupModel);
        addValueChangedListener(i_groupComboBox);
        
        
        i_detailsPanel.add(l_definedCodesLabel, "definedCodesLabel,1,1,15,4");
        i_detailsPanel.add(i_keyDataComboBox,   "definedCodesBox,17,1,45,4");
        i_detailsPanel.add(l_codeLabel,         "codeLabel,1,6,15,4");
        i_detailsPanel.add(i_codeField,         "codeField,17,6,15,4");
        i_detailsPanel.add(l_descriptionLabel,  "descriptionLabel,1,11,15,4");
        i_detailsPanel.add(i_descriptionField,  "descriptionField,17,11,30,4");
        i_detailsPanel.add(l_groupLabel,        "groupLabel,1,16,15,4");
        i_detailsPanel.add(i_groupComboBox,     "groupComboBox,17,16,45,4");

        i_detailsPanel.add(i_updateButton,      "updateButton,1,42,15,5");
        i_detailsPanel.add(i_deleteButton,      "deleteButton,17,42,15,5");
        i_detailsPanel.add(i_resetButton,       "resetButton,33,42,15,5");
    }

    /**
     * Creates the Defined Codes Panel.
     */    
    private void createDefinedCodesPanel()
    {
        JScrollPane l_scrollPane = null;
        
        i_definedCodesPanel = WidgetFactory.createPanel("Defined Codes", 100, 45, 0, 0);
        
        i_stringTableModel  = new StringTableModel(i_data, i_columnNames);
        i_table             = WidgetFactory.createTable(150, 200, i_stringTableModel, this);
        
        l_scrollPane        = new JScrollPane(i_table,
                                              JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                              JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        i_definedCodesPanel.add(l_scrollPane,
                                "i_table,1,1,100,45");                                
        
        i_verticalScrollBar = l_scrollPane.getVerticalScrollBar();
    }
    
    /**
     * Populates the Codes Table.
     */    
    private void populateCodesTable()
    {
        ChavChannelData l_chavChannelData = null;
        int             l_numRecords      = 0;
        int             l_arraySize       = 0;
        
        l_numRecords = i_definedCodes.size();
        l_arraySize  = ((l_numRecords > 14) ? l_numRecords : 15);
        i_data       = new String[l_arraySize][3];

        // Start with third element of i_definedCodes as we dont want "" or "NEW RECORD" elements.
        for (int i=2, j=0; i < i_definedCodes.size(); i++, j++)
        {
            l_chavChannelData = ((BoiChavChannelData)i_definedCodes.elementAt(i)).getInternalChannelData();
            i_data[j][0]      = l_chavChannelData.getChannelId();
            i_data[j][1]      = l_chavChannelData.getDesc();
            i_data[j][2]      = l_chavChannelData.getChannelGroup();
        }

        i_stringTableModel.setData(i_data);
    }

}
