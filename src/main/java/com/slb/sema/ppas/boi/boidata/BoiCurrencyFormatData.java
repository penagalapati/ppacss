////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BoiCurrencyFormatData.java
//      DATE            :       15-Sep-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#591/4148
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Wrapper for CufmCurrencyFormatsData class 
//                              within BOI.  
//                              Supports toString and equals methods for use 
//                              within combo boxes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/02/06 | M Erskine  | Override hashCode()             | PpacLon#1912/7969
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.boidata;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;

/** Wrapper for currency format data class within BOI. */
public class BoiCurrencyFormatData extends DataObject
{
    /** Basic currency format data object */
    private CufmCurrencyFormatsData i_currencyFormatData;
    
    /**
     * Simple constructor.
     * @param p_currencyFormatData Currency format data object to wrapper.
     */
    public BoiCurrencyFormatData(CufmCurrencyFormatsData p_currencyFormatData)
    {
        i_currencyFormatData = p_currencyFormatData;
    }

    /**
     * Return wrappered currency format data object.
     * @return Wrappered currency format data object.
     */
    public CufmCurrencyFormatsData getInternalCurrencyFormatData()
    {
        return i_currencyFormatData;
    }

    /**
     * Compares two currency format data objects and returns true if they equate.
     * @param p_currencyFormatData Currency format data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_currencyFormatData)
    {
        boolean l_return = false;
        
        if ( p_currencyFormatData != null &&
             p_currencyFormatData instanceof BoiCurrencyFormatData &&
             i_currencyFormatData.getCurrencyCode().equals( 
                 ((BoiCurrencyFormatData)p_currencyFormatData).getInternalCurrencyFormatData().getCurrencyCode()) )
        {
            l_return = true;
        }
        return l_return;
    }

    /**
     * Returns currency format data object as a String for display in BOI.
     * @return Currency format data object as a string.
     */
    public String toString()
    {
        return i_currencyFormatData.getCurrencyCode();
    }
    
    /**
     * Returns a code for use by hash Collections.
     * @return A value used by the collection to store and retrieve this object. 
     */
    public int hashCode()
    {
        return i_currencyFormatData.getCurrencyCode().length();
    }
}