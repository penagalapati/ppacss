////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       OperatorsPaneUT.java
//      DATE            :       14-Jan-2005
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PpacLon#1148/5488
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       JUnit test class for OperatorsPane.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
//----------+------------+---------------------------------+--------------------
// 02/10/06 | Chris      | Add Confirm Password test       | PpacLon#1917/9429
//          | Harrison   |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JList;
import javax.swing.JScrollPane;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;
import com.slb.sema.ppas.swing.components.GuiButton;
import com.slb.sema.ppas.swing.components.ValidatedJPasswordField;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;

/** JUnit test class for OperatorsPane. */
public class OperatorsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------
    
    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "OperatorsPaneUT";
    
    /** Constant for operator id. */
    private static final String C_TEST_OPID = "SUPER99";
    
    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------
    
    /** Operators screen. */
    private OperatorsPane i_operatorsPane;
    
    /** Text field to hold the operator id. */
    private ValidatedJTextField i_operatorId;
    
    /** Text field to hold the operator name. */
    private ValidatedJTextField i_operatorName;
    
    /** Text field to hold the new password. */
    private ValidatedJPasswordField i_newPassword;
    
    /** Text field to hold the confirmed password. */
    private ValidatedJPasswordField i_confirmPassword;
    
    /** Text field to hold the password expiry date. */
    private JFormattedTextField i_passwordExpiryDate;
    
    /** Text field to hold the maximum single adjustment amount. */
    private JFormattedTextField i_maxAdjustAmount;
    
    /** Text field to hold the daily adjustment limit. */
    private JFormattedTextField i_dailyAdjustLimit;
    
    /** Check box to hold GUI access privilege */
    private JCheckBox i_guiPrivilege;
    
    /** Check box to hold PAMI access privilege */
    private JCheckBox i_pamiPrivilege;
    
    /** Check box to hold POSI access privilege */
    private JCheckBox i_posiPrivilege;
    
    /** Table of markets that are available for selection */
    private JScrollPane i_availableMarketsScrollPane;

    /** JList of available markets */
    private JList i_availableMarketsList;
    
    /** Button to add market to operator's selected list */
    private GuiButton i_addMarketButton;
    
    /** Key data combo box. */
    protected JComboBox i_keyDataCombo;
    
    /** Button for updates and inserts. */
    protected JButton i_updateButton;
    
    /** Delete button. */
    protected JButton i_deleteButton;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public OperatorsPaneUT(String p_title)
    {
        super(p_title);
        
        i_operatorsPane = new OperatorsPane(c_context);
        super.init(i_operatorsPane);
        
        i_operatorsPane.resetScreenUponEntry();
        
        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "operatorBox");
        i_operatorId = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "operatorId");
        i_operatorName = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "operatorName");
        i_newPassword = (ValidatedJPasswordField)SwingTestCaseTT.getChildNamed(i_contentPane, "newPassword");
        i_confirmPassword = (ValidatedJPasswordField) SwingTestCaseTT.getChildNamed(i_contentPane, "confirmPassword");
        i_passwordExpiryDate = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                  "passwordExpiryDate");
        i_maxAdjustAmount = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                               "maxAdjustAmount");
        i_dailyAdjustLimit = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                "dailyAdjustLimit");
        i_guiPrivilege = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, "guiPrivilege");
        i_pamiPrivilege = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, "pamiPrivilege");
        i_posiPrivilege = (JCheckBox)SwingTestCaseTT.getChildNamed(i_contentPane, "posiPrivilege");
        i_availableMarketsScrollPane = (JScrollPane)SwingTestCaseTT.getChildNamed(i_contentPane, 
                                                                                  "availableMarkets");
        i_availableMarketsList = (JList)i_availableMarketsScrollPane.getViewport().getView();
        i_addMarketButton = (GuiButton)SwingTestCaseTT.getChildNamed(i_contentPane, "addMarketButton");
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }
    
    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(OperatorsPaneUT.class);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    
    /** 
     * @ut.when a user attempts to insert, select, update, and delete a record 
     *     through the Operators screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        int l_availableMarketsCount = 0;
        
        beginOfTest("OperatorsPane test");
        
        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_operatorsPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_operatorId, C_TEST_OPID);
        SwingTestCaseTT.setTextComponentValue(i_operatorName, "Operator name");
        SwingTestCaseTT.setTextComponentValue(i_newPassword, "Password");
        SwingTestCaseTT.setTextComponentValue(i_confirmPassword, "Password");
        SwingTestCaseTT.setTextComponentValue(i_passwordExpiryDate, "21/8/2099");
        SwingTestCaseTT.setTextComponentValue(i_maxAdjustAmount, "100.00");
        SwingTestCaseTT.setTextComponentValue(i_dailyAdjustLimit, "1000.00");
        SwingTestCaseTT.setCheckBoxValue(i_guiPrivilege, true);
        SwingTestCaseTT.setCheckBoxValue(i_pamiPrivilege, false);
        SwingTestCaseTT.setCheckBoxValue(i_posiPrivilege, true);
        
        l_availableMarketsCount = i_availableMarketsList.getComponentCount();
        assertTrue("No markets defined", l_availableMarketsCount > 0);
        i_availableMarketsList.setSelectedIndex(0);
        i_addMarketButton.doClick();
        
        doInsert(i_operatorsPane, i_updateButton);
        
        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setKeyComboSelectedItem(i_operatorsPane, i_keyDataCombo, C_TEST_OPID);
        
        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************
        
        SwingTestCaseTT.setTextComponentValue(i_operatorName, "New oper name");
        
        if (l_availableMarketsCount > 1)
        {
            i_availableMarketsList.setSelectedIndex(1);
        }
        i_addMarketButton.doClick();
        
        doUpdate(i_operatorsPane, i_updateButton);
        
        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************
        
        doDelete(i_operatorsPane, i_deleteButton);
        
        endOfTest();
    }
    
    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------
    
    /**
     * This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        deleteOperatorRecord(C_TEST_OPID);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteOperatorRecord(C_TEST_OPID);
        say(":::End Of Test:::");
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------
    
    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteOperatorRecord = "deleteOperatorRecord";
    
    /**
     * Removes rows from BICSYS_USERFILE and BICSYS_USERSRVAS for given operator.
     * @param p_opid Operator id.
     */
    private void deleteOperatorRecord(String p_opid)
    {
        String    l_sql;
        SqlString l_sqlString;
        
        l_sql = "DELETE from bicsys_userfile WHERE opid = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_opid);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteOperatorRecord);
        
        l_sql = "DELETE from bicsys_usersrvas WHERE opid = {0}";
        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setStringParam(0, p_opid);
        
        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteOperatorRecord);
    }
}
