////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :     9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :     MarketsPaneUT.java
//    DATE            :     30-Sep-2005
//    AUTHOR          :     Michael Erskine
//    REFERENCE       :     PpacLon#1709/7204
//
//    COPYRIGHT       :     WM-data 2005
//
//    DESCRIPTION     :     JUnit test for MarketsPane class.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//     DATE | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/02/06 | M Erskine  | Remove dummy row at end of test | PpacLon#1912/7982
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.boi.gui;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.boi.boidata.BoiMarket;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.swing.components.ValidatedJTextField;
import com.slb.sema.ppas.swing.test.SwingTestCaseTT;

/** JUnit test class for MarketsPane. */
public class MarketsPaneUT extends BoiTestCaseTT
{
    //------------------------------------------------------------------------
    // Constants
    //------------------------------------------------------------------------

    /** Class name used in calls to middleware. */
    private static final String C_CLASS_NAME = "MarketsPaneUT";
    
    /** Service Area to use for test. */
    private static final int C_SRVA = 3;
    
    /** Service Location to use for test. */
    private static final int C_SLOC = 4;

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Markets screen. */
    private MarketsPane         i_marketsPane;

    /** Markets area field. */
    private JFormattedTextField i_areaField;
    
    /** Markets location field. */
    private JFormattedTextField i_locationField;

    /** Markets description field. */
    private ValidatedJTextField i_descriptionField;
    
    /** Markets Pane number format field. */
    private ValidatedJTextField i_numberFormatField;
    
    /** Markets Pane quarantine period field. */
    private JFormattedTextField i_quarantinePeriodField;
    
    /** Markets Pane default Home Region Combo Box. */
    private JComboBox i_defaultHomeRegionComboBox; //TODO: Set this field to some value other than ""?

    /** Key data combo box. */
    protected JComboBox         i_keyDataCombo;

    /** Button for updates and inserts. */
    protected JButton           i_updateButton;

    /** Delete button. */
    protected JButton           i_deleteButton;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Required constructor for JUnit testcase. Any subclass of TestCase must implement a constructor that
     * takes a test case name as its argument
     * @param p_title The testcase name.
     */
    public MarketsPaneUT(String p_title)
    {
        super(p_title);

        i_marketsPane = new MarketsPane(c_context);
        super.init(i_marketsPane);

        i_marketsPane.resetScreenUponEntry();

        i_keyDataCombo = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "definedCodesBox");
        i_areaField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "areaField");
        i_locationField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "locationField");
        i_descriptionField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "descriptionField");
        i_numberFormatField = (ValidatedJTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "numberFormatField");
        i_quarantinePeriodField = (JFormattedTextField)SwingTestCaseTT.getChildNamed(i_contentPane, "quarantinePeriodField");
        i_defaultHomeRegionComboBox = (JComboBox)SwingTestCaseTT.getChildNamed(i_contentPane, "defaultHomeRegionComboBox");
        
        i_updateButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "updateButton");
        i_deleteButton = (JButton)SwingTestCaseTT.getChildNamed(i_contentPane, "deleteButton");
    }

    //------------------------------------------------------------------------
    // Public class methods
    //------------------------------------------------------------------------

    /**
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes.
     * @return an instance of <code>junit.framework.Test</code>
     */
    public static Test suite()
    {
        return new TestSuite(MarketsPaneUT.class);
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /**
     * @ut.when a user attempts to insert, select, update, delete, and make a record available again through
     * the Markets screen.
     * @ut.then the operations are successfully performed.
     * @ut.attributes +f
     */
    public void testDatabaseUpdates()
    {
        beginOfTest("MarketsPane test");

        BoiMarket l_boiMarket;

        // *********************************************************************
        say("Inserting new record.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_marketsPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_areaField, String.valueOf(C_SRVA));
        SwingTestCaseTT.setTextComponentValue(i_locationField, String.valueOf(C_SLOC));
        SwingTestCaseTT.setTextComponentValue(i_descriptionField, C_DESCRIPTION);
        SwingTestCaseTT.setTextComponentValue(i_numberFormatField, "cn");
        SwingTestCaseTT.setTextComponentValue(i_quarantinePeriodField, "9");
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }
        
        //TODO: Method is private on SwingTestCaseTT and need it here (see above).
        //SwingTestCaseTT.setComboSelectedItem
        doInsert(i_marketsPane, i_updateButton);

        // *********************************************************************
        say("Selecting newly inserted record.");
        // *********************************************************************

        l_boiMarket = createMarketsData(3, 4);
        SwingTestCaseTT.setKeyComboSelectedItem(i_marketsPane, i_keyDataCombo, l_boiMarket);
        
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            // Do nothing
        }

        // *********************************************************************
        say("Updating newly inserted record.");
        // *********************************************************************

        SwingTestCaseTT.setTextComponentValue(i_quarantinePeriodField, "3");
        doUpdate(i_marketsPane, i_updateButton);

        // *********************************************************************
        say("Deleting newly inserted record.");
        // *********************************************************************

        doDelete(i_marketsPane, i_deleteButton);

        // *********************************************************************
        say("Making deleted record available again.");
        // *********************************************************************

        SwingTestCaseTT.setKeyComboSelectedItem(i_marketsPane, i_keyDataCombo, C_NEW_RECORD);
        SwingTestCaseTT.setTextComponentValue(i_areaField, String.valueOf(C_SRVA));
        SwingTestCaseTT.setTextComponentValue(i_locationField, String.valueOf(C_SLOC));
        doMakeAvailable(i_marketsPane, i_updateButton);

        endOfTest();
    }

    //------------------------------------------------------------------------
    // Protected methods
    //------------------------------------------------------------------------

    /**
     * This method is used to setup anything required by each test.
     */
    protected void setUp()
    {
        deleteSrvaRecord(3,4);
    }
    
    /**
     * Performs standard clean up activities at the end of a test.
     */
    protected void tearDown()
    {
        deleteSrvaRecord(3,4);
        say(":::End Of Test:::");
    }

    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /**
     * Creates a BoiAccountGroupsData object with the supplied data.
     * @param p_srva Account group id.
     * @param p_sloc Account group description.
     * @return BoiAccountGroupsData object created from the supplied data.
     */
    private BoiMarket createMarketsData(int p_srva, int p_sloc)
    {
        Market l_market = new Market(p_srva, p_sloc, C_DESCRIPTION);
        
        return new BoiMarket(l_market);
    }

    /** Method name used in calls to middleware. */
    private static final String C_METHOD_deleteSrvaRecord = "deleteSrvaRecord";

    /**
     * Removes a row from SRVA_MSTR.
     * @param p_srva Service area
     * @param p_sloc Service location
     */
    private void deleteSrvaRecord(int p_srva, int p_sloc)
    {
        String l_sql;
        SqlString l_sqlString;

        l_sql = new String("DELETE from srva_mstr " + "WHERE srva = {0} and sloc = {1}");

        l_sqlString = new SqlString(100, 1, l_sql);
        l_sqlString.setIntParam(0, p_srva);
        l_sqlString.setIntParam(1, p_sloc);

        sqlConfigUpdate(l_sqlString, C_CLASS_NAME, C_METHOD_deleteSrvaRecord);
    }
    
    
}