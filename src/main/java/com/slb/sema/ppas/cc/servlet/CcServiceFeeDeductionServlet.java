////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcServiceFeeDeductionServlet.java
//      DATE            :       06-Jul-2005
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_GEN_CA_48
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from
//                              Service Fee Deduction 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcServiceFeeDeductionDetailScreenData;
import com.slb.sema.ppas.cc.screendata.CcServiceFeeDeductionScreenData;
import com.slb.sema.ppas.cc.service.CcServiceFeeDeductionService;
import com.slb.sema.ppas.cc.service.CcServiceFeeSetResponse;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/** Servlet to handle requests from Service Fee Deduction. */
public class CcServiceFeeDeductionServlet extends GuiServlet
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcServiceFeeDeductionServlet";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The CC Service Fee Deduction  Service to be used by this servlet. */
    private CcServiceFeeDeductionService i_ccServiceFeeDeductionService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructor - creates a Service Fee Deduction Servlet instance to handle
     * requests from the Service Fee Deduction GUI Screen.
     */
    public CcServiceFeeDeductionServlet()
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /**
     * Service method to control requests and responses to the JSP screen for
     * Service Fee Deduction.
     * @param p_request     Message request.
     * @param p_response    Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest  The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(HttpServletRequest  p_request,
                               HttpServletResponse p_response,
                               HttpSession         p_httpSession,
                               GuiRequest          p_guiRequest)
    {
        GuiResponse                 l_guiResponse = null;
        String                      l_command     = null;
        String                      l_forwardUrl  = null;
        
        CcServiceFeeDeductionScreenData l_ccServiceFeeDeductionScreenData = null;
        CcServiceFeeDeductionDetailScreenData l_ccServiceFeeDeductionDetailScreenData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                "Entered " + C_METHOD_doService);
        }
        
        // Construct CcServiceFeeDeductionService if it doesn't already exist
        if (i_ccServiceFeeDeductionService == null)
        {
            i_ccServiceFeeDeductionService = new CcServiceFeeDeductionService( p_guiRequest, 0, i_logger,
                i_guiContext );
        }

        l_ccServiceFeeDeductionScreenData = new CcServiceFeeDeductionScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,         // Mandatory
                                      "",           // Default Value
                                      new String [] {"GET_SCREEN_DATA",
                                                     "GET_SERVICE_FEE_DETAIL"});
            
            // Determine the forwardUrl
            if (l_command.equals("GET_SCREEN_DATA"))
            {
                l_forwardUrl = "/jsp/cc/ccservicefeededuction.jsp";
                l_ccServiceFeeDeductionScreenData.setAdjCodes(getAdjCodesFromCache(p_guiRequest));
                getServiceFeeDeduction(p_guiRequest, l_ccServiceFeeDeductionScreenData);
            }
            else
            {
                l_forwardUrl = "/jsp/cc/ccservicefeedeductiondetail.jsp";
                l_ccServiceFeeDeductionDetailScreenData = getSfdDetail(
                    i_guiContext, p_request, p_guiRequest);
                l_ccServiceFeeDeductionScreenData.setDetailScreenData(
                    l_ccServiceFeeDeductionDetailScreenData);
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_ccServiceFeeDeductionScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            p_request.setAttribute("p_guiScreenData", l_ccServiceFeeDeductionScreenData);
            l_forwardUrl = "/jsp/cc/ccerror.jsp";
        }
        
        if (l_ccServiceFeeDeductionScreenData.hasRetrievalError())
        {
            // An error occured retrieving the data necessary to populate
            // the appropriate screen data object.
            // Forward to the GuiError.jsp instead.
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

            p_request.setAttribute ("p_guiScreenData",
                                    l_ccServiceFeeDeductionScreenData);

        }
        else
        {
            // CcServiceFeeDeductionScreenData object has the necessary data
            // to populate the ServiceFeeDeduction screen.

            p_request.setAttribute("p_guiScreenData",
                                   l_ccServiceFeeDeductionScreenData);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10199,
                this,
                "Leaving " + C_METHOD_doService);
        }

        return (l_forwardUrl);
    }

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getServiceFeeDeduction = "getServiceFeeDeduction";
    /**
     * Private method to retrieve a subscribers segmentation options.
     * @param p_guiRequest The original request.
     * @param p_screenData The response screen data.
     */
    private void getServiceFeeDeduction(GuiRequest                         p_guiRequest,
                                 CcServiceFeeDeductionScreenData p_screenData)
    {
        CcServiceFeeSetResponse l_serviceFeeSetResponse = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10200,
                this,
                "Entered " + C_METHOD_getServiceFeeDeduction);
        }
        
        l_serviceFeeSetResponse = i_ccServiceFeeDeductionService.getServiceFeeDeductions(p_guiRequest,
                                                                                         i_timeout);
        
        p_screenData.addRetrievalResponse(p_guiRequest, l_serviceFeeSetResponse);
        
        if (l_serviceFeeSetResponse.isSuccess())
        {
            p_screenData.setServiceFeeDataSet( l_serviceFeeSetResponse.getDataSet() );
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10299,
                this,
                "Leaving " + C_METHOD_getServiceFeeDeduction);
        }

    } // end of getServiceFeeDeduction
    

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getSfdDetail = "getSfdDetail";

    /**
     * Retrieves the Service Fee Deduction Details from the Http request for the
     * Service Fee Deduction Detail popup screen.
     * @param p_guiContext  Context containing properties.
     * @param p_request     Message request.
     * @param p_guiRequest  The GUI request being processed.
     * @return Set of data for Service Fee Deduction Detail Screen.
     * @throws PpasServletException If an error occurs processing this request.
     */
    private CcServiceFeeDeductionDetailScreenData getSfdDetail(
        GuiContext          p_guiContext,
        HttpServletRequest  p_request,
        GuiRequest          p_guiRequest)
        throws PpasServletException
    {
        CcServiceFeeDeductionDetailScreenData l_ccServiceFeeDeductionDetailScreenData ;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_getSfdDetail);
        }

        String l_custId = getValidParam(p_guiRequest, 0, p_request, "p_custId",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_chargedCustId = getValidParam(p_guiRequest, 0, p_request, "p_chargedCustId",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_serviceFeeType = getValidParam(p_guiRequest, 0, p_request, "p_serviceFeeType",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_dateTime = getValidParam(p_guiRequest, 0, p_request, "p_dateTime",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_deduction = getValidParam(p_guiRequest, 0, p_request, "p_deduction",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_deductionCurr = getValidParam(p_guiRequest, 0, p_request, "p_deductionCurr",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_subscriberAmount = getValidParam(p_guiRequest, 0, p_request, "p_subscriberAmount",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_subscriberAmountCurr = getValidParam(p_guiRequest, 0, p_request, "p_subscriberAmountCurr",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_deductedAmount = getValidParam(p_guiRequest, 0, p_request, "p_deductedAmount",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_deductedAmountCurr = getValidParam(p_guiRequest, 0, p_request, "p_deductedAmountCurr",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_debt = getValidParam(p_guiRequest, 0, p_request, "p_debt",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_debtCurr = getValidParam(p_guiRequest, 0, p_request, "p_debtCurr",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_successCode = getValidParam(p_guiRequest, 0, p_request, "p_successCode",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_successCodeAcronym = getValidParam(p_guiRequest, 0, p_request, "p_successCodeAcronym",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_successCodeDesc = getValidParam(p_guiRequest, 0, p_request, "p_successCodeDesc",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_oldServiceClass = getValidParam(p_guiRequest, 0, p_request, "p_oldServiceClass",
            true, "", PpasServlet.C_TYPE_ANY);

        String l_newServiceClass = getValidParam(p_guiRequest, 0, p_request, "p_newServiceClass",
            false, "", PpasServlet.C_TYPE_ANY);

        String l_oldExpiryDate = getValidParam(p_guiRequest, 0, p_request, "p_oldExpiryDate",
            false, null, PpasServlet.C_TYPE_ANY);

        String l_newExpiryDate = getValidParam(p_guiRequest, 0, p_request, "p_newExpiryDate",
            false, null, PpasServlet.C_TYPE_ANY);

        l_ccServiceFeeDeductionDetailScreenData = new CcServiceFeeDeductionDetailScreenData(
            p_guiContext,
            p_guiRequest,
            l_custId,
            l_chargedCustId,
            l_serviceFeeType,
            l_dateTime,
            l_deduction,
            l_deductionCurr,
            l_subscriberAmount,
            l_subscriberAmountCurr,
            l_deductedAmount,
            l_deductedAmountCurr,
            l_debt,
            l_debtCurr,
            l_successCode,
            l_successCodeAcronym,
            l_successCodeDesc,
            l_oldServiceClass,
            l_newServiceClass,
            l_oldExpiryDate,
            l_newExpiryDate);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                C_CLASS_NAME, 11090, this,
                "Leaving " + C_METHOD_getSfdDetail);
        }

        return l_ccServiceFeeDeductionDetailScreenData;
    } // end of getSfdDetail


    /**
     * Returns the Service Fee Deduction Types from the Business Config cache.
     * @param p_guiRequest The GUI request being processed.
     * @return Set of Adjustment Codes.
     */
    private SrvaAdjCodesDataSet getAdjCodesFromCache(GuiRequest             p_guiRequest)
    {
        return i_configService.getAvailableAdjCodes(
                                        ((GuiSession)p_guiRequest.getSession()).getCsoMarket());
    } // end of getAdjCodesFromCache
} // end public class
