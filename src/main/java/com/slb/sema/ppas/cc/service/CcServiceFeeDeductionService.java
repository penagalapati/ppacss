////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcServiceFeeDeductionService.java
//      DATE            :       08-Jul-2005
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_GEN_CA_48
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       GUI Service Fee Deduction Service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.ServiceFeeData;
import com.slb.sema.ppas.common.dataclass.ServiceFeeDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasServiceFeeService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * Service Fee Deduction Functionality.
 */
public class CcServiceFeeDeductionService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcServiceFeeDeductionService";

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The atomic Service Fee Deduction service. */
    private PpasServiceFeeService i_ppasServiceFeeService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a CcServiceFeeDeductionService object that
     * can then be used to perform Service Fee Deduction services.
     * @param p_guiRequest GuiRequest
     * @param p_flags flags
     * @param p_logger a Logger
     * @param p_guiContext GuiContext containing configuration
     */
    public CcServiceFeeDeductionService( GuiRequest p_guiRequest,
                                      long       p_flags,
                                      Logger     p_logger,
                                      GuiContext p_guiContext )
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        // Create PPAS internal services to be used by this service.
        i_ppasServiceFeeService = new PpasServiceFeeService(p_guiRequest,
                                                       p_logger,
                                                       p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getServiceFeeDeductions = "getServiceFeeDeductions";
    /**
     * Generates a CcServiceFeeSetResponse object containing the
     * Service Fee Deductions for the MSISDN in the request.
     * @param p_guiRequest GuiRequest containing subscriber info
     * @param p_timeoutMillis The max time in milliseconds to wait for system
     *                        resources to perform the service.
     * @return  The subscribers list of Service Fee Deductions
     */
    public CcServiceFeeSetResponse getServiceFeeDeductions(GuiRequest p_guiRequest,
                                                 long       p_timeoutMillis)
    {
        CcServiceFeeSetResponse       l_ccServiceFeeSetResponse;
        ServiceFeeDataSet l_serviceFeeDataSet;
        GuiResponse                l_guiResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10100, this,
                           "Entered " + C_METHOD_getServiceFeeDeductions);
        }

        try
        {
            // Call the atomic service...
            l_serviceFeeDataSet = i_ppasServiceFeeService.getServiceFeeDeductionHistory(p_guiRequest, p_timeoutMillis);

            // Generate success Response
            l_ccServiceFeeSetResponse = new CcServiceFeeSetResponse(p_guiRequest,
                p_guiRequest.getGuiSession().getSelectedLocale(), GuiResponse.C_KEY_SERVICE_SUCCESS,
                (Object[])null, GuiResponse.C_SEVERITY_SUCCESS, l_serviceFeeDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 10210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_getServiceFeeDeductions, l_pSE);

            l_ccServiceFeeSetResponse = new CcServiceFeeSetResponse(
                                 p_guiRequest,
                                 p_guiRequest.getGuiSession().getSelectedLocale(),
                                 l_guiResponse.getKey(),
                                 l_guiResponse.getParams(),
                                 GuiResponse.C_SEVERITY_FAILURE,
                                 null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 10190, this,
                           "Leaving " + C_METHOD_getServiceFeeDeductions);
        }

        return(l_ccServiceFeeSetResponse);

    } // end of method 'getServiceFeeDeductions'

}
