////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcCallHistoryScreenData.Java
//DATE            :       08-Mar-2006
//AUTHOR          :       Kanta Goswami
//REFERENCE       :       PpacLon#2026/8071
//                        PRD_ASCS00_GEN_CA_066_D1
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Gui Screen Data object for the 
//                        Call History screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//27/11/06 | Chris      | Added methods isSubordinate() & | PpacLon#2749/10512
//         | Harrison   | setIsSubordinate()              |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseDataSet;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.CallHistoryData;
import com.slb.sema.ppas.common.dataclass.CallHistoryDataSet;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
 
/**
 * Gui Screen Data object for the Voucher screen.
 */
public class CcCallHistoryScreenData extends GuiScreenData
{
//  --------------------------------------------------------------------------
    // Private class constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCallHistoryScreenData";
    
    /** Define the code to be used to select service class misc code data. */
    public static final String C_SERVICE_CLASS_MISC_TYPE         = "CLS";
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** The start date of the call history viewing that this object defines. */
    private PpasDate i_startDate;

    /** The start date of the call history viewing that this object defines. */
    private PpasDate i_endDate;
    
    /** The other party number against which the retrieval needs to be done. */
    private String i_otherPartyNo;
    
    /** The default number of records to display on the screen. */
    private Integer i_defaultMaxRows;
    
    /** The requested number of records to be displayed on the screen. */
    private Integer i_requestedRows;
    
    /** The maximum number of rows that can be retrieved from the db. */
    private Integer i_recordRetrievalLimit;
    
    /** The field on which the data displayed should be sorted. */
    private String i_sortField;
    
    /** The field that specifies if the sorting should be ascending or descending. */
    private boolean i_descendSort;
    
    /** The field that specifies the level of the subscriber. */
    private boolean i_subLevel;

    /** Indicates whether the subscriber in the request for this screen
     * is a subordinate or not.
     */
    private boolean i_isSubordinate;

    //Variables for the History Data in the DataSet
    /** The call history to be displayed on the history table. */
    private CallHistoryDataSet i_callHistDataSet;
    
    
    //Variables for Business Config
    
    /** The TrafTrafficCaseDataSet to be set on the screen data object*/
    private TrafTrafficCaseDataSet         i_trafficCaseDS            = null;
    
    /** The TeleTeleserviceDataSet to be set on the screen data object*/
    private TeleTeleserviceDataSet         i_teleServiceDS            = null;
    
    /** The FachDataSet to be set on the screen data object*/
    private FachFafChargingIndDataSet      i_fachDS                   = null;
    
    /** The MiscCodeDataSet to be set on the screen data object*/
    private MiscCodeDataSet                i_servClassesDS            = null;
    
    /** The AcgrAccountGroupDataSet to be set on the screen data object*/
    private AcgrAccountGroupDataSet        i_accGrpDS                 = null;
    
    /** The CochCommunityChargingDataSet to be set on the screen data object*/
    private CochCommunityChargingDataSet   i_commIdDS                 = null;
    
    /** The DedaDedicatedAccountsDataSet to be set on the screen data object*/
    private DedaDedicatedAccountsDataSet   i_dedAccDS                 = null;
    
    

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcCallHistoryScreenData(GuiContext p_guiContext, GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 95000, this,
                "Constructing " + C_CLASS_NAME );
        }
        i_startDate = new PpasDate ("");
        i_endDate = new PpasDate ("");
        i_otherPartyNo = null;
        i_defaultMaxRows = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 95090, this,
                "Constructed " + C_CLASS_NAME );
        }
    }
    /**
     * @return Returns the bNumber.
     */
    public String getOtherPartyNo()
    {
        return i_otherPartyNo;
    }
    /**
     * @param p_number The bNumber to set.
     */
    public void setOtherPartyNo(String p_number)
    {
        i_otherPartyNo = p_number;
    }
    /**
     * @return Returns the endDate.
     */
    public PpasDate getEndDate()
    {
        return i_endDate;
    }
    /**
     * @param p_endDate The endDate to set.
     */
    public void setEndDate(PpasDate p_endDate)
    {
        i_endDate = p_endDate;
    }
    /**
     * @return Returns the default number of rows to be returned.
     */
    public Integer getDefaultMaxRows()
    {
        return i_defaultMaxRows;
    }
    /**
     * @param p_defaultMaxRows Set the default number of rows to be returned.
     * This fixed value comes from configuration, not screen input. 
     */
    public void setDefaultMaxRows(Integer p_defaultMaxRows)
    {
        i_defaultMaxRows = p_defaultMaxRows;
    }

    /**
     * @return Returns the number of rows requested by the user.
     */
    public Integer getRequestedRows()
    {
        return i_requestedRows;
    }

    /**
     * @param p_requestedRows Sets the number of rows requested by the user.
     */
    public void setRequestedRows(Integer p_requestedRows)
    {
        i_requestedRows = p_requestedRows;
    }
    
    /**
     * @return Returns the startDate.
     */
    public PpasDate getStartDate()
    {
        return i_startDate;
    }
    /**
     * @param p_startDate The startDate to set.
     */
    public void setStartDate(PpasDate p_startDate)
    {
        i_startDate = p_startDate;
    }

    /**
     * @return Returns the callHistDataSet.
     */
    public CallHistoryDataSet getCallHistDataSet()
    {
        return i_callHistDataSet;
    }
    
    /**
     * Returns an array of CallHistoryData objects.
     * 
     * @return Array of call history details.
     */
    public CallHistoryData[] getCallHistoryArray()
    {
        return i_callHistDataSet.asArray();
    }
    /**
     * @param p_callHistDataSet The callHistDataSet to set.
     */
    public void setCallHistDataSet(CallHistoryDataSet p_callHistDataSet)
    {
        i_callHistDataSet = p_callHistDataSet;
    }
    /**
     * @return Returns the descendSort.
     */
    public boolean isDescendSort()
    {
        return i_descendSort;
    }
    /**
     * @param p_descendSort The descendSort to set.
     */
    public void setDescendSort(boolean p_descendSort)
    {
        i_descendSort = p_descendSort;
    }
    /**
     * @return Returns the sortField.
     */
    public String getSortField()
    {
        return i_sortField;
    }
    /**
     * @param p_sortField The sortField to set.
     */
    public void setSortField(String p_sortField)
    {
        i_sortField = p_sortField;
    }
    /**
     * @return Returns the subLevel.
     */
    public boolean isSubLevel()
    {
        return i_subLevel;
    }
    /**
     * @param p_subLevel The subLevel to set.
     */
    public void setSubLevel(boolean p_subLevel)
    {
        i_subLevel = p_subLevel;
    }
    /**
     * @param p_accGrpDS The accGrpDS to set.
     */
    public void setAccGrpDS(AcgrAccountGroupDataSet p_accGrpDS)
    {
        i_accGrpDS = p_accGrpDS;
    }
    /**
     * @param p_accGrp The community id for which the desc should be returned.
     * @return Returns the Traffic Case Short Desc.
     */
    public String getaccGrpDesc(AccountGroupId p_accGrp)
    {
        String l_accGrpDesc = "";
        if(i_accGrpDS.getRecord(p_accGrp) != null )
        {
            l_accGrpDesc = i_accGrpDS.getRecord(p_accGrp).getAccountDescription();
        }       
        return l_accGrpDesc;  
    }
    /**
     * @param p_commIdDS The commIdDS to set.
     */
    public void setCommIdDS(CochCommunityChargingDataSet p_commIdDS)
    {
        i_commIdDS = p_commIdDS;
    }    
    /**
     * @param p_commId The community id for which the desc should be returned.
     * @return Returns the Traffic Case Short Desc.
     */
    public String getCommunityDesc(Integer p_commId)
    {
        String l_commDesc = "";
        if(i_commIdDS.getCommunityChargingData(p_commId.intValue()) != null )
        {
            l_commDesc = i_commIdDS.getCommunityChargingData(p_commId.intValue()).getCommunityChgDesc();
        }       
        return l_commDesc;  
    }
    /**
     * @param p_servClassesDS The servClassesDS to set.
     */
    public void setServClassesDS(MiscCodeDataSet p_servClassesDS)
    {
        i_servClassesDS = p_servClassesDS;
    }
    /**
     * @param p_srvId The p_srvId for which the desc should be returned.
     * @return Returns the Service Class Description.
     */
    public String getServiceClassDesc(int p_srvId)
    {
       String l_srvDesc = "";
       
       if(i_servClassesDS.getDataFromKey(C_SERVICE_CLASS_MISC_TYPE,
                                                  Integer.toString(p_srvId)) != null)
       {
           l_srvDesc = i_servClassesDS.getDataFromKey(C_SERVICE_CLASS_MISC_TYPE,
                                                  Integer.toString(p_srvId)).getDescription();
       }
           
       return l_srvDesc;
    }    
    /**
     * @param p_teleServiceDS The teleServiceDS to set.
     */
    public void setTeleServiceDS(TeleTeleserviceDataSet p_teleServiceDS)
    {
        i_teleServiceDS = p_teleServiceDS;
    }
    /**
     * @param p_teleSrvId The p_srvId for which the desc should be returned.
     * @return Returns the TeleService Description.
     */
    public String getTeleServiceDesc(Integer p_teleSrvId)
    {
       String l_teleSrvDesc = "";       
       
       if(i_teleServiceDS.getAvailableRecord(p_teleSrvId.intValue()) != null)
       {
           l_teleSrvDesc = i_teleServiceDS.getAvailableRecord(p_teleSrvId.intValue()).getTeleserviceDescription();
       }
       
       return l_teleSrvDesc;
    }    
    /**
     * @param p_trafficCaseDS The trafficCaseDS to set.
     */
    public void setTrafficCaseDS(TrafTrafficCaseDataSet p_trafficCaseDS)
    {
        i_trafficCaseDS = p_trafficCaseDS;
    }
    
    /**
     * @param p_trafficCase The trafficCase for which the short desc should be returned.
     * @return Returns the Traffic Case Short Desc.
     */
    public String getTrafficCaseShortDesc(Integer p_trafficCase)
    {
        String l_shortDesc = "";
        if(i_trafficCaseDS.getAvailableRecord(p_trafficCase.intValue()) != null)
        {
            l_shortDesc = i_trafficCaseDS.getAvailableRecord(p_trafficCase.intValue())
                          .getTrafficShortDescription();  
        }       
        return l_shortDesc;
    }
   
    /**
     * @param p_trafficCase The trafficCase for which the long desc should be returned.
     * @return Returns the Traffic Case Long Desc.
     */
    public String getTrafficCaseLongDesc(Integer p_trafficCase)
    {
       String l_longDesc = "";
       if(i_trafficCaseDS.getAvailableRecord(p_trafficCase.intValue()) != null)
       {
           l_longDesc = i_trafficCaseDS.getAvailableRecord(p_trafficCase.intValue())
                        .getTrafficDescription();
       }       
       return l_longDesc;
    }    
    /**
     * @param p_accDS The l_dedAccDS to set.
     */
    public void setDedAccDS(DedaDedicatedAccountsDataSet p_accDS)
    {
        i_dedAccDS = p_accDS;
    }
    
    /**
     * @param p_dedAcc The dedAcc for which the desc should be returned.
     * @return Returns the Dedicated Accounts Description.
     */
    public String getDedAccDesc(Integer p_dedAcc)
    {
        String l_dedAccDesc= "";
        
        if (p_dedAcc == null)
        {
            // TODO Needs to be handled better
            System.out.println("Error: Dedicated account is null");
        }
        else if (i_dedAccDS == null)
        {
            // TODO Needs to be handled better
            System.out.println("Error: Dedicated account Description object is null");
        }
        else
        {
            if( i_dedAccDS.getDescription(p_dedAcc.intValue())!= null)
            {
                l_dedAccDesc = i_dedAccDS.getDescription(p_dedAcc.intValue());
            }     
        }
            
        return l_dedAccDesc;
    }    
    /**
     * @param p_fachDS The fachDS to set.
     */
    public void setFachDS(FachFafChargingIndDataSet p_fachDS)
    {
        i_fachDS = p_fachDS;
    }
    /**
     * @param p_fachId The p_fachId for which the desc should be returned.
     * @return Returns the Family and Friends Indicator.
     */
    public String getFachDesc(Integer p_fachId)
    {
       String l_fachDesc = "";       
       
       if(i_fachDS.getRecord(p_fachId.toString()) != null)
       {
           l_fachDesc = i_fachDS.getRecord(p_fachId.toString()).getDesc();
       }
       
       return l_fachDesc; 
    }

    /** 
     * Returns <code>true</code> if this subscriber
     * is a subordinate, otherwise returns <code>false</code>.
     * 
     * @return True if the subscriber is a subordinate, otherwise, false.
     */
    public boolean isSubordinate()
    {
        return i_isSubordinate;
    }

    /**
     * Marks this object as being for a subordinate.
     */
    public void setIsSubordinate()
    {
        i_isSubordinate = true;
        return;
    }

    /**
     * @return Returns the maximum number of rows that can be retrieved.
     */
    public Integer getRecordRetrievalLimit()
    {
        return i_recordRetrievalLimit;
    }

    /**
     * @param p_retrievalLimit Sets the maximum number of rows that can be retrieved in one go.
     */
    public void setRecordRetrievalLimit(Integer p_retrievalLimit)
    {
        i_recordRetrievalLimit = p_retrievalLimit;
    }

}

