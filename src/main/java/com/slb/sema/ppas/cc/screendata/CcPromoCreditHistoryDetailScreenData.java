////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromoCreditHistoryDetailScreenData.java
//      DATE            :       8-Sep-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PpaLon#1513/6383
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Promotional 
//                              Credit Detail pop-up window.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Promotional credit Detail pop-up window.
 */
public class CcPromoCreditHistoryDetailScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** DivisionData object wrapped by this object. */
    private DivisionData          i_divisionData = null;

    /** Promotion credit value in subscriber's currency. */
    private String               i_promoCreditAppliedValue = null;
    
    /** Currency of promotional credit. */
    private String               i_promoCreditAppliedCurrency = null;
    
    /** Date/time of promotional credit. */
    private PpasDateTime         i_promoCreditDateTime = null;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcPromoCreditHistoryDetailScreenData(GuiContext p_guiContext,
                                                GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the DivisionData object wrapped within this object.
     * Returns null if there is no DivisionData object set.
     * @return The DivisionData object defining how a promotional
     *         credit was divided (over master and dedicated accounts).
     */
    public DivisionData getDivisionData()
    {
        return i_divisionData;
    }

    /** Sets the <code>i_rechargeDivisionData</code> instance attribute of
     *  this class.
     *  @param p_divisionData Value to assign to <code>i_rechargeDivisionData</code>.
     */
    public void setDivisionData(DivisionData p_divisionData)
    {
        i_divisionData = p_divisionData;
    }

    /** 
     * Returns the promotional credit value in the subscriber's currency.
     * @return Promotional credit in applied currency. 
     */
    public String getPromoCreditAppliedValue()
    {
        return i_promoCreditAppliedValue;
    }
    
    /** 
     * Sets the promotional credit value in the subscriber's currency.
     * @param p_promoCreditAppliedValue Promotional credit in applied currency. 
     */
    public void setPromoCreditAppliedValue(String p_promoCreditAppliedValue)
    {
        i_promoCreditAppliedValue = p_promoCreditAppliedValue;
    }
    
    /** 
     * Returns the applied currency of the promotional credit.
     * @return Promotional credit applied currency. 
     */
    public String getPromoCreditAppliedCurrency()
    {
        return i_promoCreditAppliedCurrency;
    }
    
    /** 
     * Sets the applied currency of the promotional credit.
     * @param p_promoCreditAppliedCurrency Promotional credit applied currency. 
     */
    public void setPromoCreditAppliedCurrency(String p_promoCreditAppliedCurrency)
    {
        i_promoCreditAppliedCurrency = p_promoCreditAppliedCurrency;
    }
    
    /** 
     * Returns the datetime of the promotional credit.
     * @return Promotional credit datetime. 
     */
    public PpasDateTime getPromoCreditDateTime()
    {
        return i_promoCreditDateTime;
    }

    /** 
     * Sets the datetime of the promotional credit.
     * @param p_promoCreditDateTime Promotional credit datetime. 
     */
    public void setPromoCreditDateTime(PpasDateTime p_promoCreditDateTime)
    {
        i_promoCreditDateTime = p_promoCreditDateTime;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return i_divisionData != null;
    }

} // end public class CcPromoCreditHistoryDetailScreenData
