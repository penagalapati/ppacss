////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDisconnectServlet.Java
//      DATE            :       03-Apr-2002
//      AUTHOR          :       Remi Isaacs
//      REFERENCE       :       PpaLon#1341/5352
//                              PRD_PPAK00_DEV_IN_035
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Disconnect Information screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
//07/10/03  | R Isaacs   | Privilege checks added          | CR#60/571
//----------+------------+---------------------------------+--------------------
//24-Apr-07 | S James    | Changes for Account Reconnect   | PpacLon#3072/11370
//----------+------------+---------------------------------+--------------------
//06-Aug-07 | K Bond     | Corrections to Account Reconnect| PpacLon#3246/11923
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcDisconnectScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataSetResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcDisconnectDataResponse;
import com.slb.sema.ppas.cc.service.CcDisconnectService;
import com.slb.sema.ppas.cc.service.CcSubordinatesService;
import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.dataclass.AccountDataSet;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.cc.service.CcReconnectService;

/**
 * Servlet for handling requests from the Disconnect screen.
 */
public class CcDisconnectServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDisconnectServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Disconnect Information Service to be used by this servlet. */
    private CcDisconnectService i_ccDisconnectService = null;

    /** The CC Subordinates Service to be used by this servlet. */
    private CcSubordinatesService i_ccSubordinatesService = null;

    /** The CC Account Service to be used by this servlet. */
    private CcAccountService i_ccAccountService = null;

    /** The CC Reconnect Service to be used by this servlet. */
    private CcReconnectService i_ccReconnectService = null;

    /** The default adjustment type for cleared credit adjustment */
    private String i_defaultAdjType = "";
    
    /** The default adjustment code for cleared credit adjustment */
    private String i_defaultAdjCode = "";

    /** The default airtime service offset for reconnected account */
    private int i_airtimeExpiryOffset = 0;

    /** The default service offset for reconnected account */
    private int i_serviceExpiryOffset = 0;

    /** Primary delay between the reconnect request returning to the servlet,
     *  and the attempt to get the basic account data of the subscriber that has just been reconnected.
     */         
    private long             i_primaryDelayPeriod;    
         
    /** Secondary delay between the reconnect request returning to the servlet,
     *  and the attempt to get the basic account data of the subscriber that has just been reconnected.
     *  If after the primary delay the data indicates that the reconnection has not been fully completed,
     *  then the servlet will wait for the 2nd configurable delay period.
     */    
    private long             i_secondaryDelayPeriod;    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */

    public CcDisconnectServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /**
     * Currently does nothing but will construct a CcDisconnectService
     * in the fullness of time.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcDisconnectService here...
        // unfortunately no GuiRequest is available.

        // Get the value for the primary delay period for the attempt
        // to get the basic account data for the subscriber being reconnected.
        i_primaryDelayPeriod = 
            ((Long)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.accountDataRetrievalPrimaryDelay")).     
            longValue();           

        // Get the value for the secondary delay period for the attempt
        // to get the basic account data for the subscriber being reconnected.         
        i_secondaryDelayPeriod = 
            ((Long)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.accountDataRetrievalSecondaryDelay")).
            longValue();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the Disconnect screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest )
    {
        String                      l_command                  = null;
        String                      l_forwardUrl               = null;
        CcDisconnectScreenData      l_disconnectScreenData     = null;
        GuiResponse                 l_guiResponse              = null;
        boolean                     l_accessGranted            = false; // CR#60/571
        GopaGuiOperatorAccessData   l_gopaData                 = null;  // CR#60/571


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService + " with input data: " +
                "p_guiRequest=" + p_guiRequest);
        }

        // Construct CcDisconnectService if it does not already exist ...
        if (i_ccDisconnectService == null)
        {
            i_ccDisconnectService = new CcDisconnectService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcSubordinatesService if it does not already exist ...
        if (i_ccSubordinatesService == null)
        {
            i_ccSubordinatesService = new CcSubordinatesService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcAccountService if it does not already exist ...
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcReconnectService if it does not already exist ...
        if (i_ccReconnectService == null)
        {
            i_ccReconnectService = new CcReconnectService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        try
        {
            i_defaultAdjCode = i_configService.getSyfgConfigData().get("RECONNECT", "ADJ_REAPPLY_CODE");
            i_defaultAdjType = i_configService.getSyfgConfigData().get("RECONNECT", "ADJ_REAPPLY_TYPE");
            i_airtimeExpiryOffset = Integer.parseInt(i_configService.getSyfgConfigData().get("RECONNECT", "AIRTIME_EXPIRY_OFFSET_PERIOD"));
            i_serviceExpiryOffset = Integer.parseInt(i_configService.getSyfgConfigData().get("RECONNECT", "SERVICE_EXPIRY_OFFSET_PERIOD"));
        }
        catch (PpasConfigException l_e)
        {
            i_logger.logMessage(l_e);
        }

        // Create screen data object.
        l_disconnectScreenData = new CcDisconnectScreenData( i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,                 //  mandatory
                                      "",
                                      new String [] { "GET_SCREEN_DATA",
                                                      "DISCONNECT",
                                                      "CANCEL_DISCONNECTION",
                                                      "RECONNECT"});


            // CR#60/571 Start

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasDisconnectScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("DISCONNECT")) && l_gopaData.hasDisconnectScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("CANCEL_DISCONNECTION")) && l_gopaData.hasDisconnectScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("RECONNECT")) && l_gopaData.isAccountReconnectionPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(), //CR#60/571
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_disconnectScreenData.addRetrievalResponse(p_guiRequest,
                                                            l_guiResponse);

                l_forwardUrl = new String("/jsp/cc/ccaccesserror.jsp");

                p_request.setAttribute("p_guiScreenData",
                                        l_disconnectScreenData);
            }
            else
            {
                getBusinessConfigData(p_guiRequest,
                                      l_disconnectScreenData);

                if (l_command.equals("DISCONNECT"))
                {
                    applyDisconnect(p_guiRequest,
                                    p_request,
                                    l_disconnectScreenData );

                }
                else if (l_command.equals("CANCEL_DISCONNECTION"))
                {
                    cancelDisconnection(p_guiRequest,
                                        l_disconnectScreenData );

                }
                else if (l_command.equals("RECONNECT"))
                {
                    applyReconnect(p_guiRequest,
                                   p_request,
                                   l_disconnectScreenData );

                }

                getDisconnectDetails(p_guiRequest,
                                     l_disconnectScreenData);
            }
            // CR#60/571 End

        } // end try
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 73872, this,
                    "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_disconnectScreenData.addRetrievalResponse( p_guiRequest, l_guiResponse );
        } //end catch

        // CR#60/571 Start

        if (l_accessGranted)
        {
            if (l_disconnectScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Disconnect screen.
                // Forward to the CcError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

            }
            else
            {
                // CcDisconnectScreenData object has the necessary data
                // to populate the disconnect screen.
                l_forwardUrl = "/jsp/cc/ccdisconnect.jsp";

            }

            p_request.setAttribute ("p_guiScreenData", l_disconnectScreenData);
        }
        // CR#60/571 End

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13050, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_applyDisconnect =
                                         "applyDisconnect";

    /** Calls CcDisconnectService to disconnect
     *  the account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void applyDisconnect( GuiRequest                 p_guiRequest,
                                  HttpServletRequest         p_request,
                                  CcDisconnectScreenData     p_screenData )
    {
        String                       l_disconnectType        = null;
        GuiSession                   l_guiSession            = null;
        GuiResponse                  l_guiResponse           = null;
        PpasDateTime                 l_now                   = null;
        String                       l_disconnectDate        = null;
        String                       l_disconnectPeriod      = null;
        PpasDate                     l_futureDate            = null;


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Entered " + C_METHOD_applyDisconnect);
        }

        try
        {
            // Get the disconnect reason
            l_disconnectType =
                    getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_disconnectReason",
                                  true,
                                  "",
                                  PpasServlet.C_TYPE_ANY);

            l_disconnectPeriod =
                   getValidParam(p_guiRequest,
                                 0,
                                 p_request,
                                 "p_disconnectPeriod",
                                 true,
                                 "",
                                 new String [] {"FutureDisconnection",
                                                "ImmediateDisconnection"});

            if ( l_disconnectPeriod.equals("FutureDisconnection"))
            {
                l_disconnectDate =
                           getValidParam(p_guiRequest,
                                         0,
                                         p_request,
                                         "p_endServiceDate",
                                         true,
                                         "",
                                         PpasServlet.C_TYPE_DATE);

                l_futureDate = new PpasDate(l_disconnectDate);

                l_guiResponse =
                      i_ccDisconnectService.futureDisconnect(p_guiRequest,
                                                             i_timeout,
                                                             l_disconnectType,
                                                             l_futureDate);

                p_screenData.addUpdateResponse(p_guiRequest,
                                               l_guiResponse);

                l_now = DatePatch.getDateTimeNow();

                if (!p_screenData.hasUpdateError())
                {
                    p_screenData.setInfoText(
                         "Successful Future Disconnection Made at " + l_now);
                }
                else
                {
                    // An error has occurred whilst trying a disconnection

                    p_screenData.setInfoText("Future Disconnection Failed at " + l_now);
                }

            }
            else //Immediate Disconnect
            {
                l_guiResponse =
                      i_ccDisconnectService.disconnectSubscriber(
                                                     p_guiRequest,
                                                     i_timeout,
                                                     l_disconnectType);

                p_screenData.addUpdateResponse(p_guiRequest,
                                               l_guiResponse);

                // Get the current time to use in display message.

                l_now = DatePatch.getDateTimeNow();

                if (p_screenData.hasUpdateError())
                {
                    // An error has occurred whilst trying a disconnection

                    p_screenData.setInfoText("Disconnection Failed at " + l_now);
                }
                else
                {
                    l_guiSession = (GuiSession)p_guiRequest.getSession();
                    l_guiSession.setCustStatus(BasicAccountData.
                                               C_SUBS_STATUS_PERM_DISC);

                    p_screenData.setInfoText("Successfully Disconnected at " + l_now);

                }
            }
        }
        catch (PpasServletException l_ppasSE)
        {

            // This should only happen if an invalid parameter was found,
            // in which case the service will not have been attempted.
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE );

            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            l_guiResponse =
                     new GuiResponse
                         (p_guiRequest,
                          p_guiRequest.getGuiSession().
                            getSelectedLocale(),  // PpacLon#1/17
                          GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                          new Object[] {"Disconnect"},
                          GuiResponse.C_SEVERITY_WARNING);

            p_screenData.addRetrievalResponse (p_guiRequest,
                                               l_guiResponse);

        } // end catch

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27099, this,
                "Leaving " + C_METHOD_applyDisconnect);
        }

        return;

    } // end private void applyDisconnect(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDisconnectDetails = "getDisconnectDetails";
    /**Get the Disconnection details.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Voucher screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void getDisconnectDetails( GuiRequest              p_guiRequest,
                                       CcDisconnectScreenData  p_screenData )
        throws PpasServletException
    {
        CcDisconnectDataResponse      l_ccDisconnectDataResponse      = null;
        CcAccountDataSetResponse      l_ccAccountDataSetResponse      = null;
        DisconnectData                l_disconnectData                = null;
        String                        l_custId                        = null;
        String                        l_btCustId                      = null;
        AccountDataSet                l_accountDataSet                = null;
        int                           l_noOfSubordinates              = 0;
        String                        l_accountType                   = null;
        GuiSession                    l_guiSession                    = null;
        String                        l_endServDate                   = null;
        PpasDate                      l_today                         = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27023, this,
                "Entered " + C_METHOD_getDisconnectDetails);
        }

        l_guiSession = (GuiSession)p_guiRequest.getSession();
        l_btCustId = l_guiSession.getBtCustId();
        l_custId = l_guiSession.getCustId();

        l_today  = DatePatch.getDateToday();

        // Determine if the account is a Master, Standalone, or Subordinate
        if ( !l_custId.equals(l_btCustId))
        {
            l_accountType = "Subordinate";
        }
        else
        {
            l_ccAccountDataSetResponse =
                    i_ccSubordinatesService.getSubordinateDataSet( p_guiRequest,
                                                                   i_timeout,
                                                                   l_custId );


            if ( l_ccAccountDataSetResponse.isSuccess())
            {
                l_accountDataSet = l_ccAccountDataSetResponse.getAccountDataSet();

                p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccAccountDataSetResponse );

                l_noOfSubordinates = l_accountDataSet.getData().size();

                if ( l_noOfSubordinates > 0 )
                {
                    l_accountType = "Master";
                }
                else
                {
                    l_accountType = "Standalone";
                }
            }
        }
        p_screenData.setAccountType(l_accountType);

        // Obtain disconnection details for the disconnected Account
        l_ccDisconnectDataResponse = i_ccDisconnectService.getDisconnectDetails(p_guiRequest, i_timeout);

        if (l_ccDisconnectDataResponse.isSuccess())
        {
            l_disconnectData = l_ccDisconnectDataResponse.getDisconnectData();

            p_screenData.addRetrievalResponse(p_guiRequest,
                                              l_ccDisconnectDataResponse);
            p_screenData.setDisconnectDetails(l_disconnectData);

            l_endServDate = l_disconnectData.getDisconnectEndServDate().toString();


            l_guiSession = (GuiSession)p_guiRequest.getSession();

            // Determine Whether the Account is disconnected.
            if (l_guiSession.isDisconnected())
            {
                p_screenData.setIsDisconnected(true);
                // set a flag to indicate that reconnection is in progress
                if (l_guiSession.isReconnectInProgress())
                {
                    p_screenData.setIsReconnectInProgress(true);
                }
            }
            // The Account is Active Or Available
            else
            {
                p_screenData.setIsDisconnected(false);
                p_screenData.setIsReconnectInProgress(false);
                // Check whether the subscriber has an End Service Date.
                // Having an End Service Date indicates the Subscriber is
                // Scheduled for Future Disconnection

                if ( l_endServDate.equals(""))
                {
                    p_screenData.setIsFutureDisconnected(false);

                    // Set end service date to today because that is the
                    // default date displayed when future disconnect is
                    // selected.

                    p_screenData.setEndServiceDate(l_today);

                }
                else
                {
                    p_screenData.setIsFutureDisconnected(true);
                    
                    p_screenData.setEndServiceDate(l_disconnectData.getDisconnectEndServDate());

                    if (l_accountType.equals("Subordinate"))
                    {
                        // If the master has an end service date set which is the same as the
                        // subordinate's end service date, then the CSO will not be
                        // able to cancel the future disconnection on the subordinate
                        // (except by cancelling the future disconnection for the master).
                        // If the master has no end service date set, or if the end date is 
                        // later than the subordinate's end service date, then the CSO can 
                        // cancel the future disconnection of the subordinate.
                        p_screenData.setCanCancelFutureDisconnect(
                             i_ccSubordinatesService.canCancelFutureDisconnection(p_guiRequest,
                                                                                  i_timeout));
                    }
                }
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Leaving " + C_METHOD_getDisconnectDetails);
        }
        return;

    } // end private void getDisconnectDetails(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getBusinessConfigData =
                                         "getBusinessConfigData";

    /**
     *  Obtains the business configuration data and makes it available in the
     *  given disconnect 'screen data' object.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void getBusinessConfigData(GuiRequest             p_guiRequest,
                                       CcDisconnectScreenData p_screenData)
    {
        DireDisconnectReasonDataSet  l_disconnectReasons   = null;
        DireDisconnectReasonData []  l_disconnectReasonArray;
        FeatureLicenceCache          l_featureLicenceCache;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27024, this,
                "Entered " + C_METHOD_getBusinessConfigData);
        }

        l_disconnectReasons = i_configService.getAvailableDiscReasons();

        l_disconnectReasonArray =
               l_disconnectReasons
               .getAvailableArray(p_guiRequest);

        p_screenData.setDiscReasons(l_disconnectReasonArray);

        // Set feature licence flags.
        try
        {
            l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
            p_screenData.setAccountReconnectFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_ACCOUNT_RECONNECTION));
        }
        catch (PpasServiceException p_ppasServiceE)
        {
            i_logger.logMessage(p_ppasServiceE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27095, this,
                "Leaving " + C_METHOD_getBusinessConfigData);
        }
        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_cancelDisconnection =
                                        "cancelDisconnection";
    /**Cancel Disconnection.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void cancelDisconnection( GuiRequest               p_guiRequest,
                                       CcDisconnectScreenData  p_screenData )
    {
        GuiResponse         l_guiResponse          = null;
        PpasDateTime        l_now                  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27723, this,
                "Entered " + C_METHOD_cancelDisconnection);
        }

        l_guiResponse =
              i_ccDisconnectService.cancelDisconnection(p_guiRequest,
                                                        i_timeout);

        p_screenData.addUpdateResponse(p_guiRequest,
                                       l_guiResponse);

        // Get the current time to use in display message.

        l_now = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            // An error has occurred whilst trying a disconnection

            p_screenData.setInfoText(
                       "Cancellation Failed for future disconnection " + l_now);
        }
        else
        {
            p_screenData.setInfoText(
                         "Successfully cancelled future disconnection " + l_now);

        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27790, this,
                "Leaving " + C_METHOD_cancelDisconnection);
        }
        return;

    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_applyReconnection =
                                         "applyReconnection";

    /** Calls CcReconnectService to reconnect
     *  the disconnected account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Disconnect/Reconnect screen.
     */
    private void applyReconnect(GuiRequest                 p_guiRequest,
                                HttpServletRequest         p_request,
                                CcDisconnectScreenData     p_screenData )
    {
        String                       l_applyClearedCreditStr = null;
        boolean                      l_applyClearedCredit    = false;
        GuiSession                   l_guiSession            = null;
        GuiResponse                  l_guiResponse           = null;
        PpasDateTime                 l_now                   = null;


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27800, this,
                "Entered " + C_METHOD_applyReconnection);
        }

        try
        {
            // Get the re-apply credit flag
            l_applyClearedCreditStr =
                    getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_applyClearedCredit",
                                  false,
                                  "false",
                                  PpasServlet.C_TYPE_BOOLEAN);

            l_applyClearedCredit = Boolean.valueOf(l_applyClearedCreditStr).booleanValue();

            l_guiResponse =
                  i_ccReconnectService.reconnectSubscriber(
                                                 p_guiRequest,
                                                 i_timeout,
                                                 l_applyClearedCredit,
                                                 i_defaultAdjType,
                                                 i_defaultAdjCode,
                                                 i_airtimeExpiryOffset,
												 i_serviceExpiryOffset);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);

            // Get the current time to use in display message.

            l_now = DatePatch.getDateTimeNow();

            if (p_screenData.hasUpdateError())
            {
                // An error has occurred whilst trying the reconnection

                p_screenData.setInfoText("Reconnection Failed at " + l_now);
            }
            else
            {
                // Let the thread sleep for a period
                // to allow the asynchronous subscriber reconnect process to fulfill.
                // Try then to get the basic account data for the subscriber being reconnected.
                // Let the thread sleep for another period if
                // the subscriber reconnection process has not yet completed.
                try
                {
                    // Let the thread sleep for the primary delay period                    
                    Thread.sleep(i_primaryDelayPeriod);

                    // Check if the status of the subsriber account is 'Available'.
                    if (i_ccAccountService.getBasicData(p_guiRequest, i_timeout).
                           getBasicAccountData().getCustStatus() != BasicAccountData.C_SUBS_STATUS_ACTIVE)
                    {
                        // Let the thread sleep for secondary delay period
                        Thread.sleep(i_secondaryDelayPeriod);
                    }
                }
                catch (InterruptedException l_interruptedE)
                {
                    // Exception handling makes no sense here, in case of a thread interruption
                    // the servlet's life cycle will end.                        
                    l_interruptedE = null;
                }          

                l_guiSession = (GuiSession)p_guiRequest.getSession();
                l_guiSession.setCustStatus(i_ccAccountService.getBasicData(p_guiRequest, i_timeout).
                                               getBasicAccountData().getCustStatus());

                p_screenData.setInfoText("Successfully Reconnected at " + l_now);

            }
        }
        catch (PpasServletException l_ppasSE)
        {

            // This should only happen if an invalid parameter was found,
            // in which case the service will not have been attempted.
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE );

            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            l_guiResponse =
                     new GuiResponse
                         (p_guiRequest,
                          p_guiRequest.getGuiSession().
                            getSelectedLocale(),
                          GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                          new Object[] {"Reconnect"},
                          GuiResponse.C_SEVERITY_WARNING);

            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

        } // end catch

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27810, this,
                "Leaving " + C_METHOD_applyReconnection);
        }

        return;

    } // end private void applyDisconnect(...)

} //end class ccdisconnectServlet

