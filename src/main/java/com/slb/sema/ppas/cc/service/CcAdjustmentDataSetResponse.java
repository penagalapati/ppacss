////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentDataSetResponse.java
//      DATE            :       01-Mar-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1295/5175
//                              PRD_PPAK00_DEV_IN_032
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       CcXResponse class for adjustment data set objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.AdjustmentHistoryDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for adjustment data set objects. 
 */
public class CcAdjustmentDataSetResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdjustmentDataSetResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The adjustment data set object wrapped by this object. */
    private AdjustmentHistoryDataSet  i_adjustmentDataSet;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives an adjustment data set object and the additional necessary 
     * objects to create a response message indicating the status of a 
     * call to an AdjustmentService method.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_adjustmentDataSet The Adjustment information.
     * @param p_messageSeverity   Severity of response.
     */
    public CcAdjustmentDataSetResponse( GuiRequest        p_guiRequest,
                                        Locale            p_messageLocale,
                                        String            p_messageKey,
                                        Object[]          p_messageParams,
                                        int               p_messageSeverity,
                                        AdjustmentHistoryDataSet p_adjustmentDataSet )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_adjustmentDataSet = p_adjustmentDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Returns the Adjustment information.
     * @return Set of adjustment data.
     */
    public AdjustmentHistoryDataSet getAdjustmentDataSet()
    {
        return i_adjustmentDataSet;
    }
} // end of class CcAdjustmentDataSetResponse
