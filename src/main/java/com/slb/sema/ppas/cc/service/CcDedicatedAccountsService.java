////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDedicatedAccountsService.java
//      DATE            :       08-Aug-2002
//      AUTHOR          :       Simone Nelson & Erik Clayton
//      REFERENCE       :       PpaLon#1448 & PpaLon#1749
//                              PRD_PPAK00_GEN_CA_372 & 415
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Gui Service that provides methods associated
//                              with dedicated accounts.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AdjustmentHistoryDataSet;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasBalanceService;
import com.slb.sema.ppas.is.isapi.PpasDedicatedAccountService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with Dedicated Accounts.
 */
public class CcDedicatedAccountsService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDedicatedAccountsService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasBalanceService i_balanceService;

    /** Atomic service used by this Gui Service. */
    private PpasDedicatedAccountService i_dedicatedAccountService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcDedicatedAccountsService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct erxceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcDedicatedAccountsService(
                            GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext )
    {
        super( p_guiRequest, p_flags, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create internal services to be used by this service.
        i_balanceService = new PpasBalanceService(p_guiRequest, p_logger, p_guiContext);

        i_dedicatedAccountService = new PpasDedicatedAccountService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDedicatedAccountBalances = "getDedicatedAccountBalances";
    /**
     * Retrieves list of DedicatedAccounts and their account data.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Balance data.
     */
    public CcBalanceDataResponse getDedicatedAccountBalances(
                                             GuiRequest p_guiRequest,
                                             long       p_timeoutMillis)
    {
        CcBalanceDataResponse       l_ccBalanceDataResponse    = null;
        GuiResponse                 l_guiResponse              = null;
        BalanceEnquiryResultData    l_balanceEnquiryResultData = null;
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 10050, this,
                "Entered " + C_METHOD_getDedicatedAccountBalances);
        }

        try
        {
            l_balanceEnquiryResultData = i_balanceService.getBalance(p_guiRequest, 0, p_timeoutMillis);

            l_ccBalanceDataResponse
                = new CcBalanceDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_balanceEnquiryResultData,
                                  null);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10160, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, C_METHOD_getDedicatedAccountBalances, l_pSE);

            l_ccBalanceDataResponse =
                new CcBalanceDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null, null);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 10070, this,
                "Leaving " + C_METHOD_getDedicatedAccountBalances);
        }

        return ( l_ccBalanceDataResponse );

    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDedicatedAccountAdjustments = "getDedicatedAccountAdjustments";
    /**
     * Retrieve dedicated account adjustment history for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Response containing the adjustment data set.
     */
    public CcAdjustmentDataSetResponse getDedicatedAccountAdjustments(
                                           GuiRequest p_guiRequest,
                                           long       p_timeoutMillis)
    {
        CcAdjustmentDataSetResponse l_ccAdjustmentDataSetResponse  = null;
        GuiResponse                 l_guiResponse                  = null;
        AdjustmentHistoryDataSet    l_adjustmentDataSet            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getDedicatedAccountAdjustments);
        }

        try
        {
            l_adjustmentDataSet =
                i_dedicatedAccountService.getDedicatedAccountAdjustments(p_guiRequest, p_timeoutMillis);

            l_ccAdjustmentDataSetResponse =
                new CcAdjustmentDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                GuiResponse.C_KEY_SERVICE_SUCCESS,
                                new Object[] {},
                                GuiResponse.C_SEVERITY_SUCCESS,
                                l_adjustmentDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get adjustments", l_pSE);

            l_ccAdjustmentDataSetResponse =
                new CcAdjustmentDataSetResponse(
                                          p_guiRequest,
                                          p_guiRequest.getGuiSession().getSelectedLocale(),
                                          l_guiResponse.getKey(),
                                          l_guiResponse.getParams(),
                                          GuiResponse.C_SEVERITY_FAILURE,
                                          null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getDedicatedAccountAdjustments);
        }

        return l_ccAdjustmentDataSetResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_postDedicatedAccountAdjustment = "postDedicatedAccountAdjustment";
    /**
     * Post a dedicated acccount adjustment for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_transAmt      Amount of the adjustment.
     * @param p_type          Type of the adjustment.
     * @param p_code          Code of the adjustment.
     * @param p_description   Description of the adjustment.
     * @param p_dedAccountId  Id of the dedicated account for the adjustment.
     * @param p_dedExpiryDateOld  Old expiry date for the adjustment.
     * @param p_dedExpiryDateNew  New expiry date for the adjustment.
     * @param p_adjApprov     Flag indicating if the adjustment has been approved or if
     *                        the limit needs to be checked.
     * @return Response indicating success or failure of the adjustment.
     */
    public GuiResponse postDedicatedAccountAdjustment(
                     GuiRequest                 p_guiRequest,
                     long                       p_timeoutMillis,
                     Money                      p_transAmt,
                     String                     p_type,
                     String                     p_code,
                     String                     p_description,
                     String                     p_dedAccountId,
                     PpasDate                   p_dedExpiryDateOld,
                     PpasDate                   p_dedExpiryDateNew,
                     Boolean                    p_adjApprov)                     
    {
        GuiResponse                 l_guiResponse       = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 30000, this,
                "Entered " + C_METHOD_postDedicatedAccountAdjustment);
        }
        try
        {
            i_dedicatedAccountService.postDedicatedAccountAdjustment(
                p_guiRequest,
                p_timeoutMillis,
                null,
                p_transAmt,
                p_type,
                p_code,
                p_description,
                p_dedAccountId,
                p_dedExpiryDateOld,
                p_dedExpiryDateNew,
                p_adjApprov );

            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                new Object[] {"post adjustment"},
                                GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 30210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "make the adjustment", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 31000, this,
                "Leaving " + C_METHOD_postDedicatedAccountAdjustment);
        }

        return (l_guiResponse);
    }
}
