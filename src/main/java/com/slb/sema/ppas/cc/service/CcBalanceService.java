////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcBalanceService.java
//      DATE            :       27-Feb-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Customer Care Service that wrappers calls to
//                              the PpasBalanceService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.churn.ChurnIndicatorData;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasBalanceService;
import com.slb.sema.ppas.util.logging.Logger;

/** Customer care service that wraps the Balance Internal
 *  service. This service is used to obtain the balance and pending credit
 *  for the account for given customer accounts.
 */
public class CcBalanceService extends GuiService
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /** Flag bit indicating that only main balance is required. */
    public static final long C_FLAG_MAIN_BALANCE = 0x1;

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcBalanceService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The balance internal service used by this Gui Service. */
    private PpasBalanceService i_balanceIS;

    /** Account IS Service. */
    private PpasAccountService i_accountIS;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the Customer Care Balance Service.
     *  This service is used to obtain the balance and pending credit for
     *  giben accounts.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcBalanceService(
        GuiRequest p_guiRequest,
        long       p_flags,
        Logger     p_logger,
        GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 65000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_balanceIS = new PpasBalanceService(p_guiRequest, p_logger, p_guiContext);
        i_accountIS = new PpasAccountService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 65090, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getBalance = "getBalance";
    /**
     * Get the balance and pending credit, in the supplied currency, for the
     * customer defined in the input request.
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Time in milliseconds to wait for resources
     *                        required to perform the service to become
     *                        available before giving up.
     * @return An event history response object containing the event history
     *         data for the customer defined in the given request.
     */
    public CcBalanceDataResponse getBalance(GuiRequest p_guiRequest,
                                            long       p_timeoutMillis)
    {
        GuiResponse                     l_guiResponse = null;
        CcBalanceDataResponse           l_balanceResponse = null;
        BalanceEnquiryResultData        l_balanceData;
        ChurnIndicatorData              l_churnIndData = null;
        FeatureLicenceCache             l_featureLicenceCache;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 65100, this,
                "Entered " + C_METHOD_getBalance);
        }

        try
        {
            l_balanceData = i_balanceIS.getBalance (p_guiRequest, 0, p_timeoutMillis);
            
            l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
            
            if (l_featureLicenceCache != null && 
                    l_featureLicenceCache.isLicensed(FeatureLicenceCache.C_FEATURE_CODE_CHURN_ANALYSIS))
            {
                l_churnIndData = i_accountIS.analyseSubscriber(
                                     p_guiRequest, p_timeoutMillis, l_balanceData.getBalance());
            }

            l_balanceResponse = new CcBalanceDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_balanceData,
                                  l_churnIndData);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 65110, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException (p_guiRequest, 0, "Get the balance", l_pSE);

            l_balanceResponse = new CcBalanceDataResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null,null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 65190, this,
                "Leaving " + C_METHOD_getBalance);
        }

        return (l_balanceResponse);

    }  // end method getBalance
} // end public class CcBalanceService
