////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcContextBalanceScreenData.java
//      DATE            :       01-mar-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Customer Care Screen Data object for the
//                              balance context bar frame.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 21/08/03 | M.Brister  | Removed getPendingCredit and    | PpacLon#43/384
//          |            | C_PENDING_CREDIT_UNAVAILABLE.   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.churn.ChurnAnalysis;
import com.slb.sema.ppas.common.churn.ChurnIndicatorData;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Customer Care Screen Data object for the balance context bar frame.
 */
public class CcContextBalanceScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcContextBalanceScreenData";

    /** String to be displayed when the balance cannot be obtained. */
    private static final String C_BALANCE_UNAVAILABLE = "Unavailable";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** BalanceEnquiryResultData object wrapped by this object. */
    private BalanceEnquiryResultData i_balanceData;

    /** The time at which the balance was read. */
    private String i_balanceTime = "";
    
    /** Data for displaying subscriber ranking and a churn indicator. */
    private ChurnIndicatorData i_churnData;
    
    /** Churn Analysis data for this subscriber. */
    private ChurnAnalysis i_churnAnalysis;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Constructor an instance of the CcContextBalanceScreenData class.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcContextBalanceScreenData (GuiContext p_guiContext,
                                       GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 87000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_balanceData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 87090, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Sets the balance data defining the customer's balance obtained from
     * the IN.
     * @param p_balanceData Balance data.
     */
    public void setBalanceData (BalanceEnquiryResultData p_balanceData)
    {
        i_balanceData = p_balanceData;
    }

    /** Get the customer's balance from the balance data. If this object does
     *  not contain any balance data, then return a fixed String indicating
     *  that the balance is not available.
     * @return String representation of the balnce data.
     */
    public String getBalance()
    {
        String l_balanceString;

        if (i_balanceData != null)
        {
            l_balanceString = i_moneyFormat.format(i_balanceData.getBalance());
        }
        else
        {
            l_balanceString = C_BALANCE_UNAVAILABLE;
        }
        return (l_balanceString);
    }

    /** Sets the time at which the balance was obtained. If the balance was
     *  unavailable, then the time is set to the currenct time.
     * @param p_request The request being processed.
     * @param p_logger  The logger to direct messages.
     */
    public void setBalanceTime (GuiRequest p_request,
                                Logger     p_logger)
    {
        String l_balanceDTStr = "";
        int    l_index = 0;
        String l_timePart = "";
        PpasDateTime l_balanceDT;

        if (i_balanceData != null)
        {
            l_balanceDT = i_balanceData.getBalanceTime();
        }
        else
        {
            l_balanceDT = DatePatch.getDateTimeNow ();
        }

        l_balanceDTStr = l_balanceDT.toString();

        // Get the time part of the date/time
        l_index    = l_balanceDTStr.indexOf (' ');
        l_timePart = l_balanceDTStr.substring (l_index + 1);

        // Now strip of the seconds bit
        l_index = l_timePart.lastIndexOf (':');
        if (l_index > 0)
        {
            i_balanceTime = l_timePart.substring (0, l_index);
        }
        else
        {
            i_balanceTime = l_timePart;
        }

        return;

    } // end method 'setBalanceTime'


    /** Get the aproximate date/time at which the balance was obtained. If the
     *  customer's balance data has not been set up; then the current time is
     *  returned.
     *  @return String giving the time at which the balance was read, or the
     *          current time if no balance was available.
     */
    public String getBalanceTime()
    {
        return (i_balanceTime);
    }

    /**
     * Returns true if the object contains a customer's balance data.
     * Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return ((i_balanceData != null) ? true : false );
    }
    
    /** Get the currency associated with this balance.
     * 
     * @return ISO currency code in which this balance is expressed.
     */
    public String getCurrency()
    {
        String l_currency = "";
        
        if (i_balanceData != null)
        {
            l_currency = i_balanceData.getBalance().getCurrency().getCurrencyCode();
        }
        
        return l_currency;
    }
    
    /**
     * Set the churn indicator data.
     * @param p_churnData
     */
    public void setChurnData (ChurnIndicatorData p_churnData)
    {
        i_churnData = p_churnData;
    }
    
    /**
     * Indicates whether the subscriber is likely to churn.
     * @return true if subscriber is likely to churn and false otherwise.
     */
    public boolean isLikelyToChurn()
    {
        boolean l_isLikelyToChurn = false;
        
        if (i_churnData != null && !i_churnData.isInsufficientData())
        {
            if (i_churnAnalysis == null)
            {
                i_churnAnalysis = i_churnData.getChurnAnalysis();
            }
            l_isLikelyToChurn = i_churnAnalysis.isLikelyToChurn();
        }
        
        return l_isLikelyToChurn;
    }
    
    /**
     * Get explanation for churn indicator display.
     * @return A String containing the results of churn analysis based on the configured conditions.
     */
    public String getReason()
    {
        String l_reason = "";
        String l_nl = System.getProperty("line.separator");
        
        if (i_churnData != null && !i_churnData.isInsufficientData())
        {
            if (i_churnAnalysis == null)
            {
                i_churnAnalysis = i_churnData.getChurnAnalysis();
            }
            
            l_reason = i_churnAnalysis.getReason();
            l_reason = l_reason.replaceAll(l_nl, "<p>");
        }
        
        return l_reason;
    }
    
    /**
     * Returns the subscriber ranking. 
     * @return H for high, M for mid, or L for low.
     */
    public char getRanking()
    {
        char l_return = ' ';
            
        if (i_churnData != null && !i_churnData.isInsufficientData())
        {
            l_return = i_churnData.getRanking();
        }
        
        return l_return;
    }

} // end class CcContextBalanceScreenData
