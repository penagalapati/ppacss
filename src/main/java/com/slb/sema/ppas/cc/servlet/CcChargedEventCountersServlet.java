////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcChargedEventCountersServlet.Java
//      DATE            :       08-Jun-2005
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1560/6587
//                              PRD_ASCS_GEN_CA_49
//
//      COPYRIGHT       :       WM-Data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Charged Event Counters screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcChargedEventCountersScreenData;
import com.slb.sema.ppas.cc.service.CcChargedEventCountersDataResponse;
import com.slb.sema.ppas.cc.service.CcChargedEventCountersService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.dataclass.ChargedEventCounterTypeId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * Constructs a new servlet used to handle requests from the Charged Event Counter screen.
 */
public class CcChargedEventCountersServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcChargedEventCountersServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Customer Details Service to be used by this servlet. */
    private CcChargedEventCountersService i_ccChargedEventCountersServics = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcChargedEventCountersServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the customer details screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                      l_command     = null;
        String                      l_forwardUrl  = null;
        CcChargedEventCountersScreenData l_chargedEventCountersScreenData  = null;
        GuiResponse                 l_guiResponse = null;
        boolean                     l_accessGranted = false;
        GopaGuiOperatorAccessData   l_gopaData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 11000, this,
                "ENTERED " + C_METHOD_doService);
        }
        
        // Construct a new service if required.
        if (i_ccChargedEventCountersServics == null)
        {
            i_ccChargedEventCountersServics =
                         new CcChargedEventCountersService(p_guiRequest,
                                                           0,
                                                           i_logger,
                                                           i_guiContext);
        }

        // Create screen data object.
        l_chargedEventCountersScreenData =
            new CcChargedEventCountersScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true, // Mandatory Value
                                      "",   // Default Value
                                      new String [] {"GET_SCREEN_DATA",
                                                     "POST_ADJUSTMENT"});
            
            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("POST_ADJUSTMENT")) &&
                (l_gopaData.eventCountersUpdateIsPermitted()))
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("GET_SCREEN_DATA")) &&
                     (l_gopaData.hasEventCountersScreenAccess()))
            {
                l_accessGranted = true;
            }
            
            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(), //CR#60/571
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_chargedEventCountersScreenData.addRetrievalResponse(p_guiRequest,
                                                                      l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_chargedEventCountersScreenData);
            }
            else
            {
                if (l_command.equals("POST_ADJUSTMENT"))
                {
                    setIsUpdateRequest(true);

                    updateChargedEventCounter(p_guiRequest,
                                              p_request,
                                              l_chargedEventCountersScreenData);
                }

                getScreenData(p_guiRequest,
                              l_chargedEventCountersScreenData);
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_chargedEventCountersScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            p_request.setAttribute("p_guiScreenData", l_chargedEventCountersScreenData);
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

        } 

        if (l_accessGranted)
        {
            if (l_chargedEventCountersScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate the screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_chargedEventCountersScreenData);

            }
            else
            {
                // Data required for the screen has been successfully retrieved.
                l_forwardUrl = "/jsp/cc/ccchargedeventcounters.jsp";

                p_request.setAttribute("p_guiScreenData",
                                       l_chargedEventCountersScreenData);
            }
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 11099, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );

    } // end doService
    
    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getScreenData = "getScreenData";
    /**
     * Retrieves the data required for the Charged Event Counters screen.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data object for the Charged Event Counters screen used to store the 
     *                     response to the request.
     */
    private void getScreenData(GuiRequest p_guiRequest,
                               CcChargedEventCountersScreenData p_screenData)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 12000, this,
                "ENTERED " + C_METHOD_getScreenData);
        }
        
        CcChargedEventCountersDataResponse l_ccChargedEventCountersDataResponse = null;
        
        l_ccChargedEventCountersDataResponse = i_ccChargedEventCountersServics.
                                                   getChargedEventCounters(p_guiRequest, i_timeout);
        
        
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccChargedEventCountersDataResponse);

        if (l_ccChargedEventCountersDataResponse.isSuccess())
        {
            p_screenData.setAccumulatorEnquiryResultData(
                l_ccChargedEventCountersDataResponse.getAccumulatorEnquiryResultData());
        }
        
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 12099, this,
                "LEAVING " + C_METHOD_getScreenData);
        }
    }  // End method getScreenData(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateChargedEventCounter = "updateChargedEventCounter";
    /**
     * Posts and adjustment to the Charged Event Counters.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data object for the Charged Event Counters screen used to store the 
     *                     response to the request.
     * @throws PpasServletException
     */
    private void updateChargedEventCounter(GuiRequest p_guiRequest,
                                           HttpServletRequest p_request,
                                           CcChargedEventCountersScreenData p_screenData)
        throws PpasServletException
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 12100, this,
                "ENTERED " + C_METHOD_updateChargedEventCounter);
        }

        GuiResponse l_guiResponse = null;
        String l_counterIdStr = null;
        String l_totCounterAdjValStr = null;
        String l_periodCounterAdjValStr = null;
        String l_adjOperationStr = null;
        String l_newPeriodClrDateStr = null;
        String l_oldPeriodClrDateStr = null;
        ChargedEventCounterTypeId l_counterId = null;
        PpasDate l_periodClrDate = null;
        PpasDate l_currentTime = null;
        int l_totCounterAdjValInt;
        int l_periodCounterAdjValInt;
        
        l_counterIdStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "counterId",
                          true,
                          "",
                          PpasServlet.C_TYPE_ANY);

        l_totCounterAdjValStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "totCounterAdjVal",
                          true,
                          "",
                          PpasServlet.C_TYPE_NUMERIC);

        l_periodCounterAdjValStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "periodCounterAdjVal",
                          true,
                          "",
                          PpasServlet.C_TYPE_NUMERIC);

        l_adjOperationStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "adjOperation",
                          true,
                          "",
                          PpasServlet.C_TYPE_ANY);

        l_newPeriodClrDateStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "newPeriodClrDate",
                          true,
                          "",
                          PpasServlet.C_TYPE_DATE);

        l_oldPeriodClrDateStr =
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "oldPeriodClrDate",
                          true,
                          "",
                          PpasServlet.C_TYPE_DATE);

        
        try
        {
            l_counterId = new ChargedEventCounterTypeId(l_counterIdStr);
        }
        catch (PpasParseException p_ppasPE)
        {
            PpasServletException l_servE = 
                new PpasServletException(
                        C_CLASS_NAME,
                        C_METHOD_updateChargedEventCounter,
                        12150,
                        this,
                        p_guiRequest,
                        0,
                        ServletKey.get().chgEvtCounterIdNotDef(l_counterIdStr,p_guiRequest.getMsisdn()),
                        p_ppasPE);            

            i_logger.logMessage (l_servE);
            throw (l_servE);
        }
        
        if (l_newPeriodClrDateStr != null && !l_newPeriodClrDateStr.equals(l_oldPeriodClrDateStr))
        {
            l_periodClrDate = new PpasDate(l_newPeriodClrDateStr);
        }
        
        l_totCounterAdjValInt = Integer.parseInt(l_totCounterAdjValStr);
        
        l_periodCounterAdjValInt = Integer.parseInt(l_periodCounterAdjValStr);
        

        l_guiResponse = i_ccChargedEventCountersServics.updateChargedEventCounter(
                            p_guiRequest,
                            i_timeout,
                            l_counterId,
                            l_totCounterAdjValInt,
                            l_periodCounterAdjValInt,
                            l_adjOperationStr,
                            l_periodClrDate);
        
        
        p_screenData.addUpdateResponse( p_guiRequest,
                                        l_guiResponse );

        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();


        // Report on the success of the last update.
        if (p_screenData.hasUpdateError())
        {            
            p_screenData.setInfoText("Adjustment failed at " + l_currentTime);
        }
        else
        {
            p_screenData.setInfoText("Adjustment succeeded at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 12199, this,
                "LEAVING " + C_METHOD_updateChargedEventCounter);
        }
    } 

}
