////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcMemoService.java
//      DATE            :       02-Apr-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1343
//                              PRD_PPAK00_DEV_IN_39
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasMemoService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.MemoData;
import com.slb.sema.ppas.common.dataclass.MemoDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasMemoService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Gui Service that wrappers calls to PpasMemoService.
 */
public class CcMemoService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcMemoService";

    /** Flag that can be passed to indicate
     * whether to retrieve the Memo Details along with the Memo.
     */
    public static final long C_FLAG_INCLUDE_MEMO_DETAILS = 0x1;

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasMemoService i_memoService;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcMemoService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct erxceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcMemoService(GuiRequest p_guiRequest,
                         long       p_flags,
                         Logger     p_logger,
                         GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal memo service to be used by this service.
        i_memoService = new PpasMemoService(p_guiRequest,
                                            p_logger,
                                            p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getMemos = "getMemos";
    /**
     * Retrieves Memos for the subscriber.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_flags Defined flags: -
     *                C_FLAG_INCLUDE_MEMO_DETAILS - the Memos
     *                returned will contain the Memo Details for that Memo.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     *
     * @return A CcMemoDataSetResponse object containing the status of the
     *         requested service.
     */
    public CcMemoDataSetResponse getMemos(GuiRequest p_guiRequest,
                                          long       p_flags,
                                          long       p_timeoutMillis)
    {
        CcMemoDataSetResponse l_ccMemoDataSetResponse  = null;
        GuiResponse           l_guiResponse            = null;
        MemoDataSet           l_memoDataSet            = null;
        long                  l_flags                  = 0;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getMemos);
        }

        try
        {
            // Check what flags are set.
            if ((p_flags & C_FLAG_INCLUDE_MEMO_DETAILS)
                        == C_FLAG_INCLUDE_MEMO_DETAILS)
            {
                l_flags =
                    l_flags | PpasMemoService.C_FLAG_INCLUDE_MEMO_DETAILS;
            }

            l_memoDataSet = i_memoService.getMemos(p_guiRequest,
                                                   p_timeoutMillis);

            l_ccMemoDataSetResponse
                = new CcMemoDataSetResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                  new Object[] {"Get Memos"},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_memoDataSet);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 20210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get all memos", l_pSE);

            l_ccMemoDataSetResponse =
                new CcMemoDataSetResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getMemos);
        }

        return l_ccMemoDataSetResponse;
    }  // end public CcMemoDataSetResponse getMemos(...)

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSingleMemo = "getSingleMemo";
    /**
     * Retrieves a Memo of the subscriber.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_flags Defined flags: -
     *                C_FLAG_INCLUDE_MEMO_DETAILS - the Memo
     *                returned will contain the Memo Details for that Memo.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_raisedOn The Raised On Date/Time identifying the Memo to
     *                   be retrieved.
     *
     * @return A CcMemoDataResponse object containing the status of the
     *         requested service.
     */
    public CcMemoDataResponse getSingleMemo(GuiRequest   p_guiRequest,
                                            long         p_flags,
                                            long         p_timeoutMillis,
                                            PpasDateTime p_raisedOn)
    {
        CcMemoDataResponse l_ccMemoDataResponse  = null;
        GuiResponse        l_guiResponse         = null;
        MemoData           l_memoData            = null;
        long               l_flags               = 0;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 80000, this,
                "Entered " + C_METHOD_getSingleMemo);
        }

        try
        {
            // Check what flags are set.
            if ((p_flags & C_FLAG_INCLUDE_MEMO_DETAILS) == C_FLAG_INCLUDE_MEMO_DETAILS)
            {
                l_flags = l_flags | PpasMemoService.C_FLAG_INCLUDE_MEMO_DETAILS;
            }

            l_memoData = i_memoService.getSingleMemo(p_guiRequest, l_flags, p_timeoutMillis, p_raisedOn);

            l_ccMemoDataResponse
                = new CcMemoDataResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                  new Object[] {"Get Single Memo"},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_memoData);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 80210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get a memo", l_pSE);

            l_ccMemoDataResponse =
                new CcMemoDataResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 81000, this,
                "Leaving " + C_METHOD_getSingleMemo);
        }

        return l_ccMemoDataResponse;
    }  // end public CcMemoDataResponse getSingleMemo(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addMemo = "addMemo";
    /**
     * Add's a new Memo to the Account defined in the request.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_subject    The Subject of the Memo.
     * @param p_text       Text detail of the Memo.
     * @param p_isInfoMemo Indicates if the Memo is an Information only Memo,
     *                     I.e. It requires no further action and cannot
     *                     be updated in the future.  If false is passed
     *                     Memo is marked as Open and can be updated and/or
     *                     resolved in the future.
     *
     * @return A CcMemoDataResponse object containing the status of the
     *         requested service.
     */
    public CcMemoDataResponse addMemo(GuiRequest p_guiRequest,
                                      long       p_timeoutMillis,
                                      String     p_subject,
                                      String     p_text,
                                      boolean    p_isInfoMemo)
    {
        CcMemoDataResponse l_response    = null;
        GuiResponse        l_guiResponse = null;
        MemoData           l_memo        = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 30000, this,
                "Entered " + C_METHOD_addMemo);
        }

        try
        {
            l_memo = i_memoService.addMemo(p_guiRequest,
                                           p_timeoutMillis,
                                           p_subject,
                                           p_text,
                                           p_isInfoMemo);
            l_response
                = new CcMemoDataResponse(p_guiRequest,
                                         p_guiRequest.getGuiSession().
                                           getSelectedLocale(),  // PpacLon#1/17
                                         GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                         new Object[] {"Add Memo"},
                                         GuiResponse.C_SEVERITY_SUCCESS,
                                         l_memo);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 71210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "add the memo", l_pSE);

            l_response =
                new CcMemoDataResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 31000, this,
                "Leaving " + C_METHOD_addMemo);
        }

        return l_response;
    } // end addMemo(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateMemo = "updateMemo";
    /**
     * Updates a Memo with a new Record and/or Resolves the Memo.
     * As a minimum, either <code>p_text</code> must not be an empty String ("")
     * or <code>p_resolve</code> must be true.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_text      New text to add as a new record of the Memo.
     *                    If en empty String ("") is given, no record is added.
     * @param p_raisedOn  The Raised On Date/Time identifying the Memo being updated.
     * @param p_resolve   Indicates if the Memo is be marked as resolved.
     *
     * @return A CcMemoDataResponse object containing the status of the
     *         requested service.
     */
    public CcMemoDataResponse updateMemo(GuiRequest   p_guiRequest,
                                         long         p_timeoutMillis,
                                         String       p_text,
                                         PpasDateTime p_raisedOn,
                                         boolean      p_resolve)
    {
        CcMemoDataResponse l_response    = null;
        GuiResponse        l_guiResponse = null;
        MemoData           l_memo        = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 20000, this,
                "Entered " + C_METHOD_updateMemo);
        }

        try
        {
            l_memo = i_memoService.updateMemo(p_guiRequest,
                                              p_timeoutMillis,
                                              p_text,
                                              p_raisedOn,
                                              p_resolve);

            l_response
                = new CcMemoDataResponse(p_guiRequest,
                                         p_guiRequest.getGuiSession().
                                           getSelectedLocale(),  // PpacLon#1/17
                                         GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                         new Object[] {"Update Memo"},
                                         GuiResponse.C_SEVERITY_SUCCESS,
                                         l_memo);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "update the memo", l_pSE);
            l_response =
                new CcMemoDataResponse
                                 (p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 21000, this,
                "Leaving " + C_METHOD_updateMemo);
        }

        return l_response;
    } // end public GuiResponse updateMemo(...)
} // end public class CcMemoService
