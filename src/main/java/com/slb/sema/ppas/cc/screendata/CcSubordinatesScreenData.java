////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubordinatesScreenData.java
//      DATE            :       28-March-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon#5287/1325
//                              PRD_PPAK00_DEV_IN_037
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Subordinates
//                              screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.AccountDataSet;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
/**
 * Gui Screen Data object for the Subordinates screen.
 */
public class CcSubordinatesScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccountDataSet object wrapped by this object. */
    private AccountDataSet     i_subordinateDataSet = null;

    /** AccountData object wrapped by this object. */
    private AccountData        i_accountData        = null;

    /** BasicAccountData object wrapped by this object. */
    private BasicAccountData   i_masterBasicData    = null;

    /** Text giving information on the status of any add subordinate request
     *  that was attempted.
     */   
    private String             i_infoText           = "";

    /** Flag to indicate whether a failed add subordinate request has occured. */
    private boolean            i_addSubFailed       = false;

    /** Flag to indicate whether a successful add subordinate request has occurred. */
    private boolean            i_subAdded           = false;

    /** Subordinate msisdn. */
    private String             i_subMsisdn          = "";

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcSubordinatesScreenData( GuiContext p_guiContext,
                                     GuiRequest p_guiRequest)
    {
        super( p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns <code>true</code> if the Operator making the request is 
     * permitted to add a Subordinate to an account.  
     * Otherwise <code>false</code> is returned.
     * @return True if the operator can add a subordinate.
     */
    public boolean addSubordinateIsPermitted()
    {
        return (i_gopaData.addSubordinateIsPermitted());
    }

    /**
     * Returns the AccountDataSet of subordinates associated with the account in
     * in the request.
     * @return Set of account data.
     */
    public Vector getSubordinates()
    {
        return ( i_subordinateDataSet.getData() );
    }

    /**
     * Returns the AccountData of the subscriber in the request.
     * @return Account data.
     */
    public AccountData getAccountData()
    {
        return ( i_accountData );
    }

    /**
     * Returns the BasicAccountData of the master of the account in the request.
     * @return Basic account data.
     */
    public BasicAccountData getMasterBasicData()
    {
        return ( i_masterBasicData );
    }

    /** Sets the subordinates AccountDataSet object wrapped within this object. 
     * @param p_accountDataSet Set of subordinate account data.
     */
    public void setAccountDataSet(
                    AccountDataSet p_accountDataSet)
    {
        i_subordinateDataSet = p_accountDataSet;
    }

    /** Sets the accountData of the subscriber in the request.
     * @param p_accountData Account data.
     */
    public void setAccountData(
                    AccountData p_accountData)
    {
        i_accountData = p_accountData;
    }

    /** Sets the BasicAccountData object of the master of the account
     *  in the request.
     * @param p_basicAccountData Master account data.
     */
    public void setMasterBasicData(
                    BasicAccountData p_basicAccountData)
    {
        i_masterBasicData = p_basicAccountData;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return (i_subordinateDataSet != null && i_accountData != null);
    }

    /** Sets the text giving information on the status on any attempts to add
     *  a subordinate.
     * @param p_text Description of status from an attempt to add a subordinate.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving information on the status on any attempts to add
     *  a subordinate.
     * @return Description of status from an attempt to add a subordinate.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Sets flag to indicate whether a subordinate has been successfully added.
     * @param p_success Flag indicating a subordinate has been added.
     */
    public void setSubAdded(boolean p_success)
    {
        i_subAdded = p_success;
    }

    /** Gets flag to indicate whether a subordinate has been successfully added.
     * @return True if a subordinate has been added.
     */
    public boolean isSubAdded()
    {
        return(i_subAdded);
    }

    /** Sets flag to indicate whether add subordinate process failed.
     * @param p_failure Flag to indicate an attempt to add a subordinate failed.
     */
    public void setAddSubFailed(boolean p_failure)
    {
        i_addSubFailed = p_failure;
    }

    /** Gets flag to indicate whether add subordinate process failed.
     * @return True if an attempt to add a subordinate failed.
     */
    public boolean isAddSubFailed()
    {
        return(i_addSubFailed);
    }
    

    /** Determines if the account is a subordinate account or not.
     *  @return True if the account is a subordinate account. Otherwise false.
     *  If data is not loaded then will go to the error page.
     */
    public boolean isSubordinate()
    {
            
        return (i_accountData.isSubordinate());
    }

    /** Sets value of Subordinate msisdn.
     * @param p_subMsisdn Subordinate MSISDN.
     */
    public void setSubMsisdn(String p_subMsisdn)
    {
        i_subMsisdn = p_subMsisdn;
    }

    /** Returns value of Subordinate msisdn.
     * @return Subordinate MSISDN.
     */
    public String getSubMsisdn()
    {
         return(i_subMsisdn);
    }
} // end public class CcSubordinateScreenData
