////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentServlet.Java
//      DATE            :       04-Mar-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1295/5175
//                              PRD_PPAK00_DEV_IN_032
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Adjustments screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//07/10/03  | Chris      | Privilege checks added          | CR#60/571
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcAdjustmentScreenData;
import com.slb.sema.ppas.cc.service.CcAdjustmentDataSetResponse;
import com.slb.sema.ppas.cc.service.CcAdjustmentService;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;




/**
 * Servlet to handle requests from the Adjustment Screen.
 */
public class CcAdjustmentServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdjustmentServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Adjustment Service to be used by this servlet. */
    private CcAdjustmentService i_ccAdjustmentService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcAdjustmentServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 
                10000, 
                this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 
                10090, 
                this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise this object.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 
                20000, 
                this,
                "Entered doInit().");
        }

        // Should probably be constructing CcAdjustmentService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 
                20090, 
                this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the account details screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                      l_command              = null;
        String                      l_forwardUrl           = null;
        CcAdjustmentScreenData      l_adjustmentScreenData = null;
        GuiResponse                 l_guiResponse          = null;
        boolean                     l_accessGranted        = false;
        GopaGuiOperatorAccessData   l_gopaData             = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 
                13000, 
                this,
                "ENTERED " + C_METHOD_doService);
        }

        if (i_ccAdjustmentService == null)
        {
            i_ccAdjustmentService = new CcAdjustmentService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create screen data object.
        l_adjustmentScreenData = new CcAdjustmentScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true ,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "APPLY"});

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasAdjustmentsScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("APPLY")) && l_gopaData.makeAdjustmentIsPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_adjustmentScreenData.addRetrievalResponse(p_guiRequest,
                                                            l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData",
                                        l_adjustmentScreenData);
            }
            else
            {
                getBusinessConfigData(p_guiRequest,
                                      l_adjustmentScreenData);

                if (l_command.equals("APPLY"))
                {
                    setIsUpdateRequest(true);

                    postAdjustment(p_guiRequest,
                                   p_request,
                                   l_adjustmentScreenData);
                }

                getAdjustments(p_guiRequest,
                               l_adjustmentScreenData);
            }

        } // end try

        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 
                    73872, 
                    this,
                    "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_adjustmentScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
        } // end catch

        if (l_accessGranted)
        {
            if (l_adjustmentScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Adjustments screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_adjustmentScreenData);
            }
            else
            {
                // CcAccountDetailsScreenData object has the necessary data
                // to populate the Adjustments screen.
                l_forwardUrl = "/jsp/cc/ccadjustments.jsp";

                p_request.setAttribute("p_guiScreenData", l_adjustmentScreenData);
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 
                13035, 
                this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_postAdjustment =
                                                    "postAdjustment";
    /**
     * Posts an adjustment to the given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void postAdjustment(GuiRequest             p_guiRequest,
                                HttpServletRequest     p_request,
                                CcAdjustmentScreenData p_screenData)
    {
        GuiResponse        l_guiResponse  = null;
        Money              l_amount       = null;
        String             l_type         = null;
        String             l_code         = null;
        String             l_description  = null;
        PpasDateTime       l_currentTime  = null;
        String             l_failureReasonKey;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           27000, 
                           this,
                          "Entered " + C_METHOD_postAdjustment);
                
        }

        // Get params from request...

        try
        {
            String l_amountAsString = getValidParam(p_guiRequest,
                                                    PpasServlet.C_FLAG_STRIP_PLUS_SIGN,
                                                    p_request,
                                                    "p_amount",
                                                    true,
                                                    "",
                                                    PpasServlet.C_TYPE_SIGNED_NUMERIC_WITH_DECIMALS);
            
            String l_ccyAsAstring = getValidParam(p_guiRequest,
                                                  0,
                                                  p_request,
                                                  "p_currency",
                                                  true,
                                                  "",
                                                  PpasServlet.C_TYPE_ANY);
            try
            {
                l_amount = i_guiContext.getMoneyFormat().parse(l_amountAsString,
                                                               i_guiContext.getCurrency(l_ccyAsAstring));

            }
            catch (ParseException l_pe)
            {
                PpasServletException l_pSE =
                    new PpasServletException(
                            C_CLASS_NAME,
                            C_METHOD_postAdjustment,
                            10121,
                            this,
                            p_guiRequest,
                            0,
                            ServletKey.get().parseMoneyError(l_amountAsString,
                                                             i_guiContext.getCurrency(l_ccyAsAstring)),
                            l_pe);
                
                i_logger.logMessage(l_pSE);
                throw l_pSE;
            }

            l_type = getValidParam(p_guiRequest,
                                   0,
                                   p_request,
                                   "p_adjustmentTypes",
                                   true,
                                   "",
                                   PpasServlet.C_TYPE_ANY);

            l_code = getValidParam(p_guiRequest,
                                   0,
                                   p_request,
                                   "p_adjustmentCodes",
                                   true,
                                   "",
                                   PpasServlet.C_TYPE_ANY);
            l_description = getValidParam(p_guiRequest,
                                          PpasServlet.C_FLAG_ALLOW_BLANK,
                                          p_request,
                                          "p_description",
                                          false,
                                          "",
                                          PpasServlet.C_TYPE_ANY);

        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest,
                               C_CLASS_NAME, 
                               27300, 
                               this,
                               "Exception parsing parameters: " + l_pSE);
            }
                    
            // Convert the exception into a suitable response...

            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        // Now check what parameters are being updated and call the required
        // service(s).

        if (p_screenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do not attempt
            // this requested service.
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"post adjustment"},
                                GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            // An Adjustment is being posted.
            l_guiResponse = i_ccAdjustmentService.postAdjustment(p_guiRequest,
                                                                 i_timeout,
                                                                 l_amount,
                                                                 l_type,
                                                                 l_code,
                                                                 l_description,
                                                                 new Boolean(false));

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);

            // Was adjustment successfully posted ?
            if (!l_guiResponse.isSuccess())
            {
                // The attempt to post the adjustment failed ...
                l_failureReasonKey = l_guiResponse.getKey();

                // Was the failure reason because the operator's adjustment
                // limits would have been exceeded?
                if (l_failureReasonKey.equals(GuiResponse.C_KEY_EXCEEDED_MAX_ADJUSTMENT_AMOUNT) ||
                    l_failureReasonKey.equals(GuiResponse.C_KEY_EXCEEDED_DAILY_ADJUSTMENT_LIMIT))
                {
                    p_screenData.setPostedForApproval();
                }

            } // end (adjustment failed)

        } // end else (screen has error)

        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            if (p_screenData.wasPostedForApproval())
            {
                p_screenData.setInfoText("Adjustment posted for approval at " + l_currentTime);
            }
            else
            {
                p_screenData.setInfoText("Failed Adjustment at " + l_currentTime);
            }
        }
        else
        {
            p_screenData.setInfoText("Successful Adjustment at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           27090, 
                           this,
                           "Leaving " + C_METHOD_postAdjustment);
        }
        return;
    } // end private void postAdjustment(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAdjustments =
                                                    "getAdjustments";

    /**
     * Gets the Adjustment history for the given account.
     * @param  p_guiRequest The GUI request being processed.
     * @param  p_screenData Data associated with the Adjustment screen.
     * @throws PpasServletException If an error occurs processing this request.
     */
    private void getAdjustments(GuiRequest             p_guiRequest,
                                CcAdjustmentScreenData p_screenData)
        throws PpasServletException
    {
        CcAdjustmentDataSetResponse  l_ccAdjustmentDataSetResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           37000,
                           this,
                          "Entered " + C_METHOD_getAdjustments);
        }

        l_ccAdjustmentDataSetResponse = i_ccAdjustmentService.getAdjustments(p_guiRequest,
                                                                             i_timeout);
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccAdjustmentDataSetResponse);

        if (l_ccAdjustmentDataSetResponse.isSuccess())
        {
            p_screenData.setAdjustmentDataSet(
                          l_ccAdjustmentDataSetResponse.getAdjustmentDataSet());

        }

        if (!(p_screenData.hasRetrievalError()))
        {
            p_screenData.setPreferredCurrency( ((GuiSession)p_guiRequest.getGuiSession())
                                               .getPreferredCurrency());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           37090,
                           this,
                           "Leaving " + C_METHOD_getAdjustments);
        }

        return;

    }


    /**
     *  Obtains the business configuration data.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void getBusinessConfigData(GuiRequest             p_guiRequest,
                                       CcAdjustmentScreenData p_screenData)
    {
        SrvaAdjCodesDataSet         l_adjCodes            = null;
        CufmCurrencyFormatsDataSet  l_availableCurrencies = null;

        l_adjCodes = i_configService.getAvailableAdjCodes(
                                        ((GuiSession)p_guiRequest.getSession()).getCsoMarket());

        p_screenData.setAdjustmentCodes(l_adjCodes);

        l_availableCurrencies = i_configService.getAvailableCurrencyFormats();
        p_screenData.setCurrencyDataSet(l_availableCurrencies);
    }
} // end class ccAdjustmentServlet
