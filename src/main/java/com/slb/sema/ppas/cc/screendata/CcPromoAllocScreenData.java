////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromoAllocScreenData.java
//      DATE            :       13-May-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Gui Screen Data object for the Customer
//                              Promotion allocation pop-up windows.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/01/06 | K Goswami  | Changed the alloc date setter   | PpacLon#1058 
//          |            | method arguments to Date        | CrRr_1058_7237_PromoAllocData.doc
//----------+------------+---------------------------------+--------------------
// 10/03/06 | M Erskine  | Use the division id associated  | PpacLon#2005
//          |            | with the promotion to get the   | PRD_ASCS00_GEN_CA_68
//          |            | VOSP row. This is really a fix  |
//          |            | for PpacLon#1334.               |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsDataSet;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Gui Screen Data object for the Customer Promotion Allocation pop-up screen.
 * Contains all data required for that screen display and helper methods to
 * reduce the amount of code required in the JSP script for that screen.
 */
public class CcPromoAllocScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Public class constants.
    //--------------------------------------------------------------------------

    // Define constants to represent which value of the 'allocation start 
    // date' radio button should be checked on the screen

    /** Indicates that the 'now' value of the 'allocation start date' radio 
     *  button should be initilly checked.
     */
    public static final int C_START_ALLOC_NOW_CHECKED = 1;

    /** Indicates that the 'on date' value of the 'allocation start date' radio 
     *  button should be initilly checked.
     */
    public static final int C_START_ALLOC_ON_DATE_CHECKED = 2;


    // Define constants to represent which value of the 'allocation end
    // date' radio button should be checked on the screen

    /** Indicates that the 'now' value of the 'allocation end date' radio 
     *  button should be initilly checked.
     */
    public static final int C_END_ALLOC_NOW_CHECKED = 1;

    /** Indicates that the 'at end of today' value of the 'allocation end
     *  date' radio button should be initilly checked.
     */
    public static final int C_END_ALLOC_END_OF_TODAY_CHECKED = 2;

    /** Indicates that the 'on date' value of the 'allocation end
     *  date' radio button should be initilly checked.
     */
    public static final int C_END_ALLOC_ON_DATE_CHECKED = 3;


    //--------------------------------------------------------------------------
    // Private class constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPromoAllocScreenData";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** The plan identifier of the allocation that this object defines. */
    private String i_planId;

    /** The start date/time of the allocation that this object defines. */
    private PpasDate i_allocStartDate;

    /** The end date/time of the allocation that this object defines. */
    private PpasDate i_allocEndDate;

    /** The promotions related business configuration data. */
    private PrplPromoPlanDataSet   i_promoConfigData;

    /** Determines whether this allocation is a new or existing allocation. */
    private boolean i_isExistingAlloc;

    /** True if the allocation represented by this screen data object is the
     *  customers current allocation. 
     */
    private boolean i_isCurrent;

    /** True if the allocation represented by this screen data object is a
     *  future allocation for the customer.
     */
    private boolean i_isFuture;

    /** Determines what the state of the 'Allocation start date' radio button
     *  should initially be.
     */
    private int     i_allocStartRadioButton;

    /** Determines what the state of the 'Allocation end date' radio button
     *  should initially be.
     */
    private int     i_allocEndRadioButton;

    /** Business configuration data from the vosp_voucher_splits table.
     *  This defines how recharges are divided over dedicated accounts.
     */
    private VospVoucherSplitsDataSet  i_vospConfigData;

    /** Defines the division that is associated with the promotion of this
     *  allocation.
     */
    private VospVoucherSplitsData     i_promoDivision;

    /** Service class of the subscriber who hold's this allocation. */
    private ServiceClass  i_serviceClass;

    /** Dedicated account business configuration valid in the market of the 
     *  current subscriber.
     */
    private DedaDedicatedAccountsDataSet i_dacDataSet;


    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Constructor; creates an 'empty' promotion allocation screen data object
     *  ie not containing the data required for the screen generation.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcPromoAllocScreenData(
        GuiContext p_guiContext,
        GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 95000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_planId                = null;
        i_allocStartDate        = new PpasDateTime ("");
        i_allocEndDate          = new PpasDateTime ("");
        i_promoConfigData       = null;
        i_isExistingAlloc       = false;
        i_isCurrent             = false;
        i_isFuture              = false;
        i_allocStartRadioButton = C_START_ALLOC_ON_DATE_CHECKED;
        i_allocEndRadioButton   = C_END_ALLOC_ON_DATE_CHECKED;
        i_serviceClass          = null;

        setAsPopup();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 95090, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns <code>true</code> if the Operator who generated the request
     * is permitted to update Promotions details
     * Otherwise <code>false</code> is returned.
     * @return True if update is permitted.
     */
    public boolean promotionsUpdateIsPermitted()
    {
        return (i_gopaData.promotionsUpdateIsPermitted());
    }

    /** Set up the plan Id of the allocation that this screen data object
     *  defines.
     *  @param p_planId Plan Id of the allocation that this screen data object
     *                  defines.
     */
    public void setPlanId (String p_planId)
    {
        i_planId = p_planId;
    }

    /** Get the plan Id of the allocation that this screen data object defines.
     *  @return Plan Id of the allocation that this screen data object defines.
     */
    public String getPlanId ()
    {
        return (i_planId);
    }

    /** Get the promotion plan start date as a PpasDate object.
     * @param p_planId Promotional plan identifier.
     *  @return (PpasDate) Promotion start date.
     */
    public PpasDate getPromStartDate(String p_planId)
    {
        return ( getPromoPlanData(p_planId).
                 getStartDateTime().
                 getPpasDate() );
    }

    /** Get the promotion plan end date as a PpasDate object.
     * @param p_planId Promotional plan identifier.
     *  @return (PpasDate) Promotion end date.
     */
    public PpasDate getPromEndDate(String p_planId)
    {
        return ( getPromoPlanData(p_planId).
                 getEndDateTime().
                 getPpasDate() );
    }

    /** Set the start date/time of the allocation that this screen data object
     *  defines.
     *  @param p_allocStartDate The start date/time of the allocation that 
     *                          this screen data object defines.
     */
    public void setAllocStartDate (PpasDate p_allocStartDate)
    {
        i_allocStartDate = p_allocStartDate;
    }

    /** Get the start date/time of the allocation that this screen data object
     *  defines in string format.
     *  @return The start date/time of the allocation that this screen data
     *          object defines.
     */
    public String getAllocStartDate ()
    {
        return (i_allocStartDate.toString());
    }

    /** Set the end date/time of the allocation that this screen data object
     *  defines.
     *  @param p_allocEndDate The end date/time of the allocation that this 
     *                        screen data object defines.
     */
    public void setAllocEndDate (PpasDate p_allocEndDate)
    {
        i_allocEndDate = p_allocEndDate;
    }

    /** Get the end date/time of the allocation that this screen data object
     *  defines in string format.
     *  @return The end date/time of the allocation that this screen data
     *          object defines.
     */
    public String getAllocEndDate ()
    {
        return (i_allocEndDate.toString());
    }


    /** Sets the boolean flags indicating whether the allocation defined by 
     *  this screen object is the current or a future allocation.
     * @param p_request The request being processed.
     * @param p_logger  The logger to direct messages.
     */
    public void setCurrentOrFuture (GuiRequest p_request,
                                    Logger     p_logger)
    {
        PpasDateTime l_now;

        l_now = DatePatch.getDateTimeNow ();

        // If start date time has been set and is before current date time ...
        if ((i_allocStartDate != null) &&
            (i_allocStartDate.before(l_now)))
        {
            // ... and end date time has been set up AND
            //     either end date is blank or is after current date time
            if ((i_allocEndDate != null) &&
                ((!i_allocEndDate.isSet()) || (i_allocEndDate.after(l_now))))
            {
                // ... then this allocation is the customer's current one.
                i_isCurrent = true;
            }
        }

        // If start date time has been set and is after current date time ...
        if ((i_allocStartDate != null) &&
            (i_allocStartDate.after(l_now)))
        {
            // ... then this allocation is a future one.
            i_isFuture = true;
        }

        return;

    } // end method 'setCurrentOrFuture'

    /** Determines whether the allocation defined by this screen data object
     *  is the current allocation for the customer. This method should only
     *  be used after the <code>setCurrentOrFuture</code> method of this
     *  class has been called.
     *  @return <code>true</code> if this allocation is the customer's current
     *          allocation; otherwise, <code>false</code>.
     */
    public boolean isCurrent()
    {
        return (i_isCurrent);
    }

    /** Determines whether the allocation defined by this screen data object
     *  is a future allocation for the customer. This method should only
     *  be used after the <code>setCurrentOrFuture </code> method of this
     *  class has been called.
     *  @return <code>true</code> if this allocation is a future allocation 
     *          for the customer; otherwise, <code>false</code>.
     */
    public boolean isFuture()
    {
        return (i_isFuture);
    }

    /** Set up the business configuration data relating to promotions.
     *  @param p_promoConfigData The business configuration data relating to
     *                           promotions.
     */
    public void setPromoConfigData (PrplPromoPlanDataSet p_promoConfigData)
    {
        i_promoConfigData = p_promoConfigData;
    }

    /** Defines whether the allocation that this screen data is for is a new
     *  or an existing allocation.
     *  @param p_isExisting <code>true</code> to set this allocation as an
     *                      existing allocation; otherwise, use 
     *                      <code>false</code>.
     */
    public void setExistingAlloc (boolean p_isExisting)
    {
        i_isExistingAlloc = p_isExisting;
    }

    /** Returns boolean determining whether the promotion allocation 
     *  represented by this screen data object defines an existing allocation
     *  or represents a new allocation for the customer.
     *  @return <code>true</code> if this object represents an existing 
     *          promotion allocation for the customer; otherwise, 
     *          <code>false</code>
     */
    public boolean isExistingAlloc ()
    {
        return (i_isExistingAlloc);
    }

    /** Get promotion business configuration data for a given promotion plan.
     *  @param p_planId The identifier of the promotion plan to obtain the
     *                  business configuration data for.
     *  @return Promotion plan configuration for the given
     *          plan. If the given plan cannot be found or is marked as
     *          deleted, then <code>null</code> is returned.
     */
    public PrplPromoPlanData getPromoPlanData (String p_planId)
    {
        PrplPromoPlanData l_promoData = null;
        PpasDate          l_allocStartDate;

        if (i_promoConfigData != null)
        {
            l_promoData = i_promoConfigData.getWithCode (p_planId);

            if (l_promoData != null)
            {
                if (l_promoData.isDeleted())
                {
                    l_promoData = null;
                }
                else
                {
                    // Null passed in for request in getting date from 
                    // date/time - should be ok since request is not really
                    // used by this method.
                    l_allocStartDate = 
                        l_promoData.getStartDateTime().getPpasDate();

                    setPromoDivision (l_allocStartDate,
                                      p_planId);
                }
            }
        }
        return (l_promoData);
    }

    /** This method returns an array of available promotion plan identifiers.
     *  @param p_firstInList This is the identifier of the plan that is to be
     *                       the first element in the array of plan identifiers
     *                       returned. If this element is <code>null</code> or
     *                       an empty String, then the available promotion plan
     *                       identifiers are returned in alphabetically 
     *                       ascending order.
     * @return An array of available promotion plan identifiers.
     */
    public String [] getPromPlanIds (String p_firstInList)
    {
        String [] l_promPlanIds = new String [1];
        PrplPromoPlanData [] l_promData;

        l_promPlanIds[0] = "";

        if (i_promoConfigData != null)
        {
            l_promData = i_promoConfigData.getAvailableArray();

            l_promPlanIds = new String [l_promData.length];

            for (int l_index = 0; l_index < l_promData.length; l_index++)
            {
                l_promPlanIds[l_index] = l_promData[l_index].getPromoPlan();
            }

            sortStringArray (p_firstInList, true, l_promPlanIds);
        }

        return (l_promPlanIds);
    }

    /** Returns true if this screen data object has been populated. Otherwise
     *  returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        boolean l_populated = false;

        if ((i_promoConfigData != null) &&
            (i_planId != null) &&
            (i_allocStartDate != null) &&
            (i_allocEndDate != null))
        {
            l_populated = true;
        }
        return (l_populated);
    }

    /** Returns <code>true</code> if promotions are configured in the PPAS 
     *  database and have been set up in this screen data object.
     *  @return <code>true</code> if promotions data is available in this
     *          object; otherwise, returns <code>false</code>.
     */
    public boolean promotionConfigured()
    {
        boolean l_promosConfigured = false;

        if (i_promoConfigData != null)
        {
            if (i_promoConfigData.getAvailableArray() != null)
            {
                if (i_promoConfigData.getAvailableArray().length > 0)
                {
                    l_promosConfigured = true;
                }
            }
        }
        return (l_promosConfigured);
    }

    /** Sets the state that the 'Allocation start date' radio button
     *  should be in.
     *  @param p_allocStartRadioButtonValue Defines value for the 'allocation
     *                                      start date' radio button. Valid
     *                                      values are:
     *                                      <br>C_START_ALLOC_NOW_CHECKED
     *                                      <br>C_START_ALLOC_ON_DATE_CHECKED.
     */
    public void setAllocStartRadioButton (int p_allocStartRadioButtonValue)
    {
        i_allocStartRadioButton = p_allocStartRadioButtonValue;
    }

    /** Gets the state that the 'Allocation start date' radio button
     *  should be in.
     *  @return State that the 'Allocation start date' radio button should be
     *          in. Possible values are:
     *          <br>C_START_ALLOC_NOW_CHECKED
     *          <br>C_START_ALLOC_ON_DATE_CHECKED.
     */
    public int getAllocStartRadioButton()
    {
        return (i_allocStartRadioButton);
    }

    /** Sets the state that the 'Allocation end date' radio button
     *  should be in.
     *  @param p_allocEndRadioButtonValue Defines value for the 'allocation
     *                                    end date' radio button. Valid values
     *                                    are:
     *                                    <br>C_END_ALLOC_NOW_CHECKED
     *                                    <br>C_END_ALLOC_END_OF_TODAY_CHECKED
     *                                    <br>C_END_ALLOC_ON_DATE_CHECKED.
     */
    public void setAllocEndRadioButton (int p_allocEndRadioButtonValue)
    {
        i_allocEndRadioButton = p_allocEndRadioButtonValue;
    }

    /** Gets the state that the 'Allocation end date' radio button
     *  should be in.
     *  @return State that the 'Allocation end date' radio button should be
     *          in. Valid values are:
     *          <br>C_END_ALLOC_NOW_CHECKED
     *          <br>C_END_ALLOC_END_OF_TODAY_CHECKED
     *          <br>C_END_ALLOC_ON_DATE_CHECKED.
     */
    public int getAllocEndRadioButton()
    {
        return (i_allocEndRadioButton);
    }

    /** Set service class of the subscriber who hold's this allocation.
     *  @param p_serviceClass The service class of the subscriber who hold's
     *                        this allocation.
     */
    public void setServiceClass (ServiceClass p_serviceClass)
    {
        i_serviceClass = p_serviceClass;
        return;
    }

    /** Sets the business configuration data from the vosp_voucher_splits table.
     *  This defines how recharges are divided over dedicated accounts.
     *  @param p_vospConfigData The voucher split business configuration
     *                          data to be set.
     */
    public void setVospConfigData (VospVoucherSplitsDataSet p_vospConfigData)
    {
        i_vospConfigData = p_vospConfigData;
        return;
    }

    /** Sets the dedicated account business configuration defined for the 
     *  market of the current subscriber.
     *  @param p_dacDataSet The dedicated account business configuration defined
     *                      for the current subscriber's market.
     */
    public void setDacDataSet (DedaDedicatedAccountsDataSet p_dacDataSet)
    {
        i_dacDataSet = p_dacDataSet;
        return;
    }

    /** Obtains the division associated with the promotion of this allocation
     *  and stores it in the attribute <code>i_promoDivision</code>.
     *  @param p_divisionDate Date on which the division must be active.
     *  @param p_planId The promotion plan to be used in identifying the 
     *                  division.
     */
    private void setPromoDivision (PpasDate    p_divisionDate,
                                   String      p_planId)
    {
        String l_divisionId = null;
        
        if (i_promoConfigData != null)
        {
            l_divisionId = i_promoConfigData.getWithCode(p_planId).getDivisionId();
        }
        
        if (i_vospConfigData != null)
        {
            i_promoDivision = i_vospConfigData.getDivision(
                                   p_divisionDate,
                                   i_serviceClass,
                                   l_divisionId);                                   
        }

        return;

    } // end method 'setPromoDivision'

    /** Get the division associated with the promotion of this allocation.
     *  @return The division associated with the promotion of this allocation.
     */
    public VospVoucherSplitsData getPromoDivision()
    {
        return (i_promoDivision);
    }

    /** Get dedicated account description.
     * 
     * @param p_dacId Dedicated account identifier.
     * @return Description of the specified deeicated account.
     */
    public String getDacDescription (int p_dacId)
    {
        String l_dacDesc = "";

        if (i_dacDataSet != null)
        {
            l_dacDesc = i_dacDataSet.getDescription (i_serviceClass,
                                                     p_dacId);
        }
        return (l_dacDesc);
    }

} // end public class CcPromoAllocScreenData
