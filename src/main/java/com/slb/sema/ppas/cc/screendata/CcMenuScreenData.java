////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcMenuScreenData.java
//      DATE            :       15-Aug-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1499/6262
//                              PRD_PPAK00_GEN_CA_379
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the 
//                              Menu bar.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//08/12/03  | Remi       | hasCommentsScreenAccess method  | CR#60/571
//          | Isaacs     | added                           |
//----------+------------+---------------------------------+--------------------
// 26/05/06 | S James    | Add hasCallHistoryScreenAccess  | PpacLon#2188/8944
//          |            | method                          |
//----------+------------+---------------------------------+--------------------
// 08/06/07 | Andy Harris| Add Statement of Account        | PpacLon#3124/11683
//          |            | methods.                        | PRD_ASCS00_GEN_CA_114
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Menu bar.
 */
public class CcMenuScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** Indicate whether Accumulators are Configured. */
    private  boolean        i_accumulatorsConfigured = false;

    /** Indicate whether Additional info is Configured. */
    private  boolean        i_isMfltMiscFieldTitles;
    
    /** Has Subscriber Segmentation Feature Licence. */
    private boolean i_hasSubscriberSegmentationFeatureLicence = false;

    /** Has Family and Friends Feature Licence. */
    private boolean i_hasFamilyAndFriendsFeatureLicence = false;

    /** Has Community Charging Feature Licence. */
    private boolean i_hasCommunityChargingFeatureLicence = false;
    
    /** Has Charged Event Counters Feature Licence. */
    private boolean i_hasChargedEventCountersFeatureLicence = false;
    
    /** Has Call History Feature Licence. */
    private boolean i_hasCallHistoryFeatureLicence = false;
    
    /** Has Service Fee Deduction Feature Licence. */
    private boolean i_hasServiceFeeDeductionFeatureLicence = false;
    
    /** Has Statement of Account Feature Licence. */
    private boolean i_hasStatementOfAccountFeatureLicence = false;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcMenuScreenData(GuiContext p_guiContext,
                            GuiRequest p_guiRequest)
    {
       super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Subscriber Details screen.
      * @return True if the operator has access to the Subscriber Details screen.
      */
    public boolean hasSubsDetailsScreenAccess()
    {
        return (i_gopaData.hasSubsDetailsScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Vouchers screen.
      * @return True if the operator has access to the Vouncher screen.
      */
    public boolean hasVouchersScreenAccess()
    {
        return (i_gopaData.hasVouchersScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Payments screen.
      * @return True if the operator has access to the Payments screen.
      */
    public boolean hasPaymentsScreenAccess()
    {
        return (i_gopaData.hasPaymentsScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Adjustments screen.
      * @return True if the operator has access to the Adjustments screen.
      */
    public boolean hasAdjustmentsScreenAccess()
    {
        return (i_gopaData.hasAdjustmentsScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Promotions screen.
      * @return True if the operator has access to the Promotions screen.
      */
    public boolean hasPromotionsScreenAccess()
    {
        return (i_gopaData.hasPromotionsScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Disconnection screen.
      * @return True if the operator has access to the Disconnect screen.
      */
    public boolean hasDisconnectScreenAccess()
    {
        return (i_gopaData.hasDisconnectScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Subordinates screen.
      * @return True if the operator has access to the Subordinates screen.
      */
    public boolean hasSubordinatesScreenAccess()
    {
        return (i_gopaData.hasSubordinatesScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Additional Information screen.
      * @return True if the operator has access to the Additional Information screen.
      */
    public boolean hasAddInfoScreenAccess()
    {
        return (i_gopaData.hasAddInfoScreenAccess());
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the screen
      * Subscriber Segmentation.
      * @return True if the operator has access to the Subscriber Segmentation screen.
      */
    public boolean hasSubscriberSegmentationScreenAccess()
    {
        return (i_gopaData.hasSubscriberSegmentationScreenAccess());
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setSubscriberSegmentationFeatureLicence(boolean p_isLicenced)
    {
        i_hasSubscriberSegmentationFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasSubscriberSegmentationFeatureLicence()
    {
        return i_hasSubscriberSegmentationFeatureLicence;
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Dedicated Accounts screen.
      * @return True if the operator has access to the Dedicated Accounts screen.
      */
    public boolean hasDedAccountsScreenAccess()
    {

        return (i_gopaData.hasDedAccountsScreenAccess());
    }

    /**
      * Sets a boolean value indicating whether the subscriber's service
      * class has accumulators configured.
      * @param p_accumulatorsConfigured Flag indicating whether Accumulators are configured for this class.
      */
    public void setAccumulatorsConfigured(boolean p_accumulatorsConfigured)
    {
        i_accumulatorsConfigured = p_accumulatorsConfigured;
    }

    /**
      * Returns a boolean value indicating whether the subscriber's service
      * class has accumulators configured.
      * @return Flag indicating whether Accumulators are configured for this class.
      */ 
    public boolean hasAccumulatorsConfigured()
    {
       return(i_accumulatorsConfigured);
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Accumulators screen.
      * @return True if the operator has access to the Accumulators screen.
      */
    public boolean hasAccumulatorsScreenAccess()
    {
        return (i_gopaData.hasAccumulatorsScreenAccess());
    }

    /**
      * Sets the boolean value to indicate whether Additional Info is configured.
      * @param p_isMfltMiscFieldTitles Flag indicating whether Additional Information is configured.
      */   
    public void setAdditionalInfoIsConfigured(boolean p_isMfltMiscFieldTitles )
    {
        i_isMfltMiscFieldTitles = p_isMfltMiscFieldTitles;
    }

    /** 
      * Returns a boolean value indicating whether the Additional Info is
      * configured and hence whether the screen is accessible.
      * @return Flag indicating whether Additional Information is configured.
      */
    public boolean hasAdditionalInfoConfigured()
    {
         
         return i_isMfltMiscFieldTitles;
    } 

    // CR#60/571 Start

    /** 
      * Returns a boolean value indicating whether the CSO has access to the
      * Memo/Comments Screen.
      * @return Flag indicating whether the operator can access the Comments screen.
      */
    public boolean hasCommentsScreenAccess()
    {
        return i_gopaData.hasCommentsScreenAccess();
    }

    // Ppalon#60/571 End
    
    /** 
      * Returns a boolean value indicating whether the CSO has access to the
      * Family and Friends Screen.
      * @return Flag indicating whether the operator can access the Family/Friends screen.
      */
    public boolean hasFamilyAndFriendsScreenAccess()
    {
        return i_gopaData.hasFafScreenAccess();
    }

    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setFamilyAndFriendsFeatureLicence(boolean p_isLicenced)
    {
        i_hasFamilyAndFriendsFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasFamilyAndFriendsFeatureLicence()
    {
        return i_hasFamilyAndFriendsFeatureLicence;
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the screen
      * Community Charging.
      * @return True if the operator has access to the Community Charging screen.
      */
    public boolean hasCommunityChargingScreenAccess()
    {
        return (i_gopaData.hasCommunityChargingScreenAccess());
    }

    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setCommunityChargingFeatureLicence(boolean p_isLicenced)
    {
        i_hasCommunityChargingFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasCommunityChargingFeatureLicence()
    {
        return i_hasCommunityChargingFeatureLicence;
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the screen
      * Service Fee Deduction.
      * @return True if the operator has access to the Service Fee Deduction screen.
      */
    public boolean hasServiceFeeDeductionScreenAccess()
    {
        return (i_gopaData.hasServiceFeeScreenAccess());
    }
    
    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasServiceFeeDeductionFeatureLicence()
    {
        return i_hasServiceFeeDeductionFeatureLicence;
    }

    /** 
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setServiceFeeDeductionFeatureLicence(boolean p_isLicenced)
    {
        i_hasServiceFeeDeductionFeatureLicence = p_isLicenced;
    }

    /** 
     * Returns a boolean value indicating whether the CSO has access to the screen.
     * @return True if the operator has screen access.
     */
    public boolean hasChargedEventCountersScreenAccess()
    {
        return (i_gopaData.hasEventCountersScreenAccess());
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasChargedEventCountersFeatureLicence()
    {
        return i_hasChargedEventCountersFeatureLicence;
    }

    /** 
     * Returns a boolean value indicating whether the CSO has access to the screen.
     * @return True if the operator has screen access.
     */
    public boolean hasCallHistoryScreenAccess()
    {
        return (i_gopaData.hasCallHistoryScreenAccess());
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasCallHistoryFeatureLicence()
    {
        return i_hasCallHistoryFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setCallHistoryFeatureLicence(boolean p_isLicenced)
    {
        i_hasCallHistoryFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasStatementOfAccountFeatureLicence()
    {
        return i_hasStatementOfAccountFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setStatementOfAccountFeatureLicence(boolean p_isLicenced)
    {
        i_hasStatementOfAccountFeatureLicence = p_isLicenced;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setChargedEventCountersFeatureLicence(boolean p_isLicenced)
    {
        i_hasChargedEventCountersFeatureLicence = p_isLicenced;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is fully populated.
     */
    public boolean isPopulated()
    {
        return (true);
    }

} // end of CcMenuScreenData class
