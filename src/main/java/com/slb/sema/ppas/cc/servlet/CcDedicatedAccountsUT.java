////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDedicatedAccountsUT.java
//      DATE            :       14-Mar-2006
//      AUTHOR          :       Michael Erskine (40771f)
//      REFERENCE       :       PpacLon#2005
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for the GUI Dedicated Accounts screen. 
//                 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/05/06 | M Erskine  | Fixes to UT for changes in      | PpacLon#2127/8610
//          |            | business config data. Other     |
//          |            | minor fixes.                    |
//----------+------------+---------------------------------+--------------------
//14-Sep-06 |Andy Harris |Changed to use new methods in    |PpacLon#2586/9791
//          |            |super class.                     |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.io.IOException;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.Button;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebRequest;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework (v 1.5.4)
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcDedicatedAccountsUT extends GuiTestTT
{
    /** Table ID of the Dedicated Account Details table. */
    private static final String C_TABLE_ID_DED_ACC_DETAILS = "dedAccDetails";
    
    /** Table ID of the Dedicated Account Event History table. */
    private static final String C_TABLE_ID_DED_ACC_HISTORY = "dedAccHistory";
    
    /** New date to use to set expiry dates. Must always be in the future. */
    private static final PpasDate C_NEW_EXPIRY_DATE;
        
    static
    {
        C_NEW_EXPIRY_DATE = DatePatch.getDateToday();
        C_NEW_EXPIRY_DATE.add(PpasDate.C_FIELD_DATE, 20);
    }
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcDedicatedAccountsUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        say("Entered CcDedicatedAccountsUT.setUp()");
        super.setUp(true);
    }
    
    /**
     * @ut.when An operator navigates to the Dedicated Accounts screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadDedicatedAccountsScreen()
    {        
        beginOfTest("Start testLoadDedicatedAccountsScreen");

        // Install MSISDN and load Maintain Subsriber Screen
        installAndLoadSubScreen(1);
           
        sendRequest(constructGetRequest("cc/DedicatedAccounts", "GET_SCREEN_DATA"));

        // Validate that correct page is returned and that the content is correct
        validateDedAccsResponse(null, null, "PPAS - Dedicated Accounts", true);
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 0, 
                         new String[] {"Account ID", "Description", "Balance",
                                       "Expiry Date"});
        try
        {
            System.out.println("Loaded Dedicated Accounts screen: \n" + c_webResponse.getText());
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 1, 
                         new String[] {"1", "SMS", "0.00\n           GBP"});
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 2, 
                         new String[] {"2", "MMS", "0.00\n           GBP"});
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 3, 
                         new String[] {"3", "GPRS", "0.00\n           GBP"});
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 4, 
                         new String[] {"4", "Share price notifications", "0.00\n           GBP"});
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 5, 
                         new String[] {"5", "Cultural event notifications", "0.00\n           GBP"});
        
        verifyRowInTable(C_TABLE_ID_DED_ACC_HISTORY, 0,
        		         new String[] {"Date/Time", "Id", "Expiry Date", "Code", "Type", "Description",
        		                       "Original\n Amount", "Original\n Amount", //Spans 2 columns
                                       "Adjustment\n Amount", "Adjustment\n Amount", //Spans 2 columns
                                       "Initiating MSISDN",
        		                       "Approver", "Source"});
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts an adjustment to a dedicated account balance amount.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testPostDedAcctAdjustmentAmountSuccess()
    {
    	beginOfTest("Start postDedAcctAdjustmentAmountSuccess");
        
        // Install MSISDN and load Maintain Subsriber Screen
        BasicAccountData l_bad = installAndLoadSubScreen(1);
        String l_strMsisdn = c_ppasContext.getMsisdnFormatInput().format(l_bad.getMsisdn());
        try
        {
            try
            {
                System.out.println("Loaded Dedicated Accounts screen: \n" + c_webResponse.getText());
            }
            catch (IOException e)
            {
                failedTestException(e);
            }
        
            sendRequest(constructGetRequest("cc/DedicatedAccounts", "GET_SCREEN_DATA"));
        
            setDedAccParamsAndSendForm("POST_ADJUSTMENT", "10", "EUR", "XXX", "YYY", "Desc", "1", "", "");
            try
            {
                System.out.println("Loaded Dedicated Accounts screen: \n" + c_webResponse.getText());
            }
            catch (IOException e)
            {
                failedTestException(e);
            }
        
            verifyRowInTable(C_TABLE_ID_DED_ACC_HISTORY, 1, new String[] {"DateTimeStamp", "1", "", "YYY",
                             "XXX", "Desc", "0.10&nbsp", "EUR", "0.07&nbsp", "GBP", "+44 (0) "+l_strMsisdn});
            
            verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 1, 
                             new String[] {"1", "SMS", "0.07\n           GBP"});
            
            verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 2, 
                             new String[] {"2", "MMS", "0.00\n           GBP"});
            
            verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 3, 
                             new String[] {"3", "GPRS", "0.00\n           GBP"});
            
            verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 4, 
                             new String[] {"4", "Share price notifications", "0.00\n           GBP"});
            
            verifyRowInTable(C_TABLE_ID_DED_ACC_DETAILS, 5, 
                             new String[] {"5", "Cultural event notifications", "0.00\n           GBP"});
        
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
 	
    	endOfTest();
    }

    /**
     * @ut.when An operator posts an adjustment to a dedicated account expiry date.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testPostDedAcctAdjustmentExpiryDateSuccess()
    {
    	beginOfTest("Start postDedAcctAdjustmentExpiryDateSuccess");
        
        try
        {
            // Install MSISDN and load Maintain Subsriber Screen
            BasicAccountData l_bad = installAndLoadSubScreen(1);
            String l_strMsisdn = c_ppasContext.getMsisdnFormatInput().format(l_bad.getMsisdn());
        
            sendRequest(constructGetRequest("cc/DedicatedAccounts", "GET_SCREEN_DATA"));
        
            setDedAccParamsAndSendForm(
                "POST_ADJUSTMENT", "", "GBP", "XXX", "YYY", "Desc", "1", C_NEW_EXPIRY_DATE.toString(), "");
        
            verifyRowInTable(
                C_TABLE_ID_DED_ACC_DETAILS, 1,
                new String[] {"1", "SMS", "0.00\n           GBP", C_NEW_EXPIRY_DATE.toString()});
            verifyRowInTable(
                C_TABLE_ID_DED_ACC_HISTORY, 1,
                new String[] {"DateTimeStamp", "1", C_NEW_EXPIRY_DATE.toString(), "YYY",
                "XXX", "Desc", "&nbsp", "", "&nbsp", "", "+44 (0) "+l_strMsisdn});
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
    	endOfTest();
    }


    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        say(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcDedicatedAccountsUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------
    
    /**
     * 
     */
    private void validateDedAccsResponse(String  p_expectedMessage,
                                         String  p_expectedFrameName,
                                         String  p_expectedTitle,
                                         boolean p_flag)
    {   
        say ("Entered validateCountersResponse()");
        validateResponse(p_expectedMessage, p_expectedFrameName, p_expectedTitle);        
        
        say ("Leaving validateDedAccsResponse");
    }
    
    
    /**
     * Gets the Dedicated Accounts form from the <code>WebResponse</code> and attempts to set various
     * parameters on it, then submits it.
     * @throws SAXException
     * @throws IOException
     */
    private void setDedAccParamsAndSendForm(String p_command,
    		                                String p_amount, 
                                            String p_currency,
                                            String p_adjustmentTypes,
                                            String p_adjustmentCodes,
                                            String p_description,
                                            String p_adjustmentDedAccId,
                                            String p_adjustmentDedAccDate,
                                            String p_oldAdjustmentDedAccDate)
        throws SAXException, IOException
    {
        WebForm    l_dedAccsForm = null;
        Button     l_submitButton = null;
        WebRequest l_request      = null;
        
        say("Entered setCounterParametersAndSendForm():");
        l_dedAccsForm = c_webResponse.getFormWithName("DedicatedAccountsForm");
        assertNotNull("In setDedAccParamsAndSendForm:- Dedicated Accounts form is null", l_dedAccsForm);
        
        l_submitButton = l_dedAccsForm.getButtonWithID("makeAdjustmentButton");
        
        //TODO: Force entry to else statement instead for the moment
        if (false && l_submitButton != null)
        {
        
            l_dedAccsForm.setParameter("p_amount", p_amount);
            l_dedAccsForm.setParameter("p_currency", p_currency);
            l_dedAccsForm.setParameter("p_adjustmentTypes", p_adjustmentTypes);
            l_dedAccsForm.setParameter("p_adjustmentCodes", p_adjustmentCodes);
            l_dedAccsForm.setParameter("p_description", p_description);
            l_dedAccsForm.setParameter("p_adjustmentDedAccId", p_adjustmentDedAccId);
            l_dedAccsForm.setParameter("p_adjustmentDedAccDate", p_adjustmentDedAccDate);
            l_dedAccsForm.setParameter("p_oldAdjustmentDedAccDate", p_oldAdjustmentDedAccDate);
            								
            // Can't do this since p_command is a hidden parameter.
            //l_countersForm.setParameter("p_command", "POST_ADJUSTMENT");
            say("Clicking Dedicated Accounts screen Update button");
            // l_submitButton.click();
            l_dedAccsForm.submit();
        }
        else
        {
            l_request = new GetMethodWebRequest(c_urlPrefix + 
                                                "ascs/gui/cc/DedicatedAccounts?p_command=" + p_command +
                                                "&p_amount=" + p_amount + 
                                                "&p_currency=" + p_currency +
                                                "&p_adjustmentTypes=" + p_adjustmentTypes + 
                                                "&p_adjustmentCodes=" + p_adjustmentCodes +
                                                "&p_description=" + p_description +
                                                "&p_adjustmentDedAccId=" + p_adjustmentDedAccId +
                                                "&p_adjustmentDedAccDate=" + p_adjustmentDedAccDate +
                                                "&p_oldAdjustmentDedAccDate=" + p_oldAdjustmentDedAccDate);
            sendRequest(l_request);
        }
        
        //c_webResponse = l_countersForm.submit();
        say("!!!Submitted Dedicated Accounts form!!!");      
    }
    
}
