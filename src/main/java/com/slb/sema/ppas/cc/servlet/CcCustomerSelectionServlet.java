////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerSelectionServlet.Java
//      DATE            :       11th Feb 2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_27
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Customer Selection GUI screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcCustSelectionCriteriaScreenData;
import com.slb.sema.ppas.cc.screendata.CcCustSelectionResultsScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcCustomerSelectionDataSetResponse;
import com.slb.sema.ppas.cc.service.CcCustomerSelectionService;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.CustomerSelectionData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/** This Servlet processes requests from the Customer Selection Criteria JSP
 *  script. These requests specify  various selection criteria on
 *  which customers are to be selected.
 *
 *  Requests to this Servlet can arise from:
 *  <br> 1) The login screen or on logout from an customer account (i.e. for
 *          the initial Customer Selection screen display).
 *  <br> 2) The CcCustomerSelectionCriteria JSP (i.e. the user has
 *          supplied some customer selection criteria to be processed.
 *       3) The CcCustomerSelectionResults JSP (i.e. from the list of
 *          Customer details that matched the selection criteria, the user
 *          has selected a customer fro processing).
 *
 *  <P>When the request is from the login screen, the Servlet simply gets
 *     and returns a list of available markets to be used on the initial
 *     Customer Selection screen display.
 *
 *  <P>When the request is from the CcCustomerSelectionCriteria JSP, if the
 *     selection criteria specified is just the Msisdn or just the customer
 *     identifier, then the selection criteria identify a single account. This
 *     single account (i.e. the cust id or Msisdn) is stored in the session
 *     and the response to the CcCustomerSelectionResults JSP indicates that
 *     a single account was selected. Otherwise, the customer selection
 *     service is called to get a list of accounts meeting the supplied
 *     selection criteria. If this list contains a single account, again, this
 *     is indicated in the response to the CcCustomerSelectionResults JSP so
 *     that it can forward straight to the account details screen.
 *
 *  <P>When the request is from the CcCustomerSelectionResults JSP, then an
 *     account has been selected the the list of accounts that met the
 *     selection criteria. In this case, the acust id for the selected account
 *     is obtained from the request and stored in the session. Then request
 *     is then forwarded on to the account details screen.
 */
public class CcCustomerSelectionServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustomerSelectionServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Customer Selection Service to be used by this servlet. */
    private CcCustomerSelectionService i_ccCustomerSelectionService = null;

    /** The CC GUI Account Service to be used by this servlet. */
    private CcAccountService i_ccAccountService = null;


    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor - creates a Customer Selection Servlet instance to handle
     *  requests from the Customer Selection Screen.
     */
    public CcCustomerSelectionServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 45000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 45090, this,
                "Constructed " + C_CLASS_NAME );
        }
    } // end Constructor CcCustomerSelectionServlet

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Performs initialisation specific to this Servlet; currently there is
     *  no specific initialisation for this Servlet.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 45200, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcCustomerSelectionService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 45290, this,
                "Leaving doInit().");
        }

        return;

    } // end doInit

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to handle requests from the Customer Selection Criteria
     *  JSP and the Customer Selection Results JSP.
     * @param p_request      HTTP request.
     * @param p_response     HTTP response.
     * @param p_httpSession  Current HttpSession.
     * @param p_guiRequest   GuiRequest object.
     * @return forwarding URL.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                             l_command     = null;
        String                             l_forwardUrl  = null;
        CcCustSelectionCriteriaScreenData  l_custSelCriteriaScreenData  = null;
        CcCustSelectionResultsScreenData   l_custSelResultsScreenData  = null;
        GuiResponse                        l_guiResponse = null;
        Msisdn                             l_msisdn;
        String                             l_custId;
        String                             l_srva;
        String                             l_sloc;
        String                             l_ssn;
        String                             l_lastName;
        String                             l_firstName;
        Market                             l_market;
        CcAccountDataResponse              l_accountDataResp;
        GuiSession                         l_guiSession;
        String                             l_accountsToSelect;
        AccountData                        l_accountData;
        Boolean                            l_defaultAllAssocSubscribers;
        Boolean                            l_searchByNamesFlag;
        Boolean                            l_searchBySSNFlag;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 45300, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcCustomerSelectionService if it does not already exist.
        if (i_ccCustomerSelectionService == null)
        {
            i_ccCustomerSelectionService =
                         new CcCustomerSelectionService (p_guiRequest,
                                                         0,
                                                         i_logger,
                                                         i_guiContext);
        }

        // Construct CcAccountService to be used by this servlet if it does
        // not already exist.
        if (i_ccAccountService == null)
        {
            i_ccAccountService =
                         new CcAccountService(p_guiRequest,
                                              0,
                                              i_logger,
                                              i_guiContext);
        }

        try
        {
            l_guiSession = (GuiSession)p_guiRequest.getSession();

            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,           // Mandatory ...
                                      null,           // ... so no default
                                      new String []{
                                          "INITIAL_DISPLAY",
                                          "GET_ACCOUNT_DETAILS",
                                          "DISPLAY_SELECTED_ACCOUNT"});

            // Command parameter value of "INITIAL_DISPLAY" means that we must
            // obtain the data required for the initial Customer Selection
            // screen display i.e. the markets available to the CSO.

            if (l_command.equals ("INITIAL_DISPLAY"))
            {
                // Set the forward URL to the Customer Selection Criteria JSP
                l_forwardUrl = "/jsp/cc/cccustselectioncriteria.jsp";

                // Create screen data object for the Customer Selection
                // Criteria JSP
                l_custSelCriteriaScreenData =
                    new CcCustSelectionCriteriaScreenData(i_guiContext, p_guiRequest);

                // Set up the markets for the current CSO in the screen data
                // object
                l_custSelCriteriaScreenData.setMarkets(l_guiSession.getCsoMarkets());

                l_defaultAllAssocSubscribers = (Boolean)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.defaultAllAssocSubscribers");
                l_custSelCriteriaScreenData.setDefaultAllAssocSubscribers(l_defaultAllAssocSubscribers);

                l_searchByNamesFlag = (Boolean)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.allowSearchByNames");
                l_custSelCriteriaScreenData.setSearchByNamesFlag(l_searchByNamesFlag);

                l_searchBySSNFlag = (Boolean)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.allowSearchBySSN");
                l_custSelCriteriaScreenData.setSearchBySSNFlag(l_searchBySSNFlag);

                // Attach the screen data to the HTTP request that will be
                // forwarded to the Customer Selection Criteria JSP.
                p_request.setAttribute ("p_guiScreenData", l_custSelCriteriaScreenData);
            } // end if

            // Command value "DISPLAY_SELECTED_ACCOUNT" means that an account
            // has been selected from the list of accounts that met the
            // selection criteria
            else if (l_command.equals ("DISPLAY_SELECTED_ACCOUNT"))
            {
                // An account has been selected. Get the custId of the account
                // from the HTTP request, store it in the session and set the
                // forward URL to be the main account details display

                l_custId = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_custId",
                                    true,   // CustId is mandatory ...
                                    "",     // No default
                                    PpasServlet.C_TYPE_NUMERIC);

                p_guiRequest.setCustId (l_custId);
                p_guiRequest.setMsisdn (null);

                // Need to get the market here to be used in the selection
                // of account data, this has already been set on the
                // session at this point so just retrieve from there.
                l_market = l_guiSession.getCsoMarket();

                // Get full data associated with the cust id
                l_accountDataResp = i_ccAccountService.getFullData(
                                                           p_guiRequest,
                                                           i_timeout,
                                                           l_market);

                // Create screen data object, this will be forwarded to
                // the mainframes.html as we have selected one customer only
                // at this point.
                l_custSelResultsScreenData =
                    new CcCustSelectionResultsScreenData(i_guiContext, p_guiRequest);

                if (l_accountDataResp.isSuccess())
                {
                    l_custSelResultsScreenData.setSingleAccountSelected();

                    // Store the account data selected on the session.
                    l_accountData = l_accountDataResp.getAccountData();
                    l_guiSession.setAccountData (l_accountData);
                    l_guiSession.setCsoMarket(l_accountData.getMarket());

                    // Also store cust id, bt cust id, and Msisdn on the session
                    setCachedCustIdentifiers(p_guiRequest,
                                             l_guiSession,
                                             l_accountData.getCustId(),
                                             l_accountData.getBtCustId(),
                                             l_accountData.getMsisdn());
                }

                // Add the response to the screen data object.
                l_custSelResultsScreenData.addRetrievalResponse(
                                               p_guiRequest,
                                               l_accountDataResp);

                l_forwardUrl = "/html/cc/mainframes.html";

            }
            else
            {
                // Command is "GET_ACCOUNT_DETAILS". This means that the user
                // has entered a set of customer selection criteria and we
                // must determine what accounts meet these criteria

                // Set the forward URL to the Customer Selection Results JSP
                l_forwardUrl = "/jsp/cc/cccustselectionresults.jsp";

                // Create screen data object for the Customer Selection
                // Results JSP
                l_custSelResultsScreenData =
                    new CcCustSelectionResultsScreenData(i_guiContext, p_guiRequest);

                // Get the Msisdn and Account Id parameter from the HTTP request
                l_msisdn = getValidMsisdn(
                                        p_guiRequest,
                                        PpasServlet.C_FLAG_ALLOW_BLANK,
                                        p_request,
                                        "p_msisdn",
                                        false,  // msisdn is not mandatory ...
                                        "",     // default to empty string
                                        i_guiContext.getMsisdnFormatInput());

                l_custId = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_accountId",
                                    false,  // Account Id is not mandatory ...
                                    "",     // default to empty string
                                    PpasServlet.C_TYPE_NUMERIC);

                // Get srva and sloc as mandatory, numeric parameters.
                // Set default to a negative value - this indicates to the
                // customer selection service that the srva/sloc is not to
                // be used in the selection criteria.
                l_srva = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_srva",
                                    true,   // Mandatory
                                    "",
                                    PpasServlet.C_TYPE_ANY);

                l_sloc = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_sloc",
                                    true,   // Mandatory
                                    "",
                                    PpasServlet.C_TYPE_ANY);

                /* Store the chosen market in the session. */
                l_market = new Market(l_srva, l_sloc);
                l_guiSession.setCsoMarket(l_market);

                // If an Msisdn value was supplied in the HTTP request, then
                // set up the Msisdn in the session and indicate in the
                // screen data object we can simply forward the request to the
                // Account Details Servlet for processing

                if (l_msisdn != null)
                {
                    // Get the parameter that defines whether we're selecting
                    // only the most recent account for the given Msisdn or
                    // all accounts with that Msisdn

                    l_accountsToSelect = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_accountsToSelect",
                                    true,   // Mandatory
                                    "",
                                    new String [] {"MOST_RECENT_ONLY",
                                                   "ALL"});

                    // Select only the most recent account for the Msisdn?
                    if (l_accountsToSelect.equals("MOST_RECENT_ONLY"))
                    {
                        // Store the Msisdn in the session
                        p_guiRequest.setMsisdn (l_msisdn);
                        p_guiRequest.setCustId ("");
                       
                        // Get full data for the Msisdn...
                        l_accountDataResp = i_ccAccountService.getFullData(p_guiRequest, i_timeout, l_market);

                        if (l_accountDataResp.isSuccess())
                        {
                            l_custSelResultsScreenData.
                                     setSingleAccountSelected();

                            // Store the account data selected on the session.
                            l_accountData = l_accountDataResp.getAccountData();
                            l_guiSession.setAccountData (l_accountData);
                            l_guiSession.setCsoMarket(l_accountData.getMarket());

                            // Also store cust id, bt cust id, and Msisdn on
                            // the session.
                            setCachedCustIdentifiers(p_guiRequest,
                                                     l_guiSession,
                                                     l_accountData.getCustId(),
                                                     l_accountData.getBtCustId(),
                                                     l_accountData.getMsisdn());
                        }

                        // Add the response to the screen data object.
                        l_custSelResultsScreenData.addRetrievalResponse(
                                          p_guiRequest,
                                          l_accountDataResp);

                    } // end if ("MOST_RECENT_ONLY")
                    else
                    {
                        // Select all accounts that have ever used the given
                        // Msisdn.
                        selectCustomers (p_guiRequest,
                                         l_market,
                                         null, // Don't use SSN in selection
                                         null, // Don't use surname in selectio
                                         null, // Don't use first name
                                         l_msisdn, // Select on this Msisdn
                                         l_custSelResultsScreenData);
                    }

                } // end if (Msisdn found in http request)

                // If a Cust ID value was supplied in the HTTP request, then
                // set up the Cust Id in the session and indicate in the
                // screen data object we can simply forward the request to the
                // Account Details Servlet for processing

                else if (!l_custId.equals (""))
                {
                    p_guiRequest.setCustId (l_custId);
                    p_guiRequest.setMsisdn (null);

                    // Get full data associated with the cust id
                    l_accountDataResp = i_ccAccountService.getFullData(p_guiRequest, i_timeout, l_market);

                    if (l_accountDataResp.isSuccess())
                    {
                        l_custSelResultsScreenData.setSingleAccountSelected();

                        // Store the account data selected on the session.
                        l_accountData = l_accountDataResp.getAccountData();
                        l_guiSession.setAccountData (l_accountData);
                        l_guiSession.setCsoMarket(l_accountData.getMarket());

                        // Also store cust id, bt cust id, and Msisdn on the
                        // session.
                        setCachedCustIdentifiers(p_guiRequest,
                                                 l_guiSession,
                                                 l_accountData.getCustId(),
                                                 l_accountData.getBtCustId(),
                                                 l_accountData.getMsisdn());
                    }

                    // Add the response to the screen data object.
                    l_custSelResultsScreenData.addRetrievalResponse(
                                          p_guiRequest,
                                          l_accountDataResp);
                }
                else
                {
                    // We are selecting based on some combination of customer
                    // name, market and SSN, which can potentially identify
                    // more than one account.

                    // Get the remaining selection criteria supplied by the
                    // user from the HTTP request

                    // Get last name, first name and social security number as
                    // non-mandatory, numeric parameters.
                    l_lastName = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_lastName",
                                    false,
                                    null,
                                    PpasServlet.C_TYPE_ANY);

                    l_firstName = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_firstName",
                                    false,
                                    null,
                                    PpasServlet.C_TYPE_ANY);

                    l_ssn = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_ssn",
                                    false,
                                    null,
                                    PpasServlet.C_TYPE_ANY);

                    // Get the details of the accounts that match the selection
                    // criteria supplied in the request.
                    selectCustomers (p_guiRequest,
                                     l_market,
                                     l_ssn,
                                     l_lastName,
                                     l_firstName,
                                     null, // Don't use Msisdn for selection
                                     l_custSelResultsScreenData);
                }

                // Attach the screen data to the HTTP request that
                // will be forwarded to the Customer Selection
                // Results JSP.
                p_request.setAttribute ("p_guiScreenData",
                                        l_custSelResultsScreenData);

            } // end else

        }  // end try

        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 86000, this,
                    "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Create new Screen Data object, if one has not already been
            // created, and add the response to it.
            if (l_custSelResultsScreenData == null)
            {
                l_custSelResultsScreenData =
                    new CcCustSelectionResultsScreenData(i_guiContext, p_guiRequest);
            }
            l_custSelResultsScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            p_request.setAttribute ("p_guiScreenData", l_custSelResultsScreenData);

            l_forwardUrl = "/jsp/cc/ccerror.jsp";
        }  // end catch

        // If we have encountered an error on retrieval, forward to the
        // error page
        if (l_custSelResultsScreenData != null)
        {
            // For the following keys, we don't want to go to the CcError
            // page, since it's just a case of an invalid account being supplied
            if (l_custSelResultsScreenData.hasRetrievalError() &&
                !(l_custSelResultsScreenData.getRetrievalErrorKey().
                    equals(GuiResponse.C_KEY_MSISDN_NOT_EXISTS)) &&
                !(l_custSelResultsScreenData.getRetrievalErrorKey().
                    equals(GuiResponse.C_KEY_ACCOUNT_NOT_EXISTS)) &&
                !(l_custSelResultsScreenData.getRetrievalErrorKey().
                    equals(GuiResponse.C_KEY_TRANSIENT_ACCOUNT)) &&
                !(l_custSelResultsScreenData.getRetrievalErrorKey().
                    equals(GuiResponse.C_KEY_WRONG_MARKET)) &&
                !(l_custSelResultsScreenData.getRetrievalErrorKey().
                    equals(GuiResponse.C_KEY_NO_CUST_SELECTION_DATA)))
            {

                p_request.setAttribute("p_guiScreenData", l_custSelResultsScreenData);

                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Performs customer selection based on the given input parameters. The
     *  response from the customer selection and the data set of customers
     *  selected are added to the screen data object passed in via the
     *  <code>p_screenData</code> parameter.
     *  @param p_guiRequest GUI request.
     *  @param p_market Market on which to base the selection of customers.
     *                  An 'empty' value indicates that the service location
     *                  is not to be used in the selection criteria.
     *  @param p_ssn    Social security number to base the selection of
     *                  customers upon. If this parameter is <code>null</code>,
     *                  then the selection of customers will not be based upon
     *                  their social security numbers.
     *  @param p_surname The surname of customers to base their selection
     *                   upon. If this parameter is <code>null</code>, then
     *                   the customer's surname will not be used in the
     *                   selection. This parameter may contain * or % wildcard
     *                   characters.
     *  @param p_firstName The first name of customers to base their selection
     *                     upon. If this parameter is <code>null</code>, then
     *                     the customer's first name will not be used in the
     *                     selection. Also, the <code>p_firstName</code> may
     *                     only be non-null if one of the <code>p_surname</code>
     *                     or <code>p_ssn</code> parameters is non-null i.e. the
     *                     service does not allow selection of customers based
     *                     on their first name only. This parameter may
     *                     contain * or % wildcard characters.
     * @param p_msisdn The MSISDN to based customer selection upon.
     *                 If this parameter is <code>null</code>,
     *                 then the Msisdn will not be used in the selection
     *                 criteria.
     * @param p_screenData Screen data object to which the response generated
     *                     in performing customer selection and the data set
     *                     of selected customers will be added.
     */
    private void selectCustomers(
        GuiRequest                        p_guiRequest,
        Market                            p_market,
        String                            p_ssn,
        String                            p_surname,
        String                            p_firstName,
        Msisdn                            p_msisdn,
        CcCustSelectionResultsScreenData  p_screenData)
    {
        CcCustomerSelectionDataSetResponse l_custSelectionResponse;
        CustomerSelectionData              l_selectedCust;
        String                             l_custId;
        String                             l_btCustId;
        char                               l_custStatus;
        GuiSession                         l_guiSession;

        // Use the Customer Selection service to get details of all accounts
        // that meet the given selection criteria
        l_custSelectionResponse =
                            i_ccCustomerSelectionService.getDetails(
                                p_guiRequest,
                                0,
                                i_timeout,
                                p_market,
                                p_ssn,
                                p_surname,
                                p_firstName,
                                p_msisdn);

        // Add response to the screen data object.
        p_screenData.addRetrievalResponse (p_guiRequest,
                                           l_custSelectionResponse);

        if (l_custSelectionResponse.isSuccess())
        {
            // Check if there is just one record in the data set within the
            // response. If so, then the selection criteria supplied identified
            // a single account. In this case, just set up the customer id of
            // the account identified in the session and indicate to the
            // Selection Results JSP that a single account has been identified.

            if (l_custSelectionResponse.getDataSet().getDataV().size() == 1)
            {
                l_selectedCust = (CustomerSelectionData)
                                          (l_custSelectionResponse.getDataSet().
                                                getDataV().elementAt(0));
                l_custId   = l_selectedCust.getCustId();
                l_btCustId = l_selectedCust.getBtCustId();
                l_custStatus = l_selectedCust.getCustStatus();

                l_guiSession = (GuiSession)p_guiRequest.getSession();
                setCachedCustIdentifiers(p_guiRequest,
                                         l_guiSession,
                                         l_custId,
                                         l_btCustId,
                                         null);
                l_guiSession.setCustStatus (l_custStatus);
                l_guiSession.setCsoMarket(l_selectedCust.getMarket());

                p_screenData.setSingleAccountSelected();
            }
            else
            {
                // Selection criteria identified either zero or more than one
                // account, therefore set up the data set of selected accounts
                // in the Customer Selection Results screen object so that a
                // list of selected accounts can be displayed by the JSP
                // and it can handle the case where no accounts were selected.

                p_screenData.setSelectedCustomers(
                                     l_custSelectionResponse.getDataSet());
            }
        }
        return;

    } // end method 'selectCustomers'
} // end class ccCustomerSelectionServlet
