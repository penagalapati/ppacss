////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcVoucherScreenData.java
//      DATE            :       07-Feb-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/5475
//                              PRD_PPAK00_DEV_IN_034
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Voucher screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 13/12/05 |K Goswami   | Added code for voucher history  | PpaLon#1892/7603
//          |            | popup screen                    | CA#39
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.TreeSet;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.EventHistoryData;
import com.slb.sema.ppas.common.dataclass.FailedRechargeData;
import com.slb.sema.ppas.common.dataclass.FailedRechargeDataSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.VoucherData;
import com.slb.sema.ppas.common.dataclass.VoucherDataSet;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.dataclass.VoucherHistoryData;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Voucher screen.
 */
public class CcVoucherScreenData extends GuiScreenData
{
    
    /** Voucher is Available GUI description. Value is {@value}. */
    private static final String C_VOUCHER_AVAILABLE_GUI_DESC   = "Available";
                                         
    /** Voucher is Assigned GUI description. Value is {@value}. */
    private static final String C_VOUCHER_ASSIGNED_GUI_DESC    = "Assigned";
                                         
    /** Voucher is Damaged GUI description. Value is {@value}. */
    private static final String C_VOUCHER_DAMAGED_GUI_DESC     = "Damaged";
                                          
    /** Voucher is Stolen GUI description. Value is {@value}. */
    private static final String C_VOUCHER_STOLEN_GUI_DESC      = "Stolen"; 
                                          
    /** Voucher is Pending GUI description. Value is {@value}. */
    private static final String C_VOUCHER_PENDING_GUI_DESC     = "Pending";

    /** Voucher is Unavailable GUI description. Value is {@value}. */
    private static final String C_VOUCHER_UNAVAILABLE_GUI_DESC = "Unavailable";
 
    /** Voucher is Unknown GUI description. Value is {@value}. */
    private static final String C_VOUCHER_UNKNOWN_GUI_DESC     = "Unknown"; 
 
    /** Voucher is Undefined GUI description. Value is {@value}. */
    private static final String C_VOUCHER_UNDEFINED_GUI_DESC   = "Undefined";
    
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

//    /** VoucherData object wrapped by this object. */
//    private VoucherData i_voucherData = null;
    
    /** VoucherEnquiryData object wrapped by this object. */
    private VoucherEnquiryData i_voucherEnquiryData = null;
    
    /** VoucherHistoryData object wrapped by this object. */
    private VoucherHistoryData i_voucherHistoryData = null;

    /** Flag to indicate that the combined recharge data has been populated with
     *  a VoucherDataSet of successful recharge data.
     */
    private boolean i_successRechargesPopulated;

    /** Flag to indicate that the combined recharge data has been populated with
     *  a FailedRechargeDataSet of failed recharge data.
     */
    private boolean i_failedRechargesPopulated;

    /** Sorted set of CcRechargeHistoryData objects wrapped by this object. */
    private TreeSet i_rechargeHistorySet;

    /** Text giving information on the status of any recharge request that was
     *  attempted.
     */
    private String  i_infoText;

    /** Voucher serial number length. */
    private int     i_serialNumberLength;

    /** Flag to indicate whether a successful recharge has been done. */
    private boolean i_rechargeDone;

    /** Flag to indicate whether a failed recharge has been done. */
    private boolean i_rechargeFailed;

    /** Flag to indicate whether a successful IVR unbarring has been done. */
    private boolean i_ivrUnbarDone;

    /** Voucher activation number. */
    private String  i_voucherActivationNumber;
    
    // TODO - not sure I should add this...
    /** Voucher serial number. */
    private String i_voucherSerialNumber;

    /** External Voucher Database Used flag. */
    private boolean i_evdbUsed;

    /** Voucher history detail screen data. */
    private CcVoucherRefillHistoryDetailScreenData i_historyScreenData;
    
    /** Recharge Type.    */
    private char i_rechargeType = ' ';
    
    /** Has Enhanced Value Voucher Feature Licence. */
    private boolean i_hasEnhancedValueVoucherFeatureLicence = false;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcVoucherScreenData(GuiContext p_guiContext,
                               GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);

        i_rechargeHistorySet = new TreeSet();
        i_successRechargesPopulated = false;
        i_failedRechargesPopulated  = false;
        i_infoText = "";
        i_rechargeDone = false;
        i_rechargeFailed = false;
        i_ivrUnbarDone = false;
        i_voucherActivationNumber = "";

        i_voucherEnquiryData = 
            new VoucherEnquiryData(
                    p_guiRequest,
                    "");
        
//        i_voucherData = new VoucherData(p_guiRequest, null);
        
        i_evdbUsed = false;
        i_historyScreenData = null;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** 
      * Returns <code>true</code> if the Operator is allowed to perform
      * an Account Recharge.  Otherwise returns <code>false</code>.
      * 
      * @return True if the operator can perform a recharge, otherwise, false.
      */
    public boolean accountRechargeIsPermitted()
    {
        return (i_gopaData.accountRechargeIsPermitted());
    }
    
    /**
     * Returns <code>true</code> if the Operator is allowed to view voucher history. 
     * Otherwise returns <code>false</code>.
     * 
     * @return true if the operator can view voucher history.
     */
    public boolean voucherHistoryIsPermitted()
    {
        return i_gopaData.voucherHistoryIsPermitted();
    }

    /** 
      * Returns <code>true</code> if the Operator is allowed to perform
      * a value voucher Recharge.  Otherwise returns <code>false</code>.
      * 
      * @return True if the operator can perform a value voucher recharge, otherwise, false.
      */
    public boolean valueVoucherRechargeIsPermitted()
    {
        return (i_gopaData.servClassChangeIsPermitted());
    }

    /** 
      * Returns <code>true</code> if the Operator is allowed to perform
      * Voucher Enquiry.  Otherwise returns <code>false</code>.
      * 
      * @return True if the operator can perform a voucher enquiry, otherwise, false.
      */
    public boolean voucherEnquiryIsPermitted()
    {
        return (i_gopaData.voucherEnquiryIsPermitted());
    }

    /** 
      * Returns <code>true</code> if the Operator is allowed to perform
      * Voucher Status Update.  Otherwise returns <code>false</code>.
      * 
      * @return True if the operator can perform voucher status update, otherwise, false.
      */
    public boolean voucherStatusUpdateIsPermitted()
    {
        return (i_gopaData.voucherUpdateIsPermitted());
    }

    /**
     * Returns the VoucherData object wrapped within this object.
     * Returns null if there is't one.
     * 
     * @return Details of a voucher.
     */
//    public VoucherData getVoucherData()
//    {
//        return i_voucherData;
//    }
    
    /**
     * Returns the VoucherData object wrapped within this object.
     * Returns null if there is't one.
     * 
     * @return Details of a voucher.
     */
    public VoucherEnquiryData getVoucherEnquiryData()
    {
        return i_voucherEnquiryData;
    }
    
    /**
     * Returns the VoucherHistoryData object wrapped within this object.
     * Returns null if there is't one.
     * 
     * @return History of a voucher.
     */
    public VoucherHistoryData getVoucherHistoryData()
    {
        return i_voucherHistoryData
        ;
    }

    /**
     * Returns an array of CcRechargeHistoryData objects.
     * 
     * @return Array of recharge history details.
     */
    public Object[] getRechargeHistoryArray()
    {
        return i_rechargeHistorySet.toArray();
    }

//    /** Sets the VoucherData object wrapped within this object.
//     * 
//     * @param p_voucherData Details of a voucher.
//     */
//    public void setVoucherData(VoucherData p_voucherData)
//    {
//        i_voucherData = p_voucherData;
//    }
//    
    /** Sets the VoucherEnquiryData object wrapped within this object.
     * 
     * @param p_voucherEnquiryData Details of a voucher.
     */
    public void setVoucherEnquiryData(VoucherEnquiryData p_voucherEnquiryData)
    {
        i_voucherEnquiryData = p_voucherEnquiryData;
    }
    
    /** Sets the VoucherHistoryData object wrapped within this object.
     * 
     * @param p_voucherHistoryData History Details of a voucher.
     */
    public void setVoucherHistoryData(VoucherHistoryData p_voucherHistoryData)
    {
        i_voucherHistoryData = p_voucherHistoryData;
    }

    /** Sets the VoucherDataSet object wrapped within this object.
     * 
     * @param p_voucherDataSet Set of vouchers.
     */
    public void setSuccessfulRechargeDataSet(VoucherDataSet p_voucherDataSet)
    {
        Vector                 l_voucherVector;
        VoucherData            l_voucher;
        CcRechargeHistoryData  l_combinedData;
        String                 l_description = null;
        String                 l_sServiceClass = null;

        l_voucherVector = p_voucherDataSet.getVoucherSetAsVector();

        for (int i = 0; i < l_voucherVector.size(); i++)
        {
            l_voucher = (VoucherData)l_voucherVector.elementAt(i);

            if (l_voucher.getRechargeType() == VoucherData.C_STANDARD_RECHARGE_TYPE)
            {
                l_description = "Standard voucher refill";
            }
            else if (l_voucher.getRechargeType() == VoucherData.C_VALUE_RECHARGE_TYPE) 
            {
                l_description = "Value voucher refill";
            }
            else
            {
                l_description = "";
            }
            if (l_voucher.getServiceClass() != null )
            {
                l_sServiceClass = Integer.toString(l_voucher.getServiceClass().getValue());
            }

            l_combinedData = new CcRechargeHistoryData(
                                    l_voucher.getSerialNumber(),  
                                    l_voucher.getGroup(),
                                    l_voucher.getValue(),
                                    l_voucher.getAppliedValue(),
                                    l_voucher.getLinkIdString(),  // TODO - change to accept Long
                                    l_voucher.getAssignedDateTime(),
                                    l_description,
                                    l_voucher.getOpid(),
                                    l_sServiceClass,
                                    l_voucher.getInitiatingMsisdn(),
                                    l_voucher.getHasDivision());

            i_rechargeHistorySet.add(l_combinedData);

        }

        i_successRechargesPopulated = true;

    }

    /** Sets the FailedrechargeDataSet object wrapped within this object.
     * 
     * @param p_failedRechargeDataSet Set of failed recharge attempts.
     */
    public void setFailedRechargeDataSet(
                           FailedRechargeDataSet p_failedRechargeDataSet)
    {
        Vector                 l_failedRechargeVector;
        FailedRechargeData     l_failedRecharge;
        CcRechargeHistoryData  l_combinedData;

        l_failedRechargeVector = p_failedRechargeDataSet
                                     .getFailedRechargeSetAsVector();

        for (int i = 0; i < l_failedRechargeVector.size(); i++)
        {
            l_failedRecharge = (FailedRechargeData)l_failedRechargeVector.elementAt(i);
            l_combinedData = new CcRechargeHistoryData(
                                    l_failedRecharge.getVoucherNumber(),
                                    "",
                                    null,
                                    null,
                                    "",
                                    l_failedRecharge.getEventDateTime(),
                                    "Failed Refill",
                                    "",
                                    "",
                                    l_failedRecharge.getMsisdn(),
                                    false);

            i_rechargeHistorySet.add(l_combinedData);

        }

        i_failedRechargesPopulated  = true;

    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * 
     * @return True if this object is fully populated, otherwise, false.
     */
    public boolean isPopulated()
    {
        return ( i_successRechargesPopulated &&
                 i_failedRechargesPopulated );
    }

    /** Sets the text giving information on the status of any recharge request
     *  that was attempted.
     * 
     * @param p_text Status of a recharge request.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving information on the status of any recharge request
     *  that was attempted.
     * 
     * @return Status of a recharge request.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Sets voucher serial number length.
     * 
     * @param p_length Length of a voucher serial number.
     */
    public void setVoucherSerialLength(int p_length)
    {
        i_serialNumberLength = p_length;
    }

    /** Gets voucher serial number length.
     * 
     * @return Length of a voucher serial number.
     */
    public int getVoucherSerialLength()
    {
        return(i_serialNumberLength);
    }
    
    /** Gets voucher serial number.
     * 
     * @return Length of a voucher serial number.
     */
    public String getVoucherSerialNumber()
    {
        return(i_voucherEnquiryData.getSerialNumber());
    }

    /** Sets flag to indicate whether a successful recharge has been done.
     * 
     * @param p_success Flag indicating a recharge request succeeded.
     */
    public void setRechargeDone(boolean p_success)
    {
        i_rechargeDone = p_success;
    }

    /** Gets flag to indicate whether a successful recharge has been done.
     * 
     * @return Flag indicating a recharge request succeeded.
     */
    public boolean isRechargeDone()
    {
        return(i_rechargeDone);
    }

    /** Sets flag to indicate whether a failed recharge has been done.
     * 
     * @param p_failure Flag indicating a recharge request failed.
     */
    public void setRechargeFailed(boolean p_failure)
    {
        i_rechargeFailed = p_failure;
    }

    /** Gets flag to indicate whether a failed recharge has been done.
     * 
     * @return Flag indicating a recharge request failed.
     */
    public boolean isRechargeFailed()
    {
        return(i_rechargeFailed);
    }

    /** Sets flag to indicate whether a successful IVR unbarring has been done.
     * 
     * @param p_success Flag indicating an IVR barring succeeded.
     */
    public void setIvrUnbarDone(boolean p_success)
    {
        i_ivrUnbarDone = p_success;
    }

    /** Gets flag to indicate whether a successful IVR unbarring has been done.
     * 
     * @return Flag indicating an IVR barring succeeded.
     */
    public boolean isIvrUnbarDone()
    {
        return(i_ivrUnbarDone);
    }

    /** Sets the voucher activation number.
     * 
     * @param p_voucherActivationNumber Activation number of a voucher.
     */
    public void setVoucherActivationNumber(String p_voucherActivationNumber)
    {
        i_voucherActivationNumber = p_voucherActivationNumber;
    }

    /** Gets the voucher activation number.
     * 
     * @return Activation number of a voucher.
     */
    public String getVoucherActivationNumber()
    {
        return(i_voucherActivationNumber);
    }

    /** Sets the voucher serial number on the voucher data object held within
     *  this object.
     * 
     * @param p_voucherSerialNumber Serial number of a voucher.
     */
    public void setVoucherSerialNumber(String p_voucherSerialNumber)
    {
        // TODO check implications
        i_voucherSerialNumber = p_voucherSerialNumber;
//        i_voucherData.setSerialNumber(p_voucherSerialNumber);
    }

    /** Gets the External Voucher Database Used flag.
     * 
     * @return Flag indicating whether vouchers are held in an external database.
     */
    public boolean getEvdbUsed()
    {
        return(i_evdbUsed);
    }

    /** Sets the External Voucher Database Used flag.
     * 
     * @param p_evdbUsed Flag indicating whether vouchers are held in an external database.
     */
    public void setEvdbUsed(boolean p_evdbUsed)
    {
        i_evdbUsed = p_evdbUsed;
    }

    /** Sets the Voucher history detail screen data.
     * 
     * @param p_historyScreenData History details of vouchers.
     */
    public void setHistoryScreenData(CcVoucherRefillHistoryDetailScreenData p_historyScreenData)
    {
        i_historyScreenData = p_historyScreenData;
    }

    /** Gets the Voucher history detail screen data.
     * 
     * @return History details of vouchers.
     */
    public CcVoucherRefillHistoryDetailScreenData getHistoryScreenData()
    {
        return (i_historyScreenData);
    }
    
    /** Set the recharge type.
     * 
     * @param p_rechargeType Type of recharge.
     */
    public void setRechargeType( char p_rechargeType)
    {
        i_rechargeType = p_rechargeType;
        
    }
    
    /** Get the recharge type.
     * 
     * @return Type of recharge.
     */
    public char getRechargeType()
    {
        return (i_rechargeType);
    }
    
    /**
     * Converts a numeric voucher status to a GUI description.
     * @param p_voucherStatus The numeric representation of a voucher status.
     * @return A text based representation of a voucher status for use in GUI.
     */
    public static String getVoucherStatusGuiStr(int p_voucherStatus)
    {
        return 
            ((p_voucherStatus == VoucherEnquiryData.C_VOUCHER_AVAILABLE   ? C_VOUCHER_AVAILABLE_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_ASSIGNED    ? C_VOUCHER_ASSIGNED_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_DAMAGED     ? C_VOUCHER_DAMAGED_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_STOLEN      ? C_VOUCHER_STOLEN_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_PENDING     ? C_VOUCHER_PENDING_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_UNAVAILABLE ? C_VOUCHER_UNAVAILABLE_GUI_DESC :
             (p_voucherStatus == VoucherEnquiryData.C_VOUCHER_UNKNOWN     ? C_VOUCHER_UNKNOWN_GUI_DESC : 
                                                                            C_VOUCHER_UNDEFINED_GUI_DESC))))))));
    }
    
    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if this feature is licensed.
     */
    public boolean hasEnhancedValueVoucherFeatureLicence()
    {
        return i_hasEnhancedValueVoucherFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if this feature should be licensed.
     */
    public void setEnhancedValueVoucherFeatureLicence(boolean p_isLicenced)
    {
        i_hasEnhancedValueVoucherFeatureLicence = p_isLicenced;
        return;
    }
    
}