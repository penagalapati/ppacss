////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPaymentScreenData.java
//      DATE            :       20-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1266/5099
//                              PRD_PPAK00_DEV_IN_30
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Payment screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PaprPaymentProfileData;
import com.slb.sema.ppas.common.dataclass.PaymentDataSet;
import com.slb.sema.ppas.common.dataclass.PaymentResultData;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;


/**
 * Gui Screen Data object for the Payment screen.
 */
public class CcPaymentScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** PaymentDataSet object wrapped by this object. */
    private PaymentDataSet i_paymentDataSet;

    /** PaymentResultData object wrapped by this object. */
    private PaymentResultData i_paymentResultData;

    /** Payment types from MiscCodes in business configuration cache. */
    private MiscCodeDataSet i_paymentTypes;

    /** Payment profiles from PaprPaymentProfiles in business configuration 
     *  cache. 
     */
    private PaprPaymentProfileData [] i_paymentProfiles;

    /** Voucher Groups DataSet from CdgrAcceCardGroupDataSet in business 
     *  config. 
     */
    private CdgrAcceCardGroupDataSet i_voucherGroupDetails;

    /** Text giving info on when/whether an update was made on this screen. */
    private String i_infoText;

    /** Flag to indicate whether a successful update has been done. */
    private boolean i_successfulUpdate;

    /** Amount attempted on failed payment. */
    private Money i_oldPayAmount = null;

    /** Description 1 attempted on failed payment. */
    private String i_oldPayDescription1 = "";

    /** Description 2 attempted on failed payment. */
    private String i_oldPayDescription2 = "";

    /** Payment profile attempted on failed payment. */
    private String i_oldPaymentProfile = "";

    /** Payment Type attempted on failed payment. */
    private String i_oldPayType = "";

    /** Payment history detail screen data. */
    private CcPaymentHistoryDetailScreenData i_paymentScreenData = null;

    /** Set of currencies and their associated data retrieved from the business
     *  configuration cache.
     */
    private CufmCurrencyFormatsDataSet i_currencyDataSet = null;

    /** Holds the subscriber's preferred currency. */
    private PpasCurrency i_preferredCurrency = null;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcPaymentScreenData( GuiContext p_guiContext,
                                GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
        i_infoText = "";
        i_successfulUpdate = false;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------


    /**
     * Returns <code>true</code> if the Operator contained in the request
     * is permitted to make a payment.  Otherwise returns <code>false</code>.
     * @return Flag indicating whether a payment may be posted.
     */
    public boolean makePaymentIsPermitted()
    {
        return (i_gopaData.makePaymentIsPermitted());
    }

    /**
     * Returns the PaymentDataSet object wrapped within this object.
     * Returns null if there isn't one.
     * @return Set of payments.
     */
    public PaymentDataSet getPaymentDataSet()
    {
        return i_paymentDataSet;
    }

    /**
     * Returns the PaymentResultData object wrapped within this object.
     * Returns null if there isn't one.
     * @return Payment Result data.
     */
    public PaymentResultData getPaymentResultData()
    {
        return i_paymentResultData;
    }

    /**
     * Returns the payment types wrapped within this object.
     * Returns null if there isn't any.
     * @return Set of payment types.
     */
    public MiscCodeDataSet getPaymentTypes()
    {
        return i_paymentTypes;
    }

    /**
     * Returns the payment profiles wrapped within this object.
     * Returns null if there isn't any.
     * @return Set of payment profiles.
     */
    public PaprPaymentProfileData [] getPaymentProfiles()
    {
        return i_paymentProfiles;
    }

    /**
     * Returns the voucher group details wrapped within this object.
     * Returns null if there isn't any.
     * @return Set of voucher groups.
     */
    public CdgrAcceCardGroupDataSet getVoucherGroupDetails()
    {
        return i_voucherGroupDetails;
    } 


    /** Sets the PaymentDataSet object wrapped within this object.
     * @param p_paymentDataSet Set of payment data objects.
     */
    public void setPaymentDataSet( PaymentDataSet p_paymentDataSet )
    {
        i_paymentDataSet = p_paymentDataSet;
    }

    /** Sets the PaymentResultData object wrapped within this object.
     * @param p_paymentResultData Payment Result data.
     */
    public void setPaymentResultData( PaymentResultData p_paymentResultData )
    {
        i_paymentResultData = p_paymentResultData;
    }

    /** Sets the payment types wrapped within this object.
     * @param p_paymentTypes Set of payment types.
     */
    public void setPaymentTypes (MiscCodeDataSet p_paymentTypes)
    {
        i_paymentTypes = p_paymentTypes;
    }

    /** Sets the Voucher groups wrapped with this object.
     * @param p_paymentProfiles Set of payment profiles.
     */
    public void setPaymentProfiles (PaprPaymentProfileData [] p_paymentProfiles)
    {
        i_paymentProfiles = p_paymentProfiles;
    }

    /** Sets the Voucher group details wrapped with this object.
     * @param p_voucherGroupDetails Set of Voucher groups.
     */
    public void setVoucherGroupDetails (
                   CdgrAcceCardGroupDataSet p_voucherGroupDetails )
    {
        i_voucherGroupDetails = p_voucherGroupDetails;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is fully populated.
     */
    public boolean isPopulated()
    {
        return ( i_paymentDataSet != null ); 
    }

    /** Sets the text giving info on when/whether an update was done on this
     *  screen.
     * @param p_text Text to be displayed.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving info when/whether a payment was done on this
     *  screen.
     * @return String indicating whether the payment was made on this screen.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Sets flag to indicate whether a successful update had been done.
     * @param p_success Flag indicating whether the update was successful.
     */
    public void setSuccessfulUpdate(boolean p_success)
    {
        i_successfulUpdate = p_success;
    }

    /** Gets flag to indicate whether a successful update has been done.
     * @return Flag indicating whether the update was successful.
     */
    public boolean getSuccessfulUpdate()
    {
        return(i_successfulUpdate);
    }

    /** Sets the failed payment Amount.
     * @param p_amount Amount of failed payment.
     */
    public void setOldPayAmount(Money p_amount)
    {
        i_oldPayAmount = p_amount;
    }

    /** Gets the previous payment amount as a string without comma separators.
     * @return Failed amount as a string.
     */
    public String getOldPayAmountAsString()
    {
        String l_oldAmount;
        
        l_oldAmount = super.getFormattedAmount(i_oldPayAmount);
        
        return (l_oldAmount.replaceAll(",", ""));
    }
   
    /** Gets the previous payment currency.
     * @return String representing the currency.
     */
    public String getOldCurrencyAsString()
    {
        return (super.getCurrencyAsString(i_oldPayAmount));
    }

    /** Sets failed payment Description 1.
     * @param p_description1 First line of description.
     */
    public void setOldPayDescription1(String p_description1)
    {
        i_oldPayDescription1 = p_description1;
    }

    /** Gets failed payment description 1.
     * @return First line of description.
     */
    public String getOldPayDescription1()
    {
        return (i_oldPayDescription1);
    }

    /** Sets failed payment Description2.
     * @param p_description2 Second line of description.
     */
    public void setOldPayDescription2(String p_description2)
    {
        i_oldPayDescription2 = p_description2;
    }

    /** Gets failed payment description 2.
     * @return Second line of description.
     */
    public String getOldPayDescription2()
    {
        return (i_oldPayDescription2);
    }

    /** Sets the failed payment Voucher Group.
     * @param p_paymentProfile Payment profile.
     */
    public void setOldPaymentProfile(String p_paymentProfile)
    {
        i_oldPaymentProfile = p_paymentProfile;
    }

    /** Gets the failed payment voucher group.
     * @return Payment profile.
     */
    public String getOldPaymentProfile()
    {
        return (i_oldPaymentProfile);
    }

    /** Sets the failed payment type.
     * @param p_paymentType Type of payment.
     */
    public void setOldPayType(String p_paymentType)
    {
        i_oldPayType = p_paymentType;
    }

    /** Gets the failed payment type.
     * @return Type of payment.
     */
    public String getOldPayType()
    {
        return (i_oldPayType);
    }

    /** Sets the Payment history detail screen data.
     * @param p_paymentScreenData History screen data.
     */
    public void setHistoryScreenData(CcPaymentHistoryDetailScreenData
                                     p_paymentScreenData)
    {
        i_paymentScreenData = p_paymentScreenData;
    }

    /** Gets the Payment history detail screen data.
     * @return History screen data.
     */
    public CcPaymentHistoryDetailScreenData getHistoryScreenData()
    {
        return (i_paymentScreenData);
    }

    /** Returns the configured set of currencies with their associated data.
     * @return Set of curency data.
     */
    public CufmCurrencyFormatsDataSet getCurrencyDataSet()
    {
        return i_currencyDataSet;
    }

    /** Sets the configured set of currencies with their associated data.
     * @param p_currencyDataSet Set of curency data.
     */
    public void setCurrencyDataSet(CufmCurrencyFormatsDataSet p_currencyDataSet)
    {
        i_currencyDataSet = p_currencyDataSet;
    }

    /**
     * Sets the subscriber's preferred currency.
     * 
     * @param p_preferredCurrency The subscriber's preferred currency.
     */
    public void setPreferredCurrency(PpasCurrency p_preferredCurrency)
    {
        i_preferredCurrency = p_preferredCurrency;
    }

    /**
     * Gets the preferred currency of the subscriber.
     * 
     * @return The subscriber's preferred currency.
     */
    public PpasCurrency getPreferredCurrency()
    {
        return i_preferredCurrency;
    }

} // end of CcPaymentScreenData class
