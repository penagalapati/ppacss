////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcMenuServlet.Java
//      DATE            :       15-Aug-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1499/6262
//                              PRD_PPAK00_GEN_CA_379
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Servlet to to handle requests for the
//                              Menu bar.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 10/06/05 | R O'Brien  | Add the setting of the charged  | Ppac#1560/6587
//          |            | event feature licence on the    | PRD_ASCS00_GEN_CA_49
//          |            | menu screen data object.        |
//----------+------------+---------------------------------+--------------------
// 10/08/05 | L Byrne    | Add the setting of the service  | Ppac#1599/6734
//          |            | fee   feature licence on the    | PRD_ASCS00_GEN_CA_48
//          |            | menu screen data object.        |
//----------+------------+---------------------------------+--------------------
// 08/06/07 | Andy Harris| Add the setting of the statement| Ppac#3124/11683
//          |            | of account feature licence on   | PRD_ASCS00_GEN_CA_114
//          |            | the menu screen data object.    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcMenuScreenData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;


/**
 * Servlet to to handle requests for the Menu bar.
 */
public class CcMenuServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcMenuServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcMenuServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /**
     * Currently does nothing.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 20000, this,
                "Entered " + C_METHOD_doInit);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 20090, this,
                "Leaving " + C_METHOD_doInit);
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the Menu bar.
     * @param p_request      HTTP request.
     * @param p_response     HTTP response.
     * @param p_httpSession  Current HttpSession.
     * @param p_guiRequest   GuiRequest object.
     * @return URL to which the request should be forwarded.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                      l_forwardUrl     = null;
        CcMenuScreenData            l_menuScreenData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Create screen data object.
        l_menuScreenData = new CcMenuScreenData(i_guiContext, p_guiRequest);

        getBusinessConfigData(p_guiRequest,
                              l_menuScreenData);

        l_forwardUrl = "/jsp/cc/ccmenu.jsp";

        p_request.setAttribute("p_guiScreenData", l_menuScreenData);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13900, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getBusinessConfigData =
                                         "getBusinessConfigData";
    /**
     *  Obtains the business configuration data and makes it available in the
     *  given menu 'screen data' object.
     * @param p_guiRequest   GuiRequest object.
     * @param p_screenData   Set of menu data.
     */
    private void getBusinessConfigData(GuiRequest       p_guiRequest,
                                       CcMenuScreenData p_screenData)
    {
        GuiSession          l_guiSession = null;
        int                 l_miscFieldTitleCount = 0;
        Market              l_market = null;
        ServiceClass        l_serviceClass = null;
        int                 l_accumulatorsCount = 0;
        boolean             l_accumulatorsConfigured = true;
        FeatureLicenceCache l_featureLicenceCache;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27024, this,
                "Entered " + C_METHOD_getBusinessConfigData);
        }

        l_guiSession = (GuiSession)p_guiRequest.getSession();
        l_market = l_guiSession.getCsoMarket();

        // Determine if any miscellaneous data fields are configured for this
        // market.
        l_miscFieldTitleCount = i_configService
                               .getMiscFieldTitles()
                               .getMfltMiscFieldTitlesData(l_market)
                               .getFieldTitleCount();

        if (l_miscFieldTitleCount == 0)
        {
            p_screenData.setAdditionalInfoIsConfigured(false);
        }
        else
        {
            p_screenData.setAdditionalInfoIsConfigured(true);
        }

        // Determine if accumulators are configured for this service class.
        // Service class is cached on the session by the CcAccountDetailsServlet
        // for this purpose, but should not be used at any other time, so is set
        // to null here.
        l_serviceClass = l_guiSession.getServiceClass();
        l_guiSession.setServiceClass(null);

        // The cached service class should not be null here, but don't want
        // to get an exception and stop the menu from displaying if it is,
        // so default to assuming that they are configured.
        if (l_serviceClass != null)
        {
            l_accumulatorsCount = i_configService.getAccumulatorConfig().getAccfCount(l_market, 
                                                                                      l_serviceClass);
            if (l_accumulatorsCount < 1)
            {
                l_accumulatorsConfigured = false;
            }
        }
        p_screenData.setAccumulatorsConfigured(l_accumulatorsConfigured);
        
        // Set feature licence flags.
        try
        {
            l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
            
            p_screenData.setSubscriberSegmentationFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_SUBSCIRBER_SEGMENTATION));
                
            p_screenData.setFamilyAndFriendsFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_FAF));
                
            p_screenData.setCommunityChargingFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_COMMUNITY_CHARGING));
            
            p_screenData.setChargedEventCountersFeatureLicence(false);
            if ((l_featureLicenceCache.isLicensed(FeatureLicence.C_FEATURE_CODE_IVR_CHANGE_CLASS)) ||
                (l_featureLicenceCache.isLicensed(FeatureLicence.C_FEATURE_CODE_FAF_NUMBER_ADDITION)) ||
                (l_featureLicenceCache.isLicensed(FeatureLicence.C_FEATURE_CODE_IVR_BAL_ENQ_CHARGE)))
            {
                p_screenData.setChargedEventCountersFeatureLicence(true);
            }

            //Service Fee Deduction
            p_screenData.setServiceFeeDeductionFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_VAR_FREQ_CHG));
            
            p_screenData.setCallHistoryFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_CALL_HISTORY));
              
            p_screenData.setStatementOfAccountFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_STATEMENT_OF_ACCOUNT));
            
        }
        catch (PpasServiceException p_ppasServiceE)
        {
            i_logger.logMessage(p_ppasServiceE);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27095, this,
                "Leaving " + C_METHOD_getBusinessConfigData);
        }

        return;
    }

} // end class CcMenuServlet
