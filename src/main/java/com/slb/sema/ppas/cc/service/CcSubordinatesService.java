////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubordinatesService.java
//      DATE            :       14-Mar-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PRD_PPAK00_DEV_IN_37
//                              PpaLon#1325/5613
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasSubordinatesService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
//14-Oct-03 | M Erskine  | Changes to allow compilation due| PpacLon#57/550
//          |            | to changes to IS API            |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccountDataSet;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.is.isapi.PpasSubordinatesService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * subordinates.
 */
public class CcSubordinatesService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcSubordinatesService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasSubordinatesService i_subordinatesService;
    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcSubordinatesService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct erxceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcSubordinatesService(
                            GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext )
    {
        super( p_guiRequest, p_flags, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal additional info service to be used by this service.
        i_subordinatesService = new PpasSubordinatesService(p_guiRequest, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addSubordinate = "addSubordinate";
    /**
     * adds account as a subordinate of account contained in the request.
     * @param p_guiRequest    The request being processed.
     * @param p_subMsisdn     Subordinate mobile number.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return A GUI response.
     */
    public GuiResponse addSubordinate( GuiRequest p_guiRequest,
                                       Msisdn     p_subMsisdn,
                                       long       p_timeoutMillis )
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 10020, this,
                "Entered " + C_METHOD_addSubordinate);
        }

        try
        {
            i_subordinatesService.addSubordinate( p_guiRequest, p_subMsisdn, p_timeoutMillis );

            l_guiResponse
                = new GuiResponse(
                          p_guiRequest,
                          p_guiRequest.getGuiSession().getSelectedLocale(),
                          GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                          new Object[] {"Create new subordinate"},
                          GuiResponse.C_SEVERITY_SUCCESS );

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10030, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "add the subordinate", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 10040, this,
                "Leaving " + C_METHOD_addSubordinate);
        }

        return ( l_guiResponse );

    }  // end public GuiResponse addSubordinate(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSubordinateDataSet = "getSubordinateDataSet";
    /**
     * Retrieves list of subordinates and their account data.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis timeout for getting a database connection
     * @param p_masterCustId master cust id of accounts to be retrieved
     * @return CcAccountDataSetResponse contains AccountDataSet object
     */
    public CcAccountDataSetResponse getSubordinateDataSet(GuiRequest p_guiRequest,
                                                          long       p_timeoutMillis,
                                                          String     p_masterCustId)
    {
        CcAccountDataSetResponse        l_ccAccountDataSetResponse  = null;
        GuiResponse                     l_guiResponse               = null;
        AccountDataSet                  l_accountDataSet            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 10050, this,
                "Entered " + C_METHOD_getSubordinateDataSet);
        }

        try
        {

            l_accountDataSet = i_subordinatesService.getSubordinates(
                                                p_guiRequest,
                                                PpasService.C_FLAG_INCLUDE_DISCONNECTED,
                                                p_timeoutMillis,
                                                p_masterCustId );

            l_ccAccountDataSetResponse
                = new CcAccountDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_accountDataSet);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10160, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "get the subordinate's details", l_pSE);

            l_ccAccountDataSetResponse =
                new CcAccountDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 10070, this,
                "Leaving " + C_METHOD_getSubordinateDataSet);
        }

        return ( l_ccAccountDataSetResponse );
    }  // end public CcAccountDataSetResponse getSubordinateDataSet(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getMasterBasicData = "getMasterBasicData";
    /**
     * Retrieves BasicAccountData for Master of account in the request.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Basic details of the master account.
     */
    public CcBasicAccountDataResponse getMasterBasicData(
                                             GuiRequest p_guiRequest,
                                             long       p_timeoutMillis )
    {
        CcBasicAccountDataResponse      l_ccBasicAccountDataResponse     = null;
        GuiResponse                     l_guiResponse               = null;
        BasicAccountData                l_masterAccount             = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 10150, this,
                "Entered " + C_METHOD_getMasterBasicData);
        }

        try
        {
            l_masterAccount = i_subordinatesService.getMasterBasicData(
                                        p_guiRequest,
                                        PpasService.C_FLAG_INCLUDE_DISCONNECTED,
                                        p_timeoutMillis );


            l_ccBasicAccountDataResponse
                = new CcBasicAccountDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),  // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_masterAccount);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10160, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "get the master account data", l_pSE);

            l_ccBasicAccountDataResponse =
                new CcBasicAccountDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(), // PpacLon#1/17
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 10170, this,
                "Leaving " + C_METHOD_getMasterBasicData);
        }

        return ( l_ccBasicAccountDataResponse );

    }  // end public CcAccountDataResponse getMasterBasicData(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_canCancelFutureDisconnection = "canCancelFutureDisconnection";
    /**
     * This method returns true if a subordinate subscription with a future-scheduled
     * disconnection can have this disconnection cancelled.  The disconnection is allowed
     * if the master is not scheduled for future disconnection, or if it is scheduled
     * but for a later date.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return True if the disconnection can be cancelled.
     */
    public boolean canCancelFutureDisconnection(
                                         GuiRequest p_guiRequest,
                                         long       p_timeoutMillis )
    {
        boolean l_isAvailable = false;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 20150, this,
                "Entered " + C_METHOD_canCancelFutureDisconnection);
        }

        try
        {
            l_isAvailable = i_subordinatesService.canCancelFutureDisconnection(p_guiRequest, p_timeoutMillis);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 20160, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            handlePpasServiceException(p_guiRequest, 0, "get the master account data", l_pSE );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 10170, this,
                "Leaving " + C_METHOD_getMasterBasicData);
        }

        return ( l_isAvailable );
    }
} // end public class CcAccountSubordinatesService
