////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcStatementOfAccountResponse.Java
//DATE            :       30-May-2007
//AUTHOR          :       Andy Harris
//REFERENCE       :       PpacLon#3124/11533
//                        PRD_ASCS00_GEN_CA_114
//
//COPYRIGHT       :       WM-data 2007
//
//DESCRIPTION     :       Statement of Account Response.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//         |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.StatementOfAccountDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
* CcXResponse class for StatementOfAccountData objects.
*/
public class CcStatementOfAccountResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcStatementOfAccountResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------
    /** The StatementOfAccountData object wrapped by this object. */
    private StatementOfAccountDataSet    i_statementOfAccountData;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------
    /**
     * Uses a StatementOfAccountDataSetobject and the additional necessary objects to create a response containing
     * statement of account event data.
     * @param p_guiRequest The request being processed.
     * @param p_messageKey Response message key.
     * @param p_messageParams Any parameters to be substituted into response message.
     * @param p_messageSeverity Severity of response.
     * @param p_statementOfAccountDataSet The Statement of Account details.
     */
    public CcStatementOfAccountResponse(GuiRequest p_guiRequest,
                                        String p_messageKey,
                                        Object[] p_messageParams,
                                        int p_messageSeverity,
                                        StatementOfAccountDataSet p_statementOfAccountDataSet)
    {
        super(p_guiRequest, p_messageKey, p_messageParams, p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_statementOfAccountData = p_statementOfAccountDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10010,
                           this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /**
     * Returns Statement of Account information.
     * @return StatementOfAccount data.
     */
    public StatementOfAccountDataSet getstatementOfAccountData()
    {
        return i_statementOfAccountData;
    }
}