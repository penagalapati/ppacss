// //////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcFamilyAndFriendsServlet.Java
//      DATE            :       29-Sep-2003
//      AUTHOR          :       Stephen Pinton
//      REFERENCE       :       PpaLon#2002/8478
//                              PRD_PPAK00_GEN_CA_428
//
//      COPYRIGHT       :       Schlumberger 2003
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Family & Friends screen and pop-up.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//12-Dec-05 | M.Alm      | Add Account level               | PpacLon#1897/7624
//          |            | functionality                   | PRD_ASCS00_GEN_CA_59
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcFamilyAndFriendsScreenData;
import com.slb.sema.ppas.cc.service.CcFamilyAndFriendsResponse;
import com.slb.sema.ppas.cc.service.CcFamilyAndFriendsService;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsData;
import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsDataSet;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * Servlet to handle requests from the Family & Friends screen.
 */
public class CcFamilyAndFriendsServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String       C_CLASS_NAME                = "CcFamilyAndFriendsServlet";

    /** Constant used to construct info text. Value is {@value}. */
    private static final String       C_ADD_INFO_TEXT             = "Add";

    /** Constant used to construct info text. Value is {@value}. */
    private static final String       C_DELETE_INFO_TEXT          = "Delete";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Family & Friends Service to be used by this servlet. */
    private CcFamilyAndFriendsService i_ccFamilyAndFriendsService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor, only calls the superclass constructor.
     */
    public CcFamilyAndFriendsServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START
                    | WebDebug.C_ST_CONFIN_END, C_CLASS_NAME, 10000, this, "Constructing/ed " + C_CLASS_NAME);
        }

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";

    /**
     * Service method to control requests and responses to the JSP script for the family and friends screen
     * service.
     * @param p_request the request
     * @param p_response the response
     * @param p_httpSession current HttpSession
     * @param p_guiRequest a GuiRequest
     * @return forwarding URL
     */
    protected String doService(HttpServletRequest p_request,
                               HttpServletResponse p_response,
                               HttpSession p_httpSession,
                               GuiRequest p_guiRequest)
    {
        String l_command = null;
        String l_forwardUrl = null;
        GuiScreenData l_screenData = null;
        boolean l_accessGranted = false;
        GopaGuiOperatorAccessData l_gopaData = null;
        FachFafChargingIndDataSet l_fachDataSet = null;
        DeciDefaultChargingIndDataSet l_deciDataSet = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START
                    | WebDebug.C_ST_REQUEST, p_guiRequest, C_CLASS_NAME, 40000, this, "ENTERED "
                    + C_METHOD_doService);
        }

        // Construct CcFamilyAndFriendsService if it does not already exist
        if (i_ccFamilyAndFriendsService == null)
        {
            i_ccFamilyAndFriendsService = new CcFamilyAndFriendsService(p_guiRequest,
                                                                        0,
                                                                        i_logger,
                                                                        i_guiContext);
        }

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest, 0, p_request, "p_command", true, "", new String[] {
                    "GET_SCREEN_DATA", "ADD_NUMBER", "DELETE_NUMBER"});

            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession)p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasFafScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("ADD_NUMBER") || l_command.equals("DELETE_NUMBER"))
                    && l_gopaData.updateFafListPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access
                GuiResponse l_guiResponse = new GuiResponse(p_guiRequest,
                                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                                            GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                            new Object[] {},
                                                            GuiResponse.C_SEVERITY_FAILURE);

                l_screenData = new GuiScreenData(i_guiContext, p_guiRequest);

                l_screenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";
            }
            else
            {
                l_fachDataSet = i_configService.getAllFach();

                l_deciDataSet = i_configService.getAvailableDeci();

                l_screenData = new CcFamilyAndFriendsScreenData(i_guiContext, p_guiRequest);

                if (l_command.equals("DELETE_NUMBER"))
                {
                    deleteNumber(p_guiRequest, p_request, (CcFamilyAndFriendsScreenData)l_screenData);
                }
                else if (l_command.equals("ADD_NUMBER"))
                {
                    addNumber(p_guiRequest, p_request, (CcFamilyAndFriendsScreenData)l_screenData);
                }

                getMainScreenData(p_guiRequest, (CcFamilyAndFriendsScreenData)l_screenData);

                ((CcFamilyAndFriendsScreenData)l_screenData).setChargingIndDataSet(l_fachDataSet);

                ((CcFamilyAndFriendsScreenData)l_screenData).setAvailableDeciData(l_deciDataSet);

                l_forwardUrl = "/jsp/cc/ccfamilyandfriends.jsp";
            }
        }

        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest,
                               C_CLASS_NAME,
                               40100,
                               this,
                               "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_screenData = new GuiScreenData(i_guiContext, p_guiRequest);

            GuiResponse l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_screenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
        } // end catch

        p_request.setAttribute("p_guiScreenData", l_screenData);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END
                    | WebDebug.C_ST_REQUEST, p_guiRequest, C_CLASS_NAME, 40200, this, "LEAVING "
                    + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // Private Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getScreenData = "getMainScreenData";

    /**
     * Method to retrieve the account's F&F numbers for the main F&F screen.
     * @param p_guiRequest a GuiRequest
     * @param p_screenData a CcFamilyAndFriendsScreenData
     */
    private void getMainScreenData(GuiRequest p_guiRequest, CcFamilyAndFriendsScreenData p_screenData)
    {
        CcFamilyAndFriendsResponse l_ccFafResponse = null;
        Boolean accountLevelEnabled = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           50000,
                           this,
                           "ENTERED " + C_METHOD_getScreenData);
        }

        l_ccFafResponse = i_ccFamilyAndFriendsService.getNumbers(p_guiRequest, i_timeout);

        p_screenData.addRetrievalResponse(p_guiRequest, l_ccFafResponse);

        if (l_ccFafResponse.isSuccess())
        {
            p_screenData.setFamilyAndFriendsDataSet(l_ccFafResponse.getDataSet());
        }

        accountLevelEnabled =
            i_configService.getSyfgConfigData().getFlagValue(SyfgSystemConfigData.C_DESTINATION_FAF,
                                                             SyfgSystemConfigData.C_ENABLE_ACCOUNT_LEVEL_FAF);

        if (accountLevelEnabled != null)
        {
            p_screenData.setAccountLevelEnabled(accountLevelEnabled.booleanValue());
        }
        else
        {
            p_screenData.setAccountLevelEnabled(false);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           50100,
                           this,
                           "Leaving " + C_METHOD_getScreenData);
        }

    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_deleteNumber = "deleteNumber";

    /**
     * Method to delete a list of F&F numbers from an account.
     * @param p_guiRequest a GuiRequest
     * @param p_request the HttpServletRequest containing the URL
     * @param p_screenData the screen data
     * @throws PpasServletException if an error occurs parsing the request parameters
     */
    private void deleteNumber(GuiRequest p_guiRequest,
                              HttpServletRequest p_request,
                              CcFamilyAndFriendsScreenData p_screenData) throws PpasServletException
    {
        GuiResponse l_guiResponse = null;
        int l_forDeletion = 0;
        String l_num = null;
        String l_level = null;
        String l_chargingInd = null;
        FamilyAndFriendsDataSet l_fafDataSet = new FamilyAndFriendsDataSet();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           60000,
                           this,
                           "ENTERED " + C_METHOD_deleteNumber);
        }

        l_forDeletion = Integer.parseInt(getValidParam(p_guiRequest,
                                                       0,
                                                       p_request,
                                                       "p_forDeletion",
                                                       true,
                                                       "",
                                                       PpasServlet.C_TYPE_NUMERIC));

        // Loop round the numbers to be deleted in the request.
        for (int l_loop = 1; l_loop <= l_forDeletion; l_loop++)
        {
            l_num = getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_number" + l_loop,
                                  true,
                                  "",
                                  PpasServlet.C_TYPE_ANY);

            l_level = getValidParam(p_guiRequest,
                                    0,
                                    p_request,
                                    "p_level" + l_loop,
                                    true,
                                    "",
                                    PpasServlet.C_TYPE_ANY);

            l_chargingInd = getValidParam(p_guiRequest,
                                          0,
                                          p_request,
                                          "p_chargingInd" + l_loop,
                                          true,
                                          "",
                                          PpasServlet.C_TYPE_ANY);

            l_fafDataSet.addData(new FamilyAndFriendsData(l_num, l_chargingInd, l_level));
        }

        p_screenData.setInfoText(C_DELETE_INFO_TEXT);

        l_guiResponse = i_ccFamilyAndFriendsService.deleteNumber(p_guiRequest, i_timeout, l_fafDataSet);

        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           60100,
                           this,
                           "Leaving " + C_METHOD_deleteNumber);
        }
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addNumber = "addNumber";

    /**
     * Method to add a single F&F number to an account.
     * @param p_guiRequest a GuiRequest
     * @param p_request the HttpServletRequest containing the URL
     * @param p_screenData the screen data
     * @throws PpasServletException if an error occurs parsing the request parameters
     */
    private void addNumber(GuiRequest p_guiRequest,
                           HttpServletRequest p_request,
                           CcFamilyAndFriendsScreenData p_screenData) throws PpasServletException
    {
        String l_fafNumber = null;
        GuiResponse l_guiResponse = null;
        String l_chargingInd = null;
        String l_level = null;
        FamilyAndFriendsDataSet l_fafDataSet = null;
        Boolean accountLevelEnabled = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START
                    | WebDebug.C_ST_REQUEST, p_guiRequest, C_CLASS_NAME, 80000, this, "ENTERED "
                    + C_METHOD_addNumber);
        }

        l_fafNumber = getValidParam(p_guiRequest,
                                    0,
                                    p_request,
                                    "p_number",
                                    true,
                                    "",
                                    PpasServlet.C_TYPE_NUMERIC,
                                    30,
                                    PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        //If the Account Level functionality is enabled then get the level
        // parameter from the GUI request, otherwise set the subscriber level.
        accountLevelEnabled =
            i_configService.getSyfgConfigData().getFlagValue(SyfgSystemConfigData.C_DESTINATION_FAF,
                                                             SyfgSystemConfigData.C_ENABLE_ACCOUNT_LEVEL_FAF);

        if(accountLevelEnabled != null && accountLevelEnabled.booleanValue())
        {
            l_level = getValidParam(p_guiRequest,
                                    0,
                                    p_request,
                                    "p_level",
                                    true,
                                    "",
                                    PpasServlet.C_TYPE_ANY);
        }
        else 
        {
            l_level = FamilyAndFriendsData.C_LEVEL_SUBSCRIBER;
        }

        l_chargingInd = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_chargingInd",
                                      true,
                                      "",
                                      PpasServlet.C_TYPE_ANY);

        p_screenData.setInfoText(C_ADD_INFO_TEXT);

        l_fafDataSet = new FamilyAndFriendsDataSet(new FamilyAndFriendsData(l_fafNumber,
                                                                            l_chargingInd,
                                                                            l_level));

        l_guiResponse = i_ccFamilyAndFriendsService.addNumber(p_guiRequest, i_timeout, l_fafDataSet);

        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           80100,
                           this,
                           "Leaving " + C_METHOD_addNumber);
        }
    }
}