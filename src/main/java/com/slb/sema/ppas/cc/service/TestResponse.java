////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TestResponse.java
//      DATE            :       24-Jan-2002
//      AUTHOR          :       Erik Clayton
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Response object for testing.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.web.support.WebDebug;

/**
 * Response from a call to the recharge service. 
 */
public class TestResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TestResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** Flag indicating whether search results should be shown. */
    private boolean i_showSearchResults;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /** Standard constructor. */
    public TestResponse()
    {

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10090, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Get flag indicating whether search results should be shown.
     * 
     * @return True if search results should be shown.
     */
    public boolean getShowSearchResults()
    {
        return(i_showSearchResults);
    }

    /** Set flag indicating whether search results should be shown.
     * 
     * @param p_showSearchResults True if search results should be shown.
     */
    public void setShowSearchResults(boolean p_showSearchResults)
    {
        i_showSearchResults = p_showSearchResults;
    }

}
