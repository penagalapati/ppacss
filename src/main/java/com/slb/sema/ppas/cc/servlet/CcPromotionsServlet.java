////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromotionsServlet.Java
//      DATE            :       29th April 2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Customer Promotions GUI screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/01/06 | K Goswami  | Changed DateTime to Date        | PpacLon#1058 
//          |            |                                 | CrRr_1058_7237_PromoAllocData.doc
//----------+------------+---------------------------------+--------------------
// 19.01.06 | MAGray     | Change to accept  boolean when  | PpacLon#1954/7831
//          |            | adding
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcPromoAllocScreenData;
import com.slb.sema.ppas.cc.screendata.CcPromoCreditHistoryDetailScreenData;
import com.slb.sema.ppas.cc.screendata.CcPromotionsScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcCustPromoAllocDataSetResponse;
import com.slb.sema.ppas.cc.service.CcCustPromoAllocService;
import com.slb.sema.ppas.cc.service.CcPromotionCreditDataSetResponse;
import com.slb.sema.ppas.cc.service.CcPromotionCreditService;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionDataResponse;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionService;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsDataSet;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.CustPromoAllocData;
import com.slb.sema.ppas.common.dataclass.CustPromoAllocDataSet;
import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.dataclass.PromotionCreditDataSet;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/** This Servlet processes requests from the Customer Promotions JSP
 *  script.
 */
public class CcPromotionsServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPromotionsServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Customer Promotions Credit Service to be used by this
     *  servlet.
     */
    private CcPromotionCreditService i_promoCreditService = null;

    /** The CC GUI Customer Promotion Allocations Service to be used by this
     *  servlet.
     */
    private CcCustPromoAllocService  i_custPromoAllocService = null;

    /** Account service. Used to obtain the subscriber's service class which
     *  is required to obtain the dedicated account division associated with
     *  the promotion.
     */
    private CcAccountService         i_accountService = null;

    /** Recharge division service. Used to obtain recharge division data
     *  detailing how a promotional credit was dividied between a master
     *  and dedicated accounts.
     */
    private CcRechargeDivisionService i_divisionService = null;


    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor - creates a Customer Promotions Servlet instance to handle
     *  requests from the Customer Promotions GUI Screen.
     */
    public CcPromotionsServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 94000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 94090, this,
                "Constructed " + C_CLASS_NAME );
        }

    } // end Constructor CcPromotionsServlet

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /** Performs initialisation specific to this Servlet; currently there is
     *  no specific initialisation for this Servlet.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 94200, this,
                "Entered " + C_METHOD_doInit);
        }

        // Should probably be constructing CcCustomerSelectionService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 94290, this,
                "Leaving " + C_METHOD_doInit);
        }

        return;

    } // end doInit


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to handle requests from the Customer Promotions GUI
     *  screen.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                       l_command              = null;
        String                       l_forwardUrl           = null;
        CcPromotionsScreenData       l_promoScreenData      = null;
        CcPromoAllocScreenData       l_promoAllocScreenData = null;
        String                       l_planId               = null;
        String                       l_allocStartStr        = "";
        String                       l_allocEndStr          = null;
        PpasDateTime                 l_allocStartDT         = null;
        PpasDateTime                 l_allocEndDT           = null;
        GuiResponse                  l_guiResponse          = null;
        PrplPromoPlanDataSet         l_promoConfigDataSet   = null;
        DedaDedicatedAccountsDataSet l_dedaDataSet          = null;
        VospVoucherSplitsDataSet     l_vospDataSet          = null;
        AccountData                  l_accountData          = null;
        CcAccountDataResponse        l_accountDataResp      = null;
        CcPromoCreditHistoryDetailScreenData  l_creditHistoryScreenData = null;
        boolean                      l_accessGranted        = false;
        GopaGuiOperatorAccessData    l_gopaData             = null;


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 94300, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcPromotionCreditService if it does not already exist.
        if (i_promoCreditService == null)
        {
            i_promoCreditService =
                         new CcPromotionCreditService (p_guiRequest,
                                                       0,
                                                       i_logger,
                                                       i_guiContext);
        }

        // Construct CcAccountService if it doesn't already exist
        if (i_accountService == null)
        {
            i_accountService =
                         new CcAccountService (p_guiRequest,
                                               0,
                                               i_logger,
                                               i_guiContext);
        }

        // Construct CcCustPromoAllocService to be used by this servlet if it
        // does not already exist.
        if (i_custPromoAllocService == null)
        {
            i_custPromoAllocService =
                         new CcCustPromoAllocService (p_guiRequest,
                                                      0,
                                                      i_logger,
                                                      i_guiContext);
        }

        // Construct CcRechargeDivisionService to be used by this servlet if it
        // does not already exist
        if (i_divisionService == null)
        {
            i_divisionService = new CcRechargeDivisionService (p_guiRequest,
                                                               i_logger,
                                                               i_guiContext);
        }

        // Create screen data for the Customer Allocation pop-up window.
        l_promoAllocScreenData = new CcPromoAllocScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam (p_guiRequest,
                                       0,
                                       p_request,
                                       "p_command",
                                       true,    // Mandatory ...
                                       "",       // ... so no default
                                       new String []{
                                           "GET_SCREEN_DATA",
                                           "INIT_NEW_ALLOC_POPUP",
                                           "INIT_UPDATE_ALLOC_POPUP",
                                           "ADD_CUST_PROMO_ALLOC",
                                           "UPDATE_CUST_PROMO_ALLOC",
                                           "REMOVE_CUST_PROMO_ALLOC",
                                           "GET_HISTORY_DETAIL"});

            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_APPLICATION_DATA | WebDebug.C_ST_TRACE,
                    p_guiRequest,
                    C_CLASS_NAME, 94400, this,
                    "Received command: " + l_command);
            }

            // Determine if the CSO has privilege to access the requested screen or function
            if ((l_command.equals("GET_SCREEN_DATA"))         ||
                (l_command.equals("INIT_NEW_ALLOC_POPUP"))    ||
                (l_command.equals("INIT_UPDATE_ALLOC_POPUP")) ||
                (l_command.equals("ADD_CUST_PROMO_ALLOC"))    ||
                (l_command.equals("UPDATE_CUST_PROMO_ALLOC")) ||
                (l_command.equals("REMOVE_CUST_PROMO_ALLOC")) ||
                (l_command.equals("GET_HISTORY_DETAIL")))
            {
                l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

                if (!l_gopaData.hasNewSubsScreenAccess())
                {
                    // Insufficient privilege - display Access Error screen & refuse access
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().getSelectedLocale(),
                                                    GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                    new Object [] {},
                                                    GuiResponse.C_SEVERITY_FAILURE);

                    l_promoAllocScreenData.addRetrievalResponse(p_guiRequest,
                                                                l_guiResponse);

                    l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                    p_request.setAttribute("p_guiScreenData",
                                            l_promoAllocScreenData);
                }
                else
                {
                    l_accessGranted = true;
                }
            }

            if (l_accessGranted)
            {
                // Use the business configuration internal service to read the
                // promotion business config data
                l_promoConfigDataSet =
                         i_configService.getAvailablePromoPlans ();

                // Get DataSet of dedicated account (Deda) information defined for
                // the subscriber's market
                l_dedaDataSet = i_configService.getDedicatedAccounts(
                                                    p_guiRequest,
                                                    ((GuiSession)p_guiRequest.getSession())
                                                     .getCsoMarket());

                if ( (l_command.equals("INIT_NEW_ALLOC_POPUP"))    ||
                     (l_command.equals("INIT_UPDATE_ALLOC_POPUP")) ||
                     (l_command.equals("ADD_CUST_PROMO_ALLOC"))    ||
                     (l_command.equals("UPDATE_CUST_PROMO_ALLOC")) ||
                     (l_command.equals("REMOVE_CUST_PROMO_ALLOC")) )
                {
                    // Get DataSet of voucher splits (divisions) (Vosp) information
                    // defined for the subscriber's market
                    l_vospDataSet = i_configService.getVoucherSplits();
    
                    // Store dedicated account and voucher splits configuration
                    // data in the screen data
                    l_promoAllocScreenData.setDacDataSet (l_dedaDataSet);
                    l_promoAllocScreenData.setVospConfigData (l_vospDataSet);

                    // Store the promotions related business configuration in the screen data.
                    l_promoAllocScreenData.setPromoConfigData(l_promoConfigDataSet);

                    // Get the service class of the current subscriber. This is
                    // required to obtain the division associated with the
                    // promotion
                    l_accountDataResp = i_accountService.getFullData(p_guiRequest,
                                                                     i_timeout);

                    l_promoAllocScreenData.addRetrievalResponse (p_guiRequest,
                                                                 l_accountDataResp);
    
                    if (l_accountDataResp.isSuccess())
                    {
                        l_accountData = l_accountDataResp.getAccountData();
    
                        // Store the service class in the screen data
                        l_promoAllocScreenData.setServiceClass (l_accountData.getActiveServiceClass());
                    }
                }

                // Is the incoming command a request to initialise the window
                // used to insert a new allocation?
                if (l_command.equals("INIT_NEW_ALLOC_POPUP"))
                {
                    // Forward to the promotion allocation pop-up window.
                    l_forwardUrl = "/jsp/cc/ccpromoallocation.jsp";

                    // No plan Id for a new allocation - it will default to the
                    // first one in the list of available plans on the screen.
                    l_promoAllocScreenData.setPlanId ("");

                    // Set a blank date in the request for the start date for a new plan.
                    l_allocStartDT = new PpasDateTime ("");

                    l_promoAllocScreenData.setAllocStartDate (l_allocStartDT);

                    // Set a blank date in the request for the end date for a new plan.
                    l_allocEndDT = new PpasDateTime ("");

                    l_promoAllocScreenData.setAllocEndDate (l_allocEndDT);

                    // Indicate that popup is for defining a new allocation
                    l_promoAllocScreenData.setExistingAlloc (false);

                    // Store the screen data as an attribute on the request
                    p_request.setAttribute ("p_guiScreenData",
                                            l_promoAllocScreenData);
                }

                // Is the incoming command a request to initialise the window
                // used to update an existing allocation?
                else if (l_command.equals("INIT_UPDATE_ALLOC_POPUP"))
                {
                    // Forward to the promotion allocation pop-up window.
                    l_forwardUrl = "/jsp/cc/ccpromoallocation.jsp";

                    // Get the promotion identifier (i.e. the plan id of the
                    // allocation that is to be updated).
                    l_planId = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_planId",
                                    true,   // p_planId is mandatory ...
                                    "",     // ... so no default
                                    PpasServlet.C_TYPE_ANY);

                    l_promoAllocScreenData.setPlanId (l_planId);

                    // Get start date for the allocation.
                    l_allocStartStr = getValidParam(
                                        p_guiRequest,
                                        0,
                                        p_request,
                                        "p_allocStartDateTime",
                                        true,   // p_allocStartDate is mandatory ...
                                        "",     // ... so no default
                                        PpasServlet.C_TYPE_DATE);

                    l_allocStartDT = new PpasDateTime (l_allocStartStr);

                    l_promoAllocScreenData.setAllocStartDate (l_allocStartDT);

                    // Get end date for the allocation. 
                    l_allocEndStr = getValidParam(
                                        p_guiRequest,
                                        0,
                                        p_request,
                                        "p_allocEndDateTime",
                                        true,    // parameter is mandatory ...
                                        "",      // ... so no default
                                        PpasServlet.C_TYPE_DATE);

                    l_allocEndDT = new PpasDateTime (l_allocEndStr);

                    l_promoAllocScreenData.setAllocEndDate (l_allocEndDT);

                    // Set up flags indicating whether the allocation is a future
                    // or the current allocation for the customer
                    l_promoAllocScreenData.setCurrentOrFuture (p_guiRequest,
                                                               i_logger);

                    // Indicate that popup is for accessing an existing
                    // allocation
                    l_promoAllocScreenData.setExistingAlloc (true);

                    // Store the screen data as an attribute on the request
                    p_request.setAttribute ("p_guiScreenData",
                                            l_promoAllocScreenData);
                }

                // Command defines request to add or update a customer allocation?
                else if (l_command.equals ("ADD_CUST_PROMO_ALLOC"))
                {
                    setIsUpdateRequest(true);

                    // Forward to the promotion allocation pop-up window from
                    // which the request originated.
                    l_forwardUrl = "/jsp/cc/ccpromoallocation.jsp";

                    insertPromoAllocation (p_request,
                                           p_guiRequest,
                                           l_promoAllocScreenData);
                }

                // Command defines request to add or update a customer allocation?
                else if (l_command.equals ("UPDATE_CUST_PROMO_ALLOC"))
                {
                    setIsUpdateRequest(true);

                    // Forward to the promotion allocation pop-up window from
                    // which the request originated.
                    l_forwardUrl = "/jsp/cc/ccpromoallocation.jsp";

                    updatePromoAllocation (p_request,
                                           p_guiRequest,
                                           l_promoAllocScreenData);
                }

                // Command defines request to delete an existing customer allocation?
                else if (l_command.equals ("REMOVE_CUST_PROMO_ALLOC"))
                {
                    setIsUpdateRequest(true);

                    // Forward to the promotion allocation pop-up window from
                    // which the request originated.
                    l_forwardUrl = "/jsp/cc/ccpromoallocation.jsp";

                    deletePromoAllocation (p_request,
                                           p_guiRequest,
                                           l_promoAllocScreenData);
                }

                else if (l_command.equals ("GET_HISTORY_DETAIL"))
                {
                    // Get details of a particular promotional credit received by
                    // the subscriber
                    l_forwardUrl = "/jsp/cc/ccpromocreditdetails.jsp";

                    l_creditHistoryScreenData =
                        new CcPromoCreditHistoryDetailScreenData (i_guiContext, p_guiRequest);

                    getPromoCreditHistoryDetail (p_request,
                                                 p_guiRequest,
                                                 l_creditHistoryScreenData);
                }
                else
                {   // Command is "GET_SCREEN_DATA" (i.e. get the data for the
                    // main Customer Promotions screen).

                    l_forwardUrl  = "/jsp/cc/ccpromotions.jsp";

                    // Create screen data object by which to pass data back.
                    l_promoScreenData =
                           new CcPromotionsScreenData (i_guiContext, p_guiRequest);

                    l_promoScreenData.setPromoConfigData (l_promoConfigDataSet);

                    // Get details of all customer allocations and promotional
                    // credits.
                    getCustomerPromotions (p_request,
                                           p_guiRequest,
                                           l_promoScreenData);
                }
            }
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 94050, this,
                    "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Create new Screen Data object, if one has not already been
            // created, and add the response to it.
            if (l_promoAllocScreenData != null)
            {
                l_promoAllocScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);
            }
            else if (l_promoScreenData != null)
            {
                l_promoScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);
            }
        }

        if (l_accessGranted)
        {
            // If we have encountered an error on attempting to retrieve the
            // promotion details, then forward to the error page and store the
            // screen data on the HTTP request for use by the error page.

            if ((l_promoAllocScreenData != null) &&
                l_promoAllocScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_promoAllocScreenData);
            }
            else if ((l_promoScreenData != null) &&
                  l_promoScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_promoScreenData);
            }
            else if ((l_creditHistoryScreenData != null) &&
                 l_creditHistoryScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_creditHistoryScreenData);
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 94590, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCustomerPromotions =
                                                   "getCustomerPromotions";
    /** Get all data for the Customer Promotions screen display and store
     *  it in the screen data object supplied as parameter.
     *  @param p_request The incoming HTTP request.
     * @param p_guiRequest The GUI request being processed.
     *  @param p_screenData The screen data object that will be forwarded to
     *                      the customer promotions JSP to enable it to
     *                      generate the screen display.
     * @throws PpasServletException No specific keys are anticipated.
     */
    private void getCustomerPromotions (
        HttpServletRequest        p_request,
        GuiRequest                p_guiRequest,
        CcPromotionsScreenData    p_screenData)
        throws PpasServletException
    {
        CcCustPromoAllocDataSetResponse  l_custAllocationsResponse;
        CustPromoAllocDataSet            l_custAllocationsDataSet;
        CcPromotionCreditDataSetResponse l_custPromoCreditResponse;
        PromotionCreditDataSet           l_custPromoCreditDataSet;
        String                           l_updateDTStr;
        GuiSession                       l_guiSession = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 97010, this,
                "Entered " + C_METHOD_getCustomerPromotions);
        }

        // Command may have resulted from a successful update, in which
        // case the 'updateDT' provides the date time at which the
        // update was successful.

        l_updateDTStr = getValidParam (p_guiRequest,
                                       0,
                                       p_request,
                                       "p_updateDT",
                                       false,  // Not mandatory
                                       "", // Default to empty String
                                       PpasServlet.C_TYPE_DATE);

        p_screenData.setUpdateSuccessDTStr (l_updateDTStr);

        // Attempt to read the customer's promotion allocations
        l_custAllocationsResponse =
                     i_custPromoAllocService.getAllPromos (p_guiRequest,
                                                           i_timeout);
        // Store the response in the screen data object.
        p_screenData.addRetrievalResponse (p_guiRequest,
                                           l_custAllocationsResponse);

        // If response was successful, store data from response in the screen
        // data object.
        if (l_custAllocationsResponse.isSuccess())
        {
            l_custAllocationsDataSet = l_custAllocationsResponse.getDataSet();

            p_screenData.setCustAllocations (p_guiRequest,
                                             l_custAllocationsDataSet);
        }

        // Check that the first retrieval was successful before continuing.
        if (!p_screenData.hasRetrievalError())
        {
            // Attempt to read the history of promotional credits received by
            // the customer
            l_custPromoCreditResponse =
                i_promoCreditService.getPromotionCredits (p_guiRequest,
                                                          i_timeout);

            // Store the response in the screen data object.
            p_screenData.addRetrievalResponse (p_guiRequest,
                                               l_custPromoCreditResponse);

            // If response was successful, store data from response in the
            // screen data object.
            if (l_custPromoCreditResponse.isSuccess())
            {
                l_custPromoCreditDataSet =
                                l_custPromoCreditResponse.getDataSet();

                p_screenData.setPromoCreditHistory (p_guiRequest,
                                                    l_custPromoCreditDataSet);
            }
        }

        // Indicate whether the subscriber contained in the request is a
        // subordinate.
        l_guiSession = (GuiSession)p_guiRequest.getSession();

        if (!l_guiSession.getCustId().equals(l_guiSession.getBtCustId()))
        {
            p_screenData.setIsSubordinate();
        }

        // Attach the screen data to the HTTP request
        p_request.setAttribute ("p_guiScreenData", p_screenData);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 97090, this,
                "Leaving " + C_METHOD_getCustomerPromotions);
        }

        return;

    }  // end method 'getCustomerPromotions'


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insertPromoAllocation =
                                                     "insertPromoAllocation";
    /** Processing to perform insertion of a customer promotion allocation.
     *  @param p_request The incoming HTTP request.
     *  @param p_guiRequest The GUI request being processed.
     *  @param p_promoAllocScreenData The screen data object that will be used
     *                                by the JSP to construct the customer
     *                                promotion allocation pop-up window.
     */
    private void insertPromoAllocation(
        HttpServletRequest        p_request,
        GuiRequest                p_guiRequest,
        CcPromoAllocScreenData    p_promoAllocScreenData)
    {
        String                  l_planId = "";
        String                  l_startType = "";
        String                  l_allocStartStr;
        PpasDate                l_allocStartDate = null;
        String                  l_allocEndStr;
        PpasDate                l_allocEndDate = null;
        GuiResponse             l_guiResponse;
        CcAccountDataResponse   l_accountDataResp;
        AccountData             l_accountData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 96110, this,
                "Entered " + C_METHOD_insertPromoAllocation);
        }

        try
        {
            l_planId = getValidParam (p_guiRequest,
                                      0,
                                      p_request,
                                      "p_planId",
                                      true,   // planId is mandatory ...
                                      "",     // No default
                                      PpasServlet.C_TYPE_ANY);

            p_promoAllocScreenData.setPlanId (l_planId);

            // Get the radio button parameter that defines whether to
            // start allocation immediately (value="NOW") or on a future date (value="FUTURE_DATE").
            l_startType = getValidParam (p_guiRequest,
                                         0,
                                         p_request,
                                         "p_startType",
                                         true,           // Mandatory ...
                                         "",             // ... so no default
                                         new String [] {"FUTURE_DATE", "NOW"});

            // Has user requested that the allocation be started immediately?
            if (l_startType.equals("NOW"))
            {
                // Set an empty date in the screen data response (screen
                // data allocation start date must be blank when the
                // 'start allocation now' radio button option is checked).
                p_promoAllocScreenData.setAllocStartDate(new PpasDateTime (""));

                // Indicate that the 'start allocation now' radio button
                // is currently checked in the screen data object.
                p_promoAllocScreenData.setAllocStartRadioButton (
                         CcPromoAllocScreenData.C_START_ALLOC_NOW_CHECKED);
            }
            else
            {   // Future allocation start date has been specified
                // Get start datetime for the allocation
                l_allocStartStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_allocStartDateTime",
                                    true,   // allocStartDate is mandatory ...
                                    "",     // No default
                                    PpasServlet.C_TYPE_DATE);

                // Construct start date/time. This will have been sent with 00:00:00 as the time portion.
                l_allocStartDate = new PpasDateTime (l_allocStartStr);

                p_promoAllocScreenData.setAllocStartDate(l_allocStartDate);

                // Indicate in the screen data object that the 'on date' option
                // of the start date radio button is checked.
                p_promoAllocScreenData.setAllocStartRadioButton (
                         CcPromoAllocScreenData.C_START_ALLOC_ON_DATE_CHECKED);
            }

            // Get end datetime for the allocation
            l_allocEndStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_allocEndDateTime",
                                    true,    // Mandatory ...
                                    "",      // ... so no default
                                    PpasServlet.C_TYPE_DATE);

            // Construct end date/time. This will have been sent with 23:59:59 as the time portion.
            l_allocEndDate = new PpasDateTime (l_allocEndStr);
            p_promoAllocScreenData.setAllocEndDate(l_allocEndDate);
        } 
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 96115, this,
                    "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Add the response generated as an update response to the screen data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest, l_guiResponse);
        }

        // Has update type error already occurred?
        if (p_promoAllocScreenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do not attempt this requested service.
            l_guiResponse = new GuiResponse(
                               p_guiRequest,
                               p_guiRequest.getGuiSession().
                                 getSelectedLocale(),
                               GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                               new Object[] {"insert promotion allocation"},
                               GuiResponse.C_SEVERITY_WARNING);

            // Add the response generated as an update response to the
            // screen data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest, l_guiResponse);
        }
        else
        {
            // Use the Cc Customer Promotion Allocations service to
            // attempt to add the new allocation.
            l_guiResponse = i_custPromoAllocService.addAllocation(
                                    p_guiRequest,
                                    i_timeout,
                                    l_planId,
                                    l_allocStartDate,
                                    l_allocEndDate,
                                    l_startType.equals("NOW")); 

            // Store the response in thePromotion Allocation screen data object
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest, l_guiResponse);

            // If attempt to insert the allocation failed, then get the
            // service class of the current subscriber to be used to obtain
            // the division of the promotion - we only need to do this after
            // failure, since the pop-up allocation window is closed otherwise.
            if (!l_guiResponse.isSuccess())
            {
                l_accountDataResp = i_accountService.getFullData (p_guiRequest, i_timeout);

                p_promoAllocScreenData.addRetrievalResponse (p_guiRequest, l_accountDataResp);

                if (l_accountDataResp.isSuccess())
                {
                    l_accountData = l_accountDataResp.getAccountData();

                    // Store the service class in the screen data
                    p_promoAllocScreenData.setServiceClass (
                                             l_accountData.getActiveServiceClass());
                }
            }
        }

        // Indicate that this is a new allocation.
        p_promoAllocScreenData.setExistingAlloc (false);

        // Set up flags indicating whether the allocation is a future
        // or the current allocation for the customer
        p_promoAllocScreenData.setCurrentOrFuture (p_guiRequest, i_logger);

        // Store the screen data as an attribute on the request
        p_request.setAttribute ("p_guiScreenData", p_promoAllocScreenData);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 96118, this,
                "Leaving " + C_METHOD_insertPromoAllocation);
        }
    } // end method insertPromoAllocation

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updatePromoAllocation =
                                                 "updatePromoAllocation";
    /** Processing to perform update or insert of a customer promotion
     *  allocation.
     *  @param p_request The incoming HTTP request.
     * @param p_guiRequest The GUI request being processed.
     *  @param p_promoAllocScreenData The screen data object that will be used
     *                                by the JSP to construct the customer
     *                                promotion allocation pop-up window.
     */
    private void updatePromoAllocation(
        HttpServletRequest        p_request,
        GuiRequest                p_guiRequest,
        CcPromoAllocScreenData    p_promoAllocScreenData)
    {
        String                  l_planId = "";
        String                  l_startType;
        String                  l_allocStartStr = "";
        PpasDateTime            l_allocStartDT = null;
        String                  l_endType;
        boolean                 l_startAllocNow = true;
        boolean                 l_endAllocNow = true;
        String                  l_allocEndStr;
        PpasDate                l_allocEndD = null;
        PpasDateTime            l_allocEndDT = null;
        GuiResponse             l_guiResponse;
        String                  l_oldPlanId = "";
        String                  l_oldAllocStartStr;
        PpasDateTime            l_oldAllocStartDT = null;
        String                  l_oldAllocEndStr;
        PpasDateTime            l_oldAllocEndDT = null;
        CcAccountDataResponse   l_accountDataResp;
        AccountData             l_accountData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 95010, this,
                "Entered " + C_METHOD_updatePromoAllocation);
        }

        try
        {
            // Get the promotion identifier.
            l_planId = getValidParam (p_guiRequest,
                                      0,
                                      p_request,
                                      "p_planId",
                                      true,   // planId is mandatory ...
                                      "",     // No default
                                      PpasServlet.C_TYPE_ANY);

            p_promoAllocScreenData.setPlanId (l_planId);

            // Get the radio button parameter that defines whether to
            // start allocation immediately (value="NOW") or on a future
            // date (value="FUTURE_DATE"). This parameter is not mandatory
            // since it will only be present when a future allocation is
            // being updated.
            l_startType = getValidParam (p_guiRequest,
                                         0,
                                         p_request,
                                         "p_startType",
                                         false,          // Not mandatory
                                         "FUTURE_DATE",  // Default to this
                                         new String [] {"FUTURE_DATE",
                                                        "NOW"});

            // Has user requested that the allocation be started
            // immediately?
            if (l_startType.equals("NOW"))
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_LOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_TRACE,
                        C_CLASS_NAME, 95030, this,
                        "Setting start date for plan to be 'now'");
                }

                // Set an empty date in the screen data response (screen
                // data allocation start date must be blank when the
                // 'start allocation now' radio button option is checked).
                p_promoAllocScreenData.setAllocStartDate(new PpasDateTime (""));

                // Indicate that the 'start allocation now' radio button
                // is currently checked in the screen data object.
                p_promoAllocScreenData.setAllocStartRadioButton (
                         CcPromoAllocScreenData.C_START_ALLOC_NOW_CHECKED);
                
                l_startAllocNow = true;
            }
            else
            {   // Future allocation start date has been specified

                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_LOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_TRACE,
                        C_CLASS_NAME, 95035, this,
                        "Setting start date for plan to be 'future date'");
                }

                // Get start datetime for the allocation
                l_allocStartStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_allocStartDateTime",
                                    true,   // allocStartDate is mandatory ...
                                    "",     // No default
                                    PpasServlet.C_TYPE_DATE);

                // Construct start date/time. This will have been sent with 00:00:00 as the time portion.
                l_allocStartDT = new PpasDateTime (l_allocStartStr);

                p_promoAllocScreenData.setAllocStartDate (l_allocStartDT);

                // Indicate in the screen data object that the 'on date' option
                // of the start date radio button is checked.
                p_promoAllocScreenData.setAllocStartRadioButton (
                         CcPromoAllocScreenData.C_START_ALLOC_ON_DATE_CHECKED);
                         
                l_startAllocNow = false;
            }

            // Get the radio button parameter that defines whether to
            // end allocation immediately, (value="NOW"), at the end of
            // today (value="END_OF_DAY") or on a future date
            // (value="FUTURE_DATE"). This parameter is optional because it
            // will only be present when the currently active allocation is
            // being updated.
            l_endType = getValidParam (p_guiRequest,
                                       0,
                                       p_request,
                                       "p_endType",
                                       false,          // Not mandatory
                                       "FUTURE_DATE",  // Default to this
                                       new String [] {"FUTURE_DATE",
                                                      "END_OF_DAY",
                                                      "NOW"});
            // Has user requested that the allocation be ended immediately?
            if (l_endType.equals("NOW"))
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_LOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_TRACE,
                        C_CLASS_NAME, 95060, this,
                        "Setting end date for plan to be 'now'");
                }

                // Set an empty date in the screen data response (screen
                // data allocation end date must be blank when the
                // 'end allocation now' radio button option is checked).
                p_promoAllocScreenData.setAllocEndDate(new PpasDateTime (""));

                // Indicate that the 'end allocation now' radio button
                // is currently checked
                p_promoAllocScreenData.setAllocEndRadioButton (
                         CcPromoAllocScreenData.C_END_ALLOC_NOW_CHECKED);

                // Set value to be used for the 'end allocation now'
                // parameter in the service method to update the allocation
                // when the end date is today
                // ... true=end now; false=end at end of day
                l_endAllocNow = true;
            }

            // Has user requested that the allocation be ended at the
            // end of today?
            else if (l_endType.equals("END_OF_DAY"))
            {
                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_LOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_TRACE,
                        C_CLASS_NAME, 95070, this,
                        "Setting end date for plan to be 'end of day'");
                }

                // Set an empty date in the screen data response (screen
                // data allocation end date must be blank when the
                // 'end allocation at end of day' radio button option is
                //  checked).
                p_promoAllocScreenData.setAllocEndDate(new PpasDateTime (""));

                // Construct end date/time by adding 23:59:59 to the date.
                l_allocEndD = DatePatch.getDateToday();
                l_allocEndDT = new PpasDateTime (l_allocEndD.toString() + " 23:59:59");

                // Indicate that the 'end allocation at end of today'
                // radio button is currently checked
                p_promoAllocScreenData.setAllocEndRadioButton (
                       CcPromoAllocScreenData.C_END_ALLOC_END_OF_TODAY_CHECKED);

                // Set value to be used for the 'end allocation now'
                // parameter in the service method to update the allocation
                // when the end date is 'today'
                // ... true=end now; false=end at end of day
                l_endAllocNow = false;
            }
            else
            {   // User is attempting to end allocation on a future date.

                if (WebDebug.on)
                {
                    WebDebug.print(
                        WebDebug.C_LVL_LOW,
                        WebDebug.C_APP_SERVLET,
                        WebDebug.C_ST_TRACE,
                        C_CLASS_NAME, 95080, this,
                        "Setting end date for plan to be 'future date'");
                }

                // Get end datetime for the allocation
                l_allocEndStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_allocEndDateTime",
                                    true,    // Mandatory ...
                                    "",      // ... so no default
                                    PpasServlet.C_TYPE_DATE);

                // Construct end date/time. This will have been sent with 23:59:59 as the time portion.
                l_allocEndDT = new PpasDateTime (l_allocEndStr);

                p_promoAllocScreenData.setAllocEndDate (l_allocEndDT);

                // Indicate that the end allocation 'on date'
                // radio button is currently checked
                p_promoAllocScreenData.setAllocEndRadioButton (
                       CcPromoAllocScreenData.C_END_ALLOC_ON_DATE_CHECKED);

                // Set value to be used for the 'end allocation now'
                // parameter in the service method to update the allocation
                // when the end date is 'today'. This parameter will not
                // actually be used here, since the date end will be a future
                // date, but false is a suitable default value to set.
                l_endAllocNow = false;
            }

            // Get the Plan Id of the plan that is to be updated

            // Old plan id is not mandatory because it will not be present
            // when we're updating the current allocation (because the
            // plan id for the current allocation cannot be updated).
            l_oldPlanId = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_oldPlanId",
                                    false,     // oldPlanId is not mandatory ...
                                    l_planId,  // ... default to same as
                                               // plan id (unchanged).
                                    PpasServlet.C_TYPE_ANY);

            // Get old start date/time for the allocation that is being
            // updated. This is only present for update of future
            // allocations (hence, obtained as non-mandatory), since for the
            // current allocation, the start date cannot be changed.
            l_oldAllocStartStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_oldAllocStartDateTime",
                                    false,           // Not mandatory ...
                                    l_allocStartStr, // ... default to same as
                                                     // alloc start date
                                    PpasServlet.C_TYPE_DATE);

            l_oldAllocStartDT = new PpasDateTime (l_oldAllocStartStr);

            // Get end date/time for the allocation that is being
            // updated
            l_oldAllocEndStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_oldAllocEndDateTime",
                                    true,  // Parameter is mandatory
                                    "",    // No default value
                                    PpasServlet.C_TYPE_DATE);

            l_oldAllocEndDT = new PpasDateTime (l_oldAllocEndStr);

        } // end try

        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 95000, this,
                    "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Add the response generated as an update response to the screen
            // data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest, l_guiResponse);
        }

        // No need to check that values have changes (since this must always be
        // true due to Javascript validation)

        // Has update error already occurred?
        if (p_promoAllocScreenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do attempt
            // this requested service.
            l_guiResponse =
                        new GuiResponse(
                               p_guiRequest,
                               p_guiRequest.getGuiSession().
                                 getSelectedLocale(),
                               GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                               new Object[] {"update promotion allocation"},
                               GuiResponse.C_SEVERITY_WARNING);

            // Add the response generated as an update response to the
            // screen data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest,
                                                      l_guiResponse);
        }
        else
        {
            // Use the Cc Customer Promotion Allocations service to
            // attempt to update the allocation.
            l_guiResponse = i_custPromoAllocService.updateAllocation(
                                    p_guiRequest,
                                    i_timeout,
                                    l_planId,
                                    l_allocStartDT,
                                    l_allocEndDT,
                                    l_oldPlanId,
                                    l_oldAllocStartDT,
                                    l_oldAllocEndDT,
                                    l_startAllocNow,
                                    l_endAllocNow);

            // Store the response in the Promotion Allocation
            // screen data object
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest,
                                                      l_guiResponse);

            // If attempt to update the allocation failed, then get the
            // service class of the current subscriber to be used to obtain
            // the division of the promotion - we only need to do this after
            // failure, since the pop-up allocation window is closed otherwise.
            if (!l_guiResponse.isSuccess())
            {
                l_accountDataResp = i_accountService.getFullData (p_guiRequest,
                                                                  i_timeout);

                p_promoAllocScreenData.addRetrievalResponse (p_guiRequest,
                                                             l_accountDataResp);

                if (l_accountDataResp.isSuccess())
                {
                    l_accountData = l_accountDataResp.getAccountData();

                    // Store the service class in the screen data
                    p_promoAllocScreenData.setServiceClass (
                                             l_accountData.getActiveServiceClass());
                }
            }

        } // end else (no update error has occurred)

        // Indicate in the screen data object that this is an
        // existing allocation that is being updated.
        p_promoAllocScreenData.setExistingAlloc (true);

        // If update error has occurred, set plan id, start date and end
        // date to be  displayed on the pop-up promotion allocation window
        // to be the values in the database looked up based on the
        // l_allocStartDT value.
        if (p_promoAllocScreenData.hasUpdateError())
        {
            getCustPromoAllocationData (p_guiRequest,
                                        l_oldAllocStartDT,
                                        p_promoAllocScreenData);
        }

        // Set up flags indicating whether the allocation is a future
        // or the current allocation for the customer
        p_promoAllocScreenData.setCurrentOrFuture (p_guiRequest,
                                                   i_logger);

        // Store the screen data as an attribute on the request
        p_request.setAttribute ("p_guiScreenData",
                                p_promoAllocScreenData);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 95190, this,
                "Leaving " + C_METHOD_updatePromoAllocation);
        }

        return;

    } // end method updatePromoAllocation

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_deletePromoAllocation =
                                                 "deletePromoAllocation";
    /** Processing to perform deletion of a customer promotion allocation.
     *  @param p_request The incoming HTTP request.
     * @param p_guiRequest The GUI request being processed.
     *  @param p_promoAllocScreenData The screen data object that will be used
     *                                by the JSP to construct the customer
     *                                promotion allocation pop-up window.
     */
    private void deletePromoAllocation(
        HttpServletRequest        p_request,
        GuiRequest                p_guiRequest,
        CcPromoAllocScreenData    p_promoAllocScreenData)
    {
        String                   l_allocStartStr = "";
        PpasDateTime             l_allocStartDT = null;
        GuiResponse              l_guiResponse;
        CcAccountDataResponse    l_accountDataResp;
        AccountData              l_accountData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 98010, this,
                "Entered " + C_METHOD_deletePromoAllocation);
        }

        try
        {
            // Get start date/time for the allocation that is being removed
            l_allocStartStr = getValidParam(
                                    p_guiRequest,
                                    0,
                                    p_request,
                                    "p_allocStartDateTime",
                                    true,   // allocStartDate mandatory when
                                            // plan is being deleted and must
                                            // not be blank.
                                    "",     // No default
                                    PpasServlet.C_TYPE_DATE);

            // Construct start date/time object to be used later in the
            // service method call to delete the allocation
            l_allocStartDT = new PpasDateTime (l_allocStartStr);

        } // end try

        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 96000, this,
                    "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Add the response generated as an update response to the screen
            // data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest, l_guiResponse);
        }

        // Has update type error already occurred?
        if (p_promoAllocScreenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do attempt
            // this requested service.
            l_guiResponse =
                    new GuiResponse(
                               p_guiRequest,
                               p_guiRequest.getGuiSession().
                                 getSelectedLocale(),
                               GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                               new Object[] {"delete promotion allocation"},
                               GuiResponse.C_SEVERITY_WARNING);


            // Add the response generated as an update response to the
            // screen data object.
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest,
                                                      l_guiResponse);
        }
        else
        {
            // Use the Cc Customer Promotion Allocations service to
            // attempt to remove the allocation.
            l_guiResponse = i_custPromoAllocService.removeAllocation(
                                    p_guiRequest,
                                    i_timeout,
                                    l_allocStartDT);

            // Store the response in the Promotion Allocation
            // screen data object
            p_promoAllocScreenData.addUpdateResponse (p_guiRequest,
                                                      l_guiResponse);

            // If attempt to delete the allocation failed, then get the
            // service class of the current subscriber to be used to obtain
            // the division of the promotion - we only need to do this after
            // failure, since the pop-up allocation window is closed otherwise.
            if (!l_guiResponse.isSuccess())
            {
                l_accountDataResp = i_accountService.getFullData (p_guiRequest,
                                                                  i_timeout);

                p_promoAllocScreenData.addRetrievalResponse (p_guiRequest,
                                                             l_accountDataResp);

                if (l_accountDataResp.isSuccess())
                {
                    l_accountData = l_accountDataResp.getAccountData();

                    // Store the service class in the screen data
                    p_promoAllocScreenData.setServiceClass (
                                             l_accountData.getActiveServiceClass());
                }
            }

            // Store the screen data as an attribute on the request
            p_request.setAttribute ("p_guiScreenData",
                                    p_promoAllocScreenData);

        } // end else

        // Indicate in the screen data object that we've accessed an
        // existing allocation
        p_promoAllocScreenData.setExistingAlloc (true);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 98090, this,
                "Leaving " + C_METHOD_deletePromoAllocation);
        }

        return;

    } // end method deletePromoAlloc


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCustPromoAllocationData =
                                                "getCustPromoAllocationData";
    /** Get all data for the Customer Allocation pop-up window and store
     *  it in the screen data object supplied as parameter.
     *  @param p_guiRequest The GUI request.
     *  @param p_allocStartDT Start date/time of the allocation that we are to
     *                        to obtain data for.
     *  @param p_promoAllocScreenData The screen data object that will be used
     *                                by the JSP to construct the customer
     *                                promotion allocation pop-up window.
     */
    private void getCustPromoAllocationData (
        GuiRequest                p_guiRequest,
        PpasDateTime              p_allocStartDT,
        CcPromoAllocScreenData    p_promoAllocScreenData)
    {
        CcCustPromoAllocDataSetResponse  l_custAllocationsResponse;
        CustPromoAllocDataSet            l_custAllocationsDataSet;
        Vector                           l_custAllocV;
        boolean                          l_found = false;
        CustPromoAllocData               l_currAlloc = null;
        GuiResponse                      l_guiResponse;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 99010, this,
                "Entered " + C_METHOD_getCustPromoAllocationData);
        }

        // Attempt to read the customer's promotion allocations
        l_custAllocationsResponse =
                     i_custPromoAllocService.getAllPromos (p_guiRequest,
                                                           i_timeout);

        // Store the response in the screen data object.
        p_promoAllocScreenData.addRetrievalResponse (p_guiRequest,
                                                     l_custAllocationsResponse);

        // If response was successful, then loop over all allocations in the
        // data set from the response to find the one that we're looking for
        // based on the allocation start date/time passed in
        if (l_custAllocationsResponse.isSuccess())
        {
            l_custAllocationsDataSet = l_custAllocationsResponse.getDataSet();

            l_custAllocV = l_custAllocationsDataSet.getDataSetV();

            for (int l_index = 0;
                 (l_index < l_custAllocV.size()) && (!l_found);
                 l_index++)
            {
                l_currAlloc = (CustPromoAllocData)
                                          l_custAllocV.elementAt(l_index);

                if (l_currAlloc.getStartDateTime().equals (p_allocStartDT))
                {
                    l_found = true;
                }

            } // end for

            // If we've found the allocation, then set up the plan id,
            // allocation start date and allocation end date in the screen data
            // object

            if (l_found)
            {
                p_promoAllocScreenData.setPlanId (l_currAlloc.getPromotion());

                p_promoAllocScreenData.setAllocStartDate(l_currAlloc.getStartDateTime());

                p_promoAllocScreenData.setAllocEndDate(l_currAlloc.getEndDateTime());
            }
            else
            {
                // This is a special case (which should in theory never occur)
                // where we've successfully obtained the data set of allocations
                // but the allocation we're expecting to find was not in it.
                // Generate an appropriate response object and add it to the
                // screen data

                l_guiResponse =
                    new GuiResponse(
                               p_guiRequest,
                               p_guiRequest.getGuiSession().
                                 getSelectedLocale(),
                               GuiResponse.C_KEY_PROMO_ALLOC_NOT_FOUND,
                               new Object[] {},
                               GuiResponse.C_SEVERITY_FAILURE);

                // Add the response generated a an retrieval response to the
                // screen data object.
                p_promoAllocScreenData.addRetrievalResponse (p_guiRequest,
                                                             l_guiResponse);
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 99090, this,
                "Leaving " + C_METHOD_getCustPromoAllocationData);
        }

        return;

    }  // end method 'getCustPromoAllocationData'


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPromoCreditHistoryDetail =
                                                "getPromoCreditHistoryDetail";
    /** Get all data for the Customer Allocation pop-up window and store
     *  it in the screen data object supplied as parameter.
     *  @param p_request The incoming HTTP request.
     *  @param p_guiRequest Incoming GUI request.
     *  @param p_creditHistoryScreenData The screen data object that will be used
     *                                   by the JSP to construct the promotion
     *                                   credit detail pop-up window.
     *  @throws PpasServletException No specific keys are anticipated.
     */
    private void getPromoCreditHistoryDetail(
        HttpServletRequest                    p_request,
        GuiRequest                            p_guiRequest,
        CcPromoCreditHistoryDetailScreenData  p_creditHistoryScreenData)
        throws PpasServletException
    {
        String          l_promoCreditDTStr = null;
        PpasDateTime    l_promoCreditDT    = null;
        String          l_value            = null;
        String          l_currency         = null;
        String          l_linkId           = null;
        String          l_sourceType       = null;

        char            l_creditType;

        CcRechargeDivisionDataResponse  l_divisionResp = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 26710, this,
                "Entered " + C_METHOD_getPromoCreditHistoryDetail);
        }

        // Get start date for the allocation.
        l_promoCreditDTStr = getValidParam (p_guiRequest,
                                            0,
                                            p_request,
                                            "p_dateTime",
                                            true,   // mandatory ...
                                            "",     // ... so no default
                                            PpasServlet.C_TYPE_DATE);

        l_promoCreditDT = new PpasDateTime (l_promoCreditDTStr);

        // Get the currency value awarded for the promotional credit
        // (does this have a dicimal point??? - should this be numeric
        // validation???)
        l_value = getValidParam (p_guiRequest,
                                 0,
                                 p_request,
                                 "p_value",
                                 true,   // mandatory ...
                                 "",     // ... so no default
                                 PpasServlet.C_TYPE_ANY);

        // Get the currency of the promotional credit awarded
        l_currency = getValidParam (p_guiRequest,
                                    0,
                                    p_request,
                                    "p_currency",
                                    true,   // mandatory ...
                                    "",     // ... so no default
                                    PpasServlet.C_TYPE_ANY);

        l_linkId = getValidParam (p_guiRequest,
                                  0,
                                  p_request,
                                  "p_linkId",
                                  true,   // mandatory ...
                                  "",     // ... so no default
                                  PpasServlet.C_TYPE_NUMERIC);

        l_sourceType = getValidParam (p_guiRequest,
                                      0,
                                      p_request,
                                      "p_sourceType",
                                      true,   // mandatory ...
                                      "",     // ... so no default
                                      PpasServlet.C_TYPE_ANY);

        // Voucher and payments are flagged as refill promotions
        if ( (l_sourceType.charAt(0) == DivisionData.C_CREDIT_TYPE_VOUCHER) ||
             (l_sourceType.charAt(0) == DivisionData.C_CREDIT_TYPE_PAYMENT) )
        {
            l_creditType = DivisionData.C_CREDIT_TYPE_REFILL_PROMOTION;
        }
        else
        {
            l_creditType = l_sourceType.charAt(0);
        }

        l_divisionResp =
                 i_divisionService.getDivision (p_guiRequest,
                                                i_timeout,
                                                new Long(l_linkId).longValue(),
                                                l_creditType);

        p_creditHistoryScreenData.addRetrievalResponse (p_guiRequest, l_divisionResp);

        if (l_divisionResp.isSuccess())
        {
            p_creditHistoryScreenData.setDivisionData (
                                l_divisionResp.getDivisionData());
            p_creditHistoryScreenData.setPromoCreditAppliedValue(l_value);
            p_creditHistoryScreenData.setPromoCreditAppliedCurrency(l_currency);
            p_creditHistoryScreenData.setPromoCreditDateTime(l_promoCreditDT);
        }

        // Attach the screen data to the HTTP request
        p_request.setAttribute ("p_guiScreenData",
                                p_creditHistoryScreenData);

        if (WebDebug.on)
        {
            WebDebug.print(
                 WebDebug.C_LVL_MODERATE,
                 WebDebug.C_APP_SERVLET,
                 WebDebug.C_ST_END,
                 C_CLASS_NAME, 26790, this,
                 "Leaving " + C_METHOD_getPromoCreditHistoryDetail);
        }

        return;

    }  // end private method 'getPromoCreditHistoryDetail'

} // end class CcPromotionsServlet
