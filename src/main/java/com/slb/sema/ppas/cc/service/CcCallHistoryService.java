////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcCallHistoryService.Java
//DATE            :       08-Mar-2006
//AUTHOR          :       Kanta Goswami
//REFERENCE       :       PpacLon#2026/8071
//                        PRD_ASCS00_GEN_CA_066_D1
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Gui Service that wraps calls to
//                        PpasCallHistoryService.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//19/03/07 | John Lee   | Fixed problem with exception    | PpacLon#1909/10997
//         |            | handling that was discovered    |
//         |            | as part of upgrade to Tomact 5. | 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CallHistoryDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasCallHistoryService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Response returned by a GUI service method that contains a 
 * Call History object.
 */
public class CcCallHistoryService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCallHistoryService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasCallHistoryService i_callHistoryService;


    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcCallHistoryService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcCallHistoryService(GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_callHistoryService = new PpasCallHistoryService(p_guiRequest,
                                                  p_logger,
                                                  p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCallHistorys = "getCallHistoryDetails";
    /**
     * Retrieves full account data for the subscriber.
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_startDate
     * @param p_endDate
     * @param p_maxRecsRetrieve
     * @param p_otherPartyNo
     * @param p_sortField
     * @param p_descendSort
     * @param p_subLevel
     * @return Set of callHistory data.
     */
    public CcCallHistoryResponse getCallHistoryDetails(GuiRequest p_guiRequest,
                                                       long       p_timeoutMillis,
                                                       PpasDate   p_startDate,
                                                       PpasDate   p_endDate,
                                                       Integer    p_maxRecsRetrieve,
                                                       String     p_otherPartyNo,
                                                       String     p_sortField,
                                                       boolean    p_descendSort,
                                                       boolean    p_subLevel
                                                       )
    {
        CcCallHistoryResponse l_ccCallHistoryResponse;
        GuiResponse           l_guiResponse;
        CallHistoryDataSet    l_callHistoryDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getCallHistorys);
        }

        try
        {
            l_callHistoryDataSet = i_callHistoryService.getCallHistoryDetails(p_guiRequest,
                                                                       p_timeoutMillis,
                                                                       p_startDate,
                                                                       p_endDate,
                                                                       p_maxRecsRetrieve,
                                                                       p_otherPartyNo,
                                                                       p_sortField,
                                                                       p_descendSort,
                                                                       p_subLevel);
//          Generate success CcVoucherDataResponse
            l_ccCallHistoryResponse = new CcCallHistoryResponse(
                                    p_guiRequest,
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_callHistoryDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get call history details", l_pSE);

            l_ccCallHistoryResponse = new CcCallHistoryResponse(
                                    p_guiRequest,
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null);
        }
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getCallHistorys);
        }

        return l_ccCallHistoryResponse;
        
        }  // end public CcCallHistoryResponse getCallHistory(...)
} // end public class CcCallHistoryService
