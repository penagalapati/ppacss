////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcRefillHistoryDetailScreenData.java
//      DATE            :       10-May-2004
//      AUTHOR          :       MAGray - q92528d
//      REFERENCE       :       PpacLon#238
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Get Refill History Screenscreen data
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;
import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Refill History Detail screen.
 */
public class CcRefillHistoryDetailScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** DivisionData object wrapped by this object. */
    private DivisionData i_divisionData  = null;

    /** The date/time of the refill. */
    private PpasDateTime         i_dateTime              = null;

    /** Original refill amount. */
    private String               i_originalAmount        = null;

    /** Currency of original refill amount. */
    private String               i_currency              = null;

    /** Applied refill amount in subscriber's preferred currency. */
    private String               i_refillAmount         = null;

    /** Currency of the applied amount (equivalent to subscriber's preferred currency). */
    private String               i_appliedCurrency       = null;  // PpacLon#43/384

    /** The voucher group. */
    private String               i_group                 = null;

    /** The source of the refill (eg operator id). */
    private String               i_source                = null;

    /** boolean indicating whether or not there is a division. */
    private boolean              i_hasDivision;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     */
    public CcRefillHistoryDetailScreenData( GuiContext p_guiContext,
                                            GuiRequest p_guiRequest)
    {
        super( p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the DivisionData object wrapped within this object.
     * Returns null if there is no DivisionData object set.
     * @return Refill division split.
     */
    public DivisionData getDivisionData()
    {
        return i_divisionData;
    }

    /** Sets <code>i_DivisionData</code> to 
     * <code>p_DivisionData</code>.
     * @param p_divisionData Refill division split.
     */
    public void setDivisionData(DivisionData p_divisionData)
    {
        i_divisionData = p_divisionData;
    }

    /** Gets the date/time of the refill.
     * @return Date/time of refill.
     */
    public PpasDateTime getRefillDateTime()
    {
        return i_dateTime;
    }

    /** Sets the date/time of the refill.
     * @param p_dateTime Date/time of refill.
     */
    public void setRefillDateTime( PpasDateTime p_dateTime )
    {
        i_dateTime = p_dateTime;
    }

    /** Gets Original refill amount.
     * @return Original amount of the refill.
     */
    public  String getOriginalAmount()
    {
        return( i_originalAmount);
    }

    /** Sets Original refill amount.
     * @param p_originalAmount Original amount of the refill.
     */
    public  void setOriginalAmount( String p_originalAmount )
    {
        i_originalAmount = p_originalAmount;
    }

    /** Gets Currency of original refill amount.
     * @return Original Currency of the refill. 
     */
    public String getOriginalCurrency()
    {
       return( i_currency );
    }

    /** Sets Currency of original refill amount.
     * @param p_currency Currency of the refill.
     */
    public void setOriginalCurrency( String p_currency)
    {
        i_currency = p_currency;
    }

    /** Gets Actual refill amount in subscriber's preferred currency.
     * @return Actual amount of this refill.
     */
    public String getAppliedAmount()
    {
       return i_refillAmount;
    }

    /** Sets Actual refill amount in subscriber's preferred currency.
     * @param p_refillAmount Actual amount of this refill.
     */
    public void setAppliedAmount( String p_refillAmount)
    {
        i_refillAmount = p_refillAmount;
    }

    // PpacLon#43/384 [begin]
    /** Returns the currency of the applied amount.
     * @return Currency the refill was applied in.
     */
    public String getAppliedCurrency()
    {
       return i_appliedCurrency;
    }

    /** Sets the currency of the applied amount.
     * @param p_appliedCurrency Currency in which the refill was applied.
     */
    public void setAppliedCurrency(String p_appliedCurrency)
    {
        i_appliedCurrency = p_appliedCurrency;
    }
    // PpacLon#43/384 [end]

    /** Gets the voucher group.
     * @return Voucher group used in this refill.
     */
    public String getGroup()
    {
       return( i_group );
    }

    /** Sets the voucher group.
     * @param p_group Voucher group used in this refill.
     */
    public void setGroup( String p_group)
    {
        i_group = p_group;
    }

    /** Gets the source of the refill (eg operator id).
     * @return Source of refill.
     */
    public String getSource()
    {
       return( i_source );
    }

    /** Sets the source of the refill (eg operator id).
     * @param p_source Source of this refill.
     */
    public void setSource( String p_source)
    {
        i_source = p_source;
    }

    /** Gets boolean indicating whether or not there is a division.
     * @return True if this refill has a split.
     */
    public boolean getHasDivision()
    {
       return( i_hasDivision );
    }

    /** Sets boolean indicating whether or not there is a division.
     * @param p_hasDivision Flag indicating whether the refill is split.
     */
    public void setHasDivision( boolean p_hasDivision)
    {
        i_hasDivision = p_hasDivision;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return i_divisionData != null;
    }
}
