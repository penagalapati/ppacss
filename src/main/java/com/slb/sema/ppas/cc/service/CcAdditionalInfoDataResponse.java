////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdditionalInfoDataResponse.java
//      DATE            :       7-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1236/5006
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Response returned by a GUI service method that
//                              is a container for AdditionalInfoData.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.common.web.support.WebDebug;

/**
 * Response returned by a GUI service method that contains an 
 * AdditionalInfoData object.
 */
public class CcAdditionalInfoDataResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdditionalInfoDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AdditionalInfoData instance contained in this response. */
    private AdditionalInfoData i_addInfoData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * Uses the Locale passed in to construct text of message.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_addInfoData   Additional customer information.
     */
    public CcAdditionalInfoDataResponse(
                                 GuiRequest         p_guiRequest,
                                 Locale             p_messageLocale,
                                 String             p_messageKey,
                                 Object[]           p_messageParams,
                                 int                p_messageSeverity,
                                 AdditionalInfoData p_addInfoData )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_addInfoData = p_addInfoData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the AdditionalInfoData instance contained within
     * this response object.
     * @return Additional customer information.
     */
    public AdditionalInfoData getAdditionalInfoData()
    {
        return ( i_addInfoData );

    } // end public AdditionalInfoData getAdditionalInfoData()
}
