////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCommunityChargingUT.java
//      DATE            :       05-Feb-2007
//      AUTHOR          :       John Lee
//      REFERENCE       :       N/A
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for the GUI Communities screen. 
//                 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// <date>   | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.HashMap;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework (v 1.5.4)
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcCommunityChargingUT extends GuiTestTT
{   
    /** Table id of the Communities screen - History table. */
    private static final String C_TABLE_ID_COMMUNITIES_HISTORY = "communitiesHistory";
    
    /** Expected title returned when Communities screen is requested. */
    private static final String C_COMMUNITIES_SCREEN_EXPECTED_TITLE = "CommunityCharging";
    
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcCommunityChargingUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        say("Entered CcCommunityChargingUT.setUp()");
        super.setUp(true);
    }
    
    /**
     * @ut.when An attempt is made to load the Communities screen for a subscriber
     * @ut.then A successful response is returned containing the correct page and details.
     */
     public void testLoadCommunitiesScreenSuccess()
     {        
         beginOfTest("Start testLoadCommunitiesScreenSuccess");
         
         //Install MSISDN and load Maintain Subsriber Screen
         installAndLoadSubScreen(1);
         
         //Request Communities Screen
         sendRequest(constructGetRequest("cc/CommunityCharging", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_COMMUNITIES_SCREEN_EXPECTED_TITLE);
         
         //Check the subscriber does not have any communities yet, in
         //which case the on-screen table only has rows that are blank 
         say("Verifying table: " + C_TABLE_ID_COMMUNITIES_HISTORY);
         verifyRowInTable(C_TABLE_ID_COMMUNITIES_HISTORY, 1, new String[] {"", ""});
         
         endOfTest();
     }
     
     /**
      * @ut.when An attempt is made to update the community list for a subscriber
      * @ut.then A successful response is returned containing the correct page and details.
      */
      public void testUpdateCommunitiesSuccess()
      {     
          int    l_community_id = 0;
          String l_community_desc = null;
          
          beginOfTest("Start testUpdateCommunitiesSuccess");
       
          //Get a community that can be added to a subscriber
          //For example, the one with greatest community id
          try
          {
              String l_sql = "select coch_community_id, coch_community_descr " +
                             "from coch_community_charging " +
                             "where coch_community_id = " +
                             "(select max(coch_community_id) from coch_community_charging)";
          
              JdbcResultSet l_resultSet = sqlQuery(new SqlString(500, 0, l_sql));
          
              while (l_resultSet.next(11110))
              {
                  l_community_id   = l_resultSet.getInt(11120, "coch_community_id");
                  l_community_desc = l_resultSet.getString(11130, "coch_community_descr");
              }
              assertNotNull("Unable to obtain a community from coch_community_charging", l_community_desc);
          }
          catch (PpasSqlException e)
          {
              failedTestException(e);
          }
          
          //Install MSISDN and load Maintain Subsriber Screen
          installAndLoadSubScreen(1);
       
          //Request Communities Screen
          sendRequest(constructGetRequest("cc/CommunityCharging", "GET_SCREEN_DATA"));        
          validateResponse(null, null, C_COMMUNITIES_SCREEN_EXPECTED_TITLE);
         
          //Loop on the number of possible communities
          //and set only the 1st one in the new list
          HashMap l_params = new HashMap();
          for (int l_count = 1; l_count <= PpasAccountService.C_NB_COMMUNITY_ID_IN_LIST; l_count++)
          {
              l_params.put("p_oldCommId_" + l_count, "");
              l_params.put("p_newCommId_" + l_count, (l_count == 1 ? String.valueOf(l_community_id) : ""));  
          }
          
          //Send request to update the communities for the subscriber
          sendRequest(constructGetRequest("cc/CommunityCharging", "UPDATE", l_params));        
          validateResponse(null, null, C_COMMUNITIES_SCREEN_EXPECTED_TITLE);

          //Check the correct community has been added to the on-screen table
          say("Verifying table: " + C_TABLE_ID_COMMUNITIES_HISTORY);
        
          verifyRowInTable(C_TABLE_ID_COMMUNITIES_HISTORY, 1, 
                           new String[] {String.valueOf(l_community_id), l_community_desc});
          
          endOfTest();
      }
    
    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        say(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcCommunityChargingUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
}