////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcRechargeResultDataResponse.java
//      DATE            :       2-Jul-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/6040
//                              PRD_PPAK00_DEV_IN_34
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for RechargeResultData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.RechargeResultData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for RechargeResultData objects. 
 */
public class CcRechargeResultDataResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcRechargeResultDataResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The RechargeResultData object wrapped by this object. */
    private RechargeResultData  i_rechargeResultData;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives a RechargeResultData object and the additional necessary objects to 
     * create a response message indicating the status of a call to a
     * RechargeService method.
     * @param p_guiRequest        The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_messageSeverity   Severity of response.
     * @param p_rechargeResultData The result data from the recharge.
     */
    public CcRechargeResultDataResponse(GuiRequest        p_guiRequest,
                                        Locale            p_messageLocale,
                                        String            p_messageKey,
                                        Object[]          p_messageParams,
                                        int               p_messageSeverity,
                                        RechargeResultData p_rechargeResultData)
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing CcRechargeResultDataResponse");
        }

        i_rechargeResultData = p_rechargeResultData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed CcRechargeResultDataResponse");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Get the make recharge results.
     * @return Result iof the recharge. 
     */
    public RechargeResultData getRechargeResultData()
    {
        return i_rechargeResultData;
    }

}

