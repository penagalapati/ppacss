////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCommentService.java
//      DATE            :       31-Jan-2002
//      AUTHOR          :       Matt Kirk
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasCommentService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CommentDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasCommentService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Response returned by a GUI service method that contains an 
 * Comment object.
 */
public class CcCommentService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCommentService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasCommentService i_commentService;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcCommentService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcCommentService(GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal comment service to be used by this service.
        i_commentService = new PpasCommentService(p_guiRequest,
                                                  p_logger,
                                                  p_guiContext);

        // Add service specific keys ...

        // none.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getComments = "getComments";
    /**
     * Retrieves full account data for the subscriber.
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @return Set of comment data.
     */
    public CcCommentDataSetResponse getComments(GuiRequest p_guiRequest,
                                                long       p_timeoutMillis)
    {
        CcCommentDataSetResponse l_ccCommentDataSetResponse  = null;
        GuiResponse              l_guiResponse               = null;
        CommentDataSet           l_commentDataSet            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getComments);
        }

        try
        {
            l_commentDataSet = i_commentService.getComments(p_guiRequest,
                                                            p_timeoutMillis);

            l_ccCommentDataSetResponse
                = new CcCommentDataSetResponse
                                  (p_guiRequest,
                                   GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                   new Object[] {"comment"},
                                   GuiResponse.C_SEVERITY_SUCCESS,
                                   l_commentDataSet);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get comments", l_pSE);

            l_ccCommentDataSetResponse =
                new CcCommentDataSetResponse(p_guiRequest,
                                             l_guiResponse.getKey(),
                                             l_guiResponse.getParams(),
                                             GuiResponse.C_SEVERITY_FAILURE,
                                             null);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getComments);
        }

        return l_ccCommentDataSetResponse;
    }  // end public CcCommentDataSetResponse getComments(...)
} // end public class CcCommentService
