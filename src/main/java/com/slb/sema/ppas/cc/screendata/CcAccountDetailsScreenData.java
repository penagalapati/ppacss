////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccountDetailsScreenData.java
//      DATE            :       30-Jan-2002
//      AUTHOR          :       Matt Kirk / Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_38
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the Account Details
//                              screen.
//
////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                         | REFERENCE
//----------+------------+-------------------------------------+--------------------
// 11/09/02 | M Hickman  | Add a flag to indicate whether      | PpaLon#1534/6499
//          |            | we need to refresh the balance.     |
//          |            | This will be set if a service       |
//          |            | class change has been carried       |
//          |            | out since the display currency      |
//          |            | might have changed.                 |
//----------+------------+-------------------------------------+--------------------
// 20/05/05 | S J Vonka  | New method                          |PRD_ASCS00_GEN_CA_038
//          |            | subscriberPinUpdateIsPermitted      | CR1537#6456
//----------+------------+-------------------------------------+---------------------
// 17/10/05 |K Goswami   | New method                          | PpacLon#463
//          |            | tempBlockUpdateIsPermitted.         |
//----------+------------+-------------------------------------+---------------------
// 14/03/06 | Ian James  | Return the default value (255) if   | PpaLon#1577/8150
//          |            | the ID is null or undefined.        | 
/////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionData;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageDataSet;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.CustomerDetailsData;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.EventHistoryDataSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.PinCode;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Account Details screen.
 */
public class CcAccountDetailsScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------

    /** Flag to indicate that the customer's current agent should be returned
     *  as the first element when agent business configuration data is to be 
     *  returned.
     */
    public static final long C_FLAG_AGENT_FIRST = 0x1;

    /** Flag to indicate that the customer's current sub-agent should be 
     *  returned as the first element when agent business configuration data 
     *  is to be  returned.
     */
    public static final long C_FLAG_SUB_AGENT_FIRST = 0x2;

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccountData object wrapped by this object. */
    private AccountData i_accountData;

    /** CustomerDetailsData object wrapped by this object. */
    private CustomerDetailsData i_customerDetailsData;

    /** EventHistoryDataSet object wrapped by this object. */
    private EventHistoryDataSet i_eventHistoryDataSet;

    /** Languages data from business configuration cache. */
    private ValaValidLanguageDataSet     i_languagesDS;
    
    /** Regions data from business configuration cache. */
    private RegiRegionDataSet i_regionsDataSet;

    /** Agents data from business configuration cache. */
    private SrvaAgentMstrDataSet         i_agentsDS;

    /** Service classes from business configuration cache. */
    private MiscCodeDataSet              i_servClassesDS;

    /** Indicates whether the account contained in this object is 
     *  disconnected (true) or not (false). 
     */
    private boolean i_isDisconnected = false;

    /** Indicates whether we need to refresh the balance. */
    private boolean i_refreshBalance = false;

    /** Display name to be used for the subscriber's market on the context bar.
     */
    private String i_marketDisplayName = "";

    /**
     * Holds the name of the field whose request to be update failed.
     * The design means that this can only ever be one field, as if an
     * update fails, no further updates are attempted.
     */
    private String i_failedUpdateField = "";

    /**
     * Indicates whether the expiry comparison date usage flag is set to
     * "Expiry After Date" or not.
     */
    private boolean i_expiryAfterDateFlag = false;

    /** Has Subscriber Pin Update Feature Licence. */
    private boolean i_hasSubscriberPinUpdateFeatureLicence = false;
    
    /** Has Regional Vouchers Feature Licence. */
    private boolean i_hasRegionalVouchersFeatureLicence = true;
    
    /**Pin Code length value - Configured in ConfCompanyConfig  */
    private int i_confPinCodeLength;
        
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcAccountDetailsScreenData(GuiContext p_guiContext,
                                      GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Sets the name of the field that was being updated, but the update
     * failed.
     * 
     * @param p_field Name of the field.
     */
    public void setFailedUpdateField(String p_field)
    {
        i_failedUpdateField = p_field;
    }

    /**
     * Returns the name of the field whose attempted update failed.
     * Empty String is returned if no field name has been set.
     * 
     * @return Name of the field.
     */
    public String getFailedUpdateField()
    {
        return i_failedUpdateField;
    }

    /** Returns <code>true</code> if the Operator is allowed to update
     * the Service and Airtime Expiry Dates.  
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can update dates, otherwise false.
     */
    public boolean expDatesUpdateIsPermitted()
    {
        return(i_gopaData.expDatesUpdateIsPermitted());
    }

    /** Returns <code>true</code> if the Operator is allowed to update
     * the USSD EoCN selection service ID.
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can update the USSD EoCN ID, otherwise false.
     */
    public boolean ussdEocnUpdateIsPermitted()
    {
        return(i_gopaData.ussdEocnIdUpdateIsPermitted());
    }

    /** Returns <code>true</code> if the Operator is allowed to
     * change the Service Class.
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can change service class, otherwise false.
     */
    public boolean servClassChangeIsPermitted()
    {
        return(i_gopaData.servClassChangeIsPermitted());
    }

    /** Returns <code>true</code> if the Operator is allowed to
     * change the Subscriber's preferred language.
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can change language, otherwise false.
     */
    public boolean languageChangeIsPermitted()
    {
        return(i_gopaData.languageChangeIsPermitted());
    }

    /** Returns <code>true</code> if the Operator is allowed to
     * update the Agent and Sub-Agent details
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can change agent, otherwise false.
     */
    public boolean agentUpdateIsPermitted()
    {
        return(i_gopaData.agentUpdateIsPermitted());
    }

    /** Returns <code>true</code> if the Operator is allowed to
     * Unbar the Subscriber from recharge via the IVR
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can unbar IVR access, otherwise false.
     */
    public boolean ivrUnbarIsPermitted()
    {
        return(i_gopaData.ivrUnbarIsPermitted());
    }
    
    /** Returns <code>true</code> if the Operator is allowed to
     * Update the Subscriber's pin code
     * Otherwise returns <code>false</code>.
     * 
     * @return True if the operator can update the subscriber pin code, otherwise false.
     */
    public boolean subscriberPinUpdateIsPermitted()
    {
        return(i_gopaData.subscriberPinUpdateIsPermitted());
    }
    
    /**
     * Returns <code>true</code> if the Operator is allowed to
     * change the Home Region of the subscriber.
     * Otherwise returns <code>false</code>.
     * 
     * @return True is the operator can change the home region, otherwise false.
     */
    public boolean homeRegionChangeIsPermitted()
    {
        return(i_gopaData.homeRegionUpdateIsPermitted());
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setRegionalVouchersFeatureLicence(boolean p_isLicenced)
    {
        i_hasRegionalVouchersFeatureLicence = p_isLicenced;
    }
    
    /**
     * 
     */
    public boolean hasRegionalVouchersFeatureLicence()
    {
        return i_hasRegionalVouchersFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setSubscriberPinUpdateFeatureLicence(boolean p_isLicenced)
    {
        i_hasSubscriberPinUpdateFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if the feature licence is valid.
     */
    public boolean hasSubscriberPinUpdateFeatureLicence()
    {
        return i_hasSubscriberPinUpdateFeatureLicence;
    }
    
    /**
     * Returns the AccountData object wrapped within this object.
     * Returns null if there is no AccountData object set.
     * 
     * @return Details of this account.
     */
    public AccountData getAccountData()
    {
        return i_accountData;
    }

    /**
     * Returns the CustomerDetailsData object wrapped within this object.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Details of this customer.
     */
    public CustomerDetailsData getCustomerDetailsData()
    {
        return i_customerDetailsData;
    }
    
    /**
     * Can the CSO change a subscribers temporary block status.
     * 
     * @return boolean true if update is allowed else false.
     */
    public boolean tempBlockUpdateIsPermitted()
    {
        return i_gopaData.tempBlockUpdateIsPermitted();
    }

    /**
     * Returns the EventHistoryDataSet object wrapped within this object.
     * Returns null if there is no EventHistoryDataSet object.
     * 
     * @return Set of events associated with this account.
     */
    public EventHistoryDataSet getEventHistoryDataSet()
    {
        return i_eventHistoryDataSet;
    }

    /** Sets <code>p_accountData</code> to <code>i_accountData</code>.
     * 
     * @param p_accountData Details of the account.
     */
    public void setAccountData (AccountData p_accountData)
    {
        i_accountData = p_accountData;

        if (i_accountData != null)
        {
            i_isDisconnected = i_accountData.isDisconnected ();
        }
    }

    /** Sets <code>p_customerDetailsData</code> to 
     * <code>i_customerDetailsData</code>.
     * 
     * @param p_customerDetailsData Details of the customer.
     */
    public void setCustomerDetailsData(
                    CustomerDetailsData p_customerDetailsData)
    {
        i_customerDetailsData = p_customerDetailsData;
    }

    /** Sets the account's Event History data for the screen display.
     * 
     * @param p_eventHistoryDataSet Set of details related to account activity.
     */
    public void setEventHistoryDataSet(
        EventHistoryDataSet p_eventHistoryDataSet)
    {
        i_eventHistoryDataSet = p_eventHistoryDataSet;
    }

    /** Get count of the number of (unresolved) memos in the event history
     *  data.
     *  @return int
     */
    public int getMemoCount()
    {
        int l_memoCount = 0;

        if (i_eventHistoryDataSet != null)
        {
            l_memoCount = i_eventHistoryDataSet.getOpenMemoCount();
        }
        return (l_memoCount);
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * 
     * @return True if all details are populated, otherwise false.
     */
    public boolean isPopulated()
    {
        return ( (i_accountData != null) &&
                 (i_customerDetailsData != null) &&
                 (i_eventHistoryDataSet != null) );
    }

    /** Returns an array of available language identifiers which are  
     * configured in the database. The customer's current preferred
     * language will be the first element in the array. If the customer's 
     * account data has not been set up, then the order of the array will be
     * random.
     * @return Array of available languages (with the customer's current 
     *         preferred language as the first element).
     */
    public String [] getLanguages()
    {
        Vector                    l_langV = new Vector (20, 10);
        ValaValidLanguageData []  l_langDataArr;
        String                []  l_langCodesArr;

        if (i_accountData != null)
        {
            if (i_accountData.getPreferredLanguage() != null)
            {
                // Set first element of Vector as customer's preferred language
                l_langV.addElement (i_accountData.getPreferredLanguage());
            }
            else
            {
                // the service class is null, so just display an empty String. this will
                // only happen if the subscriber is on the process of being disconnected,
                // so that no data is retrieved from the database or the network
                l_langV.addElement ("");
            }
        }

        if (i_languagesDS != null)
        {
            l_langDataArr = i_languagesDS.getAll();

            // Copy all languages that were generated from business config data
            // into the Vector excluding the customer's current preferred
            // language that has already been set as the first element of the
            // Vector.

            for (int l_index = 0; l_index < l_langDataArr.length; l_index++)
            {
                if (i_accountData != null && i_accountData.getPreferredLanguage() != null)
                {
                    if (!(i_accountData.getPreferredLanguage().
                        equalsIgnoreCase(l_langDataArr[l_index].getCode())))
                    {
                        l_langV.addElement(l_langDataArr[l_index].getCode());
                    }
                }
                else
                {
                    l_langV.addElement (l_langDataArr[l_index].getCode());
                }
            }

        }

        l_langCodesArr = new String [l_langV.size()];
        l_langCodesArr = (String []) l_langV.toArray (l_langCodesArr);

        return (l_langCodesArr);

    } // end method getLanguages

    /**
     * Returns an array of available region identifiers which are  
     * configured in the database. The customer's current home region
     * will be the first element in the array. If the customer's 
     * account data has not been set up, then the order of the array will be
     * random.
     * @return Array of available regions (with the customer's current 
     *         home region as the first element).
     */
    public String[] getRegions()
    {
        Vector                l_regionsV = new Vector (20, 10);
        RegiRegionData[]      l_regionDataArr;
        String[]              l_regionCodesArr;

        if (i_accountData != null)
        {
            if (i_accountData.getHomeRegionId() != null)
            {
                // Set first element of Vector as customer's preferred home region
                l_regionsV.addElement (String.valueOf(i_accountData.getHomeRegionId()));
            }
            else
            {
                // the home region id is null, so just display an empty String.
                l_regionsV.addElement ("");
            }
        }

        if (i_regionsDataSet != null)
        {
            l_regionDataArr = i_regionsDataSet.getAllArray();

            // Copy all languages that were generated from business config data
            // into the Vector excluding the customer's current preferred
            // language that has already been set as the first element of the
            // Vector.

            for (int l_index = 0; l_index < l_regionDataArr.length; l_index++)
            {
                if (i_accountData != null && i_accountData.getHomeRegionId() != null)
                {
                    if (!(i_accountData.getHomeRegionId().
                        equals(l_regionDataArr[l_index].getHomeRegionId())))
                    {
                        l_regionsV.addElement(
                                       String.valueOf(
                                           l_regionDataArr[l_index].getHomeRegionId().getValue()));
                    }
                }
                else
                {
                    l_regionsV.addElement(
                                   String.valueOf(
                                       l_regionDataArr[l_index].getHomeRegionId().getValue()));
                }
            }

        }

        
        
        l_regionCodesArr = new String [l_regionsV.size()];
        l_regionCodesArr = (String []) l_regionsV.toArray (l_regionCodesArr);

        return (l_regionCodesArr);
    }

    /**
     * Sets up the regions configuration data.
     * @param p_regionsDataSet Set of valid regions.
     */
    public void setRegions(RegiRegionDataSet p_regionsDataSet)
    {
        i_regionsDataSet = p_regionsDataSet;
    }

    /** Set up data set of agents returned from business config service.
     *  @param p_agentsDataSet Data set of agents. This must have been 
     *                         generated by calling the business configuration
     *                         cache service with this customer's market.
     */
    public void setAgentsDataSet (SrvaAgentMstrDataSet p_agentsDataSet)
    {
        i_agentsDS = p_agentsDataSet;
    }


    /** Returns an array of available agent identifiers which are 
     * configured in the database. Either the customer's current agent or
     * the customer's current sub-agent will be put as the first element in
     * the list, depending upon the value of the <code>p_flags</code>
     * parameter.
     * @param p_flags Determines whether the customers current agent or
     *                sub-agent should be placed first in the returned array.
     *                <br>C_FLAG_AGENT_FIRST = put customer's current agent
     *                    as the first element of the returned array.
     *                <br>C_FLAG_SUB_AGENT_FIRST = put customer's current
     *                    sub-agent as the first element of the returned array.
     * @return Array of available agents for the customers market (this 
     *         assumes that the <code>i_agentsDS</code> has been 
     *         generated from the customer's market. If not, then null is 
     *         returned.
     */
    public String [] getAgents (long p_flags)
    {
        Vector                    l_agentsV = new Vector (20, 10);
        SrvaAgentMstrData []      l_agentsDataArr;
        String            []      l_agentCodesArr;

        if (i_accountData != null)
        {
            if ((p_flags & C_FLAG_AGENT_FIRST) == C_FLAG_AGENT_FIRST)
            {
                // Set first element of Vector as customer's current agent
                l_agentsV.addElement (i_accountData.getAgent());
            }
            else if ((p_flags & C_FLAG_SUB_AGENT_FIRST) ==
                                                      C_FLAG_SUB_AGENT_FIRST)
            {
                // Set first element of Vector as customer's current sub-agent
                String l_subAgent = i_accountData.getSubAgent();
                if (l_subAgent == null)
                {
                    l_subAgent = "";
                }
                l_agentsV.addElement (l_subAgent);
            }
        }

        if (i_agentsDS != null)
        {
            l_agentsDataArr = i_agentsDS.getAllArray();

            // Copy all agents that were generated from business config data
            // into the Vector excluding the customer's current agent
            // or sub-agent that has already been set as the first element of 
            // the Vector.

            for (int l_index = 0; l_index < l_agentsDataArr.length; l_index++)
            {
                if (l_agentsV.size() == 0          ||
                    l_agentsV.elementAt(0) == null ||
                    !(((String)(l_agentsV.elementAt(0))).
                       equalsIgnoreCase(l_agentsDataArr[l_index].getAgent())))
                {
                    l_agentsV.addElement(l_agentsDataArr[l_index].getAgent());
                }
            }
        }
  
        l_agentCodesArr = new String [l_agentsV.size()];
        l_agentCodesArr = (String []) l_agentsV.toArray (l_agentCodesArr);

        return (l_agentCodesArr);

    } // end method getAgents

    /** Set up data set of service classes returned from business configuration
     *  service.
     *  @param p_serviceClassDataSet Data set of service classes. This must
     *                               have been generated by calling the
     *                               business configuration cache service with
     *                               this customer's market.
     */
    public void setServiceClasses(
        MiscCodeDataSet p_serviceClassDataSet)
    {
        i_servClassesDS = p_serviceClassDataSet;
    }


    /** Returns an array of available service class identifiers which are 
     * configured in the database. The customer's current service class will
     * be returned as the first element of the array.
     * @return Array of available service classes for the customers market 
     *         (this assumes that the <code>i_servClassesDS</code>
     *         attribute has been generated from the customer's market. If not,
     *         then <code>null</code> is returned).
     */
    public String [] getServiceClasses ()
    {
        Vector        l_serviceClassesV = new Vector (20, 10);
        Vector        l_miscCodesDataV;
        String []     l_serviceClassesArr;
        MiscCodeData  l_currentMiscCodeData;

        if (i_accountData != null)
        {
            if (i_accountData.getOriginalServiceClass() != null)
            {
                // Set first element of Vector as customer's current service class
                l_serviceClassesV.addElement ("" + i_accountData.getOriginalServiceClass().getValue());
            }
            else
            {
                // the service class is null, so just display an empty String. this will
                // only happen if the subscriber is on the process of being disconnected,
                // so that no data is retrieved from the database or the network
                l_serviceClassesV.addElement ("");
            }
        }

        if (i_servClassesDS != null)
        {
            l_miscCodesDataV = i_servClassesDS.getDataV();

            // Copy all service classes that were generated from business
            // config data into the Vector excluding the customer's current
            // current service class that has already been set as the first
            // element of the Vector.

            for (int l_index = 0; l_index < l_miscCodesDataV.size(); l_index++)
            {
                l_currentMiscCodeData = 
                       (MiscCodeData)(l_miscCodesDataV.elementAt(l_index));

                if (i_accountData != null         &&
                    l_serviceClassesV.size() != 0 &&
                    l_serviceClassesV.elementAt(0) != null)
                {
                    if (!((String)l_serviceClassesV.elementAt(0)).equals(l_currentMiscCodeData.getCode()))
                    {
                        l_serviceClassesV.addElement(
                                      l_currentMiscCodeData.getCode());
                    }
                }
                else
                {
                    l_serviceClassesV.addElement(
                                      l_currentMiscCodeData.getCode());
                }
            }

        }

        l_serviceClassesArr = new String [l_serviceClassesV.size()];
        l_serviceClassesArr = (String []) l_serviceClassesV.toArray(l_serviceClassesArr);

        return (l_serviceClassesArr);

    } // end method getServiceClasses


    /** Returns the customer's name as it is to be displayed by the Customer
     *  Care GUI.
     *  @return The customer's name.
     */
    public String getCustName()
    {
        String l_custName = "";
        if (i_accountData != null)
        {
            if (i_accountData.getFirstName() != null)
            { 
                l_custName += i_accountData.getFirstName() + " ";
            }
            if (i_accountData.getSurname() != null)
            { 
                l_custName += i_accountData.getSurname();
            }
        }
        return (l_custName);
    }

    /** Returns the Msisdn from the account data.
     * 
     * @return String representing the mobile number.
     */
    public String getMsisdnAsString()
    {
        String l_formattedMsisdn = super.getMsisdnAsString();
        
        if (l_formattedMsisdn.equals(""))
        {
            l_formattedMsisdn = "Unavailable";
        }

        return l_formattedMsisdn;
    }

    /** Get the Master MSISDN as a formatted string. If the Master MSISDN does not exist at this point
     * then a blank string is returned.
     * <p>
     * This is a convenience method to simplify access from the GUI.
     * 
     * @return The Master MSISDN associated with the operation as a formatted string.
     */
    public String getMasterMsisdnAsString()
    {
        Msisdn l_msisdn = i_accountData.getMasterMsisdn();
        
        return l_msisdn == null ? "" : formatMsisdn(l_msisdn);
    }

    /** Returns the cust id from the account data.
     * 
     * @return Customer identifier.
     */
    public String getCustId()
    {
        String l_custId = "Unavailable";

        if (i_accountData != null)
        {
            l_custId = i_accountData.getCustId();
        }
        return (l_custId);
    }

    /** Returns the balance status string from the account data.
     * 
     * @return String representation of the balance status.
     */
    public String getBalanceStatusStr()
    {
        String l_balanceStatus = "Unavailable";

        if (i_accountData != null)
        {
            l_balanceStatus = i_accountData.getBalanceStatusStr();
        }
        return (l_balanceStatus);
    }

    /** Determines if the account is barred on the IVR or not.
     *  @return True if the account is barred on the IVR. Otherwise false.
     *          If the account data is not loaded, then false is returned.
     */
    public boolean isIvrBarred()
    {
        boolean l_isIvrBarred = false;

        if (i_accountData != null)
        {
            if (i_accountData.getIvrBarredStatusCode().equals(AccountData.C_IVR_STATUS_BARRED))
            {
                l_isIvrBarred = true;
            }
        }
        return (l_isIvrBarred);
    }
    
    /**
     * Gets the subscriber's temp blocking status.
     * @return true if temp blocking is set, else false
     */
    public boolean isTempBlocked()
    {
        boolean l_blocked = false;
        
        if (i_accountData != null)
        {
            l_blocked = i_accountData.isTempBlocked();
        }
        
        return l_blocked;
    }

    /** Determines if the account is a subordinate account or not.
     *  @return True if the account is a subordinate account. Otherwise false.
     *          If the account data is not loaded, then false is returned.
     */
    public boolean isSubordinate()
    {
        boolean l_isSubordinate = false;

        if (i_accountData != null)
        {
            l_isSubordinate = i_accountData.isSubordinate();
        }
        return (l_isSubordinate);
    }


    /** Returns a boolean indicating whether or not the account contained in
     *  this screen data object is disconnected.
     *  @return True if the account contained in this screen data object is
     *          a disconnected one. False if there is no account contained in
     *          this screen data object or if the account contained in this 
     *          screen data object is not disconnected.
     */
    public boolean isDisconnected()
    {
        return (i_isDisconnected);
    }

    /** 
     * Returns a boolean value indicating whether a balance refresh is 
     * required (true) or not (false).
     * 
     * @return True if the balance should be refreshed.
     */
    public boolean refreshBalance()
    {
        return i_refreshBalance;
    }

    /** Sets the value returned by the balanceRefresh() method to true. */
    public void setRefreshBalance()
    {
        i_refreshBalance = true;
    }

    /** Sets up the languages configuration data.
     * 
     * @param p_languagesDS Set of valid languages.
     */
    public void setLanguageDataSet (ValaValidLanguageDataSet p_languagesDS)
    {
        i_languagesDS = p_languagesDS;
    }

    /** Sets the name of the market for the subscriber.
     *  @param p_marketDisplayName Display name of the subscriber's market.
     */
    public void setMarketDisplayName (String p_marketDisplayName)
    {
        i_marketDisplayName = p_marketDisplayName;
    }

    /** Returns the name of the subscriber's market.
     *  @return Display name of the subscriber's market.
     */
    public String getMarketDisplayName()
    {
        return (i_marketDisplayName);
    }
    
    /** Get the preferred currency.
     * 
     * @return String representing the code of the preferred currency.
     */
    public String getPreferredCurrencyCodeStr()
    {
        String l_curr = "";
        
        if (i_accountData != null)
        {
            if (i_accountData.getPreferredCurrency() != null)
            {
                l_curr = i_accountData.getPreferredCurrency().getCurrencyCode();
            }
        }
        
        return l_curr;
    }
    
    /** Get the promotion plan.
     * 
     * @return Promotion plan identifier.
     */
    public String getPromotionPlan()
    {
        String l_plan = "";
        
        if (i_accountData != null)
        {
            if (i_accountData.getPromotionPlan() != null)
            {
                l_plan = i_accountData.getPromotionPlan();
            }
        }
        
        return l_plan;
    }
    
    /**
     * Get the temporary service class.
     * @return the temp service class, or an empty string if it's null
     */
    public String getTempServiceClass()
    {
        String       l_tempServiceClassStr = "";
        ServiceClass l_tempServiceClass = null;

        if (i_accountData != null)
        {
            l_tempServiceClass = i_accountData.getActiveServiceClass();

            if ( l_tempServiceClass != null &&
                 !l_tempServiceClass.equals(i_accountData.getOriginalServiceClass()) )
            {
                l_tempServiceClassStr = Integer.toString(l_tempServiceClass.getValue());
            }
        }
        
        return l_tempServiceClassStr;
    }
    
    /**
     * Get the temp service class expiry date.
     * @return the temporary service class expiry date, or a not-set PpasDate if it's null
     */
    public PpasDate getTempServiceClassExpiry()
    {
        PpasDate l_exp = null;
        
        if (i_accountData != null)
        {
            l_exp = i_accountData.getTempServiceClassExpDate();
        }
        
        if (l_exp == null)
        {
            l_exp = new PpasDate("");
        }
        
        return l_exp;
    }

    /**
     * Sets the expiry after date flag to indicate that the system is configured to
     * process expiry after the specified expiry date.  This indicates that we must
     * allow dedicated account expiry dates to be set to the day before the current date.
     */
    public void setExpiryAfterDate()
    {
        i_expiryAfterDateFlag = true;
    }

    /**
     * Returns true if expiry after date flag is set.
     * @return True if the expiry after date is set.
     */
    public boolean getExpiryAfterDate()
    {
        return i_expiryAfterDateFlag;
    }

    /**
     * Gets the USSD End of call notification ID as a string. Retrieves the ID
     * from the account data which is then formatted according to the screen
     * requirements, returning default value (255) if the ID is null or undefined.
     * @return Eocn ID as a string.
     */
    public String getUssdEocnIdAsString()
    {
        EndOfCallNotificationId l_eocnId = null;
        String                  l_eocnIdStr = null;

        l_eocnId = i_accountData.getUssdEocnId();

        if (l_eocnId == null) 
        {
            l_eocnIdStr = Short.toString(EndOfCallNotificationId.C_UNDEFINED_VALUE);
        }
        else
        {
            l_eocnIdStr = l_eocnId.toString();
        }

        return (l_eocnIdStr);
    }
    
    /**
     * Gets the Pin Code as a string. Retrieves the Code
     * from the account data which is then formatted according to the screen
     * requirements, returning blank if the ID is null.
     * @return Pin Code as a string.
     */
    public String getPinCodeAsString()
    {
        PinCode    l_pinCode    = null;
        String     l_pinCodeStr = null;

        l_pinCode = i_accountData.getPinCode();

        if ( l_pinCode == null )
        {
            l_pinCodeStr = "";
        }
        else
        {
            l_pinCodeStr = l_pinCode.getPinCodeAsString();
        }
               
        return (l_pinCodeStr);
    }
    
    /** Sets <code>p_confPinCodeLength</code> to 
     * <code>i_confPinCodeLength</code>.
     * 
     * @param p_confPinCodeLength The max configured pin code length .
     */
    public void setConfPinCodeLength(int p_confPinCodeLength)
    {
        i_confPinCodeLength = p_confPinCodeLength;
    }
    
    /**
     * Returns Configured Pin Code Length
     * @return i_confPinCodeLength Configured pin Code length.
     */
    public int getConfPinCodeLength()
    {                
        return i_confPinCodeLength;
    }
    
}
