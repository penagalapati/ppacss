////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcFamilyAndFriendsScreenData.java
//      DATE            :       01-Oct-2003
//      AUTHOR          :       Stephen Pinton
//      REFERENCE       :       PpaLon#2002/8478
//                              PRD_PPAK00_GEN_CA_428
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the main F&F screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 13/12/05 | M.Alm      | Add Account level FaF           | PpacLon#1897/7624
//          |            | functionality                   | PRD_ASCS00_GEN_CA_59
//----------+------------+---------------------------------+--------------------
// 02/06/06 | S James    | Change getChargingIndDesc method| PpacLon#2157/9005
//          |            | to prevent null pointer ex being| 
//          |            | thrown if the passed charging   |
//          |            | indicator is invalid, and also  |
//          |            | return description of Charging  |
//          |            | Indicators marked as deleted    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;


/**
 * Gui Screen Data object for the Family & Friends screen.
 */
public class CcFamilyAndFriendsScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** FamilyAndFriendsDataSet object wrapped by this object. */
    private FamilyAndFriendsDataSet i_familyAndFriendsDataSet;

    /** Text giving info on when/whether an update was made on this screen. */
    private String i_infoText;

    /** Data set containing all charging indicator data. */
    private FachFafChargingIndDataSet i_chargingIndDataSet = null;

    /** Data set containing available deci number ranges. */
    private DeciDefaultChargingIndDataSet i_availableDeciDataSet = null;
    
    /** Boolean indicating whether Account level FaF numbers are allowed. */
    private boolean i_accountLevelEnabled = false;
      

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcFamilyAndFriendsScreenData";

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** 
     * Simple constructor.
     * @param p_guiContext GuiContext containing configuration
     * @param p_guiRequest GuiRequest
     */
    public CcFamilyAndFriendsScreenData(GuiContext p_guiContext,
                                        GuiRequest p_guiRequest )
    {
        super(p_guiContext, p_guiRequest);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 95000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        i_infoText = "";

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 95010, this,
                           "Constructed " + C_CLASS_NAME );
        }
    }

    //--------------------------------------------------------------------------
    // Public methods.
    //--------------------------------------------------------------------------

    /** 
     * Sets the textual information on the status of the FaF operation attempted.
     * @param p_text test to be displayed
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** 
     * Gets the textual information on the status of the FaF operation attempted.
     * @return test to be displayed
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /**
     * Sets the Family & Friends data set.
     * @param p_familyAndFriendsDataSet set containing FaF numbers
     */
    public void setFamilyAndFriendsDataSet(FamilyAndFriendsDataSet p_familyAndFriendsDataSet)
    {
        i_familyAndFriendsDataSet = p_familyAndFriendsDataSet;
    }

    /**
     * Gets the Family & Friends data set.
     * @return data set containing FaF numbers
     */
    public FamilyAndFriendsDataSet getFamilyAndFriendsDataSet()
    {
        return i_familyAndFriendsDataSet;
    }

    /**
     * Gets all charging indicator data records.
     * @return a data set containing active charging indicator data records
     */
    public FachFafChargingIndDataSet getChargingIndDataSet()
    {
        return i_chargingIndDataSet;
    }
    
    /**
     * Get the description of the supplied charging indicator.
     * @param p_chargingInd the charging indicator
     * @return description of the charging indicator, or an empty String if the supplied value was invalid
     */
    public String getChargingIndDesc(String p_chargingInd)
    {
    	FachFafChargingIndData l_chargingIndData = i_chargingIndDataSet.getRecord(p_chargingInd);
    	String l_desc = null;
    	
    	if (l_chargingIndData != null)
    	{
            l_desc = l_chargingIndData.getDesc();
        }
    	
    	if (l_desc == null)
    	{
    		l_desc = "";
    	}
        
        return l_desc;
    }

    /**
     * Set the charging indicator data set.
     * @param p_set a charging indicator data set
     */
    public void setChargingIndDataSet(FachFafChargingIndDataSet p_set)
    {
        i_chargingIndDataSet = p_set;
    }

    /**
     * Set the available deci data set.
     * @param p_dataSet available deci data set
     */
    public void setAvailableDeciData(DeciDefaultChargingIndDataSet p_dataSet)
    {
        i_availableDeciDataSet = p_dataSet;
    }
    
    /**
     * Get the available deci data.
     * @return data set containing available deci data
     */
    public DeciDefaultChargingIndDataSet getAvailableDeciData()
    {
        return i_availableDeciDataSet;
    }
    
    /**
     * Is the current operator allowed to update the FaF list.
     * @return boolean indicator
     */
    public boolean updateFafListPermitted()
    {
        return i_gopaData.updateFafListPermitted();
    }

    /**
     * Gets the current Family & Friends data set.
     * @return data set containing FaF numbers
     */
    public FamilyAndFriendsDataSet getCurrentFamilyAndFriendsDataSet()
    {
        FamilyAndFriendsDataSet l_currentFafDataSet = null;
        if (i_familyAndFriendsDataSet != null)
        {
            l_currentFafDataSet = i_familyAndFriendsDataSet.getCurrent();
        }
        else
        {
            l_currentFafDataSet = new FamilyAndFriendsDataSet();
        }
        return l_currentFafDataSet;
    }

    /**
     * Gets the historic Family & Friends data set.
     * @return data set containing FaF numbers
     */
    public FamilyAndFriendsDataSet getHistoricFamilyAndFriendsDataSet()
    {
        FamilyAndFriendsDataSet l_historicFafDataSet = null;
        if (i_familyAndFriendsDataSet != null)
        {
            l_historicFafDataSet = i_familyAndFriendsDataSet.getHistory();
        }
        else
        {
            l_historicFafDataSet = new FamilyAndFriendsDataSet();
        }
        return l_historicFafDataSet;
    }

    /**
     * Returns true if Account level FaF numbers are allowed. Otherwise returns false.
     * 
     * @return True if Account level FaF numbers are allowed, otherwise, false.
     */
    public boolean isAccountLevelEnabled()
    {
       return i_accountLevelEnabled;
    }

    /**
     * Sets flag to indicate whether Account level FaF numbers are allowed.
     * @param p_accountLevelEnabled True if Account level FaF numbers are allowed, otherwise, false.
     */
    public void setAccountLevelEnabled(boolean p_accountLevelEnabled)
    {
        i_accountLevelEnabled = p_accountLevelEnabled;
    }

} // end of CcFamilyAndFriendsScreenData class


