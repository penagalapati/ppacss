////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcRechargeHistoryData.java
//      DATE            :       13-Feb-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/5475
//                              PRD_PPAK00_DEV_IN_034
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Data for a single recharge or failed recharge.
//                              If used for a failed recharge then some of the
//                              data values must be set to blank strings.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;

/** Data for a single recharge or failed recharge. */
public class CcRechargeHistoryData implements Comparable
{
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** 
     * Serial or activation number.  The voucher serial number is stored 
     * except for failed recharges where the serial number was not 
     * recognised, when the activation number is stored.
     */
    private String i_voucherNumber = null;

    /** Voucher (card) group. */
    private String i_group = null;

    /** Voucher value. */
    private Money  i_value = null;

    /** Amount applied in subscriber's preferred currency. */
    private Money  i_appliedAmount = null;
    
    /** 
     * Unique identifier linking recharge record with record detailing 
     * division across dedicated accounts.
     */
    private String i_linkId = null;

     /** Event description (expected to be "failed recharge" or "successful recharge"). */
    private String i_description = null;

    /** Datetime that voucher was assigned. */
    private PpasDateTime i_assignedDateTime = null;

    /** Operator id who performed the recharge. */
    private String i_opid = null;

    /** The service class of the subscriber when the recharge was made. */
    private String i_serviceClass = null;
    
    /** MSISDN of subscriber that initiated the recharge. */
    private Msisdn i_initiatingMsisdn = null;

    /** Boolean indicating whether or not the recharge was divided across dedicated accounts. */
    private boolean i_hasDivision = false;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor to set the following instance attributes.
     * @param p_voucherNumber    Voucher serial or activation number.
     * @param p_group            Voucher (card) group.
     * @param p_value            Voucher value.
     * @param p_appliedAmount    Amount in subscriber's preferred currency.
     * @param p_linkId           Link between recharge and division details.
     * @param p_assignedDateTime DateTime the voucher was assigned
     * @param p_description      Event description.
     * @param p_opid             Operator id who performed the recharge.
     * @param p_serviceClass     Service class of subscriber receiving recharge.
     * @param p_initiatingMsisdn Mobile number of customer that made the recharge.
     * @param p_hasDivision      Indicates whether or not recharge was divided.
     */
    public CcRechargeHistoryData(String       p_voucherNumber, 
                                 String       p_group,
                                 Money        p_value,
                                 Money        p_appliedAmount,
                                 String       p_linkId,
                                 PpasDateTime p_assignedDateTime,
                                 String       p_description,
                                 String       p_opid,
                                 String       p_serviceClass,
                                 Msisdn       p_initiatingMsisdn,
                                 boolean      p_hasDivision)
    {
        i_voucherNumber    = p_voucherNumber;
        i_group            = p_group;
        i_value            = p_value;
        i_appliedAmount    = p_appliedAmount;
        i_linkId           = p_linkId;
        i_assignedDateTime = p_assignedDateTime;
        i_description      = p_description;
        i_opid             = p_opid;
        i_serviceClass     = p_serviceClass;
        i_initiatingMsisdn = p_initiatingMsisdn;
        i_hasDivision      = p_hasDivision;
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /** 
     * Retrieves voucher serial or activation number.
     * @return Voucher serial or activation number. 
     */
    public String getVoucherNumber()
    {
        return(i_voucherNumber);
    }

    /** 
     * Retrieves voucher (card) group.
     * @return Voucher group.
     */
    public String getGroup()
    {
        return(i_group);
    }

    /** 
     * Retrieves voucher value.
     * @return Voucher value.
     */
    public Money getValue()
    {
        return(i_value);
    }

    /** 
     * Retrieves the amount applied in the subscriber's preferred currency.
     * @return Amount applied in the subscriber's preferred currency.
     */
    public Money getAppliedAmount()
    {
        return (i_appliedAmount);
    }

    /** 
     * Retrieves the unique identifier linking the recharge record with the record  
     * detailing the division across dedicated accounts.
     * @return Link between recharge and dedicated account division.
     */
    public String getLinkId()
    {
        return (i_linkId);
    }

    /** 
     * Retrieves event description. 
     * @return Event description.
     */
    public String getDescription()
    {
        return(i_description);
    }

    /** 
     * Retrieves datetime that voucher was assigned.
     * @return Datetime that voucher was assigned.
     */
    public PpasDateTime getAssignedDateTime()
    {
        return(i_assignedDateTime);
    }

    /** 
     * Retrieves id of operator that performed the recharge.
     * @return Id of operator that performed the recharge.
     */
    public String getOpid()
    {
        return(i_opid);
    }

    /** 
     * Retrieves service class of subscriber at time of recharge. 
     * @return Service class of subscriber at time of recharge.
     */
    public String getServiceClass()
    {
        return(i_serviceClass);
    }

    /** 
     * Retrieves MSISDN of subscriber that initiated the recharge. 
     * @return MSISDN of subscriber that initiated the recharge.
     */
    public Msisdn getInitiatingMsisdn()
    {
        return(i_initiatingMsisdn);
    }

    /** 
     * Retrieves boolean indicating whether the recharge resulted in a
     * dedicated account division.
     * @return True if the recharge resulted in a dedicated account division.
     */
    public boolean getHasDivision()
    {
        return(i_hasDivision);
    }

    /** 
     * Function to compare datetime of this CcRechargeHistoryData object to
     * that of another CcRechargeHistoryData. Used for sorting.
     * @param  p_data  Object to compare to.
     * @return -1 if the datetime of the object passed in is earlier, 
     *         1 if it is later, 0 if they are the same object.  
     */
    public int compareTo(Object p_data)
    {
        int l_place;
        CcRechargeHistoryData l_rechargeData;

        l_rechargeData = (CcRechargeHistoryData)p_data;
        if (l_rechargeData.getAssignedDateTime().before(i_assignedDateTime))
        {
            l_place = -1;
        }
        else if (l_rechargeData.getAssignedDateTime().after(i_assignedDateTime))
        {
            l_place = 1;
        }
        else
        {
            // Use the hashcode to ensure uniqueness
            if (this.hashCode() > l_rechargeData.hashCode())
            {
                l_place = -1;
            }
            else if (this.hashCode() < l_rechargeData.hashCode())
            {
                l_place = 1;
            }
            else
            {
                // It really is the same object!!
                l_place = 0;
            } 
        }

        return(l_place);
    }
}
