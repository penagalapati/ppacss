////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerDetailsScreenData.java
//      DATE            :       26-Mar-2002
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1337/5342
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Customer
//                              Details screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.HashMap;
import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.dataclass.CustomerAddressData;
import com.slb.sema.ppas.common.dataclass.CustomerContactData;
import com.slb.sema.ppas.common.dataclass.CustomerDetailsData;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Customer Details screen.
 */
public class CcCustomerDetailsScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Constant specifying the maximum number of alternative contact types
     *  to be handled via the GUI front-end.
     */
    public static final int C_MAX_NUMBER_CONTACT_TYPES = 5;


    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** CustomerDetailsData object wrapped by this object. */
    private CustomerDetailsData i_customerDetailsData;

    /** Customer contact types. */
    private MiscCodeDataSet     i_contactTypes;

    /** Customer contact data. */
    private HashMap             i_contactDataHM;    

    /** Customer address types. */
    private MiscCodeDataSet     i_addressTypes;

    /** Customer address data. */
    private HashMap             i_addressDataHM;

    /** GUI request object wrapped by this object. */
    private GuiRequest          i_guiRequest;

    /** Last address to be updates. */
    private String              i_lastUpdatedAddress;

    /** Prefered address type. */
    private String              i_preferredAddressType;

    /** Information detailing the success of the last update. */
    private String              i_infoText;
  
    /** Indicates whether the Subscriber in the request for this screen
     * is a subordinate or not.
     */
    private boolean i_isSubordinate;

    /** Indicates whether or not the customer's name has been changed. If true,
     *  then the customer's name has been changed. If false, then the customer's
     *  name has not been changed.
     */
    private boolean i_custNameChanged;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcCustomerDetailsScreenData(GuiContext p_guiContext,
                                       GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);

        i_infoText    = "";
        i_lastUpdatedAddress = "";

        // Default to false as this is overidden by the Servlet.
        i_isSubordinate = false;

        // By default, customer's name has not been changed.
        i_custNameChanged = false;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** indicate that the customer's name has been changed. */
    public void setCustNameChanged()
    {
        i_custNameChanged = true;
    }

    /** Determines whether or not the customer's name has been changed.
     *  @return <code>true</code> if the customer's name has been changed by
     *          the current request; otherwise <code>false</code>.
     */
    public boolean custNameChanged()
    {
        return (i_custNameChanged);
    }

    /** Get customer name to be displayed in the context bar.
     *  @return Customer name to be displayed in the context bar.
     */
    public String getCustName()
    {
        String l_displayName = "";

        if (i_customerDetailsData != null)
        {
            if (i_customerDetailsData.getFirstName() != null)
            {
                l_displayName = i_customerDetailsData.getFirstName() + " ";
            }
            if (i_customerDetailsData.getLastName() != null)
            {
                l_displayName += i_customerDetailsData.getLastName();
            }
        }
        return (l_displayName);
    }

    /** 
     * Returns <code>true</code> if the Operator is allowed to update
     * the Subscriber details.  Otherwise <code>false</code> is returned.
     * 
     * @return True if the operator can update operatot details, otherwise, false,
     */
    public boolean subsDetailsUpdateIsPermitted()
    {
        return (i_gopaData.subsDetailsUpdateIsPermitted());
    }

    /**
     * Returns the CustomerDetailsData object wrapped within this object.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Details of the customer.
     */
    public CustomerDetailsData getCustomerDetailsData()
    {
        return i_customerDetailsData;
    }

    /** Sets the CustomerDetailsData object wrapped within this object.
     * 
     * @param p_customerDetailsData Details of the customer.
     */
    public void setCustomerDetailsData(CustomerDetailsData p_customerDetailsData)
    {
        i_customerDetailsData = p_customerDetailsData;
    }

    /** 
     * Indicates if the subscriber contained in the request for this
     * screen is a subordinate.  Returns <code>true</code> if subscriber
     * is a subordinate, otherwise returns <code>false</code>.
     * 
     * @return True if the subscriber is a subordinate, otherwise, false.
     */
    public boolean isSubordinate()
    {
        return i_isSubordinate;
    }

    /**
     * Marks the subscriber contained in the request for this screen as being
     * a subordinate.
     */
    public void setIsSubordinate()
    {
        i_isSubordinate = true;
        return;
    }

    /**
     * Returns the ContactTypes wrapped within this object.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Set of contact types.
     */
    public MiscCodeDataSet getContactTypes()
    {
        return i_contactTypes;
    }


    /** Sets the ContactTypes object wrapped within this object.
     * 
     * @param p_contactTypes Set of contact types.
     */
    public void setContactTypes( MiscCodeDataSet p_contactTypes )
    {
        i_contactTypes = p_contactTypes;
    }


    /**
     * Returns a hashmap of ContactData keyed on the ContactTypeCode.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Contact details for this subscriber.
     */
    public HashMap getContactDataHM()
    {
        Vector              l_contactDataV  = null;
        Vector              l_contactTypesV = null;

        i_contactDataHM = new HashMap();

        l_contactDataV = i_customerDetailsData.getContacts()
                                              .getCustomerContactSetAsVector();
        l_contactTypesV = i_contactTypes.getDataV();


        // Populate HashMap with existing customer contact data
        for (int i = 0; i < l_contactDataV.size(); i++)
        {
            i_contactDataHM.put(((CustomerContactData)l_contactDataV.get(i)).getContactTypeCode(),
                                ((CustomerContactData)l_contactDataV.get(i)).getContactDetails());
        }

        // Populate HashMap with remaining contacts as blank strings
        for (int i = 0; i < l_contactTypesV.size(); i++)
        {
            if (!i_contactDataHM.containsKey(((MiscCodeData)l_contactTypesV.get(i)).getCode()))
            {
                i_contactDataHM.put(((MiscCodeData)l_contactTypesV.get(i)).getCode(),
                                    "");
            }
        }

        return (i_contactDataHM);
    } // end of getContactDataHM()


    /**
     * Returns the AddressTypes wrapped within this object.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Set of address types.
     */
    public MiscCodeDataSet getAddressTypes()
    {
        return i_addressTypes;
    }

    /** Sets the AddressTypes object wrapped within this object.
     * 
     * @param p_addressTypes Set of address types.
     */
    public void setAddressTypes( MiscCodeDataSet p_addressTypes )
    {
        i_addressTypes = p_addressTypes;
    }


    /**
     * Returns the Customer Address wrapped within this object.
     * Returns null if there is no CustomerDetailsData object set.
     * 
     * @return Customer addresses.
     */
    public HashMap getAddressDataHM()
    {

        Vector              l_addressDataV  = null;
        Vector              l_addressTypesV = null;
        
        i_addressDataHM = new HashMap();

        l_addressDataV = i_customerDetailsData.getAddresses()
                                              .getCustomerAddressSetAsVector();
        l_addressTypesV = i_addressTypes.getDataV();

        // Populate HashMap with existing customer addresses
        for (int i = 0; i < l_addressDataV.size(); i++)
        {
            i_addressDataHM.put(((CustomerAddressData)l_addressDataV.get(i)).getAddressTypeCode(),
                                 l_addressDataV.get(i));
        }

        // Populate HashMap with remaining addresses types as blank strings
        for (int i = 0; i < l_addressTypesV.size(); i++)
        {
            if (!i_addressDataHM.containsKey(((MiscCodeData)l_addressTypesV.get(i)).getCode()))
            {
                i_addressDataHM.put(((MiscCodeData)l_addressTypesV.get(i)).getCode(),
                                    new CustomerAddressData(i_guiRequest,
                                                            ((MiscCodeData)l_addressTypesV.get(i))
                                                             .getCode(),
                                                            ((MiscCodeData)l_addressTypesV.get(i))
                                                             .getDescription(),
                                                            "",
                                                            "",
                                                            "",
                                                            "",
                                                            "",
                                                            "",
                                                            ""));
            }
        }
        return (i_addressDataHM);
    }


    /**
     * Returns either an empty string or the addressType 
     * (as stored in CustomerAddressData) that was last updated from the GUI.
     * 
     * @return Type of address that was last updated.
     */
    public String getLastUpdatedAddress()
    {
        return i_lastUpdatedAddress;
    }

    /** Sets the last addressType (as stored in CustomerAddressData) to be 
     * updated from the GUI.
     * 
     * @param p_lastUpdatedAddress Type of address that was last updated.
     */
    public void setLastUpdatedAddress( String p_lastUpdatedAddress )
    {
        i_lastUpdatedAddress = p_lastUpdatedAddress;
    }

    /**
     * Returns either an empty string or the addressType 
     * (as stored in CustomerAddressData) that was last updated from the GUI.
     * 
     * @return Type of address that is preferred to be displayed first.
     */
    public String getPreferredAddressType()
    {
        return ( (i_preferredAddressType == null) ? "" : i_preferredAddressType );
    }

    /**
     * Sets the preferred addressType to be displayed in the GUI.
     * 
     * @param p_preferredAddressType Type of address that is preferred to be displayed first.
     */
    public void setPreferredAddressType( String p_preferredAddressType )
    {
        i_preferredAddressType = p_preferredAddressType;
    }

    /**
     * Returns the info text that is necessary to populate the screen 
     * info text field.
     * 
     * @return Information about the subscriber.
     */
    public String getInfoText()
    {
        return i_infoText;
    }

    /** Sets the infoText object wrapped within this object.
     * 
     * @param p_infoText Information about the subscriber.
     */
    public void setInfoText( String p_infoText )
    {
        i_infoText = p_infoText;
    }


    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * 
     * @return True if this object is fully populated.
     */
    public boolean isPopulated()
    {
        return (i_customerDetailsData != null);
    }
} // end of CcCustomerDetailsScreenData  class