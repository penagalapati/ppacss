////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDedicatedAccountsScreenData.java
//      DATE            :       09-Aug-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon#1484/6164
//                              PRD_PPAK00_GEN_CA_372
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the dedicated
//                              accounts screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                            | REFERENCE
//----------+------------+----------------------------------------+-------------
// 06/03/06 | M Erskine  | Allow for 10 digit dedicated account id|PpacLon#2005/8047
//----------+------------+----------------------------------------+--------------------
// 04/09/06 | Ian James  | Change method getBalance() to          | PpacLon#2579/9892
//          |            | getBalanceAfter()                      | PRD_ASCS00_GEN_CA86
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.dataclass.AdjustmentData;
import com.slb.sema.ppas.common.dataclass.AdjustmentHistoryDataSet;
import com.slb.sema.ppas.common.dataclass.DedicatedAccountsData;
import com.slb.sema.ppas.common.dataclass.DedicatedAccountsDataSet;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the DedicatedAccounts screen.
 */
public class CcDedicatedAccountsScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /** String to display on screen to indicate that a balance or date is
     *  unavailable. */
    public static final String C_UNAVAILABLE = "Unavailable";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Set of dedicated accounts data and balances for a subscriber. */
    private DedicatedAccountsDataSet i_dedicatedAccountsDataSet;

    /** DataSet of dedicated account adjustments for a subscriber. */
    private AdjustmentHistoryDataSet i_adjustmentDataSet;

    /** Holds details of available currencies. */
    CufmCurrencyFormatsDataSet       i_currencyDataSet;

    /** Base currency from business configuration. */
    private PpasCurrency             i_baseCurrency;

    /** Holds the subscriber's preferred currency. */
    private PpasCurrency             i_preferredCurrency;

    /** Adjustment codes from business configuration.*/
    private SrvaAdjCodesDataSet      i_adjCodes;

    /** Text giving information on the status of any adjustment request that was
     *  attempted. */
    private String                   i_infoText = "";

    /** Indicates whether an adjustment has been posted for approval. */
    private boolean                  i_postedForApprovalFlag = false;

    /**
     * Indicates whether the expiry comparison date usage flag is set to
     * "Expiry After Date" or not.
     */
    private boolean                  i_expiryAfterDateFlag = false;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcDedicatedAccountsScreenData(GuiContext p_guiContext,
                                         GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen. Otherwise returns false.
     * 
     * @return Flag indicating whether this object is populated.
     */
    public boolean isPopulated()
    {
        return ((i_adjustmentDataSet != null) &&
                (i_dedicatedAccountsDataSet != null));
    }

    /**
     * Sets the dedicated accounts data and balances.
     * 
     * @param p_dedicatedAccountsDataSet Set of dedicated accounts.
     */
    public void setDedicatedAccountsDataSet(DedicatedAccountsDataSet p_dedicatedAccountsDataSet)
    {
        i_dedicatedAccountsDataSet = p_dedicatedAccountsDataSet;
    }

    /**
     * Returns true if the Operator making the request is permitted to make an
     * adjustment. Otherwise returns false.
     * 
     * @return True if the operator can make an adjustment, otherwise false.
     */
    public boolean makeDedAccAdjustIsPermitted()
    {
        return i_gopaData.makeDedAccAdjustIsPermitted();
    }

    /**
     * Sets the adjustment data set of dedicated account adjustments.
     * 
     * @param p_adjustmentDataSet Set of adjustments.
     */
    public void setAdjustmentDataSet( AdjustmentHistoryDataSet p_adjustmentDataSet )
    {
        i_adjustmentDataSet = p_adjustmentDataSet;
    }

    /**
     * Gets the adjustment data set of dedicated account adjustments.
     * 
     * @return Set of adjustments.
     */
    public AdjustmentHistoryDataSet getAdjustmentDataSet()
    {
        return i_adjustmentDataSet;
    }

    /**
     * Sets the base currency.
     * 
     * @param p_baseCurrency The currency in which the monetary amounts are held.
     */
    public void setBaseCurrency (PpasCurrency p_baseCurrency)
    {
        i_baseCurrency = p_baseCurrency;
    }

    /**
     * Gets the base currency.
     * 
     * @return The currency in which the monetary amounts are held.
     */
    public PpasCurrency getBaseCurrency()
    {
        return i_baseCurrency;
    }

    /**
     * Sets the available Adjustment Codes for the current market.
     * 
     * @param p_adjCodes Set of adjustment codes.
     */
    public void setAdjustmentCodes(SrvaAdjCodesDataSet p_adjCodes)
    {
        i_adjCodes = p_adjCodes;
    }

    /**
     * Gets the available Adjustment Codes for the current market.
     * 
     * @return Array of adjustment codes.
     */
    public SrvaAdjCodesData[] getAdjustmentCodes()
    {
        return i_adjCodes.getAllArray();
    }

    /**
     * Gets an array of unique adjustment types that the adjustment
     * codes returned from the business config cache associate to.
     * Adjustment types can associate with more than one code,
     * but an adjustment code is associated with a single type.
     * 
     * @return Array of adjustment types.
     */
    public String[] getAdjustmentTypes()
    {
        String[]           l_typesARR  = null;
        SrvaAdjCodesData[] l_codes     = i_adjCodes.getAllArray();
        String             l_type      = null;
        boolean            l_found;
        Vector             l_typesV    = new Vector(l_codes.length, 1);  

        // There can never be more than l_codes.length 
        // unique Adjustment Types

        // Loop through the adjustment codes, extract the adjustment type
        // and add to the Vector of unique types if it is not already there.
        for (int l_loop1 = 0; l_loop1 < l_codes.length; ++l_loop1)
        {
            l_type = l_codes[l_loop1].getAdjType();
            l_found = false;

            for(int l_loop2 = 0; l_loop2 < l_typesV.size(); ++l_loop2)
            {
                if (l_type.equals(l_typesV.elementAt(l_loop2)))
                {
                    l_found = true;
                }
            }
            if (!l_found)
            {
                l_typesV.addElement(l_type);
            }
        }

        // Now construct and populate array of unique types to be returned.
        l_typesARR = new String[l_typesV.size()];
        for(int l_loop3 = 0; l_loop3 < l_typesV.size(); ++l_loop3)
        {
            l_typesARR[l_loop3] = (String)l_typesV.elementAt(l_loop3);
        }

        return l_typesARR;

    }

    /**
     * Sets the available currencies and their associated data.
     * 
     * @param p_availableCurrencies Set of available currency formats.
     */
    public void setCurrencyDataSet(CufmCurrencyFormatsDataSet p_availableCurrencies)
    {
        i_currencyDataSet = p_availableCurrencies;
    }

    /**
     * Gets the available currencies and their associated data.
     * 
     * @return Set of available currencies.
     */
    public CufmCurrencyFormatsDataSet getCurrencyDataSet()
    {
        return i_currencyDataSet;
    }

    /**
     * Sets the subscriber's preferred currency.
     * 
     * @param p_preferredCurrency The subscriber's preferred currency.
     */
    public void setPreferredCurrency(PpasCurrency p_preferredCurrency)
    {
        i_preferredCurrency = p_preferredCurrency;
    }

    /**
     * Gets the preferred currency of the subscriber.
     * 
     * @return The subscriber's preferred currency.
     */
    public PpasCurrency getPreferredCurrency()
    {
        return i_preferredCurrency;
    }

    /**
     * Sets the posted for approval flag to indicate that an adjustment
     * has been held for approval.
     */
    public void setPostedForApproval()
    {
        i_postedForApprovalFlag = true;
    }

    /**
     * Returns a flag to indicate whether or not the adjustment was held for approval.
     * 
     * @return true if the adjustment was held for approval otherwise false.
     */
    public boolean wasPostedForApproval()
    {
        return i_postedForApprovalFlag;
    }

    /**
     * Sets the expiry after date flag to indicate that the system is configured to
     * process expiry after the specified expiry date.  This indicates that we must
     * allow dedicated account expiry dates to be set to the day before the current date.
     */
    public void setExpiryAfterDate()
    {
        i_expiryAfterDateFlag = true;
    }

    /**
     * Returns true if expiry after date flag is set.
     * @return is the expiry after date flag set
     */
    public boolean getExpiryAfterDate()
    {
        return i_expiryAfterDateFlag;
    }

    /**
     * Sets the text giving information on the status of any adjustment request
     * that was attempted.
     * 
     * @param p_text The informational tesxt.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /**
     * Gets the text giving information on the status of any adjustment request
     * that was attempted.
     * 
     * @return The informational tesxt.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /**
     * Gets the dedicated account id of the p_index element in the
     * dedicated accounts data set.
     * 
     * @param p_index Sequence of dedicated account.
     * @return Dedicated account identifier as a <code>String</code>.
     */
    public String getDedId(int p_index)
    {
        String l_return = "";
        
        DedicatedAccountsData l_data = getDedicatedAccountByIndex(p_index);
        
        if (l_data != null)
        {
            l_return = Long.toString(l_data.getIdentifier());
        }

        return l_return;
    }

    /**
     * Gets the dedicated account description of the p_index element in the
     * dedicated accounts data set.
     * 
     * @param p_index Sequence of dedicated account.
     * @return Dedicated account description.
     */
    public String getDedDescription(int p_index)
    {
        String l_return = "";
        
        DedicatedAccountsData l_data = getDedicatedAccountByIndex(p_index);
        
        if (l_data != null)
        {
            l_return = l_data.getDescription();
        }

        return l_return;
    }

    /**
     * Gets the dedicated account balance of the p_index element in the
     * dedicated accounts data set.
     * 
     * @param p_index Sequence of dedicated account.
     * @return Balance outstanding on the specified dedicated account.
     */
    public String getDedBalance(int p_index)
    {
        String l_balance = C_UNAVAILABLE;

        DedicatedAccountsData l_data = getDedicatedAccountByIndex(p_index);
        
        if ( (l_data != null) && (l_data.getBalanceAfter() != null) )
        {
            l_balance = i_guiContext.getMoneyFormat().format(l_data.getBalanceAfter());
        }

        return l_balance;
    }

    /**
     * Gets the currency associated with the dedicated account balance of the p_index element in the
     * dedicated accounts data set.
     * 
     * @param p_index Sequence of dedicated account.
     * @return String representing the currency code of this dedicated account.
     */
    public String getDedCurrency(int p_index)
    {
        String l_currency = C_UNAVAILABLE;

        DedicatedAccountsData l_data = getDedicatedAccountByIndex(p_index);
        
        if ( (l_data != null) && (l_data.getBalanceAfter() != null) )
        {
            l_currency = l_data.getBalanceAfter().getCurrency().getCurrencyCode();
        }

        return l_currency;
    }

    /**
     * Gets the dedicated account expiry date of the p_index element in the
     * dedicated accounts data set.
     * 
     * @param p_index Sequence of dedicated account.
     * @return Expiry date of the specified dedicated account.
     */
    public String getDedExpDate(int p_index)
    {
        String   l_expiryDateString = C_UNAVAILABLE;

        DedicatedAccountsData l_data = getDedicatedAccountByIndex(p_index);
        
        if (l_data != null)
        {
            PpasDate l_expiryDate = l_data.getExpiryDate();

            if (l_expiryDate != null)
            {
                l_expiryDateString = l_expiryDate.toString();
            }
        }

        return l_expiryDateString;
    }

    /**
     * Gets the number of valid dedicated accounts for the subscriber.
     * 
     * @return The number of dedicated accounts.
     */
    public int getDedicatedAccountCount()
    {
        return i_dedicatedAccountsDataSet == null ? 0 : i_dedicatedAccountsDataSet.getLength();
    }

    /** Get the dedicated account based on an index in the underlying data set.
     * 
     * @param p_index Index number to obtain.
     * @return Dedicated Account data.
     */
    private DedicatedAccountsData getDedicatedAccountByIndex(int p_index)
    {
        DedicatedAccountsData l_return = null;
        
        if (i_dedicatedAccountsDataSet != null &&
            i_dedicatedAccountsDataSet.getLength() > p_index)
        {
            l_return = (i_dedicatedAccountsDataSet.getAccountsAsArray())[p_index];
        }

        return l_return;
    }
    
    /**
     * Get the new expiry date after a dedicated account adjustment as a String. If
     * the date is <code>null</code>, an empty string will be returned. If there is a date object
     * but it is not set, <code>NONE</code> will be returned. If the date is set,
     * the string representation of the date will be returned.
     * 
     * @param p_adj the element number in the data set
     * @return a string representing the new expiry date
     */
    public String getAdjExpDateString(int p_adj)
    {
        AdjustmentData l_adjData = (AdjustmentData)i_adjustmentDataSet.getAdjustmentHistoryArr()[p_adj];
        PpasDate l_newExpDate;
        String l_return = "";
        
        if (l_adjData != null)
        {
            l_newExpDate = l_adjData.getNewExpiryDate();
            
            if (l_newExpDate != null)
            {
                l_return = (l_newExpDate.isSet() ? l_newExpDate.toString() : "NONE");
            }
        }
        
        return l_return;
    }
}
