////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentService.java
//      DATE            :       01-Mar-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1295/5175
//                              PRD_PPAK00_DEV_IN_032
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasAdjustmentService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                       | REFERENCE
//----------+------------+-----------------------------------+------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues  | PpacLon#1/17
//----------+------------+-----------------------------------+------------------
// 23/10/03 | E.P. Hebe  | Add extra parameter in method     |
//          |            | postAdjustmentForApproval while   |
//          |            | calling PPassAdjustmentService    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AdjustmentHistoryDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAdjustmentService;
import com.slb.sema.ppas.util.logging.Logger;

/** Provides methods to perform GUI specific services associated with Adjustments. */
public class CcAdjustmentService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdjustmentService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasAdjustmentService i_adjustmentService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcAdjustmentService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcAdjustmentService(GuiRequest p_guiRequest,
                               long       p_flags,
                               Logger     p_logger,
                               GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, 
                C_CLASS_NAME, 
                22000, 
                this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create internal Adjustment service to be used by this service.
        i_adjustmentService = new PpasAdjustmentService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, 
                C_CLASS_NAME, 
                22010, 
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAdjustments =
                                        "getAdjustments";
    /**
     * Retrieves Adjustment History for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Adjustment data set.
     */
    public CcAdjustmentDataSetResponse getAdjustments(
                                           GuiRequest p_guiRequest,
                                           long       p_timeoutMillis)
    {
        CcAdjustmentDataSetResponse l_ccAdjustmentDataSetResponse  = null;
        GuiResponse                 l_guiResponse                  = null;
        AdjustmentHistoryDataSet    l_adjustmentDataSet            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, 
                C_CLASS_NAME, 
                70000, 
                this,
                "Entered " + C_METHOD_getAdjustments);
        }

        try
        {
            l_adjustmentDataSet = i_adjustmentService.getAdjustments(p_guiRequest, p_timeoutMillis);

            l_ccAdjustmentDataSetResponse =
                new CcAdjustmentDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),   // PpacLon#1/17
                                GuiResponse.C_KEY_SERVICE_SUCCESS,
                                new Object[] {},
                                GuiResponse.C_SEVERITY_SUCCESS,
                                l_adjustmentDataSet);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, 
                    C_CLASS_NAME, 
                    70210, 
                    this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get adjustments", l_pSE);

            l_ccAdjustmentDataSetResponse =
                new CcAdjustmentDataSetResponse(
                                          p_guiRequest,
                                          p_guiRequest.getGuiSession().
                                            getSelectedLocale(),  // PpacLon#1/17
                                          l_guiResponse.getKey(),
                                          l_guiResponse.getParams(),
                                          GuiResponse.C_SEVERITY_FAILURE,
                                          null);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, 
                C_CLASS_NAME, 
                71000, 
                this,
                "Leaving " + C_METHOD_getAdjustments);
        }

        return l_ccAdjustmentDataSetResponse;

    }  // end public CcAdjustmentDataSetResponse getAdjustments(...)

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_postAdjustment = "postAdjustment";
    /**
     * Posts an Adjustment for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_transAmt      Transaction amount.
     * @param p_type          Type of adjustment.
     * @param p_code          Adjustment code.
     * @param p_description   Adjsutment description.
     * @param p_adjApprov     Flag indicating if the adjustment has been approved or if
     *                        the limit needs to be checked.
     * @return Response containing the success/failure status of the attempt to
     *         post Adjustment details, in the case of failure, the key defining
     *         the failure reason.
     */
    public GuiResponse postAdjustment(GuiRequest p_guiRequest,
                                      long       p_timeoutMillis,
                                      Money      p_transAmt,
                                      String     p_type,
                                      String     p_code,
                                      String     p_description,
                                      Boolean    p_adjApprov)
    {
        GuiResponse                 l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, 
                C_CLASS_NAME, 
                30000, 
                this,
                "Entered " + C_METHOD_postAdjustment);
        }
        try
        {
            i_adjustmentService.postAdjustment(
                p_guiRequest,
                p_timeoutMillis,
                p_transAmt,
                p_type,
                p_code,
                p_description,
                p_adjApprov);

            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),  // PpacLon#1/17
                                GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                new Object[] {"post adjustment"},
                                GuiResponse.C_SEVERITY_SUCCESS);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, 
                    C_CLASS_NAME, 
                    30210, 
                    this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "make the adjustment", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, 
                C_CLASS_NAME, 
                31000, 
                this,
                "Leaving " + C_METHOD_postAdjustment);
        }

        return (l_guiResponse);

    } // end public GuiResponse postAdjustment(...)
} // end public class CcAdjustmentService
