////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcStatementOfAccountScreenData.Java
//DATE            :       29-May-2007
//AUTHOR          :       Andy Harris
//REFERENCE       :       PpacLon#3124/11533
//                        PRD_ASCS00_GEN_CA_114
//
//COPYRIGHT       :       WM-data 2007
//
//DESCRIPTION     :       Gui Screen Data object for the 
//                        Statement of Account screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 12/09/07 | S James    | Get and store the prefix text to| PpacLon#3342/12077
//          |            | be used for events which affect |
//          |            | dedicated account balances      |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.StatementOfAccountData;
import com.slb.sema.ppas.common.dataclass.StatementOfAccountDataSet;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
 
/**
 * Gui Screen Data object for the Statement of Account screen.
 */
public class CcStatementOfAccountScreenData extends GuiScreenData
{
//  --------------------------------------------------------------------------
    // Private class constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcStatementOfAccountScreenData";
    
    /** Define the code to be used to select service class misc code data. */
    public static final String C_SERVICE_CLASS_MISC_TYPE         = "CLS";
    
    /** Define the field_id value to be used to get the Dedicated Account Used
     *  text from srva_fields to prefix to the Description/Other Party column value
     */
    private static final String C_DED_ACC_EVENT = "soa_ded_acc_event";
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** The start date of the statement of account viewing that this object defines. */
    private PpasDate i_startDate;

    /** The end date of the statement of account viewing that this object defines. */
    private PpasDate i_endDate;

    /** Indicates whether the subscriber in the request for this screen
     * is a subordinate or not.
     */
    private boolean i_isSubordinate;

    //Variables for the Statement of Account Data in the DataSet
    /** The statement of account events to be displayed on the table. */
    private StatementOfAccountDataSet i_statementOfAccountDataSet;
    
    /** The text to prefix the Decsription/Other Party value with on
     *  the GUI screen if dedicated accounts were used 
     */
    private String i_dedAccUsedPrefix = "";
    
    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcStatementOfAccountScreenData(GuiContext p_guiContext, GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 95000, this,
                "Constructing " + C_CLASS_NAME );
        }
        i_startDate = new PpasDate ("");
        i_endDate = new PpasDate ("");
        // get the prefix from srva_fields in the config cache
        BusinessConfigCache l_cache = (BusinessConfigCache)i_guiContext.getAttribute("BusinessConfigCache");
        i_dedAccUsedPrefix = l_cache.getSrvaFieldsCache().getAvailable().
                                 getComment(Market.C_GLOBAL_MARKET, C_DED_ACC_EVENT).getDescription();
                
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 95090, this,
                "Constructed " + C_CLASS_NAME );
        }
    }
    /**
     * @return Returns the endDate.
     */
    public PpasDate getEndDate()
    {
        return i_endDate;
    }
    /**
     * @param p_endDate The endDate to set.
     */
    public void setEndDate(PpasDate p_endDate)
    {
        i_endDate = p_endDate;
    }
    
    /**
     * @return Returns the startDate.
     */
    public PpasDate getStartDate()
    {
        return i_startDate;
    }
    /**
     * @param p_startDate The startDate to set.
     */
    public void setStartDate(PpasDate p_startDate)
    {
        i_startDate = p_startDate;
    }

    /**
     * @return Returns the StatementOfAccountDataSet.
     */
    public StatementOfAccountDataSet getStatementOfAccountDataSet()
    {
        return i_statementOfAccountDataSet;
    }
    
    /**
     * Returns an array of StatementOfAccountData objects.
     * 
     * @return Array of statement of account details.
     */
    public StatementOfAccountData[] getStatementOfAccountArray()
    {
        return i_statementOfAccountDataSet.asArray();
    }
    /**
     * @param p_statementOfAccountDataSet The StatementOfAccountDataSet to set.
     */
    public void setStatementOfAccountDataSet(StatementOfAccountDataSet p_statementOfAccountDataSet)
    {
        i_statementOfAccountDataSet = p_statementOfAccountDataSet;
    }

    /** 
     * Returns <code>true</code> if this subscriber
     * is a subordinate, otherwise returns <code>false</code>.
     * 
     * @return True if the subscriber is a subordinate, otherwise, false.
     */
    public boolean isSubordinate()
    {
        return i_isSubordinate;
    }

    /**
     * Marks this object as being for a subordinate.
     */
    public void setIsSubordinate()
    {
        i_isSubordinate = true;
        return;
    }
    
    /**
     * Returns the configured text to prefix the Description/Other Party
     * value with in the SoA GUI screen if Dedicated Accounts were used
     * @return the configured text to prefix
     */
    public String getDedAccUsedPrefix()
    {
        return i_dedAccUsedPrefix;
    }
}
