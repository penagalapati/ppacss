////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubordinatesServlet.Java
//      DATE            :       20-Mar-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon#1325/5287
//                              PRD_PPAK00_DEV_IN_037
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Subordinates Screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
// 06/10/03 | R Isaacs   | Privilege checks added          | CR#60/571
//----------+------------+---------------------------------+--------------------
// 27/10/05 | M I Erskine| Change info text and add sleep  | PpacLond#1742/7303
//          |            | to allow for persisted requests |
//          |            | to SDP that may complete after  |
//          |            | screen data object is populated |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcSubordinatesScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountDataSetResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcBasicAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcSubordinatesService;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.AccountDataSet;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData; //CR#60/571
import com.slb.sema.ppas.gui.support.GuiSession; //CR#60/571

/**
 * This servlet handles requests between the subordinates screen and the
 * subordinates service.
 */
public class CcSubordinatesServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcSubordinatesServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Subordinates Service to be used by this servlet. */
    private CcSubordinatesService i_ccSubordinatesService = null;

    /** The CC GUI Account Service to be used by this servlet. */
    private CcAccountService      i_ccAccountService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcSubordinatesServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise this object.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10040, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcSubordinatesService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10050, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the Subordinates screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest )
    {
        String                      l_command                = null;
        String                      l_forwardUrl             = null;
        CcSubordinatesScreenData    l_subordinatesScreenData = null;
        GuiResponse                 l_guiResponse            = null;
        boolean                     l_accessGranted          = false; // CR#60/571
        GopaGuiOperatorAccessData   l_gopaData               = null;  // CR#60/571

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 10060, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcSubordinatesService if it does not already exist ...
        if (i_ccSubordinatesService == null)
        {
            i_ccSubordinatesService = new CcSubordinatesService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcAccountService if it does not already exist
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create screen data object.
        l_subordinatesScreenData = new CcSubordinatesScreenData( i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam( p_guiRequest,
                                        0,
                                        p_request,
                                        "p_command",
                                        true,
                                        "",
                                        new String [] {"GET_SCREEN_DATA",
                                                       "ADD_SUBORDINATE"});

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasSubordinatesScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("ADD_SUBORDINATE")) && l_gopaData.hasSubordinatesScreenAccess())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                      p_guiRequest.getGuiSession().getSelectedLocale(), //CR#60/571
                                      GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                      new Object [] {},
                                      GuiResponse.C_SEVERITY_FAILURE);

                l_subordinatesScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_subordinatesScreenData);
            }
            else
            {
                if (l_command.equals("ADD_SUBORDINATE"))
                {
                    setIsUpdateRequest(true);

                    addSubordinate(p_guiRequest, p_request, l_subordinatesScreenData );
                    
                    // The above action has been provisioned by IS and the request to the SDP is
                    // persisted. It may succeed, fail, or timeout.
                    // Sleep for 1 second as this will normally allow enough time for the response 
                    // to be received by ASCS and the database updated.
                    try
                    {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException l_e)
                    {
                        // Do nothing.
                    }
                }

                getScreenData( p_guiRequest, l_subordinatesScreenData );
            }
        } // end try
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10070, this,
                    "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_subordinatesScreenData.addRetrievalResponse( p_guiRequest, l_guiResponse );

            // Store the response in the request
            p_request.setAttribute( "p_guiScreenData", l_subordinatesScreenData );
        } // end catch

        if (l_accessGranted)
        {
            if (l_subordinatesScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Subordinates screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                // Get the update messages and the retrieval error message and
                // attach them to the request

                p_request.setAttribute ( "p_guiScreenData", l_subordinatesScreenData );
            }
            else
            {
                // CcSubordinatesScreenData object has the necessary data
                // to populate the Subordinates screen.
                l_forwardUrl = "/jsp/cc/ccsubordinates.jsp";

                p_request.setAttribute( "p_guiScreenData", l_subordinatesScreenData );
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 10080, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );
    }

    //-------------------------------------------------------------------------
    // Private Methods
    //-------------------------------------------------------------------------


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addSubordinate =
                                        "addSubordinate";

    /** Calls CcsubordinatesService.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Subordinate screen.
     */
    private void addSubordinate( GuiRequest                p_guiRequest,
                                 HttpServletRequest        p_request,
                                 CcSubordinatesScreenData  p_screenData )

    {
        Msisdn                l_subMsisdn              = null;
        Msisdn                l_masterMsisdn           = null;
        GuiResponse           l_guiResponse            = null;
        PpasDateTime          l_currentTime            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 10090, this,
                "Entered " + C_METHOD_addSubordinate);
        }

        // Get params from request...

        try
        {
            l_subMsisdn = getValidMsisdn(
                      p_guiRequest,
                      0,
                      p_request,
                      "p_subMsisdn",
                      true,
                      "",
                      i_guiContext.getMsisdnFormatInput());
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10100, this,
                    "Exception parsing update parameters: " + l_pSE);
            }

            // Convert the exception into a suitable response...

            l_guiResponse = handleInvalidParam( p_guiRequest, l_pSE );

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse( p_guiRequest, l_guiResponse );
        }

        try
        {
            // the master msisdn is now sent from the browser in the URL, as it is
            // needed in the business service and this avoids going to the database again
            l_masterMsisdn = getValidMsisdn(
                      p_guiRequest,
                      0,
                      p_request,
                      "p_masterMsisdn",
                      true,
                      "",
                      i_guiContext.getMsisdnFormatInput());

            p_guiRequest.setMsisdn(l_masterMsisdn);
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10110, this,
                    "Exception parsing update parameters: " + l_pSE);
            }

            // Convert the exception into a suitable response...

            l_guiResponse = handleInvalidParam( p_guiRequest, l_pSE );

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse( p_guiRequest, l_guiResponse );
        }

        // Now check what parameters are being updated and call the required
        // service(s).

        if (p_screenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do attempt
            // this requested service.
            l_guiResponse =
                new GuiResponse( p_guiRequest,
                                 p_guiRequest.getGuiSession().
                                   getSelectedLocale(),  // PpacLon#1/17
                                 GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                 new Object[] {"expiry dates change"},
                                 GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            // The account is being added as a subordinate.
            l_guiResponse = i_ccSubordinatesService.addSubordinate(p_guiRequest, l_subMsisdn, i_timeout);
        }

        p_screenData.addUpdateResponse( p_guiRequest, l_guiResponse );

        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {

            //Redisplay failed msisdn in the field.
            p_screenData.setSubMsisdn(p_request.getParameter("p_subMsisdn"));

            // Add subordinate failed message.
            p_screenData.setAddSubFailed(true);
            p_screenData.setInfoText("Failed to add subordinate at " + l_currentTime);
        }
        else
        {
            // Add subordinate success message.
            p_screenData.setSubAdded(true);
            p_screenData.setInfoText("Successfully provisioned addition of subordinate at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 10110, this,
                "Leaving " + C_METHOD_addSubordinate);
        }
    } // end private void addSubordinate(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getScreenData = "getScreenData";
    /** Calls CcSubordinatesService to retrieve the list of subordinates if any
     * exist.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Subordinate screen.
     */
    private void getScreenData( GuiRequest                 p_guiRequest,
                                CcSubordinatesScreenData   p_screenData )
    {
        CcAccountDataResponse         l_ccAccountDataResponse         = null;
        CcAccountDataSetResponse      l_ccAccountDataSetResponse      = null;
        AccountDataSet                l_accountDataSet                = null;
        CcBasicAccountDataResponse    l_basicAccountDataResponse      = null;
        AccountData                   l_accountData                   = null;
        BasicAccountData              l_masterBasicData               = null;
        String                        l_masterCustId                  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 10120, this,
                "Entered " + C_METHOD_getScreenData);
        }

        l_ccAccountDataResponse = i_ccAccountService.getFullData ( p_guiRequest, i_timeout);

        p_screenData.addRetrievalResponse( p_guiRequest, l_ccAccountDataResponse);

        if ( l_ccAccountDataResponse.isSuccess())
        {
            l_accountData = l_ccAccountDataResponse.getAccountData();
            p_screenData.setAccountData( l_accountData);
        }

        if (!p_screenData.hasRetrievalError())
        {
            l_basicAccountDataResponse =
                     i_ccSubordinatesService.getMasterBasicData ( p_guiRequest, i_timeout );

            p_screenData.addRetrievalResponse( p_guiRequest, l_basicAccountDataResponse);

            if (l_basicAccountDataResponse.isSuccess())
            {
                l_masterBasicData = l_basicAccountDataResponse.getBasicAccountData();
                l_masterCustId      = l_masterBasicData.getCustId();
                p_screenData.setMasterBasicData(l_masterBasicData);
            }
        }

        if (!p_screenData.hasRetrievalError())
        {
            l_ccAccountDataSetResponse =
                 i_ccSubordinatesService.getSubordinateDataSet(p_guiRequest, i_timeout, l_masterCustId);

            p_screenData.addRetrievalResponse(p_guiRequest, l_ccAccountDataSetResponse);

            if ( l_ccAccountDataSetResponse.isSuccess())
            {
                l_accountDataSet = l_ccAccountDataSetResponse.getAccountDataSet();
                p_screenData.setAccountDataSet( l_accountDataSet );
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 10130, this,
                "Leaving " + C_METHOD_getScreenData);
        }
    } // end private void getScreenData(...)
} // End of CcSubordinatesServlet class.
