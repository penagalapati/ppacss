////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerSelectionService.java
//      DATE            :       1-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_27
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Customer Care Service that wrappers calls to 
//                              the PpasCustomerSelectionService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CustomerSelectionData;
import com.slb.sema.ppas.common.dataclass.CustomerSelectionDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasCustomerSelectionService;
import com.slb.sema.ppas.util.logging.Logger;

/** Customer care service that wraps the Ppas Customer Selection Internal
 *  service. This service returns the data generated by the Ppas Customer
 *  Selection Internal service in a service specific response and translates
 *  any exception thrown by the internal service into appropriate response
 *  keys.
 */
public class CcCustomerSelectionService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustomerSelectionService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The customer selection internal service used by this Gui Service. */
    private PpasCustomerSelectionService i_custSelectionIS;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the Customer Care Customer Selection Service.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcCustomerSelectionService(
        GuiRequest p_guiRequest,
        long       p_flags,
        Logger     p_logger,
        GuiContext p_guiContext)
    {
        super (p_guiRequest,
               p_flags,
               p_logger,
               p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 68000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal account service to be used by this service.
        i_custSelectionIS = new PpasCustomerSelectionService (p_guiRequest,
                                                              p_logger,
                                                              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 68090, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDetails = "getDetails";
    /**
     *  Gets customer details for customers that meet given selection criteria.
     *  The selection criteria are market, surname, first name, social 
     *  security number and MSISDN. The service does not allow  selection based
     *  on first name only or market only (i.e. either the surname, the ssn or
     *  the MSISDN  must be supplied to the service). 
     * 
     * @param p_guiRequest  The request being processed.
     * @param p_custStatusFlags account status flags.
     *  See {@link com.slb.sema.ppas.is.isil.sqlservice.AccountSqlService#readBasicFromMsisdn
     *  readBasicFromMsisdn} for valid values.
     * @param p_timeoutMillis Number of milli-seconds until this request should time out.
     *  @param p_market Market on which to base the selection of customers. 
     *                  A negative value indicates that the service location 
     *                  is not to be used in the selection criteria.
     *  @param p_ssn    Social security number to base the selection of 
     *                  customers upon. If this parameter is <code>null</code>,
     *                  then the selection of customers will not be based upon
     *                  their social security numbers.
     *  @param p_surname The surname of customers to base their selection
     *                   upon. If this parameter is <code>null</code>, then
     *                   the customer's surname will not be used in the
     *                   selection. This parameter may contain * or % wildcard
     *                   characters.
     *  @param p_firstName The first name of customers to base their selection
     *                     upon. If this parameter is <code>null</code>, then
     *                     the customer's first name will not be used in the
     *                     selection. Also, the <code>p_firstName</code> may
     *                     only be non-null if one of the <code>p_surname</code>
     *                     or <code>p_ssn</code> parameters is non-null i.e. the
     *                     service does not allow selection of customers based
     *                     on their first name only. This parameter may
     *                     contain * or % wildcard characters.
     * @param p_msisdn The msisdn that customers must have to be selected by
     *                 this service. If this parameter is <code>null</code>,
     *                 then the Msisdn will not be used in the selection 
     *                 criteria.
     * @return A customer selection response object containing the data
     *         selected based on the method parameters.
     */
    public CcCustomerSelectionDataSetResponse getDetails(
        GuiRequest p_guiRequest,
        long       p_custStatusFlags,
        long       p_timeoutMillis,
        Market     p_market,
        String     p_ssn,
        String     p_surname,
        String     p_firstName,
        Msisdn     p_msisdn)
    {
        GuiResponse                          l_guiResponse = null;
        CcCustomerSelectionDataSetResponse   l_custSelectionResponse  = null;
        CustomerSelectionDataSet             l_selectedCustomers;
        CustomerSelectionData                l_selectedCustomer;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 68100, this,
                "Entered " + C_METHOD_getDetails);
        }

        try
        {
            l_selectedCustomers = i_custSelectionIS.getDetails(
                                                p_custStatusFlags,
                                                p_guiRequest,
                                                p_timeoutMillis,
                                                p_market,
                                                p_ssn,
                                                p_surname,
                                                p_firstName,
                                                p_msisdn);

            // TODO  Code does not match comment.
            // Check to see if we have selected just one account and that 
            // account is either 'not active' or 'migrated' ... if so, then
            // generate an appropriate failure response.
            if (l_selectedCustomers.getDataV().size() == 1)
            {
                l_selectedCustomer = (CustomerSelectionData)
                                  (l_selectedCustomers.getDataV().elementAt(0));
            }

            // Create success response if we've not found a single 'not active'
            // or migrated account in the data set of selected accounts.
            if (l_custSelectionResponse == null)
            {
                l_custSelectionResponse =
                         new CcCustomerSelectionDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),  // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_selectedCustomers);
            }                    
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 68110, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "select customers", l_pSE);

            // Generate failure response using the key and message parameters
            // in the GuiResponse obtained above.
            l_custSelectionResponse = new CcCustomerSelectionDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 68190, this,
                "Leaving " + C_METHOD_getDetails);
        }

        return (l_custSelectionResponse);
    }  // end method getDetails

} // end public class CcCustomerSelectionService
