////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentScreenData.java
//      DATE            :       01-Mar-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1295/5175
//                              PRD_PPAK00_DEV_IN_032
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the 
//                              Adjustments screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.dataclass.AdjustmentHistoryDataSet;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Adjustments screen.
 */
public class CcAdjustmentScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Adjustment data set object wrapped by this object. */
    private AdjustmentHistoryDataSet i_adjustmentDataSet;

    /** Holds details of available currencies. */
    private CufmCurrencyFormatsDataSet       i_currencyDataSet;

    /** Holds the subscriber's preferred currency. */
    private PpasCurrency             i_preferredCurrency;

    /** GuiAdjustmentCodeDataSetResponse from business configuration cache.*/
    private SrvaAdjCodesDataSet      i_adjCodes;

    /** Text giving information on the status of any adjustment request that was
     *  attempted.
     */
    private String                   i_infoText = "";

    /** Boolean flag indicating when an adjustment has been posted for 
     *  approval. <code>true</code> if the adjustment has been successfully
     *  posted for approval; otherwise, <code>false</code>.
     */
    private boolean                  i_postedForApproval = false;


    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcAdjustmentScreenData(GuiContext p_guiContext,
                                  GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns <code>true</code> if the Operator making the request is 
     * permitted to make an Adjustment.  Otherwise <code>false</code>
     * is returned.
     * @return True if the CSO is allowed to make an adjustment.
     */
    public boolean makeAdjustmentIsPermitted()
    {
        return (i_gopaData.makeAdjustmentIsPermitted());
    }

    /**
     * Returns the adjustment data set object wrapped within this object.
     * @return Set of adjustment data.
     */
    public AdjustmentHistoryDataSet getAdjustmentDataSet()
    {
        return i_adjustmentDataSet;
    }

    /** Sets the adjustment data set object wrapped within this object.
     * @param p_adjustmentDataSet Set of adjustment data.
     */
    public void setAdjustmentDataSet( AdjustmentHistoryDataSet p_adjustmentDataSet )
    {
        i_adjustmentDataSet = p_adjustmentDataSet;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return (i_adjustmentDataSet != null);
    }

    /** Set up data set of Adjustment Codes returned from 
     * business config service.
     * @param p_adjCodes Set of adjustment codes.
     */
    public void setAdjustmentCodes(SrvaAdjCodesDataSet p_adjCodes)
    {
        i_adjCodes = p_adjCodes;
    }

    /**
     * Returns the available Adjustment Codes that were returned from
     * the BusinessConfigService for the current market.
     * @return String array of Adjustment Codes.
     */
    public SrvaAdjCodesData[] getAdjustmentCodes()
    {
        return(i_adjCodes.getAllArray());
    } // end public String[] getAdjustmentCodes()

    /**
     * Returns an array of unique adjustment types that the adjustment
     * codes returned from the business config cache associate to.
     * Adjustment types can associate with more than one code,
     * but an adjustment code is associated with a single type.
     * @return Array of adjustment types.
     */
    public String[] getAdjustmentTypes()
    {
        String[]           l_typesARR  = null;
        SrvaAdjCodesData[] l_codes     = i_adjCodes.getAllArray();
        String             l_type      = null;
        boolean            l_found;
        Vector             l_typesV    = new Vector(l_codes.length, 1);  

        // There can never be more than l_codes.length unique Adjustment Types

        // Loop through the adjustment codes, extract the adjustment type
        // and add to the Vector of unique types if it is not already there.
        for (int l_loop1 = 0; l_loop1 < l_codes.length; ++l_loop1)
        {
            l_type = l_codes[l_loop1].getAdjType();
            l_found = false;

            for(int l_loop2 = 0; l_loop2 < l_typesV.size(); ++l_loop2)
            {
                if (l_type.equals(l_typesV.elementAt(l_loop2)))
                {
                    l_found = true;
                }
            }
            if (!l_found)
            {
                l_typesV.addElement(l_type);
            }
        }

        // Now construct and populate array of unique types to be returned.
        l_typesARR = new String[l_typesV.size()];
        for(int l_loop3 = 0; l_loop3 < l_typesV.size(); ++l_loop3)
        {
            l_typesARR[l_loop3] = (String)l_typesV.elementAt(l_loop3);
        }

        return l_typesARR;

    } // end public String[] getAdjustmentTypes()

    /**
     * Sets the available currencies and their associated data.
     * 
     * @param p_availableCurrencies Set of available currency formats.
     */
    public void setCurrencyDataSet(CufmCurrencyFormatsDataSet p_availableCurrencies)
    {
        i_currencyDataSet = p_availableCurrencies;
    }

    /**
     * Gets the available currencies and their associated data.
     * 
     * @return Set of available currencies.
     */
    public CufmCurrencyFormatsDataSet getCurrencyDataSet()
    {
        return i_currencyDataSet;
    }

    /**
     * Sets the subscriber's preferred currency.
     * 
     * @param p_preferredCurrency The subscriber's preferred currency.
     */
    public void setPreferredCurrency(PpasCurrency p_preferredCurrency)
    {
        i_preferredCurrency = p_preferredCurrency;
    }

    /**
     * Gets the preferred currency of the subscriber.
     * 
     * @return The subscriber's preferred currency.
     */
    public PpasCurrency getPreferredCurrency()
    {
        return i_preferredCurrency;
    }

    /** Sets the text giving information on the status of any adjustment request
     *  that was attempted.
     * @param p_text String representing the status of an adjusment request.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving information on the status of any adjustment request
     *  that was attempted.
     * @return String representing the status of an adjusment request.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Sets the <code>i_postedForApproval</code> attribute to 
     *  <code>true</code>, indicating that the adjustment has been successfully
     *  posted for approval.
     */
    public void setPostedForApproval()
    {
        i_postedForApproval = true;
        return;
    }

    /** Returns a value indicating whether the adjustment was successfully
     *  posted for approval.
     *  @return <code>true</code> if the adjustment was successfully posted
     *          for approval; otherwise, <code>false</code>.
     */
    public boolean wasPostedForApproval()
    {
        return (i_postedForApproval);
    }
} // end of CcAdjustmentsScreenData class
