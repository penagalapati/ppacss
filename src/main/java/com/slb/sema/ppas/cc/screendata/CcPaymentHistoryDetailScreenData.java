////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPaymentHistoryDetailScreenData.java
//      DATE            :       8-Sep-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1513/6449
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Payment History
//                              Detail screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 30/09/03 | M.Brister  | Removed i_chargingUnits,        | PpacLon#43/384
//          |            | i_transId and i_baseCurrency.   |
//          |            | Added i_appliedCurrency.        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Payment History Detail screen.
 */
public class CcPaymentHistoryDetailScreenData extends CcRefillHistoryDetailScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Type of payment. */
    private String               i_type                  = null;

    /** The bank id. */
    private String               i_bankId                = null;

    /** The bank Name. */
    private String               i_bankName              = null;

    /** Additional Information line 1. */
    private String               i_info1                 = null;

    /** Additional Information line 2. */
    private String               i_info2                 = null;

    /** Additional Information line 3. */
    private String               i_info3                 = null;

    /** Additional Information line 4. */
    private String               i_info4                 = null;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     */
    public CcPaymentHistoryDetailScreenData( GuiContext p_guiContext,
                                             GuiRequest p_guiRequest)
    {
        super( p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** Gets Type of payment.
     * @return Type of payment.
     */
    public String getType()
    {
       return( i_type );
    }

    /** Sets Type of payment.
     * @param p_type Type of payment.
     */
    public void setType( String p_type)
    {
        i_type = p_type;
    }

    /** Gets the bank id.
     * @return Identifier of the bank where this payment was made.
     */
    public String getBankId()
    {
       return( i_bankId );
    }

    /** Sets the bank id.
     * @param p_bankId Identifier of the bank where this payment was made.
     */
    public void setBankId( String p_bankId)
    {
        i_bankId = p_bankId;
    }

    /** Gets the bank Name.
     * @return Name of the bank where this payment was made.
     */
    public String getBankName()
    {
       return( i_bankName );
    }

    /** Sets the bank Name.
     * @param p_bankName Name of the bank where this payment was made.
     */
    public void setBankName( String p_bankName)
    {
        i_bankName = p_bankName;
    }

    /** Gets Additional Information line 1.
     * @return Address line 1.
     */
    public String getInfo1()
    {
       return( i_info1 );
    }

    /** Sets Additional Information line 1.
     * @param p_info1 Address line 1.
     */
    public void setInfo1( String p_info1)
    {
        i_info1 = p_info1;
    }

    /** Gets Additional Information line 2.
     * @return Address line 2.
     */
    public String getInfo2()
    {
       return( i_info2 );
    }

    /** Sets Additional Information line 2.
     * @param p_info2 Address line 2.
     */
    public void setInfo2( String p_info2)
    {
        i_info2 = p_info2;
    }

    /** Gets Additional Information line 3.
     * @return Address line 3.
     */
    public String getInfo3()
    {
       return( i_info3 );
    }

    /** Sets Additional Information line 3.
     * @param p_info3 Address line 3.
     */
    public void setInfo3( String p_info3)
    {
        i_info3 = p_info3;
    }

    /** Gets Additional Information line 4.
     * @return Address line 4.
     */
    public String getInfo4()
    {
       return( i_info4 );
    }

    /** Sets Additional Information line 4.
     * @param p_info4 Address line 4.
     */
    public void setInfo4( String p_info4)
    {
        i_info4 = p_info4;
    }
    
} // end public class CcPaymentHistoryDetailScreenData
