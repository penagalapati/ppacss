////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerDetailsService.java
//      DATE            :       30-Jan-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1337/5342
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasCustomerDetailsService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CustomerDetailsData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasCustomerDetailsService;
import com.slb.sema.ppas.util.logging.Logger;

/** Gui Service that wrappers calls to PpasCustomerDetailsService. */
public class CcCustomerDetailsService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustomerDetailsService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasCustomerDetailsService i_customerDetailsService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcCustomerDetailsService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcCustomerDetailsService(GuiRequest p_guiRequest,
                                    long       p_flags,
                                    Logger     p_logger,
                                    GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_customerDetailsService = new PpasCustomerDetailsService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDetails = "getDetails";
    /**
     * Retrieves customer details data for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return A Customer details data response.
     */
    public CcCustomerDetailsDataResponse getDetails(GuiRequest p_guiRequest,
                                                    long       p_timeoutMillis)
    {
        CcCustomerDetailsDataResponse l_ccCustomerDetailsDataResponse  = null;
        GuiResponse                   l_guiResponse                    = null;
        CustomerDetailsData           l_customerDetailsData            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getDetails);
        }

        try
        {
            l_customerDetailsData = i_customerDetailsService.getDetails(p_guiRequest, p_timeoutMillis);

            l_ccCustomerDetailsDataResponse
                = new CcCustomerDetailsDataResponse(
                                  p_guiRequest,
                                  GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                  new Object[] {"get customer details"},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_customerDetailsData);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "getthe customer's details", l_pSE);

            l_ccCustomerDetailsDataResponse =
                new CcCustomerDetailsDataResponse(
                                          p_guiRequest,
                                          l_guiResponse.getKey(),
                                          l_guiResponse.getParams(),
                                          GuiResponse.C_SEVERITY_FAILURE,
                                          null);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getDetails);
        }

        return l_ccCustomerDetailsDataResponse;
    }  // end public CcCustomerDetailsDataResponse getDetails(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateDetails = "updateDetails";
    /**
     * Updates customer details data for the subscriber.
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_customerDetails Details of the customer to be updated.
     * @return A GUI response.
     */
    public GuiResponse updateDetails(GuiRequest          p_guiRequest,
                                     long                p_timeoutMillis,
                                     CustomerDetailsData p_customerDetails)
    {

        GuiResponse l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 72000, this,
                "Entered " + C_METHOD_updateDetails);
        }

        try
        {
            i_customerDetailsService.updateDetails(p_guiRequest, p_timeoutMillis, p_customerDetails);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().
                                getSelectedLocale(),  // PpacLon#1/17
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"update customer details"},
                              GuiResponse.C_SEVERITY_SUCCESS);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 72210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "update the customer's details", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 73000, this,
                "Leaving " + C_METHOD_updateDetails);
        }

        return l_guiResponse;

    }  // end public GuiResponse updateDetails(...)

} // end public class CcCustomerDetailsService
