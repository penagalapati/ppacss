////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcServiceFeeDeductionScreenData.java
//      DATE            :       07-Jul-2005
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_GEN_CA_48
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the main 
//                              Service Fee Deduction screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.dataclass.ServiceFeeData;
import com.slb.sema.ppas.common.dataclass.ServiceFeeDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;


/**
 * Gui Screen Data object for the Service Fee Deduction screen.
 */
public class CcServiceFeeDeductionScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** ServiceFeeDataSet object wrapped by this object. */
    private ServiceFeeDataSet i_serviceFeeDataSet;

    /** Holds the data for the Service Fee Deduction Detail Screen. */
    private CcServiceFeeDeductionDetailScreenData i_detailScreenData;

    /** Text giving info on when/whether an update was made on this screen. */
    private String i_infoText;

    /** Service Fee Deduction detail screen data. */
    private CcServiceFeeDeductionDetailScreenData i_DetailScreenData = null;

    /** Service Fee Deduction rows stored on the svra_adj_codes table. */
    private SrvaAdjCodesDataSet i_adjCodes = null;
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcServiceFeeDeductionScreenData";

    /** Displayed by GUI if Service Fee Deduction type is not stored in svra_adj_codes. */
    private static final String C_UNDEFINED_SFD  = "Undefined";
    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** 
     * Simple constructor.
     * @param p_guiContext GuiContext containing configuration
     * @param p_guiRequest GuiRequest
     */
    public CcServiceFeeDeductionScreenData(GuiContext p_guiContext,
                                        GuiRequest p_guiRequest )
    {
        super(p_guiContext, p_guiRequest);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 95000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        i_infoText = "";

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 95010, this,
                           "Constructed " + C_CLASS_NAME );
        }
    }

    //--------------------------------------------------------------------------
    // Public methods.
    //--------------------------------------------------------------------------

    /** 
     * Sets the textual information on the status of the Service Fee Deduction operation attempted.
     * @param p_text test to be displayed
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** 
     * Gets the textual information on the status of the Service Fee Deduction operation attempted.
     * @return test to be displayed
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /**
     * Sets the Service Fee Deduction data set.
     * @param p_serviceFeeDataSet set containing Service Fee Deduction values
     */
    public void setServiceFeeDataSet(ServiceFeeDataSet p_serviceFeeDataSet)
    {
        i_serviceFeeDataSet = p_serviceFeeDataSet;
    }

    /**
     * Gets the Service Fee Deduction data set.
     * @return data set containing Service Fee Deduction values
     */
    public ServiceFeeDataSet getServiceFeeDataSet()
    {
        return i_serviceFeeDataSet;
    }

    /**
     * Calls the method on the ServiceFeeDataSet (stored on the screen data object) that returns
     * an entire list of ServiceFeeData. This data is itself returned from this method.
     * @return The entire list of ServiceFeeData.
     */
    public ServiceFeeData[] getServiceFeeDataArr()
    {
        return i_serviceFeeDataSet.getServiceFeeDataArr();
    } // end of getServiceFeeDataArr

    /**
     * Calls the method on the ServiceFeeDataSet (stored on the screen data object) that returns
     * a subset of the most recent ServiceFeeData. This data is itself returned from this method.
     * @return The subset of the most recent ServiceFeeData.
     */
    public ServiceFeeData[] getLatestServiceFeeDataArr()
    {
        return i_serviceFeeDataSet.getLatestServiceFeeDataArr();
    } // end of getServiceFeeDataArr

    /** Sets the Detail Screen Data.
     * @param p_detailScreenData Detail Screen Data.
     */
    public void setDetailScreenData(CcServiceFeeDeductionDetailScreenData p_detailScreenData)
    {
        i_detailScreenData = p_detailScreenData;
    }

    /**
     * Returns the Detail Screen Data.
     * @return Detail Screen Data.
     */
    public CcServiceFeeDeductionDetailScreenData getDetailScreenData()
    {
        return( i_detailScreenData );
    }

    /** Sets the Sdf Adj Codes.
     * @param p_adjCodes Sdf Adj Codes.
     */
    public void setAdjCodes(SrvaAdjCodesDataSet p_adjCodes)
    {
        i_adjCodes = p_adjCodes;
    }

    /**
     * Returns the description of the Service Fee Deduction given the type. It uses information from the
     * svra_adj_codes cache. Returns empty string if type is null, i.e. type was not set by RDRP. Returns
     * the 'undefined' description if type was not found on the svra_adj_codes table.
     * @param p_serviceFeeData Service Fee Deduction
     * @return Description of Service Fee Deduction
     */
    public String getDescription(
        ServiceFeeData                p_serviceFeeData)
    {
        SrvaAdjCodesData l_adjCode = i_adjCodes.getAdjustmentData(p_serviceFeeData.getAdjustmentType(),
            p_serviceFeeData.getAdjustmentCode());

        return l_adjCode == null ? C_UNDEFINED_SFD : l_adjCode.getAdjDesc();
    } // end of getDescription
} // end of CcServiceFeeDeductionScreenData class
