////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcStatementOfAccountService.Java
//DATE            :       30-May-2007
//AUTHOR          :       Andy Harris
//REFERENCE       :       PpacLon#3124/11533
//                        PRD_ASCS00_GEN_CA_114
//
//COPYRIGHT       :       WM-data 2007
//
//DESCRIPTION     :       Gui Service that wraps calls to
//                        PpasStatementOfAccountService.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//         |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.StatementOfAccountDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasStatementOfAccountService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Response returned by a GUI service method that contains a 
 * statement of Account object.
 */
public class CcStatementOfAccountService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcStatementOfAccountService";

    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasStatementOfAccountService i_statementOfAccountService;

    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcStatementOfAccountService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcStatementOfAccountService(GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 22000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_statementOfAccountService = new PpasStatementOfAccountService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 22010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSOADetails = "getStatementOfAccountDetails";
    /**
     * Retrieves statement of account data for the subscriber.
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_startDate
     * @param p_endDate
     * @return Set of Statement of Account data.
     */
    public CcStatementOfAccountResponse getStatementOfAccountDetails(GuiRequest p_guiRequest,
                                                       long       p_timeoutMillis,
                                                       PpasDate   p_startDate,
                                                       PpasDate   p_endDate
                                                       )
    {
        CcStatementOfAccountResponse l_ccStatementOfAccountResponse =null;
        GuiResponse                  l_guiResponse;
        StatementOfAccountDataSet    l_statementOfAccountDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getSOADetails);
        }

        try
        {
            l_statementOfAccountDataSet = i_statementOfAccountService.getStatementOfAccount(p_guiRequest,
                                                                                            p_timeoutMillis,
                                                                                            p_startDate,
                                                                                            p_endDate);

            l_ccStatementOfAccountResponse = new CcStatementOfAccountResponse(
                                             p_guiRequest,
                                             GuiResponse.C_KEY_SERVICE_SUCCESS,
                                             (Object[])null,
                                             GuiResponse.C_SEVERITY_SUCCESS,
                                             l_statementOfAccountDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 10210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get statement of account details", l_pSE);

            l_ccStatementOfAccountResponse = new CcStatementOfAccountResponse(
                                             p_guiRequest,
                                             l_guiResponse.getKey(),
                                             l_guiResponse.getParams(),
                                             GuiResponse.C_SEVERITY_FAILURE,
                                             null);
        }
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getSOADetails);
        }

        return l_ccStatementOfAccountResponse;
        
        }
}
