////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcRechargeDivisionDataResponse.java
//      DATE            :       2-Sep-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1513/6383
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Response returned by the GUI getDivision service
//                              method that is a container for DivisionData.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response returned by the GUI getDivision service method that contains an 
 * DivisionData object.
 */
public class CcRechargeDivisionDataResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcRechargeDivisionDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** DivisionData instance contained in this response. */
    private DivisionData i_divisionData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * Receives a DivisionData object and the additional necessary 
     * objects to create a response message indicating the status of a call to a
     * RechargeDivisionService method.
     *
     * @param p_guiRequest        The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_messageSeverity   Severity of response.
     * @param p_divisionData      The division information.
     */
    public CcRechargeDivisionDataResponse(
                                 GuiRequest           p_guiRequest,
                                 Locale               p_messageLocale,
                                 String               p_messageKey,
                                 Object[]             p_messageParams,
                                 int                  p_messageSeverity,
                                 DivisionData         p_divisionData )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_divisionData = p_divisionData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the DivisionData instance contained within
     * this response object.
     * @return Divisional split of the recharge.
     */
    public DivisionData getDivisionData()
    {
        return i_divisionData;

    }
}
