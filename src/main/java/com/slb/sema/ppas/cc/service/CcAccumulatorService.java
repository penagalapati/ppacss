////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccumulatorService.java
//      DATE            :       8-Oct-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1597/6736
//                              PRD_PPAK00_GEN_CA_396
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to 
//                              PpasAccumulatorService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccumulatorDataSet;
import com.slb.sema.ppas.common.dataclass.AccumulatorEnquiryResultData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAccumulatorService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with 
 * Accumulators. 
 */
public class CcAccumulatorService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Set to {@value} */
    private static final String C_CLASS_NAME = "CcAccumulatorService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The atomic accumulator service. */
    private PpasAccumulatorService i_accumulatorService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** 
     * Creates an instance of a CcAccumulatorService object that
     * can then be used to perform Accumulator services.
     * @param p_request     GUI request object.
     * @param p_logger      Logger.
     * @param p_guiContext  GUI context object.
     */
    public CcAccumulatorService( GuiRequest         p_request,
                                 Logger             p_logger,
                                 GuiContext         p_guiContext )
    {
        super( p_request, 0, p_logger );

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_request, 
                           C_CLASS_NAME, 
                           10000, 
                           this,
                           "Constructing " + C_CLASS_NAME );
        }
        
        i_accumulatorService = new PpasAccumulatorService(p_request, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_request, 
                           C_CLASS_NAME, 
                           10010, 
                           this,
                           "Constructed " + C_CLASS_NAME );
        }
    } 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_getAccumulatorBalances = "getAccumulatorBalances";
    /** 
     * Generates a CcAccumulatorDataSetResponse object for the selected 
     * customer.
     * @param p_request        GUI request object.
     * @param p_timeoutMillis  Request timeout in milliseconds.
     * @return Response to the request for accumulator details.
     */
    public CcAccumulatorDataSetResponse getAccumulatorBalances(GuiRequest p_request,
                                                               long       p_timeoutMillis )
    {
        GuiResponse                  l_guiResponse    = null;
        CcAccumulatorDataSetResponse l_accumResponse  = null;
        AccumulatorDataSet           l_accumDataSet   = null;
        AccumulatorEnquiryResultData l_accumEnqResultData = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_request, 
                           C_CLASS_NAME,
                           10020,
                           this,
                           "Entered " + C_METHOD_getAccumulatorBalances );
        }
        
        try
        {
            l_accumEnqResultData = i_accumulatorService.getAccumulatorBalances(p_request,
                                                                               p_timeoutMillis );

            l_accumDataSet = l_accumEnqResultData.getAccuDataSet();
            
            l_accumResponse = new CcAccumulatorDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(), 
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_accumDataSet );
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_request, 
                               C_CLASS_NAME, 
                               10030, 
                               this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse = handlePpasServiceException( p_request, 0, "get accumulator balances", l_pSE );

            l_accumResponse = new CcAccumulatorDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null );
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_request, 
                           C_CLASS_NAME,
                           10040, 
                           this,
                           "Leaving " + C_METHOD_getAccumulatorBalances);
        }
        
        return ( l_accumResponse );
    } 

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_updateAccumulator = "updateAccumulator";
    /**
     * Posts an adjustment to the balance and/or start date of the requested accumulator.
     * @param p_guiRequest        GUI request object.
     * @param p_timeoutMillis     Request timeout in milliseconds.
     * @param p_accumId           Id of the accumulator to be updated.
     * @param p_adjustmentAmount  Amount to be added to the accumulator balance.  Set to zero if no
     *                            adjustment has been made to the balance.
     * @param p_accumStartDate    New accumulator start date.  Set to null if the date has not been changed.
     * @return Response indicating success or failure of the request.
     */
    public GuiResponse updateAccumulator(GuiRequest p_guiRequest,
                                         long       p_timeoutMillis,
                                         int        p_accumId,
                                         int        p_adjustmentAmount,
                                         PpasDate   p_accumStartDate)
    {
        GuiResponse  l_guiResponse = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10050, 
                           this,
                           "Entered " + C_METHOD_updateAccumulator);
        }

        try
        {
            i_accumulatorService.updateAccumulatorBalance( p_guiRequest,
                                                           p_timeoutMillis,
                                                           null,                //Service Class
                                                           p_accumId,
                                                           p_accumStartDate,
                                                           p_adjustmentAmount );

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {"update accumulator"},
                                            GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, 
                               C_CLASS_NAME, 
                               10060, 
                               this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "update accumulator", l_pSE);
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10070, 
                           this,
                           "Leaving " + C_METHOD_updateAccumulator);
        }

        return (l_guiResponse);
    }

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_clearAccumulators = "clearAccumulators";
    /**
     * Clears the balance of one or more accumulators.
     * @param p_guiRequest      GUI request object.
     * @param p_timeoutMillis   Request timeout in milliseconds.
     * @param p_accumIds        Array of accumulator IDs to have their balance cleared.
     * @return Response indicating success or failure of the request.
     */
    public GuiResponse clearAccumulators(GuiRequest p_guiRequest,
                                         long       p_timeoutMillis,
                                         int[]      p_accumIds)
    {
        GuiResponse  l_guiResponse = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10050, 
                           this,
                           "Entered " + C_METHOD_clearAccumulators);
        }

        try
        {
            i_accumulatorService.clearAccumulators( p_guiRequest,
                                                    p_timeoutMillis,
                                                    null,       // Service Class
                                                    p_accumIds );

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {"clear accumulator balances"},
                                            GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, 
                               C_CLASS_NAME, 
                               10060, 
                               this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse = handlePpasServiceException(p_guiRequest,
                                                       0,
                                                       "clear accumulator balances",
                                                       l_pSE);
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10070, 
                           this,
                           "Leaving " + C_METHOD_clearAccumulators);
        }

        return (l_guiResponse);
    }
}
