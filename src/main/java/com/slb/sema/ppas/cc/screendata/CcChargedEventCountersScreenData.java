////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcChargedEventCountersScreenData.java
//      DATE            :       08-Jun-2005
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1560/6587
//                              PRD_ASCS_GEN_CA_49
//
//      COPYRIGHT       :       WM-Data 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the Charged
//                              Event Counters
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.dataclass.AccumulatorEnquiryResultData;
import com.slb.sema.ppas.common.dataclass.ChargedEventCounterData;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Charged Event Counters screen.
 */
public class CcChargedEventCountersScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    
    /** Text information to be displayed on the GUI screen. */
    private String i_infoText;
    
    /** Data set containing the available event counter types. */
    private MiscCodeDataSet i_eventCounterTypes;
    
    /** Details for each event counter. */
    private AccumulatorEnquiryResultData i_accumulatorEnquiryResultData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcChargedEventCountersScreenData(GuiContext p_guiContext,
                                            GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);

    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------
    
    /**
     * Sets the info text to display the success of a request on the GUI.
     * @param p_infoText The text to be displayed on the GUI.
     */
    public void setInfoText(String p_infoText)
    {
        i_infoText = p_infoText;
    }
    
    /**
     * Retrieve the info text to be displayed on the GUI to report on the success of a request.
     * @return Text information to be displayed on the GUI.
     */
    public String getInfoText()
    {
        return (i_infoText != null ? i_infoText : "");
    }
    
    /**
     * Sets the <code>AccumulatorEnquiryResultData</code> to be displayed on the GUI. 
     * @param p_accumulatorEnquiryResultData Data required for the GUI.
     */
    public void setAccumulatorEnquiryResultData(AccumulatorEnquiryResultData p_accumulatorEnquiryResultData)
    {
        i_accumulatorEnquiryResultData = p_accumulatorEnquiryResultData;
        
    }
    
    /**
     * Returns a subscribers event counter data.
     * @return An array of <code>ChargedEventCounterData</code>
     */
    public ChargedEventCounterData[] getCounterDetails()
    {
        return (i_accumulatorEnquiryResultData != null) ? 
                   i_accumulatorEnquiryResultData.getChargedEventCountersDataSet().asArray() :
                       new ChargedEventCounterData[0];
    }
    
    /** 
     * Returns <code>true</code> if the Operator is allowed to update
     * the Charged Event Counter details.  Otherwise <code>false</code> is returned.
     * 
     * @return True if the operator can update operatot details, otherwise, false,
     */
    public boolean eventCounterUpdateIsPermitted()
    {
        return (i_gopaData.eventCountersUpdateIsPermitted());
    }


}
