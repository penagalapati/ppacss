////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDisconnectScreenData.java
//      DATE            :       28-Mar-2002
//      AUTHOR          :       Remi Isaacs
//      REFERENCE       :       PpaLon#1341/5352
//                              PRD_PPAK00_DEV_IN_035
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Disconnect screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//24-Apr-07 | S James    | Changes for Account Reconnect   | PpacLon#3072/11370
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonData;
import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Disconnect screen.
 */
public class CcDisconnectScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** The array of disconnect Reasons. */
    private DireDisconnectReasonData [] i_discReasons;
   

    /** DisconnectData object wrapped by this object. */
    private DisconnectData i_disconnectData = null;

    /** Flag to indicate whether subscription is Disconnected. */
    private boolean i_isDisconnected = false;

    /** Flag to indicate whether subscription is scheduled for future disconnection. */    
    private boolean i_isFutureDisconnected;

    /** Flag to indicate whether future-scheduled disconnection can be cancelled. */    
    private boolean i_canCancelFutureDisconnect = true;

    /** End Service Date of subscription. */
    private PpasDate  i_endServiceDate = null;

    /** Text giving information on the status of any update that was attempted. */
    private String  i_infoText = "";

    /** Flag to indicate account type either Master,Standalone, or Subordinate. */
    private String i_accountType = null;
    
    /** Flag to indicate that the Account Reconnection feature licence is installed */
    private boolean i_hasAccountReconnectFeatureLicence = false;
    
    /** Flag to indicate reconnection is in progress */
    private boolean i_isReconnectInProgress = false;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcDisconnectScreenData(GuiContext p_guiContext,
                                  GuiRequest   p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }
    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** Sets the DisconnectData object wrapped  within this object.
     * 
     * @param p_disconnectData Disconect data.
     */ 
    public void setDisconnectDetails( DisconnectData p_disconnectData)
    {
        i_disconnectData = p_disconnectData;
    }

    /** Sets flag to indicate whether account is disconnected.
     * 
     * @param p_isDisconnected Flag indicating whether the subscriber is disconnected.
     */
    public void setIsDisconnected( boolean p_isDisconnected)
    {
        i_isDisconnected = p_isDisconnected;     
    }

    /** Get flag to indicate whether account is disconnected.
     * 
     * @return True if the subscriber is disconnected, otherwise, false.
     */
    public boolean isDisconnected()
    {
        return i_isDisconnected;
    } 

    /** Sets flag to indicate whether account is disconnected 
     *  in future.
     * 
     * @param p_isFutureDisconnected Flag indicating whether the subscriber should be disconnected in the
     *          future.
     */
    public void setIsFutureDisconnected(boolean p_isFutureDisconnected)
    {
        i_isFutureDisconnected = p_isFutureDisconnected;
    }

    /** Get flag to indicate whether account will be disconnected
     *  in future.
     * 
     * @return True if the subscriber is marked for future disconnection.
     */
    public boolean isFutureDisconnected()
    {  
        
        return i_isFutureDisconnected;
    } 

    /** 
     * Sets flag to indicate whether future-scheduled disconnection can be cancelled.
     * @param p_canCancelFutureDisconnect True if future-scheduled disconnection can be cancelled.
     */
    public void setCanCancelFutureDisconnect(boolean p_canCancelFutureDisconnect)
    {
        i_canCancelFutureDisconnect = p_canCancelFutureDisconnect;
    }

    /** 
     * Get flag to indicate whether future-scheduled disconnection can be cancelled.
     * @return True if future-scheduled disconnection can be cancelled.
     */
    public boolean canCancelFutureDisconnect()
    {  
        return i_canCancelFutureDisconnect;
    } 

    /**
     * Sets the available Disconnect Reasons 
     * String array of DisconnectReasons.
     * 
     * @param p_discReasons Array of disconnect reasons.
     */    
    public void setDiscReasons(DireDisconnectReasonData [] p_discReasons )
    {
        i_discReasons = p_discReasons;       
    }

    /**
     * Get the datetime when account was disconnected.
     * 
     * @return String representing the date/time the subscriber was disconnected.
     */
    public String getDisconnectDateTime()
    {
        return( i_disconnectData.getDisconnectDateTime().toString());
    }

    /**
     * Get date for future disconnection.
     * 
     * @return String representing the date/time the subscriber should be disconnected.
     */
    public String getFutureDisconnectDate()
    {
        return( i_disconnectData.getDisconnectEndServDate().toString());
    }

    /**
     * Get the OPID of the user who disconnected the subscriber.
     * 
     * @return Identifier of the operator that disconnected the subscriber.
     */
    public String getDisconnectBy()
    {
        return( i_disconnectData.getOpid().toString());
    }

    /**
     * Get the Reason Code associated with the disconnected account.
     * 
     * @return Reason for disconnection.
     */
    public String getDisconnectCode()
    {
        return ( i_disconnectData.getDisconnectReason().toString()) ;
    }

    /**
     * Get the Reason Code description associated with the disconnected account.
     * 
     * @return Description of the discobnnection reason.
     */
    public String getDisconnectDesc()
    {
        int l_index;
        String l_temp = "";

        for (l_index = 0; l_index < i_discReasons.length; l_index++)
        {
            if (this.getDisconnectCode().equals(i_discReasons[l_index].getDireReason()))
            {
                l_temp = i_discReasons[l_index].getDireDesc();
                break;
            }
        }
        return (l_temp);
    }

    /** Sets the text giving information on the status of any Disconnection
     *  that was attempted.
     * 
     * @param p_text Information regarding the status of a disconnection attempt.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving information on the status of any disconnection
     *  that was attempted.
     * 
     * @return Information regarding the status of a disconnection attempt.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Set End Service Date for Account.
     * 
     * @param p_endServiceDate End date for the account.
     */
    public void setEndServiceDate(PpasDate p_endServiceDate)
    {
        i_endServiceDate = p_endServiceDate;
    }

    /** Get the End Service Date.
     * 
     * @return End date for the account.
     */
    public String getEndServiceDate()
    {
        String l_finalDate;

        if ( i_endServiceDate == null)
        {
            l_finalDate = "";
        }
        else
        {
            l_finalDate = i_endServiceDate.toString();
        }

        return (l_finalDate);
    } 

    /**  Set the Account type - Master, Standalone,Subordinate.
     * 
     * @param p_accountType Type of account.
     */
    public void setAccountType(String p_accountType)
    {
        i_accountType = p_accountType;
    }

    /** Get the Account type - Master, Standalone, Subordinate.
     * 
     * @return Type of account.
     */
    public String getAccountType()
    {
        return i_accountType;
    }

    /**
     * Returns an array of available Disconnect Reasons Description which 
     * are configured in the database. The customer's preferred reason
     * description will be the first element in the array. 
     * If the reason description is not selected on immediate or future
     * disconnection the order of the array will be the same as configured in 
     * the database.
     * 
     * @return Array of disconnect descriptions.
     */
    public String [] getDiscReasons()
    {
        Vector                      l_discReasonV = new Vector (20, 10);
        String                   [] l_discStringArr;
        String                      l_discReason = "";

        l_discReason = i_disconnectData.getDisconnectReason();
        
        // Set first element of Vector as Customer chosen disconnection Reason
        for (int i_index = 0; i_index < i_discReasons.length; i_index++)
        {
            if ( l_discReason
                 .equals(i_discReasons[i_index].getDireReason()))
            {                 
                l_discReasonV.addElement(i_discReasons[i_index]
                                          .getDireReason() + "  - " +
                                          i_discReasons[i_index].getDireDesc());
                               
            }
        }
        
        for (int i_index = 0; i_index < i_discReasons.length; i_index++)
        {
            if (!(l_discReason.
                  equals(i_discReasons[i_index].getDireReason())))
            {
                l_discReasonV.addElement(i_discReasons[i_index]
                             .getDireReason() + "  - " +
                              i_discReasons[i_index].getDireDesc());                   
            }
                      
        }                               

        l_discStringArr = new String [l_discReasonV.size()];
        l_discStringArr = (String []) l_discReasonV.toArray (l_discStringArr);

        return ( l_discStringArr );    
    }

    /**
     * Returns an array of available Disconnect Reasons Codes which 
     * are configured in the database. The customer's preferred reason
     * Codes will be the first element in the array. 
     * If the reason Code is not selected on immediate or future
     * disconnection the order of the array will be the same as configured in 
     * the database.
     * 
     * @return Array of disconnect codes.
     */    
    public String [] getDiscCodes()
    {
        Vector                      l_discCodeV   = new Vector (20, 10);
        String                   [] l_discCodeArr;
        String                      l_discReason;
       
        l_discReason = i_disconnectData.getDisconnectReason();
                                                
        // Set first element of Vector as Customer chosen disconnection Reason
        for (int i_index = 0; i_index < i_discReasons.length; i_index++)
        {                
            if ( l_discReason.equals(i_discReasons[i_index].getDireReason()))
            {                 
                l_discCodeV.addElement(i_discReasons[i_index].getDireReason());            
            }
        }
        
        for (int i_index = 0; i_index < i_discReasons.length; i_index++)
        {
            if (!(l_discReason.
                   equals(i_discReasons[i_index].getDireReason())))
            {
                l_discCodeV.addElement(i_discReasons[i_index].getDireReason());                      
            }
                          
        }                                
        l_discCodeArr = new String [l_discCodeV.size()];
        l_discCodeArr = (String [])l_discCodeV.toArray (l_discCodeArr);

        return ( l_discCodeArr );    
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * 
     * @return True if this object is fully populated, otherwise, false.
     */
    public boolean isPopulated()
    {
       return ((i_disconnectData != null) &&
               (i_accountType != null)) ;
    }
    
    /**
     * Returns true if the Account Reconnection feature licence is installed
     * @return True if the Account Reconnection feature licence is installed,
     *         otherwise, false.
     */
    public boolean isAccountReconnectFeatureActive()
    {
        return i_hasAccountReconnectFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setAccountReconnectFeatureLicence(boolean p_isLicenced)
    {
        i_hasAccountReconnectFeatureLicence = p_isLicenced;
        return;
    }

    /**
     * Returns <code>true</code> if the Operator is permitted to perform
     * Account Reconnection.
     * Otherwise returns <code>false</code>/
     * @return True if the operator can perform an Account Reconnection.
     */
    public boolean isAccountReconnectionPermitted()
    {
        return (i_gopaData.isAccountReconnectionPermitted());
    }

    /**
     * Sets a boolean value indicating whether a reconnection is in progress.
     * @param p_isReconnectinProgress True if the reconnection is in progress.
     */
    public void setIsReconnectInProgress(boolean p_isReconnectinProgress)
    {
        i_isReconnectInProgress = p_isReconnectinProgress;
        return;
    }

    /**
     * Returns true if the Account Reconnection is in progress
     * @return True if the Account Reconnection is in progress,
     *         otherwise, false.
     */
    public boolean isReconnectInProgress()
    {
        return i_isReconnectInProgress;
    }

}
