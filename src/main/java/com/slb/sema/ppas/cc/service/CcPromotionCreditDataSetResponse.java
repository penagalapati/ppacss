////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromotionCreditDataSetResponse.java
//      DATE            :       12-Mar-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Response returned by the Customer Care
//                              Promotion Credit service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.PromotionCreditDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response containing details of a customer's promotion credits if
 * these details were successfully obtained by the Customer Care
 * Promotion Credit service. Otherwise, the key in the GuiResponse
 * super class defines the reason for the failure of the service.
 */
public class CcPromotionCreditDataSetResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME =
                                            "CcPromotionCreditDataSetResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Data object containing details of a customer's promotion allocations.
     */
    private PromotionCreditDataSet i_promCredits;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /**
     * Construct a CcPromotionCreditDataSetResponse using the default locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_promCredits   Set of promotional credits.
     */
    public CcPromotionCreditDataSetResponse(
        GuiRequest                p_guiRequest,
        String                    p_messageKey,
        Object []                 p_messageParams,
        int                       p_messageSeverity,
        PromotionCreditDataSet    p_promCredits)
    {
        this (p_guiRequest,
              null,
              p_messageKey,
              p_messageParams,
              p_messageSeverity,
              p_promCredits);
    }

    /**
     * Construct a CcPromotionCreditDataSetResponse using the given locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_promCredits   Set of promotional credits.
     */
    public CcPromotionCreditDataSetResponse(
        GuiRequest               p_guiRequest,
        Locale                   p_messageLocale,
        String                   p_messageKey,
        Object[]                 p_messageParams,
        int                      p_messageSeverity,
        PromotionCreditDataSet   p_promCredits)
    {
        super (p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 55000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_promCredits = p_promCredits;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 55090, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // end constructor


    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the promotion credits data set object
     * instance contained within this response.
     * @return Set of promotional credits.
     */
    public PromotionCreditDataSet getDataSet()
    {
        return (i_promCredits);
    }

} // end class CcPromotionCreditDataSetResponse
