////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcMsisdnRoutingScreenData.java
//DATE            :       26-Oct_2004
//AUTHOR          :       Kevin Tongue
//
//COPYRIGHT       :       
//
//DESCRIPTION     :       Gui Screen Data object for the MSISDN Routing screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//  |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the MSISDN Routing screen.
 */
public class CcMsisdnRoutingScreenData extends GuiScreenData
{
    
    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------
    /** Flag to indicate if number range routing is cinfigured */
    
    public static final String C_NBR_ROUTING = "NBR";
    
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    
    /** ScpId used for an add routing operation.  */
    private String i_inId ;
    
    /** A set of IN Ids . */
    private ScpiScpInfoDataSet i_inIds = null;
    
    /** The MSISDN used for an add routing operation. */
    private Msisdn i_addMsisdn = null;
    
    /** The MSISDN used for a delete routing operation. */
    private Msisdn i_delMsisdn = null;
    
    /** Text to indicate status of last attempted update. */
    private String i_infoText = "";
    
    /** Request failure flag. **/
    private boolean i_failedRequest = false;
    
    /** The routing method used - either Number range (NBR) or SDP.  */
    private String i_routingMethod; 
    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** Simple constructor.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     */
    public CcMsisdnRoutingScreenData(GuiContext p_guiContext,
                                     GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }
    
    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------
    
    /**
     * Returns <code>true</code> if the Operator generating the request
     * is permitted to specify IN ID on install.
     * Otherwise returns <code>false</code>.
     * @return True if the operator can install.
     */
    public boolean routingScreenAccessAllowed()
    {
        //KK change this to return the correct values once gopa table etc. has been updated.
        return (i_gopaData.hasRoutingScreenAccess());
    }
    
    /** Returns a set of IN Ids.
     * @return Set of SDP data.
     */
    public ScpiScpInfoData[] getInIds()
    {
        return i_inIds.getAvailableArray();
    }
    
    /** Sets the SCP Id data.
     * @param p_inIds Set of SDP data.
     */
    public void setInIds(ScpiScpInfoDataSet p_inIds)
    {
        i_inIds = p_inIds;
    }
    
    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if all relevant data is present.
     */
    public boolean isPopulated()
    {
        return true;
    }
    
    /** Returns the MSISDN used for an add routing operation.
     * @return Subscribers mobile number.
     */
    public Msisdn getAddMsisdn()
    {
        return i_addMsisdn;
    }
    
    /** Stores the MSISDN used for an add routing operation
     * @param p_addMsisdn Mobile number.
     */
    public void setAddMsisdn(Msisdn p_addMsisdn)
    {
        i_addMsisdn = p_addMsisdn;
    }
    
    /** Returns the MSISDN used for a delete routing operation.
     * @return Subscribers mobile number.
     */
    public Msisdn getDelMsisdn()
    {
        return i_delMsisdn;
    }
    
    /** Stores the MSISDN used for a delete routing operation.
     * @param p_delMsisdn Mobile number.
     */
    public void setDelMsisdn(Msisdn p_delMsisdn)
    {
        i_delMsisdn = p_delMsisdn;
    }
    
    /** Set text to indicate status of last attempted update.
     * @param p_infoText Status of last attempted update.
     */
    public void setInfoText(String p_infoText)
    {
        i_infoText = p_infoText;
    }
    
    /** Get text to indicate status of last attempted update.
     * @return Status of last attempted update.
     */
    public String getInfoText()
    {
        return i_infoText;
    }
    
    /** Set the request to update routing infformation to failed.
     * @param p_failedRequest Set the status to failed.
     */
    public void setFailedRequest(boolean p_failedRequest)
    {
        i_failedRequest = p_failedRequest;
    }
    
    /** Get failed state of installed request.
     * @return Failed status.
     */
    public boolean getFailedRequest()
    {
        return i_failedRequest;
    }
    
    /** Sets ScpId for failed routing.
     * @param p_inId SDP identifier.
     */
    public void setInId(String p_inId)
    {
        i_inId = p_inId;
    }
    
    /** Returns ScpId for failed routing.
     * @return SDP identifier.
     */
    public String getInId()
    {
        return i_inId;
    }
    
    /** Set the routing method 
     * @param p_routingMethod
     */
    public void setRoutingMethod(String p_routingMethod)
    {
        i_routingMethod = p_routingMethod;
        
    }
    /** Get the routing method
     * @return routing method
     */
    public String getRoutingMethod()
    {
        return i_routingMethod;
    }
    /** Get the routing method
     * @return routing method
     */
    
    public boolean sdpEntryRequired()
    {
        if (getRoutingMethod().equals(C_NBR_ROUTING))
        {
            return false;
        }
        return true;
    }
    
}