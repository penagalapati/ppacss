////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromotionCreditService.java
//      DATE            :       12-Mar-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Customer Care Service that wrappers calls to
//                              the PpasPromotionCreditService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.PromotionCreditDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasPromotionCreditService;
import com.slb.sema.ppas.util.logging.Logger;

/** Customer care service that wraps the Ppas Promotion Credit Internal
 *  service. This service is used to obtain a history of the promotional
 *  credits that a subscriber has received.
 */
public class CcPromotionCreditService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPromotionCreditService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Promotion Credit internal service used by this Gui Service. */
    private PpasPromotionCreditService i_promCreditIS;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the Customer Care Promotion Credit Service.
     *  This service is used to obtain subscriber's promotional credit history.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcPromotionCreditService(
        GuiRequest p_guiRequest,
        long       p_flags,
        Logger     p_logger,
        GuiContext p_guiContext)
    {
        super (p_guiRequest,
               p_flags,
               p_logger,
               p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 29000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create Promotion Credit internal service to be used by this service.
        i_promCreditIS = new PpasPromotionCreditService
                   (p_guiRequest,
                    p_logger,
                    p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 29090, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPromotionCredits =
                                                      "getPromotionCredits";
    /**
     * Get the promotional credit history for the subscriber defined in the
     * input request.
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Time in milliseconds to wait for resources
     *                        required to perform the service to become
     *                        available before giving up.
     * @return A response object containing the data set of details of the
     *         promotion credits received be the subscriber if the service
     *         completed successfully. If the service was not succesful, then
     *         the key in this response defines the reason for failure.
     *
     */
    public CcPromotionCreditDataSetResponse getPromotionCredits(
        GuiRequest p_guiRequest,
        long       p_timeoutMillis)
    {
        GuiResponse                      l_guiResponse         = null;
        CcPromotionCreditDataSetResponse l_promCreditsResponse = null;
        PromotionCreditDataSet           l_promCreditsDataSet  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 29100, this,
                "Entered " + C_METHOD_getPromotionCredits);
        }

        try
        {
            l_promCreditsDataSet =
                i_promCreditIS.getPromotionCredits(p_guiRequest, p_timeoutMillis);

            l_promCreditsResponse = new CcPromotionCreditDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(), // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_promCreditsDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 29110, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(
                         p_guiRequest,
                         0,
                         "get all the subscriber's promotional credits",
                         l_pSE);

            l_promCreditsResponse = new CcPromotionCreditDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 29190, this,
                "Leaving " + C_METHOD_getPromotionCredits);
        }

        return (l_promCreditsResponse);

    }  // end method getPromotionCredits

} // end public class CcPromotionCreditService
