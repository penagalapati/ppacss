////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcMsisdnRoutingServlet.Java
//DATE            :       26th October 2004
//AUTHOR          :       Kevin Tongue
//REFERENCE       :       PRD_ASCS00_GEN_CA_37
//
//COPYRIGHT       :       
//
//DESCRIPTION     :       Servlet to to handle requests from the
//                      MSISDN Routing screen.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//  |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcMsisdnRoutingScreenData;
import com.slb.sema.ppas.cc.service.CcMsisdnRoutingService;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.MarketSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/** This Servlet processes requests from the Msisdn Routing JSP
 *  script. These requests specify  new SDP routing information to
 *  be added for a particular MSISDN, or details of an MSISDN for 
 *  which existing routing information is to be deleted.
 */
public class CcMsisdnRoutingServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcMsisdnRoutingServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** The CC GUI MSISDN Routing Service to be used by this servlet. */
    private CcMsisdnRoutingService i_ccMsisdnRoutingService = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor - creates an MSISDN Routing Servlet instance to handle
     *  requests from the MSISDN Routing Screen.
     */
    public CcMsisdnRoutingServlet()
    {
        super();
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 45000, this,
                           "Constructing " + C_CLASS_NAME );
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 45090, this,
                           "Constructed " + C_CLASS_NAME );
        }
    } // end Constructor CcMsisdnRoutingServlet
    
    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    
    /** Performs initialisation specific to this Servlet; currently there is
     *  no specific initialisation for this Servlet.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           C_CLASS_NAME, 45200, this,
                           "Entered " + C_METHOD_doInit);
        }
        
        // Should probably be constructing CcMsisdnRoutingService here...
        // unfortunately no GuiRequest is available.
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           C_CLASS_NAME, 45290, this,
                           "Leaving " + C_METHOD_doInit);
        }
        
        return;
        
    } // end doInit
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to handle requests and responses to and 
     *  from the MSISDN Routing JSP.
     * @param p_request      HTTP request.
     * @param p_response     HTTP response.
     * @param p_httpSession  Current HttpSession.
     * @param p_guiRequest   GuiRequest object.
     * @return forwarding URL.
     */
    protected String doService( HttpServletRequest     p_request,
                                HttpServletResponse    p_response,
                                HttpSession            p_httpSession,
                                GuiRequest             p_guiRequest)
    {
        String                             l_command     = null;
        String                             l_forwardUrl  = null;
        CcMsisdnRoutingScreenData          l_screenData  = null;
        GuiResponse                        l_guiResponse = null;
        GuiSession                         l_guiSession;
        boolean                            l_accessGranted = false;
        MarketSet                          l_marketSet; 
        Market                             l_market;
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 45300, this,
                           "ENTERED " + C_METHOD_doService);
        }
        
        // Construct CcMsisdnRoutingService if it does not already exist.
        if (i_ccMsisdnRoutingService == null)
        {
            i_ccMsisdnRoutingService =
                new CcMsisdnRoutingService (p_guiRequest,
                                            0L,
                                            i_logger,
                                            i_guiContext);
        }

        //      Set up the Default forward URL.
        l_forwardUrl = "/jsp/cc/ccmsisdnrouting.jsp";

        //      Create screen data object.
        l_screenData = new CcMsisdnRoutingScreenData(i_guiContext, p_guiRequest);
        
        
        try
        {
            l_guiSession = (GuiSession)p_guiRequest.getSession();
            l_marketSet  = ((PpasSession)l_guiSession).getCsoMarkets();
            l_market = (Market)l_marketSet.getMarkets().elementAt(0);
            l_guiSession.setCsoMarket (l_market);
                        
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0L,
                                      p_request,
                                      "p_command",
                                      true,           // Mandatory ...
                                      null,           // ... so no default
                                      new String []{
                                                    "GET_SCREEN_DATA",
                                                    "ADD_ROUTING",
                                                    "DELETE_ROUTING"});
            
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_MODERATE,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    C_CLASS_NAME, 40010, this,
                    "Got command, command = [" + l_command + "]");
            }

           // Determine if the CSO has privilege to access the requested screen or function
            l_accessGranted = l_screenData.routingScreenAccessAllowed();
                        
            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_screenData.addRetrievalResponse(p_guiRequest,
                                                  l_guiResponse);

                l_forwardUrl = new String("/jsp/cc/ccaccesserror.jsp");

                p_request.setAttribute("p_guiScreenData",
                                       l_screenData);
            }
            else
            {
                if (l_command.equals("ADD_ROUTING"))
                {
                    setIsUpdateRequest(true);

                    addMsisdnRouting(p_guiRequest,
                                     p_request,
                                     l_screenData);                    
                }

                else if (l_command.equals("DELETE_ROUTING"))
                {
                    setIsUpdateRequest(true);

                    deleteMsisdnRouting(p_guiRequest,
                                        p_request,
                                        l_screenData);                    
                }

                /* Get the list of availabe InIds */
                getData(p_guiRequest, l_screenData);
            }
        }
        catch (PpasServletException l_servletE)
        {
            // Handle the general case of an invalid or missing parameter
            // in the incoming request
            l_guiResponse = handleInvalidParam(p_guiRequest, l_servletE);

            // Set up the error page as the forward URL and add the message to the
            // screendata object.
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

            l_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

            // Append the screen data object to the Http request.
            p_request.setAttribute("p_guiScreenData", l_screenData);
        }

        if (l_accessGranted)
        {
            // Append the screen data object to the Http request.
            p_request.setAttribute("p_guiScreenData", l_screenData);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 40020, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return(l_forwardUrl);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addMsisdnRouting = "addMsisdnRouting";
    /**
     * Strips out the relevant params, calls the add MSISDN routing service, and
     * populates the screendata object.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data for the Msisdn routing screen, on
     *                     which to put the response to the request.
     * @throws PpasServletException If an error occurs.
     */
    private void addMsisdnRouting(GuiRequest          p_guiRequest,
                                  HttpServletRequest  p_request,
                                  CcMsisdnRoutingScreenData p_screenData)
        throws PpasServletException
    {
        GuiResponse          l_guiResponse         = null;
        Msisdn               l_addMsisdn           = null;
        String               l_sdpId               = null;
        PpasDateTime         l_currentTime         = null;
                
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50000, this,
                "ENTERED " + C_METHOD_addMsisdnRouting);
        }

        // Get and validate that there is an msisdn in the request...
        l_addMsisdn = getValidMsisdn(p_guiRequest,
                                     0L,
                                     p_request,
                                     "p_addMsisdn",
                                     true,
                                     "",
                                     i_guiContext.getMsisdnFormatInput());

        // Put the msisdn obtained into the GUI request.
        p_guiRequest.setMsisdn(l_addMsisdn);

        // Stick the MSISDN onto the screen data object. 
        p_screenData.setAddMsisdn(l_addMsisdn);                             

        // Get the sdp id from the request
        l_sdpId = getValidParam(p_guiRequest,
                                PpasServlet.C_FLAG_ALLOW_BLANK,
                                p_request,
                                "p_inId",
                                false,             //won't be populated if number range routing configured
                                "",
                                PpasServlet.C_TYPE_ANY,
                                2,
                                C_OPERATOR_EQUALS);

        // Invoke the service method to install the subscriber.
        l_guiResponse = i_ccMsisdnRoutingService.addMsisdnRouting(p_guiRequest,
                                                                  i_timeout,
                                                                  l_addMsisdn,
                                                                  l_sdpId);

        // Add the reponse to the screen data object...
        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText(
                    "Failed Insert at " + l_currentTime);

            // Sst the update variables MSISDN and SPD ID so they can be re-displayed.
            p_screenData.setAddMsisdn(l_addMsisdn);
            p_screenData.setInId(l_sdpId);
            p_screenData.setFailedRequest(true);
        }
        else
        {
            p_screenData.setInfoText(
                    "Successful Insert at " + l_currentTime);
                                       
        }
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50020, this,
                "LEAVING " + C_METHOD_addMsisdnRouting);
        }

        return;
    }
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_deleteMsisdnRouting = "deleteMsisdnRouting";
    /**
     * Strips out the relevant params, calls the delete MSISDN routing service, and
     * populates the screendata object.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data for the Msisdn routing screen, on
     *                     which to put the response to the request.
     * @throws PpasServletException If an error occurs.
     */
    private void deleteMsisdnRouting(GuiRequest          p_guiRequest,
                                     HttpServletRequest  p_request,
                                     CcMsisdnRoutingScreenData p_screenData)
        throws PpasServletException
    {
        GuiResponse          l_guiResponse         = null;
        Msisdn               l_delMsisdn           = null;
        PpasDateTime         l_currentTime         = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50000, this,
                "ENTERED " + C_METHOD_deleteMsisdnRouting);
        }

        // Get and validate that there is an msisdn in the request...
        l_delMsisdn = getValidMsisdn(p_guiRequest,
                                     0L,
                                     p_request,
                                     "p_delMsisdn",
                                     true,
                                     "",
                                     i_guiContext.getMsisdnFormatInput());

        // Put the msisdn obtained into the GUI request.
        p_guiRequest.setMsisdn(l_delMsisdn);

        // Stick the MSISDN onto the screen data object. REQUIRED FOR ROUTING SCREEN ???
        p_screenData.setDelMsisdn(l_delMsisdn);                             

        // Invoke the service method to delete the routing.
        l_guiResponse = i_ccMsisdnRoutingService.deleteMsisdnRouting(p_guiRequest,
                                                                     i_timeout,
                                                                     l_delMsisdn);

        // Add the reponse to the screen data object...
        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
       
        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText(
                    "Failed Delete at " + l_currentTime);

            // Sst the update variables MSISDN so it can be re-displayed.
            p_screenData.setDelMsisdn(l_delMsisdn);
            p_screenData.setFailedRequest(true);
        }
        else
        {
            p_screenData.setInfoText(
                    "Successful Deletion at " + l_currentTime);
                                       
        }
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50020, this,
                "LEAVING " + C_METHOD_deleteMsisdnRouting);
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getData = "getData";
    /**
     * Fetches data needed for the SDP drop down box on the MSISDN routing screen
     * from the CSO's session object and from the business config cache.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the Install screen, on
     *                     which to put the response to the request.
     */
    private void getData(GuiRequest                p_guiRequest,
                         CcMsisdnRoutingScreenData p_screenData)
    {
        ScpiScpInfoDataSet         l_inIds;
        SyfgSystemConfigData       l_syfgData;
        String                     l_routingMethod;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 60000, this,
                "ENTERED " + C_METHOD_getData);
        }

        // Get a set of IN Ids...
        l_inIds = i_configService.getAvailableINIds();

        // ...and add it to the screendata object.
        p_screenData.setInIds(l_inIds);

        // Get the System Config table data to extract the routing type
        l_syfgData = i_configService.getSyfgConfigData();
        
        try
        {
            l_routingMethod = l_syfgData.get(SyfgSystemConfigData.C_BUS_SRV,
                                             SyfgSystemConfigData.C_ROUTING_METHOD);

            p_screenData.setRoutingMethod(l_routingMethod);
        }
        catch (PpasConfigException p_pce)
        {
            i_logger.logMessage(p_pce);
            p_screenData.setRoutingMethod(null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 60010, this,
                "LEAVING " + C_METHOD_getData);
        }

        return;
    }
}
