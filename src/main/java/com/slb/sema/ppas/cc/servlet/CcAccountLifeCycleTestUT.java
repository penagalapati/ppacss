////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccountLifeCycleTestUT.java
//      DATE            :       11-Jul-2006
//      AUTHOR          :       Michael Erskine (40771f)
//      REFERENCE       :       PpacLon#2508/9419
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for GUI Account Lifecycle test. 
//                              Subscriber lifecycle events run-through. This may 
//                              be temporary until all "proper" tests have been written:
//
//      1. Installs 2 MSISDNs
//      2. Links one as a subordinate to the other one
//      3. Updates subscriber details
//      4. Refill using a voucher
//      5. Perform an account refill (payment)
//      6. Make an adjustment to the main balance
//      7. Make an adjustment to a dedicated account balance
//      8. Provision a promotion
//      9. Add family and friends numbers
//     10. Update segmentation details
//     11. Update Communities
//     12. Call History
//     13. Write a memo
//     14. Disconnect the subscriber
//     15. Reconnect by overriding the MSISDN quarantine period
//
//     Note that these tests are slightly non-standard as they rely on being run in a certain order.
//     However, this makes sense for an account lifecycle test.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 14/08/06 | M Erskine  | Ensure number range routing is  | PpacLon#2479/9683
//          |            | set-up in SYFG table as these   |
//          |            | tests currently rely on it. Also|
//          |            | do not send old airtime and     |
//          |            | service dates when setting      |
//          |            | them for the first time.        |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.HTMLElement;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebTable;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcAccountLifeCycleTestUT extends GuiTestTT
{
    /** Used for finding a specific table used in GUI screen. */
    private static final String C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY = "eventHistory";
    
    /** The format of the msisdn to use for these tests. */
    MsisdnFormat c_msisdnFormat = c_ppasContext.getMsisdnFormatInput();
    
    /** Msisdn used for testing. */
    private static Msisdn c_msisdn1Success = null;
    /** Msisdn used for testing. */
    private static Msisdn c_msisdn2Success = null;
    /** Msisdn used for testing. */
    private static Msisdn c_msisdn3Success = null;
    /** Msisdn used for testing. */
    private static Msisdn c_msisdn1Failure = null;
    /** Msisdn used for testing. */
    private static Msisdn c_msisdn2Failure = null;
    
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcAccountLifeCycleTestUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     * <BR>
     * <BR>
     * <OL>
     * <LI> Determines whether the client should automatically follow page redirect requests  
     *      (status 3xx). By default, this is true in order to simulate normal browser operation.
     *      Executed prior to the initiation of each test.
     * <LI> Specifies whether the client should automatically follow page refresh requests. 
     *      By default, this is false, so that programs can verify the redirect page presented 
     *      to users before the browser switches to the new page. Setting this to true can 
     *      cause an infinite loop on pages that refresh themselves.
     * </OL>
     */
    public void setUp()
    {
        c_webConv.getClientProperties().setAutoRedirect(true);
        c_webConv.getClientProperties().setAutoRefresh(true);
        HttpUnitOptions.setScriptingEnabled(false);
        HttpUnitOptions.setExceptionsThrownOnScriptError(false);
        login("SUPER", "SEMAUK");
    }
    
    /**
     * @ut.when An operator navigates to the Msisdn Routing screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadMsisdnRoutingScreen()
    {        
        beginOfTest("Start testLoadMsisdnRoutingScreen");
        
        // GET /ascs/gui/cc/MsisdnRouting?p_command=GET_SCREEN_DATA
        sendRequest(constructGetRequest("cc/MsisdnRouting", "GET_SCREEN_DATA", ""));

        // Validate that correct page is returned and that the content is correct
        validateResponse(null, null, "MsisdnRouting");

        endOfTest();
    }
    
    /**
     * @ut.when An operator attempts to add Msisdn Routing.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testAddMsisdnRoutingSuccess()
    {        
        beginOfTest("Start testAddMsisdnRoutingSuccess");
        SqlString l_sqlString = null;
        String    l_sql = null;
        int       l_nbOfRows = 0;
        SqlString l_nublSqlString = null;
        String    l_nublSql = null;
        JdbcResultSet l_resultSet = null;
        
        // Ensure number range routing is in use.        
        setRoutingMethod("NBR");
        
        
//            try
//            {
//                c_msisdn1Success = c_dbServiceTT.getNextFreeMsisdn(new Market(1, 2), "00");
//            }
//            catch (PpasSqlException e)
//            {
//                e.printStackTrace();
//            }
        try
        {
            c_msisdn1Success = c_ppasContext.getMsisdnFormatInput().parse("0832002500");
        }
        catch (ParseException e1)
        {
            e1.printStackTrace();
        }
            
        //Dump of NUBL before
        System.out.println("MIE Dump of NUBL before: ");
        l_nublSql = "select * from nubl_number_block where nubl_srva=1 and nubl_sloc=2";
        l_nublSqlString = new SqlString(500, 0, l_nublSql);
        l_resultSet = super.sqlQuery(l_nublSqlString);

        try
        {
            int i=0;
            while (l_resultSet.next(10000))
            {
                System.out.println("MIE Nubl row: " + ++i + ", " +
                                   "nubl_start_number: " + 
                                   l_resultSet.getTrimString(10010, "nubl_start_number") + ", " + 
                                   "nubl_end_number: " + 
                                   l_resultSet.getTrimString(10020, "nubl_end_number") + ", " +
                                   "nubl_sdp_id: " +
                                   l_resultSet.getTrimString(10030, "nubl_sdp_id"));
            }
        }
        catch (PpasSqlException e)
        {
            e.printStackTrace();
        }

        // Ensure that the NUBL row for this MSISDN has an SDP Id that isn't blank.
        l_sql = "update nubl_number_block set nubl_sdp_id='00' "
                + "where nubl_start_number <= {0} and nubl_end_number >= {1} "
                + "  and nubl_srva=1 and nubl_sloc=2";
        l_sqlString = new SqlString(500, 2, l_sql);
        l_sqlString.setStringParam(0, c_ppasContext.getMsisdnFormatInput().format(c_msisdn1Success));
        l_sqlString.setStringParam(1, c_ppasContext.getMsisdnFormatInput().format(c_msisdn1Success));

        System.out.println("About to run the following SQL: " + l_sqlString);
        l_nbOfRows = super.sqlUpdate(l_sqlString);
        System.out.println("NumberOfRowsUpdated: " + l_nbOfRows);
        reloadBusinessConfigCache();
        snooze(15);

        System.out.println("MIE Dump of NUBL after: ");
        l_nublSql = "select * from nubl_number_block where nubl_srva=1 and nubl_sloc=2";
        l_nublSqlString = new SqlString(500, 0, l_nublSql);
        l_resultSet = super.sqlQuery(l_nublSqlString);
        try
        {
            int i = 0;
            while (l_resultSet.next(10100))
            {
                System.out.println("MIE Nubl row: " + ++i + ", " + "nubl_start_number: "
                        + l_resultSet.getTrimString(10110, "nubl_start_number") + ", " + "nubl_end_number: "
                        + l_resultSet.getTrimString(10120, "nubl_end_number") + ", " + "nubl_sdp_id: "
                        + l_resultSet.getTrimString(10130, "nubl_sdp_id"));
            }
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }

        // catch (PpasServiceFailedException e)
        // {
        // e.printStackTrace();
        // }
        // catch (PpasSqlException e)
        // {
        // e.printStackTrace();
        // }
            
        // ROUTING METHOD dump
        String    l_syfgRoutingMethodSql = null;
        SqlString l_syfgSqlString        = null;
        
        System.out.println("MIE Dump of ROUTING_METHOD: ");
        l_syfgRoutingMethodSql = "select * from syfg_system_config where syfg_parameter_name='ROUTING_METHOD'";
        l_syfgSqlString = new SqlString(500, 0, l_syfgRoutingMethodSql);
        l_resultSet = super.sqlQuery(l_syfgSqlString);

        try
        {
            int i=0;
            while (l_resultSet.next(20000))
            {
                    System.out.println("MIE SYFG row: " + ++i + ", " +
                                       "ROUTING_METHOD value: " + 
                                       l_resultSet.getTrimString(20010, "syfg_parameter_value"));
            }
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }    
        
        sendRequest(
            constructPostRequest("cc/MsisdnRouting",
                                 "ADD_ROUTING",
                                 "&p_addMsisdn=" + c_msisdnFormat.format(c_msisdn1Success)));
        
        validateResponse("Successful Insert at" , null, "MsisdnRouting");
        endOfTest();
    }
    
    /**
     * @ut.when An operator attempts to delete Msisdn Routing for an Msisdn that isn't on the Account Finder.
     * @ut.then A failue response is returned containing the correct page.
     */
    public void testDeleteMsisdnRoutingFailure()
    {        
        beginOfTest("Start testDeleteMsisdnRoutingFailure");
        
        sendRequest(
            constructPostRequest("cc/MsisdnRouting",
                                 "DELETE_ROUTING",
                                 "&p_delMsisdn=" + "9999999999"));
                
        validateResponse("Failed Delete at" , null, "MsisdnRouting");
        endOfTest();
    }
    
    /**
     * @ut.when An operator attempts to add Msisdn Routing for an MSISDN that already has routing.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testAddMsisdnRoutingFailure()
    {        
        beginOfTest("Start testDeleteMsisdnRoutingFailure");
        
        sendRequest(
            constructPostRequest("cc/MsisdnRouting",
                                 "ADD_ROUTING",
                                 "&p_addMsisdn=" + c_msisdnFormat.format(c_msisdn1Success)));
                        
        validateResponse("Failed Insert at" , null, "MsisdnRouting");
        endOfTest();
    }
    
    /**
     * @ut.when An operator attempts to delete Msisdn Routing.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testDeleteMsisdnRoutingSuccess()
    {        
        beginOfTest("Start testDeleteMsisdnRoutingSuccess");
        
        sendRequest(
            constructPostRequest("cc/MsisdnRouting",
                                 "DELETE_ROUTING",
                                 "&p_delMsisdn=" + c_msisdnFormat.format(c_msisdn1Success)));
                        
        validateResponse("Successful Deletion at" , null, "MsisdnRouting");
        endOfTest();
    }
    
    /**
     * @ut.when An attempt is made to load the install screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadInstallScreenSuccess()
    {        
        beginOfTest("Start testLoadInstallScreen");
        
        WebRequest l_webRequest =
            constructGetRequest(
                "cc/Install",
                "GET_SCREEN_DATA",
                "");
        
        sendRequest(l_webRequest);        
        validateResponse(null, null, "PPAS - Install");
        
        endOfTest();
    }
    
    /**
     * @ut.when An attempt is made to install a standalone subscriber.
     * @ut.then A successful response is returned containing the correct page.
     */    
    public void testInstallStandaloneSuccess()
    {        
        beginOfTest("Start testInstallStandaloneSuccess");
        HashMap l_parameters = new HashMap();
        Msisdn  l_msisdn = null;
        
        try
        {
            l_msisdn = c_dbServiceTT.getNextFreeMsisdn(new Market(1, 2), "00");
            c_dbServiceTT.preInstallMsisdn(l_msisdn, "00", null);
            c_msisdn2Success = l_msisdn;
 
        }
        catch (PpasServiceFailedException e)
        {
            e.printStackTrace();
        }
        catch (PpasSqlException e)
        {
            e.printStackTrace();
        }
        catch (PpasServiceException e)
        {
            e.printStackTrace();
        }
        
        l_parameters.put("p_srva", "1");
        l_parameters.put("p_sloc", "2");
        l_parameters.put("p_msisdn", c_msisdnFormat.format(l_msisdn));
        l_parameters.put("p_subType", "STANDALONE");
        l_parameters.put("p_serviceClass", "0");
        l_parameters.put("p_promoPlan", "P000");
        
        WebRequest l_webRequest =
            constructGetRequest(
                "cc/Install",
                "INSTALL",
                l_parameters);
        
        sendRequest(l_webRequest);        
        
        // Wait for request to complete
        snooze(2);
        
        validateResponse(null, null, "PPAS - Install");
        
        try
        {
            // Validate the returned page.
            HTMLElement[] l_infoImage = c_webResponse.getElementsWithName("infoImage");
        
            // Proof of concept - leave in for now.
            if (l_infoImage != null)
            {
                for (int i = 0; i < l_infoImage.length; i++)
                {
                    say("infoImage[" + i + "]: " + l_infoImage[i]);
                }
            }
            else
            {
                say("l_infoImage is null!");
            }
            
            assertEquals("Wrong response code: ", 200, c_webResponse.getResponseCode());
        
            WebForm l_form = c_webResponse.getFormWithName("installForm");
            assertNotNull("installForm is null!", l_form);

            Pattern p = Pattern.compile(".*has been successfully installed.*");
            Pattern q = Pattern.compile(".*transient status.*");
            Pattern r = Pattern.compile(".*service unavailable.*");
            Matcher m = p.matcher(c_webResponse.getText());
            Matcher n = q.matcher(c_webResponse.getText());
            Matcher o = r.matcher(c_webResponse.getText());
        
            if (n.matches())
            {
                fail("Subscriber installed with a transient status. Test abandoned. Got:" + 
                     c_webResponse.getText());
            }
        
            if (o.matches())
            {
                fail("Service unavailable. Test abandoned. Got: " + c_webResponse.getText());
            }
        
            if (c_webResponse.getText().indexOf("has been successfully installed") == -1)
            {
                fail("Expected MSISDN has been successfully installed but got: " + c_webResponse.getText());
            }
        }
        catch (Exception e)
        {
            failedTestException(e);
        }
        finally
        {
            endOfTest();
        }
    }

    //TODO: Empty tests - comment out for now so that we don't skew the success/failure ratio.
//    public void testInstallStandaloneFailWrongSDPMarket()
//    {        
//        beginOfTest("Start testInstallStandaloneFailWrongSDPMarket");
//        endOfTest();
//    }
//    
//    public void testInstallSubordinate1Success()
//    {        
//        beginOfTest("Start testInstallSubordinate1Success");
//        endOfTest();
//    }
//    
//    public void testInstallSubordinate2Success()
//    {        
//        beginOfTest("Start testInstallSubordinate2Success");
//        endOfTest();
//    }
//    
//    public void testInstallStandaloneToBeMadeIntoSubordinate()
//    {        
//        beginOfTest("Start testInstallStandaloneToBeMadeIntoSubordinate");
//        endOfTest();
//    }
//    
//    public void testDisplayBalanceScreen()
//    {
//    	beginOfTest("Start displayBalanceScreen");
//        endOfTest();
//    }

    // Account Details screen
    // TODO: The Account Details screen tests can be enhanced, as described below:
    // 1. Status fields can be verified at various points:
    //    a) Subscriber status e.g. Available/Active
    //    b) Service status
    //    c) Airtime status
    //    d) Negative balance status
    // 2. Service Dates validation
    //    a) Installation
    //    b) Activation
    //    ... etc
    
    /**
     * @ut.when An attempt is made to load the account details screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadAccountDetailsScreenSuccess()
    {
        beginOfTest("Start loadAccountDetailsScreenSuccess");
        
        //TODO: Use/adapt getTestMsisdn()?Move getTestMsisdn to superclass? Shouldn't need both - messy...
        c_msisdn3Success = installAccountClass(0);
        //snooze(15);
        loadMaintainSubsScreenForMsisdn(c_msisdnFormat.format(c_msisdn3Success));
        //TODO: Add more validation of the returned Account Details screen.
    }
    
    /**
     * @ut.when An attempt is made to set/change the airtime expiry date.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testSetMasterAirtimeAndServiceDatesSuccess()
    {
        beginOfTest("Start testSetMasterAirtimeAndServiceDatesSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "0");
        l_params.put("p_newClass", "0");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000000");
        l_params.put("p_newAgent", "00000000");
        l_params.put("p_oldSubAgent", "");
        l_params.put("p_newSubAgent", "");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        //l_params.put("p_oldAirtimeExpiryDate", "");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        //l_params.put("p_oldServiceExpiryDate", "");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            HashMap l_accountDetailsFields = new HashMap();
            say("l_forms.length: " + l_forms.length);
            for (int i = 0; i < l_forms.length; i++)
            {
                say("    l_forms[" + i + "]: " + l_forms[i]);
                say("    l_forms[" + i + "].getTitle(): " + l_forms[i].getTitle());
                say("    l_forms[" + i + "].getName() : " + l_forms[i].getName());
                String[] l_parameterNames = l_forms[i].getParameterNames();
                for (int j=0; j<l_parameterNames.length; j++)
                {
                    System.out.println("l_parameterNames["+j+"]: " + l_parameterNames[j]);
                    System.out.println("    parameterValue: " + l_forms[i].getParameterValue(l_parameterNames[j]));
                    //Load all the input field names and their values into a Hash
                    l_accountDetailsFields.put(l_parameterNames[j], l_forms[i].getParameterValue(l_parameterNames[j]));
                }
            }
            
            // Validate whichever field we want to.
            assertEquals(l_accountDetailsFields.get("p_oldAirtimeExpiryDate"), "29-Dec-2009");
            
            // Perhaps also validate event history?
            WebTable[] l_webTables = c_webResponse.getTables(); // Returns all the tables on the screen.
            WebTable l_webTable = c_webResponse.getTableWithID("eventHistory");
          
            System.out.println("MIE getTableWithId: eventHistory");
            for (int i = 0; i < l_webTable.getRowCount(); i++)
            {
                for (int j = 0; j < l_webTable.getColumnCount(); j++)
                {
                    System.out.println("l_webTable[" +i +"][" + j + "]: " + l_webTable.getCellAsText(i,j));
                }
            }
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 0,
                             new String[] {"", "Date/Time", "Desc", "Source"});
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Airtime Expiry set to 29-Dec-2009", "SUPER"});
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 2,
                             new String[] {"", "DateTimeStamp", "Service Expiry set to 29-Dec-2009", "SUPER"});
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 3,
                             new String[] {"", "DateTimeStamp", "Customer Installed", "SUPER"});
            
          
            System.out.println("l_webTables.length: " + l_webTables.length); // Returns 1, but this is a 
                                                     // table containing all the sub tables. 
//            for (int i = 0; i < l_webTables.length; i++)
//            {
                //l_webTables[i].asText();
                //System.out.println("Cell 0,0 as text: " + l_webTables[i].getCellAsText(0,0));
                //System.out.println("Cell 0,1 as text: " + l_webTables[i].getCellAsText(0,1));
                //System.out.println("Cell 0,2 as text: " + l_webTables[i].getCellAsText(0,2));
                // 1,1 give an ArrayIndexOutOfBoundsException because there's only one table.
                //System.out.println("Cell 1,1 as text: " + l_webTables[i].getCellAsText(1,1));
//            }
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }

    /**
     * @ut.when An attempt is made to set/change the service expiry date.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateMasterServiceDateSuccess()
    {
        beginOfTest("Start testUpdateMasterServiceDateSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "0");
        l_params.put("p_newClass", "0");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000000");
        l_params.put("p_newAgent", "00000000");
        l_params.put("p_oldSubAgent", "");
        l_params.put("p_newSubAgent", "");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "30-Dec-2009");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the service expiry date field and validate it.
        
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            HashMap l_accountDetailsFields = new HashMap();
            say("l_forms.length: " + l_forms.length);
            for (int i = 0; i < l_forms.length; i++)
            {
                say("    l_forms[" + i + "]: " + l_forms[i]);
                say("    l_forms[" + i + "].getTitle(): " + l_forms[i].getTitle());
                say("    l_forms[" + i + "].getName() : " + l_forms[i].getName());
                String[] l_parameterNames = l_forms[i].getParameterNames();
                for (int j=0; j<l_parameterNames.length; j++)
                {
                    System.out.println("l_parameterNames["+j+"]: " + l_parameterNames[j]);
                    System.out.println("    parameterValue: " + l_forms[i].getParameterValue(l_parameterNames[j]));
                    //Load all the input field names and their values into a Hash
                    l_accountDetailsFields.put(l_parameterNames[j], l_forms[i].getParameterValue(l_parameterNames[j]));
                }
            }
            
            // Validate whichever field we want to.
            assertEquals("30-Dec-2009", l_accountDetailsFields.get("p_oldServiceExpiryDate") );

            // Quicker than above - do the following.
            assertEquals("30-Dec-2009", l_forms[0].getParameterValue("p_oldServiceExpiryDate"));
            
            // Perhaps also validate event history?
            WebTable[] l_webTables = c_webResponse.getTables(); // Returns all the tables on the screen.
            WebTable l_webTable = c_webResponse.getTableWithID("eventHistory");
          
            System.out.println("MIE getTableWithId: eventHistory");
            for (int i = 0; i < l_webTable.getRowCount(); i++)
            {
                for (int j = 0; j < l_webTable.getColumnCount(); j++)
                {
                    System.out.println("l_webTable[" +i +"][" + j + "]: " + l_webTable.getCellAsText(i,j));
                }
            }
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Service Expiry changed from 29-Dec-2009 to 30-Dec-2009", "SUPER"});
          
            System.out.println("l_webTables.length: " + l_webTables.length); // Returns 1, but this is a 
                                                                  // table containing all the sub tables. 
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }

    /**
     * @ut.when An attempt is made to change the service class from 0 to 1.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeMasterClass0to1Success()
    {
        beginOfTest("Start testChangeMasterClass0to1Success");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "0");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000000");
        l_params.put("p_newAgent", "00000000");
        l_params.put("p_oldSubAgent", "");
        l_params.put("p_newSubAgent", "");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Quicker than above - do the following.
            assertEquals("1", l_forms[0].getParameterValue("p_oldClass"));
            
            // Validate event history
            WebTable l_webTable = c_webResponse.getTableWithID("eventHistory");

            for (int i = 0; i < l_webTable.getRowCount(); i++)
            {
                for (int j = 0; j < l_webTable.getColumnCount(); j++)
                {
                    System.out.println("l_webTable[" +i +"][" + j + "]: " + l_webTable.getCellAsText(i,j));
                }
            }
            
            //TODO: We don't get this row in the event history (even in a manual test) - why not?
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Service Class Changed from 0 to 1, " + 
                                 "reason is permanent", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the Agent from 00000000 to 00000001.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeMasterAgentSuccess()
    {
        beginOfTest("Start testChangeMasterAgentSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000000");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "");
        l_params.put("p_newSubAgent", "");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("00000001", l_forms[0].getParameterValue("p_oldAgent"));
            
            // Validate event history
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Sales Agent / Sub-Agent Changed", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the Sub Agent from blank to 00000000.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeMasterSubAgentSuccess()
    {
        beginOfTest("Start testChangeMasterSubAgentSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("00000000", l_forms[0].getParameterValue("p_oldSubAgent"));
            
            // Validate event history
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Sales Agent / Sub-Agent Changed", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the home region from blank to 1.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeAccountHomeRegionSuccess()
    {
        beginOfTest("Start testChangeAccountHomeRegionSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "");
        l_params.put("p_newHomeRegion", "1");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("1", l_forms[0].getParameterValue("p_oldHomeRegion"));
            
            // Validate event history
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Account Home Region changed to 1", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the USSD Eocn Id from 255 (default value) to 199.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeEocnIdSuccess()
    {
        beginOfTest("Start testChangeEocnIdSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "1");
        l_params.put("p_newHomeRegion", "1");
        l_params.put("p_oldUssdEocnId", "");
        l_params.put("p_newUssdEocnId", "199");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Validation
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("199", l_forms[0].getParameterValue("p_oldUssdEocnId"));
            
            // Validate event history
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "USSD EoCN Id changed to 199", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the IVR Pin code from blank to 1234.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeIvrPinCodeSuccess()
    {
        beginOfTest("Start testChangeIvrPinCodeSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "1");
        l_params.put("p_newHomeRegion", "1");
        l_params.put("p_oldUssdEocnId", "199");
        l_params.put("p_newUssdEocnId", "199");
        l_params.put("p_oldPinCode", "");
        l_params.put("p_newPinCode", "1234");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Get the airtime expiry date field and validate it.
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("1234", l_forms[0].getParameterValue("p_oldPinCode"));
            
            // Validate event history
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "IVR Access PIN Code Updated", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to change the home region from 1234 to 1. This fails as the new pin code is
     *          validated on the servlet and must be 4 digits in length.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testChangeIvrPinCodeFail()
    {
        beginOfTest("Start testChangeIvrPinCodeFail");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "1");
        l_params.put("p_newHomeRegion", "1");
        l_params.put("p_oldUssdEocnId", "199");
        l_params.put("p_newUssdEocnId", "199");
        l_params.put("p_oldPinCode", "1234");
        l_params.put("p_newPinCode", "1");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Validation
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();

            // Validate the field.
            assertEquals("1234", l_forms[0].getParameterValue("p_oldPinCode"));
            
            //TODO: Validate failure response message
            // "<00190> For MSISDN, +44 (0) 00832000050, the pin code provided, 1, is invalid,
            // length must be 4."
            
            validateResponse("<00190> For MSISDN, +44 (0) " + c_msisdnFormat.format(c_msisdn3Success) +
                             ", the pin code provided, 1, is invalid, length must be 4.",
                             null,
                             "Account Details");
            
            // Validate event history hasn't changed.
            
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "IVR Access PIN Code Updated", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    
    /**
     * @ut.when An attempt is made to set the temp blocking flag.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testTemporaryBlockMasterSetSuccess()
    {
        beginOfTest("Start testTemporaryBlockMasterSetSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldTempBlocking", "false");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "1");
        l_params.put("p_newHomeRegion", "1");
        l_params.put("p_oldUssdEocnId", "199");
        l_params.put("p_newUssdEocnId", "199");
        l_params.put("p_oldPinCode", "1234");
        l_params.put("p_newPinCode", "1234");
        l_params.put("p_oldTempBlocking", "");
        l_params.put("p_tempBlocking", "true");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Validation
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            HashMap l_accountDetailsFields = new HashMap();
            say("l_forms.length: " + l_forms.length);
            for (int i = 0; i < l_forms.length; i++)
            {
                say("    l_forms[" + i + "]: " + l_forms[i]);
                say("    l_forms[" + i + "].getTitle(): " + l_forms[i].getTitle());
                say("    l_forms[" + i + "].getName() : " + l_forms[i].getName());
                String[] l_parameterNames = l_forms[i].getParameterNames();
                for (int j=0; j<l_parameterNames.length; j++)
                {
                    System.out.println("l_parameterNames["+j+"]: " + l_parameterNames[j]);
                    System.out.println("    parameterValue: " + l_forms[i].getParameterValue(l_parameterNames[j]));
                    //Load all the input field names and their values into a Hash
                    l_accountDetailsFields.put(l_parameterNames[j], l_forms[i].getParameterValue(l_parameterNames[j]));
                }
            }

            // Validate the field.
            assertEquals("true", l_forms[0].getParameterValue("p_oldTempBlocking"));
            
            // Validate event history.
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Temporary Blocking set", "SUPER"});  
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /**
     * @ut.when An attempt is made to clear the temp blocking flag.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testTemporaryBlockMasterClearSuccess()
    {
        // Note that temp blocking is different to other fields. If temp blocking check box is checked
        // then the temp blocking is changed i.e. set to the state opposite of what it was.
        
        beginOfTest("Start testTemporaryBlockMasterClearSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/AccountDetails?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Details");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_oldClass", "1");
        l_params.put("p_newClass", "1");
        l_params.put("p_oldLanguage", "EN");
        l_params.put("p_newLanguage", "EN");
        l_params.put("p_oldAgent", "00000001");
        l_params.put("p_newAgent", "00000001");
        l_params.put("p_oldSubAgent", "00000000");
        l_params.put("p_newSubAgent", "00000000");
        l_params.put("p_oldUssdEocnId", "255");
        l_params.put("p_newUssdEocnId", "255");
        l_params.put("p_oldAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_newAirtimeExpiryDate", "29-Dec-2009");
        l_params.put("p_oldServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_newServiceExpiryDate", "29-Dec-2009");
        l_params.put("p_oldHomeRegion", "1");
        l_params.put("p_newHomeRegion", "1");
        l_params.put("p_oldUssdEocnId", "199");
        l_params.put("p_newUssdEocnId", "199");
        l_params.put("p_oldPinCode", "1234");
        l_params.put("p_newPinCode", "1234");
        l_params.put("p_oldTempBlocking", "true");
        l_params.put("p_tempBlocking", "false");
        
        l_request = constructPostRequest("cc/AccountDetails", "UPDATE_ACCOUNT_DETAILS", l_params);
        sendRequest(l_request);
        
        // Validation
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            HashMap l_accountDetailsFields = new HashMap();
            say("l_forms.length: " + l_forms.length);
            for (int i = 0; i < l_forms.length; i++)
            {
                say("    l_forms[" + i + "]: " + l_forms[i]);
                say("    l_forms[" + i + "].getTitle(): " + l_forms[i].getTitle());
                say("    l_forms[" + i + "].getName() : " + l_forms[i].getName());
                String[] l_parameterNames = l_forms[i].getParameterNames();
                for (int j=0; j<l_parameterNames.length; j++)
                {
                    System.out.println("l_parameterNames["+j+"]: " + l_parameterNames[j]);
                    System.out.println("    parameterValue: " + l_forms[i].getParameterValue(l_parameterNames[j]));
                    //Load all the input field names and their values into a Hash
                    l_accountDetailsFields.put(l_parameterNames[j], l_forms[i].getParameterValue(l_parameterNames[j]));
                }
            }

            // Validate the field.
            assertEquals("false", l_forms[0].getParameterValue("p_oldTempBlocking"));
            
            // Validate event history.
            verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 1,
                             new String[] {"", "DateTimeStamp", "Temporary Blocking cleared", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    // Account refills screen
    //TODO: Add tests for payments
    /**
     * @ut.when An attempt is made to get the Account Refills screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testGetAccountRefillsScreenSuccess()
    {
        beginOfTest("Start testGetAccountRefillsScreenSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/Payments", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/Payments?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Refills");
        snooze(1);
        endOfTest();
    }

    /**
     * @ut.when An attempt is made to make an account refill.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testMakeAccountRefillMasterSuccess()
    {
        beginOfTest("Start testMakeAccountRefillMasterSuccess");
        HashMap l_params = new HashMap();
        WebRequest l_request = null;
        
        loadMaintainSubsScreenForMsisdn(c_msisdn3Success);
        debugCurrentFrame();
        validateResponse(null, null, "PPAS: Account Details");
        
        l_request = constructGetRequest("cc/Payments", "GET_SCREEN_DATA");
        sendRequest(l_request);
        System.out.println("Frame returned by cc/Payments?p_command=GET_SCREEN_DATA:"); 
        debugCurrentFrame(); 
        validateResponse(null, null, "Account Refills");
        
        System.out.println("Setting parameters and sending UPDATE_ACCOUNT_DETAILS...");
        l_params.put("p_paymentType", "C");  // Card Payment
        l_params.put("p_amount", "1.00");
        l_params.put("p_currency", "GBP");
        l_params.put("p_paymentProfile", "A2"); // Airtime, two 2 dp
        l_params.put("p_description1","Test Payment Refill Master Success");
        l_params.put("p_description2","Test description field 2");
        l_request = constructPostRequest("cc/Payments", "APPLY_PAYMENT", l_params);
        sendRequest(l_request);
        
        // Validation
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            HashMap l_accountDetailsFields = new HashMap();
            say("l_forms.length: " + l_forms.length);
            for (int i = 0; i < l_forms.length; i++)
            {
                say("    l_forms[" + i + "]: " + l_forms[i]);
                say("    l_forms[" + i + "].getTitle(): " + l_forms[i].getTitle());
                say("    l_forms[" + i + "].getName() : " + l_forms[i].getName());
                String[] l_parameterNames = l_forms[i].getParameterNames();
                for (int j=0; j<l_parameterNames.length; j++)
                {
                    System.out.println("l_parameterNames["+j+"]: " + l_parameterNames[j]);
                    System.out.println("    parameterValue: " + l_forms[i].getParameterValue(l_parameterNames[j]));
                    //Load all the input field names and their values into a Hash
                    l_accountDetailsFields.put(l_parameterNames[j], l_forms[i].getParameterValue(l_parameterNames[j]));
                }
            }
            
            System.out.println("c_msisdn3Success: " + c_msisdn3Success);
            // TODO: Validate the field.
            //assertEquals("false", l_forms[0].getParameterValue("p_oldTempBlocking"));
            
            // TODO: Validate event history.
            //verifyRowInTable(C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY, 10,
            //                 new String[] {"", "DateTimeStamp", "Customer Installed", "SUPER"});
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        finally
        {
            snooze(1); // Avoid comment history rows with the same datetime stamp.
            endOfTest();
        }
    }
    
    /*
    //...Missing tests
   
    testGetPaymentScreen
    testMakePaymentSubordinate
    testGetPaymentDetailSubordinate
    testMakePaymentMaster
    testGetPaymentDetailMaster
    
    testGetSubscriberDetailsScreen
    testUpdateCustomerGender
    testUpdateCustomerName
    testUpdateCustomerAddress
    testUpdateCustomerContact
    testCustomerConnectFailedMsisdn
    
    //...Missing tests
    
    testGetAdjustmentScreen
    testPostAdjustmentMaster
    testPostAdjustmentSubordinate
    
    
    
    testGetVouchersScreen
    testStandardVoucherRefillMaster
    testStandardVoucherEnquiry
    testValueVoucherRefill
    testValueVoucherEnquiry
    testStandardVocuherRefillSubordinate
    
    testGetPromotionsScreen
    testInsertAllocationScreen
    testAddPromotionAllocationMaster
    testGetUpdateAllocationScreen
    testUpdatePromotionAllocationMaster
    testDeletePromotionAllocationMaster
    testAddPromotionAllocationSubordinate
    
    // Dedicated Accounts tests (already tested in CcDedicatedAccountsUT)
    
    testGetSegmentationScreen
    testAddSegmentation
    
    testFetFamilyAndFriendsScreen
    testAddFamilyAndFriendsNumber
    
    testGetCommunitiesScreen
    testAddCommunity
    
    // Additional tests
    
    testLoadDisconnectionScreen
    testDisconnectSubscriber
    
    testInstallFailureQuarantinedMsisdn
    testInstallSubscriberOverrideQuarantionPeriodSuccess
    
    testRetrieveCallHistory
    ...etc
    
    testWriteMemo
    testLoadAdditionalInfoScreen
    testUpdateAdditionalInfoSuccess
    testLoadAccumulatorsScreen
    testUpdateAccumulators
    
    */
    
    
    

    
 
    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        System.out.println(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcAccountLifeCycleTestUT.class);
    }
    
    /**
     * Sets the routing method, SDP or number range.
     * @param p_routingMethod
     */
    private void setRoutingMethod(String p_routingMethod)
    {
        SqlString      l_sqlString;
        
        l_sqlString = new SqlString(500, 1, 
                                "update syfg_system_config " +
                                "set syfg_parameter_value = {0} " +
                                "where syfg_destination = {1} " +
                                "and syfg_parameter_name = {2}");
        
        l_sqlString.setStringParam(0, p_routingMethod);
        l_sqlString.setStringParam(1, SyfgSystemConfigData.C_BUS_SRV);
        l_sqlString.setStringParam(2, SyfgSystemConfigData.C_ROUTING_METHOD);

        sqlUpdate(l_sqlString);
    }
    
    
    /** Convenience method to get a test MSISDN.
     * 
     * @return String representing a test MSISDN.
     */
    private String getTestMsisdn()
    {
        String l_msisdn = c_ppasContext.getMsisdnFormatInput().format(installAccountClass(1));
        snooze(15);  // Ensure account installed.
        return l_msisdn;
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------    

}


