////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcFailedRechargeDataSetResponse.java
//      DATE            :       13-Feb-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/5475
//                              PRD_PPAK00_DEV_IN_034
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for FailedRechargeDataSet
//                              objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.FailedRechargeDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for FailedRechargeDataSet objects. 
 */
public class CcFailedRechargeDataSetResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcFailedRechargeDataSetResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The FailedRechargeDataSet object wrapped by this object. */
    private FailedRechargeDataSet  i_failedRechargeDataSet;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------
    /**
     * Uses a FailedRechargeDataSet object and the additional necessary objects
     * to create a response containing message data and a failed recharge data
     * set.
     * @param p_guiRequest            The request being processed.
     * @param p_messageLocale         Locale for the response. Can be null in
     *                                which case the default Locale will be used.
     * @param p_messageKey            Response message key.
     * @param p_messageParams         Any parameters to be substituted into
     *                                response message.
     * @param p_messageSeverity       Severity of response.
     * @param p_failedRechargeDataSet The failed recharge data set.
     */
    public CcFailedRechargeDataSetResponse(
                               GuiRequest            p_guiRequest,
                               Locale                p_messageLocale,
                               String                p_messageKey,
                               Object[]              p_messageParams,
                               int                   p_messageSeverity,
                               FailedRechargeDataSet p_failedRechargeDataSet)
    {
        super(p_guiRequest,
              p_messageLocale,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing CcFailedRechargeDataSetResponse");
        }

        i_failedRechargeDataSet = p_failedRechargeDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed CcFailedRechargeDataSetResponse");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Returns the failed recharge data set.
     * @return Set of failed recharges.
     */
    public FailedRechargeDataSet getFailedRechargeDataSet()
    {
        return(i_failedRechargeDataSet);
    }
}
