////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcBasicAccountDataResponse.java
//      DATE            :       28-May-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon#1325/5704
//                              PRD_PPAK00_DEV_IN_37
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Response returned by a GUI service method that
//                              is a container for BasicAccountData.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response returned by a GUI service method that contains an 
 * BasicAccountData object.
 */
public class CcBasicAccountDataResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcBasicAccountDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** BasicAccountData instance contained in this response. */
    private BasicAccountData i_basicAccountData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * Uses the Locale passed in to construct text of message.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_basicAccountData Basic details about the account.
     */
    public CcBasicAccountDataResponse(GuiRequest       p_guiRequest,
                                      Locale           p_messageLocale,
                                      String           p_messageKey,
                                      Object[]         p_messageParams,
                                      int              p_messageSeverity,
                                      BasicAccountData p_basicAccountData)
    {
        super(p_guiRequest,
              p_messageLocale,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_basicAccountData = p_basicAccountData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the BasicAccountData instance contained within
     * this response object.
     * @return Basic details about the account.
     */
    public BasicAccountData getBasicAccountData()
    {
        return i_basicAccountData;
    }
}
