////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubscriberSegmentationScreenData.java
//      DATE            :       01-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PRD_ASCS00_GEN_CA_016
//
//      DESCRIPTION     :       GUI screen data object for
//                              Subscriber Segmentation.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingDataSet;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Screen data object for subscriber segmentation.
 */
public class CcSubscriberSegmentationScreenData extends GuiScreenData
{

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "CcSubscriberSegmentationScreenData";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**
     * Info text to be displayed on the GUI.
     */
    private String i_infoText = "";
    
    /**
     * Stores a data set of all configured account groups.
     */
    private AcgrAccountGroupDataSet i_accountGroups;
    
    /**
     * Stores a dataset of all available service offerings.
     */
    private SeofServiceOfferingDataSet i_serviceOfferings;
    
    /**
     * Stores the subscribers current group id.
     */
    private AccountGroupId i_currentAccountGroup;
    
    /**
     * Stores the subscribers active service offering.
     */
    private ServiceOfferings i_activeServiceOffering;
    
    /**
     * Holds the maximum number of service offerings as detailed on the servlet.
     */
    private int i_maxNumberOfServiceOfferings;
    
    /** Indicates whether the Subscriber in the request for this screen
     * is a subordinate or not.
     */
    private boolean i_isSubordinate;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * simple constructor for the Subscriber Segmentation screen data object.
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcSubscriberSegmentationScreenData(GuiContext p_guiContext,
                                              GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /**
     * Returns an array of the subscriber segmentation account group descriptions.
     * @return An array of the configured account group descriptions.
     */
    public AcgrAccountGroupData[] getAccountGroupDesc()
    {
        // If an account group has been returned from the SDP check that it
        // exists in the configured accoutn groups. If it doesn't, add a "Not Defined" group to the array.
        AcgrAccountGroupData[] l_availableAccGrpArr = null;
        AcgrAccountGroupData[] l_accGrpArr = null;
        boolean                l_found = false;

        l_availableAccGrpArr = i_accountGroups.getAvailableArray();

        if (i_currentAccountGroup != null)
        {
            for (int l_counter = 0; l_counter<l_availableAccGrpArr.length; l_counter++)
            {
                 if (l_availableAccGrpArr[l_counter].getAccountID().getValue() ==
                        i_currentAccountGroup.getValue())
                 {
                      l_found = true;
                 }
            }

            if (l_found)
            {
                l_accGrpArr = l_availableAccGrpArr;
            }
            else
            {
                l_accGrpArr = new AcgrAccountGroupData[l_availableAccGrpArr.length + 1];

                for (int l_counter = 0; l_counter<l_availableAccGrpArr.length; l_counter++)
                {
                     l_accGrpArr[l_counter] = l_availableAccGrpArr[l_counter];
                }

                l_accGrpArr[l_availableAccGrpArr.length] = 
                                              new AcgrAccountGroupData(i_guiRequest,
                                                                       i_currentAccountGroup,
                                                                       "Not Defined",
                                                                       ' ',
                                                                       null,  // Operator unknown
                                                                       DatePatch.getDateTimeNow());
            }
        }
        else
        {
             l_accGrpArr = l_availableAccGrpArr;
        }

        return (l_accGrpArr);
    }

    /**
     * Returns an array of the subscriber segmentation service offering descriptions.
     * @return An array of the configured service offerings.
     */
    public String[] getServiceOfferingsDesc()
    {
        String[] l_servOfferingsDesc = new String[i_maxNumberOfServiceOfferings];
        
        for (int l_count = 0; l_count < i_maxNumberOfServiceOfferings; l_count++)
        {            
            if (i_serviceOfferings.getRecord(l_count + 1) != null)
            {
                l_servOfferingsDesc[l_count] = i_serviceOfferings.getRecord(l_count + 1).
                                                                  getServiceOfferingDescription();
            }
            else
            {
                l_servOfferingsDesc[l_count] = "Not defined";
            }
        }
        
        return l_servOfferingsDesc;
    }
    
    /**
     * Sets the subscribers current account group.
     * @param p_accountGroup Account group to set.
     */
    public void setCurrentAccountGroupId(AccountGroupId p_accountGroup)
    {
        i_currentAccountGroup = p_accountGroup;
    }

    /**
     * Returns the subscriber current accoung group id.
     * @return Current account group id.
     */
    public long getCurrentAccountGroupId()
    {
        return (i_currentAccountGroup.getValue());
    }

    /**
     * Returns the subscriber current accoung group id.
     * @return Current account group id.
     */
    public String getCurrentAccountGroupIdAsString()
    {
        return ((i_currentAccountGroup != null) ? "" + i_currentAccountGroup.getValue() : "");
    }
    
    /**
     * Sets the subscribers currently active service offering.
     * @param p_serviceOffering Service offerings to set.
     */
    public void setActiveServiceOffering(ServiceOfferings p_serviceOffering)
    {
        i_activeServiceOffering = p_serviceOffering;
    }
    
    /**
     * Returns a <code>long</code> represenatation of the subscribers active service offerings.
     * @return Active service offering.
     */
    public long getActiveServiceOfferings()
    {
        return ((i_activeServiceOffering != null) ? i_activeServiceOffering.getValue() : 0);
    }

    /**
     * Returns a <code>String</code> represenatation of the subscribers active service offerings.
     * @return Active service offering.
     */
    public String getActiveServiceOfferingsAsString()
    {
        return ((i_activeServiceOffering != null) ? "" + i_activeServiceOffering.getValue() : "");
    }
    
    /**
     * Sets the available service offerings.
     * @param p_serviceOfferings Data set of service Offerings.
     */
    public void setServiceOfferings(SeofServiceOfferingDataSet p_serviceOfferings)
    {
        i_serviceOfferings = p_serviceOfferings;
    }
    
    /**
     * Sets the available account groups.
     * @param p_accountGroups Data set of account groups.
     */
    public void setAccountGroups(AcgrAccountGroupDataSet p_accountGroups)
    {
        i_accountGroups = p_accountGroups;
    }
    

    /**
     * Determines whether a user can perform updates to the subscriber segmentation screen.
     * @return True if updates are allowed.
     */
    public boolean updateAllowed()
    {
        return (i_gopaData.subscriberSegmentationUpdateIsPermitted());
    }
    
    /**
     * Sets the maximum number of service offerings to be displayed
     * on the Subscriber Segmentation Screen.
     * @param p_numServOff Maximum number of service offerings.
     */
    public void setMaxNumberOfServiceOfferings(int p_numServOff)
    {
        i_maxNumberOfServiceOfferings = p_numServOff;
    }
    
    /**
     * Retrieves the maximum number of service offerings to be displayed
     * on the Subscriber Segmentation Screen.
     * @return Maximum number of service offerings.
     */
    public int getMaxNumberOfServiceOfferings()
    {
        return i_maxNumberOfServiceOfferings;
    }

    /** 
     * Indicates if the subscriber contained in the request for this
     * screen is a subordinate.  Returns <code>true</code> if subscriber
     * is a subordinate, otherwise returns <code>false</code>.
     * 
     * @return True if the subscriber is a subordinate, otherwise, false.
     */
    public boolean isSubordinate()
    {
        return i_isSubordinate;
    }

    /**
     * Marks the subscriber contained in the request for this screen as being
     * a subordinate.
     */
    public void setIsSubordinate()
    {
        i_isSubordinate = true;
        return;
    }

    /**
     * Returns the info text that is necessary to populate the screen 
     * info text field.
     * @return Information about the requests success.
     */
    public String getInfoText()
    {
        return i_infoText;
    }

    /**
     * Sets the Gui infoText object.
     * @param p_infoText Information about the requests success.
     */
    public void setInfoText( String p_infoText )
    {
        i_infoText = p_infoText;
    }
} // end public class
