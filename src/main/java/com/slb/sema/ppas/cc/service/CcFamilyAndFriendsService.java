////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcFamilyAndFriendsService.java
//      DATE            :       16-Sep-2003
//      AUTHOR          :       Stephen Pinton
//      REFERENCE       :       PpaLon#2002/8478
//
//      COPYRIGHT       :       SchlumbergerSema 2003
//
//      DESCRIPTION     :       GUI Family And Friends Service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/6/6   | M.Brister  | Added retrieval scope param to  | PpacLon#1544/6502
//          |            | call to getFaFList.             | PRD_ASCS00_GEN_CA_50
//          |            |                                 | BHA-016
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsData;
import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasFamilyAndFriendsService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * Family And Friends Functionality.
 */
public class CcFamilyAndFriendsService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcFamilyAndFriendsService";

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The atomic Family And Friends service. */
    private PpasFamilyAndFriendsService i_fafService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a CcFamilyAndFriendsService object that
     * can then be used to perform Family And Friends services.
     * @param p_guiRequest GuiRequest
     * @param p_flags flags
     * @param p_logger a Logger
     * @param p_guiContext GuiContext containing configuration
     */
    public CcFamilyAndFriendsService( GuiRequest p_guiRequest,
                                      long       p_flags,
                                      Logger     p_logger,
                                      GuiContext p_guiContext )
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        // Create PPAS internal services to be used by this service.
        i_fafService = new PpasFamilyAndFriendsService(p_guiRequest,
                                                       p_logger,
                                                       p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getNumbers = "getNumbers";
    /**
     * Generates a CcFamilyAndFriendsResponse object containing the
     * FAF Numbers for the MSISDN in the request.
     * @param p_guiRequest GuiRequest containing subscriber info
     * @param p_timeoutMillis The max time in milliseconds to wait for system
     *                        resources to perform the service.
     * @return  The subscribers list of current and history numbers
     *          (if requested).
     */
    public CcFamilyAndFriendsResponse getNumbers(GuiRequest p_guiRequest,
                                                 long       p_timeoutMillis)
    {
        CcFamilyAndFriendsResponse   l_ccFaFResponse = null;
        FamilyAndFriendsDataSet         l_faFDataSet    = null;
        GuiResponse                  l_guiResponse   = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10100, this,
                           "Entered " + C_METHOD_getNumbers);
        }

        try
        {
            // Call the atomic service...
            l_faFDataSet = i_fafService.getFafList(p_guiRequest,
                                                   p_timeoutMillis,
                                                   FamilyAndFriendsData.C_LEVEL_SUBSCRIBER, // TODO this will need fixing when account-level faf changes are coded
                                                   PpasFamilyAndFriendsService.C_SCOPE_ALL); // TODO - Value will be passed into this method.
                                                   

            // Generate success Response
            l_ccFaFResponse = new CcFamilyAndFriendsResponse(p_guiRequest,
                                                             p_guiRequest.getGuiSession().getSelectedLocale(),
                                                             GuiResponse.C_KEY_SERVICE_SUCCESS,
                                                             (Object[])null,
                                                             GuiResponse.C_SEVERITY_SUCCESS,
                                                             l_faFDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 10210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_getNumbers, l_pSE);

            l_ccFaFResponse = new CcFamilyAndFriendsResponse(
                                 p_guiRequest,
                                 p_guiRequest.getGuiSession().getSelectedLocale(),
                                 l_guiResponse.getKey(),
                                 l_guiResponse.getParams(),
                                 GuiResponse.C_SEVERITY_FAILURE,
                                 null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 10190, this,
                           "Leaving " + C_METHOD_getNumbers);
        }

        return(l_ccFaFResponse);

    } // end of method 'getNumbers'

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addNumber = "addNumber";
    /**
     * Adds the given number as a Family And Friends Number of the
     * subscriber contained in the request and returns a
     * CcFamilyAndFriendsResponse object containing the
     * FAF Numbers for the MSISDN in the request if request was successful.
     * @param p_guiRequest GuiRequest
     * @param p_timeoutMillis The max time in milliseconds to wait for system
     *                        resources to perform the service.
     * @param p_fafDataSet data set containing faf numbers to be added. Will only ever
     *                     contain on data item
     * @return  The success or failure response of the add operation.
     */
    public GuiResponse addNumber(GuiRequest              p_guiRequest,
                                 long                    p_timeoutMillis,
                                 FamilyAndFriendsDataSet p_fafDataSet)
    {
        GuiResponse l_guiResponse   = null;


        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 20100, this,
                           "Entered " + C_METHOD_addNumber);
        }

        try
        {
            // Call the atomic service...
            i_fafService.addFafNumber(p_guiRequest,
                                      p_timeoutMillis,
                                      p_fafDataSet,
                                      FamilyAndFriendsData.C_LEVEL_SUBSCRIBER); // TODO

            // Generate success Response
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SERVICE_SUCCESS,
                                            (Object[])null,
                                            GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 20210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_addNumber, l_pSE);

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            l_guiResponse.getKey(),
                                            l_guiResponse.getParams(),
                                            GuiResponse.C_SEVERITY_FAILURE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 20190, this,
                           "Leaving " + C_METHOD_addNumber);
        }

        return(l_guiResponse);

    } // end of method 'addNumber'

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_deleteNumber = "deleteNumber";
    /**
     * Deletes the given number from being a Family And Friends Number of the
     * subscriber contained in the request.
     * @param p_guiRequest GuiRequest
     * @param p_timeoutMillis The max time in milliseconds to wait for system
     *                        resources to perform the service.
     * @param p_fafDataSet data set contain faf details to be deleted.
     * @return  The success or failure response of the add operation.
     */
    public GuiResponse deleteNumber(GuiRequest              p_guiRequest,
                                    long                    p_timeoutMillis,
                                    FamilyAndFriendsDataSet p_fafDataSet)
    {
        GuiResponse  l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 30100, this,
                           "Entered " + C_METHOD_deleteNumber);
        }

        try
        {      
            // Call the atomic service...
            i_fafService.deleteFafNumber(p_guiRequest,
                                         p_timeoutMillis,
                                         p_fafDataSet,
                                         FamilyAndFriendsData.C_LEVEL_SUBSCRIBER); // TODO

            // Generate success Response
            l_guiResponse = new GuiResponse(
                                 p_guiRequest,
                                 p_guiRequest.getGuiSession().getSelectedLocale(),
                                 GuiResponse.C_KEY_SERVICE_SUCCESS,
                                 (Object[])null,
                                 GuiResponse.C_SEVERITY_SUCCESS);
              
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 30210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_getNumbers, l_pSE);

            // Create the response object from the key/parameters obtained
            l_guiResponse = new GuiResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE);
           
       }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 30190, this,
                           "Leaving " + C_METHOD_deleteNumber);
        }

        return(l_guiResponse);
    }
}
