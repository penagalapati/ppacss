////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerDetailsServlet.Java
//      DATE            :       26-Mar-2002
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1337/5342
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Customer Details screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//08/10/03  | R Isaacs   | Privilege checks added          | CR#60/571
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcCustomerDetailsScreenData;
import com.slb.sema.ppas.cc.service.CcCustomerDetailsDataResponse;
import com.slb.sema.ppas.cc.service.CcCustomerDetailsService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.dataclass.CustomerAddressData;
import com.slb.sema.ppas.common.dataclass.CustomerAddressDataSet;
import com.slb.sema.ppas.common.dataclass.CustomerContactData;
import com.slb.sema.ppas.common.dataclass.CustomerContactDataSet;
import com.slb.sema.ppas.common.dataclass.CustomerDetailsData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 */
public class CcCustomerDetailsServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustomerDetailsServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Customer Details Service to be used by this servlet. */
    private CcCustomerDetailsService i_ccCustomerDetailsService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcCustomerDetailsServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise the class.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcCustomerDetailsService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

    return;

    } // end doInit

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the customer details screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                      l_command     = null;
        String                      l_forwardUrl  = null;
        CcCustomerDetailsScreenData l_customerDetailsScreenData  = null;
        GuiResponse                 l_guiResponse = null;
        boolean                     l_accessGranted = false;    // CR#60/571
        GopaGuiOperatorAccessData   l_gopaData = null;          // CR#60/571


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcCustomerDetailsService if it does not already exist ...
        // this will be move into doInit later!! <<< ??? >>>
        if (i_ccCustomerDetailsService == null)
        {
            i_ccCustomerDetailsService =
                         new CcCustomerDetailsService(p_guiRequest,
                                                      0,
                                                      i_logger,
                                                      i_guiContext);
        }

        // Create screen data object.
        l_customerDetailsScreenData =
            new CcCustomerDetailsScreenData(i_guiContext, p_guiRequest);


        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,                // Mandatory ..
                                      "",                  // default to this
                                      new String [] {"GET_SCREEN_DATA",
                                                     "UPDATE"});
            // CR#60/571 Start

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("UPDATE")) &&
                (l_gopaData.subsDetailsUpdateIsPermitted()))
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("GET_SCREEN_DATA")) &&
                     (l_gopaData.hasSubsDetailsScreenAccess()))
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(), //CR#60/571
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_customerDetailsScreenData.addRetrievalResponse(p_guiRequest,
                                                      l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_customerDetailsScreenData);
            }
            else
            {
                if (l_command.equals("UPDATE"))
                {
                    setIsUpdateRequest(true);

                    updateDetails(p_guiRequest,
                                  p_request,
                                  l_customerDetailsScreenData);
                }

                getDetails(p_guiRequest,
                           l_customerDetailsScreenData);
            }
            // CR#60/571 End
        } // end try

        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_customerDetailsScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            p_request.setAttribute("p_guiScreenData", l_customerDetailsScreenData);
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

        } // end catch

        // CR#60/571 Start

        if (l_accessGranted)
        {
            if (l_customerDetailsScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Account Details screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData", l_customerDetailsScreenData);

            }
            else
            {
                // CcPaymentScreenData object has the necessary data
                // to populate the Payment screen.

                l_forwardUrl = "/jsp/cc/cccustomerdetails.jsp";

                p_request.setAttribute("p_guiScreenData",
                                       l_customerDetailsScreenData);
            }
        }
        // CR#60/571 End

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 21000, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );

    } // end doService

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /**
     * Get the customer details for a given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the Customer Details screen, on
     *                     which to put the response to the request.
     * @throws PpasServletException If an exception occurs.
     */
    private void getDetails(GuiRequest                  p_guiRequest,
                            CcCustomerDetailsScreenData p_screenData)
        throws PpasServletException
    {
        CcCustomerDetailsDataResponse l_ccCustomerDetailsDataResponse = null;
        MiscCodeDataSet               l_miscCodeDataSet               = null;
        Market                        l_market                        = null;
        GuiSession                    l_guiSession                    = null;
        String                        l_prefAddrType                  = null;

        l_ccCustomerDetailsDataResponse = i_ccCustomerDetailsService.getDetails(p_guiRequest, i_timeout);
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccCustomerDetailsDataResponse);

        if (l_ccCustomerDetailsDataResponse.isSuccess())
        {
            p_screenData.setCustomerDetailsData(
                      l_ccCustomerDetailsDataResponse.getCustomerDetailsData());
        }

        // Pass contact and address types to the screen data object
        l_guiSession = (GuiSession)p_guiRequest.getSession();

        l_market = l_guiSession.getCsoMarket();

        l_miscCodeDataSet = i_configService.getAvailableContactTypes(l_market);

        p_screenData.setContactTypes( l_miscCodeDataSet );

        // Get the preferred type of the address that is to be defaulted on-screen.
        l_prefAddrType = (String)(p_guiRequest
                                 .getContext()
                                 .getAttribute("com.slb.sema.ppas.support.PpasContext.displayAddrType"));

        p_screenData.setPreferredAddressType(l_prefAddrType);

        l_miscCodeDataSet = i_configService.getAvailableAddressTypes(l_market);

        p_screenData.setAddressTypes( l_miscCodeDataSet );

        // Indicate whether the subscriber contained in the request is a
        // subordinate.
        if (!l_guiSession.getCustId().equals(l_guiSession.getBtCustId()))
        {
            p_screenData.setIsSubordinate();
        }

    } // end private void getDetails(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateDetails = "updateDetails";
    /**
     * Updates the customer details for a given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data for the Customer Details screen, on
     *                     which to put the response to the request.
     */
    private void updateDetails(GuiRequest                  p_guiRequest,
                               HttpServletRequest          p_request,
                               CcCustomerDetailsScreenData p_screenData)
    {
        boolean                       l_paramChanged                  = false;
        String                        l_newTitle                      = null;
        String                        l_oldTitle                      = null;
        String                        l_newFirstName                  = null;
        String                        l_oldFirstName                  = null;
        String                        l_newMiddleInitial              = null;
        String                        l_oldMiddleInitial              = null;
        String                        l_newLastName                   = null;
        String                        l_oldLastName                   = null;
        String                        l_newDateOfBirthString          = null;
        String                        l_oldDateOfBirthString          = null;
        PpasDate                      l_newDateOfBirth                = null;
        String                        l_newGender                     = null;
        String                        l_oldGender                     = null;
        String                        l_newSsn                        = null;
        String                        l_oldSsn                        = null;
        String                        l_newDrivingLicenceNumber       = null;
        String                        l_oldDrivingLicenceNumber       = null;
        String                        l_newCompanyName                = null;
        String                        l_oldCompanyName                = null;
        String                        l_newAddressType                = null;
        String                        l_newAddressDescription         = null;
        String                        l_newAddressLine1               = null;
        String                        l_oldAddressLine1               = null;
        String                        l_newAddressLine2               = null;
        String                        l_oldAddressLine2               = null;
        String                        l_newAddressLine3               = null;
        String                        l_oldAddressLine3               = null;
        String                        l_newAddressLine4               = null;
        String                        l_oldAddressLine4               = null;
        String                        l_newAddressCity                = null;
        String                        l_oldAddressCity                = null;
        String                        l_newAddressPostalCode          = null;
        String                        l_oldAddressPostalCode          = null;
        String                        l_newAddressCountry             = null;
        String                        l_oldAddressCountry             = null;
        String                        l_newContact                    = null;
        String                        l_oldContact                    = null;
        CustomerDetailsData           l_customerDetailsData           = null;
        CustomerAddressDataSet        l_customerAddressDataSet        = null;
        CustomerContactDataSet        l_customerContactDataSet        = null;
        Vector                        l_contactTypesV                 = null;
        Market                        l_market                        = null;
        GuiSession                    l_guiSession                    = null;
        GuiResponse                   l_guiResponse                   = null;
        PpasDateTime                  l_currentTime                   = null;
        String                        l_male                          = "Male";
        String                        l_female                        = "Female";


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Entered " + C_METHOD_updateDetails);
        }

        l_guiSession = (GuiSession)p_guiRequest.getSession();
        l_market = l_guiSession.getCsoMarket();

        // Make all the parameters non-mandatory since they may arrive
        // with value null. Just default to empty strings.

        try
        {
            // Locate all "Personal Details" data.
            l_newTitle =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newTitle",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldTitle =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldTitle",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newTitle.equals(l_oldTitle))
            {
                l_newTitle = null;
            }
            else
            {
                l_paramChanged = true;
            }

            l_newFirstName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newFirstName",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldFirstName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldFirstName",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newFirstName.equals(l_oldFirstName))
            {
                l_newFirstName = null;
            }
            else
            {
                l_paramChanged = true;
                p_screenData.setCustNameChanged();
            }

            l_newMiddleInitial =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newMiddleInitial",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldMiddleInitial =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldMiddleInitial",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newMiddleInitial.equals(l_oldMiddleInitial))
            {
                l_newMiddleInitial = null;
            }
            else
            {
                l_paramChanged = true;
            }

            l_newLastName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newLastName",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldLastName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldLastName",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newLastName.equals(l_oldLastName))
            {
                l_newLastName = null;
            }
            else
            {
                l_paramChanged = true;
                p_screenData.setCustNameChanged();
            }

            l_newDateOfBirthString =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newDateOfBirth",
                              true,
                              "",
                              PpasServlet.C_TYPE_DATE);

            l_oldDateOfBirthString =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldDateOfBirth",
                              true,
                              "",
                              PpasServlet.C_TYPE_DATE);

            if (!l_newDateOfBirthString.equals(l_oldDateOfBirthString))
            {
                l_paramChanged = true;

                if (!l_newDateOfBirthString.equals(""))
                {
                    l_newDateOfBirth = new PpasDate(l_newDateOfBirthString);
                }
                else
                {
                    l_newDateOfBirth = new PpasDate("");
                }
            }

            l_newGender =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newGender",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldGender =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldGender",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newGender.equals(l_oldGender))
            {
                l_newGender = null;
            }
            else
            {
                if (l_newGender.equals(l_male))
                {
                    l_newGender = "M";
                }
                else if (l_newGender.equals(l_female))
                {
                    l_newGender = "F";
                }
                l_paramChanged = true;
            }

            l_newSsn =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                               p_request,
                               "newSsn",
                               true,
                               "",
                               PpasServlet.C_TYPE_ANY,
                               15,
                               PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_oldSsn =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                               p_request,
                               "oldSsn",
                               true,
                               "",
                               PpasServlet.C_TYPE_ANY,
                               15,
                               PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            if (l_newSsn.equals(l_oldSsn))
            {
                l_newSsn = null;
            }
            else
            {
                l_paramChanged = true;
            }

            l_newDrivingLicenceNumber =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newDrivingLicenceNumber",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY,
                              12,
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_oldDrivingLicenceNumber =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldDrivingLicenceNumber",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY,
                              12,
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            if (l_newDrivingLicenceNumber.equals(l_oldDrivingLicenceNumber))
            {
                l_newDrivingLicenceNumber = null;
            }
            else
            {
                l_paramChanged = true;
            }

            l_newCompanyName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "newCompanyName",
                              false,  // Not mandatory as field is disabled
                                      // for subordinates.
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldCompanyName =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "oldCompanyName",
                              false,  // Not mandatory as field is disabled
                                      // for subordinates.
                              "",
                              PpasServlet.C_TYPE_ANY);

            if (l_newCompanyName.equals(l_oldCompanyName))
            {
                l_newCompanyName = null;
            }
            else
            {
                l_paramChanged = true;
            }


            // Locate "Address" data.
            l_newAddressType =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressType",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressDescription =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressDescription",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressLine1 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressLine1",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_oldAddressLine1 =
                   getValidParam(p_guiRequest,
                                 PpasServlet.C_FLAG_ALLOW_BLANK,
                                 p_request,
                                 "oldAddressLine1",
                                 true,
                                 "",
                                 PpasServlet.C_TYPE_ANY);

            l_newAddressLine2 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressLine2",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_oldAddressLine2 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressLine2",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressLine3 =
                getValidParam(p_guiRequest,
                            PpasServlet.C_FLAG_ALLOW_BLANK,
                            p_request,
                            "newAddressLine3",
                            true,
                            "",
                            PpasServlet.C_TYPE_ANY);

            l_oldAddressLine3 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressLine3",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressLine4 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressLine4",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_oldAddressLine4 =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressLine4",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressCity =
                getValidParam(p_guiRequest,
                            PpasServlet.C_FLAG_ALLOW_BLANK,
                            p_request,
                            "newAddressCity",
                            true,
                            "",
                            PpasServlet.C_TYPE_ANY);

            l_oldAddressCity =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressCity",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressPostalCode =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressPostalCode",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_oldAddressPostalCode =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressPostalCode",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_newAddressCountry =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "newAddressCountry",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_oldAddressCountry =
                getValidParam(p_guiRequest,
                             PpasServlet.C_FLAG_ALLOW_BLANK,
                             p_request,
                             "oldAddressCountry",
                             true,
                             "",
                             PpasServlet.C_TYPE_ANY);

            l_customerAddressDataSet = new CustomerAddressDataSet(p_guiRequest,
                                                                  1,
                                                                  0);

            if ((!l_newAddressLine1.equals(l_oldAddressLine1)) ||
                (!l_newAddressLine2.equals(l_oldAddressLine2)) ||
                (!l_newAddressLine3.equals(l_oldAddressLine3)) ||
                (!l_newAddressLine4.equals(l_oldAddressLine4)) ||
                (!l_newAddressCity.equals(l_oldAddressCity)) ||
                (!l_newAddressPostalCode.equals(l_oldAddressPostalCode)) ||
                (!l_newAddressCountry.equals(l_oldAddressCountry)))
            {
                l_paramChanged = true;

                l_customerAddressDataSet.addElement(new CustomerAddressData(
                                                        p_guiRequest,
                                                        l_newAddressType,
                                                        l_newAddressDescription,
                                                        l_newAddressLine1,
                                                        l_newAddressLine2,
                                                        l_newAddressLine3,
                                                        l_newAddressLine4,
                                                        l_newAddressCity,
                                                        l_newAddressPostalCode,
                                                        l_newAddressCountry));
            }

            // Locate "Contact" data.
            l_customerContactDataSet = new CustomerContactDataSet(p_guiRequest,
                                               CcCustomerDetailsScreenData
                                                .C_MAX_NUMBER_CONTACT_TYPES,
                                               0);

            l_contactTypesV = i_configService.getAvailableContactTypes(l_market).getDataV();

            if (l_contactTypesV.size() > 0)
            {
                for (int i = 0; i < l_contactTypesV.size(); i++)
                {
                    if (i < CcCustomerDetailsScreenData.C_MAX_NUMBER_CONTACT_TYPES)
                    {
                        l_newContact =
                            getValidParam(p_guiRequest,
                                       PpasServlet.C_FLAG_ALLOW_BLANK,
                                       p_request,
                                       "newContact_"
                                        + (((MiscCodeData)l_contactTypesV.get(i)).getCode()),
                                       true,
                                       "",
                                       PpasServlet.C_TYPE_ANY);

                        l_oldContact =
                            getValidParam(p_guiRequest,
                                       PpasServlet.C_FLAG_ALLOW_BLANK,
                                       p_request,
                                       "oldContact_"
                                        + (((MiscCodeData)l_contactTypesV.get(i)).getCode()),
                                       true,
                                       "",
                                       PpasServlet.C_TYPE_ANY);


                        if (!l_newContact.equals(l_oldContact))
                        {
                            l_paramChanged = true;
                            l_customerContactDataSet.addElement(new
                                      CustomerContactData(
                                         p_guiRequest,
                                         (((MiscCodeData)l_contactTypesV.get(i)).getCode()),
                                         (((MiscCodeData)l_contactTypesV.get(i)).getDescription()),
                                         l_newContact));
                        }
                    }
                }
            }

        } // end try

        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

        } // end catch


        // Attempt to update customer details
        if (l_paramChanged)
        {
            if (p_screenData.hasUpdateError())
            {
                l_guiResponse =
                             new GuiResponse(p_guiRequest,
                                      GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                      new Object[] {"customer details changed"},
                                      GuiResponse.C_SEVERITY_WARNING);

            }
            else
            {
                // Perform update of customer details
                l_customerDetailsData = new CustomerDetailsData(
                                                      p_guiRequest,
                                                      l_newTitle,
                                                      l_newFirstName,
                                                      l_newMiddleInitial,
                                                      l_newLastName,
                                                      l_newDateOfBirth,
                                                      l_newGender,
                                                      l_newSsn,
                                                      l_newDrivingLicenceNumber,
                                                      l_newCompanyName);

                l_customerDetailsData.setAddresses(l_customerAddressDataSet);

                l_customerDetailsData.setContacts(l_customerContactDataSet);

                l_guiResponse =
                    i_ccCustomerDetailsService.updateDetails(
                                                         p_guiRequest,
                                                         i_timeout,
                                                         l_customerDetailsData);

                // Return last updated address to maintain screen focus.
                p_screenData.setLastUpdatedAddress(l_newAddressType);

            }

            // Return response to screen.
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);

            // Get the current time to use in display message.
            l_currentTime = DatePatch.getDateTimeNow();


            // Report on the success of the last update.
            if (p_screenData.hasUpdateError())
            {
                p_screenData.setInfoText("Failed Update at " + l_currentTime);
            }
            else
            {
                p_screenData.setInfoText("Update succeeded at " + l_currentTime);
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Leaving " + C_METHOD_updateDetails);
        }

        return;

    } // end private void updateDetails(...)
} // end CcCustomerDetailsServlet class
