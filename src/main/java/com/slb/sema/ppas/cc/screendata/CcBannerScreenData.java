////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcBannerScreenData.java
//      DATE            :       22-Aug-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1499/6262
//                              PRD_PPAK00_GEN_CA_379
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Screen Data object for the 
//                              Banner bar.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 28/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
// 26/05/06 | S James    | Add hasMsisdnRoutingAccess mthd | PpacLon#2188/8944
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Banner bar.
 */
public class CcBannerScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcBannerScreenData(GuiContext p_guiContext,
                              GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** 
      * Returns <code>true</code> if the Operator has access to the 
      * New Subscription screen.  Otherwise returns <code>false</code>.
      * @return True if the operator has access to the New Subscriber screen.
      */
    public boolean hasNewSubsScreenAccess()
    {
        return (i_gopaData.hasNewSubsScreenAccess());
    }
    
    /** 
      * Returns <code>true</code> if the Operator has access to the 
      * adjustment approval screen, otherwise returns <code>false</code>.
      * @return True if the operator has access to the adjustment approval screen.
      */
    public boolean hasAdjustmentApprovalAccess()
    {
        return i_gopaData.hasAdjApprovalScreenAccess();
    }

    /** 
     * Returns <code>true</code> if the Operator has access to the 
     * MSISDN routing screen, otherwise returns <code>false</code>.
     * @return True if the operator has access to the MSISDN routing screen.
     */
   public boolean hasMsisdnRoutingAccess()
   {
       return i_gopaData.hasRoutingScreenAccess();
   }

   /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return (true);
    }

    /**
     * Returns the user name of the operator who is logged in.
     * @return Operator identifier.
     */
    public String getUsername()
    {
        return i_guiRequest.getGuiSession().getUsername();
    }
} // end of CcBannerScreenData class
