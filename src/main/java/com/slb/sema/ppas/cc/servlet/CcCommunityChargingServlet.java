////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCommunityChargingServlet.Java
//      DATE            :       19-Jul-2004
//      AUTHOR          :       Ajmal Bangash
//      REFERENCE       :       PpacLon#390/3279
//                              PRD_ASCS00_GEN_CA_015
//
//      COPYRIGHT       :       ATOS Origin 2004
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Community Charging screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcCommunityChargingScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.dataclass.CommunitiesIdListData;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;


/**
 * Servlet to handle requests from the Community Charging screen.
 */
public class CcCommunityChargingServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCommunityChargingServlet";
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Community Charging Service to be used by this servlet. */
    private CcAccountService i_ccAccountService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * Constructor, only calls the superclass constructor.
     */
    public CcCommunityChargingServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START | WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10000, this,
                           "Constructing/ed " + C_CLASS_NAME );
        }

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** 
     * Service method to control requests and responses to the JSP script
     * for the community charging screen service.
     * @param p_request the request
     * @param p_response the response
     * @param p_httpSession current HttpSession
     * @param p_guiRequest a GuiRequest
     * @return forwarding URL
     */
    protected String doService(HttpServletRequest     p_request,
                               HttpServletResponse    p_response,
                               HttpSession            p_httpSession,
                               GuiRequest             p_guiRequest)
    {
        String                         l_command     = null;
        String                         l_forwardUrl  = null;
        GopaGuiOperatorAccessData      l_gopaData    = null;
        CcCommunityChargingScreenData  l_screenData  = null;

        boolean                        l_accessGranted = false;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 40000, this,
                           "ENTERED " + C_METHOD_doService);
        }

        // Construct CcCommunityChargingService if it does not already exist 
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create the community charging screen data object
        l_screenData = new CcCommunityChargingScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "UPDATE"});

            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ( (l_command.equals("GET_SCREEN_DATA")) &&
                  l_gopaData.hasCommunityChargingScreenAccess() )
            {
                l_accessGranted = true;
            }
            else if ( (l_command.equals("UPDATE")) &&
                       l_gopaData.communityChargingUpdateIsPermitted() )
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access
                GuiResponse l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_screenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";
            }
            else
            {
                getBusinessConfigData(l_screenData);

                if (l_command.equals("UPDATE"))
                {
                    updateCommunities( p_guiRequest, p_request, l_screenData);
                }

                getCommunities( p_guiRequest, l_screenData);

                l_forwardUrl = "/jsp/cc/cccommunitycharging.jsp";
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 40100, this,
                               "Caught exception: " + l_ppasSE);
            }

            l_screenData = new CcCommunityChargingScreenData(i_guiContext, p_guiRequest);

            GuiResponse l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_screenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
        }

        p_request.setAttribute("p_guiScreenData", l_screenData);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 40200, this,
                           "LEAVING " + C_METHOD_doService +  ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // Private Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCommunities = "getCommunities";
    /**
     * Method to retrieve the account's community charging identifiers.
     * @param p_guiRequest a GuiRequest
     * @param p_screenData a CcCommunityChargingScreenData
     */
    private void getCommunities( GuiRequest                    p_guiRequest,
                                 CcCommunityChargingScreenData p_screenData )
    {
        CcAccountDataResponse  l_accountDataResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 50000, this,
                           "ENTERED " + C_METHOD_getCommunities);
        }

        l_accountDataResponse = i_ccAccountService.getFullData(p_guiRequest,
                                                               i_timeout);

        p_screenData.addRetrievalResponse( p_guiRequest,
                                           l_accountDataResponse );

        if (l_accountDataResponse.isSuccess())
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_APPLICATION_DATA,
                               p_guiRequest, C_CLASS_NAME, 50050, this,
                               "Communities List: " +
                               l_accountDataResponse.getAccountData().getCommunitiesIdList());
            }

            p_screenData.setCommunityChargingData(
                        l_accountDataResponse.getAccountData().getCommunitiesIdList());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 50100, this,
                           "Leaving " + C_METHOD_getCommunities);
        }
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateCommunities = "updateCommunities";
    /**
     * Method to update (add, delete, replace) a list of community charging
     * identifiers for an account.
     * @param p_guiRequest a GuiRequest
     * @param p_request the HttpServletRequest containing the URL
     * @param p_screenData the screen data
     * @throws PpasServletException if an error occurs parsing the request parameters
     */
    private void updateCommunities( GuiRequest                     p_guiRequest,
                                    HttpServletRequest             p_request,
                                    CcCommunityChargingScreenData  p_screenData )
        throws PpasServletException
    {
        PpasDateTime    l_currentTime = null;
        GuiResponse     l_guiResponse = null;
        String          l_oldCommunity = null;
        String          l_newCommunity = null;
        Vector          l_oldCommunitiesDataV = new Vector(PpasAccountService.C_NB_COMMUNITY_ID_IN_LIST);
        Vector          l_newCommunitiesDataV = new Vector(PpasAccountService.C_NB_COMMUNITY_ID_IN_LIST);
        CommunitiesIdListData l_oldCommunitiesIdList = null;
        CommunitiesIdListData l_newCommunitiesIdList = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME, 60000, this,
                           "ENTERED " + C_METHOD_updateCommunities);
        }


        try
        {
            // Loop on the number of possible items in the old list
            for (int l_count = 1; 
                     l_count <= PpasAccountService.C_NB_COMMUNITY_ID_IN_LIST && 
                     !p_screenData.hasUpdateError(); 
                     l_count++)
            {
                l_oldCommunity = getValidParam(
                                        p_guiRequest,
                                        PpasServlet.C_FLAG_ALLOW_BLANK,
                                        p_request,
                                        "p_oldCommId_" + l_count,
                                        true,
                                        "",
                                        PpasServlet.C_TYPE_ANY);

                // Don't add blanks to array
                if (l_oldCommunity.length() > 0 && (!l_oldCommunity.equals(p_screenData.getBlankValue())))
                {
                    l_oldCommunitiesDataV.add(l_oldCommunity);
                }

                l_newCommunity = getValidParam(
                                        p_guiRequest,
                                        PpasServlet.C_FLAG_ALLOW_BLANK,
                                        p_request,
                                        "p_newCommId_" + l_count,
                                        true,
                                        "",
                                        PpasServlet.C_TYPE_ANY);

                // Don't add blanks to array
                if (l_newCommunity.length() > 0 && (!l_newCommunity.equals(p_screenData.getBlankValue())))
                {
                    l_newCommunitiesDataV.add(l_newCommunity);
                }
            }
            
            try
            {
                l_oldCommunitiesIdList = new CommunitiesIdListData(null, l_oldCommunitiesDataV);
            }
            catch (PpasParseException l_pPe)
            {
                // Generate a new servlet exception
                PpasServletException l_pSEx = 
                    new PpasServletException(
                        C_CLASS_NAME,
                        C_METHOD_doService,
                        60010,
                        this,
                        p_guiRequest,
                        0,
                        ServletKey.get().invalidOldComminityList(l_oldCommunitiesDataV.toString()),
                        l_pPe);
                
                i_logger.logMessage(l_pSEx);
                
                throw l_pSEx;
            }
            
            try
            {
                l_newCommunitiesIdList = new CommunitiesIdListData(null, l_newCommunitiesDataV);
            }
            catch (PpasParseException l_pPe)
            {
                // Generate a new servlet exception
                PpasServletException l_pSEx = 
                    new PpasServletException(
                        C_CLASS_NAME,
                        C_METHOD_doService,
                        60020,
                        this,
                        p_guiRequest,
                        0,
                        ServletKey.get().invalidNewComminityList(l_newCommunitiesDataV.toString()),
                        l_pPe);
                
                i_logger.logMessage(l_pSEx);
                
                throw l_pSEx;
            }

        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 60030, this,
                               "Exception parsing parameters: " + l_pSE);
            }

            // Convert the exception into a suitable response...
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            // Add the response to the screen data object as a normal update type response.
            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                            new Object[] {"update communities"},
                                            GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            l_guiResponse = i_ccAccountService.
                                updateCommunity(
                                    p_guiRequest,
                                    i_timeout,
                                    l_oldCommunitiesIdList,
                                    l_newCommunitiesIdList);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText("Failed community charging update at " + l_currentTime);
        }
        else
        {
            p_screenData.setInfoText("Successful community charging update at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME, 60100, this,
                           "Leaving " + C_METHOD_updateCommunities);
        }
    }

    /**
     * Obtains the business configuration data required for this screen.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void getBusinessConfigData(CcCommunityChargingScreenData p_screenData)
    {
        CochCommunityChargingDataSet  l_availableCommunities = null;

        l_availableCommunities = i_configService.getAvailableCommunityIds();
        p_screenData.setConfiguredCommunities(l_availableCommunities);

        return;

    }
}
