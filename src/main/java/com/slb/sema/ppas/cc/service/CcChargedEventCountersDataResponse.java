////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcChargedEventCountersScreenData.java
//      DATE            :       08-Jun-2005
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1560/6587
//                              PRD_ASCS_GEN_CA_49
//
//      COPYRIGHT       :       WM-Data 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the Charged
//                              Event Counters
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccumulatorEnquiryResultData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Wrapper for <code>AccumulatorEnquiryResultData</code> used within the 
 * <code>CcChargedEventCountersScreenData</code> object.
 */
public class CcChargedEventCountersDataResponse  extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcChargedEventCountersDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccountData instance contained in this response. */
    private AccumulatorEnquiryResultData i_accumEnqResultData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * No Locale required for this constructor - always uses default Locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_accumEnqResultData Details about the customer.
     */
    public CcChargedEventCountersDataResponse(GuiRequest                   p_guiRequest,
                                              String                       p_messageKey,
                                              Object[]                     p_messageParams,
                                              int                          p_messageSeverity,
                                              AccumulatorEnquiryResultData p_accumEnqResultData)
    {
        super(p_guiRequest,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_accumEnqResultData = p_accumEnqResultData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }
    }
    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------
    /**
     * Returns a reference to the AccumulatorEnquiryResultData instance contained within
     * this response object.
     * @return Set of customer detail data.
     */
    public AccumulatorEnquiryResultData getAccumulatorEnquiryResultData()
    {
        return i_accumEnqResultData;
    } 
}
