////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustPromoAllocService.java
//      DATE            :       12-Mar-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Customer Care Service that wrappers calls to
//                              the PpasCustPromoAllocService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
// 14/10/03 | M Erskine  | Knock-on change due to IS API   | PpacLon#57/550
//          |            | changes                         |
//----------+------------+---------------------------------+------------------
// 08/12/05 | K Goswami  | Changed DateTime to Date        | PpacLon#1058 
//          |            |                                 | CrRr_1058_7237_PromoAllocData.doc
//----------+------------+---------------------------------+--------------------
// 19.01.06 | MAGray     | Change to accept  boolean when  | PpacLon#1954/7831
//          |            | adding
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CustPromoAllocDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasCustPromoAllocService;
import com.slb.sema.ppas.is.isil.primitiveservice.CustPromoAllocPrimitiveService;
import com.slb.sema.ppas.util.logging.Logger;

/** Customer care service that wraps the Customer Promotion Allocations
 *  internal service. This service is used to retrieve and update details of
 *  customer's promotion plan allocations.
 */
public class CcCustPromoAllocService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustPromoAllocService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The internal service used by this Gui Service. */
    private PpasCustPromoAllocService i_custPromoAllocIS;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the Customer Care GUI Customer Promotion Plan
     *  Allocation internal service.
     *  This service is used to retrieve and update details of a subscriber's
     *  promotion plan allocations.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcCustPromoAllocService(
        GuiRequest p_guiRequest,
        long       p_flags,
        Logger     p_logger,
        GuiContext p_guiContext)
    {
        super (p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 43000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create internal customer promotion plan allocation service to be used by this service.
        i_custPromoAllocIS = new PpasCustPromoAllocService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 43090, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAllPromos = "getAllPromos";

    /**
     * A call to this method will return a CcCustPromoAllocDataSetResponse.
     * If the attempt to get all the promotion allocations for the subscriber
     * was successful, then this response contains a data set of
     * all the Promotion Plan Allocation details for the supplied Cust Id.
     * If the attempt to get all the promotion allocation for the subscriber
     * failed, then the key in the response determines the reason of the
     * failure.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @return CcCustPromoAllocDataSetResponse object containing the customer
     *         promotion allocations data if the service was successful. If
     *         the service was unsuccessful, then the key in this response
     *         defines the failure reason.
     */
    public CcCustPromoAllocDataSetResponse getAllPromos(
        GuiRequest p_guiRequest,
        long       p_timeoutMillis)
    {
        GuiResponse                     l_guiResponse        = null;
        CcCustPromoAllocDataSetResponse l_custPromosResponse = null;
        CustPromoAllocDataSet           l_custPromosDataSet  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 43100, this,
                "Entered " + C_METHOD_getAllPromos);
        }

        try
        {
            l_custPromosDataSet = i_custPromoAllocIS
                    .getAllPromos(p_guiRequest,
                                  p_timeoutMillis,
                                  CustPromoAllocPrimitiveService.C_FLAG_ALL); 

            l_custPromosResponse = new CcCustPromoAllocDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_custPromosDataSet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 43110, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(
                           p_guiRequest, 0, "get all the subscriber's promotion allocations", l_pSE);

            l_custPromosResponse = new CcCustPromoAllocDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),    // PpacLon#1/17
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 43190, this,
                "Leaving " + C_METHOD_getAllPromos);
        }

        return (l_custPromosResponse);

    }  // end method getAllPromos

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addAllocation = "addAllocation";
    /**
     * This method allocates the given promotion plan to the subscriber
     * defined in the input request.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_promId The identifier defining the promotion plan to be
     *                 allocated to the subscriber defined in the input request.
     * @param p_startDate The date at which the new allocation is to start.
     * @param p_endDate The date at which the new allocation is to end.
     * @param p_startNow Set to true if allocation is to start immediately.
     * @return GuiResponse whose status determines whether the service was
     *         successful or not. If the service was not successful, then the
     *         key in this GuiResponse defines the failure reason.
     */
    public GuiResponse addAllocation(
        GuiRequest p_guiRequest,
        long       p_timeoutMillis,
        String     p_promId,
        PpasDate   p_startDate,
        PpasDate   p_endDate,
        boolean    p_startNow
        )
    {
        GuiResponse   l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 43200, this,
                "Entered " + C_METHOD_addAllocation);
        }

        try
        {
            i_custPromoAllocIS.addPromo (p_guiRequest,
                                         p_timeoutMillis,
                                         p_promId,
                                         p_startDate,
                                         p_endDate,
                                         p_startNow);

            l_guiResponse = new GuiResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),    // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 43210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "add the promotion allocation", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 43290, this,
                "Leaving " + C_METHOD_addAllocation);
        }

        return (l_guiResponse);

    }  // end method addAllocation

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateAllocation = "updateAllocation";
    /**
     * This method updates the given promotion plan allocation.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_promId The identifier defining the promotion plan to be updated to (that is, new plan Id).
     * @param p_startDateTime The datetime at which the updated allocation is to start
     *                    (that is, new allocation start datetime).
     * @param p_endDateTime The datetime at which the updated allocation is to end
     *                  (i.e. new allocation end datetime).
     * @param p_oldPromId The identifier of the existing promotion plan
     *                    allocation which is to be updated.
     * @param p_oldStartDateTime The date/time that the Promotion Plan
     *                           Allocation currently starts on.
     * @param p_oldEndDateTime The date/time that the Promotion Plan
     *                         Allocation currently ends on.
     * @param p_startNow  True if the allocation should be updated to start immediately.
     * @param p_endNow If the updated end date = current date, passing true
     *                 as this parameter will end the allocation at current
     *                 time, passing false will end the allocation at the
     *                 end of the day.
     * @return GuiResponse whose status determines whether the service was
     *         successful or not. If the service was not successful, then the
     *         key in this GuiResponse defines the failure reason.
     */
    public GuiResponse updateAllocation(
        GuiRequest   p_guiRequest,
        long         p_timeoutMillis,
        String       p_promId,
        PpasDateTime p_startDateTime,
        PpasDateTime p_endDateTime,
        String       p_oldPromId,
        PpasDateTime p_oldStartDateTime,
        PpasDateTime p_oldEndDateTime,
        boolean      p_startNow,
        boolean      p_endNow)
    {
        GuiResponse   l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 43300, this,
                "Entered " + C_METHOD_updateAllocation);
        }

        try
        {
            i_custPromoAllocIS.updatePromo (p_guiRequest,
                                            p_timeoutMillis,
                                            p_promId,
                                            p_startDateTime,
                                            p_endDateTime,
                                            p_oldPromId,
                                            p_oldStartDateTime,
                                            p_oldEndDateTime,
                                            p_startNow,
                                            p_endNow);

            l_guiResponse = new GuiResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 43310, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException (p_guiRequest, 0, "update the promotion allocation", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 43390, this,
                "Leaving " + C_METHOD_updateAllocation);
        }

        return (l_guiResponse);

    }  // end method updateAllocation

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_removeAllocation = "removeAllocation";
    /**
     * This method removes the given promotion plan allocation.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_startDateTime The date/time at which the allocation to be
     *                        removed starts.
     * @return GuiResponse whose status determines whether the service was
     *         successful or not. If the service was not successful, then the
     *         key in this GuiResponse defines the failure reason.
     */
    public GuiResponse removeAllocation(
        GuiRequest   p_guiRequest,
        long         p_timeoutMillis,
        PpasDateTime p_startDateTime)
    {
        GuiResponse   l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 43400, this,
                "Entered " + C_METHOD_removeAllocation);
        }

        try
        {
            i_custPromoAllocIS.removePromo (p_guiRequest, p_timeoutMillis, p_startDateTime);

            l_guiResponse = new GuiResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 43410, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException (p_guiRequest, 0, "delete the promotion allocation", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 43490, this,
                "Leaving " + C_METHOD_removeAllocation);
        }

        return (l_guiResponse);

    }  // end method removeAllocation

} // end public class CcCustPromoAllocService
