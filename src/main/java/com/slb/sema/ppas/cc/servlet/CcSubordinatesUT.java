////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubordinatesUT.java
//      DATE            :       26-Jan-2007
//      AUTHOR          :       John Lee
//      REFERENCE       :       N/A
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for the GUI Subordinates screen. 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// <date>   | <name>     | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.HashMap;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;
import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework (v 1.5.4)
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcSubordinatesUT extends GuiTestTT
{
    /** Table id of the Subordinates screen - Add Subordinate table. */
    private static final String C_TABLE_ID_SUBORDINATES_ADD_SUBORDINATE = "subordinatesAddSubordinate";
    
    /** Table id of the Subordinates screen - Master Details table. */
    private static final String C_TABLE_ID_SUBORDINATES_MASTER_DETAILS = "subordinatesMasterDetails";
    
    /** Table id of the Subordinates screen - Title Toggle table. */
    private static final String C_TABLE_ID_SUBORDINATES_TITLE_TOGGLE = "subordinatesTitleToggle";
    
    /** Table id of the Subordinates screen - History table. */
    private static final String C_TABLE_ID_SUBORDINATES_HISTORY = "subordinatesHistory";
    
    /** Expected title returned when Subordinates screen is requested. */
    private static final String C_SUBORDINATES_SCREEN_EXPECTED_TITLE = "PPAS - Subordinate Accounts";
    
    /** The format of the msisdn to use for these tests. */
    private MsisdnFormat c_msisdnFormat = c_ppasContext.getMsisdnFormatInput();
    
    /** The prefix used to form a full msisdn. */
    private String c_msisdnPrefix = "+" + c_msisdnFormat.getCountryCode() + " (0) ";
    
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcSubordinatesUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        say("Entered CcSubordinatesUT.setUp()");
        super.setUp(true);
    }
    
    /**
     * @ut.when An attempt is made to load the subordinate screen for a standalone subscriber
     * @ut.then A successful response is returned containing the correct page and details.
     */
     public void testLoadSubordinatesScreenForStandaloneSuccess()
     {        
         beginOfTest("Start testLoadSubordinatesScreenForStandaloneSuccess");
         
         //Install MSISDN and load Maintain Subsriber Screen
         installAndLoadSubScreen(1);
         
         //Request Subordinates Screen
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Check screen displays correct sections for a non-subordinate msisdn 
         validateSubordinatesScreenMode(false);
         
         try
         {
             //Check the msisdn does not have any subordinates yet
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_HISTORY);
             verifyRowInTable(C_TABLE_ID_SUBORDINATES_HISTORY, 1, new String[] {"", "", "", "", ""});
             fail("Row should not exist - expect ArrayOutOfBoundsException!");
         }
         catch (ArrayIndexOutOfBoundsException l_e)
         {
             // Ignore and continue
         }
         
         endOfTest();
     }
     
     /**
     * @ut.when An attempt is made to make a standalone msisdn a subordinate of another standalone
     * @ut.then A successful response is returned containing the correct page and details.
     */
     public void testAddSubordinateSuccess()
     {        
         beginOfTest("Start testAddSubordinateSuccess");
      
         //Install the 1st msisdn
         BasicAccountData l_subAcctData = installAndLoadSubScreen(1);
         Msisdn l_subMsisdn = l_subAcctData.getMsisdn();
      
         //Install the 2nd msisdn
         BasicAccountData l_mastAcctData = installAndLoadSubScreen(1);
         Msisdn l_mastMsisdn = l_mastAcctData.getMsisdn();
      
         //Request the Subordinates screen
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
      
         //Make the 1st msisdn a subordinate of the 2nd msisdn
         HashMap l_params = new HashMap();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
        
         //Check the master msisdn has correct subordinates
         say("Verifying table: " + C_TABLE_ID_SUBORDINATES_HISTORY);
      
         verifyRowInTable(C_TABLE_ID_SUBORDINATES_HISTORY, 1,
                          new String[] {c_msisdnPrefix + c_msisdnFormat.format(l_subMsisdn), 
                                        l_subAcctData.getCustId(), l_subAcctData.getCustStatusStr(), "", ""});

         endOfTest();
     }
     
     /**
      * @ut.when An attempt is made to load the subordinate screen for a subordinate subscriber
      * @ut.then A successful response is returned containing the correct page and details.
      */
     public void testLoadSubordinatesScreenForSubordinateSuccess()
     {        
         beginOfTest("Start testLoadSubordinatesScreenForSubordinateSuccess");
         
         //Install the 1st msisdn
         BasicAccountData l_subAcctData1 = installAndLoadSubScreen(1);
         Msisdn l_subMsisdn1 = l_subAcctData1.getMsisdn();
         
         //Install the 2nd msisdn
         BasicAccountData l_subAcctData2 = installAndLoadSubScreen(1);
         Msisdn l_subMsisdn2 = l_subAcctData2.getMsisdn();
         
         //Install the 3rd msisdn
         BasicAccountData l_mastAcctData = installAndLoadSubScreen(1);
         Msisdn l_mastMsisdn = l_mastAcctData.getMsisdn();
         
         //Request the Subordinates screen
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 1st msisdn a subordinate of the 3rd msisdn
         HashMap l_params = new HashMap();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn1));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 2nd msisdn a subordinate of the 3rd msisdn
         l_params.clear();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn2));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Load Subscriber Details screen for the 1st subordinate msisdn
         //Then request the Subordinates screen
         loadMaintainSubsScreenForMsisdn(l_subMsisdn1);
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
           
         //Check screen displays correct sections for a subordinate msisdn 
         validateSubordinatesScreenMode(true);
         
         //Check the Master Subscriber details are correct for the subordinate
         say("Verifying table: " + C_TABLE_ID_SUBORDINATES_MASTER_DETAILS);
         
         verifyRowInTable(C_TABLE_ID_SUBORDINATES_MASTER_DETAILS, 1,
                          new String[] {"MSISDN:", "", c_msisdnPrefix + c_msisdnFormat.format(l_mastMsisdn)});
             
         //Check the other subordinates are correct
         say("Verifying table: " + C_TABLE_ID_SUBORDINATES_HISTORY);
         
         verifyRowInTable(C_TABLE_ID_SUBORDINATES_HISTORY, 1,
             new String[] {c_msisdnPrefix + c_msisdnFormat.format(l_subMsisdn2), 
                           l_subAcctData2.getCustId(), l_subAcctData2.getCustStatusStr(), "", ""});
              
         endOfTest();
     }
     
     /**
      * @ut.when An attempt is made to make a master msisdn a subordinate of another msisdn
      * @ut.then A successful response is returned containing the correct error message.
      */
     public void testAddMasterAsSubordinateFailure()
     {        
         beginOfTest("Start testAddMasterAsSubordinateFailure");
         
         //Install the 1st msisdn
         BasicAccountData l_subAcctData = installAndLoadSubScreen(1);
         Msisdn l_subMsisdn = l_subAcctData.getMsisdn();
         
         //Install the 2nd msisdn
         BasicAccountData l_mastAcctData = installAndLoadSubScreen(1);
         Msisdn l_mastMsisdn = l_mastAcctData.getMsisdn();
         
         //Install the 3rd msisdn
         BasicAccountData l_aloneAcctData = installAndLoadSubScreen(1);
         Msisdn l_aloneMsisdn = l_aloneAcctData.getMsisdn();
         
         //Request the Subordinates screen
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 1st msisdn a subordinate of the 2nd msisdn
         HashMap l_params = new HashMap();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Load Subscriber Details screen for the 3rd msisdn (standalone)
         //Then request the Subordinates screen
         loadMaintainSubsScreenForMsisdn(l_aloneMsisdn);
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 2nd msisdn (master) a subordinate of the 3rd msisdn (standalone)
         l_params.clear();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_aloneMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));
         validateResponse("<00033>  MSISDN " + c_msisdnPrefix + c_msisdnFormat.format(l_mastMsisdn) +
                          " cannot be made into a subordinate because it is a master account with subordinates of its own.",
                          null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         try
         {
             //Check the msisdn does not have any subordinates
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_HISTORY);
             verifyRowInTable(C_TABLE_ID_SUBORDINATES_HISTORY, 1, new String[] {"", "", "", "", ""});
             fail("Row should not exist - expect ArrayOutOfBoundsException!");
         }
         catch (ArrayIndexOutOfBoundsException l_e)
         {
             // Ignore and continue
         }
         
         endOfTest();
     }
     
     /**
      * @ut.when An attempt is made to make a subordinate msisdn a subordinate of another msisdn
      * @ut.then A successful response is returned containing the correct error message.
      */
     public void testAddAlreadySubordinateFailure()
     {        
         beginOfTest("Start testAddAlreadySubordinateFailure");
         
         //Install the 1st msisdn
         BasicAccountData l_subAcctData = installAndLoadSubScreen(1);
         Msisdn l_subMsisdn = l_subAcctData.getMsisdn();
         
         //Install the 2nd msisdn
         BasicAccountData l_mastAcctData = installAndLoadSubScreen(1);
         Msisdn l_mastMsisdn = l_mastAcctData.getMsisdn();
         
         //Install the 3rd msisdn
         BasicAccountData l_aloneAcctData = installAndLoadSubScreen(1);
         Msisdn l_aloneMsisdn = l_aloneAcctData.getMsisdn();
         
         //Request the Subordinates screen
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 1st msisdn a subordinate of the 2nd msisdn
         HashMap l_params = new HashMap();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_mastMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Load Subscriber Details screen for the 3rd msisdn (standalone)
         //Then request the Subordinates screen
         loadMaintainSubsScreenForMsisdn(l_aloneMsisdn);
         sendRequest(constructGetRequest("cc/Subordinates", "GET_SCREEN_DATA"));        
         validateResponse(null, null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         //Make the 1st msisdn (already a subordinate) a subordinate of the 3rd msisdn (standalone)
         l_params.clear();
         l_params.put("p_masterMsisdn", c_msisdnFormat.format(l_aloneMsisdn));
         l_params.put("p_subMsisdn", c_msisdnFormat.format(l_subMsisdn));
         sendRequest(constructGetRequest("cc/Subordinates", "ADD_SUBORDINATE", l_params));  
         validateResponse("<00031>  MSISDN " + c_msisdnPrefix + c_msisdnFormat.format(l_subMsisdn) +
                          " is already a subordinate account.", null, C_SUBORDINATES_SCREEN_EXPECTED_TITLE);
         
         try
         {
             //Check the msisdn does not have any subordinates
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_HISTORY);
             verifyRowInTable(C_TABLE_ID_SUBORDINATES_HISTORY, 1, new String[] {"", "", "", "", ""});
             fail("Row should not exist - expect ArrayOutOfBoundsException!");
         }
         catch (ArrayIndexOutOfBoundsException l_e)
         {
             // Ignore and continue
         }
         
         endOfTest();
     }
     
     /**
      * Validates that the screen is in the correct mode depending on whether the msisdn
      * is a subordinate or not.
      * @param isSubordinateMsisdn Whether the msisdn is a subordinate or not.
      */
     private void validateSubordinatesScreenMode(boolean isSubordinateMsisdn)
     {
         if (isSubordinateMsisdn)
         {
             //Check the 'Master Subscriber' section is displayed
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_MASTER_DETAILS);
             verifyCellInTable(C_TABLE_ID_SUBORDINATES_MASTER_DETAILS, 0, 0, "Master Subscriber");
           
             //Check the 'Other Subordinate Subscribers' section is displayed
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_TITLE_TOGGLE);
             verifyCellInTable(C_TABLE_ID_SUBORDINATES_TITLE_TOGGLE, 0, 1, "Other Subordinate Subscribers"); 
         }
         else
         {
             //Check the 'Add Subordinate' section is displayed
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_ADD_SUBORDINATE);
             verifyCellInTable(C_TABLE_ID_SUBORDINATES_ADD_SUBORDINATE, 0, 0, "Add Subordinate");
                
             //Check the 'Subordinate Subscribers' section is displayed
             say("Verifying table: " + C_TABLE_ID_SUBORDINATES_TITLE_TOGGLE);
             verifyCellInTable(C_TABLE_ID_SUBORDINATES_TITLE_TOGGLE, 0, 1, "Subordinate Subscribers");
         }
     }
    
    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        say(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcSubordinatesUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
}