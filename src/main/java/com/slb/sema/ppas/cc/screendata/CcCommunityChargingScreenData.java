////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCommunityChargingScreenData.java
//      DATE            :       19-Jul-2004
//      AUTHOR          :       Ajmal Bangash
//      REFERENCE       :       PpacLon#390/3279
//                              PRD_ASCS00_GEN_CA_015
//
//      COPYRIGHT       :       ATOS Origin 2004
//
//      DESCRIPTION     :       Gui Screen Data object for the CommunityCharging 
//                              screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 06/09/06 | L.Byrne    | List of community ids returned  | Ppacs#22613/9926
//          |            | from the SDP might not          |
//          |            | correspond to the one stored in |
//          |            | ASCS                            |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
//import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.CommunitiesIdListData;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAccountService;

/**
 * Gui Screen Data object for the CommunityCharging details screen.
 */
public class CcCommunityChargingScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Private Constants.
    //--------------------------------------------------------------------------

    /** String value of a blank community ID. */
    private static final String c_blank_value = "0";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Info text to be displayed on the GUI. */
    private String i_infoText = "";

    /**
     * Stores the subscriber data returned from the SDP, containing the active
     * communities for this account.
     */
    private CommunitiesIdListData i_currentCommunities;

    /** Stores a data set of all configured community charging groups. */
    private CochCommunityChargingDataSet i_cochCommunities;


    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /**
     * Simple constructor.
     * @param p_guiContext  GUI context object
     * @param p_guiRequest  GUI request object
     */
    public CcCommunityChargingScreenData( GuiContext p_guiContext,
                                          GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------
    
    /**
     * Returns the available community charging communities, wrappered in this object.
     * @return Data set of available community charging communities.
     */
    public CochCommunityChargingDataSet getConfiguredCommunities()
    {
        return (i_cochCommunities);
    }
    
    /**
     * Stores the available community charging communities.
     * @param p_communities Data set of available community charging communities.
     */
    public void setConfiguredCommunities(CochCommunityChargingDataSet p_communities)
    {
        i_cochCommunities = p_communities;
    }

    /**
     * Returns the current list of community charging identifiers for this account.
     * Returns null if there aren't any.
     * @return Set of Community Charging identifiers for this account. 
     */
    public CommunitiesIdListData getCommunityChargingData()
    {
        return (i_currentCommunities);
    }

    /**
     * Stores the current list of community charging identifiers for this account.
     * @param p_currentCommunities Set of CommunityChargingData objects. 
     */
    public void setCommunityChargingData(CommunitiesIdListData p_currentCommunities)
    {
        i_currentCommunities = p_currentCommunities;
    }

    /**
     * Returns the community charging identifier indexed by the input number.
     * Returns null if there aren't any.
     * @param p_index The Vector position at which the community ID is required.
     * @return Formatted community charging identifier at the specified index. 
     */
    public String getCommunityId(int p_index)
    {
        Vector l_communityIds = null;
        String l_commId = c_blank_value;
        
        if (i_currentCommunities != null)
        {
            l_communityIds = i_currentCommunities.getCommunitiesIdList();
        }

        if ( (l_communityIds != null) &&
             (!l_communityIds.isEmpty()) &&
             (l_communityIds.size() > p_index) )
        {
            l_commId = ( (Integer)l_communityIds.get(p_index) ).toString();
        }

        return ((l_commId.length() == 0) ? c_blank_value : l_commId);
    }

    /**
     * Returns the community charging description indexed by the input number.
     * Returns  ablank string if there aren't any.
     * @param p_index The Vector position at which the community ID description is required.
     * @return Description of the Community Charging ID at the specified index. 
     */
    public String getCommunityDesc(int p_index)
    {
        CochCommunityChargingData  l_communityData = null;
        String                     l_commId = null;
        String                     l_commDesc = "";

        l_commId = getCommunityId(p_index);

        if (!l_commId.equals(c_blank_value))
        {
            l_communityData = i_cochCommunities.getCommunityChargingData(Integer.parseInt(l_commId));

            // In case community list returned from SDP does not correspond to list stored in ASCS
            if ( l_communityData != null )
            {
                l_commDesc = l_communityData.getCommunityChgDesc();
            }
        }

        return (l_commDesc);
    }

    /**
     * Returns true if the Operator making the request is permitted to make an
     * update to CommunityCharging data. Otherwise returns false.
     * @return True if the operator can make an CommunityCharging update.
     */
    public boolean isUpdateAllowed()
    {
        return i_gopaData.communityChargingUpdateIsPermitted();
    }

    /**
     * Returns the configured maximum number of communities allowed per subscriber.
     * @return Number of allowed communities currently configured.
     */
    public int getMaxCommunitiesCount()
    {
        return PpasAccountService.C_NB_COMMUNITY_ID_IN_LIST;
    }

    /** 
     * Gets the text giving information on the status of any CommunityCharging 
     * update request that was attempted.
     * @return Text associated with success or failure of CommunityCharging update.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** 
     * Sets the text giving information on the status of any CommunityCharging 
     * update request that was attempted.
     * @param p_text Text associated with success or failure of CommunityCharging update.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** 
     * Gets the string value of a blank community ID (i.e no ID).
     * @return Text returned by this object when a community ID is not assigned.
     */
    public String getBlankValue()
    {
        return (c_blank_value);
    }
}

