////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccountDataResponse.java
//      DATE            :       29-Jan-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PRD_PPAK00_DEV_IN_38
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Response returned by a GUI service method that
//                              is a container for AccountData.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response returned by a GUI service method that contains an 
 * AccountData object.
 */
public class CcAccountDataResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAccountDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccountData instance contained in this response. */
    private AccountData i_accountData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * No Locale required for this constructor - always uses default Locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_accountData   Set of Accounts.
     */
    public CcAccountDataResponse(GuiRequest  p_guiRequest,
                                 String      p_messageKey,
                                 Object[]    p_messageParams,
                                 int         p_messageSeverity,
                                 AccountData p_accountData)
    {
        this(p_guiRequest,
             null,
             p_messageKey,
             p_messageParams,
             p_messageSeverity,
             p_accountData);
    }

    /** 
     * Uses the Locale passed in to construct text of message.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_accountData   Set of Accounts.
     */
    public CcAccountDataResponse(GuiRequest  p_guiRequest,
                                 Locale      p_messageLocale,
                                 String      p_messageKey,
                                 Object[]    p_messageParams,
                                 int         p_messageSeverity,
                                 AccountData p_accountData)
    {
        super(p_guiRequest,
              p_messageLocale,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_accountData = p_accountData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the AccountData instance contained within
     * this response object.
     * @return Set of accounts.
     */
    public AccountData getAccountData()
    {
        return i_accountData;
    } // end public AccountData getAccountData()
}
