////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdditionalInfoService.java
//      DATE            :       7-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1236/5006
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasAdditionalInfoService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                       | REFERENCE
//----------+------------+-----------------------------------+------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues  | PpacLon#1/17
//----------+------------+-----------------------------------+------------------
// 23/10/03 | E.P. Hebe  | Add extra parameter in method     |
//          |            | modifyAdditionalInfo while calling|
//          |            | PPassAdditionalInfoService        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.util.logging.Logger;
import java.util.Vector;

/**
 * Gui Service that wrappers calls to PpasAdditionalInfoService.
 */
public class CcAdditionalInfoService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdditionalInfoService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasAdditionalInfoService i_addInfoService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcAdditionalInfoService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcAdditionalInfoService(
                            GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext )
    {
        super( p_guiRequest, p_flags, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create internal additional info service to be used by this service.
        i_addInfoService = new PpasAdditionalInfoService(p_guiRequest, p_logger, p_guiContext );

        // No service specific keys for this service.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_modifyAdditionalInfo = "modifyAdditionalInfo";
    /**
     * Modifys the subscribers Additional Info.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_miscFieldV Vector containing all the subscribers additional information.
     * @return GuiResponse object containing the status of the requested service.
     */
    public GuiResponse modifyAdditionalInfo( GuiRequest p_guiRequest,
                                             long       p_timeoutMillis,
                                             Vector     p_miscFieldV )
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 30000, this,
                "Entered " + C_METHOD_modifyAdditionalInfo);
        }

        try
        {
            i_addInfoService.modifyAdditionalInfo(p_guiRequest, p_timeoutMillis, p_miscFieldV);

            l_guiResponse
                = new GuiResponse(
                          p_guiRequest,
                          p_guiRequest.getGuiSession().
                            getSelectedLocale(),  // PpacLon#17
                          GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                          new Object[] {"update additional information fields"},
                          GuiResponse.C_SEVERITY_SUCCESS );

        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_MODERATE,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 30210, this,
                    "Caught PpasServiceException: " + e);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "modify additional information", e);

        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 31000, this,
                "Leaving " + C_METHOD_modifyAdditionalInfo);
        }

        return l_guiResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAdditionalInfo = "getAdditionalInfo";
    /**
     * Retrieves additional info data for the subscriber.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return A response object containing the additional information received
     *         by the subscriber if the service completed successfully. If the
     *         service was not successful, then the key in this response
     *         defines the reason for failure.
     */
    public CcAdditionalInfoDataResponse getAdditionalInfo(
                                             GuiRequest p_guiRequest,
                                             long       p_timeoutMillis )
    {
        CcAdditionalInfoDataResponse l_ccAddInfoDataResponse  = null;
        GuiResponse                  l_guiResponse            = null;
        AdditionalInfoData           l_addInfoData            = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getAdditionalInfo);
        }

        try
        {
            l_addInfoData = i_addInfoService.getAdditionalInfo(p_guiRequest, p_timeoutMillis);

            l_ccAddInfoDataResponse
                = new CcAdditionalInfoDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_addInfoData );
        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_MODERATE,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + e);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get additional infomation", e);

            l_ccAddInfoDataResponse =
                new CcAdditionalInfoDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getAdditionalInfo);
        }

        return l_ccAddInfoDataResponse;
    }
}
