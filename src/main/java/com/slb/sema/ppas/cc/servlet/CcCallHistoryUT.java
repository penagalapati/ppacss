////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCallHistoryUT.java
//      DATE            :       24-Aug-2006
//      AUTHOR          :       Andy Harris ()
//      REFERENCE       :       PpacLon#2586/9791
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for GUI Call History test. 
//                              
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;


import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.HTMLElement;
import com.meterware.httpunit.WebTable;

import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.CallHistoryTestDataTT;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcCallHistoryUT extends GuiTestTT
{   
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcCallHistoryUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        super.setUp(true);
    }
    
    /**
     * @ut.when An operator navigates to the Call History screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadCallHistoryScreen()
    {        
        beginOfTest("Start testLoadCallHistoryScreen");

        // Install MSISDN and load Maintain Subsriber Screen
        installAndLoadSubScreen(0);
        
        // Access/test specific screen
        sendRequest(constructGetRequest("cc/CallHistory", "GET_SCREEN_DATA", ""));

        validateResponse(null, null, "Call History");

        endOfTest();
    }

    // TODO Problem with HttpUnit accessing <div> tags, can not retrieve from call history table.
    /**
     * @ut.when call history is loaded and then retrieved.
     * @ut.then A successful response is returned containing the correct data.
     */
    public void atestCallHistoryRetrieveScreen()
    {        
        beginOfTest("Start testCallHistoryRetrieveScreen");

        BasicAccountData l_bad = installAndLoadSubScreen(0);
        
        // Load test data

        loadCallHistoryData(l_bad.getMsisdn(), getNow(), 7);

        // Access/test specific screen
        
        sendRequest(constructGetRequest("cc/CallHistory", "GET_SCREEN_DATA", ""));

        sendRequest(constructGetRequest("cc/CallHistory", 
                                        "RETRIEVE_CALL_DATA",
                                        "&p_startDate=07-Aug-2006" +
                                        "&p_endDate=08-Nov-2006" +
                                        "&p_maxRows=20" +
                                        "&p_sortField=Date" +
                                        "&p_descendSort=true" +
                                        "&p_subLevel=true"));
        
      try
      {
          
          
          WebTable[] l_webTables = c_webResponse.getTables();
          
          try {
              HTMLElement l_e1 = c_webResponse.getElementWithID("historydiv460high");
              System.out.println("MIE l_e1: " + l_e1.toString());
          } catch (SAXException e1) {
              // TODO Auto-generated catch block
              e1.printStackTrace();
          }
          
          System.out.println("tab len " + l_webTables.length);
          for(int t = 0; t<l_webTables.length; t++)
          {
              System.out.println("tab [" + t + "]  " + l_webTables[t].getID());
          }
          
          WebTable l_webTable = c_webResponse.getTableWithID("CallHistory1");
          
          
          System.out.println("APH getTableWithId:");
          for (int i = 0; i < l_webTable.getRowCount(); i++)
          {
              for (int j = 0; j < l_webTable.getColumnCount(); j++)
              {
                  System.out.println("l_webTable[" +i +"][" + j + "]: " + l_webTable.getCellAsText(i,j));
              }
          }
          
//          try {
//            System.out.println("MIE: " + c_webResponse.getText());
//        } catch (IOException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
          
//          System.out.println("l_webTables.length: " + l_webTables.length);
//          for (int i = 0; i < l_webTables.length; i++)
//          {
//              //l_webTables[i].asText();
//              System.out.println("Cell 0,0 as text: " + l_webTables[i].getCellAsText(0,0));
//              System.out.println("Cell 0,1 as text: " + l_webTables[i].getCellAsText(0,1));
//              System.out.println("Cell 0,2 as text: " + l_webTables[i].getCellAsText(0,2));
//              // 1,1 give an ArrayIndexOutOfBoundsException...
//              //System.out.println("Cell 1,1 as text: " + l_webTables[i].getCellAsText(1,1));
//          }
      }
      catch (SAXException e)
      {
          failedTestException(e);
      }
//      catch (IOException e)
//      {
//          failedTestException(e);
//      }
      
      
        
        verifyRowInTable("CallHistory1", 0, 
                new String[] {"", "|", "sdfsd",
                              "Period Counter", "Period Counter Clear Date"});
        
        validateResponse(null, null, "Call History");

        endOfTest();
    }
    

    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        System.out.println(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcCallHistoryUT.class);
    }
    


    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------    

    /**
     * Connect to database and load call history data for given msisdn.
     * 
     * @param p_msisdn msisdn to load data onto.
     * @param p_start start date for data records.
     * @param p_numRecords number of records to be loaded.
     */
    private void loadCallHistoryData(Msisdn p_msisdn, PpasDateTime p_start, int p_numRecords)
    {
        JdbcConnection l_conn = getConnection("CcCallHistoryUT", "testLoadCallHistoryScreen", 1);
        CallHistoryTestDataTT l_callHist = new CallHistoryTestDataTT();
        try
        {
            l_callHist.createCallHistory(l_conn, p_msisdn, p_start, p_numRecords);
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        finally
        {
            putConnection(l_conn);
        }
    }
    
}

