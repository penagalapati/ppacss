////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcServiceFeeDeductionDetailScreenData.java
//      DATE            :       15-Jul-2003
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_PPAK00_GEN_CA_48
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the Service Fee Deduction
//                              Detail screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+--------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Service Fee Deduction Detail screen.
 */
public class CcServiceFeeDeductionDetailScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Cust Id. */
    private String i_custId = null;

    /** Charged Cust Id. */
    private String i_chargedCustId = null;

    /** Service Fee Type. */
    private String i_serviceFeeType = null;

    /** Date Time. */
    private String i_dateTime = null;

    /** Deduction. */
    private String i_deduction = null;

    /** Deduction Currency. */
    private String i_deductionCurr = null;

    /** Subscriber Amount. */
    private String i_subscriberAmount = null;

    /** Subscriber Amount Currency. */
    private String i_subscriberAmountCurr = null;

    /** Deducted Amount. */
    private String i_deductedAmount = null;

    /** Deducted Amount Currency. */
    private String i_deductedAmountCurr = null;

    /** Debt. */
    private String i_debt = null;

    /** Debt Currency. */
    private String i_debtCurr = null;

    /** Success Code. */
    private String i_successCode = null;

    /** Success Code Acronym. */
    private String i_successCodeAcronym = null;

    /** Success Code Desc. */
    private String i_successCodeDesc = null;

    /** Old Service Class. */
    private String i_oldServiceClass = null;

    /** New Service Class. */
    private String i_newServiceClass = null;

    /** Old Expiry Date. */
    private String i_oldExpiryDate = null;

    /** New Expiry Date. */
    private String i_newExpiryDate = null;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /**
     * Constructs a set of data for the Service Fee Deduction Detail screen.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     * @param p_custId Cust Id.
     * @param p_chargedCustId Charged Cust Id.
     * @param p_serviceFeeType Service Fee Type.
     * @param p_dateTime Date Time.
     * @param p_deduction Deduction.
     * @param p_deductionCurr Deduction Currency.
     * @param p_subscriberAmount Subscriber Amount.
     * @param p_subscriberAmountCurr Subscriber Amount Currency.
     * @param p_deductedAmount Deducted Amount.
     * @param p_deductedAmountCurr Deducted Amount Currency.
     * @param p_debt Debt.
     * @param p_debtCurr Debt Currency.
     * @param p_successCode Success Code.
     * @param p_successCodeAcronym Success Code Acronym.
     * @param p_successCodeDesc Success Code Desc.
     * @param p_oldServiceClass Old Service Class.
     * @param p_newServiceClass New Service Class.
     * @param p_oldExpiryDate Old Expiry Date.
     * @param p_newExpiryDate New Expiry Date.
     */
    public CcServiceFeeDeductionDetailScreenData(
        GuiContext p_guiContext,
        GuiRequest p_guiRequest,
        String p_custId,
        String p_chargedCustId,
        String p_serviceFeeType,
        String p_dateTime,
        String p_deduction,
        String p_deductionCurr,
        String p_subscriberAmount,
        String p_subscriberAmountCurr,
        String p_deductedAmount,
        String p_deductedAmountCurr,
        String p_debt,
        String p_debtCurr,
        String p_successCode,
        String p_successCodeAcronym,
        String p_successCodeDesc,
        String p_oldServiceClass,
        String p_newServiceClass,
        String p_oldExpiryDate,
        String p_newExpiryDate)
    {
        super( p_guiContext, p_guiRequest);
        i_custId = p_custId;
        i_chargedCustId = p_chargedCustId;
        i_serviceFeeType = p_serviceFeeType;
        i_dateTime = p_dateTime;
        i_deduction = p_deduction;
        i_deductionCurr = p_deductionCurr;
        i_subscriberAmount = p_subscriberAmount;
        i_subscriberAmountCurr = p_subscriberAmountCurr;
        i_deductedAmount = p_deductedAmount;
        i_deductedAmountCurr = p_deductedAmountCurr;
        i_debt = p_debt;
        i_debtCurr = p_debtCurr;
        i_successCode = p_successCode;
        i_successCodeAcronym = p_successCodeAcronym;
        i_successCodeDesc = p_successCodeDesc;
        i_oldServiceClass = p_oldServiceClass;
        i_newServiceClass = p_newServiceClass;
        i_oldExpiryDate = p_oldExpiryDate;
        i_newExpiryDate = p_newExpiryDate;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the Cust Id.
     * @return Cust Id.
     */
    public String getCustId()
    {
        return( i_custId );
    }

    /**
     * Returns the Charged Cust Id.
     * @return Charged Cust Id.
     */
    public String getChargedCustId()
    {
        return( i_chargedCustId );
    }

    /**
     * Returns the Service Fee Type.
     * @return Service Fee Type.
     */
    public String getServiceFeeType()
    {
        return( i_serviceFeeType );
    }

    /**
     * Returns the Date Time.
     * @return Date Time.
     */
    public String getDateTime()
    {
        return( i_dateTime );
    }

    /**
     * Returns the Deduction.
     * @return Deduction.
     */
    public String getDeduction()
    {
        return( i_deduction );
    }

    /**
     * Returns the Deduction Currency.
     * @return Deduction Currency.
     */
    public String getDeductionCurr()
    {
        return( i_deductionCurr );
    }

    /**
     * Returns the Subscriber Amount.
     * @return Subscriber Amount.
     */
    public String getSubscriberAmount()
    {
        return( i_subscriberAmount );
    }

    /**
     * Returns the Subscriber Amount Currency.
     * @return Subscriber Amount Currency.
     */
    public String getSubscriberAmountCurr()
    {
        return( i_subscriberAmountCurr );
    }

    /**
     * Returns the Deducted Amount.
     * @return Deducted Amount.
     */
    public String getDeductedAmount()
    {
        return( i_deductedAmount );
    }

    /**
     * Returns the Deducted Amount Currency.
     * @return Deducted Amount Currency.
     */
    public String getDeductedAmountCurr()
    {
        return( i_deductedAmountCurr );
    }

    /**
     * Returns the Debt.
     * @return Debt.
     */
    public String getDebt()
    {
        return( i_debt );
    }

    /**
     * Returns the Debt Currency.
     * @return Debt Currency.
     */
    public String getDebtCurr()
    {
        return( i_debtCurr );
    }

    /**
     * Returns the Success Code.
     * @return Success Code.
     */
    public String getSuccessCode()
    {
        return( i_successCode );
    }

    /**
     * Returns the Success Code Acronym.
     * @return Success Code Acronym.
     */
    public String getSuccessCodeAcronym()
    {
        return( i_successCodeAcronym );
    }

    /**
     * Returns the Success Code Desc.
     * @return Success Code Desc.
     */
    public String getSuccessCodeDesc()
    {
        return( i_successCodeDesc );
    }

    /**
     * Returns the Old Service Class.
     * @return Old Service Class.
     */
    public String getOldServiceClass()
    {
        return( i_oldServiceClass );
    }

    /**
     * Returns the New Service Class.
     * @return New Service Class.
     */
    public String getNewServiceClass()
    {
        return( i_newServiceClass );
    }

    /**
     * Returns the Old Expiry Date.
     * @return Old Expiry Date.
     */
    public String getOldExpiryDate()
    {
        return( i_oldExpiryDate );
    }

    /**
     * Returns the New Expiry Date.
     * @return New Expiry Date.
     */
    public String getNewExpiryDate()
    {
        return( i_newExpiryDate );
    }

} // end public class CcServiceFeeDeductionDetailScreenData
