////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdditionalInfoServlet.Java
//      DATE            :       7-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1236/5006
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Additional Information screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 07/10/03 | R Isaacs   | Privilege checks added          | CR#60/571
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcAdditionalInfoScreenData;
import com.slb.sema.ppas.cc.service.CcAdditionalInfoDataResponse;
import com.slb.sema.ppas.cc.service.CcAdditionalInfoService;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession; // CR#60/571
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData; // CR#60/571

/**
 * This Servlet handles requests from the Additional Information screen.
 */
public class CcAdditionalInfoServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdditionalInfoServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Additional Information Service to be used by this servlet. */
    private CcAdditionalInfoService i_ccAdditionalInfoService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcAdditionalInfoServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;
    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /**
     * Performs initialisation specific to this Servlet; currently there is no
     * specific initialisation for this Servlet.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcAdditionalInfoService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the Additional Information screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest )
    {

        String                      l_command            = null;
        String                      l_forwardUrl         = null;
        CcAdditionalInfoScreenData  l_addInfoScreenData  = null;
        GuiResponse                 l_guiResponse        = null;
        boolean                     l_accessGranted      = false;   // CR#60/571
        GopaGuiOperatorAccessData   l_gopaData           = null;    // CR#60/571

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcAdditionalInfoService if it does not already exist ...
        // this will be move into doInit later!! <<< ??? >>>
        if (i_ccAdditionalInfoService == null)
        {
            i_ccAdditionalInfoService = new CcAdditionalInfoService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create screen data object.
        l_addInfoScreenData =
            new CcAdditionalInfoScreenData( i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam( p_guiRequest,
                                       0,
                                       p_request,
                                       "p_command",
                                       true,                 // mandatory
                                       "GET_SCREEN_DATA", // default to this
                                       new String [] {"GET_SCREEN_DATA",
                                                      "UPDATE_ADDITIONAL_INFO"});

            // CR#60/571 Start

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) &&
                 l_gopaData.hasAddInfoScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("UPDATE_ADDITIONAL_INFO")) &&
                      l_gopaData.addInfoUpdateIsPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_addInfoScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_addInfoScreenData);
            }
            else
            {
                // Cust id will already be set up in the session from selection
                // on the account selection screen.

                if (l_command.equals("UPDATE_ADDITIONAL_INFO"))
                {
                    setIsUpdateRequest(true);

                    updateAdditionalInfo( p_guiRequest,
                                          p_request,
                                          l_addInfoScreenData );
                }

                l_forwardUrl = getAdditionalInfo( p_guiRequest,
                                                  p_request,
                                                  l_addInfoScreenData );
            }
            // CR#60/571 End
        } // end try

        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 73872, this,
                    "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_addInfoScreenData.addRetrievalResponse( p_guiRequest, l_guiResponse );

            // Store the response in the request
            p_request.setAttribute( "p_guiScreenData",
                                    l_addInfoScreenData );
        } // end catch

        // CR#60/571 Start

        if (l_accessGranted)
        {
            p_request.setAttribute( "p_guiScreenData",
                                    l_addInfoScreenData );
        }
        // CR#60/571 End

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateAdditionalInfo =
                                        "updateAdditionalInfo";

    /** Calls CcAdditionalInfoService to update the customer's additional
     *  information.
     *
     *  @param p_guiRequest The GUI request being processed.
     *  @param p_request    The servlet request.
     *  @param p_screenData screen data object, returned will contain the
     *                      update responses from this method.
     *
     *  @throws PpasServletException If an exception occurs.
     */
    private void updateAdditionalInfo( GuiRequest                 p_guiRequest,
                                       HttpServletRequest         p_request,
                                       CcAdditionalInfoScreenData p_screenData )
        throws PpasServletException
    {
        Vector         l_custFieldsV  = null;
        GuiResponse    l_guiResponse  = null;
        PpasDateTime   l_currentTime  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Entered " + C_METHOD_updateAdditionalInfo);
        }

        // Get params from request...

        l_custFieldsV = new Vector( 40, 0 );

        // Loop through the 40 possible fields of additional information
        // and add them to the customer fields Vector object.
        for (int i = 0; i < 40; i++)
        {
            l_custFieldsV.addElement( getValidParam(
                                              p_guiRequest,
                                              PpasServlet.C_FLAG_ALLOW_BLANK,
                                              p_request,
                                              "additionalInfo" + (i + 1),
                                              false,
                                              "",
                                              PpasServlet.C_TYPE_ANY,
                                              30,
                                              C_OPERATOR_LESS_THAN_OR_EQUAL ));

        } // end of for loop.

        // The addtional information is being updated.
        l_guiResponse =
            i_ccAdditionalInfoService.modifyAdditionalInfo( p_guiRequest,
                                                            i_timeout,
                                                            l_custFieldsV );


        p_screenData.addUpdateResponse( p_guiRequest,
                                        l_guiResponse );

        l_currentTime = DatePatch.getDateTimeNow();

        if (!p_screenData.hasUpdateError())
        {
            p_screenData.setSuccessfulUpdate(true);
            p_screenData.setInfoText(
                "Successfully Updated at " + l_currentTime);
        }
        else
        {
            p_screenData.setInfoText(
                "Failed Update at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Leaving " + C_METHOD_updateAdditionalInfo);
        }

        return;

    } // end private void updateAdditionalInfo(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAdditionalInfo = "getAdditionalInfo";

    /** Calls CcAdditionalInfoService to retrieve the customer's current
     *  additional information.
     *
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     *  @param p_screenData screen data object, returned will contain the
     *                      additional info details for this screen.
     * @return URL to forward to.
     */
    private String getAdditionalInfo( GuiRequest               p_guiRequest,
                                    HttpServletRequest         p_request,
                                    CcAdditionalInfoScreenData p_screenData )
    {
        CcAdditionalInfoDataResponse  l_ccAdditionalInfoDataResponse  = null;
        AdditionalInfoData            l_addInfoData                   = null;
        Vector                        l_miscFieldV                    = null;
        String                        l_forwardUrl                    = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27060, this,
                "Entered " + C_METHOD_getAdditionalInfo);
        }

        l_ccAdditionalInfoDataResponse =
                i_ccAdditionalInfoService.getAdditionalInfo( p_guiRequest,
                                                             i_timeout );
        l_addInfoData = l_ccAdditionalInfoDataResponse.getAdditionalInfoData();

        if (l_addInfoData == null)
        {
            l_addInfoData = new AdditionalInfoData(p_guiRequest);
        }

        // If the vector of the customers field is empty, populate it with
        // with blanks for display purposes.

        l_miscFieldV  = l_addInfoData.getMiscFieldV();

        if ( l_miscFieldV == null )
        {
            l_miscFieldV = new Vector(40, 1);
        }

        while (l_miscFieldV.size() < 40)
        {
            l_miscFieldV.addElement( "" );
        }
        l_addInfoData.setMiscFieldV( l_miscFieldV );

        p_screenData.addRetrievalResponse( p_guiRequest,
                                           l_ccAdditionalInfoDataResponse );

        if (l_ccAdditionalInfoDataResponse.isSuccess())
        {
            p_screenData.setAdditionalInfoData( l_addInfoData );

        }

        if (p_screenData.hasRetrievalError())
        {
            // An error occured retrieving the data necessary to populate
            // the Additional Information screen.
            // Forward to the GuiError.jsp instead.
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

            p_request.setAttribute ( "p_guiScreenData", p_screenData );

        }
        else
        {
            // CcAdditionalInfoScreenData object has the necessary data
            // to populate the Additional Information screen.
            l_forwardUrl = "/jsp/cc/ccadditionalinfo.jsp";
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27190, this,
                "Leaving " + C_METHOD_getAdditionalInfo);
        }

        return (l_forwardUrl);

    } // end private void getAdditionalInfo(...)

} // End of CcAdditionalInfoServlet class.
