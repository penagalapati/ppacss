////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDisconnectDataResponse.java
//      DATE            :       09-Apr-2002
//      AUTHOR          :       Remi Isaacs
//      REFERENCE       :       PpaLon#1341/5587
//                              PRD_PPAK00_DEV_IN_035
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for DisconnectData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for DisconnectData objects. 
 */
public class CcDisconnectDataResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDisconnectDataResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The DisconnectData object wrapped by this object. */
    private  DisconnectData i_disconnectData;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives a DisconnectData object and the additional necessary objects to 
     * create a response message indicating the status of a call to a
     * DisconnectService method.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_disconnectData    The disconnect information.
     * @param p_messageSeverity   Severity of response.
     */
    public CcDisconnectDataResponse   ( GuiRequest           p_guiRequest,
                                        Locale               p_messageLocale,
                                        String               p_messageKey,
                                        Object[]             p_messageParams,
                                        int                  p_messageSeverity,
                                        DisconnectData       p_disconnectData )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing CcDisconnectDataResponse");
        }

        i_disconnectData = p_disconnectData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed CcDisconnectDataResponse");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------
    /** Returns the Disconnect information.
     * @return Disconnection data.
     */
    public DisconnectData getDisconnectData()
    {
        return( i_disconnectData );
    }

} // end of class CcDisconnectDataResponse
