////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPaymentService.java
//      DATE            :       20-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1266/5099
//                              PRD_PPAK00_DEV_IN_30
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasPaymentService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 06/06/06 | S James    | change user messages which refer| PpacLon#1815/9026
//          |            | to payments to now use account  |
//          |            | refill                          |
//----------+------------+---------------------------------+--------------------
// 12.12.06 | MAGray     | Pass channel Id.                | PpacLon#2815/10635
//          |            |                                 | PRD_ASCS00_GEN_CA_105
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.PaymentDataSet;
import com.slb.sema.ppas.common.dataclass.PaymentResultData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasPaymentService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * Payments. The following services are provided:
 *
 * - Make a payment on the selected customer.
 * - Obtain a set of payment history for the selected customer
 */
public class CcPaymentService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPaymentService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The atomic payment service. */
    private PpasPaymentService i_paymentService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a CcPaymentService object that
     * can then be used to perform Payment services.
     * @param p_request The request being processed.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcPaymentService( GuiRequest         p_request,
                             Logger             p_logger,
                             GuiContext         p_guiContext )
    {
        super( p_request, 0, p_logger );

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_request, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        i_paymentService = new PpasPaymentService( p_request, p_logger, p_guiContext );

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_request, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPayments = "getPayments";
    /**
     * Generates a CcPaymentDataSetResponse object for the selected customer.
     *
     * @param p_request    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Response to the request for payments.
     */
    public CcPaymentDataSetResponse getPayments( GuiRequest p_request,
                                                 long       p_timeoutMillis )
    {
        CcPaymentDataSetResponse l_paymentResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 10100, this,
                           "Entered " + C_METHOD_getPayments );
        }

        try
        {
            // Call the atomic service...
            PaymentDataSet l_payments = i_paymentService.getPayments( p_request, p_timeoutMillis );

            // Generate success CcPaymentDataSetResponse
            l_paymentResponse = new CcPaymentDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_payments );
        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 10210, this,
                               "Caught PpasServiceException: " + e);
            }

            GuiResponse l_guiResponse = handlePpasServiceException(
                                                        p_request, 0, "get all account refills", e);

            l_paymentResponse = new CcPaymentDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().
                                      getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 13530, this,
                           "Leaving " + C_METHOD_getPayments);
        }

        return l_paymentResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_makePayment = "makePayment";
    /**
     * Calls an Internal Service to make a payment on the selected subscriber.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_transProcCode Code used to identify the source of the payment.
     * @param p_transAmt The amount of the transaction expressed as the lowest
     *                   denomination of currency. eg GBP 100 is equivalent to �1.
     * @param p_voucherGroup The payment profile used to identify the voucher
     *                       group determining how the payment is to be applied
     *                       to the account.
     * @param p_bankId    Identifier of the bank where the payment was made.
     * @param p_address1  First line of address where the payment was made.
     * @param p_address2  Second line of address where the payment was made.
     * @param p_address3  Third line of address where the payment was made.
     * @param p_postalCode  Postal/zip code of address where the payment was made.
     *
     * @return PosiMakePaymentResponse Contains the data returned from
     *                                     doing this service.
     */
    public CcPaymentResultDataResponse makePayment(
                                          GuiRequest  p_guiRequest,
                                          long        p_timeoutMillis,
                                          String      p_transProcCode,
                                          Money       p_transAmt,
                                          String      p_voucherGroup,
                                          String      p_bankId,
                                          String      p_address1,
                                          String      p_address2,
                                          String      p_address3,
                                          String      p_postalCode )
    {
        CcPaymentResultDataResponse l_paymentResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 30000, this,
                           "Entered " + C_METHOD_makePayment);
        }

        try
        {
            PaymentResultData l_paymentResultData =
                i_paymentService.makePayment( p_guiRequest,
                                              p_timeoutMillis,
                                              p_transProcCode,
                                              p_transAmt,
                                              p_voucherGroup,
                                              p_bankId,
                                              p_address1,
                                              p_address2,
                                              p_address3,
                                              p_postalCode,
                                              null);   // Channel Id

            l_paymentResponse = new CcPaymentResultDataResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"make account refill"},
                              GuiResponse.C_SEVERITY_SUCCESS,
                              l_paymentResultData );
        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 30210, this,
                               "Caught PpasServiceException: " + e);
            }

            GuiResponse l_guiResponse = handlePpasServiceException(
                                                          p_guiRequest, 0, "make the account refill", e);

            l_paymentResponse = new CcPaymentResultDataResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              l_guiResponse.getKey(),
                              l_guiResponse.getParams(),
                              GuiResponse.C_SEVERITY_FAILURE,
                              null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 31000, this,
                           "Leaving " + C_METHOD_makePayment);
        }

        return l_paymentResponse;
    }
}
