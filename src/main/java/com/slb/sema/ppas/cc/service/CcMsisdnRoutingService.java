////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcMsisdnRoutingService.java
//DATE            :       27-Oct-2004
//AUTHOR          :       Kevin Tongue
//
//COPYRIGHT       :       
//
//DESCRIPTION     :       Gui Service that wrappers calls to
//                      PpasMsisdnRouting methods.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasMsisdnRoutingService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Gui Service that wrappers calls to the MSISDN Routing service.
 */
public class CcMsisdnRoutingService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcMsisdnRoutingService";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Atomic service used by this Service. */
    private PpasMsisdnRoutingService i_msisdnRoutingService;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates an instance of the CcMsisdnRoutingService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcMsisdnRoutingService(GuiRequest p_guiRequest,
                                  long       p_flags,
                                  Logger     p_logger,
                                  GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }
        
        // Create PPAS internal session service to be used by this service.
        i_msisdnRoutingService = new PpasMsisdnRoutingService(p_guiRequest, p_logger, p_guiContext);
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addMsisdnRouting = "addMsisdnRouting";
    /** Calls the internal service to add an MSISDN routing.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_msisdn        The Msisdn.
     * @param p_sdpId         The sdpID
     * @return MSISDN Routing information.
     */
    public GuiResponse addMsisdnRouting(GuiRequest              p_guiRequest,
                                        long                    p_timeoutMillis,
                                        Msisdn                  p_msisdn,
                                        String                  p_sdpId
    )
    { 
        GuiResponse l_guiResponse  = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 20000, this,
                           "Entered " + C_METHOD_addMsisdnRouting);
        }
        
        try
        {      
            i_msisdnRoutingService.addMsisdnRouting(p_guiRequest,
                                                    p_timeoutMillis,
                                                    p_msisdn,
                                                    p_sdpId);
            
            l_guiResponse
            = new GuiResponse(p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SERVICE_SUCCESS,
                              new Object[] {p_guiRequest.getMsisdn()},
                              GuiResponse.C_SEVERITY_SUCCESS);
            
        }
        
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                               WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 20010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "add MSISDN routing", l_pSE);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 20020, this,
                           "Leaving " + C_METHOD_addMsisdnRouting);
        }
        
        return l_guiResponse;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_deleteMsisdnRouting = "deleteMsisdnRouting";
    /** Calls the internal service to add an MSISDN routing.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_msisdn        The Msisdn.
     * @return MSISDN Routing information.
     */
    public GuiResponse deleteMsisdnRouting(GuiRequest              p_guiRequest,
                                           long                    p_timeoutMillis,
                                           Msisdn                  p_msisdn)                                     
    { 
        GuiResponse l_guiResponse  = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 20000, this,
                           "Entered " + C_METHOD_deleteMsisdnRouting);
        }
        
        try
        {
            i_msisdnRoutingService.deleteMsisdnRouting(p_guiRequest,
                                                       p_timeoutMillis,
                                                       p_msisdn
            );
            
            l_guiResponse
            = new GuiResponse(p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SERVICE_SUCCESS,
                              new Object[] {p_guiRequest.getMsisdn()},
                              GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                               WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 20010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "delete MSISDN routing", l_pSE);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                           WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 20020, this,
                           "Leaving " + C_METHOD_deleteMsisdnRouting);
        }
        
        return l_guiResponse;
    }
}
