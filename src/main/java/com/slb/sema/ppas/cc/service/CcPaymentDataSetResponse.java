////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPaymentDataSetResponse.java
//      DATE            :       20-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1266/5099
//                              PRD_PPAK00_DEV_IN_30
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for PaymentDataSet objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.PaymentDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for PaymentDataSet objects. 
 */
public class CcPaymentDataSetResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPaymentDataSetResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The PaymentDataSet object wrapped by this object. */
    private PaymentDataSet  i_paymentDataSet;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives a PaymentDataSet object and the additional necessary objects to 
     * create a response message indicating the status of a call to a
     * PaymentService method.
     * @param p_guiRequest        The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_messageSeverity   Severity of response.
     * @param p_paymentDataSet    Set of payment Data.
     */
    public CcPaymentDataSetResponse( GuiRequest      p_guiRequest,
                                     Locale          p_messageLocale,
                                     String          p_messageKey,
                                     Object[]        p_messageParams,
                                     int             p_messageSeverity,
                                     PaymentDataSet  p_paymentDataSet )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing CcPaymentDataSetResponse");
        }

        i_paymentDataSet = p_paymentDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed CcPaymentDataSetResponse");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Returns the payment information.
     * @return Set of payment data.
     */
    public PaymentDataSet getPaymentDataSet()
    {
        return( i_paymentDataSet );
    }

} // end of class CcPaymentDataSetResponse

