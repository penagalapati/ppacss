////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustomerSelectionDataSetResponse.java
//      DATE            :       11-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PpaLon#1248/5523
//                              PRD_PPAK00_DEV_IN_27
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Response returned by the Customer Selection
//                              GUI service containing details for accounts
//                              selected by the service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.CustomerSelectionDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response containing details of accounts selected by the Account Selection
 * service if that service was successful. Otherwise, the key in the GuiResponse
 * super class defines the reason for the failure of the service.
 */
public class CcCustomerSelectionDataSetResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCustomerSelectionDataSetResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Data Set containing details of the accounts held in this response
     *  object.
     */
    private CustomerSelectionDataSet i_custSelectionDataSet;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /**
     * Construct a CcCustomerSelectionDataSetResponse using the default locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_custSelectionDataSet Customer selection details.
     */
    public CcCustomerSelectionDataSetResponse(
        GuiRequest                p_guiRequest,
        String                    p_messageKey,
        Object[]                  p_messageParams,
        int                       p_messageSeverity,
        CustomerSelectionDataSet  p_custSelectionDataSet)
    {
        this (p_guiRequest,
              null,
              p_messageKey,
              p_messageParams,
              p_messageSeverity,
              p_custSelectionDataSet);
    }

    /**
     * Construct a CcCustomerSelectionDataSetResponse using the given locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_custSelectionDataSet Customer selection details.
     */
    public CcCustomerSelectionDataSetResponse(
        GuiRequest               p_guiRequest,
        Locale                   p_messageLocale,
        String                   p_messageKey,
        Object[]                 p_messageParams,
        int                      p_messageSeverity,
        CustomerSelectionDataSet p_custSelectionDataSet)
    {
        super (p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 91000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_custSelectionDataSet = p_custSelectionDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 91090, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // end constructor


    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the AccountData instance contained within
     * this response object.
     * @return CustomerSelectionDataSet. Data set of selected customers stored
     *         by within this response.
     */
    public CustomerSelectionDataSet getDataSet()
    {
        return (i_custSelectionDataSet);
    }

} // end class CcCustomerSelectionDataSetresponse
