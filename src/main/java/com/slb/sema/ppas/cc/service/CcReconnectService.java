////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcReconnectService.java
//      DATE            :       03-May-2007
//      AUTHOR          :       Steven James
//
//      COPYRIGHT       :       LogicaCMG 2007
//
//      REFERENCE       :       PRD_ASCS00_GEN_CA_124
//                              PpacLon#3072/11370
//
//      DESCRIPTION     :       Wrappers the reconnectSubscriber method of
//                              the PpasReconnectService internal service
//                              returning a GuiResponse object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//06-Aug-07 | K Bond     | Corrections to Account Reconnect| PpacLon#3246/11923
//----------+------------+---------------------------------+--------------------
//23-Aug-07 | K Bond     | Use default reconnect descrip.  | PpacLon#3247/11938
////////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.ClearedCreditData;
import com.slb.sema.ppas.common.dataclass.InstallResultData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.RequestParams;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasAdjustmentService;
import com.slb.sema.ppas.is.isapi.PpasReconnectService;
import com.slb.sema.ppas.is.isil.dbservice.AdjustmentApprovalDbService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.common.dataclass.AdjustmentRequestData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;


/**
 * Provides a service method to perform a reconnection.
 */
public class CcReconnectService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcReconnectService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The atomic Reconnect service. */
    private PpasReconnectService i_ppasReconnectService;
    
    /** The atomic Adjustment service. */
    private PpasAdjustmentService i_ppasAdjustmentService;
    
    /** The atomic Account service. */
    private PpasAccountService i_ppasAccountService;
    private AdjustmentApprovalDbService i_adjAppDbService = null;
    protected BusinessConfigCache i_businessConfigCache = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates an instance of a PpasReconnectService object that
     * can then be used to perform the Subscriber reconnect service.
     * @param p_guiRequest The request being processed.
     * @param p_flags General flags
     * @param p_logger  The logger to direct messages.
     * @param p_guiContext General session information.
     */
    public CcReconnectService(GuiRequest         p_guiRequest,
                              long               p_flags,
                              Logger             p_logger,
                              GuiContext         p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_ppasReconnectService = new PpasReconnectService(p_guiRequest, p_logger, p_guiContext);
        i_ppasAdjustmentService = new PpasAdjustmentService(p_guiRequest, p_logger, p_guiContext);
        i_ppasAccountService = new PpasAccountService(p_guiRequest, p_logger, p_guiContext); 
        i_adjAppDbService = new AdjustmentApprovalDbService(null, i_logger, p_guiContext);
        i_businessConfigCache = (BusinessConfigCache)p_guiContext.getAttribute("BusinessConfigCache");
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reconnectSubscriber = "reconnectSubscriber";
    /**
     * Attempts to reconnect a subscriber and returns the outcome in a
     * GuiResponse object. Optional parameters may be supplied as a zero
     * length string, in which case a default value is used.
     *
     * @param p_guiRequest The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_reapplyCredit Flag indicating whether to reapply any credit previously cleared.
     * @param p_defaultAdjType Default adjustment type.
     * @param p_defaultAdjCode Default adjustment code.
     * @return  Response object containing key, message, and parameters.
     */
    public GuiResponse reconnectSubscriber(GuiRequest             p_guiRequest,
                                           long                   p_timeoutMillis,
                                           boolean                p_reapplyCredit,
                                           String                 p_defaultAdjType,
                                           String                 p_defaultAdjCode,
                                           int                    p_airtimeExpiryOffset,
                                           int                    p_serviceExpiryOffset)
    {        
        GuiResponse         l_reconnectResponse = null;
        InstallResultData   l_installResultData = null;
        PpasDate            l_newAirtimeExpiryDate;
        PpasDate            l_newServiceExpiryDate;
        String              l_adjDesc = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10100, this,
                           "Entered " + C_METHOD_reconnectSubscriber +
                           " timeout = " +p_timeoutMillis +
                           " reapplyCredit? = " +p_reapplyCredit +
                           " default Adj Type = " +p_defaultAdjType +
                           " default Adj Code = " +p_defaultAdjCode);
        }
            
        try
        {
            // Call the atomic service...
            l_installResultData = i_ppasReconnectService.reconnectSubscriber(
                                          p_guiRequest,
                                          p_timeoutMillis,
                                          p_reapplyCredit);

            Money l_money = null;
            ClearedCreditData l_clearedCredit = null;
            
            l_clearedCredit = l_installResultData.getClearedCredit();
            
            if (l_clearedCredit != null)
            {
                l_money = l_clearedCredit.getAmount();
            }
                      
            l_newAirtimeExpiryDate = DatePatch.getDateToday();
            l_newAirtimeExpiryDate.add(PpasDate.C_FIELD_DATE, p_airtimeExpiryOffset);
            
            l_newServiceExpiryDate = DatePatch.getDateToday();
            l_newServiceExpiryDate.add(PpasDate.C_FIELD_DATE, p_serviceExpiryOffset);

            i_ppasAccountService.updateExpiryDates(
            		p_guiRequest,
                    p_timeoutMillis,
                    l_newAirtimeExpiryDate,
                    new PpasDate(),
                    l_newServiceExpiryDate,
                    new PpasDate());

            Market l_market = ((GuiSession)p_guiRequest.getSession()).getCsoMarket();
            SrvaAdjCodesDataSet l_adjCodesDataSet = i_businessConfigCache.getSrvaAdjCodesCache().getAvailable(l_market);
            if (l_adjCodesDataSet != null)
            {
            	SrvaAdjCodesData l_adjData = l_adjCodesDataSet.getAdjustmentData(p_defaultAdjType, p_defaultAdjCode);
                if (l_adjData != null)
                {
                    l_adjDesc = l_adjData.getAdjDesc();

                }
            }

            if (l_money != null && l_money.isNotZero())
            {
                i_ppasAdjustmentService.postAdjustment(
                                           p_guiRequest,
                                           p_timeoutMillis,          // Timeout
                                           l_money,                  // Money
                                           p_defaultAdjType,         // Adj Type
                                           p_defaultAdjCode,         // Adj Code
                                           l_adjDesc,
                                           new Boolean(false));      // Approval flag
            }
 
            // Generate success GuiResponse
            l_reconnectResponse = new GuiResponse(p_guiRequest,
                                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                                  GuiResponse.C_KEY_INSTALL_SUCCESS,
                                                  new Object[] {p_guiRequest.getMsisdn()},
                                                  GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_reconnectResponse = handlePpasServiceException(p_guiRequest, 0, "reconnect the subscriber", l_pSE);


        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 11100, this,
                "Leaving " + C_METHOD_reconnectSubscriber +
                " with response = "+l_reconnectResponse);
        }

        return l_reconnectResponse;

    }  // end public GuiResponse reconnectSubscriber(...)

}
