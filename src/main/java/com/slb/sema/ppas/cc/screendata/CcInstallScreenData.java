////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcInstallScreenData.java
//      DATE            :       21-Feb-2002
//      AUTHOR          :       Mike Hickman
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Install screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 04-Dec-05| M Erskine  | Licensed feature to enable      | PpacLon#1882/7556
//          |            | operator to pass in a new flag  |PRD_ASCS00_GEN_CA_65
//          |            | specifying that install request |
//          |            | can use a quarantined MSISDN    |
//----------+------------+---------------------------------+--------------------
// 24-Apr-07| S James    | Add Single Step Install changes | PpacLon#3072/11370
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.MarketSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Install screen.
 */
public class CcInstallScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Markets available for the CSO. */
    private MarketSet i_markets = null;

    /** Markets available for the CSO. */
    private Vector i_failedMarkets = null;

    /** The Market for a failed installation. */
    Market i_intMarket = null;

    /**  Promotion plan for failed installation. */
    private String i_oldPromo;

    /** Service class for a failed installation.  */
    private String i_oldClass;

    /** ScpId for a failed installation.  */
    private String i_oldinId ;

    /** Master Msisdn for a failed installation. */
    private Msisdn i_masterMsisdn = null;

    /** A set of service classes. */
    private MiscCodeDataSet i_serviceClasses = null;

    /** A set of agents. */
    private SrvaAgentMstrDataSet i_agents = null;

    /** A set of promotion plans. */
    private PrplPromoPlanDataSet i_promoPlans = null;

    /** A set of IN Ids . */
    private ScpiScpInfoDataSet i_inIds = null;

    /** The Market for the successful installation. */
    private Market i_market = null;

    /** The MSISDN that has just been installed. */
    private Msisdn i_msisdn = null;

    /** Indicates whether CSO is allowed to specify IN ID on install. */
    private boolean i_inIdAccess;

    /** Text to indicate status of last attempted update. */
    private String i_infoText = "";

    /** Status of the Subscriber. */
    private String i_custStatus = "";

    /** Failed Msisdn installed. */
    private String i_oldMsisdn = "";

    /** The account type - SUBORDINATE or STANALONE. **/
    private String i_oldMsisdnType = "STANDALONE";

    /** Set old Agent. */
    private String i_oldAgent;

    /** Set old Service Offerings. */
    private String i_oldServiceOffering;
    
    /** Set old Account Group Id */
    private String i_oldAccountGroup;
    
    /** Request for installation fails. **/
    private boolean i_failedRequest = false;
    
    /** Has Subscriber Segmentation Feature Licence. */
    private boolean i_hasSubscriberSegmentationFeatureLicence = false;
    
    /** Has Override Msisdn Quarantine Period Feature Licence. */
    private boolean i_hasOverrideMsisdnQuarantinePeriodFeatureLicence = false;
    
    /** Has Single Step Install Feature Licence. */
    private boolean i_hasSingleStepInstallFeatureLicence = false;
    
    /** Flag to force single step installs */
    private boolean i_forceSingleStepInstall = false;
    
    /** Customer Defaults Cache */
    private CmdfCustMastDefaultsDataSet i_cmdfDataSet;
    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** Simple constructor.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     */
    public CcInstallScreenData(GuiContext p_guiContext,
                               GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
      * Returns <code>true</code> if the Operator generating the request
      * is permitted to specify IN ID on install.
      * Otherwise returns <code>false</code>.
      * @return True if the operator can install.
      */
    public boolean installInIdIsPermitted()
    {
        return (i_gopaData.installInIdIsPermitted());
    }

    /**
      * Returns <code>true</code> if the Operator generating the request
      * is permitted to configure segmentation on install.
      * Otherwise returns <code>false</code>.
      * @return True if the operator can specify segmentation on install.
      */
    public boolean updateSegmentationIsPermitted()
    {
        return (i_gopaData.subscriberSegmentationUpdateIsPermitted());
    }
    
    /**
     * Returns <code>true</code> if the Operator generating the request 
     * is permitted to override the Msisdn quarantine period on install.
     * Otherwise returns <code>false</code>/
     * @return True if the operator can override the Msisdn quarantine period.
     */
    public boolean overrideMsisdnQuarantinePeriodIsPermitted()
    {
        return (i_gopaData.overrideMsisdnQuarantinePeriodIsPermitted());
    }

    /**
     * Returns <code>true</code> if the Operator generating the request 
     * is permitted to perform a single step install.
     * Otherwise returns <code>false</code>/
     * @return True if the operator can perform a single step install.
     */
    public boolean isSingleStepInstallPermitted()
    {
        return (i_gopaData.singleStepInstallIsPermitted());
    }

    /** Returns a set of available markets for the CSO.
     * @return Set of markets.
     */
    public Vector getMarkets()
    {
        return i_markets.getMarkets();
    }

    /** Sets the available markets for the CSO.
     * @param p_markets Set of markets.
     */
    public void setMarkets(MarketSet p_markets)
    {
        i_markets = p_markets;
    }

    /** Returns a set of available markets for the CSO.
     * @return Set of available markets.
     */
    public Vector getFailedMarkets()
    {
        return i_failedMarkets ;
    }

    /** Sets the available markets for the CSO.
     * @param p_failedMarkets Set of available markets.
     */
    public void setFailedMarkets(MarketSet p_failedMarkets)
    {
        i_failedMarkets = p_failedMarkets.getMarkets();
    }

    /** Returns a set of service classes.
     * @return Set of service classes.
     */
    public Vector getServiceClasses()
    {
        return i_serviceClasses.getDataV();
    }

    /** Sets the available service classes.
     * @param p_serviceClasses Set of available service classes.
     */
    public void setServiceClasses(MiscCodeDataSet p_serviceClasses)
    {
        i_serviceClasses = p_serviceClasses;
    }

    /** Returns a set of agents.
     * @return Set of agents.
     */
    public SrvaAgentMstrData[] getAgents()
    {
        return i_agents.getAllArray();
         // return i_agents;
    }

    /** Sets the available agents.
     * @param p_agents Set of agents.
     */
    public void setAgents(SrvaAgentMstrDataSet p_agents)
    {
        i_agents = p_agents;
    }

    /** Returns a set of promotion plans.
     * @return Set of promotion plans.
     */
    public PrplPromoPlanData[] getPromoPlans()
    {
        return i_promoPlans.getAvailableArray();
    }

    /** Sets the promotion plan data.
     * @param p_promoPlans Set of promotion plans.
     */
    public void setPromoPlans(PrplPromoPlanDataSet p_promoPlans)
    {
        i_promoPlans = p_promoPlans;
    }

    /**
     * Returns the Id of the default promotion plan of the given service class. Used for the promotion
     * plan box on the screen. Has to return empty string if not set or the promotion plan is invalid.
     * @param p_market The market of the service class.
     * @param p_serviceClass The service class.
     * @return Id of the default promotion plan.
     */
    public String getDefaultPromoPlan(
                  Market      p_market,
                  String      p_serviceClass)
    {
        CmdfCustMastDefaultsData l_cmdf = i_cmdfDataSet.getData(
            p_market, new ServiceClass(p_serviceClass));
        String l_defaultPromoPlanId;

        if ( l_cmdf != null )
        {
            l_defaultPromoPlanId = l_cmdf.getPromotionPlan();

            // Check whether promotion plan found is valid. Set of all valid plans is already on this Object
            // in i_promoPlans.
            if ( l_defaultPromoPlanId == null || i_promoPlans.getWithCode(l_defaultPromoPlanId) == null )
            {
                l_defaultPromoPlanId = "";
            }
        }
        else
        {
            l_defaultPromoPlanId = "";
        }

        return l_defaultPromoPlanId;
    } // end of getDefaultPromoPlan

    /** Returns a set of IN Ids.
     * @return Set of SDP data.
     */
    public ScpiScpInfoData[] getInIds()
    {
        return i_inIds.getAvailableArray();
    }

    /** Sets the SCP Id data.
     * @param p_inIds Set of SDP data.
     */
    public void setInIds(ScpiScpInfoDataSet p_inIds)
    {
        i_inIds = p_inIds;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if all relevant data is present.
     */
    public boolean isPopulated()
    {
        return true;
    }

    /** Returns the Market of the installed subscriber.
     * @return The subscribers market.
     */
    public Market getMarket()
    {
        return i_market;
    }

    /** Stores the Market of the installed subscriber.
     * @param p_market Subscribers market.
     */
    public void setMarket(Market p_market)
    {
        i_market = p_market;
    }

    /** Returns the MSISDN that has just been installed.
     * @return Subscribers mobile number.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /** Stores the MSISDN that has just been installed.
     * @param p_msisdn Mobile number.
     */
    public void setMsisdn(Msisdn p_msisdn)
    {
        i_msisdn = p_msisdn;
    }

    // Ppalon#1502/6272 [start]
    /** Returns the CustStatus that has just been installed.
     * @return Customer status.
     */
    public String getCustStatus()
    {
        return i_custStatus;
    }

    /** Stores the CustStatus that has just been installed.
     * @param p_custStatus Customer status.
     */
    public void setCustStatus(String p_custStatus)
    {
        i_custStatus = p_custStatus;
    }
    // Ppalon#1502/6272 [end]

    /** Determines whether CSO can specify IN ID on installation.
     * @param p_inIdAccess Flag indicating whether SDP identifier can be specified. 
     */
    public void setInIdAccess(boolean p_inIdAccess)
    {
        i_inIdAccess = p_inIdAccess;
    }

    /** Indicates whether CSO can specify IN ID on installation.
     * @return True if the CSO can specify the IN.
     */
    public boolean getInIdAccess()
    {
        return i_inIdAccess;
    }

    /** Set text to indicate status of last attempted update.
     * @param p_infoText Status of last attempted update.
     */
    public void setInfoText(String p_infoText)
    {
        i_infoText = p_infoText;
    }

    /** Get text to indicate status of last attempted update.
     * @return Status of last attempted update.
     */
    public String getInfoText()
    {
        return i_infoText;
    }

    /** Set the request to install msisdn - to failed.
     * @param p_failedRequest Set the status to failed.
     */
    public void setFailedRequest(boolean p_failedRequest)
    {
        i_failedRequest = p_failedRequest;
    }

    /** Get failed state of installed request.
     * @return Failed status.
     */
    public boolean getFailedRequest()
    {
        return i_failedRequest;
    }

    /** Sets the failed msisdn.
     * @param p_oldMsisdn Mobile number.
     */
    public void setOldMsisdn(String p_oldMsisdn)
    {
        i_oldMsisdn = p_oldMsisdn;
    }

    /** Gets the failed msisdn.
     * @return Mobile number.
     */
    public String getOldMsisdn()
    {
        return (i_oldMsisdn);
    }

    /** Sets the type of Msisdn being installed - (STANDALONE or SUBORDINATE).
     * @param p_oldMsisdnType Type of subscriber.
     */
    public void setOldOption(String p_oldMsisdnType)
    {
        i_oldMsisdnType = p_oldMsisdnType;
    }

    /**  Gets the set of Msisdn type - The first in the list is the choosen
     *   Type, selected as a result of subscriber installation.
     * @return Type of account and subscriber.
     */
    public String [] getOldType()
    {
        Vector     l_msisdnV = new Vector (20, 10);
        String  [] l_accountTypeArr = null;
        boolean    l_addSubordinatePermitted = i_gopaData.addSubordinateIsPermitted();
        
        if ( l_addSubordinatePermitted)  // Allowed to install a subordinate
        {
            if ( i_failedRequest == true)
            {
                if ( i_oldMsisdnType.equals("STANDALONE"))
                {
                    l_msisdnV.addElement("STANDALONE");
                    l_msisdnV.addElement("SUBORDINATE");
                }
                else
                {
                    l_msisdnV.addElement("SUBORDINATE");
                    l_msisdnV.addElement("STANDALONE");
                }
            }
            else
            {
                l_msisdnV.addElement("STANDALONE");
                l_msisdnV.addElement("SUBORDINATE");
            }
        }
        else // not allowed to install a subordinate
        {
            l_msisdnV.addElement("STANDALONE");
        }
        
        l_accountTypeArr = new String [l_msisdnV.size()];
        l_accountTypeArr = (String []) l_msisdnV.toArray (l_accountTypeArr);

        return ( l_accountTypeArr );
    }

    /** Set the Agent for failed installed subscriber.
     * @param p_oldAgent Agent identifier.
     */
    public void setOldAgent(String p_oldAgent)
    {
        i_oldAgent = p_oldAgent;
    }

    /** Gets the Agent for failed installed subscriber.
     * @return Agent identifier.
     */
    public String getOldAgent()
    {
        return (i_oldAgent) ;
    }

    /** Stores the Market of the failed installed subscriber.
     * @param p_market Market.
     */
    public void setFailedMarket(Market p_market)
    {
        i_intMarket = p_market;
    }

    /** Returns the Market of the failed installed subscriber.
     * @return Market associated with failed install.
     */
    public Market getFailedMarket()
    {
        return i_intMarket;
    }

    /** Sets the available service class for failed installed subscriber.
     * @param p_oldClass Old service class.
     */
    public void setOldServiceClass(String p_oldClass)
    {
        i_oldClass = p_oldClass;
    }

    /** Returns a  service class for the failed installed subscriber.
     * @return Old service class.
     */
    public String getOldServiceClass()
    {
        return i_oldClass;
    }

    /** Sets ScpId for failed installed subscriber.
     * @param p_oldinId SDP identifier.
     */
    public void setOldinId(String p_oldinId)
    {
        i_oldinId = p_oldinId;
    }

    /** Returns ScpId for failed installed subscriber.
     * @return SDP identifier.
     */
    public String getOldinId()
    {
       return i_oldinId;
    }

    /** Sets Promotion plan for failed installed subscriber.
     * @param p_oldPromo Promotion Plan identifier.
     */
    public void setOldPromo(String p_oldPromo)
    {
        i_oldPromo = p_oldPromo;
    }

    /** Return promotion plan for failed installed subscriber.
     * @return Promotion plan identifier.
     */
    public String getOldPromo()
    {
       return i_oldPromo;
    }

    /** Set Master Msisdn which failed installation.
     * @param p_masterMsisdn Mobile number of account holder.
     */
    public void setOldMaster(Msisdn p_masterMsisdn)
    {
        i_masterMsisdn = p_masterMsisdn;
    }

    /** Get Master which failed installation.
     * @return Mobile number of account holder.
     */
    public Msisdn getOldMaster()
    {
       return i_masterMsisdn;
    }

    /** Get Master which failed installation as a formatted string.
     * @return String representing the mobile number of the account holder.
     */
    public String getOldMasterAsString()
    {
       return formatMsisdn(i_masterMsisdn);
    }
    
    /**
     * Returns the service offerings for a failed installed subscriber 
     * @param p_oldServiceOffering Old value for service offering.
     */
    public void setOldServiceOffering(String p_oldServiceOffering) 
    {
        i_oldServiceOffering = p_oldServiceOffering;    
    }
    /**
     * Sets the service offerings for a failed installed subscriber
     * @return The old service offering.
     */
    
    public String getOldServiceOffering()
    {
        return i_oldServiceOffering;
    }
    
    /**
     * Returns the account group Id  for a failed installed subscriber 
     * @param p_oldAccountGroup The old account group identifier.
     */
    public void setOldAccountGroup(String p_oldAccountGroup) 
    {
        i_oldAccountGroup = p_oldAccountGroup;    
    }

    /**
     * Sets the account group ID for a failed installed subscriber
     * @return The old account group identifier.
     */
    public String getOldAccountGroup()
    {
        return i_oldAccountGroup;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setSubscriberSegmentationFeatureLicence(boolean p_isLicenced)
    {
        i_hasSubscriberSegmentationFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if the feature licence is valid.
     */
    public boolean hasSubscriberSegmentationFeatureLicence()
    {
        return i_hasSubscriberSegmentationFeatureLicence;
    }
    
    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setOverrideMsisdnQuarantinePeriodFeatureLicence(boolean p_isLicenced)
    {
        i_hasOverrideMsisdnQuarantinePeriodFeatureLicence = p_isLicenced;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if the feature licence is valid.
     */
    public boolean hasOverrideMsisdnQuarantinePeriodFeatureLicence()
    {
        return i_hasOverrideMsisdnQuarantinePeriodFeatureLicence;
    }

    /**
     * Sets a boolean value indicating whether the feature has a valid licence.
     * @param p_isLicenced True if the feature licence should be enabled.
     */
    public void setSingleStepInstallFeatureLicence(boolean p_isLicenced)
    {
        i_hasSingleStepInstallFeatureLicence = p_isLicenced;
        return;
    }

    /**
     * Returns a boolean value indicating whether the feature has a valid licence.
     * @return True if the feature licence is valid.
     */
    public boolean isSingleStepInstallFeatureActive()
    {
        return i_hasSingleStepInstallFeatureLicence;
    }

    /**
     * Sets a boolean value indicating that all installs will be single step.
     * @param p_isSingleStep True if all installs are to be single step.
     */
    public void setForcedSingleStepInstall(boolean p_isSingleStep)
    {
        i_forceSingleStepInstall = p_isSingleStep;
        return;
    }
    
    /**
     * Returns a boolean value indicating whether to force single step installs.
     * @return True if single step installs are to be forced
     */
    public boolean isForcedSingleStepInstall()
    {
        return i_forceSingleStepInstall;
    }
    
    /**
     * Sets Customer Defaults Cache. Used to retrieve the default promotion plan for the service class.
     * @param p_cmdfDataSet The Customer Defaults Cache.
     */
    public void setCmdfCache(CmdfCustMastDefaultsDataSet p_cmdfDataSet)
    {
        i_cmdfDataSet = p_cmdfDataSet;
    }

}
