////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentApprovalServlet.Java
//      DATE            :       14-Oct-2003
//      AUTHOR          :       Steven James
//      REFERENCE       :       PpaLon#2071/8521
//                              PRD_PPAK00_GEN_CA_464
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Adjustment Approvals screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 4/06/10  | Siva Sanker| Adjustment approval source OPID | PpacBan#3693/13761
//          | Reddy      | is included.                    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcAdjustmentApprovalScreenData;
import com.slb.sema.ppas.cc.service.CcAdjustmentApprovalService;
import com.slb.sema.ppas.cc.service.CcHoldAdjustmentDataSetResponse;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * Servlet to handle requests from the Adjustment Approval Screen.
 */
public class CcAdjustmentApprovalServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdjustmentApprovalServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Adjustment Approval Service to be used by this servlet. */
    private CcAdjustmentApprovalService i_ccAdjAppSer = null;
    
    /** Average time in ms taken to approve an adjustment. */ 
    private Integer i_adjApprovalTime = null;
    
    /** Average time in ms taken to reject an adjustment. */ 
    private Integer i_adjRejectTime = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcAdjustmentApprovalServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START | WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10000, this,
                           "Constructing/ed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /**
     * Initialise servlet.
     */
    protected void doInit()
    {
        i_adjApprovalTime = (Integer)i_guiContext.getAttribute(
                                "com.slb.sema.ppas.support.GuiContext.adjApprovalTime");

        i_adjRejectTime = (Integer)i_guiContext.getAttribute(
                                "com.slb.sema.ppas.support.GuiContext.adjRejectTime");
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";

    /** Service method to control requests and responses to the JSP script
     *  for the Adjustment Approvals screen service.
     *
     * @param p_request  The HTTP request
     * @param p_response The HTTP response
     * @param p_httpSession  The HTTP session
     * @param p_guiRequest  The GUI request
     * @return the forwarding URL
     */
    protected String doService(HttpServletRequest     p_request,
                               HttpServletResponse    p_response,
                               HttpSession            p_httpSession,
                               GuiRequest             p_guiRequest)
    {
        String                          l_command                      = null;
        String                          l_forwardUrl                   = null;
        CcAdjustmentApprovalScreenData  l_adjustmentApprovalScreenData = null;
        GuiResponse                     l_guiResponse                  = null;
        boolean                         l_accessGranted                = false;
        GopaGuiOperatorAccessData       l_gopaData                     = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 10060, this,
                           "ENTERED " + C_METHOD_doService);
        }

        // Construct CcAdjustmentApprovalService if it does not already exist 
        
        if (i_ccAdjAppSer == null)
        {
            i_ccAdjAppSer = new CcAdjustmentApprovalService(p_guiRequest,
                                                           0L,
                                                           i_logger,
                                                           i_guiContext);
        }

        // Create screen data object.
        l_adjustmentApprovalScreenData = new CcAdjustmentApprovalScreenData(i_guiContext,
                                                                            p_guiRequest);
        
        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0L,
                                      p_request,
                                      "p_command",
                                      true ,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "UPDATE",
                                                     "GET_FAILURE_DETAILS"});

            // Determine if the CSO has privilege to access the requested screen
            // or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA") ||
                 l_command.equals("UPDATE") ||
                 l_command.equals("GET_FAILURE_DETAILS")) &&
                 l_gopaData.hasAdjApprovalScreenAccess())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(
                                        p_guiRequest,
                                        p_guiRequest.getGuiSession().getSelectedLocale(),
                                        GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                        new Object [] {},
                                        GuiResponse.C_SEVERITY_FAILURE
                                        );

                l_adjustmentApprovalScreenData.addRetrievalResponse(p_guiRequest,
                                                                    l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData",
                                       l_adjustmentApprovalScreenData);
            }
            else
            {
                getBusinssConfigData(l_adjustmentApprovalScreenData);
        
                if (l_command.equals("GET_FAILURE_DETAILS"))
                {
                    getFailureDetails(p_guiRequest,
                                      p_request,
                                      l_adjustmentApprovalScreenData);
                }
                else
                {
                    if (l_command.equals("UPDATE"))
                    {
                        updateHoldAdjustments(p_guiRequest,
                                              p_request,
                                              l_adjustmentApprovalScreenData);
                    }

                    getHoldAdjustments(p_guiRequest,
                                       l_adjustmentApprovalScreenData);
                }
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 10070, this,
                               "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam(p_guiRequest,
                                               l_ppasSE);

            l_adjustmentApprovalScreenData.addRetrievalResponse(p_guiRequest,
                                                                l_guiResponse);
        } 
        
        if (l_accessGranted)
        {
            if (l_adjustmentApprovalScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Adjustments Approval screen.  
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = new String("/jsp/cc/ccerror.jsp");

                p_request.setAttribute("p_guiScreenData", 
                                       l_adjustmentApprovalScreenData);
            } 
            else
            {
                if (l_command.equals("GET_FAILURE_DETAILS"))
                {
                    l_forwardUrl = "/jsp/cc/ccadjustmentapprovaldetail.jsp";
                }
                else
                {
                    l_forwardUrl = "/jsp/cc/ccadjustmentapproval.jsp";
                }

                p_request.setAttribute("p_guiScreenData",
                                       l_adjustmentApprovalScreenData);
            }
        } 

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 10080, this,
                           "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }

        return l_forwardUrl;
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateHoldAdjustments = "updateHoldAdjustments";
    /**
     * Update selected adjustments synchronously.
     *
     * @param p_guiRequest  The GUI request.
     * @param p_request  The HTTP request.
     * @param p_screenData  The screen data.
     */
    private void updateHoldAdjustments(GuiRequest                     p_guiRequest,
                                       HttpServletRequest             p_request,
                                       CcAdjustmentApprovalScreenData p_screenData)        
    {
        GuiResponse     l_guiResponse           = null;
        int             l_numberToAction        = 0;
        int             l_count                 = 0;
        int             l_index                 = 0;
        int             l_approveSuccessCount   = 0;
        int             l_rejectSuccessCount    = 0;
        boolean         l_submit;
        String          l_custId                = null;
        String          l_rejectAction          = null;
        String          l_approveAction         = null;
        PpasDateTime    l_dateTime              = null;
        PpasDateTime    l_currentTime           = null;
        GuiRequest      l_newGuiRequest         = null;
        String          l_dedacId               = null;
        String          l_servClass             = null;
        String          l_adjType               = null;
        String          l_adjCode               = null;
        Money           l_amount                = null;
        String          l_sourceOpid            = null;        

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10090, this,
                           "Entered " + C_METHOD_updateHoldAdjustments);
        }

        // Get params from request...
        try
        {
            l_numberToAction = Integer.parseInt(getValidParam(p_guiRequest,
                                                              0L,
                                                              p_request,
                                                              "p_forAction",
                                                              true,
                                                              "",
                                                              PpasServlet.C_TYPE_NUMERIC));

            // Loop on the number of items to action and process each set of data retrieved
            while (l_count < l_numberToAction)
            {
                // Reset request submit flag for each request
                l_submit = true;

                l_rejectAction = getValidParam(p_guiRequest,
                                               C_FLAG_NULL_IF_ABSENT,
                                               p_request,
                                               "rejectCB" + l_index,
                                               false,
                                               null,
                                               PpasServlet.C_TYPE_ANY);

                l_approveAction = getValidParam(p_guiRequest,
                                                C_FLAG_NULL_IF_ABSENT,
                                                p_request,
                                                "approveCB" + l_index,
                                                false,
                                                null,
                                                PpasServlet.C_TYPE_ANY);

                // Check if reject request
                if (l_rejectAction != null)
                {
                    try
                    {
                        // Get the cust ID and date only as relevant to a rejection request
                        l_custId = getValidParam(p_guiRequest,
                                                 0L,
                                                 p_request,
                                                 "p_custId" + l_index,
                                                 true,
                                                 "",
                                                 PpasServlet.C_TYPE_NUMERIC);

                        l_dateTime = new PpasDateTime(getValidParam(p_guiRequest,
                                                      PpasServlet.C_FLAG_DATETIME_ONLY,
                                                      p_request,
                                                      "p_date" + l_index,
                                                      true,
                                                      "",
                                                      PpasServlet.C_TYPE_DATE));
                    }
                    catch (PpasServletException l_pSE)
                    {
                        if (WebDebug.on)
                        {
                            WebDebug.print(WebDebug.C_LVL_VLOW,
                                           WebDebug.C_APP_SERVLET,
                                           WebDebug.C_ST_ERROR,
                                           p_guiRequest, C_CLASS_NAME, 10110, this,
                                           "Exception parsing rejection request parameters: " + l_pSE);
                        }
    
                        // Set flag to indicate that this request must not be submitted
                        // due to problems parsing data
                        l_submit = false;
    
                        // Convert the exception into a suitable response...
                        l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);
    
                        // Add the response to the screen data object as a normal  update type response.
                        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
                    }
                    finally
                    {
                        l_count++;
                    }

                    if (l_submit)
                    {
                        // Submit the request even if a previous error was reported from the services
                        // as the service is now synchronous, contrary to the original design.
                        l_newGuiRequest = new GuiRequest(l_custId,
                                                         p_guiRequest.getOpid(),
                                                         p_guiRequest.getGuiSession());

                        l_guiResponse = i_ccAdjAppSer.rejectHoldAdjustment(l_newGuiRequest,
                                                                           i_timeout,
                                                                           l_dateTime);

                        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

                        if (!p_screenData.hasUpdateError())
                        {
                            l_rejectSuccessCount++;
                        }
                    }
                }
                // Check if approval request
                else if (l_approveAction != null)
                {
                    try
                    {
                        // Get all the parameters relevant to an approval request
                        l_custId = getValidParam(p_guiRequest,
                                                 0L,
                                                 p_request,
                                                 "p_custId" + l_index,
                                                 true,
                                                 "",
                                                 PpasServlet.C_TYPE_NUMERIC);

                        l_dateTime = new PpasDateTime(getValidParam(p_guiRequest,
                                                                    PpasServlet.C_FLAG_DATETIME_ONLY,
                                                                    p_request,
                                                                    "p_date" + l_index,
                                                                    true,
                                                                    "",
                                                                    PpasServlet.C_TYPE_DATE));

                        l_dedacId = getValidParam(p_guiRequest,
                                                  PpasServlet.C_FLAG_ALLOW_BLANK,
                                                  p_request,
                                                  "p_id" + l_index,
                                                  false,
                                                  "",
                                                  PpasServlet.C_TYPE_ANY);

                        if (l_dedacId.length() > 0)
                        {
                            l_servClass = getValidParam(p_guiRequest,
                                                        0L,
                                                        p_request,
                                                        "p_servClass" + l_index,
                                                        true,
                                                        "",
                                                        PpasServlet.C_TYPE_ANY);
                        }

                        l_adjType = getValidParam(p_guiRequest,
                                                  0L,
                                                  p_request,
                                                  "p_type" + l_index,
                                                  true,
                                                  "",
                                                  PpasServlet.C_TYPE_ANY);

                        l_adjCode = getValidParam(p_guiRequest,
                                                  0L,
                                                  p_request,
                                                  "p_code" + l_index,
                                                  true,
                                                  "",
                                                  PpasServlet.C_TYPE_ANY);
                        
                        l_sourceOpid = getValidParam(p_guiRequest,
                                                  0L,
                                                  p_request,
                                                  "p_sourceOpid" + l_index,
                                                  true,
                                                  "",
                                                  PpasServlet.C_TYPE_ANY);
                        

                        String l_amountAsString = getValidParam(p_guiRequest,
                                                                PpasServlet.C_FLAG_STRIP_PLUS_SIGN,
                                                                p_request,
                                                                "p_amount" + l_index,
                                                                true,
                                                                "",
                                                                PpasServlet.C_TYPE_ANY);
                        
                        String l_currencyAsString = getValidParam(p_guiRequest,
                                                                  0L,
                                                                  p_request,
                                                                  "p_currency" + l_index,
                                                                  true,
                                                                  "",
                                                                  PpasServlet.C_TYPE_ANY);
                        
                        try
                        {
                            l_amount = i_guiContext.getMoneyFormat().parse(l_amountAsString,
                                                  i_guiContext.getCurrency(l_currencyAsString));
                        }
                        catch (ParseException l_pe)
                        {
                            PpasServletException l_e =
                                new PpasServletException( C_CLASS_NAME,
                                        C_METHOD_updateHoldAdjustments,
                                        10121,
                                        this,
                                        p_guiRequest,
                                        0,
                                        ServletKey.get().parseMoneyError(
                                                             l_amountAsString,
                                                             i_guiContext.getCurrency(l_currencyAsString)),
                                        l_pe);
                            
                            i_logger.logMessage(l_e);
                            throw l_e;
                        }
                    }
                    catch (PpasServletException l_pSE)
                    {
                        if (WebDebug.on)
                        {
                            WebDebug.print(WebDebug.C_LVL_VLOW,
                                           WebDebug.C_APP_SERVLET,
                                           WebDebug.C_ST_ERROR,
                                           p_guiRequest, C_CLASS_NAME, 10120, this,
                                           "Exception parsing approval request parameters: " + l_pSE);
                        }

                        // Set flag to indicate that this request must not be submitted
                        // due to problems parsing data
                        l_submit = false;

                        // Convert the exception into a suitable response...
                        l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

                        // Add the response to the screen data object as a normal update type response.
                        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
                    }
                    finally
                    {
                        l_count++;
                    }

                    if (l_submit)
                    {
                        // Submit the request even if a previous error was reported from the services
                        // as the service is now synchronous, contrary to the original design.
                        l_newGuiRequest = new GuiRequest(l_custId,
                                                         p_guiRequest.getOpid(),
                                                         p_guiRequest.getGuiSession());

                        l_guiResponse = i_ccAdjAppSer.approveHoldAdjustment(l_newGuiRequest,
                                                                            i_timeout,
                                                                            l_dedacId,
                                                                            l_adjType,
                                                                            l_adjCode,
                                                                            l_amount,
                                                                            l_servClass,
                                                                            l_dateTime,
                                                                            l_sourceOpid);

                        p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

                        if (!p_screenData.hasUpdateError())
                        {
                            l_approveSuccessCount++;
                        }
                    }
                }
                l_index++;
            }
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 10100, this,
                               "Exception parsing parameters, no request could be submitted: " + l_pSE);
            }

            // Convert the exception into a suitable response...
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            // Add the response to the screen data object as a normal  update type response.
            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText("Failed to approve one or more adjustments at " + l_currentTime +
                                     ".<br>Check screen for list of outstanding adjustments.");
        }
        else
        {
            p_screenData.setInfoText("Successfully approved " + l_approveSuccessCount +
                                     ((l_approveSuccessCount) == 1 ? " adjustment" : " adjustments") +
                                     " and rejected " + l_rejectSuccessCount + 
                                     ((l_rejectSuccessCount) == 1 ? " adjustment." : " adjustments."));
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 10130, this,
                           "Leaving " + C_METHOD_updateHoldAdjustments);
        }
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getHoldAdjustments = "getHoldAdjustments";

    /**
     * Gets the Adjustments currently held for approval.
     *
     * @param p_guiRequest  The GUI request
     * @param p_screenData  The screen data
     */
    private void getHoldAdjustments(GuiRequest                     p_guiRequest,
                                    CcAdjustmentApprovalScreenData p_screenData)        
    {
        CcHoldAdjustmentDataSetResponse l_ccHoldAdjustmentDataSetResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10290, this,
                           "Entered " + C_METHOD_getHoldAdjustments);
        }
        
        l_ccHoldAdjustmentDataSetResponse = i_ccAdjAppSer.getHoldAdjustments(p_guiRequest,
                                                                             i_timeout);
        
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccHoldAdjustmentDataSetResponse);
        
        if (l_ccHoldAdjustmentDataSetResponse.isSuccess())
        {
            p_screenData.setHoldAdjustmentDataSet(
                            l_ccHoldAdjustmentDataSetResponse.getHoldAdjustmentDataSet());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 10230, this,
                           "Leaving " + C_METHOD_getHoldAdjustments);
        }
    }
    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getFailureDetails = "getFailureDetails";

    /**
     * Method to implement the GET_FAILURE_DETAIL request.
     * @param p_guiRequest the GuiRequest
     * @param p_httpRequest the HttpRequest
     * @param p_screenData the screen data object
     */
    private void getFailureDetails(GuiRequest                     p_guiRequest,
                                   HttpServletRequest             p_httpRequest,
                                   CcAdjustmentApprovalScreenData p_screenData)
    {
        GuiResponse l_guiResponse;
        String      l_failureCode;
        String      l_failureDateTime;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 10300, this,
                           "Entered " + C_METHOD_getFailureDetails);
        }

        try
        {
            l_failureCode = getValidParam(p_guiRequest,
                                          0L,
                                          p_httpRequest,
                                          "p_failureCode",
                                          true,
                                          "",
                                          PpasServlet.C_TYPE_NUMERIC);

            p_screenData.setHoldFailureReasonCode(Integer.parseInt(l_failureCode));
            
            l_failureDateTime = getValidParam(p_guiRequest,
                                              PpasServlet.C_FLAG_DATETIME_ONLY,
                                              p_httpRequest,
                                              "p_failureDateTime",
                                              true,
                                              "",
                                              PpasServlet.C_TYPE_DATE);
        
            p_screenData.setHoldFailureDateTime(l_failureDateTime);
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 10200, this,
                               "Exception parsing parameters: " + l_pSE);
            }

            l_guiResponse = handleInvalidParam(p_guiRequest,
                                               l_pSE);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 10330, this,
                           "Leaving " + C_METHOD_getFailureDetails);
        }
    }
    
    /**
     * Set business config data on the screen data object. This data includes the
     * average time to approve and reject and adjustment, and the asce code data.
     * @param p_screenData screen data object
     */
    private void getBusinssConfigData(CcAdjustmentApprovalScreenData p_screenData)
    {
        p_screenData.setApproveAdjTime(i_adjApprovalTime.intValue());
        p_screenData.setRejectAdjTime(i_adjRejectTime.intValue());
        p_screenData.setAdjFailureReasonData(i_configService.getAllAsce());
    }
}
