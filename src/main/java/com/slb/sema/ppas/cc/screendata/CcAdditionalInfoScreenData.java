////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdditionalInfoScreenData.java
//      DATE            :       7-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1236/5006
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Additional
//                              information screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Additonal Information screen.
 */
public class CcAdditionalInfoScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AdditionalInfoData object wrapped by this object. */
    private AdditionalInfoData i_additionalInfoData = null;

    /** Text giving info on when/whether an update was made on this screen. */
    private String i_infoText;

    /** Flag to indicate whether a successful update has been done. */
    private boolean i_successfulUpdate;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * @param p_guiContext  GUI context object
     * @param p_guiRequest  GUI request object
     */
    public CcAdditionalInfoScreenData( GuiContext p_guiContext,
                                       GuiRequest p_guiRequest)
    {
        super( p_guiContext, p_guiRequest);
        i_infoText = "";
        i_successfulUpdate = false;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns <code>true</code> if the Operator making the request is
     * permitted to update the Additional Info Details.
     * Otherwise <code>false</code> is returned.
     * @return True if update is allowed.
     */
    public boolean addInfoUpdateIsPermitted()
    {
        return (i_gopaData.addInfoUpdateIsPermitted());
    }

    /**
     * Returns the AdditionalInfoData object wrapped within this object.
     * Returns null if there is no AdditionalInfoData object set.
     * @return Additional information data.
     */
    public AdditionalInfoData getAdditionalInfoData()
    {
        return ( i_additionalInfoData );
    }

    /** Sets <code>i_additionalInfoData</code> to 
     * <code>p_additionalInfoData</code>.
     * @param p_additionalInfoData Additional information data.
     */
    public void setAdditionalInfoData(
                    AdditionalInfoData p_additionalInfoData)
    {
        i_additionalInfoData = p_additionalInfoData;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return ( (i_additionalInfoData != null) &&
                 (i_additionalInfoData.getMiscFieldTitlesData() != null) );
    }

    /** Sets the text giving info on when/whether an update was done on this
     *  screen.
     * @param p_text Description of whether the update was done.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** Gets the text giving info when/whether an update was done on this
     *  screen.
     * @return Description of whether an update was done.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** Sets flag to indicate whether a successful update had been done.
     * @param p_success True if the update was successful. 
     */
    public void setSuccessfulUpdate(boolean p_success)
    {
        i_successfulUpdate = p_success;
    }

   /** Gets flag to indicate whether a successful update has been done.
    * @return True if the update was successful.
    */
   public boolean getSuccessfulUpdate()
   {
       return(i_successfulUpdate);
   }


} // end public class CcAdditionalInfoScreenData
