////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcChargedEventCountersUT.java
//      DATE            :       21-Jan-2006
//      AUTHOR          :       Michael Erskine (40771f)
//      REFERENCE       :       PpacLon#1967/7122
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for the GUI Counters screen. 
//                 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//14-Sep-06 |Andy Harris |Changed to use new methods in    |PpacLon#2586/9791
//          |            |super class.                     |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.io.IOException;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.Button;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebRequest;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework (v 1.5.4)
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcChargedEventCountersUT extends GuiTestTT
{
    private static final String C_TABLE_ID_COUNTER_HISTORY = "CounterHistory";
    
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcChargedEventCountersUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        super.setUp(true);
    }
    
    /**
     * @ut.when An operator navigates to the Counters screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadCountersScreen()
    {        
        beginOfTest("Start testLoadCountersScreen");
        
        // Install MSISDN and load Maintain Subsriber Screen
        installAndLoadSubScreen(0);
        
        // Access/test specific screen
        sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));

        // Validate that correct page is returned and that the content is correct
        validateResponse(null, null, "Customer Details");
           
        verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 0, 
                         new String[] {"Counter ID", "Description", "Total Counter",
                                       "Period Counter", "Period Counter Clear Date"});
        verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                         new String[] {"201", "Charged Service Class Change", "", "", ""});
        verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 2,
                         new String[] {"202", "Charged FaF Number Addition", "", "", ""});
        verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 3,
                         new String[] {"203", "Charged Balance Enquiry", "", "", ""});
                    
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts a valid ADD update to an event counter on the Counters screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateCountersAddSuccess()
    {   
        beginOfTest("Start testUpdateCountersAddSuccess");
        
        try
        {
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "5", "3", "ADD", "28-Jan-2006", "");
            
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "5", "3", "28-Jan-2006"});
        
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts a valid SUBTRACT update to an event counter on the Counters screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateCountersSubtractSuccess()
    {   
        beginOfTest("Start testUpdateCountersSubtractSuccess");
        
        try
        {
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "5", "3", "ADD", "28-Jan-2006", "");
            
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "5", "3", "28-Jan-2006"});
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "3", "2", "SUBTRACT", "28-Jan-2006", "");
            
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "2", "1", "28-Jan-2006"});
        
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }

    
    /**
     * @ut.when An operator posts an ADD update to a period event counter that would cause an Overflow error.
     * @ut.then A response is returned containing the correct error message.
     */
    public void testUpdateCountersPeriodOverflow()
    {   
        beginOfTest("Start testUpdateCountersPeriodOverflow");
        
        try
        {   
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "0", "500", "ADD", "28-Jan-2006", "");
            
            validateResponse("<00192> The counter balance adjustment failed as it would have caused" +
                                     " the balance of the counter to exceed the maximum value.",
                                     null, "Customer Details");
            
            // Verify no row inserted in history table
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "", "", ""});
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts an SUBTRACT update to a period event counter that would cause an 
     *          Underflow error.
     * @ut.then A response is returned containing the correct error message.
     */
    public void testUpdateCountersPeriodUnderflow()
    {   
        beginOfTest("Start testUpdateCountersPeriodUnderflow");
        
        try
        {   
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "0", "10", "SUBTRACT", "28-Jan-2006", "");
            
            validateResponse("<00193> The counter balance adjustment failed as it would have caused" +
                                     " the balance of the counter to drop below the minimum value.",
                                     null, "Customer Details");
            
            // Verify no row inserted in history table
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "", "", ""});
        
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts an ADD update to a total event counter that would cause an Overflow error.
     * @ut.then A response is returned containing the correct error message.
     */
    public void testUpdateCountersTotalOverflow()
    {   
        beginOfTest("Start testUpdateCountersTotalOverflow");
        
        try
        {   
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "500", "0", "ADD", "28-Jan-2006", "");
            
            validateResponse("<00192> The counter balance adjustment failed as it would have caused" +
                                     " the balance of the counter to exceed the maximum value.",
                                     null, "Customer Details");
            
            // Verify no row inserted in history table
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "", "", ""});
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts a SUBTRACT update to a Total event counter that would cause an Underflow 
     *          error.
     * @ut.then A response is returned containing the correct error message.
     */
    public void testUpdateCountersTotalUnderflow()
    {   
        beginOfTest("Start testUpdateCountersTotalUnderflow");
        
        try
        {   
            installAndLoadSubScreen(0);
            
            sendRequest(constructGetRequest("cc/ChargedEventCounters", "GET_SCREEN_DATA"));
            
            setCounterParametersAndSendForm("201", "10", "0", "SUBTRACT", "28-Jan-2006", "");
            
            validateResponse("<00193> The counter balance adjustment failed as it would have caused" +
                                     " the balance of the counter to drop below the minimum value.",
                                     null, "Customer Details");
            
            // Verify no row inserted in history table
            verifyRowInTable(C_TABLE_ID_COUNTER_HISTORY, 1,
                             new String[] {"201", "Charged Service Class Change", "", "", ""});
        
        }
        catch (Exception l_e)
        {
            failedTestException(l_e);
        }
        
        endOfTest();
    }
    
 
    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        System.out.println(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcChargedEventCountersUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------
    
    /**
     * Gets the Counters form from the <code>WebResponse</code> and attempts to set various parameters
     * on it, then submits it.
     * @throws SAXException
     * @throws IOException
     */
    private void setCounterParametersAndSendForm(String p_counterId,
                                                 String p_totCounterAdjVal,
                                                 String p_periodCounterAdjVal,
                                                 String p_adjOperation,
                                                 String p_newPeriodClrDate,
                                                 String p_oldPeriodClrDate)
        throws SAXException, IOException
    {
        WebForm    l_countersForm = null;
        Button     l_submitButton = null;
        WebRequest l_request      = null;
        
        say("Entered setCounterParametersAndSendForm():");
        l_countersForm = c_webResponse.getFormWithName("chargedEventCounters");
        assertNotNull("In setCounterParametersAndSendForm:- Counters form is null", l_countersForm);
        
        l_submitButton = l_countersForm.getButtonWithID("adjustButton");
        
        //TODO: Force entry to else statement instead for the moment
        if (false && l_submitButton != null)
        {
        
            //l_countersForm.setParameter("counterIdSelect", "201 - Charged Service Class Change");
            l_countersForm.setParameter("counterIdSelect", "201");
            l_countersForm.setParameter("totCounterAdjVal", "5");
            l_countersForm.setParameter("periodCounterAdjVal", "3");
            l_countersForm.setParameter("newPeriodClrDate", "28-Jan-2006");
            // Can't do this since p_command is a hidden parameter.
            //l_countersForm.setParameter("p_command", "POST_ADJUSTMENT");
            say("Clicking Counters screen submit button");
            l_submitButton.click();
        }
        else
        {
            l_request = new GetMethodWebRequest(c_urlPrefix + 
                                                "ascs/gui/cc/ChargedEventCounters?p_command=POST_ADJUSTMENT" +
                                                "&counterId=" + p_counterId + 
                                                "&totCounterAdjVal=" + p_totCounterAdjVal +
                                                "&periodCounterAdjVal=" + p_periodCounterAdjVal + 
                                                "&adjOperation=" + p_adjOperation +
                                                "&newPeriodClrDate=" + p_newPeriodClrDate +
                                                "&oldPeriodClrDate=" + p_oldPeriodClrDate);
            sendRequest(l_request);
        }
        
        //c_webResponse = l_countersForm.submit();
        say("!!!Submitted Counters form!!!");      
    }
    
}

