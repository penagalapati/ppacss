////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPromotionsScreenData.java
//      DATE            :       30-Apr-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Gui Screen Data object for the Customer
//                              Promotions screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.dataclass.CustPromoAllocDataSet;
import com.slb.sema.ppas.common.dataclass.PromotionCreditDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Customer Promotions screen. Contains all data
 * required for that screen display and helper methods to reduce the amount of
 * code required in the JSP script for that screen.
 */
public class CcPromotionsScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPromotionsScreenData";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** The data set of promotion allocations for the current subscriber. */
    private CustPromoAllocDataSet  i_custAllocationsDataSet;

    /** The data set of promotional credit history for the current subscriber.
     */
    private PromotionCreditDataSet i_custPromoCreditDataSet;

    /** The promotions related business configuration data. */
    private PrplPromoPlanDataSet   i_promoConfigData;

    /** Date/time string defining when a successful update last occurred. */
    private String i_updateSuccessDTStr;
    
    /** Indicates whether the subscriber in the request for this screen
     * is a subordinate or not.
     */
    private boolean i_isSubordinate;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Creates an 'empty' promotions screen data object. That is,
     *  not containing the data required for the screen generation.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcPromotionsScreenData(
        GuiContext p_guiContext,
        GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 95000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_custAllocationsDataSet = null;
        i_custPromoCreditDataSet = null;
        i_promoConfigData        = null;
        i_updateSuccessDTStr     = "";

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 95090, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns <code>true</code> if the Operator who generated the request
     * is permitted to update Promotions details.
     * Otherwise <code>false</code> is returned.
     * @return True if update is allowed.
     */
    public boolean promotionsUpdateIsPermitted()
    {
        return (i_gopaData.promotionsUpdateIsPermitted());
    }

    /** Initialise the data set of customer promotion allocations.
     *  @param p_guiRequest    The request being processed.
     *  @param p_allocationsDataSet Promotion allocations for the current 
     *                              customer.
     */
    public void setCustAllocations(
        GuiRequest               p_guiRequest,
        CustPromoAllocDataSet    p_allocationsDataSet)
    {
        i_custAllocationsDataSet = p_allocationsDataSet;
        return;
    }

    /** Get the data set of customer promotion allocations.
     *  @return Set of customer promotion allocations.
     */
    public CustPromoAllocDataSet getCustAllocations()
    {
        return (i_custAllocationsDataSet);
    }

    /** Initialise data set of promotional credits received by current 
     *  customer.
     *  @param p_guiRequest    The request being processed.
     *  @param p_promoCreditDataSet Data set of promotional credits received
     *                              by current customer.
     */
    public void setPromoCreditHistory(
        GuiRequest               p_guiRequest,
        PromotionCreditDataSet   p_promoCreditDataSet)
    {
        i_custPromoCreditDataSet = p_promoCreditDataSet;
        return;
    }

    /** Get the data set of promotional credits received by the current
     *  customer.
     *  @return Set of promotional credits received by
     *          the current customer.
     */
    public PromotionCreditDataSet getPromoCreditHistory()
    {
        return (i_custPromoCreditDataSet);
    }

    /** Set up the business configuration data relating to promotions.
     *  @param p_promoConfigData Set of promotions related business 
     *                           configuration data.
     */
    public void setPromoConfigData (PrplPromoPlanDataSet p_promoConfigData)
    {
        i_promoConfigData = p_promoConfigData;
    }

    /** Get promotion business configuration data for a given promotion plan.
     *  @param p_planId The identifier of the promotion plan to obtain the
     *                  business configuration data for.
     *  @return Promotion plan configuration for the given
     *          plan. If the given plan cannot be found or is marked as
     *          deleted, then <code>nul</code> is returned.
     */
    public PrplPromoPlanData getPromoConfigData (String p_planId)
    {
        PrplPromoPlanData l_promoData = null;

        if (i_promoConfigData != null)
        {
            l_promoData = i_promoConfigData.getWithCode (p_planId);

            if (l_promoData != null)
            {
                if (l_promoData.isDeleted())
                {
                    l_promoData = null;
                }
            }
        }
        return (l_promoData);
    }

    /** Sets string defining date/time when the last successful update
     *  occurred.
     *  @param p_updateSuccessDTStr String defining date/time when the last
     *                              successful update occurred.
     */
    public void setUpdateSuccessDTStr (String p_updateSuccessDTStr)
    {
        i_updateSuccessDTStr = p_updateSuccessDTStr;
    }

    /** Returns string defining date/time when the last successful update
     *  occurred.
     *  @return String defining date/time when the last successful update
     *          occurred.
     */
    public String getUpdateSuccessDTStr()
    {
        return (i_updateSuccessDTStr);
    }

    /** Returns true if this screen data object has been populated. Otherwise
     *  returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        boolean l_populated = false;

        if ((i_custAllocationsDataSet != null) &&
            (i_custPromoCreditDataSet != null))
        {
            l_populated = true;
        }
        return (l_populated);
    }

    /** Returns true if any promotions are configured in the PPAS database and 
     *  have been set up in this screen data object.
     *  @return <code>true</code> if promotions data is available in this
     *          object; otherwise, <code>false</code>.
     */
    public boolean promotionsConfigured()
    {
        boolean l_promosConfigured = false;

        if (i_promoConfigData != null)
        {
            if (i_promoConfigData.getAvailableArray() != null)
            {
                if (i_promoConfigData.getAvailableArray().length > 0)
                {
                    l_promosConfigured = true;
                }
            }
        }
        return (l_promosConfigured);
    }

    /** Returns true if the given promotion plan is configured and is not 
     *  marked as deleted.
     *  @param p_planId The identifier of the plan that we are to check is 
     *                  configured.
     *  @return <code>true</code> if the given promotion plan is configured
     *          and available; otherwise, <code>false</code>.
     */
    public boolean promotionConfigured (String p_planId)
    {
        boolean              l_isConfigured = false;
        PrplPromoPlanData [] l_promosArr = null;

        if (i_promoConfigData != null)
        {
            l_promosArr = i_promoConfigData.getAvailableArray();

            if (l_promosArr != null)
            {
                for (int l_index = 0;
                     l_index < l_promosArr.length;
                     l_index++)
                {
                    if (l_promosArr[l_index].getPromoPlan().equals (p_planId))
                    {
                        l_isConfigured = true;
                    }
                }
            }
        }
        return (l_isConfigured);
    }

    /** 
     * Indicates if the subscriber contained in the request for this
     * screen is a subordinate.  Returns <code>true</code> if subscriber
     * is a subordinate, otherwise returns <code>false</code>.
     * 
     * @return True if the subscriber is a subordinate, otherwise, false.
     */
    public boolean isSubordinate()
    {
        return i_isSubordinate;
    }

    /**
     * Marks the subscriber contained in the request for this screen as being
     * a subordinate.
     */
    public void setIsSubordinate()
    {
        i_isSubordinate = true;
        return;
    }

} // end public class CcPromotionsScreenData
