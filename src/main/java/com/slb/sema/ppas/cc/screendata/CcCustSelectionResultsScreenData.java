////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustSelectionResultsScreenData.java
//      DATE            :       13-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_27
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Customer
//                              Selection Results screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.CustomerAddressData;
import com.slb.sema.ppas.common.dataclass.CustomerSelectionData;
import com.slb.sema.ppas.common.dataclass.CustomerSelectionDataSet;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Customer Selection screen; and in particular,
 * the selection results form witin that screen.
 */
public class CcCustSelectionResultsScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** The data set of selected customers that met the selection criteria. */
    private CustomerSelectionDataSet i_selectedCustomers;

    /** Indicates to the JSP whether a single account has been selected, to 
     *  enable the Customer Selection Results JSP to determine that it can
     *  simply forward the request to the account details screen.
     */
    private boolean i_singleAccountSelected;

    /** Indicates to the JSP whether no accounts were selected. */
    private boolean i_noAccountsSelected;


    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Construct a Customer Selection Criteria Results Data object.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcCustSelectionResultsScreenData(
        GuiContext p_guiContext,
        GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);

        i_selectedCustomers = null;

        // Default to more than one account selected.
        i_singleAccountSelected = false;
        i_noAccountsSelected    = false;
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** Indicate that only a single account has been selected via the given
     *  selection criteria.
     */
    public void setSingleAccountSelected()
    {
        i_singleAccountSelected = true;
    }

    /** Return a boolean indicating whether only a single account has been
     *  selected.
     *  @return If true, then only a single account was identified that met
     *          the given account selection criteria. Otherwise, either no
     *          accounts or more than one account met the given selection 
     *          criteria.
     */
    public boolean singleAccountWasSelected()
    {
        return (i_singleAccountSelected);
    }

    /** Return a boolean indicating whether no accounts met the selection
     *  criteria.
     *  @return If true, then no accounts were identified that met
     *          the given account selection criteria. Otherwise, one or more
     *          accounts met the given selection criteria.
     */
    public boolean noAccountsWereSelected()
    {
        return (i_noAccountsSelected);
    }

    /** Initialise the data set of selected customers.
     *  @param p_selectedCustomers Data set defining customers selected.
     */
    public void setSelectedCustomers(
        CustomerSelectionDataSet p_selectedCustomers)
    {
        i_selectedCustomers = p_selectedCustomers;

        if (i_selectedCustomers != null)
        {
            if (i_selectedCustomers.getDataV().size() == 0)
            {
                i_noAccountsSelected = true;
            }
            else if (i_selectedCustomers.getDataV().size() == 1)
            {
                i_singleAccountSelected = true;
            }
        }
        return;
    }

    /** Get the data set of selected customers.
     *  @return CustomerSelectionDataSet. Data set containing the customer's
     *          selected.
     */
    public CustomerSelectionDataSet getSelectedCustomers()
    {
        return (i_selectedCustomers);
    }

    /** Returns true if the data set of customers selected is not empty.
     * @return True if the data set of customer's selected has been set up
     *         and contain at least one element.
     */
    public boolean isPopulated()
    {
        boolean l_populated = false;

        if ((i_selectedCustomers != null) &&
            (i_selectedCustomers.getDataV().size() > 0))
        {
            l_populated = true;
        }
        return (l_populated);
    }

    /** Returns the number of accounts held in the data set of selected 
     *  customers.
     *  @return The number of customers that have been selected.
     */
    public int getNumberSelected()
    {
        int l_number = 0;

        if ((i_selectedCustomers != null) &&
            (i_selectedCustomers.getDataV() != null))
        {
            l_number = i_selectedCustomers.getDataV().size();
        }
        return (l_number);
    }

    /** Get the element of selected customer data at the given index.
     *  @param p_index The index of the selected customer data that is to be returned.
     *  @return The Selected Customer data at the given index.
     */
    public CustomerSelectionData getSelectedCustData (int p_index)
    {
        CustomerSelectionData l_selectedCustData = null;

        if (isPopulated())
        {
            if ((p_index >= 0) && 
               (p_index < i_selectedCustomers.getDataV().size()))
            {
                l_selectedCustData = (CustomerSelectionData)
                           (i_selectedCustomers.getDataV().elementAt(p_index));
            }
        }
        return (l_selectedCustData);
    }

    /** Get string defining the account status of the account at the given
     *  index in the data set of selected accounts.
     *  @param p_index The index of the selected customer data whose status is to be
     *         returned.
     *  @return String defining the status of the account at the given index.
     */
    public String getSelectedCustStatus (int p_index)
    {
        String                l_custStatus = "";

        if (isPopulated())
        {
            if ((p_index >= 0) &&
                (p_index < i_selectedCustomers.getDataV().size()))
            {
                l_custStatus = BasicAccountData.convertCustStatusToString(
                                             getSelectedCustData(p_index).getCustStatus());
            }
        }

        return (l_custStatus);

    } // end method getSelectedCustStatus


    /** Get address of the account at the given index in the data set of
     *  selected accounts.
     *  @param p_index The index of the selected customer data whose address is to be
     *         returned.
     *  @return String defining the address of the account at the given index.
     */
    public String getSelectedCustAddress (int p_index)
    {
        CustomerSelectionData l_selectedCustData = null;
        String                l_custAddress = "";

        if (isPopulated())
        {
            if ((p_index >= 0) &&
                (p_index < i_selectedCustomers.getDataV().size()))
            {
                l_selectedCustData = (CustomerSelectionData)
                           (i_selectedCustomers.getDataV().elementAt(p_index));

                CustomerAddressData l_address = l_selectedCustData.getAddress();

                if (l_address != null)
                {
                    l_custAddress = l_address.getAddressLine1() + ", " +
                                    l_address.getAddressCity() + ", " +
                                    l_address.getAddressPostalCode();
                }
            }
        }
        return (l_custAddress);

    } // end method getSelectedCustAddress

} // end public class CcCustSelectionResultsScreenData
