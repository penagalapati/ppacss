////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcCallHistoryServlet.Java
//DATE            :       07-Mar-2006
//AUTHOR          :       Kanta Goswami
//REFERENCE       :       PpacLon#2026/8071
//                        PRD_ASCS00_GEN_CA_066_D1
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Servlet to handle requests from the
//                        Call History screen.
//
////////////////////////////////////////////////////////////////////////////////
//                          CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//27/11/06 | Chris      | Determine if a/c is master or   | PpacLon#2749/10512
//         | Harrison   | subordinate.                    |
//         |            | Distinguish between configured  |
//         |            | Max Rows upper limit and the    |
//         |            | value requested by the user.    |
//---------+------------+---------------------------------+--------------------
//29-nov-06| huy        | Added handling of Reset command to| PpacsLon#2747/10546
//         |            | clear any results and set params  |
//         |            | back to the default.              |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcCallHistoryScreenData;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcBasicAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcCallHistoryResponse;
import com.slb.sema.ppas.cc.service.CcCallHistoryService;
import com.slb.sema.ppas.common.businessconfig.dataclass.AcgrAccountGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.TeleTeleserviceDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
* Servlet for handling requests from the Disconnect screen.
*/
public class CcCallHistoryServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCallHistoryServlet";
    
    /** The constant representing the default sort field. Value is {@value}. */
    private static final String C_SORT_FIELD = "date";
    
    /** The constant representing the default other party number. Value is {@value}. */
    private static final String C_OTHER_PARTY_NO = "";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Call History Service to be used by this servlet. */
    private CcCallHistoryService i_ccCallHistService = null;
    
    /** Default number of records to be displayed in the Max Rows field. */ 
    private String i_defaultMaxRows = null;

    /** Maximum number of records that can be retrieved in one go */ 
    private String i_maxRetrievalLimit = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */

    public CcCallHistoryServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10090, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }
 
    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /**
     * Currently does nothing but will construct a CcCallHistoryService in the fullness of time.
     */
    public void doInit()
    {
        final String L_PROP_PREFIX = "com.slb.sema.ppas.cc.servlet.CcCallHistoryServlet.";
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 20000, this,
                           "Entered doInit().");
        }

        PpasProperties l_prop = i_guiContext.getProperties();
        i_defaultMaxRows = l_prop.getProperty(L_PROP_PREFIX + "defaultCallsToRetrieve", "100");

        i_maxRetrievalLimit = l_prop.getProperty(L_PROP_PREFIX + "maxCallsToRetrieve", "200");
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 20090, this,
                           "Leaving doInit().");
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /**
     * Service method to control requests and responses to the JSP script for the Call History screen service.
     * @param p_request Message request.
     * @param p_response Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(HttpServletRequest p_request,
                               HttpServletResponse p_response,
                               HttpSession p_httpSession,
                               GuiRequest p_guiRequest)
    {
        String l_command                             = null;
        String l_forwardUrl                          = null;
        CcCallHistoryScreenData l_callHistScreenData = null;
        boolean l_accessGranted                      = true;
        GuiResponse l_guiResponse                    = null;
        GuiSession l_guiSession                      = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 94300, this,
                           "ENTERED " + C_METHOD_doService);
        }
        
        //  Construct CcCallHistoryService if it does not already exist.
        if (i_ccCallHistService == null)
        {
            i_ccCallHistService = new CcCallHistoryService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        
        l_callHistScreenData = new CcCallHistoryScreenData(i_guiContext, p_guiRequest);
        
        try
        {
            // Get the command from the http request
            l_command = getValidParam (p_guiRequest,
                                       0,
                                       p_request,
                                       "p_command",
                                       true,    // Mandatory ...
                                       "",       // ... so no default
                                       new String []{
                                           "GET_SCREEN_DATA",
                                           "RESET_SCREEN_DATA",
                                           "RETRIEVE_CALL_DATA"});

            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_APPLICATION_DATA | WebDebug.C_ST_TRACE,
                               p_guiRequest, C_CLASS_NAME, 94400, this,
                               "Received command: " + l_command);
            }

            
            // Get business configuration data required for the screen
            getConfigData(p_guiRequest, l_callHistScreenData);

            if (l_command.equals("GET_SCREEN_DATA")    ||
                l_command.equals("RETRIEVE_CALL_DATA") ||
                l_command.equals("RESET_SCREEN_DATA")) 
            {
                l_accessGranted = true;
            }
            if (!l_accessGranted)
            {
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_callHistScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_callHistScreenData);
            }
           else
           {
               // Indicate whether the subscriber contained in the request is a
               // subordinate.
               l_guiSession = (GuiSession) p_guiRequest.getSession();

               if (!l_guiSession.getCustId().equals(l_guiSession.getBtCustId()))
               {
                   l_callHistScreenData.setIsSubordinate();
               }

               if (l_command.equals("GET_SCREEN_DATA") || l_command.equals("RESET_SCREEN_DATA"))
               {
                   doGetScreenData(p_guiRequest, l_callHistScreenData);
               }
               else if (l_command.equals("RETRIEVE_CALL_DATA"))
               {
                   doRetrieveCallData(p_request, p_guiRequest, l_callHistScreenData);
               }
            }
        }
        catch (PpasServletException l_pSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            l_callHistScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 94050, this,
                               "Caught exception:\n" + l_pSE);
            }
        }
        if (l_accessGranted)
        {
            if (l_callHistScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                if (l_command.equals("GET_SCREEN_DATA")    ||
                    l_command.equals("RETRIEVE_CALL_DATA") ||
                    l_command.equals("RESET_SCREEN_DATA"))
                {
                    l_forwardUrl = "/jsp/cc/cccallhistory.jsp";
                } 
                else if(l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_forwardUrl = "/jsp/cc/cccalldetails.jsp";
                }
            }
            p_request.setAttribute("p_guiScreenData", l_callHistScreenData);
        }
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 94590, this,
                           "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }
        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doGetScreenData = "doGetScreenData";

    /**
     * Get the Screen related data.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Call History screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void doGetScreenData(GuiRequest p_guiRequest, CcCallHistoryScreenData p_screenData)
            throws PpasServletException
    {
        CcAccountService              l_ccAccService               = null;
        PpasDate                      l_actDate                    = null;
        PpasDate                      l_configDate                 = null;
        CcBasicAccountDataResponse    l_ccBasicAccDataResponse     = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28000, this,
                           "Entered " + C_METHOD_doGetScreenData);
        }
        //Construct CcAccountService if it does not already exist ...
        if (l_ccAccService == null)
        {
            l_ccAccService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        l_ccBasicAccDataResponse = l_ccAccService.getBasicData(p_guiRequest, i_timeout);
        l_actDate = l_ccBasicAccDataResponse.getBasicAccountData().getActivationDate();
        
        l_configDate = DatePatch.getDateToday();
        l_configDate.add(PpasDate.C_FIELD_DATE,-30);
        
        if(l_actDate.after(l_configDate))
        {        
            p_screenData.setStartDate(l_actDate);
        }
        else 
        {
            p_screenData.setStartDate(l_configDate);
        }        
        p_screenData.setEndDate(DatePatch.getDateToday());        
        p_screenData.setDefaultMaxRows(Integer.valueOf(i_defaultMaxRows.trim()));
        p_screenData.setRecordRetrievalLimit(Integer.valueOf(i_maxRetrievalLimit.trim()));
        p_screenData.setRequestedRows(Integer.valueOf(i_defaultMaxRows.trim()));        
        p_screenData.setOtherPartyNo(C_OTHER_PARTY_NO);
        p_screenData.setSortField(C_SORT_FIELD);
        p_screenData.setDescendSort(true);
        p_screenData.setSubLevel(true);
    }  
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doRetrieveCallData = "doRetrieveCallData";
    /**
     * Get the Screen related data.
     * @param p_request The servlet request being processed.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Call History screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void doRetrieveCallData(HttpServletRequest p_request,
                                    GuiRequest p_guiRequest, 
                                    CcCallHistoryScreenData p_screenData)
            throws PpasServletException
    {
        CcCallHistoryResponse         l_ccCallHistResponse         = null;
        String                        l_startDateStr               = null;
        String                        l_endDateStr                 = null;
        String                        l_requestedRows              = null;
        String                        l_otherPartyNo               = null;
        String                        l_sortField                  = null;
        String                        l_descendSort                = null;
        String                        l_subLevel                   = null;
        CcAccountService              l_ccAccService               = null;
        CcBasicAccountDataResponse    l_ccBasicAccDataResponse     = null;
        PpasDate                      l_actDate                    = null;
        PpasDate                      l_configDate                 = null;
        PpasDate                      l_startDate                  = null;
        
        // Construct CcAccountService if it does not already exist ...
        if (l_ccAccService == null)
        {
            l_ccAccService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        l_ccBasicAccDataResponse = l_ccAccService.getBasicData(p_guiRequest, i_timeout);
        l_actDate = l_ccBasicAccDataResponse.getBasicAccountData().getActivationDate();
        
        l_configDate = DatePatch.getDateToday();
        l_configDate.add(PpasDate.C_FIELD_DATE,-30);
        
        // Calculate the current date minus a configurable time period. Ask about the
        // configurable time period.
        // Set the screen start date to the later date.  
        if(l_actDate.after(l_configDate))
        {        
            l_startDate = l_actDate;
        }
        else
        {
            l_startDate = l_configDate;
        }        
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28100, this,
                           "Entered " + C_METHOD_doRetrieveCallData);
        }
        
        l_startDateStr =
            getValidParam(p_guiRequest,
                          0,
                          p_request,
                          "p_startDate",
                          true,  // parameter is mandatory ...
                          l_startDate.toString(),     
                          PpasServlet.C_TYPE_DATE);
    
        l_endDateStr =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_endDate",
                              true,  // parameter is mandatory ...
                              DatePatch.getDateToday().toString(),     
                              PpasServlet.C_TYPE_DATE);
    
        l_requestedRows =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_maxRows",
                              true,  // parameter is mandatory ...
                              i_defaultMaxRows.trim(),   
                              PpasServlet.C_TYPE_NUMERIC);
        l_otherPartyNo = 
            getValidParam(p_guiRequest,
                          PpasServlet.C_FLAG_ALLOW_BLANK,
                          p_request,
                          "p_otherPartyNo",
                          false,//param is not mandatory
                          "",  //no default value
                          PpasServlet.C_TYPE_ANY);
        l_sortField =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_sortField",
                              true,  // parameter is mandatory ...
                              "date",     // default value
                              PpasServlet.C_TYPE_ALPHA_NUMERIC);
        
        l_descendSort = getValidParam(p_guiRequest,
                                        0,
                                        p_request,
                                        "p_descendSort",
                                        true,
                                        "true",
                                        PpasServlet.C_TYPE_BOOLEAN);
        l_subLevel = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_subLevel",
                                      true,
                                      "true",
                                      PpasServlet.C_TYPE_BOOLEAN);
      
        l_ccCallHistResponse = i_ccCallHistService.getCallHistoryDetails(p_guiRequest,
                                                                         i_timeout,
                                                                         new PpasDate(l_startDateStr),
                                                                         new PpasDate(l_endDateStr),
                                                                         Integer.valueOf(l_requestedRows),
                                                                         l_otherPartyNo,
                                                                         l_sortField,
                                                                         Boolean.valueOf(l_descendSort).booleanValue(),
                                                                         Boolean.valueOf(l_subLevel).booleanValue());
        p_screenData.addRetrievalResponse(p_guiRequest, l_ccCallHistResponse);

        if (!p_screenData.hasRetrievalError())
        {
           p_screenData.setCallHistDataSet(l_ccCallHistResponse.getCallHistoryData());
           p_screenData.setStartDate(new PpasDate(l_startDateStr));
           p_screenData.setEndDate(new PpasDate(l_endDateStr));
           p_screenData.setDefaultMaxRows(Integer.valueOf(i_defaultMaxRows.trim()));
           p_screenData.setRequestedRows(Integer.valueOf(l_requestedRows.trim()));
           p_screenData.setRecordRetrievalLimit(Integer.valueOf(i_maxRetrievalLimit.trim()));
           p_screenData.setOtherPartyNo(l_otherPartyNo);
           p_screenData.setSortField(l_sortField);
           p_screenData.setDescendSort(Boolean.valueOf(l_descendSort).booleanValue());
           p_screenData.setSubLevel(Boolean.valueOf(l_subLevel).booleanValue());        
        }
  
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 77531, this,
                           "LEAVING " + C_METHOD_doRetrieveCallData);
        }
    }   
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getConfigData = "getConfigData";

    /**
     * Get the Config details.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Call History screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void getConfigData(GuiRequest p_guiRequest, CcCallHistoryScreenData p_screenData)
            throws PpasServletException
    {
        TrafTrafficCaseDataSet         l_trafficCaseDS            = null;
        TeleTeleserviceDataSet         l_teleServiceDS            = null;
        FachFafChargingIndDataSet      l_fachDS                   = null;
        MiscCodeDataSet                l_servClassesDS            = null;
        AcgrAccountGroupDataSet        l_accGrpDS                 = null;
        CochCommunityChargingDataSet   l_commIdDS                 = null;
        DedaDedicatedAccountsDataSet   l_dedAccDS                 = null;
        
        Market                         l_csoMarket                = ((GuiSession)p_guiRequest.getSession()).getCsoMarket();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28200, this,
                           "Entered " + C_METHOD_getConfigData);
        }
        if(!p_screenData.hasRetrievalError())
        {
            l_trafficCaseDS = i_configService.getAllTrafficCase();
            p_screenData.setTrafficCaseDS(l_trafficCaseDS);
            
            l_teleServiceDS = i_configService.getAllTeleservice();
            p_screenData.setTeleServiceDS(l_teleServiceDS);
            
            l_fachDS = i_configService.getAllFach();
            p_screenData.setFachDS(l_fachDS);
            
            l_servClassesDS = i_configService.getAvailableServiceClasses(l_csoMarket);
            p_screenData.setServClassesDS(l_servClassesDS);
            
            l_accGrpDS = i_configService.getAllAccountGroup();
            p_screenData.setAccGrpDS(l_accGrpDS);
            
            l_commIdDS = i_configService.getAvailableCommunityIds();
            p_screenData.setCommIdDS(l_commIdDS);
            
            l_dedAccDS = i_configService.getDedicatedAccounts();
            p_screenData.setDedAccDS(l_dedAccDS);
        }  
    }  
} //end class CcCallHistoryServlet
