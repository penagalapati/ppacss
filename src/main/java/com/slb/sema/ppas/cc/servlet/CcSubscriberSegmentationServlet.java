////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubscriberSegmentationServlet.java
//      DATE            :       01-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from
//                              subscriber Segmentation.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcSubscriberSegmentationScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * JavaDoc
 */
public class CcSubscriberSegmentationServlet extends GuiServlet
{

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "CcSubscriberSegmentationServlet";
    
    /** Maximum number of subscriber segmentation services. */
    private static final int C_NUM_SERVICES = 31;

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The Account Service to be used by this servlet. */
    private CcAccountService    i_accountService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructor - creates a Subscriber Segmentation Servlet instance to handle
     * requests from the Subscriber Segmentation GUI Screen.
     */
    public CcSubscriberSegmentationServlet()
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /**
     * Service method to control requests and responses to the JSP screen for
     * subscriber segmentation.
     * @param p_request     Message request.
     * @param p_response    Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest  The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(HttpServletRequest  p_request,
                               HttpServletResponse p_response,
                               HttpSession         p_httpSession,
                               GuiRequest          p_guiRequest)
    {
        GopaGuiOperatorAccessData   l_gopaData = null;
        boolean                     l_accessGranted = false;
        GuiResponse                 l_guiResponse = null;
        String                      l_command     = null;
        String                      l_forwardUrl  = null;
        
        CcSubscriberSegmentationScreenData 
             l_ccSubscriberSegmentationScreenData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                "Entered " + C_METHOD_doService);
        }
        
        // Construct CcAccountService if it doesn't already exist
        if (i_accountService == null)
        {
            i_accountService = 
                         new CcAccountService (p_guiRequest,
                                               0,
                                               i_logger,
                                               i_guiContext);
        }
        
        l_ccSubscriberSegmentationScreenData = new CcSubscriberSegmentationScreenData(i_guiContext,
                                                                                      p_guiRequest);

        l_ccSubscriberSegmentationScreenData.setMaxNumberOfServiceOfferings(C_NUM_SERVICES);
        
        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,         // Mandatory
                                      "",           // Default Value
                                      new String [] {"GET_SCREEN_DATA",
                                                     "GET_FRAME_DATA",
                                                     "GET_POPUP",
                                                     "GET_POPUP_DATA",
                                                     "UPDATE"});
            
            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();
            
            if ((l_command.equals("UPDATE")) &&
                (l_gopaData.subscriberSegmentationUpdateIsPermitted()))
            {
                l_accessGranted = true;
            }
            else if (((l_command.equals("GET_SCREEN_DATA")) ||
                      (l_command.equals("GET_FRAME_DATA"))  ||
                      (l_command.equals("GET_POPUP"))  ||
                      (l_command.equals("GET_POPUP_DATA"))) &&
                     (l_gopaData.hasSubscriberSegmentationScreenAccess()))
            {
                l_accessGranted = true;
            }
            
            // Determine the forwardUrl
            if (l_command.equals("GET_FRAME_DATA"))
            {
                l_forwardUrl = "/jsp/cc/ccsubscribersegmentation.jsp";
            }
            else if (l_command.equals("GET_POPUP"))
            {
                l_forwardUrl = "/jsp/cc/ccsubscribersegmentationpopup.jsp";
            }
            else if (l_command.equals("GET_POPUP_DATA"))
            {
                l_forwardUrl = "/jsp/cc/ccsubscribersegmentation.jsp";
            }
            else
            {
                l_forwardUrl = "/jsp/cc/ccsubscribersegmentationmaintenance.jsp";
            }
           
            // Process request or return an insufficient privilege response.
            if (l_accessGranted)
            {
                // Perform an update.
                if (l_command.equals("UPDATE"))
                {
                    updateSegmentation(p_guiRequest,
                                       p_request,
                                       l_ccSubscriberSegmentationScreenData);
                }
                
                // Retrieve segmentation data.
                if ((l_command.equals("GET_POPUP")) || (l_command.equals("GET_POPUP_DATA")))
                {
                    getLocalData(p_guiRequest,
                                 p_request,
                                 l_ccSubscriberSegmentationScreenData);
                }
                else
                {
                    getSegmentation(p_guiRequest,
                                    l_ccSubscriberSegmentationScreenData);
                }
            }
            else
            {
                // Insufficient privilege to access or update screen.
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_ccSubscriberSegmentationScreenData.addRetrievalResponse(p_guiRequest,
                                                                          l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_ccSubscriberSegmentationScreenData);
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.

            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_ccSubscriberSegmentationScreenData.addRetrievalResponse (p_guiRequest, l_guiResponse);

            p_request.setAttribute("p_guiScreenData", l_ccSubscriberSegmentationScreenData);
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

        }
        
        if (l_accessGranted)
        {
            if (l_ccSubscriberSegmentationScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the appropriate screen data object.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData",
                                        l_ccSubscriberSegmentationScreenData);

            }
            else
            {
                // CcPaymentScreenData object has the necessary data
                // to populate the Payment screen.

                p_request.setAttribute("p_guiScreenData",
                                       l_ccSubscriberSegmentationScreenData);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10199,
                this,
                "Leaving " + C_METHOD_doService);
        }

        return (l_forwardUrl);
    }


    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getLocalData = "getLocalData";
    /**
     * Private method to retrieve the segmentation data from the database.
     * @param p_guiRequest The original request.
     * @param p_request    Message request.
     * @param p_screenData The response screen data.
     */
    private void getLocalData(GuiRequest p_guiRequest,
                              HttpServletRequest p_request,
                              CcSubscriberSegmentationScreenData p_screenData)
    {
        CcAccountDataResponse l_accountDataResponse = null;
        GuiResponse           l_guiResponse = null;
        String                l_guiResponseStatus = null;
        String                l_serviceOffering = null;
        String                l_accountGroup = null;
        int                   l_guiResponseSeverity;
        
        l_guiResponseStatus = GuiResponse.C_KEY_SERVICE_SUCCESS;
        l_guiResponseSeverity = GuiResponse.C_SEVERITY_SUCCESS;
        
        try
        {
            l_accountGroup =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_accountGroup",
                              false,
                              "",
                              PpasServlet.C_TYPE_ANY);
                              
            l_serviceOffering =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_serviceOffering",
                              false,
                              "",
                              PpasServlet.C_TYPE_ANY);

            // Parse request parameters and add values to screen data object.
            try
            {
                if ((l_accountGroup != null) && (l_accountGroup.length() > 0))
                {
                    p_screenData.setCurrentAccountGroupId(new AccountGroupId(l_accountGroup));
                }

                if ((l_serviceOffering != null) && (l_serviceOffering.length() > 0))
                {
                    p_screenData.setActiveServiceOffering(new ServiceOfferings(l_serviceOffering));
                }
                else
                {
                    p_screenData.setActiveServiceOffering(new ServiceOfferings(0));
                }
            }
            catch (PpasServiceFailedException l_ppasServFailE)
            {
                throw new PpasServletException(C_CLASS_NAME,
                                               C_METHOD_getLocalData,
                                               30110,
                                               this,
                                               p_guiRequest,
                                               0,
                                               ServletKey.get().invalidRequestData(),
                                               l_ppasServFailE);
            }
        }
        catch (PpasServletException l_ppasServE)
        {
            // Add the exception to a response
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasServE);
            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);
            l_guiResponseStatus = GuiResponse.C_KEY_SERVICE_FAILURE;
            l_guiResponseSeverity = GuiResponse.C_SEVERITY_FAILURE;
        }
        
        l_accountDataResponse = new CcAccountDataResponse(p_guiRequest,
                                                          p_guiRequest.getGuiSession().getSelectedLocale(),
                                                          l_guiResponseStatus,
                                                          new Object[] {},
                                                          l_guiResponseSeverity,
                                                          null);
        
        if (l_accountDataResponse.isSuccess())
        {
            p_screenData. addRetrievalResponse(p_guiRequest,
                                              l_accountDataResponse);
        
            // Store appropriate data on the screen data object.
            p_screenData.setAccountGroups(i_configService.getAllAccountGroup());
            
            p_screenData.setServiceOfferings(i_configService.getAvailableServiceOfferings());
            

        }

    }  // End method getLocalData(...)
    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSegmentation = "getDetails";
    /**
     * Private method to retrieve a subscribers segmentation options.
     * @param p_guiRequest The original request.
     * @param p_screenData The response screen data.
     */
    private void getSegmentation(GuiRequest                         p_guiRequest,
                                 CcSubscriberSegmentationScreenData p_screenData)
    {
        CcAccountDataResponse l_accountDataResponse = null;
        GuiSession            l_guiSession          = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10200,
                this,
                "Entered " + C_METHOD_getSegmentation);
        }
        
        l_accountDataResponse = i_accountService.getFullData(p_guiRequest,
                                                             i_timeout);
        
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_accountDataResponse);
        
        
        if (l_accountDataResponse.isSuccess())
        {
            // Store appropriate data on the screen data object.
            p_screenData.setActiveServiceOffering(l_accountDataResponse.getAccountData().
                                                  getServiceOffering());
            
            p_screenData.setCurrentAccountGroupId(l_accountDataResponse.getAccountData().getAccountGroupID());

            //p_screenData.setAccountData(l_accountDataResponse.getAccountData());
            
            p_screenData.setAccountGroups(i_configService.getAllAccountGroup());
            
            p_screenData.setServiceOfferings(i_configService.getAvailableServiceOfferings());
            
        }

        // Indicate whether the subscriber contained in the request is a
        // subordinate.
        l_guiSession = (GuiSession)p_guiRequest.getSession();

        if (!l_guiSession.getCustId().equals(l_guiSession.getBtCustId()))
        {
            p_screenData.setIsSubordinate();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10299,
                this,
                "Leaving " + C_METHOD_getSegmentation);
        }

    }
    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateSegmentation = "updateSegmentation";
    /**
     * Private method to update a subscribers segmentation options.
     * @param p_guiRequest The original request.
     * @param p_request    Message request.
     * @param p_screenData The response screen data.
     */
    private void updateSegmentation(GuiRequest                         p_guiRequest,
                                    HttpServletRequest                 p_request,
                                    CcSubscriberSegmentationScreenData p_screenData)
    {
        PpasDateTime l_currentTime = null;
        GuiResponse l_guiResponse = null;
        boolean l_paramChanged = false;
        String l_oldAccGroupId = null;
        String l_newAccGroupId = null;
        String l_oldServices = null;
        String l_newServices = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10300,
                this,
                "Entered " + C_METHOD_updateSegmentation);
        }
        
        // Extract parameters from the request.
        try
        {
            // Parse Old Account Group
            l_oldAccGroupId =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldAccGrpId",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);
                              
            // Parse New Account Group
            l_newAccGroupId =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newAccGrpId",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);
                              
            if(!l_oldAccGroupId.equals(l_newAccGroupId))
            {
                l_paramChanged = true;
            }
            
            l_oldServices =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldServOff",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_newServices =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newServOff",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);
            
            if (l_oldServices != l_newServices)
            {
                l_paramChanged = true;
            }
            
        }
        catch(PpasServletException l_ppasServE)
        {
            // Add the exception to a response
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasServE );
            p_screenData.addRetrievalResponse (p_guiRequest, l_guiResponse);
        }
        
        // Perform an update if neccessary.
        if(l_paramChanged)
        {
            if (p_screenData.hasUpdateError())
            {
                l_guiResponse =
                             new GuiResponse(p_guiRequest,
                                             GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                             new Object[] {"subscriber segmentation change"},
                                             GuiResponse.C_SEVERITY_WARNING);
            }
            else
            {
                l_guiResponse = i_accountService.updateSegmentation(p_guiRequest,
                                                                    i_timeout,
                                                                    l_oldAccGroupId,
                                                                    l_newAccGroupId,
                                                                    l_oldServices,
                                                                    l_newServices);
                
                // Return response to screen.
                p_screenData.addUpdateResponse(p_guiRequest,
                                               l_guiResponse);
            
                // Get the current time to use in display message.
                l_currentTime = DatePatch.getDateTimeNow();


                // Report on the success of the last update.
                if (p_screenData.hasUpdateError())
                {
                    p_screenData.setInfoText("Failed Update at " + l_currentTime);
                }
                else
                {
                    p_screenData.setInfoText("Update succeeded at " + l_currentTime);
                }
            }
            
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10399,
                this,
                "Leaving " + C_METHOD_updateSegmentation);
        }
        
    } // end method updateDetails(...)
    

} // end public class