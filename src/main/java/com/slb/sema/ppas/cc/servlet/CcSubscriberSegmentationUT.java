////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcSubscriberSegmentationServletUT.java
//      DATE            :       07-Aug-2006
//      AUTHOR          :       Michael Erskine (40771f)
//      REFERENCE       :       PpacLon#
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for GUI Subscriber Segmentation. 
//                              I.e. Account Group, Service Offerings
//    
//                              Commands:
//                                "GET_SCREEN_DATA",
//                                "GET_FRAME_DATA",
//                                "GET_POPUP",
//                                "GET_POPUP_DATA",
//                                "UPDATE"
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//14-Sep-06 |Andy Harris |Changed to use new methods in    |PpacLon#2586/9791
//          |            |super class.                     |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebRequest;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcSubscriberSegmentationUT extends GuiTestTT
{
    /** Table ID for service offerings. */
    private static final String C_TABLE_ID_SERVICE_OFFERINGS = "serviceOfferings";
    
    /** Table ID for event history. */
    private static final String C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY = "eventHistory";
    
    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcSubscriberSegmentationUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     */
    public void setUp()
    {
        super.setUp(true);
    }

    /**
     * @ut.when An operator navigates to the Msisdn Routing screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadInstallScreenSubsSegmentationPopup()
    {        
        beginOfTest("Start testLoadInstallScreenSubsSegmentationPopup");
        endOfTest();
    }

    /**
     * @ut.when An operator loads the Subscriber Segmentation screen.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testLoadSubsSegmentationScreen()
    {        
        beginOfTest("Start testLoadSubsSegmentationScreen");
        
        // Install MSISDN and load Maintain Subsriber Screen
        installAndLoadSubScreen(1);

        sendRequest(constructGetRequest("cc/SubscriberSegmentation", "GET_SCREEN_DATA"));

        // Validate that correct page is returned and that the content is correct
        validateResponse(null, null, "ASCS - Subscriber Segmentation");
        
        sendRequest(constructGetRequest("cc/SubscriberSegmentation", "GET_FRAME_DATA"));
        
        // Verify each of the check boxes in turn in 2 stages:
        // 1. Verify the account group id.
        // 2. Verify that all the old service offerings are unchecked
        
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            
            for (int i = 0; i < l_forms.length; i++)
            {
                System.out.println("MIE l_forms[" + i + "]: " + l_forms[i]);
            }
            
            String[] l_paramNames = l_forms[0].getParameterNames();
            
            for (int i=0; i < l_paramNames.length; i++)
            {
                System.out.println("MIE l_paramNames[" + i + "]: " + l_paramNames[i]);
                System.out.println("    paramValue: " + l_forms[0].getParameterValue(l_paramNames[i]));
            }

            //System.out.println("MIE load subs segmentation screen: \n" + c_webResponse.getText());
            
            // Validate the field.
            assertEquals("0", l_forms[0].getParameterValue("oldAccGroupId"));
            
            // TODO: Validate service offerings.
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }
    
    
    /**
     * @ut.when An operator updates the account group.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateAccountGroupSuccess()
    {        
        beginOfTest("Start testUpdateAccountGroupSuccess");
        
        HashMap l_params = new HashMap();
        
        // Install MSISDN and load Maintain Subsriber Screen
        BasicAccountData l_bad = installAndLoadSubScreen(1);
        String l_strMsisdn = c_ppasContext.getMsisdnFormatInput().format(l_bad.getMsisdn());
        
        l_params.put("p_oldAccGrpId", "0");
        l_params.put("p_newAccGrpId", "3");
        l_params.put("p_oldServOff", "0");
        l_params.put("p_newServOff", "0");
           
        sendRequest(constructPostRequest("cc/SubscriberSegmentation", "UPDATE", l_params));

        // Validate that correct page is returned and that the content is correct
        validateResponse("<00002>  The update subscriber segmentation service completed successfully.", null, "ASCS - Subscriber Segmentation");
        
        //TODO: Verify in 2 stages:
        // 1. Verify the account group id.
        // 2. Verify that all the old service offerings are unchecked
        
        sendRequest(constructGetRequest("cc/SubscriberSegmentation", "GET_FRAME_DATA"));
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            
            for (int i = 0; i < l_forms.length; i++)
            {
                System.out.println("MIE l_forms[" + i + "]: " + l_forms[i]);
            }
            
            String[] l_paramNames = l_forms[0].getParameterNames();
            
            for (int i=0; i < l_paramNames.length; i++)
            {
                System.out.println("MIE l_paramNames[" + i + "]: " + l_paramNames[i]);
                System.out.println("    paramValue: " + l_forms[0].getParameterValue(l_paramNames[i]));
            }

            //System.out.println("MIE load subs segmentation screen: \n" + c_webResponse.getText());
            
            // Validate the field.
            assertEquals("3", l_forms[0].getParameterValue("oldAccGroupId"));
            
            // Validate event history
            loadMaintainSubsScreenForMsisdn(l_strMsisdn);
            
            WebRequest l_request = null;
            
            l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
            sendRequest(l_request);
            validateResponse(null, null, "Account Details");
            verifyRowInTable(
                C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY,
                1,
                new String[] {"", "DateTimeStamp", "Account Group changed from 0 to 3", "SUPER"}
                );
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }
    
    
    /**
     * @ut.when An operator updates the selected service offerings.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateServiceOfferingsSuccess()
    {        
        beginOfTest("Start testUpdateServiceOfferingsSuccess");
        
        HashMap l_params = new HashMap();
        
        // Install MSISDN and load Maintain Subsriber Screen
        BasicAccountData l_bad = installAndLoadSubScreen(1);
        String l_strMsisdn = c_ppasContext.getMsisdnFormatInput().format(l_bad.getMsisdn());
        
        l_params.put("p_oldAccGrpId", "0");
        l_params.put("p_newAccGrpId", "0");
        l_params.put("p_oldServOff", "0");
        l_params.put("p_newServOff", "1");
           
        sendRequest(constructPostRequest("cc/SubscriberSegmentation", "UPDATE", l_params));

        // Validate that correct page is returned and that the content is correct
        validateResponse("<00002>  The update subscriber segmentation service completed successfully.",
                         null,
                         "ASCS - Subscriber Segmentation");
        
        // Verify each of the check boxes in turn in 2 stages:
        // 1. Verify the account group id.
        // 2. Verify that all the old service offerings are unchecked
        
        sendRequest(constructGetRequest("cc/SubscriberSegmentation", "GET_FRAME_DATA"));
        try
        {
            WebForm[] l_forms = c_webResponse.getForms();
            
            for (int i = 0; i < l_forms.length; i++)
            {
                System.out.println("MIE l_forms[" + i + "]: " + l_forms[i]);
            }
            
            String[] l_paramNames = l_forms[0].getParameterNames();
            
            for (int i=0; i < l_paramNames.length; i++)
            {
                System.out.println("MIE l_paramNames[" + i + "]: " + l_paramNames[i]);
                System.out.println("    paramValue: " + l_forms[0].getParameterValue(l_paramNames[i]));
            }

            //System.out.println("MIE load subs segmentation screen: \n" + c_webResponse.getText());
            
            // Validate the field.
            assertEquals("0", l_forms[0].getParameterValue("oldAccGroupId"));
            
            //TODO: Figure out how to validate checkbox values using HttpUnit
            try
            {
                System.out.println("MIE Response after service offerings update: \n" + c_webResponse.getText());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            
            // Verify changed service offerings
            verifyRowInTable(C_TABLE_ID_SERVICE_OFFERINGS, 0,
                             new String[] {"Services", "Current", "New", "Services", "Current", "New"});
            
            
            verifyRowInTable(C_TABLE_ID_SERVICE_OFFERINGS, 1,
                             new String[] {"Test Service Offerings",
                                           "",    //Checkbox
                                           "",    //Checkbox
                                           "Free voice-mail calls",
                                           "",    //Checkbox
                                           ""});  //Checkbox
            
            verifyRowInTable(C_TABLE_ID_SERVICE_OFFERINGS, 2,
                             new String[] {"International call barring",
                                           "",    //Checkbox
                                           "",    //Checkbox
                                           "Non-chargable refill",
                                           "",    //Checkbox
                                           ""});  //Checkbox

            // Validate event history
            loadMaintainSubsScreenForMsisdn(l_strMsisdn);
            
            WebRequest l_request = null;
            
            l_request = constructGetRequest("cc/AccountDetails", "GET_SCREEN_DATA");
            sendRequest(l_request);
            validateResponse(null, null, "Account Details");
            verifyRowInTable(
                C_TABLE_ID_ACCOUNTDETAILS_EVENT_HISTORY,
                1,
                new String[] {"", "DateTimeStamp", "Service offerings parameter changed", "SUPER"}
                );
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }
    
    
        /**
     * @ut.when An operator updates the selected service offerings.
     * @ut.then A successful response is returned containing the correct page.
     */
    public void testUpdateServiceOfferingsFailure()
    {        
        beginOfTest("Start testUpdateServiceOfferingsFailure");
        
        HashMap l_params = new HashMap();
        
        // Install MSISDN and load Maintain Subsriber Screen
        installAndLoadSubScreen(1);
        
        l_params.put("p_oldAccGrpId", "0");
        l_params.put("p_newAccGrpId", "0");
        l_params.put("p_oldServOff", "0");
        l_params.put("p_newServOff", "2147483647");
           
        sendRequest(constructPostRequest("cc/SubscriberSegmentation", "UPDATE", l_params));

        // Validate that correct page is returned and that the content is correct
        validateResponse("A subscriber may not belong to more than one accumulated refill bonus scheme",
                         null,
                         "ASCS - Subscriber Segmentation");
        endOfTest();
    }

 
    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        System.out.println(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcSubscriberSegmentationUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * 
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------
    
}