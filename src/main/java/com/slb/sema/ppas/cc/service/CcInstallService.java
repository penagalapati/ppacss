////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcInstallService.java
//      DATE            :       19-Feb-2002
//      AUTHOR          :       Mike Hickman
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasInstallService methods.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 21/08/02 | Sally Wells| Add p_custStatus to interface   | PpaLon#1502/6272
//          |            |                                 |PRD_PPAK00_GEN_CA374
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
//14-Oct-03 | M Erskine  | Changes to allow compilation due| PpacLon#57/550
//          |            | to changes to IS API            |
//----------+------------+---------------------------------+--------------------
//16-Oct-03 | M Erskine  | Update installSubscriber method | PpacLon#57/630
//          |            | to reflect changes in design    |
//----------+------------+---------------------------------+--------------------
// 29/09/05 | L Byrne    | Allow installation with         | PpacLon#824/7115
//          |            | no promotion.                   |
//----------+------------+---------------------------------+--------------------
// 04-Dec-05| M Erskine  | Licensed feature to enable      | PpacLon#1882/7556
//          |            | operator to pass in a new flag  |PRD_ASCS00_GEN_CA_65
//          |            | specifying that install request |
//          |            | can use a quarantined MSISDN    |
//----------+------------+---------------------------------+--------------------
// 24-Apr-07| S James    | Add Single Step Install changes | PpacLon#3072/11370
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasInstallService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Gui Service that wrappers calls to the Account Install service.
 */
public class CcInstallService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcInstallService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Atomic service used by this Service. */
    private PpasInstallService i_installService;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates an instance of the CcInstallService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcInstallService(GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_installService = new PpasInstallService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_install = "install";
    /** Calls the internal service to install the subscriber on the IN.
     *
     * Optional parameters should be supplied as blank strings - the
     * installation will be done with configured defaults.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis The Timeout in Milliseconds to wait for resources
     *                        required to perform the service.
     * @param p_market  (Optional) The market.
     * @param p_class   (Optional) The subscriber class.
     * @param p_masterMsisdn  The master account. If same as MSISDN, then
     *                        account is installed as a master.
     * @param p_promoPlan  (Optional) The promotion plan to apply.
     * @param p_agent   (Optional) The subscriber's Agent.
     * @param p_accountGroupId  (Optional) The account group ID.
     * @param p_serviceOfferings (Optional) The service offering.
     * @param p_overrideMsisdnQuarantinePeriod (Optional) Flag indicating whether or not to override the
     *                                                    MSISDN quarantine period.
     * @param p_eocnId (Optional) The USSD End of Call Notification selection structure ID.
     * @param p_singleStepInstall (Optional) Flag indicating whether to perform a single step install.
     * @return A GUI response.
     */
    public GuiResponse install(GuiRequest              p_guiRequest,
                               long                    p_timeoutMillis,
                               String                  p_market,
                               String                  p_class,
                               Msisdn                  p_masterMsisdn,
                               String                  p_promoPlan,
                               String                  p_agent,
                               AccountGroupId          p_accountGroupId,
                               ServiceOfferings        p_serviceOfferings,
                               boolean                 p_overrideMsisdnQuarantinePeriod,
                               EndOfCallNotificationId p_eocnId,
                               boolean                 p_singleStepInstall)
    {
        GuiResponse l_guiResponse  = null;
        ServiceClass l_serviceClass = null;

        // Temporary measure - inId no longer supplied by GUI screen but still expected by IS layer
        String l_inId = "";
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 20000, this,
                           "Entered " + C_METHOD_install +
                           " with timeout = " +p_timeoutMillis +
                           " and market = " +p_market +
                           " and class = " +p_class +
                           " and MSISDN = " +p_masterMsisdn +
                           " and Promo plan = " +p_promoPlan +
                           " and agent = " +p_agent +
                           " and Account Group Id = " +p_accountGroupId +
                           " and Service Offerings = " +p_serviceOfferings +
                           " and Override Quarantine? = " +p_overrideMsisdnQuarantinePeriod +
                           " and eocn Id = " +p_eocnId +
                           " and single step install? = " +p_singleStepInstall);
        }

        if (p_class != null && p_class.trim().length() > 0)
        {
            l_serviceClass = new ServiceClass (Integer.parseInt(p_class)); 
        }
        try
        {
            i_installService.installSubscriber(p_guiRequest,
                                               p_timeoutMillis,
                                               new Market(Market.getSrvaFromMarketStr(p_market),
                                                          Market.getSlocFromMarketStr(p_market)),
                                               p_masterMsisdn,
                                               p_agent,
                                               l_inId,
                                               l_serviceClass,
                                               p_promoPlan,
                                               p_accountGroupId, 
                                               p_serviceOfferings,
                                               p_overrideMsisdnQuarantinePeriod,
                                               p_eocnId,
                                               PpasAccountService.C_CLEAR_TEMPORARY_BLOCK,
                                               !p_promoPlan.equals(""),
                                               p_singleStepInstall ? 
                                                   PpasInstallService.C_INSTALL_TYPE_SINGLE_STEP :
                                                   PpasInstallService.C_INSTALL_TYPE_STANDARD);

            l_guiResponse
                = new GuiResponse(p_guiRequest,
                                  p_guiRequest.getGuiSession().getSelectedLocale(),
                                  GuiResponse.C_KEY_INSTALL_SUCCESS,
                                  new Object[] {p_guiRequest.getMsisdn()},
                                  GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 20010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "install the subscriber", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 20020, this,
                           "Leaving " + C_METHOD_install);
        }

        return l_guiResponse;
    }
}
