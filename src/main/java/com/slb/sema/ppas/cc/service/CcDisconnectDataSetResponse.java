////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDisconnectDataSetResponse.java
//      DATE            :       09-Apr-2002
//      AUTHOR          :       Remi Isaacs
//      REFERENCE       :       PpaLon#****/*****
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for DisconnectDataSet objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for DisconnectDataSet objects. 
 */
public class CcDisconnectDataSetResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDisconnectDataSetResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The DisconnectDataSet object wrapped by this object. */
    private  DireDisconnectReasonDataSet i_disconnectDataSet;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives a set of Disconnect Reason data and the additional necessary objects to 
     * create a response message indicating the status of a call to a
     * DisconnectService method.
     *
     * @param p_guiRequest GuiRequest
     * @param p_messageLocale message locale used to construct the message
     * @param p_messageKey        Response message key.
     * @param p_messageParams     Any parameters to be substituted into response
     *                            message.
     * @param p_messageSeverity   Severity of response.
     * @param p_disconnectDataSet The disconnect information.
     */
    public CcDisconnectDataSetResponse( GuiRequest                   p_guiRequest,
                                        Locale                       p_messageLocale,
                                        String                       p_messageKey,
                                        Object[]                     p_messageParams,
                                        int                          p_messageSeverity,
                                        DireDisconnectReasonDataSet  p_disconnectDataSet )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing CcDisconnectDataSetResponse");
        }

        i_disconnectDataSet = p_disconnectDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed CcDisconnectDataSetResponse");
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------
    /** Returns the Disconnect information.
     * @return Set of disconnect data.
     */
    public DireDisconnectReasonDataSet getDisconnectDataSet()
    {
        return( i_disconnectDataSet );
    }

} // end of class CcDisconnectDataSetResponse
