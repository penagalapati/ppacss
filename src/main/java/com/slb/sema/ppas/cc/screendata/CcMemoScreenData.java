////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcMemoScreenData.java
//      DATE            :       05-Apr-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1343/6031
//                              PRD_PPAK00_GEN_IN_3
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the 
//                              Memo screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.MemoData;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Memo screen.
 */
public class CcMemoScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** MemoData object wrapped by this object. */
    private MemoData     i_memoData     = null;

    /** Contains the OPID of the USER who created/is creating this Memo. */
    private String       i_raisedBy     = null;
 
    /** Contains the Date/Time the Memo being viewed/created was raised. */
    private PpasDateTime i_raisedOn     = null;

    /** Indicates whether the number of Open Memos on the account
     * has incremented, decremented, or hasn't changed after the
     * last request performed.
     */
    private int i_openMemoCountStatus = C_OPEN_MEMO_COUNT_HAS_NOT_CHANGED;

    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Constant that indicates that the count of Open Memos on the
     * account has not changed since the last action.
     */
    public static final int C_OPEN_MEMO_COUNT_HAS_NOT_CHANGED = 0;

    /** Constant that indicates that the count of Open Memos on the
     * account has incremented since the last action.
     */
    public static final int C_OPEN_MEMO_COUNT_HAS_INCREMENTED = 1;

    /** Constant that indicates that the count of Open Memos on the
     * account has decremented since the last action.
     */
    public static final int C_OPEN_MEMO_COUNT_HAS_DECREMENTED = -1;


    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * @param p_guiContext Session settings.
     * @param p_guiRequest The GUI request being processed.
     */
    public CcMemoScreenData(GuiContext p_guiContext,
                            GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
        setAsPopup();
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the MemoData object wrapped within this object.
     * Returns null if there isn't one.
     * @return Memo data.
     */
    public MemoData getMemoData()
    {
        return i_memoData;
    }


    /** Sets the MemoData object wrapped within this object.
     * @param p_memoData Memo data.
     */
    public void setMemoData(MemoData p_memoData)
    {
        i_memoData = p_memoData;
    }


    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if this object is populated.
     */
    public boolean isPopulated()
    {
        return (i_memoData != null);
    }


    /**
     * Sets the Raised by value to be displayed in the Memo window.
     * Can be used when entering the window for the first time to add
     * a new Memo that has not been created yet.
     *
     * @param p_opid The OPID of the USER raising the Memo.
     */
    public void setRaisedBy(String p_opid)
    {
        i_raisedBy = p_opid;
    }

    /**
     * Sets the Raised On value to be displayed in the Memo window.
     * Can be used when entering the window for the first time to add
     * a new Memo that has not been created yet.
     *
     * @param p_raisedOn The DateTime the Memo is being created.
     */
    public void setRaisedOn(PpasDateTime p_raisedOn)
    {
        i_raisedOn = p_raisedOn;
    }


    /**
     * Returns the OPID of the USER who is creating / has created the Memo being viewed.
     * @return Identifier of the operator that created this memo.
     */
    public String getRaisedBy()
    {
        return( (i_memoData == null) ? i_raisedBy : i_memoData.getRaisedBy() );
    }

    /**
     * Returns the DateTime the Memo is being/was created.
     * @return Date/time this memo was raised.
     */
    public PpasDateTime getRaisedOn()
    {
        return( (i_memoData == null) ? i_raisedOn : i_memoData.getRaisedOn() );
    }

    /**
     * Sets the Status of the Open Memo Count since the last Memo Service
     * request as having incremented.
     */
    public void setOpenMemoCountIncremented()
    {
        i_openMemoCountStatus = C_OPEN_MEMO_COUNT_HAS_INCREMENTED;
    }

    /**
     * Sets the Status of the Open Memo Count since the last Memo Service
     * request as having decremented.
     */
    public void setOpenMemoCountDecremented()
    {
        i_openMemoCountStatus = C_OPEN_MEMO_COUNT_HAS_DECREMENTED;
    }

    /**
     * Returns the Open Memo Count Status since the last Memo
     * Service Request.
     * @return Memo count status.
     */
    public int getOpenMemoCountStatus()
    {
        return i_openMemoCountStatus;
    }

} // end of CcMemoScreenData class
