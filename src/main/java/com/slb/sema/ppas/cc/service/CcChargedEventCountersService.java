////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcChargedEventCountersService.java
//      DATE            :       08-Jun-2005
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PpaLon#1560/6587
//                              PRD_ASCS_GEN_CA_49
//
//      COPYRIGHT       :       WM-Data 2005
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasAccumulatorService.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccumulatorEnquiryResultData;
import com.slb.sema.ppas.common.dataclass.ChargedEventCounterTypeId;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAccumulatorService;
import com.slb.sema.ppas.util.logging.Logger;


/** Gui Service that wrappers calls to PpasCustomerDetailsService. */
public class CcChargedEventCountersService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcChargedEventCountersService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasAccumulatorService i_accumulatorService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcCustomerDetailsService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcChargedEventCountersService(GuiRequest p_guiRequest,
                                    long       p_flags,
                                    Logger     p_logger,
                                    GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_accumulatorService = new PpasAccumulatorService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10099, this,
                "Constructed " + C_CLASS_NAME);
        }
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getChargedEventCounters = "getChargedEventCounters";
    /**
     * Requests the current charged event counter data from internal serves.
     * @param p_guiRequest The original GUI request.
     * @param p_timeoutMillis Database connection timeout.
     * @return A <code>CcChargedEventCountersDataResponse</code> object wrapping the response from the SDP.
     */
    public CcChargedEventCountersDataResponse getChargedEventCounters(GuiRequest p_guiRequest,
                                               long       p_timeoutMillis)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 20100, this,
                "Entered " + C_METHOD_getChargedEventCounters);
        }

        GuiResponse l_guiResponse = null;
        AccumulatorEnquiryResultData l_accumEnqResultData = null;
        CcChargedEventCountersDataResponse l_chagedEventCountersDataResponse;
        
        try
        {
            l_accumEnqResultData = i_accumulatorService.getChargedEventCounters(p_guiRequest,
                                                         p_timeoutMillis);
            
            l_chagedEventCountersDataResponse
                = new CcChargedEventCountersDataResponse(
                              p_guiRequest,
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {C_METHOD_getChargedEventCounters},
                              GuiResponse.C_SEVERITY_SUCCESS,
                              l_accumEnqResultData);

        }
        catch (PpasServiceException p_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_HIGH,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 20010, this,
                    "Caught PpasServiceException in " + 
                    C_METHOD_getChargedEventCounters + ": " + 
                    p_ppasSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, C_METHOD_getChargedEventCounters, p_ppasSE);
            
            l_chagedEventCountersDataResponse
                = new CcChargedEventCountersDataResponse(
                              p_guiRequest,
                              l_guiResponse.getKey(),
                              l_guiResponse.getParams(),
                              GuiResponse.C_SEVERITY_FAILURE,
                              null);

        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 20199, this,
                "Leaving " + C_METHOD_getChargedEventCounters);
        }

        return l_chagedEventCountersDataResponse;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateChargedEventCounter = "updateChargedEventCounter";
    /**
     * Send an update request to internal services.
     * @param p_guiRequest The original GUI request.
     * @param p_timeoutMillis Database connection timeout.
     * @param p_counterTypeId The id of the event counter being updated.
     * @param p_totCounterAdjVal The total counter adjustment value to be sent to the SDP.
     * @param p_periodCounterAdjVal The period counter adjustment value to be sent to the SDP.
     * @param p_action The ADD or SUBTRACT action detailing how the SDP should be updated.
     * @param p_periodCounterClearDate The clear date of the event counter being updated.
     * @return A <code>GuiResponse</code> used to populate the GUI screen.
     */
    public GuiResponse updateChargedEventCounter(GuiRequest                p_guiRequest,
                                                 long                      p_timeoutMillis,
                                                 ChargedEventCounterTypeId p_counterTypeId,
                                                 int                       p_totCounterAdjVal,
                                                 int                       p_periodCounterAdjVal,
                                                 String                    p_action,
                                                 PpasDate                  p_periodCounterClearDate)
    {
        GuiResponse l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 20000, this,
                "Entered " + C_METHOD_updateChargedEventCounter);
        }
        
        try
        {
            i_accumulatorService.updateChargedEventCounter(p_guiRequest,
                                                           p_timeoutMillis,
                                                           p_counterTypeId,
                                                           p_totCounterAdjVal,
                                                           p_periodCounterAdjVal,
                                                           p_action,
                                                           p_periodCounterClearDate);

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {C_METHOD_updateChargedEventCounter},
                                            GuiResponse.C_SEVERITY_SUCCESS);

        }
        catch (PpasServiceException p_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_HIGH,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 20010, this,
                    "Caught PpasServiceException in " + 
                    C_METHOD_updateChargedEventCounter + ": " + 
                    p_ppasSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateChargedEventCounter, p_ppasSE);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 20099, this,
                "Leaving " + C_METHOD_updateChargedEventCounter);
        }

        return l_guiResponse;
    }

}
