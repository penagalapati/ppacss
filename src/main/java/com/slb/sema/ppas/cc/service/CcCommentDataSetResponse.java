////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCommentDataSetResponse.java
//      DATE            :       31-Jan-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Response returned by a GUI service method that
//                              is a container for CommentDataSet.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.CommentDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response returned by a GUI service method that contains a
 * CommentDataSet object.
 */
public class CcCommentDataSetResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    // Class Name.
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCommentDataSetResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccountData instance contained in this response. */
    private CommentDataSet i_commentDataSet;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /**
     * No Locale required for this constructor - always uses default Locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_commentDataSet Set of comments data.
     */
    public CcCommentDataSetResponse(GuiRequest     p_guiRequest,
                                    String         p_messageKey,
                                    Object[]       p_messageParams,
                                    int            p_messageSeverity,
                                    CommentDataSet p_commentDataSet)
    {
        this(p_guiRequest,
             null,
             p_messageKey,
             p_messageParams,
             p_messageSeverity,
             p_commentDataSet);
    }

    /**
     * Uses the Locale passed in to construct text of message.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_commentDataSet Set of comments data.
     */
    public CcCommentDataSetResponse(GuiRequest     p_guiRequest,
                                    Locale         p_messageLocale,
                                    String         p_messageKey,
                                    Object[]       p_messageParams,
                                    int            p_messageSeverity,
                                    CommentDataSet p_commentDataSet)
    {
        super(p_guiRequest,
              p_messageLocale,
              p_messageKey,
              p_messageParams,
              p_messageSeverity);


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_commentDataSet = p_commentDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 10001, this,
                "Constructed " + C_CLASS_NAME);
        }

    }


    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the CommentDataSet instance contained within
     * this response object.
     * @return Set of comment data.
     */
    public CommentDataSet getCommentDataSet()
    {
        return i_commentDataSet;
    } // end public CommentDataSet getCommentDataSet()
}
