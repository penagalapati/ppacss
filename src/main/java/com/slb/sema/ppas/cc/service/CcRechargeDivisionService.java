////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcRechargeDivisionService.java
//      DATE            :       1-Sep-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1513/6383
//                              PRD_PPAK00_GEN_CA_372
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to getDivision
//                              method of the PpasRechargeDivisionService class.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasRechargeDivisionService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * Recharge Divisions. The following service is provided:
 *
 * - Obtain the recharge Division for the selected customer
 */
public class CcRechargeDivisionService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcRechargeDivisionService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The atomic recharge history service. */
    private PpasRechargeDivisionService i_rechargeDivisionService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a CcRechargeDivisionService object that
     * can then be used to perform Recharge Division services.
     * @param p_request The request being processed.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcRechargeDivisionService(
                             GuiRequest         p_request,
                             Logger             p_logger,
                             GuiContext         p_guiContext )
    {
        super( p_request, 0, p_logger );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_rechargeDivisionService = new PpasRechargeDivisionService(p_request, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDivision = "getDivision";
    /**
     * Generates a CcRechargeDivisionDataResponse object for the
     * selected customer which defines how the recharge was divided.
     *
     * @param p_request The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until this request times out.
     * @param p_linkId Unique identifier linking a payment or recharge record
     *                 with the record of its division across dedicated accounts.
     * @param p_type   Type of dedicated account division.
     *
     * @return Data object defining the division of the recharge.
     */
    public CcRechargeDivisionDataResponse getDivision(GuiRequest   p_request,
                                                      long         p_timeoutMillis,
                                                      long         p_linkId,
                                                      char         p_type)
    {
        GuiResponse                     l_guiResponse               = null;
        CcRechargeDivisionDataResponse  l_rechargeDivisionResponse  = null;
        DivisionData            l_division                  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_request, C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_getDivision );
        }

        try
        {
            // Call the atomic service...
            l_division = i_rechargeDivisionService.getDivision(
                                                       p_request,
                                                       p_timeoutMillis,
                                                       p_linkId,
                                                       p_type);

            // Generate success CcRechargeDivisionDataResponse
            l_rechargeDivisionResponse = new CcRechargeDivisionDataResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_division );
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_request, C_CLASS_NAME, 10210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException( p_request, 0, "get division", l_pSE );

            l_rechargeDivisionResponse = new CcRechargeDivisionDataResponse(
                                    p_request,
                                    p_request.getGuiSession().
                                      getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_request, C_CLASS_NAME, 13530, this,
                "Leaving " + C_METHOD_getDivision);
        }

        return ( l_rechargeDivisionResponse );

    } // end of getDivision() method.

} // end of CcRechargeDivisionService Class

