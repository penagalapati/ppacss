////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentApprovalScreenData.java
//      DATE            :       16-Oct-2003
//      AUTHOR          :       Steven James
//      REFERENCE       :       PpaLon#2071/8521
//                              PRD_PPAK00_GEN_CA_464
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Screen Data object for the 
//                              Adjustments Approval screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.businessconfig.dataclass.AsceFailureReasonDataSet;
import com.slb.sema.ppas.common.dataclass.HoldAdjustmentData;
import com.slb.sema.ppas.common.dataclass.HoldAdjustmentDataSet;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Adjustments Approval screen.
 */
public class CcAdjustmentApprovalScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** HoldAdjustmentDataSet object wrapped by this object. */
    private HoldAdjustmentDataSet i_holdAdjustmentDataSet;

    /** Text giving information on the status of any adjustment approve/reject
     *  request that was attempted.
     */
    private String  i_infoText = "";
    
    /** Average time taken to approve an adjustment. Retrieved from properties file. */
    private int i_approveAdjTime = 0;
    
    /** Average time taken to reject an adjustment. Retrieved from properties file. */
    private int i_rejectAdjTime = 0;
    
    /** Data set containing all asce error codes from the database. */
    private AsceFailureReasonDataSet i_ascsDataSet;
    
    /** The hold failure reason code. This is used when viewing the detail screen. */
    private int i_holdFailureReasonCode;
    
    /** The hold failure date/time. This is used when viewing the detail screen. */
    private String i_holdFailureDateTime;
    

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor. 
    *
    * @param p_guiContext  The GUI context
    * @param p_guiRequest  The GUI request
    */
    public CcAdjustmentApprovalScreenData(GuiContext p_guiContext,
                                          GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the HoldAdjustmentDataSet object wrapped within this object.
     * @return the hold_adjusts data set
     */
    public HoldAdjustmentDataSet getHoldAdjustmentDataSet()
    {
        return i_holdAdjustmentDataSet;
    }

    /** Sets the HoldAdjustmentDataSet object wrapped within this object.
     *
     * @param p_holdAdjustmentDataSet The Hold Adjustment Data Set
     */
    public void setHoldAdjustmentDataSet(
                                 HoldAdjustmentDataSet p_holdAdjustmentDataSet)
    {
        i_holdAdjustmentDataSet = p_holdAdjustmentDataSet;
    }

    /** Sets the text giving information on the status of any adjustment
     *  approve/reject request that was attempted.
     *
     * @param p_text The information text
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }

    /** 
     * Gets the text giving information on the status of any adjustment
     * approve/reject that was attempted.
     * @return the info text
     */
    public String getInfoText()
    {
        return i_infoText;
    }

    /**
     * Get the average time taken to approve an adjustment. This is a configured value
     * taken from the GUI proerties file.
     * @return average adjustment approval time
     */
    public int getApproveAdjTime()
    {
        return i_approveAdjTime;
    }

    /**
     * Set the average adjustment approval time.
     * @param p_approveTime the average adjustment approval time
     */
    public void setApproveAdjTime(int p_approveTime)
    {
        i_approveAdjTime = p_approveTime;
    }

    /**
     * Get the average time taken to reject an adjustment. This is a configured value
     * taken from the GUI proerties file.
     * @return average adjustment rejection time
     */
    public int getRejectAdjTime()
    {
        return i_rejectAdjTime;
    }

    /**
     * Set the average adjustment rejection time.
     * @param p_rejectTime the average adjustment rejection time
     */
    public void setRejectAdjTime(int p_rejectTime)
    {
        i_rejectAdjTime = p_rejectTime;
    }
    
    /**
     * Get the hold failure code for the record at the supplied index in the data set.
     * @param p_index the data set index of the required item
     * @return the hold failure code or an empty String
     */
    public String getHoldFailureCode(int p_index)
    {
        HoldAdjustmentData l_data = (HoldAdjustmentData)getHoldAdjustmentDataSet().getDataSetV().get(p_index);
        
        return (l_data.getHoldFailedReason() == null) ? "" : l_data.getHoldFailedReason().toString();
    }
    
    /**
     * Get the ded acc id for the record at the supplied index in the data set.
     * @param p_index the data set index of the required item
     * @return the ded acc id or an empty String
     */
    public String getDedAccId(int p_index)
    {
        HoldAdjustmentData l_data = (HoldAdjustmentData)getHoldAdjustmentDataSet().getDataSetV().get(p_index);
        
        return (l_data.getDedAccId() == null) ? "" : l_data.getDedAccId();
    }
    
    /**
     * Set the adjustment failure reason data set.
     * @param p_dataSet adjustment failure reason data set
     */
    public void setAdjFailureReasonData(AsceFailureReasonDataSet p_dataSet)
    {
        i_ascsDataSet = p_dataSet;
    }
    
    /**
     * Get the failure reason description related to the supplied code.
     * @param p_failureCode a failure code
     * @return a correct failure description, or an empty String
     */
    public String getAdjFailureReasonDesc(int p_failureCode)
    {
        String l_return = i_ascsDataSet.getFailureReason(p_failureCode);
        
        if (l_return == null)
        {
            l_return = "";
        }

        return l_return;
    }
    
    /**
     * Get the hold failure reason code. Used when viewing the detail screen.
     * @return hold failure reason code
     */
    public int getHoldFailureReasonCode()
    {
        return i_holdFailureReasonCode;
    }
    
    /**
     * Set the hold failure reason code. Used when viewing the detail screen.
     * @param p_code the hold failure reason code
     */
    public void setHoldFailureReasonCode(int p_code)
    {
        i_holdFailureReasonCode = p_code;
    }


    /**
     * Get the hold failure date/time. Used when viewing the detail screen.
     * @return hold failure date/time
     */
    public String getHoldFailureDateTime()
    {
        return i_holdFailureDateTime;
    }

    /**
     * Set the hold failure date/time. Used when viewing the detail screen.
     * @param p_dateTimeAsString the hold failure date/time
     */
    public void setHoldFailureDateTime(String p_dateTimeAsString)
    {
        i_holdFailureDateTime = p_dateTimeAsString;
    }
}