////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcServiceFeeDeducationUT.java
//      DATE            :       25-Jan-2006
//      AUTHOR          :       Michael Erskine (40771f)
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-Data 2006
//
//      DESCRIPTION     :       Unit Test class for the GUI Service Fees screen. 
//                 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.io.IOException;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.xml.sax.SAXException;

import com.meterware.httpunit.Button;
import com.meterware.httpunit.HTMLElement;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.httpunit.WebTable;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.gui.servlet.GuiTestTT;

/**
 * Unit Test class to test the GUI sub-system using the HTTPUnit testing framework (v 1.5.4)
 * <BR>
 * <BR>
 * NOTE: Due to the lack of JavaScript support within the HTTPUnit testing framework
 * (Rhino's js.jar implementation), all Scripting errors have been suppressed via
 * the removal of the js.jar file from the classpath.
 */
public class CcServiceFeeDeductionUT extends GuiTestTT
{
    /** Name of form. Value is {@value}. */
    private static final String C_FORM_NAME = "serviceFeeDeductionForm";

    /** Name of Account table. Value is {@value}. */
    private static final String C_TABLE_ACCOUNT_DEBTS = "accountDebts";

    /** Name of Latest Debts table. Value is {@value}. */
    private static final String C_TABLE_LATEST_DEBTS = "latestDebts";
    
    /** Name of History table. Value is {@value}. */
    private static final String C_TABLE_HISTORY_ID = "servicefeehistory";

    /**
     * Required constructor for JUnit testcase. Any subclass of <code>TestCase</code>
     * must implement a constructor that takes a test case name as it's argument and further
     * makes a super call to its Parent class 
     * {@linkplain com.slb.sema.ppas.gui.servlet.GuiTestTT}.
     *
     * @param p_name The testcase name.
     */
    public CcServiceFeeDeductionUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Prior to the execution of each test the following behaviour is implemented:
     * <BR>
     * <BR>
     * <OL>
     * <LI> Determines whether the client should automatically follow page redirect requests  
     *      (status 3xx). By default, this is true in order to simulate normal browser operation.
     *      executed prior to the initiation of each respective test.
     * <LI> Specifies whether the client should automatically follow page refresh requests. 
     *      By default, this is false, so that programs can verify the redirect page presented 
     *      to users before the browser switches to the new page. Setting this to true can 
     *      cause an infinite loop on pages that refresh themselves.
     * </OL>
     */
    public void setUp()
    {
        c_webConv.getClientProperties().setAutoRedirect(true);
        c_webConv.getClientProperties().setAutoRefresh(true);
        HttpUnitOptions.setScriptingEnabled(false);
        HttpUnitOptions.setExceptionsThrownOnScriptError(false);
        login("SUPER", "SEMAUK");
    }
    
    /**
     * This test installs a subscriber, inserts 2 rows into the SFDE_SERVICE_FEE_DEDUCTIONS table
     * for that subscriber, then loads the Service Fees screen and validates the page returned. 
     * @ut.when An operator loads the service fees screen
     * @ut.then The Service Fees page is returned and it is correctly populated.
     */
    public void testLoadServiceFeesScreen()
    {
        String      l_custId = null;
        SqlString   l_sqlString = null;
        String      l_sql = null;
        JdbcResultSet l_resultSet = null;
        
        beginOfTest("Start testLoadServiceFeesScreen");
        
            
        // Gets the next test MSISDN and installs it
        String l_msisdn = getTestMsisdn();
        System.out.println("l_msisdn installed: " + l_msisdn);
        
        try
        {
            //TODO: Should use getBAD on DbServiceTT instead.
            l_sql = "select cust_id from cust_mast where cust_mobile_number = " + l_msisdn;
            l_sqlString = new SqlString(500, 0, l_sql);
        
            l_resultSet = super.sqlQuery(l_sqlString);
        
            while (l_resultSet.next(11110))
            {
                l_custId = l_resultSet.getString(11120, "cust_id");
            }
            
            assertNotNull("Unable to obtain cust_id from cust_mast for MSISDN: " + l_msisdn, l_custId);
        
            // Insert a service fee deduction row for display in screen.
            l_sql = "insert into sfde_service_fee_deductions values({0}, {1}, {2}, {3}, {4}, {5}, {6}, " +
                    "{7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16})";
        
            l_sqlString = new SqlString(500, 18, l_sql);
            l_sqlString.setIntParam(0, l_custId);
            l_sqlString.setDateParam(1, new PpasDate("24-Jan-2006"));
            l_sqlString.setStringParam(2, C_DEFAULT_OPID);
            l_sqlString.setDateTimeParam(3, new PpasDateTime("24-Jan-2006 00:00:00"));
            l_sqlString.setStringParam(4, "AAA");
            l_sqlString.setIntParam(5, 1); //Success Code
            l_sqlString.setIntParam(6, 10);
            l_sqlString.setStringParam(7, "EUR");
            l_sqlString.setStringParam(8, l_custId);
            l_sqlString.setIntParam(9, "10"); //Debt
            l_sqlString.setStringParam(10, "01");
            l_sqlString.setStringParam(11, "02");
            l_sqlString.setDateParam(12, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(13, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(14, new PpasDate("24-Jan-2006"));
            l_sqlString.setIntParam(15, "10");
            l_sqlString.setMoneyParam(16, null);
        
            sqlUpdate(l_sqlString);
            
            l_sqlString.setIntParam(0, l_custId);
            l_sqlString.setDateParam(1, new PpasDate("24-Jan-2006"));
            l_sqlString.setStringParam(2, C_DEFAULT_OPID);
            l_sqlString.setDateTimeParam(3, new PpasDateTime("24-Jan-2006 01:00:00"));
            l_sqlString.setStringParam(4, "BBB");
            l_sqlString.setIntParam(5, 1); //Success Code
            l_sqlString.setIntParam(6, 10);
            l_sqlString.setStringParam(7, "EUR");
            l_sqlString.setStringParam(8, l_custId);
            l_sqlString.setIntParam(9, "0"); //Debt
            l_sqlString.setStringParam(10, "01");
            l_sqlString.setStringParam(11, "02");
            l_sqlString.setDateParam(12, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(13, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(14, new PpasDate("24-Jan-2006"));
            l_sqlString.setIntParam(15, "0");
            
            sqlUpdate(l_sqlString);
        }
        catch (PpasSqlException e1)
        {
            failedTestException(e1);
        }
            
        loadMaintainSubsScreenForMsisdn(l_msisdn);
//        HttpUnitOptions.setScriptingEnabled(true);
//        HttpUnitOptions.setExceptionsThrownOnScriptError(false);
//        snooze(2);
        sendRequest(constructGetRequest("cc/ServiceFeeDeduction", "GET_SCREEN_DATA"));
        
        try
        {
            System.out.println("Loaded Service Fees screen: \n" + c_webResponse.getText());
        }
        catch (IOException e)
        {
            failedTestException(e);
        }

        
        validateServiceFeeResponse(null, null, "ASCS - Service Fee Deduction");
        
        verifyRowInTable(C_TABLE_LATEST_DEBTS, 0, 
                         new String[] {"Type", "Description", "Total Debt Amount", "Total Debt Amount"});
        
        // TODO: This row will only be shown if the "All Services" radio button is selected 
        //verifyRowInTable(C_TABLE_LATEST_DEBTS, 1, new String[] {"SF Type", "Undefined", "10.00", "EUR"});
        
        try
        {
            Button l_latestDebtButton = c_webResponse.getForms()[0].getButtonWithID("latestDebtButton");
            // TODO: "latestDebtButton" is null.
            //assertNotNull("Latest Debt button is null", l_latestDebtButton);
            HTMLElement[] l_debtButton = c_webResponse.getElementsWithName("p_latestDebtButton");
            System.out.println("MIE l_debtButton[0]: " + l_debtButton[0]);
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
        
        // Javascript is suppressed so this row won't be displayed...
        //verifyRowInTable(C_TABLE_LATEST_DEBTS, 1, 
        //                 new String[] {"AAA", "Undefined", "10.00", "EUR"});
        
        //try
        //{
        //    verifyRowInTable(C_TABLE_LATEST_DEBTS, 2, new String[] {"", "", "", ""});
        //    fail("Row should not exist in " + C_TABLE_ACCOUNT_DEBTS + "- expect ArrayOutOfBoundsException!");
        //}
        //catch (ArrayIndexOutOfBoundsException l_e)
        //{
        //    // Ignore and contine
        //}
        
        // Note that Fee Amount and Debt Amount have column spans of 2.
        verifyRowInTable(C_TABLE_HISTORY_ID, 0,
                         new String[] {"Details", "Date Time", "Type", "Description", "Code", 
                                       "Fee Amount", "Fee Amount", "Debt Amount", "Debt Amount", 
                                       "Initiating MSISDN"});
        
        verifyRowInTable(C_TABLE_HISTORY_ID, 1,
                         new String[] {"", "24-Jan-2006 01:00:00", "BBB", "Undefined", "OK",
                                       "10.00", "EUR", "0.00", "EUR", "+44 (0) " + l_msisdn});
        
        verifyRowInTable(C_TABLE_HISTORY_ID, 2,
                         new String[] {"", "24-Jan-2006 00:00:00", "AAA", "Undefined", "OK",
                                       "10.00", "EUR", "10.00", "EUR", "+44 (0) " + l_msisdn});
        
        try
        {
            verifyRowInTable(C_TABLE_HISTORY_ID, 3, new String[] {"", "", "", "", "", "", "", "", "", ""});
            fail("Row should not exist - expect ArrayOutOfBoundsException!");
        }
        catch (ArrayIndexOutOfBoundsException l_e)
        {
            // Ignore and contine
        }
        HttpUnitOptions.setScriptingEnabled(false);
        HttpUnitOptions.setExceptionsThrownOnScriptError(false);     
        endOfTest();
    }
    
    /**
     * @ut.when An operator posts sends a request to view the Service Fee detail
     * @ut.then A correctly populated HTML page is returned.
     */
    public void testServiceFeesPopup()
    {
        WebRequest  l_request = null;
        WebResponse l_response = null;
        String      l_custId = null;
        SqlString   l_sqlString = null;
        String      l_sql = null;
        JdbcResultSet l_resultSet = null;
        
        beginOfTest("Start testServiceFeesPopup");
        
        // Gets the next test MSISDN and installs it
        String l_msisdn = getTestMsisdn();
        System.out.println("l_msisdn installed: " + l_msisdn);
        
        try
        {
            //TODO: Should use getBAD on DbServiceTT instead.
            l_sql = "select cust_id from cust_mast where cust_mobile_number = " + l_msisdn;
            l_sqlString = new SqlString(500, 0, l_sql);
        
            l_resultSet = super.sqlQuery(l_sqlString);
        
            while (l_resultSet.next(11110))
            {
                l_custId = l_resultSet.getString(11120, "cust_id");
            }
            
            assertNotNull("Unable to obtain cust_id from cust_mast for MSISDN: " + l_msisdn, l_custId);
        
            // Insert a service fee deduction row for display in screen.
            l_sql = "insert into sfde_service_fee_deductions values({0}, {1}, {2}, {3}, {4}, {5}, {6}, " +
                    "{7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16})";
        
            l_sqlString = new SqlString(500, 17, l_sql);
            l_sqlString.setIntParam(0, l_custId);
            l_sqlString.setDateParam(1, new PpasDate("24-Jan-2006"));
            l_sqlString.setStringParam(2, C_DEFAULT_OPID);
            l_sqlString.setDateTimeParam(3, new PpasDateTime("24-Jan-2006 00:00:00"));
            l_sqlString.setStringParam(4, "AAA");
            l_sqlString.setIntParam(5, 1); //Success Code
            l_sqlString.setIntParam(6, 10);
            l_sqlString.setStringParam(7, "EUR");
            l_sqlString.setStringParam(8, l_custId);
            l_sqlString.setIntParam(9, "10"); //Debt
            l_sqlString.setStringParam(10, "01");
            l_sqlString.setStringParam(11, "02");
            l_sqlString.setDateParam(12, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(13, new PpasDate("24-Jan-2006"));
            l_sqlString.setDateParam(14, new PpasDate("24-Jan-2006"));
            l_sqlString.setIntParam(15, "10");
            l_sqlString.setMoneyParam(16, null);
        
            sqlUpdate(l_sqlString);
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
            
        loadMaintainSubsScreenForMsisdn(l_msisdn);
        
        HashMap l_paramsHashMap = new HashMap();
        
        l_paramsHashMap.put("p_custId", l_custId);
        l_paramsHashMap.put("p_chargedCustId", l_custId);
        l_paramsHashMap.put("p_serviceFeeType", "BBB");
        l_paramsHashMap.put("p_dateTime", "24-Jan-2006 01:00:00");
        l_paramsHashMap.put("p_deduction", "10.00");
        l_paramsHashMap.put("p_deductionCurr", "EUR");
        l_paramsHashMap.put("p_subscriberAmount", "0.00");
        l_paramsHashMap.put("p_subscriberAmountCurr", "EUR");
        l_paramsHashMap.put("p_deductedAmount", "10.00");
        l_paramsHashMap.put("p_deductedAmountCurr", "EUR");
        l_paramsHashMap.put("p_debt", "0.00");
        l_paramsHashMap.put("p_debtCurr", "EUR");
        l_paramsHashMap.put("p_successCode", "1");
        l_paramsHashMap.put("p_successCodeAcronym", "OK");
        l_paramsHashMap.put("p_successCodeDesc", "Service Fee was deducted in full");
        l_paramsHashMap.put("p_oldServiceClass", "1");
        l_paramsHashMap.put("p_newServiceClass", "2");
        l_paramsHashMap.put("p_oldExpiryDate", "24-Jan-2006");
        l_paramsHashMap.put("p_newExpiryDate", "24-Jan-2006");
        sendRequest(
            constructPostRequest("cc/ServiceFeeDeduction", "GET_SERVICE_FEE_DETAIL", l_paramsHashMap));
        
//    
//    /ascs/gui/cc/ServiceFeeDeduction?p_command=GET_SERVICE_FEE_DETAIL&p_custId=1058&p_chargedCustId=1058&p_serviceFeeType=BBB&p_dateTime=24-Jan-2006 0
//            1:00:00&p_deduction=10.00&p_deductionCurr=EUR&p_subscriberAmount=0.00&p_subscriberAmountCurr=EUR&p_deductedAm
//            ount=10.00&p_deductedAmountCurr=EUR&p_debt=0.00&p_debtCurr=EUR&p_successCode=1&p_successCodeAcronym=OK&p_succ
//            essCodeDesc=Service Fee was deducted in full.&p_oldServiceClass=1&p_newServiceClass=2&p_oldExpiryDate=24-Jan-
//            2006&p_newExpiryDate=24-Jan-2006
        
        try
        {
            System.out.println("ServiceFeesPopup text: " + c_webResponse.getText());
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        
        validateServiceFeeResponse(null, null, "Service Fee Deduction Details");
        
        verifyRowInTable("accountIds", 0, 
                         new String[] {"Subscriber Id", l_custId, "Master Account Id", l_custId});
        
        endOfTest();       
    }
    

    /**
     * Performs standard clean up activities at the end of a test
     * including closing database connections
     */
    protected void tearDown()
    {
        super.tearDown();
        logout();
        System.out.println(":::End Of Test:::");
    }
    
    /** 
     * Static method that allows the framework to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes calling this suite method and
     * get an instance of the TestCase which it then executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. 
     */
    public static Test suite()
    {
        return new TestSuite(CcServiceFeeDeductionUT.class);
    }
    
    /**
     * Convenience method to get a test MSISDN.
     * @return String representing a test MSISDN.
     */
    private String getTestMsisdn()
    {
        String l_msisdn = c_ppasContext.getMsisdnFormatInput().format(installAccountClass(1));
        //snooze(2);  // Ensure account installed.
        return l_msisdn;
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class.
     * @param p_args not used
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    
    //-------------------------------------------------------------------------
    // Private methods.
    //-------------------------------------------------------------------------
    
    /**
     * Validates the response from the server. Any parameters passed in as <code>null</code> are not
     * checked. 
     * @param p_expectedMessage   The expected message popup text
     * @param p_expectedFrameName The name of the expected frame
     * @param p_expectedTitle     Expected title
     */
    private void validateServiceFeeResponse(String  p_expectedMessage,
                                            String  p_expectedFrameName,
                                            String  p_expectedTitle)
    {   
        say ("Entered validateServiceFeeResponse()");
        validateResponse(p_expectedMessage, p_expectedFrameName, p_expectedTitle);
        
        try
        {
//            if (c_webResponse.getResponseCode() != 200)
//            {
//                fail( "Expected response code is 200, but was: " + c_webResponse.getResponseCode());
//            }
//            
//            if (p_expectedTitle != null)
//            {
//                if (!p_expectedTitle.equals(c_webResponse.getTitle()))
//                {
//                    fail ("Expected Title: " + p_expectedTitle + ", but was: " + c_webResponse.getTitle());
//                }
//            }
//            
//            if (p_expectedMessage != null)
//            {
//                l_index = l_responseText.indexOf(p_expectedMessage);
//                say("MIE l_index: " + l_index);
//                if (l_index == -1)
//                {
//                    fail("Required text not found in response, Expected: " + p_expectedMessage +
//                         ", but was: " + l_responseText);
//                }
//            }
            
            
//            WebTable[] l_webTables = c_webResponse.getTables();
            
            WebTable l_webTable = c_webResponse.getTableWithID("latestDebts");
            
            if (l_webTable != null)
            {
                System.out.println("MIE getTableWithId: latestDebts");
                for (int i = 0; i < l_webTable.getRowCount(); i++)
                {
                    for (int j = 0; j < l_webTable.getColumnCount(); j++)
                    {
                        System.out.println("l_webTable[" +i +"][" + j + "]: " + l_webTable.getCellAsText(i,j));
                    }
                }
            }
            
//            System.out.println("l_webTables.length: " + l_webTables.length);
//            for (int i = 0; i < l_webTables.length; i++)
//            {
//                //l_webTables[i].asText();
//                System.out.println("Cell 0,0 as text: " + l_webTables[i].getCellAsText(0,0));
//                System.out.println("Cell 0,1 as text: " + l_webTables[i].getCellAsText(0,1));
//                System.out.println("Cell 0,2 as text: " + l_webTables[i].getCellAsText(0,2));
//                // 1,1 give an ArrayIndexOutOfBoundsException...
//                //System.out.println("Cell 1,1 as text: " + l_webTables[i].getCellAsText(1,1));
//            }
        }
        catch (SAXException e)
        {
            failedTestException(e);
        }
//        catch (IOException e)
//        {
//            failedTestException(e);
//        }
        
        say ("Leaving validateResponse");
    }
}