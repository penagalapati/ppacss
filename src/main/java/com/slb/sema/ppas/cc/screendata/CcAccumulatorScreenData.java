////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccumulatorScreenData.java
//      DATE            :       8-Oct-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1597/6736
//                              PRD_PPAK00_GEN_CA_396
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the accumulator 
//                              details screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.common.dataclass.AccumulatorDataSet;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the accumulator details screen.
 */
public class CcAccumulatorScreenData extends GuiScreenData
{
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** AccumulatorDataSet object wrapped by this object. */
    private AccumulatorDataSet i_accDataSet   = null;

    /** 
     * Text giving information on the status of any accumulator adjustment 
     * request that was attempted.
     */
    private String  i_infoText = "";

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** 
     * Simple constructor.
     * @param p_guiContext  GUI context object
     * @param p_guiRequest  GUI request object
     */ 
    public CcAccumulatorScreenData( GuiContext p_guiContext,
                                    GuiRequest p_guiRequest)
    {
        super(p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the AccumulatorDataSet object wrapped within this object.
     * Returns null if there is no AccumulatorDataSet object set.
     * @return Set of AccumulatorData objects. 
     */
    public AccumulatorDataSet getAccumulatorDataSet()
    {
        return ( i_accDataSet );
    }

    /** 
     * Sets <code>i_accDataSet</code> to <code>p_accDataSet</code>.
     * @param p_accDataSet Set of AccumulatorData objects. 
     */
    public void setAccumulatorDataSet(AccumulatorDataSet p_accDataSet)
    {
        i_accDataSet = p_accDataSet;
    }

    /**
     * Returns true if the object contains all the data necessary to populate
     * the associated screen.  Otherwise returns false.
     * @return True if the object is populated.
     */
    public boolean isPopulated()
    {
        return ( i_accDataSet != null );
    }

    /**
     * Returns true if the Operator making the request is permitted to make an
     * update to accumulator data. Otherwise returns false.
     * @return True if the operator can make an accumulator update.
     */
    public boolean isUpdateAccumPermitted()
    {
        return i_gopaData.updateAccumIsPermitted();
    }

    /** 
     * Gets the text giving information on the status of any accumulator 
     * update request that was attempted.
     * @return Text associated with success or failure of accumulator update.
     */
    public String getInfoText()
    {
        return(i_infoText);
    }

    /** 
     * Sets the text giving information on the status of any accumulator 
     * update request that was attempted.
     * @param p_text Text associated with success or failure of accumulator update.
     */
    public void setInfoText(String p_text)
    {
        i_infoText = p_text;
    }
}