////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcVoucherRefillHistoryDetailScreenData.java
//      DATE            :       2-Sep-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1513/6383
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Gui Screen Data object for the Voucher History
//                              Detail screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 19/09/03 | M.Brister  | Added original amount and       | PpacLon#43/384
//          |            | currency, applied amount and    |
//          |            | currency, and transaction       |
//          |            | datetime attributes.            |
//----------+------------+---------------------------------+--------------------
// 12/12/05 | K Goswami  | Changed code(refactored)        | PpaLon#1892/7603
//          |            | for voucher history popup       | CA#39
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Voucher History Detail screen.
 */
public class CcVoucherRefillHistoryDetailScreenData extends CcRefillHistoryDetailScreenData
{
    /** Date and time of recharge. */
    private String i_transDateTimeStr = null;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Simple constructor.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcVoucherRefillHistoryDetailScreenData(GuiContext p_guiContext,
                                                  GuiRequest p_guiRequest)
    {
        super( p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------

    /** Retrieves the transaction date and time as a string.
     * @return Transaction date/time.
     */
    public String getTransDateTimeStr()
    {
        return (i_transDateTimeStr);
    }
    
    /** Sets the transaction date and time as a string.
     * @param p_transDateTimeStr Transaction date/time.
     */
    public void setTransDateTimeStr(String p_transDateTimeStr)
    {
        i_transDateTimeStr = p_transDateTimeStr;
    }

} // end public class CcVoucherRefillHistoryDetailScreenData
