////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcCallHistoryService.Java
//DATE            :       08-Mar-2006
//AUTHOR          :       Kanta Goswami
//REFERENCE       :       PpacLon#2026/8071
//                        PRD_ASCS00_GEN_CA_066_D1
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Gui Service that wraps calls to
//                        PpasCallHistoryService.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//         |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.CallHistoryDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
* CcXResponse class for VoucherHistoryData objects.
*/
public class CcCallHistoryResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcCallHistoryResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------
    /** The CallHistoryData object wrapped by this object. */
    private CallHistoryDataSet    i_callHistoryData;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------
    /**
     * Uses a CallHistoryDataSetobject and the additional necessary objects to create a response containing
     * message data and callHistory data.
     * @param p_guiRequest The request being processed.
     * @param p_messageKey Response message key.
     * @param p_messageParams Any parameters to be substituted into response message.
     * @param p_messageSeverity Severity of response.
     * @param p_callHistoryDataSet The call History details.
     */
    public CcCallHistoryResponse(GuiRequest p_guiRequest,
                                 String p_messageKey,
                                 Object[] p_messageParams,
                                 int p_messageSeverity,
                                 CallHistoryDataSet p_callHistoryDataSet)
    {
        super(p_guiRequest, p_messageKey, p_messageParams, p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_callHistoryData = p_callHistoryDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10010,
                           this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the callHistory history information.
     * @return CallHistory History data.
     */
    public CallHistoryDataSet getCallHistoryData()
    {
        return i_callHistoryData;
    }
}