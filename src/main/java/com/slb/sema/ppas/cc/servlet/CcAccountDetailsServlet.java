////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccountDetailsServlet.Java
//      DATE            :       30-Jan-2002
//      AUTHOR          :       Matt Kirk / Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_38
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Account Details screen.
//
///////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
///////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                          | REFERENCE
//----------+------------+--------------------------------------+---------------------
// 20/05/05 | S J Vonka  | Handle old & new pin codes, updates  |PRD_ASCS00_GEN_CA_038
//          |            | made to updateAccountDetails         | CR1537#6456
//----------+------------+--------------------------------------+---------------------
//17/10/05  | K Goswami  | Changed code to allow temp           | PpacLon#463
//          |            | blocking privilege.                  |
//-----------------------+--------------------------------------+---------------------
// 21/02/06 | Ian James  | Remove empty EndOfCallNotificationId | PpacLon#1900/7937
//          |            | contructor for an undefined EoCN ID. |
//          |            | This is no longer needed because     |
//          |            | updateAccountDetails will always     |
//          |            | return a value on updates.           |
//-----------------------+--------------------------------------+---------------------
// 24/02/06 | M Erskine  | Get BVT working by allowing for blank| PpacLon#1912/7989
//          |            | Eocn Id. This will be modified once  |
//          |            | the BVT has been replaced, since the |
//          |            | GUI client will always pass a value. | 
//----------+------------+--------------------------------------+--------------------
// 14/03/06 | R Isaacs   | Additional check to verify           | CR2029#8112
//          |            | l_oldHomeRegion and l_ivrUnbar are   |
//          |            | not null to avoid a NullPointer      |
//          |            | being thrown.                        |
//----------+------------+--------------------------------------+---------------------
// 15/03/06 | Ian James  | Remove hard coded EoCN ID ("255")    | PpacLon#1577/8150
//          |            | re-instating no arguments contructor.| 
//----------+------------+--------------------------------------+---------------------
// 02/11/06 | R.Grimshaw | Additional checks to verify          | PpacLon#2712
//          |            | l_oldHomeRegion and l_ivrUnbar are   |
//          |            | not null when checking if access     |
//          |            | is granted.                          |
//----------+------------+--------------------------------------+---------------------
// 16/03/07 | Andy Harris| Tidied up PIN code validation.       | PpacLon#1862/11165 
//----------+------------+--------------------------------------+---------------------
// 22/05/07 | S James    | Changes to support GUI reconnection  | PpacLon#3072/11370
//////////////////////////////////////////////////////////////////////////////////////

// l_oldHomeRegion, l_ivrUnbar

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcAccountDetailsScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcCustomerDetailsDataResponse;
import com.slb.sema.ppas.cc.service.CcCustomerDetailsService;
import com.slb.sema.ppas.cc.service.CcEventHistoryDataSetResponse;
import com.slb.sema.ppas.cc.service.CcEventHistoryService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageDataSet;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.PinCode;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.is.isapi.PpasEventHistoryService;

/** This servlet handles requests from the Account Details screen to update 
 *  the subscriber's Account Details and also the initial request to display the
 *  Account Details for the currently selected subscriber.
 */
public class CcAccountDetailsServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAccountDetailsServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Account Service to be used by this servlet. */
    private CcAccountService i_ccAccountService = null;

    /** The CC Customer Details Service to be used by this servlet. */
    private CcCustomerDetailsService i_ccCustomerDetailsService = null;

    /** The CC Event History Service to be used by this servlet. */
    private CcEventHistoryService i_ccEventHistoryService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructs an instance of a CcAccountDetailsServlet object to handle
     *  requests for the Account Details Customer Care screen.  */
    public CcAccountDetailsServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START | WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10000, this,
                           "Constructing/ed " + C_CLASS_NAME );
        }

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /** 
     * Performs initialisation specific to this Servlet; currently there is
     * no specific initialisation for this Servlet.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START | WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 20000, this,
                           "Entered/Leaving " + C_METHOD_doInit);
        }
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** 
     * Service method to control requests for the Account Details screen
     * service. These requests will be either to update some of the 
     * subscriber's details (e.g. language or service class) or simply to
     * display the subscriber's details. The Servlet constructs a 
     * new <code>CcAccountDetailsScreenData</code> object which is attached
     * to the incoming HTTP request before it is forwarded to the JSP
     * script that generates the Account Details screen. The 
     * <code>CcAccountDetailsScreenData</code> contains all the information
     * required to construct the Account Details screen.
     * @param p_request <code>HttpServletRequest</code>
     * @param p_response <code>HttpServletResponse</code>
     * @param p_httpSession <code>HttpSession</code>
     * @param p_guiRequest <code>GuiRequest</code>
     * @return forwarding URL
     */
    protected String doService(HttpServletRequest  p_request,
                               HttpServletResponse p_response,
                               HttpSession         p_httpSession,
                               GuiRequest          p_guiRequest)
    {
        String                      l_command     = null;
        String                      l_forwardUrl  = null;
        CcAccountDetailsScreenData  l_accountDetailsScreenData  = null;
        GuiResponse                 l_guiResponse = null;
        boolean                     l_accessGranted = true;


        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 13000, this,
                           "ENTERED " + C_METHOD_doService);
        }

        // Construct CcAccountService if it does not already exist.
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcCustomerDetailsService if it does not already exist.
        if (i_ccCustomerDetailsService == null)
        {
            i_ccCustomerDetailsService = 
                         new CcCustomerDetailsService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Construct CcEventHistoryService if it does not already exist.
        if (i_ccEventHistoryService == null)
        {
            i_ccEventHistoryService = new CcEventHistoryService (p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create screen data object that will contain all the data required
        // to construct the Account Details screen.
        l_accountDetailsScreenData = new CcAccountDetailsScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the 'command' parameter from the incoming HTTP request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,  // Command is mandatory ...
                                      "",    // ... so has no default value
                                      new String [] {"GET_SCREEN_DATA",
                                                     "UPDATE_ACCOUNT_DETAILS"});

            // Get business configuration data required for the screen
            getBusinessConfigData (p_guiRequest, l_accountDetailsScreenData);

            // Is the 'command' a request to update the account's details?
            if (l_command.equals("UPDATE_ACCOUNT_DETAILS"))
            {
                setIsUpdateRequest(true);
                
                l_accessGranted = updateAccountDetails(p_guiRequest,
                                     p_request,
                                     l_accountDetailsScreenData);
                                
            }

            if (l_accessGranted)
            {
                getAccountDetails(p_guiRequest, l_accountDetailsScreenData);
            }

        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 73872, this,
                               "Caught exception: " + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Add the response generated by handling the exception to the screen data object.
            l_accountDetailsScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
        } // end catch 

        // If we have encountered an error on attempting to retrieve the 
        // account details, then forward to the error page and store the screen
        // data on the HTTP request for use by the error page.

        if (!l_accessGranted)
        {
            // Insufficient privilege - display Access Error screen & refuse access
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                            new Object [] {},
                                            GuiResponse.C_SEVERITY_FAILURE);

            l_accountDetailsScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

            l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

            p_request.setAttribute("p_guiScreenData", l_accountDetailsScreenData);

        }
        else if (l_accountDetailsScreenData.hasRetrievalError())
        {
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

            p_request.setAttribute ("p_guiScreenData", l_accountDetailsScreenData);
        }
        else
        {
            // No retrieval errors have occurred, so forward to the account
            // details JSP and store the screen data on the HTTP request for use in constructing the screen
            l_forwardUrl = "/jsp/cc/ccaccountdetails.jsp";

            // Store the screen data on the HTTP request to be forwarded to
            // the CcAccountDetails JSP
            p_request.setAttribute("p_guiScreenData", l_accountDetailsScreenData);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 13090, this,
                           "LEAVING " + C_METHOD_doService + 
                           ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateAccountDetails = "updateAccountDetails";
    /** 
     * This method updates various account details that have changed as 
     * defined by the 'old' and 'new' values for the details supplied
     * in the HTTP request.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_request <code>mHttpServletRequest</code> containing the request
     * @param p_screenData The <code>CcAccountDetailsScreenData</code> object
     *                     that will be used to return the outcome of the
     *                     requested updates and the new account details
     *                     to the account details screen.
     * @return boolean indicating if the request was permitted
     */
    private boolean updateAccountDetails(GuiRequest                 p_guiRequest,
                                         HttpServletRequest         p_request,
                                         CcAccountDetailsScreenData p_screenData)
    {
        String      l_oldAirtimeExpiryDate = "";
        String      l_newAirtimeExpiryDate = "";
        String      l_oldServiceExpiryDate = "";
        String      l_newServiceExpiryDate = "";
        String      l_oldServiceClass      = "";
        String      l_newServiceClass      = "";
        String      l_oldLanguage          = "";
        String      l_newLanguage          = "";
        String      l_ivrUnbar             = "";
        GuiResponse l_guiResponse          = null;
        String      l_oldAgent             = "";
        String      l_newAgent             = "";
        String      l_oldSubAgent          = "";
        String      l_newSubAgent          = "";
        boolean     l_accessGranted        = true; 
        GopaGuiOperatorAccessData l_gopaData = null;
        boolean     l_oldTempBlocked       = false;
        boolean     l_newTempBlocked       = false;
        String      l_newPinCodeStr        = null;
        String      l_oldPinCodeStr        = null;
        PinCode     l_newPinCode           = null;
        PinCode     l_oldPinCode           = null;
        String      l_oldEocnIdStr         = "";
        String      l_newEocnIdStr         = "";
        String      l_oldHomeRegionStr     = "";
        String      l_newHomeRegionStr     = "";
        HomeRegionId l_oldHomeRegion       = null;
        HomeRegionId l_newHomeRegion       = null;

        EndOfCallNotificationId  l_oldEocnId = null;
        EndOfCallNotificationId  l_newEocnId = null;
        PpasServletException     l_pSEx      = null;

        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 27000, this,
                           "Entered " + C_METHOD_updateAccountDetails);
        }

        // Get params from request...

        // Make all the parameters non-mandatory and default to an empty String,
        // since they may arrive with null values when the account is a 
        // subordinate account.

        try
        {
            l_newAirtimeExpiryDate = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newAirtimeExpiryDate",
                              false,
                              "",
                              PpasServlet.C_TYPE_DATE);

            l_oldAirtimeExpiryDate = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldAirtimeExpiryDate",
                              false,
                              "",
                              PpasServlet.C_TYPE_DATE);

            l_newServiceExpiryDate = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newServiceExpiryDate",
                              false,
                              "",
                              PpasServlet.C_TYPE_DATE);

            l_oldServiceExpiryDate = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldServiceExpiryDate",
                              false,
                              "",
                              PpasServlet.C_TYPE_DATE);

            l_oldServiceClass = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldClass",
                              false, 
                              "",    
                              PpasServlet.C_TYPE_NUMERIC,
                              4,     // parameter must be <= 4
                              C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_newServiceClass = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newClass",
                              false, 
                              "",    
                              PpasServlet.C_TYPE_NUMERIC,
                              4,     // parameter must be <= 4
                              C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_oldLanguage = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldLanguage",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              2,    // Length must equal 2
                              PpasServlet.C_OPERATOR_EQUALS);
                                          
            l_newLanguage = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newLanguage",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              2,    // Length must equal 2
                              PpasServlet.C_OPERATOR_EQUALS);
            
            try
            {
                l_oldHomeRegionStr = 
                    getValidParam(p_guiRequest,
                                  PpasServlet.C_FLAG_ALLOW_BLANK,
                                  p_request,
                                  "p_oldHomeRegion",
                                  false, 
                                  "",   
                                  PpasServlet.C_TYPE_ANY,
                                  3,    // Length must be <=3
                                  PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);
            
                if (l_oldHomeRegionStr != null && !l_oldHomeRegionStr.equals(""))
                {
                    l_oldHomeRegion = new HomeRegionId(l_oldHomeRegionStr);
                }
            
                l_newHomeRegionStr = 
                    getValidParam(p_guiRequest,
                                  PpasServlet.C_FLAG_ALLOW_BLANK,
                                  p_request,
                                  "p_newHomeRegion",
                                  false, 
                                  "",   
                                  PpasServlet.C_TYPE_ANY,
                                  3,    // Length must be <=3
                                  PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);
                
                if (l_newHomeRegionStr != null && !l_newHomeRegionStr.equals(""))
                {
                    l_newHomeRegion = new HomeRegionId(l_newHomeRegionStr);
                }
            }
            catch (PpasParseException l_ppasParseEx)
            {
                l_pSEx = new PpasServletException(C_CLASS_NAME,
                                                  C_METHOD_doService,
                                                  51201,
                                                  this,
                                                  p_guiRequest,
                                                  0,
                                                  ServletKey.get().invalidHomeRegion(l_newHomeRegionStr),
                                                  l_ppasParseEx);
            }

            l_oldAgent = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldAgent",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              8,    // Length must equal <= 8
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);
                                          
            l_newAgent = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newAgent",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              8,    // Length must equal <= 8
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_oldSubAgent = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldSubAgent",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              8,    // Length must equal <= 8
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);
                                          
            l_newSubAgent = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newSubAgent",
                              false, 
                              "",   
                              PpasServlet.C_TYPE_ANY,
                              8,    // Length must equal <= 8
                              PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_ivrUnbar = 
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_ivrUnbar",
                              false, 
                              "",    
                              PpasServlet.C_TYPE_BOOLEAN);

            l_oldTempBlocked = Boolean.valueOf(getValidParam(p_guiRequest,
                                               PpasServlet.C_FLAG_ALLOW_BLANK,
                                               p_request,
                                               "p_oldTempBlocking",
                                               true, 
                                               "",    
                                               PpasServlet.C_TYPE_BOOLEAN)).booleanValue();

            // if p_tempBlocking does not have a value, set it to the same as p_oldTempBlocking,
            // else the status has changed so set it to the opposite of p_oldTempBlocking
            l_newTempBlocked = getValidParam(p_guiRequest,
                                             PpasServlet.C_FLAG_ALLOW_BLANK,
                                             p_request,
                                             "p_tempBlocking",
                                             false, 
                                             "",    
                                             PpasServlet.C_TYPE_ANY).equals("")
                                   ? l_oldTempBlocked : !l_oldTempBlocked;

            l_oldEocnIdStr =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldUssdEocnId",
                              false,
                              "",
                              PpasServlet.C_TYPE_ANY);
            
            try
            {
                //TODO: 40771f - This is temporary. l_oldEocnIdStr will never be blank or "" except for BVT.
                //When the offending BVT testcase is replaced, this code will be modified. No harm leaving it
                //in for now though.
                l_oldEocnId = 
                    l_oldEocnIdStr == null || l_oldEocnIdStr.trim().equals("") ?
                        new EndOfCallNotificationId() : 
                        new EndOfCallNotificationId(l_oldEocnIdStr);
            }
            catch (PpasParseException l_ppasParseEx)
            {
                // Generate a new servlet exception
                l_pSEx = new PpasServletException(C_CLASS_NAME,
                                                  C_METHOD_doService,
                                                  52200,
                                                  this,
                                                  p_guiRequest,
                                                  0,
                                                  ServletKey.get().invalidEocnId(l_oldEocnIdStr),
                                                  l_ppasParseEx);

                i_logger.logMessage(l_pSEx);
                throw (l_pSEx);
            }

            l_newEocnIdStr =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_newUssdEocnId",
                              false,
                              "",
                              PpasServlet.C_TYPE_ANY);

            try
            {
                //TODO: 40771f - This is temporary. l_newEocnIdStr will never be null or "" except for BVT.
                //When the offending BVT testcase is replaced, this code will be modified. No harm leaving it
                //in for now, though it's not required for the .
                l_newEocnId = 
                    l_newEocnIdStr == null || l_newEocnIdStr.trim().equals("") ?
                        new EndOfCallNotificationId("255") : 
                            new EndOfCallNotificationId(l_newEocnIdStr);
            }
            catch (PpasParseException l_ppasParseEx)
            {
                // Generate a new servlet exception
                l_pSEx = new PpasServletException(C_CLASS_NAME,
                                                  C_METHOD_doService,
                                                  52400,
                                                  this,
                                                  p_guiRequest,
                                                  0,
                                                  ServletKey.get().invalidEocnId(l_newEocnIdStr),
                                                  l_ppasParseEx);

                i_logger.logMessage(l_pSEx);
                throw (l_pSEx);
            }

            try
            {                
                l_oldPinCodeStr = getValidParam(p_guiRequest,
                                                PpasServlet.C_FLAG_ALLOW_BLANK,
                                                p_request,
                                                "p_oldPinCode",
                                                false, // mandatory
                                                "",    // default
                                                PpasServlet.C_TYPE_NUMERIC);

                if (l_oldPinCodeStr != null && !l_oldPinCodeStr.equals(""))
                {
                    l_oldPinCode = new PinCode(l_oldPinCodeStr);
                }
                                          
                l_newPinCodeStr = getValidParam(p_guiRequest,
                                                PpasServlet.C_FLAG_ALLOW_BLANK,
                                                p_request,
                                                "p_newPinCode",
                                                false, // mandatory
                                                "",    // default
                                                PpasServlet.C_TYPE_NUMERIC,
                                                i_configService.getCompanyConfigData().getIvrPinCodeLength(),//default config for max length Pin Code
                                                PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);
                
                if (l_newPinCodeStr != null && !l_newPinCodeStr.equals(""))
                {
                    l_newPinCode = new PinCode(l_newPinCodeStr);
                }
            }
            catch (PpasParseException l_ppasParseEx)
            {
                // Generate a new servlet exception
                l_pSEx = new PpasServletException(C_CLASS_NAME,
                                                  C_METHOD_doService,
                                                  52201,
                                                  this,
                                                  p_guiRequest,
                                                  0,
                                                  ServletKey.get().invalidPinCode(l_newPinCodeStr),
                                                  l_ppasParseEx);
            }

        }

        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 95000, this,
                               "Caught exception:\n" + l_pSE);
            }

            // Handle the exception generating an appropriate response
            l_guiResponse = handleInvalidParam (p_guiRequest, l_pSE);

            // Add the response generated as an update response to the screen
            // data object.
            p_screenData.addUpdateResponse (p_guiRequest, l_guiResponse);
        }

        // Determine if the CSO has privilege to access the requested screen or function

        l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

        if ((!l_gopaData.expDatesUpdateIsPermitted()) &&
            ( (!l_oldAirtimeExpiryDate.equals(l_newAirtimeExpiryDate)) ||
            (!l_oldServiceExpiryDate.equals(l_newServiceExpiryDate)) ) )
        {
            l_accessGranted = false;
        }

        else if ((!l_gopaData.servClassChangeIsPermitted()) &&
             (!l_oldServiceClass.equals(l_newServiceClass)))
        {
            l_accessGranted = false;
        }

        else if ((!l_gopaData.languageChangeIsPermitted()) &&
                  (!l_oldLanguage.equals(l_newLanguage)))
        {
            l_accessGranted = false;
        }

        else if (!l_gopaData.agentUpdateIsPermitted() &&
                (!l_oldAgent.equals(l_newAgent)))
        {
            l_accessGranted = false;
        }

        else if (!l_gopaData.ivrUnbarIsPermitted() &&
                (l_ivrUnbar != null) && (!l_ivrUnbar.equals("")))
        {
            l_accessGranted = false;
        }

        else if (!l_gopaData.ussdEocnIdUpdateIsPermitted() &&
                  ((l_oldEocnId != null && !l_oldEocnId.equals(l_newEocnId)) ||
                   (l_oldEocnId == null && l_newEocnId != null)) )
        {
            l_accessGranted = false;
        }

        else if (!l_gopaData.subscriberPinUpdateIsPermitted() &&
                 l_newPinCode != null && !l_newPinCode.equals(l_oldPinCode))
        {
            l_accessGranted = false;
        }
        
        else if (!l_gopaData.homeRegionUpdateIsPermitted() &&
                 l_oldHomeRegion != null && !l_oldHomeRegion.equals(l_newHomeRegion))
        {
            l_accessGranted = false;
        }

        else if (!l_gopaData.tempBlockUpdateIsPermitted() &&
                (l_oldTempBlocked != l_newTempBlocked))
        {
            l_accessGranted = false;
        }
        
        if (l_accessGranted)
        {                
            // Now check what parameters are being updated and call the required
            // service(s).

            // Have the airtime or service expiry dates been updated?
            if ( (!l_oldAirtimeExpiryDate.equals(l_newAirtimeExpiryDate)) ||
                  (!l_oldServiceExpiryDate.equals(l_newServiceExpiryDate)) )
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"expiry dates change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }
                else
                {
                    // The Expiry Dates are being updated.
                    l_guiResponse = 
                        i_ccAccountService.updateExpiryDates(
                                       p_guiRequest,
                                       i_timeout,
                                       new PpasDate(l_oldAirtimeExpiryDate),
                                       new PpasDate(l_newAirtimeExpiryDate),
                                       new PpasDate(l_oldServiceExpiryDate),
                                       new PpasDate(l_newServiceExpiryDate));

                    // Check to see if the update failed and what field caused
                    // the failure.
                    if (!l_guiResponse.isSuccess())
                    {
                        // Get the key to see what field caused the update failure.
                        if (l_guiResponse.getKey().equals(
                                   GuiResponse.C_KEY_AIRTIME_PERIOD_TOO_LONG))
                        {
                            p_screenData.setFailedUpdateField("p_newAirtimeExpiryDate");
                        }
                        else if (l_guiResponse.getKey().equals(
                                   GuiResponse.C_KEY_SERVICE_PERIOD_TOO_LONG))
                        {
                            p_screenData.setFailedUpdateField("p_newServiceExpiryDate");
                        }
                        else
                        {
                            // We don't which of the two expiry dates fields
                            // caused a failure, or a general failure occured
                            // so set the airtime expiry date field as this is
                            // the first field that appears on the Account
                            // Details screen.
                            p_screenData.setFailedUpdateField("p_newAirtimeExpiryDate");
                        }
                    }
                }
                
                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }

            // Has the service class been changed?
            if (!l_oldServiceClass.equals(l_newServiceClass))
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"service class change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.changeServiceClass(
                                              p_guiRequest,
                                              i_timeout,
                                              new ServiceClass(Integer.parseInt(l_oldServiceClass)),
                                              new ServiceClass(Integer.parseInt(l_newServiceClass)));

                    // If the request to update the service class failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_newClass");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

                // Check whether service class change was 
                // successful and, if so, set a flag on the screendata object. We
                // will use to update the balance on the context bar since the 
                // subscriber's preferred currency may have changed.
                if (!p_screenData.hasUpdateError())
                {
                    p_screenData.setRefreshBalance();
                }
            }

            // Has the language been changed?
            if (!l_oldLanguage.equals(l_newLanguage))
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"language change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }            
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.changeLanguage(
                                           p_guiRequest,
                                           i_timeout,
                                           l_oldLanguage,
                                           l_newLanguage);                

                    // If the request to update the language failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_newLanguage");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }

            // Are we updating the agent or the sub-agent?
            if ((!l_oldAgent.equals(l_newAgent)) ||
                (!l_oldSubAgent.equals(l_newSubAgent)))
            {
                // Agent or sub-agent is being changed.

                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"update agent/sub-agent"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }
                else
                {
                    
                    
                    // Are we updating the agent?
                    if (l_oldAgent.equals(l_newAgent))
                    {
                        // Set new and old agents to null to indicate to the 
                        // update agent service method that agent is not to be
                        // updated
                        l_newAgent = null;
                        l_oldAgent = null;
                    }

                    // Are we updating the sub-agent?
                    if (l_oldSubAgent.equals(l_newSubAgent))
                    {
                        // Set new and old sub-agents to null to indicate to the 
                        // update agent service method that sub-agent is not to be
                        // updated
                        l_newSubAgent = "";
                        l_oldSubAgent = "";
                    }
                    //The service expect null for the blank SubAgent
                    if (l_oldSubAgent.length() == 0)
                    {
                        l_oldSubAgent = null;
                    }

                    if (l_newSubAgent.length() == 0)
                    {
                        l_newSubAgent = null;
                    }
                    
                    l_guiResponse = i_ccAccountService.updateAgent(p_guiRequest,
                                                                   i_timeout,
                                                                   l_oldAgent,
                                                                   l_newAgent,
                                                                   l_oldSubAgent,
                                                                   l_newSubAgent);

                    // If the request to update the agent or subagent failed,
                    // it is almost certainly a generic failure as the design
                    // is such that only valid values are displayed to the
                    // operator.  Therefore, if only one of the fields was being 
                    // updated set that field as being the fialure field, however,
                    // if both fields were being updated, just set the agent as
                    // being the failure field.
                    if (!l_guiResponse.isSuccess())
                    {
                        if (l_newAgent != null && l_newSubAgent == null)
                        {
                            p_screenData.setFailedUpdateField("p_newAgent");
                        }
                        else if (l_newAgent == null && l_newSubAgent != null)
                        {
                            p_screenData.setFailedUpdateField("p_newSubAgent");
                        }
                        else
                        {
                            p_screenData.setFailedUpdateField("p_newAgent");
                        }
                    }
                }
                 
                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }

            // Has an IVR unbar for the account been requested?
            if (l_ivrUnbar.equalsIgnoreCase("true"))
            {
                // Subscriber is to be IVR unbarred.

                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = 
                        new GuiResponse(p_guiRequest,
                                        p_guiRequest.getGuiSession().
                                        getSelectedLocale(),
                                        GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                        new Object[] {"unbar recharge"},
                                        GuiResponse.C_SEVERITY_WARNING);
                }
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.ivrUnbarAccount(p_guiRequest, i_timeout);

                    // If the request to ivr unbar failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_ivrUnbar");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }

            // Has a change of temp blocking status been requested?
            if (l_oldTempBlocked != l_newTempBlocked)
            {
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = 
                        new GuiResponse(p_guiRequest,
                                        p_guiRequest.getGuiSession().
                                        getSelectedLocale(),
                                        GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                        new Object[] {"change temporary blocking"},
                                        GuiResponse.C_SEVERITY_WARNING);
                }
                else
               {
                    l_guiResponse = 
                        i_ccAccountService.changeTempBlocking(p_guiRequest,
                                                              i_timeout,
                                                              l_newTempBlocked);

                    // If the request to change temp blocking failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_tempBlocking");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }

            // Has the USSD EoCN SS ID been changed?
            if (l_oldEocnId != null && !l_oldEocnId.equals(l_newEocnId))
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"USSD EoCN selection structure ID change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.updateUssdEocnId(
                                              p_guiRequest,
                                              i_timeout,
                                              l_newEocnId);

                    // If the request to update the USSD EoCN ID failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_newUssdEocnId");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }
            // Has the pin code been changed?
            if ( l_newPinCode != null && !l_newPinCode.equals(l_oldPinCode))
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"pin code change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }            
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.updatePinCode(
                                           p_guiRequest,
                                           i_timeout,
                                           l_newPinCode);                

                    // If the request to update the pin code failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_newPinCode");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }
            
            // Has the home region been changed?
            boolean l_changed = false;
            
            if ((l_oldHomeRegion == null && l_newHomeRegion != null) ||
                (l_oldHomeRegion != null && l_newHomeRegion == null))
            {
                l_changed = true;
            }
            else if ((l_oldHomeRegion == null) && (l_newHomeRegion == null))
            {
                l_changed = false;
            }
            else if (!l_oldHomeRegion.equals(l_newHomeRegion))
            {
                l_changed = true;
            }
                        
            if (l_changed)
            {
                // Check to see if a previous update error has not already occurred.
                if (p_screenData.hasUpdateError())
                {
                    // A previous update error has occured, therefore do not attempt
                    // this requested service.
                    l_guiResponse = new GuiResponse(p_guiRequest,
                                                    p_guiRequest.getGuiSession().
                                                    getSelectedLocale(),
                                                    GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                                    new Object[] {"home region change"},
                                                    GuiResponse.C_SEVERITY_WARNING);
                }            
                else
                {
                    l_guiResponse = 
                        i_ccAccountService.updateHomeRegion(
                                           p_guiRequest,
                                           i_timeout,
                                           l_oldHomeRegion,
                                           l_newHomeRegion);                

                    // If the request to update the language failed,
                    // set this field as the failure field on the screen data.
                    if (!l_guiResponse.isSuccess())
                    {
                        p_screenData.setFailedUpdateField("p_newHomeRegion");
                    }
                }

                // Store the update response on the screen data object.
                p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
            }
        }         
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 27090, this,
                           "Leaving " + C_METHOD_updateAccountDetails);
        }

        return  l_accessGranted;

    } // end private void updateAccountDetails(...)

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAccountDetails = "getAccountDetails";
    /** 
     * This method is called to read the account details for the currently
     * selected account from the database.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_screenData The <code>CcAccountDetailsScreenData</code> object
     *                     that will be used to return the account details
     *                     read to the account details screen.
     */
    private void getAccountDetails(GuiRequest                 p_guiRequest,
                                   CcAccountDetailsScreenData p_screenData)
    {
        CcAccountDataResponse         l_ccAccountDataResponse         = null;
        CcEventHistoryDataSetResponse l_ccEventHistorySetResponse     = null;
        CcCustomerDetailsDataResponse l_ccCustomerDetailsDataResponse = null;
        GuiSession                    l_guiSession                    = null;
        AccountData                   l_accountData                   = null;
        Market                        l_csoMarket;
        FeatureLicenceCache           l_featureLicenceCache;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28000, this,
                           "Entered " + C_METHOD_getAccountDetails);
        }

        // Get the current market from the session. This will have been set
        // up when the account was initially selected.

        l_csoMarket = ((GuiSession)p_guiRequest.getSession()).getCsoMarket();

        // Read the full account details. 
        l_ccAccountDataResponse = i_ccAccountService.getFullData(p_guiRequest, i_timeout, l_csoMarket);

        // Store the retrieval response in the screen data object.
        p_screenData.addRetrievalResponse(p_guiRequest, l_ccAccountDataResponse);

        l_guiSession = (GuiSession)p_guiRequest.getSession();

        if (l_ccAccountDataResponse.isSuccess())
        {
            // Extract the account data read and store it in the screen data object.

            l_accountData = l_ccAccountDataResponse.getAccountData();

            p_screenData.setAccountData (l_accountData);

            // Make sure that all of custId, Msisdn and Bt Cust Id are cached
            // on the session. These may already have been set up by the
            // Customer Selection service, but this guarantees that they will
            // be available on the session.
            setCachedCustIdentifiers(p_guiRequest,
                                     l_guiSession,
                                     l_accountData.getCustId(),
                                     l_accountData.getBtCustId(),
                                     l_accountData.getMsisdn());

            // check to see if the account's disconnected status is the same as
            // that currently held on the session. This is to cope with an account
            // that may have changed from status R (reconnect in progress) to A since
            // leaving the disconnection screen. The balance will need to be refreshed.
            if (l_accountData.isDisconnected() != l_guiSession.isDisconnected())
            {
                // set the screen data flag to refresh the balance
                p_screenData.setRefreshBalance();
            }

            l_guiSession.setCustStatus (l_accountData.getCustStatus());

            // explicitly set the disconnected status on the screen data, as this will not be
            // picked up from the session until the screen is exited and re-entered
            p_screenData.setSubIsDisconnected(l_guiSession.isDisconnected());
            
            // Cache the service class on the session.
            // IMPORTANT - this is only done for use by the CcMenuServlet, which
            // is always displayed immediately after the account details screen.
            // It must not be used for any other purpose.
            l_guiSession.setServiceClass(l_accountData.getActiveServiceClass());

            // Cache the preferred currency on the session.
            // IMPORTANT - this is only done for use by certain screens to default
            // the currency when multiple currencies are supported (such as in
            // adjustments and account refills).  As such, it is not a major error
            // if the defaulted currency is out of synch with the current preferred
            // currency, something that has a very small probability of occurring.
            // This must not be used for any other purpose unless similar
            // considerations apply.
            l_guiSession.setPreferredCurrency(l_accountData.getPreferredCurrency());
        }

        // If the account details were read successfully, then use the
        // Customer Details service to retrieve additional account information
        // required by the screen.
        if (!p_screenData.hasRetrievalError())
        {
            l_ccCustomerDetailsDataResponse = 
                i_ccCustomerDetailsService.getDetails(p_guiRequest, i_timeout);

            // Store the response on the screen data object.
            p_screenData.addRetrievalResponse(p_guiRequest, l_ccCustomerDetailsDataResponse);

            if (l_ccCustomerDetailsDataResponse.isSuccess())
            {
                // Store the customer details read in the screen data object.
                p_screenData.setCustomerDetailsData(
                    l_ccCustomerDetailsDataResponse.getCustomerDetailsData());
            }
        }
        
        // If no retrieval errors have occurred so far, then attempt to obtain
        // the event history for the account.
        if (!p_screenData.hasRetrievalError())
        {
            l_ccEventHistorySetResponse = 
                i_ccEventHistoryService.getHistory(p_guiRequest,
                                                   PpasEventHistoryService.C_FLAG_INCLUDE_ALL,
                                                   i_timeout);

            // Store the response on the screen data object.
            p_screenData.addRetrievalResponse (p_guiRequest, l_ccEventHistorySetResponse);

            if (l_ccEventHistorySetResponse.isSuccess())
            {
                // Set up the event history on the screen data object.
                p_screenData.setEventHistoryDataSet(l_ccEventHistorySetResponse.getDataSet());
            }
            
            // Set feature licence flags.
            try
            {
                l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
                p_screenData.setSubscriberPinUpdateFeatureLicence(
                                     l_featureLicenceCache.isLicensed(
                                           FeatureLicence.C_FEATURE_CODE_IVR_CUST_PIN));
                
                p_screenData.setRegionalVouchersFeatureLicence(
                                     l_featureLicenceCache.isLicensed(
                                           FeatureLicence.C_FEATURE_CODE_REGION_VCH));
            }
            catch (PpasServiceException p_ppasServiceE)
            {
                i_logger.logMessage(p_ppasServiceE);
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 28090, this,
                           "Leaving " + C_METHOD_getAccountDetails);
        }
    } // end private void getAccountDetails(...)

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getBusinessConfigData = "getBusinessConfigData";
    /**
     * Obtains the business configuration data and makes it available in the
     * given account 'screen data' object.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_screenData The account screen data object that the 
     *                     business configuration data will be set up in.
     */
    private void getBusinessConfigData (GuiRequest                 p_guiRequest,
                                        CcAccountDetailsScreenData p_screenData)
    {
        ValaValidLanguageDataSet       l_langSet = null;
        RegiRegionDataSet              l_regionDataSet = null;
        SrvaAgentMstrDataSet           l_agentsSet = null;
        MiscCodeDataSet                l_servClassesSet = null;
        Market                         l_csoMarket;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 29000, this,
                           "Entered " + C_METHOD_getBusinessConfigData);
        }

        // Get the current market from the session. This will have been set
        // up when the account was initially selected.

        l_csoMarket = ((GuiSession)p_guiRequest.getSession()).getCsoMarket();

        // Set the name of the subscriber's market in the screen data object
        // so that it can be displayed on the context bar
        p_screenData.setMarketDisplayName (l_csoMarket.getDisplayName());

        // Make sure earlier retrieval error has not already occurred.
        if (!p_screenData.hasRetrievalError())
        {
            // Set up available languages ...
            l_langSet = i_configService.getAvailableLanguages();

            // ... and store them on the screen data object
            p_screenData.setLanguageDataSet (l_langSet);
            
            // Set up available regions ...
            l_regionDataSet = i_configService.getAvailableRegions();
            
            // ... and store them on the screen data object
            p_screenData.setRegions(l_regionDataSet);

            // Set up available agents ...
            l_agentsSet = i_configService.getAvailableAgents (l_csoMarket);

            // ... and store them on the screen data object
            p_screenData.setAgentsDataSet (l_agentsSet);

            // Set up available service classes ...
            l_servClassesSet = i_configService.getAvailableServiceClasses(l_csoMarket);

            // ... and store them on the screen data object
            p_screenData.setServiceClasses (l_servClassesSet);

            if (i_configService.getCompanyConfigData().getExpiryAfterDateInd())
            {
                p_screenData.setExpiryAfterDate();
            }
            
            p_screenData.setConfPinCodeLength(i_configService.getCompanyConfigData().getIvrPinCodeLength());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 29090, this,
                           "Leaving " + C_METHOD_getBusinessConfigData);
        }
    }
}
