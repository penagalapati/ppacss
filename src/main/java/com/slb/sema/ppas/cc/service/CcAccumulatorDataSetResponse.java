////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccumulatorDataSetResponse.java
//      DATE            :       9-Oct-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1597/6736
//                              PRD_PPAK00_GEN_CA_396
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       CcXResponse class for AccumulatorDataSet objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.AccumulatorDataSet;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.common.web.support.WebDebug;

/**
 * CcXResponse class for AccumulatorDataSet objects. 
 */
public class CcAccumulatorDataSetResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAccumulatorDataSetResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------

    /** The AccumulatorDataSet object wrapped by this object. */
    private AccumulatorDataSet  i_accumulatorDataSet;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /**
     * Receives a AccumulatorData object and the additional necessary objects to 
     * create a response message indicating the status of a call to a
     * AccumulatorService method.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey         Response message key.
     * @param p_messageParams      Any parameters to be substituted into response
     *                             message.
     * @param p_messageSeverity    Severity of response.
     * @param p_accumulatorDataSet Set of accumulator Data.
     */
    public CcAccumulatorDataSetResponse( 
                                     GuiRequest          p_guiRequest,
                                     Locale              p_messageLocale,
                                     String              p_messageKey,
                                     Object[]            p_messageParams,
                                     int                 p_messageSeverity,
                                     AccumulatorDataSet  p_accumulatorDataSet )
    {
        super( p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_accumulatorDataSet = p_accumulatorDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /** Returns the accumulator information.
     * @return Set of accumulator data.
     */
    public AccumulatorDataSet getAccumulatorDataSet()
    {
        return( i_accumulatorDataSet );
    }

} // end of class CcAccumulatorDataSetResponse

