////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcCustSelectionCriteriaScreenData.java
//      DATE            :       12-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_27
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Gui Screen Data object for the Customer
//                              Selection Criteria screen. This object only 
//                              contains the available markets from the 
//                              business  configuration cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.screendata;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.MarketSet;
import com.slb.sema.ppas.gui.screendata.GuiScreenData;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Gui Screen Data object for the Customer Selection screen; and in particular,
 * the selection criteria form witin that screen.
 */
public class CcCustSelectionCriteriaScreenData extends GuiScreenData
{

    //--------------------------------------------------------------------------
    // Class constants.
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Vector of CsoMarketData objects defining the available markets for
     *  the current CSO.
     */
    private Vector     i_availableMarkets = null;

    /** Radio button default (Most Recent Subscriber or All Associated Subscribers). */ 
    private boolean i_defaultAllAssocSubscribers = false;

    /** Search by names enabled property. */ 
    private boolean i_searchByNames = false;

    /** Search by SSN enabled property. */ 
    private boolean i_searchBySSN = false;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** Construct a Customer Selection Criteria Screen Data object.
     * 
     * @param p_guiContext General context for this session.
     * @param p_guiRequest Details of the request being processed.
     */
    public CcCustSelectionCriteriaScreenData(
        GuiContext p_guiContext,
        GuiRequest p_guiRequest)
    {
        super (p_guiContext, p_guiRequest);
    }

    //--------------------------------------------------------------------------
    // public methods.
    //--------------------------------------------------------------------------


    /** Set up data set of available markets for the current CSO.
     * @param p_marketDataSet Data set of available markets for the current CSO.
     */
    public void setMarkets (MarketSet p_marketDataSet)
    {
        i_availableMarkets = p_marketDataSet.getMarkets();
    }

    /** Returns Vector of CsoMarketData objects defining the available markets
     * for the current CSO.
     * @return Vector of CsoMarketData objects defining the available markets
     *         for the current CSO.
     */
    public Vector getMarkets()
    {
        return (i_availableMarkets);

    } // end method getLanguages

    /** Determines whether this screen data object has been populated.
     *  @return True if this screen data object has been populated. Otherwise,
     *          false.
     */
    public boolean isPopulated()
    {
        return ((i_availableMarkets == null) ? false : true);
    }
    
    /**
     * Retrieves radio button default (Most Recent Subscriber or All Associated Subscribers). 
     * @return Flag set to true if All Associated Subscribers is the default.
     */
    public boolean getDefaultAllAssocSubscribers()
    {
        return i_defaultAllAssocSubscribers;
    }

    /**
     * Sets radio button default to Most Recent Subscriber or All Associated Subscribers. 
     * @param p_defaultAllAssocSubscribers  Set to true if All Associated Subscribers is to be
     *                                      selected by default.
     */
    public void setDefaultAllAssocSubscribers(Boolean p_defaultAllAssocSubscribers)
    {
        if (p_defaultAllAssocSubscribers != null)
        {
            i_defaultAllAssocSubscribers = p_defaultAllAssocSubscribers.booleanValue();
        }
    }
    
    /**
     * Retrieves value of property that determines whether search by names is enabled or not.
     * @return Flag set to true if search by names is enabled.
     */
    public boolean getSearchByNamesFlag()
    {
        return i_searchByNames;
    }

    /**
     * Sets  value of property that determines whether search by names is enabled or not.
     * @param p_searchByNamesFlag  Set to true if search by names is enabled.
     */
    public void setSearchByNamesFlag(Boolean p_searchByNamesFlag)
    {
        if (p_searchByNamesFlag != null)
        {
            i_searchByNames = p_searchByNamesFlag.booleanValue();
        }
    }

    /**
     * Retrieves value of property that determines whether search by social security number (SSN) is enabled 
     * or not.
     * @return Flag set to true if search by SSN is enabled.
     */
    public boolean getSearchBySSNFlag()
    {
        return i_searchBySSN;
    }

    /**
     * Sets  value of property that determines whether search by social security number (SSN) is enabled
     * or not.
     * @param p_searchBySSNFlag  Set to true if search by SSN is enabled.
     */
    public void setSearchBySSNFlag(Boolean p_searchBySSNFlag)
    {
        if (p_searchBySSNFlag != null)
        {
            i_searchBySSN = p_searchBySSNFlag.booleanValue();
        }
    }

} // end public class CcCustSelectionCriteriaScreenData
