////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcEventHistoryDataSetResponse.java
//      DATE            :       19-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PpaLon#1232/5077
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Response returned by the Event History
//                              GUI service containing details of event that
//                              have occurred on a particular customer's
//                              account.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.EventHistoryDataSet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * Response containing details of events that have occurred on a customer's
 * account if these details were successfully obtained by the Event History
 * service. Otherwise, the key in the GuiResponse
 * super class defines the reason for the failure of the service.
 */
public class CcEventHistoryDataSetResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcEventHistoryDataSetResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Data Set containing details of events that occurred on a customer
     *  account.
     */
    private EventHistoryDataSet i_eventHistoryDataSet;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /**
     * Construct a CcEventHistoryDataSetResponse using the default locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_eventHistoryDataSet   Set of historical events.
     */
    public CcEventHistoryDataSetResponse(
        GuiRequest                p_guiRequest,
        String                    p_messageKey,
        Object []                 p_messageParams,
        int                       p_messageSeverity,
        EventHistoryDataSet       p_eventHistoryDataSet)
    {
        this (p_guiRequest,
              null,
              p_messageKey,
              p_messageParams,
              p_messageSeverity,
              p_eventHistoryDataSet);
    }

    /**
     * Construct a CcEventHistoryDataSetResponse using the given locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_eventHistoryDataSet   Set of historical events.
     */
    public CcEventHistoryDataSetResponse(
        GuiRequest               p_guiRequest,
        Locale                   p_messageLocale,
        String                   p_messageKey,
        Object[]                 p_messageParams,
        int                      p_messageSeverity,
        EventHistoryDataSet      p_eventHistoryDataSet)
    {
        super (p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_eventHistoryDataSet = p_eventHistoryDataSet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // end constructor


    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the EventHistoryDataSet instance contained within
     * this response object.
     * @return Set of historical events.
     */
    public EventHistoryDataSet getDataSet()
    {
        return (i_eventHistoryDataSet);
    }

} // end class CcEventHistoryDataSetresponse
