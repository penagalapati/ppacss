////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccumulatorServlet.Java
//      DATE            :       9-Oct-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1597/6736
//                              PRD_PPAK00_GEN_CA_396
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Accumulator details screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import com.slb.sema.ppas.cc.screendata.CcAccumulatorScreenData;
import com.slb.sema.ppas.cc.service.CcAccumulatorDataSetResponse;
import com.slb.sema.ppas.cc.service.CcAccumulatorService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * Servlet to handle requests from the Accumulator details Screen.
 */
public class CcAccumulatorServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------

    /** Name of class. Set to {@value} */
    private static final String C_CLASS_NAME = "CcAccumulatorServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Accumulator Service to be used by this servlet. */
    private CcAccumulatorService i_ccAccumulatorService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** 
     * Constructor, only calls the superclass constructor.
     */
    public CcAccumulatorServlet()
    {
        super();

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 
                           10000, 
                           this,
                           "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 
                           10010, 
                           this,
                           "Constructed " + C_CLASS_NAME );
        }

        return;
    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** 
     * Initialisation method.
     */
    public void doInit()
    {
        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 
                           10030, 
                           this,
                           "Entered doInit().");
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 
                           10040, 
                           this,
                           "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_doService = "doService";
    /** 
     * Service method to control requests and responses for the accumulator screen services.
     * @param p_request      HTTP request.
     * @param p_response     HTTP response.
     * @param p_httpSession  Current HttpSession.
     * @param p_guiRequest   GuiRequest object.
     * @return forwarding URL.
     */
    protected String doService(HttpServletRequest     p_request,
                               HttpServletResponse    p_response,
                               HttpSession            p_httpSession,
                               GuiRequest             p_guiRequest)
    {
        String                    l_command               = null;
        String                    l_forwardUrl            = null;
        CcAccumulatorScreenData   l_accumulatorScreenData = null;
        GuiResponse               l_guiResponse           = null;
        boolean                   l_accessGranted         = false; 
        GopaGuiOperatorAccessData l_gopaData              = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10050, 
                           this,
                           "ENTERED " + C_METHOD_doService);
        }

        if (i_ccAccumulatorService == null)
        {
            i_ccAccumulatorService = new CcAccumulatorService( p_guiRequest,
                                                               i_logger,
                                                               i_guiContext );
        }

        l_accumulatorScreenData = new CcAccumulatorScreenData( i_guiContext,  p_guiRequest);

        try
        {
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,          // Mandatory
                                      "",            // default
                                      new String [] {"GET_SCREEN_DATA",
                                                     "POST_ADJUSTMENT",
                                                     "CLEAR_ACCUMULATORS"});

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasAccumulatorsScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("POST_ADJUSTMENT")) && l_gopaData.updateAccumIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("CLEAR_ACCUMULATORS")) && l_gopaData.updateAccumIsPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_accumulatorScreenData.addRetrievalResponse(p_guiRequest,
                                                             l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", 
                                        l_accumulatorScreenData);
            }
            
            else
            {            
                if (l_command.equals("POST_ADJUSTMENT"))
                {
                    setIsUpdateRequest(true);

                    updateAccumulator(p_guiRequest,
                                      p_request,
                                      l_accumulatorScreenData);
                }
                else if (l_command.equals("CLEAR_ACCUMULATORS"))
                {
                    setIsUpdateRequest(true);

                    clearAccumulators(p_guiRequest,
                                      p_request,
                                      l_accumulatorScreenData);
                }

                getAccumulatorBalances( p_guiRequest, l_accumulatorScreenData );
            }
        } 
        catch (PpasServletException l_ppasSE)
        {
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_accumulatorScreenData.addRetrievalResponse( p_guiRequest, l_guiResponse);
        } 

        if (l_accessGranted)
        {
            if (l_accumulatorScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                l_forwardUrl = "/jsp/cc/ccaccumulatordetails.jsp";
            }

            p_request.setAttribute( "p_guiScreenData", l_accumulatorScreenData );
        }    

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10060, 
                           this,
                           "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_getAccumulatorBalances = "getAccumulatorBalances";
    /**
     * Method to retrieve the accumulator details for display on the 
     * accumulator screen.
     * @param p_guiRequest  GuiRequest object.
     * @param p_screenData  Data used to populate Accumulator screen.
     * @throws PpasServletException If an error occurs.
     */
    private void getAccumulatorBalances(GuiRequest               p_guiRequest,
                                        CcAccumulatorScreenData  p_screenData )
        throws PpasServletException
    {
        CcAccumulatorDataSetResponse  l_ccAccumulatorDataSetResponse = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10070, 
                           this,
                           "ENTERED " + C_METHOD_getAccumulatorBalances);
        }
        
        l_ccAccumulatorDataSetResponse = i_ccAccumulatorService.getAccumulatorBalances(p_guiRequest,
                                                                                       i_timeout );

        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccAccumulatorDataSetResponse);

        if (l_ccAccumulatorDataSetResponse.isSuccess())
        {
            p_screenData.setAccumulatorDataSet(l_ccAccumulatorDataSetResponse.getAccumulatorDataSet());
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10080, 
                           this,
                           "LEAVING " + C_METHOD_getAccumulatorBalances);
        }
        
        return;
    }

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_updateAccumulator = "updateAccumulator";
    /**
     * Posts an adjustment to the requested accumulator.
     * @param p_guiRequest  GuiRequest object. 
     * @param p_request     HTTP request.
     * @param p_screenData  Data used to populate Accumulator screen.
     */
    private void updateAccumulator(GuiRequest              p_guiRequest,
                                   HttpServletRequest      p_request,
                                   CcAccumulatorScreenData p_screenData)
    {
        GuiResponse      l_guiResponse = null;
        String           l_accumulatorId = null;
        String           l_accumulatorStartDate = null;
        String           l_oldAccumStartDate = null;
        String           l_adjAmountString = null;
        int              l_adjustmentAmount = 0;
        PpasDateTime     l_currentTime = null;
        PpasDate         l_startDateToSend = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10090, 
                           this,
                           "Entered " + C_METHOD_updateAccumulator);
        }
        
        try
        {
            l_accumulatorId = getValidParam(p_guiRequest,
                                            0,
                                            p_request,
                                            "p_accumId",
                                            true,
                                            "",
                                            new String[] {"1", "2", "3", "4", "5"});

            l_accumulatorStartDate = getValidParam(p_guiRequest,
                                                   PpasServlet.C_FLAG_ALLOW_BLANK,
                                                   p_request,
                                                   "p_startDate",
                                                   false,
                                                   "",
                                                   PpasServlet.C_TYPE_DATE);

            l_oldAccumStartDate = getValidParam(p_guiRequest,
                                                0,
                                                p_request,
                                                "p_oldStartDate",
                                                false,
                                                "",
                                                PpasServlet.C_TYPE_DATE);
                                                
            l_adjAmountString = getValidParam(p_guiRequest,
                                              PpasServlet.C_FLAG_STRIP_PLUS_SIGN | 
                                              PpasServlet.C_FLAG_ALLOW_BLANK,
                                              p_request,
                                              "p_amount",
                                              false,
                                              "0",
                                              PpasServlet.C_TYPE_SIGNED_NUMERIC);
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest,
                               C_CLASS_NAME, 
                               10100, 
                               this,
                               "Exception parsing parameters: " + l_pSE);
            }
            
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        } 
    
        if (p_screenData.hasUpdateError())
        {
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                            new Object[] {"update accumulator"},
                                            GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            if (l_accumulatorStartDate.equals(l_oldAccumStartDate))
            {
                l_startDateToSend = null;
            }
            else
            {
                l_startDateToSend = new PpasDate(l_accumulatorStartDate);
            }

            if (l_adjAmountString.equals(""))
            {
                l_adjustmentAmount = 0;
            }
            else
            {
                l_adjustmentAmount = Integer.parseInt(l_adjAmountString);
            }

            l_guiResponse = i_ccAccumulatorService.updateAccumulator(p_guiRequest,
                                                                     i_timeout,
                                                                     Integer.parseInt(l_accumulatorId),
                                                                     l_adjustmentAmount,
                                                                     l_startDateToSend);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText("Failed accumulator update at " + l_currentTime);
        }
        else
        {
            p_screenData.setInfoText("Successful accumulator update at " + l_currentTime);
        }    

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10120, 
                           this,
                           "Leaving " + C_METHOD_updateAccumulator);
        }
    }

    /** Name of method. Set to {@value} */
    private static final String C_METHOD_clearAccumulators = "clearAccumulators";
    /**
     * Clears one or more accumulator balance.
     * @param p_guiRequest  GuiRequest object.
     * @param p_request     HTTP request.
     * @param p_screenData  Data used to populate Accumulator screen.
     */
    private void clearAccumulators(GuiRequest              p_guiRequest,
                                   HttpServletRequest      p_request,
                                   CcAccumulatorScreenData p_screenData)
    {
        GuiResponse      l_guiResponse = null;
        PpasDateTime     l_currentTime = null;
        String           l_checkBox = null;
        ArrayList        l_accumulatorIds = new ArrayList();
        int[]            l_accumIdsArray = null; 
        String           l_httpParam = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10090, 
                           this,
                           "Entered " + C_METHOD_clearAccumulators);
        }
        
        try
        {
            for (int l_accId = 1; l_accId < 6; l_accId++)
            {
                l_httpParam = "CB" + l_accId;
                
                l_checkBox = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           l_httpParam,
                                           false,
                                           "",
                                           PpasServlet.C_TYPE_ANY);
                                           
                if (l_checkBox.equalsIgnoreCase("on"))
                {
                    l_accumulatorIds.add(new Integer(l_accId));
                }
            }

            // Convert ArrayList of Integer objects into the more compact array of int as
            // this is passed over RMI
            l_accumIdsArray = new int[l_accumulatorIds.size()];
            for (int l_index = 0; l_index < l_accumulatorIds.size(); l_index++)
            {
                l_accumIdsArray[l_index] = ((Integer)l_accumulatorIds.get(l_index)).intValue();
            }
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_VLOW,
                               WebDebug.C_APP_SERVLET,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest,
                               C_CLASS_NAME, 
                               10130, 
                               this,
                               "Exception parsing parameters: " + l_pSE);
            }
            
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        } 
    
        if (p_screenData.hasUpdateError())
        {
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                            new Object[] {"clear accumulator balance"},
                                            GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            l_guiResponse = i_ccAccumulatorService.clearAccumulators(p_guiRequest,
                                                                     i_timeout,
                                                                     l_accumIdsArray);

            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText("Failed accumulator balance clearance at " + l_currentTime);
        }
        else
        {
            p_screenData.setInfoText("Successful accumulator balance clearance at " + l_currentTime);
        }    

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           10140, 
                           this,
                           "Leaving " + C_METHOD_clearAccumulators);
        }
    }
}