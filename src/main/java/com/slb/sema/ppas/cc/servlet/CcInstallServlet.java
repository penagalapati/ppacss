////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcInstallServlet.Java
//      DATE            :       19-Feb-2002
//      AUTHOR          :       Mike Hickman
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       GUI cust_care subscriber installation servlet.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
//07/10/03  | R Isaacs   | Privilege checks added          | CR#60/571
//----------+------------+---------------------------------+--------------------
// 04-Dec-05| M Erskine  | Licensed feature to enable      | PpacLon#1882/7556
//          |            | operator to pass in a new flag  |PRD_ASCS00_GEN_CA_65
//          |            | specifying that install request |
//          |            | can use a quarantined MSISDN    |
//----------+------------+---------------------------------+--------------------
// 24-Apr-07| S James    | Add Single Step Install changes | PpacLon#3072/11370
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcInstallScreenData;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcBasicAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcInstallService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.MarketSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.PpasServletMsg;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.is.isapi.PpasInstallService;

/** GUI cust_care subscriber installation servlet. */
public class CcInstallServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcInstallServlet";
    
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** The Install service to be used by this servlet. */
    private CcInstallService i_ccInstallService = null;

    /** The Account service to be used by this servlet. */
    private CcAccountService i_ccAccountService = null;

    /** Primary delay between the install request returning to the servlet,
     *  and the attempt to get the basic account data of the subscriber that has just been installed.
     *  If this data indicates that the installation has been fully completed,
     *  then a dialog pop-up will be shown.
     */         
    private long             i_primaryDelayPeriod;    
         
    /** Secondary delay between the install request returning to the servlet,
     *  and the attempt to get the basic account data of the subscriber that has just been installed.
     *  If after the primary delay the data indicates that the installation has not been fully completed,
     *  then the servlet will wait for the 2nd configurable delay period.
     *  It will then get the basic account data again.
     *  If this data indicates that the installation has been fully completed,
     *  then a dialog pop-up will be shown.
     *  If this new data indicates that the installation has not been fully completed,
     *  then an information pop-up will be displayed.
     */    
    private long             i_secondaryDelayPeriod;    

    /** Configuration setting to indicate the Installation Type */
    private String           i_installationType;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */
    public CcInstallServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;
    }

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /**
     * Currently does nothing but will construct a CcInstallService in
     * in the fullness of time.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 30000, this,
                "Entered " + C_METHOD_doInit);
        }

        // Should probably be constructing CcInstallService here...
        // unfortunately no GuiRequest is available.


        // Get the value for the primary delay period for the attempt
        // to get the basic account data for the subscriber beeing installed.
        i_primaryDelayPeriod = 
            ((Long)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.accountDataRetrievalPrimaryDelay")).     
            longValue();           

        // Get the value for the secondary delay period for the attempt
        // to get the basic account data for the subscriber beeing installed.         
        i_secondaryDelayPeriod = 
            ((Long)i_guiContext.getAttribute(
                    "com.slb.sema.ppas.gui.support.GuiContext.accountDataRetrievalSecondaryDelay")).
            longValue();

        // Get the configuration to force single step installs
        PpasProperties l_prop = i_guiContext.getProperties();

        i_installationType = l_prop.getTrimmedProperty(
                     "com.slb.sema.ppas.gui.cc.servlet.CcInstallServlet.installationType",
                     PpasInstallService.C_INSTALL_TYPE_STANDARD);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 30010, this,
                "Leaving " + C_METHOD_doInit);
        }
        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /**
     *  Service method to control requests and responses to and from
     *  the GUI screen for the install service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String               l_command             = null;
        String               l_forwardUrl          = null;
        GuiResponse          l_guiResponse         = null;
        CcInstallScreenData  l_screenData          = null;
        GuiSession           l_guiSession          = null;
        boolean              l_accessGranted       = true;  // CR#60/571
        GopaGuiOperatorAccessData l_gopaData       = null;  // CR#60/571


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 40000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcInstallService if it does not already exist ...
        if (i_ccInstallService == null)
        {
            i_ccInstallService = new CcInstallService(p_guiRequest,
                                                      0,
                                                      i_logger,
                                                      i_guiContext);
        }

        // Construct CcAccountService if it does not already exist ...
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest,
                                                      0,
                                                      i_logger,
                                                      i_guiContext);
        }

        // Set up the Default forward URL.
        l_forwardUrl = "/jsp/cc/ccinstall.jsp";

        // Create screen data object.
        l_screenData = new CcInstallScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "INSTALL"});

            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_MODERATE,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    C_CLASS_NAME, 40010, this,
                    "Got command, command = [" + l_command + "]");
            }

            // CR#60/571 Start

            // Determine if the CSO has privilege to access the requested screen or function

            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")|| l_command.equals("INSTALL")) &&
                 l_gopaData.hasNewSubsScreenAccess())
            {
                l_accessGranted = true;
            }
            else 
            {
                l_accessGranted = false;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(), //CR#60/571
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_screenData.addRetrievalResponse(p_guiRequest,
                                                  l_guiResponse);

                l_forwardUrl = new String("/jsp/cc/ccaccesserror.jsp");

                p_request.setAttribute("p_guiScreenData",
                                        l_screenData);
            }
            else
            {
                // it's necessary to set the cust id to null on the GuiRequest because
                // IS will look at the cust id before the msisdn...
                p_guiRequest.setCustId(null);
                
                if (l_command.equals("INSTALL"))
                {
                    setIsUpdateRequest(true);

                    install(p_guiRequest,
                            p_request,
                            l_screenData);                    
                }
                // Cmdf Cache for the Service Class Promotion Plan Defaults.
                l_screenData.setCmdfCache(i_configService.getAvailableCustMastDefaults());
                getData(p_guiRequest,
                        l_screenData);
            }
            // CR#60/571 End
        }
        catch (PpasServletException l_servletE)
        {
            // Handle the case where only part of the market information has
            // been supplied for an installation.
            if ((l_servletE.getMsgKey()).equals(
                               PpasServletMsg.C_KEY_INCOMPLETE_MARKET_DETAILS))
            {
                l_guiResponse = new GuiResponse
                                 (p_guiRequest,
                                  ((PpasSession)p_guiRequest.getSession()).
                                    getSelectedLocale(),  // PpacLon#1/17
                                  GuiResponse.C_KEY_INCOMPLETE_MARKET_DETAILS,
                                  l_servletE.getMsgParams(),
                                  GuiResponse.C_SEVERITY_FAILURE);
            }
            else
            {
                // Handle the general case of an invalid or missing parameter
                // in the incoming request
                l_guiResponse = handleInvalidParam(p_guiRequest, l_servletE);
            }
            // Set up the error page as the forward URL and add the message to the
            // screendata object.
            l_forwardUrl = "/jsp/cc/ccerror.jsp";

            l_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);

            // Append the screen data object to the Http request.
            p_request.setAttribute("p_guiScreenData", l_screenData);
        }
        finally
        {
            // Always clear down the market, which was temporarily stored in
            // the session for the attempted installation, after the atempted
            // installation has been completed (regardless of whether it
            // succeed or not), since we're not on a session on the installed
            // subscriber as yet.
            l_guiSession = (GuiSession)p_guiRequest.getSession();
            l_guiSession.setCsoMarket (null);
        }

        // CR#60/571 Start

        if (l_accessGranted)
        {
            // Append the screen data object to the Http request.
            p_request.setAttribute("p_guiScreenData", l_screenData);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 40020, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        // CR#60/571 End

        return(l_forwardUrl);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_install = "install";
    /**
     * Strips out the relevant params, calls the install service, and
     * populates the screendata object.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data for the Install screen, on
     *                     which to put the response to the request.
     * @throws PpasServletException If an error occurs.
     */
    private void install(GuiRequest          p_guiRequest,
                         HttpServletRequest  p_request,
                         CcInstallScreenData p_screenData)
        throws PpasServletException
    {
        GuiResponse          l_guiResponse         = null;
        PpasServletException l_pSE                 = null;
        Msisdn               l_msisdn              = null;
        String               l_subType             = null;
        String               l_marketStr           = null;
        String               l_class               = "";
        Msisdn               l_masterMsisdn        = null;
        Msisdn               l_oldmasterMsisdn     = null;
        String               l_promoPlan           = "";
        String               l_agent               = null;
        String               l_serviceArea         = null;
        String               l_serviceLocation     = null;
        PpasDateTime         l_currentTime         = null;

        GuiSession           l_guiSession;
        MarketSet            l_marketSet;
        Market               l_subscriberMarket;
        Market               l_market              = null;
        String               l_accountGroupIdStr   = null;
        String               l_serviceOfferingsStr  = null;
        AccountGroupId       l_accountGroupId      = null;
        ServiceOfferings     l_serviceOfferings    = null;

        String                     l_ussdEocnIdStr = null;
        EndOfCallNotificationId    l_ussdEocnId    = null;
        String                     l_overrideMsisdnQuarantinePeriodStr = null;
        boolean                    l_overrideMsisdnQuarantinePeriod = false;

        String                     l_singleStepInstallStr = null;
        boolean                    l_singleStepInstall    = false;

        CcBasicAccountDataResponse  l_ccBasicAccountDataResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50000, this,
                "ENTERED " + C_METHOD_install);
        }

        // Get the srva part of the market from the request
        l_serviceArea = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_srva",
                                      false,
                                      "",
                                      PpasServlet.C_TYPE_ANY,
                                      Market.C_MAX_NUM_OF_DIGITS_IN_SRVA,
                                      C_OPERATOR_LESS_THAN_OR_EQUAL);
        l_serviceArea = l_serviceArea.trim();

        // Get the sloc part of the market from the request
        l_serviceLocation = getValidParam(p_guiRequest,
                                          0,
                                          p_request,
                                          "p_sloc",
                                          false,
                                          "",
                                          PpasServlet.C_TYPE_ANY,
                                          Market.C_MAX_NUM_OF_DIGITS_IN_SRVA,
                                          C_OPERATOR_LESS_THAN_OR_EQUAL);
        l_serviceLocation = l_serviceLocation.trim();

        // Store the market that we're attempting to install the subscriber
        // on temporarily in the GuiSession (just until the installation either
        // succeeds or fails). This is required so that, if the attempted
        // installation fails, any MSISDNs in the failure response can be
        // correctly formatted for the market that we were trying to install
        // under
        l_guiSession = (GuiSession)p_guiRequest.getSession();
        l_marketSet  = ((PpasSession)l_guiSession).getCsoMarkets();  // PpacLon#1/17

        // Build Market
        l_market = new Market(
                        p_guiRequest,
                        l_serviceArea,
                        l_serviceLocation,
                        ""
                        );
        l_subscriberMarket = l_marketSet.getMarket(l_market);
        l_guiSession.setCsoMarket (l_subscriberMarket);

        // Validation to ensure that either full market details have
        // been entered, or none at all.
        if ((l_serviceArea.equals("")) &&
            (l_serviceLocation.equals("")))
        {
            l_marketStr = Market.C_UNDEFINED_MARKET_STR; // "00000000"
            l_market = new Market(p_guiRequest);
        }
        else if ((!l_serviceArea.equals("")) &&
                 (!l_serviceLocation.equals("")))
        {
            // Format Service Area and Location (left-pad with zeroes)
            // to a length of 4 each, then concatenate.
            l_marketStr = Market.formatSrva(l_serviceArea)
                        + Market.formatSloc(l_serviceLocation);
            l_market = new Market(
                            p_guiRequest,
                            l_serviceArea,
                            l_serviceLocation,
                            ""
                            );
        }
        else
        {
            // One of srva and sloc has been specified
            // but not the other one - this should never happen but it's still
            // safer to handle it.
            l_pSE = new PpasServletException
                                  (C_CLASS_NAME,
                                   C_METHOD_doService,
                                   50010,
                                   this,
                                   p_guiRequest,
                                   0,
                                   ServletKey.get().incompleteMarketDetails());

            i_logger.logMessage(l_pSE);

            throw l_pSE;
        }

        // Stick the market details onto the screen data object. We'll need them if
        // the install worked.
        p_screenData.setMarket(l_market);

        // Get and validate that there is an msisdn in the request...
        l_msisdn = getValidMsisdn(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_msisdn",
                                  true,
                                  "",
                                  i_guiContext.getMsisdnFormatInput());

        // Put the msisdn obtained into the GUI request.
        p_guiRequest.setMsisdn(l_msisdn);

        // Stick the MSISDN onto the screen data object. We'll need it if
        // the install worked.
        p_screenData.setMsisdn(l_msisdn);

        // Get Account group from the request
        l_accountGroupIdStr = getValidParam(p_guiRequest,
                                            PpasServlet.C_FLAG_ALLOW_BLANK,
                                            p_request,
                                            "p_accountGroup",
                                            false,
                                            "",
                                            PpasServlet.C_TYPE_NUMERIC);
                                       
        // Get Service Offering from the request
        l_serviceOfferingsStr = getValidParam(p_guiRequest,
                                              PpasServlet.C_FLAG_ALLOW_BLANK,
                                              p_request,
                                              "p_serviceOffering",
                                              false,
                                              "",
                                              PpasServlet.C_TYPE_NUMERIC);

        // Convert String values.
        l_accountGroupId = null;
        try
        {
            if (l_accountGroupIdStr != null && !l_accountGroupIdStr.trim().equals(""))
            {
                l_accountGroupId = new AccountGroupId(l_accountGroupIdStr);
            }
        }
        catch (PpasServiceFailedException l_ppasSvcFldExc)
        {
            // Generate a new servlet exception
            l_pSE = new PpasServletException(C_CLASS_NAME,
                                             C_METHOD_install,
                                             52000,
                                             this,
                                             p_guiRequest,
                                             0,
                                             ServletKey.get().invalidAccountGroupId(l_accountGroupIdStr),
                                             l_ppasSvcFldExc);
            i_logger.logMessage(l_pSE);
            throw (l_pSE);
        }
                    
        l_serviceOfferings = null;
        try
        {           
            if (l_serviceOfferingsStr != null && !l_serviceOfferingsStr.trim().equals(""))
            {
                l_serviceOfferings = new ServiceOfferings(l_serviceOfferingsStr);
            }
        }
        catch (PpasServiceFailedException l_ppasSvcFldExc)
        {
            // Generate a new servlet exception
            l_pSE = new PpasServletException(C_CLASS_NAME,
                                             C_METHOD_doService,
                                             52100,
                                             this,
                                             p_guiRequest,
                                             0,
                                             ServletKey.get().invalidNewServiceOfferings(
                                                                   l_serviceOfferingsStr),
                                             l_ppasSvcFldExc);

            i_logger.logMessage(l_pSE);
            throw (l_pSE);
        }
        
        // Get the override MSISDN quarantine period flag from the request
        l_overrideMsisdnQuarantinePeriodStr = getValidParam(p_guiRequest,
                                                            0,
                                                            p_request,
                                                            "p_resetQuarantine",
                                                            false,
                                                            "false",
                                                            PpasServlet.C_TYPE_BOOLEAN);
        
        l_overrideMsisdnQuarantinePeriod = Boolean.valueOf(l_overrideMsisdnQuarantinePeriodStr).booleanValue();

        // Get the single step install flag from the request
        l_singleStepInstallStr = getValidParam(p_guiRequest,
                                            0,
                                            p_request,
                                            "p_singleStepInstall",
                                            false,
                                            "false",
                                            PpasServlet.C_TYPE_BOOLEAN);
        
        l_singleStepInstall = Boolean.valueOf(l_singleStepInstallStr).booleanValue();

        // Get the agent from the request
        l_agent = getValidParam(p_guiRequest,
                                PpasServlet.C_FLAG_ALLOW_BLANK,
                                p_request,
                                "p_agent",
                                false,
                                "",
                                PpasServlet.C_TYPE_ANY,
                                8,
                                C_OPERATOR_LESS_THAN_OR_EQUAL);

        // Get the subscriber type parameter from the request
        l_subType = getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_subType",
                                  true,
                                  "",
                                  new String [] {"STANDALONE",
                                                 "SUBORDINATE"});

        // Get USSD EoCN Selection Structure ID from the request
        l_ussdEocnIdStr = getValidParam(p_guiRequest,
                                        PpasServlet.C_FLAG_ALLOW_BLANK,
                                        p_request,
                                        "p_ussdEocnId",
                                        false,
                                        "",
                                        PpasServlet.C_TYPE_NUMERIC);

        try
        {
            if (l_ussdEocnIdStr != null && !l_ussdEocnIdStr.trim().equals(""))
            {
                l_ussdEocnId = new EndOfCallNotificationId(l_ussdEocnIdStr);
            }
        }
        catch (PpasParseException l_ppasParseEx)
        {
            // Generate a new servlet exception
            l_pSE = new PpasServletException(C_CLASS_NAME,
                                             C_METHOD_doService,
                                             52200,
                                             this,
                                             p_guiRequest,
                                             0,
                                             ServletKey.get().invalidEocnId(l_ussdEocnIdStr),
                                             l_ppasParseEx);

            i_logger.logMessage(l_pSE);
            throw (l_pSE);
        }

        // Get details for a STANDALONE installation...
        if (l_subType.equals("STANDALONE"))
        {
            // Set the master MSISDN equal to the MSISDN.
            l_masterMsisdn = l_msisdn;

            // Get the service class from the request
            l_class = getValidParam(p_guiRequest,
                                    0,
                                    p_request,
                                    "p_serviceClass",
                                    false,
                                    "",
                                    PpasServlet.C_TYPE_ANY,
                                    4,
                                    C_OPERATOR_LESS_THAN_OR_EQUAL);

            // Get the promotion plan from the request
            l_promoPlan = getValidParam(p_guiRequest,
                                        PpasServlet.C_FLAG_ALLOW_BLANK,
                                        p_request,
                                        "p_promoPlan",
                                        false,
                                        "",
                                        PpasServlet.C_TYPE_ANY,
                                        4,
                                        C_OPERATOR_LESS_THAN_OR_EQUAL);

        }

        // Get details for SUBORDINATE installation...
        else if (l_subType.equals("SUBORDINATE"))
        {
            // Get the master MSISDN from the request
            l_masterMsisdn = getValidMsisdn(p_guiRequest,
                                            0,
                                            p_request,
                                            "p_masterMsisdn",
                                            true,
                                            "",
                                            i_guiContext.getMsisdnFormatInput());

            l_oldmasterMsisdn = l_masterMsisdn;
        }

        
        // Invoke the service method to install the subscriber.
        l_guiResponse = i_ccInstallService.install(p_guiRequest,
                                                   i_timeout,
                                                   l_marketStr,
                                                   l_class,
                                                   l_masterMsisdn,
                                                   l_promoPlan,
                                                   l_agent,
                                                   l_accountGroupId,
                                                   l_serviceOfferings,
                                                   l_overrideMsisdnQuarantinePeriod,
                                                   l_ussdEocnId,
                                                   l_singleStepInstall);

        // Add the reponse to the screen data object...
        p_screenData.addUpdateResponse(p_guiRequest,
                                       l_guiResponse);


        p_screenData.setOldAgent(l_agent);
        // Get the current time to use in display message.
        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setInfoText(
                    "Failed Install at " + l_currentTime);

            // Set old installation  MSISDN, so can be re-displayed.
            p_screenData.setOldMsisdn(
                            p_request.getParameter("p_msisdn"));

            p_screenData.setOldOption(
                            p_request.getParameter("p_subType"));

            p_screenData.setOldAgent(
                            p_request.getParameter("p_agent"));

            p_screenData.setFailedMarket(l_market);

            p_screenData.setOldServiceClass(l_class);
            
            p_screenData.setOldPromo(l_promoPlan);
            p_screenData.setOldMaster(l_oldmasterMsisdn);
            p_screenData.setOldServiceOffering(p_request.getParameter("p_serviceOffering"));
            p_screenData.setOldAccountGroup(p_request.getParameter("p_accountGroup"));
            p_screenData.setFailedRequest(true);
        }
        else
        {
            p_screenData.setInfoText(
                    "Successful Install at " + l_currentTime);
                                       
            // Let the thread sleep for a period
            // to allow the asynchronous subscriber installation process to fulfill.
            // Try then to get the basic account data for the subscriber beeing installed.
            // Let the thread sleep for another period if
            // the subscriber installation process has not yet completed.
            // Try then to get the basic account data again.             
            try
            {
                // Let the thread sleep for the primary delay period                    
                Thread.sleep(i_primaryDelayPeriod);
                // Get the basic account data for the subscriber beeing installed.
                l_ccBasicAccountDataResponse = i_ccAccountService.getBasicData(p_guiRequest, i_timeout);
                
                // Check if the status of the subsriber account is 'Available'.
                if (l_ccBasicAccountDataResponse
                    .getBasicAccountData()
                    .getCustStatus() != 'A')
                {
                    // Let the thread sleep for secondary delay period
                    Thread.sleep(i_secondaryDelayPeriod);

                    // Get the basic account data for the subscriber being installed.
                    l_ccBasicAccountDataResponse = i_ccAccountService.getBasicData(p_guiRequest, i_timeout);
                }
            }
            catch (InterruptedException l_interruptedE)
            {
                // Exception handling makes no sense here, in case of a thread interruption
                // the servlet's life cycle will end.                        
                l_interruptedE = null;
            }          
            p_screenData.setCustStatus(l_ccBasicAccountDataResponse
                                        .getBasicAccountData().getCustStatus() + "");
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 50020, this,
                "LEAVING " + C_METHOD_install);
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getData = "getData";
    /**
     * Fetches data needed for the various drop down boxes on the install screen
     * from the CSO's session object and from the business config cache.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the Install screen, on
     *                     which to put the response to the request.
     */
    private void getData(GuiRequest          p_guiRequest,
                         CcInstallScreenData p_screenData)
    {
        MarketSet                  l_markets;
        MiscCodeDataSet            l_serviceClasses;
        SrvaAgentMstrDataSet       l_agents;
        PrplPromoPlanDataSet       l_promoPlans;
        FeatureLicenceCache        l_featureLicenceCache;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 60000, this,
                "ENTERED " + C_METHOD_getData);
        }

        // Get the CSO's valid markets...
        l_markets = ((PpasSession)p_guiRequest.getSession()).
                      getCsoMarkets();  // PpacLon#1/17

        // ...and add them to the screendata object.
        p_screenData.setMarkets(l_markets);

        // Get a set of service classes for all markets...
        l_serviceClasses = i_configService.getAvailableServiceClasses(l_markets.getMarkets());

        // ...and add it to the screendata object.
        p_screenData.setServiceClasses(l_serviceClasses);

        // Get a set of agents for all markets...
        l_agents = i_configService.getAvailableAgents(l_markets.getMarkets());

        // ...and add it to the screendata object.
        p_screenData.setAgents(l_agents);
        
        // Get a set of promotion plans...
        l_promoPlans = i_configService.getAvailablePromoPlans();

        // ...and add it to the screendata object.
        p_screenData.setPromoPlans(l_promoPlans);

        // Set feature licence flags.
        try
        {
            l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
            p_screenData.setSubscriberSegmentationFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_SUBSCIRBER_SEGMENTATION));
            p_screenData.setOverrideMsisdnQuarantinePeriodFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_OVERRIDE_MSISDN_QUARANTINE));
            p_screenData.setSingleStepInstallFeatureLicence(l_featureLicenceCache.
                isLicensed(FeatureLicence.C_FEATURE_CODE_SINGLE_STEP_INSTALL));
        }
        catch (PpasServiceException p_ppasServiceE)
        {
            i_logger.logMessage(p_ppasServiceE);
        }

        // set forced single step installs if configured
        if(i_installationType.equalsIgnoreCase(PpasInstallService.C_INSTALL_TYPE_SINGLE_STEP))
        {
            p_screenData.setForcedSingleStepInstall(true);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 60010, this,
                "LEAVING " + C_METHOD_install);
        }

        return;
    }
}
