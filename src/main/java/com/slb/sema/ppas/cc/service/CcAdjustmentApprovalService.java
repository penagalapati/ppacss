////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAdjustmentApprovalService.java
//      DATE            :       14-Oct-2003
//      AUTHOR          :       Steven James
//      REFERENCE       :       PpaLon#2071/8521
//                              PRD_PPAK00_GEN_CA_464
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Gui Service that wrappers calls to 
//                              PpasAdjustmentApprovalService and
//                              PpasDedAccAdjApprovalService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 08/02/06 | Ian James  | Get the default locale for the  | PpacLon#1900/7910
//          |            | Adjustment Approval service.    |
//----------+------------+---------------------------------+--------------------
// 4/06/10  | Siva Sanker| Adjustment approval source OPID | PpacBan#3693/13761
//          | Reddy      | is included.                    |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.HoldAdjustmentDataSet;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasAdjustmentApprovalService;
import com.slb.sema.ppas.is.isapi.PpasDedAccAdjApprovalService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with 
 * Adjustment Approval.
 */
public class CcAdjustmentApprovalService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAdjustmentApprovalService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service for normal adjustments. */
    private PpasAdjustmentApprovalService i_adjustmentApprovalService;

    /** Atomic service used by this Gui Service for dedicated account adjustments. */
    private PpasDedAccAdjApprovalService  i_dedacAdjustApprovalService;


    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcAdjustmentApprovalService class.
     *
     * @param p_guiRequest  The GUI request
     * @param p_flags  Not currently used
     * @param p_logger The logging object to use
     * @param p_guiContext  The GUI context
     */
    public CcAdjustmentApprovalService(GuiRequest p_guiRequest,
                                       long       p_flags,
                                       Logger     p_logger,
                                       GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 22000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        // Create internal Adjustment Approval service to be used by this service.
        i_adjustmentApprovalService = new PpasAdjustmentApprovalService(p_guiRequest, p_logger, p_guiContext);

        // Create internal Dedicated Account Adjustment Approval service to be used by this service.
        i_dedacAdjustApprovalService = new PpasDedAccAdjApprovalService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 22010, this,
                           "Constructed " + C_CLASS_NAME);
        }
        
    }  // end constructor

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Method name to use in calls to middleware. */
    private static final String C_METHOD_getHoldAdjustments = "getHoldAdjustments";
    /**
     * Retrieves all Adjustments pending approval.
     * @param p_guiRequest  The GUI request
     * @param p_timeoutMillis  The timeout to use
     * @return data set used to populate the screen
     */
    public CcHoldAdjustmentDataSetResponse getHoldAdjustments(GuiRequest p_guiRequest,
                                                              long       p_timeoutMillis)
    {
        CcHoldAdjustmentDataSetResponse l_ccHoldAdjustmentDataSetResponse = null;
        GuiResponse                     l_guiResponse                     = null;
        HoldAdjustmentDataSet           l_holdAdjustmentDataSet           = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 22020, this,
                           "Entered " + C_METHOD_getHoldAdjustments);
        }

        try
        {
            l_holdAdjustmentDataSet = 
                    i_adjustmentApprovalService.getHoldAdjustments(p_guiRequest,
                                                                   0, // TODO ignore adjustments prog
                                                                   p_timeoutMillis);

            l_ccHoldAdjustmentDataSetResponse = 
                new CcHoldAdjustmentDataSetResponse(
                        p_guiRequest,
                        p_guiRequest.getGuiSession().getSelectedLocale(),
                        GuiResponse.C_KEY_SERVICE_SUCCESS,
                        new Object[] {},
                        GuiResponse.C_SEVERITY_SUCCESS,
                        l_holdAdjustmentDataSet);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 22030, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "get hold adjustments", l_pSE);

            l_ccHoldAdjustmentDataSetResponse = 
                new CcHoldAdjustmentDataSetResponse(
                        p_guiRequest,
                        p_guiRequest.getGuiSession().getSelectedLocale(),
                        l_guiResponse.getKey(),
                        l_guiResponse.getParams(),
                        GuiResponse.C_SEVERITY_FAILURE,
                        null);
        }


        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 22040, this,
                           "Leaving " + C_METHOD_getHoldAdjustments);
        }

        return l_ccHoldAdjustmentDataSetResponse;

    }  // end public CcHoldAdjustmentDataSetResponse getHoldAdjustments(...)

    /** Method name to use in calls to middleware. */
    private static final String C_METHOD_approveHoldAdjustment = 
                                        "approveHoldAdjustment";
    /**
     * Approve selected Adjustments for posting.
     *
     * @param p_request         The GUI request
     * @param p_timeoutMillis   The timeout to use
     * @param p_dedacId         Dedicated account ID, if a dedicated account adjustment.
     * @param p_adjType         Type of adjustment.
     * @param p_adjCode         Adjustment code.
     * @param p_amount          Adjustment amount.
     * @param p_serviceClass    Service class at time of adjustment.
     * @param p_dateTime        The date of the held adjustment to be approved
     * @param p_sourceOpid      The source OPID where the adjustment for approval was raised.
     * @return GuiResponse A GuiResponse object
     */
    public GuiResponse approveHoldAdjustment(GuiRequest     p_request,
                                             long           p_timeoutMillis,
                                             String         p_dedacId,
                                             String         p_adjType,
                                             String         p_adjCode,
                                             Money          p_amount,
                                             String         p_serviceClass,
                                             PpasDateTime   p_dateTime,
                                             String         p_sourceOpid)
    {
        GuiResponse   l_guiResponse = null;
        ServiceClass  l_serviceClass = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 22050, this,
                           "Entered " + C_METHOD_approveHoldAdjustment);
        }

        if ( (p_serviceClass != null) && (p_serviceClass.trim().length() > 0) )
        {
            l_serviceClass = new ServiceClass(Integer.parseInt(p_serviceClass));
        }

        try
        {
            if (p_dedacId.length() == 0)
            {
                i_adjustmentApprovalService.approveHoldAdjustment(p_request,
                                                                  p_timeoutMillis,
                                                                  p_adjType,
                                                                  p_adjCode,
                                                                  p_amount,
                                                                  p_dateTime,
                                                                  p_sourceOpid);
            }
            else
            {
                i_dedacAdjustApprovalService.approveHoldAdjustment(p_request,
                                                                   p_timeoutMillis,
                                                                   p_dedacId,
                                                                   p_adjType,
                                                                   p_adjCode,
                                                                   p_amount,
                                                                   l_serviceClass,
                                                                   p_dateTime,
                                                                   p_sourceOpid);
            }

            l_guiResponse = new GuiResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                    new Object[] {"approve hold adjustment"},
                                    GuiResponse.C_SEVERITY_SUCCESS
                                    );
            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 22060, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_request, 0, "approve hold adjustment", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 22070, this,
                           "Leaving " + C_METHOD_approveHoldAdjustment);
        }

        return l_guiResponse;
    }

    /** Method name to use in calls to middleware. */
    private static final String C_METHOD_rejectHoldAdjustment = "rejectHoldAdjustment";
    /**
     * Reject selected pending Adjustments.
     *
     * @param p_guiRequest  The GUI request
     * @param p_timeoutMillis  The timeout to use
     * @param p_dateTime The date of the held adjustment to be rejected
     *
     * @return GuiResponse A GuiResponse object
     */
    public GuiResponse rejectHoldAdjustment(GuiRequest      p_guiRequest,
                                            long            p_timeoutMillis,
                                            PpasDateTime    p_dateTime)
    {
        GuiResponse l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 22080, this,
                           "Entered " + C_METHOD_rejectHoldAdjustment);
        }

        try
        {                        
            i_adjustmentApprovalService.rejectHoldAdjustment(p_guiRequest, p_timeoutMillis, p_dateTime);

            l_guiResponse = new GuiResponse(
                                    p_guiRequest,
                                    p_guiRequest.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                    new Object[] {"reject hold adjustment"},
                                    GuiResponse.C_SEVERITY_SUCCESS);     
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 22090, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "reject hold adjustment", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 22100, this,
                           "Leaving " + C_METHOD_rejectHoldAdjustment);
        }

        return l_guiResponse;
    }
}
