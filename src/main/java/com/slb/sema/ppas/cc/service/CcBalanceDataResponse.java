////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcBalanceDataResponse.java
//      DATE            :       27-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Response returned by the Customer Care Balance
//                              service containing an accounts balance and
//                              pending credit.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 02/04/07 | M Erskine  | Add Churn indicator info.       | PpacLon#3011/11200
//          |            |                                 | PRD_ASCS00_GEN_CA_115
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.churn.ChurnIndicatorData;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.common.web.support.WebDebug;

/**
 * Response containing details of a customer's balance and pending credit if
 * these details were successfully obtained by the Customer Care Balance
 * service. Otherwise, the key in the GuiResponse
 * super class defines the reason for the failure of the service.
 */
public class CcBalanceDataResponse extends GuiResponse
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcBalanceDataResponse";

    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------

    /** Data object containing details of a customer's balance and pending
     *  credit.
     */
    private BalanceEnquiryResultData i_balanceData;
    
    /**
     * Data object containing customer ranking and flag for whether churn imminent.
     */
    private ChurnIndicatorData i_churnData;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------

    /** 
     * Construct a CcBalanceDataResponse using the default locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_balanceData   Customer balance information.
     * @param p_churnData     Customer churn information.
     */
    public CcBalanceDataResponse(
        GuiRequest                p_guiRequest,
        String                    p_messageKey,
        Object []                 p_messageParams,
        int                       p_messageSeverity,
        BalanceEnquiryResultData  p_balanceData,
        ChurnIndicatorData        p_churnData)
    {
        this (p_guiRequest,
              null,
              p_messageKey,
              p_messageParams,
              p_messageSeverity,
              p_balanceData,
              p_churnData);
    }

    /** 
     * Construct a CcBalanceDataResponse using the given locale.
     * @param p_guiRequest    The request being processed.
     * @param p_messageLocale Locale for displaying message text.
     * @param p_messageKey    Key of the message.
     * @param p_messageParams Parameters for the message.
     * @param p_messageSeverity Severity of the message.
     * @param p_balanceData   Customer balance information.
     * @param p_churnData     Customer churn information.
     */
    public CcBalanceDataResponse(
        GuiRequest               p_guiRequest,
        Locale                   p_messageLocale,
        String                   p_messageKey,
        Object[]                 p_messageParams,
        int                      p_messageSeverity,
        BalanceEnquiryResultData p_balanceData,
        ChurnIndicatorData       p_churnData)
    {
        super (p_guiRequest,
               p_messageLocale,
               p_messageKey,
               p_messageParams,
               p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest,
                C_CLASS_NAME, 48000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_balanceData = p_balanceData;
        i_churnData = p_churnData;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest,
                C_CLASS_NAME, 48090, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // end constructor


    //--------------------------------------------------------------------------
    // Public Methods.
    //--------------------------------------------------------------------------

    /**
     * Returns a reference to the balance data object instance contained within
     * this response.
     * @return Balance data.
     */
    public BalanceEnquiryResultData getData()
    {
        return (i_balanceData);
    }
    
    /**
     * Returns a reference to the churn data object instance containined within 
     * this response.
     * @return Churn data.
     */
    public ChurnIndicatorData getChurnData()
    {
        return (i_churnData);
    }

} // end class CcBalanceDataResponse
