////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDedicatedAccountsServlet.Java
//      DATE            :       07-Aug-2002
//      AUTHOR          :       Simone Nelson & Erik Clayton
//      REFERENCE       :       PpaLon#1448 & PpaLon#1749
//                              PRD_PPAK00_GEN_CA_372 & 415
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to handle requests from the
//                              Dedicated Accounts screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcDedicatedAccountsScreenData;
import com.slb.sema.ppas.cc.service.CcAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcAdjustmentDataSetResponse;
import com.slb.sema.ppas.cc.service.CcBalanceDataResponse;
import com.slb.sema.ppas.cc.service.CcDedicatedAccountsService;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.common.dataclass.DedicatedAccountsData;
import com.slb.sema.ppas.common.dataclass.DedicatedAccountsDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 * Servlet to handle requests from the Dedicated Accounts Screen.
 */
public class CcDedicatedAccountsServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDedicatedAccountsServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Dedicated Accounts Service to be used by this servlet. */
    private CcDedicatedAccountsService i_ccDedicatedAccountsService = null;

    /** The Account Service to be used by this servlet. */
    private CcAccountService           i_accountService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcDedicatedAccountsServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise this object.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcDedicatedAccountsService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the account details screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                             l_command     = null;
        String                             l_forwardUrl  = null;
        CcDedicatedAccountsScreenData      l_dedicatedAccountsScreenData = null;
        GuiResponse                        l_guiResponse = null;
        boolean                            l_accessGranted = false;
        GopaGuiOperatorAccessData          l_gopaData = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        if (i_ccDedicatedAccountsService == null)
        {
            i_ccDedicatedAccountsService =
                         new CcDedicatedAccountsService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        if (i_accountService == null)
        {
            i_accountService = new CcAccountService (p_guiRequest, 0, i_logger, i_guiContext);
        }

        l_dedicatedAccountsScreenData =
            new CcDedicatedAccountsScreenData(i_guiContext, p_guiRequest);

        try
        {
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true ,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "POST_ADJUSTMENT"});

            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("GET_SCREEN_DATA")) &&
                 l_gopaData.hasDedAccountsScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("POST_ADJUSTMENT")) &&
                      l_gopaData.makeDedAccAdjustIsPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_dedicatedAccountsScreenData.addRetrievalResponse(p_guiRequest,
                                                                    l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_dedicatedAccountsScreenData);
            }
            else
            {
                getBusinessConfigData(p_guiRequest, l_dedicatedAccountsScreenData);

                if (l_command.equals("POST_ADJUSTMENT"))
                {
                    setIsUpdateRequest(true);

                    postDedicatedAccountAdjustment(
                               p_guiRequest,
                               p_request,
                               l_dedicatedAccountsScreenData);
                }

                if (!l_dedicatedAccountsScreenData.subIsDisconnected())
                {
                    getDedicatedAccounts(p_guiRequest,
                                         l_dedicatedAccountsScreenData);
                }

                getDedicatedAccountAdjustments(
                           p_guiRequest,
                           l_dedicatedAccountsScreenData);
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10100, this,
                    "Caught exception: " + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_dedicatedAccountsScreenData.addRetrievalResponse(p_guiRequest,
                                                               l_guiResponse);
        }

        if (l_accessGranted)
        {
            if (l_dedicatedAccountsScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the DedicatedAccountss screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                // CcAccountDetailsScreenData object has the necessary data
                // to populate the DedicatedAccountss screen.
                l_forwardUrl = "/jsp/cc/ccdedicatedaccounts.jsp";
            }

            p_request.setAttribute ("p_guiScreenData",
                                    l_dedicatedAccountsScreenData);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13035, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDedicatedAccounts = "getDedicatedAccounts";
    /**
     * Gets the Dedicated Accounts details for the given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the dedicated accounts screen, on
     *                     which to put the dedicated account details.
     * @throws PpasServletException If there is a conflict in the dedicated accounts.
     */
    private void getDedicatedAccounts(
                                     GuiRequest                    p_guiRequest,
                                     CcDedicatedAccountsScreenData p_screenData)
        throws PpasServletException
    {
        CcBalanceDataResponse        l_ccBalanceDataResponse    = null;
        DedicatedAccountsDataSet     l_dedAccountsDataSet       = null;
        BalanceEnquiryResultData     l_balanceData              = null;
        CcAccountDataResponse        l_accountDataResponse      = null;
        ServiceClass                 l_serviceClass             = null;
        Market                       l_market                   = null;
        DedaDedicatedAccountsDataSet l_dedaDataSet              = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           28000,
                           this,
                           "Entered " + C_METHOD_getDedicatedAccounts);
        }

        l_ccBalanceDataResponse = i_ccDedicatedAccountsService.
                                       getDedicatedAccountBalances(p_guiRequest,
                                                                   i_timeout);

        // Normally we would now add the retrieval response to the screen data.
        // However, if the above balance retrieval failed, then we are still
        // going to display the screen with "unavailable" as the balance data.
        // So we can't add the response here.

        l_balanceData = l_ccBalanceDataResponse.getData();

        if (l_balanceData == null)
        {
            // Balance is unavailable, so set up the dedicated accounts balance
            // data to indicate this.

            l_accountDataResponse = i_accountService.getFullData(p_guiRequest, i_timeout);

            p_screenData.addRetrievalResponse(p_guiRequest,
                                              l_accountDataResponse);

            if (l_accountDataResponse.isSuccess())
            {
                // Since we had to retrieve the account data anyway, get the latest
                // preferred currency rather than using the cached value on the session.
                p_screenData.setPreferredCurrency(l_accountDataResponse
                                                 .getAccountData()
                                                 .getPreferredCurrency());

                l_serviceClass = l_accountDataResponse.getAccountData().getActiveServiceClass();
                l_market = ((GuiSession)p_guiRequest.getSession()).getCsoMarket();

                l_dedaDataSet = i_configService.getDedicatedAccounts(p_guiRequest,
                                                                     l_market,
                                                                     l_serviceClass);

                try
                {
                    l_dedAccountsDataSet = new DedicatedAccountsDataSet(p_guiRequest, 
                                                                        l_dedaDataSet.getDedicatedArray());
                }
                catch (PpasConfigException l_ex)
                {
                    PpasServletException l_pSE = new PpasServletException(
                                        C_CLASS_NAME,
                                        C_METHOD_getDedicatedAccounts,
                                        11601,
                                        this,
                                        p_guiRequest,
                                        0,
                                        ServletKey.get().dedicatedAccountConflict(),
                                        l_ex);

                    i_logger.logMessage(l_pSE);

                    throw l_pSE;
                }
            }
        }
        else
        {
            l_dedAccountsDataSet = l_balanceData.getDedicatedAccounts();

            // Get the preferred currency from the session
            p_screenData.setPreferredCurrency( ((GuiSession)p_guiRequest.getGuiSession())
                                                .getPreferredCurrency());
        }

        p_screenData.setDedicatedAccountsDataSet(l_dedAccountsDataSet);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           28090,
                           this,
                           "Leaving " + C_METHOD_getDedicatedAccounts);
        }

    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_postDedicatedAccountAdjustment =
                                        "postDedicatedAccountAdjustment";
    /**
     * Post an adjustment to the requested dedicated account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    The servlet request.
     * @param p_screenData The screen data for the dedicated accounts screen, on
     *                     which to put the response to the request.
     */
    private void postDedicatedAccountAdjustment(
                                GuiRequest                    p_guiRequest,
                                HttpServletRequest            p_request,
                                CcDedicatedAccountsScreenData p_screenData)
    {
        GuiResponse                l_guiResponse                   = null;
        Money                      l_amount                        = null;
        String                     l_type                          = null;
        String                     l_code                          = null;
        String                     l_description                   = null;
        PpasDateTime               l_currentTime                   = null;
        PpasDate                   l_oldDate                       = null;
        PpasDate                   l_newDate                       = null;
        String                     l_dedicatedAccountId            = null;
        String                     l_dedicatedAccountExpiryDate    = null;
        String                     l_oldDedicatedAccountExpiryDate = null;
        String                     l_failureReasonKey              = null;
        String                     l_amountStr                     = null;
        PpasCurrency               l_currency                      = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Entered " + C_METHOD_postDedicatedAccountAdjustment);
        }

        try
        {
            try
            {
                l_amountStr = getValidParam(p_guiRequest,
                                            PpasServlet.C_FLAG_ALLOW_BLANK,
                                            p_request,
                                            "p_amount",
                                            false,
                                            null,
                                            PpasServlet.C_TYPE_ANY);
                
                if (l_amountStr != null)
                {
                    l_amountStr = getValidParam(p_guiRequest,
                                                PpasServlet.C_FLAG_STRIP_PLUS_SIGN,
                                                p_request,
                                                "p_amount",
                                                false,
                                                "",
                                                PpasServlet.C_TYPE_SIGNED_NUMERIC_WITH_DECIMALS);
                }
                
                l_currency = i_guiContext.getCurrency(getValidParam(p_guiRequest,
                                                                    0,
                                                                    p_request,
                                                                    "p_currency",
                                                                    true,
                                                                    "",
                                                                    PpasServlet.C_TYPE_ANY));
                
                l_amount = i_guiContext.getMoneyFormat().parse(l_amountStr,l_currency);
            }
            catch (ParseException l_pe)
            {
                PpasServletException l_pSE = new PpasServletException
                               (C_CLASS_NAME,
                                C_METHOD_doService,
                                11601,
                                this,
                                p_guiRequest,
                                0,
                                ServletKey.get().invalidNumericValue("p_amount", l_amountStr),
                                l_pe);

                i_logger.logMessage(l_pSE);

                throw l_pSE;
            }

            l_type =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_adjustmentTypes",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_code =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_adjustmentCodes",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_description =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_description",
                              false,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_dedicatedAccountId =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_adjustmentDedAccId",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_dedicatedAccountExpiryDate =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_adjustmentDedAccDate",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

            l_oldDedicatedAccountExpiryDate =
                getValidParam(p_guiRequest,
                              PpasServlet.C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_oldAdjustmentDedAccDate",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY);

        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 27300, this,
                    "Exception parsing parameters: " + l_pSE);
            }

            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"post adjustment"},
                                GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {

            if (!l_oldDedicatedAccountExpiryDate.equals(l_dedicatedAccountExpiryDate))
            {
                l_oldDate = new PpasDate(l_oldDedicatedAccountExpiryDate);

                // Note: this could be a blank date, i.e. "not set"
                l_newDate = new PpasDate(l_dedicatedAccountExpiryDate);
            }

            l_guiResponse = i_ccDedicatedAccountsService
                                .postDedicatedAccountAdjustment(
                                              p_guiRequest,
                                              i_timeout,
                                              l_amount,
                                              l_type,
                                              l_code,
                                              l_description,
                                              l_dedicatedAccountId,
                                              l_oldDate,
                                              l_newDate,
                                              Boolean.FALSE);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);

            if (!l_guiResponse.isSuccess())
            {
                l_failureReasonKey = l_guiResponse.getKey();

                if (l_failureReasonKey.equals(GuiResponse.C_KEY_EXCEEDED_MAX_ADJUSTMENT_AMOUNT) ||
                    l_failureReasonKey.equals(GuiResponse.C_KEY_EXCEEDED_DAILY_ADJUSTMENT_LIMIT))
                {
                    p_screenData.setPostedForApproval();
                }
            }
        }

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            if (p_screenData.wasPostedForApproval())
            {
                p_screenData.setInfoText(
                    "Adjustment posted for approval at " + l_currentTime);
            }
            else
            {
                p_screenData.setInfoText(
                    "Failed Adjustment at " + l_currentTime);
            }
        }
        else
        {
            p_screenData.setInfoText(
                "Successful Adjustment at " + l_currentTime);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Leaving " + C_METHOD_postDedicatedAccountAdjustment);
        }

    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDedicatedAccountAdjustments = "getDedicatedAccountAdjustments";
    /**
     * Get the Adjustment history for the given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the dedicated accounts screen, on
     *                     which to put the adjustment data.
     */
    private void getDedicatedAccountAdjustments(
                                GuiRequest                    p_guiRequest,
                                CcDedicatedAccountsScreenData p_screenData)
    {
        CcAdjustmentDataSetResponse l_ccAdjustmentDataSetResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           26000,
                           this,
                           "Entered " + C_METHOD_getDedicatedAccountAdjustments);
        }

        l_ccAdjustmentDataSetResponse = i_ccDedicatedAccountsService
                                            .getDedicatedAccountAdjustments(
                                                       p_guiRequest,
                                                       i_timeout);
        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccAdjustmentDataSetResponse);

        if (l_ccAdjustmentDataSetResponse.isSuccess())
        {
            p_screenData.setAdjustmentDataSet(
                          l_ccAdjustmentDataSetResponse.getAdjustmentDataSet());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           26090,
                           this,
                           "Leaving " + C_METHOD_getDedicatedAccountAdjustments);
        }
    }

    /**
     * Obtain the business configuration data.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData The screen data for the dedicated accounts screen, on
     *                     which to put the config data.
     */
    private void getBusinessConfigData(
                           GuiRequest                    p_guiRequest,
                           CcDedicatedAccountsScreenData p_screenData)
    {
        SrvaAdjCodesDataSet         l_adjCodes     = null;
        CufmCurrencyFormatsDataSet  l_availableCurrencies = null;

        l_adjCodes = i_configService.getAvailableAdjCodes(
                                        ((GuiSession)p_guiRequest.getSession())
                                         .getCsoMarket());
        p_screenData.setAdjustmentCodes(l_adjCodes);

        l_availableCurrencies = i_configService.getAvailableCurrencyFormats();
        p_screenData.setCurrencyDataSet(l_availableCurrencies);

        if (i_configService.getCompanyConfigData().getExpiryAfterDateInd())
        {
            p_screenData.setExpiryAfterDate();
        }
    }
}
