////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcDisconnectService.java
//      DATE            :       26-Mar-2002
//      AUTHOR          :       Remi Isaacs
//      REFERENCE       :       PpaLon#1341/5352
//                              PRD_PPAK00_DEV_IN_035
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasDisconnectService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.dataclass.MsisdnDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasDisconnectService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Provides methods to perform GUI specific services associated with
 * Disconnects.
 */
public class CcDisconnectService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcDisconnectService";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasDisconnectService i_disconnectService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the CcDisconnectService class.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct erxceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */

    public CcDisconnectService(GuiRequest p_guiRequest,
                               long       p_flags,
                               Logger     p_logger,
                               GuiContext p_guiContext)
    {
        super(p_guiRequest,
              p_flags,
              p_logger,
              p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 22000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal Disconnect service to be used by this service.
        i_disconnectService = new PpasDisconnectService(p_guiRequest,
                                                        p_logger,
                                                        p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 22010, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    // Method name to use in calls to middleware.
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDisconnectDetails =
                                        "getDisconnectDetails";
    /**
     * Retrieves Disconnection details for subscriber.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return Response containing the success/failure status of the attempt to
     *         get Disconnect details, in the case of failure, the key defining
     *         the failure reason.
     */
    public CcDisconnectDataResponse getDisconnectDetails(
                                             GuiRequest p_guiRequest,
                                             long       p_timeoutMillis )
    {
        CcDisconnectDataResponse     l_ccDisconnectResponse   = null;
        GuiResponse                  l_guiResponse            = null;
        DisconnectData               l_disconnectData         = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 70000, this,
                "Entered " + C_METHOD_getDisconnectDetails);
        }
        try
        {
            l_disconnectData = i_disconnectService.getDisconnectDetails(
                                                p_guiRequest,
                                                p_timeoutMillis );

            l_ccDisconnectResponse
                = new CcDisconnectDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),   // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_disconnectData );

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 70210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "get the disconnection details", l_pSE);

            l_ccDisconnectResponse =
                new CcDisconnectDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),  // PpacLon#1/17
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_LOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 71000, this,
                "Leaving " + C_METHOD_getDisconnectDetails);
        }

        return ( l_ccDisconnectResponse );

    }  // end getDisconnectDetails(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_disconnectSubscriber = "disconnectSubscriber";
    /**
     * Generates a GuiDisconnectResponse object for the MSISDN passed in.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_discReason The reason for disconnection.
     * @return GuiResponse The response containing the status of
     * the request.
     */
    public GuiResponse disconnectSubscriber(
                                        GuiRequest  p_guiRequest,
                                        long        p_timeoutMillis,
                                        String      p_discReason)
    {
        GuiResponse          l_guiResponse         = null;
        MsisdnDataSet        l_msisdnDataSet = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 10100, this,
                "Entered " + C_METHOD_disconnectSubscriber);
        }

        try
        {
            l_msisdnDataSet = i_disconnectService.disconnectSubscriber(
                                                    p_guiRequest,
                                                    p_timeoutMillis,
                                                    p_discReason,
                                                    false);
            
            // Generate success GuiDisconnectResponse

            l_guiResponse
                = new GuiResponse(
                          p_guiRequest,
                          p_guiRequest.getGuiSession().
                            getSelectedLocale(),  // PpacLon#1/17
                          GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                          new Object[] {"Disconnection"},
                          GuiResponse.C_SEVERITY_SUCCESS );

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 10210, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "disconnect the subscriber", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 11100, this,
                "Leaving " + C_METHOD_disconnectSubscriber);
        }

        return l_guiResponse;

    }  // end public GuiDisconnectResponse disconnectSubscriber(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_futureDisconnect = "futureDisconnect";
    /**
     * Generates a GuiDisconnectResponse object for the MSISDN passed in.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_disconnectType The future disconnection Reason.
     * @param p_futureDate The future date for disconnection.
     * @return GuiResponse The response containing the status of
     * the request.
     */
    public GuiResponse futureDisconnect(GuiRequest                    p_guiRequest,
                                        long                          p_timeoutMillis,
                                        String                        p_disconnectType,
                                        PpasDate                      p_futureDate)
    {
        GuiResponse                     l_guiResponse         = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 11130, this,
                "Entered " + C_METHOD_futureDisconnect);
        }

        try
        {
            i_disconnectService.futureDisconnect(p_guiRequest,
                                                 p_timeoutMillis,
                                                 p_disconnectType,
                                                 p_futureDate);

            // Generate success GuiDisconnectResponse

            l_guiResponse
                = new GuiResponse(
                          p_guiRequest,
                          p_guiRequest.getGuiSession().getSelectedLocale(),
                          GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                          new Object[] {"Future disconnection"},
                          GuiResponse.C_SEVERITY_SUCCESS );
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 11131, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "Future disconnect the subscriber", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 11132, this,
                "Leaving " + C_METHOD_futureDisconnect);
        }

        return l_guiResponse;

    }  // end public GuiDisconnectResponse futureDisconnect(...)

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_cancelDisconnection = "cancelDisconnection";
    /**
     * Generates a GuiDisconnectResponse object for the MSISDN passed in.
     *
     * @param p_guiRequest    The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return GuiResponse The response containing the status of
     * the request.
     */
    public GuiResponse cancelDisconnection(GuiRequest  p_guiRequest,
                                           long        p_timeoutMillis)
    {
        GuiResponse                     l_guiResponse         = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 15130, this,
                "Entered " + C_METHOD_cancelDisconnection);
        }

        try
        {
            i_disconnectService.cancelDisconnection(p_guiRequest, p_timeoutMillis);

            // Generate success GuiDisconnectResponse

            l_guiResponse
                = new GuiResponse(
                          p_guiRequest,
                          p_guiRequest.getGuiSession().getSelectedLocale(),
                          GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                          new Object[] {"Cancel Disconnection"},
                          GuiResponse.C_SEVERITY_SUCCESS );

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 15131, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest,
                                           0,
                                           "Cancel the subscriber's future disconnection",
                                           l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 11732, this,
                "Leaving " + C_METHOD_cancelDisconnection);
        }

        return l_guiResponse;
    }
}  // end public class CcDisconnectService extends GuiService
