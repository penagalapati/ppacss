////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcVoucherService.java
//      DATE            :       6-Feb-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/5475
//                              PRD_PPAK00_DEV_IN_034
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Gui Service that wrappers calls to
//                              PpasVoucherService
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 12/12/05 |K Goswami   | Added code for voucher history  | PpaLon#1892/7603
//          |            | popup screen                    | CA#39
//----------+------------+---------------------------------+--------------------
// 12.12.06 | MAGray     | Pass channel Id.                | PpacLon#2815/10635
//          |            |                                 | PRD_ASCS00_GEN_CA_105
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.FailedRechargeDataSet;
import com.slb.sema.ppas.common.dataclass.RechargeResultData;
import com.slb.sema.ppas.common.dataclass.VoucherDataSet;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.dataclass.VoucherHistoryData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasVoucherService;
import com.slb.sema.ppas.util.logging.Logger;

/** Provides methods to perform GUI specific services associated with Vouchers. */
public class CcVoucherService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcVoucherService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The atomic voucher service. */
    private PpasVoucherService i_voucherService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates an instance of a CcVoucherService object that
     * can then be used to perform Voucher services.
     * @param p_request    The request being processed.
     * @param p_logger     The logger to direct erxceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcVoucherService(GuiRequest  p_request,
                            Logger      p_logger,
                            GuiContext  p_guiContext)
    {
        super(p_request, 0, p_logger);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_START,
                           p_request, C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        i_voucherService = new PpasVoucherService(p_request, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_CONFIN_END,
                           p_request, C_CLASS_NAME, 10010, this,
                           "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucher = "getVoucher";
    /**
     * Generates a CcVoucherDataResponse object for the serial number passed in.
     *
     * @param p_request       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_serialNumber  The voucher serial number.
     * @return  Voucher details corresponding to the serial number requested.
     */
    public CcVoucherDataResponse getVoucher(GuiRequest p_request,
                                            long       p_timeoutMillis,
                                            String     p_serialNumber)
    {
        CcVoucherDataResponse l_voucherResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 10100, this,
                           "Entered " + C_METHOD_getVoucher );
        }

        try
        {
            // Call the atomic service...
            VoucherEnquiryData l_voucher = i_voucherService.getVoucher(p_request,
                                                                       p_timeoutMillis,
                                                                       p_serialNumber);

            // Generate success CcVoucherDataResponse
            l_voucherResponse = new CcVoucherDataResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_voucher);
        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 10110, this,
                               "Caught PpasServiceException: " + e);
            }

            GuiResponse l_guiResponse = handlePpasServiceException(p_request, 0, "get voucher details", e);

            l_voucherResponse = new CcVoucherDataResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 10120, this,
                           "Leaving " + C_METHOD_getVoucher);
        }

        return l_voucherResponse;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucherHistory = "getVoucherHistory";
    /**
     * Generates a CcVoucherHistoryDataResponse object for the serial number passed in.
     *
     * @param p_request       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_serialNumber  The voucher serial number.
     * @return  Voucher details corresponding to the serial number requested.
     */
    public CcVoucherHistoryDataResponse getVoucherHistory(GuiRequest p_request,
                                                          long       p_timeoutMillis,
                                                          String     p_serialNumber)
    {
        CcVoucherHistoryDataResponse l_voucherHistoryResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 10200, this,
                           "Entered " + C_METHOD_getVoucherHistory );
        }

        try
        {
            // Call the atomic service...
            VoucherHistoryData l_voucherHistoryData = i_voucherService.getVoucherHistory(p_request,
                                                                                         p_timeoutMillis,
                                                                                         p_serialNumber);

            // Generate success CcVoucherDataResponse
            l_voucherHistoryResponse = new CcVoucherHistoryDataResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_voucherHistoryData);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 10210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            GuiResponse l_guiResponse = handlePpasServiceException(
                                                         p_request, 0, "get voucher history details", l_pSE);

            l_voucherHistoryResponse = new CcVoucherHistoryDataResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 10220, this,
                           "Leaving " + C_METHOD_getVoucherHistory);
        }

        return l_voucherHistoryResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateVoucher = "updateVoucher";
    /**
     * Update voucher status from pending to either available or assigned.
     * @param p_request       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_serialNumber  The voucher serial number.
     * @param p_action        Commit voucher to "Assigned" or rollback to "Available".
     * @return  Response indicating success or failure of the request.
     */
    public GuiResponse updateVoucher(GuiRequest p_request,
                                     long       p_timeoutMillis,
                                     String     p_serialNumber,
                                     String     p_action)
    {
        GuiResponse l_guiResponse;
        int         l_action = -1;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 20000, this,
                           "Entered " + C_METHOD_updateVoucher );
        }

        try
        {
            if (p_action.equals("COMMIT"))
            {
                l_action = PpasVoucherService.C_VOUCHER_COMMIT;
            }
            else if (p_action.equals("ROLLBACK"))
            {
                l_action = PpasVoucherService.C_VOUCHER_ROLLBACK;
            }

            i_voucherService.updateVoucher(p_request, p_timeoutMillis, p_serialNumber, l_action);

            l_guiResponse = new GuiResponse(p_request,
                                            p_request.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {"update voucher status"},
                                            GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 20010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_request, 0, "update voucher status", l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 20020, this,
                           "Leaving " + C_METHOD_updateVoucher);
        }

        return l_guiResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getRecharges = "getRecharges";
    /**
     * Generates a set of successful recharges previously performed for the
     * cust id/MSISDN in the request.
     * @param p_request       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return  Set of previously performed successful recharges
     */
    public CcVoucherDataSetResponse getRecharges(
               GuiRequest  p_request,
               long        p_timeoutMillis )
    {
        CcVoucherDataSetResponse l_getRechargesResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 10300, this,
                           "Entered " + C_METHOD_getRecharges);
        }

        try
        {
            // Call the atomic service...
            VoucherDataSet l_recharges = i_voucherService.getRecharges(p_request, p_timeoutMillis);

            // Generate success CcVoucherDataSetResponse
            l_getRechargesResponse = new CcVoucherDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_recharges);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 10310, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            GuiResponse l_guiResponse =
                handlePpasServiceException(p_request, 0, "get details of successful recharges", l_pSE);

            l_getRechargesResponse =
                new CcVoucherDataSetResponse(
                              p_request,
                              p_request.getGuiSession().getSelectedLocale(),
                              l_guiResponse.getKey(),
                              l_guiResponse.getParams(),
                              GuiResponse.C_SEVERITY_FAILURE,
                              null);

        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 10320, this,
                           "Leaving " + C_METHOD_getRecharges);
        }

        return l_getRechargesResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getFailedRecharges = "getFailedRecharges";
    /**
     * Generates a set of failed recharges previously performed for the
     * cust id/MSISDN in the request.
     * @param p_request       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @return  Set of previously performed failed recharges
     */
    public CcFailedRechargeDataSetResponse getFailedRecharges(
               GuiRequest  p_request,
               long        p_timeoutMillis )
    {
        CcFailedRechargeDataSetResponse l_failedRechargesResponse;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_request, C_CLASS_NAME, 81234, this,
                           "Entered " + C_METHOD_getFailedRecharges);
        }

        try
        {
            // Call the atomic service...
            FailedRechargeDataSet l_failedRecharges = i_voucherService.getFailedRecharges(p_request,
                                                                                          p_timeoutMillis);

            // Generate success CcVoucherDataSetResponse
            l_failedRechargesResponse = new CcFailedRechargeDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().getSelectedLocale(),
                                    GuiResponse.C_KEY_SERVICE_SUCCESS,
                                    (Object[])null,
                                    GuiResponse.C_SEVERITY_SUCCESS,
                                    l_failedRecharges);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_request, C_CLASS_NAME, 82727, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            GuiResponse l_guiResponse =
                handlePpasServiceException(p_request, 0, "get details of failed recharges", l_pSE);

            l_failedRechargesResponse = new CcFailedRechargeDataSetResponse(
                                    p_request,
                                    p_request.getGuiSession().
                                      getSelectedLocale(),
                                    l_guiResponse.getKey(),
                                    l_guiResponse.getParams(),
                                    GuiResponse.C_SEVERITY_FAILURE,
                                    null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_request, C_CLASS_NAME, 88774, this,
                           "Leaving " + C_METHOD_getFailedRecharges);
        }

        return l_failedRechargesResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_rechargeAccount = "rechargeAccount";
    /**
     * Recharges a subscriber account and returns the outcome in a GuiResponse object.
     *
     * @param p_guiRequest       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_activationNumber  The voucher activation number.
     * @return  Response object containing the status of the request.
     */
    public CcRechargeResultDataResponse rechargeAccount(
                          GuiRequest p_guiRequest,
                          long       p_timeoutMillis,
                          String     p_activationNumber)
    {
        CcRechargeResultDataResponse l_rechargeResponse       = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 30000, this,
                           "Entered " + C_METHOD_rechargeAccount);
        }

        try
        {
            RechargeResultData l_rechargeResultData =
                i_voucherService.rechargeAccount(p_guiRequest,
                                                 p_timeoutMillis,
                                                 p_activationNumber,
                                                 null);   // Channel Id

            l_rechargeResponse = new CcRechargeResultDataResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"recharge account"},
                              GuiResponse.C_SEVERITY_SUCCESS,
                              l_rechargeResultData);

        }
        catch (PpasServiceException e)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 30210, this,
                               "Caught PpasServiceException: " + e);
            }

            GuiResponse l_guiResponse = handlePpasServiceException(
                                                             p_guiRequest, 0, "recharge the account", e);

            l_rechargeResponse = new CcRechargeResultDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 31000, this,
                           "Leaving " + C_METHOD_rechargeAccount);
        }

        return l_rechargeResponse;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_valueRechargeAccount = "valueRechargeAccount";
    /**
     * Recharges a subscriber account with a value voucher and returns the outcome
     * in a GuiResponse object.
     *
     * @param p_guiRequest       The request being processed.
     * @param p_timeoutMillis Number of milli-seconds until the request times out.
     * @param p_activationNumber  The value voucher activation number.
     * @return  Response object containing the status of the request.
     */
    public CcRechargeResultDataResponse valueRechargeAccount(
                          GuiRequest p_guiRequest,
                          long       p_timeoutMillis,
                          String     p_activationNumber)
    {
        CcRechargeResultDataResponse l_rechargeResponse       = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 32000, this,
                           "Entered " + C_METHOD_valueRechargeAccount);
        }

        try
        {
            RechargeResultData l_rechargeResultData =
                i_voucherService.applyValueVoucher(p_guiRequest,
                                                   p_timeoutMillis,
                                                   p_activationNumber,
                                                   null);    // Channel Id

            l_rechargeResponse = new CcRechargeResultDataResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"recharge account with a value voucher"},
                              GuiResponse.C_SEVERITY_SUCCESS,
                              l_rechargeResultData);

        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVICE, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 32010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            GuiResponse l_guiResponse =
                handlePpasServiceException(p_guiRequest,
                                           0,
                                           "recharge the account with a value voucher",
                                           l_pSE);

            l_rechargeResponse = new CcRechargeResultDataResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),
                                  l_guiResponse.getKey(),
                                  l_guiResponse.getParams(),
                                  GuiResponse.C_SEVERITY_FAILURE,
                                  null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVICE, WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 32020, this,
                           "Leaving " + C_METHOD_valueRechargeAccount);
        }

        return l_rechargeResponse;
    }
 }
