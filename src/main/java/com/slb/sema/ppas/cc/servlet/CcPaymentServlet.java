////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcPaymentServlet.Java
//      DATE            :       20-Feb-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1299/5099
//                              PRD_PPAK00_DEV_IN_30
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Payment screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcPaymentHistoryDetailScreenData;
import com.slb.sema.ppas.cc.screendata.CcPaymentScreenData;
import com.slb.sema.ppas.cc.service.CcPaymentDataSetResponse;
import com.slb.sema.ppas.cc.service.CcPaymentResultDataResponse;
import com.slb.sema.ppas.cc.service.CcPaymentService;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionDataResponse;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionService;
import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.PaprPaymentProfileDataSet;
import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.PaymentResultData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;



/** Payment Servlet class.
 */
public class CcPaymentServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcPaymentServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Payment Service to be used by this servlet. */
    private CcPaymentService i_ccPaymentService = null;

    /** The CC Recharge Division Service to be used by the servlet. */
    private CcRechargeDivisionService i_ccRechargeDivisionService = null;

    /** The default bank code obtained from the configuration. */
    private String i_defaultBankCode = null;

    /** The default payment address1 obtained from the configuration. */
    private String i_defaultPaymentAddress1 = null;

    /** The default payment address2 obtained from the configuration. */
    private String i_defaultPaymentAddress2 = null;

    /** The default payment address3 obtained from the configuration. */
    private String i_defaultPaymentAddress3 = null;

    /** The default postal code obtained from the configuration. */
    private String i_defaultPostalCode = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcPaymentServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise the class.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcPaymentService here...
        // unfortunately no GuiRequest is available.

        // Get the default payment description set in the configuration

        i_defaultBankCode = (String)i_guiContext.getAttribute(
            "com.slb.sema.ppas.gui.support.GuiContext.defaultBankCode");

        // Set the bankCode to blank if null.
        if (i_defaultBankCode == null)
        {
            i_defaultBankCode = "";
        }
        i_defaultPaymentAddress1 = (String)i_guiContext.getAttribute(
            "com.slb.sema.ppas.gui.support.GuiContext.defaultPaymentAddress1");

        i_defaultPaymentAddress2 = (String)i_guiContext.getAttribute(
            "com.slb.sema.ppas.gui.support.GuiContext.defaultPaymentAddress2");

        i_defaultPaymentAddress3 = (String)i_guiContext.getAttribute(
            "com.slb.sema.ppas.gui.support.GuiContext.defaultPaymentAddress3");

        i_defaultPostalCode = (String)i_guiContext.getAttribute(
            "com.slb.sema.ppas.gui.support.GuiContext.defaultPostalCode");

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the payment screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                 l_command           = null;
        String                 l_forwardUrl        = null;
        CcPaymentScreenData    l_paymentScreenData = null;
        GuiResponse            l_guiResponse       = null;
        CcPaymentHistoryDetailScreenData
                               l_paymentHistoryDetailScreenData = null;

        boolean                l_accessGranted     = false;
        GopaGuiOperatorAccessData l_gopaData       = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        if (i_ccPaymentService == null)
        {
            i_ccPaymentService = new CcPaymentService( p_guiRequest, i_logger, i_guiContext );
        }

        if (i_ccRechargeDivisionService == null)
        {
            i_ccRechargeDivisionService = new CcRechargeDivisionService(p_guiRequest, i_logger, i_guiContext);
        }

        // Create screen data objects.
        l_paymentScreenData = new CcPaymentScreenData( i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,          // Mandatory
                                      "",            // default
                                      new String [] {"APPLY_PAYMENT",
                                                     "GET_SCREEN_DATA",
                                                     "GET_HISTORY_DETAIL"});

            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            if ((l_command.equals("APPLY_PAYMENT")) && l_gopaData.makePaymentIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("GET_SCREEN_DATA")) && l_gopaData.hasPaymentsScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("GET_HISTORY_DETAIL")) && l_gopaData.hasPaymentsScreenAccess())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_paymentScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_paymentScreenData);
            }
            else
            {
                // Cust id will already be set up in the session from selection
                // on the account selection screen.

                if (l_command.equals("APPLY_PAYMENT"))
                {
                    setIsUpdateRequest(true);

                    makePayment( p_guiRequest, p_request, l_paymentScreenData);

                }
                else if (l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_paymentHistoryDetailScreenData =
                        new CcPaymentHistoryDetailScreenData( i_guiContext, p_guiRequest);

                    getPaymentHistoryDetail( p_guiRequest,
                                             p_request,
                                             l_paymentHistoryDetailScreenData);

                    l_paymentScreenData.setHistoryScreenData(l_paymentHistoryDetailScreenData);
                }

                if (!(l_command.equals("GET_HISTORY_DETAIL")))
                {
                    getPayments( p_guiRequest, l_paymentScreenData);
                }
            }
        } // end try
        catch (PpasServletException l_ppasSE)
        {
            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam( p_guiRequest, l_ppasSE );

            l_paymentScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
        } // end catch

        if (l_accessGranted)
        {
            if (l_paymentScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                if (l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_forwardUrl = "/jsp/cc/ccpaymentdetails.jsp";
                }
                else
                {
                    l_forwardUrl = new String("/jsp/cc/ccpayment.jsp");
                }
            }

            p_request.setAttribute( "p_guiScreenData", l_paymentScreenData );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13090, this,
                "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }

        return ( l_forwardUrl );
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_makePayment = "makePayment";
    /**
     * Method to retrieve the payment details from the screen and to make the
     * payment.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Payment screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void makePayment( GuiRequest          p_guiRequest,
                              HttpServletRequest  p_request,
                              CcPaymentScreenData p_screenData )
        throws PpasServletException
    {
        GuiResponse                  l_guiResponse                  = null;
        CcPaymentResultDataResponse  l_ccPaymentResultDataResponse  = null;
        String                       l_transProcCode                = null;
        Money                        l_amount                       = null;
        String                       l_transAmount                  = null;
        PpasCurrency                 l_transCurrency                = null;
        String                       l_paymentProfile               = null;
        String                       l_description1                 = null;
        String                       l_description2                 = null;
        String                       l_paymentAddress1              = "";
        String                       l_paymentAddress2              = "";
        String                       l_paymentAddress3              = "";
        String                       l_postalCode                   = "";
        String                       l_origin                       = null;
        String                       l_postalCodeFull               = null;
        String                       l_tempString                   = null;
        int                          l_takeOff;
        String                       l_promText                     = null;
        PpasDateTime                 l_currentTime                  = null;
        PaymentResultData            l_paymentResultData            = null;
        Money                        l_amountData                   = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 19173, this,
                "ENTERED " + C_METHOD_makePayment);
        }


        // Get params from request.

        // Get the Transaction amount from the request.
        l_transAmount = getValidParam( p_guiRequest,
                                       PpasServlet.C_FLAG_STRIP_PLUS_SIGN |
                                       PpasServlet.C_FLAG_DISALLOW_MINUS_SIGN,
                                       p_request,
                                       "p_amount",
                                       true, // parameter is mandatory ...
                                       "",   // so there is no default.
                                       PpasServlet.C_TYPE_NUMERIC_WITH_DECIMALS,
                                       20,
                                       PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        l_transCurrency = i_guiContext.getCurrency(getValidParam( p_guiRequest,
                                         0,
                                         p_request,
                                         "p_currency",
                                         true, // parameter is mandatory ...
                                         "",   // so there is no default.
                                         PpasServlet.C_TYPE_ANY,
                                         3,
                                         C_OPERATOR_EQUALS));


        try
        {
            l_amount = i_guiContext.getMoneyFormat().parse(l_transAmount, l_transCurrency);
        }
        catch (ParseException l_pe)
        {
            PpasServletException l_servE = new PpasServletException(
                          C_CLASS_NAME,
                          C_METHOD_makePayment,
                          13030,
                          this,
                          null,
                          0,
                          ServletKey.get().parseMoneyError(l_transAmount, l_transCurrency),
                          l_pe);

            i_logger.logMessage (l_servE);
            throw (l_servE);
        }

        l_paymentProfile = getValidParam( p_guiRequest,
                                          0,
                                          p_request,
                                          "p_paymentProfile",
                                          true, // parameter is mandatory ...
                                          "",   // so there is no default.
                                          PpasServlet.C_TYPE_ANY);

        // Get the Transaction Proc. Code from the request.
        // Note: in Payments paymentType is the transProcCode (i.e. 'C' from
        //       the 'PYM' misc_table field in the srva_misc_codes table) this
        //       will populate pay_method in the cust_payments database table.
        l_transProcCode = getValidParam(
                                  p_guiRequest,
                                  0,
                                  p_request,
                                  "p_paymentType",
                                  true, // parameter is mandatory ...
                                  "",   // so there is no default.
                                  PpasServlet.C_TYPE_ANY);

        l_origin = i_defaultBankCode;

        // Get the accept location from the request.
        l_description1 = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_description1",
                                    false, // parameter is not mandatory.
                                    "", // default.
                                    PpasServlet.C_TYPE_ANY,
                                    40,
                                    PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        l_description2 = getValidParam(
                                    p_guiRequest,
                                    PpasServlet.C_FLAG_ALLOW_BLANK,
                                    p_request,
                                    "p_description2",
                                    false, // parameter is not mandatory.
                                    "", // default.
                                    PpasServlet.C_TYPE_ANY,
                                    40,
                                    PpasServlet.C_OPERATOR_LESS_THAN_OR_EQUAL);

        // If the description has not been populated then
        // use the default values.
        if ( ( l_description1.length() == 0) &&
             ( l_description2.length() == 0) )
        {
            l_paymentAddress1 = i_defaultPaymentAddress1;
            l_paymentAddress2 = i_defaultPaymentAddress2;
            l_paymentAddress3 = i_defaultPaymentAddress3;
            l_postalCode      = i_defaultPostalCode;
        }
        else
        {
            l_paymentAddress1 = l_description1;
            l_paymentAddress2 = l_description2;
        }

        // Pad out the l_postalCode to be ten chars.
        if ( l_postalCode.length() < 10 )
        {
            l_tempString     = "          ";
            l_takeOff        = 10 - ( 10 - l_postalCode.length() );
            l_postalCodeFull = l_postalCode + l_tempString.substring(l_takeOff);
        }
        else
        {
            l_postalCodeFull = l_postalCode;
        }

        // Attempt the payment.
        l_ccPaymentResultDataResponse =
            i_ccPaymentService.makePayment( p_guiRequest,
                                            i_timeout,
                                            l_transProcCode,
                                            l_amount,
                                            l_paymentProfile,
                                            l_origin,
                                            l_paymentAddress1,
                                            l_paymentAddress2,
                                            l_paymentAddress3,
                                            l_postalCodeFull);

        p_screenData.setPaymentResultData(l_ccPaymentResultDataResponse
                                            .getPaymentResultData());

        l_guiResponse = l_ccPaymentResultDataResponse;

        // Add the response to the screen data object as a normal
        // update type response.

        p_screenData.addUpdateResponse( p_guiRequest,
                                        l_guiResponse );

        l_currentTime = DatePatch.getDateTimeNow();

        if (!p_screenData.hasUpdateError())
        {
            p_screenData.setSuccessfulUpdate(true);

            l_paymentResultData = p_screenData.getPaymentResultData();

            if ( ( l_paymentResultData.getPromValue() != null ) &&
                 ( !l_paymentResultData.getPromValue().equals("0")))
            {
                l_amountData = l_paymentResultData.getPromValue();

                l_promText = ", with promotion of " +
                               i_guiContext.getMoneyFormat().format(l_amountData) + " " +
                               l_amountData.getCurrency().getCurrencyCode();
            }
            else
            {
                l_promText = "";
            }

            p_screenData.setInfoText(
                                "Successful Account Refill at "
                                + l_currentTime +
                                  l_promText );
        }
        else
        {
            p_screenData.setInfoText(
                           "Failed Account Refill at " + l_currentTime);

            try
            {
                // Set old payments values, so can be re-displayed.
                p_screenData.setOldPayAmount(
                    i_guiContext.getMoneyFormat().parse(
                                p_request.getParameter("p_displayAmount"),
                                l_transCurrency));

            }
            catch (ParseException l_pe)
            {
                PpasServletException l_servE = new PpasServletException(
                              C_CLASS_NAME,
                              C_METHOD_makePayment,
                              13030,
                              this,
                              null,
                              0,
                              ServletKey.get().parseMoneyError(
                                                  p_request.getParameter("p_displayAmount"),
                                                  l_transCurrency),
                              l_pe);

                i_logger.logMessage (l_servE);
                throw (l_servE);
            }

            p_screenData.setOldPayDescription1(
                            p_request.getParameter("p_description1"));
            p_screenData.setOldPayDescription2(
                            p_request.getParameter("p_description2"));
            p_screenData.setOldPayType(
                            p_request.getParameter("p_paymentType"));
            p_screenData.setOldPaymentProfile(
                            p_request.getParameter("p_paymentProfile"));
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 18333, this,
                "LEAVING " + C_METHOD_makePayment);
        }

        return;

    } // end of makePayment() method.


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPayment = "getPayment";
    /**
     * Method to retrieve the payment history and display on the payment screen.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Payment screen.
     * @throws PpasServletException If an error occurs processing this request.
     */
    private void getPayments( GuiRequest           p_guiRequest,
                              CcPaymentScreenData  p_screenData )
        throws PpasServletException
    {
        CcPaymentDataSetResponse    l_ccPaymentDataSetResponse = null;
        MiscCodeDataSet             l_miscCodeDataSet          = null;
        Market                      l_market                   = null;
        GuiSession                  l_guiSession               = null;
        CufmCurrencyFormatsDataSet  l_cufmData                 = null;
        PaprPaymentProfileDataSet   l_paprDataSet              = null;
        CdgrAcceCardGroupDataSet    l_voucherGroupDetails      = null;

        l_guiSession = (GuiSession)p_guiRequest.getSession();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 47829, this,
                "ENTERED " + C_METHOD_getPayment);
        }

        // Get the payment data.
        l_ccPaymentDataSetResponse =
              i_ccPaymentService.getPayments( p_guiRequest,
                                              i_timeout );

        // Get the cached currency info.
        l_cufmData = i_configService.getAvailableCurrencyFormats();

        p_screenData.setCurrencyDataSet(l_cufmData);

        l_paprDataSet = i_configService.getActivePaymentProfiles(p_guiRequest);
        p_screenData.setPaymentProfiles( l_paprDataSet
                                         .getActiveArray());

        l_voucherGroupDetails = i_configService.getAvailableCardGroups();
        p_screenData.setVoucherGroupDetails( l_voucherGroupDetails );

        p_screenData.setPaymentDataSet( l_ccPaymentDataSetResponse.getPaymentDataSet() );

        l_market = l_guiSession.getCsoMarket();

        l_miscCodeDataSet = i_configService.getAvailablePaymentTypes(l_market);

        p_screenData.setPaymentTypes( l_miscCodeDataSet );

        // Add the response to the screen data object as a normal
        // retrieval type response.

        p_screenData.addRetrievalResponse(p_guiRequest, l_ccPaymentDataSetResponse);

        if (l_ccPaymentDataSetResponse.isSuccess())
        {
            p_screenData.setPaymentDataSet(l_ccPaymentDataSetResponse.getPaymentDataSet());
        }

        if (!(p_screenData.hasRetrievalError()))
        {
            p_screenData.setPreferredCurrency( ((GuiSession)p_guiRequest.getGuiSession())
                                                .getPreferredCurrency());
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 87531, this,
                "LEAVING " + C_METHOD_getPayment);
        }

        return;

    } // end of getPayments() method.


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPaymentHistoryDetail =
                                        "getPaymentHistoryDetail";
    /** Get payment history details of recharge division.
     * 
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Payment History screen.
     * @throws PpasServletException If an error occurs processing this request.
     */
    private void getPaymentHistoryDetail(
                     GuiRequest                        p_guiRequest,
                     HttpServletRequest                p_request,
                     CcPaymentHistoryDetailScreenData  p_screenData)
        throws PpasServletException
    {
        PpasDateTime          l_dateTime              = null;
        String                l_dateTimeStr           = null;
        String                l_originalAmount        = null;
        String                l_currency              = null;
        String                l_paymentAmount         = null;
        String                l_group                 = null;
        String                l_type                  = null;
        String                l_linkId                = null;
        String                l_bankId                = null;
        String                l_bankName              = null;
        String                l_source                = null;
        String                l_info1                 = null;
        String                l_info2                 = null;
        String                l_info3                 = null;
        String                l_info4                 = null;
        String                l_hasDivStr             = null;
        boolean               l_hasDivision           = false;
        CcRechargeDivisionDataResponse l_ccDivisionDataResponse = null;
        String                l_appliedCurrency       = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 17829, this,
                "ENTERED " + C_METHOD_getPaymentHistoryDetail);
        }

        // Get the validated parameters from the request.

        l_dateTimeStr =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_dateTime",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_DATE);

        l_dateTime = new PpasDateTime (l_dateTimeStr);

        l_originalAmount =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_originalAmount",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_currency =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_currency",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY,
                              3,
                              C_OPERATOR_EQUALS);

        l_paymentAmount =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_paymentAmount",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_appliedCurrency =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_appliedCurrency",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY,
                              3,
                              C_OPERATOR_EQUALS);

        l_group =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_group",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_type =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_type",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_bankId =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_bankId",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_bankName =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_bankName",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_source =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_source",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_info1 =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_info1",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_info2 =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_info2",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_info3 =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_info3",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_info4 =
                getValidParam(p_guiRequest,
                              C_FLAG_ALLOW_BLANK,
                              p_request,
                              "p_info4",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_hasDivStr =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_hasDivision",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_BOOLEAN);

        if (l_hasDivStr.equalsIgnoreCase("TRUE"))
        {
            l_hasDivision = true;

            l_linkId =
                    getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_linkId",
                                  true,  // parameter is mandatory ...
                                  "",    // so there is no default
                                  PpasServlet.C_TYPE_NUMERIC);

            // Get the payment data.
            l_ccDivisionDataResponse = i_ccRechargeDivisionService.getDivision(
                                                  p_guiRequest,
                                                  i_timeout,
                                                  new Long(l_linkId).longValue(),
                                                  DivisionData.C_CREDIT_TYPE_PAYMENT);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addRetrievalResponse(p_guiRequest, l_ccDivisionDataResponse);
            if (l_ccDivisionDataResponse.isSuccess())
            {
                p_screenData.setDivisionData(l_ccDivisionDataResponse.getDivisionData());
            }
        }
        else
        {
            l_hasDivision = false;
        }

        p_screenData.setRefillDateTime( l_dateTime );
        p_screenData.setOriginalAmount( l_originalAmount );
        p_screenData.setOriginalCurrency(l_currency );
        p_screenData.setAppliedAmount(  l_paymentAmount );
        p_screenData.setAppliedCurrency(l_appliedCurrency);
        p_screenData.setGroup(          l_group );
        p_screenData.setType(           l_type );
        p_screenData.setBankId(         l_bankId );
        p_screenData.setBankName(       l_bankName );
        p_screenData.setSource(         l_source );
        p_screenData.setInfo1(          l_info1 );
        p_screenData.setInfo2(          l_info2 );
        p_screenData.setInfo3(          l_info3 );
        p_screenData.setInfo4(          l_info4 );
        p_screenData.setHasDivision(    l_hasDivision );

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 17531, this,
                "LEAVING " + C_METHOD_getPaymentHistoryDetail);
        }

        return;

    } // end of method getPaymentHistoryDetail

} // end of CcPaymentServlet class
