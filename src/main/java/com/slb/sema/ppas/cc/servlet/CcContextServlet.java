////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcContextServlet.Java
//      DATE            :       27th Feb 2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              context bar of the Customer Care GUI.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 10/09/02 | M Hickman  | We always pass a null value for | PpaLon#1534/6499
//          |            | currency to the balance service |
//          |            | from now on because we may not  |
//          |            | know it when we get the balance.|
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.awt.Color;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

import com.slb.sema.ppas.cc.screendata.CcContextBalanceScreenData;
import com.slb.sema.ppas.cc.service.CcBalanceDataResponse;
import com.slb.sema.ppas.cc.service.CcBalanceService;
import com.slb.sema.ppas.common.churn.ChurnIndicatorData;
import com.slb.sema.ppas.common.churn.ChurnIndicatorData.PointData;
import com.slb.sema.ppas.common.dataclass.BalanceEnquiryResultData;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/** This Servlet processes requests from the Context Bar frames and returns
 *  the data required by these frames. The context bar comprises of three
 *  frames that display basic account information (contextCustomer), the
 *  customer's balance (contextBalance) and status information for the
 *  customer (contextStatus).
 */
public class CcContextServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcContextServlet";
    
    /** The colour brown. */
    private static final Color C_COLOR_BROWN = new Color(222,214,181);

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Balance Service to be used by this servlet. */
    private CcBalanceService i_ccBalanceService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructs a Context Servlet object to handle requests from the
     *  Customer Care context bar frames.
     */
    public CcContextServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 82000, this,
                           "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 82090, this,
                           "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Performs initialisation specific to this servlet. At present,
     *  this servlet has no specific initialisation.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 45200, this,
                           "Entered doInit().");
        }

        // Should probably be constructing CcBalanceService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 45290, this,
                           "Leaving doInit().");
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to handle requests from the Context Bar. The request
     *  may arise from the contextCustomer frame (command=GET_CONTEXT_CUSTOMER),
     *  from the contextBalance (command=GET_CONTEXT_BALANCE) or from the
     *  contextStatus frame (command=GET_CONTEXT_STATUS). The data required
     *  be the requesting frame is obtained and attached to the request as an
     *  appropriate 'screen data' object and the request is forwarded on to
     *  the frame from which it originated.
     * @param p_request      HTTP request.
     * @param p_response     HTTP response.
     * @param p_httpSession  Current HttpSession.
     * @param p_guiRequest   GuiRequest object.
     * @return forwarding URL.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                             l_command ;
        String                             l_forwardUrl  = null;
        CcContextBalanceScreenData         l_balScreenData  = null;
        CcBalanceDataResponse              l_balResponse = null;
        BalanceEnquiryResultData           l_balanceData;

        JFreeChart                         l_chart;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 45300, this,
                           "ENTERED " + C_METHOD_doService);
        }

        // Construct CcBalanceService if it does not already exist.
        if (i_ccBalanceService == null)
        {
            i_ccBalanceService = new CcBalanceService (p_guiRequest, 0, i_logger, i_guiContext);
        }

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "command",
                                      true,     // Mandatory ...
                                      "",       // ... so no default value.
                                      new String [] {"GET_CONTEXT_BALANCE",
                                                     "GET_REFILL_POPUP",
                                                     "GET_CHART",
                                                     "GET_CHURN_REASON"});

            if (l_command.equals("GET_CONTEXT_BALANCE"))
            {
                // Set the forward URL to the context balance JSP
                l_forwardUrl = "/jsp/cc/cccontextbalance.jsp";

                // Create screen data object for the context balance JSP

                l_balScreenData = new CcContextBalanceScreenData (i_guiContext, p_guiRequest);

                // Call the customer care balance service to obtain the
                // balance for the customer defined in the request only
                // if they are not disconnected.
                if (!l_balScreenData.subIsDisconnected())
                {
                    // PpaLon#1534/6499 - Don't bother getting currency out of the
                    // session object. We can't guarantee that it's available here
                    // so we do it in the internal service instead.
                    l_balResponse = i_ccBalanceService.getBalance(p_guiRequest, i_timeout);
                    
                    // Add the response to the screen data
                    l_balScreenData.addRetrievalResponse(p_guiRequest, l_balResponse);

                    if (l_balResponse.isSuccess())
                    {
                        // Get the balance data from the response and store it
                        // in the screen data object
                        l_balanceData = l_balResponse.getData();
                        l_balScreenData.setBalanceData (l_balanceData);
                        
                        ChurnIndicatorData l_churnData = l_balResponse.getChurnData();
                        l_balScreenData.setChurnData(l_churnData);
                        
                        GuiSession l_guiSession = (GuiSession)p_guiRequest.getSession();
                        l_guiSession.setChurnIndicatorData(l_churnData);
                    }

                    l_balScreenData.setBalanceTime (p_guiRequest, i_logger);
                }

                // Attach the screen data to the HTTP request that will be
                // forwarded to the contextBalance JSP.
                p_request.setAttribute ("p_guiScreenData", l_balScreenData);
            }
            else if (l_command.equals("GET_REFILL_POPUP"))
            {
                GuiSession         l_guiSession = (GuiSession)p_guiRequest.getSession();
                ChurnIndicatorData l_churnData = l_guiSession.getChurnIndicatorData();
                
                if (WebDebug.on)
                {
                    WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_TRACE,
                                   p_guiRequest, C_CLASS_NAME, 45350, this,
                                   "Customer churn data: " + l_churnData);
                }
                
                l_forwardUrl = "/jsp/cc/ccchurndetails.jsp";
            }
            else if (l_command.equals("GET_CHART"))
            {
                GuiSession         l_guiSession = (GuiSession)p_guiRequest.getSession();
                ChurnIndicatorData l_churnData = l_guiSession.getChurnIndicatorData();
                OutputStream       l_out = p_response.getOutputStream();
                
                // Already checked that there is sufficient data (otherwise we wouldn't get this far).
                l_chart = createChart(createDataSet(l_churnData));
                
                p_response.setContentType("image/png");
                ChartUtilities.writeChartAsPNG(l_out, l_chart, 500,500);
            }
            else if (l_command.equals("GET_CHURN_REASON"))
            {
                // Some HTML to display details about the reason for displaying the churn indicator icon.
                GuiSession         l_guiSession = (GuiSession)p_guiRequest.getSession();
                ChurnIndicatorData l_churnData = l_guiSession.getChurnIndicatorData();
                PrintWriter        l_writer = new PrintWriter(p_response.getWriter());
                String             l_nl = System.getProperty("line.separator"); // New line terminator.
                String             l_reason = l_churnData.getChurnAnalysis().getReason();

                l_reason = l_reason.replaceAll(l_nl, "</p><p>");
                
                p_response.setContentType("text/html");
                l_writer.print("<html>");
                l_writer.print("<head>");
                l_writer.print("<title>Churn Analysis</title>");
                l_writer.print("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">");
                l_writer.print("<title>Refill Analysis</title>");
                l_writer.print("<link rel=\"stylesheet\" href=\"/ascs/gui/html/styles/ppas.css\" type=\"text/css\">");
                l_writer.print("<script language=\"javascript1.2\">");

                l_writer.print("var l_returnObject = new Object();");
                l_writer.print("l_returnObject.RETURN_ACTION = window.dialogArguments.openerTop.G_MODAL_ACTION_NONE;");
                l_writer.print("window.returnValue = l_returnObject;");
                l_writer.print("</script>");
                l_writer.print("</head>");
                l_writer.print("<body>");
                l_writer.print("<table class=\"historytable\" height=\"100%\">");
                l_writer.print("<tr>");
                l_writer.print("<td>");
                l_writer.print("<table class=\"customertable\" cellpadding=\"3\" cellspacing=\"0\">");
                l_writer.print("<tr>");
                l_writer.print("<td class =\"historycellfields\" width=\"400\" height=\"400\" align=\"left\">");
                l_writer.print("<p>");
                l_writer.print(l_reason);
                l_writer.print("</p>");
                l_writer.print("</td>");
                l_writer.print("</tr>");
                l_writer.print("</table>");
                l_writer.print("</td>");
                l_writer.print("</table>");
                l_writer.print("</body>");
                l_writer.flush();
                l_writer.close();
            }
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 86000, this,
                               "Caught exception:\n" + l_pSE);
            }

            l_forwardUrl = "/jsp/cc/ccerror.jsp";

        }
        catch (Exception e)
        {
            System.err.println(e.toString());
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 13000, this,
                           "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------
    
    /**
     * Creates a chart.
     *
     * @param p_dataset  a dataset.
     *
     * @return A chart.
     */
    private static JFreeChart createChart(XYDataset p_dataset)
    {
        XYPlot l_plot = null;
        
        JFreeChart l_chart = ChartFactory.createTimeSeriesChart(
            "Cumulative Refill Value vs Time", // title
            "Time",                            // x-axis label
            "Cumulative Refill Value",         // y-axis label
            p_dataset,                           // data
            true,                              // create legend?
            true,                              // generate tooltips?
            false                              // generate URLs?
        );

        l_chart.setBackgroundPaint(new Color(0xffffe7));

        l_plot = (XYPlot) l_chart.getPlot();
        
        l_plot.setBackgroundPaint(Color.WHITE);
        
        // TODO: Set an ASCS image l_plot.setBackgroundImage()?
        l_plot.setDomainGridlinePaint(Color.white);
        l_plot.setRangeGridlinePaint(Color.white);
        l_plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        l_plot.setDomainCrosshairVisible(true);
        l_plot.setRangeCrosshairVisible(true);

        XYItemRenderer l_itemRenderer = l_plot.getRenderer();
        if (l_itemRenderer instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer l_lineRenderer = (XYLineAndShapeRenderer) l_itemRenderer;
            l_lineRenderer.setBaseShapesVisible(true);
            l_lineRenderer.setBaseShapesFilled(true);
            l_lineRenderer.setSeriesPaint(0, new Color(0xadd6d6));    // green (button colour)
            l_lineRenderer.setSeriesPaint(1, new Color(222,214,181)); // brown
            l_lineRenderer.setSeriesPaint(2, new Color(222,214,181)); // brown
            l_lineRenderer.setSeriesPaint(3, new Color(222,214,181)); // brown
            
            l_lineRenderer.setSeriesPaint(0, Color.BLACK);
            l_lineRenderer.setSeriesPaint(1, Color.GREEN);
            l_lineRenderer.setSeriesPaint(2, C_COLOR_BROWN);
            l_lineRenderer.setSeriesPaint(3, C_COLOR_BROWN);
        }

        DateAxis l_axis = (DateAxis) l_plot.getDomainAxis();
        l_axis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yy"));
        l_axis.setVerticalTickLabels(true);

        return l_chart;
    }
    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createDataSet = "createDataSet";
    /**
     * Creates a dataset, consisting of series of refill data over the analysis period.
     * @param p_churnData ChurnData for this subscriber.
     * @return The dataset.
     */
    private XYDataset createDataSet(ChurnIndicatorData p_churnData)
    {   
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           C_CLASS_NAME, 29000, this,
                           "Entered " + C_METHOD_createDataSet);
        }
        
        TimeSeriesCollection l_dataset = new TimeSeriesCollection();
        TimeSeries           l_subsCumSpend = new TimeSeries("Subs Cumulative Spend", Millisecond.class);
        TimeSeries           l_operAve  = new TimeSeries("Operator Average", Millisecond.class);
        TimeSeries           l_operHigh = new TimeSeries("Operator High", Millisecond.class);
        TimeSeries           l_operLow = new TimeSeries("Operator Low", Millisecond.class);
        
        addPoints(p_churnData.getCumulativePoints(), l_subsCumSpend);
        l_dataset.addSeries(l_subsCumSpend);
        
        if (p_churnData.isHighLowAverageSet())
        {
            addPoints(p_churnData.getOperatorAverage(),  l_operAve);
            addPoints(p_churnData.getOperatorHigh(),     l_operHigh);
            addPoints(p_churnData.getOperatorLow(),      l_operLow);
            l_dataset.addSeries(l_operAve);
            l_dataset.addSeries(l_operHigh);
            l_dataset.addSeries(l_operLow);
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END,
                           C_CLASS_NAME, 29090, this,
                           "Leaving " + C_METHOD_createDataSet);
        }

        return l_dataset;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addPoints = "addPoints";
    /**
     * Add the supplied points to the supplied time series.
     * @param p_pd
     * @param p_timeSeries
     */
    private void addPoints(PointData[] p_pd, TimeSeries p_timeSeries)
    {
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           C_CLASS_NAME, 47100, this,
                           "ENTERED " + C_METHOD_addPoints + "with point data: " + p_pd + 
                               ", TimeSeries: " + p_timeSeries);
        }
        
        MoneyFormat l_moneyFormat = i_guiContext.getMoneyFormat();
        
        for (int i=0; i < p_pd.length; i++)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_TRACE,
                           C_CLASS_NAME, 47110, this,
                           "ENTERED " + C_METHOD_addPoints);
            
            p_timeSeries.add(
                new Millisecond(p_pd[i].getDateTime()),
                Double.parseDouble(l_moneyFormat.format(p_pd[i].getAmount())));
        }
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END,
                           C_CLASS_NAME, 47120, this,
                           "LEAVING " + C_METHOD_addPoints);
        }
    }
}
