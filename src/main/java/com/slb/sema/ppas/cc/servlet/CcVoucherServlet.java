////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcVoucherServlet.Java
//      DATE            :       07-Feb-2002
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       PpaLon#1237/5475
//                              PRD_PPAK00_DEV_IN_034
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Voucher screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 12/12/05 |K Goswami   | Added code for voucher history  | PpaLon#1892/7603
//          |            | popup screen                    | CA#39
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcVoucherRefillHistoryDetailScreenData;
import com.slb.sema.ppas.cc.screendata.CcVoucherScreenData;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcFailedRechargeDataSetResponse;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionDataResponse;
import com.slb.sema.ppas.cc.service.CcRechargeDivisionService;
import com.slb.sema.ppas.cc.service.CcRechargeResultDataResponse;
import com.slb.sema.ppas.cc.service.CcVoucherDataResponse;
import com.slb.sema.ppas.cc.service.CcVoucherDataSetResponse;
import com.slb.sema.ppas.cc.service.CcVoucherHistoryDataResponse;
import com.slb.sema.ppas.cc.service.CcVoucherService;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.dataclass.DivisionData;
import com.slb.sema.ppas.common.dataclass.VoucherData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.featurelicence.FeatureLicenceCache;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
 */
public class CcVoucherServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcVoucherServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Voucher Service to be used by this servlet. */
    private CcVoucherService          i_ccVoucherService = null;

    /** The CCAccount Service to be used by this servlet. */
    private CcAccountService          i_ccAccountService = null;

    /** The CC Recharge Division Service to be used by this servlet. */
    private CcRechargeDivisionService i_ccRechargeDivisionService = null;

    /** Has Enhanced Value Voucher Feature Licence. */
    private boolean i_hasEnhancedValueVoucherFeatureLicence = false;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcVoucherServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Initialise this class.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 20000, this,
                "Entered doInit().");
        }

        // Should probably be constructing CcVoucherService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 20090, this,
                "Leaving doInit().");
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the voucher screen service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                 l_command           = null;
        String                 l_forwardUrl        = null;
        CcVoucherScreenData    l_voucherScreenData = null;
        GuiResponse            l_guiResponse       = null;
        int                    l_serialLength;
        CcVoucherRefillHistoryDetailScreenData    l_voucherHistoryDetailScreenData = null;
        boolean                l_accessGranted     = false;
        GopaGuiOperatorAccessData l_gopaData       = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcXyzServices if they do not already exist ... this
        // will be move into doInit later!! <<< ??? >>>
        if (i_ccVoucherService == null)
        {
            i_ccVoucherService = new CcVoucherService(p_guiRequest, i_logger, i_guiContext);
        }
        if (i_ccAccountService == null)
        {
            i_ccAccountService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        if (i_ccRechargeDivisionService == null)
        {
            i_ccRechargeDivisionService = new CcRechargeDivisionService(p_guiRequest, i_logger, i_guiContext);
        }

        l_voucherScreenData = new CcVoucherScreenData(i_guiContext, p_guiRequest);
        
        // rich
        if (i_configService.getCompanyConfigData().getEvdbUsed())
        {
            l_voucherScreenData.setEvdbUsed(true);
        }

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,      // Mandatory
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "VALIDATE_VOUCHER",
                                                     "VOUCHER_HISTORY_ENQUIRY",
                                                     "UPDATE_VOUCHER",
                                                     "RECHARGE_ACCOUNT",
                                                     "VALUE_RECHARGE_ACCOUNT",
                                                     "GET_HISTORY_DETAIL"});

            // Determine if the CSO has privilege to access the requested screen or function
            l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

            i_hasEnhancedValueVoucherFeatureLicence = getEnhancedValueVoucherFeatureLicence();

            if ((l_command.equals("GET_SCREEN_DATA") || l_command.equals("GET_HISTORY_DETAIL")) &&
                 l_gopaData.hasVouchersScreenAccess())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("VALIDATE_VOUCHER")) &&
                 l_gopaData.voucherEnquiryIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("VOUCHER_HISTORY_ENQUIRY")) &&
                    l_gopaData.voucherEnquiryIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("UPDATE_VOUCHER")) &&
                 l_gopaData.voucherUpdateIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("RECHARGE_ACCOUNT")) &&
                 l_gopaData.accountRechargeIsPermitted())
            {
                l_accessGranted = true;
            }
            else if ((l_command.equals("VALUE_RECHARGE_ACCOUNT")) &&
                 l_gopaData.servClassChangeIsPermitted())
            {
                l_accessGranted = true;
            }

            if (!l_accessGranted)
            {
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_voucherScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
                l_voucherHistoryDetailScreenData =
                    new CcVoucherRefillHistoryDetailScreenData(i_guiContext, p_guiRequest);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_voucherScreenData);
            }
            else
            {
                l_serialLength = i_configService.getSerialNumberLength();

                if (l_command.equals("RECHARGE_ACCOUNT"))
                {
                    setIsUpdateRequest(true);

                    doRecharge(p_guiRequest, p_request, l_voucherScreenData);
                }
                else if (l_command.equals("VALUE_RECHARGE_ACCOUNT"))
                {
                    setIsUpdateRequest(true);

                    doValueRecharge(p_guiRequest, p_request, l_voucherScreenData);
                }
                else if (l_command.equals("VALIDATE_VOUCHER"))
                {
                    getVoucher(p_guiRequest, p_request, l_voucherScreenData, l_serialLength);
                }
                else if (l_command.equals("VOUCHER_HISTORY_ENQUIRY"))
                {   
                    getVoucherHistory(p_guiRequest, p_request, l_voucherScreenData, l_serialLength);
                }
                else if (l_command.equals("UPDATE_VOUCHER"))
                {
                    updateVoucher(p_guiRequest, p_request, l_voucherScreenData, l_serialLength);

                    // Do not retrieve voucher data if the serial number is invalid.
                    if (!(l_voucherScreenData.getUpdateErrorKey().equals(GuiResponse.C_KEY_VOUCHER_NOT_FOUND)))
                    {
                        getVoucher(p_guiRequest, p_request, l_voucherScreenData, l_serialLength);
                    }
                }
                else if (l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_voucherHistoryDetailScreenData =
                        new CcVoucherRefillHistoryDetailScreenData(i_guiContext, p_guiRequest);

                    getVoucherRefillHistoryDetail( p_guiRequest,
                                             p_request,
                                             l_voucherHistoryDetailScreenData);

                    l_voucherScreenData.setHistoryScreenData(l_voucherHistoryDetailScreenData);
                }

                if (!(l_command.equals("GET_HISTORY_DETAIL")))
                {
                    getVoucherScreenDetails(p_guiRequest, l_voucherScreenData, l_serialLength);
                }
            }
        }
        catch (PpasServletException l_ppasSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_voucherScreenData.addRetrievalResponse(p_guiRequest,
                                                     l_guiResponse);
        }

        if (l_accessGranted)
        {

            if (l_voucherScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                if (l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_forwardUrl = "/jsp/cc/ccvoucherdetails.jsp";
                }
                else
                {
                    l_forwardUrl = "/jsp/cc/ccvouchers.jsp";
                }
            }

            p_request.setAttribute("p_guiScreenData", l_voucherScreenData);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 83080, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }    

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doRecharge = "doRecharge";
    /** Perform a recharge on the subscriber using the activation number in the
     *  request.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void doRecharge(GuiRequest          p_guiRequest,
                            HttpServletRequest  p_request,
                            CcVoucherScreenData p_screenData)
    {
        CcRechargeResultDataResponse l_ccRechargeResultDataResponse = null;
        String                       l_activationNumber             = null;
        GuiResponse                  l_guiResponse                  = null;
        PpasDateTime                 l_currentTime                  = null;
        String                       l_promText                     = null;
        Money                        l_amountData                   = null;
        String l_errorMessage;
        String l_successMessage;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 19173, this,
                "ENTERED " + C_METHOD_doRecharge);
        }

        if ( i_hasEnhancedValueVoucherFeatureLicence )
        {
            l_errorMessage = "Failed Voucher Refill at ";
            l_successMessage = "Successful Voucher Refill at ";
        }
        else
        {
            l_errorMessage = "Failed Standard Refill at ";
            l_successMessage = "Successful Standard Refill at ";
        }
        try
        {
            l_activationNumber =
                    getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_activationNumber",
                                  true,  // parameter is mandatory
                                  "",    // so there is no default
                                  PpasServlet.C_TYPE_NUMERIC,
                                  20,     // parameter must be <= 20
                                  C_OPERATOR_LESS_THAN_OR_EQUAL);
        }
        catch (PpasServletException l_ppasSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"Standard Refill"},
                                GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            l_ccRechargeResultDataResponse = i_ccVoucherService.rechargeAccount(p_guiRequest,
                                                                                i_timeout,
                                                                                l_activationNumber);

            l_guiResponse = l_ccRechargeResultDataResponse;
        }

        p_screenData.addUpdateResponse(p_guiRequest,
                                       l_guiResponse);

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            // Recharge failed, so get the activation number with no validation,
            // for redisplay on the screen.
            p_screenData.setVoucherActivationNumber(p_request.getParameter("p_activationNumber"));
            p_screenData.setRechargeType(VoucherData.C_STANDARD_RECHARGE_TYPE);

            p_screenData.setRechargeFailed(true);
            p_screenData.setInfoText(l_errorMessage + l_currentTime);
            p_screenData.setIvrUnbarDone(false);
        }
        else
        {
            p_screenData.setRechargeDone(true);

            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_HIGH,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    p_guiRequest,
                    C_CLASS_NAME, 88888, this,
                    "l_ccRechargeResultDataResponse " + l_ccRechargeResultDataResponse);
            }

            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_HIGH,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    p_guiRequest,
                    C_CLASS_NAME, 88888, this,
                    "l_ccRechargeResultDataResponse.getRechargeResultData() " +
                        l_ccRechargeResultDataResponse.getRechargeResultData());
            }

            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_HIGH,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_REQUEST,
                    p_guiRequest,
                    C_CLASS_NAME, 88888, this,
                    "l_ccRechargeResultDataResponse.getRechargeResultData().getPromValue() " +
                        l_ccRechargeResultDataResponse.getRechargeResultData().getPromValue());
            }

            l_amountData = l_ccRechargeResultDataResponse.getRechargeResultData().getPromValue();

            if (l_amountData != null)
            {
                l_promText = ", with promotion of " +
                    p_guiRequest.getContext().getMoneyFormat().format(l_amountData) + " " +
                    l_amountData.getCurrency().getCurrencyCode();
            }
            else
            {
                l_promText = "";
            }

            p_screenData.setInfoText(l_successMessage + l_currentTime + l_promText);

            p_screenData.setIvrUnbarDone(true);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 18333, this,
                "LEAVING " + C_METHOD_doRecharge);
        }

        return;

    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doValueRecharge = "doValueRecharge";
    /** Perform a value voucher recharge on the subscriber using the activation
     *  number in the request.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     */
    private void doValueRecharge(GuiRequest          p_guiRequest,
                                 HttpServletRequest  p_request,
                                 CcVoucherScreenData p_screenData)
    {
        CcRechargeResultDataResponse l_ccRechargeResultDataResponse = null;
        String                       l_activationNumber             = null;
        GuiResponse                  l_guiResponse                  = null;
        PpasDateTime                 l_currentTime                  = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 30000, this,
                "ENTERED " + C_METHOD_doValueRecharge);
        }

        try
        {
            l_activationNumber =
                    getValidParam(p_guiRequest,
                                  0,
                                  p_request,
                                  "p_valueActivationNumber",
                                  true,  // parameter is mandatory
                                  "",    // so there is no default
                                  PpasServlet.C_TYPE_NUMERIC,
                                  20,     // parameter must be <= 20
                                  C_OPERATOR_LESS_THAN_OR_EQUAL);
        }
        catch (PpasServletException l_ppasSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"Value Voucher Refill"},
                                GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            l_ccRechargeResultDataResponse = i_ccVoucherService.valueRechargeAccount(
                                     p_guiRequest,
                                     i_timeout,
                                     l_activationNumber);

            l_guiResponse = l_ccRechargeResultDataResponse;
        }

        p_screenData.addUpdateResponse(p_guiRequest,
                                       l_guiResponse);

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            p_screenData.setVoucherActivationNumber(p_request.getParameter("p_valueActivationNumber"));
            p_screenData.setRechargeType(VoucherData.C_VALUE_RECHARGE_TYPE);

            p_screenData.setRechargeFailed(true);
            p_screenData.setInfoText("Failed Value Voucher Refill at " + l_currentTime);
        }
        else
        {
            p_screenData.setRechargeDone(true);
            p_screenData.setInfoText("Successful Value Voucher Refill at " + l_currentTime);
        }


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 30020, this,
                "LEAVING " + C_METHOD_doValueRecharge);
        }

        return;
    } 

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucher = "getVoucher";
    /** Get voucher details using the serial number on the request.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     * @param p_serialLength Length of serial number.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void getVoucher(GuiRequest           p_guiRequest,
                            HttpServletRequest   p_request,
                            CcVoucherScreenData  p_screenData,
                            int                  p_serialLength)
        throws PpasServletException
    {
        String                l_serialNumber          = null;
        GuiResponse           l_guiResponse           = null;
        CcVoucherDataResponse l_ccVoucherDataResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 47829, this,
                "ENTERED " + C_METHOD_getVoucher);
        }

        if (p_serialLength == 0)
        {
            l_serialNumber = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           "p_serialNumber",
                                           true,  // parameter is mandatory ...
                                           "",    // so there is no default
                                           PpasServlet.C_TYPE_ALPHA_NUMERIC);
        }
        else
        {
            l_serialNumber = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           "p_serialNumber",
                                           true,  // parameter is mandatory ...
                                           "",    // so there is no default
                                           PpasServlet.C_TYPE_ALPHA_NUMERIC,
                                           p_serialLength,
                                           C_OPERATOR_EQUALS);
        }

        l_ccVoucherDataResponse = i_ccVoucherService.getVoucher(p_guiRequest,
                                                                i_timeout,
                                                                l_serialNumber);

        l_guiResponse = l_ccVoucherDataResponse;
        
        // previous error of not pending, therefore return all the voucherData to the screen
        if (p_screenData.getUpdateErrorKey().equals(GuiResponse.C_KEY_VOUCHER_NOT_PENDING))
        {
            p_screenData.setVoucherSerialNumber(p_request.getParameter("p_serialNumber"));
            p_screenData.setVoucherEnquiryData(l_ccVoucherDataResponse.getVoucherData());
        }
        else
        {
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            // Voucher enquiry failed, so get the serial number with no
            // validation, for redisplay on the screen.
            p_screenData.setVoucherSerialNumber(p_request.getParameter("p_serialNumber"));
        }
        else
        {
            p_screenData.setVoucherEnquiryData(l_ccVoucherDataResponse.getVoucherData());
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 87531, this,
                "LEAVING " + C_METHOD_getVoucher);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucherHistory = "getVoucherHistory";
    /** Get voucher history details using the serial number on the request.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Voucher screen.
     * @param p_serialLength Length of serial number.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void getVoucherHistory(GuiRequest           p_guiRequest,
                                   HttpServletRequest   p_request,
                                   CcVoucherScreenData  p_screenData,
                                   int                  p_serialLength)
        throws PpasServletException
    {
        String                       l_serialNumber          = null;
        GuiResponse                  l_guiResponse           = null;
        CcVoucherHistoryDataResponse l_voucherHistoryResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 47829, this,
                "ENTERED " + C_METHOD_getVoucherHistory);
        }

        if (p_serialLength == 0)
        {
            l_serialNumber = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           "p_serialNumber",
                                           true,  // parameter is mandatory ...
                                           "",    // so there is no default
                                           PpasServlet.C_TYPE_ALPHA_NUMERIC);
        }
        else
        {
            l_serialNumber = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           "p_serialNumber",
                                           true,  // parameter is mandatory ...
                                           "",    // so there is no default
                                           PpasServlet.C_TYPE_ALPHA_NUMERIC,
                                           p_serialLength,
                                           C_OPERATOR_EQUALS);
        }

        l_voucherHistoryResponse = i_ccVoucherService.getVoucherHistory(p_guiRequest,
                                                                       i_timeout,
                                                                       l_serialNumber);

        l_guiResponse = l_voucherHistoryResponse;
        
        // previous error of not pending, therefore return all the voucherData to the screen
        if (p_screenData.getUpdateErrorKey().equals(GuiResponse.C_KEY_VOUCHER_NOT_PENDING))
        {
            p_screenData.setVoucherSerialNumber(p_request.getParameter("p_serialNumber"));
            p_screenData.setVoucherHistoryData(l_voucherHistoryResponse.getVoucherHistoryData());
        }
        else
        {
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            // Voucher enquiry failed, so get the serial number with no
            // validation, for redisplay on the screen.
            p_screenData.setVoucherSerialNumber(p_request.getParameter("p_serialNumber"));
        }
        else
        {
            p_screenData.setVoucherHistoryData(l_voucherHistoryResponse.getVoucherHistoryData());
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 87531, this,
                "LEAVING " + C_METHOD_getVoucherHistory);
        }
    }


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateVoucher = "updateVoucher";

    /** 
     * Update voucher status from pending to either available or assigned.
     * @param p_guiRequest   The GUI request being processed.
     * @param p_request      Message request.
     * @param p_screenData   Data associated with the Voucher screen.
     * @param p_serialLength Length of serial number.
     */
    private void updateVoucher(GuiRequest           p_guiRequest,
                               HttpServletRequest   p_request,
                               CcVoucherScreenData  p_screenData,
                               int                  p_serialLength)
    {
        String       l_serialNumber = null;
        GuiResponse  l_guiResponse = null;
        PpasDateTime l_currentTime = null;
        String       l_action = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           40000, 
                           this,
                           "ENTERED " + C_METHOD_updateVoucher);
        }

        try
        {
            l_serialNumber = getValidParam(p_guiRequest,
                                           0,
                                           p_request,
                                           "p_serialNumber",
                                           true,  // parameter is mandatory ...
                                           "",    // so there is no default
                                           PpasServlet.C_TYPE_ALPHA_NUMERIC,
                                           p_serialLength,
                                           C_OPERATOR_EQUALS);

            l_action = getValidParam(p_guiRequest,
                                     0,
                                     p_request,
                                     "p_newVoucherStatus",
                                     true,  // parameter is mandatory ...
                                     "",    // so there is no default
                                     new String [] {"COMMIT",
                                                    "ROLLBACK"});
        }
        catch (PpasServletException l_ppasSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                            new Object[] {"update voucher status"},
                                            GuiResponse.C_SEVERITY_WARNING);
        }
        else
        {
            l_guiResponse = i_ccVoucherService.updateVoucher(p_guiRequest,
                                                             i_timeout,
                                                             l_serialNumber,
                                                             l_action);
        }
        
        p_screenData.addUpdateResponse(p_guiRequest,
                                       l_guiResponse);

        l_currentTime = DatePatch.getDateTimeNow();

        if (p_screenData.hasUpdateError())
        {
            // Voucher status update failed, so get the serial number with no
            // validation, for redisplay on the screen.
            p_screenData.setVoucherSerialNumber(p_request.getParameter("p_serialNumber"));

            p_screenData.setRechargeFailed(true);
            p_screenData.setInfoText("Failed voucher status update at " + l_currentTime);
        }
        else
        {
            p_screenData.setRechargeDone(true);
            p_screenData.setInfoText("Successful voucher status update at " + l_currentTime);
        }    

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH,
                           WebDebug.C_APP_SERVLET,
                           WebDebug.C_ST_REQUEST,
                           p_guiRequest,
                           C_CLASS_NAME, 
                           40010,
                           this,
                           "LEAVING " + C_METHOD_updateVoucher);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucherScreenDetails = "getVoucherScreenDetails";
    /** Get the screen data.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Voucher screen.
     * @param p_serialLength Length of serial number.
     */
    private void getVoucherScreenDetails(GuiRequest          p_guiRequest,
                                         CcVoucherScreenData p_screenData,
                                         int                 p_serialLength)
    {
        CcVoucherDataSetResponse        l_ccVoucherDataSetResponse = null;
        CcFailedRechargeDataSetResponse l_ccFailedRechargeDataSetResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 77301, this,
                "ENTERED " + C_METHOD_getVoucherScreenDetails);
        }

        p_screenData.setVoucherSerialLength(p_serialLength);

        l_ccVoucherDataSetResponse = i_ccVoucherService.getRecharges(p_guiRequest, i_timeout);
        p_screenData.addRetrievalResponse(p_guiRequest, l_ccVoucherDataSetResponse);

        if (l_ccVoucherDataSetResponse.isSuccess())
        {
            p_screenData.setSuccessfulRechargeDataSet(
                             l_ccVoucherDataSetResponse.getVoucherDataSet());
        }

        if (!p_screenData.hasRetrievalError())
        {
            l_ccFailedRechargeDataSetResponse = i_ccVoucherService.getFailedRecharges(p_guiRequest, i_timeout);

            p_screenData.addRetrievalResponse(p_guiRequest, l_ccFailedRechargeDataSetResponse);

            if (l_ccFailedRechargeDataSetResponse.isSuccess())
            {
                p_screenData.setFailedRechargeDataSet(
                             l_ccFailedRechargeDataSetResponse
                             .getFailedRechargeDataSet());
            }
        }

        p_screenData.setEnhancedValueVoucherFeatureLicence(i_hasEnhancedValueVoucherFeatureLicence);
            
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_HIGH,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 38373, this,
                "LEAVING " + C_METHOD_getVoucherScreenDetails);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getVoucherRefillHistoryDetail =
                                        "getVoucherRefillHistoryDetail";
    /** Get voucher history details of recharge division.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     *  @param p_screenData The <code>CcVoucherRefillHistoryDetailScreenData</code>
     *                      object that will be used to return the voucher
     *                      history details to the voucher details screen.
     *  @throws PpasServletException No specific keys are anticipated.
     */
    private void getVoucherRefillHistoryDetail(
                     GuiRequest                        p_guiRequest,
                     HttpServletRequest                p_request,
                     CcVoucherRefillHistoryDetailScreenData  p_screenData)
        throws PpasServletException
    {
        String                l_dateTimeStr           = null;
        String                l_value                 = null;
        String                l_currency              = null;
        CcRechargeDivisionDataResponse l_ccDivisionDataResponse = null;
        String                l_appliedAmount         = null;
        String                l_appliedCurrency       = null;
        String                l_linkId                = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 77829, this,
                "ENTERED " + C_METHOD_getVoucherRefillHistoryDetail);
        }

        l_dateTimeStr =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_dateTime",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_DATE);

        l_value =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_value",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_currency =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_currency",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY,
                              3,
                              C_OPERATOR_EQUALS);

        l_appliedAmount =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_appliedAmount",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY);

        l_appliedCurrency =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_appliedCurrency",
                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_ANY,
                              3,
                              C_OPERATOR_EQUALS);

        l_linkId =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_linkId",                              true,  // parameter is mandatory ...
                              "",    // so there is no default
                              PpasServlet.C_TYPE_NUMERIC);

        l_ccDivisionDataResponse =
                i_ccRechargeDivisionService.getDivision(p_guiRequest,
                                                        i_timeout,
                                                        new Long(l_linkId).longValue(),
                                                        DivisionData.C_CREDIT_TYPE_VOUCHER);

        p_screenData.addRetrievalResponse(p_guiRequest, l_ccDivisionDataResponse);

        if (!p_screenData.hasRetrievalError())
        {
            p_screenData.setDivisionData(l_ccDivisionDataResponse.getDivisionData());
            p_screenData.setOriginalAmount(l_value);
            p_screenData.setOriginalCurrency(l_currency);
            p_screenData.setAppliedAmount(l_appliedAmount);
            p_screenData.setAppliedCurrency(l_appliedCurrency);
            p_screenData.setTransDateTimeStr(l_dateTimeStr);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 77531, this,
                "LEAVING " + C_METHOD_getVoucherRefillHistoryDetail);
        }
    }        

    /**
     * Gets the enhanced value voucher licence.
     * @return Value of licence.
     */
    private boolean getEnhancedValueVoucherFeatureLicence()
    {
        boolean l_licence = false;
        try
        {
            FeatureLicenceCache l_featureLicenceCache = i_guiContext.getFeatureLicenceCache();
        
            l_licence =
                l_featureLicenceCache.isLicensed(FeatureLicence.C_FEATURE_CODE_ENHANCED_VALUE_VOUCHER);
        }
        catch (PpasServiceException p_ppasServiceE)
        {
            i_logger.logMessage(p_ppasServiceE);
        }
        return l_licence;
    }
}

