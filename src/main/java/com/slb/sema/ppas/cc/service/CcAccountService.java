////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcAccountService.java
//      DATE            :       30-Jan-2002
//      AUTHOR          :       Matt Kirk / Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_38
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Gui Service that wrappers calls to PpasAccountService
//
////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                         | REFERENCE
//----------+------------+-------------------------------------+--------------------
// 20/05/05 | S J Vonka  | Add new updatePinCode method.       |PRD_ASCS00_GEN_CA_038
//          |            |                                     | CR1537#6456
//----------+------------+-------------------------------------+---------------------
// 13/08/07 | E Dangoor  | Allow for disconnected subscribers  | PpacLon#3211/11961
//          |            | when getting basic account data     |
//----------+------------+-------------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.CommunitiesIdListData;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.PinCode;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Service to facilitate the retrieval and updating of subscriber details.
 */
public class CcAccountService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcAccountService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Atomic service used by this Gui Service. */
    private PpasAccountService i_accountService;

    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** 
     * Creates an instance of the CcAccountService class.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_flags flags (not used)
     * @param p_logger instance of <code>Logger</code>
     * @param p_guiContext <code>GuiContext</code> containing configuration
     */
    public CcAccountService(GuiRequest p_guiRequest,
                            long       p_flags,
                            Logger     p_logger,
                            GuiContext p_guiContext)
    {
        super(p_guiRequest, p_flags, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest, C_CLASS_NAME, 22000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal account service to be used by this service.
        i_accountService = new PpasAccountService(p_guiRequest, p_logger, p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest, C_CLASS_NAME, 22010, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_changeServiceClass = "changeServiceClass";
    /**
     * Changes a subscriber's Service Class.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become 
     *                        available.
     * @param p_oldServClass Existing service class of the subscriber.
     * @param p_newServClass New value of the service class to be given to the 
     *                       subscriber.
     * @return Response object defining the outcome of the 
     *                     attempted service class change.
     */
    public GuiResponse changeServiceClass(GuiRequest   p_guiRequest,
                                          long         p_timeoutMillis,
                                          ServiceClass p_oldServClass,
                                          ServiceClass p_newServClass)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 30000, this,
                           "Entered " + C_METHOD_changeServiceClass);
        }

        try
        {
            i_accountService.changeServiceClass(p_guiRequest,
                                                p_timeoutMillis,
                                                p_oldServClass,
                                                p_newServClass);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"change service class"},
                              GuiResponse.C_SEVERITY_SUCCESS);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 30210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_changeServiceClass, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 31000, this,
                           "Leaving " + C_METHOD_changeServiceClass);
        }

        return l_guiResponse;
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_changeLanguage = "changeLanguage";
    /**
     * Changes a subscriber's Language.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become available.
     * @param p_oldLanguage Existing language of the subscriber.
     * @param p_newLanguage New language to be given to the subscriber.
     * @return Response object defining the outcome of the 
     *                     attempted language change.
     */
    public GuiResponse changeLanguage(GuiRequest p_guiRequest,
                                      long       p_timeoutMillis,
                                      String     p_oldLanguage,
                                      String     p_newLanguage)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 40000, this,
                           "Entered " + C_METHOD_changeLanguage);
        }

        try
        {
            i_accountService.changeLanguage(p_guiRequest,
                                            p_timeoutMillis,
                                            p_oldLanguage,
                                            p_newLanguage);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().
                                getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"change language"},
                              GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 40210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_changeLanguage, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 41000, this,
                           "Leaving " + C_METHOD_changeLanguage);
        }

        return l_guiResponse;

    }  // end public GuiResponse changeLanguage(...)

    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateHomeRegion = "updateHomeRegion";
    /**
     * Changes a subscriber's Home Region.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become available.
     * @param p_oldHomeRegion Existing language of the subscriber.
     * @param p_newHomeRegion New home region to be given to the subscriber.
     * @return Response object defining the outcome of the 
     *                     attempted language change.
     */
    public GuiResponse updateHomeRegion(GuiRequest   p_guiRequest,
                                        long         p_timeoutMillis,
                                        HomeRegionId p_oldHomeRegion,
                                        HomeRegionId p_newHomeRegion)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 42000, this,
                           "Entered " + C_METHOD_updateHomeRegion);
        }

        try
        {
            //TODO: What happens to the old home region - f40771d
            i_accountService.updateHomeRegion(p_guiRequest,
                                              p_timeoutMillis,
                                              p_newHomeRegion);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().
                                getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"change language"},
                              GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 42210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateHomeRegion, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 42400, this,
                           "Leaving " + C_METHOD_updateHomeRegion);
        }

        return l_guiResponse;

    }  // end public GuiResponse updateHomeRegion(...)

    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateAgent =  "updateAgent";
    /**
     * Updates the Agent or the subAgent (or both)  associated with a subscriber.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis timeout in milliseconds
     * @param p_oldAgent The agent currently associated with the subscriber.
     * @param p_newAgent The new agent to be associated with the subscriber.
     *                   If this is <code>null</code>, then the agent associated
     *                   with the subscriber will not be updated.
     * @param p_oldSubAgent The old sub-agent associated with the subscriber.
     * @param p_newSubAgent The new sub-agent to be associated with the 
     *                      subscriber. If this is <code>null</code>, then the
     *                      sub-agent associatedwith the subscriber will not be updated.
     * @return Response object indicating the success or failure of the 
     *         attempted agent update and, in the case of failure, the 
     *         response key indicating the cause of the failure.
     */
    public GuiResponse updateAgent(GuiRequest p_guiRequest,
                                   long       p_timeoutMillis,
                                   String     p_oldAgent,
                                   String     p_newAgent,
                                   String     p_oldSubAgent,
                                   String     p_newSubAgent)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 56000, this,
                           "Entered " + C_METHOD_updateAgent);
        }

        try
        {
            i_accountService.updateAgent (p_guiRequest,
                                          p_timeoutMillis,
                                          p_oldAgent,
                                          p_newAgent,
                                          p_oldSubAgent,
                                          p_newSubAgent);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().
                                getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"update agent/sub-agent"},
                              GuiResponse.C_SEVERITY_SUCCESS);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_MODERATE,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 56010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateAgent, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 56090, this,
                           "Leaving " + C_METHOD_updateAgent);
        }

        return (l_guiResponse);
    }  // end method updateAgent

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateExpiryDates = "updateExpiryDates";
    /**
     * Changes a subscriber's Expiry Dates.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become available.
     * @param p_oldAirtimeExpiryDate Existing airtime expiry date of the subscriber.
     * @param p_newAirtimeExpiryDate New airtime expiry date to be given to the subscriber.
     * @param p_oldServiceExpiryDate Existing service expiry date of the subscriber.
     * @param p_newServiceExpiryDate New service expiry date to be given to the subscriber.
     * @return Response object defining the outcome of the attempted expiry dates change.
     */
    public GuiResponse updateExpiryDates(GuiRequest p_guiRequest,
                                         long       p_timeoutMillis,
                                         PpasDate   p_oldAirtimeExpiryDate,
                                         PpasDate   p_newAirtimeExpiryDate,
                                         PpasDate   p_oldServiceExpiryDate,
                                         PpasDate   p_newServiceExpiryDate)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 50000, this,
                           "Entered " + C_METHOD_updateExpiryDates);
        }

        try
        {
            i_accountService.updateExpiryDates(p_guiRequest,
                                               p_timeoutMillis,
                                               p_newAirtimeExpiryDate,
                                               p_oldAirtimeExpiryDate,
                                               p_newServiceExpiryDate,
                                               p_oldServiceExpiryDate);

            l_guiResponse  = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"update expiry dates"},
                              GuiResponse.C_SEVERITY_SUCCESS);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 50210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateExpiryDates, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 51000, this,
                           "Leaving " + C_METHOD_updateExpiryDates);
        }

        return l_guiResponse;
    }  // end public GuiResponse updateExpiryDates(...)

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateSegmentation = "updateSegmentation";
    /**
     * Gui wrapper to the internal services update subscriber segmentation. If there is a change
     * in the account group ID or service offerings then sends the appropriate data. Since account
     * group ID and service offerings are completely optional and independent of each other, this
     * method only sends the corresponding encapsulated data if a change has been detected. This
     * means that the Account group IDs or the Service Offerings parameters may be null but not
     * both because to get to this stage at least one of them must have changed.
     * @param p_guiRequest Original request.
     * @param p_timeoutMs Request timeout (ms).
     * @param p_oldAccGroupId Old Account Group Id. 
     * @param p_newAccGroupId New Account Group Id. 
     * @param p_newServices New Service Offerings.
     * @param p_oldServices Old Service Offerings.
     * @return A <code>GuiResponse</code>.
     */
    public GuiResponse updateSegmentation(GuiRequest p_guiRequest,
                                          long p_timeoutMs,
                                          String p_oldAccGroupId,
                                          String p_newAccGroupId,
                                          String p_oldServices,
                                          String p_newServices)
    {
        GuiResponse      l_guiResponse = null;
        AccountGroupId   l_oldAccountGroupId = null;
        AccountGroupId   l_newAccountGroupId = null;
        ServiceOfferings l_oldServiceOfferings = null;
        ServiceOfferings l_newServiceOfferings = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 80100, this,
                           "Entered " + C_METHOD_updateSegmentation);
        }

        try
        {
            // Account groups and-or service offerings may be null when passed
            // from the GUI to the servlet since they are completely optional
            // and independent of each other.
            if ((p_oldAccGroupId != null) && (p_oldAccGroupId.length() > 0))
            {
                l_oldAccountGroupId = new AccountGroupId(p_oldAccGroupId);
            }

            if ((p_newAccGroupId != null) && (p_newAccGroupId.length() > 0))
            {
                l_newAccountGroupId = new AccountGroupId(p_newAccGroupId);

                if (l_newAccountGroupId.equals(l_oldAccountGroupId))
                {
                    // Send nulls if no change
                    l_oldAccountGroupId = l_newAccountGroupId = null;
                }
            }

            if ((p_oldServices != null) && (p_oldServices.length() > 0))
            {
                l_oldServiceOfferings = new ServiceOfferings(p_oldServices);
            }

            if ((p_newServices != null) && (p_newServices.length() > 0))
            {
                l_newServiceOfferings = new ServiceOfferings(p_newServices);

                if (l_newServiceOfferings.equals(l_oldServiceOfferings))
                {
                    // Send nulls if no change
                    l_oldServiceOfferings = l_newServiceOfferings = null;
                }
            }

            i_accountService.updateSegmentation(p_guiRequest,
                                                p_timeoutMs,
                                                l_oldAccountGroupId,
                                                l_newAccountGroupId,
                                                l_oldServiceOfferings,
                                                l_newServiceOfferings);

            l_guiResponse  = new GuiResponse(p_guiRequest,
                                             p_guiRequest.getGuiSession().getSelectedLocale(),
                                             GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                             new Object[] {"update subscriber segmentation"},
                                             GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_ppasServE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 80110, this,
                               "Caught PpasServiceException: " + l_ppasServE);
            }

            l_guiResponse = 
                handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateSegmentation, l_ppasServE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 80199, this,
                           "Leaving " + C_METHOD_updateSegmentation);
        }
        
        return (l_guiResponse);
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateUssdEocnId = "updateUssdEocnId";
    /**
     * Gui wrapper to the internal services update USSD EoCN Selection Structure ID.
     * @param p_guiRequest Original request.
     * @param p_timeoutMs Request timeout (ms).
     * @param p_ussdEocnId New USSD EoCN selection structure ID.
     * @return Response object indicating the success or failure of the 
     *         attempted USSD EoCN selection structure ID update.
     */
    public GuiResponse updateUssdEocnId(GuiRequest              p_guiRequest,
                                        long                    p_timeoutMs,
                                        EndOfCallNotificationId p_ussdEocnId)
    {
        GuiResponse      l_guiResponse = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 90100, this,
                           "Entered " + C_METHOD_updateUssdEocnId);
        }
        
        try
        {
            i_accountService.updateUssdEocnId(p_guiRequest, p_timeoutMs, p_ussdEocnId);

            l_guiResponse  = new GuiResponse(p_guiRequest,
                                             p_guiRequest.getGuiSession().getSelectedLocale(),
                                             GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                             new Object[] {"update USSD EoCN selection structure ID"},
                                             GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_ppasServE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 90110, this,
                               "Caught PpasServiceException: " + l_ppasServE);
            }

            l_guiResponse = 
                handlePpasServiceException(p_guiRequest, 0, C_METHOD_updateUssdEocnId, l_ppasServE);

        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 90199, this,
                           "Leaving " + C_METHOD_updateUssdEocnId);
        }
        
        return (l_guiResponse);
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateCommunity = "updateCommunity";
    /**
     * Posts an update to the Community Charging IDs.
     * @param p_guiRequest        GUI request object.
     * @param p_timeoutMillis     Request timeout in milliseconds.
     * @param p_oldCommunities    List of old Community Charging IDs.
     * @param p_newCommunities    List of new Community Charging IDs.
     * @return Response indicating success or failure of the request.
     */
    public GuiResponse updateCommunity(GuiRequest            p_guiRequest,
                                       long                  p_timeoutMillis,
                                       CommunitiesIdListData p_oldCommunities,
                                       CommunitiesIdListData p_newCommunities)
    {
        GuiResponse            l_guiResponse = null;

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10050, 
                           this,
                           "Entered " + C_METHOD_updateCommunity);
        }

        try
        {
            i_accountService.updateCommunity(p_guiRequest,
                                             p_timeoutMillis,
                                             p_oldCommunities,
                                             p_newCommunities);

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {"update community charging"},
                                            GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on) 
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, 
                               C_CLASS_NAME, 
                               10060, 
                               this,
                               "Caught PpasServiceException: " + l_pSE);
            }
            
            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, "update communities", l_pSE);
        }

        if (WebDebug.on) 
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, 
                           C_CLASS_NAME, 
                           10070, 
                           this,
                           "Leaving " + C_METHOD_updateCommunity);
        }

        return (l_guiResponse);
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_ivrUnbarAccount = "ivrUnbarAccount";
    /**
     * Unbars a subscriber from IVR recharge.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become 
     *                        available.
     * @return Response object defining the outcome of the 
     *                     attempted IVR unbar.
     */
    public GuiResponse ivrUnbarAccount(GuiRequest p_guiRequest,
                                       long       p_timeoutMillis)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 51000, this,
                           "Leaving " + C_METHOD_updateExpiryDates);
        }

        try
        {
            i_accountService.ivrUnbarAccount(p_guiRequest, p_timeoutMillis);

            l_guiResponse = new GuiResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),
                                GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                new Object[] {"unbar recharge"},
                                GuiResponse.C_SEVERITY_SUCCESS);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 60210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_ivrUnbarAccount, l_pSE);

        }


        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 61000, this,
                           "Leaving " + C_METHOD_ivrUnbarAccount);
        }

        return l_guiResponse;
    }
    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_changeTempBlocking = "changeTempBlocking";
    /**
     * Set or clear the temporary blocking flag.
     * @param p_guiRequest GuiRequest
     * @param p_timeoutMillis request timeout in milliseconds
     * @param p_tempBlockStatus the new temp blocking status, <code>true</code>
     *                          setting temporary blocking, and <code>false</code>
     *                          clearing it
     * @return a GuiResponse
     */
    public GuiResponse changeTempBlocking(GuiRequest p_guiRequest,
                                          long       p_timeoutMillis,
                                          boolean    p_tempBlockStatus)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 62000, this,
                           "Entered " + C_METHOD_changeTempBlocking);
        }

        try
        {
            i_accountService.changeTemporaryBlocking(p_guiRequest, p_timeoutMillis, p_tempBlockStatus);

            l_guiResponse = new GuiResponse(p_guiRequest,
                                            p_guiRequest.getGuiSession().getSelectedLocale(),
                                            GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                                            new Object[] {"change temporary blocking"},
                                            GuiResponse.C_SEVERITY_SUCCESS);
                                            
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 62010, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_changeTempBlocking, l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 62020, this,
                           "Leaving " + C_METHOD_changeTempBlocking);
        }

        return l_guiResponse;
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updatePinCode = "updatePinCode";
    /**
     * Updates a subscriber's pin code.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis Time to wait, in milliseconds, for the resources
     *                        required to perform the service to become available.
     * @param p_newPinCode New pin code to be given to the subscriber.
     * @return Response object defining the outcome of the 
     *                     attempted pin code update.
     */
    public GuiResponse updatePinCode( GuiRequest  p_guiRequest,
                                      long        p_timeoutMillis,
                                      PinCode     p_newPinCode)
    {
        GuiResponse l_guiResponse  = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 41010, this,
                           "Entered " + C_METHOD_updatePinCode);
        }

        try
        {
            i_accountService.updatePinCode( p_guiRequest,
                                            p_timeoutMillis,
                                            p_newPinCode);

            l_guiResponse = new GuiResponse(
                              p_guiRequest,
                              p_guiRequest.getGuiSession().getSelectedLocale(),
                              GuiResponse.C_KEY_SPECIFIC_SERVICE_SUCCESS,
                              new Object[] {"change pin code"},
                              GuiResponse.C_SEVERITY_SUCCESS);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 41210, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse = handlePpasServiceException(p_guiRequest, 
                                                       0, 
                                                       C_METHOD_updatePinCode, 
                                                       l_pSE);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 41310, this,
                           "Leaving " + C_METHOD_updatePinCode);
        }

        return l_guiResponse;

    }  // end public GuiResponse updatePinCode(...)


    
    
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getFullData = "getFullData";
    /**
     * Retrieves full account data for the subscriber. The method does not
     * check against a market i.e. it is assumed that at the point where
     * this method is called, we know that the current account is in the
     * market defined in the session.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis timeout in milliseconds
     * @return CcAccountDataResponse containing account details read or reason
     *         for failure to read account details.
     */
    public CcAccountDataResponse getFullData(GuiRequest p_guiRequest,
                                             long       p_timeoutMillis)
    {
        return (getFullData (p_guiRequest, p_timeoutMillis, new Market(p_guiRequest)));
    }

    /**
     * Retrieves full account data for the subscriber. The account data returned
     * must be in the market defined by <code>p_srva</code> and 
     * <code>p_sloc</code>.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis timeout in milliseconds
     * @param p_market Market that the selected account must belong to.
     *                 If a 'undefined' value is passed in for <code>p_market</code>,
     *                 then it will not be used to restrict the account selected.
     * @return CcAccountDataResponse containing account details read or reason
     *         for failure to read account details.
     */
    public CcAccountDataResponse getFullData(GuiRequest p_guiRequest,
                                             long       p_timeoutMillis,
                                             Market     p_market)
    {
        CcAccountDataResponse l_ccAccountDataResponse  = null;
        GuiResponse           l_guiResponse            = null;
        AccountData           l_accountData            = null;
        GuiSession            l_guiSession;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 70000, this,
                           "Entered " + C_METHOD_getFullData);
        }

        // See if the AccountData has been cached in the session
        l_guiSession = (GuiSession)p_guiRequest.getSession();
        l_accountData = l_guiSession.getAccountData();

        // Check to see if the account data has been cached in the session.
        // The account data is cached in the session when a single account is
        // identified on the subscriber selection screen. However, the account
        // data is only cached in going between the Subscriber Selection and
        // Account Details screens; if we're returning to Account Details screen
        // from anywhere else, then Account Data must be re-read from the database.
        if (l_accountData != null)
        {
            // Create success response containing the data from the session.

            l_ccAccountDataResponse = new CcAccountDataResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                GuiResponse.C_KEY_SERVICE_SUCCESS,
                                new Object[] {},
                                GuiResponse.C_SEVERITY_SUCCESS,
                                l_accountData);

            // Remove the cached Account data from the session to ensure that
            // it will not be re-used i.e. force a re-read of the data from the
            // database the next time that the service is called.
            l_guiSession.setAccountData (null);
        }
        else
        {
            // Need to read the account data from the database.

            try
            {
                l_accountData = i_accountService.getFullData(p_guiRequest, p_timeoutMillis, p_market);

                l_ccAccountDataResponse = new CcAccountDataResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),  // PpacLon#17
                                GuiResponse.C_KEY_SERVICE_SUCCESS,
                                new Object[] {},
                                GuiResponse.C_SEVERITY_SUCCESS,
                                l_accountData);
            } // end try
            catch (PpasServiceException l_pSE)
            {
                if (WebDebug.on)
                {
                    WebDebug.print(WebDebug.C_LVL_LOW,
                                   WebDebug.C_APP_SERVICE,
                                   WebDebug.C_ST_ERROR,
                                   p_guiRequest, C_CLASS_NAME, 70210, this,
                                   "Caught PpasServiceException: " + l_pSE);
                }

                l_guiResponse = handlePpasServiceException(p_guiRequest, 0, C_METHOD_getFullData, l_pSE);

                l_ccAccountDataResponse = 
                    new CcAccountDataResponse(p_guiRequest,
                                              l_guiResponse.getKey(),
                                              l_guiResponse.getParams(),
                                              GuiResponse.C_SEVERITY_FAILURE,
                                              null);
            } // end catch
        } // end else

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 71000, this,
                           "Leaving " + C_METHOD_getFullData);
        }

        return l_ccAccountDataResponse;
    }

    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getBasicData = "getBasicData";
    /**
     * Retrieves basic account data for the subscriber.
     * @param p_guiRequest <code>GuiRequest</code>
     * @param p_timeoutMillis timeout in milliseconds
     * @return CcBasicAccountDataResponse A response containing basic account 
     *         details for the subscriber in the request.
     */
    public CcBasicAccountDataResponse getBasicData(GuiRequest p_guiRequest,
                                                   long       p_timeoutMillis)
    {
        CcBasicAccountDataResponse l_ccBasicAccountDataResponse  = null;
        BasicAccountData           l_basicAccountData            = null;
        GuiResponse                l_guiResponse                 = null;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 70001, this,
                           "Entered " + C_METHOD_getBasicData);
        }

        try
        {
            l_basicAccountData = i_accountService.getBasicData (
                                                    p_guiRequest,
                                                    PpasService.C_FLAG_INCLUDE_DISCONNECTED,
                                                    p_timeoutMillis);

            if (l_basicAccountData != null)
            {
                // Create success response containing the data from the session.

                l_ccBasicAccountDataResponse = new CcBasicAccountDataResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),
                                GuiResponse.C_KEY_SERVICE_SUCCESS,
                                new Object[] {},
                                GuiResponse.C_SEVERITY_SUCCESS,
                                l_basicAccountData);
            }
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW,
                               WebDebug.C_APP_SERVICE,
                               WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 70211, this,
                               "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException(p_guiRequest, 0, "get the customer's basic details", l_pSE);

            l_ccBasicAccountDataResponse =
                new CcBasicAccountDataResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_LOW,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_END,
                           p_guiRequest, C_CLASS_NAME, 71001, this,
                           "Leaving " + C_METHOD_getBasicData);
        }

        return l_ccBasicAccountDataResponse; 
    }
}
