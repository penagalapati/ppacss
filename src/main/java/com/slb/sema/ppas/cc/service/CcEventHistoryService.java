////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcEventHistoryService.java
//      DATE            :       19-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PRD_PPAK00_DEV_IN_38
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Customer Care Service that wrappers calls to
//                              the PpasEventHistoryService.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/02/03 | D Bitmead  | CS2 migration compilation issues| PpacLon#1/17
//----------+------------+---------------------------------+--------------------
// 07/10/03 | M.Brister  | Amended JavaDoc comment for     | PpacLon#43/384
//          |            | getHistory method - New bits for|
//          |            | p_flags parameter.              |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.service;

import com.slb.sema.ppas.common.dataclass.EventHistoryDataSet;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.service.GuiService;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.is.isapi.PpasEventHistoryService;
import com.slb.sema.ppas.util.logging.Logger;

/** Customer care service that wraps the Ppas Event History Internal
 *  service. This service is used to access a history of events that have
 *  occurred on a subscriber's account. Events may arise from comments,
 *  memos or as a result of failed recharges.
 */
public class CcEventHistoryService extends GuiService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcEventHistoryService";

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------



    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The event history internal service used by this Gui Service. */
    private PpasEventHistoryService i_eventHistoryIS;


    //------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------

    /** Creates an instance of the Customer Care Event History Service.
     *  This service is used to access a history of events that have
     *  occurred on a subscriber's account. Events may arise from comments,
     *  memos or as a result of failed recharges.
     * @param p_guiRequest The request being processed.
     * @param p_flags      General process flags.
     * @param p_logger     The logger to direct exceptions.
     * @param p_guiContext Context containing necessary config and stuff
     */
    public CcEventHistoryService(
        GuiRequest p_guiRequest,
        long       p_flags,
        Logger     p_logger,
        GuiContext p_guiContext)
    {
        super (p_guiRequest,
               p_flags,
               p_logger,
               p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_START,
                p_guiRequest, C_CLASS_NAME, 42000, this,
                "Constructing " + C_CLASS_NAME);
        }

        // Create PPAS internal account service to be used by this service.
        i_eventHistoryIS = new PpasEventHistoryService
                   (p_guiRequest,
                    p_logger,
                    p_guiContext);

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_CONFIN_END,
                p_guiRequest, C_CLASS_NAME, 42090, this,
                "Constructed " + C_CLASS_NAME);
        }

    }  // end constructor


    //------------------------------------------------------------------------
    // public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getHistory = "getHistory";
    /**
     * Get a history of events that have occurred on the account of the
     * subscriber defined in the request.
     * @param p_guiRequest The request being processed.
     * @param p_flags Determines what information should be included in the
     *                event history. Valid bits in this mask are:
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_COMMENTS
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_MEMOS
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_FAILED_RECHARGES
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_PAYMENTS
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_RECHARGES
     *            <br>  PpasEventHistoryService.C_FLAG_INCLUDE_ADJUSTMENTS
     * @param p_timeoutMillis Time in milliseconds to wait for resources
     *                        required to perform the service to become
     *                        available before giving up.
     * @return An event history response object containing the event history
     *         data for the customer defined in the given request.
     *
     */
    public CcEventHistoryDataSetResponse getHistory(
        GuiRequest p_guiRequest,
        long       p_flags,
        long       p_timeoutMillis)
    {
        GuiResponse                     l_guiResponse = null;
        CcEventHistoryDataSetResponse   l_eventHistoryResponse  = null;
        EventHistoryDataSet             l_eventHistorySet;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_START,
                p_guiRequest, C_CLASS_NAME, 42100, this,
                "Entered " + C_METHOD_getHistory);
        }

        try
        {
            l_eventHistorySet = i_eventHistoryIS.getHistory(
                    p_guiRequest,
                    p_flags,
                    p_timeoutMillis);

            l_eventHistoryResponse = new CcEventHistoryDataSetResponse(
                                  p_guiRequest,
                                  p_guiRequest.getGuiSession().
                                    getSelectedLocale(),  // PpacLon#1/17
                                  GuiResponse.C_KEY_SERVICE_SUCCESS,
                                  new Object[] {""},
                                  GuiResponse.C_SEVERITY_SUCCESS,
                                  l_eventHistorySet);
        }
        catch (PpasServiceException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVICE,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest, C_CLASS_NAME, 42110, this,
                    "Caught PpasServiceException: " + l_pSE);
            }

            l_guiResponse =
                handlePpasServiceException (p_guiRequest,
                                           0,
                                           "get the subscriber's event history",
                                           l_pSE);

            l_eventHistoryResponse = new CcEventHistoryDataSetResponse(
                                p_guiRequest,
                                p_guiRequest.getGuiSession().
                                  getSelectedLocale(),   // PpacLon#1/17
                                l_guiResponse.getKey(),
                                l_guiResponse.getParams(),
                                GuiResponse.C_SEVERITY_FAILURE,
                                null);
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVICE,
                WebDebug.C_ST_END,
                p_guiRequest, C_CLASS_NAME, 42190, this,
                "Leaving " + C_METHOD_getHistory);
        }

        return (l_eventHistoryResponse);

    }  // end method getHistory

} // end public class CcEventHistoryService
