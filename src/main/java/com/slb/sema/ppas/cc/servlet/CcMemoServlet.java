////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcMemoServlet.Java
//      DATE            :       04-Apr-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1343
//                              PRD_PPAK00_DEV_IN_39
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Servlet to to handle requests from the
//                              Memo screen.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//06/10/03  | Remi       | Privilege checks added          | CR#60/571
//          | Isaacs     |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcMemoScreenData;
import com.slb.sema.ppas.cc.service.CcMemoDataResponse;
import com.slb.sema.ppas.cc.service.CcMemoService;
import com.slb.sema.ppas.common.dataclass.MemoData;
import com.slb.sema.ppas.common.dataclass.MemoRecord;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession; //CR#60/571
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData; //CR#60/571

/**
 * Servlet to to handle requests from the Memo screen.
 */
public class CcMemoServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcMemoServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC GUI Memo Service to be used by this servlet. */
    private CcMemoService i_ccMemoService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */
    public CcMemoServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME );
        }

        return;

    }

    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doInit = "doInit";
    /** Initialise this object.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                C_CLASS_NAME, 20000, this,
                "Entered " + C_METHOD_doInit);
        }

        // Should probably be constructing CcMemoService here...
        // unfortunately no GuiRequest is available.

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                C_CLASS_NAME, 20090, this,
                "Leaving " + C_METHOD_doInit);
        }

        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /** Service method to control requests and responses to the JSP script
     *  for the Memo service.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest             p_guiRequest)
    {
        String                      l_command        = null;
        String                      l_forwardUrl     = null;
        CcMemoScreenData            l_memoScreenData = null;
        GuiResponse                 l_guiResponse    = null;
        boolean                     l_accessGranted  = false; // CR#60/571
        GopaGuiOperatorAccessData   l_gopaData       = null;  // CR#60/571


        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13000, this,
                "ENTERED " + C_METHOD_doService);
        }

        // Construct CcMemoService if it does not already exist ... this
        // will be move into doInit later!! <<< ??? >>>
        if (i_ccMemoService == null)
        {
            i_ccMemoService = new CcMemoService(p_guiRequest, 0, i_logger, i_guiContext);
        }

        // Create screen data object.
        l_memoScreenData = new CcMemoScreenData(i_guiContext, p_guiRequest);

        try
        {
            // Get the command from the http request
            l_command = getValidParam(p_guiRequest,
                                      0,
                                      p_request,
                                      "p_command",
                                      true,
                                      "",
                                      new String [] {"GET_SCREEN_DATA",
                                                     "GET_MEMO",
                                                     "ADD_MEMO",
                                                     "UPDATE_MEMO"});

            // CR#60/571 Start

            // Determine if the CSO has privilege to access the requested screen or function

            if ((l_command.equals("GET_SCREEN_DATA")) ||
                (l_command.equals("GET_MEMO")) ||
                (l_command.equals("ADD_MEMO")) ||
                (l_command.equals("UPDATE_MEMO")))
            {
                l_gopaData = ((GuiSession) p_guiRequest.getSession()).getAccessProfile();

                if (l_gopaData.hasCommentsScreenAccess())
                {
                    l_accessGranted = true;
                }
            }

            if (!l_accessGranted)
            {
                // Insufficient privilege - display Access Error screen & refuse access

                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_memoScreenData.addRetrievalResponse(p_guiRequest,
                                                      l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_memoScreenData);
            }
            else
            {
                if (l_command.equals("GET_SCREEN_DATA"))
                {
                    getScreenData(p_guiRequest, l_memoScreenData);
                }
                else if (l_command.equals("ADD_MEMO"))
                {
                    setIsUpdateRequest(true);

                    addMemo(p_guiRequest, p_request, l_memoScreenData);
                }
                else if (l_command.equals("GET_MEMO"))
                {
                    getMemo(p_guiRequest, p_request, l_memoScreenData);
                }
                else if (l_command.equals("UPDATE_MEMO"))
                {
                    setIsUpdateRequest(true);

                    updateMemo(p_guiRequest,
                               p_request,
                               l_memoScreenData);
                }
            }
            //CR#60/571 End
        }
        catch (PpasServletException l_ppasSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_LOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 73872, this,
                    "Caught PpasServletException exception: "
                    + l_ppasSE);
            }

            // Deal with any more general parameter handling exceptions
            // thrown by the superclass methods.
            l_guiResponse = handleInvalidParam(p_guiRequest, l_ppasSE);

            l_memoScreenData.addRetrievalResponse(p_guiRequest,
                                                  l_guiResponse);
        } // end catch

        //CR#60/571 Begin
        if (l_accessGranted)
        {
            if (l_memoScreenData.hasRetrievalError())
            {
                // An error occured retrieving the data necessary to populate
                // the Memo screen.
                // Forward to the GuiError.jsp instead.
                l_forwardUrl = "/jsp/cc/ccerror.jsp";

                p_request.setAttribute ("p_guiScreenData",
                                        l_memoScreenData);
            }
            else
            {
                // CcMemoScreenData object has the necessary data
                // to populate the Memo screen.
                l_forwardUrl = "/jsp/cc/ccmemo.jsp";

                p_request.setAttribute("p_guiScreenData",
                                       l_memoScreenData);
            }
        }

        //CR#60/571 End

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                p_guiRequest,
                C_CLASS_NAME, 13900, this,
                "LEAVING " + C_METHOD_doService +
                ", forwarding to " + l_forwardUrl);
        }

        return (l_forwardUrl);
    }


    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getScreenData = "getScreenData";
    /**
     * Sets up the Screen Data object such that the Memo window has the
     * information for a new Memo to be written.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Memo screen.
     */
    private void getScreenData(GuiRequest         p_guiRequest,
                               CcMemoScreenData   p_screenData)
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 37000, this,
                "Entered " + C_METHOD_getScreenData);
        }

        p_screenData.setRaisedOn(new PpasDateTime(""));
        p_screenData.setRaisedBy(p_guiRequest.getOpid());

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27390, this,
                "Leaving " + C_METHOD_getScreenData);
        }

        return;

    } // end private void writeMemo(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addMemo = "addMemo";
    /**
     * Adds a new Memo to the given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Memo screen.
     */
    private void addMemo(GuiRequest         p_guiRequest,
                         HttpServletRequest p_request,
                         CcMemoScreenData   p_screenData)
    {
        CcMemoDataResponse l_memoResponse  = null;
        GuiResponse        l_guiResponse  = null;
        String             l_subject      = null;
        String             l_text         = null;
        String             l_status       = null;
        boolean            l_isInfoMemo   = false;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 27000, this,
                "Entered " + C_METHOD_addMemo);
        }

        // Get params from request...
        try
        {
            l_subject =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_subject",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY,
                              MemoData.C_MAX_SUBJECT_LENGTH,
                              C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_text =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_text",
                              true,
                              "",
                              PpasServlet.C_TYPE_ANY,
                              MemoRecord.C_MAX_TEXT_LENGTH,
                              C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_status =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_status",
                              true,
                              "",
                              new String[] {"OPEN", "INFO"});

        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 27300, this,
                    "Exception parsing parameters: " + l_pSE);
            }

            // Convert the exception into a suitable response...
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do not attempt
            // this requested service.
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"add Memo"},
                                GuiResponse.C_SEVERITY_WARNING);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }
        else
        {
            l_isInfoMemo = (l_status.equals("OPEN")) ? false : true;

            // Call the service...
            l_memoResponse =
                    i_ccMemoService.addMemo(p_guiRequest,
                                            i_timeout,
                                            l_subject,
                                            l_text,
                                            l_isInfoMemo);

            // The addRetrievalResponse method is being called here
            // as well as the addUpdateResponse as specified by the
            // sub-system design, because this service is different
            // in that the update and retrieval are combined into
            // one call, where-as others are two separate calls
            // i.e. update followed by retrieval.
            p_screenData.addRetrievalResponse(p_guiRequest,
                                              l_memoResponse);
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_memoResponse);

            if (l_memoResponse.isSuccess())
            {
                p_screenData.setMemoData(l_memoResponse.getMemoData());
                if (l_memoResponse.getMemoData().isOpen())
                {
                    p_screenData.setOpenMemoCountIncremented();
                }
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 27090, this,
                "Leaving " + C_METHOD_addMemo);
        }

        return;

    } // end private void addMemo(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getMemo = "getMemo";
    /**
     * Get a Memo from the DB.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Memo screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void getMemo(GuiRequest         p_guiRequest,
                         HttpServletRequest p_request,
                         CcMemoScreenData   p_screenData)
        throws PpasServletException
    {
        CcMemoDataResponse  l_ccResponse     = null;
        String              l_raisedOn       = null;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 47000, this,
                "Entered " + C_METHOD_getMemo);
        }

        // Get params from request...
        l_raisedOn = getValidParam(p_guiRequest,
                                   C_FLAG_DATETIME_ONLY,
                                   p_request,
                                   "p_raisedOn",
                                   true,
                                   "",
                                   PpasServlet.C_TYPE_DATE);

        // Call the service...
        l_ccResponse =
            i_ccMemoService.getSingleMemo(
                                p_guiRequest,
                                CcMemoService.C_FLAG_INCLUDE_MEMO_DETAILS,
                                i_timeout,
                                new PpasDateTime(l_raisedOn));

        p_screenData.addRetrievalResponse(p_guiRequest,
                                          l_ccResponse);

        if (l_ccResponse.isSuccess())
        {
            p_screenData.setMemoData(l_ccResponse.getMemoData());
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 47090, this,
                "Leaving " + C_METHOD_getMemo);
        }

        return;

    } // end private void getMemo(...)


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateMemo = "updateMemo";
    /**
     * Updates a Memo of the given account.
     * @param p_guiRequest The GUI request being processed.
     * @param p_request    Message request.
     * @param p_screenData Data associated with the Memo screen.
     */
    private void updateMemo(GuiRequest         p_guiRequest,
                            HttpServletRequest p_request,
                            CcMemoScreenData   p_screenData)
    {
        CcMemoDataResponse l_memoResponse  = null;
        GuiResponse        l_guiResponse   = null;
        String             l_text          = null;
        String             l_status        = null;
        String             l_raisedOn      = null;
        boolean            l_isResolved    = false;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_START,
                p_guiRequest,
                C_CLASS_NAME, 57000, this,
                "Entered " + C_METHOD_updateMemo);
        }

        // Get params from request...

        try
        {
            l_text = getValidParam(p_guiRequest,
                                   C_FLAG_ALLOW_BLANK,
                                   p_request,
                                   "p_text",
                                   true,
                                   "",
                                   PpasServlet.C_TYPE_ANY,
                                   MemoRecord.C_MAX_TEXT_LENGTH,
                                   C_OPERATOR_LESS_THAN_OR_EQUAL);

            l_raisedOn = getValidParam(p_guiRequest,
                                       C_FLAG_DATETIME_ONLY,
                                       p_request,
                                       "p_raisedOn",
                                       true,
                                       "",
                                       PpasServlet.C_TYPE_DATE);

            l_status = getValidParam(p_guiRequest,
                                     0,
                                     p_request,
                                     "p_status",
                                     true,
                                     "",
                                     new String[] {"OPEN", "RESOLVED"});
        }
        catch (PpasServletException l_pSE)
        {
            if (WebDebug.on)
            {
                WebDebug.print(
                    WebDebug.C_LVL_VLOW,
                    WebDebug.C_APP_SERVLET,
                    WebDebug.C_ST_ERROR,
                    p_guiRequest,
                    C_CLASS_NAME, 57300, this,
                    "Exception parsing parameters: " + l_pSE);
            }

            // Convert the exception into a suitable response...

            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse(p_guiRequest, l_guiResponse);
        }

        if (p_screenData.hasUpdateError())
        {
            // A previous update error has occured, therefore do not attempt
            // this requested service.
            l_guiResponse =
                new GuiResponse(p_guiRequest,
                                GuiResponse.C_KEY_SERVICE_NOT_ATTEMPTED,
                                new Object[] {"update Memo"},
                                GuiResponse.C_SEVERITY_WARNING);

            // Add the response to the screen data object as a normal
            // update type response.
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_guiResponse);
        }
        else
        {
            l_isResolved = (l_status.equals("RESOLVED")) ? true : false;

            // Call the service...
            l_memoResponse =
                    i_ccMemoService.updateMemo(p_guiRequest,
                                               i_timeout,
                                               l_text,
                                               new PpasDateTime(l_raisedOn),
                                               l_isResolved);

            // The addRetrievalResponse method is being called here
            // as well as the addUpdateResponse as specified by the
            // sub-system design, because this service is different
            // in that the update and retrieval are combined into
            // one call, where-as others are two seperate calls
            // i.e. update followed by retrieval.
            p_screenData.addRetrievalResponse(p_guiRequest,
                                              l_memoResponse);
            p_screenData.addUpdateResponse(p_guiRequest,
                                           l_memoResponse);

            if (l_memoResponse.isSuccess())
            {
                p_screenData.setMemoData(l_memoResponse.getMemoData());
                if (l_memoResponse.getMemoData().isResolved())
                {
                    p_screenData.setOpenMemoCountDecremented();
                }
            }
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_END,
                p_guiRequest,
                C_CLASS_NAME, 57090, this,
                "Leaving " + C_METHOD_updateMemo);
        }

        return;

    } // end private void updateMemo(...)

} // end class CcMemoServlet
