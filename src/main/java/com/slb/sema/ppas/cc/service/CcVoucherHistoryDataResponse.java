////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CcVoucherHistoryDataResponse.java
//      DATE            :       14-Dec-2005
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :       PpaLon#1892/7603
//                              
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       CcXResponse class for VoucherHistoryData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE     | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+--------------------
//  DD/MM/YY | <name>     | <brief description of           | <reference>
//           |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.service;

import java.util.Locale;

import com.slb.sema.ppas.common.dataclass.VoucherHistoryData;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.support.GuiRequest;

/**
 * CcXResponse class for VoucherHistoryData objects.
 */
public class CcVoucherHistoryDataResponse extends GuiResponse
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcVoucherHistoryDataResponse";

    //-------------------------------------------------------------------------
    // Instance Variables
    //-------------------------------------------------------------------------
    /** The VoucherHistoryData object wrapped by this object. */
    private VoucherHistoryData         i_voucherHistoryData;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------
    /**
     * Uses a VoucherHistoryData object and the additional necessary objects to create a response containing message
     * data and voucher data.
     * @param p_guiRequest The request being processed.
     * @param p_messageLocale Locale for the response. Can be null in which case the default Locale will be
     * used.
     * @param p_messageKey Response message key.
     * @param p_messageParams Any parameters to be substituted into response message.
     * @param p_messageSeverity Severity of response.
     * @param p_VoucherHistoryData The voucher history details.
     */
    public CcVoucherHistoryDataResponse(GuiRequest p_guiRequest,
            Locale p_messageLocale,
            String p_messageKey,
            Object[] p_messageParams,
            int p_messageSeverity,
            VoucherHistoryData p_VoucherHistoryData)
    {
        super(p_guiRequest, p_messageLocale, p_messageKey, p_messageParams, p_messageSeverity);

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_START,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10000,
                           this,
                           "Constructing " + C_CLASS_NAME);
        }

        i_voucherHistoryData = p_VoucherHistoryData;

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE,
                           WebDebug.C_APP_SERVICE,
                           WebDebug.C_ST_CONFIN_END,
                           p_guiRequest,
                           C_CLASS_NAME,
                           10010,
                           this,
                           "Constructed " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------

    /**
     * Returns the voucher history information.
     * @return Voucher History data.
     */
    public VoucherHistoryData getVoucherHistoryData()
    {
        return (i_voucherHistoryData);
    }
}
