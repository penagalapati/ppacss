////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CcStatementOfAccountServlet.Java
//DATE            :       22-May-2007
//AUTHOR          :       Andy Harris
//REFERENCE       :       PpacLon#3124/11533
//                        PRD_ASCS00_GEN_CA_114
//
//COPYRIGHT       :       WM-data 2007
//
//DESCRIPTION     :       Servlet to handle requests from the
//                        Statement of Account screen.
//
////////////////////////////////////////////////////////////////////////////////
//                          CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//         |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.cc.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.screendata.CcStatementOfAccountScreenData;
import com.slb.sema.ppas.cc.service.CcAccountService;
import com.slb.sema.ppas.cc.service.CcBasicAccountDataResponse;
import com.slb.sema.ppas.cc.service.CcStatementOfAccountResponse;
import com.slb.sema.ppas.cc.service.CcStatementOfAccountService;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.web.servlet.PpasServlet;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.gui.support.GuiSession;

/**
* Servlet for handling requests from the Statement of Account screen.
*/
public class CcStatementOfAccountServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CcStatementOfAccountServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    /** The CC Statement of Account Service to be used by this servlet. */
    private CcStatementOfAccountService i_ccStmtAccService = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /** Constructor, only calls the superclass constructor. */

    public CcStatementOfAccountServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 10000, this,
                           "Constructing " + C_CLASS_NAME);
        }

        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 10090, this,
                           "Constructed " + C_CLASS_NAME);
        }
    }
 
    //-------------------------------------------------------------------------
    // public Methods
    //-------------------------------------------------------------------------

    /**
     * Currently does nothing.
     */
    public void doInit()
    {
//        final String L_PROP_PREFIX = "com.slb.sema.ppas.cc.servlet.CcCallHistoryServlet.";
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_START,
                           C_CLASS_NAME, 20000, this,
                           "Entered doInit().");
        }

//        PpasProperties l_prop = i_guiContext.getProperties();
//        i_defaultMaxRows = l_prop.getProperty(L_PROP_PREFIX + "defaultCallsToRetrieve", "100");

//        i_maxRetrievalLimit = l_prop.getProperty(L_PROP_PREFIX + "maxCallsToRetrieve", "200");
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_VLOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_CONFIN_END,
                           C_CLASS_NAME, 20090, this,
                           "Leaving doInit().");
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doService = "doService";
    /**
     * Service method to control requests and responses to the JSP script for the Statement of Account screen service.
     * @param p_request Message request.
     * @param p_response Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(HttpServletRequest p_request,
                               HttpServletResponse p_response,
                               HttpSession p_httpSession,
                               GuiRequest p_guiRequest)
    {
        String l_command                                   = null;
        String l_forwardUrl                                = null;
        CcStatementOfAccountScreenData l_stmtAccScreenData = null;
        boolean l_accessGranted                            = true;
        GuiResponse l_guiResponse                          = null;
        GuiSession l_guiSession                            = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 94300, this,
                           "ENTERED " + C_METHOD_doService);
        }
        
        //  Construct CcStatementOfAccountService if it does not already exist.
        if (i_ccStmtAccService == null)
        {
            i_ccStmtAccService = new CcStatementOfAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        
        l_stmtAccScreenData = new CcStatementOfAccountScreenData(i_guiContext, p_guiRequest);
        
        try
        {
            // Get the command from the http request
            l_command = getValidParam (p_guiRequest,
                                       0,
                                       p_request,
                                       "p_command",
                                       true,    // Mandatory ...
                                       "",       // ... so no default
                                       new String []{
                                           "GET_SCREEN_DATA",
                                           "RESET_SCREEN_DATA",
                                           "RETRIEVE_SOA_DATA"});

            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_APPLICATION_DATA | WebDebug.C_ST_TRACE,
                               p_guiRequest, C_CLASS_NAME, 94400, this,
                               "Received command: " + l_command);
            }


            if (l_command.equals("GET_SCREEN_DATA")    ||
                l_command.equals("RETRIEVE_SOA_DATA") ||
                l_command.equals("RESET_SCREEN_DATA")) 
            {
                l_accessGranted = true;
            }
            if (!l_accessGranted)
            {
                l_guiResponse = new GuiResponse(p_guiRequest,
                                                p_guiRequest.getGuiSession().getSelectedLocale(),
                                                GuiResponse.C_KEY_INSUFFICIENT_PRIVILEGE,
                                                new Object [] {},
                                                GuiResponse.C_SEVERITY_FAILURE);

                l_stmtAccScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);

                l_forwardUrl = "/jsp/cc/ccaccesserror.jsp";

                p_request.setAttribute("p_guiScreenData", l_stmtAccScreenData);
            }
           else
           {
               // Indicate whether the subscriber contained in the request is a
               // subordinate.
               l_guiSession = (GuiSession) p_guiRequest.getSession();

               if (!l_guiSession.getCustId().equals(l_guiSession.getBtCustId()))
               {
                   l_stmtAccScreenData.setIsSubordinate();
               }

               if (l_command.equals("GET_SCREEN_DATA") || l_command.equals("RESET_SCREEN_DATA"))
               {
                   doGetScreenData(p_guiRequest, l_stmtAccScreenData);
               }
               else if (l_command.equals("RETRIEVE_SOA_DATA"))
               {
                   doRetrieveSoaData(p_request, p_guiRequest, l_stmtAccScreenData);
               }
            }
        }
        catch (PpasServletException l_pSE)
        {
            l_guiResponse = handleInvalidParam(p_guiRequest, l_pSE);

            l_stmtAccScreenData.addRetrievalResponse(p_guiRequest, l_guiResponse);
            if (WebDebug.on)
            {
                WebDebug.print(WebDebug.C_LVL_LOW, WebDebug.C_APP_SERVLET, WebDebug.C_ST_ERROR,
                               p_guiRequest, C_CLASS_NAME, 94050, this,
                               "Caught exception:\n" + l_pSE);
            }
        }
        if (l_accessGranted)
        {
            if (l_stmtAccScreenData.hasRetrievalError())
            {
                l_forwardUrl = "/jsp/cc/ccerror.jsp";
            }
            else
            {
                if (l_command.equals("GET_SCREEN_DATA")    ||
                    l_command.equals("RETRIEVE_SOA_DATA") ||
                    l_command.equals("RESET_SCREEN_DATA"))
                {
                    l_forwardUrl = "/jsp/cc/ccstatementofaccount.jsp";
                } 
                else if(l_command.equals("GET_HISTORY_DETAIL"))
                {
                    l_forwardUrl = "/jsp/cc/ccstatementofaccount.jsp";
                }
            }
            p_request.setAttribute("p_guiScreenData", l_stmtAccScreenData);
        }
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_END | WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 94590, this,
                           "LEAVING " + C_METHOD_doService + ", forwarding to " + l_forwardUrl);
        }
        return (l_forwardUrl);
    }

    //-------------------------------------------------------------------------
    // private Methods
    //-------------------------------------------------------------------------
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doGetScreenData = "doGetScreenData";

    /**
     * Get the Screen related data.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Statement Of Account screen.
     */
    private void doGetScreenData(GuiRequest p_guiRequest, CcStatementOfAccountScreenData p_screenData)
    {
        CcAccountService              l_ccAccService               = null;
        PpasDate                      l_actDate                    = null;
        PpasDate                      l_configDate                 = null;
        CcBasicAccountDataResponse    l_ccBasicAccDataResponse     = null;
        int                           l_startDateOffset;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28000, this,
                           "Entered " + C_METHOD_doGetScreenData);
        }
        
        PpasProperties l_prop = i_guiContext.getProperties();
        l_startDateOffset = l_prop.getIntProperty("com.slb.sema.ppas.cc.servlet.CcStatementOfAccountServlet." + 
                                                  "startDateOffset", 5);
        
        //Construct CcAccountService if it does not already exist ...
        if (l_ccAccService == null)
        {
            l_ccAccService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        l_ccBasicAccDataResponse = l_ccAccService.getBasicData(p_guiRequest, i_timeout);
        l_actDate = l_ccBasicAccDataResponse.getBasicAccountData().getActivationDate();
        
        l_configDate = DatePatch.getDateToday();
        l_configDate.add(PpasDate.C_FIELD_DATE, l_startDateOffset * -1);
        
        if(l_actDate.after(l_configDate))
        {        
            p_screenData.setStartDate(l_actDate);
        }
        else 
        {
            p_screenData.setStartDate(l_configDate);
        }        
        p_screenData.setEndDate(DatePatch.getDateToday());
    }  
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doRetrieveSoaData = "doRetrieveSoaData";
    /**
     * Get the Screen related data.
     * @param p_request The servlet request being processed.
     * @param p_guiRequest The GUI request being processed.
     * @param p_screenData Data associated with the Statement Of Account screen.
     * @throws PpasServletException If the request could not be satisfied.
     */
    private void doRetrieveSoaData(HttpServletRequest p_request,
                                    GuiRequest p_guiRequest, 
                                    CcStatementOfAccountScreenData p_screenData)
            throws PpasServletException
    {
        CcStatementOfAccountResponse  l_ccSOAResponse            = null;
        String                        l_startDateStr             = null;
        String                        l_endDateStr               = null;
        CcAccountService              l_ccAccService             = null;
        CcBasicAccountDataResponse    l_ccBasicAccDataResponse   = null;
        PpasDate                      l_actDate                  = null;
        PpasDate                      l_configDate               = null;
        PpasDate                      l_startDate                = null;
        
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_HIGH, WebDebug.C_APP_SERVLET, WebDebug.C_ST_START,
                           p_guiRequest, C_CLASS_NAME, 28100, this,
                           "Entered " + C_METHOD_doRetrieveSoaData);
        }
        
        // Construct CcAccountService if it does not already exist ...
        if (l_ccAccService == null)
        {
            l_ccAccService = new CcAccountService(p_guiRequest, 0, i_logger, i_guiContext);
        }
        l_ccBasicAccDataResponse = l_ccAccService.getBasicData(p_guiRequest, i_timeout);
        l_actDate = l_ccBasicAccDataResponse.getBasicAccountData().getActivationDate();
        
        l_configDate = DatePatch.getDateToday();
        l_configDate.add(PpasDate.C_FIELD_DATE,-30);
        
        // Calculate the current date minus a configurable time period. Ask about the
        // configurable time period.
        // Set the screen start date to the later date.  
        if(l_actDate.after(l_configDate))
        {        
            l_startDate = l_actDate;
        }
        else
        {
            l_startDate = l_configDate;
        }        
        
        l_startDateStr =
            getValidParam(p_guiRequest,
                          0,
                          p_request,
                          "p_startDate",
                          true,  // parameter is mandatory ...
                          l_startDate.toString(),     
                          PpasServlet.C_TYPE_DATE);
    
        l_endDateStr =
                getValidParam(p_guiRequest,
                              0,
                              p_request,
                              "p_endDate",
                              true,  // parameter is mandatory ...
                              DatePatch.getDateToday().toString(),     
                              PpasServlet.C_TYPE_DATE);
        
          l_ccSOAResponse = i_ccStmtAccService.getStatementOfAccountDetails(p_guiRequest,
                                                                         i_timeout,
                                                                         new PpasDate(l_startDateStr),
                                                                         new PpasDate(l_endDateStr));
 
        p_screenData.addRetrievalResponse(p_guiRequest, l_ccSOAResponse);

        if (!p_screenData.hasRetrievalError())
        {
           p_screenData.setStatementOfAccountDataSet(l_ccSOAResponse.getstatementOfAccountData());
           p_screenData.setStartDate(new PpasDate(l_startDateStr));
           p_screenData.setEndDate(new PpasDate(l_endDateStr));        
        }
  
        if (WebDebug.on)
        {
            WebDebug.print(WebDebug.C_LVL_MODERATE, WebDebug.C_APP_SERVLET, WebDebug.C_ST_REQUEST,
                           p_guiRequest, C_CLASS_NAME, 77531, this,
                           "LEAVING " + C_METHOD_doRetrieveSoaData);
        }
    }   

} //end class CcStatementOfAccountServlet
