////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TestServlet.Java
//      DATE            :       24-Jan-2002
//      AUTHOR          :       Erik Clayton
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Test servlet for handling test URLs that can
//                              then be forwarded to an arbitary JSP.
//
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.cc.servlet;

import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.slb.sema.ppas.cc.service.TestResponse;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasRuntimeException;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasServletException;
import com.slb.sema.ppas.common.support.ServletKey;
import com.slb.sema.ppas.common.web.support.WebDebug;
import com.slb.sema.ppas.gui.localisation.GuiResponse;
import com.slb.sema.ppas.gui.servlet.GuiServlet;
import com.slb.sema.ppas.gui.support.GuiContext;
import com.slb.sema.ppas.gui.support.GuiRequest;
import com.slb.sema.ppas.util.instrumentation.MultiTimer;

/**
 * Test servlet for GUI.
 */
public class TestServlet extends GuiServlet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TestServlet";

    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Constructor, only calls the superclass constructor. */
    public TestServlet()
    {
        super();

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing TestServlet()");
        }

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10090, this,
                "Constructed TestServlet()");
        }

        return;
    }

    //-------------------------------------------------------------------------
    // Overridden Superclass Methods
    //-------------------------------------------------------------------------
    /** Overrides superclass service method to allow us to deal with login
     *  requests.
     * @param p_request  The HTTP request being processed.
     * @param p_response The response.
     * @throws javax.servlet.ServletException If an error occurs in the servlet.
     * @throws java.io.IOException If an IO error occurs.
     */
    public void service(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response)
        throws
            javax.servlet.ServletException,
            java.io.IOException
    {
        String        l_forwardUrl = null;
        GuiRequest   l_guiRequest = new GuiRequest (null);
        GuiResponse  l_guiResponse = null;

        String        l_userName = "";
        String        l_password = "";
        int           l_timerId;
        MultiTimer    l_timer = null;
        GuiContext   l_guiContext;

        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_MODERATE,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_REQUEST,
                C_CLASS_NAME, 11200, null,
                "Servlet path is [" + p_request.getServletPath() + "]");
        }

        Exception l_initE = getInitException();

        if (l_initE != null)
        {
            // Reset the URL to forward to the general JSP page for handling
            // a response with no service specific data.
            l_forwardUrl = "/jsp/gui/noservicedataxml.jsp";

            String l_msgText = "";
            if (l_initE instanceof PpasException)
            {
                l_msgText = ((PpasException)l_initE).getMsgText (Locale.UK);
            }
            else if (l_initE instanceof PpasRuntimeException)
            {
                l_msgText = ((PpasRuntimeException)l_initE).
                                                     getMsgText (Locale.UK);
            }
            else
            {
                l_msgText = l_initE.toString();
            }

            l_guiResponse = new GuiResponse
                                (l_guiRequest,
                                 GuiResponse.C_KEY_SYS_INIT_FAILURE,
                                 new Object [] {l_msgText},
                                 GuiResponse.C_SEVERITY_FAILURE);

            p_request.setAttribute("p_guiRequest", l_guiRequest);

            l_guiContext = new GuiContext (l_guiRequest, i_logger);
            p_request.setAttribute("p_guiContext", l_guiContext);

            // Store the response in the request
            p_request.setAttribute("p_guiResponse",
                                   l_guiResponse);
        }
        else
        {
            try
            {
                l_timer = i_timerSet.getNewTimer();
                l_timerId = l_timer.start(i_timerSetRegisteredNameId);
                p_request.setAttribute ("com.slb.sema.ppas.util.MultiTimer",
                                       l_timer);

                // Increment requests received counter.
                i_requestsReceivedCounterInstrument.increment();

                // Create a GuiRequest object and do related processing...
                l_guiRequest = createGuiRequest(p_request,
                                                  p_response,
                                                  l_timer);

                //Erik's code in here

                String l_servletPath = p_request.getServletPath();

                System.out.println("Test servlet path: " + l_servletPath);

                if (l_servletPath.equals("/cc/Session"))
                {
                    l_userName = p_request.getParameter("loginName");
                    l_password = p_request.getParameter("loginPassword");


                    if (WebDebug.on)
                    {
                        WebDebug.print(
                            WebDebug.C_LVL_MODERATE,
                            WebDebug.C_APP_SERVLET,
                            WebDebug.C_ST_REQUEST,
                            C_CLASS_NAME, 14320, null,
                            "UserName is [" + l_userName + "]");
                    }

                    if (((l_userName == null) || (l_userName.equals(""))) ||
                        ((l_password == null) || (l_password.equals(""))))
                    {
                        l_forwardUrl = "/jsp/cc/login.jsp";
                    }
                    else
                    {
                        // Assume username/password is correct
                        l_forwardUrl = "/html/cc/selectframes.html";
                    }
                }
                else if (l_servletPath.equals("/cc/Select"))
                {
                    TestResponse l_testResponse = new TestResponse();
                    String l_action = p_request.getParameter("action");
                    Msisdn l_msisdn = null;

                    try
                    {
                        l_msisdn = i_guiContext.getMsisdnFormatInput().parse(
                                    p_request.getParameter("msisdn"));
                    }
                    catch (ParseException e)
                    {
                        PpasServletException l_pSE = new PpasServletException
                                       (C_CLASS_NAME,
                                        "service",
                                        11601,
                                        this,
                                        (PpasRequest)p_request,
                                        0,
                                        ServletKey.get().parseMsisdnError(
                                                        p_request.getParameter("msisdn"),
                                                        i_guiContext.getMsisdnFormatInput().toPattern()),
                                        e);

                        i_logger.logMessage(l_pSE);

                        throw l_pSE;
                    }

                    if (l_msisdn != null)
                    {
                        // There is an MSISDN attached to the request
                        l_forwardUrl = "/html/cc/mainframes.html";

                        // Also need to attach the selected MSISDN to the session
                        l_guiRequest.setMsisdn (l_msisdn);
                    }
                    else if (l_action.equals("showcriteria"))
                    {
                        l_forwardUrl = "/jsp/cc/selectcriteria.jsp";
                    }
                    else if (l_action.equals("showresults"))
                    {
                        l_testResponse.setShowSearchResults(true);
                        l_forwardUrl = "/jsp/cc/selectresults.jsp";
                    }

                    p_request.setAttribute("p_testResponse",
                                            l_testResponse);
                }
                else if (l_servletPath.equals("/cc/Account"))
                {
                    // l_forwardUrl = new String("/jsp/cc/accountdetails.jsp");
                    l_forwardUrl = "/cc/AccountDetails";
                }
                else if (l_servletPath.equals("/cc/Customer"))
                {
                    l_forwardUrl = "/jsp/cc/customer.jsp";
                }
                else if (l_servletPath.equals("/cc/Adjustments"))
                {
                    l_forwardUrl = "/jsp/cc/adjustments.jsp";
                }
                else if (l_servletPath.equals("/cc/Disconnect"))
                {
                    l_forwardUrl = "/jsp/cc/disconnect.jsp";
                }
                else if (l_servletPath.equals("/cc/Menu"))
                {
                    l_forwardUrl = "/jsp/cc/menu.jsp";
                }
                else if (l_servletPath.equals("/cc/AdditionalInfo"))
                {
                    //l_forwardUrl = new String("/jsp/cc/ccadditionalinfo.jsp");
                    l_forwardUrl = "/cc/AdditionalInfo";
                }
                else if (l_servletPath.equals("/cc/Payments"))
                {
                    l_forwardUrl = "/jsp/cc/payments.jsp";
                }
                else if (l_servletPath.equals("/cc/Promotions"))
                {
                    l_forwardUrl = "/jsp/cc/ccpromotions.jsp";
                }
                else if (l_servletPath.equals("/cc/Subordinates"))
                {
                    l_forwardUrl = "/jsp/cc/subordinates.jsp";
                }
                else if (l_servletPath.equals("/cc/Vouchers"))
                {
                    l_forwardUrl = "/jsp/cc/vouchers.jsp";
                }
                else if (l_servletPath.equals("/cc/ContextCustomer"))
                {
                    l_forwardUrl = "/jsp/cc/contextcustomer.jsp";
                }
                else if (l_servletPath.equals("/cc/ContextBalance"))
                {
                    l_forwardUrl = "/jsp/cc/contextbalance.jsp";
                }
                else if (l_servletPath.equals("/cc/ContextStatus"))
                {
                    l_forwardUrl = "/jsp/cc/contextStatus.jsp";
                }
                else if (l_servletPath.equals("/cc/Memo"))
                {
                    l_forwardUrl = "/jsp/cc/memo.jsp";
                }

                // Increment requests received counter.
                i_requestsProcessedCounterInstrument.increment();
                l_timer.end(l_timerId);

            }
            catch (PpasServletException l_servletE)
            {
                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = "/jsp/gui/noservicedataxml.jsp";

                // Handle the case of an invalid or missing parameter in the
                // incoming request
                l_guiResponse = handleInvalidParam(l_guiRequest, l_servletE);
            }

            // Add catch all so that we can generate GUI response if a runtime
            // exception has occurred
            catch (RuntimeException l_rtE)
            {
                String l_exceptionTxt = "";
                if (!(l_rtE instanceof PpasRuntimeException))
                {
                    // Should generate and log exception descended from
                    // PpasRuntimeException here ... but for the time being,
                    // just output to system console.
                    l_rtE.printStackTrace(System.err);

                    l_exceptionTxt = l_rtE.toString();
                }
                else
                {
                    l_exceptionTxt = ((PpasRuntimeException)l_rtE).
                                                    getMsgText (Locale.UK);
                }

                // Reset the URL to forward to the general JSP page for handling
                // a response with no service specific data.
                l_forwardUrl = "/jsp/gui/noservicedataxml.jsp";

                l_guiResponse = new GuiResponse
                                    (l_guiRequest,
                                     GuiResponse.C_KEY_RUNTIME_FAILURE,
                                     new Object [] {l_exceptionTxt},
                                     GuiResponse.C_SEVERITY_FAILURE);

                p_request.setAttribute("p_guiRequest", l_guiRequest);

                l_guiContext = new GuiContext (l_guiRequest, i_logger);
                p_request.setAttribute("p_guiContext", l_guiContext);
            }

            // Append the response to the Http request.
            p_request.setAttribute("p_guiResponse", l_guiResponse);

        } // end if (not system initialisation failure)

        forwardToUrl(p_request, p_response, l_forwardUrl, l_guiRequest);

        return;
    }

    //-------------------------------------------------------------------------
    // Object Methods
    //-------------------------------------------------------------------------
    /**
     * Currently does nothing.
     */
    public void doInit()
    {
        if (WebDebug.on)
        {
            WebDebug.print(
                WebDebug.C_LVL_VLOW,
                WebDebug.C_APP_SERVLET,
                WebDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "In TestServlet.doInit()");
        }

        // Should probably be constructing PosiSessionService here...
        // unfortunately no PosiRequest is available.

        return;
    }

    /**
     * Must be implemented in order to extend GuiServlet but actually
     * has no functional role.
     * @param p_request    Message request.
     * @param p_response   Message response.
     * @param p_httpSession Session details.
     * @param p_guiRequest The GUI request being processed.
     * @return URL to display screen.
     */
    protected String doService(
        HttpServletRequest     p_request,
        HttpServletResponse    p_response,
        HttpSession            p_httpSession,
        GuiRequest            p_guiRequest)
    {
        String                 l_forwardUrl = null;

        return(l_forwardUrl);
    }
}
