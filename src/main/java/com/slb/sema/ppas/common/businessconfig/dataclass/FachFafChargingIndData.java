////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FachFafChargingIndData.java
//      DATE            :       11-Mar-2004
//      AUTHOR          :       Neil Raymond
//      REFERENCE       :       PRD_ASCS_GEN_CA_11
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       A record of FaF charging indicator data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Object holding charging indicator data configuration.
 */
public class FachFafChargingIndData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FachFafChargingIndData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Charging indicator. */
    private String i_chargingInd = null;
    
    /** Charging indicator description. */
    private String i_desc = null;
    
    /** Defines if this record has been deleted. */
    private boolean i_deleted = false;
    
    /** The opid that last updated this record. */
    private String i_opid = null;
    
    /** The date/time of the last update. */
    private PpasDateTime i_genYmdhms = new PpasDateTime("");
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of <code>FachFafChargingIndData</code>.
     * @param p_chargingInd the charging indicator represented by this object
     * @param p_desc a description of this charging indicator
     * @param p_deleted whether this record is marked as deleted in the database
     * @param p_opid the opid that last updated this record
     * @param p_genYmdhms date/time of the last update
     */
    public FachFafChargingIndData(String       p_chargingInd,
                                  String       p_desc,
                                  boolean      p_deleted,
                                  String       p_opid,
                                  PpasDateTime p_genYmdhms)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_chargingInd    = p_chargingInd;
        i_desc           = p_desc;
        i_deleted        = p_deleted;
        i_opid           = p_opid;
        i_genYmdhms      = p_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the charging indicator this this object represents.
     * @return the charging indicator
     */
    public String getChargingInd()
    {
        return i_chargingInd;
    }

    /**
     * Get the charging indicator description.
     * @return description of this charging indicator record
     */
    public String getDesc()
    {
        return i_desc;
    }
    
    /**
     * Get the deleted status of this record.
     * @return <code>true</code> if the record is deleted, else <code>false</code>
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }

    /**
     * The opid that last updated this record.
     * @return opid of last update
     */
    public String getOpid()
    {
        return i_opid;
    }

    /**
     * The date/time that this record was last updated.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }
}
