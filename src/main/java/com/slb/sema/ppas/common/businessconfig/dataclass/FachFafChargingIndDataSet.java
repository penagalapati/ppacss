////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FachFafChargingIndDataSet.java
//      DATE            :       11-Mar-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS00_GEN_CA_11
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Holds a set of FachFafChargingIndData objects
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | Copyright info changed.         | PpacLon#1755/7425
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | New method added in order to    | PpacLon#1755/7425
//          |            | return all the available fach   |
//          |            | records, i.e. includes those    |
//          |            | marked as deleted.              |
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction      | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Object containing a set of <code>FachFafChargingIndDataSet</code> objects.
 */
public class FachFafChargingIndDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Constant used for calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FachFafChargingIndDataSet";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Array of <code>FachFafChargingIndData</code>. */
    private FachFafChargingIndData[] i_fachAllArr = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs instance of <code>FachFafChargingIndDataSet</code>.
     * @param p_fachArr an array of <code>FachFafChargingIndData</code> objects
     */
    public FachFafChargingIndDataSet(FachFafChargingIndData[] p_fachArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_fachAllArr = p_fachArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Gets the <code>FachFafChargingIndData</code> object that matches the supplied
     * charging indicator. <b>Only looks at active records.</b>
     * @param p_chargingInd charging indicator
     * @return instance of <code>FachFafChargingIndData</code> if one can be found that
     *         matches the supplied charging indicator, else <code>null</code> 
     */
    public FachFafChargingIndData getFachFafCharingIndData(String p_chargingInd)
    {
        FachFafChargingIndData l_fachData = null;
        
        for (int i = 0; (l_fachData == null) && (i < i_fachAllArr.length); i++)
        {
            if ((i_fachAllArr[i].getChargingInd().equals(p_chargingInd)) &&
                !i_fachAllArr[i].isDeleted())
            {
                l_fachData = i_fachAllArr[i];
            }
        }
        return l_fachData;
    }
    

    /**
     * Returns Faf Charging Indicators data associated with a specified charging indicator id .
     * @param p_chargingInd The Faf Charging Indicators id.
     * 
     * @return FachFafChargingIndData record from this DataSet with the given Charging Indicator ID. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public FachFafChargingIndData getRecord (String p_chargingInd)
    {
        FachFafChargingIndData l_fachfafChargingIndRecord = null;
        
        if (p_chargingInd != null)
        {
            for (int l_loop = 0; l_loop < i_fachAllArr.length; l_loop++)
            {
                if ( i_fachAllArr[l_loop].getChargingInd().equals(p_chargingInd))                
                {
                    l_fachfafChargingIndRecord = i_fachAllArr[l_loop];
                    break;
                }
            }
        }

        return (l_fachfafChargingIndRecord);
    }

    
    /**
     * Gets all <code>FachFafChargingIndData</code> records including those that have
     * been deleted.
     * @return array of <code>FachFafChargingIndData</code> objects
     */
    public FachFafChargingIndData[] getFachARR()
    {
        return i_fachAllArr;
    }
    
    /** Get set of all available F&F Charging Indicator configuration records.
     * 
     * @return Set of all available (non-deleted) records.
     */
    public FachFafChargingIndData[] getAvailableArray()
    {
        FachFafChargingIndData  l_availableArr[];
        Vector                  l_availableV = new Vector();
        int                     l_loop;
        FachFafChargingIndData  l_emptyArr[] = new FachFafChargingIndData[0];

        for(l_loop = 0; l_loop < i_fachAllArr.length; l_loop++)
        {
            if (!i_fachAllArr[l_loop].isDeleted())
            {
                // Data not deleted, add to temporary vector.
                l_availableV.addElement(i_fachAllArr[l_loop]);
            }
        }

        l_availableArr = (FachFafChargingIndData[])l_availableV.toArray(l_emptyArr);

        return l_availableArr;
    }
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_fachAllArr, p_sb);
    }
}

