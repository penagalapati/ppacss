////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PrplPromoPlanCache.Java
//      DATE            :       21-Feb-2001
//      AUTHOR          :       Matt Kirk
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A cacheing Promotion Plan business configuration
//                              service (prpl_promo_plan and prom_promotion).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 13/05/02 | N Fletcher | Just changed a couple of        | PpaLon#1194/5659
//          |            | to state that prom_promotion    |
//          |            | table data is also stored in    |
//          |            | this cache now.                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.PrplPromoPlanSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * A cacheing Promotion Plan business configuration
 * service (prpl_promo_plan and prom_promotion).
 */
public class PrplPromoPlanCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrplPromoPlanCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The Promotion Plan service to use to load cache.
     */
    private PrplPromoPlanSqlService i_prplPromoPlanSqlService = null;

    /** Cache of all Promotion Plans (including ones marked as deleted). */
    private PrplPromoPlanDataSet i_allPrplPromoPlans = null;

    // Note: A cached 'available' promotion plan cannot be used since the filtering mechanism relies on the
    //       plans that are active at the current time - this will change with each call so the 'available'
    //       data set must be re-created each time.
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Promotion Plan cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public PrplPromoPlanCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing PrplPromoPlanCache()");
        }

        i_prplPromoPlanSqlService = new PrplPromoPlanSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed PrplPromoPlanCache()");
        }
    } // End of public constructor
      //         PrplPromoPlanCache(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all Promotion Plans (including ones marked as deleted).
     * @return All promotion plans.
     */
    public PrplPromoPlanDataSet getAll()
    {
        return(i_allPrplPromoPlans);
    } // End of public method getAll

    /**
     * Returns available Promotion Plans (not including ones marked as deleted).
     * @return Available promotion plans.
     */
    public PrplPromoPlanDataSet getAvailable ()
    {
        return new PrplPromoPlanDataSet(null, i_logger, i_allPrplPromoPlans.getAvailableArray());
    } // End of public method getAvailable

    /**
     * Reloads the static configuration into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10140, this,
                "Entered reload");
        }

        i_allPrplPromoPlans = i_prplPromoPlanSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10150, this,
                "Leaving reload");
        }
    } // End of public method reload(PpasRequest, long, connection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allPrplPromoPlans);
    }
}

