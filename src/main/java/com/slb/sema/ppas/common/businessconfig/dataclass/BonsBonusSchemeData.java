//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonsBonusSchemeData.java
// DATE            :       14-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of bonus scheme data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 21/06/07 | M Erskine  | Add bonus scheme type.          | PpacLon#3183/11753
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Object holding bonus scheme data configuration. */
public class BonsBonusSchemeData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonsBonusSchemeData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Bonus scheme. */
    private String              i_bonusSchemeId;
    
    /** Bonus scheme type. 'S' for Standard or 'A' for Accumulated. Default is 'S' */
    private char                i_schemeType;

    /** Bonus scheme description. */
    private String              i_desc;

    /** Start date of this scheme. */
    private PpasDate            i_startDate;
    
    /** End date of this scheme. */
    private PpasDate            i_endDate;
    
    /** Bonus opt-in service offering. */
    private Long                i_optinServOff;
    
    /** Boolean to indicate if the scheme is operational. */
    private boolean             i_bonusActive;

    /** Defines if this record has been deleted. */
    private boolean             i_deleted;

    /** The opid that last updated this record. */
    private String              i_opid;

    /** The date/time of the last update. */
    private PpasDateTime        i_genYmdhms;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of <code>BonsBonusSchemeData</code>.
     * 
     * @param p_bonusSchemeId The bonus scheme represented by this object.
     * @param p_schemeType 'S' for Standard or 'A' for Accumulated.
     * @param p_desc Description of this channel.
     * @param p_startDate Start date of this scheme.
     * @param p_endDate End date of this scheme.
     * @param p_optinServOff The Bonus Opt-in Service Offering.
     * @param p_bonusActive boolean to indicate if scheme is active.
     * @param p_deleted Whether this record is marked as deleted in the database.
     * @param p_opid The opid that last updated this record.
     * @param p_genYmdhms date/time of the last update.
     */
    public BonsBonusSchemeData(String       p_bonusSchemeId,
                               char         p_schemeType,
                               String       p_desc,
                               PpasDate     p_startDate,
                               PpasDate     p_endDate,
                               Long         p_optinServOff,
                               boolean      p_bonusActive,
                               boolean      p_deleted,
                               String       p_opid,
                               PpasDateTime p_genYmdhms)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonusSchemeId = p_bonusSchemeId;
        i_schemeType    = p_schemeType;
        i_desc          = p_desc;
        i_startDate     = p_startDate;
        i_endDate       = p_endDate;
        i_optinServOff  = p_optinServOff;
        i_bonusActive   = p_bonusActive;
        i_deleted       = p_deleted;
        i_opid          = p_opid;
        i_genYmdhms     = p_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the bonus scheme this object represents.
     * @return the bonus scheme
     */
    public String getBonusSchemeId()
    {
        return i_bonusSchemeId;
    }
    
    /**
     * Get type of bonus scheme.
     * @return the bonus scheme type.
     */
    public char getSchemeType()
    {    
        return (i_schemeType == 'A') ? i_schemeType : 'S';
    }

    /**
     * Get the bonus scheme description.
     * @return description of this bonus scheme record
     */
    public String getDesc()
    {
        return i_desc;
    }
    
    /**
     * Gets the start date of the bonus scheme. 
     * @return Start date of the bonus scheme.
     */
    public PpasDate getStartDate()
    {
        return i_startDate;
    }
    
    /**
     * Gets the end date of the bonus scheme. 
     * @return End date of the bonus scheme.
     */
    public PpasDate getEndDate()
    {
        return i_endDate;
    }
    
    /**
     * Get the deleted status of this record.
     * @return <code>true</code> if the record is deleted, else <code>false</code>
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }

    /**
     * The opid that last updated this record.
     * @return opid of last update
     */
    public String getOpid()
    {
        return i_opid;
    }

    /**
     * The date/time that this record was last updated.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }
    
    /**
     * The Bonus Optin Service Offering.
     * @return Bonus Optin Service Offering.
     */    
    public Long getOptinServOff()
    {
        return i_optinServOff;
    }
    
    /**
     * Indicates if the scheme is flagged as active.
     * @return <code>true</code> if the scheme is active, else <code>false</code>
     */  
    public boolean isBonusActive()
    {
        return i_bonusActive;
    }
    
    /** Identify whether this data record is available for use.
     * 
     * @param p_today Today's date.
     * @return True if this record is not deleted, is flagged as active and today is between the start and
     *         end dates.
     */
    public boolean isAvailable(PpasDate p_today)
    {
        return !isDeleted() && isBonusActive() && !i_startDate.after(p_today) && !i_endDate.before(p_today);
    }
    
    /** Identify whether this scheme is relevant.
     * 
     * @param p_offerings Customer Service Offerings.
     * @return True if this scheme is relevant.
     */
    public boolean isRelevant(ServiceOfferings p_offerings)
    {
        boolean l_relevant = false;
         
        if (i_optinServOff == null)
        {
            l_relevant = true;
        }
        else
        {
            if (p_offerings != null)
            {
                long l_cust = p_offerings.getValue();
                long l_this = i_optinServOff.longValue();
                
                l_relevant = (l_cust & l_this) == l_this;
            }
        }
        
        return l_relevant;
    }
}
