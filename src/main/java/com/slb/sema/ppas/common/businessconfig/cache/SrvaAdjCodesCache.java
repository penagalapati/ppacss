////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAdjCodesCache.Java
//      DATE            :       14-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the ajustment codes
//                              data held in the srva_adj_codes table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAdjCodesDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAdjCodesSqlService;
import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * A cache for the Adjustment Codes business configuration data
 * (srva_adj_codes).
 */
public class SrvaAdjCodesCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAdjCodesCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private SrvaAdjCodesSqlService i_srvaAdjCodesSqlService = null;

    /** Array of ALL adjustment codes.  */
    private SrvaAdjCodesData []    i_allSrvaAdjCodesArr = null;
    
    /**
     * HashMap of caches. Each element of the HasMap containing a DataSet of
     * available adjustment codes (i.e. not marked as deleted) for the same
     * Market. 
     */
    private HashMap  i_availableByMarket = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Adjustment Codes cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public SrvaAdjCodesCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 11110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_srvaAdjCodesSqlService = new SrvaAdjCodesSqlService (p_request, p_logger);

        i_availableByMarket = new HashMap();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 11120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
   
    /** 
     * Returns a AdjustmentCodes DataSet for a specific Market.
     * @param  p_market The Market.
     * @return A AdjustmentCodes DataSet for a specific Market, null if not found.  
     */
    public synchronized SrvaAdjCodesDataSet getAvailable(Market  p_market)
    {
        SrvaAdjCodesDataSet l_srvaAdjCodesDataSet = null;
        
        // If DataSet for Market is not in the HashMap, 
        // build it from the Array
        l_srvaAdjCodesDataSet = (SrvaAdjCodesDataSet) i_availableByMarket.get(p_market);
        if (l_srvaAdjCodesDataSet == null)
        {
            l_srvaAdjCodesDataSet = buildDataSetFromArray(p_market);
            
            // Store DataSet in HashMap
            i_availableByMarket.put(p_market, l_srvaAdjCodesDataSet);
        }
        
        return l_srvaAdjCodesDataSet;
    }
   
    /**
     * Get adjustment data for a given market and adjustment code/type.
     * @param p_market The market for which the adjustment applies.
     * @param p_adjType Type of the adjustment.
     * @param p_adjCode Adjustment code.
     * @return Active adjustment data for the key.
     */
    public SrvaAdjCodesData getAdjustmentData(
                                Market  p_market, 
                                String  p_adjType, 
                                String  p_adjCode)
    {
        SrvaAdjCodesData    l_srvaAdjCodesData = null;
        SrvaAdjCodesDataSet l_srvaAdjCodesDataSet = getAvailable(p_market);
        if (l_srvaAdjCodesDataSet != null)
        {
            l_srvaAdjCodesData = l_srvaAdjCodesDataSet.getAdjustmentData(p_adjType, p_adjCode);
        }
        return l_srvaAdjCodesData;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest     p_request,
                        JdbcConnection  p_connection)
        throws PpasSqlException
    {
        SrvaAdjCodesDataSet l_allSrvaAdjCodes = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 11230, this,
                "Entered " + C_METHOD_reload);
        }

        // Read data from DB into DataSet
        l_allSrvaAdjCodes = i_srvaAdjCodesSqlService.readAll(p_request, p_connection);

        // Retrieve Array of AdjustmentData
        i_allSrvaAdjCodesArr = l_allSrvaAdjCodes.getAllArray();

        // Reset HashMap
        i_availableByMarket = new HashMap();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 11290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData((DataSetObject)null);
    }

    // Note: This toVerboseString appears to be non-generic so leave for now.
    /**
     * Returns a verbose string representation of this object. Can be used for
     * trace and debugging.
     * @return Verbose string representing the data held in this cache.
     */
    public String toVerboseString()
    {
        StringBuffer        l_verboseSB;
        Set                 l_keySet = null;
        Iterator            l_iter = null;
        SrvaAdjCodesDataSet l_srvaAdjCodesDataSet = null;
        Market              l_market = null;
        
        l_verboseSB = new StringBuffer(1000);

        l_verboseSB.append("SrvaAdjCodesCache=[\n");
        if (i_availableByMarket == null)
        {
            l_verboseSB.append("null");
        }
        else
        {
            // Loop on Map entries
            l_keySet = i_availableByMarket.keySet();
            l_iter = l_keySet.iterator();
            while (l_iter.hasNext())
            {
                l_market = (Market)(l_iter.next());
                l_verboseSB.append("--> Market=[" + l_market + "]\n");                
                l_srvaAdjCodesDataSet = 
                    (SrvaAdjCodesDataSet)(i_availableByMarket.get(l_market));
                l_verboseSB.append (l_srvaAdjCodesDataSet.toVerboseString());
                l_verboseSB.append("\n");
            }
        }
        l_verboseSB.append("\n]");

        return(l_verboseSB.toString());
    }
    
    /**
     * Builds a set of Adjustment Codes for the Market, from the internal Array 
     * of Adjustment Codes.
     * @param p_market The Market
     * @return a set of Adjustment Codes
     */
    private SrvaAdjCodesDataSet buildDataSetFromArray(Market p_market)
    {
        SrvaAdjCodesDataSet  l_srvaAdjCodesDataSet = null;
        SrvaAdjCodesData []  l_srvaAdjCodesArr = null;
        
        l_srvaAdjCodesArr = buildAvailableArray (
                                        p_market,
                                        i_allSrvaAdjCodesArr);
        
        l_srvaAdjCodesDataSet = new SrvaAdjCodesDataSet(
                                        null, // p_request
                                        p_market,
                                        l_srvaAdjCodesArr);
        
        return l_srvaAdjCodesDataSet;
    }
    
    /**
     * Build a data set containing the available adjustment records (i.e.
     * those not marked as deleted, based on a given market (srva/sloc
     * combination).
     * @param  p_market Market to be used in selecting adjustment records to be returned.
     * @param  p_allSrvaAdjCodesArr Array of all SRVA ADJ Codes.
     * @return Array containing available adjustment code records in the
     *         cache (i.e. those not marked as deleted) based on the given
     *         market (srva/sloc combination).
     */
    private SrvaAdjCodesData [] buildAvailableArray (
                                    Market             p_market,
                                    SrvaAdjCodesData[] p_allSrvaAdjCodesArr)
    {
        int               l_loop;
        SrvaAdjCodesData  l_srvaAdjCodesArr[] = null;
        int               l_arrLen = 0;
        SrvaAdjCodesData  l_srvaAdjCodesData = null;
        HashMap           l_hm = null;
        Market            l_tmpMarket = null;
        AdjustmentData    l_adjData = null;
        SrvaAdjCodesData  l_hmEntry = null;
        SrvaAdjCodesData  l_newSrvaAdjCodeData = null;
        
        // ---------------
        // Fill-in HashMap
        // ---------------
        if (p_allSrvaAdjCodesArr != null)
        {
            l_hm = new HashMap();
            l_arrLen = p_allSrvaAdjCodesArr.length;

            for (l_loop = 0; l_loop < l_arrLen; l_loop++)
            {
                l_srvaAdjCodesData = p_allSrvaAdjCodesArr[l_loop];
                l_adjData = new AdjustmentData(
                                        l_srvaAdjCodesData.getAdjType(),
                                        l_srvaAdjCodesData.getAdjCode());
                if ( !l_srvaAdjCodesData.isDeleted() )
                {
                    l_tmpMarket = l_srvaAdjCodesData.getMarket();
                    if ( l_tmpMarket.equals(Market.C_GLOBAL_MARKET) )
                    {
                        // Put only if the explicit Market is not there already
                        l_hmEntry = (SrvaAdjCodesData)l_hm.get(l_adjData);
                        if ( l_hmEntry == null || 
                            !l_hmEntry.getMarket().equals(p_market) )
                        {
                            l_hm.put(l_adjData, l_srvaAdjCodesData);                        
                        }
                    }
                    else
                    if ( l_tmpMarket.equals(p_market) )
                    {
                        // Always put in the HashMap
                        l_hm.put(l_adjData, l_srvaAdjCodesData);                        
                    }
                }
            } // for()
            
            // ------------------------
            // Convert HashMap to Array
            // ------------------------
            l_srvaAdjCodesArr = convertHashMapToArray(l_hm);         
            
            // -------------------------------------------
            // Set explicit Market for each entry in Array
            // -------------------------------------------
            l_arrLen = l_srvaAdjCodesArr.length;

            for (l_loop = 0; l_loop < l_arrLen; l_loop++)
            {
                l_srvaAdjCodesData = l_srvaAdjCodesArr[l_loop];
                
                l_newSrvaAdjCodeData = new SrvaAdjCodesData(
                                            l_srvaAdjCodesData.getRequest(),
                                            l_srvaAdjCodesData.getLogger(),
                                            p_market,
                                            l_srvaAdjCodesData.getAdjType(),
                                            l_srvaAdjCodesData.getAdjCode(),
                                            l_srvaAdjCodesData.getAdjDesc(),
                                            l_srvaAdjCodesData.getGenYmdhms(),
                                            l_srvaAdjCodesData.getOpid(),
                                            ' ' // p_delFlag
                                            );
                
                l_srvaAdjCodesArr[l_loop] = l_newSrvaAdjCodeData;
            }   
        }
        else
        {
            l_srvaAdjCodesArr = new SrvaAdjCodesData[0];
        }

        return (l_srvaAdjCodesArr);

    } // End of private method buildAvailableArray

    /**
     * Converts a HashMap to an Array of SrvaAdjCodesData.
     * @param p_hm a HashMap of SrvaAdjCodesData.
     * @return an Array of SrvaAdjCodesData.
     */
    private SrvaAdjCodesData [] convertHashMapToArray(HashMap p_hm)
    {
        SrvaAdjCodesData [] l_arrayRet = null;
        int                 l_i        = 0;
        Iterator            l_iter     = null;
        if (p_hm != null)
        {
            l_arrayRet = new SrvaAdjCodesData [p_hm.size()];
            l_iter = p_hm.values().iterator();
            l_i = 0;
            while ( l_iter.hasNext() )
            {
                l_arrayRet[l_i++] = (SrvaAdjCodesData)(l_iter.next());
            }
        }
        else
        {
            l_arrayRet = new SrvaAdjCodesData [0];
        }
        return l_arrayRet;
    }          

    /** A data record corresponding to a (AdjType, AdjCode) couple. */
    class AdjustmentData
    {
        //------------------------------------------------------------------------
        // Private instance attributes
        //------------------------------------------------------------------------
        /** The Adjustment Type. */
        private String i_adjType;

        /** The Adjustment Code. */
        private String i_adjCode;

        //------------------------------------------------------------------------
        // Public constructors
        //------------------------------------------------------------------------
        /** Creates an Adjustment Data object.
         * 
         * @param p_adjType The Adjustment Type.
         * @param p_adjCode The Adjustment Code.
         */
        public AdjustmentData(
                        String  p_adjType,
                        String  p_adjCode)
        {
            i_adjType = p_adjType;
            i_adjCode = p_adjCode;
        }

        //------------------------------------------------------------------------
        // Public methods
        //------------------------------------------------------------------------

        /** Overrides the hashCode() method.
         * @return a constant value.
         */
        public int hashCode()
        {
            return 1;
        }

        /** Overrides the equals() method.
         * @param p_object The Object to test for equality.
         * @return true if the Object passed is equivalent to this.
         */
        public boolean equals(Object p_object)
        {
            boolean l_ret = false;
            if (p_object != null)
            {
                if ( p_object instanceof AdjustmentData &&
                     i_adjType.equals(((AdjustmentData)p_object).i_adjType) && 
                     i_adjCode.equals(((AdjustmentData)p_object).i_adjCode) )
                {
                    l_ret = true;
                }
            }
            return l_ret;
        }
    
    } // class AdjustmentData
}
