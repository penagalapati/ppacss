////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BalanceEnquiryResultData.Java
//      DATE            :       06-Aug-2001
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PpaLon#989/3919
//                              PRD_PPAK00_GEN_CA_274
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Contains the Data returned when a successful
//                              balance inquiry is made.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 11-Sep-02 | N Fletcher    | Changes so that balance    | PpaLon#1509/6319
//           |               | can be displayed with the  |
//           |               | correct precision in a     |
//           |               | GUI (e.g. CUST_CARE).      |
//-----------+---------------+----------------------------+---------------------
// 25-Sep-02 | S Nelson      | Changes so that Dedicated  | PpaLon#1484/6164
//           |               | Acounts will be added to   |
//           |               | The balance enquiry        |
//           |               | Response                   |
//-----------+---------------+----------------------------+---------------------
// 17-Sep-03 | Martin Brister| Removed attributes and     | PpacLon#43/384
//           |               | methods relating to pending|
//           |               | credit and service days.   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Contains the Data returned when a successful install is made.
 */
public class BalanceEnquiryResultData extends DataObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BalanceEnquiryResultData";

    //------------------------------------------------------------------------
    // Instance Variables.
    //------------------------------------------------------------------------

    /** MSISDN returned from the balance enquiry request. */
    private Msisdn i_msisdn;

    /** Balance returned from the balance enquiry request. */
    private Money i_balance;

    /** Service expiry date returned from the balance enquiry request. */
    private PpasDate i_servExpDate;

    /** Airtime expiry date returned from the balance enquiry request. */
    private PpasDate i_airExpDate;

    /** Date/time at which this object was constructed. This will be used to
     *  provided an aproximate time at which the balance was obtained for
     *  display purposes.
     */
    private PpasDateTime  i_balanceDT;

    /** Dedicated Account DataSet returned from the balance enquiry request. */
    private DedicatedAccountsDataSet i_dedicatedAccountsDataSet;

    /** Service Class returned from the balance enquiry request. */
    private ServiceClass i_serviceClass = null;
    
    /** Subscirber's temp blocking status. */
    private boolean i_tempBlocked;
    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    
    /**
     * Creates BalanceEnquiryResultData and initialises data to null.
     */
    public BalanceEnquiryResultData()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10011, this,
                            "Constructing " + C_CLASS_NAME );
        }
                 
        i_msisdn                     = null;
        i_balance                    = null;
        i_dedicatedAccountsDataSet   = null;
        i_servExpDate                = new PpasDate("");
        i_airExpDate                 = new PpasDate("");
        i_serviceClass               = null;
        i_balanceDT                  = DatePatch.getDateTimeNow(); 
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10091, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    /**
     * Creates BalanceEnquiryResultData and initialises data.
     *
     * @param p_ppasRequest The request being processed.
     * @param p_msisdn  The MSISDN returned from the balance enquiry request.
     * @param p_balance The balance returned from the balance enquiry request.
     * @param p_servExpDate Date string, in the format ccyymmdd, defining the 
     *                      service expiry date of the subscriber whose balance
     *                      was obtained. If the subscriber's service expiry
     *                      date had not been set, then this parameter 
     *                      contains the string "00000000".
     * @param p_airExpDate Date string, in the format ccyymmdd, defining the 
     *                     airtime expiry date of the subscriber whose balance
     *                     was obtained. If the subscriber's airtime expiry
     *                     date had not been set, then this parameter 
     *                     contains the string "00000000".
     * @param p_dedicatedAccountsDataSet Set of dedicated accounts data.
     */
    public BalanceEnquiryResultData (
                            PpasRequest              p_ppasRequest,
                            Msisdn                   p_msisdn,
                            Money                    p_balance,
                            String                   p_servExpDate,
                            String                   p_airExpDate,
                            DedicatedAccountsDataSet p_dedicatedAccountsDataSet)
    {
        super (p_ppasRequest);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START,
                            p_ppasRequest, C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_msisdn                     = p_msisdn;
        i_balance                    = p_balance;

        i_dedicatedAccountsDataSet   = p_dedicatedAccountsDataSet;

        // Result set data is constructed with "00000000" if the service 
        // expiry date is not set
        if ( !p_servExpDate.equals("00000000"))
        {
            // Construct Date object from service expiry date string in the
            // format ccyymmdd.
            i_servExpDate = new PpasDate(p_servExpDate,
                                         PpasDate.C_DF_yyyyMMdd);
        }
        else
        {
            // Service expiry date not set - store as an unset PPAS date.
            i_servExpDate = new PpasDate("");
        }

        // Result set data is constructed with "00000000" if the airtime 
        // expiry date is not set
        if ( !p_airExpDate.equals("00000000"))
        {
            // Construct Date object from airtime expiry date string in the
            // format ccyymmdd.
            i_airExpDate = new PpasDate(p_airExpDate,
                                        PpasDate.C_DF_yyyyMMdd);
        }
        else
        {
            // Service expiry date not set - store as an unset PPAS date.
            i_airExpDate = new PpasDate("");
        }

        // Store the current date/time as the aproximate time at which this 
        // balance data was read

        i_balanceDT = DatePatch.getDateTimeNow ();  

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_END,
                            p_ppasRequest, C_CLASS_NAME, 10090, this,
                            "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Retrieves the subscribers MSISDN.
     * @return The subscribers MSISDN.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /**
     * Retrieves the subscribers balance. 
     * @return The subscribers balance.
     */
    public Money getBalance()
    {
        return(i_balance);
    }

    // PpaLon#1509/6319 [BEGIN]
    /**
     * Get the balance formatted with the precision of the balance currency.
     * The value is intended for immediate display in a GUI.
     * @return Formatted balance string.
     */ 
    public String getGuiBalance()
    {   
        return (i_context.getMoneyFormat().format(i_balance));
    }
    // PpaLon#1509/6319 [END] 


    /**
     * Returns the service expiry date of the account whose balance was
     * obtained.
     * @return The subscribers service expiry date.
     */
    public PpasDate getServExpDate()
    {
        return(i_servExpDate);
    }

    /**
     * Retrieves the airtime expiry date of the account whose balance was
     * obtained.
     * @return The subscribers airtime expiry date.
     */
    public PpasDate getAirExpDate()
    {
        return(i_airExpDate);
    }

    /**
     * Gets the aproximate date/time at which the balance data was obtained.
     * @return The aproximate date/time at which the balance data was obtained.
     */
    public PpasDateTime getBalanceTime()
    {
        return (i_balanceDT);
    }

    /** 
     * Returns a String representation of the object.
     * Useful for debugging.
     * @return A string representation of the <code>BalanceEnquiryResultData</code>.
     */
    public String toString()
    {
        StringBuffer l_buffer = new StringBuffer(70);

        l_buffer.append("BalanceEnquiryResultData@");
        l_buffer.append(this.hashCode());
        l_buffer.append(", MSISDN=[ " + i_msisdn);
        l_buffer.append("], balance=[" + i_balance);
        l_buffer.append("], service expiry date=[" + i_servExpDate);
        l_buffer.append("], airtime expiry date=[" + i_airExpDate);
        l_buffer.append("], service class=[" + i_serviceClass);
        l_buffer.append("], balance date time=[" + i_balanceDT);
        l_buffer.append("], dedicated accounts data set=[" 
                        + i_dedicatedAccountsDataSet + "]");

        return (l_buffer.toString());
    }

    /**
     * Assigns the values for dedicated accounts that are returned from the
     * response.
     * @param p_dedicatedAccountsDataSet A set of Dedicated Account Data for a subscriber.
     */
    public void setDedicatedAccounts(DedicatedAccountsDataSet p_dedicatedAccountsDataSet)
    {
        i_dedicatedAccountsDataSet = p_dedicatedAccountsDataSet;
    }

    /**
     * Retrieves a set of Dedicated Account Data for a subscriber.
     * @return Subscribers dedicated account data.
     */
    public DedicatedAccountsDataSet getDedicatedAccounts()
    {
         return (i_dedicatedAccountsDataSet);
    }

    /**
     * Sets the Airtime Expiry Date.
     * @param p_airExpDate Subscribers airtime expiry date.
     */
    public void setAirExpDate(PpasDate p_airExpDate) 
    {
        i_airExpDate = p_airExpDate;
    }

    /**
     * Sets the Account balance.
     * @param p_balance Subscribers balance.
     */
    public void setBalance(Money p_balance) 
    {
        i_balance = p_balance;
    }

    /**
     * Sets the MSISDN.
     * @param p_msisdn Subscribers MSISDN.
     */
    public void setMsisdn(Msisdn p_msisdn) 
    {
        i_msisdn = p_msisdn;
    }

    /**
     * Sets the Service Expiry Date.
     * @param p_servExpDate Subscribers Service Expiry Date.
     */
    public void setServExpDate(PpasDate p_servExpDate) 
    {
        i_servExpDate = p_servExpDate;
    }

    /**
     * Sets the Service Class.
     * @param p_serviceClass Subscribers service class.
     */
    public void setServiceClass(ServiceClass p_serviceClass) 
    {
        i_serviceClass = p_serviceClass;
    }

    /** 
     * Returns Service Class.
     * @return Subscribers Service Class
     */
    public ServiceClass getServiceClass()
    {
        return i_serviceClass;
    }
    
    /**
     * Set the subscriber's temp blocking status.
     * @param p_tempBlocked <code>true</code> is SET, <code>false</code> is CLEAR
     */
    public void setTempBlocked(boolean p_tempBlocked)
    {
        i_tempBlocked = p_tempBlocked;
    }
    
    /**
     * Get the subscriber's temp blocking status.
     * @return <code>true</code> is SET, <code>false</code> is CLEAR
     */
    public boolean isTempBlocked()
    {
        return i_tempBlocked;
    }
}
////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////