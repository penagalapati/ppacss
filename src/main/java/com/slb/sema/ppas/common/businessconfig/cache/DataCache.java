////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DataCache.java
//      DATE            :       12-Jan-2006
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Super class for cache.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.StringDumpHelper;
import com.slb.sema.ppas.common.dataclass.Verbosable;

/** Super class for cache. */
public abstract class DataCache
{
    /** Data dumper. */
    private static final StringDumpHelper C_DUMPER = new StringDumpHelper(
                      "com.slb.sema.ppas.common.businessconfig.cache.DataCache");

    /** New line terminator. */
    protected static final String C_NL = System.getProperty("line.separator");

    /** Get a detailed string representation of this object.
     * 
     * @return This object as a detailed string.
     */
    public String toVerboseString()
    {
        StringBuffer l_sb = new StringBuffer();

        l_sb.append(toString());
        l_sb.append("=[");

        Object [] l_dataSets = getData();

        if (l_dataSets != null)
        {
            for (int i = 0; i < l_dataSets.length; i++)
            {
                Object o = l_dataSets[i];

                l_sb.append(o == null ? "null" :
                            o instanceof Verbosable ? ((Verbosable)o).toVerboseString() :
                            o.toString());

                if (i < l_dataSets.length - 1)
                {
                    l_sb.append(C_NL);
                }
            }
        }

        l_sb.append("]");

        return l_sb.toString();
    }

    /** Get data sets to format.
     *
     *  @return Array of Data Sets.
     */
    protected abstract Object[] getData();

    /** Get data sets to format.
     *
     *  @param p_data Data Object.
     *  @return Array of Data Sets.
     */
    protected Object[] getData(DataObject p_data)
    {
        return new Object[]{p_data};
    }

    /** Get data sets to format.
     *
     *  @param p_set Data set.
     *  @return Array of Data Sets.
     */
    protected Object[] getData(DataSetObject p_set)
    {
        return new Object[]{p_set};
    }

    /** Get a list of the data objects.
     *
     *  @param p_all       Data set of all data.
     *  @param p_available Data set of available data.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData(DataSetObject p_all, DataSetObject p_available)
    {
        return new Object[]{"All=[", p_all, "]" + C_NL + "Available=[", p_available, "]"};
    }
}

