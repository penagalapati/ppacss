////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAgentMstrDataSet.Java
//      DATE            :       15-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of agent data (from srva_agent_mstr).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 08/11/06 | Yang Liu   | Added method                    | PpacLon#2079/10278
//          |            | getRawAvailableArray() used by  |
//          |            | BOI 'Agent Infomation' Screen.  |
//----------+------------+---------------------------------+--------------------
// 30/03/07 | Andy Harris| Add support for suspect flag    | PpacLon#3011/11241
//          |            | column.                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * An object that contains a set of an agents data, each element of
 * the set representing a record from the srva_agent_mstr table.
 */
public class SrvaAgentMstrDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAgentMstrDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of agents data records. That is, the data of this DataSet object. */
    private SrvaAgentMstrData []     i_agentsArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of agents data.
     * 
     * @param p_agentsArr Array holding the agents data records to be stored by this object.
     */
    public SrvaAgentMstrDataSet(SrvaAgentMstrData []    p_agentsArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 C_CLASS_NAME, 14510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_agentsArr = p_agentsArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 14590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns the available agents records for a given market.
     * The returned data array does not include records marked as deleted.
     * @param p_market The market forming part of the key on which to chose 
     *                 the data to be returned.
     * @return Array containing available agent records based on the given
     *         market (srva/sloc combination).
     */
    public SrvaAgentMstrData [] getAvailableArray (Market  p_market)
    {
        SrvaAgentMstrData   l_data = null;
        SrvaAgentMstrData   l_globMktData = null;
        SrvaAgentMstrData   l_specMktData = null;
        ArrayList           l_globMktDataAL = null;
        HashMap             l_specMktDataHM = null;
        ArrayList           l_retAL = null;
        int                 l_len = 0;
        Set                 l_set = null;
        Iterator            l_iter = null;
        SrvaAgentMstrData[] l_availableAgentsArr = null;
        String              l_agent = null;

        l_len = i_agentsArr.length;

        // First loop : retrieve non-deleted GlobalMarket entries
        l_globMktDataAL = new ArrayList();
        for (int l_i = 0; l_i < l_len; l_i++)
        {
            l_data = i_agentsArr[l_i];
            if ( l_data.getMarket().equals(Market.C_GLOBAL_MARKET) && 
                !l_data.isDeleted() )
            {
                l_globMktDataAL.add(l_data);
            }
        }

        // Second loop : retrieve non-deleted SpecificMarket entries
        l_specMktDataHM = new HashMap();
        for (int l_i = 0; l_i < l_len; l_i++)
        {
            l_data = i_agentsArr[l_i];
            if ( l_data.getMarket().equals(p_market) && 
                !l_data.isDeleted() )
            {
                l_specMktDataHM.put(l_data.getAgent(), l_data);
            }
        }
        
        // Create return ArrayList
        l_retAL = new ArrayList();
        
        // Travel the ArrayList of GlobMkt entries
        l_len = l_globMktDataAL.size();
        for (int l_i = 0; l_i < l_len; l_i++)
        {
            l_globMktData = (SrvaAgentMstrData)l_globMktDataAL.get(l_i);
            // Check if entry is also in HM of SpecMkt entries
            l_specMktData = (SrvaAgentMstrData)l_specMktDataHM
                                                        .get(l_globMktData.getAgent());
            if (l_specMktData != null)
            {
                // Use Specific Market entry
                l_retAL.add(l_specMktData); 
                // Remove it from HM
                l_specMktDataHM.remove(l_globMktData.getAgent());
            }     
            else
            {
                // Use Global Market entry
                l_retAL.add(l_globMktData); 
            }     
        }

        // If the SpecMkt HM still contains entries, add them to
        // the return ArrayList
        l_set = l_specMktDataHM.keySet();
        l_iter = l_set.iterator();
        while (l_iter.hasNext())
        {
            l_agent = (String)l_iter.next();
            l_specMktData = (SrvaAgentMstrData)l_specMktDataHM.get(l_agent);
            l_retAL.add(l_specMktData); 
        }
        
        // Convert the return ArrayList into an Array
        // Set p_market into returned entries.
        l_len = l_retAL.size();
        l_availableAgentsArr = new SrvaAgentMstrData[l_len];
        for (int l_i = 0; l_i < l_len; l_i++)
        {
            l_data = (SrvaAgentMstrData)l_retAL.get(l_i);
            l_availableAgentsArr[l_i] = new SrvaAgentMstrData(
                                                null, // p_request
                                                p_market,
                                                l_data.getAgent(),
                                                l_data.getAgentName(),
                                                l_data.getGenYmdhms(),
                                                l_data.getOpid(),
                                                ' ',
                                                l_data.getSuspectFlag());
        }        
        
        return (l_availableAgentsArr);

    } // End of public method getAvailableArray


    /** Get a record of agent data based on a given agent identifier and 
     *  market.
     *  @param p_agent The agent identifier defining the record to be selected.
     *  @param p_market The market defining the record to be selected.
     *  @return SrvaAgentMstrData object containing the data selected for the 
     *          given agent identifier and market. If no record could be found 
     *          with the given agent identifier in the given market, then 
     *          <code>null</code> is returned.
     */
    public SrvaAgentMstrData getAgentDetails (String  p_agent,
                                              Market  p_market)
    {
        SrvaAgentMstrData  l_data = null;
        SrvaAgentMstrData  l_dataGlobMkt = null;
        SrvaAgentMstrData  l_dataSpecMkt = null;
        Market             l_market = null;
        int                l_len = 0;

        if (i_agentsArr != null)
        {
            l_len = i_agentsArr.length;
            for (int l_i = 0; l_i < l_len; l_i++)
            {
                l_data = i_agentsArr[l_i];

                if ( l_data.getAgent().equals(p_agent) )
                {
                    l_market = l_data.getMarket();
                    if (l_market.equals(Market.C_GLOBAL_MARKET))
                    {
                        l_dataGlobMkt = l_data;
                    } 
                    else 
                    if (l_market.equals(p_market))
                    {
                        l_dataSpecMkt = l_data;
                        break;
                    }
                }
            }
        }

        if (l_dataSpecMkt != null)
        {
            l_data = l_dataSpecMkt;
        }
        else
        if (l_dataGlobMkt != null)        
        {
            l_data = l_dataGlobMkt;
        }
        else
        {
            l_data = null;
        }

        // Instanciate new entry
        // Set p_market into returned entry.
        if (l_data != null)
        {
            l_data = new SrvaAgentMstrData(
                                null, // p_request
                                p_market,
                                l_data.getAgent(),
                                l_data.getAgentName(),
                                l_data.getGenYmdhms(),
                                l_data.getOpid(),
                                l_data.isDeleted() ? '*' : ' ',
                                l_data.getSuspectFlag());
        }

        return (l_data);

    } // end method getAgentDetails


    /** Get a record of agent data based on a given agent identifier.
     *  @param p_agent The agent identifier defining the record to be selected.
     *  @return SrvaAgentMstrData object containing the data selected for the 
     *          given agent. If no record could be found 
     *          with the given agent identifier in the given market, then 
     *          <code>null</code> is returned.
     */
    public SrvaAgentMstrData getAgentDetails (String  p_agent)
    {
        SrvaAgentMstrData  l_data = null;
        SrvaAgentMstrData  l_dataGlobMkt = null;
        SrvaAgentMstrData  l_dataSpecMkt = null;
        int                l_len = 0;

        if (i_agentsArr != null)
        {
            l_len = i_agentsArr.length;
            for (int l_i = 0; l_i < l_len; l_i++)
            {
                l_data = i_agentsArr[l_i];

                if ( l_data.getAgent().equals(p_agent) )
                {
                    if (l_data.getMarket().equals(Market.C_GLOBAL_MARKET))
                    {
                        l_dataGlobMkt = l_data;
                    } 
                    else 
                    {
                        l_dataSpecMkt = l_data;
                        break;
                    }
                }
            }
        }

        if (l_dataSpecMkt != null)
        {
            l_data = l_dataSpecMkt;
        }
        else
        if (l_dataGlobMkt != null)        
        {
            l_data = l_dataGlobMkt;
        }
        else
        {
            l_data = null;
        }

        return (l_data);

    } // end method getAgentDetails


    /**
     * Returns the available agents records. This does not include records marked as
     * deleted.
     * @return Array containing all available agent records.
     */
    public SrvaAgentMstrData [] getAvailableArray()
    {
        SrvaAgentMstrData l_availableAgentsArr[];
        Vector            l_availableV = new Vector();
        int               l_loop;
        SrvaAgentMstrData l_agentsEmptyArr[] = new SrvaAgentMstrData[0];

        for (l_loop = 0; l_loop < i_agentsArr.length; l_loop++)
        {
            if (!i_agentsArr[l_loop].isDeleted())
            {
                l_availableV.addElement(i_agentsArr[l_loop]);
            }
        }

        l_availableAgentsArr =
              (SrvaAgentMstrData[])l_availableV.toArray(l_agentsEmptyArr);

        return(l_availableAgentsArr);

    } // End of public method getAvailableArray()

    /** Get available data for a given code and market.
     *  <p>
     *  Create a new SrvaAgentMstrData Array that is a subset of the data in this 
     *  object where the records selected are based on given specified market
     *  and are currently available (i.e. not marked as deleted).
     * 
     * @param p_market The market defining the record to be selected.
     * @return Data Array containing a subset of the data in this class, based on
     *         the given market value.
     */
    public SrvaAgentMstrData [] getRawAvailableArray(Market p_market)
    {
        SrvaAgentMstrData l_availableAgentsArr[];
        Vector            l_availableV = new Vector();
        SrvaAgentMstrData l_agentsEmptyArr[] = new SrvaAgentMstrData[0];
        
        for (int i = 0; i < i_agentsArr.length; i++)
        {
            if ((i_agentsArr[i].getMarket().equals(p_market)) &&
                    !i_agentsArr[i].isDeleted())
            {
                l_availableV.addElement(i_agentsArr[i]);
            }
        }
        l_availableAgentsArr =
            (SrvaAgentMstrData[])l_availableV.toArray(l_agentsEmptyArr);
        
        return l_availableAgentsArr;
    } 
    
    /** Returns array of all records in this data set object.
     * 
     * @return Array of all Service Area master records.
     */
    public SrvaAgentMstrData [] getAllArray()
    {
        return (i_agentsArr);

    } // end method getAllArray

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_agentsArr, p_sb);
    }
}

