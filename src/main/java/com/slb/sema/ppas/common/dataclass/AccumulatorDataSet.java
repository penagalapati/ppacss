////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatorDataSet.Java
//      DATE            :       08-Oct-2002
//      AUTHOR          :       Matt Kirk
//      REFERENCE       :       PpaLon#1577/6758
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       A DataSet for holding AccumulatorData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

import java.io.Serializable;

/**
 * A DataSet for holding AccumulatorData objects.
 */
public class AccumulatorDataSet implements Serializable
{
    //--------------------------------------------------------------------------
    // Class level constants.
    //--------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccumulatorDataSet";

    /** Maximum possible number of dedicated accounts. Value is {@value}. */
    public static final int  C_MAX_ACCUMULATORS = 5;
    //--------------------------------------------------------------------------
    // Instance Variables.
    //--------------------------------------------------------------------------
    /** Stores a set of <CODE>AccumulatorData</CODE> objects. */
    private Vector i_accumulatorDataV = null;

    //--------------------------------------------------------------------------
    // Constructors.
    //--------------------------------------------------------------------------
    /** 
     * Creates a new AccumulatorDataSet instance.
     * 
     * @param p_request The request to process.
     * @param p_initSize Initial number of records to store.
     * @param p_growSize Number of records to increase the data set by.
     */
    public AccumulatorDataSet(PpasRequest p_request,
                              int         p_initSize,
                              int         p_growSize)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                          PpasDebug.C_APP_BUSINESS,
                          PpasDebug.C_ST_CONFIN_START,
                          p_request,
                          C_CLASS_NAME, 10000, this,
                         "Constructing " + C_CLASS_NAME);
        }

        i_accumulatorDataV = new Vector(p_initSize, p_growSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                          PpasDebug.C_APP_BUSINESS,
                          PpasDebug.C_ST_CONFIN_END,
                          p_request,
                          C_CLASS_NAME, 10010, this,
                         "Constructed " + C_CLASS_NAME);
        }
    } // End Constructor

    //--------------------------------------------------------------------------
    // Object methods.
    //--------------------------------------------------------------------------
    /** 
     * Adds an element to the AccumulatorData Vector.
     * 
     * @param p_newElement The element to be added.
     */ 
    public void addElement(AccumulatorData p_newElement)
    {
        i_accumulatorDataV.addElement(p_newElement);
    }
    
    /**
     * Removes an element from the AccumulatorData Vector.
     * 
     * @param p_element The element to be removed.
     */
    public void removeElement(AccumulatorData p_element)
    {
        i_accumulatorDataV.removeElement(p_element);
    }


    /**
     * Returns a Vector containing a set of AccumulatorData objects.
     * 
     * @return Vector of accumulators.
     */
    public Vector getAccumulators()
    {
        return i_accumulatorDataV;
    }
    
    /** Show the accumulator data det as a <code>String</code>.
     * 
     * @return String representation of the accumulator data set.
     */
    public String toString()
    {
        String l_return = null;
        
        l_return = "AccumulatorDataSet=" + i_accumulatorDataV.toString();
        
        return l_return;
    }

} // end public class AccumulatorDataSet
