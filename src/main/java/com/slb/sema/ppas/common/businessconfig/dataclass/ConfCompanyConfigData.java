////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ConfCompanyConfigData.Java
//      DATE            :       10-July-2001
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#955/3784
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Class containing company configuration data as
//                              part of business config cache. This data relates
//                              to the database table conf_company_config.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                     | REFERENCE
//---------+---------------+---------------------------------+-----------------
//22/03/06 | M.Toernqvist  | Handles the new column values   | PpasLon#2020/8257
//         |               | retrieved from the table        |
//         |               | CONF_COMPANY_CONFIG for the     |
//         |               | ClassHistory feature.
//////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents the row of the database table conf_company_config.
 */
public class ConfCompanyConfigData extends DataObject
{

    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ConfCompanyConfigData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Indication of whether the system supports EUROs as well as local
     * currency.
     */
    private char i_multiCurrencyInd = 'N';

    /** The company's official currency or the default currency. */
    private PpasCurrency i_baseCurrency = null;

    /** Length of voucher serial numbers. */
    private int i_serialNumberLength = 0;

    /** Length of an IVR pin code. */
    private int i_ivrPinCodeLength = 0;

    /** Disconnection reason for removal of subscription from the MIN. */
    private String i_discReasonForRemoval = null;

    /** The temp block Status. */
    private char i_tempBlockStatus = 0;

    /** External Voucher Database flag. */
    private boolean i_evdbUsed = false;

    /** Expiry comparison date usage flag (true if set to "Expiry after date"). */
    private boolean i_expiryAfterDateInd = false;
    
    /** Call History method. */
    private char         i_callHistoryMethod;
    
    /** Call History Period, used to calculate default startDate as: today - callHistoryPeriod. */
    private int          i_callHistoryPeriod;
    
    /** Max number of CallHistoryData objects that are retrieved. */
    private int          i_maxCallHistoryRecords;


    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates a new company configuration and initialises data.
     *
     * @param p_request The request to process.
     * @param p_multiCurrencyInd Flag that indicates whether the system accepts multiple currencies.
     * @param p_baseCurrency Currency code of the main currency in the installation.
     * @param p_evdbUsed Flag indicating whether an external voucher database is used.
     * @param p_serialNumberLength Length of serial numbers for vouchers.
     * @param p_discReasonForRemoval Standard disconnection reason.
     * @param p_tempBlockStatus Default temp block status when installing an account.
     * @param p_expiryAfterDateInd Flag that indicates that the system is set to process expiry
     *        after the specified expiry date.
     * @param p_ivrPinCodeLength Length of IVR pin codes.
     * @param p_callHistoryMethod Flag indicating whether this installation uses a DWS ('D') 
     * or the ASCS Call History Server ('C') to access call history data, or does not 
     * support call history viewing ('N').
     * @param p_callHistoryPeriod Period (days) used to calculate the default date range for call
     *        history retrieval via the GUI.
     * @param p_maxCallHistoryRecords Default maximum no of records to retrieve during call history
     *        via the GUI.
     */
    public ConfCompanyConfigData(
        PpasRequest  p_request,
        char         p_multiCurrencyInd,
        PpasCurrency p_baseCurrency,
        boolean      p_evdbUsed,
        int          p_serialNumberLength,
        String       p_discReasonForRemoval,
        char         p_tempBlockStatus,
        char         p_expiryAfterDateInd,
        int          p_ivrPinCodeLength,
        char         p_callHistoryMethod,
        int          p_callHistoryPeriod,
        int          p_maxCallHistoryRecords )
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_multiCurrencyInd      = p_multiCurrencyInd;
        i_baseCurrency          = p_baseCurrency;
        i_serialNumberLength    = p_serialNumberLength;
        i_evdbUsed              = p_evdbUsed;
        i_discReasonForRemoval  = p_discReasonForRemoval;
        i_tempBlockStatus       = p_tempBlockStatus;
        i_expiryAfterDateInd    = p_expiryAfterDateInd == 'Y';
        i_ivrPinCodeLength      = p_ivrPinCodeLength;
        i_callHistoryMethod     = p_callHistoryMethod;        
        i_callHistoryPeriod     = p_callHistoryPeriod;        
        i_maxCallHistoryRecords = p_maxCallHistoryRecords;


        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS, 
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10050, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the EURO/local currency indicator.
     *
     * @return Flag indicating whether the system supports multiple currencies ('Y') or not ('N').
     */
    public char getMultiCurrencyInd()
    {
        return(i_multiCurrencyInd);
    }

    /** Returns the base currency of the company.
     *
     * @return Main currency of this installation.
     */
    public PpasCurrency getBaseCurrency()
    {
        return(i_baseCurrency);
    }

    /** Returns the voucher serial number length.
     *
     * @return Length of the serial number on vouchers.
     */
    public int getSerialNumberLength()
    {
        return(i_serialNumberLength);
    }

    /** Returns the length of a pin code used for accessing the IVR.
     *
     * @return Length of an IVR pin code.
     */
    public int getIvrPinCodeLength()
    {
        return i_ivrPinCodeLength;
    }

    /** Returns the disconnection reason for removal of subscription from the MIN.
     *
     * @return Standard disconnection reason.
     */
    public String getDiscReasonForRemoval()
    {
        return(i_discReasonForRemoval);
    }

    /** Returns the default temp block status.
     *
     * @return temp block status used for installing accounts.
     */
    public char getTempBlockStatus()
    {
        return i_tempBlockStatus;
    }

    /** Returns the external voucher database flag.
     *
     * @return Flag indicating whether vouchers are held in an external system.
     */
    public boolean getEvdbUsed()
    {
        return(i_evdbUsed);
    }

    /** Returns true if the expiry after date flag is set.
     *
     * @return Flag indicating if the expiry after date flag is set.
     */
    public boolean getExpiryAfterDateInd()
    {
        return (i_expiryAfterDateInd);
    }

    /** Return the earliest date for expiry/disconnect. This method takes into account the expiry
     * date flag to return either today or yesterday.
     * @return Yesterday if the Expiry After Date Flag is set, otherwise today.
     */
    public PpasDate getEarliestExpiryDate()
    {
        // Note: Need to get date each call because time moves on.
        PpasDate l_earliest = DatePatch.getDateToday();

        if (i_expiryAfterDateInd)
        {
            // Expire after the specified date so need yesterdays date.
            l_earliest.add(PpasDate.C_FIELD_DATE, -1);
        }

        return l_earliest;
    }
    
    /** Returns the Call History method.
     *
     * @return char defining the the call History method.
     */
    public char getCallHistoryMethod()
    {
        return i_callHistoryMethod;
    }
    
    /** Returns the Call History Period, used to calculate the default startDate as: 
     * today - callHistoryPeriod. 
     * 
     * @return call history period
     */
    public int getCallHistoryPeriod()
    {
        return i_callHistoryPeriod;
    }
    
    /** Returns the Max number of CallHistoryData objects that shall be retrieved.
     * 
     *  @return default max number of records.
     */
    public int getMaxCallHistoryRecords()
    {
        return i_maxCallHistoryRecords;
    }

} // End of public class ConfCompanyConfigData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////