////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountQueryResultData.Java
//      DATE            :       03-Nov-2003
//      AUTHOR          :       Burno Ferrand-Broussy
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       AccountData Class storing account data 
//                              associated with a single PPAS account.
//
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/12/08 | Sujatha C S| Added code for clearcredit date |CSR 995216:GUI Airtime 
//          |            | to change the status if today is|Status shows Credit 
//          |            | after clear credit date         |Cleared incorrectly
//----------+------------+---------------------------------+--------------------
// 17/03/09 | Sujatha C S| Added code for clearcredit date |CSR 1172149:GUI Airtime 
//          |            | to change the status if today is|Status shows Credit 
//          |            | after clear credit date,which   |Cleared incorrectly
//          |            | was going to expired to even    |
//          |            |the active customer              |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
/**
 * This data class contains the attributes defining the result account data object 
 * manage by ESI. Note that this class is linked to the common class AccountData
 * @see com.slb.sema.ppas.common.dataclass.AccountData for Static variable definition
 * 
 */
public class AccountQueryResultData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------
    /** Standard class name constant to be used in calls to Middleware methods. */
    private static final String C_CLASS_NAME = "AccountQueryResultData";

    //-------------------------------------------------------------------------
    //  Private instance data
    //-------------------------------------------------------------------------
    /** The original service class. */
    private ServiceClass i_originalServiceClass = null;

    /** The temporary service class. */
    private PpasDate i_tempServiceClassExpDate = new PpasDate("");

    /** The flags after. */
    private SdpFlagsData i_flagsAfter = null;

    /** The flags before. */
    private SdpFlagsData i_flagsBefore = null;

    /** The number of credit clear days. */
    private int i_creditClearDays = 0;
    
    /** The number of service removal days. */
    private int i_serviceRemovalDays = 0;

    /** The activation date. */
    private PpasDate i_activationDate = new PpasDate("");

    /** The airtime expiry date. */
    private PpasDate i_airtimeExpiryDate = new PpasDate((PpasDate)null);

    /** Mobile number of the account holder. */
    private Msisdn i_masterMsisdn = null;
    
    /** The current service class. */
    private ServiceClass i_serviceClass = null;

    /** The preferrred language. */
    private String i_preferredLanguage;

    /** The service expiry date. */
    private PpasDate i_serviceExpiryDate = new PpasDate((PpasDate)null);

    /** The date/time when IVR barring will be (or was) lifted for this customer. */
    private PpasDateTime i_ivrUnbarDateTime = new PpasDateTime("");

    /** The current promotion plan. */
    private String i_promotionPlan = null;

    /** The preferred currency. */
    private PpasCurrency i_preferredCurrency = null;

    /** The customer balance expiration date time. */
    private PpasDateTime i_custBalanceExpDateTime = new PpasDateTime("");

    /** Indicates whether the subscriber's account has been blocked temporarily or not. */
    private boolean i_tempBlockedFlag = false;
    
    /** Indicates whetherit is the IVR first call or not. */
    private boolean i_ivrFirstCall = false;

    /** Account Group Id of the subscriber's account. */
    private AccountGroupId i_accountGroupId = null;

    /** The Service Offerings of the subscriber. */
    private ServiceOfferings i_serviceOfferings = null;
    
    /** The Community Id List of the subscriber. */
    private CommunitiesIdListData i_communityIdList = null;
    
    /** USSD End of Cal Notification Id. */
    private EndOfCallNotificationId i_endOfCallNotificationId = null;
    
    /** Dedicated Account Division. */
    private DedicatedAccountsDataSet i_dedicatedAccounts = null;
    
    /** Account balance. */
    private Money                    i_accountBalance = null;
    
    /** Pin Code. */
    private PinCode                   i_pinCode = null;
    
    /** Home Region Id. */
    private HomeRegionId              i_homeRegionId = null;    
    
    
    //-------------------------------------------------------------------------
    //  Constructors
    //-------------------------------------------------------------------------
    /**
     * Constructor used when AccountData object is created by Esi.
     * 
     * @param p_masterMsisdn the master MSISDN
     * @param p_serviceClass the current service class
     * @param p_originalServiceClass the original service class
     * @param p_tempServiceClassExpDate The temporary service class expiration date
     * @param p_preferredLang The customer preferred language
     * @param p_flagsBefore The flags before sending to the SDP
     * @param p_flagsAfter The flags after return by SDP
     * @param p_serviceExpiryDate the service expiry date
     * @param p_airtimeExpiryDate the airtime expiry date
     * @param p_creditClearDays the number of credit clear days
     * @param p_serviceRemovalDays the number of service removal days
     * @param p_activationDate the activation date 
     * @param p_ivrUnbarDateTime Date/time thsi customer will (or did) have an IVR bar removed.
     * @param p_promotionPlan The associated promotion plan
     * @param p_preferredCurr The customer preferred currency
     * @param p_custBalanceExpDateTime The customer balance Expiration date time
     * @param p_tempBlockedFlag The blocked status of the account.
     * @param p_ivrFirstCall True if this is the first call to the IVR.
     * @param p_accountGroupId The Account Group Id.
     * @param p_serviceOfferings The Service Offerings.
     * @param p_communityIdList The Community Id List.
     * @param p_endOfCallNotificationId End of call notification indicator.
     * @param p_dedicatedAccounts Dedicated Accounts.
     * @param p_accountBalance Account balance.
     * @param p_pinCode Pin Code.
     * @param p_homeRegionId Home Region Id of the account.
     */
    public AccountQueryResultData(Msisdn                   p_masterMsisdn,
                                  ServiceClass             p_serviceClass,
                                  ServiceClass             p_originalServiceClass,
                                  PpasDate                 p_tempServiceClassExpDate,
                                  String                   p_preferredLang,
                                  SdpFlagsData             p_flagsBefore,
                                  SdpFlagsData             p_flagsAfter,
                                  PpasDate                 p_serviceExpiryDate,
                                  PpasDate                 p_airtimeExpiryDate,
                                  int                      p_creditClearDays,
                                  int                      p_serviceRemovalDays,
                                  PpasDate                 p_activationDate,
                                  PpasDateTime             p_ivrUnbarDateTime,
                                  String                   p_promotionPlan,
                                  PpasCurrency             p_preferredCurr,
                                  PpasDateTime             p_custBalanceExpDateTime,
                                  boolean                  p_tempBlockedFlag,
                                  boolean                  p_ivrFirstCall,
                                  AccountGroupId           p_accountGroupId,
                                  ServiceOfferings         p_serviceOfferings,
                                  CommunitiesIdListData    p_communityIdList,
                                  EndOfCallNotificationId  p_endOfCallNotificationId,
                                  DedicatedAccountsDataSet p_dedicatedAccounts,
                                  Money                    p_accountBalance,
                                  PinCode                  p_pinCode,
                                  HomeRegionId             p_homeRegionId)
    {
        if ( PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (p_activationDate != null)
        {
            i_activationDate = p_activationDate;
        }
        
        if (p_airtimeExpiryDate != null)
        {
            i_airtimeExpiryDate = p_airtimeExpiryDate;
        }

        i_masterMsisdn = p_masterMsisdn;
        i_serviceClass = p_serviceClass;
        i_originalServiceClass = p_originalServiceClass;
        
        if (p_tempServiceClassExpDate != null)
        {
            i_tempServiceClassExpDate = p_tempServiceClassExpDate;
        }

        i_preferredLanguage = p_preferredLang;
        i_flagsBefore = p_flagsBefore;
        i_flagsAfter = p_flagsAfter;
        
        if (p_serviceExpiryDate != null)
        {
            i_serviceExpiryDate = p_serviceExpiryDate;
        }

        i_creditClearDays = p_creditClearDays;
        i_serviceRemovalDays = p_serviceRemovalDays;
        
        if (p_ivrUnbarDateTime != null)
        {
            i_ivrUnbarDateTime = p_ivrUnbarDateTime;
        }

        i_promotionPlan = p_promotionPlan;
        i_preferredCurrency = p_preferredCurr;
        
        if (p_custBalanceExpDateTime != null)
        {
            i_custBalanceExpDateTime = p_custBalanceExpDateTime;
        }

        i_tempBlockedFlag = p_tempBlockedFlag;
        i_ivrFirstCall    = p_ivrFirstCall;
        i_accountGroupId = p_accountGroupId;
        i_serviceOfferings = p_serviceOfferings;
        i_communityIdList = p_communityIdList;
        i_endOfCallNotificationId = p_endOfCallNotificationId;
        i_dedicatedAccounts = p_dedicatedAccounts;
        i_accountBalance = p_accountBalance;        
        i_pinCode = p_pinCode;
        i_homeRegionId = p_homeRegionId;
        
        if ( PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10090, this,
                "Constructed " + C_CLASS_NAME + " this=" + this);
        }
    }

    //-------------------------------------------------------------------------
    //  Public instance methods
    //-------------------------------------------------------------------------

    /** 
     * Return the activation Date.
     * 
     * @return the activation Date 
     */
    public PpasDate getActivationDate()
    {
        return i_activationDate;
    }
    
    /** 
     * Return the airtime Expiration date.
     * 
     * @return the airtime Expiration date
     */
    public PpasDate getAirtimeExpiryDate()
    {
        return i_airtimeExpiryDate;
    }
    
    /** 
     * Return the credit clear days.
     * 
     * @return the number of credit clear days
     */
    public int getCreditClearDays()
    {
        return i_creditClearDays;
    }
    
    /** 
     * Return the flags After.
     * 
     * @return the flags After
     */
    public SdpFlagsData getFlagsAfter()
    {
        return i_flagsAfter;
    }
    
    /** 
     * Return the flags before.
     * 
     * @return the flags before
     */
    public SdpFlagsData getFlagsBefore()
    {
        return i_flagsBefore;
    }
    
    /** 
     * Return the Date/Time the current IVR barring will be removed from this customers account.
     * This date/time is either:
     * <ul>
     * <li> Date/time of the last successful refill. This will be in the past by the time the customer next 
     * access the IVR so the customer will not be barred.
     * <li> Date/time until which this customer is barred from using the IVR. The unbar date/time is
     * calculated by the date/time the barring was imposed plus a configurable amount of time.
     * </ul>
     * @return Date/time the ciustomer will be (or was) IVR unbarred.
     */
    public PpasDateTime getIvrUnbarDateTime()
    {
        return i_ivrUnbarDateTime;
    }
    
    /** 
     * Return th master Msisdn.
     * 
     * @return th master Msisdn
     */
    public Msisdn getMasterMsisdn()
    {
        return i_masterMsisdn;
    }
    
    /** 
     * Return the original Service class.
     * 
     * @return the original Service class
     */
    public ServiceClass getOriginalServiceClass()
    {
        return i_originalServiceClass;
    }

    /** 
     * Return the preferred currency.
     * 
     * @return the preferred currency
     */
    public PpasCurrency getPreferredCurrency()
    {
        return i_preferredCurrency;
    }
    
    /** 
     * Return the preferred language.
     * 
     * @return the preferred language
     */
    public String getPreferredLanguage()
    {
        return i_preferredLanguage;
    }
    
    /** 
     * Return the promotion plan.
     * 
     * @return the promotion plan
     */
    public String getPromotionPlan()
    {
        return i_promotionPlan;
    }
    
    /** 
     * Return the service class.
     * 
     * @return the service class
     */
    public ServiceClass getActiveServiceClass()
    {
        return i_serviceClass;
    }
    
    /** 
     * Return the service expiry date.
     * 
     * @return the service expiry date 
     * 
     */
    public PpasDate getServiceExpiryDate()
    {
        return i_serviceExpiryDate;
    }
    
    /** 
     * Return the service removal days.
     * 
     * @return the number of service removal days
     */
    public int getServiceRemovalDays()
    {
        return i_serviceRemovalDays;
    }
    
    /** 
     * Return the temporary Service class expiry date.
     *
     * @return the temporary Service class expiry date
     */
    public PpasDate getTempServiceClassExpDate()
    {
        return i_tempServiceClassExpDate;
    }

    /** 
     * Return the customer balance expiartion date time.
     * 
     * @return the customer balance expiartion date time
     */
    public PpasDateTime getCustBalanceExpDateTime()
    {
        return i_custBalanceExpDateTime;
    }
    
    /**
     * Returns the Account Group Id.
     *
     * @return Account Group Id.
     */
    public AccountGroupId getAccountGroupId()
    {
        return(i_accountGroupId);
    }

    /**
     * Returns the Service Offerings.
     *
     * @return Service Offerings.
     */
    public ServiceOfferings getServiceOffering()
    {
        return(i_serviceOfferings);
    }

    /** 
     * Returns the Community Id List.
     *
     * @return Community Id List.
     */
    public CommunitiesIdListData getCommunityIdList()
    {
        return( i_communityIdList );
    }

    /**
     * Returns the USSD End of call notification Id.
     * @return USSD End of call notification Id.
     */
    public EndOfCallNotificationId getEndOfCallNotificationId()
    {
        return i_endOfCallNotificationId;
    }

    /**
     * Returns the Pin Code.
     * @return Pin Code.
     */
    public PinCode getPinCode()
    {
        return i_pinCode;
    }

    /**
     * Get the correct removal date using the expiry date and the service removal grace period.
     * 
     * @return  the service Removal Date
     *
     */
    public PpasDate getServiceRemovalDate()
    {
        PpasDate l_serviceRemovalDate = new PpasDate((PpasDate)null);
        if (i_serviceExpiryDate != null && i_serviceExpiryDate.isSet())
        {
            l_serviceRemovalDate = (PpasDate) i_serviceExpiryDate.clone();
            l_serviceRemovalDate.add(PpasDate.C_FIELD_DATE, i_serviceRemovalDays);
        }
        return l_serviceRemovalDate;
    }
    
    /**
     * Get the correct service status using the SDP flags After.
     * 
     * @return the service status
     */
    public char getServiceStatus()
    {
    	char l_serviceStatus = (char) 0;
        
        if (i_flagsAfter != null)
        {
            if (i_flagsAfter.isServiceExpired())
            {
                l_serviceStatus = AccountData.C_SERVICE_EXPIRED_CHAR;
            }
            else if (i_flagsAfter.isServiceWarningSet())
            {
                l_serviceStatus = AccountData.C_SERVICE_LOW_ANNOUNCEMENT_CHAR;
            }
            else
            {
                l_serviceStatus = AccountData.C_SERVICE_ACTIVE_CHAR;
            }
        }
        return l_serviceStatus;
    }

    /**
     * Set the correct airtime status using the SDP flags After.
     * 
     * @return the airtime service status
     */
    public char getAirtimeServiceStatus()
    {
    	PpasDate l_creditClearDate = getCreditClearDate();
        char l_airtimeStatus = AccountData.C_AIRTIME_ACTIVE_CHAR;
        if(i_flagsAfter != null)
        {
        	if (i_flagsAfter.isAirtimeExpired())
        	{
        		
        	  PpasDate l_todayDate =
                  DatePatch.getDateToday();
        	   if (l_todayDate.after(l_creditClearDate))
               {
                  //'Z'
            	  l_airtimeStatus = AccountData.C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR;            
               }
        	   else 
               {
                  //'X'
            	  l_airtimeStatus = AccountData.C_AIRTIME_EXPIRED_CHAR;            
               }
        	}
            else if (i_flagsAfter.isAirtimeWarningSet())
            {
                //'B'
            	l_airtimeStatus = AccountData.C_AIRTIME_LOW_ANNOUNCEMENT_CHAR;
            }
        }
        return l_airtimeStatus;
    	
    }
    
    
   
    /**
     * Set the credit card clear date using airtime expiry date and credit card clear date.
     * 
     * @return the credit clear date
     **/
    public PpasDate getCreditClearDate()
    {
        PpasDate l_creditClearDate = new PpasDate((PpasDate)null);
        if (i_airtimeExpiryDate != null && i_airtimeExpiryDate.isSet())
        {
            l_creditClearDate = (PpasDate) i_airtimeExpiryDate.clone();
            l_creditClearDate.add(PpasDate.C_FIELD_DATE, i_creditClearDays);
        }
        return l_creditClearDate;
    }
    
    /** Get the dedicated accounts.
     * 
     * @return The dedicated accounts.
     **/
    public DedicatedAccountsDataSet getDedicatedAccounts()
    {
        return i_dedicatedAccounts;
    }
    
    /**
     * Set the correct value to the customer balance status using SDP flags.
     * 
     * @return the customer balance status
     */
    public char getCustBalanceStatus()
    {
        char l_custBalanceStatus = (char) 0;
        if (i_flagsAfter != null)
        {
            if (i_flagsAfter.isNegativeBalanceBarred())
            {
                l_custBalanceStatus = AccountData.C_CUST_BALANCE_BARRED;
            }
            else
            {
                l_custBalanceStatus = AccountData.C_CUST_BALANCE_ACTIVE;
            }
        }
        return l_custBalanceStatus;
    }
    
    /**
     * Implement the equals method to compare the both objects.
     * 
     * @param   p_account Account data set to compare.
     * @return  True if the account data sets are logically the same.
     */
    public boolean equals(Object p_account)
    {
        boolean l_result = false;
        
        if (p_account != null && p_account instanceof AccountQueryResultData)
        {
            l_result = equals((AccountQueryResultData)p_account);
        }
        
        return l_result;
    }

    /**
     * Implement the equals method to compare the both objects.
     * 
     * @param   p_account Account data set to compare.
     * @return  True if the account data sets are logically the same.
     */
    public boolean equals(AccountQueryResultData p_account)
    {
        if (this.i_activationDate != null)
        {
            if (p_account.getActivationDate() == null ||
                !this.i_activationDate.toString().equals(p_account.getActivationDate().toString()))
            {
                return false;
            }
        }
        else if (p_account.getActivationDate() != null)
        {
            return false;
        }

        if (this.i_airtimeExpiryDate != null)
        {
            if (p_account.getAirtimeExpiryDate() == null ||
                !this.i_airtimeExpiryDate.toString().equals(p_account.getAirtimeExpiryDate().toString()))
            {
                return false;
            }
        }
        else if (p_account.getAirtimeExpiryDate() != null)
        {
            return false;
        }

        if (this.i_creditClearDays != p_account.getCreditClearDays())
        {
            return false;
        }

        if (this.i_custBalanceExpDateTime != null)
        {
            if (p_account.getCustBalanceExpDateTime() == null || !this.i_custBalanceExpDateTime.toString()
                    .equals(p_account.getCustBalanceExpDateTime().toString()))
            {
                return false;
            }
        }
        else if (p_account.getCustBalanceExpDateTime() != null)
        {
            return false;
        }

        if (this.i_flagsAfter != null)
        {
            if (p_account.getFlagsAfter() == null || !this.i_flagsAfter.equals(p_account.getFlagsAfter()))
            {
                return false;
            }
        }
        else if (p_account.getFlagsAfter() != null)
        {
            return false;
        }
        
        if (this.i_flagsBefore != null)
        {
            if (p_account.getFlagsBefore() == null || !this.i_flagsBefore.equals(p_account.getFlagsBefore()))
            {
                return false;
            }
        }
        else if (p_account.getFlagsBefore() != null)
        {
            return false;
        }

        if (this.i_ivrUnbarDateTime != null)
        {
            if (p_account.getIvrUnbarDateTime() == null ||
                !this.i_ivrUnbarDateTime.toString().equals(p_account.getIvrUnbarDateTime().toString()))
            {
                return false;
            }
        }
        else if (p_account.getIvrUnbarDateTime() != null)
        {
            return false;
        }
        
        if (this.i_preferredLanguage != p_account.getPreferredLanguage())
        {
            return false;
        }
         
        if (this.i_preferredCurrency != null)
        {
            if (!this.i_preferredCurrency.equals(p_account.getPreferredCurrency()))
            {
                return false;
            }
        }
        else if (p_account.getPreferredCurrency() != null)
        {
            return false;
        }

        if (this.i_promotionPlan != null)
        {
            if (!this.i_promotionPlan.equals(p_account.getPromotionPlan()))
            {
                return false;
            }
        }
        else if (p_account.getPromotionPlan() != null)
        {
            return false;
        }

        if (i_masterMsisdn != null)
        {
            if (!this.i_masterMsisdn.equals(p_account.getMasterMsisdn()))
            {
                return false;
            }
        }
        else if (p_account.getMasterMsisdn() != null)
        {
            return false;
        }

        if (this.i_originalServiceClass != null)
        {
            if (!this.i_originalServiceClass.equals(p_account.getOriginalServiceClass()))
            {
                return false;
            }
        }
        else if (p_account.getOriginalServiceClass() != null)
        {
            return false;
        }

        if (this.i_serviceClass != null)
        {
            if (!this.i_serviceClass.equals(p_account.getActiveServiceClass()))
            {
                return false;
            }
        }
        else if (p_account.getActiveServiceClass() != null)
        {
            return false;
        }

        if (this.i_serviceExpiryDate != null)
        {
            if (p_account.getServiceExpiryDate() != null)
            {
                if (!this.i_serviceExpiryDate.toString().equals(p_account.getServiceExpiryDate().toString()))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else if (p_account.getServiceExpiryDate() != null)
        {
            return false;
        }

        if (this.i_serviceRemovalDays != p_account.getServiceRemovalDays())
        {
            return false;
        }

        if (this.i_tempServiceClassExpDate != null)
        {
            if (p_account.getTempServiceClassExpDate() != null)
            {
                if (!this.i_tempServiceClassExpDate.toString()
                    .equals(p_account.getTempServiceClassExpDate().toString()))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else if (p_account.getTempServiceClassExpDate() != null)
        {
            return false;
        }

        if (this.i_accountGroupId != null)
        {
            if (p_account.getAccountGroupId() == null ||
                !this.i_accountGroupId.equals(p_account.getAccountGroupId()))
            {
                return false;
            }
        }
        else if (p_account.getAccountGroupId() != null)
        {
            return false;
        }

        if (this.i_serviceOfferings != null)
        {
            if (p_account.getServiceOffering() == null ||
                !this.i_serviceOfferings.equals(p_account.getServiceOffering()))
            {
                return false;
            }
        }
        else if (p_account.getServiceOffering() != null)
        {
            return false;
        }

        if (this.i_pinCode != null)
        {
            if (p_account.getPinCode() == null ||
                !this.i_pinCode.equals(p_account.getPinCode()))
            {
                return false;
            }
        }
        else if (p_account.getPinCode() != null)
        {
            return false;
        }
        
        if (this.i_homeRegionId != null)
        {
            if (p_account.getHomeRegionId() == null ||
                !this.i_homeRegionId.equals(p_account.getHomeRegionId()))
            {
                return false;
            }
        }
        else if (p_account.getHomeRegionId() != null)
        {
            return false;
        }
        
        return true;
    }
    /**
     * Indicates whether the account it is the ivr first call or not.
     * @return True if it is.
     */
    public boolean isIvrFirstCall()
    {
        return i_ivrFirstCall;
    }
    /**
     * Indicates whether the account is temporarily blocked.
     * @return True if account is temporarily blocked, otherwise false.
     */
    public boolean isTempBlocked()
    {
        return i_tempBlockedFlag;
    }
    
    /**
     * Get the balance of the account.
     * @return have a guess...
     */
    public Money getAccountBalance()
    {
        return i_accountBalance;
    }
    
    /** 
     * Get the Home Region of the account.
     * @return The Home Region of the account.
     */
    public HomeRegionId getHomeRegionId()
    {
        return i_homeRegionId;
    }
    
    /**
     * Set the Home Region Id of the account
     * @param p_homeRegionId The Home Region Id to set for the account.
     */
    public void setHomeRegionId(HomeRegionId p_homeRegionId)
    {
        i_homeRegionId = p_homeRegionId;
    }
}
