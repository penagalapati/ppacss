////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaMstrData.Java
//      DATE            :       12-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A record of market information
//                              (corresponding to a single row of the
//                              srva_mstr table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of market information (i.e.
 * a single row of the srva_mstr table).
 */
public class SrvaMstrData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaMstrData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The market.
     */
    private  Market            i_market = null;

    /** The number of days after disconnection that an MSISDN can be re-used. */
    private  int               i_msisdnQuarantinePeriod = 0;

    /** Date this market record was created/last accessed in the database. */
    private  PpasDateTime      i_genYmdhms = null;

    /** Operator Id who created/last accessed this market record in the 
     *  database.
     */
    private  String            i_opid = null;
    
    /** Flag indicating if this market is deleted. */
    private  char              i_delFlag = ' ';

    /** Formatting object for this market. */
    private MsisdnFormat       i_msisdnFormat = null;
    
    /** Formatting object for this market. */
    private HomeRegionId       i_defaultHomeRegion = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates market data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this data is associated.
     * @param p_msisdnFormat <code>MsisdnFormat</code> object to format an MSISDN in the manner relevant to
     *                       this market.
     * @param p_msisdnQuarantinePeriod Number of days after disconnection that a mobile number can be re-used.
     * @param p_genYmdhms The date/time this data was last changed.
     * @param p_opid    The operator that last changed this data.
     * @param p_delFlag Flag indicating whether this data is marked as deleted.
     * @param p_defaultHomeRegion Default Home Region
     */
    public SrvaMstrData(
        PpasRequest            p_request,
        Market                 p_market,
        MsisdnFormat           p_msisdnFormat,
        int                    p_msisdnQuarantinePeriod,
        PpasDateTime           p_genYmdhms,
        String                 p_opid,
        char                   p_delFlag,
        HomeRegionId           p_defaultHomeRegion)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market                 = p_market;
        i_msisdnFormat           = p_msisdnFormat;
        i_msisdnQuarantinePeriod = p_msisdnQuarantinePeriod;
        i_genYmdhms              = p_genYmdhms;
        i_opid                   = p_opid;
        i_delFlag                = p_delFlag;
        i_defaultHomeRegion      = p_defaultHomeRegion;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the market.
     * 
     * @return The market to which this data is associated.
     */
    public Market getMarket ()
    {
        return (i_market);
    }

    /** Get the telephone format associated with this market.
     * 
     * @return The display format for MSISDN's for this market.
     */
    public String getTeleFormat()
    {
        return i_msisdnFormat.toPattern();
    }

    /** Get the telephone formatting object associated with this market.
     * 
     * @return An MSISDN formatter object for this market.
     */
    public MsisdnFormat getMsisdnFormat()
    {
        return i_msisdnFormat;
    }

    /**
     *  Get the default Msisdn Quarantine Period. 
     * 
     * @return The number of days from disconenction before a mobile number can be re-used.
     */
    public int getMsisdnQuarantinePeriod()
    {
        return (i_msisdnQuarantinePeriod);
    }

    /** Get date this market record was created/last accessed in the database.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getGenYmdhms()
    {
        return (i_genYmdhms);
    }

    /** Get operator Id who created/lst accessed this market record in the database.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid()
    {
        return (i_opid);
    }

    /** Returns true if this market has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if (i_delFlag == '*')
        {
            l_deleted = true;
        }

        return (l_deleted);
    }

    /**
     * Gets the Default Home Region.
     * @return HomeRegionId Id of the default Home Region.
     */
    public HomeRegionId getDefaultHomeRegion()
    {
        return i_defaultHomeRegion;
    }
} // End of public class SrvaMstrData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
