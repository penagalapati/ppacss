/////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       ArchiveTableData.java
//DATE            :       29 September 2004
//AUTHOR          :       R.Grimshaw
//REFERENCE       :       PRD_ASCS00_DEV_SS_082
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//|            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author 7451F
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ArchiveTableData
{
    private String i_tableName;
    
    private List i_columns;
    
    private Iterator i_columnsIt = null;
    
    public ArchiveTableData(String p_tableName)
    {
        i_tableName = p_tableName;
        
        i_columns = new ArrayList();
    }
    
    
    public void addColumn(String p_columnName, Object p_columnValue)
    { 
        i_columns.add(new ArchiveColumnData(p_columnName, p_columnValue));   
    }
    
    public String getTableName()
    {
        return i_tableName;
    }
    
    public ArchiveColumnData getNextColumn()
    {
        
        if (i_columnsIt == null)
        {
            i_columnsIt = i_columns.iterator();
        }
        
        if (i_columnsIt.hasNext())
        {
            return (ArchiveColumnData) i_columnsIt.next();
        }
        else
        {
            return null;
        }

    }
    
    /** Display the ArchiveTableData information as a <code>String</code>.
     * 
     * @return String representation of the account.
     */
    public String toString()   
    {
        StringBuffer l_sB = new StringBuffer();
        l_sB.append("ArchiveTableData=[");
        l_sB.append("Tolumn name: " + i_tableName);
        l_sB.append(",Columns: " + i_columns);
        l_sB.append("]");
        return l_sB.toString();
    }
    
}
