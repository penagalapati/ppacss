////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscCodeCache.Java
//      DATE            :       23-August-2001
//      AUTHOR          :       Mike Hickman
//
//      DESCRIPTION     :       Cache wrapper for MiscCodeDataSet object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import java.util.HashMap;
import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MiscCodeSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** Cache wrapper for MiscCodeDataSet object. */
public class MiscCodeCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscCodeCache";

    /** Define the code to be used to select payment type misc code data. */
    public static final String C_PAYMENT_TYPE_MISC_TYPE          = "PYM";

    /** Define the code to be used to select service class misc code data. */
    public static final String C_SERVICE_CLASS_MISC_TYPE         = "CLS";

    /** Define the code to be used to select contact type misc code data. */
    public static final String C_CONTACT_TYPE_MISC_TYPE          = "CBR";

    /** Define the code to be used to select address type misc code data. */
    public static final String C_ADDRESS_TYPE_MISC_TYPE          = "ADR";

    /** Define the code to be used to select the charged event counter type misc code data. */
    public static final String C_CHARGED_EVENT_COUNTER_MISC_TYPE = "CTR";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** The set of miscellaneous codes. */
    private MiscCodeDataSet i_miscCodeDataSet                       = null;

    /** Service used to select the misc code data from the database. */
    private MiscCodeSqlService i_miscCodeSqlService                 = null;

    /** Map of miscellaneous codes to available data sets grouped by market. */
    private HashMap i_availableByMarket = new HashMap();

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new misc code cache object.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public MiscCodeCache(PpasRequest    p_request,
                         Logger         p_logger,
                         PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_miscCodeSqlService = new MiscCodeSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /** Reloads the misc codes into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest     p_request,
                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_reload );
        }

        i_miscCodeDataSet = i_miscCodeSqlService.readMiscCodes(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11010, this,
                "Leaving " + C_METHOD_reload );
        }
    }

    /** Returns a set of misc codes.
     *
     * @return A set of miscellaneous codes.
     */
    public MiscCodeDataSet getMiscCodes()
    {
        return(i_miscCodeDataSet);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAvailableMiscCodes = "getAvailableMiscCodes";
    /** Get all available misc code data within a given market for a given misc type.
     *  Records marked as deleted are excluded.
     *
     *  @param p_market The market that the records to be returned are based upon.
     *  @param p_miscType The type that the records to be returned are based
     *                    upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          misc code records of a given type for a supplied market.
     */
    public MiscCodeDataSet getAvailableMiscCodes(Market      p_market,
                                                 String      p_miscType)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10000, this,
                            "Entering " + C_METHOD_getAvailableMiscCodes + " for Mkt: " + p_market +
                            " Type: " + p_miscType);
        }
        
        MiscCodeDataSet l_returnDataSet   = null;

        if (i_miscCodeDataSet != null)
        {
            // Check to see if we already have a cached set of data of the
            // required type for the given market.
            HashMap l_cache = (HashMap)i_availableByMarket.get(p_miscType);
            
            if (l_cache == null)
            {
                l_cache = new HashMap();
                i_availableByMarket.put(p_miscType, l_cache);
            }

            l_returnDataSet = (MiscCodeDataSet)l_cache.get(p_market);

            if (l_returnDataSet == null)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10000, this,
                                    C_METHOD_getAvailableMiscCodes + ": Add to local cache for Mkt: " +
                                    p_market + " Type: " + p_miscType);
                }
                
                l_returnDataSet = i_miscCodeDataSet.getAvailableMiscCodeData(p_market, p_miscType);
                l_cache.put(p_market, l_returnDataSet);
            }
        }

        return l_returnDataSet;
    }

    /** Get all available misc code data records that define payment types within a given market.
     *
     *  @param p_market The market that the records to be returned are based upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          payment types for the supplied market.
     */
    public MiscCodeDataSet getAvailablePaymentTypes(Market p_market)
    {
       return getAvailableMiscCodes(p_market, C_PAYMENT_TYPE_MISC_TYPE);
    }

    /** Get all available misc code data records that define service classes within a set of markets.
     *  Records marked as deleted are excluded.
     *
     *  @param p_markets The markets that the records to be returned are based upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          service classes for the supplied markets.
     */
    public MiscCodeDataSet getAvailableServiceClasses (Vector p_markets)
    {
        int             l_lenMktVec = 0;
        MiscCodeDataSet l_retMiscCodeDataSet = null;
        MiscCodeDataSet l_mktMiscCodeDataSet = null;
        Market          l_market = null;
        Vector          l_mktMiscCodesDataVec = null;
        int             l_lenMktMiscCodesDataVec = 0;
        MiscCodeData    l_miscCodesData = null;
       
        l_retMiscCodeDataSet = new MiscCodeDataSet();
       
        if (p_markets != null)
        {
            l_lenMktVec = p_markets.size();
            for (int l_i = 0; l_i < l_lenMktVec; l_i++)
            {
                l_market = (Market)p_markets.elementAt(l_i);
                l_mktMiscCodeDataSet = getAvailableMiscCodes(l_market, C_SERVICE_CLASS_MISC_TYPE);
                l_mktMiscCodesDataVec = l_mktMiscCodeDataSet.getDataV();
                l_lenMktMiscCodesDataVec = l_mktMiscCodesDataVec.size();
                for (int l_j = 0; l_j < l_lenMktMiscCodesDataVec; l_j++)
                {
                    l_miscCodesData = (MiscCodeData)l_mktMiscCodesDataVec.elementAt(l_j);
                    l_retMiscCodeDataSet.addMiscCode(l_miscCodesData);
                }                            
            }        
        } 
        
        return l_retMiscCodeDataSet;    
    }

    /** Get all available misc code data records that define service classes within a given market.
     *  Records marked as deleted are excluded.
     *
     *  @param p_market The market that the records to be returned are based upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          service classes for the supplied market.
     */
    public MiscCodeDataSet getAvailableServiceClasses (Market p_market)
    {
       return getAvailableMiscCodes(p_market, C_SERVICE_CLASS_MISC_TYPE);
    }

    /** Get all available misc code data records that define contact types within a given market.
     *  Records marked as deleted are excluded.
     *
     *  @param p_market The market that the records to be returned are based upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          contact types for the supplied market.
     */
    public MiscCodeDataSet getAvailableContactTypes(Market      p_market)
    {
       return getAvailableMiscCodes(p_market, C_CONTACT_TYPE_MISC_TYPE);
    }

    /** Get all available misc code data records that define address types within a given market.
     *  Records marked as deleted are excluded.
     *
     *  @param p_market The market that the records to be returned are based upon.
     *  @return Data set containing the available (i.e. not marked as deleted)
     *          address types for the supplied market.
     */
    public MiscCodeDataSet getAvailableAddressTypes(Market      p_market)
    {
       return getAvailableMiscCodes(p_market, C_ADDRESS_TYPE_MISC_TYPE);
    }
    
    /**
     * Get the charged event counter descriptions from the srva_misc_codes table where the misc_table column
     * has value 'CTR'.
     * 
     * @param p_market   The market that the method returns data for.
     * @return A <code>MiscCodeDataSet</code> containing the charged event counter descriptions.
     */
    public MiscCodeDataSet getAvailableChargedEventCounterTypes(Market      p_market)
   {
      return getAvailableMiscCodes(p_market, C_CHARGED_EVENT_COUNTER_MISC_TYPE);
   }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_miscCodeDataSet);
    }
}

