////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountData.Java
//      DATE            :       16-Jan-2001
//      AUTHOR          :       Sally Wells/ Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       AccountData Class storing account data 
//                              associated with a single ASCS account.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 16/05/05 | M.Brister  | Added new pin code attribute and| PpacLon1519#6417
//          |            | associated getter and setter.   | prd_ascs00_gen_ca38
//          |            |                                 | BHA-007
//----------+------------+---------------------------------+--------------------
// 27/04/06 | S Pinton   | Add Disconenct gen date for     | PpacLon#2201/8578
//          |            | PPSqlEnquiry                    |
//----------+------------+---------------------------------+--------------------
// 19/06/06 | M Erskine  | Add the connection type param   | PpacLon#2425/9127
//----------+------------+---------------------------------+--------------------
// 17/08/06 | S Pinton   | Add disc reason description &   | PpacLon#2522/9712
//          |            | last service class change date  |
//----------+------------+---------------------------------+--------------------
//17-Aug-06 | K Goswami  | Changed code for POSI Initial   | PpacLon#2532/9698
//          |            | Credit                          |
//----------+------------+---------------------------------+-----------------
// 16/05/07 |Steve James | Updated isDisconnected() and    | PpacLon#3072/11370 
//          |            | isMasterDisconnected() to       |
//          |            | include RecconectInProgress     | CA124
//          |            | status 'R'                      |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;


import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * This data class contains the attributes defining the full account details of a subscriber.
 */
public class AccountData extends BasicAccountData
{
    //-------------------------------------------------------------------------
    //  Private class constants
    //-------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccountData";

    //-------------------------------------------------------------------------
    //  Public class constants
    //-------------------------------------------------------------------------

    // 
    // Service status codes ...
    //

    /** Status defining a customer with no service. */
    public static final String C_SERVICE_NONE = "SERVICE_NONE";

    /** Status defining a customer with active service. */
    public static final String C_SERVICE_ACTIVE = "SERVICE_ACTIVE";

    /** Text defining the service status of an account within the low service  announcement period. */
    public static final String C_SERVICE_LOW_ANNOUNCEMENT = "SERVICE_LOW_ANNOUNCEMENT";


    /** Text defining the service status of an account within the bar service announcement period. */
    public static final String C_SERVICE_BAR_ANNOUNCEMENT = "SERVICE_BAR_ANNOUNCEMENT";

    /** Text defining the service status of an account whose service has expired. */
    public static final String C_SERVICE_EXPIRED = "SERVICE_EXPIRED";

    /** Text code to be used if an unrecognised service status is found. */
    public static final String C_SERVICE_STATUS_UNKNOWN = "SERVICE_STATUS_UNKNOWN";

    // 
    // Service status strings ...
    //
    /** String defining a customer with no service. */
    public static final String C_SERVICE_NONE_STR = "No Service";

    /** String defining a customer with active service. */
    public static final String C_SERVICE_ACTIVE_STR = "Active";

    /** String defining the service status of an account within the low service announcement period. */
    public static final String C_SERVICE_LOW_ANNOUNCEMENT_STR = "Low Service Announcement";

    /** String defining the service status of an account within the bar service announcement period. */
    public static final String C_SERVICE_BAR_ANNOUNCEMENT_STR = "Service Barring Announcement";

    /** String defining the service status of an account whose service has expired. */
    public static final String C_SERVICE_EXPIRED_STR = "Expired";

    /** String code to be used if an unrecognised service status is found. */
    public static final String C_SERVICE_STATUS_UNKNOWN_STR = "Unknown";

    // Define service status values ...
    /** Status character defining a customer with active service. */
    public static final char C_SERVICE_ACTIVE_CHAR = ' ';

    /** Character code defining the service status of an account within the low service announcement period.
     */
    public static final char C_SERVICE_LOW_ANNOUNCEMENT_CHAR = 'L';

    /** Character code defining the service status of an account within the bar service announcement period.
     */
    public static final char C_SERVICE_BAR_ANNOUNCEMENT_CHAR = 'B';

    /** Character code defining the service status of an account whose service has expired. */
    public static final char C_SERVICE_EXPIRED_CHAR = 'I';

    // 
    // Airtime status codes ...
    //
    /** Status defining a customer with no airtime. */
    public static final String C_AIRTIME_NONE = "AIRTIME_NONE";

    /** Status defining a customer with active airtime. */
    public static final String C_AIRTIME_ACTIVE = "AIRTIME_ACTIVE";

    /** Text defining the airtime status of an account within the low airtime announcement period. */
    public static final String C_AIRTIME_LOW_ANNOUNCEMENT = "AIRTIME_LOW_ANNOUNCEMENT";

    /** Text defining the airtime status of an account whose airtime has expired. */
    public static final String C_AIRTIME_EXPIRED = "AIRTIME_EXPIRED";

    /** Text defining the airtime status of an account whose airtime has
     *  expired and whose credit has been cleared.
     */
    public static final String C_AIRTIME_EXPIRED_CREDIT_CLEARED = "AIRTIME_EXPIRED_CREDIT_CLEARED";


    /** Text code to be used if an unrecognised airtime status is found. */
    public static final String C_AIRTIME_STATUS_UNKNOWN = "AIRTIME_STATUS_UNKNOWN";

    // 
    // Airtime status strings ...
    //
    /** String defining a customer with no airtime. */
    public static final String C_AIRTIME_NONE_STR = "No Airtime";

    /** String defining a customer with active airtime. */
    public static final String C_AIRTIME_ACTIVE_STR = "Active";

    /** String defining the airtime status of an account within the low airtime announcement period. */
    public static final String C_AIRTIME_LOW_ANNOUNCEMENT_STR = "Low Airtime Announcement";

    /** String defining the airtime status of an account whose airtime has expired. */
    public static final String C_AIRTIME_EXPIRED_STR = "Expired";

    /** String defining the airtime status of an account whose airtime has
     *  expired and whose credit has been cleared.
     */
    public static final String C_AIRTIME_EXPIRED_CREDIT_CLEARED_STR = "Credit Cleared";

    /** String to be used if an unrecognised airtime status is found. */
    public static final String C_AIRTIME_STATUS_UNKNOWN_STR = "Unknown";

    /** Character code defining a customer with active airtime. */
    public static final char C_AIRTIME_ACTIVE_CHAR = ' ';

    /** Character code defining the airtime status of an account within the low airtime announcement period.
     */
    public static final char C_AIRTIME_LOW_ANNOUNCEMENT_CHAR = 'B';

    /** Character code defining the airtime status of an account whose airtime has expired. */
    public static final char C_AIRTIME_EXPIRED_CHAR = 'X';

    /** Character code defining the airtime status of an account whose airtime
     *  has expired and whose credit has been cleared.
     */
    public static final char C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR = 'Z';

    //
    // Network type codes
    //
    /** Text defining a GSM network type. */
    public static final String C_NETWORK_TYPE_GSM = "NETWORK_TYPE_GSM";

    /** Text defining a non-GSM network type. */
    public static final String C_NETWORK_TYPE_NON_GSM = "NETWORK_TYPE_NON_GSM";

    /** Text defining an unknown network type. */
    public static final String C_NETWORK_TYPE_UNKNOWN = "NETWORK_TYPE_UNKNOWN";

    //
    // Network type Strings
    //
    /** String defining a GSM network type. */
    public static final String C_NETWORK_TYPE_GSM_STR = "GSM";

    /** String defining a non-GSM network type. */
    public static final String C_NETWORK_TYPE_NON_GSM_STR = "Non-GMS";

    /** String defining an unknown network type. */
    public static final String C_NETWORK_TYPE_UNKNOWN_STR = "Unknown";

    //
    // Define IVR barred status'
    //
    /** Status code defining the account as NOT IVR barred. */
    public static final String C_IVR_STATUS_NOT_BARRED = "IVR_STATUS_NOT_BARRED";

    /** Status code defining the account as IVR barred. */
    public static final String C_IVR_STATUS_BARRED = "IVR_STATUS_BARRED";

    /** Status code to be used when the IVR bar status cannot be determined. */
    public static final String C_IVR_STATUS_BARRED_UNKNOWN = "IVR_STATUS_BARRED_UNKNOWN";

    // Define service status values ...
    /** Status character defining a customer without balance barring. */
    public static final char C_CUST_BALANCE_ACTIVE = ' ';

    /** Status character defining a customer balance barring. */
    public static final char C_CUST_BALANCE_BARRED = 'I';

    //
    // Define Balance Barring Strings
    //
    /** String defining unbarred balance status. */
    public static final String C_BALANCE_BAR_NONE_STR = "Not Barred";

    /** String defining balance barring due status. */
    public static final String C_BALANCE_BAR_DUE_STR = "Barring Due";

    /** String defining balance barred status. */
    public static final String C_BALANCE_BAR_SINCE_STR = "Barred Since";

    /** String defining unrecognised  balance barring status. */
    public static final String C_BALANCE_BAR_UNKNOWN_STR = "Unknown";
    
    // temp blocking constants
    /** String defining temp blocking set. */
    public static final String C_TEMP_BLOCKED_SET = "Blocked";
    
    /** String defining temp blocking not set. */
    public static final String C_TEMP_BLOCKED_CLEAR = "Not Blocked";

    //-------------------------------------------------------------------------
    //  Private instance data
    //-------------------------------------------------------------------------

    /** The date on which the customer first received service. */
    private PpasDate     i_beginServiceDate            = new PpasDate("");

    /** The date that this customer's service would/did end and disconnected on ASCS. */
    private PpasDate     i_endServiceDate              = new PpasDate("");

    /** The date that the customer's service will/did expire. */
    private PpasDate     i_serviceExpiryDate           = new PpasDate("");

    /**  The date that the subscription will be removed from the network. */
    private PpasDate     i_serviceRemovalDate          = new PpasDate("");

    /**
     * The current active service class. If the subscriber is on a temporary service
     * class then this value will hold the temp service class, otherwise it's the
     * (original) service class.
      */
    private ServiceClass       i_activeServiceClass                = null;

    /** The language used for announcements on the IVR. */
    private String       i_preferredLanguage           = null;

    /** Currency of the tariffs associated with the service class of this customer. */
    private PpasCurrency i_preferredCurrency           = null;

    /** Promotion plan identifier for the plan that this customer is on. */
    private String       i_promotionPlan               = null;

    /** Date/time until which this customer is/was barred from accessing the IVR. */
    private PpasDateTime i_ivrUnbarDateTime           = new PpasDateTime("");

    /** Status code string defining the IVR barred status. */
    private String       i_ivrBarredStatusCode         = null;

    /** Service expiry status for this customer.
     * <p>
     * The Service Expiry Status is set to:
     * <p>
     * <ul>
     * <li>  " " = active
     * <li>  L   = Low service ann. provisioned
     * <li>  B   = Bar ann. provisioned 
     * <li>  I   = Incoming calls barred
     * </ul>
     */
    private char            i_serviceStatus             = (char)0;

    /** Airtime expiry status of this customer.
     * <p>
     * The Airtime Expiry Status is set to:
     * <p>
     * <ul>
     * <li>  " " = active
     * <li>  X   = Outgoing calls barred.
     * <li>  Z   = Balance cleared.
     * </ul>
      */
    private char            i_airtimeStatus             = (char)0;

    /** Network type on which this customer has service (G=GSM, N=Non GSM). */
    private char            i_custNetworkType           = (char)0;

    /** Credit clearance date for this customer. */
    private PpasDate        i_creditClearDate           = new PpasDate("");

    /** Master Msisdn of this account. */
    private Msisdn          i_masterMsisdn              = null;

    /** Reason for disconnection of the customer. */
    private String          i_disconnectReason          = null;

    /** Reason for disconnection of the customer description */
    private String          i_disconnectReasonDesc      = null;

    /** Company name for this customer. */
    private String          i_companyName               = null;

    /** Balance status for this customer. */
    private char            i_custBalanceStatus         = (char)0;

    /** Balance expiry date for this customer. */
    private PpasDateTime    i_custBalanceExpDateTime    = new PpasDateTime("");

    /**  Get the date that this customer's airtime will/did expire. */
    private PpasDate        i_airtimeExpiryDate         = new PpasDate("");

    /** Status of Master Account. */  
    private char            i_masterAccountStatus       = (char)0;

    /** Original service class status. */
    private ServiceClass          i_originalServiceClass      = null;
    
    /** Temporary service class expiry date time. */
    private PpasDate        i_tempServiceClassExpDate   = new PpasDate("");
    
    /** Temp blocking status. */
    private boolean         i_tempBlocked               = false;
    
    /** Account Group ID for this customer. */
    private AccountGroupId          i_accountGroupID               = null;
    
    /** Service Offering for this customer. */
    private ServiceOfferings          i_serviceOffering              = null; 
    
    /** Initial Credit for this customer. */
    private Money                     i_initialCredit                = null; 
    
    /** Home Region Id for this account. */
    private HomeRegionId              i_homeRegionId = null;
    
    /** USSD End of Call Notification selection structure ID. */
    private EndOfCallNotificationId   i_ussdEocnId      = null;
    
    /** List of Communities Id.*/
    private CommunitiesIdListData     i_communitiesIdList = null; 
    
    /** Pin code for IVR access via landline. */
    private PinCode i_pinCode;

    /** Disconnection gen date. */
    private PpasDate i_disconnectGenDate;
    
    /** Connection Type of the account: 'N' for new connection and 'R' for reconnection. */
    private char i_connectionType;

    /** Date/time of last service class change. */
    private PpasDateTime i_lastServiceClassChangeDateTime = null;

    //-------------------------------------------------------------------------
    //  Constructors
    //-------------------------------------------------------------------------

    /**
     * Constructor to initialise instance attributes.
     * 
     * @param p_request The request to process.
     * @param p_custId  Internal identifier of mobile user.
     * @param p_btCustId Internal identifier of account holder.
     * @param p_msisdn  Mobile phone number.
     * @param p_custStatus Status of this mobile phone.
     * @param p_masterAccountStatus Status of the account.
     * @param p_sdpId SDP that holds this account.
     * @param p_beginServiceDate Date of start of service.
     * @param p_endServiceDate Date of end of service.
     * @param p_airtimeExpiryDate Date of airtime expiry.
     * @param p_serviceExpiryDate Date of service expiry.
     * @param p_serviceRemovalDate Date of service removal.
     * @param p_activeServiceClass Current active service class (this could be the temp service class).
     * @param p_preferredLanguage Preferred language for this user.
     * @param p_preferredCurrency Preferred currency for this account.
     * @param p_promotionPlan Promotion plan associated with this account. 
     * @param p_lastRefillDateTime Date/time the account was last refilled.
     * @param p_ivrUnbarDateTime Date/time of end of IVR barring.
     * @param p_ivrBarredStatusCode Flag indicating whether the account is barred from using the IVR.
     * @param p_serviceStatus Service status of the account.
     * @param p_airtimeStatus Airtime status of the account.
     * @param p_market Market with which the account is associated.
     * @param p_custNetworkType Type of network for this account.
     * @param p_creditClearDate Date the account will be credit cleared.
     * @param p_disconnectReason Reason the user was disconnected.
     * @param p_firstName First name of the mobile user.
     * @param p_surname Surname of the mobile user.
     * @param p_companyName Company name of the mobile user.
     * @param p_custBalanceStatus Balance status of the account.
     * @param p_agent Agent identifier.
     * @param p_subAgent Sub-agent identifier.
     * @param p_activationDate Date the account was activated.
     * @param p_origServiceClass the original service class (same as active service class if no temp class)
     * @param p_tempServiceClassExpDate the temp service class expiry date
     * @param p_masterMsisdn the master msisdn
     * @param p_accountGroupID  The account group ID
     * @param p_serviceOffering The service offering
     * @param p_communitiesIdList   Communities Id List.
     * @param p_isPromoSynch  Flag that indicate if the promotion plan is Synchronized or not.
     * @param p_pinCode Pin code for IVR access via landline.
     * @param p_homeRegionId Home Region Id of the account.
     * @param p_disconnectGenDate Disconnection gen date.
     * @param p_connectionType The connection type - 'N' for new connection or 'R' for reconnection.
     */
    public AccountData(PpasRequest                  p_request,
                       String                       p_custId,
                       String                       p_btCustId,
                       Msisdn                       p_msisdn,
                       char                         p_custStatus,
                       char                         p_masterAccountStatus, 
                       String                       p_sdpId,
                       PpasDate                     p_beginServiceDate,
                       PpasDate                     p_endServiceDate,
                       PpasDate                     p_airtimeExpiryDate,
                       PpasDate                     p_serviceExpiryDate,
                       PpasDate                     p_serviceRemovalDate,
                       ServiceClass                 p_activeServiceClass,
                       String                       p_preferredLanguage,
                       PpasCurrency                 p_preferredCurrency,
                       String                       p_promotionPlan,
                       PpasDateTime                 p_lastRefillDateTime,  
                       PpasDateTime                 p_ivrUnbarDateTime,  
                       String                       p_ivrBarredStatusCode,
                       char                         p_serviceStatus,
                       char                         p_airtimeStatus,
                       Market                       p_market,
                       char                         p_custNetworkType,
                       PpasDate                     p_creditClearDate,
                       String                       p_disconnectReason,
                       String                       p_firstName,
                       String                       p_surname,
                       String                       p_companyName,
                       char                         p_custBalanceStatus,
                       String                       p_agent,
                       String                       p_subAgent,
                       PpasDate                     p_activationDate,
                       ServiceClass                 p_origServiceClass,
                       PpasDate                     p_tempServiceClassExpDate,
                       Msisdn                       p_masterMsisdn,
                       AccountGroupId               p_accountGroupID,
                       ServiceOfferings             p_serviceOffering,
                       CommunitiesIdListData        p_communitiesIdList,
                       boolean                      p_isPromoSynch,
                       PinCode                      p_pinCode,
                       HomeRegionId                 p_homeRegionId,
                       PpasDate                     p_disconnectGenDate,
                       char                         p_connectionType)
    {
        super(p_request,
              p_custId,
              p_btCustId,
              p_msisdn,
              p_surname,
              p_firstName,
              p_custStatus,
              p_sdpId,
              p_market,
              p_agent,
              p_subAgent,
              p_activationDate,
              p_lastRefillDateTime,
              p_isPromoSynch);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructing AccountData(...)");

            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS,
                PpasDebug.C_ST_BUSINESS_DATA, 
                p_request, C_CLASS_NAME, 10020, this,
                "Begin service date=" + p_beginServiceDate +
                "; Cust Id         =" + p_custId +
                "; End service date=" + p_endServiceDate +
                "; Airtime expiry=" + p_airtimeExpiryDate +
                "; Service expiry=" + p_serviceExpiryDate + 
                "; Service removal=" + p_serviceRemovalDate +
                "; Service class=" + p_activeServiceClass +
                "; Pref. language=" + p_preferredLanguage +
                "; Pref. currency=" + p_preferredCurrency +
                "; Promo. plan=" + p_promotionPlan +
                "; IVR unbar date/time=" + p_ivrUnbarDateTime +
                "; IVR barred status code=" + p_ivrBarredStatusCode + 
                "; Service status=" + p_serviceStatus +
                "; Airtime status=" + p_airtimeStatus +
                "; Customer network type=" + p_custNetworkType +
                "; Credit clear date=" + p_creditClearDate +
                "; Disconnect Reason=" + p_disconnectReason +
                "; Company name=" + p_companyName +
                "; Balance Status=" + p_custBalanceStatus +
                "; Account group id=" + p_accountGroupID +
                "; Service offerings=" + p_serviceOffering + 
                "; Communities Id List=" + p_communitiesIdList +
                "; Pin code=" + p_pinCode + 
                "; Home Region Id=" + p_homeRegionId +
                "; Disconnect gen date=" + p_disconnectGenDate +
                "; Connection type=" + p_connectionType);
        }

        i_beginServiceDate        = p_beginServiceDate;
        i_serviceExpiryDate       = p_serviceExpiryDate;
        i_serviceRemovalDate      = p_serviceRemovalDate;
        i_endServiceDate          = p_endServiceDate;
        i_activeServiceClass      = p_activeServiceClass;
        i_preferredLanguage       = p_preferredLanguage;
        i_preferredCurrency       = p_preferredCurrency;
        i_promotionPlan           = p_promotionPlan;
        i_masterAccountStatus     = p_masterAccountStatus;
        i_ivrBarredStatusCode     = p_ivrBarredStatusCode;
        i_serviceStatus           = p_serviceStatus;
        i_airtimeStatus           = p_airtimeStatus;
        i_custNetworkType         = p_custNetworkType;
        i_creditClearDate         = p_creditClearDate;
        i_disconnectReason        = p_disconnectReason;
        i_companyName             = p_companyName;
        i_custBalanceStatus       = p_custBalanceStatus;
        i_airtimeExpiryDate       = p_airtimeExpiryDate;
        i_originalServiceClass    = p_origServiceClass;
        i_tempServiceClassExpDate = p_tempServiceClassExpDate;
        i_masterMsisdn            = p_masterMsisdn;
        i_accountGroupID          = p_accountGroupID;
        i_serviceOffering         = p_serviceOffering;
        i_communitiesIdList       = p_communitiesIdList;
        i_pinCode                 = p_pinCode;
        i_homeRegionId            = p_homeRegionId;
        i_disconnectGenDate       = p_disconnectGenDate;
        i_connectionType          = p_connectionType;
        
        setIvrUnbarDateTime(p_ivrUnbarDateTime);
        calculateIvrBarredStatusCode();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10090, this,
                            "Constructed AccountData(...)");
        }
    }
    
    //-------------------------------------------------------------------------
    //  Private instance methods
    //-------------------------------------------------------------------------
    /**
     * Set the correct IVR barred status using the Ivr Barred Date Time.
     */
    private void calculateIvrBarredStatusCode()
    {
        if (i_ivrUnbarDateTime == null)
        {
            i_ivrBarredStatusCode = C_IVR_STATUS_NOT_BARRED;
        }
        else
        {
            PpasDate l_todayDate = DatePatch.getDateToday();
            if (i_ivrUnbarDateTime.after(l_todayDate))
            {
                i_ivrBarredStatusCode = C_IVR_STATUS_BARRED;
            }
            else
            {
                i_ivrBarredStatusCode = C_IVR_STATUS_NOT_BARRED;
            }
        }
    }

    //-------------------------------------------------------------------------
    //  Public instance methods
    //-------------------------------------------------------------------------

    /** Get the date on which this customer first received service.
     * @return Date the service started.
     */
    public PpasDate getBeginServiceDate()
    {
        return(i_beginServiceDate);
    }

    /** Get the date on which this customer's service will/did end.
     * 
     * @return The date the service ends.
     */
    public PpasDate getEndServiceDate()
    {
        return(i_endServiceDate);
    }

    /**
     * Determines whether this account is disconnected or not. <b>This includes
     * cust status of 'D' and 'R' as well as 'P'.</b>
     * @return True if the master account is disconnected <b>(status 'P', 'D' OR status 'R')</b>; 
     *         false otherwise.
     */
    public boolean isDisconnected()
    {
        return (i_custStatus  == BasicAccountData.C_SUBS_STATUS_PERM_DISC ||
                i_custStatus  == BasicAccountData.C_SUBS_STATUS_DISC_IN_PROG ||
                i_custStatus  == BasicAccountData.C_SUBS_STATUS_RECONNECTION_IN_PROG);
    }
    
    /**
     * Determines whether the Master account is disconnected or not. <b>This includes
     * cust status of 'D' and 'R' as well as 'P'.</b>
     * @return True if the master account is disconnected <b>(status 'P', 'D' OR status 'R')</b>; 
     *         false otherwise.
     */
    public boolean isMasterDisconnected ()
    {
        return ( i_masterAccountStatus  == BasicAccountData.C_SUBS_STATUS_PERM_DISC ||
                 i_masterAccountStatus  == BasicAccountData.C_SUBS_STATUS_DISC_IN_PROG ||
                 i_masterAccountStatus  == BasicAccountData.C_SUBS_STATUS_RECONNECTION_IN_PROG);
    }

    /** Get the date that this customer's service will/did expire.
     * 
     * @return Date of service expiry.
     */
    public PpasDate getServiceExpiryDate()
    {
        return(i_serviceExpiryDate);
    }

    /** Get the date that this customer's subscription will be removed from the
      * network.
      * 
      * @return Date the service will be removed.
      */
    public PpasDate getServiceRemovalDate()
    {
        return(i_serviceRemovalDate);
    }

    /** 
     * Get this customer's current active service class. If the subscriber has an active
     * temporary service class, this will be returned by this method.
     * @return the current active service class
     */
    public ServiceClass getActiveServiceClass()
    {
        return(i_activeServiceClass);
    }

    /** Get this customer's preferred language.
     * 
     * @return The preferred language for this account.
     */
    public String getPreferredLanguage()
    {
        return(i_preferredLanguage);
    }

    /** Get this customer's preferred currency.
     * @return The preferred currency for this account.
     */
    public PpasCurrency getPreferredCurrency()
    {
        return(i_preferredCurrency);
    }

    /** Get this customer's current promotion plan.
     * 
     * @return The current promotion plan on this account.
     */
    public String getPromotionPlan()
    {
        return(i_promotionPlan);
    }

    /** Get the date/time until which this customer is/was barred from accessing the IVR.
      * 
      * @return The earliest date/time this customer can next access the IVR.
      */
    public PpasDateTime getIvrUnbarDateTime()
    {
        return i_ivrUnbarDateTime;
    }

    /** Returns the IVR barred status code string.
     * 
     * @return The current status of IVR barring.
     */
    public String getIvrBarredStatusCode()
    {
        return(i_ivrBarredStatusCode);
    }

    /** Get the service expiry status for this customer.
     * <p>
     * The Service Expiry Status is set to:
     * <p>
     * <ul>
     * <li>  " " = active
     * <li>  L   = Low service ann. provisioned
     * <li>  B   = Bar ann. provisioned 
     * <li>  I   = Incoming calls barred
     * </ul>
     * @return The service status.
     */
    public char getServiceStatus()
    {
        return(i_serviceStatus);
    }

    /** Get the service status.
     * 
     * @return Code indicating the service status.
     */
    public String getServiceStatusCode()
    {
        String l_servStatusCode;

        /** Set up text code string defining the service status. */
        if (!i_serviceExpiryDate.toString().equals(""))
        {
            if (i_serviceStatus == C_SERVICE_ACTIVE_CHAR)
            {
                l_servStatusCode = C_SERVICE_ACTIVE;
            }
            else if (i_serviceStatus == C_SERVICE_LOW_ANNOUNCEMENT_CHAR)
            {
                l_servStatusCode = C_SERVICE_LOW_ANNOUNCEMENT;
            }
            else if (i_serviceStatus == C_SERVICE_BAR_ANNOUNCEMENT_CHAR)
            {
                l_servStatusCode = C_SERVICE_BAR_ANNOUNCEMENT;
            }
            else if (i_serviceStatus == C_SERVICE_EXPIRED_CHAR)
            {
                l_servStatusCode = C_SERVICE_EXPIRED;
            }
            else
            {
                l_servStatusCode = C_SERVICE_STATUS_UNKNOWN;
            }
        }
        else
        {
            l_servStatusCode = C_SERVICE_NONE;
        }

        return(l_servStatusCode);
    }

    /** Returns a meaningful string representing the service status.
     *  @return A String defining the service status of this account. Possible
     *          values are:
     * <ul>
     *          <li>  No Service
     *          <li>  Active
     *          <li>  Low Service Announcement
     *          <li>  Service Barring Announcement
     *          <li>  Expired
     *          <li>  Unknown (status is unrecognised - this should not occur).
     * </ul>
     */
    public String getServiceStatusStr()
    {
        String l_servStatusStr;

        // Set up text code string defining the service status.
        if (i_serviceExpiryDate != null &&
            !i_serviceExpiryDate.toString().equals(""))
        {
            if (i_serviceStatus == C_SERVICE_ACTIVE_CHAR)
            {
                l_servStatusStr = C_SERVICE_ACTIVE_STR;
            }
            else if (i_serviceStatus == C_SERVICE_LOW_ANNOUNCEMENT_CHAR)
            {
                l_servStatusStr = C_SERVICE_LOW_ANNOUNCEMENT_STR;
            }
            else if (i_serviceStatus == C_SERVICE_BAR_ANNOUNCEMENT_CHAR)
            {
                l_servStatusStr = C_SERVICE_BAR_ANNOUNCEMENT_STR;
            }
            else if (i_serviceStatus == C_SERVICE_EXPIRED_CHAR)
            {
                l_servStatusStr = C_SERVICE_EXPIRED_STR;
            }
            else
            {
                l_servStatusStr = C_SERVICE_STATUS_UNKNOWN_STR;
            }
        }
        else
        {
            l_servStatusStr = C_SERVICE_NONE_STR;
        }

        return(l_servStatusStr);
    }

    /** Get the airtime expiry status of this customer (" "=active,
     * X=outgoing calls barred, Z=balance cleared).
     * 
     * @return The airtime status of this account.
     */
    public char getAirtimeStatus()
    {
        return(i_airtimeStatus);
    }
    
    /** Set the date that this customer's airtime will expire.
     * 
     * @param p_airtimeExpiryDate Date of airtime expiry.
     */
    public void setAirtimeExpiryDate(PpasDate p_airtimeExpiryDate)
    {
        i_airtimeExpiryDate = p_airtimeExpiryDate;
    }
        
    /** Get the date that this customer's airtime will/did expire.
     * 
     * @return Date of airtime expiry.
     */
    public PpasDate getAirtimeExpiryDate()
    {
        return(i_airtimeExpiryDate);
    }
    
    /** Get airtime status code string.
     * 
     * @return Description of the airtime status of this account.
     */
    public String getAirtimeStatusCode()
    {
        String l_airtimeStatusCode;

        if (!getAirtimeExpiryDate().toString().equals(""))
        {
            if (i_airtimeStatus == C_AIRTIME_ACTIVE_CHAR)
            {
                l_airtimeStatusCode = C_AIRTIME_ACTIVE;
            }
            else if (i_airtimeStatus == C_AIRTIME_LOW_ANNOUNCEMENT_CHAR)
            {
                l_airtimeStatusCode = C_AIRTIME_LOW_ANNOUNCEMENT;
            }
            else if (i_airtimeStatus == C_AIRTIME_EXPIRED_CHAR)
            {
                l_airtimeStatusCode = C_AIRTIME_EXPIRED;
            }
            else if (i_airtimeStatus == C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR)
            {
                l_airtimeStatusCode = C_AIRTIME_EXPIRED_CREDIT_CLEARED;
            }
            else
            {
                l_airtimeStatusCode = C_AIRTIME_STATUS_UNKNOWN;
            }
        }
        else
        {
            l_airtimeStatusCode = C_AIRTIME_NONE;
        }

        return(l_airtimeStatusCode);
    }
    
    /** Returns a meaningful string representing the airtime status.
     *  @return A String defining the service status of this account. Possible values are: 
     * <ul>
     *          <li>  Active
     *          <li>  Low Airtime Announcement
     *          <li>  Expired
     *          <li>  Credit Cleared
     *          <li>  Unknown (status is unrecognised - this should not occur).
     * </ul>
     */
    public String getAirtimeStatusStr()
    {
        String l_airtimeStatusStr;

        if (getAirtimeExpiryDate() != null && !getAirtimeExpiryDate().toString().equals(""))
        {
            if (i_airtimeStatus == C_AIRTIME_ACTIVE_CHAR)
            {
                l_airtimeStatusStr = C_AIRTIME_ACTIVE_STR;
            }
            else if (i_airtimeStatus == C_AIRTIME_LOW_ANNOUNCEMENT_CHAR)
            {
                l_airtimeStatusStr = C_AIRTIME_LOW_ANNOUNCEMENT_STR;
            }
            else if (i_airtimeStatus == C_AIRTIME_EXPIRED_CHAR)
            {
                l_airtimeStatusStr = C_AIRTIME_EXPIRED_STR;
            }
            else if (i_airtimeStatus == C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR)
            {
                l_airtimeStatusStr = C_AIRTIME_EXPIRED_CREDIT_CLEARED_STR;
            }
            else
            {
                l_airtimeStatusStr = C_AIRTIME_STATUS_UNKNOWN_STR;
            }
        }
        else
        {
            l_airtimeStatusStr = C_AIRTIME_NONE_STR;
        }

        return(l_airtimeStatusStr);
    }

    /** Returns a meaningful string representing the balance status.
     *  @return A String defining the balance status of the account. Possible
     *          values are: 
     *          <br>  Not Barred
     *          <br>  Barring Due on DD/MM/YYYY
     *          <br>  Barred Since DD/MM/YYYY
     */
    public String getBalanceStatusStr()
    {
        String       l_balanceStatusStr;
        PpasDateTime l_now;

        l_now = DatePatch.getDateTimeNow ();  // Logger - not used

        if (i_custBalanceStatus == C_CUST_BALANCE_ACTIVE)
        {
            if ( (i_custBalanceExpDateTime.toString().equals("")) ||
                (i_custBalanceExpDateTime.before(l_now)) )
            {
                l_balanceStatusStr = C_BALANCE_BAR_NONE_STR;
            }
            else
            {
                l_balanceStatusStr = C_BALANCE_BAR_DUE_STR + " " + 
                                     i_custBalanceExpDateTime.
                                     getPpasDate().toString();
            }
        }
        else if ( (i_custBalanceStatus == C_CUST_BALANCE_BARRED) && 
                 (i_custBalanceExpDateTime.before(l_now)) )
        {
            l_balanceStatusStr = C_BALANCE_BAR_SINCE_STR + " " + 
                                 i_custBalanceExpDateTime.
                                 getPpasDate().
                                 toString();
        }
        else
        {
            l_balanceStatusStr = C_BALANCE_BAR_UNKNOWN_STR;
        }
        
        return (l_balanceStatusStr);
    }

    /** Get the network type on which this customer has service (G=GSM, N=Non GSM).
      * 
      * @return The type of network.
      */
    public char getCustNetworkType()
    {
        return(i_custNetworkType);
    }

    /** Get the network type as a meaningful string.
     *  @return Possible values are "GSM", "Non-GSM" or "Unknown".
     */
    public String getCustNetworkTypeStr()
    {
        String l_netTypeCode;

        if (i_custNetworkType == 'G')
        {
            l_netTypeCode = C_NETWORK_TYPE_GSM_STR;
        }
        else if (i_custNetworkType == 'N')
        {
            l_netTypeCode = C_NETWORK_TYPE_NON_GSM_STR;
        }
        else
        {
            l_netTypeCode = C_NETWORK_TYPE_UNKNOWN_STR;
        }
        return(l_netTypeCode);
    }

    /** Get the network type string (GSM or NON_GSM).
     * 
     * @return The typeof the network.
     */
    public String getCustNetworkTypeCode()
    {
        String l_netTypeCode;

        if (i_custNetworkType == 'G')
        {
            l_netTypeCode = C_NETWORK_TYPE_GSM;
        }
        else if (i_custNetworkType == 'N')
        {
            l_netTypeCode = C_NETWORK_TYPE_NON_GSM;
        }
        else
        {
            l_netTypeCode = C_NETWORK_TYPE_UNKNOWN;
        }
        return(l_netTypeCode);
    }
    
    /** Get the cledit clearance date for this customer.
     * 
     * @return The date the account was credit cleared.
     */
    public PpasDate getCreditClearDate()
    {
        return(i_creditClearDate);
    }

    /** Get the master msisdn of this account according the business logic.
     * 
     * @return The mobile number of the account owner.
     */
    public Msisdn getMasterMsisdn()
    {
        return isSubordinate() ? i_masterMsisdn : getMsisdn();
    }
    
    /** Get the master msisdn of this account.
     * 
     * @return MSISDN of account holder.
     */
    public Msisdn getRealMasterMsisdn()
    {
        return (i_masterMsisdn);
    }
    

    /** Get disconnect Reason for this account.
     * 
     * @return The reason for disconnection.
     */
    public String getDisconnectReason()
    {
        return(i_disconnectReason);
    }

    /** Get disconnect Reason description for this account.
     * 
     * @return The reason description for disconnection.
     */
    public String getDisconnectReasonDesc()
    {
        return(i_disconnectReasonDesc);
    }

    /** Get the company name for this customer.
     * 
     * @return The name of the company of the user.
     */
    public String getCompanyName()
    {
        return (i_companyName);
    }

    /** Set the date on which this customer first received service.
     * 
     * @param p_beginServiceDate Date ofstart of service.
     */
    public void setBeginServiceDate(PpasDate p_beginServiceDate)
    {
        i_beginServiceDate = p_beginServiceDate;
    }

    /** Set the date on which this customer's service will/did end.
     * 
     * @param p_endServiceDate Date of end of service.
     */
    public void setEndServiceDate(PpasDate p_endServiceDate)
    {
        i_endServiceDate = p_endServiceDate;
    }

    /** Set the date that this customer's service will/did expire.
     * 
     * @param p_serviceExpiryDate Date of service expiry.
     */
    public void setServiceExpiryDate(PpasDate p_serviceExpiryDate)
    {
        i_serviceExpiryDate = p_serviceExpiryDate;
    }

    /** Set the date that this customer's subscription will be removed from the
      * network.
      * 
      * @param p_serviceRemovalDate Date of service removal.
      */
    public void setServiceRemovalDate(PpasDate p_serviceRemovalDate)
    {
        i_serviceRemovalDate = p_serviceRemovalDate;
    }

    /** Set this customer's service class.
     *
     * @param p_activeServiceClass New service class for this account. 
     */
    public void setActiveServiceClass(ServiceClass p_activeServiceClass)
    {
        i_activeServiceClass = p_activeServiceClass;
    }

    /** Set this customer's preferred language.
     *
     * @param p_preferredLanguage New preferred language for this account. 
     */
    public void setPreferredLanguage(String p_preferredLanguage)
    {
        i_preferredLanguage = p_preferredLanguage;
    }

    /** Set this customer's preferred currency.
     *
     * @param p_preferredCurrency New preferred currency for this account. 
     */
    public void setPreferredCurrency(PpasCurrency p_preferredCurrency)
    {
        i_preferredCurrency = p_preferredCurrency;
    }

    /** Set this customer's current promotion plan.
     * 
     * @param p_promotionPlan New promotion plan for this account.
     */
    public void setPromotionPlan(String p_promotionPlan)
    {
        i_promotionPlan = p_promotionPlan;
    }

    /** Set the date/time until which this customer is barred from using the IVR. This will be set to the
     * current date/time for successful recharges (i.e. the customer is free to use the IVR).
      * 
      * @param p_ivrUnbarDateTime Date/time until which this customer cannot use the IVR.
      */
    public void setIvrUnbarDateTime(PpasDateTime p_ivrUnbarDateTime)
    {
        i_ivrUnbarDateTime = p_ivrUnbarDateTime;
        calculateIvrBarredStatusCode();
    }

    /** Set the service expiry status for this customer.
     * <p>
     * The Service Expiry Status is set to:
     * <ul>
     * <li>  " " = active
     * <li>  L   = Low service ann. provisioned
     * <li>  B   = Bar ann. provisioned 
     * <li>  I   = Incoming calls barred
     * </ul>
     * 
     * @param p_serviceStatus New service status.
     */
    public void setServiceStatus(char p_serviceStatus)
    {
        i_serviceStatus = p_serviceStatus;
    }

    /** Set the airtime expiry status of this customer.
     * <p>
     * The Airtime Expiry Status is set to:
     * <ul>
     * <li>  " " = active
     * <li>  X   = outoging calls barred
     * <li>  Z   = balance cleared
     * </ul>
     * 
     * @param p_airtimeStatus New airtime status. 
     */
    public void setAirtimeStatus(char p_airtimeStatus)
    {
        i_airtimeStatus = p_airtimeStatus;
    }

    /** Set the network type on which this customer has service 
      * (G=GSM, N=Non GSM).
      * 
      * @param p_custNetworkType Type of network.
      */
    public void setCustNetworkType(char p_custNetworkType)
    {
        i_custNetworkType = p_custNetworkType;
    }
    
    /** Set the credit clearance date associated with this customer.
     * 
     * @param p_creditClearDate New date of credit clearance for this account.
     */
    public void setCreditClearDate(PpasDate p_creditClearDate)
    {
        i_creditClearDate = p_creditClearDate;
    }

    /** Set the master msisdn of this account.
     * 
     * @param p_masterMsisdn MSISDN of account holder.
     */
    public void setMasterMsisdn(Msisdn p_masterMsisdn)
    {
        i_masterMsisdn = p_masterMsisdn;
    }
   
    /** Set the disconnect reason.
     * 
     * @param p_disconnectReason Reason for disconnection.
     */
    public void setDisconnectReason(String p_disconnectReason)
    {
        i_disconnectReason = p_disconnectReason;
    }

    /** Set the disconnect reason description.
     * 
     * @param p_disconnectReasonDesc Reason description for disconnection.
     */
    public void setDisconnectReasonDesc(String p_disconnectReasonDesc)
    {
        i_disconnectReasonDesc = p_disconnectReasonDesc;
    }

    /** Set the Master Account Status.
     * 
     * @param p_masterAccountStatus Status of the account holder.
     */
    public void setMasterAccountStatus(char p_masterAccountStatus)
    {
        i_masterAccountStatus = p_masterAccountStatus;
    }
   
    /** Get the Master Account Status.
     * 
     * @return The status of the master account.
     */
    public char getMasterAccountStatus()
    {
        return i_masterAccountStatus;
    }
 
    /** 
     * Return the customer balance status.
     * 
     * @return customer balance status 
     */
    public char getCustBalanceStatus()
    {
        return i_custBalanceStatus;
    }

    /** 
     * Return the original service class.
     * 
     * @return the original service class
     */
    public ServiceClass getOriginalServiceClass()
    {
        return i_originalServiceClass;
    }

    /** 
     * Return the temp service class expiry date.
     * 
     * @return the temp service class expiry date 
     */
    public PpasDate getTempServiceClassExpDate()
    {
        return i_tempServiceClassExpDate;
    }
    
    /** 
     * Allows to set the customer balance status.
     * 
     * @param p_custBalanceStatus the customer balance status
     */
    public void setCustBalanceStatus(char p_custBalanceStatus)
    {
        i_custBalanceStatus = p_custBalanceStatus;
    }

    /** 
     * Allows to set the Ivr barred status code.
     * 
     * @param p_ivrBarredStatusCode the Ivr barred status code
     */
    public void setIvrBarredStatusCode(String p_ivrBarredStatusCode)
    {
        i_ivrBarredStatusCode = p_ivrBarredStatusCode;
    }

    /** 
     * Allows to set the original Service class.
     * 
     * @param p_originalServiceClass the original Service class
     */
    public void setOriginalServiceClass(ServiceClass p_originalServiceClass)
    {
        i_originalServiceClass = p_originalServiceClass;
    }
    
    /** 
     * Allows to set the temp service class expiry date.
     * 
     * @param p_tempServiceClassExpDate the temp service class expiry date 
     */
    public void setTempServiceClassExpDate(PpasDate p_tempServiceClassExpDate)
    {
        i_tempServiceClassExpDate = p_tempServiceClassExpDate;
    }
    
    /** Compare this account with another object.
     * 
     * @param p_object Another object to compare with this account.
     * @return True if the accounts are logically the same, otherwise false.
     */
    public boolean equals(Object p_object)
    {
        return p_object instanceof AccountData ? this.equals((AccountData)p_object) : false;
    }
    
    /** Compare this account with another <code>AccountData</code> object.
     * 
     * @param p_account Another <code>AccountData</code> object to compare with this account.
     * @return True if the accounts are logically the same, otherwise false.
     */
    public boolean equals(AccountData p_account)
    {
        if (this.i_beginServiceDate != null)
        {
            if (p_account.getBeginServiceDate() != null)
            {
                if (!this.i_beginServiceDate.toString().equals(p_account.getBeginServiceDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                 return false;
            } 
        }
        else if (p_account.getBeginServiceDate() != null)
        {
            return false;
        }
         
        if (this.i_endServiceDate != null)
        {
            if (p_account.getEndServiceDate() != null)
            {
                if (!this.i_endServiceDate.toString().equals(p_account.getEndServiceDate().toString()))
                {
                    return false;
                }
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getEndServiceDate() != null)
        {
            return false;
        }
         
        if (this.i_serviceExpiryDate != null)
        {
            if (p_account.getServiceExpiryDate() != null)
            {
                if (!this.i_serviceExpiryDate.toString().equals(p_account.getServiceExpiryDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getServiceExpiryDate() != null)
        {
            return false;
        }
         
        if (this.i_serviceRemovalDate != null)
        {
            if (p_account.getServiceRemovalDate() != null)
            {
                if (!this.i_serviceRemovalDate.toString().equals(
                    p_account.getServiceRemovalDate().toString()))
                {
                     return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getServiceRemovalDate() != null)
        {
            return false;
        }
         
        if (this.i_activeServiceClass != null)
        {
            if (!this.i_activeServiceClass.equals(p_account.getActiveServiceClass()))
            {
                return false;
            } 
        }
        else if (p_account.getActiveServiceClass() != null)
        {
            return false;
        } 
        
        if (this.i_preferredLanguage != null)
        {
            if (!this.i_preferredLanguage.equals(p_account.getPreferredLanguage()))
            {
                return false;
            }
        }
        else if (p_account.getPreferredLanguage() != null)
        {
            return false;
        }
         
        if (this.i_preferredCurrency != null)
        {
            if (!this.i_preferredCurrency.equals(p_account.getPreferredCurrency()))
            {
                return false;
            } 
        }
        else if (p_account.getPreferredCurrency() != null)
        {
            return false;
        }
         
        if (this.i_promotionPlan != null)
        {
            if (!this.i_promotionPlan.equals(p_account.getPromotionPlan()))
            {
                return false;
            } 
        }
        else if (p_account.getPromotionPlan() != null)
        {
            return false;
        } 
        
        if (this.i_ivrUnbarDateTime != null)
        {
            if (p_account.getIvrUnbarDateTime() != null)
            {
                if (!this.i_ivrUnbarDateTime.toString().equals(p_account.getIvrUnbarDateTime().toString()))
                {
                    return false;
                } 
            }
            else
            {
                 return false;
            } 
        }
        else if (p_account.getIvrUnbarDateTime() != null)
        {
            return false;
        } 
        
        if (this.i_ivrBarredStatusCode != null)
        {
            if (!this.i_ivrBarredStatusCode.equals(p_account.getIvrBarredStatusCode()))
            {
                return false;
            }
        }
        else if (p_account.getIvrBarredStatusCode() != null)
        {
            return false;
        } 
          
        if (this.i_serviceStatus != p_account.getServiceStatus())
        {
            return false;
        }

        if (this.i_airtimeStatus != p_account.getAirtimeStatus())
        {
            return false;
        }

        if (this.i_creditClearDate != null)
        {
            if (p_account.getCreditClearDate() != null)
            {
                if (!this.i_creditClearDate.toString().equals(p_account.getCreditClearDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getCreditClearDate() != null)
        {
            return false;
        }
         
        if (i_masterMsisdn != null)
        {
            if (!this.i_masterMsisdn.equals(p_account.getRealMasterMsisdn()))
            {
                return false;
            }
        }
        else if (p_account.getRealMasterMsisdn() != null )
        {
            return false;
        }
         
        if (this.i_disconnectReason != null)
        {
            if (!this.i_disconnectReason.equals(p_account.getDisconnectReason()))
            {
                return false;
            } 
        }
        else if (p_account.getDisconnectReason() != null)
        {
            return false;
        }
         
        if (this.i_companyName != null)
        {
            if (!this.i_companyName.equals(p_account.getCompanyName()))
            {
                return false;
            } 
        }
        else if (p_account.getCompanyName() != null)
        {
            return false;
        }
         
        if (this.i_custBalanceStatus != p_account.getCustBalanceStatus())
        {
            return false;
        }
        
        if (this.i_custBalanceExpDateTime != null)
        {
            if (p_account.getCustBalanceExpDateTime() != null)
            {
                if (!this.i_custBalanceExpDateTime.toString().equals(
                        p_account.getCustBalanceExpDateTime().toString()))
                {
                    return false;
                } 
            }
            else
            {
                 return false;
            } 
        }
        else if (p_account.getCustBalanceExpDateTime() != null)
        {
            return false;
        }
         
        if (this.i_airtimeExpiryDate != null)
        {
            if (p_account.getAirtimeExpiryDate() != null)
            {
                if (!this.i_airtimeExpiryDate.toString().equals(p_account.getAirtimeExpiryDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getAirtimeExpiryDate() != null)
        {
            return false;
        } 
        
        if (this.i_masterAccountStatus != p_account.getMasterAccountStatus())
        {
           return false;
        }
        
        if (this.i_tempServiceClassExpDate != null)
        {
            if (p_account.getTempServiceClassExpDate() != null)
            {
                if (!this.i_tempServiceClassExpDate.toString().equals(
                        p_account.getTempServiceClassExpDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getTempServiceClassExpDate() != null)
        {
            return false;
        } 
        
        if (this.i_originalServiceClass != null)
        {
            if (!this.i_originalServiceClass.equals(p_account.getOriginalServiceClass()))
            {
               return false;
            }
        }
        else if (p_account.getOriginalServiceClass() != null)
        {
            return false;
        } 

        if (this.i_accountGroupID != null)
        {
            if (!this.i_accountGroupID.equals(p_account.getAccountGroupID()))
            {
               return false;
            }
        }
        else if (p_account.getAccountGroupID() != null)
        {
            return false;
        } 

        if (this.i_serviceOffering != null)
        {
            if (!this.i_serviceOffering.equals(p_account.getServiceOffering()))
            {
               return false;
            }
        }
        else if (p_account.getServiceOffering() != null)
        {
            return false;
        }
        
        if (this.i_communitiesIdList != null)
        {
            if (!this.i_communitiesIdList.equals(p_account.getCommunitiesIdList()))
            {
               return false;
            }
        }
        else if (p_account.getCommunitiesIdList() != null)
        {
            return false;
        } 
        
        if (i_pinCode != null)
        {
            if (!i_pinCode.equals(p_account.getPinCode()))
            {
                return false;
            }
        }
        else if (p_account.getPinCode() != null)
        {
            return false;
        }

        if (i_homeRegionId != null)
        {
            if (!i_homeRegionId.equals(p_account.getHomeRegionId()))
            {
                return false;
            }
        }
        else if (p_account.getHomeRegionId() != null)
        {
            return false;
        }
        
        return true;
    }
    
    /** Get the balance expiry date/time.
     * 
     * @return Date/time of balance expiry on this account.
     */
    public PpasDateTime getCustBalanceExpDateTime()
    {
        return i_custBalanceExpDateTime;
    }

    /** 
     * Allows to set the customer balance expiry date/time.
     * 
     * @param p_custBalanceExpDateTime the customer balance expiry date 
     */
    public void setCustBalanceExpDateTime (PpasDateTime p_custBalanceExpDateTime)
    {
        i_custBalanceExpDateTime = p_custBalanceExpDateTime;
    }
    
    /** Set the name of the company.
     * 
     * @param p_companyName Name of company.
     */
    public void setCompanyName(String p_companyName)
    {
        i_companyName = p_companyName;
    }
    
    /** Display the account information as a <code>String</code>.
     * 
     * @return String representation of the account.
     */
    public String toString()   
    {
        StringBuffer l_sB = new StringBuffer();
        l_sB.append(super.toString());
        l_sB.append(",Begin Service Date: " + i_beginServiceDate);
        l_sB.append(",End Service Date: " + i_endServiceDate);
        l_sB.append(",Service Expiry Date: " + i_serviceExpiryDate);
        l_sB.append(",Service Removal Date: " + i_serviceRemovalDate);
        l_sB.append(",Active Service class: " + i_activeServiceClass);
        l_sB.append(",Preferred language: " + i_preferredLanguage);
        l_sB.append(",Preferred currency: " + i_preferredCurrency);
        l_sB.append(",Promotion plan: " + i_promotionPlan);
        l_sB.append(",IVR unbar Date Time: " + i_ivrUnbarDateTime);
        l_sB.append(",IVR barred status code: " + i_ivrBarredStatusCode);
        l_sB.append(",Service Status: " + i_serviceStatus);
        l_sB.append(",Airtime Status: " + i_airtimeStatus);
        l_sB.append(",Customer Network type: " + i_custNetworkType);
        l_sB.append(",Credit Clear Date: " + i_creditClearDate);
        l_sB.append(",Master msisdn: " + i_masterMsisdn);
        l_sB.append(",Disconnect reason: " + i_disconnectReason);
        l_sB.append(",Company name: " + i_companyName);
        l_sB.append(",Customer Balance status: " + i_custBalanceStatus);
        l_sB.append(",Customer Balance expiry date time: " + i_custBalanceExpDateTime);
        l_sB.append(",Airtime Expiry Date: " + i_airtimeExpiryDate);
        l_sB.append(",Master Account status: " + i_masterAccountStatus);
        l_sB.append(",Original Service class: " + i_originalServiceClass);
        l_sB.append(",Temp service class expiration date: " + i_tempServiceClassExpDate);
        l_sB.append(",Account Group Id: " + i_accountGroupID);
        l_sB.append(",Service Offerings: " + i_serviceOffering);
        l_sB.append(",Community Id list: " + i_communitiesIdList);
        l_sB.append(",Pin code: " + i_pinCode);
        l_sB.append(",Home Region Id: " + i_homeRegionId);

        return l_sB.toString();
    }
    
    /** Get the subscriber's temp blocking status.
     * @return boolean indicator
     */
    public boolean isTempBlocked()
    {
        return i_tempBlocked;
    }
    
    /** Get temp blocking status as a string for display.
     * @return display string
     */
    public String getTempBlockedString()
    {
        String l_return = C_TEMP_BLOCKED_CLEAR;
        
        if (isTempBlocked())
        {
            l_return = C_TEMP_BLOCKED_SET;
        }
        
        return l_return;
    }

    /** Set the subscriber's temp blocking status.
     * @param p_blocked blocking indicator
     */
    public void setTempBlocked(boolean p_blocked)
    {
        i_tempBlocked = p_blocked;
    }

    /** Get the subscriber's Account Group ID.
     * @return The subcriber account group ID
     */
    public AccountGroupId getAccountGroupID()
    {
        return i_accountGroupID;
    }

    /** Get the subscriber's Service Offering.
     * @return The subcriber service offering
     */
    public ServiceOfferings getServiceOffering()
    {
        return i_serviceOffering;
    }
    
    /** Get the subscriber's Initial Credit.
     * @return The subcriber's Initial Credit.
     */
    public Money getInitialCredit()
    {
        return i_initialCredit;
    }    

    /** Set the subscriber's Account Group ID.
     * @param p_accountGroupID The subcriber account group ID
     */
    public void setAccountGroupID(AccountGroupId p_accountGroupID)
    {
        i_accountGroupID = p_accountGroupID;
    }

    /** Set the subscriber's Service Offering.
     * @param p_serviceOffering The subcriber service offering
     */
    public void setServiceOffering(ServiceOfferings p_serviceOffering)
    {
        i_serviceOffering = p_serviceOffering;
    }
    
    /** Set the subscriber's Initial Credit.
     * @param p_initialCredit The subcriber's Initial Credit.
     */
    public void setInitialCredit(Money p_initialCredit)
    {
        i_initialCredit = p_initialCredit;
    }

    /** Get the subscriber's USSD EoCN selection structure ID.
     * @return The subscriber's USSD EoCN ID.
     */
    public EndOfCallNotificationId getUssdEocnId()
    {
        return i_ussdEocnId;
    }

    /** Set the subscriber's USSD EoCN selection structure ID.
     * @param p_ussdEocnId The subscriber's USSD EoCN ID.
     */
    public void setUssdEocnId(EndOfCallNotificationId p_ussdEocnId)
    {
        i_ussdEocnId = p_ussdEocnId;
    }
    
    /** Return the Communities Id List. 
     * 
     * @return  CommunitiesIdListData representing Communities Id List Data.
     */
    public CommunitiesIdListData getCommunitiesIdList()
    {
        return i_communitiesIdList;
    }
    
    /** Set the Communities Id List.
     * 
     * @param p_communitiesIdList   Communities Id List.
     */
    public void setCommunitiesIdList(CommunitiesIdListData p_communitiesIdList)
    {
        i_communitiesIdList = p_communitiesIdList;
    }

    /** 
     * Get the pin code for IVR access via landline.
     * @return Pin code for IVR access via landline.
     */
    public PinCode getPinCode()
    {
        return i_pinCode;
    }

    /** 
     * Set the pin code for IVR access via landline.
     * @param p_pinCode Pin code for IVR access via landline.
     */
    public void setPinCode(PinCode p_pinCode)
    {
        i_pinCode = p_pinCode;
    }
    
    /** Get the Home Region Id. 
     * @return Home Region Id for this account.
     **/
    public HomeRegionId getHomeRegionId()
    {
        return i_homeRegionId;
    }
    
    /** Set the Home Region Id for this account. 
     * @param p_homeRegionId Home Region Id for this account.
     **/
    public void setHomeRegionId(HomeRegionId p_homeRegionId)
    {
        i_homeRegionId = p_homeRegionId;
    }
    
    /** Get the disconnection date for this account. 
     * @return Disconnect gen date.
     **/
    public PpasDate getDisconnectGenDate()
    {
        return i_disconnectGenDate;
    }

    /** Get the connection type for this account.
     * @return The connection type.
     */
    public char getConnectionType()
    {
        return i_connectionType;
    }
    
    /** Set the connection type for this account.
     * @param p_connectionType Connection type to set.
     */
    public void setConnectionType(char p_connectionType)
    {
        i_connectionType = p_connectionType;
    }

    /** Get the date/time of the last service class change for this account.
     * @return Last service class change date/time.
     */
    public PpasDateTime getLastServiceClassChangeDateTime()
    {
        return i_lastServiceClassChangeDateTime;
    }
    
    /** Set the date of the last service class change for this account.
     * @param p_lastServiceClassChangeDateTime Date/time of last service class change.
     */
    public void setLastServiceClassChangeDate(PpasDateTime p_lastServiceClassChangeDateTime)
    {
        i_lastServiceClassChangeDateTime = p_lastServiceClassChangeDateTime;
    }
} // End of class AccountData
