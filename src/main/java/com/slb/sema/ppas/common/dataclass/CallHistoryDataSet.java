////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CustMsisdnsData.Java
//      DATE            :       8-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8062, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Call History Data Set
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 29/05/06 |M.Toernqvist| Changes after review.           | PpasLon#8900
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;
import java.util.Comparator;
import java.util.TreeSet;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class handles a collection of CallHistoryData items. This class is used by the
 * Call History functionallity, that makes it possible to for example: how much a call cost
 * or what calls that have been made.
 * NOTE - in this version all the objects are added to the set - then at the end the set is trimmed
 *        to the defined maximum number of data records.
 */
public class CallHistoryDataSet extends DataSetObject implements Serializable
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CallHistoryDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Call History Data Set. */
    private TreeSet  i_callHistoryData;
    
    /** Search start date. */
    private PpasDate i_startDate;
    
    /** Search end date. */
    private PpasDate i_endDate;
    
    /** Maximum number of calls. */
    private int      i_maxNumberOfCalls;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Use this constructor if no CUMS records are found for a customer in a given period.
     * Therefore no call data cna be retrieved.
     * @param p_startDate Start date
     * @param p_endDate   End date
     */
    public CallHistoryDataSet(PpasDate p_startDate,
                              PpasDate p_endDate )
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME + " with startDate=" + 
                            p_startDate + " and endDate=" + p_endDate );
        }

        i_callHistoryData = new TreeSet();
        i_startDate       = p_startDate;
        i_endDate         = p_endDate;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME + " using start- and end date");
        }
    }
    
    /**
     * @param p_maxCalls this is the maximum number of objects the set is meant to hold.
     * @param p_comparator Defines the type of comparator to be used, the comparators are defined as inner
     *        classes: DateDscSort - Date descending,
     *                 DateAscSort - Date ascending,
     *                 OtherPartyNoDscSort - B-number descending,
     *                 OtherPartyNoAscSort - B-number ascending
     */
    public CallHistoryDataSet( int        p_maxCalls,
                               Comparator p_comparator )
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 11010, this,
                            "Constructing " + C_CLASS_NAME + " with maxCalls=" + p_maxCalls + 
                            " and Comparator" );
        }

        i_callHistoryData  = new TreeSet(p_comparator);
        i_maxNumberOfCalls = p_maxCalls;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 11020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------
    /** Adds a CallHistoryData object to the data set.
     * 
     * @param p_data the data to add into the set.
     */
    public void add( CallHistoryData p_data )
    {
        i_callHistoryData.add(p_data);
    }
    
    /** Returns the dataset as an array.
     * 
     * @return the dataset as an array.
     */
    public CallHistoryData[] asArray()
    {
        return (CallHistoryData[])i_callHistoryData.toArray(new CallHistoryData[i_callHistoryData.size()]);
    }
    
    /** Returns the number of call history records in the set.
     * 
     * @return the size of the kept dataset.
     */
    public int size()
    {
        return i_callHistoryData.size();
    }
 
    /** Returns the maximium size of this set.
     * 
     * @return the maximum allowable size.
     */
    public int getMaxSize()
    {
        return i_maxNumberOfCalls;
    }
 
    /** Trim the TreeSet to p_maxSixe objects. Remove all the last objects
     * above the max size limit.
     */
    public void trim()
    {
        if ( i_callHistoryData != null )
        {
            while ( i_callHistoryData.size() > i_maxNumberOfCalls )
            {
                i_callHistoryData.remove(i_callHistoryData.last());
            }
        }
    }
    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    public void describeThis( StringBuffer p_sb )
    {
        describeData("Call History Data", i_callHistoryData, p_sb);
    }

    /** Returns the search end date.
     * 
     * @return search end date.
     */
    public PpasDate getEndDate()
    {
        return i_endDate;
    }

    /** Sets the search end date.
     * 
     * @param p_endDate new search end date.
     */
    public void setEndDate(PpasDate p_endDate)
    {
        i_endDate = p_endDate;
    }

    /** Returns the search start date.
     * 
     * @return search start date.
     */
    public PpasDate getStartDate()
    {
        return i_startDate;
    }

    /** Sets the search start date.
     * 
     * @param p_startDate new search start date.
     */
    public void setStartDate(PpasDate p_startDate)
    {
        i_startDate = p_startDate;
    }
}
