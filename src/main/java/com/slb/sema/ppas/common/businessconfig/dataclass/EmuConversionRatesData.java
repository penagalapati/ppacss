////////////////////////////////////////////////////////////////////////////////
//    ACSC IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :      EmuConversionRatesData.Java
//    DATE            :      10-Mar-2004
//    AUTHOR          :      Olivier Duparc
//    REFERENCE       :      PRD_ASCS_GEN_SS_071
//
//    COPYRIGHT       :      ATOS ORIGIN 2004
//
//    DESCRIPTION     :      A Emu Conversion Rate.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |       
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single Emu Conversion Rate.
 */
public class EmuConversionRatesData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "EmuConversionRatesData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Currency code. */
    private  String            i_emcrCurrency = null;

    /** Start Date. */
    private  PpasDateTime      i_emcrStartDate = null;

    /** End Date. */
    private  PpasDateTime      i_emcrEndDate = null;

    /** Country. */
    private  String            i_emcrCountry = null;

    /** Euro Conv Rate. */
    private  double            i_emcrEuroConvRate = 0;

    /** Currency per unit. */
    private  float             i_emcrCurrencyPerUnit = 0;

    /** 'Deleted' flag. */
    private  char              i_emcrDelFlag = '\0';
    
    /** The operator identifier of the operator that either
     * created the row or performed the last update. 
     */
    private String             i_emcrOpid = null;
    
    /** The date and time of the last modification to this row of the table. */
    private PpasDateTime       i_emcrGenYmdhms;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid language object and initialises instance variables.
     * 
     * @param p_request             The request to process.
     * @param p_emcrCurrency        The currency code.
     * @param p_emcrStartDate       The start date time for the rate.
     * @param p_emcrEndDate         The end date time for the rate.
     * @param p_emcrCountry         The country of the currency.
     * @param p_emcrEuroConvRate    The Euro conversion rate.
     * @param p_emcrCurrencyPerUnit The currency per unit.
     * @param p_emcrDelFlag         The 'deleted' flag.
     * @param p_emcrOpid            The operator that last changed this data.
     * @param p_emcrGenYmdhms       The date/time this data was last changed.
     */
    public EmuConversionRatesData(
        PpasRequest            p_request,
        String                 p_emcrCurrency,
        PpasDateTime           p_emcrStartDate,
        PpasDateTime           p_emcrEndDate,
        String                 p_emcrCountry,
        double                 p_emcrEuroConvRate,
        float                  p_emcrCurrencyPerUnit,
        char                   p_emcrDelFlag,
        String                 p_emcrOpid,
        PpasDateTime           p_emcrGenYmdhms)
    {

        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_emcrCurrency        = p_emcrCurrency;
        i_emcrStartDate       = p_emcrStartDate;
        i_emcrEndDate         = p_emcrEndDate;
        i_emcrCountry         = p_emcrCountry;
        i_emcrEuroConvRate    = p_emcrEuroConvRate;
        i_emcrCurrencyPerUnit = p_emcrCurrencyPerUnit;
        i_emcrDelFlag         = p_emcrDelFlag;
        i_emcrOpid            = p_emcrOpid;
        i_emcrGenYmdhms       = p_emcrGenYmdhms;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor
      //         EmuConversionRatesData(PpasRequest, long, Logger, ...)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the currency code.
     * 
     * @return The currency code for this rate.
     */
    public String getCurrency()
    {
        return(i_emcrCurrency);
    }

    /** Returns the start date of the rate.
     * 
     * @return The start date of the rate.
     */
    public PpasDateTime getStartDate()
    {
        return(i_emcrStartDate);
    }

    /** Returns the end date of the rate.
     * 
     * @return The end date of the rate.
     */
    public PpasDateTime getEndDate()
    {
        return(i_emcrEndDate);
    }

    /** Returns the country.
     * 
     * @return The country for this rate.
     */
    public String getCountry()
    {
        return(i_emcrCountry);
    }

    /** Returns the Euro conversion rate.
     * 
     * @return The Euro conversion rate.
     */
    public double getEuroConvRate()
    {
        return i_emcrEuroConvRate;
    }
    
    /** Returns the currency per unit.
     * 
     * @return The currency per unit.
     */
    public float getCurrencyPerUnit()
    {
        return (i_emcrCurrencyPerUnit);
    }
    
    /** Returns true if the entry is flagged as 'deleted'.
     * 
     * @return True if the entry is flagged as 'deleted'.
     */
    public boolean isDeleted()
    {
        return (i_emcrDelFlag == '*');
    }   
    
    /** Returns Operator Identifier.
     * 
     * @return The operator who last changed this data.
     */
    public String getEmcrOpid()
    {
        return (i_emcrOpid);
    }
    
    /** Return Date and time of last modification to the row in the table.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getEmcrGenYmdhms()
    {
        return (i_emcrGenYmdhms);
    }
} // End of public class EmuConversionRatesData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////