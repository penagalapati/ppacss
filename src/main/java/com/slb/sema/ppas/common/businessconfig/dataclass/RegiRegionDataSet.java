////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RegiRegionDataSet.Java
//      DATE            :       11-Aug-2005
//      AUTHOR          :       David Bitmead
//
//      REFERENCE       :       PRD_ASCS00_GEN_CA_44
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       A set of data corresponding to the contents of
//                              the regi_region table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                    | REFERENCE
//----------+------------+--------------------------------+---------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Iterator;
import java.util.TreeSet;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** A set of data corresponding to the contents of the regi_region db table. */
public class RegiRegionDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RegiRegionDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Set of home region data objects. */
    private TreeSet   i_regiDataSet;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new empty set of home region data.
     *  @param p_request The request being processed.
     */
    public RegiRegionDataSet(PpasRequest p_request)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 12510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_regiDataSet = new TreeSet();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 12590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Adds element to Data Set.
     *  @param  p_regiRegionData Home Region data to be added to the underlying data set.
     */
    public void add(RegiRegionData p_regiRegionData)
    {
        i_regiDataSet.add (p_regiRegionData);
    }
    
    
    /**
     * Returns home region data associated with a specified <code>HomeRegionId</code> .
     * 
     * @param p_homeRegionId The home region id.
     * @return RegiRegionData record from this DataSet with the given <code>HomeRegionId</code>. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public RegiRegionData getRecord (HomeRegionId p_homeRegionId)
    {
        RegiRegionData l_regiRegionData = null;
        
        if (p_homeRegionId != null)
        {
            for (Iterator l_it = i_regiDataSet.iterator(); l_it.hasNext();)
            {
                l_regiRegionData = (RegiRegionData) l_it.next();
                
                if(!l_regiRegionData.getHomeRegionId().equals(p_homeRegionId))
                {
                    l_regiRegionData = null;
                }
                else
                {
                    break;
                }
            }
        }

        return l_regiRegionData;
    }

    /**
     * Returns available (that is, not marked as deleted) home region data associated with a specified home 
     * region id.
     * @param p_homeRegionId The home region id.
     * 
     * @return RegiRegionData record from this DataSet with the given home region id. If no record can be
     * found, then <code>null</code> is returned.
     */
    public RegiRegionData getAvailableRecord(HomeRegionId p_homeRegionId)
    {
        RegiRegionData l_regiRegionData = getRecord(p_homeRegionId);

        return (l_regiRegionData != null && l_regiRegionData.isDeleted()) ? null : l_regiRegionData;
    }
    
    /** Returns array of all records in this data set object.
     * 
     * @return All Regions.
     */
    public RegiRegionData [] getAllArray()
    {      
        return (RegiRegionData []) i_regiDataSet.toArray(new RegiRegionData[i_regiDataSet.size()]);
    }

    /**
     * Get a data set containing the all available home region records, that is, those not marked as deleted.
     * @return Array containing available home region records in the cache (i.e. those not marked as deleted).
     */
    public RegiRegionData [] getAvailableArray()
    {
        RegiRegionData [] l_availableARR = null;
        RegiRegionData    l_regiRegionData = null;
        TreeSet           l_availableTS = new TreeSet();
        
        for (Iterator l_it = i_regiDataSet.iterator(); l_it.hasNext();)
        {
            l_regiRegionData = (RegiRegionData) l_it.next();
            
            if(!l_regiRegionData.isDeleted())
            {
                l_availableTS.add(l_regiRegionData);
            }
        }
        
        l_availableARR = (RegiRegionData []) l_availableTS.toArray(new RegiRegionData[0]); 
        
        return l_availableARR;
    }
    
    /**
     * Get a data set containing the all available home region records, that is, those not marked as deleted.
     * @return Data Set containing available home region records in the cache
     * (i.e. those not marked as deleted).
     */
    public RegiRegionDataSet getAvailable()
    {
        RegiRegionData    l_regiRegionData = null;
        RegiRegionDataSet l_available = new RegiRegionDataSet(null);
        
        for (Iterator l_it = i_regiDataSet.iterator(); l_it.hasNext();)
        {
            l_regiRegionData = (RegiRegionData) l_it.next();
            
            if(!l_regiRegionData.isDeleted())
            {
                l_available.add(l_regiRegionData);
            }
        } 
        
        return l_available;
    }
    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("HomeRegions", i_regiDataSet, p_sb);
    }
}
