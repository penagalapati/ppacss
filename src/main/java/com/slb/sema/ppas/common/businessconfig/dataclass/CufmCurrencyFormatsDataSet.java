////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CufmCurrencyFormatsDataSet.Java
//      DATE            :       23-October-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1074/4394
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Contains a set CufmCurrencyFormatsData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 19-Sep-03 | M.Brister     | Added                      | PpacLon#43/384
//           |               | getCurrencySetAsVector.    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains currency formats selected from the CUFM_CURRENCY_FORMATS table. */
public class CufmCurrencyFormatsDataSet extends DataSetObject implements Serializable
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Initial size of currency formats vector. */
    public static final int C_CURRENCY_V_INITIAL_SIZE = 20;
    
    /** Increment size of currency formats vector. */
    public static final int C_CURRENCY_V_GROW_SIZE = 10;
    
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CufmCurrencyFormatsDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** A Set of CufmCurrencyFormatsData objects. */
    private Vector i_cufmCurrencyFormatsDataSetV;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize   Initial number of currencies.
     * @param p_vGrowSize Number of currencies by which to grow the underlying data set.
     */
    
    public CufmCurrencyFormatsDataSet(PpasRequest p_request,
                                      int         p_initialVSize,
                                      int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_cufmCurrencyFormatsDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    /** 
     * Creates a currency formats data set from a currency formats data set vector.
     * 
     * @param p_request                      The request to process.
     * @param p_cufmCurrencyFormatsDataSetV  Currency formats data vector.
     */
    
    public CufmCurrencyFormatsDataSet(PpasRequest p_request,
                                      Vector      p_cufmCurrencyFormatsDataSetV)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, 
                            C_CLASS_NAME, 
                            10020, 
                            this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_cufmCurrencyFormatsDataSetV = p_cufmCurrencyFormatsDataSetV;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, 
                            C_CLASS_NAME, 
                            10030, 
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Overridden superclass methods.
    //------------------------------------------------------------------------
    /** 
     * Returns the contents of the CufmCurrencyFormatsDataSet object in 
     * a String.
     * 
     * @return String representtaion of the currency format data set.
     */
    public String toString()
    {
        int           l_loop;
        StringBuffer  l_stringBuffer = 
                           new StringBuffer("CufmCurrencyFormatsDataSet=\n[");

        for(l_loop = 0; l_loop < i_cufmCurrencyFormatsDataSetV.size(); l_loop++)
        {
            l_stringBuffer.append((i_cufmCurrencyFormatsDataSetV
                                       .elementAt(l_loop)).toString());
            if ((l_loop + 1) < i_cufmCurrencyFormatsDataSetV.size())
            {
                l_stringBuffer.append("\n");
            }
        }
        l_stringBuffer.append("]");

        return(l_stringBuffer.toString());
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------
    /** 
     * Returns the precision for the currency passed in. A value of -1 is 
     * returned if the currency has not been configured.
     *
     * @param p_currency The currency.
     * @return The precision of the specified currency.
     */    
    public int getPrecision(String p_currency)
    {
        boolean                 l_notFound = true;
        int                     l_precision = -1;
        CufmCurrencyFormatsData l_data;

        for (int l_loop = 0; 
             (l_loop < i_cufmCurrencyFormatsDataSetV.size() && l_notFound);
             l_loop++)
        {
            l_data = (CufmCurrencyFormatsData)
                          i_cufmCurrencyFormatsDataSetV.elementAt(l_loop);

            if (l_data.getCurrencyCode().equals(p_currency))
            {
                l_precision = l_data.getPrecision();
                l_notFound = false;
            }
        }

        return l_precision;
    }

    /** 
     * Returns a data record for the currency passed in. A value of <code>null</code> is 
     * returned if the currency has not been configured.
     *
     * @param p_currencyCode The currency.
     * @return The currency format data for the specified currency.
     */    
    public CufmCurrencyFormatsData getCurrencyFormatData(String p_currencyCode)
    {
        CufmCurrencyFormatsData l_data = null;

        for (int l_loop = 0; l_loop < i_cufmCurrencyFormatsDataSetV.size() && l_data == null; l_loop++)
        {
            if (((CufmCurrencyFormatsData)i_cufmCurrencyFormatsDataSetV.elementAt(l_loop)).getCurrencyCode()
                     .equals(p_currencyCode))
            {
                l_data = (CufmCurrencyFormatsData)i_cufmCurrencyFormatsDataSetV.elementAt(l_loop);
            }
        }

        return l_data;
    }

    /** Get the <code>PpasCurrency</code> object that defines this currency.
     * This is a convenience method to simplify general access.
     * @param p_currencyCode String defining the code of a valid currency.
     * @return Currency object.
     */
    public PpasCurrency getCurrency(String p_currencyCode)
    {
        PpasCurrency l_currency = null;
        CufmCurrencyFormatsData l_cufm = getCurrencyFormatData(p_currencyCode);

        if (l_cufm != null)
        {
            l_currency = l_cufm.getCurrency();
        }
        
        return l_currency;
    }
    
    /** 
     * Adds an element to the vector.
     *
     * @param  p_currencyFormat The misc code object to be added.
     */
    public void addCurrencyFormat(CufmCurrencyFormatsData p_currencyFormat)
    {
        i_cufmCurrencyFormatsDataSetV.addElement(p_currencyFormat);
    }
    
    /**
     * Returns a vector of all defined currency format records.
     * 
     * @return Vector containing all defined currency records.
     */
    public Vector getCurrencySetAsVector()
    {
        return i_cufmCurrencyFormatsDataSetV;
    }

    /**
     * Returns a vector of available currency format records.  Those marked
     * as deleted are not included.
     * 
     * @return Vector containing all available currency records.
     */
    public Vector getAvailable()
    {
        CufmCurrencyFormatsData l_currencyData = null;
        Vector                  l_availableCurrencyFormatsV = new Vector(C_CURRENCY_V_INITIAL_SIZE,
                                                                         C_CURRENCY_V_GROW_SIZE);
                                                                         
        for(int i = 0; i < i_cufmCurrencyFormatsDataSetV.size(); i++)
        {
            l_currencyData = (CufmCurrencyFormatsData)i_cufmCurrencyFormatsDataSetV.get(i);
            
            if (!l_currencyData.isDeleted())
            {
                l_availableCurrencyFormatsV.addElement(l_currencyData);
            }
        }
        return l_availableCurrencyFormatsV;
    }
    
    /**
     * Returns a String array of available currency codes.  Those marked
     * as deleted are not included.
     * 
     * @return String array containing all available currency records.
     */
    public String[] getAvailableCodes()
    {
        CufmCurrencyFormatsData l_currencyData = null;
        ArrayList     l_tempList = null;
        String[]      l_availableCurrencyFormats = new String[0];
        
        l_tempList = new ArrayList();
                                                                         
        for(int i = 0; i < i_cufmCurrencyFormatsDataSetV.size(); i++)
        {
            l_currencyData = (CufmCurrencyFormatsData)i_cufmCurrencyFormatsDataSetV.get(i);
            
            if (!l_currencyData.isDeleted())
            {
                l_tempList.add((l_currencyData.getCurrencyCode()));
            }
        }
        return (String[])l_tempList.toArray(l_availableCurrencyFormats);
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_cufmCurrencyFormatsDataSetV, p_sb);
    }

}
