////////////////////////////////////////////////////////////////////////////////
//        ASCS IPR ID   :     9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      ValaValidLanguageData.Java
//      DATE            :      17-Feb-2001
//      AUTHOR          :      Marek Vonka
//
//      DESCRIPTION     :      A valid language (vala_valid_language).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |       
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group plc 2000 
//
// The copyright in this work belongs to Sema Group plc. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group plc
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group plc. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasDateTime;

/**
 * An object that represents a single valid language (vala_valid_language).
 */
public class ValaValidLanguageData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ValaValidLanguageData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Valid language code. */
    private  String            i_valaCode = null;

    /** Valid language description. */
    private  String            i_valaDescription = null;

    /** Valid language delete flag. */
    private  String            i_valaDeleteFlag = null;

    /** Valid announcement language number. */
    private  int               i_valaAnnouncementLanguageNumber = 0;

    /** IVR syntax. */
    private String  i_valaSyntax = null;
    
    /** The operator identifier of the operator that either
     * created the row or performed the last update. 
     */
    private String i_valaOpid = null;
    
    /** The date and time of the last modification to this row of the table. */
    private PpasDateTime i_valaGenYmdhms;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid language object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_valaCode The identifier for this language.
     * @param p_valaDescription The description/name of this language.
     * @param p_valaSyntax The syntax of this language.
     * @param p_valaAnnouncementLanguageNumber The number of this language when playing announcements.
     * @param p_valaDeleteFlag Flag indicating whether this data is marked as deleted.
     * @param p_valaOpid    The operator that last changed this data.
     * @param p_valaGenYmdhms The date/time this data was last changed.
     */
    public ValaValidLanguageData(
        PpasRequest            p_request,
        String                 p_valaCode,
        String                 p_valaDescription,
        String                 p_valaSyntax,
        int                    p_valaAnnouncementLanguageNumber,
        String                 p_valaDeleteFlag,
        String                 p_valaOpid,
        PpasDateTime           p_valaGenYmdhms)
    {

        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing ValaValidLanguageData(...)");
        }

        i_valaCode                       = p_valaCode;
        i_valaDescription                = p_valaDescription;
        i_valaDeleteFlag                 = p_valaDeleteFlag;
        i_valaAnnouncementLanguageNumber = p_valaAnnouncementLanguageNumber; 

        i_valaSyntax                     = p_valaSyntax;
        i_valaOpid                       = p_valaOpid;
        i_valaGenYmdhms                  = p_valaGenYmdhms;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed ValaValidLanguageData(...)");
        }
    } // End of public constructor
      //         ValaValidLanguageData(PpasRequest, long, Logger, ...)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the valid language code.
     * 
     * @return The identifier of this language.
     */
    public String getCode()
    {
        return(i_valaCode);
    }

    /** Returns the valid language description.
     * 
     * @return The name of this language.
     */
    public String getDescription()
    {
        return(i_valaDescription);
    }
    
    /** Returns the valid announcement language number.
     * 
     * @return The number of this language for use when playing announcements.
     */
    public int getValaAnnouncementLanguageNumber()
    {
       return(i_valaAnnouncementLanguageNumber);
    }

    /** Returns true if valid language has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if ("*".equals(i_valaDeleteFlag))
        {
            l_deleted = true;
        }

        return(l_deleted);
    }
    
    /** Returns IVR syntax.
     * 
     * @return The language syntax.
     */
    public String getValaSyntax()
    {
        return (i_valaSyntax);
    }
    
    /** Returns Operator Identifier.
     * 
     * @return The operator who last changed this data.
     */
    public String getValaOpid()
    {
        return (i_valaOpid);
    }
    
    /** Return Date and time of last modification to the row in the table.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getValaGenYmdhms()
    {
        return (i_valaGenYmdhms);
    }
} // End of public class ValaValidLanguageData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////