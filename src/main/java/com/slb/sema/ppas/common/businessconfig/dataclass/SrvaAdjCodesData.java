////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAdjCodesData.Java
//      DATE            :       15-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A record of adjustment codes data 
//                              (corresponding to a single row of the
//                              srva_adj_codes table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/08/03 | Remi       | Remove i_adjBillDesc and        | CR#15/440
//          | Isaacs     | i_journalClass and the          |
//          |            | corresponding get methods to    |
//          |            | reflect PRD_ASCS00_SYD_AS_003   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a single record of adjustment codes data (i.e.
 * a single row of the srva_adj_codes table).
 */
public class SrvaAdjCodesData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAdjCodesData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The PpasRequest.
     */
    private  PpasRequest       i_request = null;

    /** The Logger to be used to log exceptions generated within this class.
     */
    private  Logger            i_logger = null;

    /** The market that this adjustment code record belongs to.
     */
    private  Market            i_market = null;

    /** An adjustment code. */
    private  String            i_adjCode = null;

    /** An adjustment code type. */
    private  String            i_adjType = null;

    /** Description of this adjustment code. */
    private  String            i_adjDesc = null;

    /** Date the adjustment code record was created in the database. */
    private  PpasDateTime      i_genYmdhms = null;

    /** Operator Id who created the adjustment code record in the database. */
    private  String            i_opid = null;
    
    /** Flag indicating if this adjustment code is deleted. */
    private  char              i_delFlag = ' ';


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new adjustment data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger object to allow routing of messages.
     * @param p_market  The market to which this data is associated.
     * @param p_adjType Type of adjustment.
     * @param p_adjCode Adjustment code/identifier.
     * @param p_adjDesc Description of adjustment.
     * @param p_genYmdhms The date/time this data was last changed.
     * @param p_opid    The operator that last changed this data.
     * @param p_delFlag Flag indicating whether this data is marked as deleted.
     */
    public SrvaAdjCodesData(
        PpasRequest            p_request,
        Logger                 p_logger,
        Market                 p_market,
        String                 p_adjType,
        String                 p_adjCode,
        String                 p_adjDesc,
        PpasDateTime           p_genYmdhms,
        String                 p_opid,
        char                   p_delFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_request      = p_request;
        i_logger       = p_logger;
        i_market       = p_market;
        i_adjType      = p_adjType;
        i_adjCode      = p_adjCode;
        i_adjDesc      = p_adjDesc;
        i_genYmdhms    = p_genYmdhms;
        i_opid         = p_opid;
        i_delFlag      = p_delFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the request.
     * 
     * @return The request.
     */
    public PpasRequest getRequest ()
    {
        return (i_request);
    }

    /** Get the logger.
     * 
     * @return The logger.
     */
    public Logger getLogger ()
    {
        return (i_logger);
    }

    /** Get the market that this adjustment code record belongs to.
     * 
     * @return The market to which this data is associated.
     */
    public Market getMarket ()
    {
        return (i_market);
    }

    /** Get the adjustment code.
     * 
     * @return The identifier of the adjustment.
     */
    public String getAdjCode ()
    {
        return (i_adjCode);
    }

    /** Get the adjustment code type.
     * 
     * @return The type of adjustment.
     */
    public String getAdjType ()
    {
        return (i_adjType);
    }

    /** Get description of this adjustment code.
     * 
     * @return The description of the adjustment.
     */
    public String getAdjDesc ()
    {
        return (i_adjDesc);
    }

    /** Get date the adjustment code record was created in the database.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getGenYmdhms ()
    {
        return (i_genYmdhms);
    }

    /** Get operator Id who created the adjustment code record in the database.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid ()
    {
        return (i_opid);
    }

    /** Returns true if this adjustment code has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if (i_delFlag == '*')
        {
            l_deleted = true;
        }

        return (l_deleted);
    }
    
    /** 
     * Returns explicit instance of Misc Code data
     * @param p_market The customer's market.
     * 
     * @return MiscCodeData Object
     */
    public SrvaAdjCodesData getExplicInstance(Market p_market)
    {
        return new SrvaAdjCodesData(null,
                                    null,
                                    p_market,
                                    i_adjType,
                                    i_adjCode,
                                    i_adjDesc,
                                    null,
                                    null,
                                    i_delFlag);
    }
    
    /** Check whether the specified object is the same as this object.
     * 
     * @param p_obj Object to be compared with this object.
     * @return True if the objects are the same.
     */
    public boolean equals(Object p_obj)
    {
        if (p_obj instanceof SrvaAdjCodesData)
        {
            SrvaAdjCodesData l_sAC = (SrvaAdjCodesData)p_obj;
            
            if (!this.getMarket().equals(l_sAC.getMarket()))
            {
                return false;
            }
            else if (!this.getAdjCode().equals(l_sAC.getAdjCode()))
            {
                return false;
            }
            else if (!this.getAdjType().equals(l_sAC.getAdjType()))
            {
                return false;
            }
            else if (!this.getAdjDesc().equals(l_sAC.getAdjDesc()))
            {
                return false;
            }
            else if (!this.getGenYmdhms().equals(l_sAC.getGenYmdhms()))
            {
                return false;
            }
            else if (!this.getOpid().equals(l_sAC.getOpid()))
            {
                return false;
            }
            else if (this.isDeleted() != l_sAC.isDeleted())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        if (this == p_obj)
        {
            return true;
        }

        return false;
    }

} // End of public class SrvaAdjCodesData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
