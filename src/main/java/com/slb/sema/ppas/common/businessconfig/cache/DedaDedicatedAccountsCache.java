////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DedaDedicatedAccountsCache.Java
//      DATE            :       22-Aug-2002
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon#1484/6141
//                              PpaLon#1522/6421
//                              PRD_PPAK00_GEN_CA_372
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for dedicated accounts
//                              data held in the deda_dedicated_accounts table
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.cache;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.DedaDedicatedAccountsSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * A cache for the dedicated accounts business configuration data
 * (from the deda_dedicated_accounts table).
 */
public class DedaDedicatedAccountsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DedaDedicatedAccountsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The SQL service to use to load cache. */
    private DedaDedicatedAccountsSqlService i_dedaSqlService = null;

    /** Data set containing all dedicated accounts data. */
    private DedaDedicatedAccountsDataSet i_allDedaDataSet = null;

    /**
     * Vector of caches, each element of the Vector containing a cache of
     * available dedicated accounts for the same market.
     */
    private Vector  i_dedaByMarket = null;

    /**
     * Vector of caches, each element of the Vector containing a cache of
     * available dedicated accounts for the same market and service class.
     */
    private Vector  i_dedaByMarketServClass = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new dedicated Accounts business configuration cache object.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public DedaDedicatedAccountsCache(PpasRequest    p_request,
                                      Logger         p_logger,
                                      PpasProperties p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_dedaSqlService = new DedaDedicatedAccountsSqlService (p_request,
                                                                p_logger);

        i_dedaByMarket = new Vector (10, 5);
        i_dedaByMarketServClass = new Vector (5, 5);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all dedicated accounts data.
     * @return Data set containing all dedicated account records in this cache.
     */
    public DedaDedicatedAccountsDataSet getAll()
    {
        return(i_allDedaDataSet);

    } // End of public method getAll

    /** Get all dedicated accounts for a given market.
     * @param p_request The request being processed.
     * @param p_market  The market for which dedicated accounts are needed.
     * Returns dedicated accounts configured for a given market.
     * @return Data set containing dedicated accounts configured for a given
     * market.
     */
    public DedaDedicatedAccountsDataSet getAll (PpasRequest p_request,
                                                Market      p_market)
    {
        DedaDedicatedAccountsDataSet l_dedaDataSet = null;
        DedaDedicatedAccountsDataSet l_returnDataSet = null;
        int                          l_index;

        if (i_allDedaDataSet != null)
        {
            // Check to see if we already have a cached set of dedicated
            // account data for the given market
            for (l_index = 0; l_index < i_dedaByMarket.size() && l_returnDataSet == null; l_index++)
            {
                l_dedaDataSet = (DedaDedicatedAccountsDataSet)
                                      (i_dedaByMarket.elementAt(l_index));

                if ( ((l_dedaDataSet.getDedaArray())[0]).getDedaMarket().equals(p_market) )
                {
                    l_returnDataSet = l_dedaDataSet;
                }
            }

            if (l_returnDataSet == null)
            {
                l_returnDataSet = new DedaDedicatedAccountsDataSet
                    (p_request, i_allDedaDataSet.getDedaArray (p_market));

                // If the data set has some data, then add it the the Vector
                // of cached data stored against market
                if (l_returnDataSet.getDedaArray().length > 0)
                {
                    i_dedaByMarket.add (l_returnDataSet);
                }
            }
        }

        return (l_returnDataSet);

    } // End of public method getAll

    /** Get all dedicated accounts for a given market and service class.
     * @param p_request The request being processed.
     * @param p_market  The market for which dedicated accounts are needed.
     * @param p_serviceClass The service class for which dedicated accounts are needed.
     * Returns dedicated accounts configured for a given market.
     * @return Data set containing dedicated accounts configured for a given
     * market and service class.
     */
    public DedaDedicatedAccountsDataSet getAll (PpasRequest  p_request,
                                                Market       p_market,
                                                ServiceClass p_serviceClass)
    {
        DedaDedicatedAccountsDataSet l_dedaDataSet = null;
        DedaDedicatedAccountsDataSet l_returnDataSet = null;
        int                          l_index;

        if (i_allDedaDataSet != null)
        {
            // Check to see if we already have a cached set of dedicated
            // account data for the given market and service class
            for (l_index = 0;
                 l_index < i_dedaByMarketServClass.size() && l_returnDataSet == null;
                 l_index++)
            {
                l_dedaDataSet = (DedaDedicatedAccountsDataSet)
                                      (i_dedaByMarketServClass.elementAt(l_index));

                if ( ((l_dedaDataSet.getDedaArray())[0]).getDedaMarket().equals(p_market) &&
                     ((l_dedaDataSet.getDedaArray())[0]).getDedaServiceClass().equals(p_serviceClass))
                {
                    l_returnDataSet = l_dedaDataSet;
                }
            }

            if (l_returnDataSet == null)
            {
                l_returnDataSet = new DedaDedicatedAccountsDataSet
                    (p_request, i_allDedaDataSet.getDedaArray (p_market, p_serviceClass));

                // If the data set has some data, then add it the the Vector
                // of cached data stored against market
                if (l_returnDataSet.getDedaArray().length > 0)
                {
                    i_dedaByMarketServClass.add (l_returnDataSet);
                }
            }
        }

        return (l_returnDataSet);

    } // End of public method getAll

    /**
     * Takes parameters service area, location, class and dedicated account Id
     * and returns a string containing dedicated account Id description.
     * @param p_market  Target market.
     * @param p_class   Target customer class.
     * @param p_accountId Dedicated account Id
     * @return Description of the market.
     */
    public String getDescription (Market  p_market,
                                  ServiceClass  p_class,
                                  int     p_accountId)
    {
        String l_dedaDescription = null;

        if (i_allDedaDataSet != null)
        {
            l_dedaDescription = i_allDedaDataSet.getDescription(p_market,
                                                                p_class,
                                                                p_accountId);
        }

        return (l_dedaDescription);

    } // End of public method getDescription


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the cache's data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10020, this,
                "Entered " + C_METHOD_reload);
        }

        i_allDedaDataSet = i_dedaSqlService.read(p_request,
                                                 p_connection);

        i_dedaByMarket = new Vector (10, 5);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10030, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allDedaDataSet);
    }
}

