////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GopaGuiOperatorAccessDataSet.Java
//      DATE            :       29-January-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1211/4922
//                              PRD_PPAK00_GEN_CA_318
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Contains the data selected from the 
//                              GOPA_GUI_OPERATOR_ACCESS table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains GopaGuiOperatorAccessData objects. */
public class GopaGuiOperatorAccessDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "GopaGuiOperatorAccessDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** A set of GopaGuiOperatorAccessCodeData objects. */
    private Vector   i_gopaGuiOperatorAccessDataSetV;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     */
    public GopaGuiOperatorAccessDataSet(PpasRequest p_request,
                                        int         p_initialVSize,
                                        int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_gopaGuiOperatorAccessDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Overridden superclass methods.
    //------------------------------------------------------------------------
    /** Returns the contents of the this object in a String.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        StringBuffer  l_stringBuffer = 
                          new StringBuffer("GopaGuiOperatorAccessDataSet=\n[");
        int           l_loop;

        for(l_loop = 0; l_loop < i_gopaGuiOperatorAccessDataSetV.size(); l_loop++)
        {
            l_stringBuffer.append((i_gopaGuiOperatorAccessDataSetV
                                   .elementAt(l_loop)).toString());
            if ((l_loop + 1) < i_gopaGuiOperatorAccessDataSetV.size())
            {
                l_stringBuffer.append("\n");
            }
        }
        l_stringBuffer.append("]");

        return(l_stringBuffer.toString());
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------
    /** 
     * Returns an Operator GUI access profile object.
     *
     * @param p_privilegeId The privilege level of the operator.
     * @return Operator access flags for the specified privilege level.
     */
    public GopaGuiOperatorAccessData getGuiOperatorAccessProfile(int p_privilegeId)
    {
        boolean                   l_notFound = true;
        GopaGuiOperatorAccessData l_data = null;
        GopaGuiOperatorAccessData l_guiOperatorAccessProfile = null;

        for (int l_loop = 0; 
             (l_loop < i_gopaGuiOperatorAccessDataSetV.size() && l_notFound);
             l_loop++)
        {
            l_data = (GopaGuiOperatorAccessData)
                         i_gopaGuiOperatorAccessDataSetV.elementAt(l_loop);

            if (l_data.getPrivilegeId() == p_privilegeId)
            {
                l_guiOperatorAccessProfile = l_data;
                l_notFound = false;
            }
        }

        return l_guiOperatorAccessProfile;
    }

    /**
     * Returns an array of the Privilege Ids.
     * @return An array of Strings containing the definedPrivilege Ids
     */    
    public String[] getPrivilegeIds()
    {
        ArrayList                 l_tempList = new ArrayList();
        int                       l_loop;
        int                       l_count = 0;
        GopaGuiOperatorAccessData l_data = null;
        String[]                  l_privilegeIds = new String[0];

        for (l_loop = 0; l_loop < i_gopaGuiOperatorAccessDataSetV.size(); l_loop++)
        {
            l_data = (GopaGuiOperatorAccessData)
                         i_gopaGuiOperatorAccessDataSetV.elementAt(l_loop);
                         
            // Language not deleted, add to temporary ArrayList.
            l_tempList.add(String.valueOf(l_data.getPrivilegeId()));
            l_count++;
        }

        return (String[])l_tempList.toArray(l_privilegeIds);

    } // End of public method getPrivilegeIds()

    /** 
     * Adds an element to the data set.
     *
     * @param  p_gopaGuiOperatorAccessData The data object to be added.
     */
    public void addElement(GopaGuiOperatorAccessData p_gopaGuiOperatorAccessData)
    {
        i_gopaGuiOperatorAccessDataSetV.addElement(p_gopaGuiOperatorAccessData);
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_gopaGuiOperatorAccessDataSetV, p_sb);
    }

}
