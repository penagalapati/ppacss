////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CochCommunityChargingSqlService.java
//      DATE            :      11-Jul-2004
//      AUTHOR          :      Julien CHENELAT
//      REFERENCE       :
//
//      COPYRIGHT       :      WM-data 2005
//
//      DESCRIPTION     :      Sql Service to Retrieve information from
//                             COCH_COMMUNITY_CHARGING table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                     | REFERENCE
//----------+---------------+---------------------------------+--------------------
// dd/mm/yy |               |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Sql Service to Retrieve information from COCH_COMMUNITY_CHARGING table.
 */
public class CochCommunityChargingSqlService
{

    //------------------------------------------------------------------------
    // Class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CochCommunityChargingSqlService";


    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** Logger. */
    protected Logger           i_logger = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a CochCommunityChargingSqlService.
     * 
     * @param p_request     The request to process.
     * @param p_logger      Logger.
     */
    public CochCommunityChargingSqlService( PpasRequest            p_request,
                                            Logger                 p_logger )
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_START,
                             p_request, 
                             C_CLASS_NAME, 
                             10110, 
                             this,
                             "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_END,
                             p_request, 
                             C_CLASS_NAME, 
                             10150, 
                             this,
                             "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Read the coch_community_charging set from the database.
     * 
     * @param p_request         The request to process.
     * @param p_connection      Jdbc Connection.
     * @return  A set of community charging objects.
     * @throws PpasSqlException     General Exception. No special Keys are anticipated.
     */
    public CochCommunityChargingDataSet readAll( PpasRequest            p_request,
                                                 JdbcConnection         p_connection )
        throws PpasSqlException
    {
        int                             l_cochCommunityChgId            = 0;
        String                          l_cochCommunityChgDesc          = null;
        char                            l_cochCommunityChgDelFlag       = ' ';
        JdbcStatement                   l_statement                     = null;
        SqlString                       l_sql                           = null;
        JdbcResultSet                   l_resultSet                     = null;
        CochCommunityChargingData       l_cochCommunityChgData          = null;
        Vector                          l_cochCommunityChgDataSetV      = null;
        CochCommunityChargingDataSet    l_cochCommunityChgDataSet       = null;
        CochCommunityChargingData       l_cochCommunityChgEmptyARR[]    = new CochCommunityChargingData[0];
        CochCommunityChargingData       l_cochCommunityChgARR[];
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_MODERATE,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START, 
                             C_CLASS_NAME, 
                             95020, 
                             this,
                             "Entering " + C_METHOD_readAll);
        }

        l_cochCommunityChgDataSetV = new Vector(20, 10);

        l_sql = new SqlString(1000, 0, "SELECT coch_community_id," +
                                       " coch_community_descr,"    +
                                       " coch_community_del_flag," +
                                       " coch_opid," +
                                       " coch_gen_ymdhms" +
                                       " FROM coch_community_charging" +
                                       " ORDER BY coch_community_id");

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 95030, this, p_request);

        l_resultSet = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 95040, this, p_request, l_sql);

        while (l_resultSet.next(95050))
        {
            l_cochCommunityChgId        = l_resultSet.getInt        ( 95060, "coch_community_id" );
            l_cochCommunityChgDesc      = l_resultSet.getTrimString ( 95070, "coch_community_descr" );
            l_cochCommunityChgDelFlag   = l_resultSet.getChar       ( 95080, "coch_community_del_flag");

            l_cochCommunityChgData 
                = new CochCommunityChargingData( p_request,
                                                 l_cochCommunityChgId,
                                                 l_cochCommunityChgDesc,
                                                 l_cochCommunityChgDelFlag,
                                                 l_resultSet.getString(95084, "coch_opid"),
                                                 l_resultSet.getDateTime(95086, "coch_gen_ymdhms"));

            l_cochCommunityChgDataSetV.add(l_cochCommunityChgData);

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VHIGH,
                                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_SQL, 
                                 C_CLASS_NAME, 
                                 95090, 
                                 this,
                                 l_cochCommunityChgData.toString() );
            }
        }

        l_resultSet.close(95100);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 95110, this, null);

        l_cochCommunityChgARR =
                (CochCommunityChargingData[])l_cochCommunityChgDataSetV.toArray(l_cochCommunityChgEmptyARR);

        l_cochCommunityChgDataSet = new CochCommunityChargingDataSet( p_request,
                                                                      i_logger,
                                                                      l_cochCommunityChgARR );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_MODERATE,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END, 
                             C_CLASS_NAME, 
                             95120, 
                             this,
                             "Leaving " + C_METHOD_readAll);
        }

        return(l_cochCommunityChgDataSet);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a community charging record into the coch_community_charging database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_communityChgId Community charging identifier.
     * @param p_communityChgDesc Community charging description.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       int            p_communityChgId,
                       String         p_communityChgDesc,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10000, 
                this,
                "Entered " + C_METHOD_insert);
        }

        l_sql = "INSERT INTO coch_community_charging (" +
                    "coch_community_id, " +
                    "coch_community_descr, " +
                    "coch_community_del_flag, " +
                    "coch_opid, " +
                    "coch_gen_ymdhms ) " +
                "VALUES(" + 
                    "{0},{1},{2},{3},{4})";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setIntParam     (0, p_communityChgId);
        l_sqlString.setStringParam  (1, p_communityChgDesc);
        l_sqlString.setCharParam    (2, ' ');
        l_sqlString.setStringParam  (3, p_userOpid);
        l_sqlString.setDateTimeParam(4, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10010,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10020,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 10030, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10040, 
                    this,
                    "Database error: unable to insert row in coch_community_charging table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_insert,
                                               10050,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10060, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    }
   
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates a community charging record in the coch_community_charging database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_communityChgId Community charging identifier.
     * @param p_communityChgDesc Community charging description.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       int            p_communityChgId,
                       String         p_communityChgDesc,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10100, 
                this,
                "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE coch_community_charging " +
                "SET coch_community_descr = {0}, " +
                    "coch_community_del_flag = {1}, " +
                    "coch_opid = {3}, " +
                    "coch_gen_ymdhms = {4} " +
                "WHERE coch_community_id = {2}";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_communityChgDesc);
        l_sqlString.setCharParam    (1, ' ');
        l_sqlString.setIntParam     (2, p_communityChgId);
        l_sqlString.setStringParam  (3, p_userOpid);
        l_sqlString.setDateTimeParam(4, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       10110,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       10120,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 10130, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10140, 
                    this,
                    "Database error: unable to update row in coch_community_charging table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_update,
                                               10150,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10160, 
                this,
                "Leaving " + C_METHOD_update);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the coch_community_charging database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_communityChgId Community charging identifier.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       int            p_communityChgId,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        PpasDateTime     l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10200, 
                this,
                "Entered " + C_METHOD_delete);
        }

        l_sql = "UPDATE coch_community_charging " +
                "SET coch_community_del_flag = {0}, " +
                    "coch_opid = {2}, " +
                    "coch_gen_ymdhms = {3} " +
                "WHERE coch_community_id = {1}";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setCharParam    (0, '*');
        l_sqlString.setIntParam     (1, p_communityChgId);
        l_sqlString.setStringParam  (2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10210,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10220,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 10230, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10240, 
                    this,
                    "Database error: unable to mark row as deleted in coch_community_charging table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_delete,
                                               10250,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10260, 
                this,
                "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn community charging record as available in the 
     * coch_community_charging database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_communityChgId Community charging identifier.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                int            p_communityChgId,
                                String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10300, 
                this,
                "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE coch_community_charging " +
                "SET coch_community_del_flag = {0}, " +
                    "coch_opid = {2}, " +
                    "coch_gen_ymdhms = {3} " +
                "WHERE coch_community_id = {1}";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setCharParam (0, ' ');
        l_sqlString.setIntParam  (1, p_communityChgId);
        l_sqlString.setStringParam  (2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10340, 
                    this,
                    "Database error: unable to update row in coch_community_charging table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10360, 
                this,
                "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}
