////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafTrafficCaseService.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8240, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Traffic Case Sql Service, retrieves data for the business cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseData;
import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Implements a service that performs the SQL required by the traffic case business config cache.
 */
public class TrafTrafficCaseSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TrafTrafficCaseSqlService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The Logger to be used to log exceptions generated by this object.
     */
    private Logger        i_logger;

    /**
     * Context containing useful items like pools and configuration data.
     */
    protected PpasContext i_ppasContext = null;

    /**
     * Empty static array.
     */
    private static TrafTrafficCaseData i_trafEmptyARR[] = new TrafTrafficCaseData[0];

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new Sql Service for populating the traffic case business
     * configuration cache.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_ppasContext The context containing business and system configuration
     *         data required by this service.
     */
    public TrafTrafficCaseSqlService(PpasRequest  p_request,
                                      Logger       p_logger,
                                      PpasContext  p_ppasContext)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            31310,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;

        i_ppasContext = p_ppasContext;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            31390,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all traffic cases data.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set containing all configured traffic case from the
     *         traf_traffic_case table.
     * @throws PpasSqlException If the data cannot be read.
     */
    public TrafTrafficCaseDataSet readAll(PpasRequest            p_request,
                                          JdbcConnection         p_connection)
        throws PpasSqlException
                
    {
        JdbcStatement           l_statement;
        SqlString               l_sql;
        TrafTrafficCaseData     l_trafRecord;
        Vector                  l_trafV = new Vector (50, 50);
        TrafTrafficCaseData     l_trafARR[];
        JdbcResultSet           l_results;
        TrafTrafficCaseDataSet  l_trafDataSet;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            31410,
                            this,
                            "Entered " +  C_METHOD_readAll);
        }
        
        l_sql = new SqlString(200, 0,
                              "SELECT * FROM traf_traffic_case " +
                              "ORDER BY traf_traffic_case");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 31420, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 31430, this, p_request, l_sql);

        while ( l_results.next(31440) )
        {
            l_trafRecord = new TrafTrafficCaseData(p_request, 
                                                   l_results.getInt(11445, "traf_traffic_case"), 
                                                   l_results.getString(10300, "traf_desc").trim(),
                                                   l_results.getString(10300, "traf_short_desc").trim(),
                                                   l_results.getChar(11540, "traf_del_flag"),
                                                   l_results.getString(11550, "traf_opid"),
                                                   l_results.getDateTime(11560, "traf_gen_ymdhms"));

            l_trafV.addElement (l_trafRecord);
        }

        l_results.close (31550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 31560, this, p_request);

        l_trafARR = (TrafTrafficCaseData [])l_trafV.toArray (i_trafEmptyARR);

        l_trafDataSet = new TrafTrafficCaseDataSet(p_request, l_trafARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            31590,
                            this,
                            "Leaving " + C_METHOD_readAll);
        }

        return (l_trafDataSet);

    } // End of public method readAll
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Attempts to insert a new row into the traf_traffic_case database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_trafficCaseId The traffic case id.
     * @param p_trafficCaseDesc The traffic case description. 
     * @param p_trafficCaseShortDesc The traffic case short description.  
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest            p_request,
                      JdbcConnection         p_connection,
                      String                 p_opid,
                      int                    p_trafficCaseId,
                      String                 p_trafficCaseDesc,
                      String                 p_trafficCaseShortDesc)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;       
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " +  C_METHOD_insert);
        }
            
        l_sql = "INSERT INTO traf_traffic_case " +
                "(traf_traffic_case," +
                " traf_desc," +
                " traf_short_desc," +
                " traf_del_flag," +
                " traf_opid, " +
                " traf_gen_ymdhms)" +
                "VALUES( {0}, {1}, {2},' ', {3}, {4})";
                       
        l_sqlString = new SqlString(150, 5, l_sql);
           
        l_sqlString.setIntParam(0, p_trafficCaseId);
        l_sqlString.setStringParam(1, p_trafficCaseDesc);
        l_sqlString.setStringParam(2, p_trafficCaseShortDesc);
        l_sqlString.setStringParam(3, p_opid);
        l_sqlString.setDateTimeParam(4, DatePatch.getDateTimeNow());

        try
        {                   
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       16050,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }        
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in traf_traffic_case table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;     
    }    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update row in the traf_traffic_case database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_trafficCaseId The traffic case id.
     * @param p_trafficCaseDesc The traffic case description. 
     * @param p_trafficCaseShortDesc The traffic case short description.
     * @return A count of the number of rows updated
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest            p_request,
                      JdbcConnection         p_connection,
                      String                 p_opid,
                      int                    p_trafficCaseId,
                      String                 p_trafficCaseDesc,
                      String                 p_trafficCaseShortDesc)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " +  C_METHOD_update);
        }
    
        l_sql = new String("UPDATE traf_traffic_case " +
                           "SET traf_desc = {0}, " +
                           "traf_short_desc = {1}, " +
                           "traf_del_flag = ' ', " +
                           "traf_opid = {2}, " +
                           "traf_gen_ymdhms = {3} " +
                           "WHERE traf_traffic_case = {4}");
                       
        l_sqlString = new SqlString(150, 5, l_sql);

        l_sqlString.setStringParam(0, p_trafficCaseDesc);
        l_sqlString.setStringParam(1, p_trafficCaseShortDesc);
        l_sqlString.setStringParam(2, p_opid);
        l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());
        l_sqlString.setIntParam(4, p_trafficCaseId);

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in traf_traffic_case table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;     
    }

    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Mark a row as having been deleted in traf_traffic_case.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_trafficCaseId The traffic case id to be withdrawn.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest     p_request,
                       JdbcConnection  p_connection,
                       String          p_opid,
                       int             p_trafficCaseId)
        throws PpasSqlException
    {
        JdbcStatement    l_statement  = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            32100, 
                            this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE traf_traffic_case " +
                           "SET traf_del_flag= '*', " +
                           "traf_opid = {1}, " +
                           "traf_gen_ymdhms = {2} " +
                           "WHERE traf_traffic_case = {0}");

        l_sqlString = new SqlString(150, 3, l_sql);
            
        l_sqlString.setIntParam(0, p_trafficCaseId);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_delete,
                                                       32300,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }            
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10240, 
                                this,
                                "Database error: unable to mark row as deleted in traf_traffic_case table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_delete,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            32700,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn traffic case record as available in the 
     * traf_traffic_case database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_trafficCaseId The traffic case id.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                int            p_trafficCaseId,
                                String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10300, 
                this,
                "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE traf_traffic_case " +
                "SET traf_del_flag = {0}, " +
                    "traf_opid = {2}, " +
                    "traf_gen_ymdhms = {3} " +
                "WHERE traf_traffic_case = {1}";

        l_sqlString = new SqlString(500, 4, l_sql);
        
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setIntParam     (1, p_trafficCaseId);
        l_sqlString.setStringParam  (2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10340, 
                    this,
                    "Database error: unable to update row in traf_traffic_case table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10360, 
                this,
                "Leaving " + C_METHOD_markAsAvailable);
        }
    }    

}
