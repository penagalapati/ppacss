/*
 * Filename        : AlarmClientLogHandler.java
 * Created on      : 20-Jun-2004
 * Author          : MVonka
 * 
 * Description     :
 * 
 * Copyright       :
 */
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+--------------------
//28/02/07 | R.Grimshaw | Pass instrument manager to      | PpacLon#2964
//         |            | AlarmClient.                    |
//---------+------------+---------------------------------+--------------------
package com.slb.sema.ppas.common.awd.alarmclient;

import java.util.Locale;

import com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarmEvent;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.LogHandler;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.support.ConfigException;
import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.RequestInterface;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 * This...
 */
public class AlarmClientLogHandler extends LogHandler
{
    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmClientLogHandler";
    
    /** Maximum length of an alarm to be sent. */
    public static final int C_MAX_ALARM_LENGTH = 4500;   // Allow space for extras to make 8096;
    
    private static boolean      c_enabled = true;

    private AlarmClient         i_alarmClient;

    /** Name of originating node. */
    private String              i_originatingNodeName;
    
    /** Name of originating process. */
    private String              i_originatingProcessName;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Create the FILE object representing the log file. Call
     * the setConfigFromProperties method of this class to instanciate class
     * variables from the properties provided by <code>p_fromProperties</code>.
     * Calls the createLog method of this class to create the physical log
     * file based on the class instance variable values.
     *
     * @param p_logName  The name to be associated with this object.
     * @param p_fromProperties  Properties defining the attributes of the log
     *                          file represented by this object.
     * @throws ConfigException The config for the LogHandlerFile was missing or invalid.
     */
    public AlarmClientLogHandler(
        String                  p_logName,
        UtilProperties          p_fromProperties)
        throws ConfigException
    {
        super();
        
        if (Debug.on) 
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START | Debug.C_ST_TRACE,
                        C_CLASS_NAME, 10010, this,
                        "Constructing " + C_CLASS_NAME + ": " + p_logName);    
        }
        
        // TODO: sort out node property.
        i_originatingNodeName = System.getProperty("ascs.nodeName", "UndefinedNode");
        i_originatingProcessName = System.getProperty("ascs.procName", "UndefinedProcessName");
        
        // Set up the name of this log file handler.
        setName(p_logName);

        // Initialise the class instance variables based on the given properties.
        setConfigFromProperties (p_fromProperties);

        if (Debug.on) 
        {
            Debug.print(Debug.C_LVL_LOW, Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_END | Debug.C_ST_TRACE,
                        C_CLASS_NAME, 10090, this,
                        "Constructed " + C_CLASS_NAME);    
        }
        
    }  // End of constructor 'LogHandlerFile'

    /**
     * Open the physical object managed by this LogHandler for writing.
     * 
     * @param p_request The request.
     * @return true or false to indicate success or failure respectively.
     */
    public boolean open(RequestInterface p_request)
    {
        return true;
    }

    /**
     * Close the physical object managed by this LogHandler for writing.
     * @param p_request The request.
     *
     */
    public void close (RequestInterface p_request)
    {
        return;
    } 

    /**
     * Log a message to the physical log sink managed by this LogHandler.
     * @param p_request  The request.
     * @param p_logObj   The object to be logged. The object must implement
     *                   LoggableInterface, which defines the interface
     *                   of a method whose implementation generates the
     *                   text to be logged.
     */
    public void logMessage(
        RequestInterface        p_request,   
        LoggableInterface       p_logObj)
    {
        PpasException           l_ppasException;
        AlarmEvent              l_alarmEvent;
        StringBuffer            l_eventTextSB;
        String                  l_exceptionId;
        String                  l_eventId;
        
        if(c_enabled)
        {
            l_eventTextSB = new StringBuffer(C_MAX_ALARM_LENGTH);
            if(p_logObj instanceof PpasException)
            {
                l_ppasException = (PpasException)p_logObj;
                l_exceptionId = l_ppasException.getExceptionId();
                l_eventId = "EXC." + l_exceptionId;

                l_eventTextSB.append(l_ppasException.getFullMessage());
                
                String l_eventText = l_eventTextSB.length() > C_MAX_ALARM_LENGTH ?
                        l_eventTextSB.substring(0, C_MAX_ALARM_LENGTH) :
                            l_eventTextSB.toString();
                        
                if (Debug.on) 
                {
                    Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_MWARE,
                                Debug.C_ST_TRACE,
                                C_CLASS_NAME, 20010, this,
                                "Length of event text is " + l_eventText.length());    
                }                        
                
                l_alarmEvent = new SimpleAlarmEvent(l_eventId,
                                                    l_eventText,
                                                    i_originatingNodeName,
                                                    i_originatingProcessName);
                
                i_alarmClient.sendEvent(l_alarmEvent);
            }
            else
            {
                l_eventId = p_logObj.getEventId();
                l_eventTextSB.append(p_logObj.getMessageText(Locale.getDefault()));
                
                l_alarmEvent = new SimpleAlarmEvent(l_eventId,
                                                    l_eventTextSB.length() > C_MAX_ALARM_LENGTH ?
                                                            l_eventTextSB.substring(0, C_MAX_ALARM_LENGTH) :
                                                                l_eventTextSB.toString(),
                                                    i_originatingNodeName,
                                                    i_originatingProcessName);
                i_alarmClient.sendEvent(l_alarmEvent);
            }
        }
    }

    /**
     * Log a message to the physical log sink managed by this LogHandler.
     * <p>
     * Note that this method should NOT be used when a PpasRequest is 
     * available to the caller; in this case the logMessage method that takes
     * a PpasRequest as its first parameter should be used.
     *
     * @param p_logObj   The object to be logged. The object must implement
     *                   LoggableInterface, which defines the interface
     *                   of a method whose implementation generates the
     *                   text to be logged.
     */
    public void logMessage(
        LoggableInterface       p_logObj
        )
    {
        logMessage((RequestInterface)null, p_logObj);
    }

    /**
     * Sets the enabled state of this class. Allows easy disabling of all
     * Alarm Client Log Handler's in a JVM (it is anticipated this will only
     * be called by Alarm Servers to prevent circular attempts to log (raise) an
     * event which occurs in an Alarm Server.
     * 
     * @param p_newState        new enabled state.
     * @return                  The old enabled state.
     */    
    public static boolean setEnabled(boolean p_newState)
    {
        boolean                 l_oldState;
        
        l_oldState = c_enabled;
        c_enabled = p_newState;
        
        return l_oldState;
    }
        
    /** Sets the InstrumentManager. Overrides the super class - calls the
     * super class implementation then adds the instrument manager to
     * the AlarmClient. 
     * 
     * @param p_instrumentManager The InstrumentManager for this.
     */
    public synchronized void setInstrumentManager(InstrumentManager       p_instrumentManager)
    {
        super.setInstrumentManager(p_instrumentManager);
        i_alarmClient.setInstrumentManager(p_instrumentManager);
    }

    /**
     * Set up the class instance variables from the configuration data
     * provided by <code>p_fromProperties</code>.
     * @param p_fromProperties Configuration data defining the setup for this
     *                         log handler file.
     * @throws ConfigException The log handler file configuration data is missing or invalid.
     */
    private void setConfigFromProperties (UtilProperties   p_fromProperties)
        throws ConfigException
    {
        i_alarmClient = new AlarmClient(i_instrumentManager, p_fromProperties);
    }
}
