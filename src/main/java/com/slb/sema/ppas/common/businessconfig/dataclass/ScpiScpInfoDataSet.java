////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScpiScpInfoDataSet.Java
//      DATE            :       21-Jan-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of SCP information data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/08/03 | Olivier    | Added public method:            | PpacLon#273 / 
//          | Duparc     | getRecord()                     | CR PpacLon#33
//----------+------------+---------------------------------+--------------------
// 16/11/05 | Yang L.    | Copyright info changed.         | PpacLon#1755/7463
//----------+------------+---------------------------------+--------------------
// 16/11/05 | Yang L.    | New method getAllRecord() added | PpacLon#1755/7463
//          |            | in order to return all the      |
//          |            | available fach records, i.e.    |
//          |            | includes those marked as deleted|
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction      | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an SCP information data, each element of
 * the set representing a record from the scpi_scp_info table.
 */
public class ScpiScpInfoDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScpiScpInfoDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of SCP information data records defining the data in this 
     *  data set object.
     */
    private ScpiScpInfoData []     i_scpInfoARR;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of SCP information data.
     * 
     * @param p_request The request to process.
     * @param p_scpInfoARR A set of SDP data objects.
     */
    public ScpiScpInfoDataSet(
        PpasRequest             p_request,
        ScpiScpInfoData []      p_scpInfoARR)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 86510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_scpInfoARR = p_scpInfoARR;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 96590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    //  PpacLon#273: CR PpacLon#33 [Begin]
    /**
     * Returns ScpiScpInfoData record for the given SDP Id.
     * @param p_sdpId SDP Id that the record must have to be returned.
     * @return ScpiScpInfoData record from this DataSet with the given SDP Id. If
     *         no active record can be found with the given SDP Id, then 
     *         <code>null</code> is returned.
     */
    public ScpiScpInfoData getRecord (String p_sdpId)
    {
        ScpiScpInfoData l_scpiScpInfoRecord = null;
        int             l_loop;

        for (l_loop = 0; l_loop < i_scpInfoARR.length; l_loop++)
        {
            if ((!i_scpInfoARR[l_loop].isDeleted()) &&
                (i_scpInfoARR[l_loop].getScpId().equals(p_sdpId)))
            {
                l_scpiScpInfoRecord = i_scpInfoARR[l_loop];
                break;
            }
        }

        return (l_scpiScpInfoRecord);

    } // End of public method getRecord
    //  PpacLon#273: CR PpacLon#33 [End]
    
    /**
     * Returns the ScpiScpInfoData record for the given Scp Id, even if it�s withdrawn (marked as deleted).
     * @param p_sdpId SDP Id that the record must have to be returned.
     * @return ScpiScpInfoData record for the given Scp Id, even if it�s withdrawn (marked as deleted).
     */
    public ScpiScpInfoData getRecordIncludeDeleted(String p_sdpId)
    {
        ScpiScpInfoData l_scpiScpInfoRecord = null;
        int l_loop;
        
        for (l_loop = 0; l_loop < i_scpInfoARR.length; l_loop++)
        {
            if ((i_scpInfoARR[l_loop].getScpId().equals(p_sdpId)))
            {
                l_scpiScpInfoRecord = i_scpInfoARR[l_loop];
                break;
            }
        }
        
        return (l_scpiScpInfoRecord);
        
    } 

    /**
     * Get information relating to a given SCP/SDP address.
     * @param p_sdpIpAddress Address of the SDP/SCP.
     * @return ScpiScpInfoData Information about the SCP/SDP.
     */
    public ScpiScpInfoData getRecordFromIpAddress(String p_sdpIpAddress) {
        ScpiScpInfoData l_scpiScpInfoRecord = null;
        int             l_loop;

        for (l_loop = 0; l_loop < i_scpInfoARR.length; l_loop++)
        {
            if ((!i_scpInfoARR[l_loop].isDeleted()) &&
                (i_scpInfoARR[l_loop].getScpIpAddress().equals(p_sdpIpAddress)))
            {
                l_scpiScpInfoRecord = i_scpInfoARR[l_loop];
                break;
            }
        }
        return l_scpiScpInfoRecord;
    }

    /**
     * Get an array containing the all available SCP information records.
     * The array does not contain records marked as deleted.
     * @return Array containing the available SCP information records in this
     *         data set (i.e. those not marked as deleted).
     */
    public ScpiScpInfoData [] getAvailableArray()
    {
        ScpiScpInfoData   l_availableARR[];
        Vector            l_availableV = new Vector (10, 10);
        int               l_loop;
        ScpiScpInfoData   l_emptyARR[] = new ScpiScpInfoData[0];

        for (l_loop = 0; l_loop < i_scpInfoARR.length; l_loop++)
        {
            if (!i_scpInfoARR[l_loop].isDeleted())
            {
                l_availableV.addElement (i_scpInfoARR[l_loop]);
            }
        }

        l_availableARR =
              (ScpiScpInfoData[])l_availableV.toArray(l_emptyARR);

        return (l_availableARR);

    } // End of public method getAvailableArray()

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_scpInfoARR, p_sb);
    }
}

