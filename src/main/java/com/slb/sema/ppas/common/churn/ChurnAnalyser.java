////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnAnalyser.java
//      DATE            :       27-Mar-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Analyse Churn Data.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import java.text.ParseException;
import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/** Analyse Churn Data. */
public class ChurnAnalyser extends DataObject
{
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "ChurnAnalyser";
    
    /** Prefix of Churn analysis properties. Value is {@value}. */
    protected static final String C_PROP_PREFIX = "com.slb.sema.ppas.common.CustChurn.";

    /** No conditions for analysis. */
    private static final ChurnCondition[] C_NO_CONDITIONS = new ChurnCondition[0];
    
    /** New line terminator. */
    public static final String C_NL = System.getProperty("line.separator");

    /** Constant for 1/30 of a period (that is, daily). */
    private static final double C_PER_DAY = 1.0/30.0;
    
    /** Business configuration cache. */
    private BusinessConfigCache i_bcCache;
    
    /** Set of conditions for identifying whether an account is likely to churn. */
    private ChurnCondition[] i_conditions;
    
    /** Flag indicating whether conditions should be 'and'ed (true) or 'or'ed (false). */
    private boolean i_andCondition;
    
    /** Flag indicating subscriber snapshot is needed. */
    private boolean i_needSnapshot;
    
    /** Minimum activity period of account. */
    private int i_minActPeriod;
    
    /** Average operator spend per period. */
    private Money i_operAveragePeriod;
    
    /** Average operator spend per Day. */
    private Money i_operAverageDay;
    
    /** Average operator spend per period (High). */
    private Money i_operAveragePeriodHigh;
    
    /** Average operator spend per day (High). */
    private Money i_operAverageDayHigh;
    
    /** Average operator spend per period (Low). */
    private Money i_operAveragePeriodLow;
    
    /** Average operator spend per day (Low). */
    private Money i_operAverageDayLow;
    
    /** Money formatter to parse properties. */
    private static final MoneyFormat C_MONEY = new MoneyFormat("0.p"); 
    
    /** System currency. */
    private PpasCurrency i_systemCcy;

    /** Zero amount - for calculating stating point. */
    private Money i_zeroAmt;
    
    /** Number of months for which analysis is required. */
    private int i_analysisPeriod;
    
    /** Number of months to track as 'current'. */
    private int i_trackingPeriod;
    
    /** Flag indicating whether High/Low averages are configured. */
    private boolean i_highLowAvgConfigured;
    
    /** Average period spend to be in the upper ranking. */
    private Money i_upperRankAmt;
    
    /** Average period spend to be in the lower ranking. */
    private Money i_lowerRankAmt;
    
    /**
     * Standard constructor for creating Churn analysis.
     * 
     * @param p_properties Properties defining the Churn identification criteria.
     * @param p_bcCache    Business Configuration Cache.
     * @throws PpasConfigException If the conditions cannot be read.
     */
    public ChurnAnalyser(PpasProperties p_properties, BusinessConfigCache p_bcCache) throws PpasConfigException
    {
        i_bcCache = p_bcCache;
        
        i_conditions = getConditions(p_properties, p_bcCache);
        
        String l_and = p_properties.getTrimmedProperty(C_PROP_PREFIX + "ruleCombination", "AND");
        
        i_andCondition = l_and.equalsIgnoreCase("AND");
        
        i_minActPeriod   = p_properties.getIntProperty(C_PROP_PREFIX + "minActivityPeriod");
        i_analysisPeriod = p_properties.getIntProperty(C_PROP_PREFIX + "analysisPeriod");
        
        if (i_analysisPeriod == 0)
        {
            // Cannot use zero periods so round to one
            i_analysisPeriod = 1;
        }

        i_trackingPeriod = p_properties.getIntProperty(C_PROP_PREFIX + "trackingPeriod");
        
        if (i_trackingPeriod == 0)
        {
            // Cannot use zero periods so round to one
            i_trackingPeriod = 1;
        }

        i_systemCcy = p_bcCache.getConfCompanyConfigCache().getConfCompanyConfigData().getBaseCurrency();
        
        i_upperRankAmt = parseMoneyProperty(p_properties, C_PROP_PREFIX + "upperRanking", null, "100.00");
        i_lowerRankAmt = parseMoneyProperty(p_properties, C_PROP_PREFIX + "lowerRanking", null, "0");
        i_zeroAmt      = parseMoneyProperty(p_properties, "Zero Amount", "0", "0");  // Use mechanism because it exists
        
        String l_property = C_PROP_PREFIX + "monthlySpendAverage";
        String l_average = p_properties.getTrimmedProperty(l_property, "0");
        String[] l_avg = l_average.split("\\s*,\\s*");
        
        if (l_avg.length >= 3)
        {
            i_highLowAvgConfigured = true;

            i_operAveragePeriodLow  = parseMoneyProperty(p_properties, l_property, l_avg[0], null);
            i_operAveragePeriod     = parseMoneyProperty(p_properties, l_property, l_avg[1], null);
            i_operAveragePeriodHigh = parseMoneyProperty(p_properties, l_property, l_avg[2], null);
        }
        else
        {
            i_highLowAvgConfigured = false;
            
            i_operAveragePeriodLow  = i_zeroAmt;
            i_operAveragePeriod     = parseMoneyProperty(p_properties, l_property, l_avg[0], null);
            i_operAveragePeriodHigh = i_zeroAmt;
        }
        
        i_operAverageDayLow  = i_operAveragePeriodLow.multiply(C_PER_DAY);
        i_operAverageDay     = i_operAveragePeriod.multiply(C_PER_DAY);
        i_operAverageDayHigh = i_operAveragePeriodHigh.multiply(C_PER_DAY);
    }
    
    /** Parse a property to generate a Money amount.
     * 
     * @param p_properties Configuration properties.
     * @param p_property Property name.
     * @param p_value    Property Value - if <code>null</code>, this will be obtained based on the property name.
     * @param p_default  Default property value.
     * @return Amount as a Money object.
     * @throws PpasConfigException If the property is invalid.
     */
    private Money parseMoneyProperty(PpasProperties p_properties, String p_property, String p_value, String p_default)
        throws PpasConfigException
    {
        String l_value = p_value;
        
        if (p_value == null)
        {
            l_value = p_properties.getTrimmedProperty(p_property, p_default);
        }
        
        try
        {
            return C_MONEY.parse(l_value, i_systemCcy);
        }
        catch (ParseException e)
        {
            throw new PpasConfigException(C_CLASS_NAME, C_CLASS_NAME, 10100, this, null, 0,
                                          ConfigKey.get().configNotNumeric("Property: " + p_property + ", Value: " + l_value), e);
        }
    }
    
    /**
     * Get conditions for identifying Churn accounts.
     * 
     * @param p_properties Properties defining the Churn identification criteria.
     * @param p_bcCache    Business Configuration Cache.
     * @return Array of conditional tests.
     * @throws PpasConfigException If the conditions cannot be read.
     */
    private ChurnCondition[] getConditions(PpasProperties p_properties, BusinessConfigCache p_bcCache) throws PpasConfigException
    {
        Vector l_conditionList = p_properties.getListFromProperty(C_PROP_PREFIX + "activeRules", "");
        
        Vector l_conditions = new Vector();
        
        for (int i = 0; i < l_conditionList.size(); i++)
        {
            String l_condition = (String)l_conditionList.get(i);
            
            if (l_condition != null && !l_condition.trim().equals(""))
            {
                l_conditions.add(ChurnCondition.makeChurnCondition(p_properties, p_bcCache, l_condition, this));
            }
        }
        
        return (ChurnCondition[])l_conditions.toArray(C_NO_CONDITIONS);
    }

    /** Analyse a set of data.
     * 
     * @param p_data Data to analyse.
     * @return Status of the analysis.
     */
    public ChurnAnalysis analyse(ChurnIndicatorData p_data)
    {
        StringBuffer l_sb = new StringBuffer();
        
        boolean l_mayChurn = i_andCondition;
        
        // Always perform all tests - we have already incurred the cost of getting data so may as well get a description for all.
        for (int i = 0; i < i_conditions.length; i++)
        {
            boolean l_test = i_conditions[i].matches(p_data, l_sb);
            
            l_sb.append(C_NL);
            
            if (i_andCondition && !l_test)
            {
                // Want all conditions to be true but one is false so set flag.
                l_mayChurn = false;
            }
            else if (!i_andCondition && l_test)
            {
                // Want any condition to be true and one is true so set flag.
                l_mayChurn = true;
            }
        }
        
        return new ChurnAnalysis(l_mayChurn, l_sb.toString());
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getRanking = "getRanking";
    /** Get ranking based on an average spend per period.
     * 
     * @param p_averageSpendPeriod Average spend in a period.
     * @return Ranking based on the average spend and configuration.
     */
    public char getRanking(Money p_averageSpendPeriod)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                            null, C_CLASS_NAME, 81000, this,
                            "Get Ranking: Amt: " + p_averageSpendPeriod + ", High: " + i_upperRankAmt + ", Low: " + i_lowerRankAmt);
        }

        char l_rank;
        try
        {
            int l_compare = p_averageSpendPeriod.compareTo(i_upperRankAmt);
            
            if (l_compare > 0)
            {
                // Large amount so High ranking
                l_rank = ChurnIndicatorData.C_RANK_HIGH;
            }
            else
            {
                l_compare = p_averageSpendPeriod.compareTo(i_lowerRankAmt);
                
                if (l_compare < 0)
                {
                    // Small amount so Low ranking
                    l_rank = ChurnIndicatorData.C_RANK_LOW;
                }
                else
                {
                    
                    l_rank = ChurnIndicatorData.C_RANK_MEDIUM;
                }
            }
        }
        catch (PpasConfigException e)
        {
            // Only set up for single currency at the moment so software error if wrong.
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_getRanking, 81110, this, null, 0,
                                            SoftwareKey.get().generalRuntime(), e);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END | PpasDebug.C_ST_TRACE,
                            null, C_CLASS_NAME, 81100, this,
                            "Get Ranking: Rank: " + l_rank);
        }
        
        return l_rank;
    }
    
    /** Flag indicating Customer Snapshot data is needed.
     * 
     * @return True if snapshot data is needed.
     */
    public boolean needsSnapshot()
    {
        return i_needSnapshot;
    }
    
    /** Set a flag indicating Customer Snapshot data is needed. */
    public void setNeedsSnapshot()
    {
        i_needSnapshot = true;
    }
    
    /** Get the minimum number of months an account must be active before Churn analysis can be performed.
     * 
     * @return Minimum number of months since account activation.
     */
    public int getMinActPeriod()
    {
        return i_minActPeriod;
    }

    /** Get the average spend (per period) for this operator.
     * 
     * @return Average spend.
     */
    public Money getOperatorAverage()
    {
        return i_operAveragePeriod;
    }

    /** Get the average spend (per day) for this operator.
     * 
     * @return Average spend.
     */
    public Money getOperatorAverageDay()
    {
        return i_operAverageDay;
    }

    /** Get the expected average spend over time for this operator.
     * 
     * @param p_numberDays Number of days.
     * @return Average spend for a period of time.
     */
    public Money getOperatorAverage(int p_numberDays)
    {
        return i_operAveragePeriod.multiply(p_numberDays/30.0);
    }

    /** Get the Low average spend (per day) for this operator.
     * 
     * @return Low average spend.
     */
    public Money getOperatorAverageLowDay()
    {
        return i_operAverageDayLow;
    }

    /** Get the expected Low average spend over time for this operator.
     * 
     * @param p_numberDays Number of days.
     * @return Low average spend for a period of time.
     */
    public Money getOperatorAverageLow(int p_numberDays)
    {
        return i_operAveragePeriodLow.multiply(p_numberDays/30.0);
    }

    /** Get the High average spend (per period) for this operator.
     * 
     * @return High average spend.
     */
    public Money getOperatorAverageHigh()
    {
        return i_operAveragePeriodHigh;
    }

    /** Get the High average spend (per day) for this operator.
     * 
     * @return High average spend.
     */
    public Money getOperatorAverageHighDay()
    {
        return i_operAverageDayHigh;
    }

    /** Get the expected High average spend over time for this operator.
     * 
     * @param p_numberDays Number of days.
     * @return High average spend for a period of time.
     */
    public Money getOperatorAverageHigh(int p_numberDays)
    {
        return i_operAveragePeriodHigh.multiply(p_numberDays/30.0);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getZeroAmount = "getZeroAmount";
    /** Get Zero amount in the subscribers currency.
     * 
     * @param p_ccy Subscribers currency.
     * @return Zero amount (in relevant currency).
     */
    public Money getZeroAmount(PpasCurrency p_ccy)
    {
        if (p_ccy.equals(i_systemCcy))
        {
            return i_zeroAmt;
        }
        
        try
        {
            return C_MONEY.parse("0", p_ccy);
        }
        catch (ParseException e)
        {
            // Cannot happen
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_getZeroAmount, 20000, this, null, 0,
                                            SoftwareKey.get().generalRuntime(), e);
        }
    }

    /** Get the tracking period.
     * 
     * @return Number of periods which should be tracked as 'current'.
     */
    public int getTrackingPeriod()
    {
        return i_trackingPeriod;
    }
    
    /** Get the analysis period.
     * 
     * @return Number of periods for which analysis is needed.
     */
    public int getAnalysisPeriod()
    {
        return i_analysisPeriod;
    }
    
    /** Get the analysis period in days.
     * 
     * @return Number of days for which analysis is needed.
     */
    public int getAnalysisPeriodDays()
    {
        return i_analysisPeriod * 30;
    }
    
    /** Flag indicating whether the High/Low averages are configured.
     * 
     * @return True if High/Low balances are configured.
     */
    public boolean isHighLowAverageSet()
    {
        return i_highLowAvgConfigured;
    }
    
    /** Convert an amount into another currency.
     * 
     * @param p_amount Amount to convert.
     * @param p_newCcy Desured currency.
     * @return Amount in the specified currency.
     */
    public Money convertAmount(Money p_amount, PpasCurrency p_newCcy)
    {
        if (p_amount == null || p_newCcy == null || p_amount.getCurrency().equals(p_newCcy))
        {
            return p_amount;
        }

        double l_xr = i_bcCache.getEmuConversionRatesCache().getAvailable().getConversionRate(
                                                      p_amount.getCurrency().getCurrencyCode(), p_newCcy.getCurrencyCode());
        
        return p_amount.convertToCurrency(p_newCcy, l_xr);
    }
    
    /** Describe this analyser. This method is in effect a <code>toString()</code> method but is intended to be used in a log file,
     *  so should be neatly formatted and some internal attributes will not be detailed. Note: Only one instance of this object is
     *  expected to exist per process so logging it will be acceptable - and useful.
     * 
     * @return Description of this object in a form suitable for logging.
     */
    public String describe()
    {
        StringBuffer l_sb = new StringBuffer();
        
        l_sb.append("Churn Analyser configuration:" + C_NL);

        l_sb.append("    Conditions:" + C_NL);
        
        for (int i = 0; i < i_conditions.length; i++)
        {
            l_sb.append("        ");
            l_sb.append(i + 1);
            l_sb.append(": ");
            l_sb.append(i_conditions[i].describe());
            l_sb.append(C_NL);
        }
        
        l_sb.append("    Conditions will be '");
        l_sb.append(i_andCondition ? "AND" : "OR");
        l_sb.append("'ed" + C_NL);

        l_sb.append("    Average Spend in ");
        l_sb.append(i_operAveragePeriod.getCurrency().getCurrencyCode());
        l_sb.append(":" + C_NL);
        if (i_highLowAvgConfigured)
        {
            l_sb.append("        Low:    ");
            l_sb.append(C_MONEY.format(i_operAveragePeriodLow));
            l_sb.append(C_NL);
        }
        
        l_sb.append("        Normal: ");
        l_sb.append(C_MONEY.format(i_operAveragePeriod));
        l_sb.append(C_NL);
        
        if (i_highLowAvgConfigured)
        {
            l_sb.append("        High:   ");
            l_sb.append(C_MONEY.format(i_operAveragePeriodHigh));
            l_sb.append(C_NL);
        }
        
        l_sb.append("    Minimum Activation Period: ");
        l_sb.append(i_minActPeriod);
        l_sb.append(C_NL);
        
        l_sb.append("    Analysis Period: ");
        l_sb.append(i_analysisPeriod);
        l_sb.append(C_NL);
        
        l_sb.append("    Tracking Period: ");
        l_sb.append(i_trackingPeriod);
        l_sb.append(C_NL);
        
        return l_sb.toString();
    }
}
