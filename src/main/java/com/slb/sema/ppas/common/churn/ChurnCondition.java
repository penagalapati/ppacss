////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnCondition.java
//      DATE            :       27-Mar-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Churn Condition checkers.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import java.text.ParseException;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/** Churn Condition checkers. */
public abstract class ChurnCondition
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChurnCondition";

    /** Prefix of Churn analysis properties. Value is {@value}. */
    protected static final String C_PROP_PREFIX = "com.slb.sema.ppas.common.CustChurn.";

    /** Text suggesting likely to Churn. Value is {@value}. */
    protected static final String C_CHURN_TEXT = "Suggests may Churn.";

    /** Text suggesting not likely to Churn. Value is {@value}. */
    protected static final String C_NO_CHURN_TEXT = "Does not suggest Churn.";

    /** Configuration properties. */
    protected PpasProperties i_properties;
    
    /** Business Configuration cache. */
    protected BusinessConfigCache i_bcCache;
    
    /** Churn analyser - for setting flags. */
    protected ChurnAnalyser i_analyser;
    
    /** Money formatter to parse properties. */
    private static final MoneyFormat C_MONEY = new MoneyFormat("0.p"); 
    
    /** Standard constructor.
     * 
     * @param p_properties Configuration properties.
     * @param p_bcCache    Business Configuration cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     */
    protected ChurnCondition(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
    {
        i_properties = p_properties;
        i_bcCache    = p_bcCache;
        i_analyser   = p_analyser;
    }
    
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public abstract boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason);
    
    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public abstract String describe();
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getMoneyProperty = "getMoneyProperty";
    /** Get a property as a Money object - using base currency.
     * 
     * @param p_property   Key of the property to get.
     * @return The property value as a Money object.
     * @throws PpasConfigException If configuration is invalid.
     */
    protected Money getMoneyProperty(String p_property) throws PpasConfigException
    {
        return getMoneyProperty(p_property, null);
    }
    
    /** Get a property as a Money object - using base currency.
     * 
     * @param p_property   Key of the property to get.
     * @param p_defaultAmount Default amount for the value.
     * @return The property value as a Money object.
     * @throws PpasConfigException If configuration is invalid.
     */
    protected Money getMoneyProperty(String p_property, String p_defaultAmount) throws PpasConfigException
    {
        String       l_avgSpend = i_properties.getTrimmedProperty(p_property, p_defaultAmount);
        PpasCurrency l_base     = i_bcCache.getConfCompanyConfigCache().getConfCompanyConfigData().getBaseCurrency();

        try
        {
            return C_MONEY.parse(l_avgSpend, l_base);
        }
        catch (ParseException e)
        {
            throw new PpasConfigException(C_CLASS_NAME, C_METHOD_getMoneyProperty, 10100, this, null, 0,
                                           ConfigKey.get().configNotNumeric(p_property), e);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAverageMonthlySpend = "getAverageMonthlySpend";
    /** Get average monthly spend - using base currency.
     * 
     * @return Average Monthly Spend as a Money object.
     * @throws PpasConfigException If configuration is invalid.
     */
    protected Money getAverageMonthlySpend() throws PpasConfigException
    {
        String   l_property = C_PROP_PREFIX + "monthlySpendAverage";
        String   l_avgSpend = i_properties.getTrimmedProperty(l_property, "0");
        String[] l_avg = l_avgSpend.split("\\s*,\\s*");

        String l_amt = l_avg.length >= 3 ? l_avg[1] : l_avg[0];
        
        PpasCurrency l_base     = i_bcCache.getConfCompanyConfigCache().getConfCompanyConfigData().getBaseCurrency();

        try
        {
            return C_MONEY.parse(l_amt, l_base);
        }
        catch (ParseException e)
        {
            throw new PpasConfigException(C_CLASS_NAME, C_METHOD_getAverageMonthlySpend, 11100, this, null, 0,
                                           ConfigKey.get().configNotNumeric(l_property), e);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_makeChurnCondition = "makeChurnCondition";
    /** Create an instance of a Churn Condition based on a String.
     * 
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_condition  String representing the condition, e.g. "C1".
     * @param p_analyser   Churn analyser - so flags can be set.
     * @throws PpasConfigException if properties are incorrect.
     * @return An instance of a ChurnCondition class.
     */
    public static ChurnCondition makeChurnCondition(PpasProperties      p_properties,
                                                    BusinessConfigCache p_bcCache,
                                                    String              p_condition,
                                                    ChurnAnalyser       p_analyser)
        throws PpasConfigException
    {
        String l_condition = p_condition.trim().toUpperCase();
        
        if (l_condition.equals("C1"))
        {
            return new ChurnConditionAvgSpendOverTime(p_properties, p_bcCache, p_analyser);
        }
        else if (l_condition.equals("C2"))
        {
            return new ChurnConditionAvgNumberRefills(p_properties, p_bcCache, p_analyser);
        }
        else if (l_condition.equals("C3"))
        {
            return new ChurnConditionCurrentBalance(p_properties, p_bcCache, p_analyser);
        }
        else if (l_condition.equals("C4"))
        {
            return new ChurnConditionTimeLastRefill(p_properties, p_bcCache, p_analyser);
        }
        else if (l_condition.equals("C5"))
        {
            return new ChurnConditionTimeServiceExpiry(p_properties, p_bcCache, p_analyser);
        }
        else if (l_condition.equals("C6"))
        {
            return new ChurnConditionSuspectAgent(p_properties, p_bcCache, p_analyser);
        }
        
        throw new PpasConfigException(C_CLASS_NAME, C_METHOD_makeChurnCondition, 20100, null, null, 0,
                                      ConfigKey.get().badConfigValue(C_PROP_PREFIX + "activeRules",
                                                                     p_condition,
                                                                     "C1, C2, C3, C4, C5, C6"));
    }
    
    /** Convert a money amount into a String.
     * 
     * @param p_amount Amount to format.
     * @return String representing the amount.
     */
    protected String formatMoney(Money p_amount)
    {
        return C_MONEY.format(p_amount);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getDifference = "getDifference";
    /** Get the difference between two money amounts. This is essentially <code>p_amount1 - p_amount2</code>.
     * 
     * @param p_amount1 First amount.
     * @param p_amount2 Second amount.
     * @return Difference between first and second amounts. That is, first - second.
     */
    protected Money getDifference(Money p_amount1, Money p_amount2)
    {
        try
        {
            return p_amount1.subtract(p_amount2);
        }
        catch (PpasConfigException e)
        {
            // Cannot happen - so software exception.
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_getDifference, 40010, null, null, 0,
                                            SoftwareKey.get().unexpectedException(), e);
        }
    }
}
