////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PucoPurgeControlData.Java
//      DATE            :       23-Qug-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#112/3801
//                              PRD_PPAK00_ANA_FD_19
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Stores a row from the PUCO_PURGE_CONTROL database
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** An object that represents a row from the SRVA_MISC_CODES database table. */
public class PucoPurgeControlData
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PucoPurgeControlData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Symbolic Name of the purge job. */
    private String i_jobType;
    
    /** Timestamp when job was launched. */
    private PpasDateTime i_jobExecutionDateTime;
    
    /** Date when data is eligible for purging. */
    private PpasDate i_eligibilityDate;

    /** Indicates whether the archive option has been selected or not. */
    private boolean i_archiveOption;

    /**
     * Job status:
     * 'I' - in progress
     * 'C' - completed
     * 'X' - stopped
     * 'F' - failed 
     */
    private char i_status;
    
    /** Initially the number of started sub-jobs. Internal use only and only by master started purges. */
    private int i_subJobCount;
    
    /** Last time row was modified. */
    private PpasDate i_genYyyymmdd;
    
    /** The Operator id of the submitter. */
    private String i_opid;
    
    /** Unique job ID that is assigned to the job by Job Scheduler. */
    private long i_jsJobId;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a Purge Control data object.
     * 
     * @param p_request The request to process.
     * @param p_jobType Symbolic Name of the purge job.
     * @param p_jobExecutionDateTime Timestamp when purge was launched.
     * @param p_eligibilityDate Date when data is eligible for purging.
     * @param p_archiveOption Indicates whether the option to archive has been selected.
     * @param p_status The status of the job
     * @param p_subJobCount Initially the number of started sub-jobs.
     * @param p_genYyyymmdd Last time row was modified.
     * @param p_opid The Operator id of the submitter.
     * @param p_jsJobId Unique job ID that is assigned to the job by the Job Scheduler.
     */
    public PucoPurgeControlData(PpasRequest  p_request,
                                String       p_jobType,
                                PpasDateTime p_jobExecutionDateTime,
                                PpasDate     p_eligibilityDate,
                                boolean      p_archiveOption,
                                char         p_status,
                                int          p_subJobCount,
                                PpasDate     p_genYyyymmdd,
                                String       p_opid,
                                long         p_jsJobId)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_jobType              = p_jobType;
        i_jobExecutionDateTime = p_jobExecutionDateTime;
        i_eligibilityDate      = p_eligibilityDate;
        i_archiveOption        = p_archiveOption;
        i_status               = p_status;
        i_subJobCount          = p_subJobCount;
        i_genYyyymmdd          = p_genYyyymmdd;
        i_opid                 = p_opid;
        i_jsJobId              = p_jsJobId;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {          
        return ("PucoPurgeControlData: " +
                "i_jobType=" + i_jobType + ":" +
                "i_jobExecutionDateTime=" +  i_jobExecutionDateTime + ":" +
                "i_eligibilityDate=" + i_eligibilityDate + ":" +
                "i_archiveOption=" + i_archiveOption + ":" +
                "i_status=" + i_status + ":" +
                "i_subJobCount=" + i_subJobCount + ":" +
                "i_genYyyymmdd=" + i_genYyyymmdd + ":" +
                "i_opid=" + i_opid + ":" +
                "i_jsJobId=" + i_jsJobId);

    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /** 
     * Returns the Job Type.
     * @return Symbolic Name of the purge job.
     */
    public String getJobType()
    {
        return i_jobType;
    }
    
    /**
     * Returns the Job Execution Date Time.
     * @return Timestamp when job was launched.
     */
    public PpasDateTime getJobExecutionDateTime()
    {
        return i_jobExecutionDateTime;
    }
    
    /**
     * Returns the Purge Eligibility Date.
     * @return Date when data is eligible for purging.
     */
    public PpasDate getEligibilityDate()
    {
        return i_eligibilityDate;
    }
    
    /**
     * Returns the status of the archive option.
     * @return Whether or not the archive option has been selected.
     */
    public boolean getArchiveOption()
    {
        return i_archiveOption;
    }

    /**
     * Returns the Job Status. This is one of the following:
     * <p>
     * 'I' - in progress
     * 'C' - completed
     * 'X' - stopped
     * 'F' - failed
     * @return The Job Status 
     */
    public char getStatus()
    {
        return i_status;
    }
    
    /** 
     * Returns the number of started sub-jobs. Internal use only and only by master started purges.
     * @return A count of the number of sub jobs
     */
    public int getSubJobCount()
    {
        return i_subJobCount;
    }
    
    /**
     * The date when this database information was last modified.
     * @return Date/time this data was last changed.
     */
    public PpasDate getGenYyyymmdd()
    {
        return i_genYyyymmdd;
    }
    
    /** 
     * The Operator id of the submitter. 
     * @return The Operator Username
     */
    public String getOpid()
    {
        return i_opid;
    }
    
    /**
     * Returns the js job id.
     * @return Unique job ID that is assigned to the job by Job Scheduler
     */
    public long getJsJobId() 
    {
        return i_jsJobId;
    }
}
