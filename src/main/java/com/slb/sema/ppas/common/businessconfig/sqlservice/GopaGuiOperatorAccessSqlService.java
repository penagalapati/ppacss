////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GopaGuiOperatorAccessSqlService.Java
//      DATE            :       29-January-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1211/4922
//                              PRD_PPAK00_GEN_CA_318
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Service to read the GOPA_GUI_OPERATOR_ACCESS
//                              table and return a GopaGuiOperatorAccessDataSet
//                              object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME       | DESCRIPTION                     | REFERENCE
//-----------+------------+---------------------------------+---------------------
// 17/10/05  | K Goswami  | Changed code to allow temp      | PpacLon#463
//           |            | blocking privilege.             |
//-----------+------------+---------------------------------+---------------------
//  15/12/05 |K Goswami   | Changed code to allow voucher   | PpaLon#1892/7603
//           |            | history enquiry privilege       | CA#39
//-----------+------------+---------------------------------+----------------
// 04-Dec-05 | M Erskine  | Licensed feature to enable      | PpacLon#1882/7556
//           |            | operator to pass in a new flag  |PRD_ASCS00_GEN_CA_65
//           |            | specifying that install request |
//           |            | can use a quarantined MSISDN    |
//-----------+------------+---------------------------------+----------------
// 21-Mar-06 | Ian James  | Add operator access privilege   | PpacLon#2055/8226
//           |            | Call History screen function.   | PRD_ASCS00_GEN_CA_66
//-----------+------------+---------------------------------+-------------------
// 24-Apr-07 | S James    | Add Single Step Install changes | PpacLon#3072/11370
//           |            | & Account Reconnection changes  |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Service to read the GOPA_GUI_OPERATOR_ACCESS table and return a
 * GopaGuiOperatorAccessDataSet object.
 */
public class GopaGuiOperatorAccessSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "GopaGuiOperatorAccessSqlService";

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------
    /** The Logger object to be used in logging any exceptions arising from the
     *  use of this service.
     */
    private Logger i_logger = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new GUI operator access sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public GopaGuiOperatorAccessSqlService(PpasRequest p_request,
                                           Logger      p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Fetches GUI Operator Access Profiles from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of GUI Operator Access Profile objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public GopaGuiOperatorAccessDataSet readAll(PpasRequest            p_request,
                                                JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement                l_statement;
        JdbcResultSet                l_resultSet;
        SqlString                    l_sql;
        GopaGuiOperatorAccessData    l_gopaData;
        GopaGuiOperatorAccessDataSet l_gopaDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(1000, 0, "SELECT gopa_privilege_id," +
                       "gopa_new_subs_screen," +
                       "gopa_subs_details_screen," +
                       "gopa_vouchers_screen," +
                       "gopa_payments_screen," +
                       "gopa_adjustments_screen," +
                       "gopa_promotions_screen," +
                       "gopa_disconnect_screen," +
                       "gopa_subordinates_screen," +
                       "gopa_add_info_screen," +
                       "gopa_ded_accounts_screen," +
                       "gopa_tbau_screen," +
                       "gopa_comment_screen," +
                       "gopa_faf_screen," +
                       "gopa_adj_apprv_screen," +
                       "gopa_sub_segment_screen," +
                       "gopa_commun_charg_screen," +
                       "gopa_msisdn_routing_screen," +
                       "gopa_counters_screen," +
                       "gopa_deductions_screen," +
                       "gopa_exp_dates_update," +
                       "gopa_serv_class_change," +
                       "gopa_language_change," +
                       "gopa_agent_update," +
                       "gopa_ivr_unbar," +
                       "gopa_install_in_id," +
                       "gopa_subs_details_update," +
                       "gopa_account_recharge," +
                       "gopa_voucher_enquiry," +
                       "gopa_make_payment," +
                       "gopa_make_adjustment," +
                       "gopa_promotions_update," +
                       "gopa_add_subordinate," +
                       "gopa_add_info_update," +
                       "gopa_make_ded_acc_adjust," +
                       "gopa_update_faf_list," +
                       "gopa_update_accum," +
                       "gopa_voucher_update," +
                       "gopa_sub_segment_update," +
                       "gopa_commun_charg_update," +
                       "gopa_ussd_eocn_update," +
                       "gopa_ivr_pin_code_update," +
                       "gopa_update_counters," +
                       "gopa_home_region_update," +
                       "gopa_clear_temp_blocking," +
                       "gopa_voucher_history," +
                       "gopa_override_quarantine," +
                       "gopa_one_step_install," +
                       "gopa_call_hist_screen," +
                       "gopa_reconnect_subscriber," +
                       "gopa_opid," +
                       "gopa_gen_ymdhms " +
                "FROM gopa_gui_operator_access");

        l_gopaDataSet = new GopaGuiOperatorAccessDataSet(p_request, 20, 10);

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 11010, this, p_request);

        l_resultSet = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 11020, this, p_request, l_sql);

        while (l_resultSet.next(11030))
        {
            // Create a new GUI Operator Access Profile object...
            l_gopaData = new GopaGuiOperatorAccessData
                                 (p_request,
                                  l_resultSet.getInt(11040, "gopa_privilege_id"),
                                  l_resultSet.getChar(11050, "gopa_new_subs_screen"),
                                  l_resultSet.getChar(11060, "gopa_subs_details_screen"),
                                  l_resultSet.getChar(11070, "gopa_vouchers_screen"),
                                  l_resultSet.getChar(11080, "gopa_payments_screen"),
                                  l_resultSet.getChar(11090, "gopa_adjustments_screen"),
                                  l_resultSet.getChar(11100, "gopa_promotions_screen"),
                                  l_resultSet.getChar(11110, "gopa_disconnect_screen"),
                                  l_resultSet.getChar(11120, "gopa_subordinates_screen"),
                                  l_resultSet.getChar(11130, "gopa_add_info_screen"),
                                  l_resultSet.getChar(11131, "gopa_ded_accounts_screen"),
                                  l_resultSet.getChar(11132, "gopa_tbau_screen"),
                                  l_resultSet.getChar(11140, "gopa_comment_screen"),
                                  l_resultSet.getChar(11145, "gopa_faf_screen"),
                                  l_resultSet.getChar(11147, "gopa_adj_apprv_screen"),
                                  l_resultSet.getChar(11148, "gopa_sub_segment_screen"),
                                  l_resultSet.getChar(11149, "gopa_commun_charg_screen"),
                                  l_resultSet.getChar(11150, "gopa_msisdn_routing_screen"),
                                  l_resultSet.getChar(11153, "gopa_counters_screen"),
                                  l_resultSet.getChar(11154, "gopa_deductions_screen"),
                                  l_resultSet.getChar(11155, "gopa_exp_dates_update"),
                                  l_resultSet.getChar(11160, "gopa_serv_class_change"),
                                  l_resultSet.getChar(11170, "gopa_language_change"),
                                  l_resultSet.getChar(11180, "gopa_agent_update"),
                                  l_resultSet.getChar(11190, "gopa_ivr_unbar"),
                                  l_resultSet.getChar(11195, "gopa_install_in_id"),
                                  l_resultSet.getChar(11200, "gopa_subs_details_update"),
                                  l_resultSet.getChar(11210, "gopa_account_recharge"),
                                  l_resultSet.getChar(11220, "gopa_voucher_enquiry"),
                                  l_resultSet.getChar(11230, "gopa_make_payment"),
                                  l_resultSet.getChar(11240, "gopa_make_adjustment"),
                                  l_resultSet.getChar(11250, "gopa_promotions_update"),
                                  l_resultSet.getChar(11260, "gopa_add_subordinate"),
                                  l_resultSet.getChar(11270, "gopa_add_info_update"),
                                  l_resultSet.getChar(11275, "gopa_make_ded_acc_adjust"),
                                  l_resultSet.getChar(11277, "gopa_update_faf_list"),
                                  l_resultSet.getChar(11280, "gopa_update_accum"),
                                  l_resultSet.getChar(11295, "gopa_voucher_update"),
                                  l_resultSet.getChar(11300, "gopa_sub_segment_update"),
                                  l_resultSet.getChar(11305, "gopa_commun_charg_update"),
                                  l_resultSet.getChar(11310, "gopa_ussd_eocn_update"),
                                  l_resultSet.getChar(11320, "gopa_ivr_pin_code_update"),
                                  l_resultSet.getChar(11325, "gopa_update_counters"),
                                  l_resultSet.getChar(11330, "gopa_home_region_update"),
                                  l_resultSet.getChar(11331, "gopa_clear_temp_blocking"),
                                  l_resultSet.getChar(11332, "gopa_voucher_history"),
                                  l_resultSet.getChar(11333, "gopa_override_quarantine"),
                                  l_resultSet.getChar(11334, "gopa_one_step_install"),
                                  l_resultSet.getChar(11335, "gopa_call_hist_screen"),
                                  l_resultSet.getChar(11336, "gopa_reconnect_subscriber"),
                                  l_resultSet.getTrimString(11280, "gopa_opid"),
                                  l_resultSet.getDateTime(11290, "gopa_gen_ymdhms"));

            // ...and stuff it in the GOPA data set.
            l_gopaDataSet.addElement(l_gopaData);
        }

        l_resultSet.close(11900);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 11910, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11920, this,
                "Leaving " + C_METHOD_readAll);
        }

        return(l_gopaDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates the Customer Care GUI operator access privileges.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_privilegeId Operator Privilege Identifier.
     * @param p_newSubsScreen Determines if access is allowed to the New Subscriber screen.
     * @param p_subsDetailsScreen Determines if access is allowed to the Subscriber Details screen.
     * @param p_vouchersScreen Determines if access is allowed to the Vouchers screen.
     * @param p_paymentsScreen Determines if access is allowed to the Payments screen.
     * @param p_adjustmentsScreen Determines if access is allowed to the Adjustments screen.
     * @param p_promotionsScreen Determines if access is allowed to the Promotions screen.
     * @param p_disconnectScreen Determines if access is allowed to the Disconnection screen.
     * @param p_subordinatesScreen Determines if access is allowed to the Subordinates screen.
     * @param p_addInfoScreen Determines if access is allowed to the Additional Information screen.
     * @param p_dedAccountsScreen Determines if access is allowed to the Dedicated Accounts screen.
     * @param p_tbauScreen Determines if access is allowed to the Accumulators screen.
     * @param p_commentScreen Determines if access is allowed to the Write Memo screen.
     * @param p_fafScreen Determines if access is allowed to the Family and Friends screen.
     * @param p_adjApprvScreen Determines if access is allowed to the Adjustment Approval screen.
     * @param p_msisdnRoutingScreen Determines if access is allowed to the Msisdn routing screen.
     * @param p_eventCountersScreen Determines if access is allowed to the Charged Event Counters screen.
     * @param p_serviceFeeScreen Determines if access is allowed to the Service Fee Deductions screen.
     * @param p_expDatesUpdate Determines if access is allowed to update expiry dates on the 
     *                         Account Details screen.
     * @param p_servClassChange Determines if access is allowed to change service class on the
     *                          Account Details screen.
     * @param p_languageChange Determines if access is allowed to change language on the 
     *                         Account Details screen. 
     * @param p_agentUpdate Determines if access is allowed to update agent or sub agent on the
     *                      Account Details screen.
     * @param p_ivrUnbar Determines if access is allowed to refill unbar on the Account Details screen. 
     * @param p_installInId Determines if access is allowed to select IN for a new subscriber on the 
     *                      New Subscriber screen. 
     * @param p_subsDetailsUpdate Determines if access is allowed to update subscriber details on the 
     *                            Subscriber Details screen. 
     * @param p_accountRecharge Determines if access is allowed to Standard Voucher Recharge on the 
     *                          Vouchers screen. 
     * @param p_voucherEnquiry Determines if access is allowed to Voucher Enquiry on the Vouchers screen. 
     * @param p_makePayment Determines if access is allowed to make payment on the Payments screen.
     * @param p_makeAdjustment Determines if access is allowed to make adjustment on the Adjustments
     *                         screen.
     * @param p_promotionsUpdate Determines if access is allowed to modify promotion plan allocations on
     *                           the Promotions screen. 
     * @param p_addSubordinate Determines if access is allowed to add subordinates on the Subordinates
     *                         screen. 
     * @param p_addInfoUpdate Determines if access is allowed to update additional information on the 
     *                        Additional Information screen.
     * @param p_makeDedAccAdjust Determines if access is allowed to make dedicated account adjustments on 
     *                           the Dedicated Account screen.
     * @param p_updateFafList Determines if access is allowed to modify family and friends numbers on the 
     *                        Family and Friends screen. 
     * @param p_updateAccum Determines if access is allowed to update accumulators on the Accumulators
     *                      screen.
     * @param p_voucherUpdate Determines if access is allowed to update vouchers on the Vouchers screen.
     * @param p_segmentationScreen Determines if access is allowed to the Subscriber Segmentation screen.
     * @param p_segmentationUpdate Determines if access is allowed to update subscriber segmentation.
     * @param p_commChargingScreen Determines if access is allowed to the Community Charging screen.
     * @param p_commChargingUpdate Determines if access is allowed to update community charging.
     * @param p_ussdEocnUpdate Determines if access is allowed to update USSD EoCN selection structure IDs.
     * @param p_subscriberIvrPinUpdate Determines if access is allowed to update subscriber's IVR pin.
     * @param p_eventCountersUpdate Determines if operator is allowed to update charged event counters.
     * @param p_homeRegionIdUpdate Determines if operator is allowed to update home region Ids.
     * @param p_tempBlockUpdate Determines if operator is allowed to update home region Ids.
     * @param p_voucherHistory Determines if operator is allowed to view voucher history popup.
     * @param p_overrideQuarantinePeriod Determines if operator is allowed to override the MSISDN
     *                                   quarantine period.
     * @param p_singleStepInstall Determines if the operator may perform a single step install.
     * @param p_callHistoryScreen Determines if access is allowed to the Call History screen.
     * @param p_accountReconnection Determines if the operator may perform an Account Reconnection.
     * @param p_opid The username of the operator who has done this update.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_privilegeId,
                       char                   p_newSubsScreen,
                       char                   p_subsDetailsScreen,
                       char                   p_vouchersScreen,
                       char                   p_paymentsScreen,
                       char                   p_adjustmentsScreen,
                       char                   p_promotionsScreen,
                       char                   p_disconnectScreen,
                       char                   p_subordinatesScreen,
                       char                   p_addInfoScreen,
                       char                   p_dedAccountsScreen,
                       char                   p_tbauScreen,
                       char                   p_commentScreen,
                       char                   p_fafScreen,
                       char                   p_adjApprvScreen,
                       char                   p_msisdnRoutingScreen,
                       char                   p_eventCountersScreen,
                       char                   p_serviceFeeScreen,
                       char                   p_expDatesUpdate,
                       char                   p_servClassChange,
                       char                   p_languageChange,
                       char                   p_agentUpdate,
                       char                   p_ivrUnbar,
                       char                   p_installInId,
                       char                   p_subsDetailsUpdate,
                       char                   p_accountRecharge,
                       char                   p_voucherEnquiry,
                       char                   p_makePayment,
                       char                   p_makeAdjustment,
                       char                   p_promotionsUpdate,
                       char                   p_addSubordinate,
                       char                   p_addInfoUpdate,
                       char                   p_makeDedAccAdjust,
                       char                   p_updateFafList,
                       char                   p_updateAccum,
                       char                   p_voucherUpdate,
                       char                   p_segmentationScreen,
                       char                   p_segmentationUpdate,
                       char                   p_commChargingScreen,
                       char                   p_commChargingUpdate,
                       char                   p_ussdEocnUpdate,
                       char                   p_subscriberIvrPinUpdate,
                       char                   p_eventCountersUpdate,
                       char                   p_homeRegionIdUpdate,
                       char                   p_tempBlockUpdate,
                       char                   p_voucherHistory,
                       char                   p_overrideQuarantinePeriod,
                       char                   p_singleStepInstall,
                       char                   p_callHistoryScreen,
                       char                   p_accountReconnection,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement   l_statement = null;
        JdbcResultSet   l_resultSet;
        String          l_sql;
        SqlString       l_sqlString;
        PpasDateTime    l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 12000, this,
                "Entered " + C_METHOD_update);
        }

        try
        {
            
            l_sql = new String("UPDATE gopa_gui_operator_access " +
                               "SET gopa_new_subs_screen = '" + p_newSubsScreen + "'," +
                               "    gopa_subs_details_screen = '" + p_subsDetailsScreen + "'," +
                               "    gopa_vouchers_screen = '" + p_vouchersScreen + "'," +
                               "    gopa_payments_screen = '" + p_paymentsScreen + "'," +
                               "    gopa_adjustments_screen = '" + p_adjustmentsScreen + "'," +
                               "    gopa_promotions_screen = '" + p_promotionsScreen + "'," +
                               "    gopa_disconnect_screen = '" + p_disconnectScreen + "'," +
                               "    gopa_subordinates_screen = '" + p_subordinatesScreen + "'," +
                               "    gopa_add_info_screen = '" + p_addInfoScreen + "'," +
                               "    gopa_ded_accounts_screen = '" + p_dedAccountsScreen + "'," +
                               "    gopa_tbau_screen = '" + p_tbauScreen + "'," +
                               "    gopa_comment_screen = '" + p_commentScreen + "'," +
                               "    gopa_faf_screen = '" + p_fafScreen + "'," +
                               "    gopa_adj_apprv_screen = '" + p_adjApprvScreen + "'," +
                               "    gopa_sub_segment_screen = '" + p_segmentationScreen + "'," +
                               "    gopa_commun_charg_screen = '" + p_commChargingScreen + "'," +
                               "    gopa_msisdn_routing_screen = '" + p_msisdnRoutingScreen + "'," +
                               "    gopa_counters_screen = '" + p_eventCountersScreen + "'," +
                               "    gopa_deductions_screen = '" + p_serviceFeeScreen + "'," +
                               "    gopa_exp_dates_update = '" + p_expDatesUpdate + "'," +
                               "    gopa_serv_class_change = '" + p_servClassChange + "'," +
                               "    gopa_language_change = '" + p_languageChange + "'," +
                               "    gopa_agent_update = '" + p_agentUpdate + "'," +
                               "    gopa_ivr_unbar = '" + p_ivrUnbar + "'," +
                               "    gopa_install_in_id = '" + p_installInId + "'," +
                               "    gopa_subs_details_update = '" + p_subsDetailsUpdate + "'," +
                               "    gopa_account_recharge = '" + p_accountRecharge + "'," +
                               "    gopa_voucher_enquiry = '" + p_voucherEnquiry + "'," +
                               "    gopa_make_payment = '" + p_makePayment + "'," +
                               "    gopa_make_adjustment = '" + p_makeAdjustment + "'," +
                               "    gopa_promotions_update = '" + p_promotionsUpdate + "'," +
                               "    gopa_add_subordinate = '" + p_addSubordinate + "'," +
                               "    gopa_add_info_update = '" + p_addInfoUpdate + "'," +
                               "    gopa_make_ded_acc_adjust = '" + p_makeDedAccAdjust + "'," +
                               "    gopa_update_faf_list = '" + p_updateFafList + "'," +
                               "    gopa_update_accum = '" + p_updateAccum + "'," +
                               "    gopa_voucher_update = '" + p_voucherUpdate + "'," +
                               "    gopa_sub_segment_update = '" + p_segmentationUpdate + "'," +
                               "    gopa_commun_charg_update = '" + p_commChargingUpdate + "'," +
                               "    gopa_ussd_eocn_update = '" + p_ussdEocnUpdate + "'," +
                               "    gopa_ivr_pin_code_update = '" + p_subscriberIvrPinUpdate + "'," +
                               "    gopa_update_counters = '" + p_eventCountersUpdate + "'," +
                               "    gopa_home_region_update = '" + p_homeRegionIdUpdate + "'," +
                               "    gopa_clear_temp_blocking = '" + p_tempBlockUpdate + "'," +
                               "    gopa_voucher_history = '" + p_voucherHistory + "'," +
                               "    gopa_override_quarantine = '" + p_overrideQuarantinePeriod + "'," +
                               "    gopa_one_step_install = '" + p_singleStepInstall + "'," +
                               "    gopa_call_hist_screen = '" + p_callHistoryScreen + "'," +
                               "    gopa_reconnect_subscriber = '" + p_accountReconnection + "'," +
                               "    gopa_opid = '" + p_opid + "'," +
                               "    gopa_gen_ymdhms = '" + l_now + "' " +
                               "WHERE gopa_privilege_id = " + p_privilegeId);

            l_sqlString = new SqlString(500, 0, l_sql);

            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 12050, this, p_request);

            l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       12100,
                                       this,
                                       p_request,
                                       l_sqlString);


            l_resultSet.close(12150);

            l_statement.close( C_CLASS_NAME, C_METHOD_update, 12200, this, p_request);
        }
        catch (PpasSqlException l_pe)
        {
            if (i_logger != null)
            {
                i_logger.logMessage(l_pe);
            }
            throw l_pe;
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 12250, this, p_request);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 12300, this,
                "Leaving " + C_METHOD_update);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts Customer Care GUI operator access privileges for a specified privilege id.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_privilegeId Operator Privilege Identifier.
     * @param p_newSubsScreen Determines if access is allowed to the New Subscriber screen.
     * @param p_subsDetailsScreen Determines if access is allowed to the Subscriber Details screen.
     * @param p_vouchersScreen Determines if access is allowed to the Vouchers screen.
     * @param p_paymentsScreen Determines if access is allowed to the Payments screen.
     * @param p_adjustmentsScreen Determines if access is allowed to the Adjustments screen.
     * @param p_promotionsScreen Determines if access is allowed to the Promotions screen.
     * @param p_disconnectScreen Determines if access is allowed to the Disconnection screen.
     * @param p_subordinatesScreen Determines if access is allowed to the Subordinates screen.
     * @param p_addInfoScreen Determines if access is allowed to the Additional Information screen.
     * @param p_dedAccountsScreen Determines if access is allowed to the Dedicated Accounts screen.
     * @param p_tbauScreen Determines if access is allowed to the Accumulators screen.
     * @param p_commentScreen Determines if access is allowed to the Write Memo screen.
     * @param p_fafScreen Determines if access is allowed to the Family and Friends screen.
     * @param p_adjApprvScreen Determines if access is allowed to the Adjustment Approval screen.
     * @param p_msisdnRoutingScreen Determines if access is allowed to the Msisdn routing screen.
     * @param p_eventCountersScreen Determines if access is allowed to the Charged Event Counters screen.
     * @param p_serviceFeeScreen Determines if access is allowed to the Service Fee Deductions screen.
     * * @param p_expDatesUpdate Determines if access is allowed to update expiry dates on the 
     *                         Account Details screen.
     * @param p_servClassChange Determines if access is allowed to change service class on the
     *                          Account Details screen.
     * @param p_languageChange Determines if access is allowed to change language on the 
     *                         Account Details screen. 
     * @param p_agentUpdate Determines if access is allowed to update agent or sub agent on the
     *                      Account Details screen.
     * @param p_ivrUnbar Determines if access is allowed to refill unbar on the Account Details screen. 
     * @param p_installInId Determines if access is allowed to select IN for a new subscriber on the 
     *                      New Subscriber screen. 
     * @param p_subsDetailsUpdate Determines if access is allowed to update subscriber details on the 
     *                            Subscriber Details screen. 
     * @param p_accountRecharge Determines if access is allowed to Standard Voucher Recharge on the 
     *                          Vouchers screen. 
     * @param p_voucherEnquiry Determines if access is allowed to Voucher Enquiry on the Vouchers screen. 
     * @param p_makePayment Determines if access is allowed to make payment on the Payments screen.
     * @param p_makeAdjustment Determines if access is allowed to make adjustment on the Adjustments
     *                         screen.
     * @param p_promotionsUpdate Determines if access is allowed to modify promotion plan allocations on
     *                           the Promotions screen. 
     * @param p_addSubordinate Determines if access is allowed to add subordinates on the Subordinates
     *                         screen. 
     * @param p_addInfoUpdate Determines if access is allowed to update additional information on the 
     *                        Additional Information screen.
     * @param p_makeDedAccAdjust Determines if access is allowed to make dedicated account adjustments on 
     *                           the Dedicated Account screen.
     * @param p_updateFafList Determines if access is allowed to modify family and friends numbers on the 
     *                        Family and Friends screen. 
     * @param p_updateAccum Determines if access is allowed to update accumulators on the Accumulators
     *                      screen.
     * @param p_voucherUpdate Determines if access is allowed to update vouchers on the Vouchers screen.
     * @param p_segmentationScreen Determines if access is allowed to the Subscriber Segmentation screen.
     * @param p_segmentationUpdate Determines if access is allowed to update subscriber segmentation.
     * @param p_commChargingScreen Determines if access is allowed to the Subscriber Segmentation screen.
     * @param p_commChargingUpdate Determines if access is allowed to update subscriber segmentation.
     * @param p_ussdEocnUpdate Determines if access is allowed to update USSD EoCN selection structure IDs.
     * @param p_subscriberIvrPinUpdate Determines if access is allowed to update subscriber's IVR pin.
     * @param p_eventCountersUpdate Determines if operator is allowed to update charged event counters.
     * @param p_homeRegionIdUpdate Determines if operator is allowed to update home region Ids.
     * @param p_tempBlockUpdate Determines if operator is allowed to update home region Ids.
     * @param p_voucherHistory Determines if operator is allowed to view voucher history popup.
     * @param p_overrideQuarantinePeriod Determines if operator is allowed to override the MSISDN
     *                                   quarantine period.
     * @param p_singleStepInstall Determines if the operator may perform a single step install.
     * @param p_callHistoryScreen Determines if access is allowed to the Call History screen.
     * @param p_accountReconnection Determines if the operator may perform an Account Reconnection.
     * @param p_opid The username of the operator who has done this update. )
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_privilegeId,
                       char                   p_newSubsScreen,
                       char                   p_subsDetailsScreen,
                       char                   p_vouchersScreen,
                       char                   p_paymentsScreen,
                       char                   p_adjustmentsScreen,
                       char                   p_promotionsScreen,
                       char                   p_disconnectScreen,
                       char                   p_subordinatesScreen,
                       char                   p_addInfoScreen,
                       char                   p_dedAccountsScreen,
                       char                   p_tbauScreen,
                       char                   p_commentScreen,
                       char                   p_fafScreen,
                       char                   p_adjApprvScreen,
                       char                   p_msisdnRoutingScreen,
                       char                   p_eventCountersScreen,
                       char                   p_serviceFeeScreen,
                       char                   p_expDatesUpdate,
                       char                   p_servClassChange,
                       char                   p_languageChange,
                       char                   p_agentUpdate,
                       char                   p_ivrUnbar,
                       char                   p_installInId,
                       char                   p_subsDetailsUpdate,
                       char                   p_accountRecharge,
                       char                   p_voucherEnquiry,
                       char                   p_makePayment,
                       char                   p_makeAdjustment,
                       char                   p_promotionsUpdate,
                       char                   p_addSubordinate,
                       char                   p_addInfoUpdate,
                       char                   p_makeDedAccAdjust,
                       char                   p_updateFafList,
                       char                   p_updateAccum,
                       char                   p_voucherUpdate,
                       char                   p_segmentationScreen,
                       char                   p_segmentationUpdate,
                       char                   p_commChargingScreen,
                       char                   p_commChargingUpdate,
                       char                   p_ussdEocnUpdate,
                       char                   p_subscriberIvrPinUpdate,
                       char                   p_eventCountersUpdate,
                       char                   p_homeRegionIdUpdate,
                       char                   p_tempBlockUpdate,
                       char                   p_voucherHistory,
                       char                   p_overrideQuarantinePeriod,
                       char                   p_singleStepInstall,
                       char                   p_callHistoryScreen,
                       char                   p_accountReconnection,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement   l_statement = null;
        JdbcResultSet   l_resultSet;
        String          l_sql;
        SqlString       l_sqlString;
        PpasDateTime    l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 12350, this,
                "Entered " + C_METHOD_insert);
        }

        try
        {
            l_sql = new String("INSERT INTO gopa_gui_operator_access" +
                               "(gopa_privilege_id, gopa_new_subs_screen, gopa_subs_details_screen, " +                               "gopa_vouchers_screen, gopa_payments_screen, gopa_adjustments_screen, " +                               "gopa_promotions_screen, gopa_disconnect_screen, gopa_subordinates_screen, " +                               "gopa_add_info_screen, gopa_comment_screen, gopa_ded_accounts_screen, " +                               "gopa_tbau_screen, gopa_counters_screen, gopa_deductions_screen, gopa_exp_dates_update, " + 
                               "gopa_serv_class_change, gopa_language_change, gopa_agent_update, gopa_ivr_unbar, " +                               "gopa_install_in_id, gopa_subs_details_update, gopa_account_recharge, " +                               "gopa_voucher_enquiry, gopa_make_payment, gopa_make_adjustment, " +                               "gopa_make_ded_acc_adjust, gopa_promotions_update, gopa_add_subordinate, " +
                               "gopa_add_info_update, gopa_faf_screen, gopa_update_faf_list, " + 
                               "gopa_update_accum, gopa_adj_apprv_screen, gopa_msisdn_routing_screen, " +                               "gopa_voucher_update, gopa_sub_segment_screen, gopa_sub_segment_update, " +
                               "gopa_commun_charg_screen, gopa_commun_charg_update, gopa_ussd_eocn_update, " +
                               "gopa_ivr_pin_code_update, gopa_update_counters, gopa_home_region_update," +
                               "gopa_clear_temp_blocking, gopa_voucher_history, gopa_override_quarantine," +
                               "gopa_one_step_install, gopa_call_hist_screen, gopa_reconnect_subscriber, " +
                               "gopa_opid, gopa_gen_ymdhms) " +
                               "VALUES(" + 
                               p_privilegeId + ", '" +
                               p_newSubsScreen + "', '" +
                               p_subsDetailsScreen + "', '" +
                               p_vouchersScreen + "', '" +
                               p_paymentsScreen + "', '" +
                               p_adjustmentsScreen + "', '" +
                               p_promotionsScreen + "', '" +
                               p_disconnectScreen + "', '" +
                               p_subordinatesScreen + "', '" +
                               p_addInfoScreen + "', '" +
                               p_commentScreen + "', '" +
                               p_dedAccountsScreen + "', '" +
                               p_tbauScreen + "', '" +
                               p_eventCountersScreen  + "', '" +
                               p_serviceFeeScreen  + "', '" +
                               p_expDatesUpdate + "', '" +
                               p_servClassChange + "', '" +
                               p_languageChange + "', '" +
                               p_agentUpdate + "', '" +
                               p_ivrUnbar + "', '" +
                               p_installInId + "', '" +
                               p_subsDetailsUpdate + "', '" +
                               p_accountRecharge + "', '" +
                               p_voucherEnquiry + "', '" +
                               p_makePayment + "', '" +
                               p_makeAdjustment + "', '" +
                               p_makeDedAccAdjust + "', '" +
                               p_promotionsUpdate + "', '" +
                               p_addSubordinate + "', '" +
                               p_addInfoUpdate + "', '" +
                               p_fafScreen + "', '" +
                               p_updateFafList + "', '" +
                               p_updateAccum + "', '" +
                               p_adjApprvScreen + "', '" +
                               p_msisdnRoutingScreen + "', '" +
                               p_voucherUpdate + "', '" +
                               p_segmentationScreen + "', '" +
                               p_segmentationUpdate + "', '" +
                               p_commChargingScreen + "', '" +
                               p_commChargingUpdate + "', '" +
                               p_ussdEocnUpdate + "', '" +
                               p_subscriberIvrPinUpdate + "', '" +
                               p_eventCountersUpdate + "', '" +
                               p_homeRegionIdUpdate + "', '" +
                               p_tempBlockUpdate + "', '" +
                               p_voucherHistory + "', '" +
                               p_overrideQuarantinePeriod + "', '" +
                               p_singleStepInstall + "', '" +
                               p_callHistoryScreen + "', '" +
                               p_accountReconnection + "', '" +
                               p_opid + "', '" +
                               l_now + "')");

            l_sqlString = new SqlString(500, 0, l_sql);

            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME, C_METHOD_insert, 12400, this, p_request);

            l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME, C_METHOD_insert, 12450, this, p_request, l_sqlString);

            l_resultSet.close(12500);

            l_statement.close( C_CLASS_NAME, C_METHOD_insert, 12550, this, p_request);
        }
        catch (PpasSqlException l_pe)
        {
            if (i_logger != null)
            {
                i_logger.logMessage(l_pe);
            }
            throw l_pe;
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 12600, this, p_request);
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 12650, this,
                "Leaving " + C_METHOD_insert);
        }
    }
}
