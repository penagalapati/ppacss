////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BonusCountsDataSet.Java
//      DATE            :       21-Jun-2007
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       PpaLon#1232/5077
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Data set containing records of a subscriber's
//                              progress towards an accumulated bonus.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;
import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** A Set (collection) of bonus count accumulation data. */
public class BonusCountsDataSet extends DataSetObject implements Serializable
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonusCountsDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** The bonus count data contained in this object. */
    private Vector i_bonusCountsDataV;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates a new, empty set of bonus accumulation history for an account.
     * @param p_request The request being processed.
     */
    public BonusCountsDataSet(PpasRequest p_request)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_bonusCountsDataV = new Vector();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Adds element to the vector of <data>BonusCountsData</data>.
     * @param  p_bonusCountsData Data object to be added to this data set.
     */
    public void add(BonusCountsData p_bonusCountsData)
    {
        i_bonusCountsDataV.add(p_bonusCountsData);
    }

    /**
     * Gets the whole set of bonus count data as an array.
     * @return Array of all the bonus count data.
     */
    public BonusCountsData[] asArray()
    {
        return (BonusCountsData[])i_bonusCountsDataV.toArray(new BonusCountsData[i_bonusCountsDataV.size()]);
    }

    /**
     * Returns the bonus counts data set as a Vector.
     * @return treeSet of CommentData.
     */
    public Vector getBonusCountsDataV()
    {
        return i_bonusCountsDataV;
    }


    /** Get the number of bonus count data records held.
     * 
     * @return Number of bonus accumulation records.
     */
    public int size()
    {
        return i_bonusCountsDataV.size();
    }
    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("BonusCountsDataSet", i_bonusCountsDataV, p_sb);
    }
}