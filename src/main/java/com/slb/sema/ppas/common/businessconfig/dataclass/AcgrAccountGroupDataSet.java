////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       AcgrAccountGroupDataSet.java
//    DATE            :       03-Jun-2004
//    AUTHOR          :       Oualid Gharach 78022K
//    REFERENCE       :       PpaLon#288/2615
//
//
//    COPYRIGHT       :       ATOSORIGIN 2004
//
//    DESCRIPTION     :       Contains the account groups selected from the 
//                            ACGR_ACCOUNT_GROUP table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 | 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Contains the account groups selected from the ACGR_ACCOUNT_GROUP table.
 */
public class AcgrAccountGroupDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AcgrAccountGroupDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of account group data records. That is, the data of this DataSet object. */
    private AcgrAccountGroupData []     i_accountGroupDataArr;
    
    /** Array of available account group data records. That is, the data of this DataSet object. */    
    private AcgrAccountGroupData []     i_availableAcgrAccountGroupArr;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of account group data.
     * 
     * @param p_request The request to process.
     * @param p_accountGroupDataArr Array holding the account group data records to be stored
     *                              by this object.
     */
    public AcgrAccountGroupDataSet(PpasRequest             p_request,
                                   AcgrAccountGroupData [] p_accountGroupDataArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 14510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_accountGroupDataArr = p_accountGroupDataArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 14590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns account group data associated with a specified account group id .
     * @param p_accountID The account group id.
     * 
     * @return AcgrAccountGroupData record from this DataSet with the given account ID. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public AcgrAccountGroupData getRecord (AccountGroupId p_accountID)
    {
        AcgrAccountGroupData l_accountGroupRecord = null;
        
        if (p_accountID != null)
        {
            for (int l_loop = 0; l_loop < i_accountGroupDataArr.length; l_loop++)
            {
                if ( i_accountGroupDataArr[l_loop].getAccountID().equals(p_accountID))                
                {
                    l_accountGroupRecord = i_accountGroupDataArr[l_loop];
                    break;
                }
            }
        }

        return (l_accountGroupRecord);
    }

    /**
     * Returns available (that is, not marked as deleted) account group data associated 
     * with a specified account group id.
     * @param p_accountID The account group id.
     * 
     * @return AcgrAccountGroupData record from this DataSet with the given account ID. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public AcgrAccountGroupData getAvailableRecord (AccountGroupId p_accountID)
    {
        AcgrAccountGroupData l_accountGroupRecord = getRecord(p_accountID);
        
        if (l_accountGroupRecord != null && l_accountGroupRecord.isDeleted())
        {
            l_accountGroupRecord = null;
        }

        return l_accountGroupRecord;
    }
    
    /** Returns array of all records in this data set object.
     * 
     * @return All Account Group.
     */
    public AcgrAccountGroupData [] getAllArray()
    {
        return (i_accountGroupDataArr);
    }

    /**
     * Get a data set containing the all available account group records (i.e.
     * those not marked as deleted.
     * @return Data set containing available account group records in the
     *         cache (i.e. those not marked as deleted).
     */
    public AcgrAccountGroupData [] getAvailableArray()
    {
        Vector            l_availableV = new Vector();
        int               l_loop;
        AcgrAccountGroupData  l_acgrAccountGroupEmptyArr[] = new AcgrAccountGroupData[0];

        if (i_availableAcgrAccountGroupArr == null)
        {
            for (l_loop = 0; l_loop < i_accountGroupDataArr.length; l_loop++)
            {
                if (!i_accountGroupDataArr[l_loop].isDeleted())
                {
                    l_availableV.addElement(i_accountGroupDataArr[l_loop]);
                }
            }
    
            i_availableAcgrAccountGroupArr =
                  (AcgrAccountGroupData[])l_availableV.toArray(l_acgrAccountGroupEmptyArr);
        }

        return(i_availableAcgrAccountGroupArr);
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_accountGroupDataArr, p_sb);
    }
}

