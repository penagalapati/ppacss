////////////////////////////////////////////////////////////////////////////////
//      CABS 2000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ConfigCache.Java
//      DATE            :       04-Apr-2001
//      AUTHOR          :       Erik Clayton
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A cache of business configuration data
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/** A cache of business configuration data. */
public abstract class ConfigCache extends DataCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ConfigCache";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Indicates has been loaded. */
    private boolean            i_loaded = false;

    /** Logger to be used to log exceptions generated. */
    protected Logger             i_logger;
    
    /** Configuration properties. */
    protected PpasProperties     i_properties;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new cache with properties.
     * 
     * @param p_logger     The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public ConfigCache(Logger p_logger, PpasProperties p_properties)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 98010, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_logger     = p_logger;
        i_properties = p_properties;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 28438, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Loads the static configuration for the first time.
     * 
     * @param p_request The request to process.
     * @param p_connection Database connection to use to exract the configuration.
     * @throws PpasSqlException If the configuration cannot be loaded into the cache.
     */
    public void load(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (!i_loaded)
        {
            reload(p_request, p_connection);
            i_loaded = true;

            String l_msg = "Successful execution of reload on " +  this.getClass().getName();

            if (i_properties != null && i_properties.getBooleanProperty(
                             "com.slb.sema.ppas.common.businessconfig.cache.ConfigCache.displayCache"))
            {
                l_msg += ":" + System.getProperty("line.separator") + toVerboseString();
            }
            
            i_logger.logMessage(new LoggableEvent(l_msg, LoggableInterface.C_SEVERITY_INFO));
        }
    } // End of public method load(PpasRequest, long, Connection)

    /** Reloads the static configuration.
     * 
     * @param p_request The request to process.
     * @param p_connection Database connection to use to exract the configuration.
     * @throws PpasSqlException If the configuration cannot be loaded into the cache.
     */
    public abstract void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException;
} // End of public class ConfigCache
