////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DireDisconnectReasonData.Java
//      DATE            :       16-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A record of disconnect reason data 
//                              (corresponding to a single row of the
//                              dire_disconnect_reason table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of disconnect reason data (i.e.
 * a single row of the dire_disconnect_reason table).
 */
public class DireDisconnectReasonData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DireDisconnectReasonData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Disconnect teason type code. */
    private  char              i_direType = ' ';

    /** Disconnect reason code. */
    private  String            i_direReason = null;

    /** Description of this disconnect reason. */
    private  String            i_direDesc = null;

    /** Adjustment code associated with this disconnect reason. */
    private  String            i_direAdjCode = null;

    /** Charge associated with this disconnect reason. */
    private  double            i_direCharge = 0.0;

    /** Date this disconnect reason record was created/last accessed in the
     *  database.
     */
    private  PpasDateTime      i_genYmdhms = null;

    /** Operator Id who created/last accessed this disconnect reason record in
     *  the database.
     */
    private  String            i_opid = null;
    
    /** Flag indicating if this disconnect reason is deleted. */
    private  char              i_delFlag = ' ';


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new disconnect reason data object and initialises its instance
     * variables.
     * 
     * @param p_request The request to process.
     * @param p_direType Type of disconnection reason.
     * @param p_direReason Key of this disconnection reason.
     * @param p_direDesc Description of this disconnection reason.
     * @param p_direAdjCode Adjustment code to use when applying an adjustment as a result of a disconnection.
     * @param p_direCharge Charge for disconnection the service.
     * @param p_genYmdhms Date/time of last change to this disconnection reason.
     * @param p_opid Operator that made the last change to this disconnection reason.
     * @param p_delFlag Flag to indicate the disconnection reason is marked as deleted.
     */
    public DireDisconnectReasonData(
        PpasRequest            p_request,
        char                   p_direType,
        String                 p_direReason,
        String                 p_direDesc,
        String                 p_direAdjCode,
        double                 p_direCharge,
        PpasDateTime           p_genYmdhms,
        String                 p_opid,
        char                   p_delFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 68010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_direType     = p_direType;
        i_direReason   = p_direReason;
        i_direDesc     = p_direDesc;
        i_direAdjCode  = p_direAdjCode;
        i_direCharge   = p_direCharge;
        i_genYmdhms    = p_genYmdhms;
        i_opid         = p_opid;
        i_delFlag      = p_delFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 68090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /** Get the Disconnect Reason Type.
     * 
     * @return The type of disconnection reason.
     */
    public char getDireType()
    {
        return (i_direType);
    }

    /** Get the disconnect reason code.
     * 
     * @return The disconnection reason code.
     */
    public String getDireReason()
    {
        return (i_direReason);
    }

    /** Get the description of this disconnect reason.
     * 
     * @return Description of this disconnection reason.
     */
    public String getDireDesc()
    {
        return (i_direDesc);
    }

    /** Get the adjustment code associated with this disconnect reason.
     * 
     * @return The adjustment code associated with this disconnection reason.
     */
    public String getDireAdjCode()
    {
        return (i_direAdjCode);
    }

    /** Get the charge associated with this disconnect reason.
     * 
     * @return The charge associated with this disconnection reason.
     */
    public double getDireCharge()
    {
        return (i_direCharge);
    }

    /** Get the Date this disconnect reason record was created/last accessed in the
     *  database.
     * 
     * @return Date/time of last change to this disconnection reason.
     */
    public PpasDateTime getGenYmdhms()
    {
        return (i_genYmdhms);
    }

    /** Get the Operator Id who created/last accessed this disconnect reason
     * record in the database.
     * 
     * @return Identifier of last operator to change this disconnection reason.
     */
    public String getOpid()
    {
        return (i_opid);
    }

    /** Returns true if this disconnect reason record has been marked as
     *  deleted.
     * 
     * @return Flag indicating whether thi sdisconnection reason is marked as deleted.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if (i_delFlag == '*')
        {
            l_deleted = true;
        }

        return (l_deleted);
    }

} // End of public class DireDisconnectReasonData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////