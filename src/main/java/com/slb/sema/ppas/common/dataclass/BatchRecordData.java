////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchDataRecord.java
//    DATE            :       15-Apr-2004
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PpacLon#219/2129
//                            PRD_ASCS_DEV_SS_083
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       This abstract class serves as a super class for
//                            all records stored in either the in- or the out- 
//                            queue.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY  | <name>     | <brief description of the       | <reference>
//          |            | changes>                        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;


/**
 * This abstract class serves as a super class for all records stored in either
 * the in- or the out-queue.
 */
public abstract class BatchRecordData
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME = "BatchDataRecord";

    //-----------------------------------------------------
    //  Class level constant
    //  ---------------------------------------------------
    
    /** Delimitor used by the method dumpRecord.  Value is {@value}. */
    protected final String C_DELIMITER = "\n";

    /** Constant for Debug printout.  Value is {@value}. */
    protected static final String C_CONSTRUCTING = "Constructing ";
    
    /** Constant for Debug printout.  Value is {@value}. */
    protected static final String C_CONSTRUCTED  = "Constructed ";

    /** Constant for Debug printout.  Value is {@value}. */
    protected static final String C_ENTERING     = "Entering ";

    /** Constant for Debug printout.  Value is {@value}. */
    protected static final String C_LEAVING      = "Leaving ";
    
    // Recovery status. Status written to recovery file for each process
    // by batch program.
    /** Recovery file status ERR - then recovery is needed to re-process that record. Value is {@value}. */
    public static final String C_NOT_PROCESSED          = "ERR";
    
    // Recovery status for successful status.
    /** Recovery file status OK - the record is not needed to be processed during recovery. Value is {@value}. */
    public static final String C_SUCCESSFULLY_PROCESSED = "OK";
    
    //-------------------------------------------------------------------------
    // Private variables
    //-------------------------------------------------------------------------
    /** The input filename (only used by file driven batch jobs). */
    private String  i_inputFilename = null;

    /** The current line number (only used by file driven batch jobs). */
    private long    i_rowNumber     = 0;

    /** The original input line (only used by file driven batch jobs). */
    private String  i_inputLine     = null;

    /** 
     * Corrupt record indicator (only used by file driven batch jobs).
     * Current record will not be processed if set to <code>true</code>.
     */
    private boolean i_corruptLine   = false;

    /** Error code, to be written into the error file. */
    private String  i_errorLine     = null;

    /** Recovery message, to be written into the recovery file. */
    private String  i_recoveryLine  = null;

    /** Output (log) message, to be written into the output file. */
    private String  i_outputLine    = null;

    /** Retry record indicator. Is set to <code>true</code> if current record shall be processed again. */
    private boolean i_retryRecord   = false;

    // Variables for handle recovery status of success and error counters
    /** Number of successfully processed records in prevous run. Used in recovery mode */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;

    /** Number of successfully processed records in prevous run. Used in recovery mode */
    private int     i_numberOfErrorRecords = 0;

    /** Flag to show that this record was successful or not in last run */
    private boolean i_failureFlag = false;
    
    /**
     * The no-arg Constructor.
     */
    public BatchRecordData()
    {
        super();
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10800,
                this,
                C_CONSTRUCTING + C_CLASS_NAME);
        }


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10810,
                this,
                C_CONSTRUCTED + C_CLASS_NAME);
        }

    }

    /**
     * An Object that represents a line of data read from a file by a batch program.
     * @param p_inputFilename The name of the input file.
     * @param p_rowNumber The number of the line.
     * @param p_inputLine The actual line of data.
     * @param p_retryRecord Whether the record is a retry.
     */
    public BatchRecordData(
        String                        p_inputFilename,
        long                          p_rowNumber,
        String                        p_inputLine,
        boolean                       p_retryRecord)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10800,
                this,
                C_CONSTRUCTING + C_CLASS_NAME);
        }


        i_inputFilename = p_inputFilename;
        i_rowNumber = p_rowNumber;
        i_inputLine = p_inputLine;
        i_retryRecord = p_retryRecord;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10810,
                this,
                C_CONSTRUCTED + C_CLASS_NAME);
        }

    }



    /**
     * Returns the record content in a readable format.
     * This method is declared <code>abstract</code>, and has to be
     * implemented in a subclass.
     * 
     * @return the record content in a readable format.
     */
    public abstract String dumpRecord();


    /**
     * @return <code>true</code> if the current line is corrupt, otherwise it returns <code>false</code>.
     */
    public boolean isCorruptLine()
    {
        return i_corruptLine;
    }

    /**
     * @return the error message.
     */
    public String getErrorLine()
    {
        return i_errorLine;
    }

    /**
     * @return the batch input filename.
     */
    public String getInputFilename()
    {
        return i_inputFilename;
    }

    /**
     * @return the input line (read from the batch input file).
     */
    public String getInputLine()
    {
        return i_inputLine;
    }

    /**
     * @return the output line.
     */
    public String getOutputLine()
    {
        return i_outputLine;
    }

    /**
     * @return the recovery line.
     */
    public String getRecoveryLine()
    {
        return i_recoveryLine;
    }

    /**
     * @return <code>true</code> if the current line shall be processed again,
     *         otherwise it returns <code>false</code>.
     */
    public boolean isRetryRecord()
    {
        return i_retryRecord;
    }

    /**
     * @return the current row number.
     */
    public long getRowNumber()
    {
        return i_rowNumber;
    }

    /**
     * @return Number of successfully processed records. 
     */
    public int getNumberOfSuccessfullyProcessedRecords()
    {
        return i_numberOfSuccessfullyProcessedRecords;
    }
    
    /**
     * @return Number of non-successfully processed records. 
     */
    public int getNumberOfErrorRecords()
    {
        return i_numberOfErrorRecords;
    }
    
    /**
     * Returns a flag telling if this record failed last time
     * it was processed. 
     * @return TRUE - failed FALSE - succeeded
     */
    public boolean getFailureStatus()
    {
        return i_failureFlag;
    }
    
    /**
     * Sets the corrupt record indicator.
     * @param p_corruptLine the corrupt record indicator.
     */
    public void setCorruptLine( boolean p_corruptLine )
    {
        i_corruptLine = p_corruptLine;
    }

    /**
     * @param p_errorLine the erroneous line.
     */
    public void setErrorLine( String p_errorLine )
    {
        i_errorLine = p_errorLine;
    }

    /**
     * @param p_inputFilename the name of the batch input file.
     */
    public void setInputFilename( String p_inputFilename )
    {
        i_inputFilename = p_inputFilename;
    }

    /**
     * @param p_inputLine the original line read from the batch input file.
     */
    public void setInputLine( String p_inputLine )
    {
        i_inputLine = p_inputLine;
    }

    /**
     * @param p_outputLine the output (log) message.
     */
    public void setOutputLine( String p_outputLine )
    {
        i_outputLine = p_outputLine;
    }

    /**
     * @param p_recoveryLine the recovery message
     */
    public void setRecoveryLine( String p_recoveryLine )
    {
        i_recoveryLine = p_recoveryLine;
    }

    /**
     * Sets the retry record indicator.
     * @param p_retryRecord the retry record indicator.
     */
    public void setRetryRecord( boolean p_retryRecord )
    {
        i_retryRecord = p_retryRecord;
    }

    /**
     * @param p_rowNumber the current row number.
     */
    public void setRowNumber( long p_rowNumber )
    {
        i_rowNumber = p_rowNumber;
    }
 
    /**
     * Keep the the number of previous successfully processed records.
     * @param p_counter
     */
    public void setNumberOfSuccessfullyProcessedRecords( int p_counter )
    {
        i_numberOfSuccessfullyProcessedRecords = p_counter;
    }

    /**
     * Keep the number of previous unsuccessfully processed records.
     * @param p_counter
     */
    public void setNumberOfErrorRecords( int p_counter )
    {
        i_numberOfErrorRecords = p_counter;
    }
    
    /**
     * Flag if this record failed last time it was processed.
     * @param p_flag
     */
    public void setFailureStatus( boolean p_flag )
    {
        i_failureFlag = p_flag;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_toString = "toString";
    /**
     * @return the record content in a readable format.
     */   
    protected String getDumpRecord()
    {
        StringBuffer l_tmp = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10830,
                this,
                C_ENTERING + C_METHOD_toString);
        }
        
        l_tmp.append( "inputFileName = ");
        l_tmp.append( this.i_inputFilename );
        l_tmp.append( C_DELIMITER );

        l_tmp.append( "rowNumber     = ");
        l_tmp.append( this.i_rowNumber );
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( "inputLine     = ");
        l_tmp.append( this.i_inputLine );
        l_tmp.append( C_DELIMITER );

        l_tmp.append( "curruptLine   = ");
        l_tmp.append( this.i_corruptLine );
        l_tmp.append( C_DELIMITER );
                
        l_tmp.append( "errorLine     = " );
        l_tmp.append( this.i_errorLine );
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( "recoveryLine  = " );
        l_tmp.append( this.i_recoveryLine );
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( "outputLine    = " );
        l_tmp.append( this.i_outputLine );
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( "retryRecord   = " );
        l_tmp.append( this.i_retryRecord );
        l_tmp.append( C_DELIMITER );

        l_tmp.append( "numberOfSuccessfullyProcessedRecords   = " );
        l_tmp.append( this.i_numberOfSuccessfullyProcessedRecords );
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( "numberOfErrorRecords   = " );
        l_tmp.append( this.i_numberOfErrorRecords );
        l_tmp.append( C_DELIMITER );

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10840,
                this,
                C_LEAVING + C_METHOD_toString);
        }

        return l_tmp.toString();  
              
    } // End of toString()
    
} // End of public class BatchDataRecord
