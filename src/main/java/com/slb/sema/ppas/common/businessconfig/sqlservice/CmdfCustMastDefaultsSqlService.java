////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CmdfCustMastDefaultsSqlService.Java
//      DATE            :      26-Mar-2001
//      AUTHOR          :      Sally Wells
//      REFERENCE       :
//
//      COPYRIGHT       :      WM-data 2005
//
//      DESCRIPTION     :      A service to manage persistance of
//                             cmdf data to the database.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that implements a service to manage persistance of cmdf data
 * to the database.
 */
public class CmdfCustMastDefaultsSqlService
{

    //------------------------------------------------------------------------
    // Class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CmdfCustMastDefaultsSqlService";


    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /**
     * The Logger to be used to log exceptions generated within this class.
     */
    protected Logger           i_logger = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new cmdf sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public CmdfCustMastDefaultsSqlService(
        PpasRequest            p_request,
        Logger                 p_logger)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10110, this,
                "Constructing CmdfCustMastDefaultsSqlService(...)");
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10150, this,
                "Constructed CmdfCustMastDefaultsSqlService(...)");
        }
    } // End of public constructor
      //         CmdfCustMastDefaultsSqlService(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_read = "read";
    /**
     * Read the cmdf set from the database where the rows are not marked
     * as deleted.
     * 
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @return  A set of customer default data objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public CmdfCustMastDefaultsDataSet read(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement               l_statement;
        SqlString                   l_sql;
        JdbcResultSet               l_resultSet = null;
        CmdfCustMastDefaultsData    l_cmdfData;
        Vector                      l_cmdfDataSetV;
        CmdfCustMastDefaultsDataSet l_cmdfDataSet;
        CmdfCustMastDefaultsData    l_cmdfCustMastDefaultsARR[];
        CmdfCustMastDefaultsData    l_cmdfCustMastDefaultsEmptyARR[] =
                new CmdfCustMastDefaultsData[0];
        int                         l_srva;
        int                         l_sloc;
        Market                      l_market;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, C_CLASS_NAME, 95020, this,
                "Entered read");
        }

        l_cmdfDataSetV = new Vector(20, 10);

        l_sql = new SqlString(1000, 0, "SELECT cmdf_srva,"                +
                      " cmdf_sloc,"                +
                      " cmdf_cust_class,"          +
                      " cmdf_default_language,"    +
                      " cmdf_opid,"                +
                      " cmdf_ymdhms,"              +
                      " cmdf_del_flag,"            +
                      " cmdf_currency,"            +
                      " cmdf_promo_plan,"          +
                      " cufm_precision "           +
               " FROM   cmdf_cust_mast_defaults, cufm_currency_formats " +
               " WHERE  cmdf_currency = cufm_currency");

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_read, 90050, this, p_request);

        l_resultSet = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_read, 90090, this, p_request, l_sql );

        while (l_resultSet.next(30940))
        {
            l_srva   = l_resultSet.getInt(    39480, "cmdf_srva" );
            l_sloc   = l_resultSet.getInt(    39490, "cmdf_sloc" );
            l_market = new Market(l_srva, l_sloc);

            l_cmdfData = new CmdfCustMastDefaultsData(
                  p_request,
                  l_market,
                  l_resultSet.getServiceClass(39500, "cmdf_cust_class" ),
                  l_resultSet.getTrimString(39510, "cmdf_default_language"),
                  l_resultSet.getTrimString(39520, "cmdf_opid"),
                  l_resultSet.getDateTime(39530,   "cmdf_ymdhms"),
                  l_resultSet.getChar(39540,       "cmdf_del_flag"),
                  new PpasCurrency(l_resultSet.getTrimString( 39550, "cmdf_currency" ),
                                   l_resultSet.getInt(33955, "cufm_precision")),
                  l_resultSet.getString(69003, "cmdf_promo_plan"));

            l_cmdfDataSetV.add(l_cmdfData);

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_SQL, C_CLASS_NAME, 96100, this,
                    l_cmdfData.toString() );
            }
        }

        l_resultSet.close(83470);

        l_statement.close(C_CLASS_NAME, C_METHOD_read, 91160, this, null);

        l_cmdfCustMastDefaultsARR =
                (CmdfCustMastDefaultsData[])l_cmdfDataSetV.toArray(
                     l_cmdfCustMastDefaultsEmptyARR);

        l_cmdfDataSet = new CmdfCustMastDefaultsDataSet(
                                       p_request,
                                       i_logger,
                                       l_cmdfCustMastDefaultsARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, C_CLASS_NAME, 98010, this,
                "Leaving read");
        }

        return(l_cmdfDataSet);
    } // End of public method
      //       read(PpasRequest, long, Connection)
      
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Update a row in the cmdf_cust_mast_defaults table.
     * 
     * @param p_request The request to process
     * @param p_connection Database Connection
     * @param p_opid Username of Operator who last updated this row
     * @param p_defaultLanguage Default language associated with this service class
     * @param p_defaultCurrency Default currency associated with this service class
     * @param p_defaultPromoPlan Default promotion plan associated with this service class
     * @param p_srva The service area for this market
     * @param p_sloc The service location for this market
     * @param p_code Service class code
     * @throws PpasSqlException Any exception that occurs due to a database transaction
     */
    public void update(PpasRequest     p_request,
                       JdbcConnection  p_connection,
                       String          p_opid,
                       String          p_defaultLanguage,
                       String          p_defaultCurrency,
                       String          p_defaultPromoPlan,
                       String          p_srva,
                       String          p_sloc,
                       String          p_code)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 31000, this,
                "Entered " + C_METHOD_update);
        }

        l_sql = new String("UPDATE cmdf_cust_mast_defaults " +
                           "SET cmdf_default_language = {6}, " +
                               "cmdf_currency = {7}, " +
                               "cmdf_promo_plan = {8}, " +
                               "cmdf_ymdhms = {5}, " +
                               "cmdf_del_flag = {0}, " +
                               "cmdf_opid = {4} " +
                           "WHERE cmdf_srva = {1} " +
                           "AND cmdf_sloc = {2} " +
                           "AND cmdf_cust_class = {3}");

        l_sqlString = new SqlString(31050, 9, l_sql);
                
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_srva);
        l_sqlString.setStringParam  (2, p_sloc);
        l_sqlString.setStringParam  (3, p_code);        
        l_sqlString.setStringParam  (4, p_opid);
        l_sqlString.setDateTimeParam(5, l_now);
        l_sqlString.setStringParam  (6, p_defaultLanguage);
        l_sqlString.setStringParam  (7, p_defaultCurrency);
        l_sqlString.setStringParam  (8, p_defaultPromoPlan);        

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 31100, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       31150,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        { 
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 31155, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31160, 
                    this,
                    "Database error: unable to update row in cmdf_cust_mast_defaults table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_update,
                                               31175,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 31300, this,
                "Leaving " + C_METHOD_update);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Insert a row in the cmdf_cust_mast_defaults table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Username of Operator who last updated this row
     * @param p_defaultLanguage Default language associated with this service class
     * @param p_defaultCurrency Default currency associated with this service class
     * @param p_defaultPromoPlan Default promotion plan associated with this service class
     * @param p_srva The service area for this market
     * @param p_sloc The service location for this market
     * @param p_code Service class code
     * @throws PpasSqlException Any exception that occurs due to a database transaction
     */
    public void insert(PpasRequest     p_request,
                       JdbcConnection  p_connection,
                       String          p_opid,
                       String          p_defaultLanguage,
                       String          p_defaultCurrency,
                       String          p_defaultPromoPlan,
                       String          p_srva,
                       String          p_sloc,
                       String          p_code)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 31400, this,
                "Entered " + C_METHOD_insert);
        }

        l_sql = new String(
                    "INSERT INTO cmdf_cust_mast_defaults "
                        + "("
                        + "CMDF_SRVA,"
                        + "CMDF_SLOC,"
                        + "CMDF_CUST_CLASS,"
                        + "CMDF_DEFAULT_LANGUAGE,"
                        + "CMDF_OPID, "
                        + "CMDF_YMDHMS, "
                        + "CMDF_DEL_FLAG, "
                        + "CMDF_CURRENCY, "
                        + "CMDF_PROMO_PLAN"
                        + ")" +        
                        "VALUES({1}, {2}, {3}, {6}, {4}, {5}, {0}, {7}, {8})");    

        l_sqlString = new SqlString(11500, 9, l_sql);
               
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_srva);
        l_sqlString.setStringParam  (2, p_sloc);
        l_sqlString.setStringParam  (3, p_code);        
        l_sqlString.setStringParam  (4, p_opid);
        l_sqlString.setDateTimeParam(5, l_now);
        l_sqlString.setStringParam  (6, p_defaultLanguage);
        l_sqlString.setStringParam  (7, p_defaultCurrency);
        l_sqlString.setStringParam  (8, p_defaultPromoPlan);
        
        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       31600,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       31700,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        { 
            l_statement.close( C_CLASS_NAME, C_METHOD_insert, 31155, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31800, 
                    this,
                    "Database error: unable to insert row in cmdf_cust_mast_defaults table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_insert,
                                               31900,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 32000, this,
                "Leaving " + C_METHOD_insert);
        }
        return;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Mark a row as having been deleted in cmdf_cust_mast_defaults.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_srva The service area for this market
     * @param p_sloc The service location for this market
     * @param p_code Service class code
     * @param p_opid Username of Operator who last updated this row 
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest     p_request,
                       JdbcConnection  p_connection,
                       String          p_srva,
                       String          p_sloc,
                       String          p_code,
                       String          p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement  = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 32100, this,
                "Entered " + C_METHOD_delete);
        }
            
        l_sql = new String("UPDATE cmdf_cust_mast_defaults " +
                           "SET cmdf_del_flag = {0}, " +
                               "cmdf_opid = {4}, " +
                               "cmdf_ymdhms = {5} " +
                           "WHERE cmdf_srva = {1} " +
                           "AND cmdf_sloc = {2} " +
                           "AND cmdf_cust_class = {3}");

        l_sqlString = new SqlString(32200, 6, l_sql);
        
        l_sqlString.setCharParam    (0, '*');
        l_sqlString.setStringParam  (1, p_srva);
        l_sqlString.setStringParam  (2, p_sloc);
        l_sqlString.setStringParam  (3, p_code);        
        l_sqlString.setStringParam  (4, p_opid);
        l_sqlString.setDateTimeParam(5, l_now);        

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       32300,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       32400,
                                       this,
                                       p_request,
                                       l_sqlString);

        }
        finally
        { 
            l_statement.close( C_CLASS_NAME, C_METHOD_insert, 32500, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31800, 
                    this,
                    "Database error: unable to delete row from cmdf_cust_mast_defaults table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_delete,
                                               32600,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 32700, this,
                "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";    
    /**
     * Marks a withdrawn service class detail record as available in the 
     * cmdf_cust_mast_defaults database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_userOpid Operator updating the business config record.
     * @param p_srva The service area for this market
     * @param p_sloc The service location for this market
     * @param p_code Service class code
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                String         p_srva,
                                String         p_sloc,
                                String         p_code,                                
                                String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10300, 
                this,
                "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE cmdf_cust_mast_defaults " +
                "SET cmdf_del_flag = {0}, " +
                    "cmdf_opid = {4}, " +
                    "cmdf_ymdhms = {5} " +
                "WHERE cmdf_srva = {1} " +
                "AND cmdf_sloc = {2} " +
                "AND cmdf_cust_class = {3}";

        l_sqlString = new SqlString(500, 6, l_sql);
        
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_srva);
        l_sqlString.setStringParam  (2, p_sloc);
        l_sqlString.setStringParam  (3, p_code);        
        l_sqlString.setStringParam  (4, p_userOpid);
        l_sqlString.setDateTimeParam(5, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME,
                               C_METHOD_markAsAvailable,
                               10330,
                               this,
                               p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10340, 
                    this,
                    "Database error: unable to update row in cmdf_cust_mast_defaults table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10360, 
                this,
                "Leaving " + C_METHOD_markAsAvailable);
        }
    }       
} // End of public class CmdfCustMastDefaultsSqlService
