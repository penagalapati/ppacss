////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionTimeServiceExpiry.java
//      DATE            :       09-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              time since Service Expiry.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn; 

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.SubscriberLifecycleSnapshotData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasProperties;

/** Identify whether a customer qualifies for churn based on time since Service Expiry. */
public class ChurnConditionTimeServiceExpiry extends ChurnCondition  
{
    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C5 - Time since Service Expiry. ";
    
    /** Number of days since Service Expiry threshold. */
    private int i_serviceExpiryThreshold;
    
    /** Constructor for a Churn condition based on time since Service Expiry.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     */
    public ChurnConditionTimeServiceExpiry(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
    {
        super(p_properties, p_bcCache, p_analyser);

        i_serviceExpiryThreshold = p_properties.getIntProperty(C_PROP_PREFIX + "serviceExpiredThreshold", 0);
        
        i_analyser.setNeedsSnapshot();
    }
        
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason) 
    {
        SubscriberLifecycleSnapshotData l_snapshot = p_data.getSnapshot();

        p_reason.append(C_CONDITION_NAME);

        p_reason.append("Subscribers Status is '");
        p_reason.append(l_snapshot.getServiceStatus());
        p_reason.append("'");
        
        if ( !l_snapshot.getServiceStatus().equals(String.valueOf(AccountData.C_SERVICE_EXPIRED_CHAR)) )
        {
            p_reason.append(" - Service not expired. ");
            p_reason.append(C_NO_CHURN_TEXT);
            
            return false;
        }
        
        PpasDateTime l_when = l_snapshot.getServStatusYmdhms();

        if (l_when == null || !l_when.isSet())
        {
            // Should not happen - status is set so date/time should be. Assume really old data.
            
            p_reason.append(". ");
            p_reason.append(C_CHURN_TEXT);
            
            return false;
        }
        
        int l_days = l_when.getDaysDiff(DatePatch.getDateTimeNow());
        
        boolean l_result = l_days > i_serviceExpiryThreshold;

        p_reason.append(" - expired ");
        p_reason.append(l_days);
        p_reason.append(" days ago, ");
        p_reason.append(l_days > i_serviceExpiryThreshold ? "before" : l_days == i_serviceExpiryThreshold ? "on" : "after");
        p_reason.append(" the threshold of ");
        p_reason.append(i_serviceExpiryThreshold);
        p_reason.append(" days. ");
        p_reason.append(l_result ? C_CHURN_TEXT : C_NO_CHURN_TEXT);
        
        return l_result;
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if Service expiry > " + i_serviceExpiryThreshold + " days ago.";
    }
}
