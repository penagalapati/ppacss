////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MfltMiscFieldTitlesDataSet.Java
//      DATE            :       29-January-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1218/4955
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A Set (collection) of misc. field title data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.HashMap;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A Set (collection) of miscellaneous field title data.
 */
public class MfltMiscFieldTitlesDataSet extends DataSetObject
{

    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MfltMiscFieldTitlesDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
   
    /** A Set of MfltMiscFieldTitlesData objects. */
    private Vector   i_mfltMiscFieldTitlesDataSetV;

    /** A HashMap of MfltMiscFieldTitlesData objects. */
    private HashMap  i_mfltMiscFieldTitlesDataSetHM;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /** 
     * Creates a new set of miscellaneous field title data containing an empty
     * Vector. 
     * 
     * @param p_request The request to process.
     */
    public MfltMiscFieldTitlesDataSet( PpasRequest p_request )
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }
        
        i_mfltMiscFieldTitlesDataSetHM = new HashMap();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10020, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
      //         MfltMiscFieldTitlesDataSet(PpasRequest)

    /** 
     * Creates a new set of miscellaneous field title data containing an empty
     * Vector. 
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     */
    public MfltMiscFieldTitlesDataSet(
        PpasRequest p_request,
        int         p_initialVSize,
        int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10030, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_mfltMiscFieldTitlesDataSetV = new Vector( p_initialVSize, p_vGrowSize);
        i_mfltMiscFieldTitlesDataSetHM = new HashMap();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10040, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
      //         MfltMiscFieldTitlesDataSet(PpasRequest)

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** Adds element to MfltMiscFieldTitles Set.
     *  @param  p_mfltMiscFieldTitlesData miscellaneous field title data to be 
     *          added to Vector.
     */
    public void addMiscFieldTitle( 
        MfltMiscFieldTitlesData p_mfltMiscFieldTitlesData)
    {
        i_mfltMiscFieldTitlesDataSetV.addElement( p_mfltMiscFieldTitlesData);
    }

    /** Returns a MfltMiscFieldTitlesData Object for the market passed in.
     * 
     * @param p_market The market for which miscellaneous fields data is required.
     * @return MfltMiscFieldTitlesData
     */
    public synchronized MfltMiscFieldTitlesData getMfltMiscFieldTitlesData(Market p_market )
    {
        MfltMiscFieldTitlesData l_mfltMiscFieldTitlesData = null;
        MfltMiscFieldTitlesData l_globalMktMfltMiscFieldTitlesData = null;
        Market                  l_market = null;
        boolean                 l_found = false;
        int                     l_len = 0;

        // Check the HashMap
        l_mfltMiscFieldTitlesData = (MfltMiscFieldTitlesData)i_mfltMiscFieldTitlesDataSetHM.get(p_market);
        if (l_mfltMiscFieldTitlesData == null)
        {
            // Look in the Vector
            if (i_mfltMiscFieldTitlesDataSetV != null)
            {
                l_len = i_mfltMiscFieldTitlesDataSetV.size();
                for (int l_i = 0; l_i < l_len; l_i++)
                {
                    l_mfltMiscFieldTitlesData = 
                            (MfltMiscFieldTitlesData)i_mfltMiscFieldTitlesDataSetV
                                                    .elementAt(l_i);
                                                
                    l_market = l_mfltMiscFieldTitlesData.getMarket();                                
                    if ( l_market.equals(Market.C_GLOBAL_MARKET) )
                    {
                        l_globalMktMfltMiscFieldTitlesData = l_mfltMiscFieldTitlesData;
                    }
                    else
                    if ( l_market.equals(p_market) )
                    {
                        l_found = true;
                        break;
                    }
                }
            }

            if (l_found)
            {
                // Put entry in HashMap
                i_mfltMiscFieldTitlesDataSetHM.put(p_market, l_mfltMiscFieldTitlesData);
            }
            else
            {
                // If GlobalMarket entry has been found
                if (l_globalMktMfltMiscFieldTitlesData != null)
                {
                    // Create a new entry from GlobalMarket
                    l_mfltMiscFieldTitlesData = 
                            new MfltMiscFieldTitlesData(
                                    null, // p_request
                                    p_market,
                                    l_globalMktMfltMiscFieldTitlesData.getTitle(),
                                    l_globalMktMfltMiscFieldTitlesData.getFieldTitlesVector(),
                                    l_globalMktMfltMiscFieldTitlesData.getOpid(),
                                    l_globalMktMfltMiscFieldTitlesData.getGenYmdhms());
                    
                    // Put entry in HashMap
                    i_mfltMiscFieldTitlesDataSetHM.put(p_market, l_mfltMiscFieldTitlesData);                    
                }
                else 
                {
                    l_mfltMiscFieldTitlesData = null;
                }
            }
        }

        return(l_mfltMiscFieldTitlesData);

    } // end of getMfltMiscFieldTitlesData(...)

    /** Returns the MfltMiscFieldTitlesData Set as a Vector.
     *  @return Vector of MfltMiscFieldTitlesData.
     */
    public Vector getMfltMiscFieldTitlesSetAsVector()
    {
        return( i_mfltMiscFieldTitlesDataSetV );
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_mfltMiscFieldTitlesDataSetV, p_sb);
    }
}

