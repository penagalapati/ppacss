////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DeciDefaultChargingIndData.java
//      DATE            :       17-Mar-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS_DEV_SS_?
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Data object representing one row from the
//                              deci_default_charging_ind table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Data object representing one row from the deci_default_charging_ind table.
 */
public class DeciDefaultChargingIndData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DeciDefaultChargingIndData";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The start of he number range that this row is configured for. */
    private String i_numberSeriesStart = null;
    
    /** The end of he number range that this row is configured for. */
    private String i_numberSeriesEnd = null;
    
    /** The charging indicator that this row represents. */
    private String i_chargingInd = null;
    
    /** The charging indicator description retrieved from FACH. */
    private String i_description = null;
    
    /** Indicates if this record is marked as deleted or not. */
    private boolean i_isDeleted = false;
    
    /** Opid that last updated this record. */
    private String i_opid = null;
    
    /** Date/time of last update. */
    private PpasDateTime i_genYmdhms = new PpasDateTime("");
    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of DeciDefaultChargingIndData.
     * @param p_numberStart the start of them number range this object represents
     * @param p_numberEnd the end of them number range this object represents
     * @param p_chargingInd charging indicator used for this number prefix
     * @param p_description charging indicator description
     * @param p_isDeleted deleted flag
     * @param p_opid opid
     * @param p_genYmdhms gen date/time
     */
    public DeciDefaultChargingIndData(String       p_numberStart,
                                      String       p_numberEnd,
                                      String       p_chargingInd,
                                      String       p_description,
                                      boolean      p_isDeleted,
                                      String       p_opid,
                                      PpasDateTime p_genYmdhms)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_numberSeriesStart = p_numberStart;
        i_numberSeriesEnd   = p_numberEnd;
        i_chargingInd       = p_chargingInd;
        i_description       = p_description;
        i_isDeleted         = p_isDeleted;
        i_opid              = p_opid;
        i_genYmdhms         = p_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10010, this,
                "Constructed" + C_CLASS_NAME);
        }
    }
    
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get start number of this range.
     * @return start number prefix for this record
     */
    public String getNumberStart()
    {
        return i_numberSeriesStart;
    }

    /**
     * Get end number of this range.
     * @return end number prefix for this record
     */
    public String getNumberEnd()
    {
        return i_numberSeriesEnd;
    }

    /**
     * Get charging indicator.
     * @return charging indicator for this record
     */
    public String getChargingInd()
    {
        return i_chargingInd;
    }

    /**
     * Get charging indicator desciption.
     * @return charging indicator desciption
     */
    public String getDescription()
    {
        return i_description;
    }
    
    /**
     * Is this record marked as deleted.
     * @return deleted indicator
     */
    public boolean isDeleted()
    {
        return i_isDeleted;
    }

    /**
     * Get date/time of last update.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }

    /**
     * Opid that last updated the row.
     * @return opid
     */
    public String getOpid()
    {
        return i_opid;
    }
}
