////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScpiScpInfoSqlService.Java
//      DATE            :       21-Jan-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the ACP information business
//                              config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 16/11/05 | Yang L.    | Copyright info changed.         | PpacLon#1755/7463
//----------+------------+---------------------------------+--------------------
// 16/11/05 | Yang L.    | Four new methods are added:     | PpacLon#1755/7463
//          |            | * insert(...)                   |
//          |            | * update(...)                   |
//          |            | * delete(...)                   |
//          |            | * markAsAvailable(...)          |
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction      | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
//10/09/06  | Paul Rosser| Add new scpi transitioned       | PRD_ASCS00_GEN_CA_108
//          |            | column to scpi table. Get data. | PpacLon#2858/10794
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Implements a service that performs the SQL required to populate the
 * SCP information business config cache.
 */
public class ScpiScpInfoSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScpiScpInfoSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Sql service to be used in generating the data for the
     * SCP information business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public ScpiScpInfoSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 51310, this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 51390, this,
                            "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all SCP information data (including ones marked as
     * deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return A data set containing all the SCP information records read from
     *         the database.
     * @throws PpasSqlException If the data cannot be read.
     */
    public ScpiScpInfoDataSet readAll (PpasRequest p_request, JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement          l_statement;
        SqlString              l_sql;
        String                 l_scpId;
        String                 l_scpIpAddress;
        String                 l_description;
        PpasDateTime           l_genYmdhms;
        String                 l_opid;
        char                   l_delFlag;
        char                   l_networkType;
        Character              l_transitioned;
        ScpiScpInfoData        l_scpiRecord;
        Vector                 l_allScpInfoV = new Vector (20, 10);
        ScpiScpInfoData        l_allScpInfoARR[];
        ScpiScpInfoData        l_scpInfoEmptyARR[] = new ScpiScpInfoData[0];
        JdbcResultSet          l_results;
        ScpiScpInfoDataSet     l_allScpInfoDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 51410, this,
                            "Entered " +  C_METHOD_readAll);
        }

        l_sql =   new SqlString(200, 0, "SELECT " +
                                        "scpi_scp_id, " +
                                        "scpi_ip_address, " +
                                        "scpi_description, " +
                                        "scpi_gen_ymdhms, " +
                                        "scpi_opid, " +
                                        "scpi_del_flag, " +
                                        "scpi_network_type, " +
                                        "scpi_transitioned " +
                                        "FROM scpi_scp_info " +
                                        "ORDER BY scpi_scp_id");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll,
                                                   51420, this, p_request);
        
        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll,
                                             51430, this, p_request, 
                                             l_sql);
        while ( l_results.next(51440) )
        {
            l_scpId        = l_results.getTrimString (51460, "scpi_scp_id");
            l_scpIpAddress = l_results.getTrimString (51450, "scpi_ip_address");
            l_description  = l_results.getTrimString (51470, "scpi_description");
            l_genYmdhms    = l_results.getDateTime   (51520, "scpi_gen_ymdhms");
            l_opid         = l_results.getTrimString (51530, "scpi_opid");
            l_delFlag      = l_results.getChar       (51540, "scpi_del_flag");
            l_networkType  = l_results.getChar       (51545, "scpi_network_type");
            l_transitioned = l_results.getCharO      (51550, "scpi_transitioned");

            l_scpiRecord = new ScpiScpInfoData(p_request,
                                               l_scpId,
                                               l_scpIpAddress,
                                               l_description,
                                               l_genYmdhms,
                                               l_opid,
                                               l_delFlag,
                                               l_networkType,
                                               l_transitioned != null && l_transitioned.charValue() == 'Y'
                                               );

            l_allScpInfoV.addElement (l_scpiRecord);
        }

        l_results.close (51550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll,
                           51560, this, p_request);

        l_allScpInfoARR = (ScpiScpInfoData[])l_allScpInfoV.toArray (l_scpInfoEmptyARR);

        l_allScpInfoDataSet = new ScpiScpInfoDataSet (p_request, l_allScpInfoARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 51570, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return (l_allScpInfoDataSet);

    } // End of public method readAll
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a record into the SCPI_SCP_INFO database table.
     * 
     * @param p_request          The request to process.
     * @param p_connection       Database Connection.
     * @param p_scpId            SDP identifier.
     * @param p_description      SDP ID Description.
     * @param p_opid             Operator updating the business config record.
     * @param p_scpIpAddress     The IP address of the SDP node.
     * @throws PpasSqlException  Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_scpId,
                       String         p_description,
                       String         p_opid,
                       String         p_scpIpAddress)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10000, 
                this,
                "Entered " + C_METHOD_insert);
        }
        
        l_sql = "INSERT INTO SCPI_SCP_INFO (scpi_scp_id, " +
                                           "scpi_ip_address, " +
                                           "scpi_description," +
                                           "scpi_gen_ymdhms, " + 
                                           "scpi_opid, " + 
                                           "scpi_del_flag, " + 
                                           "scpi_network_type) " + 
                                           "VALUES({0},{1},{2},{3},{4}, ' ', 'G')";
        
        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_scpId);
        l_sqlString.setStringParam  (1, p_scpIpAddress);
        l_sqlString.setStringParam  (2, p_description);
        l_sqlString.setDateTimeParam(3, l_now);
        l_sqlString.setStringParam  (4, p_opid);
             
        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 10010, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_insert,
                                                   10020, this, p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 10030, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10040, 
                    this,
                    "Database error: unable to insert row in SCPI_SCP_INFO table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_insert, 
                                       10050, this, p_request,
                                       0, SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10060, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    }
   
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates a scpi record in the SCPI_SCP_INFO database table.
     * 
     * @param p_request          The request to process.
     * @param p_connection       Database Connection.
     * @param p_scpId            SDP identifier.
     * @param p_sdpDesc          SDP description.
     * @param p_opid             Operator ID.
     * @throws PpasSqlException  Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_scpId,
                       String         p_sdpDesc,
                       String         p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10100, 
                this,
                "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE SCPI_SCP_INFO " +
                "SET SCPI_DESCRIPTION = {0}, " +
                "SCPI_OPID = {1}, " +
                "SCPI_GEN_YMDHMS = {2}, " +
                "SCPI_DEL_FLAG = ' ' " +
                "WHERE scpi_scp_id = {3} " ;

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_sdpDesc);
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setStringParam  (3, p_scpId);
        
        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 10110, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_update,
                                                   10120, this, p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 10130, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR, 
                                C_CLASS_NAME, 10140, this,
                                "Database error: unable to update row in SCPI_SCP_INFO table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_update, 
                                       10150, this, p_request,
                                       0, SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10160, this,
                            "Leaving " + C_METHOD_update);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the SCPI_SCP_INFO database table.
     * 
     * @param p_request      The request to process.
     * @param p_connection   Database Connection.
     * @param p_opId         Operator identifier.
     * @param p_scpId        SCP identifier.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opId,
                       String         p_scpId)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
     
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10200, this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = "UPDATE SCPI_SCP_INFO " +
                "SET SCPI_DEL_FLAG = '*', " +
                "SCPI_OPID = {1}, " +
                "SCPI_GEN_YMDHMS = {2} " +
                "WHERE SCPI_SCP_ID = {0}" ;

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_scpId);
        l_sqlString.setStringParam  (1, p_opId);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());
       
        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete,
                                                       10210, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_delete,
                                                   10220, this, p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 10230, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10240, this,
                                "Database error: unable to delete row from SCPI_SCP_INFO table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_delete,
                                       10250, this, p_request, 
                                       0, SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10260, this,
                            "Leaving " + C_METHOD_delete);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn SDP identifiers record as available in the 
     * SCPI_SCP_INFO database table.
     * 
     * @param p_request    The request to process.
     * @param p_connection Database Connection.
     * @param p_sdpId      SDP identifier.
     * @param p_opId       Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest     p_request,
                                JdbcConnection  p_connection,
                                String          p_sdpId,
                                String          p_opId)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10300, this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE SCPI_SCP_INFO " +
                "SET SCPI_DEL_FLAG = {0}, " +
                "SCPI_OPID = {2}, " +
                "SCPI_GEN_YMDHMS = {3} " +
                "WHERE SCPI_SCP_ID = {1}";

        l_sqlString = new SqlString(500, 4, l_sql);
        
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_sdpId);
        l_sqlString.setStringParam  (2, p_opId);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_markAsAvailable,
                                                       10310, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_markAsAvailable,
                                                   10320, this, p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10340, this,
                                "Database error: unable to update row in scpi_scp_info table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME, C_METHOD_markAsAvailable,
                                              10350, this, p_request,
                                              0, SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10360, this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }    

}// End of public class ScpiScpInfoSqlService
