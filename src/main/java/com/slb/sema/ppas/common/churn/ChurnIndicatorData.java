////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnIndicatorData.java
//      DATE            :       27-Mar-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Analysis data defining whether a customer is likely 
//                              to 'churn'. That is, likely to move to another operator.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.EventHistoryDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.PaymentDataSet;
import com.slb.sema.ppas.common.dataclass.RefillData;
import com.slb.sema.ppas.common.dataclass.SubscriberLifecycleSnapshotData;
import com.slb.sema.ppas.common.dataclass.VoucherDataSet;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;


/** Analysis data defining whether a customer is likely to 'churn'. That is, likely to move to another operator. */
public class ChurnIndicatorData extends DataObject
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChurnIndicatorData";
    
    /** Length of a period. Value is {@value}. */
    public static final int C_PERIOD_LENGTH = 30;
    
    /** High ranking - large value of refills per month. Value is {@value}. */
    public static final char C_RANK_HIGH   = 'H';

    /** Medium ranking - normal value of refills per month. Value is {@value}. */
    public static final char C_RANK_MEDIUM = 'M';

    /** Low ranking - low value of refills per month. Value is {@value}. */
    public static final char C_RANK_LOW    = 'L';

    /** Churn Analyser. */
    private ChurnAnalyser i_analyser;
    
    /** Current balance of the account. */
    private Money i_balance;
    
    /** Current balance in subscribers currency. */
    private Money i_balanceOwnCcy;
    
    /** Basic details of this account. */
    private BasicAccountData i_account;
    
    /** Subscriber Snapshot Data. */
    private SubscriberLifecycleSnapshotData i_snapshot;
    
    /** Date of yesterday. All offsets will be calculated from this date - i.e. todays data will be ignored. */
    private PpasDateTime i_yesterday;
    
    /** Date of start of analysis start. */
    private PpasDateTime i_analysisStart;
    
    /** Number of days for analysis peiod. */
    private int          i_analysisDays;
    
    /** Total Value of refills. */
    private Money i_totalValue;
    
    /** Data periods. */
    private Vector i_periods = new Vector(); 
    
    /** Flag indicating there is insufficient data to perform Churn Analysis. */
    private boolean i_insufficientData;
    
    /** Average number of days per period. That is, 30 / number of days. */
    private double i_averageDaysPeriod;

    /** Number of periods to track refills. */
    private int i_trackingPeriod;
    
    /** Value of refills during tracking period. */
    private Money i_trackingValue;
    
    /** Value of refills in remaining periods. */
    private Money i_remainingValue;
    
    /** Number of refills during tracking period. */
    private int i_trackingNumber;
    
    /** Number of refills in remaining periods. */
    private int i_remainingNumber;
    
    /** Base cuirrency of this account. */
    private PpasCurrency i_baseCcy;
    
    /** Zero amount in the base currency - exists so only need to create once. */
    private Money i_zeroAmt;
    
    /**
     * Standard constructor. Note: This is relatively sparse since data is expected to be added as entire sets.
     * 
     * @param p_analyser Churn Analyser.
     * @param p_balance  Current balance of the account.
     * @param p_account  Basic Account Details.
     */
    public ChurnIndicatorData(ChurnAnalyser p_analyser, Money p_balance, BasicAccountData p_account)
    {
        i_analyser      = p_analyser;
        i_balance       = p_balance;
        i_balanceOwnCcy = p_balance;
        i_account       = p_account;
        
        PpasDate l_activationDate = i_account.getActivationDate();
        
        if (l_activationDate == null || !l_activationDate.isSet())
        {
            setInsufficientData();
            return;
        }
    
        PpasDate l_earliestActivation = DatePatch.getDateToday();
        l_earliestActivation.add(PpasDate.C_FIELD_MONTH, -1 * i_analyser.getMinActPeriod());

        if (l_activationDate.after(l_earliestActivation))
        {
            setInsufficientData();
            return;
        }
        
        i_yesterday = new PpasDateTime(DatePatch.getDateToday().toString_yyyyMMdd() + " 23:59:59");
        i_yesterday.add(PpasDate.C_FIELD_DATE, -1);
        
        i_analysisDays  = p_analyser.getAnalysisPeriodDays();
        i_analysisStart = new PpasDateTime(i_yesterday.getPpasDate().toString_yyyyMMdd() + " 00:00:00");
        i_analysisStart.add(PpasDate.C_FIELD_DATE, -1 * i_analysisDays);

        if (l_activationDate.after(i_analysisStart))
        {
            i_analysisStart = new PpasDateTime(l_activationDate.toString_yyyyMMdd() + " 00:00:00");
            i_analysisDays  = i_analysisStart.getDaysDiff(i_yesterday) + 1;  // Include all of yesterday and start date - so +1
        }
        
        i_trackingPeriod = i_analyser.getTrackingPeriod();
        
        i_averageDaysPeriod = (double)C_PERIOD_LENGTH/i_analysisDays;
    }

    /** Set base currency for this subscriber - used for all calculations.
     * 
     * @param p_refillAmt Example refill amount.
     */
    private void setBaseCurrency(Money p_refillAmt)
    {
        i_baseCcy = p_refillAmt.getCurrency();
        
        i_zeroAmt = i_analyser.getZeroAmount(i_baseCcy);
        
        i_totalValue     = i_zeroAmt;
        i_trackingValue  = i_zeroAmt;
        i_remainingValue = i_zeroAmt;

        i_balanceOwnCcy = i_analyser.convertAmount(i_balance, i_baseCcy);
    }
    
    /** Add a set of Account Refills.
     * 
     * @param p_refills Set of account refills.
     */
    public void addRefills(PaymentDataSet p_refills)
    {
        addRefillData(p_refills);
    }

    /** Add a set of Voucher Refills.
     * 
     * @param p_refills Set of account refills.
     */
    public void addRefills(VoucherDataSet p_refills)
    {
        addRefillData(p_refills);
    }

    /** Add snapshot data.
     * 
     * @param p_snapshot Snapshot Data.
     */
    public void addSnapshot(SubscriberLifecycleSnapshotData p_snapshot)
    {
        i_snapshot = p_snapshot;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addRefillData = "addRefillData";
    /** Add refills to relevant buckets.
     * 
     * @param p_dataSet Refill Event History. Note: The public interfaces should ensure only refill history is passed through.
     */
    private void addRefillData(EventHistoryDataSet p_dataSet)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11000, this,
                            "Entered " + C_METHOD_addRefillData + " for '" + 
                            ((p_dataSet == null) ? null : p_dataSet.toVerboseString() + "'"));
        }
        
        Iterator l_it = p_dataSet.getEventHistory().iterator();
        
        while (l_it.hasNext())
        {
            // Filter out refills with a zero amount (Value Vouchers).
            RefillData l_refill = (RefillData)l_it.next();
            
            if (l_refill != null && l_refill.getAppliedValue() != null && l_refill.getAppliedValue().isNotZero())
            {
                if (i_baseCcy == null)
                {
                    setBaseCurrency(l_refill.getAppliedValue());
                }
                
                int l_period = calculatePeriod(l_refill.getEventDateTime().getPpasDate());
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 11100, this,
                            "Period: " + l_period);
                }
                
                // Ignore anything with negative period (refill made today)
                if (l_period >= 0)
                {
                    for (int i = i_periods.size(); i <= l_period; i++)
                    {
                        i_periods.add(new EventHistoryDataSet(null));
                    }
                    
                    ((EventHistoryDataSet)i_periods.get(l_period)).add(l_refill);
                    
                    try
                    {
                        i_totalValue     = i_totalValue.add(l_refill.getAppliedValue());
                    
                        if (l_period < i_trackingPeriod)
                        {
                            i_trackingValue  = i_trackingValue.add((l_refill.getAppliedValue()));
                            i_trackingNumber++;
                        }
                        else
                        {
                            i_remainingValue = i_remainingValue.add((l_refill.getAppliedValue()));
                            i_remainingNumber++;
                        }
                    }
                    catch (PpasConfigException e)
                    {
                        // Cannot happen - so software exception.
                        throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_addRefillData, 40010, null, null, 0,
                                                        SoftwareKey.get().unexpectedException(), e);
                    }
                }
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 11200, this,
                            "Leaving " + C_METHOD_addRefillData + " for '");
        }
    }
    
    /** Calculate the period a given date falls into.
     * 
     * @param p_when Date of refill.
     * @return Period the date falls into.
     */
    private int calculatePeriod(PpasDate p_when)
    {
        if (i_yesterday == null)
        {
            return 0;
        }
        
        int l_days = p_when.getDaysDiff(i_yesterday);
        
        return l_days < 0 ? -1 : l_days / C_PERIOD_LENGTH;
    }
    
    /** Set flag indicating there is insufficient data to analyse Churn. */
    public void setInsufficientData()
    {
        i_insufficientData = true;
    }
    
    /** Get flag indicating whether there is sufficient data to analyse the subscriber.
     * 
     * @return True if the account was not active long enough to analyse.
     */
    public boolean isInsufficientData()
    {
        return i_insufficientData;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCumulativePoints = "getCumulativePoints";
    /** Get refill amounts as a series of points with cumulative amounts.
     * 
     * @return Array of cumulative points.
     */
    public PointData[] getCumulativePoints()
    {
        Vector l_return = new Vector();
        
        Money l_cum = i_analyser.getZeroAmount(i_baseCcy);

        l_return.add(new PointData(i_analysisStart, l_cum));
        
        for (int i = i_periods.size() - 1; i >= 0; i--)
        {
            EventHistoryDataSet l_set = (EventHistoryDataSet)i_periods.get(i);

            RefillData[] l_refills = (RefillData[])l_set.getEventHistory().toArray(new RefillData[l_set.size()]);
            
            for (int j = l_refills.length - 1; j >= 0; j--)
            {
                RefillData l_refill = l_refills[j];

                try
                {
                    l_cum = l_cum.add(l_refill.getAppliedValue());
                }
                catch (PpasConfigException e)
                {
                    // Cannot happen - so software exception.
                    throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_getCumulativePoints, 30010, null, null, 0,
                            SoftwareKey.get().unexpectedException(), e);
                }
                
                // Ensure that there are no duplicates.This avoids errors when creating graphs from this data.
                // Note that the refills are being iterated through in reverse time order.
                l_refill.getEventDateTime().add(PpasDate.C_FIELD_MILLISECOND, 500 - j);
                l_return.add(new PointData(l_refill.getEventDateTime(), l_cum));
            }
        }
        
        return (PointData[])l_return.toArray(new PointData[l_return.size()]);
    }

    /** Get the status of the Churn Analysis.
     * 
     * @return Churn analysis.
     */
    public ChurnAnalysis getChurnAnalysis()
    {
        return i_analyser.analyse(this);
    }

    /** Get a ranging of the type of account.
     * 
     * @return 'H' for High, 'M' for Medium or 'L' for Low;
     */
    public char getRanking()
    {
        return i_analyser.getRanking(i_totalValue.multiply(i_averageDaysPeriod));
    }

    /** Get average spend over time for this operator.
     * 
     * @return Points that represent the average spend of a customer over time.
     */
    public PointData[] getOperatorAverage()
    {
        return getAveragePoints(i_analyser.getOperatorAverageDay());
    }

    /** Get High average spend over time for this operator.
     * 
     * @return Points that represent the average spend of a customer over time for those spending a lot.
     */
    public PointData[] getOperatorHigh()
    {
        return getAveragePoints(i_analyser.getOperatorAverageHighDay());
    }

    /** Get Low average spend over time for this operator.
     * 
     * @return Points that represent the average spend of a customer over time for those spending a little.
     */
    public PointData[] getOperatorLow()
    {
        return getAveragePoints(i_analyser.getOperatorAverageLowDay());
    }

    /** Get the 'average' points.
     * 
     * @param p_operatorAverageDays Operator daily average for low, high, etc.
     * @return Start and end points for operator daily averages.
     */
    private PointData[] getAveragePoints(Money p_operatorAverageDays)
    {
        return new PointData[] {new PointData(i_analysisStart, i_zeroAmt),
                                new PointData(i_yesterday,     p_operatorAverageDays.multiply(i_analysisDays))};
    }
    
    /** Get the earliest date/time for analysis.
     * 
     * @return Midnight on earliest analysis date (yesterday - number of periods).
     */
    public PpasDateTime getAnalysisStart()
    {
        return i_analysisStart;
    }
    
    /** Get the latest date/time for analysis.
     * 
     * @return Midnight on latest analysis date (end of yesterday).
     */
    public PpasDateTime getAnalysisEnd()
    {
        return i_yesterday;
    }
    
    /** Flag indicating whether the High/Low averages are configured.
     * 
     * @return True if High/Low balances are configured.
     */
    public boolean isHighLowAverageSet()
    {
        return i_analyser.isHighLowAverageSet();
    }
    
    /** Get the current balance of this account.
     * 
     * @return Current balance of this account.
     */
    public Money getCurrentBalance()
    {
        return i_balanceOwnCcy;
    }
    
    /** Get Customer Snapshot details.
     * 
     * @return Customer Snapshot.
     */
    public SubscriberLifecycleSnapshotData getSnapshot()
    {
        return i_snapshot;
    }
    
    /** Get the subscribers market.
     * 
     * @return Market of this subscriber.
     */
    public Market getMarket()
    {
        return i_account.getMarket();
    }

    /** Get the subscribers agent.
     * 
     * @return Agent associated with this subscriber.
     */
    public String getAgent()
    {
        return i_account.getAgent();
    }

    /** Get the number of days since the most recent refill.
     * 
     * @return Number of days since last refill.
     */
    public int getDaysSinceLastRefill()
    {
        for (int i = 0; i < i_periods.size(); i++)
        {
            TreeSet l_periodData = ((EventHistoryDataSet)i_periods.get(i)).getEventHistory();
 
            if (l_periodData.size() != 0)
            {
                RefillData l_latest = (RefillData)l_periodData.first();
 
                return l_latest.getEventDateTime().getDaysDiff(i_yesterday);
            }
        }
 
        return i_analysisDays;
    }

    
    /** Get the total value of refills during the tracking period.
     * 
     * @return Total value of refills during tracking period.
     */
    public Money getTrackingValue()
    {
        return i_trackingValue;
    }

    /** Get the average value during the tracking period.
     * 
     * @return Average value of refill during tracking period.
     */
    public Money getTrackingAverageValue()
    {
        return i_trackingValue.multiply((double)C_PERIOD_LENGTH/getTrackingDays());
    }

    /** Get the total number of refills during the tracking period.
     * 
     * @return Total number of refills during tracking period.
     */
    public int getTrackingNumber()
    {
        return i_trackingNumber;
    }

    /** Get the total number of refills.
     * 
     * @return Total number of refills.
     */
    public int getTotalRefillNumber()
    {
        return i_trackingNumber + i_remainingNumber;
    }

    /** Get the average number of refills during the tracking period.
     * 
     * @return Average number of refills during tracking period.
     */
    public double getTrackingAverageNumber()
    {
        return i_trackingNumber * (double)C_PERIOD_LENGTH/getTrackingDays();
    }

    /** Get the average value before the tracking period.
     * 
     * @return Average value of refill before tracking period.
     */
    public Money getRemainingAverageValue()
    {
        return i_remainingValue.multiply((double)C_PERIOD_LENGTH/getRemainingDays());
    }

    /** Get the average number of refills before the tracking period.
     * 
     * @return Average number of refills before tracking period.
     */
    public double getRemainingAverageNumber()
    {
        return i_remainingNumber * (double)C_PERIOD_LENGTH/getRemainingDays();
    }

    /** Get the number of days during the tracking period.
     * 
     * @return Number of days in tracking period.
     */
    private int getTrackingDays()
    {
        int l_trackingDays = i_trackingPeriod * C_PERIOD_LENGTH;
        
        if (l_trackingDays > i_analysisDays)
        {
            // Account not been in use for full tracking period. Should not happen but handle it.
            i_trackingPeriod = i_analysisDays;
        }

        return l_trackingDays == 0 ? C_PERIOD_LENGTH : l_trackingDays;
    }
    
    /** Get the number of days before the tracking period.
     * 
     * @return Number of days before tracking period.
     */
    private int getRemainingDays()
    {
        int l_remainingDays = i_analysisDays - getTrackingDays();
        
        // If remaining days is zero (i.e. analysis period is same as tracking period), assume one period to stop division errors.
        return l_remainingDays == 0 ? C_PERIOD_LENGTH : l_remainingDays;
    }
    
    /** Mapping of refill date/time to amount. */
    public static class PointData
    {
        /** Date/time of refill. */
        private PpasDateTime i_when;
        
        /** Amount of refill. */
        private Money        i_amount;
        
        /** Construct an object containing the date/time and amount of a refill.
         * 
         * @param p_when   Date/Time of refill.
         * @param p_amount Amount of refill.
         */
        public PointData(PpasDateTime p_when, Money p_amount)
        {
            i_when   = p_when;
            i_amount = p_amount;
        }
        
        /** Get the date/time of the refill.
         * 
         * @return Refill date/time.
         */
        public PpasDateTime getDateTime()
        {
            return i_when;
        }
        
        /** Get the amount of the refill.
         * 
         * @return Amount of refill.
         */
        public Money getAmount()
        {
            return i_amount;
        }

        /** Get a string representation of this object.
         * 
         * @return String describing this object.
         */
        public String toString()
        {
            return super.toString() + ": When: " + i_when + " Amt: " + i_amount;
        }
    }
}
