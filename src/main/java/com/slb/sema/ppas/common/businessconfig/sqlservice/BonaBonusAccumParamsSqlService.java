//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonsBonusSchemeSqlService.java
// DATE            :       30-June-2007
// AUTHOR          :       Michael Erskine
// REFERENCE       :       PRD_ASCS_GEN_CA_128
//
// COPYRIGHT       :       WM-data 2007
//
// DESCRIPTION     :       An SQL service to retrieve bonus scheme accumulation 
//                         parameters from the BONA_BONUS_ACCUM_PARAMS table.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** SQL service for accessing Bonus Scheme codes. */
public class BonaBonusAccumParamsSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonaBonusAccumParamsSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Bonus Accumumlaton Params sql service.
     * @param p_request The request to process.
     */
    public BonaBonusAccumParamsSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid Bonus Schemes (including ones marked as deleted).
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Bonus Schemes.
     * @throws PpasSqlException If the data cannot be read.
     */
    public BonaBonusAccumParamsDataSet readAll(PpasRequest p_request, JdbcConnection p_connection)
            throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        SqlString l_sql;
        Vector l_allBonaBonusAccumDataV = new Vector(20, 10);
        BonaBonusAccumParamsDataSet l_bonaBonusAccumParamsDataSet;
        BonaBonusAccumParamsData l_allBonaARR[];
        BonaBonusAccumParamsData l_bonaBonusAccumParamsEmptyARR[] = new BonaBonusAccumParamsData[0];
        JdbcResultSet l_results;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 10100, this,
                            "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(500, 0, "select bona_scheme_id," 
                                          + " bona_bonus_percent,"
                                          + " bona_adj_code,"
                                          + " bona_adj_type,"
                                          + " bona_ded_acc_id,"
                                          + " bona_refill_value_target,"
                                          + " bona_refill_count_target,"
                                          + " bona_counting_period_days,"
                                          + " bona_bonus_amount,"
                                          + " bona_ded_acc_expiry_days,"
                                          + " bona_set_ded_acc "
                                    + "from bona_bonus_accum_params");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL,
                            p_request, C_CLASS_NAME, 10110, this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while (l_results.next(10140))
        {
            char l_setDedAcc = l_results.getChar(10250, "bona_set_ded_acc");

            l_allBonaBonusAccumDataV.addElement(
                new BonaBonusAccumParamsData(l_results.getTrimString(10150, "bona_scheme_id"),
                                             l_results.getFloatO(10155, "bona_bonus_percent"),
                                             l_results.getTrimString(10160, "bona_adj_code"),
                                             l_results.getTrimString(10165, "bona_adj_type"),
                                             l_results.getInt(10170, "bona_ded_acc_id"),
                                             l_results.getDoubleO(10200, "bona_refill_value_target"),
                                             l_results.getIntO(10210, "bona_refill_count_target"),
                                             l_results.getInt(10220, "bona_counting_period_days"),
                                             l_results.getDoubleO(10230, "bona_bonus_amount"),
                                             l_results.getIntO(10240, "bona_ded_acc_expiry_days"),
                                             l_setDedAcc == 'Y'));
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10245, this, p_request);

        l_allBonaARR = (BonaBonusAccumParamsData[])l_allBonaBonusAccumDataV.toArray(l_bonaBonusAccumParamsEmptyARR);

        l_bonaBonusAccumParamsDataSet = new BonaBonusAccumParamsDataSet(p_request, l_allBonaARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 10250, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_bonaBonusAccumParamsDataSet;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Attempts to insert a new row into the database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_bonusSchemeId The bonus scheme identifier.
     * @param p_bonusPercent The bonus scheme bonus as a percentage.
     * @param p_adjCode The bonus adjustment code.
     * @param p_adjType The bonus adjustment type.
     * @param p_dedAccId The dedicated account to apply the bonus to (0 if main account).
     * @param p_refillValueTarget The refill target as a value.
     * @param p_refillCountTarget The refill target as a count.
     * @param p_countingPeriodDays The counting period.
     * @param p_bonusAmount The bonus scheme bonus as an amount.
     * @param p_dedAccExpiryDays The number of days before the bouns expires.
     * @param p_setDedicatedAcct Apply the bonus to the ded acc by set or adjustment?.
     * @return A count of the number of rows inserted.
     * @throws PpasSqlException If the data cannot be inserted.
     */
    public int insert(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String  p_opid,
                      String  p_bonusSchemeId,
                      Float   p_bonusPercent,
                      String  p_adjCode,
                      String  p_adjType,
                      int     p_dedAccId,
                      Double  p_refillValueTarget,
                      Integer p_refillCountTarget,
                      int     p_countingPeriodDays,
                      Double  p_bonusAmount,
                      Integer p_dedAccExpiryDays,
                      char    p_setDedicatedAcct) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " + C_METHOD_insert);
        }

        l_sql = "insert into bona_bonus_accum_params("
                +    " bona_scheme_id,"
                +    " bona_bonus_percent,"
                +    " bona_adj_code,"
                +    " bona_adj_type, "
                +    " bona_ded_acc_id,"
                +    " bona_refill_value_target,"
                +    " bona_refill_count_target,"
                +    " bona_counting_period_days,"
                +    " bona_bonus_amount,"
                +    " bona_ded_acc_expiry_days,"
                +    " bona_set_ded_acc)"
                + " values({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})";

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_bonusSchemeId);
        l_sqlString.setFloatParam(1, p_bonusPercent);
        l_sqlString.setStringParam(2, p_adjCode);
        l_sqlString.setStringParam(3, p_adjType);
        l_sqlString.setIntParam(4, p_dedAccId);
        l_sqlString.setDoubleParam(5, p_refillValueTarget);
        l_sqlString.setIntParam(6, p_refillCountTarget);
        l_sqlString.setIntParam(7, p_countingPeriodDays);
        l_sqlString.setDoubleParam(8, p_bonusAmount);
        l_sqlString.setIntParam(9, p_dedAccExpiryDays);
        l_sqlString.setCharParam(10, p_setDedicatedAcct);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 16050, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 16740, this,
                                "Database error: unable to insert row in table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_insert, 16750, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 16760, this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a new row into the database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_bonusSchemeId The bonus scheme identifier.
     * @param p_bonusPercent The bonus scheme bonus as a percentage.
     * @param p_adjCode The bonus adjustment code.
     * @param p_adjType The bonus adjustment type.
     * @param p_dedAccId The dedicated account to apply the bonus to (0 if main account).
     * @param p_refillValueTarget The refill target as a value.
     * @param p_refillCountTarget The refill target as a count.
     * @param p_countingPeriodDays The counting period.
     * @param p_bonusAmount The bonus scheme bonus as an amount.
     * @param p_dedAccExpiryDays The number of days before the bouns expires.
     * @param p_setDedicatedAcct Apply the bonus to the ded acc by set or adjustment?.
     * @return A count of the number of rows updated
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String  p_opid,
                      String  p_bonusSchemeId,
                      Float   p_bonusPercent,
                      String  p_adjCode,
                      String  p_adjType,
                      int     p_dedAccId,
                      Double  p_refillValueTarget,
                      Integer p_refillCountTarget,
                      int     p_countingPeriodDays,
                      Double  p_bonusAmount,
                      Integer p_dedAccExpiryDays,
                      char    p_setDedicatedAcct) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 20000, this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = new String("update bona_bonus_accum_params set" 
                           +     " bona_bonus_percent = {0},"
                           +     " bona_adj_code = {1},"
                           +     " bona_adj_type = {2}, "
                           +     " bona_ded_acc_id = {3},"
                           +     " bona_refill_value_target = {4},"
                           +     " bona_refill_count_target = {5},"
                           +     " bona_counting_period_days = {6},"
                           +     " bona_bonus_amount = {7},"
                           +     " bona_ded_acc_expiry_days = {8},"
                           +     " bona_set_ded_acc = {9}"
                           + " where bona_scheme_id = {10}");

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setFloatParam(0, p_bonusPercent);
        l_sqlString.setStringParam(1, p_adjCode);
        l_sqlString.setStringParam(2, p_adjType);
        l_sqlString.setIntParam(3, p_dedAccId);
        l_sqlString.setDoubleParam(4, p_refillValueTarget);
        l_sqlString.setIntParam(5, p_refillCountTarget);
        l_sqlString.setIntParam(6, p_countingPeriodDays);
        l_sqlString.setDoubleParam(7, p_bonusAmount);
        l_sqlString.setIntParam(8, p_dedAccExpiryDays);
        l_sqlString.setCharParam(9, p_setDedicatedAcct);
        l_sqlString.setStringParam(10, p_bonusSchemeId);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 20050, this, null);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 20130, this,
                                "Database error: unable to update row in table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_update, 20140, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 20150, this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;
    }
}
