////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionTimeLastRefill.java
//      DATE            :       08-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              time since last refill.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.support.PpasProperties;

/** Identify whether a customer qualifies for churn based on time since last refill. */
public class ChurnConditionTimeLastRefill extends ChurnCondition
{
    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C4 - Time since last refill. ";
    
    /** Number of days since last refill threshold. */
    private int i_lastRefillThreshold;
    
    /** Constructor for a Churn condition based on time since last refill.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     */
    public ChurnConditionTimeLastRefill(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
    {
        super(p_properties, p_bcCache, p_analyser);

        i_lastRefillThreshold = p_properties.getIntProperty(C_PROP_PREFIX + "lastRefillThreshold", 0);
    }
        
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason)
    {
        int l_days = p_data.getDaysSinceLastRefill();
        
        boolean l_result = l_days > i_lastRefillThreshold;
        
        p_reason.append(C_CONDITION_NAME);

        p_reason.append("Latest Refill occurred ");
        p_reason.append(l_days);
        p_reason.append(" days ago. This is ");
        p_reason.append(l_days > i_lastRefillThreshold ? "before" : l_days == i_lastRefillThreshold ? "on" : "after");
        p_reason.append(" the threshold of ");
        p_reason.append(i_lastRefillThreshold);
        p_reason.append(" days. ");
        p_reason.append(l_result ? C_CHURN_TEXT : C_NO_CHURN_TEXT);
        
        return l_result;
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if no refill in last " + i_lastRefillThreshold + " days";
    }
}
