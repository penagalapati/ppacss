/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.common.awd.alarmcommon;

import java.text.SimpleDateFormat;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SimpleAlarmEvent
implements AlarmEvent
{

    private static final SimpleDateFormat C_DF_DEFAULT =
            new SimpleDateFormat("dd-MMM-yy HH:mm:ss");

    private String              i_eventName;
    private String              i_eventText;
    private long                i_raisedTimeMillis;

    private String              i_originatingNodeName;
    private String              i_originatingProcessName;
    
    public SimpleAlarmEvent(
        String                  p_eventName,
        String                  p_eventText,
        String                  p_originatingNodeName,
        String                  p_originatingProcessName
    )
    {
        i_eventName = p_eventName;
        i_eventText = p_eventText;
        i_raisedTimeMillis = System.currentTimeMillis();                
        i_originatingNodeName = p_originatingNodeName;
        i_originatingProcessName = p_originatingProcessName;
    }

    /* (non-Javadoc)
     * @see com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent#getEventName()
     */
    public String getEventName()
    {
        return i_eventName;
    }

    /* (non-Javadoc)
     * @see com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent#getEventText()
     */
    public String getEventText()
    {
        return i_eventText;
    }

    public long getRaisedTime()
    {
        return i_raisedTimeMillis;
    }
    
    public String toString()
    {
        StringBuffer            l_sb;
        
        l_sb = new StringBuffer(160);
        
        l_sb.append("SimpleAlarmEvent=[ename=");
        l_sb.append(i_eventName);
        l_sb.append(",etext=");
        l_sb.append(i_eventText);
        l_sb.append(",raisedTime=");
        l_sb.append(i_raisedTimeMillis);
        l_sb.append(",origNodeName=");
        l_sb.append(i_originatingNodeName);
        l_sb.append(",origProcName=");
        l_sb.append(i_originatingProcessName);
        l_sb.append("]");
        
        return l_sb.toString();
    }

    /**
     * Returns the originating node name.
     * @return                  The originating node name.
     */
    public String getOriginatingNodeName()
    {
        return i_originatingNodeName;
    }

    /**
     * Returns the originating process name.
     * @return                  The originating process name.
     */
    public String getOriginatingProcessName()
    {
        return i_originatingProcessName;
    }

    // Time (normal Java UTC millis time) alarm was raised. 
    public void setRaisedTime(long p_raisedTimeMillis)
    {
        i_raisedTimeMillis = p_raisedTimeMillis;
    }

}
