////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatorData.Java
//      DATE            :       8th October 2002
//      AUTHOR          :       Matt Kirk
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Data object used to represent an Accumulator.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Data object used to represent an Accumulator.
 */
public class AccumulatorData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccumulatorData";

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    /** The ID of this Accumulator. */
    private int i_id;

    /** The balance of this Accumulator. */
    private int i_balance;

    /** The units description of this Accumulator. */
    private String i_units;

    /** The description of this Accumulator. */
    private String i_desc;

    /** The validity start date of this Accumulator. */
    private PpasDate i_startDate;

    /** The validity end date of this Accumulator. */
    private PpasDate i_endDate;


    //-------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------
    
    /**
     * Allows to create an Accumulator data object.
     * @param p_id the Id
     * @param p_balance the balance
     * @param p_units the units
     * @param p_desc the description
     * @param p_startDate the start date
     * @param p_endDate the end date
     */
    public AccumulatorData(int          p_id,
                           int          p_balance,
                           String       p_units,
                           String       p_desc,
                           PpasDate     p_startDate,
                           PpasDate     p_endDate)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_id = p_id;
        i_balance = p_balance;
        i_units = p_units;
        i_desc = p_desc;
        i_startDate = p_startDate;
        i_endDate = p_endDate;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 19000, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // public methods
    //-------------------------------------------------------------------------

    /** Returns the ID of this Accumulator.
     * 
     * @return The identifier of this accumulator.
     */
    public int getId()
    {
        return i_id;
    }

    /** Returns the balance of this Accumulator.
     * 
     * @return The balance on this accumulator.
     */
    public int getBalance()
    {
        return i_balance;
    }
    
    /** Returns the units of this Accumulator.
     * 
     * @return The type of units in which the balance is held.
     */
    public String getUnits()
    {
        return i_units;
    }
  
    /** Returns the description of this Accumulator.
     * 
     * @return The descriptio of this accumulator.
     */
    public String getDescription()
    {
        return i_desc;
    }

    /** Returns the validity start date of this Accumulator.
     * 
     * @return The start date of this accumulator.
     */
    public PpasDate getValidityStartDate()
    {
        return i_startDate;
    }

    /** Returns the validity end date of this Accumulator.
     * 
     * @return The end date of this accumulator.
     */
    public PpasDate getValidityEndDate()
    {
        return i_endDate;
    }

    /** Returns the validity start date of this Accumulator in String format.
     * 
     * @return String representation of the start date of this accumulator.
     */
    public String getValidityStartDateString()
    {
        return i_startDate == null ? "" : i_startDate.toString();
    }

    /** Returns the validity end date of this Accumulator in String format.
     * 
     * @return String representation of the end date of this accumulator.
     */
    public String getValidityEndDateString()
    {
        return i_endDate == null ? "" : i_endDate.toString();
    }
    
    /** Show the accumulator as a <code>String</code>.
     * 
     * @return String representation of the accumulator.
     */
    public String toString()
    {
        String l_return = "AccumulatorData=[id=" + i_id + ", balance=" + i_balance + 
                          ", start date=" + i_startDate + ", end date=" + i_endDate;
                          
        if (i_units != null)
        {
            l_return  += ", units=" + i_units;
        }
         
        if (i_desc != null)
        {
            l_return  += ", description=" + i_desc;
        }
                      
        l_return += "]";
                          
        return l_return;
    }
    
    /** 
     * Allows to set the description.
     * 
     * @param p_desc the Description
     */
    public void setDescription(String p_desc)
    {
        i_desc = p_desc;
    }

    /** 
     * Allows to set the units.
     * 
     * @param p_units the units
     */
    public void setUnits(String p_units)
    {
        i_units = p_units;
    }
    
    /**
     * Sets the usage accumulators new balance.
     * @param p_balance The new balance.
     */
    public void setBalance(int p_balance)
    {
        i_balance = p_balance;
    }

    /**
     * Sets the usage accumulators new validity start date.
     * @param p_startDate The new validity start date.
     */
    public void setValidityStartDate(PpasDate p_startDate)
    {
        i_startDate = p_startDate;
    }
    
    /**
     * Allows to compare 2 objects.
     *
     * @param p_data The accumulator data to compare this with.
     * @return true if the value are the same false in the other case
     */
    public boolean equals(AccumulatorData p_data)
    {
        if (this.i_balance != p_data.getBalance())
        {
            return false;
        }
         
        if (this.i_id != p_data.getId())
        {
            return false;
        }
         
        if (this.i_desc != null)
        {
            if (!this.i_desc.equals(p_data.getDescription()))
            {
                return false;
            } 
        }
        else if (p_data.getDescription() != null)
        {
            return false;
        }
         
        if (this.i_units != null)
        {
            if (!this.i_units.equals(p_data.getUnits()))
            {
                return false;
            }
        }
        else if (p_data.getUnits() != null)
        {
            return false;
        }
         
        if (this.i_startDate != null)
        {
            if (p_data.getValidityStartDate() != null)
            {
                if (!this.i_startDate.toString().equals(p_data.getValidityStartDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_data.getValidityStartDate() != null)
        {
            return false;
        }
         
        if (this.i_endDate != null)
        {
            if (p_data.getValidityEndDate() != null)
            {
                if (!this.i_endDate.toString().equals(p_data.getValidityEndDate().toString()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_data.getValidityEndDate() != null)
        {
            return false;
        }
         
        return true;
    }
    /**
     * Allows to compare 2 objects usefull for Junit framework.
     *
     * @param p_data The accumulator data to compare this with.
     * @return true if the value are the same false in the other case
     */
    public boolean equals(Object p_data)
    {
        if (p_data instanceof AccumulatorData)
        {
            return this.equals((AccumulatorData)p_data);
        }
        return false;
    }
}