////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       ArchiveTableDataSet.java
//DATE            :       29 September 2004
//AUTHOR          :       R.Grimshaw
//REFERENCE       :       PRD_ASCS00_DEV_SS_082
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//|            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * @author 7451F
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ArchiveTableDataSet extends DataObject
{

//  ------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ArchiveTableDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    
    /** The name of this ArchiveTableDatatSet transaction. */
    private String i_name;
    
    /** Vector containing valid markets for CSO. */
    private Vector i_tables;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates an empty vector of markets for the CSO.
     * 
     * @param p_name The name of this ArchiveTableDatatSetrequest being processed.
     * @param p_initialVSize Initial size of the vector.
     * @param p_vGrowSize    Number of elements by which vector should be increased.
     */
    public ArchiveTableDataSet(String       p_name,
                               int          p_initialVSize,
                               int          p_vGrowSize)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME);
        }
        
        i_name = p_name;

        i_tables = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }
    //------------------------------------------------------------------------
    // Overridden Superclass Methods.
    //------------------------------------------------------------------------
    /** Returns the contents of the ArchiveTableDataSet object in a String.
     * @return String representation of this object.
     */
    public String toString()
    {
        StringBuffer  l_stringBuffer = new StringBuffer("ArchiveTableDataSet=[");
        int           l_loop;

        if (i_tables == null)
        {
            l_stringBuffer.append("null");
        }
        else
        {
            for(l_loop = 0; l_loop < i_tables.size(); l_loop++)
            {
                l_stringBuffer.append((i_tables.elementAt(l_loop)).toString());
                if ((l_loop + 1) < i_tables.size())
                {
                    l_stringBuffer.append("\n");
                }
            }
        }

        l_stringBuffer.append("]");

        return(l_stringBuffer.toString());
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------
    /** Adds an element to the CSO markets vector.
     * @param p_table Market to add to the vector.
     */
    public void addElement(ArchiveTableData p_table)
    {
        i_tables.addElement(p_table);
    }

    /** Returns the set of ArchiveTableDatas as a Vector.
     * @return All ArchiveTableData.
     */
    public Vector getArchiveTableData()
    {
        return(i_tables);
    } 
    
    /**
     * Returns the name of the transaction
     * @return The name of the transaction.
     */
    public String getName()
    {
        return i_name;
    }

}
