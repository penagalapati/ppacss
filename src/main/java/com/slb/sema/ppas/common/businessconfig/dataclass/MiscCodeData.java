////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscCodeData.Java
//      DATE            :       23-Aug-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#964/4107
//                              PRD_PPAK00_GEN_CA_288
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Stores a row from the SRVA_MISC_CODES database
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;

/** An object that represents a row from the SRVA_MISC_CODES database table. */
public class MiscCodeData extends DataObject implements Comparable
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscCodeData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The Market. */
    private Market i_market;
    
    /** The code type eg 'ADR' = address. */
    private String i_codeType;

    /** The code. */
    private String i_code;
    
    /** The description. */
    private String i_description;

    /** Currency flag - when false this code is no longer valid. */
    private boolean i_isCurrent;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a misc code object.
     * 
     * @param p_market The Market.
     * @param p_codeType The code type.
     * @param p_code The code.
     * @param p_description The code description.
     * @param p_deleteFlag Indicates whether data item is still current.
     */
    public MiscCodeData(Market      p_market,
                        String      p_codeType,
                        String      p_code,
                        String      p_description,
                        String      p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME + " for Mkt: " + p_market + " type: " +
                            p_codeType + " Code: " + p_code + " Desc: " + p_description);
        }

        i_market      = p_market;
        i_codeType    = p_codeType;
        i_code        = p_code;
        i_description = p_description;

        if (p_deleteFlag.equals("*"))
        {
            i_isCurrent = false;
        }
        else
        {
            i_isCurrent = true;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }
        
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /** Returns the Market.
     * 
     * @return The market associated with this miscellaneous data.
     */
    public Market getMarket()
    {
        return i_market;
    }

    /** Returns the code type.
     * 
     * @return The type of miscellaneous data.
     */
    public String getCodeType()
    {
        return i_codeType;
    }

    /** Returns the code.
     * 
     * @return The code (key) of this miscellaneous data.
     */
    public String getCode()
    {
        return i_code;
    }
    
    /** Returns the description.
     * 
     * @return The description of this miscellansous data.
     */
    public String getDescription()
    {
        return i_description;
    }

    /** Returns the card group delete flag.
     * 
     * @return Flag indicating whether the miscellaneous data is still current or marked as deleted.
     */
    public boolean getIsCurrent()
    {
        return i_isCurrent;
    }

    /** 
     * Returns a String containing the code padded to p_codeWidth characters
     * and the description.
     * 
     * @param p_codeWidth Defines width of field to display code in including
     *                    trailing spaces. Maximum value is 12 characters.
     * @return Miscellaneous data in a format suitable for display.
     */
    public String getDisplayString(int p_codeWidth)
    {
        int l_codeWidth = (p_codeWidth > 12) ? 12 : p_codeWidth;
        String l_code = i_code + "            ";
        
        return (l_code.substring(0, l_codeWidth) + i_description);
    }

    /** Defines how objects of this class should be compared for sorting purposes.
     *  @param p_that An <code>Object</code> to compare with.
     *  @return 1, 0 or -1 depending on whether the comparision object is greater than, equal to or less than
     *           this object. 
     * 
     */
    public int compareTo(Object p_that)
    {
        MiscCodeData l_that = (MiscCodeData)p_that;
        
        int l_result = i_market.toString().compareTo(l_that.i_market.toString());

        if (l_result == 0)
        {
            l_result = i_codeType.compareTo(l_that.i_codeType);
        }
         
        if (l_result == 0)
        {
            l_result = i_code.compareTo(l_that.i_code);
        }
                 
        return l_result;        
    }
    
    /** 
     * Returns explicit instance of Misc Code data
     * @param p_market The customer's market.
     * 
     * @return MiscCodeData Object
     */
    public MiscCodeData getExplicInstance(Market p_market)
    {
        return new MiscCodeData(p_market,
                                i_codeType,
                                i_code,
                                i_description,
                                i_isCurrent ? " " : "*");
    }
}
