////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500 
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NublNumberBlockDataSet.Java
//      DATE            :       15-Oct-2003
//      AUTHOR          :       Oualid Gharach
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of Number Block (from nubl_number_block).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;


import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains A set of data, each element in the set defining                               
 * a record of Number Block (from nubl_number_block).
 */
public class NublNumberBlockDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "NublNumberBlockDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of nubl data records. That is, the data of this DataSet object. */
    private NublNumberBlockData []     i_nublArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of number block.
     * 
     * @param p_request The request to process.
     * @param p_nublArr Array holding the number block data records to be stored
     *                     by this object.
     */
    public NublNumberBlockDataSet(
        PpasRequest             p_request,
        NublNumberBlockData []  p_nublArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 14510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_nublArr = p_nublArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 14590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns NublNumberBlockData record for the given market.
     * @param p_market Market that selected record must have.
     * @param p_sdpId The SDP Id.
     * @param p_nublStartNumber the block start number.
     * 
     * @return NublNumberBlockData record from this DataSet with the given market. If
     *         no record can be found with the given market, then 
     *         <code>null</code> is returned.
     */
    public NublNumberBlockData getRecord (Market p_market,
                                          String p_sdpId,
                                          Msisdn p_nublStartNumber)
    {
        NublNumberBlockData l_nublRecord = null;
        int          l_loop;

        for (l_loop = 0; l_loop < i_nublArr.length; l_loop++)
        {
            if ( i_nublArr[l_loop].getMarket().equals(p_market) &&
                 i_nublArr[l_loop].getSdpId().equals(p_sdpId) &&
                 i_nublArr[l_loop].getNublStartNumber().equals(p_nublStartNumber) )                
            {
                l_nublRecord = i_nublArr[l_loop];
            }
        }

        return (l_nublRecord);

    } // End of public method getRecord

    /**
     * Returns a Number Block Data record that is associated with the for the specified MSISDN.
     * @param p_msisdn Mobile number for which the associated NUBL record is needed..
     * 
     * @return NublNumberBlockData record that would apply to the specified MSISDN. If no record can be found,
     *        then <code>null</code> is returned.
     */
    public NublNumberBlockData getRecord(Msisdn p_msisdn)
    {
        NublNumberBlockData l_nublRecord = null;
        int                 l_loop;

        if (p_msisdn != null)
        {
            for (l_loop = 0; l_loop < i_nublArr.length; l_loop++)
            {
                if (p_msisdn.isBetween(i_nublArr[l_loop].getNublStartNumber(),
                                       i_nublArr[l_loop].getNublEndNumber()))
                {
                    l_nublRecord = i_nublArr[l_loop];
                    break;
                }
            }
        }

        return l_nublRecord;
    }

    /**
     * Returns NublNumberBlockData records for the given market.
     * @param p_market Market that selected record must have.
     * @return NublNumberBlockData records from this DataSet with the given market. 
     */
    public NublNumberBlockData[] getRecords (Market    p_market)
    {
        NublNumberBlockData l_nublRecordsArr[];
        java.util.Vector    l_nublV = new java.util.Vector();
        int                 l_loop;
        NublNumberBlockData l_nublEmptyArr[] = new NublNumberBlockData[0];
        
        for (l_loop = 0; l_loop < i_nublArr.length; l_loop++)
        {
            if ( i_nublArr[l_loop].getMarket().equals(p_market) )                
                
            {
                l_nublV.addElement(i_nublArr[l_loop]);
            }
        }

        l_nublRecordsArr = (NublNumberBlockData[])l_nublV.toArray(l_nublEmptyArr);


        return (l_nublRecordsArr);

    } // End of public method getRecord


    /** Returns array of all records in this data set object.
     * 
     * @return All Number Block ranges.
     */
    public NublNumberBlockData [] getAllArray()
    {
        return (i_nublArr);

    } // end method getAllArray

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_nublArr, p_sb);
    }
}

