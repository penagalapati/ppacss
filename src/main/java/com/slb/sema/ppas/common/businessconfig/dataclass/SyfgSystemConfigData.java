////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SyfgSystemConfigData.Java
//      DATE            :       14-October-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1621/6878
//                              PRD_PPAK00_GEN_CA_269
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Stores data from the SYFG_SYSTEM_CONFIG database
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 19-May-05 | Lars Lundberg | New constants for charged  | PRD_ASCS00_GEN_CA_49
//           |               | events are added.          | 
// 19-May-05 | Lars Lundberg | All constant definitions   |
//           |               | are aligned.               |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Properties;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** An object that stores data from the SYFG_SYSTEM_CONFIG database table. */
public class SyfgSystemConfigData extends DataObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SyfgSystemConfigData";

    //------------------------------------------------------------------------
    // Public constants.
    //------------------------------------------------------------------------
    /** The destination key of the configuration for adjustment init. Value is {@value}. */
    public static final String C_INITIAL_ADJUST            = "ADJ_INIT";

    /** The name key of the configuration for adjustment init credit code. Value is {@value}. */
    public static final String C_ADJ_INITIAL_CREDIT_CODE   = "ADJ_INIT_CREDIT_CODE";
    
    /** The name key of the configuration for adjustment init credit type. Value is {@value}. */
    public static final String C_ADJ_INITIAL_CREDIT_TYPE   = "ADJ_INIT_CREDIT_TYPE";
    
    /** The destination key of the configuration for adjustment service fee. Value is {@value}. */
    public static final String C_ADJ_SERVICE_FEE           = "ADJ_SERVICE_FEE";

    /** The name key of the configuration for adjustment service fee code. Value is {@value}. */
    public static final String C_ADJ_SERVICE_FEE_CODE      = "ADJ_SERVICE_FEE_CODE";
    
    /** The name key of the configuration for adjustment service fee type. Value is {@value}. */
    public static final String C_ADJ_SERVICE_FEE_TYPE      = "ADJ_SERVICE_FEE_TYPE";

    /** The destination key of the configuration for adjustment. Value is {@value}. */
    public static final String C_DC_INTERFACE              = "DC_INTERFACE";

    /** The name key of the configuration for adjustment credit code. Value is {@value}. */
    public static final String C_ADJ_CREDIT_CODE           = "ADJ_CREDIT_CODE";
    
    /** The name key of the configuration for adjustment credit type. Value is {@value}. */
    public static final String C_ADJ_CREDIT_TYPE           = "ADJ_CREDIT_TYPE";

    /** The name key of the configuration for adjustment credit code. Value is {@value}. */
    public static final String C_ADJ_DEBIT_CODE            = "ADJ_DEBIT_CODE";
    
    /** The name key of the configuration for adjustment credit type. Value is {@value}. */
    public static final String C_ADJ_DEBIT_TYPE            = "ADJ_DEBIT_TYPE";
    
    /** The destination key of the configuration for business service. Value is {@value}. */
    public static final String C_BUS_SRV                   = "BUS_SRV";
    
    /** The name key of the configuration for account finder. Value is {@value}. */
    public static final String C_ACCOUNT_FINDER_ENABLE     = "ACCOUNT_FINDER_ENABLE";
    
    /** The name key of the configuration for routing mechanism. Value is {@value}. */
    public static final String C_ROUTING_METHOD            = "ROUTING_METHOD";
    
    /** The destination key of the configuration for adjustment history service. Value is {@value} */
    public static final String C_DESTINATION_IS_STORE_ADJ = "IS_STORE_ADJ";
    
    /** The default adjustment description if none found using type and code. Value is {@value} */
    public static final String C_PARAM_NAME_DEFAULT_ADJ_DESC = "DEFAULT_ADJ_DESC";
    
    /** The destination key of the configuration for DRP BatchAdjustmentService. Value is {@value} */
    public static final String C_DESTINATION_RDRP_AIR_ADJ = "RDRP_AIR_ADJ";
    
    /** The destination key of the configuration for DRP AccountAdjustmentService. Value is {@value} */
    public static final String C_DESTINATION_RDRP_SDP_ADJ = "RDRP_SDP_ADJ";
    
    /** The destination key of the configuration for DRP PeriodicAdjustmentService. Value is {@value} */
    public static final String C_DESTINATION_RDRP_PER_ADJ = "RDRP_PER_ADJ";
    
    /** The destination key of the configuration for DRP PeriodicResetService. Value is {@value} */
    public static final String C_DESTINATION_RDRP_PER_RES = "RDRP_PER_RES";
    
    /** The default adjustment code if not supplied in DR. Value is {@value} */
    public static final String C_PARAM_NAME_ADJ_CODE = "ADJ_CODE";
    
    /** The default adjustment type if not supplied in DR. Value is {@value} */
    public static final String C_PARAM_NAME_ADJ_TYPE = "ADJ_TYPE";

    /** The destination key of the configuration for charged events. Value is {@value}. */
    public static final String C_ADJ_CHARGED_EVT           = "ADJ_CHARGED_EVT";

    /** The destination key of Customer Install. Value is {@value}. */
    public static final String C_DEST_CUST_INSTALL         = "CUST_INSTALL";

    /** The parameter name for Default SDP. Value is {@value}. */
    public static final String C_NAME_DEFAULT_SDP          = "DEFAULT_SDP";

    /** The name key of the configuration for charged FAF event code. Value is {@value}. */
    public static final String C_FAF_NUMBER_ADDITION_CODE  = "FAF_NUMBER_ADDITION_CODE";
    
    /** The name key of the configuration for charged FAF event type. Value is {@value}. */
    public static final String C_FAF_NUMBER_ADDITION_TYPE  = "FAF_NUMBER_ADDITION_TYPE";
    
    /** The destination key of the configuration for FAF. Value is {@value} */
    public static final String C_DESTINATION_FAF           = "FAF";
    
    /** Parameter name for the FAF Account Level. */
    public static final String C_ENABLE_ACCOUNT_LEVEL_FAF  = "ENABLE_ACCOUNT_LEVEL_FAF";
    
    /** The name key of the configuration for charged balance enquiry event code. Value is {@value}. */
    public static final String C_BALANCE_ENQUIRY_CODE      = "BALANCE_ENQUIRY_CODE";
    
    /** The name key of the configuration for charged balance enquiry event type. Value is {@value}. */
    public static final String C_BALANCE_ENQUIRY_TYPE      = "BALANCE_ENQUIRY_TYPE";

    /** The name key of the configuration for charged service class update event code. Value is {@value}. */
    public static final String C_SERVICE_CLASS_UPDATE_CODE = "SERVICE_CLASS_UPDATE_CODE";

    /** The name key of the configuration for charged service class update event type. Value is {@value}. */
    public static final String C_SERVICE_CLASS_UPDATE_TYPE = "SERVICE_CLASS_UPDATE_TYPE";

    /** Destination for password related configuration. Value is {@value}. */
    public static final String C_DEST_PASSWORD = "PASSWORD";

    /** Parameter name for number of warning days until password expiry. Value is {@value}. */
    public static final String C_NAME_PASSWORD_EXPIRY_WARNING_DAYS = "PASSWORD_EXPIRY_WARNING_DAYS";

    /** Destination for login security related configuration. Value is {@value}. */
    public static final String C_DEST_LOGIN_SECURITY = "LOGIN_SECURITY";

    /** Parameter name for maximum login attempts. Value is {@value}. */
    public static final String C_NAME_MAX_LOGIN_ATTEMPTS = "MAX_LOGIN_ATTEMPTS";

    /** Parameter name for maximum login attempts. Value is {@value}. */
    public static final String C_NAME_LOGIN_BARRING_PERIOD = "LOGIN_BARRING_PERIOD";
    
    /** Parameter name for maximum number of passwords to retain in the password history. Value is {@value}.
     */
    public static final String C_NAME_USER_PASSWORD_HISTORY = "USER_PASSWORD_HISTORY";
    
    /** The destination key of the configuration for bonus. Value is {@value}. */
    public static final String C_BONUS            = "BONUS";

    /** The name key of the configuration for bonus schemes active. Value is {@value}. */
    public static final String C_BONUS_SCHEMES_ACTIVE   = "BONUS_SCHEMES_ACTIVE";
    
    /** The name key of the configuration for bonus multiplier. Value is {@value}. */
    public static final String C_BONUS_MULTIPLIER   = "BONUS_MULTIPLIER";
    
    /** The name key of the configuration for bonus residue rollover. Value is {@value}. */
    public static final String C_RESIDUE_ROLLOVER   = "RESIDUE_ROLLOVER";
    
    /** The name key of the configuration for multiple bonus awards. Value is {@value}. */
    public static final String C_MULTIPLE_AWARDS   = "MULTIPLE_AWARDS";

    /** The name key of the configuration for cust type community. Value is {@value}. */
    public static final String C_CUST_TYPE_COMMUNITY   = "CUST_TYPE_COMMUNITY";
    
    /** The destination key of the configuration for Statement of Account Event Descriptions. Value is {@value}. */
    public static final String C_EVENT_DESC            = "EVENT_DESC";

    /** The destination key of the configuration for Statement of Account. Value is {@value}. */
    public static final String C_SAC_CONF              = "SAC_CONF";

    /** The parameter name of the default SoA statement period. Value is {@value}. */
    public static final String C_STATEMENT_PERIOD      = "STATEMENT_PERIOD";

    /** The parameter name of the default maximum SoA events. Value is {@value}. */
    public static final String C_MAX_EVENTS            = "MAX_EVENTS";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Properties object to store SYFG values. */
    private Properties i_config;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /** 
     * Creates a System Configuration data object.
     * 
     * @param p_request The request to process.
     */
    public SyfgSystemConfigData(PpasRequest p_request)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME );
        }

        // Create hashmap to store config.
        i_config = new Properties();

        // Set the default values. These will be overwritten by actual config values as required.
        this.set("ACTIVATION_NO", "ENCRYPTION",                        "PLAIN");
        this.set(C_DEST_PASSWORD, "PASSWORD_STRONG",                   "FALSE");
        this.set(C_DEST_PASSWORD, C_NAME_PASSWORD_EXPIRY_WARNING_DAYS, "7");
        this.set(C_DEST_PASSWORD, "PASSWORD_VALID_DAYS",               "");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of system configuration.
     */
    public String toString()
    {
        return("SyfgSystemConfigData:" +
               i_config.toString());
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Set the configuration key and value.
     *  The destination and parameterName must not contain any space
     *  characters, and they are made uppercase. The parameter value is trimmed.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @param p_parameterValue The configured value.
     */
    public void set(String p_destination,
                    String p_parameterName,
                    String p_parameterValue)
    {
        i_config.setProperty(p_destination.trim().toUpperCase() + " " +
                                 p_parameterName.trim().toUpperCase(),
                             p_parameterValue.trim());
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_get = "get";
    /** Get a configuration value.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @return                 The configured value.
     *  @throws PpasConfigException If the requested property does not exist.
     *          Specific exceptions that may need to be handled are:
     *          "CONFIG_MISSING_FROM_TABLE"
     *         - configuration was not present in the SYFG table or in the
     *           default values.
     */
    public String get(String p_destination,
                      String p_parameterName)
        throws PpasConfigException
    {
        String l_value = null;

        l_value = i_config.getProperty(
                             p_destination.trim().toUpperCase() + " " +
                                 p_parameterName.trim().toUpperCase());

        // If the parameter isn't in the table, and there isn't a default value,
        // then throw an exception.
        if (l_value == null)
        {
            throw new PpasConfigException(
                              C_CLASS_NAME,
                              C_METHOD_get,
                              12010,
                              this,
                              (PpasRequest)null,
                              0,
                              ConfigKey.get().noConfigRows(
                                  "SYFG_SYSTEM_CONFIG", p_destination, p_parameterName));
        }

        return(l_value);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getInt = "getInt";
    /** Get a configuration value as an int.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @return                 The configured value.
     *  @throws PpasConfigException If the configuration is not found or is not an integer.
     *          Specific exceptions that may need to be handled are:
     *          "CONFIG_MISSING_FROM_TABLE"
     *         - configuration was not present in the SYFG table or in the
     *           default values.
     *          "CONFIG_NOT_INTEGER"
     *         - configuration value could not be converted to integer
     */
    public int getInt(String p_destination,
                      String p_parameterName)
        throws PpasConfigException
    {
        String l_stringValue;
        int    l_intValue;

        l_stringValue = get(p_destination, p_parameterName);
        try
        {
            l_intValue = Integer.parseInt(l_stringValue);
        }
        // If the value can't be converted to integer
        // then throw an exception.
        catch (NumberFormatException l_numberFormatEx)
        {
            throw new PpasConfigException(
                              C_CLASS_NAME,
                              C_METHOD_getInt,
                              12020,
                              this,
                              (PpasRequest)null,
                              0,
                              ConfigKey.get().configNotInteger(
                                     "SYFG_SYSTEM_CONFIG", p_destination, p_parameterName, l_stringValue),
                              l_numberFormatEx);
        }

        return(l_intValue);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPostiveInt = "getPostiveInt";
    /** Get a configuration value as zero or a positive int.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @return                 The configured value.
     *  @throws PpasConfigException If the configuration is not found or is not an integer.
     *          Specific exceptions that may need to be handled are:
     *          "CONFIG_MISSING_FROM_TABLE"
     *         - configuration was not present in the SYFG table or in the
     *           default values.
     *          "CONFIG_NOT_INTEGER"
     *         - configuration value could not be converted to integer
     */
    public int getPositiveInt(String p_destination,
                      String p_parameterName)
        throws PpasConfigException
    {
        int    l_intValue;

        l_intValue = getInt(p_destination, p_parameterName);
        if ( l_intValue < 0 )
        {
            throw new PpasConfigException(
                              C_CLASS_NAME,
                              C_METHOD_getPostiveInt,
                              12025,
                              this,
                              (PpasRequest)null,
                              0,
                              ConfigKey.get().configNotZeroOrPositiveInteger(
                                     "SYFG_SYSTEM_CONFIG", p_destination, p_parameterName,l_intValue));
        }

        return(l_intValue);
    }

    /** Return a boolean indicating whether the configuration key and value is
     *  present.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @param p_requiredValue  The value to be looked for.
     *  @return                 TRUE if the required value was found.
     *                          FALSE if the configuration was not found or was
     *                          different to the required value.
     */
    public boolean getBoolean(String p_destination,
                              String p_parameterName,
                              String p_requiredValue)
    {
        String  l_stringValue;
        boolean l_booleanValue = false;

        try
        {
            l_stringValue = get(p_destination,
                                p_parameterName);
            l_booleanValue = (l_stringValue.equals(p_requiredValue));
        }
        catch (PpasConfigException l_ppasConfigEx)
        {
            // Couldn't find the config. Doesn't matter - just return false.
            l_booleanValue = false;
        }

        return(l_booleanValue);
    }
    
    /** Return a Boolean indicating whether the configuration key and value is
     *  present and if the value can be intepreted to a true or false value.
     *  This method 'translate' a string such as: TRUE, YES, T, Y, 1 to true.
     *  And a string susch as: FALSE, NO, F, N, 0 to true. It the characters may be in
     *  upper of lower case.
     *
     *  @param p_destination    The destination key of the configuration.
     *  @param p_parameterName  The name key of the configuration.
     *  @return                 TRUE if it was a "true" value.
     *                          FALSE if it was a "false" value.
     *                          NULL if no match or not found.
     */
    public Boolean getFlagValue(String p_destination,
                                String p_parameterName)
    {

        final String  L_PATTERN_TRUE  = "TRUE";
        final String  L_PATTERN_YES   = "YES";
        final String  L_PATTERN_1     = "1";
        final String  L_PATTERN_Y     = "Y";
        final String  L_PATTERN_T     = "T";
        
        final String  L_PATTERN_FALSE = "FALSE";
        final String  L_PATTERN_NO    = "NO";
        final String  L_PATTERN_0     = "0";
        final String  L_PATTERN_N     = "N";
        final String  L_PATTERN_F     = "F";
        
        final String L_PATTERN_TRUE_VALUES  = "^" + L_PATTERN_TRUE  + "|" +
                                                    L_PATTERN_YES   + "|" +
                                                    L_PATTERN_1     + "|" +
                                                    L_PATTERN_Y     + "|" +
                                                    L_PATTERN_T;

        final String L_PATTERN_FALSE_VALUES = "^" + L_PATTERN_FALSE + "|" +
                                                    L_PATTERN_NO    + "|" +
                                                    L_PATTERN_0     + "|" +
                                                    L_PATTERN_N     + "|" +
                                                    L_PATTERN_F;
  
        Boolean  l_returnValue;
        String   l_stringValue;

        try
        {
            l_stringValue = get(p_destination,
                                p_parameterName).toUpperCase();
            
            if ( l_stringValue.matches(L_PATTERN_TRUE_VALUES))
            {
                l_returnValue = new Boolean(true);
            }
            else if ( l_stringValue.matches(L_PATTERN_FALSE_VALUES))
            {
                l_returnValue = new Boolean(false);
            }
            else
            {
                l_returnValue = null;
            }
        }
        catch (PpasConfigException l_ppasConfigEx)
        {
            // Couldn't find the config. Doesn't matter - just return false.
            l_returnValue = null;
        }

        return l_returnValue;
        
    }
    
    
}
