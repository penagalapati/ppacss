//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonsBonusSchemeCache.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for bonus scheme data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BonsBonusSchemeSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for Bonus Scheme.
 */
public class BonsBonusSchemeCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String          C_CLASS_NAME                   = "BonsBonusSchemeCodeCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The bonus scheme SQL service to use to load cache.
     */
    private BonsBonusSchemeSqlService i_bonsBonusSchemeSqlService = null;

    /** Cache of all valid Bonus Schemes(including ones marked as deleted). */
    private BonsBonusSchemeDataSet    i_allBonsBonusScheme        = null;

    /**
     * Cache of available Bonus Schemes(not including ones marked as deleted). This attribute exists
     * for performance reasons so the array is not generated from the complete list each time it is required.
     */
    private BonsBonusSchemeDataSet    i_availableBonsBonusScheme  = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Bonus Schemescache.
     * @param p_request The request to process.
     * @param p_logger The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public BonsBonusSchemeCache(PpasRequest p_request, Logger p_logger, PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonsBonusSchemeSqlService = new BonsBonusSchemeSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Bonus Schemes(including ones marked as deleted).
     * @return All valid Bonus Schemes.
     */
    public BonsBonusSchemeDataSet getAll()
    {
        return i_allBonsBonusScheme;
    }

    /**
     * Returns available valid Bonus Schemes(not including ones marked as deleted).
     * @return Available Bonus Schemes.
     */
    public BonsBonusSchemeDataSet getAvailable()
    {
        if (i_availableBonsBonusScheme == null)
        {
            i_availableBonsBonusScheme = new BonsBonusSchemeDataSet(null, i_allBonsBonusScheme.getAvailableArray());
        }

        return i_availableBonsBonusScheme;
    }

    /**
     * Reloads the static configuration into the cache. This method is synchronised so a reload will not cause
     * unexpected errors.
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest p_request, JdbcConnection p_connection) throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered reload");
        }

        i_allBonsBonusScheme = i_bonsBonusSchemeSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all rates and old list of deleted
        // languages, but rather then synchronise this is acceptable.
        i_availableBonsBonusScheme = new BonsBonusSchemeDataSet(p_request, i_allBonsBonusScheme
                                                                .getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allBonsBonusScheme);
    }
}

