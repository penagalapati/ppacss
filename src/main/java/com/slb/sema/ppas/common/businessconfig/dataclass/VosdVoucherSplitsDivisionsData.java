////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       VosdVoucherSplitsDivisionsData.Java
//      DATE            :       03-03-2006
//      AUTHOR          :       Michael Erskine
//
//      REFERENCE       :       PRD_ASCS00_GEN_CA_68
//                              PpacLon#2005/8047
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A record defining a voucher split (or division)
//                              (corresponding to a single row of the
//                              vosd_voucher_splits_divisions table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <Name>     | <Description>                   | <Reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Class defining a single dedicated account within a division.
 */
public class VosdVoucherSplitsDivisionsData extends DataObject
{
    /** Used in calls to middleware. */
	private static final String C_CLASS_NAME = "VosdVoucherSplitsDivisionsData";
	
    /** Identifier of the dedicated account. */
    private int       i_identifier;

    /** Percentage to be applied to this dedicated account. */
    private int       i_percentage;

    /** End date of this dedicated account. */
    private PpasDate  i_endDate;

    /** Period (days from transaction date) to set end date of dedicated 
     *  account to.
     */
    private int       i_period;
    
    /** Indicates from when the 'period' will commence to derive the 'new' expiry date when funds added
     *  into dedicated account balance will expire.
     */
    private String    i_extType;

    /** Construct an instance of a VosdVoucherSplitsDivisionsData object.
     * 
     * @param p_request The request to process.
     *  @param p_identifier Identifier of the dedicated account.
     *  @param p_percentage Percentage to be applied to the dedicated
     *                      account.
     *  @param p_endDate End date to be set of the dedicated account.
     *  @param p_period Period (offset in days from today) to be used in
     *                  setting the end date of the dedicated account.
     *  @param p_extType The Dedicated account period extension type.
     */

    public VosdVoucherSplitsDivisionsData(
        PpasRequest            p_request,
        int                    p_identifier,
        int                    p_percentage,
        PpasDate               p_endDate,
        int                    p_period,
        String                 p_extType)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10110, this,
                "Constructing VospDedicatedAccountData");
        }

        i_identifier = p_identifier;
        i_percentage = p_percentage;
        i_endDate    = p_endDate;
        i_period     = p_period;
        i_extType    = p_extType;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10190, this,
                "Constructed VospDedicatedAccountData");
        }
    }

    /** Get the identifier of the dedicated account.
     *  @return The identifier of the dedicated account.
     */
    public int getIdentifier()
    {
        return (i_identifier);
    }

    /** Get the percentage to be applied to this dedicated account. 
     *  @return The percentage to be applied to this dedicated account.
     */
    public int getPercentage()
    {
        return (i_percentage);
    }

    /** Get the end date of this dedicated account.
     *  @return The end date of this dedicated account.
     */
    public PpasDate getEndDate()
    {
        return (i_endDate);
    }

    /** Get the period (days from transaction date) to set end date of
     *  dedicated account to.
     *  @return The period (days from transaction date) to set end date of
     *          dedicated account to.
     */
    public int getPeriod()
    {
        return (i_period);
    }
    
    /**
     * Returns the extension type.
     * @return The extension type.
     */
    public String getExtType()
    {
        return i_extType;
    }

    /**
     * Returns the contents of the instance variables contained in an instance of this class.
     * @return A String containing the contents of an instance of this class.
     */
    public String toString()
    {
        StringBuffer l_stringBuffer = new StringBuffer("");
        
        l_stringBuffer.append("\nDedicated Account Id: '" + i_identifier + "', Percentage: '" + i_percentage + 
                              "', End date: '" + i_endDate + "', Period: '" + i_period + "', Period: '" + 
                              i_extType + "'");
        
        return l_stringBuffer.toString();
    }

}