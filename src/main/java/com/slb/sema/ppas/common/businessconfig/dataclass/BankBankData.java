////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      BankBankData.Java
//      DATE            :      11-July-2001
//      AUTHOR          :      Sally Wells
//
//      COPYRIGHT       :      WM-data 2005
//
//      DESCRIPTION     :      Class containing bank data as
//                             part of business config cache. This data relates
//                             to the database table bank_bank.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of change>   | <reference>
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | Copyright info changed.         | PpacLon#1755/7279
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | Method 'getIsCurrent' is renamed| PpacLon#1755/7279
//          |            | to 'isWithdrawn'.               |
//          |            | Instance variable 'i_isCurrent' |
//          |            | is renamed to 'i_withDrawn'.    |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** An object that represents a row of the database table bank_bank. */
public class BankBankData extends DataObject
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BankBankData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** The bank code of a bank (aka bank number). */
    private String i_bankCode   = null;

    /** Name of a bank. */
    private String i_bankName   = null;

    /** Withdrawal flag. When true this code is no longer available for
     * allocation.
     */
    private boolean i_withdrawn = false;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates new bank details and initialises data.
     * 
     * @param p_request The request to process.
     * @param p_bankCode The identifier of the bank.
     * @param p_bankName The name of the bank.
     * @param p_deleteFlag Flag to indicate the bank should be marked as deleted.
     */
    public BankBankData(
        PpasRequest p_request,
        String      p_bankCode,
        String      p_bankName,
        String      p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10020, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_bankCode  = p_bankCode;
        i_bankName  = p_bankName;
        i_withdrawn = (p_deleteFlag.equals("*"));

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10060, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the bank code.
     * 
     * @return The identifier of the bank.
     */
    public String getBankCode()
    {
        return(i_bankCode);
    }

    /** Returns the bank name.
     * 
     * @return The name of the bank.
     */
    public String getBankName()
    {
        return(i_bankName);
    }

    /** Returns the bank code delete flag.
     * 
     * @return Flag indicating whether the bank is deleted.
     */
    public boolean isWithdrawn()
    {
        return(i_withdrawn);
    }
}
