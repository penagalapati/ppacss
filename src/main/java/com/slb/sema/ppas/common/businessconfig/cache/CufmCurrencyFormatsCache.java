////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CufmCurrencyFormatsCache.Java
//      DATE            :       23-October-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1074/4394
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Cache wrapper for CufmCurrencyFormats data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CufmCurrencyFormatsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** Cache wrapper for CufmCurrencyFormats data. */
public class CufmCurrencyFormatsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CufmCurrencyFormatsCache";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** Data set containing all defined currency records. */
    private CufmCurrencyFormatsDataSet i_cufmCurrencyFormatsDataSet = null;

    /** Data set containing all available currency records. */
    private CufmCurrencyFormatsDataSet i_availableCurrenciesDataSet = null;
    
    /** Service used to select CufmCurrencyFormats data from the database. */
    private CufmCurrencyFormatsSqlService i_cufmCurrencyFormatsSqlService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new CufmCurrencyFormats cache object.
     * 
     * @param p_request    The request to process.
     * @param p_logger     The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public CufmCurrencyFormatsCache(PpasRequest    p_request,
                                    Logger         p_logger,
                                    PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_cufmCurrencyFormatsSqlService = 
                          new CufmCurrencyFormatsSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";

    /** Reloads the CufmCurrencyFormats data into the cache.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @throws PpasSqlException If the cache cannot be loaded.
     */
    public void reload(PpasRequest     p_request,
                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on) 
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, 
                            p_request, 
                            C_CLASS_NAME, 
                            11000,
                            this,
                            "Entered " + C_METHOD_reload );
        }
 
        i_cufmCurrencyFormatsDataSet = i_cufmCurrencyFormatsSqlService.readCurrencyFormats(
                                               p_request, 
                                               p_connection);

        i_availableCurrenciesDataSet = new CufmCurrencyFormatsDataSet(
                                               p_request,
                                               i_cufmCurrencyFormatsDataSet.getAvailable());

        if (PpasDebug.on) 
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END, 
                            p_request, 
                            C_CLASS_NAME, 
                            11010, 
                            this,
                            "Leaving " + C_METHOD_reload );
        }
    }

    /** 
     * Returns a set of all defined currency records.
     * @return Data set containing all defined currency records.
     */
    public CufmCurrencyFormatsDataSet getCufmCurrencyFormats()
    {
        return(i_cufmCurrencyFormatsDataSet);
    }

    /** 
     * Returns a set of all available currency records.
     * @return Data set containing all available currency records.
     */
    public CufmCurrencyFormatsDataSet getAvailableCurrencyFormats()
    {
        return(i_availableCurrenciesDataSet);
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_cufmCurrencyFormatsDataSet);
    }
}

