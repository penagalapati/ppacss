////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAgentMstrCache.Java
//      DATE            :       15-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the agents
//                              data held in the srva_agents_mstr table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaAgentMstrSqlService;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the Agents business configuration data (from the
 * srva_agents_mstr table).
 */
public class SrvaAgentMstrCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAgentMstrCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private SrvaAgentMstrSqlService i_srvaAgentMstrSqlService = null;

    /** Data set containing all configured agent records. */
    private SrvaAgentMstrDataSet i_allAgents = null;

    /**
     * Cache of available agents (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private SrvaAgentMstrDataSet i_availableAgents = null;

    /**
     * Vector of caches, each element of the Vector containing a cache of
     * available agents for the same market. This does not include records marked as deleted.
     */
    private Vector  i_availableByMarket = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Agents data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public SrvaAgentMstrCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 21110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_srvaAgentMstrSqlService = new SrvaAgentMstrSqlService (p_request);

        i_availableByMarket = new Vector (10, 5);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 21120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all agents data (including records marked as deleted).
     * @return Data set containing all agents data configured in the
     *         srva_agent_mstr table.
     */
    public SrvaAgentMstrDataSet getAll()
    {
        return (i_allAgents);

    } // End of public method getAll

    /**
     * Returns available agents data. 
     * This does not include records marked as deleted.
     * @return Data set containing agents data that is not marked as deleted
     *         in the srva_agent_mstr table.
     */
    public SrvaAgentMstrDataSet getAvailable()
    {
        return (i_availableAgents);

    } // End of public method getAvailable
    
    /**
     * Returns available agents data for a list of Markets. 
     * This does not include records marked as deleted.
     * @param p_markets  List of markets.
     * @return Data set containing agents data that is not marked as deleted
     *         in the srva_agent_mstr table.
     */
    public SrvaAgentMstrDataSet getAvailable (Vector      p_markets)
    {
        int                  l_lenMktVec = 0;
        SrvaAgentMstrDataSet l_retSrvaAgentMstrDataSet = null;
        SrvaAgentMstrDataSet l_mktSrvaAgentMstrDataSet = null;
        Market               l_market = null;
        SrvaAgentMstrData [] l_mktSrvaAgentMstrDataARR = null;
        int                  l_lenMktSrvaAgentMstrDataARR = 0;
        SrvaAgentMstrData    l_srvaAgentMstrData = null;        
        ArrayList            l_srvaAgentMstrDataAL = null;
        int                  l_lenAL = 0;                 
        SrvaAgentMstrData [] l_srvaAgentMstrDataARR = null;
       
        l_srvaAgentMstrDataAL = new ArrayList();
        
        if (p_markets != null)
        {
            l_lenMktVec = p_markets.size();
            for (int l_i = 0; l_i < l_lenMktVec; l_i++)
            {
                l_market = (Market)p_markets.elementAt(l_i);
                
                l_mktSrvaAgentMstrDataSet = getAvailable(l_market);
                                                
                l_mktSrvaAgentMstrDataARR = l_mktSrvaAgentMstrDataSet.getAllArray();
                l_lenMktSrvaAgentMstrDataARR = l_mktSrvaAgentMstrDataARR.length;
                for (int l_j = 0; l_j < l_lenMktSrvaAgentMstrDataARR; l_j++)
                {
                    l_srvaAgentMstrData = l_mktSrvaAgentMstrDataARR[l_j];
                    l_srvaAgentMstrDataAL.add(l_srvaAgentMstrData);
                }                            
            }        
        } 

        // Convert ArrayList to Array
        l_lenAL = l_srvaAgentMstrDataAL.size();
        l_srvaAgentMstrDataARR = new SrvaAgentMstrData[l_lenAL];
        for(int l_i = 0; l_i < l_lenAL; l_i++)
        {
            l_srvaAgentMstrDataARR[l_i] = (SrvaAgentMstrData)l_srvaAgentMstrDataAL.get(l_i);
        }
        
        l_retSrvaAgentMstrDataSet = new SrvaAgentMstrDataSet(l_srvaAgentMstrDataARR);
        
        return l_retSrvaAgentMstrDataSet;    
    }

    /**
     * Returns available agent data for a given market.
     * The returned data set does not include records marked as deleted.
     *
     * @param p_market  The market for which agents are required.
     * @return Data set containing agents data for a given market that
     *         is not marked as deleted in the srva_agent_mstr table.
     */
    public SrvaAgentMstrDataSet getAvailable (Market      p_market)
    {
        SrvaAgentMstrDataSet l_agentsDataSet = null;
        SrvaAgentMstrDataSet l_returnDataSet = null;
        int                  l_index;

        if (i_allAgents != null)
        {
            // Check to see if we already have a cached set of agent for the
            // given market
            for (l_index = 0; l_index < i_availableByMarket.size() && l_returnDataSet == null; l_index++)
            {
                l_agentsDataSet = (SrvaAgentMstrDataSet)
                                      (i_availableByMarket.elementAt(l_index));

                if ( (l_agentsDataSet.getAllArray())[0].getMarket().equals(p_market) )
                {
                    l_returnDataSet = l_agentsDataSet;
                }
            }

            if (l_returnDataSet == null)
            {
                l_returnDataSet = new SrvaAgentMstrDataSet(i_allAgents.getAvailableArray(p_market));

                // If the data set has some data, then add it the the Vector
                // of cached data stored against market
                if (l_returnDataSet.getAllArray().length > 0)
                {
                    i_availableByMarket.add (l_returnDataSet);
                }
            }
        }

        return (l_returnDataSet);

    } // End of public method getFromMarket

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 21230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allAgents = i_srvaAgentMstrSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all agents and old list of
        // available agents, but rather than synchronise this is acceptable.
        i_availableAgents = new SrvaAgentMstrDataSet(i_allAgents.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 21290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allAgents, i_availableAgents);
    }
}

