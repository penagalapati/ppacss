////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountDetailBatchRecordData
//      DATE            :       6-August-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       General account details data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/** General account details batch record. */
public class AccountDetailBatchRecordData extends BatchRecordData
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME   = "AccountDetailBatchRecordData";
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** Customer Identifier. */
    private String i_customerId = null;
    
    /**
     * Constructor for the AccountDetailBatchRecordData class.
     * @param p_custId Customer Identifier.
     */
    public AccountDetailBatchRecordData(String p_custId)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10001,
                             this,
                             C_CONSTRUCTING + C_CLASS_NAME + " with " + p_custId);
        }

        i_customerId = p_custId;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             10010,
                             this,
                             C_CONSTRUCTED + C_CLASS_NAME );
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_dumpRecord = "dumpRecord";
    /**
     * @return the record content in a readable format.
     */
    public String dumpRecord()
    {

        StringBuffer l_tmp = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             10020,
                             this,
                             BatchRecordData.C_ENTERING + C_METHOD_dumpRecord );
        }

        // Get the super class's data
        l_tmp.append( C_DELIMITER );
        l_tmp.append( "*** Class name = " );
        l_tmp.append( C_CLASS_NAME );
        l_tmp.append( " ***");
        l_tmp.append( C_DELIMITER );

        l_tmp.append( super.getDumpRecord() );

        l_tmp.append( "Customer id    = " );
        l_tmp.append( i_customerId );
                
        return l_tmp.toString();
    }

    /** Get the customer identifier.
     * @return Customer Id.
     */
    public String getCustomerId()
    {
        return i_customerId;
    }
}
