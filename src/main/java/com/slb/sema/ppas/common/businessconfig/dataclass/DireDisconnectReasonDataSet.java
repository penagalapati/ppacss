////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DireDisconnectReasonDataSet.Java
//      DATE            :       16-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of disconnect reason data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 27/04/06 | S Pinton   | Add Serializable interface so   | PpacLon#2201/8578
//          |            | dataset can be used in HA cache |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.io.Serializable;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an disconnect reasons data, each element of
 * the set representing a record from the dire_disconnect_reason table.
 */
public class DireDisconnectReasonDataSet extends DataSetObject implements Serializable
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DireDisconnectReasonDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of disconnect reason data records. */
    private DireDisconnectReasonData []     i_disconnectReasonArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of disconnect reasons data.
     * 
     * @param p_request The request to process.
     * @param p_disconnectReasonArr Set of disconnection reason objects.
     */
    public DireDisconnectReasonDataSet(
        PpasRequest                  p_request,
        DireDisconnectReasonData []  p_disconnectReasonArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 54510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_disconnectReasonArr = p_disconnectReasonArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 54590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getAvailableArray = 
                                                       "getAvailableArray";
    /**
     * Get a data set containing the all available disconnect reason records. 
     * This does not include records marked as deleted.
     * 
     * @param p_request The request to process.
     * @return Data set containing available disconnect reason records in the
     *         cache (i.e. those not marked as deleted).
     */
    public DireDisconnectReasonData [] getAvailableArray (PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_START | PpasDebug.C_ST_TRACE,
                 p_request, C_CLASS_NAME, 54610, this,
                 "Entered " + C_METHOD_getAvailableArray);
        }

        DireDisconnectReasonData  l_availableDireArr[];
        Vector                    l_availableV = new Vector();
        int                       l_loop;
        DireDisconnectReasonData  l_direEmptyArr[] = 
                                            new DireDisconnectReasonData[0];

        for (l_loop = 0; l_loop < i_disconnectReasonArr.length; l_loop++)
        {
            if (!i_disconnectReasonArr[l_loop].isDeleted())
            {
                l_availableV.addElement(i_disconnectReasonArr[l_loop]);
            }
        }

        l_availableDireArr =
              (DireDisconnectReasonData[])l_availableV.toArray(l_direEmptyArr);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_END | PpasDebug.C_ST_TRACE,
                 p_request, C_CLASS_NAME, 54690, this,
                 "Leaving " + C_METHOD_getAvailableArray);
        }

        return(l_availableDireArr);

    } // End of public method getAvailableArray()

    /**
     * Returns a DireDisconnectReasonData according the disconnection reason
     * given in parameter.
     * 
     * @param p_disconnectionReason     Disconnection Reason 
     * 
     * @return DireDisconnectReasonData for the given disconnection reason.
     */
    public DireDisconnectReasonData getDisconnectionReasonData( String p_disconnectionReason )
    {
        int                      l_disconnectReasonArrLength = 0;
        DireDisconnectReasonData l_currentDireDiscReasonData = null;
        
        if (p_disconnectionReason != null && !p_disconnectionReason.equals(""))
        {
            l_disconnectReasonArrLength = i_disconnectReasonArr.length;
            
            for (int l_i = 0; l_i < l_disconnectReasonArrLength; l_i++) 
            {
                l_currentDireDiscReasonData = i_disconnectReasonArr[l_i];
                
                if ( !l_currentDireDiscReasonData.isDeleted() 
                    && l_currentDireDiscReasonData.getDireReason()
                                                  .equals(p_disconnectionReason)) 
                {
                    return l_currentDireDiscReasonData;
                }
            }
        }

        return null; 
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_disconnectReasonArr, p_sb);
    }
}

