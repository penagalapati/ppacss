////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchJobControlData.java
//    DATE            :       12-September-2005
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PRD_ASCS00_DEV_SS_071
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       A container for batch job control data
//                           (see database table "BACO_BATCH_CONTROL").
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
// 23/02/06| Lars L.       | Public constants defining the    | PpacLon#1419/6110
//         |               | different batch job statuses are |
//         |               | added.                           |
//---------+---------------+----------------------------------+--------------------
// DD/MM/YY| <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This <code>BatchJobControlData</code> class is a container for batch job control data.
 */
public class BatchJobControlData
{
    //-------------------------------------------------------------------------
    // Public class constants.
    //-------------------------------------------------------------------------
    /** Constant for an undefined batch job status.  Value is {@value}. */
    public static final char C_BATCH_JOB_STATUS_UNDEFINED  = 0;

    /** Constant for batch job status <code>Completed</code>.  Value is {@value}. */
    public static final char C_BATCH_JOB_STATUS_COMPLETED  = 'C';

    /** Constant for batch job status <code>In progress</code>.  Value is {@value}. */
    public static final char C_BATCH_JOB_STATUS_INPROGRESS = 'I';

    /** Constant for batch job status <code>Stopped</code>.  Value is {@value}. */
    public static final char C_BATCH_JOB_STATUS_STOPPED    = 'X';

    /** Constant for batch job status <code>Failed</code>.  Value is {@value}. */
    public static final char C_BATCH_JOB_STATUS_FAILED     = 'F';


    //-------------------------------------------------------------------------
    // Private class constants.
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchJobControlData";


    //-------------------------------------------------------------------------
    // Private instance attributes.
    //-------------------------------------------------------------------------
    /** The job type (the "name" of the batch).*/
    private String       i_jobType              = null;
    
    /** The execution date and time. */
    private PpasDateTime i_executionDateTime    = null;

    /** The status of the (batch) job.*/
    private char         i_status               = C_BATCH_JOB_STATUS_UNDEFINED;

    /** The unique job id that is assigned to the job by the Job Scheduler. */
    private long         i_jsJobId              = -1; // -1 indicates that the id is not defined.

    /**
     * The initial number of sub jobs.
     * It will be decremented by one (1) by each sub job when finished, i.e. the value of this variable
     * will be zero (0) for a complete batch.
     * NB: Only used by data base driven batches. For file driven batches this value will always be -1.
     */
    private int          i_subJobCnt            = -1; // -1 indicates that no of sub jobs is not defined.

    /** The date derived from the filename. NB: Only used by file driven batches. */
    private PpasDate     i_fileDate             = null;

    /** The sequence number derived from the filename. NB: Only used by file driven batches. */
    private int          i_seqNo                = -1; // -1 indicates that the seq. no is not defined.

    /** The sub sequence number derived from the filename. NB: Only used by file driven batches. */
    private int          i_subSeqNo             = -1; // -1 indicates that the sub seq. no is not defined.

    /** The number of successfully processed records. NB: Only used by file driven batches. */
    private long         i_noOfSuccessfullyRecs = -1; // -1 indicates that it is not defined.

    /** The number of rejected records. NB: Only used by file driven batches. */
    private long         i_noOfRejectedRecs     = -1; // -1 indicates that it is not defined.

    /** Extra data 1. */
    private String       i_extraData1           = null;

    /** Extra data 2. */
    private String       i_extraData2           = null;

    /** Extra data 3. */
    private String       i_extraData3           = null;

    /** Extra data 4. */
    private String       i_extraData4           = null;

    /** The operator id.*/
    private String       i_opId                 = null;


    //------------------------------------------------------------------------
    // Constructor(s)
    //------------------------------------------------------------------------
    /**
     * Connstructs a <code>BatchSubJobControlData</code> instance that holds the
     * specified batch sub job details.
     * 
     * @param p_jobType               the job type of the sub job (the "name" of the batch).
     * @param p_executionDateTime     the execution date and time.
     * @param p_status                the status of the (batch) job.
     * @param p_jsJobId               the unique job id that is assigned to the job by the Job Scheduler.
     * @param p_subJobCnt             the initial number of sub jobs.
     * @param p_fileDate              the date derived from the filename.
     * @param p_seqNo                 the sequence number derived from the filename.
     * @param p_subSeqNo              the sub sequence number derived from the filename.
     * @param p_noOfSuccessfullyRecs  the number of successfully processed records.
     * @param p_noOfRejectedRecs      the number of successfully processed records.
     * @param p_extraData1            extra data 1.
     * @param p_extraData2            extra data 2.
     * @param p_extraData3            extra data 3.
     * @param p_extraData4            extra data 4.
     * @param p_opId                  the operator id.
     */
    public BatchJobControlData(String       p_jobType,
                               PpasDateTime p_executionDateTime,
                               char         p_status,
                               long         p_jsJobId,
                               int          p_subJobCnt,
                               PpasDate     p_fileDate,
                               int          p_seqNo,
                               int          p_subSeqNo,
                               long         p_noOfSuccessfullyRecs,
                               long         p_noOfRejectedRecs,
                               String       p_extraData1,
                               String       p_extraData2,
                               String       p_extraData3,
                               String       p_extraData4,
                               String       p_opId)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        
        // Save the attributes.
        i_jobType              = p_jobType;
        i_executionDateTime    = (PpasDateTime)p_executionDateTime.clone();
        i_status               = p_status;
        i_jsJobId              = p_jsJobId;
        i_subJobCnt            = p_subJobCnt;
        i_fileDate             = p_fileDate;
        i_seqNo                = p_seqNo;
        i_subSeqNo             = p_subSeqNo;
        i_noOfSuccessfullyRecs = p_noOfSuccessfullyRecs;
        i_noOfRejectedRecs     = p_noOfRejectedRecs;
        i_extraData1           = p_extraData1;
        i_extraData2           = p_extraData2;
        i_extraData3           = p_extraData3;
        i_extraData4           = p_extraData4;
        i_opId                 = p_opId;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME + this.toString());
        }
    }

    /**
     * Connstructs a <code>BatchSubJobControlData</code> instance that holds the
     * specified batch sub job details.
     * 
     * @param p_executionDateTime     the execution date and time.
     * @param p_jsJobId               the unique job id that is assigned to the job by the Job Scheduler.
     */
    public BatchJobControlData(PpasDateTime p_executionDateTime,
                               long         p_jsJobId)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        
        // Save the attributes.
        i_executionDateTime    = (PpasDateTime)p_executionDateTime.clone();
        i_jsJobId              = p_jsJobId;


        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME + this.toString());
        }
    }
    
    
    //------------------------------------------------------------------------
    // Public methods.
    //------------------------------------------------------------------------
    /**
     * Compares this object to the specified object.
     * The result is true if and only if the argument is not null and is a
     * <code>BatchJobControlData</code> object that contains the same values for all
     * attributes as this object.
     * 
     * @param p_obj  the object to compare with.
     * 
     * @return  <code>true</code> if the objects are the same, <code>false</code> otherwise.
     */
    public boolean equals(Object p_obj)
    {
        boolean             l_equal   = false;
        BatchJobControlData l_compObj = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering the 'equals(...)' method.");
        }

        if (p_obj != null  &&  p_obj instanceof BatchJobControlData)
        {
            l_compObj = (BatchJobControlData)p_obj;

            if (isEqual(l_compObj.i_jobType, i_jobType)                     &&
                isEqual(l_compObj.i_executionDateTime, i_executionDateTime) &&
                l_compObj.i_status               == i_status                &&
                l_compObj.i_jsJobId              == i_jsJobId               &&
                l_compObj.i_subJobCnt            == i_subJobCnt             &&
                isEqual(l_compObj.i_fileDate, i_fileDate)                   &&
                l_compObj.i_seqNo                == i_seqNo                 &&
                l_compObj.i_subSeqNo             == i_subSeqNo              &&
                l_compObj.i_noOfSuccessfullyRecs == i_noOfSuccessfullyRecs  &&
                l_compObj.i_noOfRejectedRecs     == i_noOfRejectedRecs      &&
                isEqual(l_compObj.i_extraData1, i_extraData1)               &&
                isEqual(l_compObj.i_extraData2, i_extraData2)               &&
                isEqual(l_compObj.i_extraData3, i_extraData3)               &&
                isEqual(l_compObj.i_extraData4, i_extraData4)               &&
                isEqual(l_compObj.i_opId, i_opId))
            {
                l_equal = true;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving the 'equals(...)' method.");
        }
        return l_equal;
    }


    /**
     * Displays the batch sub job details (control data) as a <code>String</code>.
     * 
     * @return a <code>String</code> representation of the batch sub job details.
     */
    public String toString()   
    {
        StringBuffer l_stringBuf = new StringBuffer();

        l_stringBuf.append("job type: "      + i_jobType);
        l_stringBuf.append(",  exec. time: " +
                           (i_executionDateTime != null  ?  i_executionDateTime.toString() : "null"));
        l_stringBuf.append(",  status: "                  + i_status);
        l_stringBuf.append(",  js job id: "               + i_jsJobId);
        l_stringBuf.append(",  sub job count: "           + i_subJobCnt);
        l_stringBuf.append(",  file date: "               + 
                           (i_fileDate != null  ?  i_fileDate.toString() : "null"));
        l_stringBuf.append(",  file seq. no: "            + i_seqNo);
        l_stringBuf.append(",  file sub seq. no: "        + i_subSeqNo);
        l_stringBuf.append(",  no of success records: "   + i_noOfSuccessfullyRecs);
        l_stringBuf.append(",  no of rejected records: "  + i_noOfRejectedRecs);
        l_stringBuf.append(",  extra data 1: "            + i_extraData1);
        l_stringBuf.append(",  extra data 2: "            + i_extraData2);
        l_stringBuf.append(",  extra data 3: "            + i_extraData3);
        l_stringBuf.append(",  extra data 4: "            + i_extraData4);
        l_stringBuf.append(",  op id: '"                  + i_opId + "'.");

        return l_stringBuf.toString();
    }

    /**
     * @return Returns the executionDateTime.
     */
    public PpasDateTime getExecutionDateTime()
    {
        return i_executionDateTime;
    }

    /**
     * @return Returns the extraData1.
     */
    public String getExtraData1()
    {
        return i_extraData1;
    }

    /**
     * @return Returns the extraData2.
     */
    public String getExtraData2()
    {
        return i_extraData2;
    }

    /**
     * @return Returns the extraData3.
     */
    public String getExtraData3()
    {
        return i_extraData3;
    }

    /**
     * @return Returns the extraData4.
     */
    public String getExtraData4()
    {
        return i_extraData4;
    }

    /**
     * @return Returns the fileDate.
     */
    public PpasDate getFileDate()
    {
        return i_fileDate;
    }

    /**
     * @return Returns the jobType.
     */
    public String getJobType()
    {
        return i_jobType;
    }

    /**
     * @return Returns the jsJobId.
     */
    public long getJsJobId()
    {
        return i_jsJobId;
    }

    /**
     * @return Returns the noOfRejectedRecs.
     */
    public long getNoOfRejectedRecs()
    {
        return i_noOfRejectedRecs;
    }

    /**
     * @return Returns the noOfSuccessfullyRecs.
     */
    public long getNoOfSuccessfullyRecs()
    {
        return i_noOfSuccessfullyRecs;
    }

    /**
     * @return Returns the opId.
     */
    public String getOpId()
    {
        return i_opId;
    }

    /**
     * @return Returns the seqNo.
     */
    public int getSeqNo()
    {
        return i_seqNo;
    }

    /**
     * @return Returns the status.
     */
    public char getStatus()
    {
        return i_status;
    }

    /**
     * @return Returns the subJobCnt.
     */
    public int getSubJobCnt()
    {
        return i_subJobCnt;
    }

    /**
     * @return Returns the subSeqNo.
     */
    public int getSubSeqNo()
    {
        return i_subSeqNo;
    }

    /**
     * Sets the extraData1.
     * @param p_extraData1 The Extra data.
     */
    public void setExtraData1(String p_extraData1)
    {
        i_extraData1 = p_extraData1;
    }
    
    /**
     * Sets the extraData2.
     * @param p_extraData2 The Extra data.
     */
    public void setExtraData2(String p_extraData2)
    {
        i_extraData2 = p_extraData2;
    }
    
    /**
     * Sets the extraData3.
     * @param p_extraData3 The Extra data.
     */
    public void setExtraData3(String p_extraData3)
    {
        i_extraData3 = p_extraData3;
    }
    
    /**
     * Sets the extraData4.
     * @param p_extraData4 The Extra data.
     */
    public void setExtraData4(String p_extraData4)
    {
        i_extraData4 = p_extraData4;
    }
    
    
    /**
     * Sets the file date.
     * @param p_fileDate The file date.
     */
    public void setFileDate(PpasDate p_fileDate)
    {
        i_fileDate = p_fileDate;
    }
 
    /**
     * Sets the JobType.
     * @param p_jobType The JobType.
     */
    public void setJobType(String p_jobType)
    {
        i_jobType = p_jobType;
    }
    
    /**
     * Sets the number of rejected records.
     * @param p_noOfRejectedRecs The number of rejected records.
     */
    public void setNoOfRejectedRecs(long p_noOfRejectedRecs)
    {
         i_noOfRejectedRecs = p_noOfRejectedRecs;
    }
    
   
    /**
     * Sets the number of successfully processed records.
     * @param p_noOfSuccessfullyRecs The number of successfully processed records.
     */
    public void setNoOfSuccessfullyRecs(long p_noOfSuccessfullyRecs)
    {
        i_noOfSuccessfullyRecs = p_noOfSuccessfullyRecs;
    }

    /**
     * Sets the opid.
     * @param p_opId The opid.
     */
    public void setOpId(String p_opId)
    {
        i_opId = p_opId;
    }

    /**
     * Sets the seqNo.
     * @param p_seqNo The sequence number.
     */
    public void setSeqNo(int p_seqNo)
    {
        i_seqNo = p_seqNo;
    }
    
    /**
     * Sets the status.
     * @param p_status The status.
     */
    public void setStatus(char p_status)
    {
        i_status = p_status;
    }
    
    /**
     * Sets the subJobCnt.
     * @param p_subJobCnt The subJobCnt.
     */
    public void setSubJobCnt(int p_subJobCnt)
    {
        i_subJobCnt = p_subJobCnt;
    }
    
    /**
     * Sets the subSeqNo.
     * @param p_subSeqNo The subSeqNo.
     */
    public void setSubSeqNo(int p_subSeqNo)
    {
        i_subSeqNo = p_subSeqNo;
    }   

    //------------------------------------------------------------------------
    // Private methods.
    //------------------------------------------------------------------------
    /**
     * Returns <code>true</code> if the specified <code>Object</code>s both are 'null' or if both are 
     * not 'null' and equal according to the first <code>Object</code>'s <code>equals</code> method.
     * Note: It is assumed that the first <code>Object</code>'s <code>equals</code> method handles a 'null'
     *       argument or an <code>Object</code> of a different type in a proper way.
     * 
     * @return <code>true</code> if the specified two <code>Object</code>s both are 'null'
     *         or if both are not 'null' and equal according to the first <code>Object</code>'s
     *         <code>equals</code> method, <code>false</code> otherwise.
     * 
     * @param p_obj1  the first  <code>Object</code>.
     * @param p_obj2  the second <code>Object</code>.
     */
    private boolean isEqual(Object p_obj1, Object p_obj2)
    {
        boolean l_equal = false;

        if (p_obj1 == p_obj2)
        {
            l_equal = true;
        }
        else if (p_obj1 != null && p_obj1.equals(p_obj2))
        {
            l_equal = true;
        }

        return l_equal;
    }
}
