////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScapServClassAdjParamSqlService.java
//      DATE            :       3-Jun-2004
//      AUTHOR          :       Bruno Ferrand-Broussy
//      REFERENCE       :       PRD_ASCS_DEV_SS_?
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Sql service to retrieve data from
//                              scap_serv_class_adj_param
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 22/12/05 | Lars L.    | Three new methods are added:    | PpacLon#1755/7585
//          |            | insert(..), update(..) and      |
//          |            | delete(..) a row in the         |
//          |            | SCAP_SERV_CLASS_ADJ_PARAM table.|
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScapServClassAdjDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Sql service to retrieve data from scap_serv_class_adj_param table.
 */
public class ScapServClassAdjParamSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScapServClassAdjParamSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new default charging indicators sql service.
     * 
     * @param p_request The request to process.
     */
    public ScapServClassAdjParamSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid Charging Indicators (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid fast adjustment data.
     * @throws PpasSqlException If the data cannot be read.
     */
    public ScapServClassAdjDataSet readAll(PpasRequest    p_request,
                                           JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement                 l_statement = null;
        SqlString                     l_sql;
        String                        l_adjType   = null;
        String                        l_adjCode   = null;
        char                          l_cWriteIndicator ;
        boolean                       l_writeIndicator = true;
        String                        l_opid;
        PpasDateTime                  l_genYmdhms;
        PpasCurrency                  l_currency = null;
        Money                         l_defaultAmount = null;
        ScapServClassAdjData          l_scapData;
        Vector                        l_allScapDataV = new Vector(20, 10);
        ScapServClassAdjDataSet       l_scapDataSet;
        ScapServClassAdjData[]        l_allScapParamARR;
        ScapServClassAdjData[]        l_scapParamEmptyARR = new ScapServClassAdjData [0];
        JdbcResultSet                 l_results;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered " +  C_METHOD_readAll);
        }
        
        l_sql =   new SqlString(500,
                                0,
                                "SELECT " +
                                "SCAP_ADJ_TYPE, " +
                                "SCAP_ADJ_CODE, " +
                                "SCAP_DEFAULT_ADJ_AMOUNT, " +
                                "SCAP_DEFAULT_CURRENCY_CODE, " +
                                "SCAP_WRITE_CUST_ADJ_INDIC, " +
                                "SCAP_OPID, " +
                                "cufm_precision, " +
                                "SCAP_GEN_YMDHMS " +
                                "FROM SCAP_SERV_CLASS_ADJ_PARAM, cufm_currency_formats " +
                                "WHERE SCAP_DEFAULT_CURRENCY_CODE = cufm_currency");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 10110, this,
                "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_readAll,
                                                   10120,
                                                   this,
                                                   p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while ( l_results.next(10140) )
        {
            l_writeIndicator   = true;
            l_adjCode          = l_results.getTrimString(10140, "SCAP_ADJ_CODE");
            l_adjType          = l_results.getTrimString(10145, "SCAP_ADJ_TYPE");
            l_cWriteIndicator  = l_results.getChar      (10150, "SCAP_WRITE_CUST_ADJ_INDIC");
            if (l_cWriteIndicator == 'N')
            {
                l_writeIndicator = false;
            }
            l_currency          = new PpasCurrency(
                                        l_results.getTrimString(13051, "SCAP_DEFAULT_CURRENCY_CODE"),
                                        l_results.getInt(       13052, "cufm_precision"));

            l_defaultAmount         = l_results.getMoney   (12080, "SCAP_DEFAULT_ADJ_AMOUNT", l_currency);

            l_opid             = l_results.getTrimString(10200, "SCAP_OPID");
            l_genYmdhms        = l_results.getDateTime  (10210, "SCAP_GEN_YMDHMS");

            l_scapData = new ScapServClassAdjData(
                                            l_adjType,
                                            l_adjCode,            
                                            l_defaultAmount,
                                            l_writeIndicator,
                                            l_opid,
                                            l_genYmdhms);

            l_allScapDataV.addElement(l_scapData);
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10240, this, p_request);

        l_allScapParamARR = (ScapServClassAdjData[])l_allScapDataV.toArray(
                                l_scapParamEmptyARR);

        l_scapDataSet = new ScapServClassAdjDataSet(l_allScapParamARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10250, this,
                "Leaving " + C_METHOD_readAll);
        }

        return l_scapDataSet;
    }


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /***************************************************************************
     * Inserts a new row into the SCAP_SERV_CLASS_ADJ_PARAM database table.
     * 
     * @param p_ppasRequest    The request to process.
     * @param p_connection     The database Connection.
     * @param p_opid           The operator making the change.
     * @param p_adjType        The adjustment type.
     * @param p_adjCode        The adjustment code.
     * @param p_defaultAmount  The default amount as a <code>Money</code> object.
     * @param p_writeIndicator The write adjustment indicator.
     * 
     * @return The number of the inserted rows.
     * 
     * @throws PpasSqlException  if a new row cannot be inserted.
     */
    public int insert(PpasRequest    p_ppasRequest,
                      JdbcConnection p_connection,
                      String         p_opid,
                      String         p_adjType,
                      String         p_adjCode,
                      Money          p_defaultAmount,
                      char           p_writeIndicator) throws PpasSqlException
    {
        int              l_rowCount     = 0;
        StringBuffer     l_insertStrBuf = null;
        SqlString        l_sqlString    = null;
        JdbcStatement    l_statement    = null;       
        PpasSqlException l_ppasSqlEx    = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_ppasRequest,
                            C_CLASS_NAME, 10100, this,
                            "Entered " +  C_METHOD_insert);
        }

        // Create the inserts string.
        l_insertStrBuf = new StringBuffer("INSERT INTO scap_serv_class_adj_param (");
        l_insertStrBuf.append("SCAP_ADJ_TYPE, ");
        l_insertStrBuf.append("SCAP_ADJ_CODE, ");
        l_insertStrBuf.append("SCAP_DEFAULT_ADJ_AMOUNT, ");
        l_insertStrBuf.append("SCAP_DEFAULT_CURRENCY_CODE, ");
        l_insertStrBuf.append("SCAP_WRITE_CUST_ADJ_INDIC, ");
        l_insertStrBuf.append("SCAP_OPID, ");
        l_insertStrBuf.append("SCAP_GEN_YMDHMS) ");
        l_insertStrBuf.append("VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})");

        // Create the SQL string.
        l_sqlString = new SqlString(150, 4, l_insertStrBuf.toString());
        l_sqlString.setStringParam(      0, p_adjType);
        l_sqlString.setStringParam(      1, p_adjCode);
        l_sqlString.setMoneyParam(       2, p_defaultAmount);
        l_sqlString.setCurrencyCodeParam(3, p_defaultAmount);
        l_sqlString.setCharParam(        4, p_writeIndicator);
        l_sqlString.setStringParam(      5, p_opid);
        l_sqlString.setDateTimeParam(    6, DatePatch.getDateTimeNow());

        try
        {                   
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       16050,
                                                       this,
                                                       p_ppasRequest);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_ppasRequest,
                                                   l_sqlString);
        }        
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_ppasRequest);
                l_statement = null;
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in SCAP_SERV_CLASS_ADJ_PARAM table.");
            }

            l_ppasSqlEx = new PpasSqlException(C_CLASS_NAME,
                                               C_METHOD_insert,
                                               10050,
                                               this,
                                               p_ppasRequest,
                                               0,
                                               SqlKey.get().databaseInconsistency());
            throw (l_ppasSqlEx);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_ppasRequest,
                            C_CLASS_NAME, 10100, this,
                            "Leaving " +  C_METHOD_insert);
        }
        return l_rowCount;
    }


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /***************************************************************************
     * Updates a row in the SCAP_SERV_CLASS_ADJ_PARAM database table.
     * 
     * @param p_ppasRequest    The request to process.
     * @param p_connection     The database Connection.
     * @param p_opid           The operator making the change.
     * @param p_adjType        The adjustment type.
     * @param p_adjCode        The adjustment code.
     * @param p_defaultAmount  The default amount as a <code>Money</code> object.
     * @param p_writeIndicator The write adjustment indicator.
     * 
     * @return The number of updated rows.
     * 
     * @throws PpasSqlException If the update fails.
     */
    public int update(PpasRequest    p_ppasRequest,
                      JdbcConnection p_connection,
                      String         p_opid,
                      String         p_adjType,
                      String         p_adjCode,
                      Money          p_defaultAmount,
                      char           p_writeIndicator) throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        StringBuffer     l_updStrBuf = null;
        SqlString        l_sqlString = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlEx = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_ppasRequest,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " +  C_METHOD_update);
        }

        // Create the update string.
        l_updStrBuf = new StringBuffer("UPDATE scap_serv_class_adj_param ");
        l_updStrBuf.append("SET    SCAP_DEFAULT_ADJ_AMOUNT    = {0},");
        l_updStrBuf.append("       SCAP_DEFAULT_CURRENCY_CODE = {1},");
        l_updStrBuf.append("       SCAP_WRITE_CUST_ADJ_INDIC  = {2},");
        l_updStrBuf.append("       SCAP_OPID                  = {3},");
        l_updStrBuf.append("       SCAP_GEN_YMDHMS            = {4} ");
        l_updStrBuf.append("WHERE  SCAP_ADJ_TYPE              = {5} ");
        l_updStrBuf.append("AND    SCAP_ADJ_CODE              = {6}");

        // Create the SQL string.
        l_sqlString = new SqlString(150, 4, l_updStrBuf.toString());
        l_sqlString.setMoneyParam(       0, p_defaultAmount);
        l_sqlString.setCurrencyCodeParam(1, p_defaultAmount);
        l_sqlString.setCharParam(        2, p_writeIndicator);
        l_sqlString.setStringParam(      3, p_opid);
        l_sqlString.setDateTimeParam(    4, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(      5, p_adjType);
        l_sqlString.setStringParam(      6, p_adjCode);

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       p_ppasRequest);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   p_ppasRequest,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_ppasRequest);
                l_statement = null;
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in acgr_account_group table.");
            }

            l_ppasSqlEx = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_ppasRequest,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlEx);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_ppasRequest,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;     
    }


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /***************************************************************************
     * Updates a row in the SCAP_SERV_CLASS_ADJ_PARAM database table.
     * 
     * @param p_ppasRequest    The request to process.
     * @param p_connection     The database Connection.
     * @param p_adjType        The adjustment type.
     * @param p_adjCode        The adjustment code.
     * 
     * @throws PpasSqlException If the update fails.
     */
    public void delete(PpasRequest     p_ppasRequest,
                       JdbcConnection  p_connection,
                       String          p_adjType,
                       String          p_adjCode) throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        StringBuffer     l_delStrBuf = null;
        SqlString        l_sqlString = null;
        PpasSqlException l_ppasSqlEx = null;
        int              l_rowCount  = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_ppasRequest,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " +  C_METHOD_delete);
        }

        // Create the update string.
        l_delStrBuf = new StringBuffer("DELETE FROM scap_serv_class_adj_param ");
        l_delStrBuf.append("WHERE  SCAP_ADJ_TYPE = {0} ");
        l_delStrBuf.append("AND    SCAP_ADJ_CODE = {1}");

        // Create the SQL string.
        l_sqlString = new SqlString(150, 4, l_delStrBuf.toString());
        l_sqlString.setStringParam(      0, p_adjType);
        l_sqlString.setStringParam(      1, p_adjCode);

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_delete,
                                                       20050,
                                                       this,
                                                       p_ppasRequest);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   20100,
                                                   this,
                                                   p_ppasRequest,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 20125, this, p_ppasRequest);
                l_statement = null;
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to delete a row in scap_serv_class_adj_param table.");
            }

            l_ppasSqlEx = new PpasSqlException(C_CLASS_NAME,
                                               C_METHOD_delete,
                                              10150,
                                              this,
                                              p_ppasRequest,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlEx);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_ppasRequest,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
    }
}
