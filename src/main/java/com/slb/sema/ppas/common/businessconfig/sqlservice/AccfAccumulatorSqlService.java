////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccfAccumulatorSqlService.Java
//      DATE            :       03-Sep-2002
//      AUTHOR          :       Mike Hickman
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_353
//                              PpaLon#1517/6395
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the accf accumulator business
//                              config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AccfAccumulatorDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Implements a service that performs the SQL required by the accf accumulator
 * business config cache.
 */
public class AccfAccumulatorSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccfAccumulatorSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new sql service to be used in generating the data for the
     * accf accumulator business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public AccfAccumulatorSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 11310, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 11390, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_read = "read";
    /**
     * Reads and returns the accf_accumulator_configuration table data.
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return A data set containing the accumulator records read from the db.
     * @throws PpasSqlException If the data cannot be read.
     */
    public AccfAccumulatorDataSet read(PpasRequest     p_request,
                                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        JdbcStatement            l_statement;
        JdbcResultSet            l_results;
        SqlString                l_sql = null;
        int                      l_srva;
        int                      l_sloc;
        Market                   l_market;
        ServiceClass             l_servClass = null;
        int                      l_id;
        String                   l_description;
        String                   l_unitsDescription;
        AccfAccumulatorData      l_accfData;
        Vector                   l_accfDataV = new Vector(50, 20);
        AccfAccumulatorData[]    l_accfDataArr;
        AccfAccumulatorDataSet   l_accfDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 11410, this,
                "Entered " +  C_METHOD_read);
        }

        l_sql = new SqlString(500, 0, "SELECT " +
                        "accf_srva, " +
                        "accf_sloc, " +
                        "accf_service_class, " +
                        "accf_id, " +
                        "accf_description, " +
                        "accf_units " +
                "FROM " +
                        "accf_accumulator_configuration");

        // Create the statement object and execute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_read, 11420, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_read, 11430, this, p_request, l_sql);
        while (l_results.next(11440))
        {
            l_srva   = l_results.getInt(11445, "accf_srva");
            l_sloc   = l_results.getInt(11450, "accf_sloc");
            l_market = new Market(l_srva, l_sloc);

            l_servClass = l_results.getServiceClass(11460, "accf_service_class");
            l_id = l_results.getInt(11470, "accf_id");
            l_description = l_results.getTrimString(11480, "accf_description");
            l_unitsDescription = l_results.getTrimString(11480, "accf_units");

            l_accfData = new AccfAccumulatorData(p_request,
                                                 l_market,
                                                 l_servClass,
                                                 l_id,
                                                 l_description,
                                                 l_unitsDescription);

            l_accfDataV.addElement(l_accfData);
        }

        l_results.close (11740);

        l_statement.close(C_CLASS_NAME, C_METHOD_read, 11750, this, p_request);

        l_accfDataArr = (AccfAccumulatorData[])l_accfDataV.toArray(new AccfAccumulatorData[0]);

        l_accfDataSet = new AccfAccumulatorDataSet(p_request, l_accfDataArr);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 11790, this,
                "Leaving " + C_METHOD_read);
        }

        return (l_accfDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts an accumulator record into the accf_accumulator_configuration database table.
     * 
     * @param p_request The request to process.
     * @param p_connection       Database Connection.
     * @param p_market           Market identifier.
     * @param p_serviceClass     Service Class identifier.
     * @param p_accumulatorId    Accumulator identifier.
     * @param p_accumulatorDesc  Accumulator description.
     * @param p_accumulatorUnits Accumulator units.
     * @param p_userOpid         Operator updating the business config record.
     * @throws PpasSqlException  Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       int            p_accumulatorId,
                       String         p_accumulatorDesc,
                       String         p_accumulatorUnits,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10000, 
                this,
                "Entered " + C_METHOD_insert);
        }

        l_sql = "INSERT INTO accf_accumulator_configuration (" +
                    "accf_srva, " +
                    "accf_sloc, " +
                    "accf_service_class, " +
                    "accf_id, " +
                    "accf_description, " +
                    "accf_units, " +
                    "accf_opid, " +
                    "accf_gen_ymdhms ) " +
                "VALUES(" + 
                    "{0},{1},{2},{3},{4}, {5}, {6}, {7})";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setIntParam     (3, p_accumulatorId);
        l_sqlString.setStringParam  (4, p_accumulatorDesc);
        l_sqlString.setStringParam  (5, p_accumulatorUnits);
        l_sqlString.setStringParam  (6, p_userOpid);
        l_sqlString.setDateTimeParam(7, l_now);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 10010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10020,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 10030, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10040, 
                    this,
                    "Database error: unable to insert row in accf_accumulator_configuration table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10050,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10060, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    }
   
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates an accumulator record in the accf_accumulator_configuration database table.
     * 
     * @param p_request The request to process.
     * @param p_connection       Database Connection.
     * @param p_market           Market identifier.
     * @param p_serviceClass     Service Class identifier.
     * @param p_accumulatorId    Accumulator identifier.
     * @param p_accumulatorDesc  Accumulator description.
     * @param p_accumulatorUnits Accumulator units.
     * @param p_userOpid         Operator updating the business config record.
     * @throws PpasSqlException  Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       int            p_accumulatorId,
                       String         p_accumulatorDesc,
                       String         p_accumulatorUnits,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10100, 
                this,
                "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE accf_accumulator_configuration " +
                "SET accf_description = {4}, " +
                    "accf_units = {5}, " +
                    "accf_opid = {6}, " +
                    "accf_gen_ymdhms = {7} " +
                "WHERE accf_id = {3} " +
                "AND   accf_srva = {0}" +
                "AND   accf_sloc = {1}" +
                "AND   accf_service_class = {2}";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setIntParam     (3, p_accumulatorId);
        l_sqlString.setStringParam  (4, p_accumulatorDesc);
        l_sqlString.setStringParam  (5, p_accumulatorUnits);
        l_sqlString.setStringParam  (6, p_userOpid);
        l_sqlString.setDateTimeParam(7, l_now);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 10110, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       10120,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 10130, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10140, 
                    this,
                    "Database error: unable to update row in accf_accumulator_configuration table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_update,
                                       10150,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10160, 
                this,
                "Leaving " + C_METHOD_update);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the coch_community_charging database table.
     * 
     * @param p_request The request to process.
     * @param p_connection       Database Connection.
     * @param p_market           Market identifier.
     * @param p_serviceClass     Service Class identifier.
     * @param p_accumulatorId    Accumulator identifier.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       int            p_accumulatorId)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasDateTime     l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10200, 
                this,
                "Entered " + C_METHOD_delete);
        }

        l_sql = "DELETE from  accf_accumulator_configuration " +
                "WHERE accf_srva = {0} " +
                "AND   accf_sloc = {1} " +
                "AND   accf_service_class = {2}" +
                "AND   accf_id = {3}";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setIntParam     (3, p_accumulatorId);
        l_sqlString.setDateTimeParam(4, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10210,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10220,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 10230, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10240, 
                    this,
                    "Database error: unable to delete row from accf_accumulator_configuration table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10250,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10260, 
                this,
                "Leaving " + C_METHOD_delete);
        }
    }
}
