////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchSynchPromoResultData
//      DATE            :       17-Sept-2004
//      AUTHOR          :       Bruno Ferrand-Broussy
//      REFERENCE       :
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       An object that represents a promotion allocations data and a
//                              basic account.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/** Customer Promotion Allocation Data.
 * This class contains information relating to a promotion applied to an account.
 */
public class BatchSynchPromoResultData extends DataObject
{
    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------

    /** The new promotion. */
    private CustPromoAllocData        i_newPromotion         = null;
    /** The old promotion. */
    private CustPromoAllocData        i_oldPromotion         = null;
    
    /** The basic account data. */
    private BasicAccountData          i_account              = null;
    /** Promotion allocations for this customer. */
    private CustPromoAllocDataSet     i_allocations          = null;
    
    /**
     * Standard class name constant to be used in calls to Middleware methods.
     */
    private static final String C_CLASS_NAME = "BatchSynchPromoResultData";

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Simple constructor that instantiates an empty CustPromoAllocData object.
     * @param p_account     Basic details of the account.
     * @param p_allocations Promotion allocations for this account.
     */
    public BatchSynchPromoResultData(BasicAccountData      p_account,
                                     CustPromoAllocDataSet p_allocations)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE | PpasDebug.C_APP_BUSINESS,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 11010, this,
                "Constructing " + C_CLASS_NAME);
        }
        
        i_account     = p_account;
        i_allocations = p_allocations;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE | PpasDebug.C_APP_BUSINESS,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 11010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }
    
    /**
     * Returns the basic accoutn data.
     * @return Returns the account.
     */
    public BasicAccountData getAccount()
    {
        return i_account;
    }
    
    /**
     * Returns priomtion allocations relevant to this subscriber.
     * @return Returns the account.
     */
    public CustPromoAllocDataSet getAllocations()
    {
        return i_allocations;
    }
    
    /**
     * Returns the new Promotion.
     * @return Returns the new Promotion.
     * @deprecated
     */
    public CustPromoAllocData getNewPromotion()
    {
        return i_newPromotion;
    }
    /**
     * Returns the old Promotion.
     * @return Returns the old Promotion.
     * @deprecated
     */
    public CustPromoAllocData getOldPromotion()
    {
        return i_oldPromotion;
    }
}
