////////////////////////////////////////////////////////////////////////////////
//       ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//       FILE NAME       :       AFBulkLoadBatchResponseData.java
//       DATE            :       04-Nov-2004
//       AUTHOR          :       Neil Raymond (r41087d)
//       REFERENCE       :       PRD_ASCS00_GEN_CA_37
//
//       COPYRIGHT       :       Atos Origin 2004
//
//       DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//       CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.exceptions.PpasServiceException;

/**
 */
public class AFBulkLoadBatchResponseData extends DataObject
{

    /** The MSISDN that this object represents. */
    private Msisdn i_msisdn = null;
    
    /** The exception that was thrown whilst adding this MSISDN to the AF. */
    private PpasServiceException i_exception = null;

    /**
     * @param p_msisdn the MSISDN that was added to the account finder
     * @param p_exception the exception that was thrown when adding the MSISDN to the AF, or <code>null</code>
     */
    public AFBulkLoadBatchResponseData(Msisdn               p_msisdn,
                                       PpasServiceException p_exception)
    {
        super();
        
        i_msisdn = p_msisdn;
        i_exception = p_exception;
    }
    
    
    /**
     * @return Returns the exception.
     */
    public PpasServiceException getException()
    {
        return i_exception;
    }

    /**
     * @param p_exception The exception to set.
     */
    public void setException(PpasServiceException p_exception)
    {
        i_exception = p_exception;
    }
    /**
     * @return Returns the msisdn.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }
}
