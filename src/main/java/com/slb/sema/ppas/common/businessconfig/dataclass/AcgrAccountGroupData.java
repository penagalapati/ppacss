////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :  9500     
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       AcgrAccountGroupData.java
//    DATE            :       03-Jun-2004
//    AUTHOR          :       Oualid Gharach 78022K
//    REFERENCE       :       PpaLon#288/2615
//
//
//    COPYRIGHT       :       ATOSORIGIN 2004
//
//    DESCRIPTION     :       Stores a row from the ACGR_ACCOUNT_GROUP database
//                            table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 | 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a row from the ACGR_ACCOUNT_GROUP database table.
 */
public class AcgrAccountGroupData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AcgrAccountGroupData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The account group ID. */
    private AccountGroupId i_accountID;
    
    /** The account group description. */
    private String i_accountDescription;
    
    /** Flag indicating if this adjustment code is deleted. */
    private boolean i_delFlag = false;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates an Account Group Data object.
     * 
     * @param p_request The request to process.
     * @param p_accountID The account group id.
     * @param p_delFlag The deleted flag.
     * @param p_opid    Operator that last changed this record.
     * @param p_genYmdHms Date/time of last change to this record.
     * @param p_accountDescription The account group description.
     */
    public AcgrAccountGroupData(PpasRequest     p_request,
                                AccountGroupId  p_accountID,
                                String          p_accountDescription,
                                char            p_delFlag,
                                String          p_opid,
                                PpasDateTime    p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_accountID          = p_accountID;
        i_accountDescription = p_accountDescription;
        i_delFlag            = (p_delFlag == '*');
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /**
     * Return the account group id.
     * @return the account group id
     */
    public AccountGroupId getAccountID()
    {
        return i_accountID;
    }

    /**
     * Return the account group description.
     * @return the account group description
     */
    public String getAccountDescription()
    {
        return i_accountDescription;
    }

    /** Returns true if this account group has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_delFlag;
    }
}
