////////////////////////////////////////////////////////////////////////////////
//      ACSC IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ValaValidLanguageCache.Java
//      DATE            :       17-Feb-2001
//      AUTHOR          :       Marek Vonka
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A cacheing valid language business configuration
//                              service (vala_valid_language).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ValaValidLanguageSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** A cacheing valid language business configuration service (vala_valid_language). */
public class ValaValidLanguageCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ValaValidLanguageCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The valid language service to use to load cache. */
    private ValaValidLanguageSqlService i_valaValidLanguageSqlService = null;

    /** Cache of all valid languages (including ones marked as deleted). */
    private ValaValidLanguageDataSet i_allValaValidLanguages = null;

    /**
     * Cache of available valid languages (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated from i_allValaValidLanguagesARR each time it is required.
     */
    private ValaValidLanguageDataSet i_availableValaValidLanguages = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid language cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public ValaValidLanguageCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing ValaValidLanguageCache()");
        }

        i_valaValidLanguageSqlService = new ValaValidLanguageSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed ValaValidLanguageCache()");
        }
    } // End of public constructor
      //         ValaValidLanguageCache(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid languages (including ones marked as deleted).
     * 
     * @return All valid languages.
     */
    public ValaValidLanguageDataSet getAll()
    {
        return(i_allValaValidLanguages);
    } // End of public method getAll

    /**
     * Returns available valid languages (not including ones marked as deleted).
     * 
     * @return Available languages.
     */
    public ValaValidLanguageDataSet getAvailable()
    {
        return(i_availableValaValidLanguages);
    } // End of public method getAvailable

    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10140, this,
                "Entered reload");
        }

        i_allValaValidLanguages = i_valaValidLanguageSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all languages and old list of deleted
        // languages, but rather then synchronise this is acceptable.
        i_availableValaValidLanguages = new ValaValidLanguageDataSet(
                                  p_request,
                                  i_allValaValidLanguages.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10150, this,
                "Leaving reload");
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allValaValidLanguages);
    }
}

