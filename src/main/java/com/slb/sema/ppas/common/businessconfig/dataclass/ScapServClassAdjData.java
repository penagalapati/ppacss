////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScapServClassAdjData.java
//      DATE            :       3-Jun-2004
//      AUTHOR          :       Bruno Ferrand-Broussy
//      REFERENCE       :       
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A record of Scap Service class Adjustment Parameter data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Object holding charging indicator data configuration.
 */
public class ScapServClassAdjData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScapServClassAdjData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Default Amount. */
    private Money i_defaultAmount = null;
    
    /** Flag to know if we need to write or not the Adjustment in the Transaction list.*/
    private boolean i_writeIndicator = true;
    
    /** The opid that last updated this record. */
    private String i_opid = null;
    
    /** The adjustment type. */
    private String i_adjType = null;
    
    /** The adjustment code. */
    private String i_adjCode = null;
    
    /** The date/time of the last update. */
    private PpasDateTime i_genYmdhms = new PpasDateTime("");
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of <code>ScapServClassAdjData</code>.
     * @param p_adjType The adjustment type.
     * @param p_adjCode The adjustment code.
     * @param p_defaultAmount The defalt amount.
     * @param p_writeIndicator The write indicator.
     * @param p_opid the opid that last updated this record
     * @param p_genYmdhms date/time of the last update
     */
    public ScapServClassAdjData(String       p_adjType,
                                String       p_adjCode,
                                Money        p_defaultAmount,
                                boolean      p_writeIndicator,
                                String       p_opid,
                                PpasDateTime p_genYmdhms)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        i_defaultAmount  = p_defaultAmount;
        i_writeIndicator = p_writeIndicator;
        i_opid           = p_opid;
        i_genYmdhms      = p_genYmdhms;
        i_adjType        = p_adjType;
        i_adjCode        = p_adjCode;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the default Amount.
     * @return the default Amount.
     */
    public Money getDefaultAmount()
    {
        return i_defaultAmount;
    }

    /**
     * Get the write indicator flag.
     * @return the write indicator flag.
     */
    public boolean isWriteIndicator()
    {
        return i_writeIndicator;
    }
    
    /**
     * The opid that last updated this record.
     * @return opid of last update
     */
    public String getOpid()
    {
        return i_opid;
    }

    /**
     * The date/time that this record was last updated.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }

    /**
     * The adjustment type.
     * @return the adjustment type.
     */
    public String getAdjType()
    {
        return i_adjType;
    }

    /**
     * The adjustment code.
     * @return the adjustment code.
     */
    public String getAdjCode()
    {
        return i_adjCode;
    }
    /**
     * To display the parameter as a string.
     * @return The string.
     */
    public String toString()
    {
        StringBuffer l_buffer = new StringBuffer(100);

        l_buffer.append("ScapServClassAdjData@");
        l_buffer.append(this.hashCode());
        l_buffer.append(", ADJ type=[ " + i_adjType);
        l_buffer.append("], ADJ code=[" + i_adjCode);
        l_buffer.append("], write indicator=[");
        if (i_writeIndicator)
        {
            l_buffer.append("TRUE");
        }
        else
        {
            l_buffer.append("FALSE");
        }
        l_buffer.append("], defaultAmount=[" + i_defaultAmount.toString());
        l_buffer.append("], opid=[" + i_opid);
        l_buffer.append("], genYYYYMMDDSS=[" + i_genYmdhms + "]");
        return (l_buffer.toString());
    }

}
