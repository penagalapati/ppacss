////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BicsysUsersrvasData.Java
//      DATE            :       25-June-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A record of market information
//                              (corresponding to a single row of the
//                              bicsys_usersrvas table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of market information (i.e.
 * a single row of the srva_mstr table).
 */
public class BicsysUsersrvasData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BicsysUsersrvasData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Operator Id. */
    private  String            i_opid = null;

    /** The market.
     */
    private  Market            i_market = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates market data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this data is associated.
     * @param p_opid    The operator that last changed this data.
    */
    public BicsysUsersrvasData(
        PpasRequest            p_request,
        String                 p_opid,
        Market                 p_market)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market        = p_market;
        i_opid          = p_opid;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the market.
     * 
     * @return The market to which this data is associated.
     */
    public Market getMarket ()
    {
        return (i_market);
    }

    /** Get operator Id who created/lst accessed this market record in the database.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid()
    {
        return (i_opid);
    }

    /** Checks whether this object is the same as another instance.
     * 
     * @param p_object Object to which this instance should be compared.
     * @return True if the objects are logically the same.
     */
    public boolean equals(Object p_object)
    {
        boolean l_return = false;
        
        if (((Market)p_object).equals(i_market))
        {
            l_return = true;
        }
        
        return l_return;
    }

} // End of public class BicsysUsersrvasData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
