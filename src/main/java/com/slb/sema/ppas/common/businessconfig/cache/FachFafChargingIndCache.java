////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FachFafChargingIndCache.java
//      DATE            :       15-Mar-2004
//      AUTHOR          :       MAGray - q92528d
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       A cache for Charging Indicators 
//                              (Table FACH_FAF_CHARGING_IND).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.FachFafChargingIndSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for F&F Charging Indicators
 * (Table FACH_FAF_CHARGING_IND).
 */
public class FachFafChargingIndCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FachFafChargingIndCodeCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The F&F Charging Indicator Codes SQL service to use to load cache.
     */
    private FachFafChargingIndSqlService i_fachFafChargingIndSqlService = null;

    /** Cache of all valid Charging Indicators (including ones marked as deleted). */
    private FachFafChargingIndDataSet i_allFachFafChargingInd = null;

    /**
     * Cache of available Charging Indicators (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated from the complete list each time it is required.
     */
    private FachFafChargingIndDataSet i_availableFachFafChargingInd = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Charging Indicators cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public FachFafChargingIndCache(PpasRequest    p_request,
                                   Logger         p_logger,
                                   PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_fachFafChargingIndSqlService = new FachFafChargingIndSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Charging Indicators (including ones marked as deleted).
     * 
     * @return All valid Charging Indicators.
     */
    public FachFafChargingIndDataSet getAll()
    {
        return i_allFachFafChargingInd;
    }

    /**
     * Returns available valid Charging Indicators (not including ones marked as deleted).
     * 
     * @return Available Charging Indicators.
     */
    public FachFafChargingIndDataSet getAvailable()
    {
        return i_availableFachFafChargingInd;
    }

    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered reload");
        }

        i_allFachFafChargingInd = i_fachFafChargingIndSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all rates and old list of deleted
        // languages, but rather then synchronise this is acceptable.
        i_availableFachFafChargingInd = new FachFafChargingIndDataSet(
                                  i_allFachFafChargingInd.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10110, this,
                "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allFachFafChargingInd);
    }
}

