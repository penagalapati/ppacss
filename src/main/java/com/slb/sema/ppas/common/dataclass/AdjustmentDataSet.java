////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentDataSet.java
//      DATE            :       29-May-2001
//      AUTHOR          :       Matt Kirk
//
//      DESCRIPTION     :       An object that contains a set of Adjustment
//                              objects (AdjustmentData)for a subscriber.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A set of adjustment data objects representing a subscribers adjustment
 * history details.
 */
public class AdjustmentDataSet extends EventHistoryDataSet
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Middleware methods. */
    private static final String C_CLASS_NAME = "AdjustmentDataSet";

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Creates an empty AdjustmentDataSet.
     *
     * @param p_request The request to process.
     */
    public AdjustmentDataSet(PpasRequest p_request)
    {
        super(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 20020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    } // End public constructor

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Returns a Vector containing references to the AdjustmentData
     * instances in the data set.
     *
     * @return The set of adjsutment data objects.
     */
    public Vector getDataSetV()
    {
        return new Vector(getEventHistory());
    }

    /**
     * Returns a verbose string representation of the object. Can be used for trace and debugging.
     *
     * @return String representation of this data set.
     */
    public String toVerboseString()
    {
        StringBuffer           l_buffer = new StringBuffer();

        l_buffer.append(C_CLASS_NAME + "@" + this.hashCode() + "\n");
        l_buffer.append(super.toVerboseString());

        return l_buffer.toString();
    }
} // end public class AdjustmentDataSet
