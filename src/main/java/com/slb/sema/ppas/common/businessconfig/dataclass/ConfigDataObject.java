////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ConfigDataObject.java
//      DATE            :       26-Jul-2004
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Configuration Data object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;

/** Data object for configuration data. */
public class ConfigDataObject extends DataObject
{
    /** Operator identifier of the user that last changed this record. */
    private String  i_opid = null;
        
    /** Date/time of the last change to this record. */
    private PpasDateTime  i_genYmdHms = null;
        
    /** Standard constructor.
     * @param p_opid Identifier of the operator that last changed this data.
     * @param p_genYmdHms Date/time data was last changed.
     */
    public ConfigDataObject(String p_opid, PpasDateTime p_genYmdHms)
    {
        super();

        i_opid      = p_opid;
        i_genYmdHms = p_genYmdHms;
    }

    /** Get the identifier of the operator that last changed this data.
     * 
     * @return Operator identifier.
     */
    public String getOpid()
    {
        return i_opid;
    }

    /** Get the date/time of the last change to this data.
     * 
     * @return Date/time of last change.
     */
    public PpasDateTime getLastUpdateTime()
    {
        return i_genYmdHms;
    }
}
