////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       VospVoucherSplitsCache.Java
//      DATE            :       16-Aug-2002
//      AUTHOR          :       Nick Fletcher
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_382
//                              PpaLon#1500/6266
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the recharge division
//                              definitions held in the vosp_voucher_splits
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi       | Remove i_allVospByMarket, and   | CR#15/440
//          | Isaacs     | its instantiation in the        |
//          |            | constructor and reload method.  |
//          |            | Remove the getAll method which  |
//          |            | takes srva and sloc as input    |
//          |            | parameter                       |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.VospVoucherSplitsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the recharge division definitions business configuration data
 * (vosp_voucher_splits).
 */
public class VospVoucherSplitsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "VospVoucherSplitsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The SQL service to use to load cache. */
    private VospVoucherSplitsSqlService i_vospSqlService = null;

    /** Data set containing all recharge division data. */
    private VospVoucherSplitsDataSet i_allVospData = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new vosp voucher split cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public VospVoucherSplitsCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 11110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_vospSqlService = new VospVoucherSplitsSqlService (p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 11190, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all recharge division definitions.
     * @return Data set containing all VOSP table records held in this cache.
     */
    public VospVoucherSplitsDataSet getAll()
    {
        return(i_allVospData);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the data for this cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 11410, this,
                "Entered " + C_METHOD_reload);
        }

        i_allVospData = i_vospSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 11490, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allVospData);
    }
}

