////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountQueryResultDataUT.java
//      DATE            :       19-Dec-2008
//      AUTHOR          :       Sujatha C S
//
//      COPYRIGHT       :       
//
//      DESCRIPTION     :       Unit Test AccountQueryResult Data object
//
///////////////////////////////////////////////////////////////////////////////////////
// 17/03/09 | Sujatha C S| Added code for Active state         |CSR 1172149:GUI Airtime 
//          |            | testGetAirtimeStatusForActiveAirtime|Status shows Credit 
//          |            |                                     |Cleared incorrectly
//----------+------------+-------------------------------------+-----------------------
///////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

public class AccountQueryResultDataUT extends UtilTestCaseTT
{
    /** Default constructor giving name of the test.
    *
    * @param p_title Name of test.
    */
    public AccountQueryResultDataUT(String p_title)
    {
        super(p_title);
    }
    
    /** Perform standard activities at end of a test, for example, clear date patching. */
    protected void tearDown()
    {
        say("---------------------------------------");
        say("Clearing date patch...");
        DatePatch.clearPpasOffset();
        say("---------------------------------------");
    }
    AccountQueryResultData i_AccountQueryResultData = null;
    /** The flags after. */
    private SdpFlagsData i_flagsAfter = null;
    /** The airtime expiry date. */
    private PpasDate i_airtimeExpiryDate = new PpasDate((PpasDate)null);

    /** 
     *  Create an AccountQueryResultData object
     */
    public void createAccountQueryResultData()
    {
        i_AccountQueryResultData = new AccountQueryResultData(null,null,null,null,
                 null,null,i_flagsAfter,
                 null,i_airtimeExpiryDate,
                 31,
                 31,
                 null,null,null,
                 null,null,false,
                 false,null,null,null,
                 null,null,null,null,
                 null);

        DatePatch.setDateTime("20-Nov-2008 15:34:26");
    }   
    
    /** @category Default data object created
     *  @When Account is airtime expired
     *  @When Credit clear date is greater then current date
     *  @ut.then Airtime status of an account whose airtime has expired is 'AccountData.C_AIRTIME_EXPIRED_CHAR'.
     */
    public void testGetAirtimeStatusForExpiredAirtime()
    {
        beginOfTest("testGetAirtimeStatusForExpiredAirtime with future date");
        
        i_airtimeExpiryDate= new PpasDateTime("21-Oct-2008 12:13:14") ;
        i_flagsAfter = new SdpFlagsData("00000010"); 
        createAccountQueryResultData();
        
        assertEquals("Airtime has expired",AccountData.C_AIRTIME_EXPIRED_CHAR,
                           i_AccountQueryResultData.getAirtimeServiceStatus());     
        endOfTest();
    }
    
    /** @category Default data object created
     *  @When Account is airtime expired
     *  @When Credit clear date same as current date
     *  @ut.then Airtime status of an account whose airtime has expired is 'AccountData.C_AIRTIME_EXPIRED_CHAR'.
     */
    public void testGetAirtimeStatusForExpiredAirtime_p()
    {
        beginOfTest("testGetAirtimeStatusForExpiredAirtime_p for currnet date");
        
        i_airtimeExpiryDate= new PpasDateTime("20-Nov-2008 15:34:26") ;
        i_flagsAfter = new SdpFlagsData("00000010");
        createAccountQueryResultData();
        
        assertEquals("Airtime has expired",   AccountData.C_AIRTIME_EXPIRED_CHAR,
                             i_AccountQueryResultData.getAirtimeServiceStatus());
        endOfTest();
    }  
    
    /** @category Default data object created
     *  @When Account is airtime expired
     *  @When Credit clear date less then current date
     *  @ut.then Airtime status of an account whose airtime has expired is 'AccountData.C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR'.
     */
    public void testGetAirtimeStatusForExpiredAirtimeAndCreditCleared()
    {
        beginOfTest("testGetAirtimeStatusForExpiredAirtimeAndCreditCleared with past date");
        
        i_airtimeExpiryDate= new PpasDateTime("19-Oct-2008 12:13:14") ;
        i_flagsAfter = new SdpFlagsData("00000010");
        createAccountQueryResultData();
        
        assertEquals("Airtime has expired and credit has been cleared.",
                   AccountData.C_AIRTIME_EXPIRED_CREDIT_CLEARED_CHAR,
                         i_AccountQueryResultData.getAirtimeServiceStatus());
        endOfTest();
    }
    
    /** @category Default data object created
     *  @When Account has an airtime expiry warning.
     *  @ut.then Airtime status of an account is 'AccountData.C_AIRTIME_LOW_ANNOUNCEMENT_CHAR'.
     */
  /*  public void testGetAirtimeStatusForLowAirtime()
    {
        beginOfTest("testGetAirtimeStatusForLowAirtime with airtime expiry warning indicator");
        
        i_flagsAfter = new SdpFlagsData(8);
        createAccountQueryResultData();
        boolean l_airtimeWarningSet = i_flagsAfter.isAirtimeWarningSet();
         
        if(l_airtimeWarningSet == true)
        {
       
            assertEquals("Airtime for low airtime announcement period",
                           AccountData.C_AIRTIME_LOW_ANNOUNCEMENT_CHAR,
                          i_AccountQueryResultData.getAirtimeServiceStatus());
        
        }
        endOfTest();
    }*/
    
    /** @category Default data object created
     *  @When Account of the customer is active.
     *  @ut.then Airtime status of an account is 'AccountData.C_AIRTIME_ACTIVE_CHAR'.
     */
    public void testGetAirtimeStatusForActiveAirtime()
    {
        beginOfTest("testGetAirtimeStatusForActiveAirtime for active airtime");
        
        i_flagsAfter = new SdpFlagsData(128);
        createAccountQueryResultData();
        boolean l_airtimeActive = i_flagsAfter.isActivated();
        say("i_flagsAfter: "+i_flagsAfter);
           say("l_airtimeActive: "+l_airtimeActive);
           
           char l_airtimeStatus = AccountData.C_AIRTIME_ACTIVE_CHAR;
        if(l_airtimeActive)
        assertEquals("Customer with active airtime",
                                      l_airtimeStatus,   
                                      i_AccountQueryResultData.getAirtimeServiceStatus() );
        endOfTest();
    }
    
  
}

