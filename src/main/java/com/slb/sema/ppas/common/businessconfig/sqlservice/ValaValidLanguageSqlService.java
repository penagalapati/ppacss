////////////////////////////////////////////////////////////////////////////////
//      PPAS
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ValaValidLanguageSqlService.Java
//      DATE            :       17-Feb-2001
//      AUTHOR          :       Marek Vonka
//
//      DESCRIPTION     :       Valid language business config sql service
//                              (vala_valid_language).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                     | REFERENCE
//---------+---------------+---------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of           | <reference>
//         |               | change>                         |
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group 2001
//
// The copyright in this work belongs to Sema Group. The information
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group.
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ValaValidLanguageDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Valid language business configuration sql service (vala_valid_language).
 */
public class ValaValidLanguageSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ValaValidLanguageSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid language sql service.
     * 
     * @param p_request The request to process.
     */
    public ValaValidLanguageSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing ValaValidLanguageSqlService()");
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed ValaValidLanguageSqlService()");
        }

    } // End of public constructor
      //         ValaValidLanguageSqlService(PpasRequest, long, Logger)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid languages (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid languages.
     * @throws PpasSqlException If the data cannot be read.
     */
    public ValaValidLanguageDataSet readAll(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement             l_statement = null;
        SqlString                 l_sql;
        String                    l_valaCode;
        String                    l_valaDescription;
        String                    l_valaSyntax;
        String                    l_valaOpid;
        PpasDateTime              l_valaGenYmdhms;
        int                       l_valaAnnLanguageNumber;
        String                    l_valaDeleteFlag;
        ValaValidLanguageData     l_valaValidLangData;
        Vector                    l_allValaValidLangDataV = new Vector(20, 10);
        ValaValidLanguageDataSet  l_valaValidLanguageDataSet;
        ValaValidLanguageData     l_allValaValidLanguageARR[];
        ValaValidLanguageData     l_valaValidLanguageEmptyARR[] =
                new ValaValidLanguageData[0];
        JdbcResultSet             l_results;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 84650, this,
                "Entered " +  C_METHOD_readAll);
        }

        l_sql =   new SqlString(500, 0, "SELECT " +
                      "vala_code, " +
                      "vala_description, " +
                      "vala_ann_lang_number," +
                      "vala_syntax," +
                      "vala_opid," +
                      "vala_gen_ymdhms," +
                      "vala_del_flag " +
                      "FROM " +
                      "vala_valid_language");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 28760, this,
                "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 29000, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 29100, this, p_request, l_sql);

        while ( l_results.next(29110) )
        {
            l_valaCode              = l_results.getTrimString (29120, "vala_code");
            l_valaDescription       = l_results.getTrimString (29130, "vala_description");
            l_valaSyntax            = l_results.getTrimString (29210, "vala_syntax");
            l_valaOpid              = l_results.getTrimString (29220, "vala_opid");
            l_valaGenYmdhms         = l_results.getDateTime   (29230, "vala_gen_ymdhms");
            l_valaDeleteFlag        = l_results.getString     (29140, "vala_del_flag");
            l_valaAnnLanguageNumber = l_results.getInt        (29150, "vala_ann_lang_number");

            l_valaValidLangData = new ValaValidLanguageData(
                    p_request,
                    l_valaCode,
                    l_valaDescription,
                    l_valaSyntax,
                    l_valaAnnLanguageNumber,
                    l_valaDeleteFlag,
                    l_valaOpid,
                    l_valaGenYmdhms
                    );

            l_allValaValidLangDataV.addElement(l_valaValidLangData);
        }

        l_results.close(83470);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 90280, this, p_request);

        l_allValaValidLanguageARR =
                (ValaValidLanguageData[])l_allValaValidLangDataV.toArray(
                     l_valaValidLanguageEmptyARR);

        l_valaValidLanguageDataSet = new ValaValidLanguageDataSet(p_request, l_allValaValidLanguageARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10140, this,
                "Leaving " + C_METHOD_readAll);
        }

        return(l_valaValidLanguageDataSet);
    } // End of public method
      // readAll(PpasRequest, long, JdbcConnection)

} // End of public class ValaValidLanguageSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
