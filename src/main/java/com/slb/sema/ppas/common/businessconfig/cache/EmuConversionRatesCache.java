////////////////////////////////////////////////////////////////////////////////
//      ACSC IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       EmuConversionRatesCache.Java
//      DATE            :       10-Mar-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS_GEN_SS_071
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A cache for EMU Conversion Rates 
//                              (Table EMCR_EMU_CONVERSION_RATE).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.EmuConversionRatesDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.EmuConversionRatesSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for EMU Conversion Rates
 * (Table EMCR_EMU_CONVERSION_RATE).
 */
public class EmuConversionRatesCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "EmuConversionRatesCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The emu conversion rates service to use to load cache. */
    private EmuConversionRatesSqlService i_emuConversionRatesSqlService = null;

    /** Cache of all valid rates (including ones marked as deleted). */
    private EmuConversionRatesDataSet i_allEmuConversionRates = null;

    /**
     * Cache of available rates (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated from i_allEmuConversionRatesARR each time it is required.
     */
    private EmuConversionRatesDataSet i_availableEmuConversionRates = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid rates cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public EmuConversionRatesCache(PpasRequest    p_request,
                                   Logger         p_logger,
                                   PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_emuConversionRatesSqlService = new EmuConversionRatesSqlService(
                p_request,
                p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor
      //  EmuConversionRatesCache(PpasRequest, long, Logger)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid rates (including ones marked as deleted).
     * 
     * @return All valid rates.
     */
    public EmuConversionRatesDataSet getAll()
    {
        return(i_allEmuConversionRates);
    } // End of public method getAll

    /**
     * Returns available valid rates (not including ones marked as deleted).
     * 
     * @return Available rates.
     */
    public EmuConversionRatesDataSet getAvailable()
    {
        return(i_availableEmuConversionRates);
    } // End of public method getAvailable

    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered reload");
        }

        i_allEmuConversionRates = i_emuConversionRatesSqlService.readAll(
                                        p_request,
                                        p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all rates and old list of deleted
        // languages, but rather then synchronise this is acceptable.
        i_availableEmuConversionRates = new EmuConversionRatesDataSet(
                                  p_request,
                                  i_logger,
                                  i_allEmuConversionRates.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10110, this,
                "Leaving reload");
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allEmuConversionRates);
    }
}

