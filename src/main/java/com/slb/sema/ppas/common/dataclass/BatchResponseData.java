////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchResponseData.Java
//      DATE            :       21-Oct-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PRD_PPACS2_DEV_SS_47
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Response data class for all batch business 
//                              services returning a collection.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;
import java.util.Collection;

/** Response data class for all batch business services returning a collection. */
public class BatchResponseData implements Serializable
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    
    /** Batch response data. */
    private Collection i_batchData;
    
    /** Flag indicating whether all the records in the batch were successfully processed. */
    private boolean i_allSuccess = false;
    
    /** Number of requests sent to ESI by the business service. */
    private int i_esiRequestsSent = -1;
    
    /** Number of responses received from ESI by the business service. */
    private int i_esiResponsesReceived = 0;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Standard constructor.
     * @param p_batchData Batch response data.
     * @param p_allSuccess Flag indicating whether all the records in the batch were successfully processed.
     */
    public BatchResponseData(Collection p_batchData,
                             boolean    p_allSuccess)
    {
        i_batchData = p_batchData;
        i_allSuccess = p_allSuccess;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Returns batch response data. 
     * @return Batch response data. 
     */
    public Collection getData()
    {
        return i_batchData;
    }
    
    /** 
     * Sets batch response data. 
     * @param p_batchData Batch response data. 
     */
    public void setData(Collection p_batchData)
    {
        i_batchData = p_batchData;
    }
    
    /** 
     * Returns flag indicating whether all the records in the batch were successfully processed. 
     * @return p_allSuccess True if all the records in the batch were successfully processed. 
     */
    public boolean getAllSuccess()
    {
        return i_allSuccess;
    }
    
    /** 
     * Sets flag indicating whether all the records in the batch were successfully processed. 
     * @param p_allSuccess Flag indicating whether all the records in the batch were successfully processed. 
     */
    public void setAllSuccess(boolean p_allSuccess)
    {
        i_allSuccess = p_allSuccess;
    }
    
    /**
     * Sets the number of ESI requests sent by the batch service.
     * @param p_requestsSent Number of ESI requests sent by the batch service.
     */
    public void setEsiRequestsSent(int p_requestsSent)
    {
        i_esiRequestsSent = p_requestsSent;
    }
    
    /**
     * Increments the number of ESI responses received, and returns flag to
     * indicate whether all responses have been received.
     * @return True if responses have been received for all the ESI requests sent.
     */
    public synchronized boolean incrementEsiResponsesReceived()
    {
        return ((i_esiRequestsSent == ++i_esiResponsesReceived) ? true : false);
    }

    /** 
     * Returns string representation.
     * @return String representation of the object.
     */
    public String toString()
    {
        return("BatchResponseData: " +
               "[i_batchData = " + i_batchData + ", " +
               "i_allSuccess = " + i_allSuccess + ", " +
               "i_esiRequestsSent = " + i_esiRequestsSent + ", " +
               "i_esiResponsesReceived = " + i_esiResponsesReceived + "]");
    }
}