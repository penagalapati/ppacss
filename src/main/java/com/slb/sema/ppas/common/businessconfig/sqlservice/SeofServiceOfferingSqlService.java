////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SeofServiceOfferingSqlService.java
//      DATE            :       11-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PRD_ASCS00_GEN_CA_016
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Service used to read service offering data from the database.
//
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Service used to read service offering data from the database.
 */
public class SeofServiceOfferingSqlService
{

    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "SeofServiceOfferingSqlService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**
     * Reference tothe logger used within exceptions. 
     */
    private Logger i_logger = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new Sql Service for populating the account group business
     * configuration cache.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public SeofServiceOfferingSqlService(PpasRequest  p_request,
                                         Logger       p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Attempts to insert a new row into the SEOF_SERVICE_OFFERING database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_userOpid  Operator making the change.
     * @param p_serviceOfferingNumber Service offering number for this service offering.
     * @param p_serviceOfferingDescription Service offering description for this service offering.
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest    p_request,
                      JdbcConnection p_connection,
                      String         p_userOpid,                      
                      int            p_serviceOfferingNumber,
                      String         p_serviceOfferingDescription)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;        
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();        

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " +  C_METHOD_insert);                
        }
    
        l_sql = new String("INSERT INTO seof_service_offering " +
                           "(SEOF_NUMBER," +
                           " SEOF_DESCR," +
                           " SEOF_OPID," +
                           " SEOF_GEN_YMDHMS) " +
                           "VALUES({0}, {1}, {2}, {3})");

        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setIntParam     (0, p_serviceOfferingNumber);
        l_sqlString.setStringParam  (1, p_serviceOfferingDescription);
        l_sqlString.setStringParam  (2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);        

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       16050,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {            
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in seof_service_offering table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;     
    }        

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Deletes a row from the SEOF_SERVICE_OFFERING database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_serviceOfferingNumber Service offering number for this service offering.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       int                    p_serviceOfferingNumber)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            13000, 
                            this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("DELETE FROM seof_service_offering " +
                           "WHERE seof_number = {0}");

        l_sqlString = new SqlString(13050, 0, l_sql);

        l_sqlString.setIntParam (0, p_serviceOfferingNumber);

        try
        {    
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_delete,
                                                       13100,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   13150,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {   
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 13250, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10240, 
                    this,
                    "Database error: unable to mark row as deleted in seof_service_offering table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_delete,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            13300,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */   
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Method to read all service offering data from the database.
     * @param p_request The originating request.
     * @param p_connection A connection to the database.
     * @return A service offering data set.
     * @throws PpasSqlException If an error occurs when accessing the database.
     */
    public SeofServiceOfferingDataSet readAll (PpasRequest            p_request,
                                               JdbcConnection         p_connection)
        throws PpasSqlException
    {
        SeofServiceOfferingDataSet l_seofDataSet;
        SeofServiceOfferingData    l_seofRecord = null;
        SeofServiceOfferingData    l_seofEmptyARR[] = new SeofServiceOfferingData[0];
        SeofServiceOfferingData    l_seofARR[];
        JdbcStatement              l_statement;
        JdbcResultSet              l_results;
        SqlString                  l_sql;
        Vector                     l_seofV = new Vector (50, 50);
        String                     l_seofDesc;
        int                        l_seofNumber;


        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered " + C_METHOD_readAll);
        }
        
        l_sql = new SqlString(200, 0,
                              "SELECT seof_number, " +
                              " seof_descr, " +
                              " seof_opid, " +
                              " seof_gen_ymdhms " +
                              " FROM seof_service_offering " +
                              " ORDER BY seof_number");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 31420, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 31430, this, p_request, l_sql);

        while ( l_results.next(31440) )
        {
            l_seofNumber = l_results.getInt(11445, "SEOF_NUMBER");
            l_seofDesc   = l_results.getString(10300, "SEOF_DESCR").trim();

            l_seofRecord = new SeofServiceOfferingData(p_request, 
                                                       l_seofNumber, 
                                                       l_seofDesc,
                                                       l_results.getString(  10310, "SEOF_OPID").trim(),
                                                       l_results.getDateTime(10320, "SEOF_GEN_YMDHMS"));

            l_seofV.addElement (l_seofRecord);
        }

        l_results.close (31550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 31560, this, p_request);

        l_seofARR = (SeofServiceOfferingData [])l_seofV.toArray (l_seofEmptyARR);

        l_seofDataSet = new SeofServiceOfferingDataSet(p_request, l_seofARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10190,
                            this,
                            "Leaving " + C_METHOD_readAll);
        }

        return (l_seofDataSet);
    } 

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a row in the SEOF_SERVICE_OFFERING database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid       Operator making the change.
     * @param p_serviceOfferingId The Service Offering Id
     * @param p_serviceOfferingDescription The Service Offering description 
     * @return A count of the number of rows updated
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest            p_request,
                      JdbcConnection         p_connection,
                      String                 p_opid,
                      int                    p_serviceOfferingId,
                      String                 p_serviceOfferingDescription)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null; 

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 20000, this,
                            "Entered " +  C_METHOD_update);                
        }
    
        l_sql = new String("UPDATE seof_service_offering " +
                           "SET SEOF_DESCR = {0}, " +
                           " SEOF_OPID = {2}, " +
                           " SEOF_GEN_YMDHMS = {3} " +
                           "WHERE SEOF_NUMBER = {1}");
                       
        l_sqlString = new SqlString(150, 4, l_sql);
    
        l_sqlString.setStringParam  (0, p_serviceOfferingDescription);
        l_sqlString.setIntParam     (1, p_serviceOfferingId);
        l_sqlString.setStringParam  (2, p_opid);
        l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());

        try
        { 
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);                                            
        }
        finally
        {
            if (l_statement != null)
            {            
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 10130, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in seof_service_offering table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;     
    }
}
