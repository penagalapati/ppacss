////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatorRequestData.Java
//      DATE            :       15th March 2004
//      AUTHOR          :       Julien CHENELAT
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Data object used to store data for Accumulators 
//                              Update\Clear Request.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Data object used to represent an Accumulator.
 */
public class AccumulatorRequestData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants 
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccumulatorRequestData";
    
    /** Basic Account Data. */
    private BasicAccountData    i_basicAccountData           = null;
    
    /** Accumulator Id Array. */
    private int[]               i_accumulatorIdsArray        = null;
    
    /** Accumulators Description. */
    private String[]            i_accumulatorsDescription    = null;

    //-------------------------------------------------------------------------
    // Instance Attributes
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // constructors
    //------------------------------------------------------------------------
    /**
     * Construct an AccumulatorRequestData object.
     * 
     * @param p_basicAccountData        BasicAccountData
     * @param p_accumulatorIdsArray     Array of Accumulator Ids
     * @param p_accumulatorsDescription  Array of Accumulator Description
     * 
     */
    public AccumulatorRequestData( BasicAccountData     p_basicAccountData,
                                   int[]                p_accumulatorIdsArray,
                                   String[]             p_accumulatorsDescription )
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }
        
        i_basicAccountData           = p_basicAccountData;
        i_accumulatorIdsArray        = p_accumulatorIdsArray;
        i_accumulatorsDescription    = p_accumulatorsDescription;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 19000, this,
                "Constructed " + C_CLASS_NAME );
        }
    }
    
    //-------------------------------------------------------------------------
    // public methods
    //-------------------------------------------------------------------------
    /**
     * Return the basic account data.
     * 
     * @return BasicAccountData object.
     */
    public BasicAccountData  getBasicAccountData()
    {
        return i_basicAccountData;
    }

    /**
     * Return the array of accumulator ids.
     * 
     * @return Array of AccumulatorIds (i.e : Array of int).
     */
    public int[]  getAccumulatorIdsArray()
    {
        return i_accumulatorIdsArray;
    }
    
    /**
     * Return the array of accumulator description.
     * 
     * @return Array of Accumulators Description (i.e : Array of String).
     */
    public String[]  getAccumulatorsDescription()
    {
        return i_accumulatorsDescription;
    }
}