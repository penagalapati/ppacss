////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DireDisconnectReasonCache.Java
//      DATE            :       16-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the disconnection reasons
//                              data held in the dire_disconnect_reason table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.DireDisconnectReasonSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the disconnection reasons business configuration data
 * (from the dire_disconnect_reason table).
 */
public class DireDisconnectReasonCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DireDisconnectReasonCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private DireDisconnectReasonSqlService i_direSqlService;

    /** Data set containing all disconnect reasons in this codes. */
    private DireDisconnectReasonDataSet i_allDisconnectReasons;

    /**
     * Cache of available disconnect reasons (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private DireDisconnectReasonDataSet i_availableDisconnectReasons;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new disconnection reasons business configuration data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public DireDisconnectReasonCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 23110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_direSqlService = new DireDisconnectReasonSqlService (p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 23190, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all disconnect reasons data (including records marked as
     * deleted).
     * @return Data set containing all disconnect reasons records in this cache.
     */
    public DireDisconnectReasonDataSet getAll()
    {
        return(i_allDisconnectReasons);
    } // End of public method getAll

    /**
     * Returns available disconnect reasons data. Records marked as deleted are not included.
     * @return Data set containing available disconnect reasons records in this
     *         cache (i.e. those not marked as deleted).
     */
    public DireDisconnectReasonDataSet getAvailable()
    {
        return(i_availableDisconnectReasons);

    } // End of public method getAvailable

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the disconnection reasons from the DIRE table in this  cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 23230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allDisconnectReasons = i_direSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all discponnection reasons and old list
        // of available disconnection reasons, but rather than synchronise
        // this is acceptable.
        i_availableDisconnectReasons = new DireDisconnectReasonDataSet(
                          p_request,
                          i_allDisconnectReasons.getAvailableArray (p_request));

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 23290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allDisconnectReasons, i_availableDisconnectReasons);
    }
}

