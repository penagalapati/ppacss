////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       ArchiveColumnData.java
//DATE            :       29 September 2004
//AUTHOR          :       R.Grimshaw
//REFERENCE       :       PRD_ASCS00_DEV_SS_082
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY | <name>     | <brief description of           | <reference>
//|            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

/**
 * @author 7451F
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ArchiveColumnData
{
    private String i_columnName;
    
    private Object i_columnValue;
    
    ArchiveColumnData(String p_colummName, Object p_columnValue)
    {
        i_columnName = p_colummName;
        
        i_columnValue = p_columnValue;
    }
    
    public String getColumnName()
    {
        return i_columnName;
    }
    
    public Object getColumnValue()
    {
        return i_columnValue;
    }
    
    /** Display the ArchiveColumData information as a <code>String</code>.
     * 
     * @return String representation of the account.
     */
    public String toString()   
    {
        StringBuffer l_sB = new StringBuffer();
        l_sB.append("ArchiveColumData=[");
        l_sB.append("Column name: " + i_columnName);
        l_sB.append(",Column value: " + i_columnValue);
        l_sB.append("]");
        return l_sB.toString();
    }
}

