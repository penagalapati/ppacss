////////////////////////////////////////////////////////////////////////////////
//      ACSC IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      EmuConversionRatesDataSet.Java
//      DATE            :      10-Mar-2004
//      AUTHOR          :      Olivier Duparc
//      REFERENCE       :      PRD_ASCS_GEN_SS_071
//
//      COPYRIGHT       :      ATOS-ORIGIN 2004
//
//      DESCRIPTION     :      A set (collection) of Emu Conversion Rates data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a rates data set which can be read from * the database.
 */
public class EmuConversionRatesDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "EmuConversionRatesDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Logger to be used to log exceptions generated within this class.  */
    protected Logger   i_logger = null;

    /** Array of valid rates. */
    private EmuConversionRatesData[] i_emuConversionRatesArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of rates data.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger object to allow routing of messages.
     * @param p_emuConversionRatesArr A set of rates data objects.
     */
    public EmuConversionRatesDataSet(
        PpasRequest              p_request,
        Logger                   p_logger,
        EmuConversionRatesData[] p_emuConversionRatesArr)
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, 
                 C_CLASS_NAME, 
                 10000, 
                 this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;
        i_emuConversionRatesArr = p_emuConversionRatesArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, 
                 C_CLASS_NAME, 
                 10010, 
                 this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor
      //         EmuConversionRatesDataSet


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the array of all rates data objects held in this object.
     *  @return Array of all rates data objects held in this object.
     */
    public EmuConversionRatesData[]  getAll()
    {
        return (i_emuConversionRatesArr);
    }

    /** Get the conversion ratio from one currency to another
     *  for a given DateTime.
     *  Starts by retrieving conversion data for each currency.
     *  Then computes the ration between each currency.
     * 
     * @param p_fromCcy   ISO code of 'From' currency.
     * @param p_toCcy     ISO code of 'To' currency.
     * @param p_dateTime  Date/time of conversion. 
     * @return Exchange rate to convert between the currencies.
     */
    public double getConversionRate(String       p_fromCcy,
                                    String       p_toCcy,
                                    PpasDateTime p_dateTime)
    {
        double l_rate = 0.0;
        
        EmuConversionRatesData l_from = getWithCode(p_fromCcy, p_dateTime);
        EmuConversionRatesData l_to   = getWithCode(p_toCcy,   p_dateTime);

        if (l_from != null && l_to != null)
        {
            l_rate = l_to.getEuroConvRate() / l_from.getEuroConvRate();        
        }
        
        return l_rate;
    }
    
    /** Get the conversion rate from one currency to another
     *  for the current DateTime.
     * 
     * @param p_fromCcy   ISO code of 'From' currency.
     * @param p_toCcy     ISO code of 'To' currency.
     * @return Exchange rate to convert between the currencies.
     */
    public double getConversionRate(String       p_fromCcy,
                                    String       p_toCcy)
    {
        return getConversionRate(p_fromCcy, p_toCcy, DatePatch.getDateTimeNow());
    }
    
    /**
     * Returns the valid rates object for the supplied currency and date.
     * Returns the first rates object for the given currency where p_dateTime 
     * falls between start_date and end_date.
     * 
     * @param p_currency A valid currency code.
     * @param p_dateTime A dateTime for the conversion rate.
     * @return Rate data for a specified currency code.
     */
    public EmuConversionRatesData getWithCode(
                                    String       p_currency,
                                    PpasDateTime p_dateTime)
    {
        int                     l_loop;
        EmuConversionRatesData  l_retRate = null;
        EmuConversionRatesData  l_rate = null;
        PpasDateTime            l_startDate = null;
        PpasDateTime            l_endDate = null;
        int                     l_len;
        
        if (i_emuConversionRatesArr != null)
        {
            l_len = i_emuConversionRatesArr.length;
            for(l_loop = 0; l_loop < l_len; l_loop++)
            {
                l_rate = i_emuConversionRatesArr[l_loop];
                if (l_rate.getCurrency().equals(p_currency))
                {
                    l_startDate = l_rate.getStartDate();
                    // Start date must be a valid date
                    if (l_startDate != null && 
                        l_startDate.isSet() && 
                        l_startDate.before(p_dateTime))
                    {
                        l_endDate = l_rate.getEndDate();
                        // End date can be null
                        if ( l_endDate == null || 
                             !l_endDate.isSet() || 
                             l_endDate.after(p_dateTime) )
                        {
                            l_retRate = l_rate;
                            break;
                        }
                    }
                } 
            }
        }

        return(l_retRate);

    } // End of public method getWithCode(String)

    /**
     * Returns an array of this valid rates data set, but with deleted
     * rates omitted.
     * 
     * @return Array of all available (non-deleted) rates.
     */
    public EmuConversionRatesData[] getAvailableArray()
    {
        EmuConversionRatesData  l_availableEmuConversionRatesArr[];
        Vector                  l_availableV = new Vector();
        int                     l_loop;
        EmuConversionRatesData  l_emuConversionRatesEmptyArr[] =
                new EmuConversionRatesData[0];

        for(l_loop = 0; l_loop < i_emuConversionRatesArr.length; l_loop++)
        {
            if (!i_emuConversionRatesArr[l_loop].isDeleted())
            {
                // Rate not deleted, add to temporary vector.
                l_availableV.addElement(i_emuConversionRatesArr[l_loop]);
            }
        }

        l_availableEmuConversionRatesArr =
                (EmuConversionRatesData[])l_availableV.toArray(l_emuConversionRatesEmptyArr);

        return(l_availableEmuConversionRatesArr);

    } // End of public method getAvailableArray()


    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("AllRates", i_emuConversionRatesArr, p_sb);
    }
}

