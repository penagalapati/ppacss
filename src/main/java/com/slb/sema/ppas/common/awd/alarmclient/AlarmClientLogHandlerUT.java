/*
 * Filename        : AlarmClientLogHandlerUT.java
 * Created on      : 23-Jun-2004
 * Author          : MVonka
 * 
 * Description     :
 * 
 * Copyright       : Copyright Marek Vonka 2004, all rights reserved.
 */
package com.slb.sema.ppas.common.awd.alarmclient;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * @author MVonka
 * This...
 */
public class AlarmClientLogHandlerUT
{
    private static final String C_CLASS_NAME = "AlarmClientLogHandlerUT";
    
    private Logger              i_logger;

    /**
     * 
     */
    public AlarmClientLogHandlerUT()
    {
        super();
    }
    
    public void init(PpasProperties p_configProperties)
    throws Exception
    {
        PpasLoggerPool          l_loggerPool;

        l_loggerPool = new PpasLoggerPool("LoggerPool", p_configProperties,
                System.getProperty("ascs.procName"));

        i_logger = l_loggerPool.getLogger();
    }
    
    public static void main(String p_argsARR[])
    throws Exception
    {
        AlarmClientLogHandlerUT l_alarmClientLogHandlerUT;
        PpasProperties        l_configProperties;
        
        // Debug.setDebugFullToSystemErr();

        l_configProperties = new PpasProperties();
        l_configProperties.loadLayeredProperties();
        l_configProperties.list(System.out);
        l_alarmClientLogHandlerUT = new AlarmClientLogHandlerUT();
        l_alarmClientLogHandlerUT.init(l_configProperties);
        if(p_argsARR.length >= 1)
        {
            int l_testNo = Integer.parseInt(p_argsARR[0]);
            switch(l_testNo)
            {
                case 1:
                l_alarmClientLogHandlerUT.test1();
                break;
                case 2:
                l_alarmClientLogHandlerUT.test2();
                break;
            }
        }
        else
        {
            l_alarmClientLogHandlerUT.test1();
        }
    }
    
    private void test2()
    {
        int                     l_loop;
        PpasException           l_ppasE;

        for(l_loop = 0; l_loop < 100; l_loop++)
        {
            l_ppasE = new PpasConfigException(
                    C_CLASS_NAME,
                    "test1",
                    11010,
                    this,
                    (PpasRequest)null,
                    0,
                    ConfigKey.get().errorReading(C_CLASS_NAME + "_test2,loop=" + l_loop));
            System.out.println("Logging " + l_ppasE);
            i_logger.logMessage(l_ppasE);
    
            try
            {        
                Thread.sleep(2000);
            }
            catch (InterruptedException l_e)
            {
            }
        }
    }
    
    private void test1()
    {
        PpasException           l_ppasE;
        
        l_ppasE = new PpasConfigException(
                C_CLASS_NAME,
                "test1",
                11010,
                this,
                (PpasRequest)null,
                0,
                ConfigKey.get().errorReading(C_CLASS_NAME + "_test1"));
        System.out.println("Logging " + l_ppasE);
        i_logger.logMessage(l_ppasE);

        try
        {        
            Thread.sleep(2000);
        }
        catch (InterruptedException l_e)
        {
        }

        l_ppasE = new PpasConfigException(
                C_CLASS_NAME,
                "test1",
                11010,
                this,
                (PpasRequest)null,
                0,
                ConfigKey.get().errorReading(C_CLASS_NAME + "_test2"));
        System.out.println("Logging " + l_ppasE);
        i_logger.logMessage(l_ppasE);

        try
        {        
            Thread.sleep(60000);
        }
        catch (InterruptedException l_e)
        {
        }

    }

}
;