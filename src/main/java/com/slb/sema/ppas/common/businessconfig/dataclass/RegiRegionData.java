////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :  9500     
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       RegiRegionData.java
//    DATE            :       11-Aug-05
//    AUTHOR          :       David Bitmead
//    REFERENCE       :       PRD_ASCS00_GEN_CA_44
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Stores a row from the REGI_REGION table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//          |            |                                 | 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.HomeRegionId;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a row from the REGI_REGION table.
 */
public class RegiRegionData extends DataObject implements Comparable
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RegiRegionData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The home region ID. */
    private HomeRegionId i_homeRegionId;
    
    /** The home region description. */
    private String i_homeRegionDescription;
    
    /** Flag indicating if this home region id is marked for deleted. */
    private boolean i_delFlag = false;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a <code>RegiRegionData</code> object.
     * 
     * @param p_request The request to process.
     * @param p_homeRegionId The home region id.
     * @param p_homeRegionDescription The home region description.
     * @param p_delFlag The deletion flag.
     */
    public RegiRegionData(PpasRequest     p_request,
                          HomeRegionId    p_homeRegionId,
                          String          p_homeRegionDescription,
                          char            p_delFlag)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_homeRegionId          = p_homeRegionId;
        i_homeRegionDescription = p_homeRegionDescription;
        i_delFlag               = (p_delFlag == '*');
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /**
     * Return the home region id.
     * @return the home region id
     */
    public HomeRegionId getHomeRegionId()
    {
        return i_homeRegionId;
    }

    /**
     * Return the home region description.
     * @return the home region description
     */
    public String getRegionDescription()
    {
        return i_homeRegionDescription;
    }

    /** Returns true if this home region has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_delFlag;
    }
    
    /** Defines how objects of this class should be compared for sorting purposes.
     * @param p_otherRegiRegion Home Region to compare with.
     * @return a -1, zero, or 1 depending on whether this object is less than, equal to, or greater than the 
     * specified object.
     */
    public int compareTo (Object p_otherRegiRegion)
    {
        int               l_retVal = 0;
        RegiRegionData    l_otherRegiRegion = (RegiRegionData)p_otherRegiRegion;

        if (p_otherRegiRegion == null)
        {
            l_retVal = 1;
        }
        else if (this.getHomeRegionId().getValue() < l_otherRegiRegion.getHomeRegionId().getValue())
        {
            l_retVal = -1;
        }
        else if (this.getHomeRegionId().equals(l_otherRegiRegion.getHomeRegionId()))
        {
            l_retVal = 0;
        }
        else
        {
            l_retVal = 1;
        }

        return (l_retVal);

    } // end method compareTo
}
