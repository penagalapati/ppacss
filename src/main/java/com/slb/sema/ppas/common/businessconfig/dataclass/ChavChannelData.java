//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       ChavChannelData.java
// DATE            :       14-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of channel data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Object holding channel data configuration.
 */
public class ChavChannelData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChavChannelData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** channel . */
    private String              i_channelId    = null;

    /** channel description. */
    private String              i_desc       = null;
    
    /** channel group. */
    private String              i_channelGroup = null;

    /** The opid that last updated this record. */
    private String              i_opid       = null;

    /** The date/time of the last update. */
    private PpasDateTime        i_genYmdhms  = new PpasDateTime("");
    
    /** Defines if this record has been deleted. */
    private boolean i_deleted = false;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of <code>ChavChannelData</code>.
     * @param p_channel The channel represented by this object.
     * @param p_desc Description of this channel.
     * @param p_channelGroup The group that this channel belongs.
     * @param p_opid The opid that last updated this record.
     * @param p_genYmdhms date/time of the last update.
     * @param p_deleted whether this record is marked as deleted in the database.
     */
    public ChavChannelData(String p_channel,
                           String p_desc,
                           String p_channelGroup,
                           String p_opid,
                           PpasDateTime p_genYmdhms,
                           boolean p_deleted)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_channelId = p_channel;
        i_desc = p_desc;
        i_channelGroup = p_channelGroup;
        i_opid = p_opid;
        i_genYmdhms = p_genYmdhms;
        i_deleted = p_deleted;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the channel this this object represents.
     * @return the channel
     */
    public String getChannelId()
    {
        return i_channelId;
    }

    /**
     * Get the channel description.
     * @return description of this channel record
     */
    public String getDesc()
    {
        return i_desc;
    }

    /**
     * The opid that last updated this record.
     * @return opid of last update
     */
    public String getOpid()
    {
        return i_opid;
    }

    /**
     * The date/time that this record was last updated.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }
    
    /**
     * The group that this channel belongs. If left blank, the channel is a group or a standalone channel.
     * @return The channel group.
     */    
    public String getChannelGroup()
    {
        return i_channelGroup;
    }
    
    /**
     * The group that this channel belongs to or the channel identifier if the identifier does not have a
     * parent.
     * @return The channel group or identifier.
     */    
    public String getLogicalGroup()
    {
        return i_channelGroup == null || i_channelGroup.trim().equals("") ? i_channelId : i_channelGroup;
    }
    
    /**
     * Get the deleted status of this record.
     * @return <code>true</code> if the record is deleted, else <code>false</code>
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }
}
