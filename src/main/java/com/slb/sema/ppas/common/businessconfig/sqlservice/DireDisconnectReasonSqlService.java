////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DireDisconnectReasonSqlService.Java
//      DATE            :       16-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the disconnect reason business
//                              config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DireDisconnectReasonDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;


/**
 * Implements a service that performs the SQL required to populate the
 * disconnect reasons business config cache.
 */
public class DireDisconnectReasonSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DireDisconnectReasonSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Sql service to be used in generating the data for the
     * disconnect reasons business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public DireDisconnectReasonSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 31310, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 31390, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all disconnect reason records (including ones marked as
     * deleted) from the dire_disconnect_reason database table.
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return A data set containing all the disconnect reason records read from
     *         the database.
     * @throws PpasSqlException If the data cannot be read.
     */
    public DireDisconnectReasonDataSet readAll (PpasRequest     p_request,
                                                JdbcConnection  p_connection)
        throws PpasSqlException
    {
        JdbcStatement               l_statement;
        SqlString                   l_sql;
        char                        l_direType;
        String                      l_direReason;
        String                      l_direDesc;
        String                      l_direAdjCode;
        double                      l_direCharge;
        PpasDateTime                l_genYmdhms;
        String                      l_opid;
        char                        l_delFlag;
        DireDisconnectReasonData    l_direRecord;
        Vector                      l_allDisconnectReasonV = new Vector (20, 10);
        DireDisconnectReasonData    l_allDisconnectReasonARR[];
        DireDisconnectReasonData    l_disconnectReasonEmptyARR[] =
                                               new DireDisconnectReasonData[0];
        JdbcResultSet               l_results;
        DireDisconnectReasonDataSet l_allDisconnectReasonDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 31410, this,
                "Entered " +  C_METHOD_readAll);
        }

        l_sql =   new SqlString(500, 0, "SELECT " +
                          "dire_type, " +
                          "dire_reason, " +
                          "dire_description, " +
                          "dire_adj_code, " +
                          "dire_charge," +
                          "dire_del_flag, " +
                          "dire_gen_ymdhms, " +
                          "dire_opid " +
                  "FROM " +
                          "dire_disconnect_reason ");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_readAll,
                                                   31420,
                                                   this,
                                                   p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME,
                                             C_METHOD_readAll,
                                             31430,
                                             this,
                                             p_request,
                                             l_sql);

        while ( l_results.next(31440) )
        {
            l_direType    = l_results.getChar       (31445, "dire_type");
            l_direReason  = l_results.getTrimString (31460, "dire_reason");
            l_direDesc    = l_results.getTrimString (31470, "dire_description");
            l_direAdjCode = l_results.getTrimString (31480, "dire_adj_code");
            l_direCharge  = l_results.getDouble     (31490, "dire_charge");
            l_delFlag     = l_results.getChar       (31540, "dire_del_flag");
            l_genYmdhms   = l_results.getDateTime   (31520, "dire_gen_ymdhms");
            l_opid        = l_results.getTrimString (31530, "dire_opid");

            l_direRecord = new DireDisconnectReasonData(
                    p_request,
                    l_direType,
                    l_direReason,
                    l_direDesc,
                    l_direAdjCode,
                    l_direCharge,
                    l_genYmdhms,
                    l_opid,
                    l_delFlag
                    );

            l_allDisconnectReasonV.addElement (l_direRecord);
        }

        l_results.close (31550);

        l_statement.close (C_CLASS_NAME,
                           C_METHOD_readAll,
                           31560,
                           this,
                           p_request);

        l_allDisconnectReasonARR = (DireDisconnectReasonData[])
                   l_allDisconnectReasonV.toArray (l_disconnectReasonEmptyARR);

        l_allDisconnectReasonDataSet = new DireDisconnectReasonDataSet(p_request, l_allDisconnectReasonARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 31570, this,
                "Leaving " + C_METHOD_readAll);
        }

        return (l_allDisconnectReasonDataSet);

    } // End of public method readAll

} // End of public class DireDisconnectReasonSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
