//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       ChavChannelCache.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for channel data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ChavChannelSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for Channel.
 */
public class ChavChannelCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String          C_CLASS_NAME                   = "ChavChannelCodeCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The channel SQL service to use to load cache.
     */
    private ChavChannelSqlService i_channelSqlService = null;

    /** Cache of all valid Channels(including ones marked as deleted). */
    private ChavChannelDataSet    i_allChavChannel        = null;

    /**
     * Cache of available Channels(not including ones marked as deleted). This attribute exists
     * for performance reasons so the array is not generated from the complete list each time it is required.
     */
    private ChavChannelDataSet    i_availableChavChannel  = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Channelscache.
     * @param p_request The request to process.
     * @param p_logger The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public ChavChannelCache(PpasRequest p_request, Logger p_logger, PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_channelSqlService = new ChavChannelSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Channels(including ones marked as deleted).
     * @return All valid Channels.
     */
    public ChavChannelDataSet getAll()
    {
        return i_allChavChannel;
    }

    /**
     * Returns available valid Channels(not including ones marked as deleted).
     * @return Available Channels.
     */
    public ChavChannelDataSet getAvailable()
    {
        if (i_availableChavChannel == null)
        {
            i_availableChavChannel = new ChavChannelDataSet(null, i_allChavChannel.getAvailableArray());
        }
        
        return i_availableChavChannel;
    }

    /**
     * Reloads the static configuration into the cache. This method is synchronised so a reload will not cause
     * unexpected errors.
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest p_request, JdbcConnection p_connection) throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered reload");
        }

        i_allChavChannel = i_channelSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all rates and old list of deleted
        // languages, but rather then synchronise this is acceptable.
      //  i_availableChavChannel = new ChavChannelDataSet(p_request, i_allChavChannel.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allChavChannel);
    }
}

