//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonsBonusSchemeSqlService.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for bonus scheme data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 20/07/07 | S James    | Changed markAsAvailable method  | PpacLon#3183/11851
//          |            | to only allow the update if the |
//          |            | service offering on the record  |
//          |            | is not in use by another scheme |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** SQL service for accessing Bonus Scheme codes. */
public class BonsBonusSchemeSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonsBonusSchemeSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new F&F Bonus Schemes sql service.
     * @param p_request The request to process.
     */
    public BonsBonusSchemeSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid Bonus Schemes (including ones marked as deleted).
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Bonus Schemes.
     * @throws PpasSqlException If the data cannot be read.
     */
    public BonsBonusSchemeDataSet readAll(PpasRequest p_request, JdbcConnection p_connection)
            throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        SqlString l_sql;
        Vector l_allBonsBonusSchemeDataV = new Vector(20, 10);
        BonsBonusSchemeDataSet l_bonsBonusSchemeDataSet;
        BonsBonusSchemeData l_allChavARR[];
        BonsBonusSchemeData l_bonsBonusSchemeEmptyARR[] = new BonsBonusSchemeData[0];
        JdbcResultSet l_results;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 10100, this,
                            "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(500, 0, "select bons_scheme_id," 
                                          + " bons_description,"
                                          + " bons_start_date,"
                                          + " bons_end_date,"
                                          + " bons_active_ind,"
                                          + " bons_gen_ymdhms,"
                                          + " bons_opid,"
                                          + " bons_del_flag,"
                                          + " bons_opt_in_service_offerings,"
                                          + " bons_scheme_type "
                                    + "from bons_bonus_scheme");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL,
                            p_request, C_CLASS_NAME, 10110, this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while (l_results.next(10140))
        {
            char l_bonusActive = l_results.getChar(10170, "bons_active_ind");
            char l_bonusDelFlag = l_results.getChar(10190, "bons_del_flag");

            l_allBonsBonusSchemeDataV.addElement(new BonsBonusSchemeData(           
                                             l_results.getTrimString(10150, "bons_scheme_id"),
                                             l_results.getChar(10155, "bons_scheme_type"),
                                             l_results.getTrimString(10160, "bons_description"),
                                             l_results.getDate(10230, "bons_start_date"),
                                             l_results.getDate(10240, "bons_end_date"),
                                             l_results.getLongO(10220, "bons_opt_in_service_offerings"),
                                             l_bonusActive == 'Y',
                                             l_bonusDelFlag == '*',
                                             l_results.getTrimString(10200, "bons_opid"),
                                             l_results.getDateTime(10210, "bons_gen_ymdhms")));
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10245, this, p_request);

        l_allChavARR = (BonsBonusSchemeData[])l_allBonsBonusSchemeDataV.toArray(l_bonsBonusSchemeEmptyARR);

        l_bonsBonusSchemeDataSet = new BonsBonusSchemeDataSet(p_request, l_allChavARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 10250, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_bonsBonusSchemeDataSet;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";

    /**
     * Attempts to insert a new row into the database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_bonusSchemeId scheme The Channel.
     * @param p_bonusDescription The bonus scheme description.
     * @param p_bonusStartDate The bonus start date.
     * @param p_bonusEndDate The bonus end date.
     * @param p_bonsActiveInd The char to indicate if bonus is active.
     * @param p_bonsServOffOptin The bonus Service Offerings Optin.
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_opid,
                      String p_bonusSchemeId,
                      String p_bonusDescription,
                      PpasDate p_bonusStartDate,
                      PpasDate p_bonusEndDate,
                      char p_bonsActiveInd,
                      Long p_bonsServOffOptin,
                      char p_bonsSchemeType) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " + C_METHOD_insert);
        }

        l_sql = "insert into bons_bonus_scheme ("
                         + " bons_scheme_id," 
                         + " bons_description,"
                         + " bons_start_date," 
                         + " bons_end_date, "
                         + " bons_active_ind,"
                         + " bons_del_flag," 
                         + " bons_opid, " 
                         + " bons_gen_ymdhms,"
                         + " bons_opt_in_service_offerings,"
                         + " bons_scheme_type )"
                + " values( {0}, {1}, {2}, {3}, {4}, ' ', {5}, {6}, {7}, {8})";

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_bonusSchemeId);
        l_sqlString.setStringParam(1, p_bonusDescription);
        l_sqlString.setDateParam(2, p_bonusStartDate);
        l_sqlString.setDateParam(3, p_bonusEndDate);
        l_sqlString.setCharParam(4, p_bonsActiveInd);
        l_sqlString.setStringParam(5, p_opid);
        l_sqlString.setDateTimeParam(6, DatePatch.getDateTimeNow());
        l_sqlString.setLongParam(7, p_bonsServOffOptin);
        l_sqlString.setCharParam(8, p_bonsSchemeType);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 16050, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 16740, this,
                                "Database error: unable to insert row in table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_insert, 16750, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 16760, this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a new row into the database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_bonusSchemeId scheme The Channel.
     * @param p_bonusDescription The bonus scheme description.
     * @param p_bonusStartDate The bonus start date.
     * @param p_bonusEndDate The bonus end date.
     * @param p_bonsActiveInd The char to indicate if bonus is active.
     * @param p_bonsServOffOptin The bonus Service Offerings Optin.
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_opid,
                      String p_bonusSchemeId,
                      String p_bonusDescription,
                      PpasDate p_bonusStartDate,
                      PpasDate p_bonusEndDate,
                      char p_bonsActiveInd,
                      Long p_bonsServOffOptin) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 20000, this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = new String("update bons_bonus_scheme " + "set bons_description = {0},"
                           + " bons_start_date = {1}," + " bons_end_date = {2}, " + " bons_active_ind = {3},"
                           + " bons_del_flag = ' '," + " bons_opid = {4}, " + " bons_gen_ymdhms = {5},"
                           + " bons_opt_in_service_offerings = {6} "
                           + " where bons_scheme_id = {7}");

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_bonusDescription);
        l_sqlString.setDateParam(1, p_bonusStartDate);
        l_sqlString.setDateParam(2, p_bonusEndDate);
        l_sqlString.setCharParam(3, p_bonsActiveInd);
        l_sqlString.setStringParam(4, p_opid);
        l_sqlString.setDateTimeParam(5, DatePatch.getDateTimeNow());
        l_sqlString.setLongParam(6, p_bonsServOffOptin);
        l_sqlString.setStringParam(7, p_bonusSchemeId);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 20050, this, null);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 20130, this,
                                "Database error: unable to update row in table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_update, 20140, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 20150, this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;
    }

    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";

    /**
     * Mark a row as having been deleted in BONS_FAF_SCHEME_ID.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_bonusScheme scheme The Bonus Schemes to be withdrawn.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest p_request,
                       JdbcConnection p_connection,
                       String p_opid,
                       String p_bonusScheme) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 32100, this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE BONS_BONUS_SCHEME SET BONS_DEL_FLAG = '*', "
                + "BONS_OPID = {1}, " + "BONS_GEN_YMDHMS = {2} " + "WHERE BONS_SCHEME_ID = {0}");

        l_sqlString = new SqlString(150, 3, l_sql);

        l_sqlString.setStringParam(0, p_bonusScheme);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete, 32300, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 32610, this,
                                "Database error: unable to mark row as deleted in bonus scheme table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_delete, 32620, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 32700, this,
                            "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn Bonus Schemes record as available in the bonus scheme database
     * table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_bonusScheme Bonus scheme identifier.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest p_request,
                                JdbcConnection p_connection,
                                String p_bonusScheme,
                                String p_userOpid) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        PpasDateTime l_now;
        int l_rowCount = 0;

        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10300, this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE BONS_BONUS_SCHEME SET BONS_DEL_FLAG = {0}, " + 
                       "BONS_OPID = {2}, BONS_GEN_YMDHMS = {3} " + 
                "WHERE BONS_SCHEME_ID = {1} " +
                "AND (BONS_OPT_IN_SERVICE_OFFERINGS IS NULL OR BONS_OPT_IN_SERVICE_OFFERINGS NOT IN " +
                "    (SELECT BONS_OPT_IN_SERVICE_OFFERINGS FROM BONS_BONUS_SCHEME " +
                "    WHERE  BONS_DEL_FLAG != '*' AND BONS_OPT_IN_SERVICE_OFFERINGS IS NOT NULL))";

        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setCharParam(0, ' ');
        l_sqlString.setStringParam(1, p_bonusScheme);
        l_sqlString.setStringParam(2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       10310,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   10320,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10340, this,
                                "Database error: unable to update row in bonus scheme table.");
            }

            throw new PpasSqlException(C_CLASS_NAME, C_METHOD_markAsAvailable, 10350, this, p_request, 0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10360, this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}
