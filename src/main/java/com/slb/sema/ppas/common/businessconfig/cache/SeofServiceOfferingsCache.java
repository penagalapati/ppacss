////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SeofServiceOfferingsCache.java
//      DATE            :       11-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PRD_ASCS00_GEN_CA_016
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Cache containing service offering data.
//
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.SeofServiceOfferingDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SeofServiceOfferingSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Cache containing service offering data.
 */
public class SeofServiceOfferingsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "SeofServiceOfferingsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Instance of the service used to retrieve service offering data from the database. */
    private SeofServiceOfferingSqlService i_seofServiceOfferingSqlService = null;
    
    /** Collection of service offerin data objects. */
    private SeofServiceOfferingDataSet i_seofDataSet = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new account group data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public SeofServiceOfferingsCache(PpasRequest            p_request,
                                     Logger                 p_logger,
                                     PpasProperties         p_properties)
    {
        super(p_logger, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        i_seofServiceOfferingSqlService = new SeofServiceOfferingSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /**
     * Returns a set of service offering data objects.
     * @return Set of service offering data objects.
     */
    public SeofServiceOfferingDataSet getAvailable()
    {
        return (i_seofDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Re-populate the service Offering cache.
     * @param p_request Originating request.
     * @param p_connection A database connection.
     * @throws PpasSqlException If a error occurs while accessing the database.
     */
    public void reload(PpasRequest    p_request,
                       JdbcConnection p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                "Entered " + C_METHOD_reload);
        }

        i_seofDataSet = i_seofServiceOfferingSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10190,
                this,
                "Leaving " + C_METHOD_reload);
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_seofDataSet);
    }
}

