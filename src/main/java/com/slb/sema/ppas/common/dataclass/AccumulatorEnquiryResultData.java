////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatorEnquiryResultData.java
//      DATE            :       21-Aug-2003
//      AUTHOR          :       Neil Raymond
//      REFERENCE       :       PRD_ASCS00_DEV_SS_69
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       AccumulatorEnquiryResultData class for storing 
//                              accumulator data associated with a single account.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

/**
 * Class used to store a single accounts accumulator data.
 */
public class AccumulatorEnquiryResultData extends DataObject
{
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The accounts MSISDN. */
    private Msisdn              i_msisdn = null;

    /** The accounts service class. */
    private ServiceClass              i_serviceClass = null;

    /** The accounts SDP temp blocked status. */
    private boolean             i_tempBlockStatus = false;
    
    /** The accounts accumulator data records. */
    private AccumulatorDataSet  i_accuDataSet = null;
    
    /** Charged Event Counters. */
    private ChargedEventCounterDataSet  i_counters = null;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Constructs a new <code>DataObject</code> containing an accounts 
     * accumulator data from the SDP.
     * @param p_msisdn The accounts MSISDN.
     * @param p_serviceClass The accounts service class.
     * @param p_tempBlockStatus The accounts temporary blocked status.
     * @param p_accuDataSet A set of all of the accounts accumulator data.
     * @param p_counters A set of Charged Event Counters.
     */
    public AccumulatorEnquiryResultData(Msisdn              p_msisdn,
                                        ServiceClass        p_serviceClass,
                                        boolean             p_tempBlockStatus,
                                        AccumulatorDataSet  p_accuDataSet,
                                        ChargedEventCounterDataSet p_counters)
    {
        i_msisdn = p_msisdn;
        i_serviceClass = p_serviceClass;
        i_tempBlockStatus = p_tempBlockStatus;
        i_accuDataSet = p_accuDataSet;
        i_counters    = p_counters;
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Get the accumulators.
     * @return Accumulator Data Set
     */
    public AccumulatorDataSet getAccuDataSet()
    {
        return i_accuDataSet;
    }

    /** Get the mobile number of the associated account.
     * @return MSISDN
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /** Get the service class of the associated account.
     * @return Service Class
     */
    public ServiceClass getServiceClass()
    {
        return i_serviceClass;
    }

    /**
     *  Get whether the account is barred or not.
     * @return Temporary Block Status
     */
    public boolean getTempBlockStatus()
    {
        return i_tempBlockStatus;
    }

    /** Get the Changed Event Counters.
     * @return Charged Event Counters.
     */
    public ChargedEventCounterDataSet getChargedEventCountersDataSet()
    {
        return i_counters;
    }
}
