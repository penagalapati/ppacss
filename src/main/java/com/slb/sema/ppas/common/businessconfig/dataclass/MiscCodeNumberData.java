////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscCodeNumberData.Java
//      DATE            :       08-Sept-2005
//      AUTHOR          :       Remi Isaacs/Mark Gray
//      REFERENCE       :       PpaLon#1474/7072
//                              
//      COPYRIGHT       :       VM Data 2005
//
//      DESCRIPTION     :       Stores a row from the SRVA_MISC_CODES database
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//                  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;

/** An object that represents a row from the SRVA_MISC_CODES database table. */
public class MiscCodeNumberData extends MiscCodeData implements Comparable
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscCodeNumberData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The code as as int. */
    private int  i_numCode;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
    * Creates a misc code object.
    * 
    * @param p_market The Market.
    * @param p_codeType The code type.
    * @param p_code The code.
    * @param p_description The code description.
    * @param p_deleteFlag Indicates whether data item is still current.
    */
    public MiscCodeNumberData(Market      p_market,
                              String      p_codeType,
                              String      p_code,
                              String      p_description,
                              String      p_deleteFlag)
    {
        super(p_market, p_codeType, p_code, p_description, p_deleteFlag);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME + " for Mkt: " + p_market + " type: " +
                            p_codeType + " Code: " + p_code + " Desc: " + p_description);
        }

        i_numCode = Integer.parseInt(p_code);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }
        
    /** Defines how objects of this class should be compared for sorting purposes.
     *  @param p_that An <code>Object</code> to compare with.
     *  @return 1, 0 or -1 depending on whether the comparision object is greater than, equal to or less than
     *           this object. 
     * 
     */
    public int compareTo(Object p_that)
    {
        int l_result = 0;

        if (p_that instanceof MiscCodeNumberData)
        {
            MiscCodeNumberData l_that = (MiscCodeNumberData)p_that;

            l_result = getMarket().toString().compareTo(l_that.getMarket().toString());

            if (l_result == 0)
            {
                l_result = getCodeType().compareTo(l_that.getCodeType());
            }
            
            if (l_result == 0)
            {
                l_result = i_numCode > l_that.i_numCode ? 1 :
                           i_numCode < l_that.i_numCode ? -1 : 0;
            }            
        }
        else
        {
            l_result = super.compareTo(p_that);
        }
        
        return l_result;
    }

    /** 
     * Returns explicit instance of Misc Code Number data
     * @param p_market The customer's market.
     * 
     * @return MiscCodeData Object
     */    
    public MiscCodeData getExplicInstance(Market p_market)
    {
        return new MiscCodeNumberData(p_market,
                                      getCodeType(),
                                      getCode(),
                                      getDescription(),
                                      getIsCurrent() ? " " : "*");
    }
}
