////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BicsysUserfileSqlService.java
//    DATE            :       4-Jun-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#227/2175
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Services to read and update data in the
//                            BICSYS_USERFILE table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.OperatorData;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Services to read and update data in the BICSYS_USERFILE table.
 */
public class BicsysUserfileSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BicsysUserfileSqlService";

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------
    /** Logger for any exceptions arising from the use of this service. */
    private Logger i_logger = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Standard constructor.
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public BicsysUserfileSqlService(PpasRequest p_request,
                                    Logger      p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getOperatorData = "getOperatorData";
    /**
     * Fetches operator data record for the given operator.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator to fetch configuration for.
     * @param p_baseCurrency Currency of adjustment limits.
     * @return Operator data record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public OperatorData getOperatorData(PpasRequest    p_request,
                                        JdbcConnection p_connection,
                                        String         p_opid,
                                        PpasCurrency   p_baseCurrency)
        throws PpasSqlException
    {
        return getOperatorData(p_request, p_connection, p_opid, p_baseCurrency, true);
    }
    /**
     * Fetches operator data record for the given operator.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator to fetch configuration for.
     * @param p_baseCurrency Currency of adjustment limits.
     * @param p_operatorMustExist True if the operator is expected to exist.
     * @return Operator data record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public OperatorData getOperatorData(PpasRequest    p_request,
                                        JdbcConnection p_connection,
                                        String         p_opid,
                                        PpasCurrency   p_baseCurrency,
                                        boolean        p_operatorMustExist)
        throws PpasSqlException
    {
        JdbcStatement       l_statement = null;
        JdbcResultSet       l_resultSet = null;
        String              l_sql;
        SqlString           l_sqlString;
        OperatorData        l_operatorData = null;
        Market              l_market;
        PpasSqlException    l_ppasSqlE;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 13000, this,
                            "Entered " + C_METHOD_getOperatorData + " for '" + p_opid + "'");
        }

        l_sql = "SELECT opid_name," +
                "default_sloc," +
                "default_srva," +
                "privilege_id," +
                "max_adj_amount," +
                "user_max_daily_adj_limit," +
                "user_password," +
                "password_expiry_date," +
                "gui_user," +
                "posi_user," +
                "pami_user," +
                "user_login_failures," +
                "user_last_login," +
                "user_bar_counter," +
                "user_bar_to_date_time," +
                "srva_name " +
                "FROM bicsys_userfile b, srva_mstr s " +
                "WHERE b.opid = {0} " +
                "AND default_sloc = sloc " +
                "AND default_srva = srva";
        
        l_sqlString = new SqlString(1000, 0, l_sql); 

        l_sqlString.setStringParam(0, p_opid);

        try
        {
            l_statement = p_connection.createStatement (
                                       C_CLASS_NAME,
                                       C_METHOD_getOperatorData,
                                       13010,
                                       this,
                                       p_request);

            l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME,
                                       C_METHOD_getOperatorData,
                                       13020,
                                       this,
                                       p_request,
                                       l_sqlString);

            if (l_resultSet.next(13030))
            {
                l_market = new Market(l_resultSet.getInt(13070, "default_srva"),
                                      l_resultSet.getInt(13060, "default_sloc"),
                                      l_resultSet.getString(13065, "srva_name")); 

                l_operatorData = new OperatorData(
                                         p_request,
                                         p_opid,
                                         l_resultSet.getString(13050, "opid_name"),
                                         l_market,
                                         l_resultSet.getInt(13090, "privilege_id"),
                                         l_resultSet.getDoubleO(13100, "max_adj_amount"),
                                         l_resultSet.getDoubleO(13110, "user_max_daily_adj_limit"),
                                         p_baseCurrency,
                                         l_resultSet.getTrimString(13120, "user_password"),
                                         l_resultSet.getDate(13130, "password_expiry_date"),
                                         l_resultSet.getChar(13140, "gui_user") == 'Y' ? true : false,
                                         l_resultSet.getChar(13150, "posi_user") == 'Y' ? true : false,
                                         l_resultSet.getChar(13160, "pami_user") == 'Y' ? true : false,
                                         l_resultSet.getInt(13170, "user_login_failures"),
                                         l_resultSet.getDateTime(13180, "user_last_login"),
                                         l_resultSet.getInt(13190, "user_bar_counter"),
                                         l_resultSet.getDateTime(13200, "user_bar_to_date_time"));
            }
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME, 13210, this,
                                    "Database error: No row found in bicsys_userfile for operator " + p_opid);
                }

                if (p_operatorMustExist)
                {
                    l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                                       C_METHOD_getOperatorData,
                                                       13220,
                                                       this,
                                                       p_request,
                                                       0,
                                                       SqlKey.get().databaseInconsistency());

                    if (i_logger != null)
                    {
                        i_logger.logMessage (l_ppasSqlE);
                    } 
                    throw (l_ppasSqlE);
                }
            }
        }
        finally
        {
            l_resultSet.close(13200);

            l_statement.close(C_CLASS_NAME, C_METHOD_getOperatorData, 13230, this, p_request);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 13240, this,
                            "Leaving " + C_METHOD_getOperatorData);
        }

        return l_operatorData;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getOperators = "getOperators";
    /**
     * Fetches operator ids from bicsys_userfile table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return A vector of operator ids.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public Vector getOperators(PpasRequest    p_request,
                               JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement  l_statement;
        JdbcResultSet  l_resultSet;
        String         l_sql;
        SqlString      l_sqlString;
        Vector         l_operators = new Vector(20, 10);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request,
                C_CLASS_NAME, 
                11500, 
                this,
                "Entered " + C_METHOD_getOperators);
        }

        l_sql = "SELECT opid " +
                "FROM bicsys_userfile";
        
        l_sqlString = new SqlString(1000, 0, l_sql); 

        l_statement = p_connection.createStatement (
                                       C_CLASS_NAME,
                                       C_METHOD_getOperators,
                                       11510,
                                       this,
                                       p_request);

        l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME,
                                       C_METHOD_getOperators,
                                       11520,
                                       this,
                                       p_request,
                                       l_sqlString);

        while (l_resultSet.next(11530))
        {
            l_operators.add(l_resultSet.getTrimString(11540, "opid"));
        }

        l_resultSet.close(11550);

        l_statement.close(C_CLASS_NAME, C_METHOD_getOperators, 11560, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request,
                C_CLASS_NAME, 
                11570, 
                this,
                "Leaving " + C_METHOD_getOperators);
        }

        return(l_operators);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Update BICSYS_USERFILE.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid                Operator id of amended operator data record.
     * @param p_opidName            Operator name.
     * @param p_defaultMarket       Default market.
     * @param p_privilegeId         Privilege identifier.
     * @param p_maxAdjustAmount     Maximum single adjustment amount.
     * @param p_dailyAdjustLimit    Daily adjustment limit.
     * @param p_password            Operator password.
     * @param p_passwordExpiryDate  Password expiry date, empty string means the password will never expire.
     * @param p_guiUser             Privilege flag for GUI.
     * @param p_posiUser            Privilege flag for POSI.
     * @param p_pamiUser            Privilege flag for PAMI.
     * @param p_userOpid            Operator updating the business config record.
     * @param p_unbarUser           Flag indicating whether the user should be unbarred.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid,
                       String         p_opidName,
                       Market         p_defaultMarket,
                       int            p_privilegeId,
                       Double         p_maxAdjustAmount,
                       Double         p_dailyAdjustLimit,
                       String         p_password,
                       PpasDate       p_passwordExpiryDate,
                       boolean        p_guiUser,
                       boolean        p_posiUser,
                       boolean        p_pamiUser,
                       String         p_userOpid,
                       boolean        p_unbarUser)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 12000, this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE bicsys_userfile " +
                "SET opid_name = {1}," +
                    "default_srva = {2}," +
                    "default_sloc = {3}," +
                    "privilege_id = {4}," +
                    "max_adj_amount = {5}," +
                    "user_max_daily_adj_limit = {6}," +
                    "password_expiry_date = {7}," +
                    "gui_user = {8}," +
                    "posi_user = {9}," +
                    "pami_user = {10}," +
                    "user_opid = {11}," +
                    "user_gen_ymdhms = {12} ";

        if (!p_password.equals(""))
        {
            l_sql += ", user_password = {13} ";
        }

        if (p_unbarUser)
        {
            l_sql += ", user_bar_TO_DATE_TIME = {14}" +
                     ", user_bar_counter = 0";
        }
        
        l_sql += "WHERE opid = {0}";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam(0, p_opid);
        l_sqlString.setStringParam  (1,  p_opidName);
        l_sqlString.setIntParam     (2,  p_defaultMarket.getSrva());
        l_sqlString.setIntParam     (3,  p_defaultMarket.getSloc());
        l_sqlString.setIntParam     (4,  p_privilegeId);
        l_sqlString.setDoubleParam  (5,  p_maxAdjustAmount);
        l_sqlString.setDoubleParam  (6,  p_dailyAdjustLimit);
        l_sqlString.setDateParam    (7,  p_passwordExpiryDate);
        l_sqlString.setCharParam    (8,  p_guiUser ? 'Y' : 'N');
        l_sqlString.setCharParam    (9,  p_posiUser ? 'Y' : 'N');
        l_sqlString.setCharParam    (10, p_pamiUser ? 'Y' : 'N');
        l_sqlString.setStringParam  (11, p_userOpid);
        l_sqlString.setDateTimeParam(12, l_now);
        
        if (!p_password.equals(""))
        {
            l_sqlString.setStringParam(13,  p_password);
        }

        if (p_unbarUser)
        {
            l_sqlString.setDateTimeParam(14, DatePatch.getDateTimeNow());
        }
        
        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_update, 12010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       12020,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 12040, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 12050, this,
                                "Database error: unable to update row in bicsys_userfile table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_update,
                                               12060,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 12070, this,
                            "Leaving " + C_METHOD_update);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a service class code into the BICSYS_USERFILE database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid                Operator id of amended operator data record.
     * @param p_opidName            Operator name.
     * @param p_defaultMarket       Default market.
     * @param p_privilegeId         Privilege identifier.
     * @param p_maxAdjustAmount     Maximum single adjustment amount.
     * @param p_dailyAdjustLimit    Daily adjustment limit.
     * @param p_password            Operator password.
     * @param p_passwordExpiryDate  Password expiry date, empty string means the password will never expire.
     * @param p_guiUser             Privilege flag for GUI.
     * @param p_posiUser            Privilege flag for POSI.
     * @param p_pamiUser            Privilege flag for PAMI.
     * @param p_userOpid            Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid,
                       String         p_opidName,
                       Market         p_defaultMarket,
                       int            p_privilegeId,
                       Double         p_maxAdjustAmount,
                       Double         p_dailyAdjustLimit,
                       String         p_password,
                       PpasDate       p_passwordExpiryDate,
                       boolean        p_guiUser,
                       boolean        p_posiUser,
                       boolean        p_pamiUser,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        PpasDate         l_today;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();
        l_today = DatePatch.getDateToday();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                12300, 
                this,
                "Entered " + C_METHOD_insert);
        }

        l_sql = "INSERT INTO bicsys_userfile (" +
                    "opid," +
                    "opid_name," +
                    "default_srva," +
                    "default_sloc," +
                    "privilege_id," +
                    "max_adj_amount," +
                    "user_max_daily_adj_limit," +
                    "user_password," +
                    "password_expiry_date," +
                    "gui_user," +
                    "posi_user," +
                    "pami_user," +
                    "language," +
                    "user_curr_adj_amt," +
                    "user_curr_date," +
                    "user_opid," +
                    "user_gen_ymdhms )" +
                "VALUES(" + 
                    "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16})";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0,  p_opid);
        l_sqlString.setStringParam  (1,  p_opidName);
        l_sqlString.setIntParam     (2,  p_defaultMarket.getSrva());
        l_sqlString.setIntParam     (3,  p_defaultMarket.getSloc());
        l_sqlString.setIntParam     (4,  p_privilegeId);
        l_sqlString.setDoubleParam  (5,  p_maxAdjustAmount);
        l_sqlString.setDoubleParam  (6,  p_dailyAdjustLimit);
        l_sqlString.setStringParam  (7,  p_password);
        l_sqlString.setDateParam    (8,  p_passwordExpiryDate);
        l_sqlString.setCharParam    (9, p_guiUser ? 'Y' : 'N');
        l_sqlString.setCharParam    (10, p_posiUser ? 'Y' : 'N');
        l_sqlString.setCharParam    (11, p_pamiUser ? 'Y' : 'N');
        l_sqlString.setCharParam    (12, ' ');
        l_sqlString.setDoubleParam  (13, 0.0);
        l_sqlString.setDateParam    (14, l_today);
        l_sqlString.setStringParam  (15, p_userOpid);
        l_sqlString.setDateTimeParam(16, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       12310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       12320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 12330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    12340, 
                    this,
                    "Database error: unable to insert row in bicsys_userfile table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_update,
                                               12350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                12360, 
                this,
                "Leaving " + C_METHOD_insert);
        }
        return;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Method to delete a row from bicsys_userfile.
     * @param p_request The request being processed.
     * @param p_connection The JdbcConnection.
     * @param p_opid The Username of the operator to be deleted.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                16300, 
                this,
                "Entered " + C_METHOD_delete);
        }

        l_sql = "DELETE FROM bicsys_userfile " +
                "WHERE opid = {0}";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0,  p_opid);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       16310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       16320,
                                       this,
                                       p_request,
                                       l_sqlString);

        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 16330, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 16340, this,
                                "Database error: unable to delete row from bicsys_userfile table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_delete,
                                               16350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 16360, this,
                            "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writePassword = "writePassword";
    /**
     * Write password data to BICSYS_USERFILE. The following fields are written:
     * USER_PASSWORD, USER_GEN_YMDHMS, PASSWORD_EXPIRY_DATE, USER_OPID.
     *
     * @param  p_request    The request to process.
     * @param  p_connection Database connection.
     * @param  p_changeOperator Identifier of the operator changing the password.
     * @param  p_operator   Operator to change.
     * @param  p_when       Date/time of password change.
     * @param  p_expiryDate Date of password expiry.
     * @param  p_hashedPassword A hashed password.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public void writePassword(PpasRequest    p_request,
                              JdbcConnection p_connection,
                              String         p_changeOperator,
                              String         p_operator,
                              PpasDateTime   p_when,
                              PpasDate       p_expiryDate,
                              String         p_hashedPassword)
        throws PpasSqlException
    {
        JdbcStatement    l_statement;
        SqlString        l_sql;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 45700, this,
                            "Entered " + C_METHOD_writePassword);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_writePassword,
                                                   45000,
                                                   this,
                                                   p_request);

        l_sql = new SqlString(500, 5, "UPDATE bicsys_userfile " +
                              "SET    user_password = {0}, " +
                              "       user_gen_ymdhms = {1}, " +
                              "       password_expiry_date = {2}, " +
                              "       user_opid = {3} " +
                              "WHERE  opid = {4}");

        l_sql.setStringParam(0, p_hashedPassword);
        l_sql.setDateTimeParam(1, p_when);
        l_sql.setDateParam(2, p_expiryDate);
        l_sql.setStringParam(3, p_changeOperator);
        l_sql.setStringParam(4, p_operator);

        l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_writePassword, 45070, this, p_request, l_sql);

        l_statement.close(C_CLASS_NAME, C_METHOD_writePassword, 45130, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 45310, this,
                            "Leaving " + C_METHOD_writePassword);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeLoginSuccess = "writeLoginSuccess";
    /**
     * Record the fact that the user has logged in successfully and re-set invalid attempt counters.
     *
     * @param  p_request    The request to process.
     * @param  p_connection Database connection.
     * @param  p_operator   Operator logging in.
     * @param  p_when       Date/time of log in.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public void writeLoginSuccess(PpasRequest    p_request,
                                  JdbcConnection p_connection,
                                  String         p_operator,
                                  PpasDateTime   p_when)
        throws PpasSqlException
    {
        JdbcStatement    l_statement;
        SqlString        l_sql;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 54000, this,
                            "Entered " + C_METHOD_writeLoginSuccess);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_writeLoginSuccess,
                                                   54010,
                                                   this,
                                                   p_request);

        l_sql = new SqlString(500, 4, "UPDATE bicsys_userfile " +
                              "SET    user_last_login = {0}, " +
                              "       user_login_failures = {1}, " +
                              "       user_bar_counter = {1}, " +
                              "       user_gen_ymdhms = {2}, " +
                              "       user_opid = {3} " +
                              "WHERE  opid = {3}");

        l_sql.setDateTimeParam(0, p_when);
        l_sql.setIntParam(1, 0);
        l_sql.setDateTimeParam(2, DatePatch.getDateTimeNow());
        l_sql.setStringParam(3, p_operator);

        l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_writeLoginSuccess, 54020, this, p_request, l_sql);

        l_statement.close(C_CLASS_NAME, C_METHOD_writeLoginSuccess, 54030, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 54040, this,
                            "Leaving " + C_METHOD_writeLoginSuccess);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeLoginFailure = "writeLoginFailure";
    /**
     * Record the fact that the user has failed to log in.
     *
     * @param  p_request    The request to process.
     * @param  p_connection Database connection.
     * @param  p_operator   Operator attempting to log in.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public void writeLoginFailure(PpasRequest    p_request,
                                  JdbcConnection p_connection,
                                  String         p_operator)
        throws PpasSqlException
    {
        JdbcStatement    l_statement;
        SqlString        l_sql;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 55000, this,
                            "Entered " + C_METHOD_writeLoginFailure);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_writeLoginFailure,
                                                   55010,
                                                   this,
                                                   p_request);

        l_sql = new SqlString(500, 2, "UPDATE bicsys_userfile " +
                              "SET    user_login_failures = user_login_failures + 1, " +
                              "       user_bar_counter = user_bar_counter + 1, " +
                              "       user_gen_ymdhms = {1}, " +
                              "       user_opid = {0} " +
                              "WHERE  opid = {0}");

        l_sql.setStringParam(0, p_operator);
        l_sql.setDateTimeParam(1, DatePatch.getDateTimeNow());

        l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_writeLoginFailure, 55020, this, p_request, l_sql);

        l_statement.close(C_CLASS_NAME, C_METHOD_writeLoginFailure, 55030, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 55040, this,
                            "Leaving " + C_METHOD_writeLoginFailure);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_barOperator = "barOperator";
    /**
     * Record the fact that the user has logged in successfully and re-set invalid attempt counters.
     *
     * @param  p_request       The request to process.
     * @param  p_connection    Database connection.
     * @param  p_operator      Operator logging in.
     * @param  p_barToDateTime Date/time of log in.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public void barOperator(PpasRequest    p_request,
                            JdbcConnection p_connection,
                            String         p_operator,
                            PpasDateTime   p_barToDateTime)
        throws PpasSqlException
    {
        JdbcStatement    l_statement;
        SqlString        l_sql;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 56000, this,
                            "Entered " + C_METHOD_barOperator);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_barOperator,
                                                   56010,
                                                   this,
                                                   p_request);

        l_sql = new SqlString(500, 2, "UPDATE bicsys_userfile " +
                              "SET    user_bar_to_date_time = {0}, " +
                              "       user_gen_ymdhms = {1}, " +
                              "       user_opid = {2} " +
                              "WHERE  opid = {2}");

        l_sql.setDateTimeParam(0, p_barToDateTime);
        l_sql.setDateTimeParam(1, DatePatch.getDateTimeNow());
        l_sql.setStringParam(2, p_operator);

        l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_barOperator, 56020, this, p_request, l_sql);

        l_statement.close(C_CLASS_NAME, C_METHOD_barOperator, 56030, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 56040, this,
                            "Leaving " + C_METHOD_barOperator);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeAllPasswords = "writeAllPasswords";
    /**
     * Write password data to BICSYS_USERFILE for all users.
     * The following fields are written:
     * USER_PASSWORD, USER_GEN_YMDHMS, PASSWORD_EXPIRY_DATE, USER_OPID.
     *
     * @param  p_request    The request to process.
     * @param  p_connection Database connection.
     * @param  p_changeOperator Identifier of the operator making the change.
     * @param  p_when       Date/time of password change.
     * @param  p_expiryDate Date of password expiry.
     * @param  p_hashedPassword A hashed password.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public void writeAllPasswords(PpasRequest    p_request,
                                  JdbcConnection p_connection,
                                  String         p_changeOperator,
                                  PpasDateTime   p_when,
                                  PpasDate       p_expiryDate,
                                  String         p_hashedPassword)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        SqlString        l_sql;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 46700, this,
                            "Entered " + C_METHOD_writeAllPasswords);
        }

        l_statement = p_connection.createStatement(
                                          C_CLASS_NAME,
                                          C_METHOD_writeAllPasswords,
                                          46000,
                                          this,
                                          p_request);

        l_sql = new SqlString(500, 4, "UPDATE bicsys_userfile " +
            "SET    user_password = {0}, " +
            "       user_gen_ymdhms = {1}, " +
            "       password_expiry_date = {2}, " +
            "       user_opid = {3} ");

        l_sql.setStringParam(0, p_hashedPassword);
        l_sql.setDateTimeParam(1, p_when);
        l_sql.setDateParam(2, p_expiryDate);
        l_sql.setStringParam(3, p_changeOperator);

        l_statement.executeUpdate(C_CLASS_NAME, C_METHOD_writeAllPasswords, 46070, this, p_request, l_sql);

        l_statement.close(C_CLASS_NAME, C_METHOD_writeAllPasswords, 46130, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 46310, this,
                            "Leaving " + C_METHOD_writeAllPasswords);
        }
    }
}
