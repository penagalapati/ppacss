////////////////////////////////////////////////////////////////////////////////
//    ACSC IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :      FabaFafBarredListData.Java
//    DATE            :      10-Mar-2004
//    AUTHOR          :      Olivier Duparc
//    REFERENCE       :      PRD_ASCS_GEN_SS_071
//
//    COPYRIGHT       :      ATOS ORIGIN 2004
//
//    DESCRIPTION     :      A Barred number.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |       
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single Barred number.
 */
public class FabaFafBarredListData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FabaFafBarredListData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Market. */
    private Market i_market = null;
    
    /** Number prefix. */
    private String i_fabaNumberPrefix = null;

    /** Number length. */
    private int i_fabaNumberLength = 0;

    /** Start Date. */
    private PpasDate i_fabaStartDate = null;

    /** End Date. */
    private PpasDate i_fabaEndDate = null;

    /** The operator identifier of the operator that either
     * created the row or performed the last update. 
     */
    private String i_fabaOpid = null;
    
    /** The date and time of the last modification to this row of the table. */
    private PpasDateTime i_fabaGenYmdhms;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Barred Number object and initialises instance variables.
     * 
     * @param p_request             The request to process.
     * @param p_market              The market.
     * @param p_fabaNumberPrefix    The Number prefix.
     * @param p_fabaNumberLength    The Number length.
     * @param p_fabaStartDate       The start date time for the Barred Number.
     * @param p_fabaEndDate         The end date time for the Barred Number.
     * @param p_fabaOpid            The operator that last changed this data.
     * @param p_fabaGenYmdhms       The date/time this data was last changed.
     */
    public FabaFafBarredListData(
        PpasRequest        p_request,
        Market             p_market,
        String             p_fabaNumberPrefix,
        int                p_fabaNumberLength,
        PpasDate           p_fabaStartDate,
        PpasDate           p_fabaEndDate,
        String             p_fabaOpid,
        PpasDateTime       p_fabaGenYmdhms)
    {

        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market = p_market;
        i_fabaNumberPrefix = p_fabaNumberPrefix;
        i_fabaNumberLength = p_fabaNumberLength;
        i_fabaStartDate    = p_fabaStartDate;
        i_fabaEndDate      = p_fabaEndDate;
        i_fabaOpid         = p_fabaOpid;
        i_fabaGenYmdhms    = p_fabaGenYmdhms;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor
      //         FabaFafBarredListData(PpasRequest, long, Logger, ...)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** 
     * Returns the market.
     * @return Market.
     */
    public Market getMarket()
    {
        return(i_market);
    }

    /** Returns the Number prefix.
     * 
     * @return The Number prefix.
     */
    public String getNumberPrefix()
    {
        return(i_fabaNumberPrefix);
    }

    /** Returns the Number length.
     * 
     * @return The Number length.
     */
    public int getNumberLength()
    {
        return(i_fabaNumberLength);
    }

    /** Returns the start date of the Barred number.
     * 
     * @return The start date of the Barred number.
     */
    public PpasDate getStartDate()
    {
        return(i_fabaStartDate);
    }

    /** Returns the end date of the Barred number.
     * 
     * @return The end date of the Barred number.
     */
    public PpasDate getEndDate()
    {
        return(i_fabaEndDate);
    }
    
    /** Returns Operator Identifier.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid()
    {
        return (i_fabaOpid);
    }
    
    /** Return Date and time of last modification to the row in the table.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getGenYmdhms()
    {
        return (i_fabaGenYmdhms);
    }
} // End of public class FabaFafBarredListData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////