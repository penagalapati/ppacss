////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SyfgSystemConfigCache.Java
//      DATE            :       14-October-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1621/6878
//                              PRD_PPAK00_GEN_CA_369
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A cache of data from SYFG_SYSTEM_CONFIG
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SyfgSystemConfigSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** A cache of data from SYFG_SYSTEM_CONFIG. */
public class SyfgSystemConfigCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SyfgSystemConfigCache";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** The System Configuration data. */
    private SyfgSystemConfigData i_syfgSystemConfigData = null;

    /** Sql service used to select the data from the database. */
    private SyfgSystemConfigSqlService i_syfgSystemConfigSqlService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new System Configuration data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public SyfgSystemConfigCache(PpasRequest    p_request,
                                 Logger         p_logger,
                                 PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_syfgSystemConfigSqlService = new SyfgSystemConfigSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10100, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the System Configuration data.
     * 
     * @return System Configuration data.
     */
    public SyfgSystemConfigData getSyfgSystemConfigData()
    {
        return i_syfgSystemConfigData;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /** Reloads the System Configuration data into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest     p_request,
                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_reload );
        }

        i_syfgSystemConfigData = i_syfgSystemConfigSqlService.read(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11100, this,
                "Leaving " + C_METHOD_reload );
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_syfgSystemConfigData);
    }
}

