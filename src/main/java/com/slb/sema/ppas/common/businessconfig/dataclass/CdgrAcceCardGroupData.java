////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CdgrAcceCardGroupData.Java
//      DATE            :      03-Apr-2001
//      AUTHOR          :      Erik Clayton
//
//      COPYRIGHT       :      WM-data 2007
//
//      DESCRIPTION     :      An object that represents a row of the database table cdgr_acce_card_group.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDebug;

/** An object that represents a row of the database table cdgr_acce_card_group. */
public class CdgrAcceCardGroupData extends DataObject
{
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new card group and initialises data.
     * 
     * @param p_cardGroup Card Group.
     * @param p_groupDescription description of this card group.
     * @param p_deleteFlag Flag to indicate whether the card group has been flagged as deleted.
     */
    public CdgrAcceCardGroupData(
        String                 p_cardGroup,
        String                 p_groupDescription,
        String                 p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10010, this,
                            "Constructing CdgrAcceCardGroupData(...)");
        }

        i_cardGroup           = p_cardGroup;
        i_groupDescription    = p_groupDescription;
        i_isCurrent= !p_deleteFlag.equals("*");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10050, this,
                            "Constructed CdgrAcceCardGroupData(...)");
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the card group.
     * 
     * @return The key of this card group.
     */
    public String getCardGroup()
    {
        return i_cardGroup == null ? "" : i_cardGroup;
    }

    /** Returns the card group description.
     * 
     * @return The description associated with this acrd group.
     */
    public String getGroupDescription()
    {
        return i_groupDescription == null ? "" : i_groupDescription;
    }

    /** Returns the card group delete flag.
     * 
     * @return Flag indicating whether this card group is marked as derleted. */
    public boolean getIsCurrent()
    {
        return i_isCurrent ;
    }

    /** Sets the card group.
     * 
     * @param p_cardGroup New card group.
     */
    public void setCardGroup(String p_cardGroup)
    {
        i_cardGroup = p_cardGroup;
    }

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------

    /** Card group code. */
    protected  String   i_cardGroup = null;

    /** Description of card group. */
    protected  String   i_groupDescription = null;

    /** Description of card group. */
    protected  boolean   i_isCurrent = false;

    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CdgrAcceCardGroupData";
}
