////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BicsysUserfileDataSet.java
//    DATE            :       4-Jun-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#227/2175
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Holds a set of BICSYS_USERFILE data records.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Holds bicsys_userfile data objects. */
public class BicsysUserfileDataSet
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BicsysUserfileDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** A set of bicsys_userfile data objects. */
    private Vector   i_bicsysUserfileDataSetV;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a new object containing an empty Vector.
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     */
    public BicsysUserfileDataSet(PpasRequest p_request,
                                 int         p_initialVSize,
                                 int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, 
                 C_CLASS_NAME, 
                 10000, 
                 this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_bicsysUserfileDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, 
                 C_CLASS_NAME, 
                 10010, 
                 this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------
    /** 
     * Adds an element to the data set.
     * @param  p_operatorData The data object to be added.
     */
    public void addElement(BicsysUserfileData p_operatorData)
    {
        i_bicsysUserfileDataSetV.addElement(p_operatorData);
    }
}