////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscCodeDataSet.Java
//      DATE            :       21-August-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#964/4107
//                              PRD_PPAK00_GEN_CA_288
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Contains the misc codes selected from the 
//                              SRVA_MISC_CODES table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 29-Sep-03 | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.ChargedEventCounterTypeId;
import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Contains the misc codes selected from the SRVA_MISC_CODES table. */
public class MiscCodeDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "MiscCodeDataSet";

    /** String indicating that this is a contact type code. */
    private static final String C_CONTACT_TYPE_CODE         = "CBR";

    /** String indicating that this is an address type code. */
    private static final String C_ADDRESS_TYPE_CODE         = "ADR";

    /** String indicating that this is a payment type code. */
    private static final String C_PAYMENT_TYPE_CODE         = "PYM";

    /** String indicating that this is a charged event counter type code. */
    public static final String C_CHARGED_EVENT_COUNTER_TYPE = "CTR";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** A Set of MiscCodeData objects. */
    private Set  i_miscCodes;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty TreeSet. */
    public MiscCodeDataSet()
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_miscCodes = new TreeSet();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** 
     * Returns the description for the contact type code passed in.
     *
     * @param p_market The customer's market.
     * @param p_contactTypeCode The contact type code.
     * @return Description for the specified market and contact type.
     */    
    public String getContactTypeDescription(Market p_market,
                                            String p_contactTypeCode)
    {
        return getDescription(p_market, C_CONTACT_TYPE_CODE, p_contactTypeCode);
    }

    /** 
     * Returns the description for the address type code passed in.
     *
     * @param p_market The customer's market.
     * @param p_addressTypeCode The address type code.
     * @return Address for the specified market and contact type.
     */
    public String getAddressTypeDescription(Market p_market,
                                            String p_addressTypeCode)
    {
        return getDescription(p_market, C_ADDRESS_TYPE_CODE, p_addressTypeCode);
    }

    /** 
     * Returns data associated with a specified key.
     * Use only non-deleted entries.
     *
     * @param p_dataType Type of data.
     * @param p_market The customer's market.
     * @param p_dataKey The key of the data.
     * 
     * @return Details associated with the key or <code>null</code> if not found.
     */    
    public MiscCodeData getDataFromKey(
                                    String  p_dataType,
                                    Market  p_market,
                                    String  p_dataKey)
    {
        return getDataFromKey(p_dataType, p_market, p_dataKey, true);
    }
    
    /** 
     * Returns raw data associated with a specified key.
     *
     * @param p_dataType Type of data.
     * @param p_market The customer's market.
     * @param p_dataKey The key of the data.
     * 
     * @return Details associated with the key or <code>null</code> if not found.
     */    
    public MiscCodeData getRawDataFromKey(
                                    String  p_dataType,
                                    Market  p_market,
                                    String  p_dataKey)
    {
        MiscCodeData  l_tmpData = null;
        MiscCodeData  l_retData = null;
        Iterator l_it = i_miscCodes.iterator();
        while (l_it.hasNext())
        {
            l_tmpData = (MiscCodeData)l_it.next();
            
            if ( l_tmpData.getMarket()  .equals(p_market) &&
                 l_tmpData.getCodeType().equals(p_dataType) &&
                 l_tmpData.getCode()    .equals(p_dataKey) )
               {
                   l_retData = l_tmpData;
                   break;
               }

        }
        return l_retData;
    }
    
    /** 
     * Returns data associated with a specified key - assuming the market is already restricted.
     *
     * @param p_dataType Type of data.
     * @param p_dataKey The key of the data.
     * 
     * @return Details associated with the key or <code>null</code> if not found.
     */    
    public MiscCodeData getDataFromKey(String p_dataType,
                                       String p_dataKey)
    {
        MiscCodeData l_data      = null;
        MiscCodeData l_dataFirst = null;
        
        if (i_miscCodes.size() > 0)
        {
            l_dataFirst = (MiscCodeData)i_miscCodes.toArray()[0];
            l_data = getDataFromKey(p_dataType, l_dataFirst.getMarket(), p_dataKey, true);
        }
        
        return l_data;
    }
    
    /** 
     * Returns details of a payment type.
     *
     * @param p_market The customer's market.
     * @param p_paymentType The payment type code.
     * 
     * @return Details associated with the payment type or <code>null</code> if not found.
     */    
    public MiscCodeData getPaymentTypeData(Market p_market,
                                           String p_paymentType)
    {
        return getDataFromKey(C_PAYMENT_TYPE_CODE, p_market, p_paymentType, true);
    }

    /**
     * Returns details of a payment type - assuming market already restricted.
     *
     * @param p_paymentType The payment type code.
     * 
     * @return Details associated with the payment type or <code>null</code> if not found.
     */    
    public MiscCodeData getPaymentTypeData(String p_paymentType)
    {
        return getDataFromKey(C_PAYMENT_TYPE_CODE, p_paymentType);
    }

    /** 
     * Returns details of a specific charged event counter type.
     *
     * @param p_market                  The customer's market.
     * @param p_chargedEventCounterType The charged event counter type code.
     * 
     * @return Details associated with the charged event counter type or <code>null</code> if not found.
     */    
    public MiscCodeData getChargedEventCounterData(Market p_market,
                                                   ChargedEventCounterTypeId p_chargedEventCounterType)
    {
        return getChargedEventCounterData(p_market, p_chargedEventCounterType.getcounterTypeIdAsString());
    }

    /** 
     * Returns details of a specific charged event counter type.
     *
     * @param p_market                  The customer's market.
     * @param p_chargedEventCounterType The charged event counter type code.
     * 
     * @return Details associated with the charged event counter type or <code>null</code> if not found.
     */    
    public MiscCodeData getChargedEventCounterData(Market p_market, String p_chargedEventCounterType)
    {
        return getDataFromKey(C_CHARGED_EVENT_COUNTER_TYPE, p_market, p_chargedEventCounterType, true);
    }
    
    /**
     * Returns details of a specific charged event counter type - assuming market already restricted.
     *
     * @param p_chargedEventCounterType The charged event counter type code.
     * 
     * @return Details associated with the charged event counter type or <code>null</code> if not found.
     */    
    public MiscCodeData getChargedEventCounterData(ChargedEventCounterTypeId p_chargedEventCounterType)
    {
        return getChargedEventCounterData(p_chargedEventCounterType.getcounterTypeIdAsString());
    }

    /**
     * Returns details of a specific charged event counter type - assuming market already restricted.
     *
     * @param p_chargedEventCounterType The charged event counter type code.
     * 
     * @return Details associated with the charged event counter type or <code>null</code> if not found.
     */    
    public MiscCodeData getChargedEventCounterData(String p_chargedEventCounterType)
    {
        return getDataFromKey(C_CHARGED_EVENT_COUNTER_TYPE, p_chargedEventCounterType);
    }

    /** 
     * Adds an element to the vector.
     *
     * @param  p_miscTypeData The misc code object to be added.
     */
    public void addMiscCode(MiscCodeData p_miscTypeData)
    {
        i_miscCodes.add(p_miscTypeData);
    }

    /** Create a new MiscCodeDataSet that is a subset of the data in this 
     *  object where the records selected are based on given market and
     *  msic code values.
     * 
     * @param p_market Market to select the subset of records in this class upon.
     * @param p_miscType Misc type to use to select the subset of records in
     *                   this class upon.
     * @return Data set containing a subset of the data in this class, based on
     *         the given market and misc code values.
     */
    public MiscCodeDataSet getMiscCodeData (Market      p_market,
                                            String      p_miscType)
    {
        return getMiscCodeData(p_market, p_miscType, false);
    }

    /** Get available data for a given market.
     *  <p>
     *  Create a new MiscCodeDataSet that is a subset of the data in this 
     *  object where the records selected are based on given market and
     *  msic code values and are currently available (i.e. not marked as
     *  deleted).
     * 
     * @param p_market Market to select the subset of records in this class upon.
     * @param p_miscType Misc type to use to select the subset of records in
     *                   this class upon.
     * @return Data set containing a subset of the data in this class, based on
     *         the given market and misc code values.
     */
    public MiscCodeDataSet getAvailableMiscCodeData (Market      p_market,
                                                     String      p_miscType)
    {
        return getMiscCodeData(p_market, p_miscType, true);
    }

    /** Get available data for a given code and market.
     *  <p>
     *  Create a new MiscCodeDataSet that is a subset of the data in this 
     *  object where the records selected are based on given misc code 
     *  values in the specified market and are currently available (i.e. not marked as deleted).
     * 
     * @param p_market  The market for which data is required.
     * @param p_miscType Misc type to use to select the subset of records in this class upon.
     * @return Data set containing a subset of the data in this class, based on
     *         the given misc code value.
     */
    public MiscCodeDataSet getRawAvailableMiscCodeData (Market      p_market,
                                                        String      p_miscType)
    {
        MiscCodeDataSet l_returnData = null;
        MiscCodeData    l_data       = null;

        l_returnData = new MiscCodeDataSet();
        Iterator l_it = i_miscCodes.iterator();
        while (l_it.hasNext())
        {
            l_data = (MiscCodeData)l_it.next();

            if ( l_data.getMarket()  .equals(p_market) &&
                    l_data.getCodeType().equals(p_miscType) && 
                    l_data.getIsCurrent() )
            {
                l_returnData.addMiscCode(l_data);
            }
        }

        return (l_returnData);
    }

    /** Return the data within this data set.
     *  @return Vector containing all data within this dataset. 
     */
    public Vector getDataV ()
    {
        return new Vector(i_miscCodes);
    }

    /** Get available data for a given market.
     *  <p>
     *  Create a new MiscCodeDataSet that is a subset of the data in this 
     *  object where the records selected are based on given market and
     *  msic code values. A flag determines if non-deleted entries must be
     *  included.
     * 
     * @param p_market Market to select the subset of records in this class upon.
     * @param p_miscType Misc type to use to select the subset of records in this class upon.
     * @param p_useCurrent True if use only non-deleted items.
     * 
     * @return Data set containing a subset of the data in this class, based on
     *         the given market and misc code values.
     */
    private MiscCodeDataSet getMiscCodeData (Market      p_market,
                                             String      p_miscType,
                                             boolean     p_useCurrent)
    {
        MiscCodeDataSet l_returnData   = new MiscCodeDataSet();
        MiscCodeData    l_data = null;
        MiscCodeData    l_dataGlobMkt = null;
        MiscCodeData    l_dataSpecMkt = null;
        ArrayList       l_globMktAL = null;
        HashMap         l_specMktHM = null;
        Iterator        l_iter = null;
        Iterator        l_it   = null;

        // First pass : build 2 data structures :
        // 1. an Arraylist for entries with Global Market and MiscType
        // 2. an HashMap for entries with Specific Market and MiscType
        l_globMktAL = new ArrayList();
        l_specMktHM = new HashMap();
        
        l_it = i_miscCodes.iterator();
        while(l_it.hasNext())
        {
            l_data = (MiscCodeData)l_it.next();
            if ( l_data.getMarket()  .equals(Market.C_GLOBAL_MARKET) &&
                    l_data.getCodeType().equals(p_miscType) )
            {
                l_globMktAL.add(l_data);
            }
            else
            if ( l_data.getMarket()  .equals(p_market) &&
                    l_data.getCodeType().equals(p_miscType) )
            {
               l_specMktHM.put(l_data.getCode(), l_data);
            }            
        }

        // 2nd pass : merge previous AL and HM into return DataSet
        // 2.a Travel ArrayList of GlobalMkt entries
        int l_lenAL = l_globMktAL.size();
        for (int l_i = 0; l_i < l_lenAL; l_i++)
        {
            l_dataGlobMkt = (MiscCodeData) l_globMktAL.get(l_i);
            l_dataSpecMkt = (MiscCodeData) l_specMktHM.get(l_dataGlobMkt.getCode());   
            l_data = combineData(l_dataGlobMkt, l_dataSpecMkt, p_market, p_useCurrent);
            // Remove entry from HasMap 
            if (l_dataSpecMkt != null)
            {
                l_specMktHM.remove(l_dataGlobMkt.getCode());
            }
            // Add to result DataSet
            if (l_data != null)
            {
                l_returnData.addMiscCode(l_data);
            }
        } 
        
        // 2.b Travel HashMap of remaining SpecMkt entries
        Collection l_vals = l_specMktHM.values();
        l_iter = l_vals.iterator();
        while ( l_iter.hasNext() )
        {
            l_dataSpecMkt = (MiscCodeData) l_iter.next();
            l_data = combineData(null, l_dataSpecMkt, p_market, p_useCurrent);
            // Add to result DataSet
            if (l_data != null)
            {
                l_returnData.addMiscCode(l_data);
            }
        }

        return (l_returnData);

    } // end of method getMiscCodeData

    /**
     * For the same (Market, CodeType, Code) combines a GlobalMarket and 
     * a SpecificMarket entries in order to handle 'non-deleted' issues.
     *  
     * @param p_dataGlobMkt a MiscCodeData entry for the Global Market 
     * @param p_dataSpecMkt a MiscCodeData entry for the Specific Market 
     * @param p_market the specific Market used to select a subset of records.
     * @param p_useCurrent True if use only non-deleted items.
     * 
     * @return a combined MiscCodeData object.
     */
    private MiscCodeData combineData(
                            MiscCodeData  p_dataGlobMkt, 
                            MiscCodeData  p_dataSpecMkt, 
                            Market        p_market, 
                            boolean       p_useCurrent)
    {
        MiscCodeData l_data = null;
        
        if (p_dataGlobMkt == null)
        {
            // Only a SpecificMarket entry, keep it, barring 
            // non-deleted condition
            if ( p_dataSpecMkt.getIsCurrent() 
                 || 
                 (!p_dataSpecMkt.getIsCurrent() && !p_useCurrent) )
            {
                l_data = p_dataSpecMkt;
            }
        }
        else
        {
            if (p_dataSpecMkt == null)
            {
                // Only a GlobalMarket entry, keep it, barring 
                // non-deleted condition
                if ( p_dataGlobMkt.getIsCurrent() 
                     || 
                     (!p_dataGlobMkt.getIsCurrent() && !p_useCurrent) )
                {
                    l_data = p_dataGlobMkt;
                }
            } 
            else 
            {
                // Determine which one to keep depending on non-deleted condition
                // and precedence of SpecificMkt above GlobalMkt
                if ( p_dataSpecMkt.getIsCurrent() 
                     || 
                     (!p_dataSpecMkt.getIsCurrent() && !p_useCurrent) )
                {
                    l_data = p_dataSpecMkt;
                }
                else
                if ( p_dataGlobMkt.getIsCurrent() 
                     || 
                     (!p_dataGlobMkt.getIsCurrent() && !p_useCurrent) )
                {
                    l_data = p_dataGlobMkt;
                }
            }
        }

        return l_data == null ? null : l_data.getExplicInstance(p_market);
    }
    
    /** 
     * Returns the description for the type code passed in.
     *
     * @param p_market The customer's market.
     * @param p_codeType The code type.
     * @param p_code The code.
     * @return Description for the specified market and type code.
     */    
    private String getDescription(Market p_market,
                                  String p_codeType,
                                  String p_code)
    {
        MiscCodeData l_data = null;
        String       l_ret = null;
        
        l_data = getDataFromKey(
                        p_codeType,
                        p_market,
                        p_code,
                        true // p_useCurrent
                        );
                        
        if (l_data != null)
        {
            l_ret = l_data.getDescription();
        }
        
        return l_ret;
    }

    /** 
     * Returns data associated with a specified key.
     *
     * @param p_dataType Type of data.
     * @param p_market The customer's market.
     * @param p_dataKey The key of the data.
     * @param p_useCurrent True if use only non-deleted items
     * 
     * @return Details associated with the key or <code>null</code> if not found.
     */    
    private MiscCodeData getDataFromKey(
                                    String  p_dataType,
                                    Market  p_market,
                                    String  p_dataKey,
                                    boolean p_useCurrent)
    {
        // Entry found for the Global Market
        MiscCodeData  l_dataGlobMkt = null;
        MiscCodeData  l_data = null;
        boolean       l_found = false;
        Iterator      l_it    = null;
        
        l_it = i_miscCodes.iterator();
        while(l_it.hasNext())
        {
            l_data = (MiscCodeData)l_it.next();
            if ( l_data.getMarket()  .equals(Market.C_GLOBAL_MARKET) &&
                    l_data.getCodeType().equals(p_dataType) &&
                    l_data.getCode()    .equals(p_dataKey) )
            {
                l_dataGlobMkt = l_data;
            }
            else
            if ( l_data.getMarket()  .equals(p_market) &&
                 l_data.getCodeType().equals(p_dataType) &&
                 l_data.getCode()    .equals(p_dataKey) )
            {
                l_found = true;
                break;
            }
        }

        if (l_found)
        {
            // if must use non-deleted
            if (p_useCurrent)
            {
                // if specific is deleted
                if (!l_data.getIsCurrent())
                {
                    // if global exists
                    if (l_dataGlobMkt != null)
                    {
                        // and is non-deleted
                        if (l_dataGlobMkt.getIsCurrent())
                        {
                            l_data = l_dataGlobMkt;
                        } 
                        else 
                        {
                            l_data = null;
                        }
                    } 
                    else
                    {
                        l_data = null;
                    }
                }
            } 
        }
        else
        {
            l_data = null;
            // if global exists
            if (l_dataGlobMkt != null)
            {
                if (p_useCurrent)
                {
                    if (l_dataGlobMkt.getIsCurrent())
                    {
                        l_data = l_dataGlobMkt;
                    } 
                } 
                else 
                {
                    l_data = l_dataGlobMkt;
                }
            } 
        }

        // Instantiate data with requested Market
        if (l_data != null)
        {
            l_data = new MiscCodeData(
                            p_market,
                            l_data.getCodeType(),
                            l_data.getCode(),
                            l_data.getDescription(),
                            (l_data.getIsCurrent() ? " " : "*" ) );
        }

        return l_data;
    }
    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("MiscData", i_miscCodes, p_sb);
    }
}
