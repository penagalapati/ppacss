////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PrilPrivilegeLevelSqlService.Java
//      DATE            :       02-June-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpaLon#112/2508
//                              PRD_ASCS00_DEV_SS_088
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       SQL service class for the PRIL_PRIVILEGE_LEVEL 
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrilPrivilegeLevelDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * SQL service class for the PRIL_PRIVILEGE_LEVEL table.
 */
public class PrilPrivilegeLevelSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrilPrivilegeLevelSqlService";

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------
    /** The Logger object to be used in logging any exceptions arising from the
     *  use of this service.
     */
    private Logger i_logger = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Simple constructor.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public PrilPrivilegeLevelSqlService(PpasRequest p_request,
                                        Logger      p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Fetches privilege level profiles from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of Privilege Level profile objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public PrilPrivilegeLevelDataSet readAll(PpasRequest            p_request,
                                             JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement                l_statement;
        JdbcResultSet                l_resultSet;
        SqlString                    l_sql;
        PrilPrivilegeLevelData       l_prilData;
        PrilPrivilegeLevelDataSet    l_prilDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            11000,
                            this,
                           "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(1000, 0, "SELECT pril_level," +
                                       "pril_description," +
                                       "pril_opid," +
                                       "pril_gen_ymdhms," +
                                       "pril_del_flag " +
                                       "FROM pril_privilege_level " +
                                       "ORDER BY pril_level");

        l_prilDataSet = new PrilPrivilegeLevelDataSet(p_request, 20, 10);

        l_statement = p_connection.createStatement (C_CLASS_NAME,
                                                    C_METHOD_readAll,
                                                    11010,
                                                    this,
                                                    p_request);

        l_resultSet = l_statement.executeQuery( C_CLASS_NAME,
                                                C_METHOD_readAll,
                                                11020,
                                                this,
                                                p_request,
                                                l_sql);

        while (l_resultSet.next(11030))
        {
            l_prilData = new PrilPrivilegeLevelData
                                 (p_request,
                                  l_resultSet.getInt(11040, "pril_level"),
                                  l_resultSet.getTrimString(11050, "pril_description"),
                                  l_resultSet.getString(11080, "pril_del_flag"));

            l_prilDataSet.addElement(l_prilData);
        }

        l_resultSet.close(11300);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 11310, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            11320,
                            this,
                            "Leaving " + C_METHOD_readAll);
        }

        return(l_prilDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Update the PRIL_PRIVILEGE_LEVEL database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_level Privilege Level to be updated
     * @param p_description Description of the privilege level
     * @param p_opid The username of the Operator requesting the update
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_level,
                       String                 p_description,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            12000,
                            this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = new String("UPDATE pril_privilege_level " +
                           "SET pril_description = {0}, " +
                           "    pril_opid = {1}, " + 
                           "    pril_gen_ymdhms = {2}, " + 
                           "    pril_del_flag = {3} " +
                           "WHERE pril_level = {4}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_description);
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setCharParam    (3, ' ');
        l_sqlString.setStringParam  (4, p_level);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       12050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   12100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 89040, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in pril_privilege_level table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
 
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            12300,
                            this,
                            "Leaving " + C_METHOD_update);
        }
        return;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a new privilege into the PRIL_PRIVILEGE_LEVEL database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_level The privilege level
     * @param p_description Description of the privilege level
     * @param p_opid The username of the operator requesting this operation
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_level,
                       String                 p_description,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            12350,
                            this,
                            "Entered " +  C_METHOD_insert);
        }

        l_sql = new String("INSERT INTO pril_privilege_level " +
                           "(PRIL_LEVEL, PRIL_DESCRIPTION, PRIL_OPID, PRIL_GEN_YMDHMS, PRIL_DEL_FLAG)" +
                           "VALUES({0},{1},{2},{3},{4})");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_level);
        l_sqlString.setStringParam  (1, p_description);
        l_sqlString.setStringParam  (2, p_opid);
        l_sqlString.setDateTimeParam(3, l_now);
        l_sqlString.setCharParam    (4, ' ');

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       12400,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   12450,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 12600, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in pril_privilege_level table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            12650,
                            this,
                            "Leaving " + C_METHOD_insert);
        }
        return;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the PRIL_PRIVILEGE_LEVEL table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_level The privilege level to be marked as deleted.
     * @param p_opid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_level,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            32100, 
                            this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE pril_privilege_level " +
                           "SET pril_del_flag = {0}, " +
                           "    pril_opid = {1}, " + 
                           "    pril_gen_ymdhms = {2} " + 
                           "WHERE pril_level = {3}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setCharParam    (0, '*');
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setStringParam  (3, p_level);

        try
        {                                      
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_delete,
                                                       13100,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   13150,
                                                   this,
                                                   p_request,
                                                   l_sqlString);                                       
        }
        finally
        {
            if (l_statement != null)
            {                   
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 13250, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10240, 
                    this,
                    "Database error: unable to mark row as deleted in pril_privilege_level table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_delete,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME,
                            13300,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
        return;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a previously-withdrawn privilege level as available in the PRIL_PRIVILEGE_LEVEL table.
     * 
     * @param p_request Request to process.
     * @param p_connection Database Connection.
     * @param p_level Privilege level to be marked as available.
     * @param p_opid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                String         p_level,
                                String         p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            14000, 
                            this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = new String("UPDATE pril_privilege_level " +
                           "SET pril_del_flag = {0}, " +
                           "    pril_opid = {1}, " + 
                           "    pril_gen_ymdhms = {2} " + 
                           "WHERE pril_level = {3}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setStringParam  (3, p_level);

        try
        {                                      
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       14010,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   14020,
                                                   this,
                                                   p_request,
                                                   l_sqlString);                                       
        }
        finally
        {
            if (l_statement != null)
            {                   
                l_statement.close(C_CLASS_NAME, C_METHOD_markAsAvailable, 14030, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    14040, 
                    this,
                    "Database error: unable to mark row as available in pril_privilege_level table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_markAsAvailable,
                                              14050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME,
                            14060,
                            this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}
