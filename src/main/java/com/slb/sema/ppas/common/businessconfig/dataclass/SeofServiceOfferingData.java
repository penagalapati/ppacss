////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SeofServiceOfferingData.java
//      DATE            :       11-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PRD_ASCS00_GEN_CA_016
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Data object used to store Service Offering data.
//                              
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Data object used to store Service Offering data.
 */
public class SeofServiceOfferingData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "SeofServiceOfferingData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**
     * Number representation of the service offering.
     */
    private int i_seofNumber;
    
    /**
     * description of the service offering.
     */
    private String i_seofDesc;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** 
     * Creates an Service Offering Data object.
     * 
     * @param p_request The request to process.
     * @param p_seofNumber The service offering id.
     * @param p_seofDesc The service offering description.
     * @param p_opid Operator that changed this data.
     * @param p_genYmdHms Date/time this data was last changed.
     */
    public SeofServiceOfferingData(PpasRequest  p_request,
                                   int          p_seofNumber,
                                   String       p_seofDesc,
                                   String       p_opid,
                                   PpasDateTime p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }
        
        i_seofNumber = p_seofNumber;
        i_seofDesc   = p_seofDesc;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /**
     * Returns a service offering number.
     * @return Service offering number.
     */
    public int getServiceOfferingNumber()
    {
        return (i_seofNumber);
    }

    /**
     * Returns a service offering description.
     * @return Service offering description.
     */
    public String getServiceOfferingDescription()
    {
        return (i_seofDesc);
    }

    /**
     * Returns a string representation of the service offering data.
     * @return Service offering data.
     */
    public String toString()
    {
        return("SeofServiceOfferingData:" +
               "i_seofNumber=" + i_seofNumber + ":" +
               "i_seofDesc=" + i_seofDesc);
    }
    
    /**
     * Compares two service offering data objects and returns true if they equate.
     * @param p_object Service offering data object to compare the current instance with.
     * @return True if both instances are equal
     */
    public boolean equals(Object p_object)
    {
        boolean l_equals = false;
        
        if (p_object != null && p_object instanceof SeofServiceOfferingData)
        {
            if (getServiceOfferingNumber() == ((SeofServiceOfferingData)p_object).getServiceOfferingNumber())
            {
                l_equals = true;
            }
        }
        else
        {
            if (this == p_object)
            {
                l_equals = true;
            }
        }
        
        return l_equals;
    }
}
