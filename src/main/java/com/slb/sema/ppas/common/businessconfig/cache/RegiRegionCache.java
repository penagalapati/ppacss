////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RegiRegionCache
//      DATE            :       11-Aug-2005
//      AUTHOR          :       David Bitmead
//      REFERENCE       :       PRD_ASCS00_GEN_CA_44
//
//      COPYRIGHT       :       WM-data
//
//      DESCRIPTION     :       Implements a cache for the home region information
//                              held in the regi_region table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.RegiRegionDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.RegiRegionSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the home region business configuration data (from the
 * regi_region table).
 */
public class RegiRegionCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "RegiRegionCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private RegiRegionSqlService i_regiRegionSqlService = null;

    /** Data set containing all configured home region records. */
    private RegiRegionDataSet i_allRegions = null;

    /**
     * Cache of available home region information (not including records marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private RegiRegionDataSet i_availableRegions = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Home Region information data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_ppasContext The ASCS context containing business and system configuration
     *         data required by this service.
     */
    public RegiRegionCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasContext            p_ppasContext)
    {
        super (p_logger, p_ppasContext.getProperties());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 83110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;
        i_regiRegionSqlService = new RegiRegionSqlService(p_request, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 83120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all home region data (including records marked as deleted).
     * @return Data set containing all region data.
     */
    public RegiRegionDataSet getAll()
    {
        return i_allRegions;
    }

    /**
     * Returns available home region data.
     * The returned data set does not include records marked as deleted.
     * @return Data set containing market data that is not marked as deleted.
     */
    public RegiRegionDataSet getAvailable()
    {
        return (i_availableRegions);

    } // End of public method getAvailable

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 73230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allRegions = i_regiRegionSqlService.readAll (p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded!
        i_availableRegions = i_allRegions.getAvailable();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 73290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData( i_allRegions, i_availableRegions);
    }
}
