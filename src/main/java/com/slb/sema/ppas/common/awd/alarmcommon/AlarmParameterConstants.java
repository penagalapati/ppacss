////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AlarmParameterConstants.java
//      DATE            :       22-Jul-2004
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       PpaLon#246
//
//      COPYRIGHT       :       ATOSORIGIN 2004
//
//      DESCRIPTION     :       This interface defines Alarm Parameter Constants.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.awd.alarmcommon;

/**
 * This interface defines Alarm Parameter Constants.
 */
public interface AlarmParameterConstants
{
    
    /** Parameter name of Alarm Sequence Number. Value is {@value}. */
    public static final String C_PARAM_ALARM_SEQUENCE_NUMBER = "_AlarmSequenceNumber";

    /** Parameter name of Alarm Name. Value is {@value}. */
    public static final String C_PARAM_NAME = "_AlarmName";

    /** Parameter name of Alarm Text. Value is {@value}. */
    public static final String C_PARAM_TEXT = "_AlarmText";

    /** Parameter name of Alarm Severity. Value is {@value}. */
    public static final String C_PARAM_SEVERITY = "_AlarmSeverity";

    /** Parameter name of Alarm Raised Time Millis. Value is {@value}. */
    public static final String C_PARAM_RAISED_TIME_MILLIS = "_AlarmRaisedTimeMillis";

    /** Parameter name of Alarm Raised Time. Value is {@value}. */
    public static final String C_PARAM_RAISED_TIME = "_AlarmRaisedTime";

    /** Parameter name of Alarm Originating Node Name. Value is {@value}. */
    public static final String C_PARAM_ORIGINATING_NODE_NAME = "_OriginatingNodeName";

    /** Parameter name of Alarm Originating Process. Value is {@value}. */
    public static final String C_PARAM_ORIGINATING_PROCESS_NAME = "_OriginatingProcessName";
    
    /**
     * Parameter name of Process Name. For example process name the alarm refers to
     * which is not necessarily the originating process - e.g. watchdogs raises
     * alarms about other processes, the other procee name would be held in this
     * parameter.
     * Value is {@value}.
     */
    public static final String C_PARAM_PROCESS_NAME = "_ProcessName";

} // End of public interface AlarmParameterConstants

////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////
