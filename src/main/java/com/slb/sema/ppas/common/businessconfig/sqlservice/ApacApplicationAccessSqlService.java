////////////////////////////////////////////////////////////////////////////////
//        ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       ApacApplicationAccessSqlService
//    DATE            :       03-October-2003
//    AUTHOR          :       Marek Vonka
//    REFERENCE       :       PpaLon#964/4107
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Service to read and update the APAC_APPLICATION_ACCESS
//                            table and return a ApacApplicationAccessDataSet
//                            object.
//
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ApacApplicationAccessDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Service to read the APAC_APPLICATION_ACCESS table and return a
 * ApplicationAccessDataSet object.
 */
public class ApacApplicationAccessSqlService
{
    
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */    
    private static final String C_CLASS_NAME = "ApacApplicationAccessSqlService";

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------    
    
    /** The Logger object to be used in logging any exceptions arising from the
     *  use of this service.
     */
    private Logger             i_logger = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new application access sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public ApacApplicationAccessSqlService(PpasRequest p_request,
                              Logger      p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Fetches Application Access Profiles from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of Application Access Profile objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public ApacApplicationAccessDataSet readAll(PpasRequest            p_request,
                                                JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement                l_statement;
        JdbcResultSet                l_resultSet;
        SqlString                    l_sql;
        ApacApplicationAccessData    l_apacData;
        ApacApplicationAccessDataSet l_apacDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            11000,
                            this,
                            "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(1000, 0, "SELECT apac_privilege_id," +
                                       "apac_batch_flag," +
                                       "apac_bus_config_flag," +
                                       "apac_purge_arc_flag," +
                                       "apac_reports_flag," +
                                       "apac_opid," +
                                       "apac_gen_ymdhms " +
                                       "FROM apac_application_access");

        l_apacDataSet = new ApacApplicationAccessDataSet(p_request, 20, 10);

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 11010, this, p_request);

        l_resultSet = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 11020, this, p_request, l_sql);

        while (l_resultSet.next(11030))
        {
            // Create a new Application Access Profile object...
            l_apacData = new ApacApplicationAccessData
                                 (p_request,
                                  l_resultSet.getInt(11040, "apac_privilege_id"),
                                  l_resultSet.getChar(11050, "apac_batch_flag"),
                                  l_resultSet.getChar(11060, "apac_bus_config_flag"),
                                  l_resultSet.getChar(11070, "apac_purge_arc_flag"),
                                  l_resultSet.getChar(11070, "apac_reports_flag"),
                                  l_resultSet.getTrimString(11280, "apac_opid"), 
                                  l_resultSet.getDateTime(11290, "apac_gen_ymdhms"));

            // ...and stuff it in the misc code data set.
            l_apacDataSet.addElement(l_apacData);
        }

        l_resultSet.close(11300);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 11310, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            11320,
                            this,
                            "Leaving " + C_METHOD_readAll);
        }

        return(l_apacDataSet);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Update APAC_APPLICATION_ACCESS.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_privilegeId Operator privilege identifier.
     * @param p_batchApplication Batch application flag.
     * @param p_businessConfigurationApplication Business Configuration application flag.
     * @param p_purgeAndArchiveApplication Purge & Archive application flag.
     * @param p_reportsApplication Reports application flag.
     * @param p_opid Operator identifier.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       int                    p_privilegeId,
                       char                   p_batchApplication,
                       char                   p_businessConfigurationApplication,
                       char                   p_purgeAndArchiveApplication,
                       char                   p_reportsApplication,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDate         l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateToday();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME,
                            12000,
                            this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = new String("UPDATE apac_application_access " +
                           "SET apac_batch_flag = '" + p_batchApplication + "'," +
                           "    apac_bus_config_flag = '" + p_businessConfigurationApplication + "'," +
                           "    apac_purge_arc_flag = '" + p_purgeAndArchiveApplication + "'," +
                           "    apac_reports_flag = '" + p_reportsApplication + "'," +
                           "    apac_opid = '" + p_opid + "'," +
                           "    apac_gen_ymdhms = '" + l_now + "' " +
                           "WHERE apac_privilege_id = " + p_privilegeId);


        l_sqlString = new SqlString(500, 0, l_sql);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       12050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   12500,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 12250, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in apac_application_access table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            12300,
                            this,
                            "Leaving " + C_METHOD_update);
        }
        return;
    }
    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a row into the table APAC_APPLICATION_ACCESS
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_privilegeId Operator privilege identifier.
     * @param p_batchFlag Batch application flag.
     * @param p_busConfigFlag Business Configuration application flag.
     * @param p_purgeArcFlag Purge & Archive application flag.
     * @param p_reportsFlag Reports application flag.
     * @param p_opid The username of the operator who has done this update.
     * @param p_delFlag Deletion flag.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_privilegeId,
                       char                   p_batchFlag,
                       char                   p_busConfigFlag,
                       char                   p_purgeArcFlag,
                       char                   p_reportsFlag,
                       char                   p_delFlag,
                       String                 p_opid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDate         l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;        
        
        l_now = DatePatch.getDateToday();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME,
                            12350,
                            this,
                            "Entered " + C_METHOD_insert);
        }

        l_sql = new String("INSERT INTO apac_application_access" +
                           "(apac_privilege_id, apac_batch_flag, apac_bus_config_flag, " +
                           "apac_purge_arc_flag, apac_reports_flag, apac_opid, " +
                           "apac_gen_ymdhms, apac_del_flag) " +
                           "VALUES(" + 
                           p_privilegeId + ", '" +
                           p_batchFlag + "', '" +
                           p_busConfigFlag + "', '" +
                           p_purgeArcFlag + "', '" +
                           p_reportsFlag + "', '" +
                           p_opid + "', '" +
                           l_now + "', '" +
                           p_delFlag + "')");

        l_sqlString = new SqlString(500, 0, l_sql);

        try
        {                                                                                                
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       12400,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   12450,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 12600, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in apac_application_access table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            12650,
                            this,
                            "Leaving " + C_METHOD_insert);
        }
    }
}

