////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScapServClassAdjDataSet.java
//      DATE            :       3-Jun-2004
//      AUTHOR          :       Bruno Ferrand-Broussy
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Holds a set of ScapServClassAdjData objects
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 22/12/05 | Lars. L.   | One new method is added:        | PpacLon#1755/7585
//          |            | getScapServClassAdjDataArr(..)  |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Object containing a set of <code>ScapServClassAdjDataSet</code> objects.
 */
public class ScapServClassAdjDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Constant used for calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScapServClassAdjDataSet";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Array of <code>ScapServClassAdjData</code>. */
    private ScapServClassAdjData[] i_scapAllARR = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs instance of <code>ScapServClassAdjDataSet</code>.
     * @param p_scapARR an array of <code>ScapServClassAdjData</code> objects
     */
    public ScapServClassAdjDataSet(ScapServClassAdjData[] p_scapARR)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_scapAllARR = p_scapARR;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------e
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Gets the <code>ScapServClassAdjData</code> object that matches the adjustment 
     * code and type. 
     * @param p_adjCode The adjustment code.
     * @param p_adjType The adjustment type. 
     * @return instance of <code>ScapServClassAdjData</code> if one can be found that
     *         matches the supplied adjustment type and code, else <code>null</code> 
     */
    public ScapServClassAdjData getScapServClassAdjData(String p_adjCode, String p_adjType)
    {
        ScapServClassAdjData l_scapData = null;
        
        for (int i = 0; (l_scapData == null) && (i < i_scapAllARR.length); i++)
        {
            if ((i_scapAllARR[i].getAdjCode().equals(p_adjCode)) &&
                i_scapAllARR[i].getAdjType().equals(p_adjType))
            {
                l_scapData = i_scapAllARR[i];
            }
        }
        return l_scapData;
    }
    
    /***************************************************************************
     * Returns an array of <code>ScapServClassAdjData</code> objects that matches
     * the passed adjustment type.
     * 
     * @param p_adjType The adjustment type.
     *  
     * @return  an array of <code>ScapServClassAdjData</code> objects that matches
     *          the passed adjustment type. 
     */
    public ScapServClassAdjData[] getScapServClassAdjDataArr(String p_adjType)
    {
        ScapServClassAdjData[] l_scapDataArr = null;
        Vector                 l_scapDataVec = new Vector();
        
        for (int l_ix = 0; l_ix < i_scapAllARR.length; l_ix++)
        {
            if (i_scapAllARR[l_ix].getAdjType().equals(p_adjType))
            {
                l_scapDataVec.add(i_scapAllARR[l_ix]);
            }
        }

        l_scapDataArr = new ScapServClassAdjData[l_scapDataVec.size()];
        l_scapDataVec.toArray(l_scapDataArr);

        return l_scapDataArr;
    }
    
    /** Allows to know if we need  store the value in cust adjusts.
     * If there is no entry in the database we write the Adjustment.
     * @param p_adjCode The adjustment code.
     * @param p_adjType The adjustment type. 
     * @return TRUE if we need to store the value in cust adjusts.
     */
    public boolean getWriteIndicator(String p_adjCode, String p_adjType)
    {
        boolean l_writeIndicator = true; 
        ScapServClassAdjData l_scapData = null;
        
        l_scapData = getScapServClassAdjData(p_adjCode, p_adjType);
        if (l_scapData != null )
        {
            l_writeIndicator = l_scapData.isWriteIndicator();
        }
        return l_writeIndicator;
    }
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("AdjustmentConfiguration", i_scapAllARR, p_sb);
    }
}

