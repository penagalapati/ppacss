////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionCurrentBalance.java
//      DATE            :       08-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              current balance.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;

/** Identify whether a customer qualifies for churn based on current balance. */
public class ChurnConditionCurrentBalance extends ChurnCondition
{
    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C3 - Current Balance. ";
    
    /** Low Balance Threshold. */
    private Money i_lowBalanceThreshhold;
    
    /** Constructor for a Churn condition based on current balance.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     * @throws PpasConfigException if properties are incorrect.
     */
    public ChurnConditionCurrentBalance(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
        throws PpasConfigException
    {
        super(p_properties, p_bcCache, p_analyser);

        i_lowBalanceThreshhold = getMoneyProperty(C_PROP_PREFIX + "lowBalanceThreshold", "0");
    }
        
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason)
    {
        Money l_balance  = p_data.getCurrentBalance();
        
        p_reason.append(C_CONDITION_NAME);

        if (l_balance == null)
        {
            p_reason.append("Current Balance not available assume okay. " + C_NO_CHURN_TEXT);
            return false;
        }
        
        // Note: Both amounts are positive/zero.
        Money l_difference = getDifference(l_balance, i_lowBalanceThreshhold);
        
        p_reason.append("Current Balance (");
        p_reason.append(formatMoney(l_balance));
        p_reason.append(") ");
        p_reason.append(l_difference.isNegative() ? "<" : l_difference.isZero() ? "=" : ">");
        p_reason.append(" threshold (");
        p_reason.append(formatMoney(i_lowBalanceThreshhold));
        p_reason.append("). ");
        
        boolean l_result = l_difference.isNegative();
        p_reason.append(l_result ? C_CHURN_TEXT : C_NO_CHURN_TEXT);
        
        return l_result;
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if Current Balance < Threshold (" + formatMoney(i_lowBalanceThreshhold) + ")";
    }
}
