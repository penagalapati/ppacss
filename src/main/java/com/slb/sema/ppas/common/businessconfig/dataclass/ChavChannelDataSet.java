//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       ChavChannelDataSet.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of channel data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 22/12/06 | M.T�rnqvist| Added method getAvailableArray  | PpacLon#2822/10710
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Object containing a set of <code>ChavChannelData</code> objects.
 */
public class ChavChannelDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChavChannelDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of channel records. That is, the data of this DataSet object. */
    private ChavChannelData[] i_allRecords;
    
    /** Array of available channel data records. That is, the data of this DataSet object. */    
    private ChavChannelData[] i_availableRecords;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Channel data.
     * @param p_request The request to process.
     * @param p_data Array holding the  channel data records to be stored by this object.
     */
    public ChavChannelDataSet(PpasRequest             p_request,
                              ChavChannelData[]  p_data)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_allRecords = p_data;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns Channel data associated with a specified case.
     * 
     * @param p_schemeId The channel.
     * @return ChavChannelData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public ChavChannelData getRecord(String p_schemeId)
    {
        ChavChannelData l_record = null;
        
        for (int i = 0; i < i_allRecords.length; i++)
        {
            if ( i_allRecords[i].getChannelId().equals(p_schemeId))                
            {
                l_record = i_allRecords[i];
                break;
            }
        }

        return l_record;
    }    
    
    /**
     * Returns available (that is, not marked as deleted) Channel records associated 
     * with a specified case.
     * 
     * @param p_schemeId The channel.
     * @return ChavChannelData record from this DataSet with the given case. If
     *         no record can be found, then <code>null</code> is returned.
     */
    /*public ChavChannelData getAvailableRecord (String p_schemeId)
    {
        ChavChannelData l_chavChannelRecord = getRecord(p_schemeId);
        
        if (l_chavChannelRecord != null && l_chavChannelRecord.isDeleted())
        {
            l_chavChannelRecord = null;
        }

        return l_chavChannelRecord;
    }*/
    
    /** Returns array of all records in this data set object.
     * 
     * @return Array of all data records.
     */
    public ChavChannelData[] getAllArray()
    {
        return i_allRecords;
    }    
    
    /**
     * Get a data set containing the all available  records. That is, those not marked as deleted.
     * 
     * @return Data set containing available records.
     */
    public ChavChannelData[] getAvailableArray()
    {
        if (i_availableRecords == null)
        {
            Vector l_available = new Vector();
            
            for (int i = 0; i < i_allRecords.length; i++)
            {
                if (!i_allRecords[i].isDeleted())
                {
                    l_available.addElement(i_allRecords[i]);
                }
            }
    
            i_availableRecords =
                  (ChavChannelData[])l_available.toArray(new ChavChannelData[l_available.size()]);
        }

        return i_availableRecords;
    }
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_allRecords, p_sb);
    }
}
