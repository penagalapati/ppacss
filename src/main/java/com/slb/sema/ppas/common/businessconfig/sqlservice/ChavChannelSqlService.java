//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       ChavChannelSqlService.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for channel data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 22/12/06 | M.T�rnqvist| Added "," in SQL code           | PpacLon#2822/10659
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelData;
import com.slb.sema.ppas.common.businessconfig.dataclass.ChavChannelDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** SQL service for accessing Tele Service codes. */
public class ChavChannelSqlService 
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChavChannelSqlService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new F&F Channels sql service.
     * @param p_request The request to process.
     */
    public ChavChannelSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */ 
    private static final String C_METHOD_readAll = "readAll";

    /**
     * Reads and returns all valid Channels (including ones marked as deleted).
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Tele Services.
     * @throws PpasSqlException If the data cannot be read.
     */
    public ChavChannelDataSet readAll(PpasRequest p_request, JdbcConnection p_connection)
            throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        SqlString l_sql;

        String l_channelId;
        String l_channelDesc;
        String l_channelGroup;
        char   l_chavDelFlag;

        String l_channelOpid;
        PpasDateTime l_channelGenYmdhms;

        ChavChannelData l_ChavChannelData;
        Vector l_allChavChannelDataV = new Vector(20, 10);
        ChavChannelDataSet l_chavChannelDataSet;
        ChavChannelData l_allChavARR[];
        ChavChannelData l_chavChannelEmptyARR[] = new ChavChannelData[0];
        JdbcResultSet l_results;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(500, 0, "SELECT " + "chav_channel_id, " 
                              + "chav_description, "
                              + "chav_gen_ymdhms, "
                              + "chav_opid, "
                              + "chav_del_flag, "
                              + "chav_channel_group " 
                              + "FROM chav_channel_values");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while (l_results.next(10140))
        {
            l_channelId = l_results.getTrimString(10150, "chav_channel_id");
            l_channelDesc = l_results.getTrimString(10160, "chav_description");            
            l_channelGenYmdhms = l_results.getDateTime(10170, "chav_gen_ymdhms");
            l_chavDelFlag = l_results.getChar(10180, "chav_del_flag");
            l_channelGroup = l_results.getTrimString(10210, "chav_channel_group");
            l_channelOpid = l_results.getTrimString(10200, "chav_opid");
            
            l_ChavChannelData = new ChavChannelData(l_channelId,
                                                    l_channelDesc,
                                                    l_channelGroup,
                                                    l_channelOpid,
                                                    l_channelGenYmdhms,
                                                    l_chavDelFlag == '*');

            l_allChavChannelDataV.addElement(l_ChavChannelData);
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10240, this, p_request);

        l_allChavARR = (ChavChannelData[])l_allChavChannelDataV.toArray(l_chavChannelEmptyARR);

        l_chavChannelDataSet = new ChavChannelDataSet(p_request, l_allChavARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            10250,
                            this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_chavChannelDataSet;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";

    /**
     * Attempts to insert a new row into the chav_channel_values database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_channelId The Channel.
     * @param p_channelDescription The channel description
     * @param p_channelGroup The channel Group
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_channelId,
                      String p_channelDescription,
                      String p_channelGroup,
                      String p_opid) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " + C_METHOD_insert);
        }

        l_sql = "insert into chav_channel_values " + 
                "(chav_channel_id, chav_description, chav_del_flag," +
                " chav_channel_group, chav_opid, chav_gen_ymdhms) " +
                " VALUES( {0}, {1},' ', {2}, {3},{4})";

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_channelId);
        l_sqlString.setStringParam(1, p_channelDescription);
        l_sqlString.setStringParam(2, p_channelGroup);
        l_sqlString.setStringParam(3, p_opid);
        l_sqlString.setDateTimeParam(4, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 16050, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10040,
                                this,
                                "Database error: unable to insert row in chav_channel_values table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";

    /**
     * Attempts to update a new row into the chav_channel_values database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_channelId The Channel.
     * @param p_channelGroup The Channel Group.
     * @param p_channelDescription The channel description
     * @return A count of the number of rows updated
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_opid,
                      String p_channelId,
                      String p_channelGroup,
                      String p_channelDescription) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = new String("update chav_channel_values " + "set chav_description = {0}, " 
                           + "chav_channel_group = {1}, chav_opid = {2}, chav_gen_ymdhms = {3},"
                           + "chav_del_flag = ' ' where chav_channel_id = {4}");

        l_sqlString = new SqlString(150, 5, l_sql);

        l_sqlString.setStringParam(0, p_channelDescription);
        l_sqlString.setStringParam(1, p_channelGroup);
        l_sqlString.setStringParam(2, p_opid);
        l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(4, p_channelId);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10140,
                                this,
                                "Database error: unable to update row in chav_channel_values table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;
    }

    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";

    /**
     * Mark a row as having been deleted in chav_channel_values.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_channel The Channels to be withdrawn.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest p_request,
                       JdbcConnection p_connection,
                       String p_opid,
                       String p_channel) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            32100,
                            this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE chav_channel_values " + "SET CHAV_DEL_FLAG = '*', "
                + "CHAV_OPID = {1}, " + "CHAV_GEN_YMDHMS = {2} " + "WHERE CHAV_CHANNEL_ID = {0}");

        l_sqlString = new SqlString(150, 3, l_sql);

        l_sqlString.setStringParam(0, p_channel);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete, 32300, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug
                        .print(PpasDebug.C_LVL_VHIGH,
                               PpasDebug.C_APP_SERVICE,
                               PpasDebug.C_ST_ERROR,
                               C_CLASS_NAME,
                               10240,
                               this,
                               "Database error: unable to mark row as deleted in channel table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_delete,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            32700,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";

    /**
     * Marks a withdrawn Channels record as available in the channel database
     * table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_channel Channel identifier.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest p_request,
                                JdbcConnection p_connection,
                                String p_channel,
                                String p_userOpid) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        PpasDateTime l_now;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10300,
                            this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE chav_channel_values " + "SET CHAV_DEL_FLAG = {0}, " + "CHAV_OPID = {2}, "
                + "CHAV_GEN_YMDHMS = {3} " + "WHERE CHAV_CHANNEL_ID = {1}";

        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setCharParam(0, ' ');
        l_sqlString.setStringParam(1, p_channel);
        l_sqlString.setStringParam(2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       10310,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   10320,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10340,
                                this,
                                "Database error: unable to update row in channel table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_markAsAvailable,
                                              10350,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10360,
                            this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}