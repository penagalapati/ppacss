////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FachFafChargingIndSqlService.java
//      DATE            :       15-Mar-2004
//      AUTHOR          :       MAGray - q92528d
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       SQL service for accessing F&F Charging Indicators codes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | Copyright info changed.         | PpacLon#1755/7425
//----------+------------+---------------------------------+--------------------
// 15/11/05 | Yang L.    | Four new methods are added:     | PpacLon#1755/7425
//          |            | * insert(...)                   |
//          |            | * update(...)                   |
//          |            | * delete(...)                   |
//          |            | * markAsAvailable(...)          |
//----------+------------+---------------------------------+--------------------
// 24/11/05 | Yang L.    | Review comments correction      | PpacLon#1755/7488
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.FachFafChargingIndDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** SQL service for accessing Tele Service codes. */
public class FachFafChargingIndSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FachFafChargingIndSqlService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new F&F Charging Indicators sql service.
     * 
     * @param p_request The request to process.
     */
    public FachFafChargingIndSqlService(
        PpasRequest            p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid Charging Indicators (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Tele Services.
     * @throws PpasSqlException If the data cannot be read.
     */
    public FachFafChargingIndDataSet readAll(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement             l_statement = null;
        SqlString                 l_sql;
        
        int                       l_fachChargingInd = 0;
        String                    l_fachChargingIndDesc = "";
        char                      l_fachDelFlag;
        String                    l_fachOpid;
        PpasDateTime              l_fachGenYmdhms;

        FachFafChargingIndData    l_FachFafChargingIndData;
        Vector                    l_allFachFafChargingIndDataV = new Vector(20, 10);
        FachFafChargingIndDataSet l_fachFafChargingIndDataSet;
        FachFafChargingIndData    l_allFachFafChargingIndARR[];
        FachFafChargingIndData    l_fachFafChargingIndEmptyARR[] = new FachFafChargingIndData[0];
        JdbcResultSet             l_results;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered " +  C_METHOD_readAll);
        }
        
        l_sql =   new SqlString(500, 0, "SELECT " +
                      "fach_charging_ind, " +
                      "fach_charging_ind_desc, " +
                      "fach_del_flag, " +
                      "fach_opid, " +
                      "fach_gen_ymdhms " +
                      "FROM fach_faf_charging_ind");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 10110, this,
                "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while ( l_results.next(10140) )
        {
            l_fachChargingInd      = l_results.getInt       (10150, "fach_charging_ind");
            l_fachChargingIndDesc  = l_results.getTrimString(10160, "fach_charging_ind_desc");
            l_fachDelFlag          = l_results.getChar      (10190, "fach_del_flag");
            l_fachOpid             = l_results.getTrimString(10200, "fach_opid");
            l_fachGenYmdhms        = l_results.getDateTime  (10210, "fach_gen_ymdhms");

            l_FachFafChargingIndData = new FachFafChargingIndData(
                    "" + l_fachChargingInd,
                    l_fachChargingIndDesc,
                    l_fachDelFlag == '*',
                    l_fachOpid,
                    l_fachGenYmdhms);

            l_allFachFafChargingIndDataV.addElement(l_FachFafChargingIndData);
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10240, this, p_request);

        l_allFachFafChargingIndARR =
                (FachFafChargingIndData[])l_allFachFafChargingIndDataV.toArray(
                     l_fachFafChargingIndEmptyARR);

        l_fachFafChargingIndDataSet = new FachFafChargingIndDataSet(l_allFachFafChargingIndARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10250, this,
                "Leaving " + C_METHOD_readAll);
        }

        return l_fachFafChargingIndDataSet;
    }
        
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Attempts to insert a new row into the FACH_FAF_CHARGING_IND database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator ID.
     * @param p_fach_charging_Ind The faf charging indicator.
     * @param p_fafchargingIndDescription The Faf charging Indicators description 
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest            p_request,
                      JdbcConnection         p_connection,
                      String                 p_opid,
                      int                    p_fach_charging_Ind,
                      String                 p_fafchargingIndDescription)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;       
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " +  C_METHOD_insert);
        }
            
        l_sql = "INSERT INTO fach_faf_charging_ind " +
                "(FACH_CHARGING_IND," +
                " FACH_CHARGING_IND_DESC," +
                " FACH_DEL_FLAG," +
                " FACH_OPID, " +
                " FACH_GEN_YMDHMS)" +
                "VALUES( {0}, {1}, ' ', {2}, {3})";
                       
        l_sqlString = new SqlString(150, 4, l_sql);
           
        l_sqlString.setIntParam(0, p_fach_charging_Ind);
        l_sqlString.setStringParam(1, p_fafchargingIndDescription);
        l_sqlString.setStringParam(2, p_opid);
        l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());

        try
        {                   
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insert,
                                                       16050,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }        
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in FACH_FAF_CHARGING_IND table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

           
            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;     
    }  
    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a new row into the FACH_FAF_CHARGING_IND database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_fach_charging_Ind The faf charging indicator.
     * @param p_fafchargingIndDescription The Faf charging Indicators description 
     * @return A count of the number of rows updated
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest            p_request,
                      JdbcConnection         p_connection,
                      String                 p_opid,
                      int                    p_fach_charging_Ind,
                      String                 p_fafchargingIndDescription)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " +  C_METHOD_update);
        }
    
        l_sql = new String("UPDATE fach_faf_charging_ind " +
                           "SET FACH_CHARGING_IND_DESC = {0}, " +
                           "FACH_DEL_FLAG = ' ', " +
                           "FACH_OPID = {1}, " +
                           "FACH_GEN_YMDHMS = {2} " +
                           "WHERE FACH_CHARGING_IND = {3}");
                       
        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_fafchargingIndDescription);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());
        l_sqlString.setIntParam(3, p_fach_charging_Ind);

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in FACH_FAF_CHARGING_IND table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;     
    }

    
    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Mark a row as having been deleted in FACH_FAF_CHARGING_IND.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_fach_charging_Ind The faf charging indicators to be withdrawn.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest     p_request,
                       JdbcConnection  p_connection,
                       String          p_opid,
                       int             p_fach_charging_Ind)
        throws PpasSqlException
    {
        JdbcStatement    l_statement  = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            32100, 
                            this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE FACH_FAF_CHARGING_IND " +
                           "SET FACH_DEL_FLAG = '*', " +
                           "FACH_OPID = {1}, " +
                           "FACH_GEN_YMDHMS = {2} " +
                           "WHERE FACH_CHARGING_IND = {0}");

        l_sqlString = new SqlString(150, 3, l_sql);
            
        l_sqlString.setIntParam(0, p_fach_charging_Ind);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_delete,
                                                       32300,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }            
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10240, 
                                this,
                                "Database error: unable to mark row as deleted in fach_faf_charging_ind table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_delete,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            32700,
                            this,
                            "Leaving " + C_METHOD_delete);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn faf charging indicators record as available in the 
     * fach_faf_charging_ind database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_fafChargingInd faf charging indicator identifier.
     * @param p_userOpid Operator updating the business config record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest     p_request,
                                JdbcConnection  p_connection,
                                int             p_fafChargingInd,
                                String          p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10300, 
                this,
                "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE fach_faf_charging_ind " +
                "SET FACH_DEL_FLAG = {0}, " +
                    "FACH_OPID = {2}, " +
                    "FACH_GEN_YMDHMS = {3} " +
                "WHERE FACH_CHARGING_IND = {1}";

        l_sqlString = new SqlString(500, 4, l_sql);
        
        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setIntParam     (1, p_fafChargingInd);
        l_sqlString.setStringParam  (2, p_userOpid);
        l_sqlString.setDateTimeParam(3, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10340, 
                    this,
                    "Database error: unable to update row in fach_faf_charging_ind table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10360, 
                this,
                "Leaving " + C_METHOD_markAsAvailable);
        }
    }    
}