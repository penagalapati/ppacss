////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AsceFailureReasonData.java
//      DATE            :       10-May-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;

/**
 * Class representing a row from the asce_ascs_error_codes table.
 */
public class AsceFailureReasonData extends DataObject
{
    //-------------------------------------------------------------------------
    // Private attributes
    //-------------------------------------------------------------------------
    /** The failure code. */
    private int          i_failureCode;
    
    /** The failure reason description. */
    private String       i_failureReason;
    
    /** Is this record marked as deleted. */
    private boolean      i_deleted = false;
    
    /** The opid that last updated this record. */
    private String       i_opid;
    
    /** Date/time of the last update. */
    private PpasDateTime i_genYmdhms;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Construct an instance of <code>AsceFailureReasonData</code>.
     * @param p_failureCode the failure reason code
     * @param p_failureReason the failure reason description
     * @param p_deleted is this record marked as deleted
     * @param p_opid the opid that last updated this record
     * @param p_genYmdhms the date/time of the last update
     */
    public AsceFailureReasonData(int          p_failureCode,
                                 String       p_failureReason,
                                 boolean      p_deleted,
                                 String       p_opid,
                                 PpasDateTime p_genYmdhms)
    {
        i_failureCode = p_failureCode;
        i_failureReason = p_failureReason;
        i_deleted = p_deleted;
        i_opid = p_opid;
        i_genYmdhms = p_genYmdhms;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Get the failure code.
     * @return the failure code
     */
    public int getFailureCode()
    {
        return i_failureCode;
    }

    /**
     * The failure reason description.
     * @return the failure reason description
     */
    public String getFailureReason()
    {
        return i_failureReason;
    }
    
    /**
     * Is this record marked as deleted.
     * @return <code>true</code> if the record is marked as deleted, else <code>false</code>
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }

}
