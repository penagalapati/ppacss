////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutBatchRecordData.java
//  DATE            :       02 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE   | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This <code>AutoMsisdnRoutBatchRecordData</code> class encapsulates all data needed
 * for Automated MSISDN Routing Deletion.
 */
public class AutoMsisdnRoutBatchRecordData extends BatchRecordData
{
    //--------------------------------------------------------------------------
    //  Constants. --
    //--------------------------------------------------------------------------

    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AutoMsisdnRoutBatchRecordData";

    //--------------------------------------------------------------------------
    // Instance variables. --
    //--------------------------------------------------------------------------

    /** MSISDN. Mandatory parameter. */
    private String              i_msisdn     = null;

    //--------------------------------------------------------------------------
    // Constructors. --
    //--------------------------------------------------------------------------

    /**
     * Constructs a <code>MsisdnRoutingBatchRecordData</code> instance.
     * @param p_msisdn an MSISDN
     */

    public AutoMsisdnRoutBatchRecordData(String p_msisdn)
    {
        super();
        
        i_msisdn = p_msisdn;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            C_CONSTRUCTING);
        }

        // Nothing to do here..

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            C_CONSTRUCTED);
        }
    } // End of constructor

    //--------------------------------------------------------------------------
    // Public methods. --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_dumpRecord = "dumpRecord";

    /**
     * @return the record content in a readable format.
     */
    public String dumpRecord()
    {
        StringBuffer l_tmp = new StringBuffer();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10030,
                            this,
                            C_ENTERING + C_METHOD_dumpRecord);
        }

        // Get the super class data
        l_tmp.append(C_DELIMITER);
        l_tmp.append("*** Class name = ");
        l_tmp.append(C_CLASS_NAME);
        l_tmp.append(" ***");
        l_tmp.append(C_DELIMITER);

        l_tmp.append(super.getDumpRecord());

        // This class's data
        l_tmp.append("MSISDN = ");
        if (i_msisdn != null)
        {
            l_tmp.append(this.i_msisdn);
        }
        else
        {
            l_tmp.append("'null'");
        }
        l_tmp.append(C_DELIMITER);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10040,
                            this,
                            C_LEAVING + C_METHOD_dumpRecord);
        }

        return l_tmp.toString();
    }

    /**
     * @return Returns the msisdn.
     */

    public String getMsisdn()
    {
      return i_msisdn;
    }

    /**
     * @param p_msisdn The msisdn to set.
     */
    public void setMsisdn(String p_msisdn)
    {
        i_msisdn = p_msisdn;
    }
}