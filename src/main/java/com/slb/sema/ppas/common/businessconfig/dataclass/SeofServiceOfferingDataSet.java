////////////////////////////////////////////////////////////////////////////////
//     ASCS IPR ID      :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SeofServiceOfferingDataSet.java
//      DATE            :       11-Jun-2004
//      AUTHOR          :       Rob O'Brien
//      REFERENCE       :       PRD_ASCS00_GEN_CA_016
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Collection of service offering data objects.
//                              
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Collection of service offering data objects.
 */
public class SeofServiceOfferingDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Used for calls to middleware. */
    private static final String C_CLASS_NAME = "SeofServiceOfferingDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**
     * Collection of service offering data.
     */
    private SeofServiceOfferingData [] i_serviceOfferingDataArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of service offering data.
     * 
     * @param p_request The request to process.
     * @param p_serviceOfferingDataArr Array holding the service offering data records to be stored
     *                              by this object.
     */
    public SeofServiceOfferingDataSet(PpasRequest                p_request,
                                      SeofServiceOfferingData [] p_serviceOfferingDataArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }
        
        i_serviceOfferingDataArr = p_serviceOfferingDataArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /**
     * Returns service offering data associated with a specified service offering number.
     * @param p_seofNumber The service offering number.
     * @return SeofServiceOfferingData The recored associated with the requested service
     *         offering number.
     *         If no record can be found, then <code>null</code> is returned.
     */
    public SeofServiceOfferingData getRecord (int p_seofNumber)
    {
        SeofServiceOfferingData l_serviceOfferingRecord = null;
       
        for (int l_loop = 0; l_loop < i_serviceOfferingDataArr.length; l_loop++)
        {
            if ( i_serviceOfferingDataArr[l_loop].getServiceOfferingNumber() == p_seofNumber)                
            {
                l_serviceOfferingRecord = i_serviceOfferingDataArr[l_loop];
            }
        }

        return (l_serviceOfferingRecord);

    } // End of public method getRecord
    
    /**
     * Returns array of all records in this data set object.
     * @return All Service Offerings.
     */
    public SeofServiceOfferingData [] getAllArray()
    {
        return (i_serviceOfferingDataArr);

    } // end method getAllArray

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("AllServiceOffering", i_serviceOfferingDataArr, p_sb);
    }
}

