////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnAnalysis.java
//      DATE            :       09-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Churn Analysis Status.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.dataclass.DataObject;

/** Chun Analysis status. */
public class ChurnAnalysis extends DataObject
{
    /** Flag indicating whether this customer is likely to Churn. */
    private boolean i_likelyToChurn;
    
    /** Descriptive reason of why the analysis suggests they are likely to Churn. */
    private String i_reason;
    
    /** Standard constructor.
     * 
     * @param p_likelyToChurn True if this customer is likely to churn.
     * @param p_reason        Reason for the lieklihood of Churn.
     */
    public ChurnAnalysis(boolean p_likelyToChurn, String p_reason)
    {
        i_likelyToChurn = p_likelyToChurn;
        i_reason        = p_reason;
    }

    /** Is the customer likely to Churn ?
     * 
     * @return True if this customer is likely to Churn.
     */
    public boolean isLikelyToChurn()
    {
        return i_likelyToChurn;
    }
    
    /** Get the reason the analysis suggests Churn is likely.
     * 
     * @return Descriptive reason of why the customer is likely to Churn.
     */
    public String getReason()
    {
        return i_reason;
    }
}
