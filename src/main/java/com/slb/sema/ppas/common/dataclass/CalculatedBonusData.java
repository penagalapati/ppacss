//////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CalculatedBonusData.java
//      DATE            :       28-Dec-2006
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS_GEN_CA_104
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Details of a calculated bonus.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/07/07 | SJ Vonka   | Allow for wildcard selection    | PpacLon#3184/11796
//          |            |                                 | PRD_ASCS00_GEN_CA_120
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;

/** Details of a calculated bonus. */
public class CalculatedBonusData extends DataObject
{
    /** Details of the bonus criteria. */
    private BoneBonusElementData i_bone;
    
    /** Bonus amount. */
    private Money                i_amount;
    
    /** Expiry date of a dedicated account or null if main account. */
    private PpasDate             i_expiryDate;
    
    /** Number of wildcards in the key elements. */
    private int                  i_wildCount = -1;
    
    /** Standard constructor.
     * 
     * @param p_bone       Details of the bonus criteria.
     * @param p_amount     Amount of the bonus.
     * @param p_expiryDate Expiry date of dedicated account or null if the main account.
     */
    public CalculatedBonusData(BoneBonusElementData p_bone, Money p_amount, PpasDate p_expiryDate)
    {
        i_bone       = p_bone;
        i_amount     = p_amount;
        i_expiryDate = p_expiryDate;
    }
    
    /** Get details of the Bonus criteria.
     * 
     * @return Bonus criteria.
     */
    public BoneBonusElementData getBonusCriteria()
    {
        return i_bone;
    }
    
    /** Get amount of the bonus.
     * 
     * @return Bonus amount.
     */
    public Money getBonusAmount()
    {
        return i_amount;
    }
    
    /** Get the expiry date of the dedicated account.
     * 
     * @return Expiry date of dedicated account.
     */
    public PpasDate getExpiryDate()
    {
        return i_expiryDate;
    }
    
    /** Get the number of wildcards held within the key elements.
     * 
     * @return number of wildcards in key elements.
     */
    public int getNumberOfWildcards()
    {
        int     l_wildIndex     = -1;
        int     l_wildCount     = 0;
        String  l_keyElements   = i_bone.getKeyElements();

        if (i_wildCount != -1)
        {
            return i_wildCount;
        }

        do
        {
            l_wildIndex = l_keyElements.indexOf('*', l_wildIndex+1);

            if (l_wildIndex >= 0)
            {
                l_wildCount++;
            }

        }while (l_wildIndex != -1);

        i_wildCount = l_wildCount;
        
        return l_wildCount;
    }
   
    
}
