////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaFieldsDataSet.Java
//      DATE            :       13-Apr-2004
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a Comment (from srva_fields).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of Comment data, each element of
 * the set representing a record from the srva_fields table.
 */
public class SrvaFieldsDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaFieldsDataSet";

    /** Global market that agts as a default for all comments. */
    private static final Market C_GLOBAL_MARKET = new Market(0, 0, "Global Market");
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of Comment data records. That is, the data of this DataSet object. */
    private SrvaFieldsData []     i_commentArray = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of Comment data.
     * 
     * @param p_request The request to process.
     * @param p_commentArr Array holding the Comment data records to be stored by this object.
     */
    public SrvaFieldsDataSet(
        PpasRequest             p_request,
        SrvaFieldsData []       p_commentArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_commentArray = p_commentArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns the available Comment records. That is, those not marked as deleted.
     * @return Array containing all available Comments.
     */
    public SrvaFieldsData [] getAvailableArray()
    {
        Vector         l_availableV = new Vector();
        int            l_loop;
        SrvaFieldsData l_emptyArray[] = new SrvaFieldsData[0];

        for (l_loop = 0; l_loop < i_commentArray.length; l_loop++)
        {
            if (!i_commentArray[l_loop].isDeleted())
            {
                l_availableV.addElement(i_commentArray[l_loop]);
            }
        }

        return (SrvaFieldsData[])l_availableV.toArray(l_emptyArray);
    }
    
    /**
     * Get a specific comment record that would be used by a given market.
     * @param p_market  Market for which comment is relevant.
     * @param p_fieldId Identifier of the comment.
     * @return Comment data object.
     */
    public SrvaFieldsData getComment (Market p_market, String p_fieldId)
    {
        return getCommentData(p_market, p_fieldId, false);
    }

    /**
     * Get a specific comment record for a specific market.
     * @param p_market  Market for which comment is relevant.
     * @param p_fieldId Identifier of the comment.
     * @return Comment data object.
     */
    public SrvaFieldsData getCommentForMarket (Market p_market, String p_fieldId)
    {
        return getCommentData(p_market, p_fieldId, true);
    }

    /** Returns array of all records in this data set object.
     * 
     * @return Array of all records. */
    public SrvaFieldsData [] getAllArray()
    {
        return i_commentArray;
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_commentArray, p_sb);
    }

    /**
     * Get a specific comment record.
     * @param p_market  Market for which comment is relevant.
     * @param p_fieldId Identifier of the comment.
     * @param p_exactMarket Flag indicating whether the comment should only match the exact market
     * (not global).
     * @return Comment data object.
     */
    private SrvaFieldsData getCommentData (Market p_market, String p_fieldId, boolean p_exactMarket)
    {
        SrvaFieldsData l_comment = null;
        SrvaFieldsData l_global  = null;

        for (int i = 0; i < i_commentArray.length && (l_comment == null || l_global == null); i++)
        {
            if ( i_commentArray[i].getKey().equals(p_fieldId) &&
                !i_commentArray[i].isDeleted())
            {
                if (i_commentArray[i].getMarket().equals(p_market))
                {
                    l_comment = i_commentArray[i];
                }
                else if (!p_exactMarket && i_commentArray[i].getMarket().equals(C_GLOBAL_MARKET))
                {
                    l_global = i_commentArray[i];
                }
            }
        }

        if (l_comment == null && l_global != null)
        {
            l_comment = l_global;
        }
        
        return l_comment;
    }
}
