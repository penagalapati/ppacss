//////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CalculatedBonusDataSet.java
//      DATE            :       28-Dec-2006
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS_GEN_CA_104
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Set of details of a calculated bonus.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/07/07 | SJ Vonka   | Allow for wildcard selection    | PpacLon#3184/11796
//          |            |                                 | PRD_ASCS00_GEN_CA_120
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/** Set of details of a calculated bonus. */
public class CalculatedBonusDataSet extends DataSetObject
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CalculatedBonusDataSet";

    /** Underlying data set. */
    private Vector i_set = new Vector();
    
    /** The least number of wildcards in the key elements of any CalculatedBonusData in this set so far. */
    private int i_leastNumberOfWildcards = 99;
    
    /** Indicator that highest bonus should be obtained where multiple ones match. Value is {@value}. */
    public static final String C_HIGH = "High";

    /** Indicator that lowest bonus should be obtained where multiple ones match. Value is {@value}. */
    public static final String C_LOW  = "Low";

    /** Add details of a bonus.
     * 
     * @param p_data Bonus to be added to the set.
     */
    public void addBonus(CalculatedBonusData p_data)
    {
        int l_leastNumberOfWildcards = 0;
        
        if (p_data != null)
        {
            l_leastNumberOfWildcards = p_data.getNumberOfWildcards();
        }

        i_set.add(p_data);

        if (this.size() == 1) // this is the first data object to be added.
        {
            i_leastNumberOfWildcards = l_leastNumberOfWildcards;
        }
        else
        {
            if (i_leastNumberOfWildcards > l_leastNumberOfWildcards)
            {
                i_leastNumberOfWildcards = l_leastNumberOfWildcards;
            }
        }
    }
    
    /** Get the number of bonuses that were calculated.
     * 
     * @return Size of the underlying data set.
     */
    public int size()
    {
        return i_set.size();
    }
    
    /** Get bonus based on criteria.
     * 
     * @param p_highLow      String indicating whether highest or lowest values should be taken. This is the
     *                       initial criteria (if correctly specified).
     * @param p_accountOrder Order of dedicated accounts. This is secondary criteria if multiple bonuses have
     *                       the same highest/lowest amount (or any amount if <code>p_highLow</code> is not
     *                       specified).
     * @return Bonus that matches the criteria (or first bonus after applying criteria if there are still
     *         multiple bonuses.
     */
    public CalculatedBonusData get(String p_highLow, int[] p_accountOrder)
    {
        if (size() == 0)
        {
            // No matching bonus.
            return null;
        }
        
        if (size() == 1)
        {
            // Only one entry so order not important.
            return (CalculatedBonusData)i_set.get(0);
        }
        
        CalculatedBonusDataSet l_set = p_highLow == null        ? this :
                                       p_highLow.equals(C_HIGH) ? restrictHighLow(C_HIGH) :
                                       p_highLow.equals(C_LOW)  ? restrictHighLow(C_LOW)  :
                                       this;

        if (l_set.size() == 0)
        {
            // No matching bonus.
            return null;
        }
       
        if (l_set.size() == 1)
        {
            // Only one entry so order not important.
            return (CalculatedBonusData)l_set.i_set.get(0);
        }

        l_set = p_accountOrder == null || p_accountOrder.length == 0 ? l_set :
                l_set.restrictDedOrder(p_accountOrder);
        
        if (l_set.size() == 0)
        {
            // No matching bonus.
            return null;
        }
        
        // Only one entry so order not important or more than one so just return first.
        return (CalculatedBonusData)l_set.i_set.get(0);
    }
    
    
    /** Get a set of the bonuses that have the number of wildcard matches in the key elements.
     * 
     * @return Data set containing bonuses that have the least number of wildcard matches in the key elements
     *         no wildcard matches being the best match. 
     */
    public CalculatedBonusDataSet filterOnWildcards()
    {
        if (i_set.size() <= 1)
        {
            // Nothing to do
            return this;
        }
        
        CalculatedBonusDataSet l_set = new CalculatedBonusDataSet();

        for (int i = 0; i < i_set.size(); i++)
        {
            CalculatedBonusData l_data = (CalculatedBonusData)i_set.get(i);

            if (i_leastNumberOfWildcards == l_data.getNumberOfWildcards())
            {
                l_set.addBonus(l_data);
            }
        }
        
        return l_set;
    }
    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_restrictHighLow = "restrictHighLow";
    /** Get a set of the bonuses that have the highest or lowest amount.
     * 
     * @param p_mode Identifier for high or low data.
     * @return Data set containing all the bonuses that have the highest/lowest amount. 
     */
    private CalculatedBonusDataSet restrictHighLow(String p_mode)
    {
        if (i_set.size() <= 1)
        {
            // Nothing to do
            return this;
        }
        
        CalculatedBonusDataSet l_set = new CalculatedBonusDataSet();
        
        try
        {
            Money l_amount = ((CalculatedBonusData)i_set.get(0)).getBonusAmount();
            
            for (int i = 1; i < i_set.size(); i++)
            {
                CalculatedBonusData l_data = (CalculatedBonusData)i_set.get(i);
                
                if (l_amount == null ||
                    (p_mode.equals(C_HIGH) && l_data.getBonusAmount().compareTo(l_amount) > 0) ||
                    (p_mode.equals(C_LOW)  && l_data.getBonusAmount().compareTo(l_amount) < 0))
                {
                    l_amount = l_data.getBonusAmount();
                }
            }
            
            for (int i = 0; i < i_set.size(); i++)
            {
                CalculatedBonusData l_data = (CalculatedBonusData)i_set.get(i);
                
                if (l_amount.equals(l_data.getBonusAmount()))
                {
                    l_set.addBonus(l_data);
                }
            }
        }
        catch (PpasConfigException e)
        {
            // Config exception thrown if currencies are different - will not happen.
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_restrictHighLow, 11010, this, null, 0,
                    SoftwareKey.get().unexpectedException(), e);
        }
        
        return l_set;
    }
    
    /** Get a set of the bonuses based on dedicated account order.
     * 
     * @param p_order Order of dedicated accounts.
     * @return Data set containing all the bonuses that have the most desired dedicated account.
     */
    private CalculatedBonusDataSet restrictDedOrder(int[] p_order)
    {
        if (i_set.size() <= 1 || p_order == null)
        {
            // Nothing to do
            return this;
        }
        
        CalculatedBonusDataSet l_set = new CalculatedBonusDataSet();
        
        for (int j = 0; l_set.size() == 0 && j < p_order.length; j++)
        {
            for (int i = 0; i < i_set.size(); i++)
            {
                CalculatedBonusData l_data = (CalculatedBonusData)i_set.get(i);
                
                if (l_data.getBonusCriteria().getDedAccId().intValue() == p_order[j])
                {
                    // Found data for a dedicated account id.
                    l_set.addBonus(l_data);
                }
            }
        }
        
        if (l_set.size() == 0)
        {
            // No dedicated accounts matched so return full set
            
            for (int i = 0; i < i_set.size(); i++)
            {
                l_set.addBonus((CalculatedBonusData)i_set.get(i));
            }
        }
        
        return l_set;
    }
    

    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("Calculated Bonuses", i_set, p_sb);
    }
}
