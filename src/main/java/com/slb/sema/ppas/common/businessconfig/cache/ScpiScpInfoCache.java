////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScpiScpInfoCache.Java
//      DATE            :       21-Jan-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the SCP information
//                              data held in the scpi_scp_info table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ScpiScpInfoSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the SCP information business configuration data.
 */
public class ScpiScpInfoCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScpiScpInfoCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private ScpiScpInfoSqlService i_scpiSqlService = null;

    /** Data set containing all SCP information. */
    private ScpiScpInfoDataSet i_allScpInfo = null;

    /**
     * Cache of available SCP Ids (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private ScpiScpInfoDataSet i_availableScpInfo = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new SCP information business configuration cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public ScpiScpInfoCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 38110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_scpiSqlService = new ScpiScpInfoSqlService (p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 38120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all SCP information data (including records marked as deleted).
     * @return Data set containing all SCP information records in this cache.
     */
    public ScpiScpInfoDataSet getAll()
    {
        return (i_allScpInfo);

    } // End of public method getAll

    /**
     * Returns available SCP information data. This does not include records
     * marked as deleted.
     * @return Data set containing available SCP information records in the
     *         cache (i.e. those not marked as deleted).
     */
    public ScpiScpInfoDataSet getAvailable()
    {
        return (i_availableScpInfo);

    } // End of public method getAvailable

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the SCP information cache's data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 38230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allScpInfo = i_scpiSqlService.readAll (p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all SCP information and old list of
        // available SCP information data, but rather than synchronise this is
        // acceptable.
        i_availableScpInfo = new ScpiScpInfoDataSet(p_request, i_allScpInfo.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 38290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allScpInfo, i_availableScpInfo);
    }
}

