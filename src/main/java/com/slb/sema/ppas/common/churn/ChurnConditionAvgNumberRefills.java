////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionAvgNumberRefills.java
//      DATE            :       09-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              average number of refills over time.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/** Identify whether a customer qualifies for churn based on average number of refills over time. */
public class ChurnConditionAvgNumberRefills extends ChurnCondition
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ChurnConditionAvgNumberRefills";

    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C2 - Ratio of Refills to value over time. ";
    
    /** Threshold limit for high number of refills. */
    private double i_highRefillNumberThreshhold;
    
    /** Threshold limit for small refill values. */
    private Money  i_smallValueThreshhold;
    
    /** Number of 'months' to track data. */
    private int i_trackPeriod;
    
    /** Constructor for a Churn condition based on average number of refills over time.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     * @throws PpasConfigException if properties are incorrect.
     */
    public ChurnConditionAvgNumberRefills(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
        throws PpasConfigException
    {
        super(p_properties, p_bcCache, p_analyser);

        i_highRefillNumberThreshhold = p_properties.getDoubleProperty(C_PROP_PREFIX + "highRefillNumberThreshold", 1.0);
        i_smallValueThreshhold       = getMoneyProperty(C_PROP_PREFIX + "smallRefillValueThreshold");
        i_trackPeriod                = p_properties.getIntProperty(C_PROP_PREFIX + "trackingPeriod", 1);
    }
        
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_matches = "matches";
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason)
    {
        p_reason.append(C_CONDITION_NAME);

        double l_trackAverage  = p_data.getTrackingAverageNumber();

        double l_difference = l_trackAverage - i_highRefillNumberThreshhold;
        
        if (Math.abs(l_difference) < 0.01)
        {
            l_difference = 0.0;  // Close enough to the same value.
        }
        
        p_reason.append("Average in tracking period (");
        p_reason.append(l_trackAverage);
        p_reason.append(") ");
        p_reason.append(l_difference > 0 ? ">" : l_difference == 0.0 ? "=" : "<");
        p_reason.append(" threshold (");
        p_reason.append(i_highRefillNumberThreshhold);
        p_reason.append(")");

        if (l_difference <= 0.0)
        {
            // Note: No Churn if on/below the threshold
            p_reason.append(". " + C_NO_CHURN_TEXT);
            
            return false;
        }
        
        // Looks like they have a lot of refills - so check value of those refills.
        
        Money l_totalrefillValue = p_data.getTrackingValue();
        int   l_numberRefills    = p_data.getTrackingNumber();
        
        Money l_averageValue = l_totalrefillValue.multiply(1.00/(l_numberRefills == 0 ? 1 : l_numberRefills));

        Money l_smallValueThreshold = i_analyser.convertAmount(i_smallValueThreshhold, l_totalrefillValue.getCurrency());
        
        Money l_diffValue;
        try
        {
            l_diffValue = l_averageValue.subtract(l_smallValueThreshold);
        }
        catch (PpasConfigException e)
        {
            // Cannot happen - so software exception.
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_matches, 30010, null, null, 0,
                                            SoftwareKey.get().unexpectedException(), e);
        }

        
        p_reason.append(", average value (");
        p_reason.append(formatMoney(l_averageValue));
        p_reason.append(") ");
        p_reason.append(l_diffValue.isNegative() ? "<" : l_diffValue.isZero() ? "=" : ">");
        p_reason.append(" threshold (");
        p_reason.append(formatMoney(l_smallValueThreshold));
        p_reason.append("). ");
        
        boolean l_result = l_diffValue.isNegative();

        p_reason.append(l_result ? C_CHURN_TEXT : C_NO_CHURN_TEXT);
        
        return l_result;
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if number of refills over last " + i_trackPeriod + " months > threshold of "
               + i_highRefillNumberThreshhold + ", and also the average "
               + "value of each refill must also be less than " + formatMoney(i_smallValueThreshhold);
    }
}
