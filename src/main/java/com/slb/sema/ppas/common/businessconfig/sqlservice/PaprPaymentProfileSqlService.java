////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PaprPaymentProfileSqlService.Java
//      DATE            :       17-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the payment profiles business
//                              config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/09/03 | Remi       | Changes to reflect              | CR#15/440
//          | Isaacs     | PAPR_PAYMENT_PROFILE table as   |
//          |            | defined in                      |
//          |            | PRD_ASCS00_SYD_AS_003           |
//----------+------------+---------------------------------+--------------------
// 30/10/03 | MAGray     | Change ccy to string.           | CR#56/531
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.PaprPaymentProfileData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PaprPaymentProfileDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;


/**
 * Implements a service that performs the SQL required to populate the
 * payment profiles business config cache.
 */
public class PaprPaymentProfileSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PaprPaymentProfileSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Sql service to be used in generating the data for the
     * payment profiles business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public PaprPaymentProfileSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 92310, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 92390, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all payment profiles data (including ones marked as
     * deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return A data set containing all the payment profile records read from
     *         the database.
     * @throws PpasSqlException If the data cannot be read.
     */
    public PaprPaymentProfileDataSet readAll (PpasRequest     p_request,
                                              JdbcConnection  p_connection)
        throws PpasSqlException
    {
        JdbcStatement             l_statement = null;
        //String                    l_sql = null;
        String                    l_paymentProfile = null;
        PaprPaymentProfileData    l_paprRecord = null;
        Vector                    l_allPaprV = new Vector (50, 50);
        PaprPaymentProfileData    l_allPaprARR[] = null;
        PaprPaymentProfileData    l_paprEmptyARR[] =
                                                new PaprPaymentProfileData[0];
        JdbcResultSet             l_results         = null;
        PaprPaymentProfileDataSet l_allPaprDataSet  = null;
        //CR#15/440 [Begin]
        String                    l_paprDescription = null;
        PpasCurrency              l_paprCurrency    = null;
        char                      l_paprDeleteFlag  = (char)0;
        //CR#15/440 [End]
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 94410, this,
                "Entered " +  C_METHOD_readAll);
        }

        SqlString l_sql = new SqlString( 200, 1,
                                         "SELECT " +
                                         "papr_payment_profile, " +
                                         "papr_description, " +
                                         "papr_currency, " +
                                         "papr_delete_flag, " +
                                         "cufm_precision " +
                                         "FROM papr_payment_profile, cufm_currency_formats " +
                                         "WHERE papr_currency = cufm_currency");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 94420, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 94430, this, p_request, l_sql);
        while ( l_results.next(94440) )
        {
            l_paymentProfile      = l_results.getTrimString(94445, "papr_payment_profile");

            l_paprDescription     = l_results.getTrimString(94450, "papr_description");

            l_paprDeleteFlag      = l_results.getChar(94452, "papr_delete_flag");

            l_paprCurrency        = new PpasCurrency(l_results.getTrimString(94452, "papr_currency"),
                                                     l_results.getInt(       94455, "cufm_precision"));


            l_paprRecord = new PaprPaymentProfileData(
                    p_request,
                    l_paymentProfile,
                    l_paprDescription,
                    l_paprCurrency,
                    l_paprDeleteFlag);

            l_allPaprV.addElement (l_paprRecord);
        }

        l_results.close (11550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 11560, this, p_request);

        l_allPaprARR = (PaprPaymentProfileData[])
                         l_allPaprV.toArray (l_paprEmptyARR);

        l_allPaprDataSet = new PaprPaymentProfileDataSet(p_request, l_allPaprARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 96570, this,
                "Leaving " + C_METHOD_readAll);
        }

        return (l_allPaprDataSet);

    } // End of public method readAll

} // End of public class PaprPaymentProfileSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
