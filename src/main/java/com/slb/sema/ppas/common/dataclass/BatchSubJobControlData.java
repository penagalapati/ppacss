////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       BatchSubJobControlData.java 
//DATE            :       24-August-2005
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       WM-data 2005
//
//DESCRIPTION     :       A container for batch sub job control data
//                        (see database table "BSCO_BATCH_SUB_CONTROL").
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This <code>BatchSubJobControlData</code> class is a container for
 * batch sub job control data.
 */
public class BatchSubJobControlData
{
    //-------------------------------------------------------------------------
    // Private class constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of the current class. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchSubJobControlData";


    //-------------------------------------------------------------------------
    // Private instance attributes.
    //-------------------------------------------------------------------------
    /** The job type (the "name" of the batch).*/
    private String       i_jobType           = null;
    
    /** The execution date and time. */
    private PpasDateTime i_executionDateTime = null;

    /** The unique sequence number for the sub job. */
    private int          i_subJobId          = -1; // -1 indicates that the id is not defined.

    /** The unique job id that is assigned to the sub job by the Job Scheduler. */
    private long         i_jsJobId           = -1; // -1 indicates that the id is not defined.
    
    /** The unique js job id assigned to the sub (batch) job's master (batch) job. */
    private long         i_masterJsJobId     = -1; // -1 indicates that the id is not defined.

    /** The status of the (batch) sub job.*/
    private char         i_status            = 0;  //  0 indicates that the status is not defined.

    /** The start (lowest) customer id to be processed by the sub job. */
    private long         i_startCustId       = -1; // -1 indicates that the customer id is not defined.

    /** The end (highest) customer id to be processed by the sub job. */
    private long         i_endCustId         = -1; // -1 indicates that the customer id is not defined.

    /** The last customer id that was processed by the sub job. */
    private long         i_lastCustId        = -1; // -1 indicates that the customer id is not defined.

    /** Extra data 1. */
    private String       i_extraData1        = null;

    /** Extra data 2. */
    private String       i_extraData2        = null;

    /** The operator id.*/
    private String       i_opId              = null;

    //------------------------------------------------------------------------
    // Constructor(s)
    //------------------------------------------------------------------------
    /**
     * Connstructs a <code>BatchSubJobControlData</code> instance that holds the
     * specified batch sub job details.
     * 
     * @param p_jobType            the job type of the sub job (the "name" of the batch).
     * @param p_executionDateTime  the execution date and time.
     * @param p_subJobId           the unique sequence number for the sub job.
     * @param p_jsJobId            the unique job id that is assigned to the sub job by the Job Scheduler.
     * @param p_masterJsJobId      the unique js job id assigned to the sub job's master job. 
     * @param p_status             the status of the (batch) sub job.
     * @param p_startCustId        the start (lowest) customer id to be processed by the sub job.
     * @param p_endCustId          the end (highest) customer id to be processed by the sub job.
     * @param p_lastCustId         last customer id that was processed by the sub job.
     * @param p_extraData1         extra data 1.
     * @param p_extraData2         extra data 2.
     * @param p_opId               the operator id.
     */
    public BatchSubJobControlData(String       p_jobType,
                                  PpasDateTime p_executionDateTime,
                                  int          p_subJobId,
                                  long         p_jsJobId,
                                  long         p_masterJsJobId,
                                  char         p_status,
                                  long         p_startCustId,
                                  long         p_endCustId,
                                  long         p_lastCustId,
                                  String       p_extraData1,
                                  String       p_extraData2,
                                  String       p_opId)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        
        // Save the attributes.
        i_jobType           = p_jobType;
        i_executionDateTime = (PpasDateTime)p_executionDateTime.clone();
        i_subJobId          = p_subJobId;
        i_jsJobId           = p_jsJobId;
        i_masterJsJobId     = p_masterJsJobId;
        i_status            = p_status;
        i_startCustId       = p_startCustId;
        i_endCustId         = p_endCustId;
        i_lastCustId        = p_lastCustId;
        i_extraData1        = p_extraData1;
        i_extraData2        = p_extraData2;
        i_opId              = p_opId;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME + this.toString());
        }
    }

    //------------------------------------------------------------------------
    // Public methods.
    //------------------------------------------------------------------------
    /**
     * Compares this object to the specified object.
     * The result is true if and only if the argument is not null and is a
     * <code>BatchSubJobControlData</code> object that contains the same values for all
     * attributes as this object.
     * 
     * @param p_obj  the object to compare with.
     * 
     * @return  <code>true</code> if the objects are the same, <code>false</code> otherwise.
     */
    public boolean equals(Object p_obj)
    {
        boolean                l_equal   = false;
        BatchSubJobControlData l_compObj = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering the 'equals(...)' method.");
        }

        if (p_obj != null  &&  p_obj instanceof BatchSubJobControlData)
        {
            l_compObj = (BatchSubJobControlData)p_obj;

            if (isEqual(l_compObj.i_jobType, i_jobType)                     &&
                isEqual(l_compObj.i_executionDateTime, i_executionDateTime) &&
                l_compObj.i_subJobId      == i_subJobId                     &&
                l_compObj.i_jsJobId       == i_jsJobId                      &&
                l_compObj.i_masterJsJobId == i_masterJsJobId                &&
                l_compObj.i_status        == i_status                       &&
                l_compObj.i_startCustId   == i_startCustId                  &&
                l_compObj.i_endCustId     == i_endCustId                    &&
                l_compObj.i_lastCustId    == i_lastCustId                   &&
                isEqual(l_compObj.i_extraData1, i_extraData1)               &&
                isEqual(l_compObj.i_extraData2, i_extraData2)               &&
                isEqual(l_compObj.i_opId, i_opId))
            {
                l_equal = true;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving the 'equals(...)' method.");
        }
        return l_equal;
    }


    /**
     * Displays the batch sub job details (control data) as a <code>String</code>.
     * 
     * @return a <code>String</code> representation of the batch sub job details.
     */
    public String toString()   
    {
        StringBuffer l_stringBuf = new StringBuffer();

        l_stringBuf.append("job type: "      + i_jobType);
        l_stringBuf.append(",  exec. time: " +
                           (i_executionDateTime != null  ?  i_executionDateTime.toString() : "null"));
        l_stringBuf.append(",  sub job id: "              + i_subJobId);
        l_stringBuf.append(",  js job id: "               + i_jsJobId);
        l_stringBuf.append(",  master js job id: "        + i_masterJsJobId);
        l_stringBuf.append(",  status: "                  + i_status);
        l_stringBuf.append(",  start cust. id: "          + i_startCustId);
        l_stringBuf.append(",  end cust. id: "            + i_endCustId);
        l_stringBuf.append(",  last processed cust. id: " + i_lastCustId);
        l_stringBuf.append(",  extra data 1: "            + i_extraData1);
        l_stringBuf.append(",  extra data 2: "            + i_extraData2);
        l_stringBuf.append(",  op id: '"                  + i_opId + "'.");

        return l_stringBuf.toString();
    }


    /**
     * @return Returns the endCustId.
     */
    public long getEndCustId()
    {
        return i_endCustId;
    }

    /**
     * @return Returns the executionDateTime.
     */
    public PpasDateTime getExecutionDateTime()
    {
        return i_executionDateTime;
    }

    /**
     * @return Returns the extraData1.
     */
    public String getExtraData1()
    {
        return i_extraData1;
    }

    /**
     * @return Returns the extraData2.
     */
    public String getExtraData2()
    {
        return i_extraData2;
    }

    /**
     * @return Returns the jobType.
     */
    public String getJobType()
    {
        return i_jobType;
    }

    /**
     * @return Returns the jsJobId.
     */
    public long getJsJobId()
    {
        return i_jsJobId;
    }

    /**
     * @return Returns the lastCustId.
     */
    public long getLastCustId()
    {
        return i_lastCustId;
    }

    /**
     * @return Returns the masterJsJobId.
     */
    public long getMasterJsJobId()
    {
        return i_masterJsJobId;
    }

    /**
     * @return Returns the opId.
     */
    public String getOpId()
    {
        return i_opId;
    }

    /**
     * @return Returns the startCustId.
     */
    public long getStartCustId()
    {
        return i_startCustId;
    }

    /**
     * @return Returns the status.
     */
    public char getStatus()
    {
        return i_status;
    }

    /**
     * @return Returns the subJobId.
     */
    public int getSubJobId()
    {
        return i_subJobId;
    }


    //------------------------------------------------------------------------
    // Private methods.
    //------------------------------------------------------------------------
    /**
     * Returns <code>true</code> if the specified <code>Object</code>s both are 'null' or if both are 
     * not 'null' and equal according to the first <code>Object</code>'s <code>equals</code> method.
     * Note: It is assumed that the first <code>Object</code>'s <code>equals</code> method handles a 'null'
     *       argument or an <code>Object</code> of a different type in a proper way.
     * 
     * @return <code>true</code> if the specified two <code>Object</code>s both are 'null'
     *         or if both are not 'null' and equal according to the first <code>Object</code>'s
     *         <code>equals</code> method, <code>false</code> otherwise.
     * 
     * @param p_obj1  the first  <code>Object</code>.
     * @param p_obj2  the second <code>Object</code>.
     */
    private boolean isEqual(Object p_obj1, Object p_obj2)
    {
        boolean l_equal = false;

        if (p_obj1 == p_obj2)
        {
            l_equal = true;
        }
        else if (p_obj1 != null && p_obj1.equals(p_obj2))
        {
            l_equal = true;
        }

        return l_equal;
    }
}
