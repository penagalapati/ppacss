////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CCIChangeBatchRecordData
//      DATE            :       30-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class encapsulates the data which origins fromn a flat 
//                              file. Each such object contains all information necessary to
//                              change community for one subscription.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/** This class encapsulates the data which origins fromn a flat file. Each such object contains all
 * information necessary to change community for one subscription.
 */
public class CCIChangeBatchRecordData extends BatchRecordData
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME   = "CCIChangeBatchRecordData";
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** MSISDN. */
    private Msisdn   i_msisdn     = null;
    
    /** Old community charging list as <code>CommunitiesIdListData</code>. */
    private CommunitiesIdListData i_oldCCIList = null;
    
    /** New community charging list as <code>CommunitiesIdListData</code>. */
    private CommunitiesIdListData i_newCCIList = null;
    
    /**
     * Constructor for CCIChangeBatchRecordData.
     */
    public CCIChangeBatchRecordData()
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10700,
                this,
                C_CONSTRUCTING + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10710,
                this,
                C_CONSTRUCTED + C_CLASS_NAME);
        }
    } // End of constructor CCIChangeBatchRecordData()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_dumpRecord = "dumpRecord";
    /** Dump the recoird in a readable form.
     * @return the record content in a readable format.
     */
    public String dumpRecord()
    {
        StringBuffer l_tmp = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10780,
                this,
                C_ENTERING + C_METHOD_dumpRecord);
        }

        // Get the super class's data
        l_tmp.append( C_DELIMITER );
        l_tmp.append( "*** Class name = " );
        l_tmp.append( C_CLASS_NAME );
        l_tmp.append( " ***");
        l_tmp.append( C_DELIMITER );

        l_tmp.append( super.getDumpRecord() );

        // This class's data
        l_tmp.append( "msisdn      = " );
        if (i_msisdn != null)
        {
            l_tmp.append( this.i_msisdn.getCountryCode() );
            l_tmp.append( this.i_msisdn.getSignificantNumber());
        }
        else
        {
            l_tmp.append("null");
        }
        l_tmp.append( C_DELIMITER );

        if ( i_oldCCIList != null &&
             i_newCCIList != null )
        {
            l_tmp.append(" old    new CCI\n"); 
            l_tmp.append("=================\n");
            l_tmp.append( this.i_oldCCIList );
            l_tmp.append( "    " );
            l_tmp.append( this.i_newCCIList );
            l_tmp.append( C_DELIMITER );
        }
        else
        {
            l_tmp.append("old- and new CCI are null ");
        }


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10790,
                this,
                C_LEAVING + C_METHOD_dumpRecord);
        }

        return l_tmp.toString();
    } // end of method dumpRecord()

    /** Get the mobile number for which this change applies.
     * 
     * @return Mobile number of the account being changed.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /** Get the new Community Id List.
     * 
     * @return New Community Id List.
     */
    public CommunitiesIdListData getNewCCIList()
    {
        return i_newCCIList;
    }

    /** Get the old Community Id List.
     * 
     * @return Old Community Id List.
     */
    public CommunitiesIdListData getOldCCIList()
    {
        return i_oldCCIList;
    }

    /** Set the mobile number for which this change applies.
     * 
     * @param p_msisdn Mobile number of the account being changed.
     */
    public void setMsisdn(Msisdn p_msisdn)
    {
        i_msisdn = p_msisdn;
    }

    /** Set the new Community Id List.
     * 
     * @param p_newCCIList New Community Id List.
     */
    public void setNewCCIList(CommunitiesIdListData p_newCCIList)
    {
        i_newCCIList = p_newCCIList;
    }

    /** Set the old Community Id List.
     * 
     * @param p_oldCCIList Old Community Id List.
     */
    public void setOldCCIList(CommunitiesIdListData p_oldCCIList)
    {
        i_oldCCIList = p_oldCCIList;
    }
}
