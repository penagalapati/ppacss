////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :          9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CufmCurrencyFormatsSqlService.Java
//      DATE            :       23-October-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1074/4394
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Service to read the CUFM_CURRENCY_FORMATS table 
//                              and  return a CufmCurrencyFormatsDataSet object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to read the CUFM_CURRENCY_FORMATS table and  return a 
 * CufmCurrencyFormatsDataSet object.
 */
public class CufmCurrencyFormatsSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CufmCurrencyFormatsSqlService";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new currency formats sql service.
     * 
     * @param p_request The request to process.
     */
    public CufmCurrencyFormatsSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readCurrencyFormats = "readCurrencyFormats";
    /**
     * Read the currency formats from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of currency format data objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public CufmCurrencyFormatsDataSet readCurrencyFormats(PpasRequest     p_request,
                                                          JdbcConnection  p_connection)
        throws PpasSqlException
    {
        JdbcStatement              l_statement;
        JdbcResultSet              l_resultSet;
        SqlString                  l_sql;
        CufmCurrencyFormatsData    l_cufmData;
        CufmCurrencyFormatsDataSet l_cufmDataSet;
        String                     l_currency;
        int                        l_precision;
        char                       l_deleteFlag;

        if (PpasDebug.on) 
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, 
                            p_request, 
                            C_CLASS_NAME, 
                            11000, 
                            this,
                            "Entered " + C_METHOD_readCurrencyFormats);
        }
        
        l_sql = new SqlString(200, 
                              0, 
                              "SELECT cufm_currency," +
                              "cufm_precision, " +
                              "cufm_delete_flag " +
                              "FROM cufm_currency_formats");

        l_cufmDataSet = new CufmCurrencyFormatsDataSet(p_request, 
                                                       CufmCurrencyFormatsDataSet.C_CURRENCY_V_INITIAL_SIZE,
                                                       CufmCurrencyFormatsDataSet.C_CURRENCY_V_GROW_SIZE);

        l_statement = p_connection.createStatement(
                                   C_CLASS_NAME, C_METHOD_readCurrencyFormats, 11010, this, p_request);

        l_resultSet = l_statement.executeQuery(
                                   C_CLASS_NAME, C_METHOD_readCurrencyFormats, 11020, this, p_request, l_sql);

        while (l_resultSet.next(11030))
        {
            l_currency = l_resultSet.getTrimString(11040, "cufm_currency");
            l_precision = l_resultSet.getInt(11050, "cufm_precision");
            l_deleteFlag = l_resultSet.getChar(11051, "cufm_delete_flag");

            // Create a new currency format object...
            l_cufmData = new CufmCurrencyFormatsData(p_request,
                                                     l_currency,
                                                     l_precision,
                                                     l_deleteFlag);

            // ...and stuff it in the currency format data set.
            l_cufmDataSet.addCurrencyFormat(l_cufmData);
        }

        l_resultSet.close(11100);

        l_statement.close(C_CLASS_NAME, C_METHOD_readCurrencyFormats, 11110, this, p_request);

        if (PpasDebug.on) 
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END, 
                            p_request, 
                            C_CLASS_NAME, 
                            11120, 
                            this,
                            "Leaving " + C_METHOD_readCurrencyFormats);
        }
        
        return(l_cufmDataSet);
    }
}
