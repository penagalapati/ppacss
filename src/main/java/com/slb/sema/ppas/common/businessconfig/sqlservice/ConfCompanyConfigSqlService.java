////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ConfCompanyConfigSqlService.Java
//      DATE            :       10-July-2001
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#955/3784
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Service to return a ConfCompanyConfig object
//                              from the database
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                     | REFERENCE
//---------+---------------+---------------------------------+-----------------
//22/03/06 | M.Toernqvist  | New columns added to the table  | PpaLon#2020/8257
//         |               | CONF_COMPANY_CONFIG with        |
//         |               | ClassHistory default values.    |
//---------+---------------+---------------------------------+--------------------
//04-Jul-06| M.Toernqvist  | changes after code review       | PpasLon#2322/9274
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.ConfCompanyConfigData;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to run SQL to obtain company configuration from the database table
 * conf_company_config.
 */
public class ConfCompanyConfigSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ConfCompanyConfigSqlService";

    //------------------------------------------------------------------------
    // Constructors and Finalizers
    //------------------------------------------------------------------------

    /**
     * Creates a new conf sql service.
     *
     * @param p_request The request to process.
     */
    public ConfCompanyConfigSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed " + C_CLASS_NAME );
        }

    } // End of public constructor
      //         ConfCompanyConfigSqlService(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readConfCompanyConfig = "readConfCompanyconfig";
    /**
     * Read the company configuration from the database.
     *
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return ConfCompanyConfigData
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public ConfCompanyConfigData readConfCompanyConfig(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement          l_statement = null;
        JdbcResultSet          l_result    = null;
        SqlString              l_sql       = null;
        ConfCompanyConfigData  l_confData  = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 84650, this,
                            "Entered " + C_METHOD_readConfCompanyConfig);
        }

        l_sql =   new SqlString(1000, 0, "SELECT conf_multi_currency_ind, "   +
                         "conf_base_currency, "         +
                         "conf_serial_length, "         +
                         "conf_removal_disc_reason, "   +
                         "conf_temp_block,  "           +
                         "conf_ext_vch_database_used, " +
                         "conf_expiry_after_date_ind, " +
                         "conf_ivr_pin_length, "        +
                         "cufm_precision, "             +
                         "conf_call_hist_method, "      +
                         "conf_call_hist_period, "      +
                         "conf_max_call_hist_recs "     +
                  "FROM   conf_company_config, cufm_currency_formats " +
                  "WHERE cufm_currency = conf_base_currency");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL,
                p_request, C_CLASS_NAME, 28760, this,
                "About to execute SQL " + l_sql);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                   C_METHOD_readConfCompanyConfig,
                                                   29000,
                                                   this,
                                                   p_request);

        l_result = l_statement.executeQuery(C_CLASS_NAME,
                                            C_METHOD_readConfCompanyConfig,
                                            29100,
                                            this,
                                            p_request,
                                            l_sql);

        if ( l_result.next(51000) )
        {
            l_confData = new ConfCompanyConfigData(
                                    p_request,
                                    l_result.getChar(44810, "conf_multi_currency_ind"),
                                    new PpasCurrency(
                                        l_result.getTrimString(44820, "conf_base_currency"),
                                        l_result.getInt       (44825, "cufm_precision")),
                                    l_result.getChar(44881, "conf_ext_vch_database_used") == 'Y',
                                    l_result.getInt(44830, "conf_serial_length"),
                                    l_result.getTrimString(44840, "conf_removal_disc_reason"),
                                    l_result.getChar(44850, "conf_temp_block"),
                                    l_result.getChar(44890, "conf_expiry_after_date_ind"),
                                    l_result.getInt(45000,  "conf_ivr_pin_length"),
                                    l_result.getChar(45010,"conf_call_hist_method"),
                                    l_result.getInt(45020, "conf_call_hist_period"),
                                    l_result.getInt(45030, "conf_max_call_hist_recs") );
        }

        l_result.close(53280);

        l_statement.close( C_CLASS_NAME, C_METHOD_readConfCompanyConfig, 90280, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 28770, this,
                            "Leaving " + C_METHOD_readConfCompanyConfig);
        }

        return(l_confData);
    }
} // End of public class ConfCompanyConfigSqlService

