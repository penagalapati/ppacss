////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CochCommunityChargingDataSet.Java
//      DATE            :      11-Jul-2004
//      AUTHOR          :      Julien CHENELAT
//
//      COPYRIGHT       :      ATOS ORIGIN 2004
//
//      DESCRIPTION     :      A set (collection) of coch_community_charging data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a coch_community_charging data set which can be read from
 * the database.
 */
public class CochCommunityChargingDataSet
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CochCommunityChargingDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Logger. */
    protected Logger                     i_logger               = null;

    /** Array of coch_community_charging data. */
    private CochCommunityChargingData[]  i_cochCommunityChgARR;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of coch_community_charging data.
     * 
     * @param p_request                     Ppas Request.
     * 
     * @param p_logger                      Logger.
     * 
     * @param p_cochCommunityChgARR         Set of Communities Id data objects.
     */
    public CochCommunityChargingDataSet( PpasRequest                    p_request,
                                         Logger                         p_logger,
                                         CochCommunityChargingData[]    p_cochCommunityChgARR)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_START,
                             p_request, 
                             C_CLASS_NAME, 
                             10110, 
                             this,
                             "Constructing " + C_CLASS_NAME );
        }

        i_logger                = p_logger;
        i_cochCommunityChgARR   = p_cochCommunityChgARR;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_END,
                             p_request, 
                             C_CLASS_NAME, 
                             10150, 
                             this,
                             "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCommunityChargingData = "getCommunityChargingData";
    /**
     * Returns the coch_community_charging object for the supplied Community Charging Id.
     * 
     * @param p_communityChgId  Community Charging Id.
     * 
     * @return CochCommunityChargingData for the given Community Charging Id.
     */
    public CochCommunityChargingData getCommunityChargingData(int p_communityChgId)
    {
        int                        l_loop;
        CochCommunityChargingData  l_communityChargingData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START, 
                             null, 
                             C_CLASS_NAME, 
                             11100, 
                             this,
                             "Entering " + C_METHOD_getCommunityChargingData );
        }

        if (i_cochCommunityChgARR != null)
        {
            for ( l_loop = 0; l_loop < i_cochCommunityChgARR.length; l_loop++ )
            {
                if (i_cochCommunityChgARR[l_loop].getCommunityChgId() == p_communityChgId)
                {
                    l_communityChargingData = i_cochCommunityChgARR[l_loop];
                    break;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END, 
                             null, 
                             C_CLASS_NAME, 
                             11190, 
                             this,
                             "Leaving " + C_METHOD_getCommunityChargingData );
        }

        return(l_communityChargingData);
    }

    /**
     * Returns an array of this coch_community_charging data set, but with deleted
     * rows omitted.
     * 
     * @return Return a set of all available coch_community_charging recordd.
     */
    public CochCommunityChargingData[] getAvailableArray()
    {
        int                             l_loop;
        CochCommunityChargingData       l_availableCochCommunityChgARR[];
        Vector                          l_availableV                    = new Vector();
        CochCommunityChargingData       l_cochCommunityChgEmptyARR[]    = new CochCommunityChargingData[0];

        for(l_loop = 0; l_loop < i_cochCommunityChgARR.length; l_loop++)
        {
            if (!i_cochCommunityChgARR[l_loop].isDeleted())
            {
                l_availableV.addElement(i_cochCommunityChgARR[l_loop]);
            }
        }

        l_availableCochCommunityChgARR = (CochCommunityChargingData[])l_availableV
                                                                        .toArray(l_cochCommunityChgEmptyARR);

        return(l_availableCochCommunityChgARR);
    }
    
    /**
     * Check if the id given in parameter exist in the list.
     * 
     * @param p_communityId     Community Id to check.
     * 
     * @return True if the id exists in the list.
     */
    public boolean isIdExists(int p_communityId)
    {
        boolean l_found = false;
        
        for(int l_loop = 0; l_loop < i_cochCommunityChgARR.length && !l_found; l_loop++)
        {
            if ( !i_cochCommunityChgARR[l_loop].isDeleted()
                 && i_cochCommunityChgARR[l_loop].getCommunityChgId() == p_communityId )
            {
                l_found = true;
            }
        }
        
        return l_found;
    }


    /**
     * Returns a verbose string representation of the object. Can be used for
     * trace and debugging.
     * 
     * @return String representing the CMDF data set.
     */
    public String toVerboseString()
    {
        StringBuffer              l_buffer = new StringBuffer();
        int                       l_loop;

        l_buffer.append(C_CLASS_NAME + "@" + this.hashCode() + "\n");

        if (i_cochCommunityChgARR != null)
        {
            l_buffer.append("availCochCommunityCharging=");

            for( l_loop = 0;
                 l_loop < i_cochCommunityChgARR.length;
                 l_loop++ )
            {
                l_buffer.append("\n[" + i_cochCommunityChgARR[l_loop].toString() + "]");
            }
        }
        else
        {
            l_buffer.append("availCochCommunityCharging=null");
        }

        return(l_buffer.toString());
    }
}