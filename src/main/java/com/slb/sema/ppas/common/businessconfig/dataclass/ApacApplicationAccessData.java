////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ApplicationAccessData.Java
//      DATE            :       16-Oct-2003
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       Stores a row from the APAC_APPLICATION_ACCESS
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.HashSet;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
  * An object that represents a row from the APCA_APPLICATION_ACCESS database 
  * table. This table defines access to screens and functions in the PPAS GUI.
  * This object is thus designed for use with the GUI only - all references to
  * screens and functions refer to GUI functions.
  */
public class ApacApplicationAccessData
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ApacApplicationAccessData";
    
    /** Business configuration menu. */
    public static final String  C_PRIV_MENU_BUSINESS_CONFIG = "BusinessConfig";
    /** Batch menu. */
    public static final String  C_PRIV_MENU_BATCH = "Batch";    

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Hash of privileges. */
    private HashSet             i_privilegeHM;    
    
    /** The privilege ID. */
    private int i_privilegeId;

    /** Indicates whether access to the Batch application is permitted. */
    private char i_batchApplicationAccess;

    /** Indicates whether access to the Business Configuration application is permitted. */
    private char i_businessConfigurationApplicationAccess;

    /** Indicates whether access to the Purge and Archive application is permitted. */
    private char i_purgeAndArchiveApplicationAccess;

    /** Indicates whether access to the Reports application is permitted. */
    private char i_reportsApplicationAccess;
    
    /** The operator who last updated this Access Profile. */
    private String i_opid;

    /** The date that this profile was last updated. */
    private PpasDateTime i_lastModified; 

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Standard constructor with a list of privileges.
     * 
     * @param p_privilegeARR List of privileges.
     */
    public ApacApplicationAccessData(
        String                  p_privilegeARR[]
    )
    {
        int                     l_loop;
        
        i_privilegeHM = new HashSet();
        for(l_loop = 0; l_loop < p_privilegeARR.length; l_loop++)
        {
            i_privilegeHM.add(p_privilegeARR[l_loop]);
        }
    }
    
    /** Standard constructor with a hash of privileges.
     * 
     * @param p_privilegeHM Set of privileges.
     */
    public ApacApplicationAccessData(
        HashSet                 p_privilegeHM
    )
    {
        i_privilegeHM = p_privilegeHM;
    }

    /** Simple constructor that sets all screen access attributes to 'N'.
     * 
     * @param p_request The request to process.
     * @param p_privilegeId The privilege level identifier.
     * @param p_opid The operator who last updated this Access Profile. 
     * @param p_lastModified The date that this profile was last updated.
     */
    public ApacApplicationAccessData(PpasRequest  p_request,
                                     int          p_privilegeId,
                                     String       p_opid,
                                     PpasDateTime p_lastModified)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10100, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_privilegeId  = p_privilegeId;
        i_opid         = p_opid;
        i_lastModified = p_lastModified;

        i_batchApplicationAccess                 = 'N';
        i_businessConfigurationApplicationAccess = 'N';
        i_purgeAndArchiveApplicationAccess       = 'N';
        i_reportsApplicationAccess               = 'N';

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10190, this,
                 "Constructed " + C_CLASS_NAME );
        }
    } // end constructor

    /** 
     * Creates a Application Access profile object.
     * 
     * @param p_request The request to process.
     * @param p_privilegeId The privilege level identifier.
     * @param p_batchApplicationAccess Indicates whether access to the Batch application is permitted.
     * @param p_businessConfigurationApplicationAccess Indicates whether access to the 
     *                                  Business Configuration application is permitted.
     * @param p_purgeAndArchiveApplicationAccess Indicates whether access to the 
     *                                  Purge and Archive application is permitted.
     * @param p_reportsApplicationAccess Indicates whether access to the Reports application is permitted.
     * @param p_opid The operator who last updated this Access Profile. 
     * @param p_lastModified The date that this profile was last updated.
     */
    public ApacApplicationAccessData(PpasRequest  p_request,
                                     int          p_privilegeId,
                                     char         p_batchApplicationAccess,
                                     char         p_businessConfigurationApplicationAccess,
                                     char         p_purgeAndArchiveApplicationAccess,
                                     char         p_reportsApplicationAccess,
                                     String       p_opid, 
                                     PpasDateTime p_lastModified)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_privilegeId                            = p_privilegeId;
        i_batchApplicationAccess                 = p_batchApplicationAccess;
        i_businessConfigurationApplicationAccess = p_businessConfigurationApplicationAccess;
        i_purgeAndArchiveApplicationAccess       = p_purgeAndArchiveApplicationAccess;
        i_reportsApplicationAccess               = p_reportsApplicationAccess;
        i_opid                                   = p_opid;
        i_lastModified                           = p_lastModified;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        return("ApacApplicationAccessData"                  +
               ":i_privilegeId="                            + i_privilegeId +
               ":i_batchApplicationAccess="                 + i_batchApplicationAccess +
               ":i_businessConfigurationApplicationAccess=" + i_businessConfigurationApplicationAccess +
               ":i_purgeAndArchiveApplicationAccess="       + i_purgeAndArchiveApplicationAccess +
               ":i_reportsApplicationAccess="               + i_reportsApplicationAccess +
               ":i_opid="                                   + i_opid +
               ":i_lastModified="                           + i_lastModified);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Returns the privilege level identifer.
     * 
     * @return The privilege level.
     */
    public int getPrivilegeId()
    {
        return i_privilegeId;
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Batch application.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasBatchApplicationAccess()
    {
        return (i_batchApplicationAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Business Configuration application.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasBusinessConfigurationApplicationAccess()
    {
        return (i_businessConfigurationApplicationAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Purge and Archive application.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasPurgeAndArchiveApplicationAccess()
    {
        return (i_purgeAndArchiveApplicationAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Reports application.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasReportsApplicationAccess()
    {
        return (i_reportsApplicationAccess == 'Y');
    }

    /** Returns the ID of the operator who last updated this Access Profile.
     * 
     * @return Identifier of the operator that last changed this access data.
     */
    public String getOpid()
    {
        return i_opid;
    }

    /** Returns the data and time that this Access Profile was last modified.
     * 
     * @return date/time this access data was last changed.
     */
    public PpasDateTime getLastModified()
    {
        return i_lastModified;
    }
    
//    public boolean hasPrivilege(
//        String                  p_privilege
//    )
//    {
//        boolean l_hasPrivilege = false;
//        
//        if(i_privilegeHM.contains(p_privilege))
//        {
//            l_hasPrivilege = true;
//        }       
//        return(l_hasPrivilege);
//    }

}

