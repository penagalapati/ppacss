////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ScpiScpInfoData.Java
//      DATE            :       21-Jan-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       A record of SCP information data 
//                              (corresponding to a single row of the
//                              scpi_scp_info table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//10/09/06  | Paul Rosser| Add new scpi transitioned       | PRD_ASCS00_GEN_CA_108
//          |            | column to scpi table. Get data. | PpacLon#2858/10794
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of SCP information data (i.e.
 * a single row of the scpi_scp_info table).
 */
public class ScpiScpInfoData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ScpiScpInfoData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The SCP (or SDP) identifier. */
    private  String            i_scpId = null;

    /** The SCP (or SDP) IP address. */
    private  String            i_scpIpAddress = null;

    /** The description of this SCP (or SDP) node. */
    private  String            i_description = null;

    /** The date/time that this SCP info record was created/last accessed in
     *  the database.
     */
    private  PpasDateTime      i_genYmdhms = null;

    /** The operator Id who created/last accessed this SCP Info record in the
     *  database.
     */
    private  String            i_opid = null;
    
    /** Flag indicating if this SCP information record is deleted. */
    private  char              i_delFlag = ' ';

    /** The network type on which this SCP (or SDP) is connected. */
    private  char              i_networkType = ' ';

    /** Whether or not this SDP has been transitioned to ASCS. */
    private  boolean           i_transitioned = true;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new SCP information data object and initialises its instance
     * variables.
     * 
     * @param p_request The request to process.
     * @param p_scpId   The identifier of this SDP.
     * @param p_scpIpAddress   The Ip Address.
     * @param p_description The name of this SDP.
     * @param p_genYmdhms The date/time this data was last updated.
     * @param p_opid    The operator that last changed this data.
     * @param p_delFlag Flag indicating whether this SDP has been marked as deleted.
     * @param p_networkType Type of network.
     * @param p_transitioned Whether the SDP has been transitioned to ASCS.
     */
    public ScpiScpInfoData(PpasRequest  p_request,
                           String       p_scpId,
                           String       p_scpIpAddress,
                           String       p_description,
                           PpasDateTime p_genYmdhms,
                           String       p_opid,
                           char         p_delFlag,
                           char         p_networkType,
                           boolean      p_transitioned)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 24010, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_scpId        = p_scpId;
        i_scpIpAddress = p_scpIpAddress;
        i_description  = p_description;
        i_genYmdhms    = p_genYmdhms;
        i_opid         = p_opid;
        i_delFlag      = p_delFlag;
        i_networkType  = p_networkType;
        i_transitioned = p_transitioned;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 24090, this,
                            "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the SCP (or SDP) identifier.
     * 
     * @return The SDP identifier.
     */
    public String getScpId()
    {
        return (i_scpId);
    }

    /** Get the IP address of the SCP/SDP.
     * @return String representing the address of this SCP/SDP.
     */
    public String getScpIpAddress() {

        return (i_scpIpAddress);
    }

    /** Get the description of this SCP (or SDP) node.
     * 
     * @return The SDP description.
     */
    public String getDescription()
    {
        return (i_description);
    }

    /** Get the date/time that this SCP info record was created/last accessed in
     *  the database.
     * 
     * @return The date/time this data was last changed.
     */
    public PpasDateTime getGenYmdhms()
    {
        return (i_genYmdhms);
    }

    /** Get the operator Id who created/last accessed this SCP Info record in
     *  the database.
     * 
     * @return Operator identifier that last changed this data.
     */
    public String getOpid()
    {
        return (i_opid);
    }

    /** Get the network type on which this SCP (or SDP) is connected.
     * 
     * @return The type of network to which the SDP is conected. 
     */
    public char getNetworkType()
    {
        return (i_networkType);
    }

    /** 
     * Returns true if this SDP has been transitioned to ASCS.
     * 
     * @return whether or not the SDP has been transitioned. 
     */
    public boolean isTransitioned()
    {
        return i_transitioned;
    }

    /** Returns true if this SCP information record has been marked as deleted.
     * 
     * @return Flag indicating the SDp is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if (i_delFlag == '*')
        {
            l_deleted = true;
        }

        return (l_deleted);
    }

    /** 
     * Returns a String containing the SCP ID padded to p_codeWidth 
     * characters and the description.
     * 
     * @param p_codeWidth Defines width of field to display SCP ID in including
     *                    trailing spaces. Maximum value is 4 characters.
     * @return SDP identifier and description as a text string for displaying in a GUI.
     */
    public String getDisplayString(int p_codeWidth)
    {
        int l_codeWidth = (p_codeWidth > 4) ? 4 : p_codeWidth;
        String l_scpId = i_scpId + "    ";
        
        return (l_scpId.substring(0, l_codeWidth) + i_description);
    }
    
    /** 
     * Return ScpiScpInfoData as a string.
     * @return A String representing the ScpiScpInfoData object.
     */
    public String toString()
    {
        return (i_scpId + " - " + i_description);
    }
    
    /** 
     * Compares an object is the same as this one.
     * 
     * @param p_scpiScpInfo Object which will be compared.
     * @return true if the objects are equal and false otherwise
     */
    public boolean equals(Object p_scpiScpInfo)
    {
        boolean l_equal = false;
        
        if ( p_scpiScpInfo != null && p_scpiScpInfo instanceof ScpiScpInfoData &&
            ((ScpiScpInfoData)p_scpiScpInfo).getScpId().equals(this.i_scpId))
        {
            l_equal = true;
        }
        
        return l_equal;
    }

}