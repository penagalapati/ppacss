//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BoneBonusElementData.java
// DATE            :       14-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of bonus element data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Object holding bonus element data configuration. */
public class BoneBonusElementData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BoneBonusElementData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Bonus element. */
    private String              i_bonusSchemeId;
    
    /** Bonus element description. */
    private String              i_keyElements;
    
    /** Bonus  non key elements. */
    private String              i_nonkeyElements;
    
    /** Bonus amount without currency. */
    private Double              i_bonusAmount;
    
    /** Bonus percent. */
    private Float              i_bonusPercent;
    
    /** Bonus adjustment code. */
    private String              i_bonusAdjCode;
    
    /** Bonus adjustment type. */
    private String              i_bonusAdjType;
    
    /** Dedicated account id. */
    private Integer             i_dedAccId;
    
    /** Dedicated account expiry days. */
    private Integer             i_dedAccExpDays;
    
    /** Boolean to indicate if the element is operational. Default is false. */
    private boolean             i_bonusActive;

    /** Defines if this record has been deleted. */
    private boolean             i_deleted;

    /** The opid that last updated this record. */
    private String              i_opid;

    /** The date/time of the last update. */
    private PpasDateTime        i_genYmdhms  = new PpasDateTime("");
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of <code>BoneBonusElementData</code>.
     * @param p_bonusSchemeId 
     * The bonus element represented by this object.
     * @param p_keyElements The key elements.
     * @param p_nonkeyElements The nonkey elements.
     * @param p_bonusAmount The Bonus Amount.
     * @param p_bonusPercent The Bonus Percent.
     * @param p_bonusAdjCode The Bonus Adjustment Code.
     * @param p_bonusAdjType The Bonus Adjustment Type.
     * @param p_dedAccId The dedicated Account Id.
     * @param p_dedAccExpDays The dedicated account expiry dates.
     * @param p_bonusActive boolean to indicate if element is active.
     * @param p_deleted Whether this record is marked as deleted in the database.
     * @param p_opid The opid that last updated this record.
     * @param p_genYmdhms date/time of the last update.
     */
    public BoneBonusElementData(String  p_bonusSchemeId,
                                String  p_keyElements,
                                String  p_nonkeyElements,
                                Double  p_bonusAmount,
                                Float   p_bonusPercent,
                                String  p_bonusAdjCode,
                                String  p_bonusAdjType,
                                Integer p_dedAccId,
                                Integer p_dedAccExpDays,
                                boolean p_bonusActive,
                                boolean p_deleted,
                                String  p_opid,
                                PpasDateTime p_genYmdhms)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonusSchemeId = p_bonusSchemeId;
        i_keyElements = p_keyElements;
        i_nonkeyElements = p_nonkeyElements;
        i_bonusAmount = p_bonusAmount;
        i_bonusPercent = p_bonusPercent;
        i_bonusAdjCode = p_bonusAdjCode;
        i_bonusAdjType = p_bonusAdjType;
        i_dedAccId = p_dedAccId;
        i_dedAccExpDays = p_dedAccExpDays;
        i_bonusActive = p_bonusActive;
        i_deleted = p_deleted;
        i_opid = p_opid;
        i_genYmdhms = p_genYmdhms;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the bonus element this this object represents.
     * @return the bonus element
     */
    public String getBonusSchemeId()
    {
        return i_bonusSchemeId;
    }

     /**
     * Get the deleted status of this record.
     * @return <code>true</code> if the record is deleted, else <code>false</code>
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }

    /**
     * The opid that last updated this record.
     * @return opid of last update
     */
    public String getOpid()
    {
        return i_opid;
    }

    /**
     * The date/time that this record was last updated.
     * @return last update date/time
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }
    
    /**
     * The Bonus Amount as a numeric (no currency).
     * @return Bonus Amount.
     */    
    public Double getBonusAmount()
    {
        return i_bonusAmount;
    }
    
    /**
     * Indicates if the element is active.
     * @return <code>true</code> if the element is active, else <code>false</code>
     */  
    public boolean isBonusActive()
    {
        return i_bonusActive;
    }
    
    /**
     * The Bonus Percent.
     * @return Bonus Percent.
     */    
    public Float getBonusPercent()
    {
        return i_bonusPercent;
    }
    
    /**The Bonus Adjustment Code.
     * @return Bonus Adjustment Code.
     */
    public String getBonusAdjCode()
    {
        return i_bonusAdjCode;
    }
    
    /**The Bonus Adjustment Type.
     * @return Bonus Adjustment Type.
     */
    public String getBonusAdjType()
    {
        return i_bonusAdjType;
    }
    
    /**The Dedicated Account Id.
     * @return Dedicated Account Id.
     */
    public Integer getDedAccId()
    {
        return i_dedAccId;
    }
    
    /**The Dedicated Account Expiry Days.
     * @return Dedicated Account Expiry Days.
     */
    public Integer getDedAccExpDays()
    {
        return i_dedAccExpDays;
    }
    
    /**The Key elements.
     * @return Key Elements.
     */
    public String getKeyElements()
    {
        return i_keyElements;
    }
    
    /**The NonKey elements.
     * @return NonKey Elements.
     */
    public String getNonkeyElements()
    {
        return i_nonkeyElements;
    }
}
