////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentHistoryData.java
//      DATE            :       10-Jun-2005
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Superclass of adjustment data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+---------------------
// dd/mm/yy | <Name>     | <Brief description of change>   | <reference>
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** A data object representing a single adjustment history record. */
public abstract class AdjustmentHistoryData extends EventHistoryData
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------
    /** Standard class name constant to be used in calls to Middleware methods. */
    private static final String C_CLASS_NAME = "AdjustmentHistoryData";

    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------
    /** The Type of the adjustment. */
    private String       i_type                     = null;

    /** The Code of the adjustment. */
    private String       i_code                     = null;

    /** The Amount of the adjustment. */
    private Money       i_appliedAmount             = null;

    /** Original amount in which the adjustment was made. */
    private Money       i_origAmount                = null;

    /** Mobile Number of the customer that instigated the adjustment. */
    private Msisdn       i_initiatingMsisdn         = null;

    /** Balance before the adjustment. */
    private Money        i_balanceBefore;
    
    /** Balance after the adjustment. */
    private Money        i_balanceAfter;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Constructor that takes a full set of Adjustment details.
     *
     * @param p_request             The request being processed.
     * @param p_dateTime            The Adjustment dateTime.
     * @param p_type                The Adjustment type.
     * @param p_code                The Adjustment code.
     * @param p_appliedAmount       The Adjustment amount.
     * @param p_opid                The Adjustment Operator Id.
     * @param p_custId              Customer id of the subordinate account.
     * @param p_initiatingMsisdn    Mobile number of customer that caused the adjustment to be applied.
     * @param p_origAmount          The original Adjustment amount.
     * @param p_balanceBefore       Account Balance before this transaction.
     * @param p_balanceAfter        Account Balance after this transaction.
     */
    public AdjustmentHistoryData(PpasRequest     p_request,
                                 PpasDateTime    p_dateTime,
                                 String          p_type,
                                 String          p_code,
                                 Money           p_appliedAmount,
                                 String          p_opid,
                                 String          p_custId,
                                 Msisdn          p_initiatingMsisdn,
                                 Money           p_origAmount,
                                 Money           p_balanceBefore,
                                 Money           p_balanceAfter)
    {
        super(p_request, p_custId, p_opid, p_dateTime);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 11011, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_type              = p_type;
        i_code              = p_code;
        i_appliedAmount     = p_appliedAmount;
        i_origAmount        = p_origAmount;
        i_initiatingMsisdn  = p_initiatingMsisdn;
        i_balanceBefore     = p_balanceBefore;
        i_balanceAfter      = p_balanceAfter;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,  PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 11013, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------

    /**
     * Returns the Mobile number of the customer whose query caused the adjustment.
     *
     * @return Mobile number of the customer that made the query.
     */
    public Msisdn getInitiatingMsisdn()
    {
        return i_initiatingMsisdn;
    }

    /**
     * Returns the dateTime the adjustment was posted.
     *
     * @return The date/time the adjustment was posted.
     */
    public PpasDateTime getDateTime()
    {
        return getEventDateTime();
    }

    /**
     * Returns the adjustment type.
     *
     * @return The type of the adjustment.
     */
    public String getAdjustmentType()
    {
        return i_type;
    }

    /**
     * Returns the adjustment code.
     *
     * @return The adjustment code.
     */
    public String getAdjustmentCode()
    {
        return i_code;
    }

    /**
     * Returns the adjustment amount.
     *
     * @return The value of the adjustment.
     */
    public Money getAppliedAmount()
    {
        return i_appliedAmount;
    }

    /**
     * Returns the adjustment description.
     *
     * @return The adjustment description.
     */
    public abstract String getAdjustmentText();

    /**
     * Returns the Id of the Operator that approved the adjustment, if it was held for approval. This is a
     * default implementation that returns an empty string - it should be over-ridden where there is the
     * concept of approving adjustments.
     *
     * @return Operator identifier.
     */
    public String getApprovingOpid()
    {
        return "";
    }

    /**
     * Returns the original adjustment amount.
     *
     * @return Original amount of adjustment.
     */
    public Money getOrigAmount()
    {
        return i_origAmount;
    }

    /** Get the balance before the adjustment.
     * 
     * @return Account balance before the adjustment.
     */
    public Money getBalanceBefore()
    {
        return i_balanceBefore;
    }
    
    /** Get the balance after the adjustment.
     * 
     * @return Account balance after the adjustment.
     */
    public Money getBalanceAfter()
    {
        return i_balanceAfter;
    }
    
    /**
     * Equals method used for testing.
     *
     * @param p_adjData <code>AdjustmentData</code> object to be compared with
     *
     * @return <code>true</code> if the current object and the supplied object are the
     * same from a business logic point of view, else <code>false</code>
     */
    public boolean equals(Object p_adjData)
    {
        if (p_adjData instanceof AdjustmentHistoryData)
        {
            AdjustmentHistoryData l_adjData = (AdjustmentHistoryData)p_adjData;

            if (!getAppliedAmount().equals(l_adjData.getAppliedAmount()))
            {
                return false;
            }

            if (!getAdjustmentCode().equals(l_adjData.getAdjustmentCode()))
            {
                return false;
            }

            if (Integer.parseInt(getCustId()) != Integer.parseInt(l_adjData.getCustId()))
            {
                return false;
            }

            if (!getDateTime().equals(l_adjData.getDateTime()))
            {
                return false;
            }

            if (!getAdjustmentText().equals(l_adjData.getAdjustmentText()))
            {
                return false;
            }

            if (!getOpid().equals(l_adjData.getOpid()))
            {
                return false;
            }

            if (!getOrigAmount().equals(l_adjData.getOrigAmount()))
            {
                return false;
            }

            if (!getAdjustmentType().equals(l_adjData.getAdjustmentType()))
            {
                return false;
            }

            return true;
        }

        return false;
    }

    /** Get the event in a form suitable for sorting. This approach calls the standard formatter with
     * <code>null</code> formatters.
     * @return String representing the event.
     */
    protected String getSortSubKey()
    {
        return getEventText(null, null);
    }
}
