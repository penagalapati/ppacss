////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountGroupId.Java
//      DATE            :       10-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Class containing Account Group Id information.
//                              This Data is used in Customer Segmentation.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;

/**
 * Class containing Account Group Id information.
 * This Data is used in Customer Segmentation.
 */
public class AccountGroupId extends DataObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccountGroupId";

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final long C_MAX_VALUE = 2147483647;

    //------------------------------------------------------------------------
    // Instance Variables.
    //------------------------------------------------------------------------
    /** Account Group Id. */
    private long i_accountGroupId;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    /**
     * Creates a new AccountGroupId Object and initialises data.
     * 
     * @param p_accountGroupId String representation of a Account Group Id.
     * @throws PpasServiceFailedException if the conversion from a String fails.
     */
    public AccountGroupId (String p_accountGroupId)
        throws PpasServiceFailedException
    {
        super();

        try
        {
            i_accountGroupId = Long.parseLong(p_accountGroupId);
        }
        catch (NumberFormatException e)
        {
            throw new PpasServiceFailedException(
                         C_CLASS_NAME,
                         C_CLASS_NAME,
                         88705,
                         this,
                         null,
                         0,
                         ServiceKey.get().invalidAccountGroupId(p_accountGroupId),
                         e);
        }
        
        checkRange();
    } // End of public constructor 

    /**
     * Creates a new AccountGroupId Object and initialises data.
     * 
     * @param p_accountGroupId Account Group Id.
     * @throws PpasServiceFailedException if the Account group Id is out of range
     */
    public AccountGroupId (long p_accountGroupId)
        throws PpasServiceFailedException
    {
        i_accountGroupId = p_accountGroupId;

        checkRange();
    }
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** Returns the Account Group Id as a long.
     * @return the Account Group Id as a long.
     */
    public long getValue()
    {
        return i_accountGroupId;
    }

    /** Returns String representation of the Account Group Id.
     * @return String representation of the Account Group Id.
     */
    public String toString()
    {
        return Long.toString(i_accountGroupId);
    }

    /** equals() method overloading.
     * @param p_obj the Object to compare
     * @return true if current Object equals passed-in Object.
     */
    public boolean equals(Object p_obj)
    {
        boolean l_ret = false;
        if ( p_obj != null && 
             p_obj instanceof AccountGroupId  &&
             ((AccountGroupId)p_obj).i_accountGroupId == this.i_accountGroupId ) 
        {
            l_ret = true;
        }
        return l_ret;
    }

    //------------------------------------------------------------------------
    // Private Methods.
    //------------------------------------------------------------------------
    /**
     * Check if the account group id is in range.
     * @throws PpasServiceFailedException if the Service Offerings is out of range
     */
    private void checkRange()
        throws PpasServiceFailedException
    {
        if (i_accountGroupId < 0 || i_accountGroupId > C_MAX_VALUE)
        {           
            throw new PpasServiceFailedException(
                         C_CLASS_NAME,
                         C_CLASS_NAME,
                         89705,
                         this,
                         null,
                         0,
                         ServiceKey.get().invalidAccountGroupId(toString()));
        }
    }
} // End of public class AccountGroupId

