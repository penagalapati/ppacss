////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CdgrAcceCardGroupCache.Java
//      DATE            :       03-Apr-2001
//      AUTHOR          :       Erik Clayton
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A cache of a CdgrAcceCardGroupDataSet object
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CdgrAcceCardGroupSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * A cache of a CdgrAcceCardGroup object.
 */
public class CdgrAcceCardGroupCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CdgrAcceCardGroupCache";

    //------------------------------------------------------------------------
    // Private attributes
    //------------------------------------------------------------------------

    /** Data set containing all card group records in this cache. */
    private CdgrAcceCardGroupDataSet i_allCardGroupSet = null;

    /**
     * Data set containing available card group records in this cache.
     * This does not include records marked as deleted.
     */
    private CdgrAcceCardGroupDataSet i_availableCardGroupSet = null;

    /** SQL service used to select the set from the database. */
    private CdgrAcceCardGroupSqlService i_cdgrAcceCardGroupSqlService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new cdgr cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public CdgrAcceCardGroupCache(PpasRequest    p_request,
                                  Logger         p_logger,
                                  PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing CdgrAcceCardGroupCache()");
        }

        i_cdgrAcceCardGroupSqlService =
                 new CdgrAcceCardGroupSqlService(
                                        p_request,
                                        p_logger );

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed CdgrAcceCardGroupCache()");
        }

    } // End of public constructor
      //         CdgrAcceCardGroupCache(PpasRequest, long, Logger)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns set of all card groups in this cache.
     * @return Data set containing all card groups in this cache.
     */
    public CdgrAcceCardGroupDataSet getCdgrAcceCardGroupSet()
    {
        return(i_allCardGroupSet);

    } // End of public method getCdgrAcceCardGroupSet

    /** Returns set of available card groups in this cache.
     * This does not include those marked as deleted.
     * @return Data set containing available card groups in this cache.
     */
    public CdgrAcceCardGroupDataSet getAvailable()
    {
        return (i_availableCardGroupSet);
    }

    /** Returns the card group details associated with the given card group.
     *  @param p_cardGroup The group group identifier defining the record
     *                     whose details are to be returned.
     *  @return The card group data associated with the given card group.
     */
    public CdgrAcceCardGroupData getCardGroupDetails (String p_cardGroup)
    {
        CdgrAcceCardGroupData l_record = null;

        if (i_allCardGroupSet != null)
        {
            l_record = i_allCardGroupSet.getCardGroupDetails (p_cardGroup);
        }
        return (l_record);

    } // end method getCardGroupDetails

    /**
     * Reloads the static configuration into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10140, this,
                "Entered reload");
        }

        i_allCardGroupSet =
                      i_cdgrAcceCardGroupSqlService.readAll(
                                                p_request,
                                                p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all card groups and old list of
        // available card groups, but rather than synchronise this is
        // acceptable.
        i_availableCardGroupSet = new CdgrAcceCardGroupDataSet(
                           p_request,
                           i_logger,
                           i_allCardGroupSet.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10150, this,
                "Leaving reload");
        }

        return;

    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allCardGroupSet, i_availableCardGroupSet);
    }
}

