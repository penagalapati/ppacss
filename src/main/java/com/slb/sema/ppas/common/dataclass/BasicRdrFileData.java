////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BasicRdrFileData.java
//      DATE            :       13-Oct-2003
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS00_DEV_SS_71
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Encapsulates basic data about an RDR file
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 06/03/06 | A Kutthan  | Added additional parameter      | 
//          |            | within constructor for the file | PpacLon#1931/8017
//          |            | logged date/time variable       |
//----------+------------+---------------------------------+--------------------
// 06/03/06 | A Kutthan  | Added getLogDateTime() method   | PpacLon#1931/8017
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Encapsulates basic RDR file data. */
public class BasicRdrFileData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BasicRdrFileData";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Stream identifier. */
    private String        i_stream;
    
    /** Host that supplied the file. */
    private String        i_hostName;
    
    /** Sequence number of the file. */
    private int           i_sequenceNumber;
    
    /** Date time that the file was generated. */
    private PpasDateTime  i_genDateTime;
    
    /** Date time that the file was logged. */
    private PpasDateTime  i_logDateTime;
    
    /** End of stream indicator. */
    private boolean       i_endOfBatch;

    /** Weighting indicator. This is essentially the number of the series within the data set. */
    private int           i_weighting;
    
    /** Flag indicating whether the weighting has been set or not. */
    private boolean       i_weightingSet;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Full constructor.
     * 
     * @param p_stream          Stream identifier.
     * @param p_hostName        Name of the hostthat supplied the file.
     * @param p_sequenceNumber  Sequence number of the file.
     * @param p_genDateTime     Date/time that the file was generated.
     * @param p_logDateTime     Date/time that the file was logged.
     * @param p_endOfBatch      End of stream indicator.
     */
    public BasicRdrFileData(String        p_stream,
                            String        p_hostName,
                            int           p_sequenceNumber,
                            PpasDateTime  p_genDateTime,
                            PpasDateTime  p_logDateTime,
                            boolean       p_endOfBatch)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing");
        }
        
        i_stream         = p_stream;
        i_hostName       = p_hostName;
        i_sequenceNumber = p_sequenceNumber;
        i_genDateTime    = p_genDateTime;
        i_logDateTime    = p_logDateTime;
        i_endOfBatch     = p_endOfBatch;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10005, this,
                            "Constructed");
        }
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /**
     * @return end of batch indicator
     */
    public boolean isEndOfBatch()
    {
        return i_endOfBatch;
    }

    /** Get the date/time of this file. Note: This is taken from the file name rather than the operating system.
     * @return generated date time of the file
     */
    public PpasDateTime getGenDateTime()
    {
        return i_genDateTime;
    }
    
    /** Get the date/time this file was stored in RFCI.
     * @return logged date time of the file
     */
    public PpasDateTime getLogDateTime()
    {
        return i_logDateTime;
    }

    /** Get the name of the node that sent this file.
     * @return host name of the file
     */
    public String getHostName()
    {
        return i_hostName;
    }

    /**
     * @return sequence number of the file
     */
    public int getSequenceNumber()
    {
        return i_sequenceNumber;
    }

    /**
     * @return stream identifier
     */
    public String getStream()
    {
        return i_stream;
    }
 
    /** Set the weighting for this record. The weighting identifies which series the data belongs to in a
     * set of data that contains rolled over data. Note: The weighting can only be set after all data has
     * been ordered and analysed.
     * 
     * @param p_weighting Number of the series.
     */
    public void setWeighting(int p_weighting)
    {
        i_weighting    = p_weighting;
        i_weightingSet = true;
    }
    
    /** Get the weighting for this record. The weighting identifies which series the data belongs to in a
     * set of data that contains rolled over data. Note: The weighting can only be set after all data has
     * been ordered and analysed.
     * 
     * @return Number of the series.
     */
    public int getWeighting()
    {
        return i_weighting;
    }

    /** Has weighting been set for this record ?
     * 
     * @return True if weighting has already been set.
     */
    public boolean isWeightingSet()
    {
        return i_weightingSet;
    }
}
