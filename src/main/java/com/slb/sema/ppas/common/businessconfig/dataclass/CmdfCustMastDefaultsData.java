////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CmdfCustMastDefaultsData.Java
//      DATE            :      28-Feb-2001
//      AUTHOR          :      Sally Wells
//      REFERENCE       :       
//
//      COPYRIGHT       :      SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :      db table cmdf_cust_mast_defaults, only the rows
//                             used by PPAS are used here.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 03/09/02 | M Hickman  | Added TBAU and Dedicated        | PpaLon#1517/6395
//          |            | Accounts data to the CMDF cache.|
//----------+------------+---------------------------------+--------------------
// 05/08/03 | Olivier    | Added attributes:               | PpacLon#273 /
//          | Duparc     | promoPlan, defltAnnLang         | CR PpacLon#33
//----------+------------+---------------------------------+--------------------
// 16/09/03 | Remi       | Changes Made to reflect the     | CR#15/440
//          | Isaacs     | CMDF_CUST_MAST_DEFAULTS table   |
//          |            | as defined in                   |
//          |            | PRD_ASCS00_SYD_AS_003           |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents the database table cmdf_cust_mast_defaults.
 */
public class CmdfCustMastDefaultsData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CmdfCustMastDefaultsData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Market. */
    private  Market   i_cmdfMarket = null;
    
    /** Customer class for enquiry & reporting purposes. */
    private  ServiceClass   i_cmdfCustClass = null;
    
    //CR# 15/440 [Begin]
    /** Default Language for announcements on the network. */
    private String    i_cmdfDefaultLanguage = null;
    
    /** The operator identifier of the operator that either created
     *  the row or performed the last update.
     */
    private String      i_cmdfOpid = null;
    
    /** The date and time of the last modification to this row or table. */
    private PpasDateTime  i_cmdfYmdhms = null;
    
    /** Indicator that cmdf row has been deleted, if it is "*". **/
    private  char     i_cmdfDeleteFlag = ' ';
    
    /** Currency of tariffs associated with service class on network. **/
    private  PpasCurrency   i_cmdfCurrency = null;

    /** The Promotion Plan. */
    private String    i_cmdfPromoPlan = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new row of CmdfCustMastDefaultsData and initialises data.
     * 
     * @param p_request The request to process.
     * @param p_cmdfMarket The market associated with the data.
     * @param p_cmdfCustClass The class that defines the data.
     * @param p_cmdfDefaultLanguage The default language for this class.
     * @param p_cmdfOpid The operator that defined or last updated this class.
     * @param p_cmdfYmdhms The date/time this class was last updated.
     * @param p_cmdfDeleteFlag Flag indicating whether the class is marked as deleted.
     * @param p_cmdfCurrency The currency in which accounts on this class will be stored.
     * @param p_cmdfPromoPlan The default promotion plan associated with this class.
     */
    public CmdfCustMastDefaultsData(
       PpasRequest        p_request,
        Market             p_cmdfMarket,
        ServiceClass       p_cmdfCustClass,
        String             p_cmdfDefaultLanguage,
        String             p_cmdfOpid,
        PpasDateTime       p_cmdfYmdhms,  
        char               p_cmdfDeleteFlag, 
        PpasCurrency       p_cmdfCurrency,
        String             p_cmdfPromoPlan)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing CmdfCustMastDefaultsData(...)");
        }

        i_cmdfMarket           = p_cmdfMarket;
        i_cmdfCustClass        = p_cmdfCustClass;
        i_cmdfDefaultLanguage  = p_cmdfDefaultLanguage;
        i_cmdfOpid             = p_cmdfOpid;
        i_cmdfYmdhms           = p_cmdfYmdhms;
        i_cmdfDeleteFlag       = p_cmdfDeleteFlag;
        i_cmdfCurrency         = p_cmdfCurrency;        
        i_cmdfPromoPlan        = p_cmdfPromoPlan;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed CmdfCustMastDefaultsData(...)");
        }
    } // End of public constructor
      // CmdfCustMastDefaultsData(PpasRequest, long, Logger, ...)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the market.
     * 
     * @return The market with which this class is associated.
     */
    public Market getCmdfMarket()
    {
        return(i_cmdfMarket);
    }

    /** Returns the cmdfCustClass.
     * 
     * @return The customer class (identifier).
     */
    public ServiceClass getCmdfCustClass()
    {
        return(i_cmdfCustClass);
    }

    /** Sets the cmdfCurrency.
     * 
     * @param p_cmdfCurrency The new currency that will apply to this class.
     */
    public void setCmdfCurrency(PpasCurrency p_cmdfCurrency)
    {
        i_cmdfCurrency = p_cmdfCurrency;
    }


    /** Returns the cmdfCurrency.
     * 
     * @return The currency associated with this class.
     */
    public PpasCurrency getCmdfCurrency()
    {
        return(i_cmdfCurrency);
    }

    /** Returns true if valid language has been marked as deleted.
     * 
     * @return Flag indicating whether the class has been deleted.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if ('*' == i_cmdfDeleteFlag)
        {
            l_deleted = true;
        }

        return(l_deleted);
    }

    /**
     * Returns a string representation of this object (suitable for trace/debug).
     * 
     * @return String representation of the class.
     */
    public String toString()
    {
        StringBuffer           l_buffer = new StringBuffer();

        l_buffer.append(  "cmdfSrva="       + i_cmdfMarket.getSrva());
        l_buffer.append(", cmdfSloc="       + i_cmdfMarket.getSloc());
        l_buffer.append(", cmdfCustClass="  + i_cmdfCustClass);
        l_buffer.append(", cmdf_default_language=" + i_cmdfDefaultLanguage);
        l_buffer.append(", cmdf_opid="      + i_cmdfOpid);
        l_buffer.append(", cmdf_ymdhms="    + i_cmdfYmdhms);
        l_buffer.append(", cmdfCurrency="   + i_cmdfCurrency);
        l_buffer.append(", cmdfDeleteFlag=" + i_cmdfDeleteFlag);
        l_buffer.append(", cmdfPromoPlan="  + i_cmdfPromoPlan);

        return(l_buffer.toString());
    }

    /** Returns the Default Language for announcements on the network.
     * 
     * @return The default language for the class.
     */
    public String getDefaultLanguage()
    {
        return i_cmdfDefaultLanguage;
    }

    /** Returns the Operator identifier of the operator that either 
     *  created the row or performed the last update.
     * 
     * @return The operator identifier that last changed this class.
     */
    public String getOpid()
    {
        return i_cmdfOpid;
    }

    /** Returns the date and time of the last modification to
     *  this row of the table.
     * 
     * @return The date/time the class was last changed.
     */
    public PpasDateTime getYmdhms()
    {
        return i_cmdfYmdhms;
    }
    
    /** Get the default promotion plan.
     * 
     * @return The default promotion plan associated with this class.
     */
    public String getPromotionPlan()
    {
        return i_cmdfPromoPlan;  
    }
}
