////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       VospVoucherSplitsData.Java
//      DATE            :       20-Aug-2002
//      AUTHOR          :       Nick Fletcher
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_382
//                              PpaLon#1500/6266
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A record defining a voucher split (or division)
//                              (corresponding to a single row of the
//                              vosp_voucher_splits table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi Isaacs|  Remove i_srva, i_sloc, and     | CR#15/440
//          |            | i_vospType variables from the   |
//          |            | constructor and the get methods |
//----------+------------+---------------------------------+--------------------
// 10/03/06 | M Erskine  | Changes for 10 dedicated accounts| CR#2005
//          |            | VOSP table changed and related  |  PRD_ASCS00_GEN_CA68
//          |            | VOSD table added.               |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of voucher split (or decision) 
 * data. Each record represents a single row of the vosp_voucher_splits table.
 */
public class VospVoucherSplitsData extends DataObject
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------    

    /** Constant defining the maximum number of dedicated accounts allowed
     *  in a division.
     */
    public static final int  C_MAX_NO_OF_DEDICATED_ACCOUNTS = 2147483647;

    /** Constant used to indicate that a dedicated account hs not been defined.
     */
    public static final int  C_DEDICATED_ACCOUNT_DOES_NOT_EXIST = -1;

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "VospVoucherSplitsData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------


    /** Identifier of the selected division (corresponds to the 
     *  vosp_division_id column of the vosp table).
     */
    private String             i_divisionId;

    /** Date on which this division became/becomes active. */
    private PpasDate           i_startDate;

    /** Service class that this division belongs to. */
    private ServiceClass       i_serviceClass;

    /** Date on which this division ceases/ceased to be active. */
    private PpasDate           i_endDate;

    /** Percentage of the recharge value to be applied to the master account
     *  for this division.
     */
    private int                i_mastPct;

    /** Array of up to 10 sets of division data for each of the dedicated
     *  accounts within this division.
     */
    private VosdVoucherSplitsDivisionsData  i_dedicatedAccounts[];


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new voucher split data object and initialises instance
     * variables.
     * 
     * @param p_request The request to process.
     * @param p_divisionId The key to this division data.
     * @param p_startDate Date from which the split applies.
     * @param p_serviceClass The service class that is associated with the division.
     * @param p_endDate Date until which the division applies. This can be null/empty if there is no end date.
     * @param p_mastPct The percentage of the transaction that is applied to the master account.
     * @param p_divisions The dedicated account divisions from the vosd_voucher_split_divisions table.
     * @param p_opid Operator making the underlying transaction.
     * @param p_genYmdhms Date/time of the underlying transaction.
     */
    public VospVoucherSplitsData(
        PpasRequest                      p_request,
        String                           p_divisionId,
        PpasDate                         p_startDate,
        ServiceClass                     p_serviceClass,
        PpasDate                         p_endDate,
        int                              p_mastPct,
        VosdVoucherSplitsDivisionsData[] p_divisions,
        String                           p_opid,
        PpasDateTime                     p_genYmdhms)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_divisionId   = p_divisionId;
        i_startDate    = p_startDate;
        i_serviceClass = p_serviceClass;
        i_endDate      = p_endDate;
        i_mastPct      = p_mastPct;
        i_dedicatedAccounts = p_divisions;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get identifier of the selected division (corresponds to the 
     *  vosp_division_id column of the vosp table).
     *  @return Identifier of the selected division. This is either the card
     *          group for a voucher/payment type division, or the promotion
     *          plan for a promotion type division.
     */
    public String getDivisionId()
    {
        return (i_divisionId);
    }

    /** Get date on which this division became/becomes active.
     *  @return Date on which this division became/becomes active.
     */
    public PpasDate getStartDate()
    {
        return (i_startDate);
    }

    /** Get service class that this division belongs to.
     *  @return Service class that this division belongs to.
     */
    public ServiceClass getServiceClass()
    {
        return (i_serviceClass);
    }

    /** Get date on which this division ceases/ceased to be active.
     *  @return Date on which this division ceases/ceased to be active.
     */
    public PpasDate getEndDate()
    {
        return (i_endDate);
    }

    /** Get percentage of the recharge value to be applied to the master account
     *  for this division.
     *  @return Percentage of the recharge value to be applied to the master 
     *          account for this division.
     */
    public int getMastPct()
    {
        return (i_mastPct);
    }
    
    /**
     *  Returns the division data for each of the dedicated accounts within this division.
     *  @return Associated dedicated account division data for this division.
     */
    
    public VosdVoucherSplitsDivisionsData[] getDedicatedAccountDivisionData()
    {
        return i_dedicatedAccounts;
    }

    /** Get the identifier of a given dedicated account.
     *  The dedicated account is given by the <code>p_accountNo</code> parameter (and is in the range 
     *  [1..<code>C_MAX_NO_OF_DEDICATED_ACCOUNTS</code>]). If there is no 
     *  account (e.g. <code>p_accountNo</code>=3, but only accounts 1 and 2 are defined in this
     *  division), then <code>C_DEDICATED_ACCOUNT_DOES_NOT_EXIST</code> is returned).
     *  @param p_accountNo The number of the dedicated account whose identifier
     *  is to be returned.
     *  @return The identifier of dedicated account <code>p_accountNo</code>.
     *          If no dedicated account <code>p_accountNo</code> exists, then
     *          <code>C_DEDICATED_ACCOUNT_DOES_NOT_EXIST</code> is returned.
     */
    public int getDacId (int p_accountNo)
    {
        int l_accountNo = C_DEDICATED_ACCOUNT_DOES_NOT_EXIST;

        if ( (i_dedicatedAccounts != null) && (i_dedicatedAccounts.length >= p_accountNo) &&
             (p_accountNo >= 1) && (p_accountNo <= C_MAX_NO_OF_DEDICATED_ACCOUNTS))
        {
            if (i_dedicatedAccounts[p_accountNo - 1] != null &&
                i_dedicatedAccounts[p_accountNo - 1].getIdentifier() != 0)
            {
                l_accountNo = 
                      i_dedicatedAccounts[p_accountNo - 1].getIdentifier();
            }
        }

        return (l_accountNo);

    } // end method 'getDacId'


    /** Get the percentage to be applied to a given dedicated account.
     *  The dedicated account is given by the <code>p_accountNo</code> parameter (and is in the range 
     *  [1..<code>C_MAX_NO_OF_DEDICATED_ACCOUNTS</code>]). If there is no 
     *  account (e.g. <code>p_accountNo</code>=3, but only accounts 1 and 2 are defined in this
     *  division), then 0 is returned).
     *  @param p_accountNo The number of the dedicated account whose percentage
     *  is to be returned.
     *  @return The percentage to be applied to dedicated account
     *          <code>p_accountNo</code>.
     *          If no dedicated account <code>p_accountNo</code> exists, then
     *          0 is returned.
     */
    public int getDacPercentage (int p_accountNo)
    {
        int l_percentage = 0;

        if ((p_accountNo >= 1) && 
            (p_accountNo <= C_MAX_NO_OF_DEDICATED_ACCOUNTS))
        {
            if (i_dedicatedAccounts[p_accountNo - 1].getIdentifier() != 0)
            {
                l_percentage = 
                      i_dedicatedAccounts[p_accountNo - 1].getPercentage();
            }
        }

        return (l_percentage);

    } // end method 'getDacPercentage'


    /** Get the end date to be applied to a given dedicated account.
     *  The dedicated account is given by the <code>p_accountNo</code> parameter (and is in the range 
     *  [1..<code>C_MAX_NO_OF_DEDICATED_ACCOUNTS</code>]). If there is no 
     *  account (e.g. <code>p_accountNo</code>=3, but only accounts 1 and 2 are defined in this
     *  division), then <code>null</code> is returned).
     *  @param p_accountNo The number of the dedicated account whose end date
     *  is to be returned.
     *  @return The end date to be applied to dedicated account
     *          <code>p_accountNo</code>.
     *          If no dedicated account <code>p_accountNo</code> exists, then
     *          <code>null</code> is returned.
     */
    public PpasDate getDacEndDate (int p_accountNo)
    {
        PpasDate l_endDate = null;

        if ((p_accountNo >= 1) && 
            (p_accountNo <= C_MAX_NO_OF_DEDICATED_ACCOUNTS))
        {
            if (i_dedicatedAccounts[p_accountNo - 1].getIdentifier() != 0)
            {
                l_endDate = 
                      i_dedicatedAccounts[p_accountNo - 1].getEndDate();
            }
        }

        return (l_endDate);

    } // end method 'getDacEndDate'


    /** Get the period (the offset in days from the transaction date)
     *  to be applied to a given dedicated account.
     * The dedicated account is given by the <code>p_accountNo</code> parameter (and is in the range 
     *  [1..<code>C_MAX_NO_OF_DEDICATED_ACCOUNTS</code>]). If there is no 
     *  account (e.g. <code>p_accountNo</code>=3, but only accounts 1 and 2 are defined in this
     *  division), then 0 is returned).
     *  @param p_accountNo The number of the dedicated account whose period
     *  is to be returned.
     *  @return The period to be applied to dedicated account
     *          <code>p_accountNo</code>.
     *          If no dedicated account <code>p_accountNo</code> exists, then
     *          0 is returned.
     */
    public int getDacPeriod (int p_accountNo)
    {
        int l_period = 0;

        if ((p_accountNo >= 1) && 
            (p_accountNo <= C_MAX_NO_OF_DEDICATED_ACCOUNTS))
        {
            if (i_dedicatedAccounts[p_accountNo - 1].getIdentifier() != 0)
            {
                l_period = 
                      i_dedicatedAccounts[p_accountNo - 1].getPeriod();
            }
        }

        return (l_period);

    } // end method 'getDacPeriod'
    
    /** Gets the extension type, which indicates from when the 'period' will commence to derive the 'new' 
     *  expiry date when funds added into dedicated account balance will expire.
     *  The dedicated account is given by the <code>p_accountNo</code> parameter (and is in the range 
     *  [1..<code>C_MAX_NO_OF_DEDICATED_ACCOUNTS</code>]). If there is no 
     *  account (e.g. <code>p_accountNo</code>=3, but only accounts 1 and 2 are defined in this
     *  division), then "" is returned).
     *  @param p_accountNo The number of the dedicated account whose period
     *  is to be returned.
     *  @return The extension type to be applied to dedicated account
     *          <code>p_accountNo</code>.
     *          If no dedicated account <code>p_accountNo</code> exists, then
     *          "" is returned.
     */
    public String getDacExtType (int p_accountNo)
    {
        String l_period = "";

        if ((p_accountNo >= 1) && 
            (p_accountNo <= C_MAX_NO_OF_DEDICATED_ACCOUNTS))
        {
            if (i_dedicatedAccounts[p_accountNo - 1].getIdentifier() != 0)
            {
                l_period = 
                      i_dedicatedAccounts[p_accountNo - 1].getExtType();
            }
        }

        return (l_period);

    } // end method 'getDacPeriod'

} // End of public class VospVoucherSplitsData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////