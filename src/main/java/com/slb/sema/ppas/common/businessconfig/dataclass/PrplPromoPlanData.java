////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID       :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      PrplPromoPlanData.Java
//      DATE            :      21-Feb-2001
//      AUTHOR          :      Matt Kirk
//
//      DESCRIPTION     :      A promotion plan (prpl_promo_plan).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 13/05/02 | N Fletcher | Changes so that prom_promotion  | PpaLon#1194/5659
//          |            | table data is stored here.      |
//----------+------------+---------------------------------+--------------------
// 10/03/06 | M Erskine  | Store the division id from      | PpacLon#2005/8080
//          |            | prom_promotion also             |
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group 2001
//
// The copyright in this work belongs to Sema Group. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Date;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single promotion plan (prpl_promo_plan).
 */
public class PrplPromoPlanData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrplPromoPlanData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Valid Promotion Plan ID. */
    private  String            i_prplPromoPlan = null;

    /** Valid Promotion Plan Description. */
    private  String            i_prplDescription = null;

    /** Date the Promotion Plan was created. */
    private  PpasDateTime      i_prplGenYMDHMS = null;

    /** OPID who created the Promotion Plan. */
    private  String            i_prplOpid = null;
    
    /** Flag indicating if Promotion Plan is deleted. */
    private  char              i_prplDelFlag = ' ';
    
    /** Identifies which division id this promotion plan is associated with. */
    private String             i_divisionId = null;

    /** Date/time from which this promotion plan becomes active. */
    private PpasDateTime       i_startDateTime;

    /** Date/time at which this promotion plan ceases to be active. */
    private PpasDateTime       i_endDateTime;

    /** The real Date/time at which this promotion plan ceases to be active. */
    private PpasDateTime       i_endDateTimeReal;

    /** Additional service days that will be awarded for this promotion
     *  as a percentage of those arising from the
     *  recharge/payment that lead to this promotion.
     */
    private int                i_servPeriodPct;

    /** Additional service days as an absolute value that will be awarded for
     *  this promotion.
     */
    private int                i_servPeriodAbs;

    /** Additional airtime days that will be awarded for this promotion
     *  as a percentage of those arising from the
     *  recharge/payment that lead to this promotion.
     */
    private int                i_airPeriodPct;

    /** Additional airtime days as an absolute value that will be awarded for
     *  this promotion.
     */
    private int                i_airPeriodAbs;

    /** Additional credit that will be awarded for this promotion
     *  as a percentage of the credit arising from the
     *  recharge/payment that lead to this promotion.
     */
    private int                i_creditPct;

    /** Additional credit as an absolute value that will be awarded for
     *  this promotion.
     */
    private Money              i_creditAbs;

    /** Total recharge value that must be achieved before this promotion is
     *  awarded.
     */
    private Money              i_qualLimit;

    /** Total number of refills (recharges/payments) that must have been made
     *  in order for this promotion to be awarded.
     */
    private int                i_refillCount;

    /** Total recharge value that must be achieved in order to progress to the
     *  next promotion plan tier.
     */
    private Money              i_progLimit;

    /** Total number of refills (recharges/payments) that must have been made
     *  in order to progress to the next promotion plan tier.
     */
    private int                i_progCount;

    /** The identifier of the next promotion plan that will be used when 
     *  promotion plan progression is achieved.
     */
    private String             i_nextTier;

    /** Default allocation period for the plan in days. */
    private int                i_defaultAllocPeriod;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new promotion plan object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_prplPromoPlan The promotion plan identifier.
     * @param p_prplDescription The description of the promotion plan.
     * @param p_prplGenYMDHMS Date/time this promotion plan was last updated.
     * @param p_prplOpid Operator who last updated this promotion plan.
     * @param p_prplDelFlag Flag indicating whether this promotion is marked as deleted.
     * @param p_divisionId Identifies which division id this promotion plan is associated with.
     * @param p_startDateTime Start date for this promotion plan.
     * @param p_endDateTime End date for this promotion plan. A null/empty value indicates the promotion will
     *                      never end.
     * @param p_servPeriodPct Percentage of Service period to apply as a result of this promotion.
     * @param p_servPeriodAbs Absolute number of Service period days to add as part of this promotion.
     * @param p_airPeriodPct Percentage of Airtime period to apply as a result of this promotion.
     * @param p_airPeriodAbs Absolute number of Airtime period days to add as part of this promotion.
     * @param p_creditPct Percentage of refill value to apply as part of this promotion.
     * @param p_creditAbs Absolute value to apply as part of this promotion.
     * @param p_qualLimit Qualifying limit to activate this promotion.
     * @param p_refillCount Number of refills needed before qualifying for this promotion.
     * @param p_progLimit Amount of money that must be spent to move to the next level.
     * @param p_progCount Number of refills before moving to the next level.
     * @param p_nextTier Promotion plan identifier of the next level.
     * @param p_defaultAllocPeriod Default allocation period for the plan.
     */
    public PrplPromoPlanData(
        PpasRequest            p_request,
        String                 p_prplPromoPlan,
        String                 p_prplDescription,
        PpasDateTime           p_prplGenYMDHMS,
        String                 p_prplOpid,
        char                   p_prplDelFlag,
        String                 p_divisionId,
        PpasDateTime           p_startDateTime,
        PpasDateTime           p_endDateTime,
        int                    p_servPeriodPct,
        int                    p_servPeriodAbs,
        int                    p_airPeriodPct,
        int                    p_airPeriodAbs,
        int                    p_creditPct,
        Money                  p_creditAbs,
        Money                  p_qualLimit,
        int                    p_refillCount,
        Money                  p_progLimit,
        int                    p_progCount,
        String                 p_nextTier,
        int                    p_defaultAllocPeriod)
    {

        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing PrplPromoPlanData(...)");
        }

        i_prplPromoPlan      = p_prplPromoPlan;
        i_prplDescription    = p_prplDescription;
        i_prplGenYMDHMS      = p_prplGenYMDHMS;
        i_prplOpid           = p_prplOpid;
        i_prplDelFlag        = p_prplDelFlag;
        
        i_divisionId         = p_divisionId;
        i_startDateTime      = p_startDateTime;
        i_endDateTime        = p_endDateTime;
        i_servPeriodPct      = p_servPeriodPct;
        i_servPeriodAbs      = p_servPeriodAbs;
        i_airPeriodPct       = p_airPeriodPct;
        i_airPeriodAbs       = p_airPeriodAbs;
        i_creditPct          = p_creditPct;
        i_creditAbs          = p_creditAbs;
        i_qualLimit          = p_qualLimit;
        i_refillCount        = p_refillCount;
        i_progLimit          = p_progLimit;
        i_progCount          = p_progCount;
        i_nextTier           = p_nextTier;
        i_defaultAllocPeriod = p_defaultAllocPeriod;

        // i_endDateTime should be just a date. 
        // This variable has been added as a hack to fix time related bugs.
        i_endDateTimeReal    = new PpasDateTime(p_endDateTime.toString_yyyyMMdd() + " 23:59:59");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed PrplPromoPlanData(...)");
        }
    } // End of public constructor
      //         PrplPromoPlanData(PpasRequest, long, Logger, ...)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the Promotion Plan ID.
     * 
     * @return Promotion plan identifier.
     */
    public String getPromoPlan()
    {
        return(i_prplPromoPlan);
    }

    /** Returns the Promotion Plan Description.
     * 
     * @return Description of this promotion plan.
     */
    public String getPromoDescription()
    {
        return(i_prplDescription);
    }

    /** Returns the date the Promotion Plan was generated.
     * 
     * @return Date/time of last change to this promotion plan.
     */
    public Date getPromoGenDate()
    {
        return(i_prplGenYMDHMS);
    }

    /** Returns the OPID of the user who created the Promotion Plan.
     * 
     * @return Identifier of the operator that last changed this promotion plan.
     */
    public String getPromoOpid()
    {
        return(i_prplOpid);
    }
    
    /** Returns the division id associated with this promotion plan.
     * 
     * @return Division Id of this promotion plan.
     */
    public String getDivisionId()
    {
        return i_divisionId;
    }

    /** Get the date/time from which this promotion plan becomes active.
     * 
     * @return Date the promotion starts.
     */
    public PpasDateTime getStartDateTime()
    {
        return (i_startDateTime);
    }

    /** Get the date/time at which this promotion plan ceases to be active.
     * 
     * @return Date the promotion ends.
     */
    public PpasDateTime getEndDateTime()
    {
        return (i_endDateTime);
    }

    /** Get the additional service days that will be awarded for this promotion
     *  as a percentage of those arising from the recharge/payment that lead to this promotion.
     * 
     * @return Percentage of the Service days arising from a refill that will be additionally allocated as a
     *         result of this promotion.
     */
    public int getServPeriodPct()
    {
        return (i_servPeriodPct);
    }

    /** Get the additional service days as an absolute value that will be awarded for this promotion.
     * 
     * @return Absolute number of Service days arising from this promotion.
     */
    public int getServPeriodAbs()
    {
        return (i_servPeriodAbs);
    }

    /** Get the additional airtime days that will be awarded for this promotion
     *  as a percentage of those arising from the recharge/payment that lead to this promotion.
     * 
     * @return Percentage of the Airtime days arising from a refill that will be additionally allocated as a
     *         result of this promotion.
     */
    public int getAirPeriodPct()
    {
        return (i_airPeriodPct);
    }

    /** Get the additional airtime days as an absolute value that will be awarded for this promotion.
     * 
     * @return Absolute number of Airtime days arising from this promotion.
     */
    public int getAirPeriodAbs()
    {
        return (i_airPeriodAbs);
    }

    /** Get the additional credit that will be awarded for this promotion
     *  as a percentage of the credit arising from the recharge/payment that lead to this promotion.
     * 
     * @return Percentage that will be applied to the refill value.
     */
    public int getCreditPct()
    {
        return (i_creditPct);
    }

    /** Get the Additional credit as an absolute value that will be awarded for
     *  this promotion.
     * 
     * @return Absolute value that will be applied to the account as a result of a refill that qualifies for
     *         this promotion plan.
     */
    public Money getCreditAbs()
    {
        return (i_creditAbs);
    }

    /** Get the total recharge value that must be achieved before this promotion is awarded.
     * @return The amount of money needed to qualify for this promotion.
     */
    public Money getQualLimit()
    {
        return (i_qualLimit);
    }

    /** Get the total number of refills (recharges/payments) that must have been made
     *  in order for this promotion to be awarded.
     * @return The number of refills needed to qualify for this promotion.
     */
    public int getRefillCount()
    {
        return (i_refillCount);
    }

    /** Get the total recharge value that must be achieved in order to progress to the
     *  next promotion plan tier.
     * 
     * @return The total refill value needed to qualify for the next promotion level.
     */
    public Money getProgLimit ()
    {
        return (i_progLimit);
    }

    /** Get the total number of refills (recharges/payments) that must have
     *  been made in order to progress to the next promotion plan tier.
     * 
     * @return The number of refills needed to qualify for the next promotion level.
     */
    public int getProgCount()
    {
        return (i_progCount);
    }

    /** Get the the identifier of the next promotion plan that will be used when 
     *  promotion plan progression is achieved.
     * 
     * @return The identifier of the next promotion plan level.
     */
    public String getNextTier()
    {
        return (i_nextTier);
    }

    /** Returns true if Promotion Plan has been marked as deleted.
     * 
     * @return Flag indicating whether the promotion plan is marked as deleted.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if ('*' == i_prplDelFlag)
        {
            l_deleted = true;
        }

        return(l_deleted);
    }

    /** 
     * Returns true if Promotion Plan is no longer valid. 
     * i.e. If its end date time is in the past. 
     * @return Flag indicating whether the promotion plan is no longer valid. 
     */
    public boolean isObsolete()
    {
        boolean      l_obsolete = false;
        PpasDateTime l_now = DatePatch.getDateTimeNow();

        if (i_endDateTime != null &&
            i_endDateTime.isSet() && 
            i_endDateTime.compareTo(l_now) < 0)
        {
            l_obsolete = true;
        }

        return(l_obsolete);
    }

    /** 
     * Returns a String containing the promotion plan code padded to p_codeWidth 
     * characters and the description.
     * 
     * @param p_codeWidth Defines width of field to display code in including
     *                    trailing spaces. Maximum value is 6 characters.
     * @return The promotion plan as a formatted string for display on a GUI.
     */
    public String getDisplayString(int p_codeWidth)
    {
        int l_codeWidth = (p_codeWidth > 6) ? 6 : p_codeWidth;
        String l_plan = i_prplPromoPlan + "      ";
        
        return (l_plan.substring(0, l_codeWidth) + i_prplDescription);
    }
    
    /** 
     * Return default allocation period for the plan in days.
     * @return Default allocation period for the plan in days.
     */
    public int getDefaultAllocPeriod()
    {
        return i_defaultAllocPeriod;
    }
    
    /** 
     * Returns either p_startDateTime plus the default allocation period of the plan or the end date of the
     * plan, whichever is the earlier. 
     * 
     * @param p_startDateTime The start date to add the defulat allocation period to.
     * @return A default end date.
     */
    public PpasDateTime getDefaultEndDateTime(PpasDateTime p_startDateTime)
    {
        // Default end date/time must be at 23:59:59.
        PpasDateTime l_endDateTime = new PpasDateTime(p_startDateTime.toString_yyyyMMdd() + " 23:59:59");

        l_endDateTime.add(PpasDate.C_FIELD_DAY, i_defaultAllocPeriod);

        return (i_endDateTimeReal.before(l_endDateTime)) ? i_endDateTimeReal : l_endDateTime;
    }    
}
