////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CmdfCustMastDefaultsCache.Java
//      DATE            :       16-Mar-2001
//      AUTHOR          :       Sally Wells
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A cacheing of cmdf_cust_mast_defaults business
//                              configuration service.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CmdfCustMastDefaultsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cacheing of the customer master defaults business configuration
 * service (cmdf_cust_mast_defaults).
 */
public class CmdfCustMastDefaultsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CmdfCustMastDefaultsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The SQL service used to load the data set. */
    private CmdfCustMastDefaultsSqlService i_cmdfCustMastDefaultsSqlService = null;

    /** Set of all CMDF rows (including ones marked as deleted). */
    private CmdfCustMastDefaultsDataSet i_allCmdfCustMastDefaults = null;

    /** Set of available CMDF rows (not including ones marked as deleted). */
    private CmdfCustMastDefaultsDataSet i_availableCmdfCustMastDefaults = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new CMDF cust mast defaults cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public CmdfCustMastDefaultsCache(PpasRequest    p_request,
                                     Logger         p_logger,
                                     PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing CmdfCustMastDefaultsCache()");
        }

        i_cmdfCustMastDefaultsSqlService = new CmdfCustMastDefaultsSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed CmdfCustMastDefaultsCache()");
        }

    } // End of public constructor
      //         CmdfCustMastDefaultsCache(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all cmdf cust mast defaults (including ones marked as deleted).
     * 
     * @return All default customer classes.
     */
    public CmdfCustMastDefaultsDataSet getAll()
    {
        return(i_allCmdfCustMastDefaults);
    } // End of public method getAll

    /**
     * Returns available cmdf cust mast defaults (not including ones marked
     * as deleted).
     * 
     * @return Available default customer classes.
     */
    public CmdfCustMastDefaultsDataSet getAvailable()
    {
        return(i_availableCmdfCustMastDefaults);
    } // End of public method getAvailable

    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10140, this,
                "Entering reload");
        }

        i_allCmdfCustMastDefaults = i_cmdfCustMastDefaultsSqlService.read(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all languages and old list of deleted
        // languages, but rather then synchronise this is acceptable.

        i_availableCmdfCustMastDefaults = new CmdfCustMastDefaultsDataSet(
                                  p_request,
                                  i_logger,
                                  i_allCmdfCustMastDefaults.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10150, this,
                "Leaving reload");
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allCmdfCustMastDefaults);
    }
}

