////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentRequestData.java
//      DATE            :       12-Aug-03
//      AUTHOR          :       Neil Raymond
//      REFERENCE       :       PpacLon#15/337
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Object to hold Adjustment Request data for the 
//                              Adjustment Business Service
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Request data object for making an adjustment. */
public class AdjustmentRequestData extends DataObject
{
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Description of the adjustment. */
    private String              i_adjType              = null;
                
    /** Description of the adjustment. */
    private String              i_adjCode              = null;
    
    /** Description of the adjustment. */
    private String              i_adjDesc              = null;
    
    /** Details of the account to which teh adjustment is made. */
    private BasicAccountData    i_basicAccountData     = null;
    
    /** Flag to indicate if any limit has been exceeded for the adjustment. */
    private String              i_adjustmentStatus     = null;
    
    /** If an adjustment has exceeded a limit, this variable store the value of the limit exceeded. */
    private Money               i_limitExceeded        = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Constructor with no parameters. */
    public AdjustmentRequestData()
    {
        super();
    }
    
    /** Constructor using a request.
     * 
     * @param p_request The request being processed.
     */
    public AdjustmentRequestData(PpasRequest p_request)
    {
        super(p_request);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Set the adjustment description.
     * 
     * @param p_adjDesc Description of the adjustment.
     */
    public void setAdjDescription(String p_adjDesc)
    {
        i_adjDesc = p_adjDesc;
    }
    
    /** Get the adjustment description.
     * 
     * @return Description of the adjustment.
     */
    public String getAdjDescription()
    {
        return i_adjDesc;
    }

    /** Get the adjustment code.
     * @return Code of the adjustment.
     */
    public String getAdjCode()
    {
        return i_adjCode;
    }

    /** Get the adjustment type.
     * 
     * @return Type of the adjustment.
     */
    public String getAdjType()
    {
        return i_adjType;
    }

    /** Set the adjustment code.
     * 
     * @param p_adjCode Code of the adjustment.
     */
    public void setAdjCode(String p_adjCode)
    {
        i_adjCode = p_adjCode;
    }

    /** Set the adjustment type.
     * 
     * @param p_adjType Type of the adjustment.
     */
    public void setAdjType(String p_adjType)
    {
        i_adjType = p_adjType;
    }

    /** Get details of the account to which the adjustment is applied.
     * 
     * @return Details of the account.
     */
    public BasicAccountData getBasicAccountData()
    {
        return i_basicAccountData;
    }

    /** Set details of the account to which the adjustment is applied.
     * 
     * @param p_data Details of the account.
     */
    public void setBasicAccountData(BasicAccountData p_data)
    {
        i_basicAccountData = p_data;
    }
    
    /**
     * Return the adjustment status.
     * 
     * @return  Adjustment Status.
     */
    public String getAdjustmentStatus()
    {
        return i_adjustmentStatus;
    }
    
    /**
     * Set the adjustment status.
     * 
     * @param p_adjStatus   Adjustment Status.
     */
    public void setAdjustmentStatus(String p_adjStatus)
    {
        i_adjustmentStatus = p_adjStatus;
    }
    
    /**
     * Return the limit exceeded.
     * 
     * @return  Money object containing the limit exceeded.
     */
    public Money getLimitExceeded()
    {
        return i_limitExceeded;
    }
    
    /**
     * Set the limit exceeded.
     * 
     * @param p_limitExceeded   Limit exceeded value.
     */
    public void setLimitExceeded(Money p_limitExceeded)
    {
        i_limitExceeded = p_limitExceeded;
    }
    

}