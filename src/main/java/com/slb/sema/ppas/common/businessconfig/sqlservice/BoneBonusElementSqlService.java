//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BoneBonusElementSqlService.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for bonus element data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** SQL service for accessing Tele Service codes. */
public class BoneBonusElementSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BoneBonusElementSqlService";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new F&F Bonus Elements sql service.
     * @param p_request The request to process.
     */
    public BoneBonusElementSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";

    /**
     * Reads and returns all valid Bonus Elements (including ones marked as deleted).
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Tele Services.
     * @throws PpasSqlException If the data cannot be read.
     */
    public BoneBonusElementDataSet readAll(PpasRequest p_request, JdbcConnection p_connection)
            throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        SqlString l_sql;

        String l_bonusSchemeId;
        String l_bonusKeyElements;
        String l_bonusNonKeyElements;
        Double              l_bonusAmount;
        Float               l_bonusPercent;
        String              l_bonusAdjCode;
        String              l_bonusAdjType;
        Integer             l_dedAccId;
        Integer             l_dedAccExpDays;
        char                l_bonusActive = 'N';

        char l_bonusElementDelFlag;
        String l_bonusElementOpid;
        PpasDateTime l_bonusElementGenYmdhms;

        BoneBonusElementData l_boneBonusElementData;
        Vector l_allBoneBonusElementDataV = new Vector(20, 10);
        BoneBonusElementDataSet l_boneBonusElementDataSet;
        BoneBonusElementData l_allBoneARR[];
        BoneBonusElementData l_boneBonusElementEmptyARR[] = new BoneBonusElementData[0];
        JdbcResultSet l_results;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(500, 0, "select " + "bone_scheme_id, bone_key_elements, bone_adj_code," + 
                                      " bone_adj_type, bone_ded_acc_id, bone_active_ind," + 
                                      " bone_nonkey_elements, bone_bonus_amount, bone_bonus_percent," + 
                                      " bone_ded_acc_expiry_days, bone_del_flag, bone_opid," + 
                                      " bone_gen_ymdhms, bone_nonkey_elements" +
                                      " from bone_bonus_elements");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while (l_results.next(10140))
        {
            l_bonusSchemeId = l_results.getTrimString(10150, "bone_scheme_id");
            l_bonusKeyElements = l_results.getTrimString(10160, "bone_key_elements");
            l_bonusNonKeyElements = l_results.getTrimString(10170, "bone_nonkey_elements");
            l_bonusAmount = l_results.getDoubleO(10180, "bone_bonus_amount");
            l_bonusPercent = l_results.getFloatO(10190, "bone_bonus_percent");
            l_bonusAdjCode = l_results.getTrimString(10200, "bone_adj_code");
            l_bonusAdjType = l_results.getTrimString(10210, "bone_adj_type");
            l_dedAccId = l_results.getIntO(10220, "bone_ded_acc_id");
            l_dedAccExpDays = l_results.getIntO(10230, "bone_ded_acc_expiry_days");
            l_bonusActive = l_results.getChar(10235, "bone_active_ind");
            l_bonusElementDelFlag = l_results.getChar(10240, "bone_del_flag");
            l_bonusElementOpid = l_results.getTrimString(10250, "bone_opid");
            l_bonusElementGenYmdhms = l_results.getDateTime(10260, "bone_gen_ymdhms");
            
            l_boneBonusElementData = new BoneBonusElementData(l_bonusSchemeId,
                                                              l_bonusKeyElements,
                                                              l_bonusNonKeyElements,
                                                              l_bonusAmount,
                                                              l_bonusPercent,
                                                              l_bonusAdjCode,
                                                              l_bonusAdjType,
                                                              l_dedAccId,
                                                              l_dedAccExpDays,
                                                              l_bonusActive == 'Y',
                                                              l_bonusElementDelFlag == '*',
                                                              l_bonusElementOpid,
                                                              l_bonusElementGenYmdhms);

            l_allBoneBonusElementDataV.addElement(l_boneBonusElementData);
        }

        l_results.close(10270);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10280, this, p_request);

        l_allBoneARR = (BoneBonusElementData[])l_allBoneBonusElementDataV.toArray(l_boneBonusElementEmptyARR);

        l_boneBonusElementDataSet = new BoneBonusElementDataSet(p_request, l_allBoneARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 10290, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_boneBonusElementDataSet;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";

    /**
     * Attempts to insert a new row into the table database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_bonusSchemeId The bonus scheme id.
     * @param p_keyElements Concatenated string containing the key elements.
     * @param p_nonKeyElements Concatenated string containing the non-key elements.
     * @param p_bonusAmount The bonus Amount.
     * @param p_bonusPercent The bonus percent.
     * @param p_bonusAdjCode The bonus adjustment code.
     * @param p_bonusAdjType The bonus adjustment type.
     * @param p_dedAccId The dedicated account id.
     * @param p_dedAccExpDays The dedicated account expiry days.
     * @param p_bonusActive Indicator for bonus active
     * @param p_opid Operator ID.
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int insert(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_bonusSchemeId,
                      String p_keyElements,
                      String p_nonKeyElements,
                      Double p_bonusAmount,
                      Float p_bonusPercent,
                      String p_bonusAdjCode,
                      String p_bonusAdjType,
                      Integer p_dedAccId,
                      Integer p_dedAccExpDays,
                      char p_bonusActive,
                      String p_opid) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " + C_METHOD_insert);
        }

        l_sql = "INSERT into bone_bonus_elements (bone_scheme_id, bone_key_elements, "
                + "bone_adj_code, bone_adj_type, bone_ded_acc_id, bone_active_ind, "
                + "bone_bonus_amount, bone_bonus_percent, bone_ded_acc_expiry_days, " 
                + "bone_del_flag, bone_opid, bone_nonkey_elements, bone_gen_ymdhms)"
                + "values( {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, ' ', {9}, {10}, {11})";

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_bonusSchemeId);
        l_sqlString.setStringParam(1, p_keyElements);
        l_sqlString.setStringParam(2, p_bonusAdjCode);
        l_sqlString.setStringParam(3, p_bonusAdjType);
        l_sqlString.setIntParam(4, p_dedAccId);
        l_sqlString.setCharParam(5, p_bonusActive);
        l_sqlString.setDoubleParam(6, p_bonusAmount);
        l_sqlString.setFloatParam(7, p_bonusPercent);
        l_sqlString.setIntParam(8, p_dedAccExpDays);        
        l_sqlString.setStringParam(9, p_opid);
        l_sqlString.setStringParam(10, p_nonKeyElements);
        l_sqlString.setDateTimeParam(11, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 16050, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10040,
                                this,
                                "Database error: unable to insert row in table table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insert,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a new row into the table database table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_schemeId The bonus scheme id.
     * @param p_keyElements A concatenated string of key bonus elements.
     * @param p_nonKeyElements A concatenated string of non-key bonus elements.
     * @param p_bonusAmount The bonus Amount.
     * @param p_bonusPercent The bonus percent.
     * @param p_bonusAdjCode The bonus adjustment code.
     * @param p_bonusAdjType The bonus adjustment type.
     * @param p_dedAccId The dedicated account id.
     * @param p_dedAccExpDays The dedicated account expiry days.
     * @param p_bonusActive Indicator for bonus active
     * @param p_opid Operator ID.
     * @return A count of the number of rows inserted
     * @throws PpasSqlException If the data cannot be read.
     */
    public int update(PpasRequest p_request,
                      JdbcConnection p_connection,
                      String p_schemeId,
                      String p_keyElements,
                      String p_nonKeyElements,
                      Double p_bonusAmount,
                      Float p_bonusPercent,
                      String p_bonusAdjCode,
                      String p_bonusAdjType,
                      Integer p_dedAccId,
                      Integer p_dedAccExpDays,
                      char p_bonusActive,
                      String p_opid) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE bone_bonus_elements " + 
                "SET bone_adj_code = {3}, bone_adj_type = {4}, bone_ded_acc_id = {5}, " + 
                "bone_active_ind = {6}, bone_bonus_amount = {7}, bone_bonus_percent = {8}, " + 
                "bone_ded_acc_expiry_days = {9}, bone_del_flag = ' ', bone_opid = {10}, " +  
                "bone_gen_ymdhms = {11} " +
                "WHERE bone_scheme_id = {0} AND bone_key_elements = {1} AND bone_nonkey_elements = {2}";

        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_schemeId);
        l_sqlString.setStringParam(1, p_keyElements);
        l_sqlString.setStringParam(2, p_nonKeyElements);
        l_sqlString.setStringParam(3, p_bonusAdjCode);
        l_sqlString.setStringParam(4, p_bonusAdjType);
        l_sqlString.setIntParam(5, p_dedAccId);
        l_sqlString.setCharParam(6, p_bonusActive);
        l_sqlString.setDoubleParam(7, p_bonusAmount);
        l_sqlString.setFloatParam(8, p_bonusPercent);
        l_sqlString.setIntParam(9, p_dedAccExpDays);        
        l_sqlString.setStringParam(10, p_opid);
        l_sqlString.setDateTimeParam(11, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                20140,
                                this,
                                "Database error: unable to update row in table table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              20150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20160,
                            this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;
    }

    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";

    /**
     * Mark a row as having been deleted in table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_opid Operator making the change.
     * @param p_bonusId The Bonus Id to be withdrawn.
     * @param p_keyElements Concatenated String of the key elements.
     * @param p_nonKeyElements Concatenated String of the non key elements.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest p_request,
                       JdbcConnection p_connection,
                       String p_opid,
                       String p_bonusId,
                       String p_keyElements,
                       String p_nonKeyElements) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        int l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 32100, this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = "UPDATE BONE_BONUS_ELEMENTS " + "SET BONE_DEL_FLAG = '*', "
                + "BONE_OPID = {0}, BONE_GEN_YMDHMS = {1} " + 
                "WHERE BONE_SCHEME_ID = {2} AND BONE_KEY_ELEMENTS = {3} " + 
                "AND BONE_NONKEY_ELEMENTS = {4}";

        l_sqlString = new SqlString(150, 3, l_sql);

        l_sqlString.setStringParam(0, p_opid);
        l_sqlString.setDateTimeParam(1, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(2, p_bonusId);
        l_sqlString.setStringParam(3, p_keyElements);
        l_sqlString.setStringParam(4, p_nonKeyElements);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete, 32300, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 32410, this,
                                "Database error: unable to mark row as deleted in bonus element table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_delete,
                                       32420,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 32700, this,
                            "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";

    /**
     * Marks a withdrawn Bonus Elements record as available in the bonus element database
     * table.
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_schemeId element Channel identifier.
     * @param p_userOpid Operator updating the business config record.
     * @param p_keyElements Concatenated String of the key elements.
     * @param p_nonKeyElements Concatenated String of the non key elements.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest p_request,
                                JdbcConnection p_connection,
                                String  p_schemeId,
                                String p_userOpid,
                                String p_keyElements,
                                String p_nonKeyElements) throws PpasSqlException
    {
        JdbcStatement l_statement = null;
        String l_sql;
        SqlString l_sqlString;
        PpasDateTime l_now;
        int l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10300,
                            this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE BONE_BONUS_ELEMENTS " + "SET BONE_DEL_FLAG = ' ', "
                + "BONE_OPID = {0}, BONE_GEN_YMDHMS = {1} " + 
                "WHERE BONE_SCHEME_ID = {2} AND BONE_KEY_ELEMENTS = {3} " + 
                "AND BONE_NONKEY_ELEMENTS = {4}";

        l_sqlString = new SqlString(500, 4, l_sql);

        l_sqlString.setStringParam(0, p_userOpid);
        l_sqlString.setDateTimeParam(1, l_now);
        l_sqlString.setStringParam(2, p_schemeId);
        l_sqlString.setStringParam(3, p_keyElements);
        l_sqlString.setStringParam(4, p_nonKeyElements);
        

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       10310,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   10320,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10340,
                                this,
                                "Database error: unable to update row in bonus element table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_markAsAvailable,
                                              10350,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10360,
                            this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}