////////////////////////////////////////////////////////////////////////////////
//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchJobData.java
//    DATE            :       03-May-2004
//    AUTHOR          :       Urban Wigstrom
//    REFERENCE       :       PRD_ASCS00_DEV_SS_071
//
//    COPYRIGHT       :       ATOS ORIGIN 2004
//
//    DESCRIPTION     :       Holding batch job data.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//        |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Holding batch job data.*/

public class BatchJobData
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchJobData";

    /**The name of the batch.*/
    private String i_batchType = null;
    
    /**The operator id.*/
    private String i_opId = null;

    /**The date derived from filename. */
    private String i_batchDate = null;

    /**The file sequence number.*/
    private String i_fileSeqNo = null;

    /**The file sub sequence number. */
    private String i_fileSubSeqNo = null;

    /**The status of the batch job.*/
    private String i_batchJobStatus = null;

    /**Number of successesfully processed records.*/
    private String i_noOfSuccessRec = null;

    /**Number of rejected records. */
    private String i_noOfRejectedRec = null;

    /**The first line of extra data. */
    private String i_extraData1 = null;

    /**The second line of extra data. */
    private String i_extraData2 = null;

    /**The third line of extra data. */
    private String i_extraData3 = null;

    /**The fourth line of extra data. */
    private String i_extraData4 = null;

    /**Time stamp when the batch was launched. */
    private String i_jobExecutionDateTime;

    /**A uniqe job ID that is assigned to the job by Job Schedlur. */
    private String i_jsJobId;
    
    /**The time when the record was inserted in the database. */
    private PpasDateTime i_genTime;

    /**
     * Initally the number of started sub jobs.
     * Internal use only and only by master started batches. 
     */
    private String i_subJobCnt;

    /**Constructs a BatchJobData object. */
    public BatchJobData()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10110,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    /**
     * @return The type of batch job.
     */
    public String getBatchType()
    {
        return i_batchType;
    }

    /**
     * @return The batch job date.
     */
    public String getBatchDate()
    {
        return i_batchDate;
    }

    /**
     * @return the Batch job status.
     */
    public String getBatchJobStatus()
    {
        return i_batchJobStatus;
    }

    /**
     * @return Extra data 1.
     */
    public String getExtraData1()
    {
        return i_extraData1;
    }

    /**
     * @return Extra data 2.
     */
    public String getExtraData2()
    {
        return i_extraData2;
    }

    /**
     * @return Extra data 3.
     */
    public String getExtraData3()
    {
        return i_extraData3;
    }

    /**
     * @return Extra data 4.
     */
    public String getExtraData4()
    {
        return i_extraData4;
    }

    /**
     * @return The file sequence number.
     */
    public String getFileSeqNo()
    {
        return i_fileSeqNo;
    }

    /**
     * @return The file sub sequence number.
     */
    public String getFileSubSeqNo()
    {
        return i_fileSubSeqNo;
    }

    /**
     * @return Number or rejected records.
     */
    public String getNoOfRejectedRec()
    {
        return i_noOfRejectedRec;
    }

    /**
     * @return Numbers of successfull processed record.
     */
    public String getNoOfSuccessRec()
    {
        return i_noOfSuccessRec;
    }

    /**
     * @return The operator id.
     */
    public String getOpId()
    {
        return i_opId;
    }

    /**
     * @return The time stamp when the batch was launched.
     */
    public String getExecutionDateTime()
    {
        return i_jobExecutionDateTime;
    }

    /**
     * @return The unique id that was assigned to the job by the Job Scheduler.
     */
    public String getSubJobCnt()
    {
        return i_subJobCnt;
    }

    /**
      * @return The unique id that was assigned to the job by the Job Scheduler.
      */
    public String getJsJobId()
    {
        return i_jsJobId;
    }
    
    /**
     * @return The time when the record was inserted in the database.
     */
    public PpasDateTime getGenerationTime()
    {
        return i_genTime;
    }

    /**
     * @param p_subJobCnt The unique id assigned to the job by the Job Scheduler.
     */

    public void setSubJobCnt(String p_subJobCnt)
    {
        i_subJobCnt = p_subJobCnt;
    }

    /**
     * @param p_jsJobId The unique id assigned to the job by the Job Scheduler.
     */

    public void setJsJobId(String p_jsJobId)
    {
        i_jsJobId = p_jsJobId;
    }

    /**
     * @param p_batchDate The date for the batch job.
     */
    public void setBatchDate(String p_batchDate)
    {
        i_batchDate = p_batchDate; 
    }

    /**
     * @param p_batchJobStatus The batch job status.
     */
    public void setBatchJobStatus(String p_batchJobStatus)
    {
        i_batchJobStatus = p_batchJobStatus;
    }

    /**
     * @param p_batchType The batch job name.
     */
    public void setBatchType(String p_batchType)
    {
        i_batchType = p_batchType;
    }

    /**
     * @param p_extraData1 The first line of extra data.
     */
    public void setExtraData1(String p_extraData1)
    {
        i_extraData1 = p_extraData1;
    }

    /**
     * @param p_extraData2 The second line of extra data.
     */
    public void setExtraData2(String p_extraData2)
    {
        i_extraData2 = p_extraData2;
    }

    /**
     * @param p_extraData3 The third line of extra data.
     */
    public void setExtraData3(String p_extraData3)
    {
        i_extraData3 = p_extraData3;
    }

    /**
     * @param p_extraData4 The fourth line of extra data.
     */
    public void setExtraData4(String p_extraData4)
    {
        i_extraData4 = p_extraData4;
    }

    /**
     * @param p_fileSeqNo The sequence number of the file.
     */
    public void setFileSeqNo(String p_fileSeqNo)
    {
        i_fileSeqNo = p_fileSeqNo;
    }

    /**
     * @param p_fileSubSeqNo The sub sequence number of the file.
     */
    public void setFileSubSeqNo(String p_fileSubSeqNo)
    {
        i_fileSubSeqNo = p_fileSubSeqNo;
    }

    /**
     * @param p_noOfRejectedRec Number of rejected records.
     */
    public void setNoOfRejectedRec(String p_noOfRejectedRec)
    {
        i_noOfRejectedRec = p_noOfRejectedRec;
    }

    /**
     * @param p_noOfSuccessRec Number of successfully processed records.
     */
    public void setNoOfSuccessRec(String p_noOfSuccessRec)
    {
        i_noOfSuccessRec = p_noOfSuccessRec;
    }

    /**
     * @param p_opId The operator id.
     */
    public void setOpId(String p_opId)
    {
        i_opId = p_opId;
    }

    /**
     * @param p_jobExecutionDateTime The time stamp when the batch was launched.
     */
    public void setExecutionDateTime(String p_jobExecutionDateTime)
    {
        i_jobExecutionDateTime = p_jobExecutionDateTime;
    }
    
    /**
     * @param p_genTime The time when the record was inserted in the database.
     */
    public void setGenerationTime(PpasDateTime p_genTime)
    {
        i_genTime = p_genTime; 
    }

}