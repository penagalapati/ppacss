////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CochCommunityChargingCache.Java
//      DATE            :       11-Jul-2004
//      AUTHOR          :       Julien CHENELAT
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Cache for COCH_COMMUNITY_CHARGING table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.CochCommunityChargingDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.CochCommunityChargingSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Cache for COCH_COMMUNITY_CHARGING table.
 */
public class CochCommunityChargingCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CochCommunityChargingCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The sql service used to load the data set. */
    private CochCommunityChargingSqlService i_cochCommunityChargingSqlService   = null;

    /** Set of all coch_community_charging rows (including ones marked as deleted). */
    private CochCommunityChargingDataSet    i_allCochCommunityCharging          = null;

    /** Set of available coch_community_charging rows (not including ones marked as deleted). */
    private CochCommunityChargingDataSet    i_availableCochCommunityCharging    = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new coch_community_charging cache.
     *
     * @param p_request    The Request being processed.
     * @param p_logger     Logger.
     * @param p_properties Configuration properties.
     */
    public CochCommunityChargingCache( PpasRequest      p_request,
                                       Logger           p_logger,
                                       PpasProperties   p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             p_request, 
                             C_CLASS_NAME, 
                             98000, 
                             this,
                             "Constructing " + C_CLASS_NAME);
        }

        i_cochCommunityChargingSqlService = new CochCommunityChargingSqlService( p_request, p_logger );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             p_request, 
                             C_CLASS_NAME, 
                             98010, 
                             this,
                             "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all coch_community_charging (including ones marked as deleted).
     * 
     * @return All coch_community_charging.
     */
    public CochCommunityChargingDataSet getAll()
    {
        return(i_allCochCommunityCharging);
    }

    /**
     * Returns available coch_community_charging (not including ones marked as deleted).
     * 
     * @return Available coch_community_charging.
     */
    public CochCommunityChargingDataSet getAvailable()
    {
        return(i_availableCochCommunityCharging);
    }

    /** Name of the methods. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request         Ppas Request.
     * @param p_connection      Jdbc Connection.
     * @throws PpasSqlException General Exception. No special keys anticipated.
     */
    public void reload( PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START, 
                             p_request, 
                             C_CLASS_NAME, 
                             98050, 
                             this,
                             "Entering " + C_METHOD_reload);
        }

        i_allCochCommunityCharging = i_cochCommunityChargingSqlService.readAll(p_request, p_connection);

        i_availableCochCommunityCharging 
            = new CochCommunityChargingDataSet( p_request,
                                                i_logger,
                                                i_allCochCommunityCharging.getAvailableArray() );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END, 
                             p_request, 
                             C_CLASS_NAME, 
                             98060, 
                             this,
                             "Leaving " + C_METHOD_reload);
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return new Object[]{"All=[",
                            i_allCochCommunityCharging,
                            "]" + C_NL + "Available=[",
                            i_availableCochCommunityCharging,
                            "]"};
    }
}
