////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchSubJobControlDataSet.java 
//      DATE            :       Aug 31 2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS00_DEV_SS_071
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :      Contains a set of BatchSubJobControlData objects.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

/** Container for a set of Batch Sub Job records. */
public class BatchSubJobControlDataSet
{
    
    /**The vector that contains the <code>BatchSubJobControlData</code> objects.*/
    private final Vector i_vec;

    /**
     * Constructs a new <code>BatchSubJobControlDataSet</code> object.
     * 
     * @param p_vec The vector that contains the <code>BatchSubJobControlData</code> objects.
     */
    public BatchSubJobControlDataSet(Vector p_vec)
    {
        i_vec = p_vec;
    }

    /**
     * Returns a vector containing <code>BatchSubJobControlData</code> objects.
     * @return A vector containing <code>BatchSubJobControlData</code> objects.
     */
    public Vector getDataSet()
    {
        return i_vec;
    }
}
