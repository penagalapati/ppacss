////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentResultData.java
//      DATE            :       27-Aug-2003
//      AUTHOR          :       41087r
//      REFERENCE       :
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 01/12/05 | Remi Isaacs| The AdjustmentResultData        | CR1825/7538
//          |            | constructor no longer contains  | 
//          |            |as part of it�s signature two    |
//          |            | DedicatedAccountDataSets        |
//          |            |(One set for dedicated account   |
//          |            |records and the other for cleared|
//          |            |dedicated account records).      |
//          |            |The constructor now has just one |
//          |            |set meaning the cleared amounts  |
//          |            |are part of the dedicated account|
//          |            |records that making up the set.  |
//----------+------------+-----------------------------------+------------------
// 29/08/06 | L.Byrne    | POSI Balance and After Transaction| PpacLon#2600
//          |            |                                   | PRD_ASCS00_GEN_CA_102
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Result data object for making an adjustment. */
public class AdjustmentResultData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AdjustmentResultData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Value of the adjustment. */
    private Money i_amount                                          = null;
        
    /** SDP flags before the adjustment. */
    private SdpFlagsData i_flagsBefore                              = null;
    
    /** SDP flags after the adjustment. */
    private SdpFlagsData i_flagsAfter                               = null;
    
    /** Date/time the adjustment was applied. */
    private PpasDateTime i_applyDateTime                            = null;
    
    /** Subscriber's new balance after adjustment. */
    private Money i_balanceAfter = null;

    /** Dedicated Account Data Set. */
    private DedicatedAccountsDataSet i_dedAccDataSet                = null;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Full constructor.
     * 
     * @param p_amount          Amount of the adjustment.
     * @param p_flagsBefore     SDP flags before the adjustment.
     * @param p_flagsAfter      SDP flags after the adjustment.
     * @param p_applyDateTime   Date/time the adjustment was applied.
     * @param p_balanceAfter    Subscriber's new balance after adjustment.
     */
    public AdjustmentResultData(Money           p_amount,
                                SdpFlagsData    p_flagsBefore,
                                SdpFlagsData    p_flagsAfter,
                                PpasDateTime    p_applyDateTime,
                                Money           p_balanceAfter)
    {
        this(p_amount, p_flagsBefore, p_flagsAfter, p_applyDateTime, p_balanceAfter, null);
    }
    
    /** 
     * Full constructor with dedicated account data set where each dedicated account can have as
     * attributes: transaction amount, applied amount, balance amount, cleared amount, 
     *             expiry dates of dedicated account.
     * 
     * @param p_amount                  Amount of the adjustment.
     * @param p_flagsBefore             SDP flags before the adjustment.
     * @param p_flagsAfter              ADP flags after the adjustment.
     * @param p_applyDateTime           Date/time the adjustment was applied.
     * @param p_balanceAfter            Subscriber's new balance after adjustment.
     * @param p_dedAccDataSet           Dedicated Account Data Set.
     *                                                                                  
     */
    public AdjustmentResultData( Money                      p_amount,
                                 SdpFlagsData               p_flagsBefore,
                                 SdpFlagsData               p_flagsAfter,
                                 PpasDateTime               p_applyDateTime,
                                 Money                      p_balanceAfter,
                                 DedicatedAccountsDataSet   p_dedAccDataSet )
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10020, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_amount                    = p_amount;
        i_flagsBefore               = p_flagsBefore;
        i_flagsAfter                = p_flagsAfter;
        i_applyDateTime             = p_applyDateTime;
        i_balanceAfter              = p_balanceAfter;
        i_dedAccDataSet             = p_dedAccDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10030, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Get a string representation of the amount.
     * 
     * @return Amount.
     */
    public Money getAmount()
    {
        return i_amount;
    }

    /** Get the account flags after the adjustment has been made.
     * 
     * @return Account Flags (after adjustment)
     */
    public SdpFlagsData getFlagsAfter()
    {
        return i_flagsAfter;
    }

    /** Get the account flags before the adjustment was made.
     * 
     * @return Account Flags (before adjustment)
     */
    public SdpFlagsData getFlagsBefore()
    {
        return i_flagsBefore;
    }
    
    /** Get the date/time the adjustment was applied.
     * 
     * @return Date/time the adjustment was applied.
     */
    public PpasDateTime getApplyDateTime()
    {
        return i_applyDateTime;
    }
    
    /**
     * Get the Subscriber's new balance after adjustment.
     * 
     * @return Subscriber's new balance.
     */
    public Money getBalanceAfter()
    {
        return i_balanceAfter;
    }
        
    /**
     * Get the Dedicated Accounts Data Set object.
     * 
     * @return  DedicatedAccountsDataSet
     */
    public DedicatedAccountsDataSet getDedicatedAccountsDataSet()
    {
        return i_dedAccDataSet;
    }
        
}  // end of class AdjustmentResultData
