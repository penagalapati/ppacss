////////////////////////////////////////////////////////////////////////////////
//      ACSC IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FabaFafBarredListCache.Java
//      DATE            :       10-Mar-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS_GEN_SS_071
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A cache for Barred numbers 
//                              (Table FABA_FAF_BARRED_LIST).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.FabaFafBarredListDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.FabaFafBarredListSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for Barred numbers
 * (Table FABA_FAF_BARRED_LIST).
 */
public class FabaFafBarredListCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FabaFafBarredListCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Barred numbers service to use to load cache. */
    private FabaFafBarredListSqlService i_fabaFafBarredListSqlService = null;

    /** Cache of all valid Barred Numbers (including ones marked as deleted). */
    private FabaFafBarredListDataSet i_allFabaFafBarredList = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Barred Numbers cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public FabaFafBarredListCache(PpasRequest    p_request,
                                  Logger         p_logger,
                                  PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_fabaFafBarredListSqlService = new FabaFafBarredListSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor
      //  FabaFafBarredListCache(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Barred Numbers (including ones marked as deleted).
     * 
     * @return All valid Barred Numbers.
     */
    public FabaFafBarredListDataSet getAll()
    {
        return(i_allFabaFafBarredList);
    } // End of public method getAll

    /**
     * Reloads the static configuration into the cache.
     * This method is synchronised so a reload will not cause unexpected errors.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered reload");
        }

        i_allFabaFafBarredList = i_fabaFafBarredListSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10110, this,
                "Leaving reload");
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allFabaFafBarredList);
    }
}

