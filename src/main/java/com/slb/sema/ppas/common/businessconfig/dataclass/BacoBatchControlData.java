////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BacoBatchControlData.Java
//      DATE            :       15-Jul-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#112/3215
//                              PRD_PPAK00_ANA_FD_13
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Stores a row from the BACO_BATCH_CONTROL database
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** An object that represents a row from the SRVA_MISC_CODES database table. */
public class BacoBatchControlData
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BacoBatchControlData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Symbolic Name of the batch. */
    private String i_jobType;
    
    /** Timestamp when batch was launched. */
    private PpasDateTime i_jobExecutionDateTime;

    /**
     * Job status:
     * 'I' - in progress
     * 'C' - completed
     * 'X' - stopped
     * 'F' - failed 
     */
    private char i_status;

    /** Unique job ID that is assigned to the job by Job Scheduler. */
    private long i_jsJobId;

    /** Initially the number of started sub-jobs. Internal use only and only by master started batches. */
    private int i_subJobCount;
    
    /** Date (or datetime) derived from filename. */
    private PpasDateTime i_fileDate;
    
    /** Sequence Number derived from filename. */
    private Integer i_sequenceNumber;
    
    /** Sub Sequence Number derived from filename. */
    private Integer i_subSequenceNumber; 
    
    /** Counter of number of successfully processed records. */
    private Long i_success;
    
    /** Counter of number of faulty processed records. */
    private Long i_failure;
    
    /** Process dependent. */
    private String i_extraData1;
    
    /** Process dependent. */
    private String i_extraData2;
    
    /** Process dependent. */
    private String i_extraData3;
    
    /** Process dependent. */
    private String i_extraData4;
    
    /** The Operator id of the submitter. */
    private String i_opid;
    
    /** Last time row was modified. */
    private PpasDate i_genYmdhms;
    

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a Batch Control data object.
     * 
     * @param p_request The request to process.
     * @param p_jobType Symbolic Name of the batch.
     * @param p_jobExecutionDateTime Timestamp when batch was launched.
     * @param p_status The status of the job
     * @param p_jsJobId Unique job ID that is assigned to the job by Job Scheduler.
     * @param p_subJobCount Initially the number of started sub-jobs.
     * @param p_fileDate Date (or datetime) derived from filename.
     * @param p_sequenceNumber Sequence Number derived from filename.
     * @param p_subSequenceNumber Sub Sequence Number derived from filename.
     * @param p_success Counter of number of successfully processed records.
     * @param p_failure Counter of number of faulty processed records.
     * @param p_extraData1 Process dependent.
     * @param p_extraData2 Process dependent.
     * @param p_extraData3 Process dependent.
     * @param p_extraData4 Process dependent.
     * @param p_opid The Operator id of the submitter.
     * @param p_genYmdhms Last time row was modified.
     */
    public BacoBatchControlData(PpasRequest  p_request,
                                String       p_jobType,
                                PpasDateTime p_jobExecutionDateTime,
                                char         p_status,
                                long         p_jsJobId,
                                int          p_subJobCount,
                                PpasDateTime p_fileDate,
                                Integer      p_sequenceNumber,
                                Integer      p_subSequenceNumber, 
                                Long         p_success,
                                Long         p_failure,
                                String       p_extraData1,
                                String       p_extraData2,
                                String       p_extraData3,
                                String       p_extraData4,
                                String       p_opid,
                                PpasDate     p_genYmdhms)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_jobType              = p_jobType;
        i_jobExecutionDateTime = p_jobExecutionDateTime;
        i_status               = p_status;
        i_jsJobId              = p_jsJobId;
        i_subJobCount          = p_subJobCount;
        i_fileDate             = p_fileDate;
        i_sequenceNumber       = p_sequenceNumber;
        i_subSequenceNumber    = p_subSequenceNumber; 
        i_success              = p_success;
        i_failure              = p_failure;
        i_extraData1           = p_extraData1;
        i_extraData2           = p_extraData2;
        i_extraData3           = p_extraData3;
        i_extraData4           = p_extraData4;
        i_opid                 = p_opid;
        i_genYmdhms            = p_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {          
        return ("BacoBatchControlData: " +
                "i_jobType=" + i_jobType + ":" +
                "i_jobExecutionDateTime=" +  i_jobExecutionDateTime + ":" +
                "i_status=" + i_status + ":" +
                "i_jsJobId=" + i_jsJobId + ":" +
                "i_subJobCount=" + i_subJobCount + ":" +
                "i_fileDate=" + i_fileDate + ":" +
                "i_sequenceNumber=" + i_sequenceNumber + ":" +
                "i_subSequenceNumber=" + i_subSequenceNumber + ":" + 
                "i_success=" + i_success + ":" +
                "i_failure=" + i_failure + ":" +
                "i_extraData1=" + i_extraData1 + ":" +
                "i_extraData2=" + i_extraData2 + ":" +
                "i_extraData3=" + i_extraData3 + ":" +
                "i_extraData4=" + i_extraData4 + ":" +
                "i_opid=" + i_opid + ":" +
                "i_genYmdhms=" + i_genYmdhms);

    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /** 
     * Returns the Job Type.
     * @return Symbolic Name of the batch.
     */
    public String getJobType()
    {
        return i_jobType;
    }
    
    /**
     * Returns the Job Execution Date Time.
     * @return Timestamp when batch was launched.
     */
    public PpasDateTime getJobExecutionDateTime()
    {
        return i_jobExecutionDateTime;
    }

    /**
     * Returns the Job Status. This is one of the following:
     * <p>
     * 'I' - in progress
     * 'C' - completed
     * 'X' - stopped
     * 'F' - failed
     * @return The Job Status 
     */
    public char getStatus()
    {
        return i_status;
    }
    
    /**
     * Returns the js job id.
     * @return Unique job ID that is assigned to the job by Job Scheduler
     */
    public long getJsJobId() 
    {
        return i_jsJobId;
    }

    /** 
     * Returns the number of started sub-jobs. Internal use only and only by master started batches.
     * @return A count of the number of sub jobs
     */
    public int getSubJobCount()
    {
        return i_subJobCount;
    }
    
    /** 
     * Returns the Date (or datetime) derived from filename.
     * @return Date (or datetime) derived from filename
     */
    public PpasDateTime getFileDate()
    {
        return i_fileDate;
    }
    
    /** 
     * Sequence Number derived from filename.
     * @return Sequence Number from filename
     */
    public Integer getSequenceNumber()
    {
        return i_sequenceNumber;
    }
    
    /** 
     * Sub Sequence Number derived from filename.
     * @return Sub Sequence Number from filename
     */
    public Integer getSubSequenceNumber()
    {
        return i_subSequenceNumber;
    } 
    
    /** 
     * Counter of number of successfully processed records.
     * @return Successful record count
     */
    public Long getSuccess()
    {
        return i_success;
    }
    
    /** 
     * Counter of number of faulty processed records.
     * @return Failed record count
     */
    public Long getFailure()
    {
        return i_failure;
    }
    
    /** 
     * Process dependent.
     * @return Extra data 1 
     */
    public String getExtraData1()
    {
        return i_extraData1;
    }
    
    /** 
     * Process dependent.
     * @return Extra data 2 
     */
    public String getExtraData2()
    {
        return i_extraData2;
    }
    
    /** 
     * Process dependent.
     * @return Extra data 3
     */
    public String getExtraData3()
    {
        return i_extraData3;
    }
    
    /** 
     * Process dependent.
     * @return Extra data 4
     */
    public String getExtraData4()
    {
        return i_extraData4;
    }
    
    /** 
     * The Operator id of the submitter. 
     * @return The Operator Username
     */
    public String getOpid()
    {
        return i_opid;
    }
    
    /** 
     * Last time row was modified. 
     * @return The time the row was last modified
     */
    public PpasDate getGenYmdhms()
    {
        return i_genYmdhms;
    }
}
