////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentData.java
//      DATE            :       29-May-2001
//      AUTHOR          :       Matt Kirk
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       An object that represents an Adjustment
//                              for the subscriber
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+---------------------
// dd/mm/yy | <Name>     | <Brief description of change>   | <reference>
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A data object representing a single adjustment history record.
 */
public class AdjustmentData extends AdjustmentHistoryData
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------
    /** Standard class name constant to be used in calls to Middleware methods. */
    private static final String C_CLASS_NAME = "AdjustmentData";

    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------
    /** The Cust ID of the adjustment. */
    private String       i_btCustId                 = null;

    /** The Description of the adjustment. */
    private String       i_description              = null;

    /** The market that the adjustment is for. */
    private Market       i_market                   = null;

    /** Dedicated account id of an adjustment. */
    private String       i_dedAccId                 = null;

    /** Old Expiry date of a dedicated account adjustment. */
    private PpasDate     i_oldExpiryDate            = null;

    /** New Expiry date of a dedicated account adjustment. */
    private PpasDate     i_newExpiryDate            = null;

    /** Service class used only for a dedicated account adjustment and purge adjustment batch. */
    private ServiceClass i_serviceClass             = null;
    
    /** The transaction id: Either the external trans id, or an internally
     *  generated one if the eternal id was not available. */
    private String       i_transId                  = null;

    /** The Id of the Operator that approved the adjustment if it was held for approval. */
    private String       i_approvingOpid            = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Constructor that takes most of the Adjustment details (except for the original amount).
     *
     * @param p_request         The request being processed.
     * @param p_btCustId        The Cust ID that the Adjustment Details belong to (i.e.
     *                          the master account).
     * @param p_dateTime        The Adjustment dateTime.
     * @param p_type            The Adjustment type.
     * @param p_code            The Adjustment code.
     * @param p_appliedAmount   The Adjustment amount.
     * @param p_description     The Adjustment description.
     * @param p_opid            The Adjustment Operator Id.
     * @param p_market          The market that the adjustment is for.
     * @param p_custId          Customer id of the subordinate account.
     */
    public AdjustmentData( PpasRequest     p_request,
                           String          p_btCustId,
                           PpasDateTime    p_dateTime,
                           String          p_type,
                           String          p_code,
                           Money           p_appliedAmount,
                           String          p_description,
                           String          p_opid,
                           Market          p_market,
                           String          p_custId )
    {
        this(p_request,
             p_btCustId,
             p_dateTime,
             p_type,
             p_code,
             p_appliedAmount,
             p_description,
             p_opid,
             p_market,
             p_custId,
             null,  // p_initiatingMsisdn
             null,  // p_origAmount
             null,  // p_approvingOpid
             null,  // p_dedAccId
             null,  // p_oldExpiryDate
             null,  // p_newExpiryDate
             null,  // p_serviceClass
             null,
             null,  // Balance Before
             null); // Balance After
    }

    /**
     * Constructor that takes most of the Adjustment details for DedicatedAccount(except for
     * the original amount).
     *
     * @param p_request         The request being processed.
     * @param p_btCustId        The Cust ID that the Adjustment Details belong to (i.e.
     *                          the master account).
     * @param p_dateTime        The Adjustment dateTime.
     * @param p_type            The Adjustment type.
     * @param p_code            The Adjustment code.
     * @param p_appliedAmount   The Adjustment amount.
     * @param p_description     The Adjustment description.
     * @param p_opid            The Adjustment Operator Id.
     * @param p_market          The market that the adjustment is for.
     * @param p_custId          Customer id of the subordinate account.
     * @param p_dedAccId        DedicatedAccountId.
     * @param p_oldExpiryDate   Old Expiry Date.
     * @param p_newExpiryDate   New Expiry Date.
     * @param p_serviceClass    The service class when the adjustment is performed.
     */
    public AdjustmentData( PpasRequest           p_request,
                           String                p_btCustId,
                           PpasDateTime          p_dateTime,
                           String                p_type,
                           String                p_code,
                           Money                 p_appliedAmount,
                           String                p_description,
                           String                p_opid,
                           Market                p_market,
                           String                p_custId,
                           String                p_dedAccId,
                           PpasDate              p_oldExpiryDate,
                           PpasDate              p_newExpiryDate,
                           ServiceClass          p_serviceClass )
    {
        this(p_request,
             p_btCustId,
             p_dateTime,
             p_type,
             p_code,
             p_appliedAmount,
             p_description,
             p_opid,
             p_market,
             p_custId,
             null,  // p_initiatingMsisdn
             null,  // p_origAmount,
             null,  // p_approvingOpid,
             p_dedAccId,
             p_oldExpiryDate,
             p_newExpiryDate,
             p_serviceClass,
             null,
             null,      // Balance Before
             null);     // Balance After
    }

    /**
     * Constructor that takes a full set of Adjustment details.
     *
     * @param p_request             The request being processed.
     * @param p_btCustId            The Cust ID that the Adjustment Details belong to (i.e.
     *                              the master account).
     * @param p_dateTime            The Adjustment dateTime.
     * @param p_type                The Adjustment type.
     * @param p_code                The Adjustment code.
     * @param p_appliedAmount       The Adjustment amount.
     * @param p_description         The Adjustment description.
     * @param p_opid                The Adjustment Operator Id.
     * @param p_market              The market that the adjustment is for.
     * @param p_custId              Customer id of the subordinate account.
     * @param p_initiatingMsisdn    Mobile number of customer that caused the adjustment to be applied.
     * @param p_origAmount          The original Adjustment amount.
     * @param p_approvingOpid       ID of the operator that approved the adjustment (if it
     *                              was held for approval).
     * @param p_transId             The transaction id: Either the external trans id, or an internally
     *                              generated one if the eternal id was not available.
     * @param p_balanceBefore       Account Balance before this transaction.
     * @param p_balanceAfter        Account Balance after this transaction.
     */
    public AdjustmentData( PpasRequest     p_request,
                           String          p_btCustId,
                           PpasDateTime    p_dateTime,
                           String          p_type,
                           String          p_code,
                           Money           p_appliedAmount,
                           String          p_description,
                           String          p_opid,
                           Market          p_market,
                           String          p_custId,
                           Msisdn          p_initiatingMsisdn,
                           Money           p_origAmount,
                           String          p_approvingOpid,
                           String          p_transId,
                           Money           p_balanceBefore,
                           Money           p_balanceAfter)
    {
        this(p_request,
             p_btCustId,
             p_dateTime,
             p_type,
             p_code,
             p_appliedAmount,
             p_description,
             p_opid,
             p_market,
             p_custId,
             p_initiatingMsisdn,
             p_origAmount,
             p_approvingOpid,
             null,  // p_dedAccId,
             null,  // p_oldExpiryDate,
             null,  // p_newExpiryDate,
             null,  // p_serviceClass);
             p_transId,
             p_balanceBefore,
             p_balanceAfter);
    }

    /**
     * Constructor that takes a full set of Adjustment details plus dedicated
     * account adjustment data.
     *
     * @param p_request             The request being processed.
     * @param p_btCustId            The Cust ID that the Adjustment Details belong to
     *                              (i.e. the master account).
     * @param p_dateTime            The Adjustment dateTime.
     * @param p_type                The Adjustment type.
     * @param p_code                The Adjustment code.
     * @param p_appliedAmount       The Adjustment amount.
     * @param p_description         The Adjustment description.
     * @param p_opid                The Adjustment Operator Id.
     * @param p_market              The market that the adjustment is for.
     * @param p_custId              Customer id of the subordinate account.
     * @param p_initiatingMsisdn    Mobile number of the user that made the query that instigated this
     *                              adjustment.
     * @param p_origAmount          The original Adjustment amount.
     * @param p_approvingOpid       ID of the operator that approved the adjustment (if it was
     *                              held for approval).
     * @param p_dedAccId            The dedicated account id of the adjustment.
     * @param p_oldExpiryDate       The old expiry date of the dedicated account adjustment.
     * @param p_newExpiryDate       The new expiry date of the dedicated account adjustment.
     * @param p_serviceClass        The service class when the adjustment is performed.
     * @param p_transId             The transaction id: Either the external trans id, or an internally
     *                              generated one if the eternal id was not available.
     * @param p_balanceBefore       Account Balance before this transaction.
     * @param p_balanceAfter        Account Balance after this transaction.
     */
    public AdjustmentData( PpasRequest           p_request,
                           String                p_btCustId,
                           PpasDateTime          p_dateTime,
                           String                p_type,
                           String                p_code,
                           Money                 p_appliedAmount,
                           String                p_description,
                           String                p_opid,
                           Market                p_market,
                           String                p_custId,
                           Msisdn                p_initiatingMsisdn,
                           Money                 p_origAmount,
                           String                p_approvingOpid,
                           String                p_dedAccId,
                           PpasDate              p_oldExpiryDate,
                           PpasDate              p_newExpiryDate,
                           ServiceClass          p_serviceClass,
                           String                p_transId,
                           Money                 p_balanceBefore,
                           Money                 p_balanceAfter)
    {
        super(p_request,
              p_dateTime,
              p_type,
              p_code,
              p_appliedAmount,
              p_opid,
              p_custId,
              p_initiatingMsisdn,
              p_origAmount,
              p_balanceBefore,
              p_balanceAfter);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 11011, this,
                             "Constructing " + C_CLASS_NAME );
        }

        i_btCustId          = p_btCustId;
        i_description       = p_description;
        i_market            = p_market;
        i_dedAccId          = p_dedAccId;
        i_oldExpiryDate     = p_oldExpiryDate;
        i_newExpiryDate     = p_newExpiryDate;
        i_serviceClass      = p_serviceClass;
        i_transId           = p_transId;
        i_approvingOpid     = p_approvingOpid;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 11013, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------

    /**
     * Returns the Cust Id of the account the Adjustment belongs to.
     *
     * @return Internal identifier of mobile account.
     */
    public String getBtCustId()
    {
        return i_btCustId;
    }

    /**
     * Returns the dateTime the adjustment was posted.
     *
     * @return The date/time the adjustment was posted.
     */
    public PpasDateTime getDateTime()
    {
        return getEventDateTime();
    }

    /**
     * Returns the adjustment description.
     *
     * @return The adjustment description.
     */
    public String getAdjustmentText()
    {
        return i_description;
    }

    /**
     * Get the market that the adjustment is for.
     *
     * @return The market with which the account is associated.
     */
    public Market getMarket()
    {
        return i_market;
    }

    /**
     * Gets the adjustments dedicated account id.
     *
     * @return The dedicated account identifier that the adjustment was made to.
     */
    public String getDedAccId()
    {
        return i_dedAccId;
    }

    /**
     * Gets the old expiry date of the dedicated account adjustment.
     *
     * @return The old expiry date.
     */
    public PpasDate getOldExpiryDate()
    {
        return i_oldExpiryDate;
    }

    /**
     * Gets the old expiry date of the dedicated account adjustment.
     *
     * @return The old expiry date.
     */
    public PpasDate getNewExpiryDate()
    {
        return i_newExpiryDate;
    }

    /**
     * Gets the service class of the dedicated account adjustment.
     *
     * @return The service class of the dedicated account adjustment.
     */
    public ServiceClass getServiceClass()
    {
        return i_serviceClass;
    }

    /**
     * Get the type of event.
     *
     * @return Flag indicating this is an adjustment.
     */
    public int getEventType()
    {
        return EventHistoryData.C_EVENT_TYPE_ADJUSTMENT;
    }

    /** 
     * Gets the transaction id: Either the external trans id, or an internally
     * generated one if the eternal id was not available.
     * @return The transaction id.
     */
    public String getTransId()
    {
        return i_transId;
    }

    /**
     * Returns the Id of the Operator that approved the adjustment,
     * if it was held for approval.
     *
     * @return Operator identifier.
     */
    public String getApprovingOpid()
    {
        return(i_approvingOpid);
    }

    /**
     * Get a description of the event.
     *
     * @param p_moneyFormat     Object to format monetary amounts for display. If <code>null</vcode> then any
     *                          <code>Money</code> amounts are not formated.
     * @param p_msisdnFormat    Formatter for mobile numbers. If <code>null</vcode> then any
     *                          <code>Msisdn</code>'s are not formated.
     * @return Description of the adjustment.
     */
    public String getEventText( MoneyFormat p_moneyFormat, MsisdnFormat p_msisdnFormat )
    {
        String l_msisdn = p_msisdnFormat == null ? "" + getInitiatingMsisdn() :
                                                   p_msisdnFormat.format(getInitiatingMsisdn());
        String l_amount = null;

        StringBuffer l_retStr = new StringBuffer("Adjustment applied for ");
        l_retStr.append(l_msisdn);

        if (i_dedAccId != null && !i_dedAccId.trim().equals(""))
        {
            l_retStr.append(", account ");
            l_retStr.append(i_dedAccId);
        }

        if (getAppliedAmount() != null && !getAppliedAmount().equals(getAppliedAmount().negate()))
        {
            l_amount = (p_moneyFormat == null ? "" + getAppliedAmount() :
                                                     p_moneyFormat.format(getAppliedAmount())) +
                       " " +
                       getAppliedAmount().getCurrency().getCurrencyCode();

            l_retStr.append(", value ");
            l_retStr.append(l_amount);
        }

        if (i_oldExpiryDate != null)
        {
            l_retStr.append(", old expiry date ");
            l_retStr.append(i_oldExpiryDate);
        }

        if (i_newExpiryDate != null)
        {
            l_retStr.append(", new expiry date ");
            l_retStr.append(i_newExpiryDate);
        }

        if (i_description != null && !i_description.trim().equals(""))
        {
            l_retStr.append(", description ");
            l_retStr.append(i_description);
        }

        return l_retStr.toString();
    }

    /**
     * Equals method used for testing.
     *
     * @param p_adjData <code>AdjustmentData</code> object to be compared with
     *
     * @return <code>true</code> if the current object and the supplied object are the
     * same from a business logic point of view, else <code>false</code>
     */
    public boolean equals(Object p_adjData)
    {
        if (p_adjData instanceof AdjustmentData)
        {
            AdjustmentData l_adjData = (AdjustmentData)p_adjData;

            if (!super.equals(p_adjData))
            {
                return false;
            }

            if (Integer.parseInt(getBtCustId()) != Integer.parseInt(l_adjData.getBtCustId()))
            {
                return false;
            }

            if (Integer.parseInt(getCustId()) != Integer.parseInt(l_adjData.getCustId()))
            {
                return false;
            }

            if (getDedAccId() != null)
            {
                if (!getDedAccId().equals(l_adjData.getDedAccId()))
                {
                    return false;
                }
            }

            if (!getAdjustmentText().equals(l_adjData.getAdjustmentText()))
            {
                return false;
            }

            if (getOldExpiryDate() != null)
            {
                if (!getOldExpiryDate().equals(l_adjData.getOldExpiryDate()))
                {
                    return false;
                }
            }

            if (getNewExpiryDate() != null)
            {
                if (!getNewExpiryDate().equals(l_adjData.getNewExpiryDate()))
                {
                    return false;
                }
            }

            if (!getMarket().equals(l_adjData.getMarket()))
            {
                return false;
            }
            
            if (!getTransId().equals(l_adjData.getTransId()))
            {
                return false;
            }

            return true;
        }

        return false;
    }
}
