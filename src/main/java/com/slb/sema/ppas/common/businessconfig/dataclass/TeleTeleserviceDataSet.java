////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TeleTeleserviceDataSet.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8229, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Teleservice data set.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//27-June-06|M.Toernqvist| Changes due to code review      | PpasLon#2322/9154
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an teleservice data, each element of
 * the set representing a record from the tele_teleservice table.
 */
public class TeleTeleserviceDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TeleTeleserviceDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of tele teleservice records. That is, the data of this DataSet object. */
    private TeleTeleserviceData[] i_teleTeleserviceDataObjects;
    
    /** Array of available tele teleservice data records. That is, the data of this DataSet object. */    
    private TeleTeleserviceData[] i_availableTeleTeleserviceDataObjects;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Tele Teleservice data.
     * @param p_request The request to process.
     * @param p_teleTeleserviceDataObjects Array holding the tele teleservice data records to be stored
     *                              by this object.
     */
    public TeleTeleserviceDataSet(PpasRequest             p_request,
                                  TeleTeleserviceData []  p_teleTeleserviceDataObjects)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_teleTeleserviceDataObjects = p_teleTeleserviceDataObjects;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns teleservice data associated with a specified code .
     * @param p_code The tele service code.
     * 
     * @return TeleTeleserviceData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public TeleTeleserviceData getRecord (int p_code )
    {
        TeleTeleserviceData l_teleTeleserviceRecord = null;
        
        for (int i = 0; i < i_teleTeleserviceDataObjects.length; i++)
        {
            if ( i_teleTeleserviceDataObjects[i].getTeleserviceCode() == p_code )                
            {
                l_teleTeleserviceRecord = i_teleTeleserviceDataObjects[i];
                break;
            }
        }

        return l_teleTeleserviceRecord;
    }

    
    
    /**
     * Returns available (that is, not marked as deleted) teleTeleservice records associated 
     * with a specified code.
     * @param p_code The teleservice code.
     * 
     * @return TeleTeleserviceData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public TeleTeleserviceData getAvailableRecord (int p_code)
    {
        TeleTeleserviceData l_teleTeleserviceRecord = getRecord(p_code);
        
        if (l_teleTeleserviceRecord != null && l_teleTeleserviceRecord.isDeleted())
        {
            l_teleTeleserviceRecord = null;
        }

        return l_teleTeleserviceRecord;
    }
 
    
    
    /** Returns array of all records in this data set object.
     * 
     * @return All Tele-teleservice data.
     */
    public TeleTeleserviceData [] getAllArray()
    {
        return i_teleTeleserviceDataObjects;
    }

    
    
    /**
     * Get an array containing the all available TeleTeleserviceData records (i.e.
     * those not marked as deleted.
     * @return Data set containing available TeleTeleserviceData records in the
     *         cache (i.e. those not marked as deleted).
     */
    public TeleTeleserviceData[] getAvailableArray()
    {
        Vector                l_availableV                = new Vector();
        TeleTeleserviceData[] l_teleTeleserviceEmptyArray = new TeleTeleserviceData[0];

        if (i_availableTeleTeleserviceDataObjects == null)
        {
            for (int i = 0; i < i_teleTeleserviceDataObjects.length; i++)
            {
                if (!i_teleTeleserviceDataObjects[i].isDeleted())
                {
                    l_availableV.addElement(i_teleTeleserviceDataObjects[i]);
                }
            }
    
            i_availableTeleTeleserviceDataObjects =
                  (TeleTeleserviceData[])l_availableV.toArray(l_teleTeleserviceEmptyArray);
        }

        return i_availableTeleTeleserviceDataObjects;
    }

    
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_availableTeleTeleserviceDataObjects, p_sb);
    }
    

    
//    public String toString()
//    {
//        final String L_NL  = "\n";
//        StringBuffer l_tmp = new StringBuffer(200);
//        
//        l_tmp.append("AvailableTeleTeleserviceDataObjects: ");
//        if ( i_availableTeleTeleserviceDataObjects != null )
//        {
//            for ( int i = 0; i < i_availableTeleTeleserviceDataObjects.length; i++ )
//            {
//                l_tmp.append(i_availableTeleTeleserviceDataObjects[i].toString());
//                l_tmp.append(L_NL);
//            }
//        }
//        else
//        {
//            l_tmp.append("is NULL");
//        }
//        
//        l_tmp.append("TeleTeleserviceDataObjects: ");
//        if ( i_teleTeleserviceDataObjects != null )
//        {
//            for ( int i = 0; i < i_teleTeleserviceDataObjects.length; i++ )
//            {
//                l_tmp.append(i_teleTeleserviceDataObjects[i].toString());
//                l_tmp.append(L_NL);
//            }
//        }
//        else
//        {
//            l_tmp.append("is NULL");
//        }
//        
//        return l_tmp.toString();
//    }

}
