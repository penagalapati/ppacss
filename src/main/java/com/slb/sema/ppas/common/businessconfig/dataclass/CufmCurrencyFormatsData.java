////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CufmCurrencyFormatsData.Java
//      DATE            :       23-October-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1074/4394
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2001
//
//      DESCRIPTION     :       Holds data from the CufmCurrencyFormats table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 30-Sep-03 | MAGray        | Add method to convert from | PpacLon#56/531
//           |               | string to double           |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.io.Serializable;

import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents data from the CUFM_CURRENCY_FORMATS db table.
 * NB - The initial implementation is being used only for formatting currency
 * values from the database for presentation to the client and requires only
 * the currency code and the precision. 
 */
public class CufmCurrencyFormatsData implements Serializable
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CufmCurrencyFormatsData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The currency. */
    private String i_currency;

    /** 
     * The precision of the currency - ie the number of significant decimal 
     * places in the value held in the database that should be used for 
     * presentation to the client.
     */
    private int i_precision;

    /** Instance of <code>PpasCurrency</code>.
     * The <code>PpasCurrency</code> class defines details about a currency.
     */
    private PpasCurrency i_ppasCurrency = null;
    
    /** Flag set to '*' if currency has been deleted, else blank. */
    private char i_deleteFlag = ' ';
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a currency format data object.
     * 
     * @param p_request The request to process.
     * @param p_currency The currency.
     * @param p_precision The precision.
     * @param p_deleteFlag Set to '*' if currency record has been deleted, else blank.
     */
    public CufmCurrencyFormatsData(PpasRequest p_request,
                                   String      p_currency,
                                   int         p_precision,
                                   char        p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, 
                            C_CLASS_NAME, 
                            10000, 
                            this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_currency = p_currency;
        i_precision = p_precision;
        i_ppasCurrency = new PpasCurrency(p_currency, p_precision);
        i_deleteFlag = p_deleteFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_BUSINESS,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 
                            10010, 
                            this,
                            "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of the currency format.
     */
    public String toString()
    {
        return("CufmCurrencyFormatsData:" +
               "i_currency=" + i_currency + ":" +
               "i_precision=" + i_precision + ":" +
               "i_deleteFlag=" + i_deleteFlag);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Returns the currency code.
     * 
     * @return The currency code.
     */
    public String getCurrencyCode()
    {
        return i_currency;
    }

    /** Returns the currency.
     * 
     * @return The currency object for this corrency code.
     */
    public PpasCurrency getCurrency()
    {
        return i_ppasCurrency;
    }

    /** Returns the precision.
     * 
     * @return The precision associated with this currency.
     */
    public int getPrecision()
    {
        return i_precision;
    }
    
    /** Convert a string representation of an amount to a double format.
     * 
     * @param p_amount String representation of an amount.
     * @return Numeric representation of the amount based on the precision of this currency.
     */
    public double convertAmountToDouble (String p_amount)
    {
        return Double.parseDouble(p_amount) / Math.pow(10, i_precision);
    }

    /** 
     * Returns true if currency has been marked as deleted.
     * 
     * @return Flag indicating whether the currency has been deleted.
     */
    public boolean isDeleted()
    {
        return('*' == i_deleteFlag);
    }
}