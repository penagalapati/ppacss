/*
 * Created on 28-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.common.awd.alarmcommon;

import java.util.Map;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface Alarm
{

    public String               getAlarmName();
    public String               getAlarmText();
    public int                  getAlarmSeverity();
    
    public String               getOriginatingNodeName();
    public String               getOriginatingProcessName();
    
    /** Time (normal Java UTC millis time) alarm was raised. */ 
    public long                 getRaisedTime();

    public String               getParameter(String p_parameterName);
    public Map                  getParameterMap();
}
