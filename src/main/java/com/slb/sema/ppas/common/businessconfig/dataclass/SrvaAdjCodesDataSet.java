////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAdjCodesDataSet.Java
//      DATE            :       14-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of adjustment codes data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 22/12/05 | Lars. L.   | Two new methods are added:      | PpacLon#1755/7585
//          |            | * getAvailableAdjTypesArray()   | 
//          |            | * getAvailableAdjCodesArray(...)| 
//----------+------------+---------------------------------+--------------------
// 04/01/06 | M Erskine  | Method getAdjCodeData added.    | PpacLon#2827/10751
//          |            | This returns global market data |
//          |            | + specific market data.         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an adjustment codes data, each element of
 * the set representing a record from the srva_adj_codes table.
 */
public class SrvaAdjCodesDataSet
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAdjCodesDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Market. */
    private Market                  i_market;

    /** Array of adjustment codes data records. */
    private SrvaAdjCodesData []     i_srvaAdjCodesARR;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of adjustment codes data.
     * 
     * @param p_request The request to process.
     * @param p_market  The Market the Adjustment data is for.
     * @param p_srvaAdjCodesARR A set of service area adjustment data objects.
     */
    public SrvaAdjCodesDataSet(
        PpasRequest             p_request,
        Market                  p_market,
        SrvaAdjCodesData []     p_srvaAdjCodesARR)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 11510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market          = p_market;
        i_srvaAdjCodesARR = p_srvaAdjCodesARR;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 11590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    /**
     * Creates a new set of adjustment codes data where the market is not known.
     * 
     * @param p_request The request to process.
     * @param p_srvaAdjCodesARR A set of service area adjustment data objects.
     */
    public SrvaAdjCodesDataSet(
        PpasRequest             p_request,
        SrvaAdjCodesData []     p_srvaAdjCodesARR)
    {
        this(p_request, null, p_srvaAdjCodesARR);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get all data contained in this data set.
     *  @return Array containing all the data in this data set.
     */
    public SrvaAdjCodesData []  getAllArray()
    {
        return (i_srvaAdjCodesARR);
    }
    
    /**
     * Get data for a given adjustment code/type.
     * @param p_adjType Type of the adjustment.
     * @param p_adjCode Adjustment code.
     * @return Active adjustment data for the key.
     */
    public SrvaAdjCodesData getAdjustmentData(
                                String  p_adjType, 
                                String  p_adjCode)
    {
        SrvaAdjCodesData    l_srvaAdjCodesData = null;
        SrvaAdjCodesData    l_tmpSrvaAdjCodesData = null;
        int                 l_len = 0;

        l_len = i_srvaAdjCodesARR.length;
        for (int i = 0; i < l_len; i++)
        {
            l_tmpSrvaAdjCodesData = i_srvaAdjCodesARR[i];
            if ( l_tmpSrvaAdjCodesData.getAdjType().equals(p_adjType) &&
                 l_tmpSrvaAdjCodesData.getAdjCode().equals(p_adjCode) )
            {
                l_srvaAdjCodesData = l_tmpSrvaAdjCodesData;
                break;
            }
        }

        return l_srvaAdjCodesData;

    } // End of public method getAdjustmentData()
    
    /** Get array of adjsutment codes data objects for the given market.
     *  @param p_market Market which the adjustment codes data must belong to.
     *  @return Array of adjsustment codes data object with the given market.
     */
    public SrvaAdjCodesData [] getAdjCodesArray (Market p_market)
    {
        int                       l_loop;
        Vector                    l_availableV = new Vector (30, 20);
        SrvaAdjCodesData          l_adjCodesARR[];
        SrvaAdjCodesData          l_adjCodesEmptyARR[] = new SrvaAdjCodesData[0];

        if (i_srvaAdjCodesARR != null)
        {
            for (l_loop = 0;
                 l_loop < i_srvaAdjCodesARR.length;
                 l_loop++)
            {
                if ( i_srvaAdjCodesARR[l_loop].getMarket().equals(p_market) )
                {
                    l_availableV.addElement (i_srvaAdjCodesARR[l_loop]);
                }
            }
        }

        l_adjCodesARR = (SrvaAdjCodesData [])
                                    l_availableV.toArray (l_adjCodesEmptyARR);

        return (l_adjCodesARR);
    }

    /** Get array of adjustment codes data objects for the given market and adjustment type.
     *  @param p_market Market which the adjustment type and code data must belong to.
     *  @param p_adjustmentType adjustment type.
     *  @return  Array of adjsustment codes data object with the given market and adjustment type.
     */
    public SrvaAdjCodesData [] getAdjCodesArray (Market p_market,
                                                 String p_adjustmentType)
    {
        int                       l_loop;
        Vector                    l_availableV = new Vector (30, 20);
        SrvaAdjCodesData l_adjCodesARR[];
        SrvaAdjCodesData l_adjCodesEmptyARR[] = new SrvaAdjCodesData[0];

        if (i_srvaAdjCodesARR != null)
        {
            for (l_loop = 0;
                 l_loop < i_srvaAdjCodesARR.length;
                 l_loop++)
            {
                if ( ( i_srvaAdjCodesARR[l_loop].getMarket().equals(p_market) ) &&
                     ( i_srvaAdjCodesARR[l_loop].getAdjType().equals(p_adjustmentType) ) )
                {
                    l_availableV.addElement (i_srvaAdjCodesARR[l_loop]);
                }
            }
        }

        l_adjCodesARR = (SrvaAdjCodesData [])
                                    l_availableV.toArray (l_adjCodesEmptyARR);

        return (l_adjCodesARR);
    }
    
    /**
     * Get array of adjustment codes data objects for the given market and adjustment type that 
     * have NOT been marked as deleted.
     * @param p_market Market which the adjustment type and code data must belong to.
     * @param p_adjustmentType adjustment type.
     * @return  Array of adjsustment codes data object with the given market and adjustment type.
     */
    public SrvaAdjCodesData[] getAvailableAdjCodesArray(Market p_market,
                                                        String p_adjustmentType)
    {
        int                       l_loop;
        Vector                    l_availableV = new Vector (30, 20);
        SrvaAdjCodesData l_adjCodesARR[];
        SrvaAdjCodesData l_adjCodesEmptyARR[] = new SrvaAdjCodesData[0];

        if (i_srvaAdjCodesARR != null)
        {
            for (l_loop = 0;
                 l_loop < i_srvaAdjCodesARR.length;
                 l_loop++)
            {
                if ( ( i_srvaAdjCodesARR[l_loop].getMarket().equals(p_market) ) &&
                     ( i_srvaAdjCodesARR[l_loop].getAdjType().equals(p_adjustmentType) ) &&
                     ( !i_srvaAdjCodesARR[l_loop].isDeleted()))
                {
                    l_availableV.addElement (i_srvaAdjCodesARR[l_loop]);
                }
            }
        }

        l_adjCodesARR = (SrvaAdjCodesData [])
                                    l_availableV.toArray (l_adjCodesEmptyARR);

        return (l_adjCodesARR);
    }


    /** 
     * Returns Market of DataSet.
     * @return Market of DataSet. 
     */
    public Market getMarket()
    {
        return i_market;
    }


    /***************************************************************************
     * Returns an array of available adjustment types.
     * 
     * @return an array of available adjustment types.
     */
    public String[] getAvailableAdjTypesArray()
    {
        String[]         l_availableAdjTypesArr = null;
        TreeSet          l_availableAdjTypesSet = null;
        SrvaAdjCodesData l_srvaAdjCodesData     = null;
        
        if (i_srvaAdjCodesARR != null)
        {
            l_availableAdjTypesSet = new TreeSet();
            for (int l_ix = 0; l_ix < i_srvaAdjCodesARR.length; l_ix++)
            {
                l_srvaAdjCodesData = i_srvaAdjCodesARR[l_ix];
                if (!l_srvaAdjCodesData.isDeleted())
                {
                    l_availableAdjTypesSet.add(l_srvaAdjCodesData.getAdjType());
                }
            }
            l_availableAdjTypesArr = new String[l_availableAdjTypesSet.size()];
            l_availableAdjTypesSet.toArray(l_availableAdjTypesArr);
        }
        else
        {
            l_availableAdjTypesArr = new String[1];
            l_availableAdjTypesArr[0] = "";
        }

        return l_availableAdjTypesArr;
    }


    /***************************************************************************
     * Returns an array of available adjustment codes that corresponds to the
     * passed adjustment type.
     *
     * @param p_adjType  the adjustment type.
     * 
     * @return  the available adjustment codes as an array of
     *          <code>SrvaAdjCodesData</code> objects.
     */
    public SrvaAdjCodesData[] getAvailableAdjCodesArray(String p_adjType)
    {
        SrvaAdjCodesData[] l_srvaAdjCodesDataArr = null;
        SrvaAdjCodesData   l_srvaAdjCodesData    = null;
        Vector             l_srvaAdjCodesDataVec = null;
        
        if (i_srvaAdjCodesARR != null)
        {
            l_srvaAdjCodesDataVec = new Vector(i_srvaAdjCodesARR.length);
            for (int l_ix = 0; l_ix < i_srvaAdjCodesARR.length; l_ix++)
            {
                l_srvaAdjCodesData = i_srvaAdjCodesARR[l_ix];
                if (l_srvaAdjCodesData.getAdjType().equals(p_adjType) && !l_srvaAdjCodesData.isDeleted())
                {
                    l_srvaAdjCodesDataVec.add(l_srvaAdjCodesData);
                }
                l_srvaAdjCodesDataArr = new SrvaAdjCodesData[l_srvaAdjCodesDataVec.size()];
                l_srvaAdjCodesDataVec.toArray(l_srvaAdjCodesDataArr);
            }
        }

        return l_srvaAdjCodesDataArr;
    }
    
    /** Get available data for a given market.
     *  <p>
     *  Create a new SrvaAdjCodesDataSet that is a subset of the data in this 
     *  object where the records selected are based on given market and
     *  msic code values. A flag determines if non-deleted entries must be
     *  included.
     * 
     * @param p_market Market to select the subset of records in this class upon.
     * @param p_adjType Adjustment type to use to select the subset of records in this class upon.
     * @param p_useCurrent True if use only non-deleted items.
     * 
     * @return Data set containing a subset of the data in this class, based on
     *         the given market adjustment type values.
     */
    public Vector getAdjCodeData (Market      p_market,
                                             String      p_adjType,
                                             boolean     p_useCurrent)
    {
        Vector l_returnData = new Vector(5);
        SrvaAdjCodesData    l_data = null;
        SrvaAdjCodesData    l_dataGlobMkt = null;
        SrvaAdjCodesData    l_dataSpecMkt = null;
        ArrayList       l_globMktAL = null;
        HashMap         l_specMktHM = null;
        Iterator        l_iter = null;

        // First pass : build 2 data structures :
        // 1. an Arraylist for entries with Global Market and AdjType
        // 2. an HashMap for entries with Specific Market and AdjType
        l_globMktAL = new ArrayList();
        l_specMktHM = new HashMap();
        
        for (int i=0; i<i_srvaAdjCodesARR.length; i++)
        {
            l_data = i_srvaAdjCodesARR[i];
            if ( l_data.getMarket()  .equals(Market.C_GLOBAL_MARKET) &&
                    l_data.getAdjType().equals(p_adjType) )
            {
                l_globMktAL.add(l_data);
            }
            else if ( l_data.getMarket()  .equals(p_market) &&
                    l_data.getAdjType().equals(p_adjType) )
            {
               l_specMktHM.put(l_data.getAdjCode(), l_data);
            }            
        }

        // 2nd pass : merge previous AL and HM into return DataSet
        // 2.a Travel ArrayList of GlobalMkt entries
        int l_lenAL = l_globMktAL.size();
        for (int l_i = 0; l_i < l_lenAL; l_i++)
        {
            l_dataGlobMkt = (SrvaAdjCodesData) l_globMktAL.get(l_i);
            l_dataSpecMkt = (SrvaAdjCodesData) l_specMktHM.get(l_dataGlobMkt.getAdjCode());   
            l_data = combineData(l_dataGlobMkt, l_dataSpecMkt, p_market, p_useCurrent);
            // Remove entry from HasMap 
            if (l_dataSpecMkt != null)
            {
                l_specMktHM.remove(l_dataGlobMkt.getAdjCode());
            }
            // Add to result DataSet
            if (l_data != null)
            {
                l_returnData.add(l_data);
            }
        } 
        
        // 2.b Travel HashMap of remaining SpecMkt entries
        Collection l_vals = l_specMktHM.values();
        l_iter = l_vals.iterator();
        while ( l_iter.hasNext() )
        {
            l_dataSpecMkt = (SrvaAdjCodesData) l_iter.next();
            l_data = combineData(null, l_dataSpecMkt, p_market, p_useCurrent);
            // Add to result DataSet
            if (l_data != null)
            {
                l_returnData.add(l_data);
            }
        }

        return (l_returnData);

    }

    /**
     * For the same (Market, CodeType, Code) combines a GlobalMarket and 
     * a SpecificMarket entries in order to handle 'non-deleted' issues.
     *  
     * @param p_dataGlobMkt a MiscCodeData entry for the Global Market 
     * @param p_dataSpecMkt a MiscCodeData entry for the Specific Market 
     * @param p_market the specific Market used to select a subset of records.
     * @param p_useCurrent True if use only non-deleted items.
     * 
     * @return a combined MiscCodeData object.
     */
    private SrvaAdjCodesData combineData(
                            SrvaAdjCodesData  p_dataGlobMkt, 
                            SrvaAdjCodesData  p_dataSpecMkt, 
                            Market            p_market, 
                            boolean           p_useCurrent)
    {
        SrvaAdjCodesData l_data = null;
        
        if (p_dataGlobMkt == null)
        {
            // Only a SpecificMarket entry, keep it, barring 
            // non-deleted condition
            if ( !p_dataSpecMkt.isDeleted() 
                 || 
                 (p_dataSpecMkt.isDeleted() && !p_useCurrent) )
            {
                l_data = p_dataSpecMkt;
            }
        }
        else
        {
            if (p_dataSpecMkt == null)
            {
                // Only a GlobalMarket entry, keep it, barring 
                // non-deleted condition
                if ( !p_dataGlobMkt.isDeleted() 
                     || 
                     (p_dataGlobMkt.isDeleted() && !p_useCurrent) )
                {
                    l_data = p_dataGlobMkt;
                }
            } 
            else 
            {
                // Determine which one to keep depending on non-deleted condition
                // and precedence of SpecificMkt above GlobalMkt
                if (!p_dataSpecMkt.isDeleted() 
                     || 
                     (p_dataSpecMkt.isDeleted() && !p_useCurrent) )
                {
                    l_data = p_dataSpecMkt;
                }
                else if (!p_dataGlobMkt.isDeleted() 
                     || 
                     (p_dataGlobMkt.isDeleted() && !p_useCurrent) )
                {
                    l_data = p_dataGlobMkt;
                }
            }
        }

        return l_data == null ? null : l_data.getExplicInstance(p_market);
    }


    /**
     * Returns a verbose string representation of the object. Can be used for
     * trace and debugging.
     * @return Verbose string representing all data contained in this object.
     */
    public String toVerboseString()
    {
        StringBuffer           l_buffer = new StringBuffer();
        int                    l_loop;

        l_buffer.append(C_CLASS_NAME + "@" + this.hashCode() + "\n");
        if (i_srvaAdjCodesARR != null)
        {
            l_buffer.append("allSrvaAdjCodes=");
            for(l_loop = 0; l_loop < i_srvaAdjCodesARR.length; l_loop++)
            {
                l_buffer.append("\n[" + i_srvaAdjCodesARR[l_loop].toString() + "]");
            }
        }
        else
        {
            l_buffer.append("allSrvaAdjCodes=null");
        }

        return(l_buffer.toString());

    } // End of public method toVerboseString

} // End of public class SrvaAdjCodesDataSet

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
