////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      DedaDedicatedAccountsdata.java
//      DATE            :      28-Aug-2002 
//      AUTHOR          :      Simone Nelson
//      REFERENCE       :      PpaLon#1484/6164
//
//      COPYRIGHT       :      SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :      Class containing deda data forming
//                             part of business config cache.                   
//                             relates to the table deda_dedicated_accounts.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 06/03/06 | M Erskine  | Allow for 10 digit dedicated    | PpacLon#2005/8047
//          |            | account ids.                    |
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema plc 2001 IPRID3501
//
// The copyright in this work belongs to Sema plc. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema plc
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema plc. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a row of the database table deda_dedicated_accounts.
 */
public class DedaDedicatedAccountsData extends DataObject
{

    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DedaDedicatedAccountsData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** The Market. */
    private Market  i_dedaMarket = null;

    /** The Service class. */
    private ServiceClass i_dedaServiceClass = null;

    /** Unique Dedicated Account Id. */
    private long i_dedaAccountId = 0;

    /** Dedicated account Description. */
    private String i_dedaDescription = null;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates new dedicated Accounts details and initialises data.
     * 
     * @param p_request The request to process.
     */
    public DedaDedicatedAccountsData( PpasRequest p_request)
    {
        this ( p_request, new Market(p_request), null, 0, null );
  
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    /**
     * Creates new deda details and initialises data.
     * 
     * @param p_request The request to process.
     * @param p_dedaMarket The Market to which the dedicated account refers.
     * @param p_dedaServiceClass The service class to which the dedicated account is associated.
     * @param p_dedaAccountId The identifier of this dedicated account.
     * @param p_dedaDescription The description of this dedicated account.
     */
    public DedaDedicatedAccountsData(
        PpasRequest  p_request,
        Market       p_dedaMarket,
        ServiceClass p_dedaServiceClass,
        long         p_dedaAccountId,
        String       p_dedaDescription)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10020, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_dedaMarket        = p_dedaMarket;
        i_dedaServiceClass  = p_dedaServiceClass;
        i_dedaAccountId     = p_dedaAccountId;
        i_dedaDescription   = p_dedaDescription;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10060, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the deda market.
     * 
     * @return The market associated with this dedicated account.
     */
    public Market getDedaMarket()
    {
        return i_dedaMarket;
    }

    /** Returns the deda service class.
     * 
     * @return The service class associated with this dedicated account.
     */
    public ServiceClass getDedaServiceClass()
    {
        return(i_dedaServiceClass);
    }

    /** Returns the deda account id.
     * 
     * @return The identifier of this dedicated account.
     */
    public long getDedaAccountId()
    {
        return(i_dedaAccountId);
    }

    /** Returns the deda description.
     * 
     * @return The description of this dedicated account.
     */
    public String getDedaDescription()
    {
        return(i_dedaDescription);
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////