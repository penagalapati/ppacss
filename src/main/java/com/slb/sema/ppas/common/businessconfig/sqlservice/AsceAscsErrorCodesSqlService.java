////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AsceAscsErrorCodesSqlService.java
//      DATE            :       12-May-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS_DEV_SS_?
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Sql service to retrieve data from
//                              asce_ascs_error_codes
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.AsceFailureReasonData;
import com.slb.sema.ppas.common.businessconfig.dataclass.AsceFailureReasonDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Sql service to retrieve data from asce_ascs_error_codes table.
 */
public class AsceAscsErrorCodesSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AsceAscsErrorCodesSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new ascs error codes sql service.
     * 
     * @param p_request The request to process.
     */
    public AsceAscsErrorCodesSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid error codes (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid error codes.
     * @throws PpasSqlException If the data cannot be read.
     */
    public AsceFailureReasonDataSet readAll(PpasRequest    p_request,
                                            JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement                 l_statement = null;
        JdbcResultSet                 l_results = null;
        SqlString                     l_sql;
        
        int                           l_errorCode = 0;
        String                        l_errorDesc = "";
        char                          l_delFlag;
        String                        l_opid;
        PpasDateTime                  l_genYmdhms;

        AsceFailureReasonData         l_asceData;
        Vector                        l_allAsceDataV = new Vector(20, 10);
        AsceFailureReasonDataSet      l_asceDataSet;
        AsceFailureReasonData[]       l_allAsceDataARR;
        AsceFailureReasonData[]       l_asceDataEmptyARR = new AsceFailureReasonData[0];

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                            "Entered " +  C_METHOD_readAll);
        }
        
        l_sql = new SqlString(500, 0,
                              "SELECT " +
                              "asce_error_code, " +
                              "asce_error_description, " +
                              "asce_del_flag, " +
                              "asce_opid, " +
                              "asce_gen_ymdhms " +
                              "FROM asce_ascs_error_codes");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 10110, this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while ( l_results.next(10140) )
        {
            l_errorCode        = l_results.getInt       (10140, "asce_error_code");
            l_errorDesc        = l_results.getTrimString(10145, "asce_error_description");
            l_delFlag          = l_results.getChar      (10190, "asce_del_flag");
            l_opid             = l_results.getTrimString(10200, "asce_opid");
            l_genYmdhms        = l_results.getDateTime  (10210, "asce_gen_ymdhms");

            l_asceData = new AsceFailureReasonData(l_errorCode,
                                                   l_errorDesc,
                                                   (l_delFlag == 'Y'),
                                                   l_opid,
                                                   l_genYmdhms);
                    

            l_allAsceDataV.addElement(l_asceData);
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10240, this, p_request);

        l_allAsceDataARR = (AsceFailureReasonData[])l_allAsceDataV.toArray(l_asceDataEmptyARR);

        l_asceDataSet = new AsceFailureReasonDataSet(l_allAsceDataARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10250, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_asceDataSet;
    }
}
