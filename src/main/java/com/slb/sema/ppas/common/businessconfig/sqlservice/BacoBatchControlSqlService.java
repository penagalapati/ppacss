////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BacoBatchControlSqlService.Java
//      DATE            :       19-July-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#112/3215
//                              PRD_ASCS00_ANA_FD_13
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Service to read the BACO_BATCH_CONTROL table and
//                              return a BacoBatchControlDataSet object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 11-Sep-02 | Mike Hickman  | Specifically exclude the   | PpaLon#1534/6511
//           |               | external recharge service  |
//           |               | class from the SQL.        |
//-----------+---------------+----------------------------+---------------------
// 07-Aug-06 | M Erskine     | Order the returned rows    | PpacLon#2479/9483
//           |               | by execution date/time,    |
//           |               | descending in readAll().   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.BacoBatchControlData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BacoBatchControlDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to read the SRVA_MISC_CODE table and return a
 * MiscCodeDataSet object.
 */
public class BacoBatchControlSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BacoBatchControlSqlService";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new BacoBatchControl sql service.
     * 
     * @param p_request The request to process.
     */
    public BacoBatchControlSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Read the batch control data from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of misc code data objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public BacoBatchControlDataSet readAll(PpasRequest            p_request,
                                           JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement           l_statement;
        JdbcResultSet           l_resultSet;
        SqlString               l_sql;
        BacoBatchControlData    l_batchControlData;
        BacoBatchControlDataSet l_batchControlDataSet;
        String                  l_jobType;
        PpasDateTime            l_jobExecutionDateTime;
        char                    l_status;
        long                    l_jsJobId;
        int                     l_subJobCount;
        PpasDateTime            l_fileDate;
        Integer                 l_sequenceNumber;
        Integer                 l_subSequenceNumber; 
        Long                    l_success;
        Long                    l_failure;
        String                  l_extraData1;
        String                  l_extraData2;
        String                  l_extraData3;
        String                  l_extraData4;
        String                  l_opid;
        PpasDate                l_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_readAll);
        }

        // PpaLon#1534/6511 - Specifically exclude the external recharge
        // service class.
        l_sql = new SqlString(500, 0, 
                    "SELECT baco_job_type," +
                       "baco_job_execution_date_time," +
                       "baco_status," +
                       "baco_js_job_id," +
                       "baco_sub_job_cnt," +
                       "baco_file_date," +
                       "baco_seq_no," +
                       "baco_sub_seq_no," + 
                       "baco_success," +
                       "baco_failure," +
                       "baco_extra_data_1," + 
                       "baco_extra_data_2," +
                       "baco_extra_data_3," + 
                       "baco_extra_data_4," +
                       "baco_opid," +
                       "baco_gen_ymdhms " + 
                "FROM baco_batch_control ORDER BY baco_job_execution_date_time DESC");

        l_batchControlDataSet = new BacoBatchControlDataSet(p_request,
                                                            20,
                                                            10);

        l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_readAll,
                                       11010,
                                       this,
                                       p_request);

        l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME,
                                       C_METHOD_readAll,
                                       11020,
                                       this,
                                       p_request,
                                       l_sql);

        while (l_resultSet.next(11030))
        {
            l_jobType              = l_resultSet.getString(11040, "baco_job_type");
            l_jobExecutionDateTime = l_resultSet.getDateTime(11050, "baco_job_execution_date_time");
            l_status               = l_resultSet.getChar(11060, "baco_status"); 
            l_jsJobId              = l_resultSet.getLong(11060, "baco_js_job_id");
            l_subJobCount          = l_resultSet.getInt(11070, "baco_sub_job_cnt");
            l_fileDate             = l_resultSet.getDateTime(11080, "baco_file_date");
            l_sequenceNumber       = l_resultSet.getIntO(11090, "baco_seq_no");
            l_subSequenceNumber    = l_resultSet.getIntO(11100, "baco_sub_seq_no");
            l_success              = l_resultSet.getLongO(11110, "baco_success");
            l_failure              = l_resultSet.getLongO(11120, "baco_failure");
            l_extraData1           = l_resultSet.getString(11130, "baco_extra_data_1");
            l_extraData2           = l_resultSet.getString(11140, "baco_extra_data_2");
            l_extraData3           = l_resultSet.getString(11150, "baco_extra_data_3");
            l_extraData4           = l_resultSet.getString(11160, "baco_extra_data_4");
            l_opid                 = l_resultSet.getString(11170, "baco_opid");
            l_genYmdhms            = l_resultSet.getDateTime(11180, "baco_gen_ymdhms");

            // Create a new batch control data object...
            l_batchControlData = new BacoBatchControlData(p_request,
                                                          l_jobType,
                                                          l_jobExecutionDateTime,
                                                          l_status,
                                                          l_jsJobId,
                                                          l_subJobCount,
                                                          l_fileDate,
                                                          l_sequenceNumber,
                                                          l_subSequenceNumber,
                                                          l_success,
                                                          l_failure,
                                                          l_extraData1,
                                                          l_extraData2,
                                                          l_extraData3,
                                                          l_extraData4,
                                                          l_opid,
                                                          l_genYmdhms);


            // ...and add it to the batch control data set.
            l_batchControlDataSet.addBatchControlDataObject(l_batchControlData);
        }

        l_resultSet.close(11100);

        l_statement.close( C_CLASS_NAME, C_METHOD_readAll, 12110, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 12120, this,
                "Leaving " + C_METHOD_readAll);
        }

        return(l_batchControlDataSet);
    }
    
}
