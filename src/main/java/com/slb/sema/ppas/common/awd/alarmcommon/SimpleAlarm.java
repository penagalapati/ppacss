/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.common.awd.alarmcommon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SimpleAlarm implements Alarm
{
    
    private String              i_alarmName;
    private String              i_alarmText;
    private int                 i_alarmSeverity;
    private long                i_raisedTimeMillis;

    private String              i_originatingNodeName;
    private String              i_originatingProcessName;

    private Map                 i_parameterMap;
    private boolean             i_parameterMapDefaultsAdded = false;
    
    private static final SimpleDateFormat C_DF_DEFAULT =
            new SimpleDateFormat("dd-MMM-yy HH:mm:ss");

    public SimpleAlarm(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity,
        String                  p_originatingNodeName,
        String                  p_originatingProcessName
    )
    {
        this(   p_alarmName,
                p_alarmText,
                p_alarmSeverity,
                p_originatingNodeName,
                p_originatingProcessName,
                new HashMap()
        );
    }

    public SimpleAlarm(
        String                  p_alarmName,
        String                  p_alarmText,
        int                     p_alarmSeverity,
        String                  p_originatingNodeName,
        String                  p_originatingProcessName,
        Map                     p_parameterMap
    )
    {
        i_alarmName = p_alarmName;
        i_alarmText = p_alarmText;
        i_alarmSeverity = p_alarmSeverity;
        i_originatingNodeName = p_originatingNodeName;
        i_originatingProcessName = p_originatingProcessName;
        i_raisedTimeMillis = System.currentTimeMillis();
        i_parameterMap = p_parameterMap;
    }

    /* (non-Javadoc)
     * @see com.slb.sema.ppas.common.awd.awdcommon.Alarm#getAlarmName()
     */
    public String getAlarmName()
    {
        return i_alarmName;
    }

    /* (non-Javadoc)
     * @see com.slb.sema.ppas.common.awd.awdcommon.Alarm#getAlarmSeverity()
     */
    public int getAlarmSeverity()
    {
        return i_alarmSeverity;
    }

    /* (non-Javadoc)
     * @see com.slb.sema.ppas.common.awd.awdcommon.Alarm#getAlarmText()
     */
    public String getAlarmText()
    {
        return i_alarmText;
    }

    // Time (normal Java UTC millis time) alarm was raised. 
    public long getRaisedTime()
    {
        return i_raisedTimeMillis;
    }

    public String getOriginatingNodeName()
    {
        return i_originatingNodeName;
    }
    
    public String getOriginatingProcessName()
    {
        return i_originatingProcessName;
    }

    public String getParameter(
        String                  p_parameterName
    )
    {
        String                  l_value;
        
        l_value = (String)(getParameterMap().get(p_parameterName));
/* TODO: Take out
        Date                    l_date;
        SimpleDateFormat        l_format;
        String                  l_formatString;
        
        l_value = (String)i_parameterMap.get(p_parameterName);
        if(l_value == null)
        {
            //TODO:<<Sort this out - just call getParameterMap and then getP????>>
            if((p_parameterName != null) && p_parameterName.startsWith("_"))
            {
                if(AlarmParameterConstants.C_PARAM_NAME.equals(p_parameterName))
                {
                    l_value = i_alarmName;
                }
                else if(AlarmParameterConstants.C_PARAM_TEXT.equals(p_parameterName))
                {
                    l_value = i_alarmText;
                }
                else if(AlarmParameterConstants.C_PARAM_SEVERITY.equals(p_parameterName))
                {
                    l_value = Integer.toString(i_alarmSeverity);
                }
                else if(AlarmParameterConstants.C_PARAM_RAISED_TIME_MILLIS.equals(p_parameterName))
                {
                    l_value = Long.toString(i_raisedTimeMillis);
                }
                else if(AlarmParameterConstants.C_PARAM_RAISED_TIME.equals(p_parameterName))
                {
                    l_date = new Date(i_raisedTimeMillis);
                    l_value = C_DF_DEFAULT.format(l_date);
                }
                // TODO: <<Take out????>>
                / *
                else if(p_parameterName.startsWith("_AlarmRaisedTime_"))
                {
                    // Add some faster default formats????>>
                    l_formatString = p_parameterName.substring(
                            "_AlarmRaisedTime_".length() - 1,
                            p_parameterName.length());
                    l_format = C_DF_DEFAULT;
                    try
                    {
                        l_format = new SimpleDateFormat(l_formatString);
                    }
                    catch(Exception l_e)
                    {
                        System.err.println("EXCEPTION creating date format " +
                                l_formatString + ", using default date format.");
                        l_e.printStackTrace();
                    }
                    l_date = new Date(i_raisedTimeMillis);
                    l_value = l_format.format(l_date);
                }
                * /
            }
       }
       */
          
        return l_value;
    }

    public Map getParameterMap()
    {
        if(!i_parameterMapDefaultsAdded)
        {
            setParameter("_AlarmName", i_alarmName);
            setParameter("_AlarmText", i_alarmText);
            setParameter("_AlarmSeverity", Long.toString(i_alarmSeverity));
            setParameter("_AlarmRaisedTimeMillis", Long.toString(i_raisedTimeMillis));
            setParameter("_AlarmRaisedTime",
                    C_DF_DEFAULT.format(new Date(i_raisedTimeMillis)));
            setParameter("_OriginatingNodeName", i_originatingNodeName);
            setParameter("_OriginatingProcessName", i_originatingProcessName);
            i_parameterMapDefaultsAdded = true;
        }
        
        return i_parameterMap;
    }
    
    public void setParameter(
        String                  p_parameterName,
        String                  p_parameterValue
    )
    {
        i_parameterMap.put(p_parameterName, p_parameterValue);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return "SimpleAlarm=[name=" + i_alarmName + ", text=[" + i_alarmText +
                "], severity=" + i_alarmSeverity + ", raisedTimeMillis=" +
                i_raisedTimeMillis + ", origNodeName=" + i_originatingNodeName +
                ", origProcName=" + i_originatingProcessName + "]";
    }


    /**
     * @param p_alarmName
     */
    public void setAlarmName(
        String                  p_alarmName)
    {
        i_alarmName = p_alarmName;
    }

    /**
     * @param p_alarmText
     */
    public void setAlarmText(
        String                  p_alarmText)
    {
        i_alarmText = p_alarmText;
    }

    /**
     * @param p_severity
     */
    public void setAlarmSeverity(
        int                     p_severity
    )
    {
        i_alarmSeverity = p_severity;
    }

    /**
     * @param p_originatingNodeName
     */
    public void setOriginatingNodeName(
        String                  p_originatingNodeName
    )
    {
        i_originatingNodeName = p_originatingNodeName;
    }

    /**
     * @param p_originatingProcessName
     */
    public void setOriginatingProcessName(
        String                  p_originatingProcessName
    )
    {
        i_originatingProcessName = p_originatingProcessName;
    }

    // Time (normal Java UTC millis time) alarm was raised. 
    public void setRaisedTime(long p_raisedTimeMillis)
    {
        i_raisedTimeMillis = p_raisedTimeMillis;
    }

    /**
     * @param p_parameterMap
     */
    public void setParameterMap(
        Map                     p_parameterMap)
    {
        i_parameterMap = p_parameterMap;
        i_parameterMapDefaultsAdded = false;
    }

}
