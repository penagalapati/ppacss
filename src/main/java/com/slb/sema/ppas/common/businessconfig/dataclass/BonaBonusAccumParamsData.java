//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonaBonusAccumParamsData.java
// DATE            :       30-Jun-2007
// AUTHOR          :       Michael Erskine
// REFERENCE       :       PRD_ASCS_GEN_CA_128
//
// COPYRIGHT       :       WM-data 2007
//
// DESCRIPTION     :       A record of bonus scheme data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <Name>     | <Brief description of change.   | PpacLon#XXXX/YYYYY
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Object holding bonus scheme accumulation parameters configuration. */
public class BonaBonusAccumParamsData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonaBonusAccumParamsData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    
    /** Bonus scheme. This is also the primary key for this data. */
    private String              i_bonusSchemeId;
    
    /** Percentage of refill to apply as a bonus. */
    private Float               i_bonusPercent;
    
    /** Adjustment code to use when applying an accumulated bonus. */
    private String              i_adjCode;
    
    /** Adjustment type to use when applying an accumulated bonus. */
    private String              i_adjType;
    
    /** Dedicated account id to apply the adjustment to. Set to zero for the main account. */
    private int                 i_dedAccId;
    
    /** Value based target for accumulated bonus. */
    private Double              i_refillValueTarget;
    
    /** Count based target for accumulated bonus. */
    private Integer             i_refillCountTarget;
    
    /** Number of days in a counting period. */
    private int                 i_countingPeriodDays;
    
    /** Amount of bonus to apply. */
    private Double              i_bonusAmount;
    
    /** Number of days to add to the dedicated account expiry date. */
    private Integer             i_dedAccExpiryDays;
    
    /** Indicates whether to set the dedicated account value in the adjustment. */
    private boolean             i_setDedAccBalance;
        

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    
    /**
     * Standard constructor expecting all parameters.
     * @param p_bonusSchemeId      Bonus scheme identifier.
     * @param p_bonusPercent       Percentage of refill to apply as a bonus.
     * @param p_adjCode            Adjustment code to use when applying a bonus adjustment.
     * @param p_adjType            Adjustment type to use when applying a bonus adjustment.
     * @param p_dedAccId           Account ID to apply the adjustment to.
     * @param p_refillValueTarget  Value-based target for bonus qualification.
     * @param p_refillCountTarget  Count-based target for bonus qualification.
     * @param p_countingPeriodDays Number of days in a counting period.
     * @param p_bonusAmount        Amount of money to apply as a bonus.
     * @param p_dedAccExpiryDays   Number of days to add to the dedicated account expiry date.
     * @param p_setDedAccBalance   Whether or not to set the specified dedicated account balance, as opposed
     *                             to adjusting it.
     */
    public BonaBonusAccumParamsData(String       p_bonusSchemeId,
                                    Float        p_bonusPercent,
                                    String       p_adjCode,
                                    String       p_adjType,
                                    int          p_dedAccId,
                                    Double       p_refillValueTarget,
                                    Integer      p_refillCountTarget,
                                    int          p_countingPeriodDays,
                                    Double       p_bonusAmount,
                                    Integer      p_dedAccExpiryDays,
                                    boolean      p_setDedAccBalance)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonusSchemeId      = p_bonusSchemeId;
        i_bonusPercent       = p_bonusPercent;
        i_adjCode            = p_adjCode;
        i_adjType            = p_adjType;
        i_dedAccId           = p_dedAccId;
        i_refillValueTarget  = p_refillValueTarget;
        i_refillCountTarget  = p_refillCountTarget;
        i_countingPeriodDays = p_countingPeriodDays;
        i_bonusAmount        = p_bonusAmount;
        i_dedAccExpiryDays   = p_dedAccExpiryDays;
        i_setDedAccBalance   = p_setDedAccBalance;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Get the bonus scheme this object represents.
     * @return the bonus scheme
     */
    public String getBonusSchemeId()
    {
        return i_bonusSchemeId;
    }
    
    /**
     * Get the bonus percentage.
     * @return Percentage of refill to apply as a bonus. 
     */
    public Float getBonusPercent()
    {
        return i_bonusPercent;
    }
    
    /**
     * Gets the adjustment code to use for adjustments due to bonus qualification.
     * @return Adjustment code.
     */
    public String getAdjCode()
    {
        return i_adjCode;
    }
    
    /**
     * Gets the adjustment type to use for adjustments due to bonus qualification.
     * @return Adjustment type.
     */
    public String getAdjType()
    {
        return i_adjType;
    }
    
    /**
     * Gets the account ID to apply the adjustment to.
     * @return The dedicated account id, or 0 if this is a main account.
     */
    public int getDedAccId()
    {
        return i_dedAccId;
    }
    
    /**
     * Gets the value-based target for bonus qualification.
     * @return Value target to reach for a bonus to be applied.
     */
    public Double getRefillValueTarget()
    {
        return i_refillValueTarget;
    }
    
    /**
     * Gets the count-based target for bonus qualification.
     * @return Count target to reach for a bonus to be applied.
     */
    public Integer getRefillCountTarget()
    {
        return i_refillCountTarget;
    }
    
    /**
     * Gets the number of days in a counting period.
     * @return Number of days in an accumulating period.
     */
    public int getCountingPeriodDays()
    {
        return i_countingPeriodDays;
    }
    
    /**
     * Gets the amount of bonus to apply.
     * @return An amount to apply as a bonus to an account.
     */
    public Double getBonusAmount()
    {
        return i_bonusAmount;
    }
    
    /**
     * Gets the number of days to add to the dedicated account expiry date.
     * @return
     */
    public Integer getDedAccExpiryDays()
    {
        return i_dedAccExpiryDays;
    }
    
    /**
     * Gets whether or not to set the specified dedicated account balance (as opposed to adjusting it).
     * @return 'true' if a set is required or 'false' otherwise.
     */
    public boolean isSetOfDedAccBalRequired()
    {
        return i_setDedAccBalance;
    }
}
