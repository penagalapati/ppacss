////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CdgrAcceCardGroupSqlService.Java
//      DATE            :       17-Feb-2001
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Provides the Sql required to populate the
//                              Card groups business configuration cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupData;
import com.slb.sema.ppas.common.businessconfig.dataclass.CdgrAcceCardGroupDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * Service to return a cdgr set from the database.
 */
public class CdgrAcceCardGroupSqlService
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CdgrAcceCardGroupSqlService";

    //------------------------------------------------------------------------
    // Private attributes
    //------------------------------------------------------------------------

    /**
     * The Logger to be used to log exceptions generated by this object.
     */
    private Logger             i_logger = null;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new cdgr sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public CdgrAcceCardGroupSqlService(
        PpasRequest            p_request,
        Logger                 p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing CdgrAcceCardGroupSqlService()");
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed CdgrAcceCardGroupSqlService()");
        }

    } // End of public constructor
      //         CdgrAcceCardGroupSqlService(PpasRequest, long, Logger)


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid languages (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Card group data.
     * @throws PpasSqlException If the data cannot be read.
     */
    public CdgrAcceCardGroupDataSet readAll(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement            l_statement = null;
        JdbcResultSet            l_result;
        SqlString                l_sql;
        String                   l_cardGroup;
        String                   l_groupDescription;
        String                   l_deleteFlag;
        CdgrAcceCardGroupData    l_cardGroupData;
        CdgrAcceCardGroupDataSet l_cardGroupSet = null;
        Vector                   l_cardGroupDataV = new Vector(20, 10);

        CdgrAcceCardGroupData [] l_allCardGroupARR;
        CdgrAcceCardGroupData    l_cardGroupEmptyARR[] =
                                               new CdgrAcceCardGroupData[0];

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 84650, this,
                "Entered " + C_METHOD_readAll);
        }

        l_sql =   new SqlString(500, 0, "SELECT " +
                      "cdgr_card_group, " +
                      "cdgr_group_description, " +
                      "cdgr_del_flag " +
                  "FROM " +
                      "cdgr_acce_card_group");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 28760, this,
                "About to execute SQL " + l_sql);
        }

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 29000, this, p_request);

        l_result = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 29100, this, p_request, l_sql);

        while ( l_result.next(51000) )
        {
            l_cardGroup        = l_result.getTrimString(44800, "cdgr_card_group");
            l_groupDescription = l_result.getTrimString(44810, "cdgr_group_description");
            l_deleteFlag       = l_result.getString(    44820, "cdgr_del_flag");

            l_cardGroupData = new CdgrAcceCardGroupData(
                    l_cardGroup,
                    l_groupDescription,
                    l_deleteFlag);

            l_cardGroupDataV.addElement(l_cardGroupData);
        }

        l_allCardGroupARR = (CdgrAcceCardGroupData [])l_cardGroupDataV.toArray (l_cardGroupEmptyARR);

        l_cardGroupSet = new CdgrAcceCardGroupDataSet(p_request, i_logger, l_allCardGroupARR);

        if ( l_result != null )
        {
            l_result.close(53280);
        }

        if ( l_statement != null )
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 90280, this, p_request);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10140, this,
                "Leaving " + C_METHOD_readAll);
        }

        return(l_cardGroupSet);

    } // End of public method
      // readAll(PpasRequest, long, JdbcConnection)
} // End of public class CdgrAcceCardGroupSqlService
