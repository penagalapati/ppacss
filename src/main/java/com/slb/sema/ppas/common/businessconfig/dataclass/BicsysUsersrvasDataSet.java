////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BicsysUsersrvasDataSet.Java
//      DATE            :       25-June-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of market data (from bicsys_usersrvas).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <Name>     | <Description>                   | <Reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an market data, each element of
 * the set representing a record from the srva_mstr table.
 */
public class BicsysUsersrvasDataSet
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BicsysUsersrvasDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of market data records. That is, the data of this DataSet object. */
    private BicsysUsersrvasData []     i_marketsARR;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of markets data.
     * 
     * @param p_request The request to process.
     * @param p_marketsARR Array holding the market data records to be stored
     *                     by this object.
     */
    public BicsysUsersrvasDataSet(
        PpasRequest             p_request,
        BicsysUsersrvasData []  p_marketsARR)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 14510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_marketsARR = p_marketsARR;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 14590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /**
     * Returns the available markets for a certain operator. That is, those not marked as
     * deleted.
     * @param p_opid Operator identifier.
     * @return Array containing all available markets.
     */
    public Vector getSelectedMarkets(String p_opid)
    {
        Vector    l_tempList = null;
        int          l_loop;
        
        l_tempList = new Vector();

        for (l_loop = 0; l_loop < i_marketsARR.length; l_loop++)
        {
            if (i_marketsARR[l_loop].getOpid().equals(p_opid))
            {
                l_tempList.add(i_marketsARR[l_loop].getMarket());
            }
        }

        return(l_tempList);
    }

    /**
     * Returns BicsysUsersrvasData record for the given market.
     * @param p_market Market that selected record must have.
     * @return BicsysUsersrvasData record from this DataSet with the given market. If
     *         no active record can be found with the given market, then 
     *         <code>null</code> is returned.
     */
    public BicsysUsersrvasData getRecord (Market   p_market)
    {
        BicsysUsersrvasData l_bicsysUsersrvasRecord = null;
        int          l_loop;

        for (l_loop = 0; l_loop < i_marketsARR.length; l_loop++)
        {
            if ( i_marketsARR[l_loop].getMarket().equals(p_market) )
            {
                l_bicsysUsersrvasRecord = i_marketsARR[l_loop];
            }
        }

        return (l_bicsysUsersrvasRecord);

    } // End of public method getRecord

    // PpaLon#1613/6834 [END]


    /** Returns array of all records in this data set object.
     * 
     * @return Array of all records. */
    public BicsysUsersrvasData [] getAllArray()
    {
        return (i_marketsARR);

    } // end method getAllArray

    /**
     * Returns a verbose string representation of the object. Can be used for
     * trace and debugging.
     * @return String representation of the data of object.
     */
    public String toVerboseString()
    {
        StringBuffer           l_buffer = new StringBuffer();
        int                    l_loop;

        l_buffer.append(C_CLASS_NAME + "@" + this.hashCode() + "\n");
        if (i_marketsARR != null)
        {
            l_buffer.append("AllBicsysUsersrvas=");
            for (l_loop = 0; l_loop < i_marketsARR.length; l_loop++)
            {
                l_buffer.append("\n[" + i_marketsARR[l_loop].toString() + "]");
            }
        }
        else
        {
            l_buffer.append("AllBicsysUsersrvas=null");
        }

        return(l_buffer.toString());

    } // End of public method toVerboseString

} // End of public class BicsysUsersrvasDataSet

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
