////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAgentMstrSqlService.Java
//      DATE            :       14-Jul-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the agents business config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi       | From the Select statement       | CR#15/440
//          | Isaacs     | Remove agent_type, agent_level, |
//          |            | agent_guide_to,sagm_user_dept   |
//          |            | to reflect PRD_ASCS00_SYD_AS_003|
//----------+------------+---------------------------------+--------------------
// 29/11/05 | M Alm      | Add methods used by the BOI     | PpaLon#1755/7500
//          |            | screen "Agent Information"      | 
//----------+------------+---------------------------------+--------------------
// 30/03/07 | Andy Harris| Add support for suspect flag    | PpaLon#3011/11241
//          |            | column.                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Implements a service that performs the SQL required to populate the
 * agents business config cache.
 */
public class SrvaAgentMstrSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAgentMstrSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Sql Service for populating the agents business
     * configuration cache.
     * 
     * @param p_request The request to process.
     */
    public SrvaAgentMstrSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 31310, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 31390, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all agents data (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return Data set containing all configured agents from the srva_agent_mstr table.
     * @throws PpasSqlException If the data cannot be read.
     */
    public SrvaAgentMstrDataSet readAll (PpasRequest            p_request,
                                         JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement          l_statement;
        SqlString              l_sql;
        int                    l_srva;
        int                    l_sloc;
        Market                 l_market;
        String                 l_agent;
        String                 l_agentName;
        PpasDateTime           l_genYmdhms;
        String                 l_opid;
        char                   l_delFlag;
        char                   l_susFlag;
        SrvaAgentMstrData      l_agentRecord;
        Vector                 l_allAgentsV = new Vector (50, 50);
        SrvaAgentMstrData      l_allAgentsARR[];
        SrvaAgentMstrData      l_agentEmptyARR[] = new SrvaAgentMstrData[0];
        JdbcResultSet          l_results;
        SrvaAgentMstrDataSet   l_allAgentsDataSet;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 31410, this,
                "Entered " +  C_METHOD_readAll);
        }

        l_sql =   new SqlString(200, 0, "SELECT " +
                          "srva, " +
                          "sloc, " +
                          "agent, " +
                          "agent_name, " +
                          "gen_ymdhms, " +
                          "opid, " +
                          "del_flag, " +
                          "suspect_flag " +
                  "FROM srva_agent_mstr ");

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 31420, this, p_request);
        
        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 31430, this, p_request, l_sql);
        while ( l_results.next(31440) )
        {
            l_srva         = l_results.getInt        (11445, "srva");
            l_sloc         = l_results.getInt        (11450, "sloc");
            l_market       = new Market(l_srva, l_sloc);
            l_agent        = l_results.getTrimString (11460, "agent");
            l_agentName    = l_results.getTrimString (11480, "agent_name");
            l_genYmdhms    = l_results.getDateTime   (11520, "gen_ymdhms");
            l_opid         = l_results.getTrimString (11530, "opid");
            l_delFlag      = l_results.getChar       (11540, "del_flag");
            l_susFlag      = l_results.getChar       (11550, "suspect_flag");

            l_agentRecord = new SrvaAgentMstrData(
                    p_request,
                    l_market,
                    l_agent,
                    l_agentName,
                    l_genYmdhms,
                    l_opid,
                    l_delFlag,
                    l_susFlag
                    );
            
            l_allAgentsV.addElement (l_agentRecord);
        }

        l_results.close (31550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 31560, this, p_request);

        l_allAgentsARR = (SrvaAgentMstrData [])
                                    l_allAgentsV.toArray (l_agentEmptyARR);

        l_allAgentsDataSet = new SrvaAgentMstrDataSet(l_allAgentsARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 31590, this,
                "Leaving " + C_METHOD_readAll);
        }

        return (l_allAgentsDataSet);

    } // End of public method readAll
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts an Agent record into the srva_agent_mstr database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_opid                  Operator updating the business config record.
     * @param p_market                Market identifier.
     * @param p_agent                 Agent identifier.
     * @param p_agentName             Agent name.
     * @param p_susFlag               Suspect Flag.
     * @throws PpasSqlException       Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid,
                       Market         p_market,                       
                       String         p_agent,
                       String         p_agentName,
                       char           p_susFlag)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                31600, 
                this,
                "Entered " + C_METHOD_insert);
        }
         
        l_sql = "INSERT INTO srva_agent_mstr (" +
                "srva, " +
                "sloc, " +
                "agent, " +
                "agent_name, " +
                "opid, " +
                "gen_ymdhms, " +
                "suspect_flag) " +
                "VALUES(" + 
                    "{0}, {1}, {2}, {3}, {4}, {5}, {6})";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setStringParam  (2, p_agent);
        l_sqlString.setStringParam  (3, p_agentName);
        l_sqlString.setStringParam  (4, p_opid);
        l_sqlString.setDateTimeParam(5, l_now);
        l_sqlString.setCharParam    (6, p_susFlag);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       31610,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       31620,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 31630, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31640, 
                    this,
                    "Database error: unable to insert row in srva_agent_mstr table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_insert,
                                       31650,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                31660, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    } // End of public method insert 
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates an Agent record in the srva_agent_mstr database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_opid                  Operator updating the business config record.
     * @param p_market                Market identifier.
     * @param p_agent                 Agent identifier.
     * @param p_agentName             Agent name.
     * @param p_susFlag               Suspect Flag.
     * @throws PpasSqlException       Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid,
                       Market         p_market,                       
                       String         p_agent,
                       String         p_agentName,
                       char           p_susFlag)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                31700, 
                this,
                "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE srva_agent_mstr " +
                "SET   agent_name =   {3}, " +
                "      opid       =   {4}, " +
                "      del_flag   =   ' ', " +
                "      gen_ymdhms =   {5}, " +
                "      suspect_flag = {6} " +
                "WHERE agent      = {2} " +
                "AND   srva       = {0} " +
                "AND   sloc       = {1} ";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setStringParam  (2, p_agent);
        l_sqlString.setStringParam  (3, p_agentName);
        l_sqlString.setStringParam  (4, p_opid);
        l_sqlString.setDateTimeParam(5, l_now);
        l_sqlString.setCharParam    (6, p_susFlag);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       31710,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       31720,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 31730, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31740, 
                    this,
                    "Database error: unable to update row in srva_agent_mstr table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_update,
                                       31750,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                31760, 
                this,
                "Leaving " + C_METHOD_update);
        }
    } // End of public method update
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the srva_agent_mstr database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_opid                  Operator updating the business config record.
     * @param p_market                Market identifier.
     * @param p_agent                 Agent identifier.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_opid,
                       Market         p_market,
                       String         p_agent)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasDateTime     l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                31800, 
                this,
                "Entered " + C_METHOD_delete);
        }
 
        l_sql = "UPDATE srva_agent_mstr " +
                "SET   opid       = {3}, " +
                "      del_flag   = '*', " +
                "      gen_ymdhms = {4}  " +
                "WHERE srva       = {0} " +
                "AND   sloc       = {1} " +
                "AND   agent      = {2} ";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setStringParam  (2, p_agent);
        l_sqlString.setStringParam  (3, p_opid);
        l_sqlString.setDateTimeParam(4, l_now);
        
        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       31810,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       31820,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 31830, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31840, 
                    this,
                    "Database error: unable to delete row from srva_agent_mstr table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_delete,
                                       31850,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                31860, 
                this,
                "Leaving " + C_METHOD_delete);
        }
    } // End of public method delete

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn bank record as available in the srva_agent_mstr database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_opid                  Operator updating the business config record.
     * @param p_market                Market identifier.
     * @param p_agent                 Agent identifier.
     * 
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                String         p_opid,
                                Market         p_market,
                                String         p_agent)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql       = null;
        SqlString        l_sqlString = null;
        PpasDateTime     l_now       = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlE  = null;
        
        l_now = DatePatch.getDateTimeNow();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            31900, 
                            this,
                            "Entered " + C_METHOD_markAsAvailable);
        }        
        
        l_sql = "UPDATE srva_agent_mstr " +
                "SET   opid       = {0}, " +
                "      del_flag   = ' ', " +
                "      gen_ymdhms = {1}  " +
                "WHERE srva       = {2} " +
                "AND   sloc       = {3} " +
                "AND   agent      = {4} ";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_opid);
        l_sqlString.setDateTimeParam(1, l_now);
        l_sqlString.setStringParam  (2, p_market.getSrva());
        l_sqlString.setStringParam  (3, p_market.getSloc());
        l_sqlString.setStringParam  (4, p_agent);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       31910,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   31920,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 31930, this, p_request);
            l_statement = null;
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    31940, 
                    this,
                    "Database error: unable to update row in srva_agent_mstr table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               31950,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME, 
                            31960, 
                            this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    } //End of public method markAsAvailable(...).    

} // End of public class SrvaAgentMstrSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
