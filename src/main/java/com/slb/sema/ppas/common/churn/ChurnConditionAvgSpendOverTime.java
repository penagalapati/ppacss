////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionAvgSpendOverTime.java
//      DATE            :       27-Mar-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              average spend over time.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;

/** Identify whether a customer qualifies for churn based on average spend over time. */
public class ChurnConditionAvgSpendOverTime extends ChurnCondition
{
    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C1 - Average Spend over Time. ";
    
    /** Average spend over time. */
    private Money i_avgRefillValueThreshhold;
    
    /** Number of 'months' to track data. */
    private int i_trackPeriod;
    
    /** Constructor for a Churn condition based on average spend.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     * @throws PpasConfigException if properties are incorrect.
     */
    public ChurnConditionAvgSpendOverTime(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
        throws PpasConfigException
    {
        super(p_properties, p_bcCache, p_analyser);

        i_avgRefillValueThreshhold = getMoneyProperty(C_PROP_PREFIX + "aveMonthlyValueThreshold");
        i_trackPeriod = p_properties.getIntProperty(C_PROP_PREFIX + "trackingPeriod", 1);
    }
        
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason)
    {
        boolean l_result;
        
        Money l_trackAverage  = p_data.getTrackingAverageValue();
        Money l_remainAverage = p_data.getRemainingAverageValue();
        
        p_reason.append(C_CONDITION_NAME);

        // Note: Both amounts are positive/zero.
        Money l_difference = getDifference(l_trackAverage, l_remainAverage);
        
        p_reason.append("Average in tracking period (");
        p_reason.append(formatMoney(l_trackAverage));
        p_reason.append(") ");
        p_reason.append(l_difference.isNegative() ? "<" : l_difference.isZero() ? "=" : ">");
        p_reason.append(" older average (");
        p_reason.append(formatMoney(l_remainAverage));
        p_reason.append(")");
        
        if (l_difference.isNegative())
        {
            // Remain average > Track average (refills dropping).
            Money l_threshDiff = getDifference(l_trackAverage, i_avgRefillValueThreshhold);
            
            if (l_threshDiff.isNegative())
            {
                // Tracked average less than threshold
                p_reason.append(", also < threshold (");
                p_reason.append(formatMoney(i_avgRefillValueThreshhold));
                p_reason.append("). " + C_CHURN_TEXT);
                
                l_result = true;
           }
            else
            {
                // Tracked average >= threshold
                p_reason.append(", but ");
                p_reason.append(l_threshDiff.isZero() ? "=" : ">");
                p_reason.append(" threshold (");
                p_reason.append(formatMoney(i_avgRefillValueThreshhold));
                p_reason.append("). " + C_NO_CHURN_TEXT);
                
                l_result = false;
            }
        }
        else
        {
            // Remain average <= Track average (refills increasing).
            p_reason.append(". " + C_NO_CHURN_TEXT);
            
            l_result = false;
        }
        
        return l_result;
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if average spend over last " + i_trackPeriod + " months < "
               + i_avgRefillValueThreshhold + " and also less than average over rest of analysis period";
    }
}
