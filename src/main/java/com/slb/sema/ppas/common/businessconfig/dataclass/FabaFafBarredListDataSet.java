////////////////////////////////////////////////////////////////////////////////
//      ACSC IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      FabaFafBarredListDataSet.Java
//      DATE            :      10-Mar-2004
//      AUTHOR          :      Olivier Duparc
//      REFERENCE       :      PRD_ASCS_GEN_SS_071
//
//      COPYRIGHT       :      ATOS ORIGIN 2004
//
//      DESCRIPTION     :      A set (collection) of Barred Number data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a Barred numbers data set which can be read from
 * the database.
 */
public class FabaFafBarredListDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FabaFafBarredListDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Logger to be used to log exceptions generated within this class. */
    protected Logger   i_logger = null;

    /** Array of valid Barred numbers. */
    private FabaFafBarredListData[] i_fabaFafBarredListArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of Barred numbers data.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger object to allow routing of messages.
     * @param p_fabaFafBarredListArr A set of Barred numbers data objects.
     */
    public FabaFafBarredListDataSet(
        PpasRequest              p_request,
        Logger                   p_logger,
        FabaFafBarredListData[]  p_fabaFafBarredListArr)
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;
        i_fabaFafBarredListArr = p_fabaFafBarredListArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } 

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the array of all Barred numbers data objects held in this object.
     *  @return Array of all Barred numbers data objects held in this object.
     */
    public FabaFafBarredListData[]  getAll()
    {
        return (i_fabaFafBarredListArr);
    }

    /** Check whether a given number is in a list of barred numbers.
     * @param p_number Mobile number to compare with barred list.
     * @param p_market Market of subscriber to check FaF number for.
     * @param p_date   Date for which barring check is needed.
     * @return True if the number is barred on the specified date, otherwise, false.
     */
    public boolean isBarred(String   p_number,
                            Market   p_market,
                            PpasDate p_date)
    {
        boolean l_result = false;

        for(int i = 0; i < i_fabaFafBarredListArr.length && !l_result; i++)
        {
            if (p_market.equals(i_fabaFafBarredListArr[i].getMarket()) &&
                p_number.startsWith(i_fabaFafBarredListArr[i].getNumberPrefix()) &&
                ( p_number.trim().length() == i_fabaFafBarredListArr[i].getNumberLength() ||
                  i_fabaFafBarredListArr[i].getNumberLength() == 0 ) &&
                p_date.isBetween(i_fabaFafBarredListArr[i].getStartDate(),
                                 i_fabaFafBarredListArr[i].getEndDate()))
            {
                l_result = true;
            }
        }
        
        return l_result;    
    }
    
    /** 
     * Returns an array of FaF barred list data objects for the given market.
     * @param p_market Market of FaF-barred numbers.
     * @return Array of FaF barred list data objects.
     */
    public FabaFafBarredListData[] getArray(Market p_market)
    {
        Vector                  l_fafBarredListByMarket = new Vector(5);
        FabaFafBarredListData[] l_emptyArrayList = new FabaFafBarredListData[0];
        
        for(int i = 0; i < i_fabaFafBarredListArr.length; i++)
        {
            if (i_fabaFafBarredListArr[i].getMarket().equals(p_market))
            {
                l_fafBarredListByMarket.add(i_fabaFafBarredListArr[i]);
            }
        }
        
        return ((FabaFafBarredListData[])l_fafBarredListByMarket.toArray(l_emptyArrayList));
    }

    /** 
     * Returns a FaF barred list data object matching the key data passed in.
     * @param p_market Market of FaF-barred numbers.
     * @param p_numberPrefix Prefix of FaF-barred numbers.
     * @param p_numberLength Length of FaF-barred numbers.
     * @param p_startDate Date from which the barring commences.
     * @return FaF barred list data object matching the key data passed in.
     */
    public FabaFafBarredListData getFafBarredListData(Market   p_market,
                                                      String   p_numberPrefix,
                                                      int      p_numberLength,
                                                      PpasDate p_startDate)
    {
        FabaFafBarredListData l_fafBarredListData = null;
        boolean               l_found = false;
        
        for(int i = 0; i < i_fabaFafBarredListArr.length && !l_found; i++)
        {
            if (i_fabaFafBarredListArr[i].getMarket().equals(p_market) &&
                i_fabaFafBarredListArr[i].getNumberPrefix().equals(p_numberPrefix) &&
                i_fabaFafBarredListArr[i].getNumberLength() == p_numberLength &&
                i_fabaFafBarredListArr[i].getStartDate().equals(p_startDate))
            {
                l_fafBarredListData = i_fabaFafBarredListArr[i];
                l_found = true;
            }
        }
        
        return (l_fafBarredListData);
    }
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("AllNumbers", i_fabaFafBarredListArr, p_sb);
    }
}

