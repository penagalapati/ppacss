////////////////////////////////////////////////////////////////////////////////
//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchJobDataSet.java
//    DATE            :       03-May-2004
//    AUTHOR          :       Urban Wigstrom
//    REFERENCE       :       PRD_ASCS00_DEV_SS_071
//
//    COPYRIGHT       :       ATOS ORIGIN 2004
//
//    DESCRIPTION     :       Contains a set of BatchJobData objects.                          
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//        |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

/**Contains a set of BatchJobData objects.*/
public class BatchJobDataSet
{
    /**The vector that contains the <code>BatchJobData</code> objects.*/
    private Vector i_vec = null;

    /**
     * Constructs a new BatchJobDataSet object.
     * @param p_vec The vector that contains the <code>BatchJobData</code> objects.
     */
    public BatchJobDataSet(Vector p_vec)
    {
        i_vec = p_vec;
    }

    /**
     * Returns a vector containing <code>BatchJobData</code> objects.
     * @return A vector containing <code>BatchJobData</code> objects.
     */
    public Vector getDataSet()
    {
        return i_vec;
    }
}