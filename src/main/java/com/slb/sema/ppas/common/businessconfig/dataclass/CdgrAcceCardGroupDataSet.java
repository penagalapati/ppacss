////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CdgrAcceCardGroupDataSet.Java
//      DATE            :       03-Apr-2001
//      AUTHOR          :       Erik Clayton
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Erik Clayton
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a cdgr data set which can be read from
 * the database.
 */
public class CdgrAcceCardGroupDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CdgrAcceCardGroupDataSet";

    //------------------------------------------------------------------------
    // Private attributes
    //------------------------------------------------------------------------

    /**
     * The Logger to be used to log exceptions generated within this class.
     */
    protected Logger   i_logger = null;

    /** The card group records held in this data set. */
    private CdgrAcceCardGroupData []   i_cardGroupDataArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of cdgr data.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_cardGroupDataArr Set of card group data objects.
     */
    public CdgrAcceCardGroupDataSet(
        PpasRequest               p_request,
        Logger                    p_logger,
        CdgrAcceCardGroupData []  p_cardGroupDataArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10110, this,
                 "Constructing CdgrAcceCardGroupDataSet(...)");
        }

        i_logger             = p_logger;
        i_cardGroupDataArr   = p_cardGroupDataArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10150, this,
                 "Constructed CdgrAcceCardGroupDataSet(...)");
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the cdgr data object corresponding to the given card group.
     *  @param p_cardGroup The card group identifying the record to be returned.
     *  @return The card group data record with the given card group identifier.
     */
    public CdgrAcceCardGroupData getCardGroupDetails (String p_cardGroup)
    {
        CdgrAcceCardGroupData l_cardGroupData = null;
        boolean l_found = false;

        if (!(i_cardGroupDataArr == null))
        {
            for (int l_index = 0;
                 l_index < i_cardGroupDataArr.length;
                 l_index++)
            {
                l_cardGroupData = i_cardGroupDataArr[l_index];
                if (l_cardGroupData.getCardGroup().equals(p_cardGroup))
                {
                    l_found = true;
                    break;
                }
            }
        }

        if (!l_found)
        {
            l_cardGroupData = null;
        }

        return (l_cardGroupData);

    } // end method getCardGroupData


    /** Get an array containing the subset of records in this cache that are
     *  available. This does not include records marked for deletion.
     *  @return Array containing the subset of records in this cache that are
     *          available.
     */
    public CdgrAcceCardGroupData [] getAvailableArray ()
    {
        CdgrAcceCardGroupData   l_availableArr[];
        Vector                  l_availableV = new Vector (10, 10);
        int                     l_loop;
        CdgrAcceCardGroupData   l_emptyArr[] = new CdgrAcceCardGroupData[0];

        for (l_loop = 0; l_loop < i_cardGroupDataArr.length; l_loop++)
        {
            if (i_cardGroupDataArr[l_loop].getIsCurrent())
            {
                l_availableV.addElement (i_cardGroupDataArr[l_loop]);
            }
        }

        l_availableArr =
              (CdgrAcceCardGroupData [])l_availableV.toArray(l_emptyArr);

        return (l_availableArr);

    }  // end method getAvailableArray

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_cardGroupDataArr, p_sb);
    }
}

