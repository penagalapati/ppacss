////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AsceFailureReasonDataSet.java
//      DATE            :       10-May-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS00_GAN_CA_11
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Data set hold a collection of records from the
//                              asce_ascs_error_codes table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;

/**
 * Data set hold a collection of records from the asce_ascs_error_codes table.
 */
public class AsceFailureReasonDataSet extends DataSetObject
{
    //-------------------------------------------------------------------------
    // Class level constants
    //-------------------------------------------------------------------------

    /** Constant used for calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AsceFailureReasonDataSet";
    
    //-------------------------------------------------------------------------
    // Private attributes
    //-------------------------------------------------------------------------
    
    /** Array containing all asce data records. */
    private AsceFailureReasonData[] i_ascsFailureReasonDataAllArr = null;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Create an instance of <code>AsceFailureReasonDataSet</code>.
     * @param p_dataArr array of <code>AsceFailureReasonData</code> objects
     */
    public AsceFailureReasonDataSet(AsceFailureReasonData[] p_dataArr)
    {
        i_ascsFailureReasonDataAllArr = p_dataArr;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------

    /**
     * Retrieve all asce records.
     * @return all ascs records from the database
     */
    public AsceFailureReasonData[] getAll()
    {
        return i_ascsFailureReasonDataAllArr;
    }
    
    /**
     * Retrieve all available asce data.
     * @return array containing only those asce rows which are not marked as deleted
     */
    public AsceFailureReasonData[] getAvailableArray()
    {
        AsceFailureReasonData       l_availableAsceArr[];
        Vector                      l_availableV = new Vector();
        int                         l_loop;
        AsceFailureReasonData       l_asceEmptyArr[] = new AsceFailureReasonData[0];

        for(l_loop = 0; l_loop < i_ascsFailureReasonDataAllArr.length; l_loop++)
        {
            if (!i_ascsFailureReasonDataAllArr[l_loop].isDeleted())
            {
                l_availableV.addElement(i_ascsFailureReasonDataAllArr[l_loop]);
            }
        }

        l_availableAsceArr = (AsceFailureReasonData[])l_availableV.toArray(l_asceEmptyArr);

        return l_availableAsceArr;

    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_ascsFailureReasonDataAllArr, p_sb);
    }

    /**
     * Gets the failure description associated with the supplied failure code.
     * @param p_failureCode a failure code
     * @return the error description associated with the supplied failure code
     */
    public String getFailureReason(int p_failureCode)
    {
        String l_desc = null;
        
        for (int i = 0; i < i_ascsFailureReasonDataAllArr.length && l_desc == null; i++)
        {
            if (i_ascsFailureReasonDataAllArr[i].getFailureCode() == p_failureCode)
            {
                l_desc = i_ascsFailureReasonDataAllArr[i].getFailureReason();
            }
        }
        
        return l_desc;
    }
}
