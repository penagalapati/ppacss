//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonsBonusSchemeDataSet.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of bonus scheme data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Object containing a set of <code>BonsBonusSchemeData</code> objects. */
public class BonsBonusSchemeDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonsBonusSchemeDataSet";
    
    /** Constant for no schemes available. */
    private static final String[] C_NO_SCHEMES = new String[0];
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of bonus scheme records. That is, the data of this DataSet object. */
    private BonsBonusSchemeData[] i_allRecords;
    
    /** Array of available bonus scheme data records. That is, the data of this DataSet object. */    
    private BonsBonusSchemeData[] i_availableRecords;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Bonus Scheme data.
     * @param p_request The request to process.
     * @param p_data Array holding the  bonus scheme data records to be stored by this object.
     */
    public BonsBonusSchemeDataSet(PpasRequest             p_request,
                                   BonsBonusSchemeData[]  p_data)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_allRecords = p_data;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns Bonus Scheme data associated with a specified case.
     * 
     * @param p_schemeId The bonus scheme.
     * @return BonsBonusSchemeData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BonsBonusSchemeData getRecord(String p_schemeId)
    {
        BonsBonusSchemeData l_record = null;
        
        for (int i = 0; i < i_allRecords.length; i++)
        {
            if ( i_allRecords[i].getBonusSchemeId().equals(p_schemeId))
            {
                l_record = i_allRecords[i];
                break;
            }
        }

        return l_record;
    }    
    
    /**
     * Returns available (that is, not marked as deleted) Bonus Scheme records associated 
     * with a specified case.
     * 
     * @param p_schemeId The bonus scheme.
     * @return BonsBonusSchemeData record from this DataSet with the given case. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BonsBonusSchemeData getAvailableRecord (String p_schemeId)
    {
        BonsBonusSchemeData l_bonsBonusSchemeRecord = getRecord(p_schemeId);
        
        if (l_bonsBonusSchemeRecord != null && l_bonsBonusSchemeRecord.isDeleted())
        {
            l_bonsBonusSchemeRecord = null;
        }

        return l_bonsBonusSchemeRecord;
    }
    
    /** Returns array of all records in this data set object.
     * 
     * @return Array of all data records.
     */
    public BonsBonusSchemeData[] getAllArray()
    {
        return i_allRecords;
    }    
    
    /**
     * Get a data set containing the all available  records. That is, those not marked as deleted.
     * 
     * @return Data set containing available records.
     */
    public BonsBonusSchemeData[] getAvailableArray()
    {
        if (i_availableRecords == null)
        {
            Vector l_available = new Vector();
            
            for (int i = 0; i < i_allRecords.length; i++)
            {
                if (!i_allRecords[i].isDeleted())
                {
                    l_available.addElement(i_allRecords[i]);
                }
            }
    
            i_availableRecords =
                  (BonsBonusSchemeData[])l_available.toArray(new BonsBonusSchemeData[l_available.size()]);
        }

        return i_availableRecords;
    }    
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_allRecords, p_sb);
    }
    
    /** Get a list of schemes that are applicable based on Service Offerings.
     * 
     * @param p_offerings  Customers Service Offerings.
     * @param p_schemeType 'S' for standard bonus schemes and 'A' for accumulated bonus schemes.
     * @return List of schemes that are valid.
     */
    public String[] getAvailableSchemes(ServiceOfferings p_offerings,
                                        char             p_schemeType)
    {
    	return getAvailableSchemes(p_offerings, p_schemeType, false);
    }
    
    /** Get a list of schemes that are applicable based on Service Offerings.
     * 
     * @param p_offerings  Customers Service Offerings.
     * @param p_schemeType 'S' for standard bonus schemes and 'A' for accumulated bonus schemes.
     * @param p_ignoreStartEndDates <code>true</code> if the start date and end date are to be ignored.
     * @return List of schemes that are valid.
     */
    public String[] getAvailableSchemes(ServiceOfferings p_offerings,
                                        char             p_schemeType,
                                        boolean          p_ignoreStartEndDates)
    {
        Vector l_schemes = new Vector();
        
        PpasDate l_today = DatePatch.getDateToday();

        for (int i = 0; i < i_allRecords.length; i++)
        {
        	boolean l_isAvailable;
            BonsBonusSchemeData l_data = i_allRecords[i];
            
            if (p_ignoreStartEndDates)
            {
            	l_isAvailable = l_data.isBonusActive() && !l_data.isDeleted();
            }
            else
            {
            	l_isAvailable = l_data.isAvailable(l_today);
            }
            
            if (l_isAvailable)
            {
                if (l_data.isRelevant(p_offerings) && (l_data.getSchemeType() == p_schemeType))
                {
                    l_schemes.add(l_data.getBonusSchemeId());
                }
            }
        }
        
        return (String [])l_schemes.toArray(C_NO_SCHEMES);
    }

    /** Get a list of available records for a particular scheme type.
     * 
     * @param p_schemeType  The scheme type to filter on.
     * @return Array of BonsBonusSchemeData objects that are valid.
     */
    public BonsBonusSchemeData[] getAvailableArrayForType(char p_schemeType)
    {
        Vector l_available = new Vector();
        
        for (int i = 0; i < i_allRecords.length; i++)
        {
            
            if (!i_allRecords[i].isDeleted() && i_allRecords[i].getSchemeType() == p_schemeType)
            {
                l_available.addElement(i_allRecords[i]);
            }
        }
        
        return (BonsBonusSchemeData[])l_available.toArray(new BonsBonusSchemeData[l_available.size()]);
    }

}
