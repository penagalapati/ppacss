////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DeciDefaultChargingIndDataSet.java
//      DATE            :       17-Mar-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS_DEV_SS_?
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Holds a set of DeciDefaulfChargingIndData objects
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/11/05 | K Goswami  | Added method getRecord          |PpacLon#1838/7448
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Comparator;
import java.util.TreeSet;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.FamilyAndFriendsNumber;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Holds a set of DeciDefaulfChargingIndData objects.
 */
public class DeciDefaultChargingIndDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DeciDefaultChargingIndDataSet";
    
    /** Comparator. */
    private static final DeciComparator C_COMPARATOR = new DeciComparator();
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Array of DeciDefaultChargingIndData objects, including deleted data. */
    private DeciDefaultChargingIndData[] i_deciAllArr = null;
    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    /**
     * Constructs an instance of DeciDefaultChargingIndDataSet.
     * @param p_deciAllArr array containing all deci data
     */
    public DeciDefaultChargingIndDataSet(DeciDefaultChargingIndData[] p_deciAllArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_deciAllArr = p_deciAllArr;
        
        //if (c_comparator == null)
        //{
        //    c_comparator = new DeciComparator();
        //}

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10010, this,
                "Constructed" + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Retrieve all configured deci data.
     * @return array containing all configured deci data
     */
    public DeciDefaultChargingIndData[] getAll()
    {
        return i_deciAllArr;
    }
    /**
     * Returns Faf Default Charging Indicators data associated with a specified charging indicator id .
     * @param p_startNo The Start Number.
     * 
     * @return DeciDefaultChargingIndData record from this DataSet with the given Start No. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public DeciDefaultChargingIndData getRecord(String p_startNo)
    {
        DeciDefaultChargingIndData l_deciChargIndRecord = null;

        if (p_startNo != null)
        {
            for (int l_loop = 0; l_loop < i_deciAllArr.length; l_loop++)
            {
                if (i_deciAllArr[l_loop].getNumberStart().equals(p_startNo))
                {
                    l_deciChargIndRecord = i_deciAllArr[l_loop];
                    break;
                }
            }
        }

        return (l_deciChargIndRecord);
    }

    /**
     * Retrieve all available deci data.
     * @return array containing only those deci rows which are not marked as deleted
     */
    public DeciDefaultChargingIndData[] getAvailableArray()
    {
        DeciDefaultChargingIndData  l_availableDeciArr[];
        Vector                      l_availableV = new Vector();
        int                         l_loop;
        DeciDefaultChargingIndData  l_deciEmptyArr[] = new DeciDefaultChargingIndData[0];

        for(l_loop = 0; l_loop < i_deciAllArr.length; l_loop++)
        {
            if (!i_deciAllArr[l_loop].isDeleted())
            {
                l_availableV.addElement(i_deciAllArr[l_loop]);
            }
        }

        l_availableDeciArr = (DeciDefaultChargingIndData[])l_availableV.toArray(l_deciEmptyArr);

        return(l_availableDeciArr);

    }

    /**
     * Retrieve a default charging indicator.
     * @param p_number Family and Friends number.
     * @return Default charging indicator data for the specified number (null if cannot match).
     */
    public DeciDefaultChargingIndData getDefaultChargingIndicator(FamilyAndFriendsNumber p_number)
    {
        // This method will use a crude approach to getting the default charging indicator, i.e. scanning
        // through array of data until the end - possibly over-writing a match. This should be more efficient
        // than sorting the array each time.
        
        TreeSet l_sorted = new TreeSet(C_COMPARATOR);
        
        String l_faf = p_number.getNumberAsString();

        for (int i = 0; i < i_deciAllArr.length; i++)
        {
            DeciDefaultChargingIndData l_deci = i_deciAllArr[i];
            if (!l_deci.isDeleted() && (l_deci.getNumberStart().length() <= l_faf.length()))
            {
                String l_partOfMsisdnToMatch = l_faf.substring(0, l_deci.getNumberStart().length());
                if (l_partOfMsisdnToMatch.compareTo(l_deci.getNumberStart()) >= 0 &&
                    l_partOfMsisdnToMatch.compareTo(l_deci.getNumberEnd()) <= 0 )
                {
                    l_sorted.add(l_deci);
                }
            }
        }
        
        return l_sorted.size() > 0 ? (DeciDefaultChargingIndData)l_sorted.last() : null;
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_deciAllArr, p_sb);
    }

    //------------------------------------------------------------------------
    // Private inner class
    //------------------------------------------------------------------------
    /** Inner class to compare DECI records. */
    private static class DeciComparator implements Comparator
    {
        /** Standard constructor. */
        public DeciComparator()
        {
            // No processing required.
        }
        
        /** Compare two DECI objects.
         * @param p_1 First DECI object.
         * @param p_2 Second DECI object.
         * @return {@inheritDoc}
         */
        public int compare(Object p_1, Object p_2)
        {
            if (p_1 instanceof DeciDefaultChargingIndData &&
                p_2 instanceof DeciDefaultChargingIndData)
            {
                if (((DeciDefaultChargingIndData)p_1).getNumberStart().length() >
                    ((DeciDefaultChargingIndData)p_2).getNumberStart().length())
                {
                    return 1;
                }
                else if (((DeciDefaultChargingIndData)p_1).getNumberStart().length() <
                         ((DeciDefaultChargingIndData)p_2).getNumberStart().length())
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }

            if (p_1.hashCode() > p_2.hashCode()) return 1;
            else if (p_1.hashCode() < p_2.hashCode()) return -1;
            else return 0;
        }
    }

}
