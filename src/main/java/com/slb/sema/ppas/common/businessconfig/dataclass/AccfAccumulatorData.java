////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccfAccumulatorData.Java
//      DATE            :       03-Sep-2002
//      AUTHOR          :       Mike Hickman
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_353
//                              PpaLon#1517/6395
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A data record corresponding to a single row of
//                              the accf_accumulator_configuration db table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A data record corresponding to a single row of the 
 * accf_accumulator_configuration db table.
 */
public class AccfAccumulatorData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccfAccumulatorData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** The market that this accumulator belongs to. */
    private Market i_market = null;

    /** The service class that this accumulator belongs to. */
    private ServiceClass i_servClass;

    /** The accumulator ID. */
    private int i_id;

    /** The accumulator description. */
    private String i_description;

    /** The accumulator units description, eg 'Minutes', 'Messages'. */
    private String i_unitsDescription ;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates an accumulator data object.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which the accumulator applies.
     * @param p_servClass The service class associated with the accumulator.
     * @param p_id      The identifier of the accumulator.
     * @param p_description The description associated with this accumulator.
     * @param p_unitsDescription Description of the units the accumulator is held in.
     */
    public AccfAccumulatorData(PpasRequest  p_request,
                               Market       p_market,
                               ServiceClass p_servClass,
                               int          p_id,
                               String       p_description,
                               String       p_unitsDescription)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market           = p_market;
        i_servClass        = p_servClass;
        i_id               = p_id;
        i_description      = p_description;
        i_unitsDescription = p_unitsDescription;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }
    
    /**
     * Constructor when creating a new record.
     * @param p_market           The market to which the accumulator applies.
     * @param p_serviceClass     The service class associated with the accumulator.
     * @param p_accumulatorId    The identifier of the accumulator.
     * @param p_accumulatorDesc  The description associated with this accumulator.
     * @param p_accumulatorUnits Description of the units the accumulator is held in.
     */
    public AccfAccumulatorData( Market             p_market,
                                ServiceClass       p_serviceClass,
                                int                p_accumulatorId,
                                String             p_accumulatorDesc,
                                String             p_accumulatorUnits)
    {
        this(null, p_market, p_serviceClass, p_accumulatorId, p_accumulatorDesc, p_accumulatorUnits);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Get the market that this accumulator belongs to.
     * @return The market to which the accumulator belongs.
     */
    public Market getMarket ()
    {
        return (i_market);
    }

    /** Get the service class that this accumulator belongs to.
     * 
     * @return The service class associated with the accumulator.
     */
    public ServiceClass getServiceClass()
    {
        return (i_servClass);
    }

    /** Get the ID of this accumulator.
     * 
     * @return The identifier of this accumulator.
     */
    public int getId()
    {
        return (i_id);
    }

    /** Get the description of this accumulator.
     * 
     * @return The description associated with this accumulator.
     */
    public String getDescription()
    {
        return (i_description);
    }

    /** Get the accumulator units description, eg 'Minutes', 'Messages'.
     * 
     * @return Description of the units associated with this accumulator.
     */
    public String getUnitsDescription()
    {
        return (i_unitsDescription);
    }
    
    /**
     * Returns the contents of this object in a readable format.
     * @return A String containing the values of the contents of this object.
     */
    public String toString()
    {
        StringBuffer l_stringBuffer = null;
        
        l_stringBuffer = new StringBuffer("");
        l_stringBuffer.append("[i_market: " + i_market);
        l_stringBuffer.append(", i_serviceClass: " + i_servClass);
        l_stringBuffer.append(", i_id: " + i_id);
        l_stringBuffer.append(", i_description: " + i_description);
        l_stringBuffer.append(", i_unitsDescription: " + i_unitsDescription + "]");
        
        return l_stringBuffer.toString();
    }
}
