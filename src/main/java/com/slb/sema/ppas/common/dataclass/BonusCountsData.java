////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BonusCountsData.java
//      DATE            :       21-Jun-2007
//      AUTHOR          :       Michael Erskine
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Used to represent progress towards an 
//                              accumulated bonus.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+---------------------
// dd/mm/yy | <Name>     | <Brief description of change>   | <reference>
//          |            |                                 |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.math.BigDecimal;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A record of progress made towards an accumulated bonus.
 */
public class BonusCountsData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------
    /** Standard class name constant to be used in calls to Middleware methods. */
    private static final String C_CLASS_NAME = "BonusCountsData";

    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------
    
    /** The Customer identifier of the bonus accumulation. */
    private String       i_custId;
    
    /** Bonus scheme id of the accumulation */
    private String       i_schemeId;

    /** Date/Time of opt-in. */
    private PpasDateTime i_optInStartDateTime;
    
    /** Date/Time of opt-out. */
    private PpasDateTime i_optInEndDateTime;

    /** Start date time of this period. */
    private PpasDateTime i_countStartDateTime;
    
    /** End date/time of this perod. */
    private PpasDateTime i_countEndDateTime;
    
    /** Total value of the refills in the counting period. */
    private Money        i_refillTotal;
    
    /** Old total value of the refills in this counting period. */
    private Money        i_oldRefillTotal;
    
    /** Number of refills in the counting period. */
    private long         i_refillCount;
    
    /** Total value of the refills counting towards the next bonus. */
    private Money        i_targetTotal;

    /** Number of refills counting towards the next bonus. */
    private long         i_targetCount;
    
    /** Residue for the next target total to count towards a bonus award. */ 
    private BigDecimal   i_residue                = new BigDecimal(0.00); // Assume zero residue unless set to
                                                                          // something different.

    /** Number of bonuses awarded in this period. */
    private long         i_nbBonusesAwarded;
    
    /** Target threshold value for the bonus scheme opted into. */
    private Double       i_schemeTargetValue;
    
    /** Target threshold number for the bonus scheme opted into. */
    private Integer      i_schemeTargetNumber;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Create a new object based on CUPO data, account currency and period length. This constructor 
     * calculates the relevant start and end dates of the period.
     * 
     * @param p_schemeId           Scheme Identifier.
     * @param p_optInDateTime      Date/time of opt-in.
     * @param p_zero               Zero amount in the currency of the account.
     * @param p_period             Length of period.
     * @param p_schemeTargetValue  Refill value target from business config data.
     * @param p_schemeTargetNumber Refill count target from business config data.
     */
    public BonusCountsData(String       p_schemeId,
                           PpasDateTime p_optInDateTime,
                           Money        p_zero,
                           BonaBonusAccumParamsData p_bonaData)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 32000, this,
                             "Constructing " + C_CLASS_NAME );
        }
        
        int l_period = 0;
        
        i_schemeId           = p_schemeId;
        i_refillTotal        = p_zero;
        i_refillCount        = 0;
        i_targetTotal        = p_zero;
        i_oldRefillTotal     = p_zero;
        i_targetCount        = 0;
        i_nbBonusesAwarded   = 0;
        
        if (p_bonaData != null)
        {
            l_period = p_bonaData.getCountingPeriodDays();
            i_schemeTargetValue  = p_bonaData.getRefillValueTarget();
            i_schemeTargetNumber = p_bonaData.getRefillCountTarget();
        }
        
        calculatePeriodDates(p_optInDateTime, l_period);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 32010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    
    /**
     * Constructor for this object supplying all parameters.
     * @param p_custId
     * @param p_schemeId           Scheme Identifier.
     * @param p_optInStartDateTime Date/time of opt-in.
     * @param p_optInEndDateTime   Date/time of opt-out
     * @param p_countStartDateTime Counting period start date/time.
     * @param p_countEndDateTime   Counting period end date/time.
     * @param p_refillTotal        Total value of refills in the counting period.
     * @param p_refillCount        Total number of refills in the counting period.
     * @param p_targetTotal        Total value of refills counting towards the next bonus.
     * @param p_targetCount        Total number of refills counting towards the next bonus.
     * @param p_nbBonusesAwarded   Number of bonuses awarded in the counting period.
     */
    public BonusCountsData(String        p_custId,
                           String        p_schemeId,
                           PpasDateTime  p_optInStartDateTime,
                           PpasDateTime  p_optInEndDateTime,
                           PpasDateTime  p_countStartDateTime,
                           PpasDateTime  p_countEndDateTime,
                           Money         p_refillTotal,
                           long          p_refillCount,
                           Money         p_targetTotal,
                           long          p_targetCount,
                           long          p_nbBonusesAwarded,
                           BonaBonusAccumParamsData p_bonaData)
    {
        super();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            null, C_CLASS_NAME, 11011, this,
                             "Constructing " + C_CLASS_NAME );
        }
        
        i_custId = p_custId;
        i_schemeId = p_schemeId;
        i_optInStartDateTime = p_optInStartDateTime;
        i_optInEndDateTime = p_optInEndDateTime;
        i_countStartDateTime =  p_countStartDateTime;
        i_countEndDateTime = p_countEndDateTime;
        i_oldRefillTotal = p_refillTotal;
        i_refillTotal = p_refillTotal;
        i_refillCount = p_refillCount;
        i_targetTotal = p_targetTotal;
        i_targetCount = p_targetCount;
        i_nbBonusesAwarded = p_nbBonusesAwarded;
        if (p_bonaData != null)
        {
            i_schemeTargetValue = p_bonaData.getRefillValueTarget();
            i_schemeTargetNumber = p_bonaData.getRefillCountTarget();
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            null, C_CLASS_NAME, 11013, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------

    /**
     * Returns the Cust Id of the account the bonus belongs to.
     *
     * @return Internal identifier of mobile account.
     */
    public String getCustId()
    {
        return i_custId;
    }

    /**
     * Gets the bonus scheme id.
     * @return Accumulated bonus scheme identifier.
     */
    public String getSchemeId()
    {
        return i_schemeId;
    }
    
    /**
     * Gets the date and time of opt-in to this bonus scheme.
     * @return Date and time of accumulated bonus scheme opt-in.
     */
    public PpasDateTime getOptInStartDateTime()
    {
        return i_optInStartDateTime;
    }
    
    /**
     * Gets the date and time of opt-out from this bonus scheme.
     * @return Date and time of accumulated bonus scheme opt-out.
     */
    public PpasDateTime getOptInEndDateTime()
    {
        return i_optInEndDateTime;
    }
    
    /**
     * Gets the date and time of the start of the counting period.
     * @return Start date and time of counting period.
     */
    public PpasDateTime getCountStartDateTime()
    {
        return i_countStartDateTime;
    }
    
    /**
     * Gets the date and time of the end of the counting period.
     * @return End date and time of counting period.
     */
    public PpasDateTime getCountEndDateTime()
    {
        return i_countEndDateTime;
    }
    
    /**
     * Get the total value of the refills in the counting period.
     * @return Total value of the refills in the counting period.
     */
    public Money getRefillTotal()
    {
        return i_refillTotal;
    }
    
    /**
     * Set the total value of the refills in the counting period.
     * @param p_refillTotal New total value of the refills in the counting period.
     */
    public void setRefillTotal(Money p_refillTotal)
    {
        i_refillTotal = p_refillTotal;
    }
    
    /**
     * Get the number of refills in the counting period.
     * @return Number of refills in the counting period
     */
    public long getRefillCount()
    {
        return i_refillCount;
    }
    
    /**
     * Gets the total value of the refills counting towards the next bonus.
     * @return Total value of the refills counting towards the next bonus.
     */
    public Money getTargetTotal()
    {
        return i_targetTotal;
    }
    
    /**
     * Sets the total value of the refills counting towards the next bonus.
     * @param p_targetTotal New total value of the refills counting towards the next bonus.
     */
    public void setTargetTotal(Money p_targetTotal)
    {
        i_targetTotal = p_targetTotal;
    }
    
    /**
     * Get the number of refills counting towards the next bonus.
     * @return The number of refills counting towards the next bonus.
     */
    public long getTargetCount()
    {
        return i_targetCount;
    }
    
    /**
     * Set the number of refills counting towards the next bonus.
     * @param p_targetCount New number of refills counting towards the next bonus.
     */
    public void setTargetCount(long p_targetCount)
    {
        i_targetCount = p_targetCount;
    }
    
    /**
     * Sets the number of refills in the counting period.
     * @param New number of refills in the counting period.
     */
    public void setRefillCount(long p_refillCount)
    {
        i_refillCount = p_refillCount;
    }
    
    /**
     * Gets the number of bonuses awarded in this counting period.
     * @return Number of bonuses awarded.
     */
    public long getBonusesAwarded()
    {
        return i_nbBonusesAwarded;
    }
    
    /**
     * Sets the number of bonuses awarded in this counting period.
     * @param New number of bonuses awarded.
     */
    public void setNbBonusesAwarded(long p_nbBonuses)
    {
        i_nbBonusesAwarded = p_nbBonuses;
    }
    
    /** 
     * Returns whether or not a bonus is due.
     * @return 'true' if a bonus is due and false otherwise.
     */
    public boolean isBonusDue()
    {
        //return i_bonusAmount != 0
        return true;           // TODO: 'true' if a bonus is due and 'false' otherwise. This can be derived
                               // from the data contained in this object. 
    }
    
    /**
     * Checks to see if this is a 'real' CUPA row or a zero 'dummy' one.
     * @return 'true' if this represents an existing CUPA row, 'false' otherwise.
     */
    public boolean isExistingCupaRow()
    {
        return (i_oldRefillTotal != null) && (i_oldRefillTotal.getAmountAsBigDecimal().doubleValue() != 0) &&
                   (i_refillCount - 1 != 0);
    }
    
    public void setResidueAmount(BigDecimal p_amount)
    {
        i_residue = p_amount;
    }
    
    public BigDecimal getResidueAmount()
    {
        return i_residue;
    }
    
    public Double getSchemeTargetValue()
    {
        return i_schemeTargetValue;
    }
    
    public void setSchemeTargetValue(Double p_schemeTargetValue)
    {
        i_schemeTargetValue = p_schemeTargetValue;
    }
    
    public Integer getSchemeTargetNumber()
    {
        return i_schemeTargetNumber;
    }
    
    public void setSchemeTargetNumber(Integer p_schemeTargetNumber)
    {
        i_schemeTargetNumber = p_schemeTargetNumber;
    }

    /**
     * Equals method used for testing.
     *
     * @param p_bonusCountsData <code>BonusCountsData</code> object to be compared with
     *
     * @return <code>true</code> if the current object and the supplied object are the
     * same from a business logic point of view, else <code>false</code>
     */
    public boolean equals(Object p_bonusCountsData)
    {
        boolean l_return = false;
        
        if ((p_bonusCountsData != null) && (p_bonusCountsData instanceof BonusCountsData))
        {
            BonusCountsData l_bonusCountsData = (BonusCountsData)p_bonusCountsData;

            if (i_custId.equals(l_bonusCountsData.getCustId())
                && i_schemeId.equals(l_bonusCountsData.getSchemeId()))
            {
                l_return = true;
            }
        }

        return l_return;
    }
    
    /** Calculate period start/end dates.
     * 
     * @param p_optInDateTime Opt-in date/time.
     * @param p_period Length of period.
     */
    private void calculatePeriodDates(PpasDateTime p_optInDateTime, int p_period)
    {
        i_optInStartDateTime = p_optInDateTime;
        
        String[] l_parts = i_optInStartDateTime.toString_yyyyMMdd_HHcmmcss().split("\\s");
        PpasDate l_periodStart = new PpasDate(l_parts[0], PpasDate.C_DF_yyyyMMdd);
        
        if (!l_parts[1].equals("00:00:00"))
        {
            l_periodStart.add(PpasDate.C_FIELD_DATE, 1);
        }
        
        PpasDate l_today = DatePatch.getDateToday();
        
        int l_numberPeriods = l_periodStart.getDaysDiff(l_today)/p_period;
        
        if (l_numberPeriods > 0)
        {
            l_periodStart.add(PpasDate.C_FIELD_DATE, l_numberPeriods * p_period);
            
            i_countStartDateTime = new PpasDateTime(l_periodStart + " 00:00:00");
        }
        else
        {
            i_countStartDateTime = new PpasDateTime(i_optInStartDateTime);
        }
        
        PpasDate l_periodEnd = new PpasDate(l_periodStart);
        l_periodEnd.add(PpasDate.C_FIELD_DATE, p_period - 1);
        
        i_countEndDateTime = new PpasDateTime(l_periodEnd + " 23:59:59");
    }
    
    public String toString()
    {
        StringBuffer l_sb = new StringBuffer("");
        
        l_sb.append("Dump of BonusCountsData object: \n");
        l_sb.append("  i_schemeId: " + i_schemeId + "\n");
        l_sb.append("  i_refillTotal: " + i_refillTotal + "\n");
        l_sb.append("  i_targetTotal: " + i_targetTotal + "\n");
        l_sb.append("  i_refillCount: " + i_refillCount + "\n");
        l_sb.append("  i_targetCount: " + i_targetCount + "\n");
        l_sb.append("  i_residue    : " + i_residue + "\n");
        l_sb.append("  i_countStartDateTime: " + i_countStartDateTime + "\n");
        l_sb.append("  i_countEndDateTime: " + i_countEndDateTime + "\n");
        l_sb.append("  i_nbBonusesAwarded: " + i_nbBonusesAwarded + "\n");
        l_sb.append("  i_optInStartDateTime: " + i_optInStartDateTime + "\n");
        l_sb.append("  i_optInEndDateTime: " + i_optInEndDateTime + "\n");
        
        return l_sb.toString();
    }
    
}
