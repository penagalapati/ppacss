////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BankBankSqlService.Java
//      DATE            :       11-July-2001
//      AUTHOR          :       Sally Wells
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Service to return a BankBankData object
//                              from the database
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | Copyright info changed.         | PpacLon#1755/7279
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | Four new methods are added:     | PpacLon#1755/7279
//          |            | * insertBankDetails(...)        |
//          |            | * updateBankDetails(...)        |
//          |            | * deleteBankDetails(...)        |
//          |            | * markAsAvailable(...)          |
//          |            |                                 |
//          |            | Method 'readBankDetails' is     |
//          |            | modified (partly rewritten).    |
//          |            | Some comments are modified.     |
//----------+------------+---------------------------------+--------------------
// 18/11/05 | Lars L.    | Review comments corrections.    |PpacLon#1755/7469
//----------+------------+---------------------------------+--------------------
//13/Jan/07 | Yang Liu   | Remove Data Object Status from  | Ppaclon#1970/10936
//          |            | this Class.                     | 
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of change>   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BankBankDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to run SQL to obtain bank details from the database table BANK_BANK.
 */
public class BankBankSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BankBankSqlService";

    //------------------------------------------------------------------------
    // Constructors and Finalizers
    //------------------------------------------------------------------------

    /**
     * Creates a new bank sql service.
     * 
     * @param p_request The request to process.
     */
    public BankBankSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed " + C_CLASS_NAME );
        }

    } // End of public Constructor.

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readBankDetails = "readBankDetails";
    /**
     * Read and returns the bank details from the BANK_BANK database table.
     * 
     * @param p_request     The request to process.
     * @param p_connection  Database Connection.
     * 
     * @return the bank details from the BANK_BANK database table.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public BankBankDataSet readBankDetails(PpasRequest    p_request,
                                           JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement          l_statement   = null;
        JdbcResultSet          l_result      = null;
        SqlString              l_sql         = null;

        BankBankData           l_bankData    = null;
        BankBankDataSet        l_bankDataSet = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 84650, this,
                "Entered " + C_METHOD_readBankDetails);
        }

        l_sql =   new SqlString(1000, 0,
                                "SELECT   bank_code, bank_name, bank_del_flag " +
                                "FROM     bank_bank " +
                                "ORDER BY bank_code");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL,
                p_request, C_CLASS_NAME, 28760, this,
                "About to execute SQL " + l_sql);
        }

        l_bankDataSet = new BankBankDataSet( p_request, 20, 10);

        try
        {
            l_statement =
                p_connection.createStatement(C_CLASS_NAME, C_METHOD_readBankDetails, 29000, this, p_request);
    
            l_result = l_statement.executeQuery(C_CLASS_NAME,
                                                C_METHOD_readBankDetails,
                                                29100,
                                                this,
                                                p_request,
                                                l_sql);
    
            while ( l_result.next(51000) )
            {
    
                l_bankData = new BankBankData(
                                      p_request,
                                      l_result.getTrimString( 44800, "bank_code"),
                                      l_result.getTrimString( 44810, "bank_name"),
                                      l_result.getString( 44820, "bank_del_flag") );
    
                // Add the bank data object to the bank data set.
    
                l_bankDataSet.addBank( l_bankData);
    
            } // end of while next
        }
        finally
        {
            if (l_result != null)
            {
                l_result.close(53280);
                l_result = null;
            }

            if (l_statement != null)
            {
                l_statement.close( C_CLASS_NAME, C_METHOD_readBankDetails, 90280, this, p_request);
                l_statement = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 10140, this,
                "Leaving " + C_METHOD_readBankDetails);
        }

        return(l_bankDataSet);
    } //End of public method readBankDetails(...).


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insertBankDetails = "insertBankDetails";
    /**
     * Attempts to insert a new row into the BANK_BANK database table.
     * 
     * @param p_request     The request to process.
     * @param p_connection  The database connection.
     * @param p_opid        The operator making the change.
     * @param p_bankCode    The bank identifier.
     * @param p_bankName    The name of the bank.
     *  
     * @return The number of the inserted rows.
     * 
     * @throws PpasSqlException If the data cannot be inserted.
     */
    public int insertBankDetails(PpasRequest    p_request,
                                 JdbcConnection p_connection,
                                 String         p_opid,
                                 String         p_bankCode,
                                 String         p_bankName)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;       
        String           l_insStr    = null;
        SqlString        l_sqlString = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlE  = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            16000,
                            this,
                            "Entered " +  C_METHOD_insertBankDetails +
                            ",  bank code: '" + p_bankCode + ",  bank name: '" + p_bankName + "'");
        }

        l_insStr = "INSERT INTO bank_bank (BANK_CODE, BANK_NAME, BANK_OPID, BANK_GEN_YMDHMS, BANK_DEL_FLAG)" +
                   " VALUES ({0}, {1}, {2}, {3}, ' ')";
                       
        l_sqlString = new SqlString(150, 4, l_insStr);
           
        l_sqlString.setStringParam(  0, p_bankCode);
        l_sqlString.setStringParam(  1, p_bankName);
        l_sqlString.setStringParam(  2, p_opid);
        l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());

        try
        {                   
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_insertBankDetails,
                                                       16050,
                                                       this,
                                                       p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insertBankDetails,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }        
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insertBankDetails, 16600, this, p_request);
                l_statement = null;
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10040, 
                                this,
                                "Database error: unable to insert row in bank_bank table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_insertBankDetails,
                                              10050,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());
            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            17590,
                            this,
                            "Leaving " + C_METHOD_insertBankDetails);
        }

        return l_rowCount;     
    } //End of public method insertBankDetails(...).


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateBankDetails = "updateBankDetails";
    /**
     * Attempts to update a row in the BANK_BANK database table.
     * 
     * @param p_request     The request to process.
     * @param p_connection  The database Connection.
     * @param p_opid        The operator making the change.
     * @param p_bankCode    The bank identifier.
     * @param p_bankName    The name of the bank.
     * 
     * @return the number of updated rows.
     * 
     * @throws PpasSqlException If the data cannot be updated.
     */
    public int updateBankDetails(PpasRequest    p_request,
                                 JdbcConnection p_connection,
                                 String         p_opid,
                                 String         p_bankCode,
                                 String         p_bankName)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql       = null;
        SqlString        l_sqlString = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlE  = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            20000,
                            this,
                            "Entered " +  C_METHOD_updateBankDetails);
        }
    
        l_sql = "UPDATE bank_bank "             +
                "SET    BANK_NAME = {0},"       +
                "       BANK_OPID = {1},"       +
                "       BANK_DEL_FLAG = ' ',"   +
                "       BANK_GEN_YMDHMS = {2} " +
                "WHERE  BANK_CODE = {3}";
                       
        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_bankName);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(3, p_bankCode);

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_updateBankDetails,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_updateBankDetails,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_updateBankDetails, 20125, this, p_request);
                l_statement = null;
            }            
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10140, 
                                this,
                                "Database error: unable to update row in acgr_account_group table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_updateBankDetails,
                                              10150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            20150,
                            this,
                            "Leaving " + C_METHOD_updateBankDetails);
        }

        return l_rowCount;     
    } //End of public method updateBankDetails(...).


    /** Used in call to middleware. */
    private static final String C_METHOD_deleteBankDetails = "deleteBankDetails";
    /**
     * Mark a row as having been deleted in BANK_BANK database table.
     * 
     * @param p_request     The request to process.
     * @param p_connection  The database connection.
     * @param p_opid        The operator making the change.
     * @param p_bankCode    The bank identifier.
     * 
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void deleteBankDetails(PpasRequest     p_request,
                                  JdbcConnection  p_connection,
                                  String          p_opid,
                                  String          p_bankCode)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql       = null;
        SqlString        l_sqlString = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlE  = null;
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            32100, 
                            this,
                            "Entered " + C_METHOD_deleteBankDetails);
        }

        l_sql = "UPDATE bank_bank " +
                "SET    BANK_OPID = {0}," + 
                "       BANK_DEL_FLAG = '*'," +
                "       BANK_GEN_YMDHMS = {1} " +
                "WHERE  BANK_CODE = {2}";

        l_sqlString = new SqlString(150, 3, l_sql);
            
        l_sqlString.setStringParam(  0, p_opid);
        l_sqlString.setDateTimeParam(1, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(  2, p_bankCode);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_deleteBankDetails,
                                                       32300,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_deleteBankDetails,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_deleteBankDetails, 32600, this, p_request);
                l_statement = null;
            }            
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 
                                10240, 
                                this,
                                "Database error: unable to mark row as deleted in acgr_account_group table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_deleteBankDetails,
                                              10250,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());
            throw (l_ppasSqlE);
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            32700,
                            this,
                            "Leaving " + C_METHOD_deleteBankDetails);
        }
    } //End of public method deleteBankDetails(...).


    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn bank record as available in the BANK_BANK database table.
     * 
     * @param p_request     The request to process.
     * @param p_connection  The database connection.
     * @param p_bankCode    The bank identifier.
     * @param p_userOpid    The operator making the change.
     * 
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                String         p_bankCode,
                                String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql       = null;
        SqlString        l_sqlString = null;
        int              l_rowCount  = 0;
        PpasSqlException l_ppasSqlE  = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 
                            10300, 
                            this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = "UPDATE bank_bank "     +
                "SET    BANK_OPID = {0},"       +
                "       BANK_DEL_FLAG = ' ',"   +
                "       BANK_GEN_YMDHMS = {1} " +
                "WHERE  BANK_CODE = {2}";

        l_sqlString = new SqlString(500, 4, l_sql);
        
        l_sqlString.setStringParam(  0, p_userOpid);
        l_sqlString.setDateTimeParam(1, DatePatch.getDateTimeNow());
        l_sqlString.setStringParam(  2, p_bankCode);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_markAsAvailable,
                                                       10310,
                                                       this,
                                                       p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_markAsAvailable,
                                                   10320,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
            l_statement = null;
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10340, 
                    this,
                    "Database error: unable to update row in acgr_account_group table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME, 
                            10360, 
                            this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    } //End of public method markAsAvailable(...).
} // End of public class BankBankSqlService.
