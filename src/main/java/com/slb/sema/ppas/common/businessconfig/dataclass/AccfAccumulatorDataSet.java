////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccfAccumulatorDataSet.Java
//      DATE            :       02-sep-2002
//      AUTHOR          :       Mike Hickman
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_353
//                              PpaLon#1517/6395
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A set of data corresponding to the contents of
//                              the accf_accumulator_configuration table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                    | REFERENCE
//----------+------------+--------------------------------+---------------------
// 10/10/02 |Sally Wells | Add methods on AccfAccumumlator|PpaLon#1597/6736
//          |            | DataSet                        |PRD_PPAK00_GEN_CA_396
//----------+------------+--------------------------------+---------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A set of data corresponding to the contents of the 
 * accf_accumulator_configuration db table.
 */
public class AccfAccumulatorDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccfAccumulatorDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Array of accumulator data objects. */
    private AccfAccumulatorData[] i_accfDataSetArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new set of accumulator data.
     * @param p_request The request to process.
     * @param p_dataArr A set of Accumulator data objects.
     */
    public AccfAccumulatorDataSet(PpasRequest            p_request,
                                  AccfAccumulatorData[]  p_dataArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 11510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_accfDataSetArr = p_dataArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 11590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    //PpaLon#1597/6736 [start]
    /**
     * Returns a description according to the market, service class and 
     * accumulator Id.
     * @param p_market Market, to be searched on.
     * @param p_serviceClass Service Class, to be searched on.
     * @param p_id Accumulator Id to be searched on.
     * @return the account description.
     */
    public String getDescription(Market p_market,
                                 ServiceClass p_serviceClass,
                                 int    p_id)
    {
        AccfAccumulatorData  l_accfData         = null;
        String               l_description      = "";
        
        int                  l_index            = 0;
        ServiceClass         l_accfServiceClass = null;

        if (i_accfDataSetArr != null)
        {
            while (l_index < i_accfDataSetArr.length)
            {
                l_accfData = i_accfDataSetArr[l_index];

                l_accfServiceClass = l_accfData.getServiceClass();

                if ( l_accfData.getMarket().equals(p_market) &&
                     l_accfServiceClass.equals(p_serviceClass) &&
                     l_accfData.getId() == p_id )
                {
                    l_description = l_accfData.getDescription();
                    break;
                }
                l_index++;
            }
        }

        return(l_description);

    } // end of getDescriptions(...)

    /**
     * Returns the units description according to the market, service class and 
     * accumulator Id.
     * @param p_market Market, to be searched on.
     * @param p_serviceClass Service Class, to be searched on.
     * @param p_id Accumulator Id to be searched on.
     * @return the account description.
     */
    public String getUnitsDescription(Market p_market,
                                      ServiceClass p_serviceClass,
                                      int    p_id)
    {
        AccfAccumulatorData  l_accfData    = null;
        String               l_description = "";
        int                  l_index       = 0;

        if (i_accfDataSetArr != null)
        {
            while (l_index < i_accfDataSetArr.length)
            {
                l_accfData = i_accfDataSetArr[l_index];

                if ( l_accfData.getMarket().equals(p_market) &&
                     l_accfData.getServiceClass().equals(p_serviceClass) &&
                     l_accfData.getId() == p_id )
                {
                    l_description = l_accfData.getUnitsDescription();
                    break;
                }
                l_index++;
            }
        }

        return(l_description);

    } // end of getUnitsDescription(...)

    /** 
     * Returns the Accf Data Set as an Array.
     * @return Array of AccfData.
     */
    public AccfAccumulatorData [] getAccfArray()
    {
        return ( i_accfDataSetArr );
    }
    /**
     * Returns the number of elements in the Accf array.
     * @return length of accf array
     */
    public int getAccfCount()
    {
        return (i_accfDataSetArr.length);
    }

    /**
     * For the market and service class returns the number of accf's.
     * @param p_market Market to search on.
     * @param p_serviceClass Service class to search on.
     * @return The number of Accumulator records that match the specified market and service class.
     */
    public int getAccfCount(Market p_market,
                            ServiceClass p_serviceClass )
    {
        int                  l_index    = 0;
        AccfAccumulatorData  l_accfData = null;
        int                  l_count    = 0;

        if (i_accfDataSetArr != null)
        {
            while (l_index < i_accfDataSetArr.length)
            {
                l_accfData = i_accfDataSetArr[l_index];
                if ( l_accfData.getMarket().equals(p_market) &&
                     l_accfData.getServiceClass().equals(p_serviceClass) )
                {
                    l_count++;
                }
                l_index++;
            }
        }

        return(l_count);
    } //end of getAccfCount(...)


    //PpaLon#1597/6736 [end]
    
    /** Used in calls to middleware. */
    private static final String C_METHOD_getAccumulatorData = "getAccumulatorData";
    /**
     * Returns the accf_accumulator_configuration object for the supplied Market, Service Class
     * and Accumulator Id.
     * 
     * @param p_market          Market that accumulator is associated with.
     * @param p_serviceClass    Service that accumulator is associated with.
     * @param p_accumulatorId   Accumulator Id.
     * 
     * @return AccfAccumulatorData for the given Market, Service Class and Accumulator Id.
     */
    public AccfAccumulatorData getAccumulatorData(Market       p_market,
                                                  ServiceClass p_serviceClass,
                                                  int          p_accumulatorId)
    {
        int                        l_loop;
        AccfAccumulatorData        l_accumulatorData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             null, 
                             C_CLASS_NAME, 
                             11100, 
                             this,
                             "Entering " + C_METHOD_getAccumulatorData );
        }

        if (i_accfDataSetArr != null)
        {
            for ( l_loop = 0; l_loop < i_accfDataSetArr.length; l_loop++ )
            {
                if (i_accfDataSetArr[l_loop].getMarket().equals(p_market) &&
                    i_accfDataSetArr[l_loop].getServiceClass().equals(p_serviceClass) &&
                    i_accfDataSetArr[l_loop].getId() == p_accumulatorId)
                {
                    l_accumulatorData = i_accfDataSetArr[l_loop];
                    break;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END, 
                             null, 
                             C_CLASS_NAME, 
                             11190, 
                             this,
                             "Leaving " + C_METHOD_getAccumulatorData );
        }

        return l_accumulatorData;
    }
    
    /** Used in calls to middleware. */
    private static final String C_METHOD_getAccfArray = "getAccfArray";
    /**
     * Returns an array of accumulator data objects for the supplied Market and Service Class.
     * 
     * @param p_market          Market that accumulator is associated with.
     * @param p_serviceClass    Service that accumulator is associated with.
     * 
     * @return Array of AccfAccumulatorData for the given Market and Service Class.
     */
    public AccfAccumulatorData[] getAccfArray(Market       p_market,
                                              ServiceClass p_serviceClass)
    {
        int                        l_loop;
        AccfAccumulatorData[]      l_accumulatorData       = null;
        Vector                     l_availableV            = new Vector();
        AccfAccumulatorData[]      l_accumulatorEmptyArr   = new AccfAccumulatorData[0];

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             null, 
                             C_CLASS_NAME, 
                             11200, 
                             this,
                             "Entering " + C_METHOD_getAccfArray );
        }
        
        if (i_accfDataSetArr != null)
        {
            for ( l_loop = 0; l_loop < i_accfDataSetArr.length; l_loop++ )
            {
                if (i_accfDataSetArr[l_loop].getMarket().equals(p_market) &&
                    i_accfDataSetArr[l_loop].getServiceClass().equals(p_serviceClass))
                {
                    l_availableV.add(i_accfDataSetArr[l_loop]);
                }
            }
        }
        
        l_accumulatorData = (AccfAccumulatorData[])l_availableV.toArray(l_accumulatorEmptyArr);
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_LOW,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END, 
                             null, 
                             C_CLASS_NAME, 
                             11290, 
                             this,
                             "Leaving " + C_METHOD_getAccfArray );
        }

        return l_accumulatorData;
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_accfDataSetArr, p_sb);
    }
}
