////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaMstrCache.Java
//      DATE            :       12-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a cache for the market information
//                              held in the srva_mstr table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaMstrDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaMstrSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the market business configuration data (from the
 * srva_mstr table).
 */
public class SrvaMstrCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaMstrCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private SrvaMstrSqlService i_srvaMstrSqlService = null;

    /** Data set containing all configured market records. */
    private SrvaMstrDataSet i_allMarkets = null;

    /**
     * Cache of available market information (not including records marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private SrvaMstrDataSet i_availableMarkets = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new market information data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_ppasContext The PPAS context containing business and system configuration
     *         data required by this service.
     */
    public SrvaMstrCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasContext            p_ppasContext)
    {
        super (p_logger, p_ppasContext.getProperties());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 82110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_logger = p_logger;
        i_srvaMstrSqlService = new SrvaMstrSqlService (p_request, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 82120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all market data (including records marked as deleted).
     * @return Data set containing all market data configured in the
     *         srva_mstr table.
     */
    public SrvaMstrDataSet getAll()
    {
        return i_allMarkets;
    }

    /**
     * Returns available market data.
     * The returnes data set does not include records
     * marked as deleted.
     * @return Data set containing market data that is not marked as deleted
     *         in the srva_mstr table.
     */
    public SrvaMstrDataSet getAvailable()
    {
        return (i_availableMarkets);

    } // End of public method getAvailable

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 82230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allMarkets = i_srvaMstrSqlService.readAll (p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all markets and old list of
        // available markets, but rather than synchronise this is acceptable.
        i_availableMarkets = new SrvaMstrDataSet(
                                  p_request,
                                  i_allMarkets.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 82290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allMarkets, i_availableMarkets);
    }
}

