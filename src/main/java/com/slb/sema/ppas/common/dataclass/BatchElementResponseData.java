////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchElementResponseData.Java
//      DATE            :       9-Nov-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PRD_PPACS2_DEV_SS_47
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Response data class to hold MSISDN and any
//                              exception related to its processing in the batch.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;

import com.slb.sema.ppas.common.exceptions.PpasServiceException;

/** Response data class to hold MSISDN and any exception related to its processing in the batch. */
public class BatchElementResponseData implements Serializable
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    
    /** MSISDN identifying the subscription. */
    private Msisdn i_msisdn;
    
    /** Exception related to the processing of the request for the MSISDN. */
    private PpasServiceException i_serviceException;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    
    /**
     * Standard constructor.
     * @param p_msisdn MSISDN identifying the subscription.
     * @param p_serviceException Exception related to the processing of the request for the MSISDN.
     */
    public BatchElementResponseData(Msisdn               p_msisdn,
                                    PpasServiceException p_serviceException)
    {
        i_msisdn = p_msisdn;
        i_serviceException = p_serviceException;
    }
    
    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    
    /** 
     * Get MSISDN identifying the subscription.
     * @return MSISDN identifying the subscription.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }
    
    /** 
     * Get exception related to the processing of the request for the MSISDN. 
     * @return Exception related to the processing of the request for the MSISDN. 
     */
    public PpasServiceException getException()
    {
        return i_serviceException;
    }
    
    /** 
     * Set exception related to the processing of the request for the MSISDN. 
     * @param p_serviceException Exception related to the processing of the request for the MSISDN. 
     */
    public void setException(PpasServiceException p_serviceException)
    {
        i_serviceException = p_serviceException;
    }
    
    /** 
     * Returns string representation.
     * @return String representation of the object.
     */
    public String toString()
    {
        return("BatchElementResponseData: " +
                "[i_msisdn = " + i_msisdn + ", " +
                "i_serviceException = " + i_serviceException + "]");
    }
}