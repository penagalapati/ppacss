//////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccumulatedBonusData.java
//      DATE            :       09-Jul-2007
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PRD_ASCS_GEN_CA_128
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Details of an accumulated bonus and/or 
//                              current progress towards an accumulated bonus.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementData;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDate;

/**
 * Details of an accumulated bonus and/or current progress towards an accumulated bonus.
 * @author wmmiesr
 */
public class AccumulatedBonusData extends CalculatedBonusData
{
    /** Details of bonus accumulation parameters. */
    private BonaBonusAccumParamsData i_bona;
    
    /** Details of the current bonus refill accumulation for the account. */
    private BonusCountsData i_bonusCounts;
    
    /** Whether or not it is necessary to record progress towards an accumulated bonus in the database. */
    private boolean i_recordProgressIsRequired = true;
    
    /** Standard constructor.
     * 
     * @param p_bone         Details of the bonus criteria.
     * @param p_bona         Details of bonus accumulator parameters.
     * @param p_bonusCounts  Contains the progress towards an accumulated bonus.
     * @param p_bonusAmount  Amount of any bonus due.
     * @param p_expiryDate   Expiry date of dedicated account or null if the main account.
     * @param p_stopCounting Only record the bonus progress if we've stopped counting.
     */
    public AccumulatedBonusData(BoneBonusElementData     p_bone,    //TODO: Probably don't need BONE...
                                BonaBonusAccumParamsData p_bona,
                                BonusCountsData          p_bonusCounts,
                                Money                    p_bonusAmount,
                                PpasDate                 p_expiryDate,
                                boolean                  p_stopCounting)
    {
        super(p_bone, p_bonusAmount, p_expiryDate);
        i_bona                     = p_bona;
        i_bonusCounts              = p_bonusCounts;
        i_recordProgressIsRequired = p_stopCounting;
    }
    
    /** Get the bonus accumulation parameters.
     * 
     * @return Bonus accumulation parameters.
     */
    public BonaBonusAccumParamsData getBonusAccumParams()
    {
        return i_bona;
    }
    
    /** Set the bonus accumulation parameters.
     * 
     * @param p_bona Accumulated bonus parameters.
     */
    public void setBonusAccumParams(BonaBonusAccumParamsData p_bona)
    {
        i_bona = p_bona;
    }
    
    /** Get the progress towards an accumulated bonus.
     * 
     * @return Progress towards an accumulated bonus.
     */
    public BonusCountsData getBonusCountsData()
    {
        return i_bonusCounts;
    }
    
    /** Returns 'true' if a dedicated account balance set is required and 'false' otherwise.
     * 
     * @return 'true' if the bonus amount is to be used to set a dedicated account balance. 
     */
    public boolean isDedAccSetRequired()
    {
        return i_bona.isSetOfDedAccBalRequired();
    }
    
    /**
     * 
     * @return
     */
    public boolean stopCounting()
    {
        return !i_recordProgressIsRequired;
    }
}