////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AfDnsUpdateResultData.Java
//      DATE            :       24-Oct-2004
//      AUTHOR          :       Martin Brister
//      REFERENCE       :       PRD_PPACS2_DEV_SS_47
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Result data class for an account finder DNS 
//                              update.
//                              This object has been made cloneable to allow 
//                              common data to be replicated within batch
//                              requests.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

/** Result data class for an account finder DNS update. */
public class AfDnsUpdateResultData extends DataObject implements Cloneable
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** 
     * MSISDN identifying the subscription.
     * This value will vary within batched requests.
     */
    private Msisdn i_msisdn;
    
    /** 
     * Index used to identify a request within a batch.
     * This value will vary within batched requests.
     */
    private int i_index;
    
    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /** Simple constructor. */
    public AfDnsUpdateResultData()
    {
        
    }
    
    /**
     * Test constructor. Used within JUnit tests.
     * @param p_msisdn MSISDN identifying the subscription.
     * @param p_index Batch index.
     */
    public AfDnsUpdateResultData(Msisdn p_msisdn,
                                 int    p_index)
    {
        i_msisdn = p_msisdn;
        i_index = p_index;
    }

    //-------------------------------------------------------------------------
    // Public methods
    //-------------------------------------------------------------------------
    /** 
     * Get MSISDN identifying the subscription.
     * @return MSISDN identifying the subscription.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /** 
     * Sets MSISDN identifying the subscription.
     * @param p_msisdn MSISDN for the subscription.
     *        This parameter may need to change whilst preserving the rest of the data object.
     */
    public void setMsisdn(Msisdn p_msisdn)
    {
        i_msisdn = p_msisdn;
    }

    /** 
     * Get batch index.
     * @return Index used to identify a request within a batch.
     */
    public int getIndex()
    {
        return i_index;
    }

    /** 
     * Sets the batch index.
     * @param p_index The index of the batch request.
     */
    public void setIndex(int p_index)
    {
        i_index = p_index;
    }
    
    /**
     * Create e clone of the current object.
     * @return The objects clone.
     * @throws CloneNotSupportedException Unable to create a clone.
     */
    public Object clone()
        throws CloneNotSupportedException
    {
        return super.clone();
    }

    /** 
     * Returns string representation.
     * @return String representation of the object.
     */
    public String toString()
    {
        return("AfDnsUpdateResultData: " +
               "[i_msisdn = " + i_msisdn + ", " +
               "i_index = " + i_index + "]");
    }
}
