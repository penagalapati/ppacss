////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GopaGuiOperatorAccessData.Java
//      DATE            :       22-Jan-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1211/4922
//                              PRD_PPAK00_GEN_CA_318
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Stores a row from the GOPA_GUI_OPERATOR_ACCESS
//                              table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME       | DESCRIPTION                   | REFERENCE
//-----------+------------+-------------------------------+---------------------
// 17/10/05  |K Goswami   | New method                    | PpacLon#463
//           |            | tempBlockUpdateIsPermitted.   |  
//-----------+------------+-------------------------------+---------------------
// 12/12/05  | Paul Rosser| Add check to see if voucher   | PpacLon#1888/7595
//           |            | history enquiry is permitted. | PRD_ASCS00_GEN_CA_039 
//-----------+------------+-------------------------------+---------------------
// 15/12/05  |K Goswami   | Changed code to allow voucher | PpaLon#1892/7603
//           |            | history enquiry privilege     | CA#39
//-----------+------------+---------------------------------+--------------------
// 12/12/05  | M Erskine  | New function access privilege | PpacLon#1882/7556
//           |            |added for overriding the Msisdn| PRD_ASCS00_GEN_CA_65
//           |            | quarantine period. This is a  |
//           |            | licensed feature.             |
//-----------+------------+-------------------------------+--------------------
// 20/03/06  | Ian James  | Add operator access privilege | PpacLon#2055/8226
//           |            | Call History screen function. | PRD_ASCS00_GEN_CA_66
//-----------+------------+-------------------------------+---------------------
// 24-Apr-07 | S James    | Add Single Step Install change| PpacLon#3072/11370
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
  * An object that represents a row from the GOPA_GUI_OPERATOR_ACCESS database 
  * table. This table defines access to screens and functions in the PPAS GUI.
  * This object is thus designed for use with the GUI only - all references to
  * screens and functions refer to GUI functions.
  */
public class GopaGuiOperatorAccessData
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "GopaGuiOperatorAccessData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The privilege ID. */
    private int i_privilegeId;

    /** Indicates whether access to the New Subscriber screen is permitted. */
    private char i_newSubsScreenAccess;

    /** Indicates whether access to the Subscriber Details screen is permitted. */
    private char i_subsDetailsScreenAccess;

    /** Indicates whether access to the Vouchers screen is permitted. */
    private char i_vouchersScreenAccess;

    /** Indicates whether access to the Payments screen is permitted. */
    private char i_paymentsScreenAccess;

    /** Indicates whether access to the Adjustments screen is permitted. */
    private char i_adjustmentsScreenAccess;

    /** Indicates whether access to the Promotions screen is permitted. */
    private char i_promotionsScreenAccess;

    /** Indicates whether access to the Disconnection screen is permitted. */
    private char i_disconnectScreenAccess;

    /** Indicates whether access to the Subordinates screen is permitted. */
    private char i_subordinatesScreenAccess;

    /** Indicates whether access to the Additional Information screen is permitted. */
    private char i_addInfoScreenAccess;

    /** Indicates whether access to the Dedicated Accounts screen is permitted. */
    private char i_dedAccountsScreenAccess;

    /** Indicates whether access to the Accumulators screen is permitted. */
    private char i_accumulatorsScreenAccess;

    /** Indicates whether access to the Comments screen is permitted. */
    private char i_commentsScreenAccess;
    
    /** Indicates whether access to the FaF screen is permitted. */
    private char i_fafScreenAccess;
    
    /** Indicates whether access to the adjustment approval screen is permitted. */
    private char i_adjApprovalScreenAccess;
    
    /** Indicates whether access to the Subscriber Segmentation screen is permitted. */
    private char i_subSegmentScreenAccess;

    /** Indicates whether access to the Charged Event Counters screen is permitted. */
    private char i_eventCountersScreenAccess;
    
    /** Indicates whether access to the Service Fee Deductions screen is permitted. */
    private char i_serviceFeeScreenAccess;
    
    /** Indicates whether updates to the Subscriber Segmentation screen are permitted. */
    private char i_subSegmentUpdatePermitted;

    /** Indicates whether the operator may Update Expiry Dates. */
    private char i_expDatesUpdatePermitted;

    /** Indicates whether the operator may Change Service Class. */
    private char i_servClassChangePermitted;

    /** Indicates whether the operator may Change Language. */
    private char i_languageChangePermitted;

    /** Indicates whether the operator may Update Agent and Sub-agent. */
    private char i_agentUpdatePermitted;

    /** Indicates whether the operator may IVR Unbar subscribers. */
    private char i_ivrUnbarPermitted;

    /** Indicates whether the operator may specify IN Id on install. */
    private char i_installInIdPermitted;

    /** Indicates whether the operator may Update Subscriber Details. */
    private char i_subsDetailsUpdatePermitted;

    /** Indicates whether the operator may Recharge Accounts. */
    private char i_accountRechargePermitted;

    /** Indicates whether the operator may use Voucher Enquiry. */
    private char i_voucherEnquiryPermitted;

    /** Indicates whether the operator may Make Payments. */
    private char i_makePaymentPermitted;

    /** Indicates whether the operator may Make Adjustments . */
    private char i_makeAdjustmentPermitted;

    /** Indicates whether the operator may Update Promotion Plan Allocations. */
    private char i_promotionsUpdatePermitted;

    /** Indicates whether the operator may Add Subordinates . */
    private char i_addSubordinatePermitted;

    /** Indicates whether the operator may Update Additional Information. */
    private char i_addInfoUpdatePermitted;

    /** Indicates whether the operator may Make a Dedicated Account Adjustment. */
    private char i_makeDedAccAdjustPermitted;
    
    /** Indicated whether the operator may modify a subscriber's FaF list. */
    private char i_updateFafList;

    /** Indicates whether the operator may modify a subscriber's accumulator data. */
    private char i_updateAccumPermitted;

    /** Indicates whether the operator may update a voucher's status. */
    private char i_voucherUpdatePermitted;
    
    /** Indicates whether access to the Community Charging screen is permitted. */
    private char i_communityChgScreenAccess;
    
    /** Indicates whether updates to the Community Charging screen are permitted. */
    private char i_communityChgUpdatePermitted;
    
    /** Indicates whether updates to the USSD EoCN selection structure ID are permitted. */
    private char i_ussdEocnIdUpdatePermitted;

    /** Indicates whether access to the adjustment approval screen is permitted. */
    private char i_routingScreenAccess;
    
    /** Indicates whether the operator may update a subscriber's IVR pin code. */
    private char i_subscriberPinUpdatePermitted;
    
    /** Indicates whether the operator may update a subscriber's charged event counters. */
    private char i_eventCountersUpdatePermitted;

    /** Indicates whether the operator may update a subscriber's Home Region. */
    private char i_homeRegionUpdatePermitted;
    
    /** Indicates whether the operator may set or clear temporary blocking. */
    private char i_tempBlockUpdatePermitted;
    
    /** Indicates whether the operator may do a voucher history enquiry. */
    private char i_voucherHistoryPermitted;
    
    /** Indicates whether the operator may override the Msisdn quarantine period. */
    private char i_overrideMsisdnQuarantinePeriodPermitted;
    
    /** Indicates whether the operator may perform a single step install. */
    private char i_singleStepInstallPermitted;
    
    /** The operator who last updated this Access Profile. */
    private String i_opid;

    /** The date that this profile was last updated. */
    private PpasDateTime i_lastModified; 

    /** Indicates whether access to the Call History screen is permitted. */
    private char i_callHistoryScreenAccess;

    /** Indicates whether the operator may perform Account Reconnection. */
    private char i_accountReconnectionPermitted;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /** Simple constructor that sets all screen access attributes to 'N'.
     * 
     * @param p_request The request to process.
     * @param p_privilegeId The privilege level identifier.
     * @param p_opid The operator who last updated this Access Profile. 
     * @param p_lastModified The date that this profile was last updated.
     */
    public GopaGuiOperatorAccessData(PpasRequest  p_request,
                                     int          p_privilegeId,
                                     String       p_opid,
                                     PpasDateTime p_lastModified)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10100, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_privilegeId  = p_privilegeId;
        i_opid         = p_opid;
        i_lastModified = p_lastModified;

        i_newSubsScreenAccess         = 'N';
        i_subsDetailsScreenAccess     = 'N';
        i_vouchersScreenAccess        = 'N';
        i_paymentsScreenAccess        = 'N';
        i_adjustmentsScreenAccess     = 'N';
        i_promotionsScreenAccess      = 'N';
        i_disconnectScreenAccess      = 'N';
        i_subordinatesScreenAccess    = 'N';
        i_addInfoScreenAccess         = 'N';
        i_dedAccountsScreenAccess     = 'N';
        i_accumulatorsScreenAccess    = 'N';
        i_commentsScreenAccess        = 'N';
        i_routingScreenAccess         = 'N';
        i_eventCountersScreenAccess   = 'N';
        i_serviceFeeScreenAccess      = 'N';
        i_expDatesUpdatePermitted     = 'N';
        i_servClassChangePermitted    = 'N';
        i_languageChangePermitted     = 'N';
        i_agentUpdatePermitted        = 'N';
        i_ivrUnbarPermitted           = 'N';
        i_installInIdPermitted        = 'N';
        i_subsDetailsUpdatePermitted  = 'N';
        i_accountRechargePermitted    = 'N';
        i_voucherEnquiryPermitted     = 'N';
        i_makePaymentPermitted        = 'N';
        i_makeAdjustmentPermitted     = 'N';
        i_promotionsUpdatePermitted   = 'N';
        i_addSubordinatePermitted     = 'N';
        i_addInfoUpdatePermitted      = 'N';
        i_makeDedAccAdjustPermitted   = 'N';
        i_updateAccumPermitted        = 'N';
        i_voucherUpdatePermitted      = 'N';
        i_subSegmentScreenAccess      = 'N';
        i_subSegmentUpdatePermitted   = 'N';
        i_communityChgScreenAccess    = 'N';
        i_communityChgUpdatePermitted = 'N';
        i_subscriberPinUpdatePermitted = 'N';
        i_eventCountersUpdatePermitted = 'N';
        i_homeRegionUpdatePermitted   = 'N';
        i_tempBlockUpdatePermitted    = 'N';
        i_voucherHistoryPermitted     = 'N';
        i_overrideMsisdnQuarantinePeriodPermitted = 'N';
        i_singleStepInstallPermitted  = 'N';
        i_callHistoryScreenAccess     = 'N';
        i_accountReconnectionPermitted = 'N';

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10190, this,
                 "Constructed " + C_CLASS_NAME );
        }
    } // end constructor

    /** 
     * Creates a GUI Operator Access profile object.
     * 
     * @param p_request The request to process.
     * @param p_privilegeId The privilege level identifier.
     * @param p_newSubsScreenAccess Indicates whether access to the New Subscriber screen is permitted.
     * @param p_subsDetailsScreenAccess Indicates whether access to the 
     *                                  Subscriber Details screen is permitted.
     * @param p_vouchersScreenAccess Indicates whether access to the Vouchers screen is permitted.
     * @param p_paymentsScreenAccess Indicates whether access to the Payments screen is permitted.
     * @param p_adjustmentsScreenAccess Indicates whether access to the Adjustments screen is permitted.
     * @param p_promotionsScreenAccess Indicates whether access to the Promotions screen is permitted.
     * @param p_disconnectScreenAccess Indicates whether access to the Disconnection screen is permitted.
     * @param p_subordinatesScreenAccess Indicates whether access to the Subordinates screen is permitted.
     * @param p_addInfoScreenAccess Indicates whether access to the Additional 
     *                              Information screen is permitted.
     * @param p_dedAccountsScreenAccess Indicates whether access to the Dedicated
     *                              Accounts screen is permitted.
     * @param p_accumulatorsScreenAccess Indicates whether access to the Accumulators screen is permitted.
     * @param p_commentsScreenAccess Indicates whether access to the Comments screen is permitted.
     * @param p_fafScreenAccess Indicates whether the operator can access F&F.
     * @param p_adjApprovalScreenAccess Indicates whether the operator can access Adjustment Approval.
     * @param p_subsSegmentationScreenAccess Indicates whether access to the Subscriber Segmentation screen
     *                                   is permitted.
     * @param p_communityChgScreenAccess Indicates whether access to the Community Charging screen
     *                                   is permitted.
     * @param p_routingScreenAccess Indicates whether access to the MSISDN routing screen
     *                                   is permitted.
     * @param p_eventCountersScreenAccess Indicates whether the operator can access the 
     *                                    Charged Event Counters screen.
     * @param p_serviceFeeScreenAccess Indicates whether the operator can access the 
     *                                 Service Fee Deductions screen.
     * @param p_expDatesUpdatePermitted Indicates whether the operator may Update Expiry Dates.
     * @param p_servClassChangePermitted Indicates whether the operator may Change Service Class.
     * @param p_languageChangePermitted Indicates whether the operator may Change Language.
     * @param p_agentUpdatePermitted Indicates whether the operator may Update Agent and Sub-agent.
     * @param p_ivrUnbarPermitted Indicates whether the operator may IVR Unbar subscribers.
     * @param p_installInIdPermitted Indicates whether the operator may specify IN ID on install.
     * @param p_subsDetailsUpdatePermitted Indicates whether the operator may Update Subscriber Details.
     * @param p_accountRechargePermitted Indicates whether the operator may Recharge Accounts.
     * @param p_voucherEnquiryPermitted Indicates whether the operator may use Voucher Enquiry.
     * @param p_makePaymentPermitted Indicates whether the operator may Make Payments.
     * @param p_makeAdjustmentPermitted Indicates whether the operator may Make Adjustments .
     * @param p_promotionsUpdatePermitted Indicates whether the operator may
     *                                    Update Promotion Plan Allocations.
     * @param p_addSubordinatePermitted Indicates whether the operator may Add Subordinates .
     * @param p_addInfoUpdatePermitted Indicates whether the operator may Update Additional Information.
     * @param p_makeDedAccAdjustPermitted Indicates whether the operator may make a dedicated Account
     *                                    Adjustment.
     * @param p_updateFafList Indicates whether the operator can update the F&F list.
     * @param p_updateAccumPermitted Indicates whether the operator may modify accumulator data.
     * @param p_voucherUpdatePermitted Indicates whether the operator may update a voucher's status.
     * @param p_subsSegmentationUpdatePermitted Indicates whether the operator may change subscriber 
     *                                          segmentation.
     * @param p_communityChgUpdatePermitted Indicates whether the operator may change community charging 
     *                                      identifiers.
     * @param p_ussdEocnIdUpdatePermitted Indicates whether the operator may change USSD EoCN selection
     *                                     structure identifiers.
     * @param p_subscriberPinUpdatePermitted Indicates whether the operator may update a subscriber's
     *                                       IVR pin code.
     * @param p_eventCountersUpdatePermitted Indicates whether the operator may update a subscriber's
     *                                       charged event counters.
     * @param p_homeRegionUpdatePermitted Indicates whethger the operator may update a subscriber's home
     *                                    region.
     * @param p_tempBlockUpdatePermitted Indicates whether the operator may set or clear temporary blocking
     * @param p_voucherHistoryEnquiryPermitted Indicates whether the operator may view the voucher history
     * @param p_overrideMsisdnQuarantinePeriodPermitted Indicates whether the operator may override the
     *                                                  Msisdn quarantine period
     * @param p_singleStepInstallPermitted Indicates whether the operator may perform a single step install.
     * @param p_callHistoryScreenAccess Indicates whether access to the Call History screen is permitted.
     * @param p_accountReconnectionPermitted Indicates whether the operator may perform an Account Reconnection
     * @param p_opid The operator who last updated this Access Profile. 
     * @param p_lastModified The date that this profile was last updated.
     */
    public GopaGuiOperatorAccessData(PpasRequest  p_request,
                                     int          p_privilegeId,
                                     char         p_newSubsScreenAccess,
                                     char         p_subsDetailsScreenAccess,
                                     char         p_vouchersScreenAccess,
                                     char         p_paymentsScreenAccess,
                                     char         p_adjustmentsScreenAccess,
                                     char         p_promotionsScreenAccess,
                                     char         p_disconnectScreenAccess,
                                     char         p_subordinatesScreenAccess,
                                     char         p_addInfoScreenAccess,
                                     char         p_dedAccountsScreenAccess,
                                     char         p_accumulatorsScreenAccess,
                                     char         p_commentsScreenAccess,
                                     char         p_fafScreenAccess,
                                     char         p_adjApprovalScreenAccess,
                                     char         p_subsSegmentationScreenAccess,
                                     char         p_communityChgScreenAccess,
                                     char         p_routingScreenAccess,
                                     char         p_eventCountersScreenAccess,
                                     char         p_serviceFeeScreenAccess,
                                     char         p_expDatesUpdatePermitted,
                                     char         p_servClassChangePermitted,
                                     char         p_languageChangePermitted,
                                     char         p_agentUpdatePermitted,
                                     char         p_ivrUnbarPermitted,
                                     char         p_installInIdPermitted,
                                     char         p_subsDetailsUpdatePermitted,
                                     char         p_accountRechargePermitted,
                                     char         p_voucherEnquiryPermitted,
                                     char         p_makePaymentPermitted,
                                     char         p_makeAdjustmentPermitted,
                                     char         p_promotionsUpdatePermitted,
                                     char         p_addSubordinatePermitted,
                                     char         p_addInfoUpdatePermitted,
                                     char         p_makeDedAccAdjustPermitted,
                                     char         p_updateFafList,
                                     char         p_updateAccumPermitted,
                                     char         p_voucherUpdatePermitted,
                                     char         p_subsSegmentationUpdatePermitted,
                                     char         p_communityChgUpdatePermitted,
                                     char         p_ussdEocnIdUpdatePermitted,
                                     char         p_subscriberPinUpdatePermitted,
                                     char         p_eventCountersUpdatePermitted,
                                     char         p_homeRegionUpdatePermitted,
                                     char         p_tempBlockUpdatePermitted,
                                     char         p_voucherHistoryEnquiryPermitted,
                                     char         p_overrideMsisdnQuarantinePeriodPermitted,
                                     char         p_singleStepInstallPermitted,
                                     char         p_callHistoryScreenAccess,
                                     char         p_accountReconnectionPermitted,
                                     String       p_opid, 
                                     PpasDateTime p_lastModified)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_privilegeId                 = p_privilegeId;
        i_newSubsScreenAccess         = p_newSubsScreenAccess;
        i_subsDetailsScreenAccess     = p_subsDetailsScreenAccess;
        i_vouchersScreenAccess        = p_vouchersScreenAccess;
        i_paymentsScreenAccess        = p_paymentsScreenAccess;
        i_adjustmentsScreenAccess     = p_adjustmentsScreenAccess;
        i_promotionsScreenAccess      = p_promotionsScreenAccess;
        i_disconnectScreenAccess      = p_disconnectScreenAccess;
        i_subordinatesScreenAccess    = p_subordinatesScreenAccess;
        i_addInfoScreenAccess         = p_addInfoScreenAccess;
        i_dedAccountsScreenAccess     = p_dedAccountsScreenAccess;
        i_accumulatorsScreenAccess    = p_accumulatorsScreenAccess;
        i_commentsScreenAccess        = p_commentsScreenAccess;
        i_fafScreenAccess             = p_fafScreenAccess;
        i_adjApprovalScreenAccess     = p_adjApprovalScreenAccess;
        i_subSegmentScreenAccess      = p_subsSegmentationScreenAccess;
        i_communityChgScreenAccess    = p_communityChgScreenAccess;
        i_routingScreenAccess         = p_routingScreenAccess;
        i_eventCountersScreenAccess   = p_eventCountersScreenAccess;
        i_serviceFeeScreenAccess      = p_serviceFeeScreenAccess;
        i_expDatesUpdatePermitted     = p_expDatesUpdatePermitted;
        i_servClassChangePermitted    = p_servClassChangePermitted;
        i_languageChangePermitted     = p_languageChangePermitted;
        i_agentUpdatePermitted        = p_agentUpdatePermitted;
        i_ivrUnbarPermitted           = p_ivrUnbarPermitted;
        i_installInIdPermitted        = p_installInIdPermitted;
        i_subsDetailsUpdatePermitted  = p_subsDetailsUpdatePermitted;
        i_accountRechargePermitted    = p_accountRechargePermitted;
        i_voucherEnquiryPermitted     = p_voucherEnquiryPermitted;
        i_makePaymentPermitted        = p_makePaymentPermitted;
        i_makeAdjustmentPermitted     = p_makeAdjustmentPermitted;
        i_promotionsUpdatePermitted   = p_promotionsUpdatePermitted;
        i_addSubordinatePermitted     = p_addSubordinatePermitted;
        i_addInfoUpdatePermitted      = p_addInfoUpdatePermitted;
        i_makeDedAccAdjustPermitted   = p_makeDedAccAdjustPermitted;
        i_updateFafList               = p_updateFafList;
        i_updateAccumPermitted        = p_updateAccumPermitted;
        i_voucherUpdatePermitted      = p_voucherUpdatePermitted;
        i_subSegmentUpdatePermitted   = p_subsSegmentationUpdatePermitted;
        i_communityChgUpdatePermitted = p_communityChgUpdatePermitted;
        i_ussdEocnIdUpdatePermitted   = p_ussdEocnIdUpdatePermitted;
        i_subscriberPinUpdatePermitted= p_subscriberPinUpdatePermitted;
        i_eventCountersUpdatePermitted= p_eventCountersUpdatePermitted;
        i_homeRegionUpdatePermitted   = p_homeRegionUpdatePermitted;
        i_tempBlockUpdatePermitted    = p_tempBlockUpdatePermitted;
        i_voucherHistoryPermitted     = p_voucherHistoryEnquiryPermitted;
        i_opid                        = p_opid;
        i_lastModified                = p_lastModified;
        i_overrideMsisdnQuarantinePeriodPermitted = p_overrideMsisdnQuarantinePeriodPermitted;
        i_singleStepInstallPermitted  = p_singleStepInstallPermitted;
        i_callHistoryScreenAccess     = p_callHistoryScreenAccess;
        i_accountReconnectionPermitted = p_accountReconnectionPermitted;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        return("GopaGuiOperatorAccessData" +
               ":i_privilegeId="                + i_privilegeId +
               ":i_newSubsScreenAccess="        + i_newSubsScreenAccess +
               ":i_subsDetailsScreenAccess="    + i_subsDetailsScreenAccess +
               ":i_vouchersScreenAccess="       + i_vouchersScreenAccess +
               ":i_paymentsScreenAccess="       + i_paymentsScreenAccess +
               ":i_adjustmentsScreenAccess="    + i_adjustmentsScreenAccess +
               ":i_promotionsScreenAccess="     + i_promotionsScreenAccess +
               ":i_disconnectScreenAccess="     + i_disconnectScreenAccess +
               ":i_subordinatesScreenAccess="   + i_subordinatesScreenAccess +
               ":i_addInfoScreenAccess="        + i_addInfoScreenAccess +
               ":i_dedAccountsScreenAccess="    + i_dedAccountsScreenAccess +
               ":i_accumulatorsScreenAccess="   + i_accumulatorsScreenAccess +
               ":i_commentsScreenAccess="       + i_commentsScreenAccess +
               ":i_fafScreenAccess="            + i_fafScreenAccess +
               ":i_subSegmentScreenAccess="     + i_subSegmentScreenAccess +
               ":i_communityChgScreenAccess="   + i_communityChgScreenAccess +
               ":i_routingScreenAccess="        + i_routingScreenAccess +
               ":i_eventCountersScreenAccess="  + i_eventCountersScreenAccess +
               ":i_serviceFeeScreenAccess="     + i_serviceFeeScreenAccess +
               ":i_expDatesUpdatePermitted="    + i_expDatesUpdatePermitted +
               ":i_servClassChangePermitted="   + i_servClassChangePermitted +
               ":i_languageChangePermitted="    + i_languageChangePermitted +
               ":i_agentUpdatePermitted="       + i_agentUpdatePermitted +
               ":i_ivrUnbarPermitted="          + i_ivrUnbarPermitted +
               ":i_installInIdPermitted="       + i_installInIdPermitted +
               ":i_subsDetailsUpdatePermitted=" + i_subsDetailsUpdatePermitted +
               ":i_accountRechargePermitted="   + i_accountRechargePermitted +
               ":i_voucherEnquiryPermitted="    + i_voucherEnquiryPermitted +
               ":i_makePaymentPermitted="       + i_makePaymentPermitted +
               ":i_makeAdjustmentPermitted="    + i_makeAdjustmentPermitted +
               ":i_promotionsUpdatePermitted="  + i_promotionsUpdatePermitted +
               ":i_addSubordinatePermitted="    + i_addSubordinatePermitted +
               ":i_addInfoUpdatePermitted="     + i_addInfoUpdatePermitted +
               ":i_makeDedAccAdjustPermitted="  + i_makeDedAccAdjustPermitted +
               ":i_updateFafList="              + i_updateFafList +
               ":i_updateAccumPermitted="       + i_updateAccumPermitted +
               ":i_voucherUpdatePermitted="     + i_voucherUpdatePermitted +
               ":i_subSegmentUpdatePermitted="  + i_subSegmentUpdatePermitted +
               ":i_communChgUpdatePermitted="   + i_communityChgUpdatePermitted +
               ":i_ussdEocnIdUpdatePermitted="  + i_ussdEocnIdUpdatePermitted +
               ":i_subscriberPinUpdatePermitted=" + i_subscriberPinUpdatePermitted +
               ":i_eventCountersUpdatePermitted=" + i_eventCountersUpdatePermitted +
               ":i_homeRegionUpdatePermitted="  + i_homeRegionUpdatePermitted +
               ":i_tempBlockUpdatePermitted="   + i_tempBlockUpdatePermitted +
               ":i_voucherHistoryPermitted="    + i_voucherHistoryPermitted +
               ":i_overrideMsisdnQuarantinePeriodPermitted=" + i_overrideMsisdnQuarantinePeriodPermitted +
               ":i_singleStepInstallPermitted=" + i_singleStepInstallPermitted +
               ":i_callHistoryScreenAccess="    + i_callHistoryScreenAccess +
               ":i_accountReconnectionPermitted=" + i_accountReconnectionPermitted +
               ":i_opid="                       + i_opid +
               ":i_lastModified="               + i_lastModified);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Returns the privilege level identifer.
     * 
     * @return The privilege level.
     */
    public int getPrivilegeId()
    {
        return i_privilegeId;
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * New Subscribers screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasNewSubsScreenAccess()
    {
        return (i_newSubsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Subscriber Segmentation screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasSubscriberSegmentationScreenAccess()
    {
        return (i_subSegmentScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has the provilege to update the 
      * Subscriber Segmentation screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean subscriberSegmentationUpdateIsPermitted()
    {
        return (i_subSegmentUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Community Charging screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasCommunityChargingScreenAccess()
    {
        return (i_communityChgScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Subscriber Details screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasSubsDetailsScreenAccess()
    {
        return (i_subsDetailsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Vouchers screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasVouchersScreenAccess()
    {
        return (i_vouchersScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasPaymentsScreenAccess()
    {
        return (i_paymentsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Adjustments screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasAdjustmentsScreenAccess()
    {
        return (i_adjustmentsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Promotions screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasPromotionsScreenAccess()
    {
        return (i_promotionsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Disconnection screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasDisconnectScreenAccess()
    {
        return (i_disconnectScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Subordinates screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasSubordinatesScreenAccess()
    {
        return (i_subordinatesScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Additional Information screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasAddInfoScreenAccess()
    {
        return (i_addInfoScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Dedicated Accounts screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasDedAccountsScreenAccess()
    {
        return (i_dedAccountsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Accumulators screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasAccumulatorsScreenAccess()
    {
        return (i_accumulatorsScreenAccess == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Comments screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasCommentsScreenAccess()
    {
        return (i_commentsScreenAccess == 'Y');
    }
    
    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * FaF screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasFafScreenAccess()
    {
        return (i_fafScreenAccess == 'Y');
    }
    
    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * adjustment approval screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasAdjApprovalScreenAccess()
    {
        return (i_adjApprovalScreenAccess == 'Y');
    }

    /** 
     * Returns a boolean value indicating whether the CSO has access to the 
     * MSISDN routing information screen.
     * 
     * @return Flag indicating whether access is allowed.
     */
     public boolean hasRoutingScreenAccess()
     {
         return (i_routingScreenAccess == 'Y');
     }

     /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Charged Event Counters screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
      public boolean hasEventCountersScreenAccess()
      {
          return (i_eventCountersScreenAccess == 'Y');
      }

      /** 
       * Returns a boolean value indicating whether the CSO has access to the 
       * Service Fee Deductions screen.
       * 
       * @return Flag indicating whether access is allowed.
       */
       public boolean hasServiceFeeScreenAccess()
       {
           return (i_serviceFeeScreenAccess == 'Y');
       }
      
      /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Update Expiry Dates.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean expDatesUpdateIsPermitted()
    {
        return (i_expDatesUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Change Service Class.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean servClassChangeIsPermitted()
    {
        return (i_servClassChangePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Change Language.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean languageChangeIsPermitted()
    {
        return (i_languageChangePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Update Agent and Sub-agent.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean agentUpdateIsPermitted()
    {
        return (i_agentUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * IVR Unbar subscribers.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean ivrUnbarIsPermitted()
    {
        return (i_ivrUnbarPermitted == 'Y');
    }
    
    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * specify IN ID on install.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean installInIdIsPermitted()
    {
        return (i_installInIdPermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Update Subscriber Details.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean subsDetailsUpdateIsPermitted()
    {
        return (i_subsDetailsUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Recharge Accounts.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean accountRechargeIsPermitted()
    {
        return (i_accountRechargePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * perform Voucher Enquiry.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean voucherEnquiryIsPermitted()
    {
        return (i_voucherEnquiryPermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Make Payments.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean makePaymentIsPermitted()
    {
        return (i_makePaymentPermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Make Adjustments.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean makeAdjustmentIsPermitted()
    {
        return (i_makeAdjustmentPermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Update Promotion Plan allocations.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean promotionsUpdateIsPermitted()
    {
        return (i_promotionsUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Add Subordinates.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean addSubordinateIsPermitted()
    {
        return (i_addSubordinatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Update Additional Information.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean addInfoUpdateIsPermitted()
    {
        return (i_addInfoUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * Make Adjustments to Dedicated Accounts.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean makeDedAccAdjustIsPermitted()
    {
        return (i_makeDedAccAdjustPermitted == 'Y');
    }
    
    /**
     * Returns a boolean value indicating whether the CSO is allowed to
     * update a subscriber's FaF list.
     * 
     * @return flag indicating if an update is allowed
     */
    public boolean updateFafListPermitted()
    {
        return (i_updateFafList == 'Y');
    }

    /**
     * Returns a boolean value indicating whether the CSO is allowed to
     * update a subscriber's accumulator data.
     * 
     * @return flag indicating if an update is allowed
     */
    public boolean updateAccumIsPermitted()
    {
        return (i_updateAccumPermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO is allowed to  
      * perform a voucher status update.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean voucherUpdateIsPermitted()
    {
        return (i_voucherUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has the provilege to update the 
      * Community Charging screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean communityChargingUpdateIsPermitted()
    {
        return (i_communityChgUpdatePermitted == 'Y');
    }

    /** 
      * Returns a boolean value indicating whether the CSO has the provilege to update the
      * USSD EoCN selection structure ID on the Accout Details screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean ussdEocnIdUpdateIsPermitted()
    {
        return (i_ussdEocnIdUpdatePermitted == 'Y');
    }

    /** 
     * Returns a boolean value indicating whether the CSO is allowed to  
     * perform a subscriber IVR pin update.
     * @return Flag indicating whether update is allowed.
     */
    public boolean subscriberPinUpdateIsPermitted()
    {
        return (i_subscriberPinUpdatePermitted == 'Y');
    }

    /** 
     * Returns a boolean value indicating whether the CSO is allowed to  
     * update a subscriber's charged event counters.
     * @return Flag indicating whether update is allowed.
     */
    public boolean eventCountersUpdateIsPermitted()
    {
        return (i_eventCountersUpdatePermitted == 'Y');
    }

    /** 
     * Returns a boolean value indicating whether the CSO is allowed to  
     * update a subscriber's home region.
     * @return Flag indicating whether update is allowed.
     */
    public boolean homeRegionUpdateIsPermitted()
    {
        return (i_homeRegionUpdatePermitted == 'Y');
    } 
    
    /** 
     * Returns a boolean value indicating whether the CSO is allowed to  
     * set or clear Temporary Blocking.
     * @return Flag indicating whether update is allowed.
     */
    public boolean tempBlockUpdateIsPermitted()
    {
        return (i_tempBlockUpdatePermitted == 'Y');
    } 
    
    /** 
     * Returns a boolean value indicating whether the CSO is allowed to  
     * view the voucher history.
     * @return Flag indicating whether viewing is allowed.
     */
    public boolean voucherHistoryIsPermitted()
    {
        return (i_voucherHistoryPermitted == 'Y');
    } 

    /** 
     * Returns a boolean value indicating whether the CSO is allowed to
     * override the MSISD quarantine period.
     * @return Flag indicating whether update is allowed.
     */
    public boolean overrideMsisdnQuarantinePeriodIsPermitted()
    {
        return (i_overrideMsisdnQuarantinePeriodPermitted == 'Y');
    } 

    /** 
     * Returns a boolean value indicating whether the CSO is allowed to
     * perform a single step install.
     * @return Flag indicating whether a single step install is allowed.
     */
    public boolean singleStepInstallIsPermitted()
    {
        return (i_singleStepInstallPermitted == 'Y');
    } 

    /** 
      * Returns a boolean value indicating whether the CSO has access to the 
      * Dedicated Accounts screen.
      * 
      * @return Flag indicating whether access is allowed.
      */
    public boolean hasCallHistoryScreenAccess()
    {
        return (i_callHistoryScreenAccess == 'Y');
    }
    
    /** 
     * Returns a boolean value indicating whether the CSO has access to the 
     * Account Reconnection screen.
     * 
     * @return Flag indicating whether access is allowed.
     */
   public boolean isAccountReconnectionPermitted()
   {
       return (i_accountReconnectionPermitted == 'Y');
   }

   /** Returns the ID of the operator who last updated this Access Profile.
     * 
     * @return Identifier of the operator that last changed this access data.
     */
    public String getOpid()
    {
        return i_opid;
    }

    /** Returns the data and time that this Access Profile was last modified.
     * 
     * @return date/time this access data was last changed.
     */
    public PpasDateTime getLastModified()
    {
        return i_lastModified;
    }
}
