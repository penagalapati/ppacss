////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ApacApplicationAccessDataSet.java
//      DATE            :       09-June-2004
//      AUTHOR          :       Mario Imfeld
//      REFERENCE       :       PpaLon#287/2622 
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       Contains the data selected from the 
//                              APAC_APPLICATION_ACCESS table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Vector;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains ApplicationAccessData objects. */
public class ApacApplicationAccessDataSet
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ApacApplicationAccessDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** A set of ApacApplicationAccessData objects. */
    private Vector   i_apacApplicationAccessDataSetV;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     */
    public ApacApplicationAccessDataSet(PpasRequest p_request,
                                        int         p_initialVSize,
                                        int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_apacApplicationAccessDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Overridden superclass methods.
    //------------------------------------------------------------------------
    /** Returns the contents of the this object in a String.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        StringBuffer  l_stringBuffer = 
                          new StringBuffer("ApacApplicationAccessDataSet=\n[");
        int           l_loop;

        for(l_loop = 0; l_loop < i_apacApplicationAccessDataSetV.size(); l_loop++)
        {
            l_stringBuffer.append((i_apacApplicationAccessDataSetV
                                   .elementAt(l_loop)).toString());
            if ((l_loop + 1) < i_apacApplicationAccessDataSetV.size())
            {
                l_stringBuffer.append("\n");
            }
        }
        l_stringBuffer.append("]");

        return(l_stringBuffer.toString());
    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** 
     * Returns an Operator GUI access profile object.
     *
     * @param p_privilegeId The privilege level of the operator.
     * @return Operator access flags for the specified privilege level.
     */
    public ApacApplicationAccessData getApplicationAccessProfile(int p_privilegeId)
    {
        boolean                   l_notFound = true;
        ApacApplicationAccessData l_data = null;
        ApacApplicationAccessData l_apacApplicationAccessProfile = null;

        for (int l_loop = 0; 
             (l_loop < i_apacApplicationAccessDataSetV.size() && l_notFound);
             l_loop++)
        {
            l_data = (ApacApplicationAccessData)
            i_apacApplicationAccessDataSetV.elementAt(l_loop);
            if (l_data.getPrivilegeId() == p_privilegeId)
            {
                l_apacApplicationAccessProfile = l_data;
                l_notFound = false;
            }
        }

        return l_apacApplicationAccessProfile;
    }

    /**
     * Returns an array of the Privilege Ids 
     * @return An array of Strings containing the definedPrivilege Ids
     */    
    public String[] getPrivilegeIds()
    {
        ArrayList                 l_tempList = new ArrayList();
        int                       l_loop;
        int                       l_count = 0;
        ApacApplicationAccessData l_data = null;
        String[]                  l_privilegeIds = new String[0];

        for (l_loop = 0; l_loop < i_apacApplicationAccessDataSetV.size(); l_loop++)
        {
            l_data = (ApacApplicationAccessData)
                         i_apacApplicationAccessDataSetV.elementAt(l_loop);
                         
            // Language not deleted, add to temporary ArrayList.
            l_tempList.add(String.valueOf(l_data.getPrivilegeId()));
            l_count++;
        }

        return (String[])l_tempList.toArray(l_privilegeIds);

    } // End of public method getPrivilegeIds()

    /** 
     * Adds an element to the data set.
     *
     * @param  p_apacApplicationAccessData The data object to be added.
     */
    public void addElement(ApacApplicationAccessData p_apacApplicationAccessData)
    {
        i_apacApplicationAccessDataSetV.addElement(p_apacApplicationAccessData);
    }
} // end public class