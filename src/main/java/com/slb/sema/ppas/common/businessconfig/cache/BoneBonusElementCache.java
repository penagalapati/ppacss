//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BoneBonusElementCache.java
// DATE            :       15-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for bonus element data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.BoneBonusElementDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BoneBonusElementSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for Bonus Element.
 */
public class BoneBonusElementCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String          C_CLASS_NAME                   = "BoneBonusElementCodeCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The bonus element SQL service to use to load cache.
     */
    private BoneBonusElementSqlService i_bonsBonusElementSqlService = null;

    /** Cache of all valid Bonus Elements(including ones marked as deleted). */
    private BoneBonusElementDataSet    i_allBoneBonusElement        = null;

    /**
     * Cache of available Bonus Elements(not including ones marked as deleted). This attribute exists
     * for performance reasons so the array is not generated from the complete list each time it is required.
     */
    private BoneBonusElementDataSet    i_availableBoneBonusElement  = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Bonus Elementscache.
     * @param p_request The request to process.
     * @param p_logger The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public BoneBonusElementCache(PpasRequest p_request, Logger p_logger, PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonsBonusElementSqlService = new BoneBonusElementSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Bonus Elements(including ones marked as deleted).
     * @return All valid Bonus Elements.
     */
    public BoneBonusElementDataSet getAll()
    {
        return i_allBoneBonusElement;
    }

    /**
     * Returns available valid Bonus Elements(not including ones marked as deleted).
     * @return Available Bonus Elements.
     */
    public BoneBonusElementDataSet getAvailable()
    {
        if (i_availableBoneBonusElement == null)
        {
            i_availableBoneBonusElement = new BoneBonusElementDataSet(null, i_allBoneBonusElement.getAvailableArray());
        }

        return i_availableBoneBonusElement;
    }

    /**
     * Reloads the static configuration into the cache. This method is synchronised so a reload will not cause
     * unexpected errors.
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest p_request, JdbcConnection p_connection) throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered reload");
        }

        i_allBoneBonusElement = i_bonsBonusElementSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all rates and old list of deleted
        // languages, but rather then synchronise this is acceptable.
        i_availableBoneBonusElement = new BoneBonusElementDataSet(p_request, i_allBoneBonusElement
                                                                  .getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allBoneBonusElement);
    }
}

