////////////////////////////////////////////////////////////////////////////////
//    ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BicsysUserfileData.java
//    DATE            :       4-Jun-2004
//    AUTHOR          :       Martin Brister
//    REFERENCE       :       PpacLon#227/2175
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       Holds data from the BICSYS_USERFILE table.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy | <name>     | <description>                   | <reference>
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A data object representing the data from a row in the bicsys_userfile table.
 */
public class BicsysUserfileData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------
    
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BicsysUserfileData";

    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------

    /** Operator id. */
    private String       i_opid;

    /** Operator name. */
    private String       i_opidName;
    
    /** Default market. */
    private Market       i_defaultMarket;
    
    /** Privilege identifier. */
    private int          i_privilegeId;

    /** Maximum single adjustment amount. */
    private Double       i_maxAdjustAmount;
    
    /** Daily adjustment limit. */
    private Double       i_dailyAdjustLimit;
    
    /** Operator password. */
    private String       i_password;

    /** Password expiry date. Empty string means the password will never expire. */
    private PpasDate     i_passwordExpiryDate;
    
    /** Privilege flag for Customer Care GUI. */
    private boolean      i_guiUser;

    /** Privilege flag for POSI. */
    private boolean      i_posiUser;

    /** Privilege flag for PAMI. */
    private boolean      i_pamiUser;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Standard constructor.
     * @param p_request             The request being processed.
     * @param p_opid                Operator id.
     * @param p_opidName            Operator name.
     * @param p_defaultMarket       Default market.
     * @param p_privilegeId         Privilege identifier.
     * @param p_maxAdjustAmount     Maximum single adjustment amount.
     * @param p_dailyAdjustLimit    Daily adjustment limit.
     * @param p_password            Operator password.
     * @param p_passwordExpiryDate  Password expiry date, empty string means the password will never expire.
     * @param p_guiUser             Privilege flag for GUI.
     * @param p_posiUser            Privilege flag for POSI.
     * @param p_pamiUser            Privilege flag for PAMI.
     */
    public BicsysUserfileData (PpasRequest p_request,
                               String      p_opid,
                               String      p_opidName,
                               Market      p_defaultMarket,
                               int         p_privilegeId,
                               Double      p_maxAdjustAmount,
                               Double      p_dailyAdjustLimit,
                               String      p_password,
                               PpasDate    p_passwordExpiryDate,
                               boolean     p_guiUser,
                               boolean     p_posiUser,
                               boolean     p_pamiUser)
    {
        super(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print (PpasDebug.C_LVL_MODERATE,
                             PpasDebug.C_APP_SERVICE ,
                             PpasDebug.C_ST_CONFIN_START,
                             p_request,
                             C_CLASS_NAME, 
                             10010, 
                             this,
                             "Constructing " + C_CLASS_NAME);
        }

        i_opid = p_opid;
        i_opidName = p_opidName;
        i_defaultMarket = p_defaultMarket;
        i_privilegeId = p_privilegeId;
        i_maxAdjustAmount = p_maxAdjustAmount;
        i_dailyAdjustLimit = p_dailyAdjustLimit;
        i_password = p_password;
        i_passwordExpiryDate = p_passwordExpiryDate;
        i_guiUser = p_guiUser; 
        i_posiUser = p_posiUser;
        i_pamiUser = p_pamiUser;
        
        if (PpasDebug.on)
        {
            PpasDebug.print (PpasDebug.C_LVL_MODERATE,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             p_request,
                             C_CLASS_NAME, 
                             10020, 
                             this,
                             "Constructed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------

    /** 
     * Get the id of the operator.
     * @return Operator identifier.
     */
    public String getOpid()
    {
        return (i_opid);
    }

    /** 
     * Get the privilege identifier of the operator.
     * @return Privilege level of this operator.
     */
    public int getPrivilegeId()
    {
        return (i_privilegeId);
    }

    /** 
     * Get the password of the operator.
     * @return The password of this operator.
     */
    public String getPassword()
    {
        return (i_password);
    }

    /** 
     * Get the expiry date for this operators password.
     * @return Expiry date of password.
     */
    public PpasDate getPasswordExpiryDate()
    {
        return i_passwordExpiryDate;
    }

    /** 
     * Get the operator name. 
     * @return Operator name.
     */
    public String getOperatorName()
    {
        return(i_opidName);
    }
    
    /** 
     * Get the default market. 
     * @return Default market.
     */
    public Market getDefaultMarket()
    {
        return(i_defaultMarket);
    }
    
    /** 
     * Get maximum single adjustment amount.
     * @return Maximum single adjustment amount.
     */
    public Double getMaxAdjustAmount()
    {
        return(i_maxAdjustAmount);
    }
    
    /** 
     * Get daily adjustment limit. 
     * @return daily adjustment limit.
     */
    public Double getDailyAdjustLimit()
    {
        return(i_dailyAdjustLimit);
    }
    
    /** 
     * Get privilege flag for Customer Care. 
     * @return True if operator has Customer Care privilege, false otherwise.
     */
    public boolean hasGuiPrivilege()
    {
        return(i_guiUser);
    }

    /** 
     * Get privilege flag for POSI. 
     * @return True if operator has POSI privilege, false otherwise.
     */
    public boolean hasPosiPrivilege()
    {
        return(i_posiUser);
    }

    /** 
     * Get privilege flag for PAMI. 
     * @return True if operator has PAMI privilege, false otherwise.
     */
    public boolean hasPamiPrivilege()
    {
        return(i_pamiUser);
    }
}