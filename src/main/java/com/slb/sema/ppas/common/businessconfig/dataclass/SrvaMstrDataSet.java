////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaMstrDataSet.Java
//      DATE            :       12-Feb-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of market data (from srva_mstr).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 10/10/01 | N Fletcher | Add method to allow selection   | PpaLon#1613/6834
//          |            | of SrvaMstrData based on market.|
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an market data, each element of
 * the set representing a record from the srva_mstr table.
 */
public class SrvaMstrDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaMstrDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of market data records. That is, the data of this DataSet object. */
    private SrvaMstrData []     i_marketsArr;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of markets data.
     * 
     * @param p_request The request to process.
     * @param p_marketsArr Array holding the market data records to be stored
     *                     by this object.
     */
    public SrvaMstrDataSet(
        PpasRequest             p_request,
        SrvaMstrData []         p_marketsArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 14510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_marketsArr = p_marketsArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 14590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns the available market records. That is, those not marked as
     * deleted.
     * @return Array containing all available markets records.
     */
    public SrvaMstrData [] getAvailableArray()
    {
        SrvaMstrData l_availableMarketsArr[];
        Vector       l_availableV = new Vector();
        int          l_loop;
        SrvaMstrData l_marketsEmptyArr[] = new SrvaMstrData[0];

        for (l_loop = 0; l_loop < i_marketsArr.length; l_loop++)
        {
            if (!i_marketsArr[l_loop].isDeleted())
            {
                l_availableV.addElement(i_marketsArr[l_loop]);
            }
        }

        l_availableMarketsArr =
              (SrvaMstrData[])l_availableV.toArray(l_marketsEmptyArr);

        return(l_availableMarketsArr);

    } // End of public method getAvailableArray()
    
    /**
     * Returns the available markets. That is, those not marked as
     * deleted.
     * @return Array containing all available markets.
     */
    public Market[] getAvailableMarkets()
    {
        Market[]     l_availableMarketsArr;
        ArrayList    l_tempList = null;
        int          l_loop;
        Market l_marketsEmptyArr[] = new Market[0];
        
        l_tempList = new ArrayList();

        for (l_loop = 0; l_loop < i_marketsArr.length; l_loop++)
        {
            if (!i_marketsArr[l_loop].isDeleted())
            {
                l_tempList.add(i_marketsArr[l_loop].getMarket());
            }
        }

        l_availableMarketsArr =
              (Market[])l_tempList.toArray(l_marketsEmptyArr);

        return(l_availableMarketsArr);

    } // End of public method getAvailableMarkets()


    // PpaLon#1613/6834 [BEGIN]

    /**
     * Returns SrvaMstrData record for the given market.
     * @param p_market Market that selected record must have.
     * @return SrvaMstrData record from this DataSet with the given market. If
     *         no active record can be found with the given market, then 
     *         <code>null</code> is returned.
     */
    public SrvaMstrData getRecord (Market   p_market)
    {
        SrvaMstrData l_srvaMstrRecord = null;
        int          l_loop;

        for (l_loop = 0; l_loop < i_marketsArr.length; l_loop++)
        {
            if ( !i_marketsArr[l_loop].isDeleted() &&
                  i_marketsArr[l_loop].getMarket().equals(p_market) )
            {
                l_srvaMstrRecord = i_marketsArr[l_loop];
            }
        }

        return (l_srvaMstrRecord);

    } // End of public method getRecord

    // PpaLon#1613/6834 [END]

    /**
     * Returns the market representing the service area/location.
     * @param p_srva  Service Area.
     * @param p_sloc  Service Location.
     * @return Market record.
     */
    public SrvaMstrData getRecord(int p_srva, int p_sloc)
    {
        return getRecord(new Market(p_srva, p_sloc, null));
    }
    
    /**
     * Returns SrvaMstrData record for the given market, even when it's been withdrawn.
     * @param p_market Market that selected record must have.
     * @return SrvaMstrData record from this DataSet with the given market. If
     *         no active record can be found with the given market, then 
     *         <code>null</code> is returned.
     */
    public SrvaMstrData getRecordIncludeWithdrawn(Market   p_market)
    {
        SrvaMstrData l_srvaMstrRecord = null;
        int          l_loop;

        for (l_loop = 0; l_loop < i_marketsArr.length; l_loop++)
        {
            if (i_marketsArr[l_loop].getMarket().equals(p_market) )
            {
                l_srvaMstrRecord = i_marketsArr[l_loop];
            }
        }

        return (l_srvaMstrRecord);

    } // End of public method getRecordIncludeWithdrawn
    
    /** Returns array of all records in this data set object.
     * 
     * @return Array of all records. */
    public SrvaMstrData [] getAllArray()
    {
        return (i_marketsArr);

    } // end method getAllArray

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_marketsArr, p_sb);
    }
}

