////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BasicRdrFileDataSet
//      DATE            :       10-06-2003
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_74
//
//      COPYRIGHT        :      WM-data 2006
//
//      DESCRIPTION     :       The class encapsulates the data in a set of rows
//                              of the RFCI_RDR_FILE_CTL_INFO dabase table.
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

/** Container for a set of RDR File Control records. */
public class BasicRdrFileDataSet extends DataSetObject
{
    /** Set of RDR File Control records. */
    Vector i_fileConDataSetV;
    
    /**
     * Constructs a new BasicRdrFileDataSet object.
     * 
     * @param p_vector A vector of <code>BasicRdrFileData</code> object
     */

    public BasicRdrFileDataSet(Vector p_vector)
    {
        i_fileConDataSetV = p_vector;
    }
   
    /** Constructs a new BasicRdrFileDataSet object with no parameters. */
    public BasicRdrFileDataSet()
    {
        i_fileConDataSetV = new Vector();
    }

    /** Add an instance of File Control data.
     * 
     * @param p_data File control data to be added.
     */
    public void addFileData(BasicRdrFileData p_data)
    {
        i_fileConDataSetV.add(p_data);
    }
    
    /**
     * Gets a vector of <code>FileControlSequenceData</code> object
     * 
     * @return A vector of <code>FileControlSequenceData</code>
     *          object or an empty vector
     */

    public Vector getFileControlDataSetV ()
    {
        return i_fileConDataSetV;
    }

    /** Get the data as an array.
     * 
     * @return Array of RDR File objects.
     */
    public BasicRdrFileData[] asArray()
    {
        return (BasicRdrFileData[])i_fileConDataSetV.toArray(new BasicRdrFileData[i_fileConDataSetV.size()]);
    }
    
    /** Description of this object.
     * 
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("Files", i_fileConDataSetV, p_sb);
    }
    
    /** Get the number of records obtained.
     * 
     * @return Number of records read.
     */
    public int size()
    {
        return i_fileConDataSetV.size();
    }
}

