////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BankBankDataSet.Java
//      DATE            :       11-July-2001
//      AUTHOR          :       Sally Wells
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       A Set (collection) of bank data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | Copyright info changed.         | PpacLon#1755/7279
//----------+------------+---------------------------------+--------------------
// 09/11/05 | Lars L.    | New method and corresponding    | PpacLon#1755/7279
//          |            | instance variable added in order|
//          |            | to return only the available    |
//          |            | bank records, i.e. only those   |
//          |            | not marked as deleted.          |
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A Set (collection) of bank data.
 */
public class BankBankDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BankBankDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** The complete set of <code>BankBankData</code> objects. */
    private Vector i_bankBankDataSetV          = null;

    /** The available set of <code>BankBankData</code> objects (that is, those not marked as deleted). */
    private Vector i_availableBankBankDataSetV = null;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /** 
     * Creates a new set of Bank data containing an empty Vector. 
     * 
     * @param p_request The request to process.
     */
    public BankBankDataSet( PpasRequest p_request )
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10020, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
      //         BankBankDataSet(PpasRequest)

    /** 
     * Creates a new set of Bank data containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of banks for which space is allocated.
     * @param p_vGrowSize Number of banks by which the data set should be grown.
     */
    public BankBankDataSet(
        PpasRequest p_request,
        int         p_initialVSize,
        int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 11010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_bankBankDataSetV = new Vector( p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 11020, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
      //         BankBankDataSet(PpasRequest)

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** Adds element to BankBank Set.
     *  @param  p_bankBankData Bank data to be added to Vector.
     */
    public void addBank( BankBankData p_bankBankData)
    {
        i_bankBankDataSetV.addElement( p_bankBankData);
    }

    /** Returns a BankBankData Object for the Bank code passed in.
     * 
     * @param p_bankCode Identifier of a bank.
     *  @return Details of the bank associated with the specified bank code.
     */
    public BankBankData getBankBankData(String p_bankCode)
    {
        BankBankData l_bankBankData = null;
        boolean      l_found        = false;

        if (i_bankBankDataSetV != null)
        {
            for (int l_i = 0; l_i < i_bankBankDataSetV.size(); l_i++)
            {
                l_bankBankData = (BankBankData)i_bankBankDataSetV.elementAt(l_i);
                if (l_bankBankData.getBankCode().equals(p_bankCode))
                {
                    l_found = true;
                    break;
                }
            }
        }

        if (!l_found)
        {
            l_bankBankData = null;
        }

        return(l_bankBankData);

    } // end of getBankBankData(...)


    /** Returns the BankBankData Set as a Vector.
     *  @return Vector of BankBankData.
     */
    public Vector getBankBankSetAsVector()
    {
        return( i_bankBankDataSetV );
    }

    /**
     * Returns a data set containing the all available bank records, that is,  those not marked as deleted.
     * @return a data set containing available bank records in the cache (i.e. those not marked as deleted).
     */
    public Vector getAvailableBankBankSet()
    {
        if (i_availableBankBankDataSetV == null)
        {
            i_availableBankBankDataSetV = new Vector();
            for (int l_ix = 0; l_ix < i_bankBankDataSetV.size(); l_ix++)
            {
                if (!((BankBankData)i_bankBankDataSetV.elementAt(l_ix)).isWithdrawn())
                {
                    i_availableBankBankDataSetV.addElement(i_bankBankDataSetV.elementAt(l_ix));
                }
            }
        }

        return i_availableBankBankDataSetV;
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_bankBankDataSetV, p_sb);
    }
}

