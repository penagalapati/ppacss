////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      DedaDedicatedAccountsSqlService.Java
//      DATE            :      22-Aug-2002
//      AUTHOR          :      Simone Nelson
//      REFERENCE       :      PpaLon1484/6164
//                             PpaLon#1522/6421
//                             PRD_PPAK00_GEN_CA_372
//      COPYRIGHT       :      WM-data 2005
//
//      DESCRIPTION     :      A service to manage persistance of
//                             deda data to the database.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
//----------+------------+---------------------------------+--------------------
// 04/12/02 | N Raymond  | Mod. debug level for consistency| PpaLon#1707/7314
//----------+------------+---------------------------------+--------------------
// 06/03/06 | M Erskine  | Ded Acc Id can be 10 digits long| PpacLon#2005/8047
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that implements a service to manage persistance of deda data
 * to the database.
 */
public class DedaDedicatedAccountsSqlService
{
    //------------------------------------------------------------------------
    // Class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DedaDedicatedAccountsSqlService";

    //------------------------------------------------------------------------
    // Instance attributes
    //------------------------------------------------------------------------

    /** The Logger to be used to log exceptions generated within this class. */
    protected Logger           i_logger = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new deda sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public DedaDedicatedAccountsSqlService(PpasRequest       p_request,
                                           Logger            p_logger)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,    // PpaLon#1707/7314
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructing DedaDedicatedAccountsSqlService(...)");
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,    // PpaLon#1707/7314
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10020, this,
                "Constructed DedaDedicatedAccountsSqlService(...)");
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_read = "read";
    /**
     * @return DedaDedicatedAccountsDataSet
     * Read the deda set from the database where the rows are not marked
     * as deleted.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @throws PpasSqlException If the data cannot be read.
     */
    public DedaDedicatedAccountsDataSet read(PpasRequest       p_request,
                                             JdbcConnection    p_connection)
        throws PpasSqlException
    {
        JdbcStatement                 l_statement   = null;
        SqlString                     l_sql         = null;
        JdbcResultSet                 l_result      = null;
        int                           l_index       = 0;
        DedaDedicatedAccountsData     l_dedaData    = null;
        DedaDedicatedAccountsDataSet  l_dedaDataSet = null;
        Vector                        l_allDedaV    = new Vector (20, 10);
        DedaDedicatedAccountsData     l_allDedaARR[];
        DedaDedicatedAccountsData     l_dedaEmptyARR[] = new DedaDedicatedAccountsData[0];
        int                           l_srva;
        int                           l_sloc;
        Market                        l_market;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 10030, this,
                "Entered " + C_METHOD_read);
        }

        l_sql =   new SqlString(500, 0, 
                  "SELECT deda_srva, " +
                         "deda_sloc, "             +
                         "deda_service_class, "    +
                         "deda_id, "               +
                         "deda_desc "              +
                  "FROM   deda_dedicated_accounts");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_SQL,
                p_request, C_CLASS_NAME, 10040, this,
                "About to execute SQL " + l_sql);
        }

        l_statement = p_connection.createStatement( C_CLASS_NAME,
                                                    C_METHOD_read,
                                                    10050,
                                                    this,
                                                    p_request);

        l_result = l_statement.executeQuery( C_CLASS_NAME,
                                             C_METHOD_read,
                                             10060,
                                             this,
                                             p_request,
                                             l_sql );

        while ( l_result.next(99990) )
        {
            l_srva   = l_result.getInt(99980, "deda_srva");
            l_sloc   = l_result.getInt(99970, "deda_sloc");
            l_market = new Market(l_srva, l_sloc);

            l_dedaData = new DedaDedicatedAccountsData (
                            p_request,
                            l_market,
                            l_result.getServiceClass(99960, "deda_service_class"),
                            l_result.getInt(99950,          "deda_id"),
                            l_result.getTrimString(99940,   "deda_desc"));

            l_allDedaV.addElement (l_dedaData);
            l_index++;
        }

        l_result.close(99930);

        l_statement.close(C_CLASS_NAME, C_METHOD_read, 10060, this, p_request);

        l_allDedaARR = (DedaDedicatedAccountsData [])l_allDedaV.toArray (l_dedaEmptyARR);

        l_dedaDataSet = new DedaDedicatedAccountsDataSet (p_request, l_allDedaARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, C_CLASS_NAME, 98010, this,
                "Leaving read");
        }

        return(l_dedaDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts an Dedicated Account record into the deda_dedicated_accounts database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_market                Market identifier.
     * @param p_serviceClass          Service Class identifier.
     * @param p_dedicatedAccountId    Dedicated Account identifier.
     * @param p_dedicatedAccountDesc  Dedicated Account description.
     * @param p_userOpid              Operator updating the business config record.
     * @throws PpasSqlException       Any exception derived from this Class.
     */
    public void insert(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       long           p_dedicatedAccountId,
                       String         p_dedicatedAccountDesc,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10000, 
                this,
                "Entered " + C_METHOD_insert);
        }
         
        l_sql = "INSERT INTO deda_dedicated_accounts (" +
                    "deda_srva, " +
                    "deda_sloc, " +
                    "deda_service_class, " +
                    "deda_id, " +
                    "deda_desc, " +
                    "deda_opid, " +
                    "deda_gen_ymdhms ) " +
                "VALUES(" + 
                    "{0},{1},{2},{3},{4}, {5}, {6})";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setLongParam    (3, p_dedicatedAccountId);
        l_sqlString.setStringParam  (4, p_dedicatedAccountDesc);
        l_sqlString.setStringParam  (5, p_userOpid);
        l_sqlString.setDateTimeParam(6, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10010,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10020,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close(C_CLASS_NAME, C_METHOD_insert, 10030, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10040, 
                    this,
                    "Database error: unable to insert row in deda_dedicated_accounts table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10050,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10060, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    }
   
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Updates an Dedicated Account record in the deda_dedicated_accounts database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_market                Market identifier.
     * @param p_serviceClass          Service Class identifier.
     * @param p_dedicatedAccountId    Dedicated Account identifier.
     * @param p_dedicatedAccountDesc  Dedicated Account description.
     * @param p_userOpid              Operator updating the business config record.
     * @throws PpasSqlException       Any exception derived from this Class.
     */
    public void update(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       long           p_dedicatedAccountId,
                       String         p_dedicatedAccountDesc,
                       String         p_userOpid)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10100, 
                this,
                "Entered " + C_METHOD_update);
        }

        l_sql = "UPDATE deda_dedicated_accounts " +
                    "SET deda_desc       = {4}, " +
                        "deda_opid       = {5}, " +
                        "deda_gen_ymdhms = {6}  " +
                    "WHERE deda_id            = {3} " +
                    "AND   deda_srva          = {0} " +
                    "AND   deda_sloc          = {1} " +
                    "AND   deda_service_class = {2} ";

        l_sqlString = new SqlString(500, 0, l_sql);
        
        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setLongParam    (3, p_dedicatedAccountId);
        l_sqlString.setStringParam  (4, p_dedicatedAccountDesc);
        l_sqlString.setStringParam  (5, p_userOpid);
        l_sqlString.setDateTimeParam(6, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       10110,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_update,
                                       10120,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_update, 10130, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10140, 
                    this,
                    "Database error: unable to update row in deda_dedicated_accounts table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_update,
                                       10150,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10160, 
                this,
                "Leaving " + C_METHOD_update);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Marks a record as deleted in the deda_dedicated_accounts database table.
     * 
     * @param p_request               The request to process.
     * @param p_connection            Database Connection.
     * @param p_market                Market identifier.
     * @param p_serviceClass          Service Class identifier.
     * @param p_dedicatedAccountId    Dedicated Account identifier.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       Market         p_market,
                       ServiceClass   p_serviceClass,
                       long           p_dedicatedAccountId)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasDateTime     l_now;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 
                10200, 
                this,
                "Entered " + C_METHOD_delete);
        }
 
        l_sql = "DELETE from deda_dedicated_accounts " +
                    "WHERE deda_srva          = {0} " +
                    "AND   deda_sloc          = {1} " +
                    "AND   deda_service_class = {2} " +
                    "AND   deda_id            = {3} ";

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_market.getSrva());
        l_sqlString.setStringParam  (1, p_market.getSloc());
        l_sqlString.setIntParam     (2, p_serviceClass.getValue());
        l_sqlString.setLongParam    (3, p_dedicatedAccountId);
        l_sqlString.setDateTimeParam(4, l_now);

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10210,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10220,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 10230, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    10240, 
                    this,
                    "Database error: unable to delete row from deda_dedicated_accounts table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_delete,
                                       10250,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                10260, 
                this,
                "Leaving " + C_METHOD_delete);
        }
    }
    
} // End of public class DedaDedicatedAccountsSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
