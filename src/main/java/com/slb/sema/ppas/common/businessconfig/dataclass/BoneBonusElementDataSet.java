//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BoneBonusElementDataSet.java
// DATE            :       14-Dec-2006
// AUTHOR          :       Kanta Goswami
// REFERENCE       :       PRD_ASCS_GEN_CA_104
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A record of bonus element data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 05/07/07 | SJ Vonka   | Allow for wildcard selection    | PpacLon#3184/11796
//          |            |                                 | PRD_ASCS00_GEN_CA_120
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Object containing a set of <code>BoneBonusElementData</code> objects. */
public class BoneBonusElementDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BoneBonusElementDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of bonus element records. That is, the data of this DataSet object. */
    private BoneBonusElementData[] i_allRecords;
    
    /** Array of available bonus element data records. That is, the data of this DataSet object. */    
    private BoneBonusElementData[] i_availableRecords;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Bonus Element data.
     * @param p_request The request to process.
     * @param p_data Array holding the  bonus element data records to be stored by this object.
     */
    public BoneBonusElementDataSet(PpasRequest             p_request,
                                   BoneBonusElementData[]  p_data)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_allRecords = p_data;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns Bonus Element data associated with a specified case.
     * 
     * @param p_schemeId The bonus element.
     * @param p_keyElements Key bonus elements concatenated into a String.
     * @param p_nonKeyElements Non-key bonus elements concatenated into a String.
     * @return BoneBonusElementData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BoneBonusElementData getRecord(String p_schemeId,
                                          String p_keyElements,
                                          String p_nonKeyElements)
    {
        BoneBonusElementData l_record = null;
        
        for (int i = 0; i < i_allRecords.length; i++)
        {
            // The logic below depends on BONE_NONKEY_ELEMENTS containing a numeric value and no commas
            // which is the case in the initial version of TUBS, but will probably not be in the full version.
            if ( i_allRecords[i].getBonusSchemeId().equals(p_schemeId) &&
                 i_allRecords[i].getKeyElements().equals(p_keyElements) &&
                 Double.valueOf(i_allRecords[i].getNonkeyElements()).equals(Double.valueOf(p_nonKeyElements)))
            {
                l_record = i_allRecords[i];
                break;
            }
        }

        return l_record;
    }    
    
    /**
     * Returns available (that is, not marked as deleted) Bonus Element records associated 
     * with a specified case.
     * 
     * @param p_schemeId The bonus element.
     * @param p_keyElements Key bonus elements concatenated into a String.
     * @param p_nonKeyElements Non-key bonus elements concatenated into a String.
     * @return BoneBonusElementData record from this DataSet with the given case. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BoneBonusElementData getAvailableRecord (String p_schemeId,
                                                    String p_keyElements,
                                                    String p_nonKeyElements)
    {
        BoneBonusElementData l_boneBonusElementRecord =
                                 getRecord(p_schemeId, p_keyElements, p_nonKeyElements);
        
        if (l_boneBonusElementRecord != null && l_boneBonusElementRecord.isDeleted())
        {
            l_boneBonusElementRecord = null;
        }

        return l_boneBonusElementRecord;
    }
    
    /** Returns array of all records in this data set object.
     * 
     * @return Array of all data records.
     */
    public BoneBonusElementData[] getAllArray()
    {
        return i_allRecords;
    }    
    
    /**
     * Get a data set containing the all available  records. That is, those not marked as deleted.
     * 
     * @return Data set containing available records.
     */
    public BoneBonusElementData[] getAvailableArray()
    {
        if (i_availableRecords == null)
        {
            Vector l_available = new Vector();
            
            for (int i = 0; i < i_allRecords.length; i++)
            {
                if (!i_allRecords[i].isDeleted())
                {
                    l_available.addElement(i_allRecords[i]);
                }
            }
    
            i_availableRecords =
                  (BoneBonusElementData[])l_available.toArray(new BoneBonusElementData[l_available.size()]);
        }

        return i_availableRecords;
    }
    
    /** Get a data set of available BONE data based on key elements.
     * 
     * @param p_key Key elements.
     * @return BONE data set.
     */
    public BoneBonusElementDataSet getAvailableForKey(String p_key)
    {
        Vector                  l_result    = new Vector();
        String[]                l_splitKey  = new String[3];
        BoneBonusElementData[]  l_available = getAvailableArray();
        
        // split up p_key on commas so have three elements
        l_splitKey =  p_key.split(",", 3); 
        
        for (int i = 0; i < l_available.length; i++)
        {
            if (l_available[i].getKeyElements().matches("^(?:\\Q"+l_splitKey[0]+
                                                        "\\E|\\*),(?:\\Q"+l_splitKey[1]+
                                                        "\\E|\\*),(?:\\Q"+l_splitKey[2]+"\\E|\\*)$"))
            {
                l_result.add(l_available[i]);
            }
        }
        
        return new BoneBonusElementDataSet(null,
                                           (BoneBonusElementData[])l_result.toArray(new BoneBonusElementData[l_result.size()]));
    }
    
    /**
     * Get a data set containing available records for a given bonus scheme.
     * @param p_bonusSchemeId The bonus scheme id.
     * @return Data set containing available records for a given bonus scheme.
     */
    public BoneBonusElementData[] getAvailableArrayWithId(String p_bonusSchemeId)
    {
        Vector l_availableWithId = new Vector();
        BoneBonusElementData[] l_availableWithIdArray = null;
        getAvailableArray();
        
        for (int i = 0; i < i_availableRecords.length; i++)
        {
            if (i_availableRecords[i].getBonusSchemeId().equals(p_bonusSchemeId))
            {
                l_availableWithId.addElement(i_availableRecords[i]);
            }
        }
        
        l_availableWithIdArray = 
            (BoneBonusElementData[])l_availableWithId.toArray(
                                        new BoneBonusElementData[l_availableWithId.size()]);
        
        return l_availableWithIdArray;
    }
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_allRecords, p_sb);
    }
}
