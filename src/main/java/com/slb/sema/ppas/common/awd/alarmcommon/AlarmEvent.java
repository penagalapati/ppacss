/*
 * Created on 30-Oct-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.common.awd.alarmcommon;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AlarmEvent
{
    public String getEventName();
    public String getEventText();

    /** Time (normal Java UTC millis time) alarm was raised. */ 
    public long                 getRaisedTime();

    public String getOriginatingNodeName();
    public String getOriginatingProcessName();

}
