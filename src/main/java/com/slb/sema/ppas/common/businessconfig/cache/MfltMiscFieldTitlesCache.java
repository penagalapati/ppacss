////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MfltMiscFieldTitlesCache.Java
//      DATE            :       30-January-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1218/4955
//
//      COPYRIGHT       :       SCLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A cache of MfltMiscFieldTitlesData set
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.MfltMiscFieldTitlesDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.MfltMiscFieldTitlesSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache of MfltMiscFieldTitlesData set, which contains the miscellaneous
 * field titles for each market, from mflt_misc_field_titles table.
 */
public class MfltMiscFieldTitlesCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MfltMiscFieldTitlesCache";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /**
     * Miscellaneous field titles Details data set instance.
     */
    private MfltMiscFieldTitlesDataSet i_miscFieldTitlesDataSet = null;

    /**
     * Sql service used to load the Cache.
     */
    private MfltMiscFieldTitlesSqlService i_miscFieldTitlesSqlService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new miscellaneous field titles business config cache object.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public MfltMiscFieldTitlesCache(PpasRequest    p_request,
                                    Logger         p_logger,
                                    PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_miscFieldTitlesDataSet = new MfltMiscFieldTitlesDataSet( p_request );

        i_miscFieldTitlesSqlService = new MfltMiscFieldTitlesSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed " + C_CLASS_NAME );
        }
    } // End of public constructor
      //         MfltMiscFieldTitlesCache(PpasRequest, long, Logger)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns set of miscellaneous field titles.
     * @return MfltMiscFieldTitlesDataSet
     */
    public MfltMiscFieldTitlesDataSet getMfltMiscFieldTitlesDataSet()
    {
        return(i_miscFieldTitlesDataSet);

    } // End of public method getMfltMiscFieldTitlesDataSet

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the static configuration into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(
        PpasRequest     p_request,
        JdbcConnection  p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 10140, this,
                "Entered " + C_METHOD_reload );
        }

        i_miscFieldTitlesDataSet = i_miscFieldTitlesSqlService.readMiscFieldTitles( p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 10150, this,
                "Leaving " + C_METHOD_reload );
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_miscFieldTitlesDataSet);
    }
}

