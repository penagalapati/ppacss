////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaFieldsData.Java
//      DATE            :       13-Apr-2004
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       A record of a Comment Description
//                              (corresponding to a single row of the
//                              srva_fields table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of Comment Description (i.e.
 * a single row of the srva_fields table).
 */
public class SrvaFieldsData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaFieldsData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The market. */
    private  Market            i_market = null;

    /** Identifier for this comment. */
    private  String            i_fieldId = null;
    
    /** Description for this comment. */
    private  String            i_codeDescription = null;
    
    /** Date this Comment Description was created/last accessed in the database. */
    private  PpasDateTime      i_genYmdhms = null;

    /** Operator Id who created/last accessed this market record in the database. */
    private  String            i_opid = null;
    
    /** Flag indicating if this market is deleted. */
    private  boolean           i_deleted = false;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a Comment Description data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this data is associated.
     * @param p_fieldId Key of the comment.
     * @param p_codeDescription Desccription of this comment.
     * @param p_genYmdhms The date/time this data was last changed.
     * @param p_opid    The operator that last changed this data.
     * @param p_deleted Flag indicating whether this data is marked as deleted.
     */
    public SrvaFieldsData(
        PpasRequest            p_request,
        Market                 p_market,
        String                 p_fieldId,
        String                 p_codeDescription,
        PpasDateTime           p_genYmdhms,
        String                 p_opid,
        boolean                p_deleted)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market          = p_market;
        i_fieldId         = p_fieldId;
        i_codeDescription = p_codeDescription;
        i_genYmdhms       = p_genYmdhms;
        i_opid            = p_opid;
        i_deleted         = p_deleted;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the market.
     * 
     * @return The market to which this data is associated.
     */
    public Market getMarket ()
    {
        return (i_market);
    }

    /** Get the key of this comment.
     * 
     * @return The Comment key.
     */
    public String getKey()
    {
        return i_fieldId;
    }

    /** Get the Description of this Comment.
     * 
     * @return The Comment Description.
     */
    public String getDescription()
    {
        return i_codeDescription;
    }

    /** Get date/time this Comment was created/last accessed in the database.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }

    /** Get operator Id who created/lst accessed this market record in the database.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid()
    {
        return i_opid;
    }

    /** Returns true if this Comment has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_deleted;
    }
}

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////