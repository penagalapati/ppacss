////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BacoBatchControlDataSet.Java
//      DATE            :       15-July-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpaLon#112/3215
//                              PRD_PPAK00_ANA_FD_13
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Contains the rows selected from the 
//                              BACO_BATCH_CONTROL table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 29-Sep-03 | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains the misc codes selected from the SRVA_MISC_CODES table. */
public class BacoBatchControlDataSet
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BacoBatchControlDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** A Set of BacoBatchControlData objects. */
    private Vector   i_batchControlDataSetV;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     * */
    public BacoBatchControlDataSet(PpasRequest p_request,
                                   int         p_initialVSize,
                                   int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_batchControlDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /** Create a new BacoBatchControlDataSet that is a subset of the data in this 
     *  object where the records selected are based on a given job type and status.
     * 
     * @param p_request The request to process.
     * @param p_jobType Symbolic name of the Batch.
     * @param p_jobStatus The status of the jobs for retrieval:
     *                    'I' - in progress
     *                    'C' - completed
     *                    'X' - stopped
     *                    'F' - failed
     * @return Data set containing a subset of the data in this class, based on
     *         the given job type and status
     */
    public BacoBatchControlDataSet getJobInfo(PpasRequest p_request,
                                              String      p_jobType,
                                              char        p_jobStatus)
    {
        int                     l_index;
        int                     l_thisDataSetSize;
        BacoBatchControlDataSet l_returnData;
        BacoBatchControlData    l_currRecord;
        
        l_thisDataSetSize = i_batchControlDataSetV.size();
        l_returnData = new BacoBatchControlDataSet (p_request, 
                                                    l_thisDataSetSize,
                                                    10);

        for (l_index = 0; l_index < l_thisDataSetSize; l_index++)
        {

            l_currRecord = (BacoBatchControlData)(i_batchControlDataSetV.elementAt(l_index));
            if (l_currRecord.getJobType().equals(p_jobType) &&
                l_currRecord.getStatus() == p_jobStatus)
            {
                l_returnData.addBatchControlDataObject(l_currRecord);
            }
        }
        return (l_returnData);

    } // end of method getJobInfo
    
    /** Create a new BacoBatchControlDataSet that is a subset of the data in this 
     *  object where the records selected are based on a given job type.
     * 
     * @param p_request The request to process.
     * @param p_jobType Symbolic name of the Batch.
     * @return Data set containing a subset of the data in this class, based on
     *         the given job type and status
     */
    public BacoBatchControlDataSet getJobInfo(PpasRequest p_request,
                                              String      p_jobType)
    {
        int                     l_index;
        int                     l_thisDataSetSize;
        BacoBatchControlDataSet l_returnData;
        BacoBatchControlData    l_currRecord;
        
        l_thisDataSetSize = i_batchControlDataSetV.size();
        l_returnData = new BacoBatchControlDataSet (p_request, 
                                                    l_thisDataSetSize,
                                                    10);

        for (l_index = 0; l_index < l_thisDataSetSize; l_index++)
        {

            l_currRecord = (BacoBatchControlData)(i_batchControlDataSetV.elementAt(l_index));
            if (l_currRecord.getJobType().equals(p_jobType))
            {
                l_returnData.addBatchControlDataObject(l_currRecord);
            }
        }
        return (l_returnData);

    } // end of method getJobInfo
    
    /** Return the data within this data set.
     *  @return Vector containing all data within this dataset. 
     */
    public Vector getDataV ()
    {
        return (i_batchControlDataSetV);
    }
    
    /** 
     * Adds an element to the vector.
     *
     * @param  p_batchControlData The batch control data object to be added.
     */
    public void addBatchControlDataObject(BacoBatchControlData p_batchControlData)
    {
        i_batchControlDataSetV.addElement(p_batchControlData);
    }
}