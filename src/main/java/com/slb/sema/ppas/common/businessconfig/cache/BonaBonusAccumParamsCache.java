//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonaBonusAccumParamsCache.java
// DATE            :       29-Jun-2007
// AUTHOR          :       Michael Erskine
// REFERENCE       :       PRD_ASCS_GEN_CA_128
//
// COPYRIGHT       :       WM-data 2006
//
// DESCRIPTION     :       A cache for bonus accumulation parameters data.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.BonsBonusSchemeDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BonaBonusAccumParamsSqlService;
import com.slb.sema.ppas.common.businessconfig.sqlservice.BonsBonusSchemeSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A class implementing a cache for Bonus Scheme.
 */
public class BonaBonusAccumParamsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonaBonusAccumParamsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The bonus scheme accumulation params SQL service to use to load cache.
     */
    private BonaBonusAccumParamsSqlService i_bonaSqlService = null;

    /** Cache of all valid Bonus Accumulations (including ones marked as deleted). */
    private BonaBonusAccumParamsDataSet    i_allBonaDataSet        = null;

    /**
     * Cache of available Bonus Schemes(not including ones marked as deleted). This attribute exists
     * for performance reasons so the array is not generated from the complete list each time it is required.
     */
    private BonaBonusAccumParamsDataSet    i_availableBonaDataSet  = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid Bonus Schemescache.
     * @param p_request The request to process.
     * @param p_logger The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public BonaBonusAccumParamsCache(PpasRequest p_request, Logger p_logger, PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_bonaSqlService = new BonaBonusAccumParamsSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Bonus Schemes(including ones marked as deleted).
     * @return All valid Bonus Schemes.
     */
    public BonaBonusAccumParamsDataSet getAll()
    {
        return i_allBonaDataSet;
    }

    /**
     * Returns available valid Bonus Schemes(not including ones marked as deleted).
     * @return Available Bonus Schemes.
     */
    public BonaBonusAccumParamsDataSet getAvailable()
    {
        if (i_availableBonaDataSet == null)
        {
            i_availableBonaDataSet = new BonaBonusAccumParamsDataSet(null, i_allBonaDataSet.getAvailableArray());
        }

        return i_availableBonaDataSet;
    }

    /**
     * Reloads the static configuration into the cache.
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest p_request, JdbcConnection p_connection) throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Entered reload");
        }

        i_allBonaDataSet = i_bonaSqlService.readAll(p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded!
        i_availableBonaDataSet = new BonaBonusAccumParamsDataSet(p_request,
                                                                 i_allBonaDataSet.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allBonaDataSet);
    }
}

