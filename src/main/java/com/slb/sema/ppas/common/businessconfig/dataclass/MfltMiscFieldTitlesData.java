////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :      9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      MfltMiscFieldTitlesData.Java
//      DATE            :      29-January-2002
//      AUTHOR          :      Sally Wells
//      REFERENCE       :      PpaLon#1218/4955
//
//      COPYRIGHT       :      SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :      Class containing misc. field titles data as
//                             part of business config cache. This data relates
//                             to the database table mlft_misc_field_titles.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a row of the database table mflt_misc_field_titles.
 */
public class MfltMiscFieldTitlesData extends DataObject
{

    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MfltMiscFieldTitlesData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------

    /** Customer's market. */
    private Market i_market;

    /** Holds the screen title that will be used when entering miscellaneous 
     *  data against the misc titles.
     */
    private String i_title           = null;


    /** Vector that holds the miscellaneous titles that the operator will enter
     *  data against (40 fields in total).
     */
    private Vector i_fieldTitlesV;

    /** The operator identifier of the operator that either created the row
     *  or performed the last update. 
     */
    private String i_opid            = null;

    /** Date/time row was created/updated.*/
    private PpasDateTime i_genYmdhms = null;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates new miscellaneous field titles details and initialises data.
     *
     * @param p_request The request to process.
     */
    public MfltMiscFieldTitlesData( PpasRequest p_request)
    {
        this ( p_request,
               new Market(p_request),
               (String)null,
               null,
               (String)null, 
               null);
               
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed " + C_CLASS_NAME );
        }
    } // End of public constructor
      //         MfltMiscFieldTitlesData

    /**
     * Creates new miscellaneous field titles details and initialises data.
     *
     * @param p_request The request to process.
     * @param p_market The mnarket with which the miscellaneus fields are associated.
     * @param p_title Title of the miscellaneous field set.
     * @param p_fieldTitlesV Set of miscellaneous field titles.
     * @param p_opid Identifier of the operator that last changed this data.
     * @param p_genYmdhms Date/time this data was last changed.
     */
    public MfltMiscFieldTitlesData(
        PpasRequest  p_request,
        Market       p_market,
        String       p_title,
        Vector       p_fieldTitlesV,
        String       p_opid,
        PpasDateTime p_genYmdhms)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10020, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_market       = p_market;
        i_title        = p_title;
        i_fieldTitlesV = p_fieldTitlesV;
        i_opid         = p_opid;
        i_genYmdhms    = p_genYmdhms;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10060, this,
                 "Constructed " + C_CLASS_NAME );
        }
    } // End of public constructor
      //         MfltMiscFieldTitlesData

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Returns the market.
     * 
     * @return The market associated with this data.
     */
    public Market getMarket()
    {
        return(i_market);
    }

    /** Returns the Title.
     * 
     * @return The title of the miscellaneous field data.
     */
    public String getTitle()
    {
        return(i_title);
    }

    /** Returns the field titles vector.
     * 
     * @return The set of titles of miscellaneous fields.
     */
    public Vector getFieldTitlesVector()
    {
        return(i_fieldTitlesV);
    }

    /** Returns the operator identifier.
     * 
     * @return The identifier of the operator that last changed this data.
     */
    public String getOpid()
    {
        return(i_opid);
    }

    /** Returns the date/time of creation/updation.
     * 
     * @return The date/time this data was last changed.
     */
    public PpasDateTime getGenYmdhms()
    {
        return(i_genYmdhms);
    }

    /** Returns the number of titles that have been populated.
     * 
     * @return A count of the number of titles that have been populated.
     */
    public int getFieldTitleCount()
    {
        int l_count = 0;

        for (int l_i = 0; l_i < i_fieldTitlesV.size(); l_i++)
        {
            if ( !(i_fieldTitlesV.get(l_i).equals("")) )
            {
                l_count++;
            }
        }

        return (l_count);
    }

} // End of public class MfltMiscFieldTitlesData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////