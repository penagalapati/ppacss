////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       VospVoucherSplitsSqlService.Java
//      DATE            :       19-Aug-2002
//      AUTHOR          :       Nick Fletcher
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_382
//                              PpaLon#1500/6266
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the vosp voucher splits business
//                              config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi       | Remove declaration, retrieval,  | CR#15/440
//          | Isaacs     | and assignment of following     |
//          |            | variables in readAll method;    |
//          |            | l_srva, l_sloc, l_vospType;     |
//          |            | Retrieve vosp_division_id       |
//          |            | instead of vosp_card_group, and |
//          |            | set l_divisionld to the         |
//          |            | retrieved value                 |
//----------+------------+---------------------------------+--------------------
// 04/03/06 | M Erskine  | VOSP table split into two.      | PpacLon#2005/8047
//          |            | Initially this is part of       | PRD_ASCS00_GEN_CA_68
//          |            | supporting 10 dedicated accounts|
//          |            | but will allow for future       |
//          |            | expansion of this number.       |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.VosdVoucherSplitsDivisionsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.VospVoucherSplitsDataSet;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;


/**
 * Implements a service that performs the SQL required to populate the
 * vosp voucher splits business config cache.
 */
public class VospVoucherSplitsSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "VospVoucherSplitsSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new sql service to be used in generating the data for the
     * vosp voucher splits business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public VospVoucherSplitsSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 11310, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 11390, this,
                "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all vosp_voucher_splits table data.
     * 
     * @param p_request The request to process.
     * @param p_connection JDBC connection to be used to access the database.
     * @return A data set containing all the vosp_voucher_splits records read
     *         from the database.
     * @throws PpasSqlException If the data cannot be read.
     */
    public VospVoucherSplitsDataSet readAll(
        PpasRequest            p_request,
        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement                  l_statement;
        JdbcStatement                  l_statementVosd;
        SqlString                      l_sql;
        SqlString                      l_sqlStringVosd;
        String                         l_divisionId;
        PpasDate                       l_startD;
        PpasDate                       l_endD;
        String                         l_servClassString = null;
        ServiceClass                   l_servClass = null;
        int                            l_mastPct;
        int                            l_accId;
        int                            l_accPct;
        PpasDate                       l_accEndDt;
        int                            l_accPeriod;
        String                         l_extenType;
        PpasDateTime                   l_genYmdhms;
        String                         l_opid;
        JdbcResultSet                  l_results;
        JdbcResultSet                  l_vosdResults;
        VospVoucherSplitsData          l_vospRecord;
        VospVoucherSplitsDataSet       l_allVospDataSet;
        VospVoucherSplitsData          l_allVospArr[];
        VospVoucherSplitsData          l_vospEmptyArr[] = new VospVoucherSplitsData[0];
        Vector                         l_allVospV = new Vector (50, 20);
        VosdVoucherSplitsDivisionsData l_allVosdArr[] = null;
        VosdVoucherSplitsDivisionsData l_vosdEmptyArr[] = new VosdVoucherSplitsDivisionsData[0];
        Vector                         l_allVosdV = new Vector(10,10);
        String                         l_sqlVosd = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 11410, this,
                "Entered " +  C_METHOD_readAll);
        }
        
        l_sql = new SqlString(2000, 0, "SELECT " +
        		        "vosp_start_date, " +
                        "vosp_division_id, " +
                        "vosp_service_class, " +
                        "vosp_description, " +
                        "vosp_end_date, " +
                        "vosp_master_percent," +
                        "vosp_opid, " +
                        "vosp_gen_ymdhms " +
                "FROM vosp_voucher_splits ");   

        // Create the statement object and excecute the SQL query
        l_statement = p_connection.createStatement (C_CLASS_NAME,
                                                    C_METHOD_readAll,
                                                    11420,
                                                    this,
                                                    p_request);

        // Get all the rows in VOSP
        l_results = l_statement.executeQuery(C_CLASS_NAME,
                                             C_METHOD_readAll,
                                             11430,
                                             this,
                                             p_request,
                                             l_sql);

        // For each row in VOSP
        while (l_results.next(11440))
        {
            l_divisionId = l_results.getTrimString (11460, "vosp_division_id");
            l_startD    = l_results.getDate       (11480, "vosp_start_date");
            l_servClassString = l_results.getString(11485, "vosp_service_class");
            l_servClass = l_results.getServiceClass(11490, "vosp_service_class");
            l_endD      = l_results.getDate       (11500, "vosp_end_date");
            l_mastPct   = (int)(l_results.getFloat(11510, "vosp_master_percent"));
            l_opid      = l_results.getString(11520, "vosp_opid");
            l_genYmdhms = l_results.getDateTime(11530, "vosp_gen_ymdhms");
            
            l_sqlVosd = "SELECT " +
                             "vosd_start_date, " +
                             "vosd_division_id, " +
                             "vosd_service_class, " +
                             "vosd_ded_acc_id, " +
                             "vosd_ded_acc_percent, " +
                             "vosd_ded_acc_end_date, " +
                             "vosd_ded_acc_period, " +
                             "vosd_ded_acc_period_ext_type " +
                         "FROM vosd_voucher_split_divisions " +
                         "WHERE vosd_start_date = '" + l_startD + "' " +
                         "AND vosd_division_id = '" + l_divisionId + "' " + 
                         "AND vosd_service_class = '" + l_servClassString + "'";
            
            l_sqlStringVosd = new SqlString(2000, 0, l_sqlVosd);
            
            // Create the statement object and excecute the SQL query
            l_statementVosd = p_connection.createStatement (C_CLASS_NAME,
                                                        C_METHOD_readAll,
                                                        11450,
                                                        this,
                                                        p_request);

            // Get the corresponding rows in VOSD
            l_vosdResults = l_statementVosd.executeQuery(C_CLASS_NAME,
                                                  C_METHOD_readAll,
                                                  11460,
                                                  this,
                                                  p_request,
                                                  l_sqlStringVosd);

            l_allVosdV = null;
            l_allVosdArr = null;
            l_vosdEmptyArr = new VosdVoucherSplitsDivisionsData[0];
            l_allVosdV = new Vector(10,10);

            // Add these corresponding rows to the VOSP data
            while (l_vosdResults.next(11470))
            {
                l_accId     = l_vosdResults.getInt   (11520, "vosd_ded_acc_id");
                l_accPct    = l_vosdResults.getInt   (11530, "vosd_ded_acc_percent");
                l_accEndDt  = l_vosdResults.getDate  (11540, "vosd_ded_acc_end_date");
                l_accPeriod = l_vosdResults.getInt   (11550, "vosd_ded_acc_period");
                l_extenType  = l_vosdResults.getString  (11560, "vosd_ded_acc_period_ext_type");
                
                l_allVosdV.addElement (
                	new VosdVoucherSplitsDivisionsData(
                            null, l_accId, l_accPct, l_accEndDt, l_accPeriod, l_extenType));
                l_allVosdArr = (VosdVoucherSplitsDivisionsData [])l_allVosdV.toArray (l_vosdEmptyArr);
            }
            
            l_vosdResults.close (11740);
            l_statementVosd.close (C_CLASS_NAME, C_METHOD_readAll, 11750, this, p_request);

            l_vospRecord = new VospVoucherSplitsData(
                    p_request,
                    l_divisionId,
                    l_startD,
                    l_servClass,
                    l_endD,
                    l_mastPct,
                    l_allVosdArr,
                    l_opid,
                    l_genYmdhms
                    );

            l_allVospV.addElement (l_vospRecord);
        }

        l_results.close (11740);
        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 11750, this, p_request);

        l_allVospArr = (VospVoucherSplitsData [])
                         l_allVospV.toArray (l_vospEmptyArr);

        l_allVospDataSet = new VospVoucherSplitsDataSet(p_request, l_allVospArr);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 11790, this,
                "Leaving " + C_METHOD_readAll);
        }

        return (l_allVospDataSet);
    } // End of public method readAll
} // End of public class VospVoucherSplitsSqlService
