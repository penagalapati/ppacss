////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BasicAccountData.Java
//      DATE            :       23-Mar-2001
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Data defining basic key account details.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 10/09/06 | M Erskine  | Add customer status 'R' for     | PRD_ASCS00_GEN_CA_97
//          |            | reconnection in progress.       | PpacLon#2596/9829
//----------+------------+---------------------------------+--------------------
// 10/09/06 | Paul Rosser| Add customer status 'T' for     | PRD_ASCS00_GEN_CA_108
//          |            | accounts to be transitioned.    | PpacLon#2858/10794
//----------+------------+---------------------------------+-----------------
// 16/05/07 |Steve James | Incl status 'R' as disconnected | PpacLon#3072/11370 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Each instance of this class defines a record contains key information 
 *  associated with an account.
 */
public class BasicAccountData extends DataObject implements Serializable
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------
    
    // Constants for Customer Type

    /** Defines the type of a Master customer. */
    public static final String C_CUST_TYPE_MASTER = "1P";
    
    /** Defines the type of a Subordinate customer. */
    public static final String C_CUST_TYPE_SUBORDINATE = "S1";    

    // Constants for Customer Status

    /** Defines the status of a just installed customer. */
    public static final char C_SUBS_STATUS_INSTALLED = 'I';

    /** Defines the status of a currently active customer. */
    public static final char C_SUBS_STATUS_ACTIVE = 'A';

    /** Defines the status of a currently available customer (i.e. No  
     *  recharges have yet been performed. 
     */
    public static final char C_SUBS_STATUS_AVAILABLE = 'V';

    /** Defines the status of a customer who has been active but is now 
     *  permanently disconnected.
     */
    public static final char C_SUBS_STATUS_PERM_DISC = 'P';

    /** Defines the status of a subscriber that is being converted to a subordinate. */
    public static final char C_SUBS_STATUS_MAKE_SUBORDINATE_IN_PROG = 'S';
    
    /** Defines the status of a customer whose disconnection is in progress. */
    public static final char C_SUBS_STATUS_DISC_IN_PROG = 'D';
    
    /** Defines the status of a customer whose reconnection is in progress. */
    public static final char C_SUBS_STATUS_RECONNECTION_IN_PROG = 'R';
       
    /** Defines the status of a customer who is to be 'transitioned'. */
    public static final char C_SUBS_STATUS_TO_BE_TRANSITIONED = 'T';

    // Define string code values to represent account status and type

    /** String code representation for a customer status of available. 
      * (i.e. An Active account that has yet to perform a recharge
      */
    public static final String C_SUBS_STATUS_CODE_AVAILABLE = "SUBS_STATUS_AVAILABLE";

    /** String code representation for a customer status of active. */
    public static final String C_SUBS_STATUS_CODE_ACTIVE = "SUBS_STATUS_ACTIVE";

    /** String code representation for a customer status of disconnected. */
    public static final String C_SUBS_STATUS_CODE_DISCONNECTED = "SUBS_STATUS_DISCONNECTED";

    /** String code representation for an unrecognised, invalid customer status. */
    public static final String C_SUBS_STATUS_CODE_INVALID = "SUBS_STATUS_INVALID";

    // Define string values to represent account status and type

    /** String representation for a customer status of available. 
      * (i.e. An Active account that has yet to perform a recharge
      */
    public static final String C_SUBS_STATUS_STR_AVAILABLE = "Available";

    /** String representation for a customer status of active. */
    public static final String C_SUBS_STATUS_STR_ACTIVE = "Active";

    /** String representation for a customer status of disconnected. */
    public static final String C_SUBS_STATUS_STR_DISCONNECTED = "Disconnected";

    /** String representation for a customer status of 'I' (during installation). */
    public static final String C_SUBS_STATUS_STR_NOT_INSTALLED = "Not Installed";

    /** String representation for an unrecognised, invalid customer status. */
    public static final String C_SUBS_STATUS_STR_INVALID = "Invalid";

    /** String code defining the type of the account as an account holder,
     *  with or without any subordinates.
     */
    public static final String C_ACCOUNT_TYPE_CODE_ACCOUNT_HOLDER = "ACCOUNT_HOLDER";

    /** String code defining the type of the account as a master. That is, an
     *  account holder with at least one subordinate.
     */
    public static final String C_ACCOUNT_TYPE_CODE_MASTER = "SUBS_TYPE_MASTER";

    /** String code defining the type of the account as a standalone. That is, an
     *  account holder without any subordinates.
     */
    public static final String C_ACCOUNT_TYPE_CODE_STANDALONE = "SUBS_TYPE_STANDALONE";

    /** String code defining the type of the account as a S1 (or subordinate)
     *  account.
     */
    public static final String C_ACCOUNT_TYPE_CODE_SUBORDINATE = "SUBS_TYPE_SUBORDINATE";

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BasicAccountData";

    //------------------------------------------------------------------------
    // Private class instance attributes
    //------------------------------------------------------------------------

    /** The customer identifier associated with the account defined by this
     *  object.
     */
    private String  i_custId;

    /** The msisdn associated with the account .
     */
    private Msisdn  i_msisdn;
    
    /** First name of the customer. */
    private String       i_firstName;

    /** Surname of the customer. */
    private String       i_surname;

    /** The 'bill to' customer identifier associated with the account defined
     *  by this object. Tbat is, the master account associated with this account).
     */
    private String  i_btCustId;

    /** The Identifier of the SCP or SDP that this account is stored on. */
    private String  i_scpId;

    /** The status of the account . */
    protected char    i_custStatus;

    /** Customer's market. */
    private Market  i_market;

    /** Flag to indicate that account type is STANDALONE, as opposed to MASTER.
     *  It is a Boolean object rather than a boolean primitive because it must
     *  be nullable.
     */
    private Boolean i_isStandalone = null;
    
    /** Flag to indicate if a promotion plan synchronization is required.
     */
    private boolean i_isPromoPlanSynch = false;
    
    /** Agent for this customer. */
    private String       i_agent;
    
    /** Subagent for this customer. */
    private String       i_subAgent;  
    
    /** The date that the customer's account is/was activated. */    
    private PpasDate i_activationDate;
               
    /** The date that the customer's account was last refilled. */    
    private PpasDateTime i_lastRefillDateTime = new PpasDateTime("");

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /** Construct an instance of a BasicAccountData object. This
     *  object can then be used to access key account information from the 
     *  database.
     * 
     * @param p_request The request to process.
     * @param p_custId  Internal identifier of this mobile user.
     * @param p_btCustId Internal representation of the account holder.
     * @param p_msisdn  This mobile number.
     * @param p_surname Last name of this mobile user.
     * @param p_firstName First name of this mobile user.
     * @param p_custStatus Status of this mobile user.
     * @param p_scpId   Identifier of the SDP that contains this account.
     * @param p_market  Market with which this account is associated.
     * @param p_agent   Identifier of agent that sold this mobile phone.
     * @param p_subAgent Identifier of sub-agent that sold this mobile phone.
     * @param p_activationDate Date the account was activated.
     * @param p_isPromoSynch  Flag that indicate if the promotion plan synchronization is required or not.
     * @param p_lastRefillDateTime Date/time the account was last refilled.
     */
    public BasicAccountData(PpasRequest  p_request,
                            String       p_custId,
                            String       p_btCustId,
                            Msisdn       p_msisdn,
                            String       p_surname,
                            String       p_firstName,
                            char         p_custStatus,
                            String       p_scpId,
                            Market       p_market,
                            String       p_agent, 
                            String       p_subAgent, 
                            PpasDate     p_activationDate,
                            PpasDateTime p_lastRefillDateTime,
                            boolean      p_isPromoSynch)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_BUSINESS,
                PpasDebug.C_ST_CONFIN_START, 
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing BasicAccountData(...)");
        }

        i_custId            = p_custId;
        i_btCustId          = p_btCustId;
        i_msisdn            = p_msisdn;
        i_surname           = p_surname;
        i_firstName         = p_firstName;
        i_scpId             = p_scpId;
        i_custStatus        = p_custStatus;
        i_market            = p_market;
        i_agent             = p_agent;
        i_subAgent          = p_subAgent;
        i_activationDate    = p_activationDate;    
        i_lastRefillDateTime = p_lastRefillDateTime;    
        i_isPromoPlanSynch  = p_isPromoSynch;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_BUSINESS,
                PpasDebug.C_ST_BUSINESS_DATA, 
                p_request, C_CLASS_NAME, 10005, this,
                this.toString());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10090, this,
                "Constructed BasicAccountData(...)");
        }
    }  // end of constructor BasicAccountData(...)
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    // Getter methods

    /** Get the customer id of the account.
     * 
     * @return Customer Id
     */
    public String getCustId()
    {
        return(i_custId);
    }

    /** Get the promotion flag synchronized status.
     * 
     * @return TRUE if a promotion plan synchronization is required, FALSE if not.
     */
    public boolean isPromoSynchronized()
    {
        return i_isPromoPlanSynch;
    }

    /** Get the 'bill to' customer id of the account. That is, the master account for this account.
     * 
     * @return Internal identifier of account holder.
     */
    public String getBtCustId()
    {
        return(i_btCustId);
    }

    /** Get the msisdn of the account.
     * 
     * @return Mobile number for this user.
     */
    public Msisdn getMsisdn()
    {
        return(i_msisdn);
    }

    /** Get the identifier of the SCP/SDP that the account is held on.
     * 
     * @return Identifier of SDP that holds this account.
     */
    public String getScpId()
    {
        return(i_scpId);
    }
    
    /** Get the status of the account.
     * 
     * @return The status of this account.
     */
    
    public char getCustStatus()
    {
        return(i_custStatus);
    }

    /** Get the customer status code string.
     * 
     * @return The status of this account as a descriptive string.
     */
    public String getCustStatusCode()
    {
        String l_custStatusCode;

        switch(i_custStatus)
        {
            case C_SUBS_STATUS_ACTIVE:
                if (!i_activationDate.toString().equals(""))
                {
                    l_custStatusCode = C_SUBS_STATUS_CODE_ACTIVE;
                }
                else
                {
                    l_custStatusCode = C_SUBS_STATUS_CODE_AVAILABLE;
                }
                break;
            case C_SUBS_STATUS_PERM_DISC:
                l_custStatusCode = C_SUBS_STATUS_CODE_DISCONNECTED;
                break;
            default:
                l_custStatusCode = C_SUBS_STATUS_CODE_INVALID;
                break;
        }
 
        return(l_custStatusCode);

    }

     /** Get the customer status as a meaningful string.
     *  @return A meaningful string defining the customer
     */
    public String getCustStatusStr()
    {
        char l_custStatus = i_custStatus;

        if ((i_custStatus == C_SUBS_STATUS_ACTIVE) &&
                 (i_activationDate.toString().equals("")))
        {
            l_custStatus = C_SUBS_STATUS_AVAILABLE;
        }
        return(convertCustStatusToString(l_custStatus));
    }

    /** Converts a customer status into a meaningful string representation.
     * 
     * @param p_custStatus Status to convert to description.
     * @return a string representation of the cust status
     */
    public static String convertCustStatusToString(char p_custStatus)
    {
        
        String l_custStatusStr;

        switch(p_custStatus)
        {
            case C_SUBS_STATUS_MAKE_SUBORDINATE_IN_PROG: // status 'S' to be displayed as active
            case C_SUBS_STATUS_ACTIVE:
                l_custStatusStr = C_SUBS_STATUS_STR_ACTIVE;
                break;

            case C_SUBS_STATUS_AVAILABLE:
                l_custStatusStr = C_SUBS_STATUS_STR_AVAILABLE;
                break;

            case C_SUBS_STATUS_RECONNECTION_IN_PROG: // status 'R' to be displayed as disconnected
            case C_SUBS_STATUS_DISC_IN_PROG: // status 'D' to be displayed as disconnected
            case C_SUBS_STATUS_PERM_DISC:
                l_custStatusStr = C_SUBS_STATUS_STR_DISCONNECTED;
                break;

            case C_SUBS_STATUS_INSTALLED:
                l_custStatusStr = C_SUBS_STATUS_STR_NOT_INSTALLED;
                break;

            default:
                l_custStatusStr = C_SUBS_STATUS_STR_INVALID;
                break;
        }
        
        return(l_custStatusStr);
    }
 
    /** Get the account type string code.
     * 
     * @return Type of account.
     */
    public String getAccountTypeCode()
    {
        String l_accountTypeCode;

        // Set up the account type code string.
        if (i_custId.equals(i_btCustId))
        {
            l_accountTypeCode = C_ACCOUNT_TYPE_CODE_ACCOUNT_HOLDER;
        }
        else
        {
            l_accountTypeCode = C_ACCOUNT_TYPE_CODE_SUBORDINATE;
        }

        return(l_accountTypeCode);
    }

    /** Get the STANDALONE flag, which indicates that the account is an account
     *  holder, but has no subordinates. This flag is not guaranteed to be set,
     *  so any service using it must ensure it is set.
     *
     * @return Flag indicating whether the account is stand-alone or not.
     */
    public Boolean isStandalone()
    {
        return i_isStandalone;
    }
    
    /**
     * Is the subscriber disconnected.
     * @return <code>true</code> if cust_status == C_SUBS_STATUS_PERM_DISC, else <code>false</code>
     */
    public boolean isDisconnected()
    {
        return (getCustStatus() == C_SUBS_STATUS_PERM_DISC);
    }

    /** Get the market.
     * 
     * @return The market with which this account si associated.
     */
    public Market getMarket()
    {
        return(i_market);
    }

    /**  Get the activation date associated with this account.
     * 
     * @return The date the account was activated.
     */
    public PpasDate getActivationDate()
    {
        return i_activationDate;
    }
        
    /**  Get the last date/time this account was refilled.
     * 
     * @return The date/time the account was last refilled.
     */
    public PpasDateTime getLastRefillDateTime()
    {
        return i_lastRefillDateTime;
    }
        
    /** Get the agent associated with this customer.
     * 
     * @return The identifier of the agent that sold the phone.
     */
    public String getAgent()
    {
        return(i_agent);
    }
        
    /** Get the subagent associated with this customer.
     * 
     * @return The identifier of the sub-agent that sold the phone.
     */
    public String getSubAgent()
    {
        return(i_subAgent);
    }

    // Setter methods

    /** Set the bt customer id associated with this account.
     * 
     * @param p_btCustId The internal identifier of the account holder.
     */
    public void setBtCustId(String p_btCustId)
    {
        i_btCustId = p_btCustId;
        return;
    }
    /** Set the promotion synchronization flag.
     * 
     * @param p_isPromoSynch The flag value TRUE if a synchronization is required, FALSE if not.
     */
    public void setPromoSynchFlag(boolean p_isPromoSynch)
    {
        i_isPromoPlanSynch = p_isPromoSynch;       
    }

    /** Set the customer's market.
     * 
     * @param p_market The market of the account.
     */
    public void setMarket(Market p_market)
    {
        i_market = p_market;
    }

    /** Set the agent associated with this customer.
     * 
     * @param p_agent The identifier of the agent that sold the phone.
     */
    public void setAgent(String p_agent)
    {
        i_agent = p_agent;
    }
    
    /** Set the SubAgent associated with this customer.
     * 
     * @param p_subAgent The identifier of the sub-agent that sold the phone.
     */
    public void setSubAgent(String p_subAgent)
    {
        i_subAgent = p_subAgent;
    }
    
    /** Set the activation date of the account.
     * 
     * @param p_activationDate Date the account was activated.
     */
    public void setActivationDate(PpasDate p_activationDate)
    {
        i_activationDate = p_activationDate;
    }

    /** Set the last refill date/time for the account.
     * 
     * @param p_lastRefillDateTime Date/Time the account was last refilled.
     */
    public void setLastRefillDateTime(PpasDateTime p_lastRefillDateTime)
    {
        i_lastRefillDateTime = p_lastRefillDateTime;
    }

    /** Set flag to indicate that account type is STANDALONE.
     * 
     * @param p_isStandalone True if the account is stand alone.
     */
    public void setStandalone(boolean p_isStandalone)
    {
        i_isStandalone = new Boolean(p_isStandalone);
    }

    // Utility methods

    /** This method returns true if the account is a 
     *  subordinate (S1) account. Otherwise, it returns false.
     *  @return True if this account is a subordinate account; otherwise false.
     */
    public boolean isSubordinate()
    {
        return(!i_custId.equals(i_btCustId));
    }

    /** Get the first name of the customer.
     * 
     * @return First name of the mobile user.
     */
    public String getFirstName()
    {
        return (i_firstName);
    }

    /** Get the surname of the customer.
     * 
     * @return Last name of the mobile user.
     */
    public String getSurname()
    {
        return (i_surname);
    }

    /** Show the account as a <code>String</code>.
     * 
     * @return String representation of the accumulator.
     */
    public String toString()
    {
        StringBuffer l_sB = new StringBuffer();
        
        l_sB.append("MSISDN: " + i_msisdn);
        l_sB.append(", Status: " + i_custStatus);
        l_sB.append(", Activation date: " + i_activationDate);
        l_sB.append(", Id: " + i_custId);
        l_sB.append(", BT Id: " + i_btCustId);
        l_sB.append(", Name: " + i_firstName + " " + i_surname);
//        l_sB.append(", Type: " + (i_isStandalone.booleanValue() ? "Subordinate" : "Master"));
        l_sB.append(", Mkt: " + i_market.getSrva() + "/" + i_market.getSloc());
        l_sB.append(", SCP: " + i_scpId);
        l_sB.append(", Agent: " + i_agent);
        l_sB.append(", SubAgent: " + i_subAgent);
        
        return l_sB.toString();
    }
    
    /** Compare whether this account is logically the same as another account.
     * 
     * @param p_account Account to which this account will be compared.
     * @return True if the accounts are logically the same.
     */
    public boolean equals (BasicAccountData p_account)
    {
        if (this.i_activationDate != null)
        {
            if (p_account.getActivationDate() != null)
            {
                if (!this.i_activationDate.toString().equals(p_account.getActivationDate().toString()))
                {
                    return false;
                }
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getActivationDate() != null)
        {
            return false;
        } 
        
        if (this.i_agent != null)
        {
            if (!this.i_agent.equals(p_account.getAgent()))
            {
                return false;
            } 
        }
        else if (p_account.getAgent() != null)
        {
            return false;
        }        

        if (i_btCustId != null)
        {
            if (p_account.getBtCustId() != null)
            {
                if (Integer.parseInt(i_btCustId) != Integer.parseInt(p_account.getBtCustId()))
                {
                    return false;
                } 
            }
            else
            {
                return false;
            } 
        }
        else if (p_account.getBtCustId() != null) 
        {
            return false;
        } 

        if (i_custId != null)
        {
            if (p_account.getCustId() != null)
            {
                if (Integer.parseInt(i_custId) != Integer.parseInt(p_account.getCustId()))
                {
                    return false;
                } 
            }
        }
        else if (p_account.getCustId() != null)
        {
            return false;
        }        

        if (this.i_firstName != null)
        {
            if (!this.i_firstName.equals(p_account.getFirstName()))
            {
                return false;
            } 
        }
        else if (p_account.getFirstName() != null)
        {
            return false;
        }

        if (this.isStandalone() != p_account.isStandalone())
        {
            return false;
        } 

        if (this.i_msisdn != null)
        {
            if (!this.i_msisdn.equals(p_account.getMsisdn()))
            {
                return false;
            } 
        }
        else if (p_account.getMsisdn() != null)
        {
            return false;
        } 

        if (this.i_scpId != null)
        {
            if (!this.i_scpId.equals(p_account.getScpId()))
            {
                 return false;
            } 
        }
        else if (p_account.getScpId() != null)
        {
            return false;
        } 

        if ( !this.i_market.equals(p_account.getMarket()) )
        {
            return false;
        }        

        if (this.i_subAgent != null)
        {
            if (!this.i_subAgent.equals(p_account.getSubAgent()))
            {
                return false;
            } 
        }
        else if (p_account.getSubAgent() != null)
        {
            return false;
        } 

        if (this.i_surname != null)
        {
            if (!this.i_surname.equals(p_account.getSurname()))
            {
                return false;
            } 
        }
        else if (p_account.getSurname() != null)
        {
            return false;
        } 
        if (this.isPromoSynchronized() != p_account.isPromoSynchronized())
        {
            return false;
        }
        return true;
    }
    
    /** Compare whether this account is logically the same as another account object.
     * 
     * @param p_that Object to which this object is compared.
     * @return True if the accounts are logically the same.
     */
    public boolean equals (Object p_that)
    {
        if (p_that instanceof BasicAccountData)
        {
            return equals((BasicAccountData)p_that);
        }

        return super.equals(p_that);
    }
    
    //------------------------------------------------------------------------
    // Private methods
    //------------------------------------------------------------------------

    /** 
     * Allows to set the status.
     * 
     * @param p_c the Status code 
     */
    public void setCustStatus(char p_c)
    {
        i_custStatus = p_c;
    }

} // end of class 'BasicAccountData'
