////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PrilPrivilegeLevelData.Java
//      DATE            :       02-June-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpaLon#112/2508
//                              PRD_ASCS00_DEV_SS_088
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Stores a row from the PRIL_PRIVILEGE_LEVEL 
//                              database table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** An object that represents a row from the SRVA_MISC_CODES database table. */
public class PrilPrivilegeLevelData
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrivilegeLevelData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The privilege level. */
    private int i_level;
    
    /** The description of this privilege. */
    private String i_description;

    /** Currency flag - when false this privilege is no longer valid. */
    private boolean i_isCurrent;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates a privilege level object.
     * 
     * @param p_request The request to process.
     * @param p_level The privilege level
     * @param p_description The privilege description
     * @param p_deleteFlag Indicates whether data item is still current.
     */
    public PrilPrivilegeLevelData(PpasRequest p_request,
                                  int         p_level,
                                  String      p_description,
                                  String      p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_level       = p_level;
        i_description = p_description;

        if (p_deleteFlag.equals("*"))
        {
            i_isCurrent = false;
        }
        else
        {
            i_isCurrent = true;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME );
        }
    }
    
    //------------------------------------------------------------------------
    // Overridden superclass methods
    //------------------------------------------------------------------------
    /** Returns a string representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        return(i_level + " - " + i_description);
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Returns the Privilege Level.
     * 
     * @return The Privilege level
     */
    public int getPrivilegeLevel()
    {
        return i_level;
    }

    /** Returns the Privilege description.
     * 
     * @return The description associated with this data object.
     */
    public String getDescription()
    {
        return i_description;
    }

    /** Returns the privilege level delete flag.
     * 
     * @return Flag indicating whether this privilege level is still current or marked as deleted.
     */
    public boolean getIsCurrent()
    {
        return i_isCurrent;
    }

    /** 
     * Returns a String containing the code and the description.
     * 
     * @return Miscellaneous data in a format suitable for display.
     */
    public String getDisplayString()
    {
        return(i_level + " - " + i_description);
    }

    /**
     * Compares this object with another PrilPrivilegeLevelData object.
     * @param p_privilegeData Object to compare with.
     * @return True if objects represent the same privilege level, false otherwise.
     */
    public boolean equals(Object p_privilegeData)
    {
        if (p_privilegeData instanceof PrilPrivilegeLevelData)
        {
            return (i_level == ((PrilPrivilegeLevelData)p_privilegeData).getPrivilegeLevel() ? true : false);
        }

        return false;
    }
}
