////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ChurnConditionSuspectAgent.java
//      DATE            :       09-Apr-2007
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_ASCS00_GEN_CA_115
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Identify whether a customer qualifies for churn based on
//                              a suspect agent.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.churn;

import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaAgentMstrDataSet;
import com.slb.sema.ppas.common.support.PpasProperties;

/** Identify whether a customer qualifies for churn based on a suspect agent. */
public class ChurnConditionSuspectAgent extends ChurnCondition
{
    /** Name of this condition. Value is {@value}. */
    private final static String C_CONDITION_NAME = "C6 - Suspect Agent. ";
    
    /** Constructor for a Churn condition based on suspect agent.
     *
     * @param p_properties Properties to determine criteria.
     * @param p_bcCache    Busness Configuration Cache.
     * @param p_analyser   Churn Analysier - for setting flags.
     */
    public ChurnConditionSuspectAgent(PpasProperties p_properties, BusinessConfigCache p_bcCache, ChurnAnalyser p_analyser)
    {
        super(p_properties, p_bcCache, p_analyser);
    }
        
    /** Analyse whether the data matches this criteria for churning.
     * 
     * @param p_data Details of the account.
     * @param p_reason Description of why the account matches or doesn't match. The method should add to this field.
     * @return True if the match suggests the customer is likely to churn.
     */
    public boolean matches(ChurnIndicatorData p_data, StringBuffer p_reason)
    {
        p_reason.append(C_CONDITION_NAME);

        SrvaAgentMstrDataSet l_agents = i_bcCache.getSrvaAgentMstrCache().getAvailable(p_data.getMarket());

        SrvaAgentMstrData l_agent = l_agents.getAgentDetails(p_data.getAgent());

        // TODO Should probably limit to within a few months - if the customer was from a suspect agent but have been active for
        //      a long time then the impact of the agent will be minimal.
        
        p_reason.append("Customer asssociated with agent ");
        p_reason.append(p_data.getAgent());
        p_reason.append(" (");
        p_reason.append(l_agent.getAgentName());
        p_reason.append("). Suspect ? ");
        p_reason.append(l_agent.isSuspect() ? "Yes" : "No");
        p_reason.append(". ");
        p_reason.append(l_agent.isSuspect() ? C_CHURN_TEXT : C_NO_CHURN_TEXT);
        
        return l_agent.isSuspect();
    }

    /** Describe this condition.
     * 
     * @return Description of this condition.
     */
    public String describe()
    {
        return C_CONDITION_NAME + "Likely to Churn if from a Suspect Agent";
    }
}
