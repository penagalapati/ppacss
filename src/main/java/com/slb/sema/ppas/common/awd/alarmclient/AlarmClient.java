//
//      DESCRIPTION     :       Alarm Client for sending events and alarms
//                              to the Alarm Server.
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 28/02/07 | R.Grimshaw | Retrieve max queue size config. | PpacLon#2964
//          |            | Pass instrumentManager to alarm |
//          |            | message queue.                  | 
//----------+------------+---------------------------------+--------------------
package com.slb.sema.ppas.common.awd.alarmclient;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.slb.sema.ppas.common.awd.alarmcommon.Alarm;
import com.slb.sema.ppas.common.awd.alarmcommon.AlarmEvent;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.InstrumentedObjectInterface;
import com.slb.sema.ppas.util.instrumentation.ThroughputCounterInstrument;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.net.TraceableSocket;
import com.slb.sema.ppas.util.structures.Queue;
import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * Alarm Client for sending events and alarms
 * to the Alarm Server. The alarm client is asynchronous (does not block
 * callers). If the alarm server is not contactable TODO:!!!! log, not silently discard????
 */
public class AlarmClient
implements InstrumentedObjectInterface
{

    /** Name of class used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME  = "AlarmClient";

    /**
     * Base part of name for configuration properties for this class/component.
     * Value is {@value}.
     */    
    private static final String C_CFG_PROPERTY_BASE =
                                        "com.slb.sema.ppas.common.awd.alarmclient.AlarmClient.";
    /**
    * Configuration property name for the alarm server node name / IP addess this
    * alarm client should send alarms and events to.
    * Value is {@value}.
    */    
    public static final String C_CFG_PARAM_ALARM_ENABLED = C_CFG_PROPERTY_BASE +
                                       "enabled";

    /**
    * Configuration property name for the alarm client's queue size. If the queue size
    * is greater than zero and exceeded alarms will be silently discarded.
    * Value is {@value}.
    */
    public static final String C_CFG_PARAM_ALARM_MAX_QUEUE_SIZE = C_CFG_PROPERTY_BASE +
                                       "maxQueueSize";

     /**
     * Configuration property name for the alarm server node name / IP addess this
     * alarm client should send alarms and events to.
     * Value is {@value}.
     */    
    public static final String C_CFG_PARAM_ALARM_SERVER_NODE = C_CFG_PROPERTY_BASE +
                                        "alarmServerNode";
    /**
     * Default for the alarm server node name / IP addess.
     * Value is {@value}.
     */    
    private static final String C_CFG_PARAM_ALARM_SERVER_NODE_DEFAULT =
                                        "localhost";
    
    /**
     * Configuration property name for the alarm server port number.
     * Value is {@value}.
     */    
    public static final String C_CFG_PARAM_ALARM_SERVER_PORT_NUMBER = C_CFG_PROPERTY_BASE +
                                        "alarmServerPortNumber";

    /** Simple date formatter. */
    private static final SimpleDateFormat C_DF =
            new SimpleDateFormat("dd-MMM-yy HH:mm:ss.SSS"); 

    /** Instrument Manager to register any instruments with. */
    private InstrumentManager  i_instrumentManager;
    
    /** Instrument Set for this instrumented object. */
    private InstrumentSet       i_instrumentSet;

    /**
     * Number of alarms queued to be delivered by this handler
     * counter instrument.
     */
    private ThroughputCounterInstrument   i_alarmsQueuedTCI;

    /**
     * Number of events queued to be delivered by this handler
     * counter instrument.
     */
    private ThroughputCounterInstrument   i_eventsQueuedTCI;

    private ClientThread        i_clientThread;

    /** Flag indicating whether we are connected to a server. */
    private boolean             i_connected = false;
    private boolean             i_enabled = false;
    /** Flag indicating whether a fatal error has occurred. */
    private boolean             i_alarmClientFatalError = false;
    
    /** Queus of alarm messages. */
    private Queue               i_alarmMessageQueue;
    
    /** Traceable socket. */
    private TraceableSocket     i_socket = null;
    
    /** Stream on which an event should be sent to the server. */
    private OutputStream        i_os;
    
    /** Format for specifying length of the message. */
    private DecimalFormat       C_LENGTH_FORMAT = new DecimalFormat("0000");
    
    /** Name of the server. */
    private String              i_serverName;
    
    /** Port number on whic hteh server is listening. */
    private int                 i_serverPortNumber;
    
    /** Flag indicating this is teh first parameter to be sent. */
    private boolean             i_firstParameter = true;

    /* TODO:<<Add inner thread to handle background inactivity disconnection!!!!>>
     * TODO:<<Add inner thread to handle background sending of messages from Q!!!!>>
     */

    /**
     *  Constructor where configuration is specified in properties.
     * @param p_configProperties Properties defining the configuration.
     */
    public AlarmClient(
        UtilProperties          p_configProperties
        )
    {
        this((InstrumentManager)null, p_configProperties);
    }
    
    /**
     *  Constructor with an instrument manager where configuration is specified in properties.
     * @param p_instrumentManager Instrument manager.
     * @param p_configProperties Properties defining the configuration.
     */
    public AlarmClient(
        InstrumentManager       p_instrumentManager,
        UtilProperties          p_configProperties
        )
    {
        super();

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10000, this,
                        "Constructing " + C_CLASS_NAME + " with InstrumentManager" + p_instrumentManager);
        }

        i_instrumentManager = p_instrumentManager;

        init(p_configProperties);

        if(Debug.on)
        {
            Debug.print(Debug.C_LVL_VLOW, Debug.C_APP_INIT | Debug.C_APP_MWARE,
                        Debug.C_ST_CONFIN_START, C_CLASS_NAME, 10090, this,
                        "Constructed " + C_CLASS_NAME + " " + this);
        }
    }
    
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_sendEvent = "sendEvent";

    /** Send an event to the queue.
     * 
     * @param p_event Alarm event to queue.
     */
    public void sendEvent(
        AlarmEvent              p_event
        )
    {
        if (Debug.on) 
        {
            Debug.print(Debug.C_LVL_VHIGH, Debug.C_APP_MWARE,
                        Debug.C_ST_START,
                        C_CLASS_NAME, 110010, this,
                        "Entered " + C_METHOD_sendEvent + " with " + p_event);    
        }
        // doSend(p_event);
        queueMessage(p_event);
    }
    
    /** Name of method used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_doSend = "doSend";
    /** Send a queued event to the server.
     * 
     * @param p_event Event to send.
     */
    private synchronized void doSend(
        AlarmEvent              p_event
        )
    {
        if(i_enabled)
        {
            // setupOutputStream();
            if(i_os != null)
            {
    
                if(Debug.on) Debug.print(
                        Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 11000, this,
                        "Connection available, sending event " + p_event);
                try
                {
                    sendHeader();
                    sendField("awpType", "event");
                    sendField("eventName", p_event.getEventName());
                    sendField("raisedTime", Long.toString(p_event.getRaisedTime()));
                    sendField("eventText", p_event.getEventText());
                    sendField("origNodeName", p_event.getOriginatingNodeName());
                    sendField("origProcName", p_event.getOriginatingProcessName());
                    i_os.write("&&".getBytes());
                    i_os.flush();
                }
                catch(IOException l_e)
                {
                    outputThrowable(C_METHOD_doSend,
                            " Failed to send event " + p_event +
                            " to " + i_serverName + ":" +
                            i_serverPortNumber, l_e);
                    close();
                }
            }
            else
            {
                if(Debug.on) Debug.print(
                        Debug.C_LVL_MODERATE, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 11000, this,
                        "Connection NOT available, ignoring event " + p_event);
            }
        }
    }

    /**
     * Sets the <code>InstrumentManager</code>. 
     * 
     * @param p_instrumentManager the new <code>InstrumentManager</code>.
     */
    public synchronized void setInstrumentManager(
        InstrumentManager       p_instrumentManager
        )
    {
        if(i_instrumentManager != null)
        {
            i_instrumentManager.unregisterObject(this);
        }
        i_instrumentManager = p_instrumentManager;
        i_instrumentManager.registerObject(this);
        
        if (i_alarmMessageQueue != null)
        {
            i_alarmMessageQueue.setInstrumentManager(p_instrumentManager);
        }
    }

    /** Send an alarm to the queue.
     * 
     * @param p_alarm Alarm event to queue.
     */
    public synchronized void sendAlarm(
        Alarm                   p_alarm
        )
    {
        // doSend(p_alarm);
        queueMessage(p_alarm);
    }
    
    /** Send a queued alarm to the server.
     * 
     * @param p_alarm Alarm to send.
     */
    public synchronized void doSend(
        Alarm                   p_alarm
        )
    {
        Map                     l_parameterMap;
        Object                  l_keyObjectARR[];
        int                     l_loop;
       
        if(i_enabled)
        {
            // setupOutputStream();
            if(i_os != null)
            {
    
                if(Debug.on) Debug.print(
                        Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 11000, this,
                        "Connection available, sending alarm " + p_alarm);
                try
                {
                    sendHeader();
                    sendField("armpType", "alarm");
                    sendField("alarmName", p_alarm.getAlarmName());
                    sendField("alarmSeverity", Integer.toString(p_alarm.getAlarmSeverity()));
                    sendField("raisedTime", Long.toString(p_alarm.getRaisedTime()));
                    sendField("origNodeName", p_alarm.getOriginatingNodeName());
                    sendField("origProcName", p_alarm.getOriginatingProcessName());
                    sendField("alarmText", p_alarm.getAlarmText());
                    l_parameterMap = p_alarm.getParameterMap();
                    if(l_parameterMap != null)
                    {
                        l_keyObjectARR = l_parameterMap.keySet().toArray();
                        for(l_loop = 0; l_loop < l_keyObjectARR.length; l_loop++)
                        {
                            sendField((String)l_keyObjectARR[l_loop],
                                    (String)l_parameterMap.get(l_keyObjectARR[l_loop]));
                        }
                    }
                    i_os.write("&&".getBytes());
                    i_os.flush();
                }
                catch(IOException l_e)
                {
                    outputThrowable(C_METHOD_doSend,
                            " Failed to send alarm " + p_alarm +
                            " to " + i_serverName + ":" +
                            i_serverPortNumber, l_e);
                    close();
                }
            }
            else
            {
                // TODO: <<Don't ignore alarms????>>
                if(Debug.on) Debug.print(
                        Debug.C_LVL_HIGH, Debug.C_APP_MWARE,
                        Debug.C_ST_TRACE, C_CLASS_NAME, 11000, this,
                        "Connection NOT available, ignoring event " + p_alarm);
            }
        }
    }

    /**
     * Returns this instrumented object's instrument set.
     * @return This instrumented object's instrument set.
     */
    public InstrumentSet getInstrumentSet()
    {
        return i_instrumentSet;
    }
    
    /**
     * Returns this instrumented object's name.
     * @return This instrumented object's name.
     */
    public String getName()
    {
        //TODO: Improve????
        return "AlarmClient";
    }
    
    /** Initialisation of this object.
     * 
     * @param p_configProperties Configuration properties.
     */
    private void init(
        UtilProperties          p_configProperties
        )
    {        
        PpasProperties          l_ppasConfigProperties;
        boolean                 l_enabled;
        int                     l_maxQueueSize = 0;
        
        l_enabled = p_configProperties.getBooleanProperty(
                C_CFG_PARAM_ALARM_ENABLED, true);

        if(l_enabled)
        {
            i_instrumentSet = new InstrumentSet("root");
            i_alarmsQueuedTCI = new ThroughputCounterInstrument(
                    "Alarms Queued",
                    "Number of alarms queued to be sent to alarm server.");
            i_instrumentSet.add(i_alarmsQueuedTCI);
            i_eventsQueuedTCI = new ThroughputCounterInstrument(
                    "Events Queued",
                    "Number of events queued to be sent to alarm server.");
            i_instrumentSet.add(i_eventsQueuedTCI);

            l_maxQueueSize = p_configProperties.getIntProperty(
                  C_CFG_PARAM_ALARM_MAX_QUEUE_SIZE, 1000);
            
            l_maxQueueSize = p_configProperties.getIntProperty(
                  C_CFG_PARAM_ALARM_MAX_QUEUE_SIZE, 1000);

            i_alarmMessageQueue = new Queue("AlarmMessageQ",
                                            i_instrumentManager,
                                            Queue.C_FLAG_DISCARD_OBJECTS,
                                            l_maxQueueSize);
            i_alarmMessageQueue.init();
 
            i_serverName = p_configProperties.getTrimmedProperty(
                    C_CFG_PARAM_ALARM_SERVER_NODE,
                    C_CFG_PARAM_ALARM_SERVER_NODE_DEFAULT);
            if(p_configProperties instanceof PpasProperties)
            {
                l_ppasConfigProperties = (PpasProperties)p_configProperties;
                try
                {
                    i_serverPortNumber = Integer.parseInt(l_ppasConfigProperties.getPortNumber(
                            C_CFG_PARAM_ALARM_SERVER_PORT_NUMBER));
                }
                catch(PpasConfigException l_e)
                {
                    // Best not to try to log this as this might have been
                    // called from a logger!
                    l_e.printStackTrace();
                    i_alarmClientFatalError = true;
                }
            }
            else
            {
                i_serverPortNumber = p_configProperties.getIntProperty(
                        C_CFG_PARAM_ALARM_SERVER_PORT_NUMBER);
            }

            i_clientThread = new ClientThread();
            i_clientThread.start();

            if(!i_alarmClientFatalError)
            {
                i_enabled = true;
            }
        }
    }

    /** Queue a message.
     * 
     * @param p_message Object to queue.
     */
    private void queueMessage(Object p_message)
    {
        try
        {
            // Queue if enabled, silently discard if not.
            // TODO: Count discarded in isntrument????
            if(i_enabled)
            {
                //if(i_alarmMessageQueue.)
                i_alarmMessageQueue.addLast(p_message);                
                if (InstrumentManager.i_on) // Don't add to this if (so can compile out)
                {
                    if (i_instrumentSet.i_isEnabled)
                    {
                        if(p_message instanceof AlarmEvent)
                        {
                            i_eventsQueuedTCI.increment();
                        }
                        else if(p_message instanceof Alarm)
                        {
                            i_alarmsQueuedTCI.increment();
                        }
                    }
                }
            }
            else
            {
                if (Debug.on)
                {
                    Debug.print(Debug.C_LVL_MODERATE,
                            Debug.C_APP_MWARE, Debug.C_ST_TRACE | Debug.C_ST_APPLICATION_DATA,
                            C_CLASS_NAME, 10000, this,
                            "AlarmClient disabled, silently discarding " + p_message);
                }
            }
        }
        catch(Throwable l_t)
        {
            // Output and ignore (we don't want alarm failures to
            // cause problems to application).
            // TODO: Count these and diable client????
            String l_messaage = "Alarm client " + this + ", error " + l_t +
            " trying to queue " + p_message;
            System.err.print(l_messaage);
            l_t.printStackTrace();
            if (Debug.on)
            {
                Debug.print(Debug.C_LVL_VLOW,
                        Debug.C_APP_MWARE, Debug.C_ST_ERROR,
                        C_CLASS_NAME, 10000, this,
                        l_messaage);
            }
        }
    }

    /** Send message header to the server.
     * 
     * @throws IOException If the header cannot be sent.
     */
    private void sendHeader()
    throws IOException
    {
        i_os.write("ARMPv001".getBytes());
        i_firstParameter = true;
    }

    /** Send a specified field to the server.
     * 
     * @param p_fieldName Name of field.
     * @param p_fieldValue Value of the field.
     * @throws IOException If the field cannot be sent.
     */
    private void sendField(
        String                  p_fieldName,
        String                  p_fieldValue
    )
    throws IOException
    {
        //int                     l_index;
        byte                      l_byteARR[];
        int                       l_pos;
        int                       l_previousPos = 0;
        
        if(!i_firstParameter)
        {
            i_os.write('&');
        }
        else
        {
            i_firstParameter = false;
        }
           
        i_os.write(p_fieldName.getBytes());
        i_os.write("=".getBytes());
        if(p_fieldValue != null)
        {
            l_byteARR = p_fieldValue.getBytes();
        }
        else
        {
            l_byteARR = new byte[0];
        }
        for(l_pos = 0; l_pos < l_byteARR.length; l_pos++)
        {
            switch(l_byteARR[l_pos])
            {
                case '&':
                i_os.write(l_byteARR, l_previousPos, (l_pos - l_previousPos));
                i_os.write("%26".getBytes());
                l_previousPos = l_pos + 1; // Skip &
                break;
                
                case '%':
                i_os.write(l_byteARR, l_previousPos, (l_pos - l_previousPos));
                i_os.write("%25".getBytes());
                l_previousPos = l_pos + 1; // Skip %
                break;
                
                default:
                break;
            }   
        }

        if(l_previousPos < l_pos)
        {
            // Write remaining
            i_os.write(l_byteARR, l_previousPos, (l_pos - l_previousPos));
            l_previousPos = l_pos;
        }
    }

    /** Close the connection to the server. */
    private void close()
    {
        i_connected = false;
        if(i_os != null)
        {
            try
            {
                i_os.close();
            }
            catch(IOException l_e)
            {
                l_e.printStackTrace();
            }
            finally
            {
                i_os = null;
            }
        }
        if(i_socket != null)
        {
            try
            {
                i_socket.close();
            }
            catch(IOException l_e)
            {
                l_e.printStackTrace();
            }
            finally
            {
                i_socket = null;
            }
        }
    }
    
    //TODO: Take out
    private void setupOutputStream()
    {
        if(i_socket == null)
        {
            try
            {
                i_socket = new TraceableSocket(i_serverName, i_serverPortNumber);
                i_os = i_socket.getOutputStream();
                i_connected = true;
            }
            catch(IOException l_e)
            {
                l_e.printStackTrace();
                close();
            }
        }        
    }
    
    /**
     * As this class is typically used to send logged events to the alarm server,
     * we need to prevent an infinite loop if an exception occurs, so we can't
     * log this easily, so just output to System.err.
     * @param p_method Name of method where problem occurred.
     * @param p_message Reason for failure.
     * @param p_throwable Exception that needs to be logged..
     */
    private void outputThrowable(String p_method, String p_message, Throwable p_throwable)
    {
        System.err.println(
                "[" + Thread.currentThread().getName() + "] " +
                C_DF.format(new Date()) + " " + C_CLASS_NAME +
                "." + p_method + ": " + p_message
                );
        p_throwable.printStackTrace();
    }

    /**
     * Returns a string representation suitable for debugging (use
     * <code>appendToStringBuffer</code> if appending to existing
     * <code>StringBuffer</code>).
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        StringBuffer           l_stringBuffer;

        l_stringBuffer = new StringBuffer(160);
        appendToStringBuffer(l_stringBuffer);

        return(l_stringBuffer.toString());
    }

    /** Appends a string representation suitable for debugging.
     * 
     * @param p_stringBuffer <code>StringBuffer</code> object to which the description will be added.
     */
    public void appendToStringBuffer(StringBuffer p_stringBuffer)
    {
        p_stringBuffer.append("AlarmClient=[i_clientThread=");
        p_stringBuffer.append(i_clientThread);
        p_stringBuffer.append(", i_connected=");
        p_stringBuffer.append(i_connected);
        p_stringBuffer.append(", i_enabled=");
        p_stringBuffer.append(i_enabled);
        p_stringBuffer.append(", i_alarmClientFatalError=");
        p_stringBuffer.append(i_alarmClientFatalError);
        p_stringBuffer.append(", i_alarmMessageQueue=");
        p_stringBuffer.append(i_alarmMessageQueue);
        p_stringBuffer.append(", i_serverName=");
        p_stringBuffer.append(i_serverName);
        p_stringBuffer.append(", i_serverPortNumber=");
        p_stringBuffer.append(i_serverPortNumber);
        p_stringBuffer.append("]");
    }
    
    private static int      c_threadInstanceCounter = 0; 
    private class ClientThread extends ThreadObject
    {
        /** name of this thread. */
        private String          i_threadName;
        /** Time of last connection failure. */
        private long            i_lastConnectFailureTime = 0;
        /** Number of consecutive connection failures. */
        private int             i_consecutiveConnectFailureCount = 0;
        /** Retry limit in fast mode. */
        private int             i_fastRetryLimit = 10;
        /** Retry interval in fast mode. */
        private int             i_fastRetryIntervalMillis = 10000;
        /** Retry interval in slow mode. */
        private int             i_slowRetryIntervalMillis = 60000;
        
        /** Standard constructor. */
        public ClientThread()
        {
            super(ThreadObject.C_FLAG_DAEMON);
            synchronized(ClientThread.class)
            {
                c_threadInstanceCounter++;
                i_threadName = "ArmClt-" + c_threadInstanceCounter;
            }
        }
        
        public void doRun()
        {
            try
            {
               while(!isStopping())
               {
                   doAlarmClientProcessing();
               }
            }
            finally
            {
                // Incase something goes seriously wrong, make sure
                // client disabled.
                i_enabled = false;
            }
            return;
        }
        
        /** Perform specific processing for the alarm client. */
        private void doAlarmClientProcessing()
        {
            Object              l_alarmMessage;
            //AlarmEvent          l_alarmEvent;
            //Alarm               l_alarm;
            
            //TODO: Check stop on thread will wake this up - should generate
            // interruptedexception I think. Perhaps catch this and triy up?
            l_alarmMessage = i_alarmMessageQueue.removeFirst();
            if(l_alarmMessage != null)
            {
                // TODO: Implement one retry if con just lost!
                trySend(l_alarmMessage);
            }

        }
        
        /** Name of method used in calls to middleware. Value is {@value}. */
        private static final String C_METHOD_trySend = "trySend";
        /** Attempt to send a message to the server.
         * 
         * @param p_alarmMessage Message to be sent.
         */
        private void trySend(Object p_alarmMessage)
        {
            if (Debug.on)
            {
                Debug.print(Debug.C_LVL_MODERATE,
                        Debug.C_APP_MWARE, Debug.C_ST_START | Debug.C_ST_APPLICATION_DATA,
                        C_CLASS_NAME, 10000, this,
                        "Entered " + C_METHOD_trySend + ", msg=" + p_alarmMessage);
            }
            if(!i_connected)
            {
                handleConnectionAttempt();
            }
            if(i_connected)
            {
                i_lastConnectFailureTime = 0;
                if(p_alarmMessage instanceof AlarmEvent)
                {
                    doSend((AlarmEvent)p_alarmMessage);
                }
                else if(p_alarmMessage instanceof Alarm)
                {
                    doSend((Alarm)p_alarmMessage);
                }
            }            
        }
        
        /** Name of method used in calls to middleware. Value is {@value}. */
        private static final String C_METHOD_handleConnectionAttempt = "handleConnectionAttempt";
        /** Handle an attempt to connect to the server. */
        private void handleConnectionAttempt()
        {
            long                l_currentTime;
            
            if(i_socket == null)
            {
                l_currentTime = System.currentTimeMillis();
                if(
                    (i_lastConnectFailureTime == 0)
                    ||
                    (
                    (i_consecutiveConnectFailureCount < i_fastRetryLimit) &&
                    (l_currentTime >= (i_lastConnectFailureTime + i_fastRetryIntervalMillis))
                    )               
                    ||           
                    (
                    (l_currentTime >= (i_lastConnectFailureTime + i_slowRetryIntervalMillis))
                    )
                
                )
                {
                    if (Debug.on)
                    {
                        Debug.print(Debug.C_LVL_VLOW,
                                Debug.C_APP_MWARE, Debug.C_ST_TRACE,
                                C_CLASS_NAME, 10000, this,
                                "Not connected, retry time exceeded, trying to connect to " +
                                i_serverName + ":" + i_serverPortNumber);
                    }
                    // Rety interval exceeded, so can try to connect.
                    try
                    {
                        i_socket = new TraceableSocket(i_serverName, i_serverPortNumber);
                        i_os = i_socket.getOutputStream();
                        // TODO: Add test message.
                        i_connected = true;
                        i_lastConnectFailureTime = 0;
                        i_consecutiveConnectFailureCount = 0;
                        if (Debug.on)
                        {
                            Debug.print(Debug.C_LVL_VLOW,
                                    Debug.C_APP_MWARE, Debug.C_ST_TRACE,
                                    C_CLASS_NAME, 10000, this,
                                    "Connected!, socket=" + i_socket);
                        }
                    }
                    catch(IOException l_e)
                    {
                        if (Debug.on)
                        {
                            Debug.print(Debug.C_LVL_VLOW,
                                    Debug.C_APP_MWARE, Debug.C_ST_TRACE | Debug.C_ST_ERROR,
                                    C_CLASS_NAME, 10000, this,
                                    "Connect failed, exception " + l_e);
                        }
                        i_lastConnectFailureTime = l_currentTime;
                        i_consecutiveConnectFailureCount++;
                        outputThrowable(C_METHOD_handleConnectionAttempt,
                                " Failed to connect to " + i_serverName + ":" +
                                i_serverPortNumber, l_e);
                        close();
                    }
                }
                else
                {
                    if (Debug.on)
                    {
                        Debug.print(Debug.C_LVL_HIGH,
                                Debug.C_APP_MWARE, Debug.C_ST_TRACE,
                                C_CLASS_NAME, 10000, this,
                                "Not connected, retry interval NOT exceeded, not connecting.");
                    }
                }
            }        
        }
       
        /** Get the name of this thread.
         * @return Name of this thread.
         */
        public String getThreadName()
        {
            return i_threadName;
        }
        
    } // End of private inner class ClientThread

} // End of public class AlarmClient
