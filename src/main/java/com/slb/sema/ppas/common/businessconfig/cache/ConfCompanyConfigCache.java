////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       ConfCompanyConfigCache.Java
//      DATE            :       10-July-2001
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1194/4921
//
//      COPYRIGHT       :       Atos Origin 2005
//
//      DESCRIPTION     :       A cache of ConfCompanyConfigData object
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/08/02 | R O'Brien  | Add method to return the Exter- | PpaLon#1498/6250
//          |            | nal Voucher Database Used flag. |
//          |            | Add method to return the voucher|
//          |            | serial number length, moving the|
//          |            | functionallity out of ccVoucher-|
//          |            | Servlet.                        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.ConfCompanyConfigData;
import com.slb.sema.ppas.common.businessconfig.sqlservice.ConfCompanyConfigSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache of ConfCompanyConfigData object.
 */
public class ConfCompanyConfigCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ConfCompanyConfigCache";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Company configuration data instance. */
    private ConfCompanyConfigData i_confCompanyConfigData = null;

    /** Sql service used to select the conf data from the database. */
    private ConfCompanyConfigSqlService i_confCompanyConfigSqlService = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new conf cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public ConfCompanyConfigCache(PpasRequest    p_request,
                                  Logger         p_logger,
                                  PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 98010, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_confCompanyConfigSqlService = new ConfCompanyConfigSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 28438, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns the company's configuration.
     *
     * @return Company configuratin data.
     */
    public ConfCompanyConfigData getConfCompanyConfigData()
    {
        return(i_confCompanyConfigData);

    } // End of public method getConfCompanyConfigData

    /** Get a boolean from the Company Configuration Data specifying whether
     *  an External Voucher Database used.
     *
     * @return Flag indicating whether an external voucher database is used.
     */
    public boolean getEvdbUsed()
    {
        return (i_confCompanyConfigData.getEvdbUsed());

    } // end method getEvdbUsed

    /** Get the voucher serial number length from Company Configuration Data.
     *
     * @return Length of a serial number.
     */
    public int getSerialNumberLength()
    {
        return (i_confCompanyConfigData.getSerialNumberLength());
    } // end method getSerialNumberLength

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the static configuration into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest     p_request,
                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 10140, this,
                            "Entered " + C_METHOD_reload );
        }

        i_confCompanyConfigData = i_confCompanyConfigSqlService.readConfCompanyConfig(p_request,
                                                                                      p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 10150, this,
                            "Leaving " + C_METHOD_reload );
        }
    } // End of public method reload(PpasRequest, long, Jdbcconnection)

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_confCompanyConfigData);
    }
}

