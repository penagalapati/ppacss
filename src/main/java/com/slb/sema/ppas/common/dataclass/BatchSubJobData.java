////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchSubJobData.java 
//      DATE            :       07-June-2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :      
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/** Details of a batch sub job. */
public class BatchSubJobData
{    
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchSubJobData";

    /**The name of the batch.*/
    private String i_batchType = null;
    
    /**The operator id.*/
    private String i_opId = null;
 
    /**The batch start customer id. */
    private String i_startCustId = null;

    /**The batch end customer id. */
    private String i_endCustId = null;

    /**The status of the batch job.*/
    private String i_batchJobStatus = null;

    /**The last processed customer id.*/
    private String i_lastProcessedCustId = null;

    /**The first line of extra data. */
    private String i_extraData1 = null;

    /**The second line of extra data. */
    private String i_extraData2 = null;


    /**Time stamp when the batch was launched. */
    private String i_jobExecutionDateTime;

    /**A uniqe js job ID that is assigned to the job by Job Schedlur. */
    private String i_jsJobId;
    
    /**The js job ID of the master batch process. */
    private String i_masterJsJobId;
    
    /**The sub-job id. */
    private String i_subJobId;

    /**Constructs a BatchJobData object. */
    public BatchSubJobData()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10110,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    /**
     * @return The type of batch job.
     */
    public String getBatchType()
    {
        return i_batchType;
    }
    
    /**
     * @return the Batch job status.
     */
    public String getBatchJobStatus()
    {
        return i_batchJobStatus;
    }

    /**
     * @return The save point.
     */
    public String getLastProcessedCustId()
    {
        return i_lastProcessedCustId;
    }

    /**
     * @return The end cust id for the batch job.
     */
    public String getEndCustId()
    {
        return i_endCustId;
    }

    /**
     * @return Extra data.
     */
    public String getExtraData1()
    {
        return i_extraData1;
    }

    /**
     * @return Extra data.
     */
    public String getExtraData2()
    {
        return i_extraData2;
    }

    /**
     * @return The batch job start id.
     */
    public String getStartCustId()
    {
        return i_startCustId;
    }

    /**
     * @return The operator id.
     */
    public String getOpId()
    {
        return i_opId;
    }
    
    /** Get the date/time this batch was started.
     * @return The time stamp when the batch was launched.
     */
    public String getExecutionDateTime()
    {
        return i_jobExecutionDateTime;
    }

    /** Get the job identifier of the sub process.
     * @return The unique id that was assigned to the job by the Job Scheduler.
     */
    public String getSubJobId()
    {
        return i_subJobId;
    }

    /** Getthe Job Scheduler job identifier.
      * @return The unique id that was assigned to the job by the Job Scheduler.
      */
    public String getJsJobId()
    {
        return i_jsJobId;
    }

    /** Set the Job Scheduler job identifier.
     * @param p_jsJobId The unique id assigned to the job by the Job Scheduler.
     */
    public void setJsJobId(String p_jsJobId)
    {
        i_jsJobId = p_jsJobId;
    }

    /** Get the Job Scheduler identifier of the master process.
      * @return The unique id that was assigned to the job by the Job Scheduler.
      */
    public String getMasterJsJobId()
    {
        return i_masterJsJobId;
    }

    /** Set the Job Scheduler identifier of the master process.
     * @param p_masterJsJobId The unique id assigned to the master job by the Job Scheduler.
     */
    public void setMasterJsJobId(String p_masterJsJobId)
    {
        i_masterJsJobId = p_masterJsJobId;
    }

    /** Get the status of the batch.
     * @param p_batchJobStatus The batch job status.
     */
    public void setBatchJobStatus(String p_batchJobStatus)
    {
        i_batchJobStatus = p_batchJobStatus;
    }

    /** Get the type of the batch.
     * @param p_batchType The batch job name.
     */
    public void setBatchType(String p_batchType)
    {
        i_batchType = p_batchType;
    }

    /** Get the identifier of the last customer processed.
     * @param p_lastProcessedCustId The save point.
     */
    public void setLastProcessedCustId(String p_lastProcessedCustId)
    {
        i_lastProcessedCustId = p_lastProcessedCustId;
    }
    
    /** Set the identifier of the first customer to be processed.
     * @param p_startCustId The start cust id.
     */
    public void setStartCustId(String p_startCustId)
    {
        i_startCustId = p_startCustId;
    }

    /**  Set the identifier of the last customer to be processed.
     * @param p_endCustId The end cust id of the batch job.
     */
    public void setEndCustId(String p_endCustId)
    {
        i_endCustId = p_endCustId;
    }

    /** Get the first piece of extra data.
     * @param p_extraData1 The first line of extra data.
     */
    public void setExtraData1(String p_extraData1)
    {
        i_extraData1 = p_extraData1;
    }

    /** Get the second piece of extra data.
     * @param p_extraData2 The second line of extra data.
     */
    public void setExtraData2(String p_extraData2)
    {
        i_extraData2 = p_extraData2;
    }

    /** Set the date/time the batch was started.
     * @param p_jobExecutionDateTime The time stamp when the batch was launched.
     */
    public void setExecutionDateTime(String p_jobExecutionDateTime)
    {
        i_jobExecutionDateTime = p_jobExecutionDateTime;
    }
    
    /** Get the operator that submitted the job.
     * @param p_opId The operator id.
     */
    public void setOpId(String p_opId)
    {
        i_opId = p_opId;
    }

   /** Set the job identifier of the sub process.
    * 
    * @param p_subJobId Job identifier.
    */ 
    public void setSubJobId(String p_subJobId)
    {
        i_subJobId = p_subJobId;
    }
}
