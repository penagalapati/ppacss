////////////////////////////////////////////////////////////////////////////////
//      PPAS IPR ID     :       9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SyfgSystemConfigSqlService.Java
//      DATE            :       14-September-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1621/6878
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Service to return the system configuration data
//                              from the database as a SyfgSystemConfigData
//                              object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 10-Dec-02 | Erik Clayton  | Totally rewritten to be    | PpaLon#1725/7281
//           |               | able to handle any config  | PRD_PPAK00_GEN_CA413
//           |               | without adding new code.   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to return the system configuration data from the database as a
 * SyfgSystemConfigData object.
 */
public class SyfgSystemConfigSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SyfgSystemConfigSqlService";

    //------------------------------------------------------------------------
    // Constructors and Finalizers
    //------------------------------------------------------------------------
    /**
     * Creates a new system configuration sql service.
     * 
     * @param p_request The request to process.
     */
    public SyfgSystemConfigSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10100, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_read = "read";
    /**
     * Read the system configuration from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return System Configuration data
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public SyfgSystemConfigData read(PpasRequest    p_request,
                                     JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement        l_statement;
        JdbcResultSet        l_result;
        SqlString            l_sql;
        SyfgSystemConfigData l_data = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_read);
        }

        l_sql =   new SqlString(200, 0, "SELECT * FROM syfg_system_config");

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_read, 11100, this, p_request);

        l_result = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_read, 11200, this, p_request, l_sql);

        // Create the System Configuration data object.
        l_data = new SyfgSystemConfigData(p_request);

        // If there's a row we'll use the value...
        while (l_result.next(11300))
        {
            l_data.set(l_result.getTrimString(11400, "syfg_destination"),
                       l_result.getTrimString(11401, "syfg_parameter_name"),
                       l_result.getTrimString(11402, "syfg_parameter_value"));
        }

        l_result.close(11500);

        l_statement.close(C_CLASS_NAME, C_METHOD_read, 11600, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11700, this,
                "Leaving " + C_METHOD_read);
        }

        return l_data;
    }
}
