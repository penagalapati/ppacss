////////////////////////////////////////////////////////////////////////////////
//                             SEMA
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      ValaValidLanguageDataSet.Java
//      DATE            :      18-Sep-2001
//      AUTHOR          :      Erik Clayton
//
//      DESCRIPTION     :      A set (collection) of vala data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group 2001
//
// The copyright in this work belongs to Sema Group plc. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a vala data set which can be read from
 * the database.
 */
public class ValaValidLanguageDataSet extends DataSetObject implements Serializable
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "ValaValidLanguageDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Array of valid languages. */
    private ValaValidLanguageData[]     i_valaValidLanguageArr;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of vala valid language data.
     * 
     * @param p_request The request to process.
     * @param p_valaValidLanguageArr A set of valid language data objects.
     */
    public ValaValidLanguageDataSet(
        PpasRequest             p_request,
        ValaValidLanguageData[] p_valaValidLanguageArr)
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10110, this,
                 "Constructing ValaValidLanguageDataSet(...)");
        }

        i_valaValidLanguageArr   = p_valaValidLanguageArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10150, this,
                 "Constructed ValaValidLanguageDataSet(...)");
        }
    } // End of public constructor
      //         ValaValidLanguageDataSet


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the array of all language data objects held in this object.
     *  @return Array of all language data objects held in this object.
     */
    public ValaValidLanguageData []  getAll()
    {
        return (i_valaValidLanguageArr);
    }

    /**
     * Returns the valid language object for the supplied valid language code.
     * 
     * @param p_valaCode A valid language code.
     * @return Language data for a specified language code.
     */
    public ValaValidLanguageData getWithCode(String p_valaCode)
    {
        int                    l_loop;
        ValaValidLanguageData  l_vala = null;

        if (i_valaValidLanguageArr != null)
        {
            for(l_loop = 0; l_loop < i_valaValidLanguageArr.length; l_loop++)
            {
                if (i_valaValidLanguageArr[l_loop].getCode().equals(p_valaCode))
                {
                    l_vala = i_valaValidLanguageArr[l_loop];
                    break;
                }
            }
        }

        return(l_vala);

    } // End of public method getWithCode(String)

    /**
     * Returns the valid language object for the supplied valid number language code.
     * 
     * @param p_valaNumberCode The number used for playing announcements.
     * @return Language data for a specified language number.
     */
    public ValaValidLanguageData getWithLanguageNumber(int p_valaNumberCode)
    {
        int                    l_loop;
        ValaValidLanguageData  l_vala = null;

        if (i_valaValidLanguageArr != null)
        {
            for(l_loop = 0; l_loop < i_valaValidLanguageArr.length; l_loop++)
            {
                if (i_valaValidLanguageArr[l_loop].getValaAnnouncementLanguageNumber() == p_valaNumberCode)
                {
                    l_vala = i_valaValidLanguageArr[l_loop];
                    break;
                }
            }
        }

        return(l_vala);

    } // End of public method getAvailable(String)

    /**
     * Returns an array of this valid language data set, but with deleted
     * languages omitted.
     * 
     * @return Array of all available (non-deleted) languages.
     */
    public ValaValidLanguageData[] getAvailableArray()
    {
        ValaValidLanguageData  l_availableValaValidLanguagesArr[];
        Vector                 l_availableV = new Vector();
        int                    l_loop;
        ValaValidLanguageData  l_valaValidLanguagesEmptyArr[] =
                new ValaValidLanguageData[0];

        for(l_loop = 0; l_loop < i_valaValidLanguageArr.length; l_loop++)
        {
            if (!i_valaValidLanguageArr[l_loop].isDeleted())
            {
                // Language not deleted, add to temporary vector.
                l_availableV.addElement(i_valaValidLanguageArr[l_loop]);
            }
        }

        l_availableValaValidLanguagesArr =
                (ValaValidLanguageData[])l_availableV.toArray(l_valaValidLanguagesEmptyArr);

        return(l_availableValaValidLanguagesArr);

    } // End of public method getAvailableArray()
    
    /**
     * Returns an array of Strings representing the valid language codes, but with deleted
     * languages omitted.
     * 
     * @return Array of all available (non-deleted) language codes.
     */
    public String[] getAvailableCodesArray()
    {
        ArrayList              l_tempList = new ArrayList();
        int                    l_loop;
        int                    l_count = 0;
        String[]  l_valaValidLanguagesEmptyArr = new String[0];

        for(l_loop = 0; l_loop < i_valaValidLanguageArr.length; l_loop++)
        {
            if (!i_valaValidLanguageArr[l_loop].isDeleted())
            {
                // Language not deleted, add to temporary ArrayList.
                l_tempList.add(new String(i_valaValidLanguageArr[l_loop].getCode()));
                l_count++;
            }
        }

        return (String[])l_tempList.toArray(l_valaValidLanguagesEmptyArr);

    } // End of public method getAvailableCodesArray()


    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_valaValidLanguageArr, p_sb);
    }
}

