////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryOrder.java
//      DATE            :       30-Jan-2007
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Mechanism to get a comparator for Call History.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.io.Serializable;
import java.util.Comparator;

/** Mechanism to get a comparator for Call History. */
public class CallHistoryOrder
{
    /** Array of Comparators that will define the sort order. */
    private static Comparator[] C_SORT = new Comparator[]
                                        {new CallHistoryOrder.DateDescendingSort(),
                                         new CallHistoryOrder.DateAscendingSort(),
                                         new CallHistoryOrder.OtherPartyNumberDescendingSort(),
                                         new CallHistoryOrder.OtherPartyNumberAscendingSort()};
    
    /** Defining sort order on the Call History Data's starting date. */
    public static final String  C_SORT_BY_DATE                     = "date";
    
    /** Defining sort order on the Call History Data's b-number. */
    public static final String  C_SORT_BY_OTHER_PARTY_NO           = "otherPartyNo";    

    /** Get comparator for sorting call history data records.
     * 
     * @param p_sortField sort by call date if equal to "date", else sort by other party number if equals
     * to "otherPartyNo".
     * @param p_descendSort use descending if true, else ascending.
     * @return Comparator for sorting records.
     */
    public Comparator getComparator(String p_sortField, boolean p_descendSort)
    {
        return C_SORT[(p_sortField.equals(C_SORT_BY_DATE) ? 0 : 2) + (p_descendSort ? 0 : 1)];
    }
    
    //------------------------------------------------------------------------
    // Inner classes for comparators to the Call History Data set.
    //------------------------------------------------------------------------

    /** Comparator to sort Call History Data on descending dates. */
    private static class DateDescendingSort implements Comparator, Serializable 
    {
        /** Standard <code>compare</code> method using Start date to define the order.
         * @param p_this Call History Data object to compare.
         * @param p_that Call History Data object to compare.
         * @return Standard -1, 0 or 1.
         */
        public int compare(Object p_this, Object p_that)
        {
            CallHistoryData l_this = (CallHistoryData)p_this;
            CallHistoryData l_that = (CallHistoryData)p_that;
            
            int l_result = l_that.getCallStartDateTime().compareTo(l_this.getCallStartDateTime());
            
            if (l_result == 0 )
            {                
                l_result = l_this.getOtherPartyNumber().compareTo(l_that.getOtherPartyNumber());
            }
            
            if (l_result == 0 )
            {                
                l_result = l_this.getMsisdn().toString().compareTo(l_that.getMsisdn().toString());
            }
            
            return l_result;
        }
    }

    /** Comparator to sort Call History Data on ascending dates. */
    private static class DateAscendingSort implements Comparator, Serializable 
    {
        /** Standard <code>compare</code> method using Start date to define the order.
         * @param p_this Call History Data object to compare.
         * @param p_that Call History Data object to compare.
         * @return Standard -1, 0 or 1.
         */
        public int compare(Object p_this, Object p_that)
        {
            CallHistoryData l_this = (CallHistoryData)p_this;
            CallHistoryData l_that = (CallHistoryData)p_that;
            
            int l_result = l_this.getCallStartDateTime().compareTo(l_that.getCallStartDateTime());
            
            if (l_result == 0 )
            {                
                l_result = l_this.getOtherPartyNumber().compareTo(l_that.getOtherPartyNumber());
            }
            
            if (l_result == 0 )
            {                
                l_result = l_this.getMsisdn().toString().compareTo(l_that.getMsisdn().toString());
            }
            
            return l_result;
        }
    }

    /** Comparator to sort Call History Data on descending BNumber. */
    private static class OtherPartyNumberDescendingSort implements Comparator, Serializable
    {
        /** Standard <code>compare</code> method using BNumber to define the order.
         * @param p_this Call History Data object to compare.
         * @param p_that Call History Data object to compare.
         * @return Standard -1, 0 or 1.
         */
        public int compare(Object p_this, Object p_that)
        {
            CallHistoryData l_this = (CallHistoryData)p_this;
            CallHistoryData l_that = (CallHistoryData)p_that;
            
            return compareBNumber(l_that.getOtherPartyNumber(), 
                                  l_this.getOtherPartyNumber(),
                                  l_that,
                                  l_this);
        }
    }

    /** Comparator to sort Call History Data on descending BNumber. */
    private static class OtherPartyNumberAscendingSort implements Comparator, Serializable
    {
        /** Standard <code>compare</code> method using BNumber to define the order.
         * @param p_this Call History Data object to compare.
         * @param p_that Call History Data object to compare.
         * @return Standard -1, 0 or 1.
         */
        public int compare(Object p_this, Object p_that)
        {
            CallHistoryData l_this = (CallHistoryData)p_this;
            CallHistoryData l_that = (CallHistoryData)p_that;
            
            return compareBNumber(l_this.getOtherPartyNumber(), 
                                  l_that.getOtherPartyNumber(),
                                  l_that,
                                  l_this);
        }
    }
    /** Compare based on the B-Number. This method will compare numbers based on:
     * <ul>
     * <li> Length of number (shorter numbers are 'less than' longer numbers).
     * <li> Ascii value of the number ("012" is 'less than' "013").
     * <li> Date/time of call - this will only happen if the calls are to (or from) the same number.
     * <li> Ascii value of the MSISDN making the call - to prevent losing calls from subordinates.
     * </ul>
     * Note: Only the 'Other' number can be ascending/descending (other elements use a consistent direction)
     * so the other number is passed in explicitly (callers can reverse the order of paramaters.
     * 
     * @param p_thisBNumber  This 'other' number'
     * @param p_thatBNumber  Other 'Other' number to compare with.
     * @param p_this         This call history record.
     * @param p_that         Other call history record.
     * @return standard contract of compareTo
     */
    private static int compareBNumber(String          p_thisBNumber,
                                      String          p_thatBNumber,
                                      CallHistoryData p_that,
                                      CallHistoryData p_this)
    {
        // Both arguments must be != null
        
        int l_result;
        
        if (p_thisBNumber.length() == p_thatBNumber.length())
        {
            // MSISDN's are same length so need further investigation.
            l_result = p_thisBNumber.compareTo(p_thatBNumber);
        }
        else
        {
            l_result = p_thisBNumber.length() < p_thatBNumber.length() ? -1 : 1;
        }

        if (l_result == 0)
        {
            // Other numbers appear the same - use date/time.
            l_result = p_this.getCallStartDateTime().compareTo(p_that.getCallStartDateTime());
        }
        
        if (l_result == 0)
        {
            // Two calls at same date/time going to (or from) the same number - so check direction.
            l_result = p_this.getTrafficCase().compareTo(p_that.getTrafficCase());
        }
        
        if (l_result == 0)
        {
            // Two calls at same date/time going to (or from) the same number (same way) - so check MSISDN.
            l_result = p_this.getMsisdn().toString().compareTo(p_that.getMsisdn().toString());
        }
        
        return l_result;
    }
}
