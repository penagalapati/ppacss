////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscCodeSqlService.Java
//      DATE            :       23-August-2001
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#964/4107
//                              PRD_PPAK00_GEN_CA_288
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Service to read the SRVA_MISC_CODE table and
//                              return a MiscCodeDataSet object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
//           |               |                            |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.HashSet;
import java.util.Set;

import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeDataSet;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeNumberData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** Service to read the SRVA_MISC_CODE table and return a MiscCodeDataSet object. */
public class MiscCodeSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscCodeSqlService";

    //-------------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------------        

    /** Set of code types (tables) that will have all numeric data. */
    private static final Set C_NUMERIC_TYPE = new HashSet();
    
    static
    {
        C_NUMERIC_TYPE.add("CLS");   // Service Class
        C_NUMERIC_TYPE.add("CTR");   // Charged Event Counter Type
    }

    //------------------------------------------------------------------------
    // Private data
    //------------------------------------------------------------------------
    /** The Logger object to be used in logging any exceptions arising from the
     *  use of this service.
     */
    private Logger i_logger = null;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new misc code sql service.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     */
    public MiscCodeSqlService(PpasRequest p_request,
                              Logger      p_logger)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_logger = p_logger;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readMiscCodes = "readMiscCodes";
    /**
     * Read the misc codes from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of misc code data objects.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public MiscCodeDataSet readMiscCodes(PpasRequest            p_request,
                                         JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement   l_statement;
        JdbcResultSet   l_resultSet;
        SqlString       l_sql;
        MiscCodeData    l_miscCodeData;
        MiscCodeDataSet l_miscCodeDataSet;
        int             l_srva;
        int             l_sloc;
        Market          l_market;
        String          l_codeType;
        String          l_code;
        String          l_description;
        String          l_deleteFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_readMiscCodes);
        }

        l_sql = new SqlString(500, 0, 
                              "SELECT srva," +
                              "sloc," +
                              "misc_table," +
                              "misc_code," +
                              "misc_desc," +
                              "del_flag " +
                              "FROM srva_misc_codes ");

        l_miscCodeDataSet = new MiscCodeDataSet();

        l_statement = p_connection.createStatement(
                                       C_CLASS_NAME, C_METHOD_readMiscCodes, 11010, this, p_request);

        l_resultSet = l_statement.executeQuery(
                                       C_CLASS_NAME, C_METHOD_readMiscCodes, 11020, this, p_request, l_sql);

        while (l_resultSet.next(11030))
        {
            l_srva   = l_resultSet.getInt(11040, "srva");
            l_sloc   = l_resultSet.getInt(11050, "sloc");
            l_market = new Market(l_srva, l_sloc);

            l_codeType = l_resultSet.getTrimString(11060, "misc_table");
            l_code = l_resultSet.getTrimString(11070, "misc_code");
            l_description = l_resultSet.getTrimString(11080, "misc_desc");
            l_deleteFlag = l_resultSet.getTrimString(11090, "del_flag");
            
            if(C_NUMERIC_TYPE.contains(l_codeType))
            {
                l_miscCodeData = new MiscCodeNumberData(
                                             l_market, l_codeType, l_code, l_description, l_deleteFlag);
            }
            else
            {
                l_miscCodeData = new MiscCodeData(l_market, l_codeType, l_code, l_description, l_deleteFlag);
            }    

            // ...and stuff it in the misc code data set.
            l_miscCodeDataSet.addMiscCode(l_miscCodeData);
        }

        l_resultSet.close(11100);

        l_statement.close( C_CLASS_NAME, C_METHOD_readMiscCodes, 11110, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11120, this,
                "Leaving " + C_METHOD_readMiscCodes);
        }

        return(l_miscCodeDataSet);
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Update the SRVA_MISC_CODES database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_srva The service area for this market.
     * @param p_sloc The service location for this market.
     * @param p_code The miscellaneous code to be updated.
     * @param p_description Description of the miscellaneous code.
     * @param p_opid The username of the operator who caused this update.
     * @param p_type The type of miscellaneous code. Maps to the misc_table database column.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void update(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_srva,
                       String                 p_sloc,
                       String                 p_code,
                       String                 p_description,
                       String                 p_opid,
                       String                 p_type)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, 
                C_CLASS_NAME, 12000, this,
                "Entered " + C_METHOD_update);
        }

        l_sql = new String("UPDATE srva_misc_codes " +
                           "SET misc_desc = {0}, " + 
                           "    opid = {1}, " + 
                           "    gen_ymdhms = {2}, " + 
                           "    del_flag = {3} " +
                           "WHERE srva = {4} " + 
                           "AND   sloc = {5} " + 
                           "AND   misc_code = {6} " + 
                           "AND   misc_table = {7}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setStringParam  (0, p_description);
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setCharParam    (3, ' ');
        l_sqlString.setStringParam  (4, p_srva);
        l_sqlString.setStringParam  (5, p_sloc);
        l_sqlString.setStringParam  (6, p_code);
        l_sqlString.setStringParam  (7, p_type);


        try
        {    
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME, C_METHOD_update, 12010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME, C_METHOD_update, 12020, this, p_request, l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 12030, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    12040, 
                    this,
                    "Database error: unable to update row in srva_misc_codes table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_update,
                                               12050,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                12060, 
                this,
                "Leaving " + C_METHOD_update);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    /**
     * Inserts a miscellaneous code into the SRVA_MISC_CODES database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_srva The service area for this market.
     * @param p_sloc The service location for this market.
     * @param p_code Miscellaneous code.
     * @param p_description Description of the miscellaneous code.
     * @param p_opid The username of the operator who caused this update.
     * @param p_type The type of miscellaneous code. Maps to the misc_table database column.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void insert(PpasRequest            p_request,
                       JdbcConnection         p_connection,
                       String                 p_srva,
                       String                 p_sloc,
                       String                 p_code,
                       String                 p_description,
                       String                 p_opid,
                       String                 p_type)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, 
                C_CLASS_NAME, 13000, this,
                "Entered " + C_METHOD_insert);
        }

        l_sql = new String("INSERT INTO srva_misc_codes " +
                               "(company, srva, sloc, misc_table, misc_code, " +      
                                "misc_desc, gen_ymdhms, opid, del_flag) " +
                           "VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setIntParam     (0, 0);
        l_sqlString.setStringParam  (1, p_srva);
        l_sqlString.setStringParam  (2, p_sloc);
        l_sqlString.setStringParam  (3, p_type);
        l_sqlString.setStringParam  (4, p_code);
        l_sqlString.setStringParam  (5, p_description);
        l_sqlString.setDateTimeParam(6, l_now);
        l_sqlString.setStringParam  (7, p_opid);
        l_sqlString.setCharParam    (8, ' ');

        try
        {    
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME, C_METHOD_insert, 13010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME, C_METHOD_insert, 13020, this, p_request, l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 13030, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    13040, 
                    this,
                    "Database error: unable to insert row into srva_misc_codes table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_insert,
                                               13050,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                13060, 
                this,
                "Leaving " + C_METHOD_insert);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Deletes a row from the SRVA_MISC_CODES database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_srva The service area for this market.
     * @param p_sloc The service location for this market.
     * @param p_code Miscellaneous code.
     * @param p_opid The username of the operator who caused this update.
     * @param p_type The type of miscellaneous code. Maps to the misc_table database column.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void delete(PpasRequest    p_request,
                       JdbcConnection p_connection,
                       String         p_srva,
                       String         p_sloc,
                       String         p_code,
                       String         p_opid,
                       String         p_type)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, 
                C_CLASS_NAME, 14000, this,
                "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE srva_misc_codes " +
                           "SET del_flag = {0}, " + 
                           "    opid = {1}, " + 
                           "    gen_ymdhms = {2} " + 
                           "WHERE srva = {3} " + 
                           "AND   sloc = {4} " + 
                           "AND   misc_code = {5} " + 
                           "AND   misc_table = {6}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setCharParam    (0, '*');
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setStringParam  (3, p_srva);
        l_sqlString.setStringParam  (4, p_sloc);
        l_sqlString.setStringParam  (5, p_code);
        l_sqlString.setStringParam  (6, p_type);

        try
        {    
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete, 14010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME, C_METHOD_delete, 14020, this, p_request, l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 14030, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    14040, 
                    this,
                    "Database error: unable to delete row from srva_misc_codes table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_delete,
                                               14050,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                14060, 
                this,
                "Leaving " + C_METHOD_delete);
        }
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a row as available in the SRVA_MISC_CODES database table.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @param p_srva The service area for this market.
     * @param p_sloc The service location for this market.
     * @param p_code Miscellaneous code.
     * @param p_opid The username of the operator who caused this update.
     * @param p_type The type of miscellaneous code. Maps to the misc_table database column.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest    p_request,
                                JdbcConnection p_connection,
                                String         p_srva,
                                String         p_sloc,
                                String         p_code,
                                String         p_opid,
                                String         p_type)
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        PpasDateTime     l_now;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        l_now = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, 
                C_CLASS_NAME, 15000, this,
                "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = new String("UPDATE srva_misc_codes " +
                           "SET del_flag = {0}, " + 
                           "    opid = {1}, " + 
                           "    gen_ymdhms = {2} " + 
                           "WHERE srva = {3} " + 
                           "AND   sloc = {4} " + 
                           "AND   misc_code = {5} " + 
                           "AND   misc_table = {6}");

        l_sqlString = new SqlString(500, 0, l_sql);

        l_sqlString.setCharParam    (0, ' ');
        l_sqlString.setStringParam  (1, p_opid);
        l_sqlString.setDateTimeParam(2, l_now);
        l_sqlString.setStringParam  (3, p_srva);
        l_sqlString.setStringParam  (4, p_sloc);
        l_sqlString.setStringParam  (5, p_code);
        l_sqlString.setStringParam  (6, p_type);

        try
        {    
            l_statement = p_connection.createStatement(
                               C_CLASS_NAME, C_METHOD_markAsAvailable, 15010, this, p_request);

            l_rowCount = l_statement.executeUpdate(
                               C_CLASS_NAME, C_METHOD_markAsAvailable, 15020, this, p_request, l_sqlString);

        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(
                    C_CLASS_NAME, C_METHOD_markAsAvailable, 15030, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 
                    15040, 
                    this,
                    "Database error: unable to mark row as available in srva_misc_codes table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               15050,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            if (i_logger != null)
            {
                i_logger.logMessage (l_ppasSqlE);
            }
            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 
                15060, 
                this,
                "Leaving " + C_METHOD_markAsAvailable);
        }
    }
}
