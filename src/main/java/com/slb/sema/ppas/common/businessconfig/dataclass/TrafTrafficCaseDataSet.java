////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafTrafficCaseDataSet.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8229, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Traffic case data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

public class TrafTrafficCaseDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TrafTrafficCaseDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of tele teleservice records. That is, the data of this DataSet object. */
    private TrafTrafficCaseData[] i_trafTrafficCaseDataObjects;
    
    /** Array of available tele teleservice data records. That is, the data of this DataSet object. */    
    private TrafTrafficCaseData[] i_availableTrafTrafficCaseDataObjects;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Tele Teleservice data.
     * @param p_request The request to process.
     * @param p_TrafTrafficCaseDataObjects Array holding the tele teleservice data records to be stored
     *                              by this object.
     */
    public TrafTrafficCaseDataSet(PpasRequest             p_request,
                                  TrafTrafficCaseData []  p_TrafTrafficCaseDataObjects)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_trafTrafficCaseDataObjects = p_TrafTrafficCaseDataObjects;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns Trafic Case data associated with a specified case .
     * @param p_case The traffic case.
     * 
     * @return TrafTrafficCaseData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public TrafTrafficCaseData getRecord(int p_case )
    {
        TrafTrafficCaseData l_teleTeleserviceRecord = null;
        
            for (int i = 0; i < i_trafTrafficCaseDataObjects.length; i++)
            {
                if ( i_trafTrafficCaseDataObjects[i].getTrafficCase() == p_case )                
                {
                    l_teleTeleserviceRecord = i_trafTrafficCaseDataObjects[i];
                    break;
                }
            }

        return l_teleTeleserviceRecord;
    }

    
    
    /**
     * Returns available (that is, not marked as deleted) Traffic Case records associated 
     * with a specified case
     * @param p_case The traffic case.
     * 
     * @return TrafTrafficCaseData record from this DataSet with the given case. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public TrafTrafficCaseData getAvailableRecord (int p_case)
    {
        TrafTrafficCaseData l_teleTeleserviceRecord = getRecord(p_case);
        
        if (l_teleTeleserviceRecord != null && l_teleTeleserviceRecord.isDeleted())
        {
            l_teleTeleserviceRecord = null;
        }

        return l_teleTeleserviceRecord;
    }


    
    /** Returns array of all records in this data set object.
     * 
     * @return All TrafTrafficCaseData objects.
     */
    public TrafTrafficCaseData[] getAllArray()
    {
        return i_trafTrafficCaseDataObjects;
    }

    
    
    /**
     * Get a data set containing the all available TrafTrafficCaseData records (i.e.
     * those not marked as deleted.
     * @return Data set containing available TrafTrafficCaseData records in the
     *         cache (i.e. those not marked as deleted).
     */
    public TrafTrafficCaseData[] getAvailableArray()
    {
        Vector                l_availableV = new Vector();
        TrafTrafficCaseData[] l_teleTeleserviceEmptyArray = new TrafTrafficCaseData[0];

        if (i_availableTrafTrafficCaseDataObjects == null)
        {
            for (int i = 0; i < i_trafTrafficCaseDataObjects.length; i++)
            {
                if (!i_trafTrafficCaseDataObjects[i].isDeleted())
                {
                    l_availableV.addElement(i_trafTrafficCaseDataObjects[i]);
                }
            }
    
            i_availableTrafTrafficCaseDataObjects =
                  (TrafTrafficCaseData[])l_availableV.toArray(l_teleTeleserviceEmptyArray);
        }

        return i_availableTrafTrafficCaseDataObjects;
    }

    
    
    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_availableTrafTrafficCaseDataObjects, p_sb);
    }


    
    public String toString()
    {
        final String L_NL = "\n";
        StringBuffer l_tmp = new StringBuffer(200);
        
        l_tmp.append("AvailableTrafficCaseDataObjects: ");
        if ( i_availableTrafTrafficCaseDataObjects != null )
        {
            for ( int i = 0; i < i_availableTrafTrafficCaseDataObjects.length; i++ )
            {
                l_tmp.append(i_availableTrafTrafficCaseDataObjects[i].toString());
                l_tmp.append(L_NL);
            }
        }
        else
        {
            l_tmp.append("is NULL");
        }
        
        l_tmp.append("TrafficCaseDataObjects: ");
        if ( i_trafTrafficCaseDataObjects != null )
        {
            for ( int i = 0; i < i_trafTrafficCaseDataObjects.length; i++ )
            {
                l_tmp.append(i_trafTrafficCaseDataObjects[i].toString());
                l_tmp.append(L_NL);
            }
        }
        else
        {
            l_tmp.append("is NULL");
        }
        
        return l_tmp.toString();
    }

}
