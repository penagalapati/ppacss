////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafTrafficCaseData.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8229, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Traffic case data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;


import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

public class TrafTrafficCaseData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TrafTrafficCaseData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The account group ID. */
    private int     i_trafficCase;
    
    /** The traffic description. */
    private String  i_trafficDescription;

    /** The traffic short description. */
    private String  i_trafficShortDescription;
    
    /** Flag indicating if this adjustment code is deleted. */
    private boolean i_deleteFlag = false;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates an Traffic Case Data object.
     * 
     * @param p_request The request to process.
     * @param p_trafficCase The traffic case.
     * @param p_deleteFlag The deleted flag.
     * @param p_opid    Operator that last changed this record.
     * @param p_genYmdHms Date/time of last change to this record.
     * @param p_trafficDescription The account group description.
     */
    public TrafTrafficCaseData(PpasRequest  p_request,
                               int          p_trafficCase,
                               String       p_trafficDescription,
                               String       p_trafficShortDescription,
                               char         p_deleteFlag,
                               String       p_opid,
                               PpasDateTime p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_trafficCase             = p_trafficCase;
        i_trafficDescription      = p_trafficDescription;
        i_trafficShortDescription = p_trafficShortDescription;
        i_deleteFlag              = (p_deleteFlag == '*');
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /**
     * Return the traffic case.
     * @return the account group id
     */
    public int getTrafficCase()
    {
        return i_trafficCase;
    }

    
    
    /**
     * Return the traffic description.
     * @return the traffic description
     */
    public String getTrafficDescription()
    {
        return i_trafficDescription;
    }


    
    /**
     * Return the traffic short description.
     * @return the traffic short description
     */
    public String getTrafficShortDescription()
    {
        return i_trafficShortDescription;
    }

    
    
    /** Returns true if this traffic Case has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_deleteFlag;
    }

    
    
    public String toString()
    {
        final String L_NL = "\n";
        StringBuffer l_tmp = new StringBuffer(200);
        l_tmp.append("TrafficCase       = " + i_trafficCase);
        l_tmp.append(L_NL);
        l_tmp.append("Description       = " + i_trafficDescription);
        l_tmp.append(L_NL);
        l_tmp.append("Short Description = " + i_trafficShortDescription);
        l_tmp.append(L_NL);
        l_tmp.append("DeleteFlag        = " + i_deleteFlag );
        l_tmp.append(L_NL);
        
        return l_tmp.toString();
    }

}
