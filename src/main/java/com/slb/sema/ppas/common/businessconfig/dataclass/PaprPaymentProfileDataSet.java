////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PaprPaymentProfileDataSet.Java
//      DATE            :       14-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of payment profile data (i.e. a row
//                              from the papr_payment_profile table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 29/09/03 | MAGray     | Add method to return instance   | PpacLon#56/531
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of payment profiles data, each element of
 * the set representing a record from the papr_payment_profile table.
 */
public class PaprPaymentProfileDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Initial size of currency formats vector. */
    public static final int C_CURRENCY_V_INITIAL_SIZE = 20;
    
    /** Increment size of currency formats vector. */
    public static final int C_CURRENCY_V_GROW_SIZE = 10;

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PaprPaymentProfileDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of payment profile data records. */
    private PaprPaymentProfileData []     i_paprPaymentProfileArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of payment profile data records.
     * 
     * @param p_request The request to process.
     * @param p_paprPaymentProfileArr A set of Payment Profile data objects.
     */
    public PaprPaymentProfileDataSet(
        PpasRequest                 p_request,
        PaprPaymentProfileData []   p_paprPaymentProfileArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 52510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_paprPaymentProfileArr = p_paprPaymentProfileArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 52590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Get a data set containing all non-deleted payment profile records.
     * @return Array containing all non-deleted payment profiles records.
     */
    public PaprPaymentProfileData [] getActiveArray ()
    {
        PaprPaymentProfileData l_data = null;
        PaprPaymentProfileData l_paprEmptyArr[] = new PaprPaymentProfileData[0];
        Vector                 l_availablePaprPaymentProfilesV = new Vector(C_CURRENCY_V_INITIAL_SIZE,
                                                                            C_CURRENCY_V_GROW_SIZE);
        int                    l_len = i_paprPaymentProfileArr.length;
        for(int i = 0; i < l_len; i++)
        {
            l_data = i_paprPaymentProfileArr[i];
            
            if (!l_data.isDeleted())
            {
                l_availablePaprPaymentProfilesV.addElement(l_data);
            }
        }

        return (PaprPaymentProfileData []) l_availablePaprPaymentProfilesV.toArray(l_paprEmptyArr);
        
    } // End of public method getActiveArray
    
    /** 
     * Returns data associated with a specified key.
     *
     * @param p_paymentProfile Payment profile code that defines the key of the data to be obtained.
     * 
     * @return Details associated with the key or <code>null</code> if not found.
     */    
    public PaprPaymentProfileData getPaymentProfileData(String p_paymentProfile)
    {
        PaprPaymentProfileData l_data = null;
        
        for(int l_i = 0; l_i < i_paprPaymentProfileArr.length && l_data == null; l_i++)
        {
            if (i_paprPaymentProfileArr[l_i].getPaymentProfile().equals(p_paymentProfile))
            {
                l_data  = i_paprPaymentProfileArr[l_i];
            }
        }

        return l_data;
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_paprPaymentProfileArr, p_sb);
    }
}

