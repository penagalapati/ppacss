////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NublNumberBlockData.Java
//      DATE            :       15-Oct-2003
//      AUTHOR          :       Oualid Gharach
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2003
//
//      DESCRIPTION     :       A record of number block information
//                              (corresponding to a single row of the
//                              nubl_number_block table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of number block information (i.e.
 * a single row of the nubl_number_block table).
 */
public class NublNumberBlockData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "NublNumberBlockData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The market. */
    private  Market            i_market = null;

    /** The Number Block Start Number. */
    private  Msisdn            i_nublStartNumber = null;
    
    /** The Number Block Start Number as a String. */
    private  String            i_nublStartNumberString = null;

    /** The Number Block End Number. */
    private  Msisdn            i_nublEndNumber = null;
    
    /** The Number Block End Number as a String. */
    private  String            i_nublEndNumberString = null;

    /** The sdp Id. */
    private  String            i_sdpId = "";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates nubl data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this range of numbers applies.
     * @param p_nublStartNumber Start number of number range.
     * @param p_nublEndNumber End number of number range.
     * @param p_sdpId   SDP identifier for these mobile numbers.
     * @param p_opid    Operator that last changed this data.
     * @param p_genYmdHms Date/time this data was last changed.
     */
    public NublNumberBlockData(
        PpasRequest            p_request,
        Market                 p_market,
        Msisdn                 p_nublStartNumber,
        Msisdn                 p_nublEndNumber,
        String                 p_sdpId,
        String                 p_opid,
        PpasDateTime           p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market          = p_market;
        i_nublStartNumber = p_nublStartNumber;
        i_nublEndNumber   = p_nublEndNumber;
        i_sdpId           = p_sdpId;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor
    
    /**
     * Creates nubl data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this range of numbers applies.
     * @param p_nublStartNumber Start number of number range.
     * @param p_nublEndNumber End number of number range.
     * @param p_sdpId   SDP identifier for these mobile numbers.
     * @param p_opid    Operator that last changed this data.
     * @param p_genYmdHms Date/time this data was last changed.
     */
    public NublNumberBlockData(
        PpasRequest            p_request,
        Market                 p_market,
        String                 p_nublStartNumber,
        String                 p_nublEndNumber,
        String                 p_sdpId,
        String                 p_opid,
        PpasDateTime           p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10093, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_market                = p_market;
        i_nublStartNumberString = p_nublStartNumber;
        i_nublEndNumberString   = p_nublEndNumber;
        i_sdpId                 = p_sdpId;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10097, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the market.
     * 
     * @return The market associated with this range of mobile numbers.
     */
    public Market getMarket ()
    {
        return (i_market);
    }
    
    /**
     * Get the first number in the block.
     * 
     * @return First mobile number in the block.
     */
    public Msisdn getNublStartNumber ()
    {
        return (i_nublStartNumber);
    }
    
    /**
     * Get the first number in the block as a String.
     * 
     * @return First mobile number in the block.
     */
    public String getNublStartNumberString ()
    {
        return (i_nublStartNumberString);
    }
    

    /**
     * Get the last number in the block.
     * 
     * @return Last mobile number in the block.
     */
    public Msisdn getNublEndNumber ()
    {
        return (i_nublEndNumber);
    }
    
    /**
     * Get the last number in the block as a String.
     * 
     * @return Last mobile number in the block.
     */
    public String getNublEndNumberString ()
    {
        return (i_nublEndNumberString);
    }

    /**
     * Get the SDP associated with this range of mobile numbers.
     * 
     * @return Identifier of the SDP that holds this range of mobile numbers.
     */
    public String getSdpId ()
    {
        return (i_sdpId);
    }
} // End of public class NublNumberBlockData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////