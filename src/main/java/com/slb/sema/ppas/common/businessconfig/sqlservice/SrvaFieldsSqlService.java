////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaFieldsSqlService.Java
//      DATE            :       13-Apr-2004
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Implements a service that performs the SQL
//                              required by the Comment Description config cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////


package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaFieldsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaFieldsDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Implements a service that performs the SQL required to populate the Comments business config cache.
 */
public class SrvaFieldsSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaFieldsSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Sql Service for populating the Comments business configuration cache.
     * 
     * @param p_request The request to process.
     */
    public SrvaFieldsSqlService (PpasRequest  p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all Comment data (including ones marked as deleted).
     * @param p_request The request to process.
     * @param p_connection  Database connection.
     * @return Data set containing all configured Comments from the srva_fields table.
     * @throws PpasSqlException If the data cannot be read.
     */
    public SrvaFieldsDataSet readAll (PpasRequest p_request, JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement          l_statement;
        SqlString              l_sql;
        Vector                 l_allCommentsV = new Vector (50, 50);
        SrvaFieldsData         l_emptyArray[] = new SrvaFieldsData[0];
        JdbcResultSet          l_results;
        SrvaFieldsDataSet      l_allCommentsDataSet = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered " +  C_METHOD_readAll);
        }

        l_sql =   new SqlString(500, 0, "SELECT " +
                                        "srva, " +
                                        "sloc, " +
                                        "field_id, " +
                                        "code_description, " +
                                        "gen_ymdhms, " +
                                        "opid, " +
                                        "del_flag " +
                                        "FROM srva_fields " +
                                        "ORDER BY field_id, srva, sloc");

        // Create the statement object and excecute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10110, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request, l_sql);
        while ( l_results.next(31440) )
        {
            // NOTE - 'code_description' is deliberately got using getString NOT getTrimString.  
            l_allCommentsV.addElement(new SrvaFieldsData(
                                            p_request,
                                            new Market(p_request,
                                                       l_results.getInt(10130, "srva"),
                                                       l_results.getInt(10140, "sloc"),
                                                       null),
                                            l_results.getTrimString(10150, "field_id"),
                                            l_results.getString(10150, "code_description"),
                                            l_results.getDateTime(11520, "gen_ymdhms"),
                                            l_results.getTrimString(11530, "opid"),
                                            l_results.getChar(11540, "del_flag") == '*'));
        }

        l_results.close (31550);

        l_statement.close (C_CLASS_NAME, C_METHOD_readAll, 31560, this, p_request);

        l_allCommentsDataSet = new SrvaFieldsDataSet(
                                    p_request,
                                    (SrvaFieldsData [])l_allCommentsV.toArray(l_emptyArray));

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 31590, this,
                "Leaving " + C_METHOD_readAll);
        }

        return l_allCommentsDataSet;
    }
}