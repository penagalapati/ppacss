////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      PrplPromoPlanDataSet.Java
//      DATE            :      18-Sep-2001
//      AUTHOR          :      Erik Clayton
//
//      DESCRIPTION     :      A set (collection) of prpl data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//10/10/01  | R Isaacs   | Now display future dated        | CR#60/600
//          |            | promotion plans                 |
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group 2001
//
// The copyright in this work belongs to Sema Group plc. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a prpl data set which can be read from
 * the database.
 */
public class PrplPromoPlanDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrplPromoPlanDataSet";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Logger to be used to log exceptions generated within this class. */
    protected Logger   i_logger = null;

    /** Array of valid languages. */
    private PrplPromoPlanData[]     i_prplPromoPlanArr;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of prpl promotion plan data.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger object to allow routing of messages.
     * @param p_prplPromoPlanArr A set of promotion plan data objects.
     */
    public PrplPromoPlanDataSet(
        PpasRequest             p_request,
        Logger                  p_logger,
        PrplPromoPlanData[]     p_prplPromoPlanArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10110, this,
                 "Constructing PrplPromoPlanDataSet(...)");
        }

        i_logger           = p_logger;
        i_prplPromoPlanArr = p_prplPromoPlanArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10150, this,
                 "Constructed PrplPromoPlanDataSet(...)");
        }
    } // End of public constructor
      //         PrplPromoPlanDataSet


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns the promotion plan object for the supplied promo plan id.
     * 
     * @param p_prplCode Promotion plan code.
     * @return Prootion plan data for the specified code.
     */
    public PrplPromoPlanData getWithCode(String p_prplCode)
    {
        int                l_loop;
        PrplPromoPlanData  l_prpl = null;

        if (i_prplPromoPlanArr != null)
        {
            for(l_loop = 0; l_loop < i_prplPromoPlanArr.length; l_loop++)
            {
                if (i_prplPromoPlanArr[l_loop].getPromoPlan().equals(p_prplCode))
                {
                    l_prpl = i_prplPromoPlanArr[l_loop];
                    break;
                }
            }
        }

        return(l_prpl);

    } // End of public method getAvailable(String)

    /**
     * Returns an array of this valid language data set, but with deleted
     * languages omitted.
     * @return set of available promotion plan data.
     */
    public PrplPromoPlanData[] getAvailableArray ()
    {
        PrplPromoPlanData      l_availablePrplPromoPlansArr[];
        Vector                 l_availableV = new Vector();
        int                    l_loop;
        PrplPromoPlanData      l_prplPromoPlansEmptyArr[] =
                                            new PrplPromoPlanData[0];
        PpasDateTime           l_startDT;
        PpasDateTime           l_endDT;
        PpasDate               l_endDate;
        PpasDate               l_today;

        l_today = DatePatch.getDateToday ();

        for(l_loop = 0; l_loop < i_prplPromoPlanArr.length; l_loop++)
        {
            if (!i_prplPromoPlanArr[l_loop].isDeleted())
            {
                l_startDT   = i_prplPromoPlanArr[l_loop].getStartDateTime();

                l_endDT     = i_prplPromoPlanArr[l_loop].getEndDateTime();

                if (l_startDT != null)
                {
                    if (l_endDT != null)
                    {
                        l_endDate = l_endDT.getPpasDate();
                    }
                    else
                    {
                        l_endDate = null;
                    }
                    // CR#60/600 Remove start date restriction
                    if ((l_endDate == null) ||
                         (!l_endDate.isSet()) ||
                         (!l_endDate.before (l_today)))
                    {
                        // Plan not deleted, add to temporary vector.
                        l_availableV.addElement(i_prplPromoPlanArr[l_loop]);
                    }
                }
            }
        }

        l_availablePrplPromoPlansArr = (PrplPromoPlanData[])l_availableV.toArray(l_prplPromoPlansEmptyArr);

        return(l_availablePrplPromoPlansArr);

    } // End of public method getAvailableArray()
    
    /**
     * Returns an array of valid promotion plan codes, but with deleted
     * promotions omitted.
     * @return String array of available promotion plan codes
     */
    public String[] getAvailableCodes()
    {
        String[]               l_availablePrplPromoPlansArr;
        int                    l_loop;
        String[]               l_prplPromoPlansEmptyArr = new String[0];
        PpasDateTime           l_startDT;
        PpasDateTime           l_endDT;
        PpasDate               l_endDate;
        PpasDate               l_today;
        
        ArrayList              l_tempList = null;
        
        l_tempList = new ArrayList();

        l_today = DatePatch.getDateToday ();

        for(l_loop = 0; l_loop < i_prplPromoPlanArr.length; l_loop++)
        {
            if (!i_prplPromoPlanArr[l_loop].isDeleted())
            {
                l_startDT   = i_prplPromoPlanArr[l_loop].getStartDateTime();

                l_endDT     = i_prplPromoPlanArr[l_loop].getEndDateTime();

                if (l_startDT != null)
                {
                    if (l_endDT != null)
                    {
                        l_endDate = l_endDT.getPpasDate();
                    }
                    else
                    {
                        l_endDate = null;
                    }
                    
                    if ((l_endDate == null) ||
                         (!l_endDate.isSet()) ||
                         (!l_endDate.before (l_today)))
                    {
                        // Plan not deleted, add to temporary vector.
                        //l_availableV.addElement(i_prplPromoPlanArr[l_loop]);
                        l_tempList.add(i_prplPromoPlanArr[l_loop].getPromoPlan());
                    }
                }
            }
        }

        l_availablePrplPromoPlansArr = (String[])l_tempList.toArray(l_prplPromoPlansEmptyArr);

        return(l_availablePrplPromoPlansArr);

    } // End of public method getAvailableCodes()

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_prplPromoPlanArr, p_sb);
    }
}

