////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       VospVoucherSplitsDataSet.Java
//      DATE            :       20-Aug-2002
//      AUTHOR          :       Nick Fletcher
//
//      REFERENCE       :       PRD_PPAK00_GEN_CA_382
//                              PpaLon#1500/6266
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       A set of data, each element in the set defining
//                              a record of vosp_voucher_splits table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi Isaacs| Remove the getAllArray that     | CR#15/440
//          |            | takes srva and sloc as input    |
//          |            | parameters;                     |
//          |            | Remove the srva and sloc input  |
//          |            | parameters from the signature   |
//          |            | getAvailableArray method. Remove|
//          |            | checks for srva,sloc, against   |
//          |            | data in i_dataArr;              |
//          |            | Remove the p_type parameter from|
//          |            | getDivision method. Remove      |
//          |            | the checksfor p_type against    |
//          |            | data held in i_dataArr          |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that contains a set of an vosp voucher splits data, each element of
 * the set representing a record from the vosp_voucher_splits table.
 */
public class VospVoucherSplitsDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "VospVoucherSplitsDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** Array of data objects, each containing data from a row of the 
     *  vosp_voucher_splits table.
     */
    private VospVoucherSplitsData []     i_dataArr;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of vosp voucher splits data.
     * 
     * @param p_request The request to process.
     * @param p_dataArr A set of voucher split data objects.
     */
    public VospVoucherSplitsDataSet(
        PpasRequest                p_request,
        VospVoucherSplitsData []   p_dataArr)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 11510, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_dataArr = p_dataArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 11590, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get all data contained in this data set.
     *  @return Array containing all the data in this data set.
     */
    public VospVoucherSplitsData []  getAllArray()
    {
        return (i_dataArr);
    }

    /**
     * Get a data set containing the all active vosp voucher splits records. 
     * @return Data set containing all vosp voucher splits records
     */
    public VospVoucherSplitsData [] getAvailableArray ()
    {
        int                    l_loop;
        Vector                 l_availableV = new Vector (30, 20);
        VospVoucherSplitsData  l_vospArr[];
        VospVoucherSplitsData  l_vospEmptyArr[] = new VospVoucherSplitsData[0];
        PpasDate               l_today;
        PpasDate               l_startDate;
        PpasDate               l_endDate;

        l_today = DatePatch.getDateToday ();

        if (i_dataArr != null)
        {
            for (l_loop = 0;
                 l_loop < i_dataArr.length;
                 l_loop++)
            {
                
                l_startDate = i_dataArr[l_loop].getStartDate();
                l_endDate   = i_dataArr[l_loop].getEndDate();

                if (!l_startDate.after (l_today) &&
                     ((l_endDate == null) ||
                     (!l_endDate.isSet()) ||
                     (!l_endDate.before (l_today))))
                {
                    l_availableV.addElement (i_dataArr[l_loop]);
                }
            }
        }

        l_vospArr = (VospVoucherSplitsData[])
                         l_availableV.toArray (l_vospEmptyArr);

        return (l_vospArr);

    } // End of public method getAvailableArray


    /** Returns a single vosp voucher splits record (or 'division') that 
     *  matches the input criteria suppied by the parameters of this method.
     *  If no vosp voucher splits record could be found in this data set
     *  matching the input criteria, then <code>null</code> is returned.
     *
     *  @param p_date Date on which the division selected must be active.
     *  @param p_serviceClass The service class that the selected division 
     *                        must belong to.
     *  @param p_divisionId Identifier of the selected division (corresponds to
     *                      the vosp_card_group column of the vosp table). This
     *                      is either the card group if required division type
     *                      is <code>C_TYPE_VOUCHER_DIVISION</code>,
     *                      or the promotion plan identifier if the required
     *                      division type is 
     *                      <code>C_TYPE_PROMO_DIVISION</code>
     *                      or <code>C_TYPE_BATCH_PROMO_DIVISION</code>.
     *  @return Data object containing the vosp record in this data set that
     *          matches the criteria supplied by the parameters of this 
     *          method. If no vosp record in this data set was found to match
     *          the supplied criteria, then <code>null</code> is returned.
     */
    public VospVoucherSplitsData getDivision (PpasDate     p_date,
                                              ServiceClass p_serviceClass,
                                              String       p_divisionId)
    {
        VospVoucherSplitsData  l_vospRecord = null;
        int                    l_loop;
        boolean                l_found = false;
        ServiceClass           l_vospServiceClass;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_LOW,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_START | PpasDebug.C_ST_APPLICATION_DATA,
                 null, C_CLASS_NAME, 22510, this,
                 "Entering getDivision with data [date: " + p_date +
                                               "; service class: " + p_serviceClass +
                                               "; division ID: " + p_divisionId + "; ]");
        }

        if (i_dataArr != null)
        {
            
            for (l_loop = 0;
                 ((l_loop < i_dataArr.length) && !l_found);
                 l_loop++)
            {
                // Get Service class in current VOSP row as an int to use in 
                // comparison test
                l_vospServiceClass = i_dataArr[l_loop].getServiceClass();

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                         PpasDebug.C_LVL_MODERATE,
                         PpasDebug.C_APP_BUSINESS,
                         PpasDebug.C_ST_APPLICATION_DATA,
                         null, C_CLASS_NAME, 22520, this,
                         "Data array [" + l_loop + "]: " + i_dataArr[l_loop] +
                         "\nGetDivisionId: " + i_dataArr[l_loop].getDivisionId());
                }

                if ( (p_serviceClass != null) &&
                     (p_serviceClass.equals(l_vospServiceClass) &&
                      i_dataArr[l_loop].getDivisionId().equals (p_divisionId)) )
                {
                    if ((p_date != null) && p_date.isSet() && 
                        i_dataArr[l_loop].getStartDate().isSet())
                    {   
                        if (!p_date.before (i_dataArr[l_loop].getStartDate()) &&
                             (!(i_dataArr[l_loop].getEndDate().isSet())
                                 ||
                              (!p_date.after (i_dataArr[l_loop].getEndDate()))))
                        {
                            l_found = true;
                            l_vospRecord = i_dataArr[l_loop];
                        }
                    }
                }
            }
        }

        return (l_vospRecord);

    } //  end method 'getDivision'

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_dataArr, p_sb);
    }
}

