////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CmdfCustMastDefaultsDataSet.Java
//      DATE            :      26-Mar-2001
//      AUTHOR          :      Sally Wells
//
//      DESCRIPTION     :      A set (collection) of cmdf data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// (c) Copyright Sema Group 2001 
//
// The copyright in this work belongs to Sema Group. The information 
// contained in this work is confidential and must not be reproduced or
// disclosed to others without the prior written permission of Sema Group
// or the company within the Sema group of companies which supplied it.
// 'Sema' is a registered trade mark of Sema Group. 
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * An object that represents a cmdf data set which can be read from
 * the database.
 */
public class CmdfCustMastDefaultsDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CmdfCustMastDefaultsDataSet";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The Logger to be used to log exceptions generated within this class.
     */
    protected Logger   i_logger = null;

    /** Array of cmdf data. */
    private CmdfCustMastDefaultsData[]  i_cmdfCustMastDefaultsArr;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new set of cmdf data.
     * 
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_cmdfCustMastDefaultsArr Set of CMDF data objects.
     */
    public CmdfCustMastDefaultsDataSet(
        PpasRequest                p_request,
        Logger                     p_logger,
        CmdfCustMastDefaultsData[] p_cmdfCustMastDefaultsArr)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10110, this,
                 "Constructing CmdfCustMastDefaultsDataSet(...)");
        }

        i_logger       = p_logger;
        i_cmdfCustMastDefaultsArr = p_cmdfCustMastDefaultsArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10150, this,
                 "Constructed CmdfCustMastDefaultsDataSet(...)");
        }
    } // End of public constructor
      //         CmdfCustMastDefaultsDataSet

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getData = "getData";
    /**
     * Returns the cmdf cust mast defaults object for the supplied Market
     * and cust class (these are the table keys).
     * 
     * @param p_cmdfMarket The market associated with the class.
     * @param p_cmdfCustClass The key of the class.
     * @return A sert of CMDF data.
     */
    public CmdfCustMastDefaultsData getData(
        Market  p_cmdfMarket,
        ServiceClass  p_cmdfCustClass)
    {
        int                       l_loop;
        CmdfCustMastDefaultsData  l_cmdf = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START, null, C_CLASS_NAME, 11100, this,
                    "Entering " + C_METHOD_getData);
        }

        if (i_cmdfCustMastDefaultsArr != null)
        {
            for ( l_loop = 0; l_loop < i_cmdfCustMastDefaultsArr.length; l_loop++ )
            {
                if (i_cmdfCustMastDefaultsArr[l_loop].getCmdfMarket().equals(p_cmdfMarket) &&
                    i_cmdfCustMastDefaultsArr[l_loop].getCmdfCustClass().equals(p_cmdfCustClass) )
                {
                    l_cmdf = i_cmdfCustMastDefaultsArr[l_loop];
                    break;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END, null, C_CLASS_NAME, 11190, this,
                    "Leaving " + C_METHOD_getData);
        }

        return(l_cmdf);
    } // End of public method getData(int, int, String)


    /**
     * Returns the cmdf cust mast defaults data object for the supplied
     * cust class (these are the table keys).
     * @param p_custClass The customer class to obtain the CMDF data for (there
     *                    should only be one active CMDF record for a given
     *                    customer class).
     * @return The CMDF record obtained.
     */
    public CmdfCustMastDefaultsData getData(ServiceClass  p_custClass)
    {
        int                       l_loop;
        CmdfCustMastDefaultsData  l_cmdf = null;
        CmdfCustMastDefaultsData  l_cmdfArr[];

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START, null, C_CLASS_NAME, 21100, this,
                    "Entering " + C_METHOD_getData);
        }

        // Use a local reference so method does not need to be synchronized.
        // i.e. assignment is atomic, so if a reload occurs during execution of
        // this method, this method will either work on the old array or the
        // completely loaded new array. 

        l_cmdfArr = i_cmdfCustMastDefaultsArr;

        if (l_cmdfArr != null)
        {
            for ( l_loop = 0; l_loop < l_cmdfArr.length; l_loop++ )
            {
                if (l_cmdfArr[l_loop].getCmdfCustClass().equals(p_custClass))
                {
                    l_cmdf = l_cmdfArr[l_loop];
                    break;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END, null, C_CLASS_NAME, 21190, this,
                    "Leaving " + C_METHOD_getData);
        }

        return(l_cmdf);

    } // End of public method getData (String)


    /**
     * Returns an array of this cmdf data set, but with deleted
     * rows omitted.
     * 
     * @return Return a set of all available CMDF recordd.
     */
    public CmdfCustMastDefaultsData[] getAvailableArray()
    {
        CmdfCustMastDefaultsData  l_availableCmdfCustMastDefaultsArr[];
        Vector                    l_availableV = new Vector();
        int                       l_loop;
        CmdfCustMastDefaultsData  l_cmdfCustMastDefaultsEmptyArr[] =
                new CmdfCustMastDefaultsData[0];

        for(l_loop = 0; l_loop < i_cmdfCustMastDefaultsArr.length; l_loop++)
        {
            if (!i_cmdfCustMastDefaultsArr[l_loop].isDeleted())
            {
                // Language not deleted, add to temporary vector.
                l_availableV.addElement(i_cmdfCustMastDefaultsArr[l_loop]);
            }
        }

        l_availableCmdfCustMastDefaultsArr = (CmdfCustMastDefaultsData[])l_availableV.toArray(
                    l_cmdfCustMastDefaultsEmptyArr);

        return(l_availableCmdfCustMastDefaultsArr);

    } // End of public method getAvailableArray()

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("Available", i_cmdfCustMastDefaultsArr, p_sb);
    }
}

