////////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PaprPaymentProfileData.Java
//      DATE            :       17-Jul-2002
//      AUTHOR          :       Nick Fletcher
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       A record of payment profile data 
//                              (corresponding to a single row of the
//                              papr_payment_profile table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 23/09/03 | R Isaacs   | Changes to reflect              | CR#15/440 
//          |            | PAPR_PAYMENT_PROFILE tables as  |
//          |            | defined in PRD_ASCS00_SYD_AS_003|
//----------+------------+---------------------------------+--------------------
// 30/10/03 | MAGray     | Change ccy to string.           | CR#56/531
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of payment profile data (i.e.
 * a single row of the papr_payment_profile table).
 */
public class PaprPaymentProfileData extends DataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PaprPaymentProfileData";


    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The payment profile identifier.
     */
    private  String            i_paymentProfile = null;

    /**  The Payment Profile description. */
    private String  i_description = null;
    
    /** Currency associated with the payment profile. */
    private PpasCurrency    i_currency = null;
    
    /** Flag set to '*' if payment profile has been deleted, else blank. */
    private char i_deleteFlag = ' ';

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new payment profile data object and initialises its instance
     * attributes.
     * 
     * @param p_request The request to process.
     * @param p_paymentProfile The key of this payment profile.
     * @param p_description The description associated with this payment profile.
     * @param p_currency The currency that is associated with this payment profile.
     * @param p_deleteFlag Set to '*' if payment profile record has been deleted, else blank.
     */
    public PaprPaymentProfileData(
        PpasRequest            p_request,
        String                 p_paymentProfile,
        String                 p_description, 
        PpasCurrency           p_currency,
        char                   p_deleteFlag)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 93010, this,
                 "Constructing " + C_CLASS_NAME);
        }

        i_paymentProfile  = p_paymentProfile;
        i_description     = p_description;
        i_currency        = p_currency;
        i_deleteFlag      = p_deleteFlag;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 93090, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor


    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the payment profile identifier.
     * 
     * @return The payment profile key.
     */
    public String getPaymentProfile()
    {
        return i_paymentProfile;
    }
    
    /** Get the payment profile description.
     * 
     * @return The description of this payment profile.
     */
    public String getDescription()
    {
        return i_description;
    }

    /** Get the currency associated with the payment profile.
     * 
     * @return The currency that is associated with this payment profile.
     */
    public PpasCurrency getCurrency()
    {
        return i_currency;
    }

    /** 
     * Returns true if payment profile has been marked as deleted.
     * 
     * @return Flag indicating whether the payment profile has been deleted.
     */
    public boolean isDeleted()
    {
        return('*' == i_deleteFlag);
    }

    //  CR#15/440 [End]
} // End of public class PaprPaymentProfileData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////