////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PucoPurgeControlSqlService.Java
//      DATE            :       23-August-2004
//      AUTHOR          :       Michael Erskine
//      REFERENCE       :       PpacLon#112/3801
//                              PRD_ASCS00_ANA_FD_19
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Service to read the PUCO_PURGE_CONTROL table and
//                              return a PucoPurgeControlDataSet object.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// 11-Sep-02 | Mike Hickman  | Specifically exclude the   | PpaLon#1534/6511
//           |               | external recharge service  |
//           |               | class from the SQL.        |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import com.slb.sema.ppas.common.businessconfig.dataclass.PucoPurgeControlData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PucoPurgeControlDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to read the SRVA_MISC_CODE table and return a
 * MiscCodeDataSet object.
 */
public class PucoPurgeControlSqlService
{
    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PucoPurgeControlSqlService";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new object for the readingo the  PUCO_PURGE_CONTROL table.
     * 
     * @param p_request The request to process.
     */
    public PucoPurgeControlSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Read the purge control data from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return  A set of misc code data objects.
     * @throws PpasSqlException Any exception derived from this class.
     */
    public PucoPurgeControlDataSet readAll(PpasRequest            p_request,
                                           JdbcConnection         p_connection)
        throws PpasSqlException
    {
        JdbcStatement           l_statement;
        JdbcResultSet           l_resultSet;
        SqlString               l_sql;
        PucoPurgeControlData    l_purgeControlData;
        PucoPurgeControlDataSet l_purgeControlDataSet;
        String                  l_jobType;
        PpasDateTime            l_jobExecutionDateTime;
        PpasDate                l_purgeEligibilityDate;
        char                    l_archiveOption;
        char                    l_status;
        int                     l_subJobCount;
        PpasDate                l_genYyyymmdd;
        String                  l_opid;
        long                    l_jsJobId;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_readAll);
        }

        l_sql = new SqlString(500, 0, 
                    "SELECT puco_job_type," +
                       "puco_job_execution_date_time," +
                       "puco_purge_eligibility_date," +
                       "puco_archive_option," +
                       "puco_status," +
                       "puco_sub_job_cnt," +
                       "puco_gen_ymdhms," +
                       "puco_opid," +
                       "puco_js_job_id " +
                "FROM puco_purge_control");
                
        l_purgeControlDataSet = new PucoPurgeControlDataSet(p_request, 20, 10);

        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 11010, this, p_request);

        l_resultSet = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 11020, this, p_request, l_sql);

        while (l_resultSet.next(11030))
        {
            l_jobType              = l_resultSet.getString(11040, "puco_job_type");
            l_jobExecutionDateTime = l_resultSet.getDateTime(11050, "puco_job_execution_date_time");
            l_purgeEligibilityDate = l_resultSet.getDate(11060, "puco_purge_eligibility_date");
            l_archiveOption        = l_resultSet.getChar(11070, "puco_archive_option");
            l_status               = l_resultSet.getChar(11080, "puco_status"); 
            l_subJobCount          = l_resultSet.getInt(11090, "puco_sub_job_cnt");
            l_genYyyymmdd          = l_resultSet.getDateTime(11100, "puco_gen_ymdhms");
            l_opid                 = l_resultSet.getString(11110, "puco_opid");
            l_jsJobId              = l_resultSet.getLong(11120, "puco_js_job_id");

            // Create a new purge control data object...
            l_purgeControlData = new PucoPurgeControlData(p_request,
                                                          l_jobType,
                                                          l_jobExecutionDateTime,
                                                          l_purgeEligibilityDate,
                                                          (l_archiveOption == 'Y'),
                                                          l_status,
                                                          l_subJobCount,
                                                          l_genYyyymmdd,
                                                          l_opid,
                                                          l_jsJobId);


            // ...and add it to the purge control data set.
            l_purgeControlDataSet.addPurgeControlDataObject(l_purgeControlData);
        }

        l_resultSet.close(11100);

        l_statement.close( C_CLASS_NAME, C_METHOD_readAll, 12110, this, p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 12120, this,
                "Leaving " + C_METHOD_readAll);
        }

        return(l_purgeControlDataSet);
    }
    
}