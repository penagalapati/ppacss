////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryEnquiryData.Java
//      DATE            :       01-June-2006
//      AUTHOR          :       Ian James
//      REFERENCE       :       PRD_ASCS00_DEV_SS_99
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Call History Enquiry data, retrieved from the Call Hsitory Server.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * A data object representing a Get Chsi request.
 */
public class CallHistoryEnquiryData extends DataObject
{
    //-------------------------------------------------------------------------
    // Class level constants.
    //-------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to middleware methods. */
    private static final String C_CLASS_NAME = "CallHistoryEnquiryData";

    //-------------------------------------------------------------------------
    // Private constants
    //-------------------------------------------------------------------------

    /** Set of CHSI data objects. */
    private CallHistoryDataSet  i_chsiList = null;

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------

    /**
     * Full Constructor.
     *
     * @param p_chsiList Set of Call History data objects.
     */
    public CallHistoryEnquiryData(CallHistoryDataSet p_chsiList)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10100, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_chsiList = p_chsiList;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10013, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // Public Methods
    //-------------------------------------------------------------------------

    /** Returns the Set of F&F data objects.
     * @return Set of F&F data objects.
     */
    public CallHistoryDataSet getChsiList()
    {
        return i_chsiList;
    }
}
    
