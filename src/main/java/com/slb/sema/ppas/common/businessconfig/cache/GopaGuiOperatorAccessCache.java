////////////////////////////////////////////////////////////////////////////////
//          PPAS IPR ID : 9000
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       GopaGuiOperatorAccessCache.Java
//      DATE            :       29-Jan-2002
//      AUTHOR          :       Mike Hickman
//      REFERENCE       :       PpaLon#1211/4922
//                              PRD_PPAK00_GEN_CA_318
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2002
//
//      DESCRIPTION     :       Cache wrapper for GopaGuiOperatorAccessDataSet.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.GopaGuiOperatorAccessDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.GopaGuiOperatorAccessSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/** Cache wrapper for GopaGuiOperatorAccessDataSet object. */
public class GopaGuiOperatorAccessCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "GopaGuiOperatorAccessCache";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** The set of Gui Operator Access Profiles. */
    private GopaGuiOperatorAccessDataSet i_gopaGuiOperatorAccessDataSet = null;

    /** Service used to select GUI operator access profiles from the db. */
    private GopaGuiOperatorAccessSqlService i_gopaGuiOperatorAccessSqlService;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /** Creates a new GUI operator access profile cache object.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public GopaGuiOperatorAccessCache(PpasRequest    p_request,
                                      Logger         p_logger,
                                      PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 10000, this,
                "Constructing " + C_CLASS_NAME );
        }

        i_gopaGuiOperatorAccessSqlService = new GopaGuiOperatorAccessSqlService(p_request, p_logger);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 10010, this,
                "Constructed " + C_CLASS_NAME );
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /** Reloads the GUI operator access profiles into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest     p_request,
                       JdbcConnection  p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 11000, this,
                "Entered " + C_METHOD_reload );
        }

        i_gopaGuiOperatorAccessDataSet = i_gopaGuiOperatorAccessSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 11010, this,
                "Leaving " + C_METHOD_reload );
        }
    }

    /** Returns a set of GUI operator access profiles.
     * 
     * @return All GUI Operator access data.
     */
    public GopaGuiOperatorAccessDataSet getAll()
    {
        return(i_gopaGuiOperatorAccessDataSet);
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_gopaGuiOperatorAccessDataSet);
    }
}

