////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CallHistoryData.Java
//      DATE            :       7-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8062, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Call History data, retrieved from the Call History Server.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 24/05/06 |M.Toernqvist| Changes after review.           | PpasLon#8900
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import java.util.ArrayList;

import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/** Call History data, retrieved from the Call History Server. */
public class CallHistoryData extends DataObject implements Comparable
{
    /** Standard class name constant to be used in calls to middleware methods. */
    private static final String C_CLASS_NAME = "CallHistoryData";

    //------------------------------------------------------------------------
    // Instance Variables.
    //------------------------------------------------------------------------
    /** The MSISDN to which the call history data belongs. */
    private Msisdn           i_msisdn;
    /** Date and Time when the call started. */
    private PpasDateTime     i_callStartDateTime;
    /** The 'B-number', phone number to which the call is connected. */
    private String           i_otherPartyNumber;
    /** Defined timezone. */
    private String           i_timezone;
    /** The time length of the call. */
    private Integer          i_duration;
    /** The cost for the call. */
    private Money            i_cost;
    /** A collection containing dedicated accounts. */
    private ArrayList        i_dedicatedAccountUsed;
    /** The balance after the call. */
    private Money            i_balanceAfter;
    /** The current traffic case. */
    private Integer          i_trafficCase;
    /** The current traffic service. */
    private Integer          i_teleService;
    /** The subscriber's location. */
    private String           i_location;
    /** Transmitted data volumne. */
    private Integer          i_dataVolume;
    /** Number of events. */
    private Integer          i_numberOfEvents;
    /** Family and Friends indicator. */
    private Integer          i_fafIndicator;
    /** The network id. */
    private String           i_networkId;
    /** Name of the service provider. */
    private String           i_serviceProviderId;
    /** Current service class. */
    private ServiceClass     i_serviceClass;
    /** The account group id. */
    private AccountGroupId   i_accountGroupId;
    /** Information about service offerings. */
    private ServiceOfferings i_serviceOfferings;
    /** The community id. */
    private Integer          i_communityId;
    
//    /** Help variable for sorting in CallHistoryDataSet. */
//    private String           i_keyDateAndBNumber;
//    /** Help variable for sorting in CallHistoryDataSet. */
//    private String           i_keyBNumberAndDate;

    
    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    /**
     * Public constructor that creates an instance of CallHistoryData and populates it
     * with the input arguments.
     * @param p_msisdn               The MSISDN to which the call history data belongs.
     * @param p_callStartDateTime    Date and Time when the call started.
     * @param p_otherPartyNumber     The 'B-number', phone number to which the call is connected.
     * @param p_timezone             Defined timezone. 
     * @param p_duration             The time length of the call.
     * @param p_cost                 The cost for the call.
     * @param p_dedicatedAccountUsed A collection containing dedicated accounts.
     * @param p_balanceAfter         The balance after the call.
     * @param p_trafficCase          The current traffic case. 
     * @param p_teleService          The current traffic service.
     * @param p_location             The subscriber's location.
     * @param p_dataVolume           Transmitted data volumne.
     * @param p_numberOfEvents       Number of events.
     * @param p_fafIndicator         Family and Friends indicator.
     * @param p_networkId            The network id.
     * @param p_serviceProviderId    Name of the service provider.
     * @param p_serviceClass         Current service class.
     * @param p_accountGroupId       The account group id.
     * @param p_serviceOfferings     Information about service offerings.
     * @param p_communityId          The community id. 
     */
    public CallHistoryData( Msisdn           p_msisdn,
                            PpasDateTime     p_callStartDateTime,
                            String           p_otherPartyNumber,
                            String           p_timezone,
                            Integer          p_duration,
                            Money            p_cost,
                            ArrayList        p_dedicatedAccountUsed,
                            Money            p_balanceAfter,
                            Integer          p_trafficCase,
                            Integer          p_teleService,
                            String           p_location,
                            Integer          p_dataVolume,
                            Integer          p_numberOfEvents,
                            Integer          p_fafIndicator,
                            String           p_networkId,
                            String           p_serviceProviderId,
                            ServiceClass     p_serviceClass,
                            AccountGroupId   p_accountGroupId,
                            ServiceOfferings p_serviceOfferings,
                            Integer          p_communityId )
    {
        super(null);
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_msisdn               = p_msisdn;
        i_callStartDateTime    = p_callStartDateTime;
        i_otherPartyNumber     = p_otherPartyNumber;
        i_timezone             = p_timezone;
        i_duration             = p_duration;
        i_cost                 = p_cost;
        i_dedicatedAccountUsed = p_dedicatedAccountUsed;
        i_balanceAfter         = p_balanceAfter;
        i_trafficCase          = p_trafficCase;
        i_teleService          = p_teleService;
        i_location             = p_location;
        i_dataVolume           = p_dataVolume;
        i_numberOfEvents       = p_numberOfEvents;
        i_fafIndicator         = p_fafIndicator;
        i_networkId            = p_networkId;
        i_serviceProviderId    = p_serviceProviderId;
        i_serviceClass         = p_serviceClass;
        i_accountGroupId       = p_accountGroupId;
        i_serviceOfferings     = p_serviceOfferings;
        i_communityId          = p_communityId;
        
//        i_keyDateAndBNumber    = p_callStartDateTime.toString_yyyyMMdd_HHmmss() + p_otherPartyNumber;
//        i_keyBNumberAndDate    = p_otherPartyNumber + p_callStartDateTime.toString_yyyyMMdd_HHmmss();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }

    }

    
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------
    
    /**
     * @return The Account Group Id.
     */
    public AccountGroupId getAccountGroupId()
    {
        return i_accountGroupId;
    }

    /**
     * @return The balance after the call.
     */
    public Money getBalanceAfter()
    {
        return i_balanceAfter;
    }

    /**
     * @return The Date and time when the call started.
     */
    public PpasDateTime getCallStartDateTime()
    {
        return i_callStartDateTime;
    }

    /**
     * @return The Communitiy Id.
     */
    public Integer getCommunityId()
    {
        return i_communityId;
    }

    /**
     * @return The cost for the call.
     */
    public Money getCost()
    {
        return i_cost;
    }

    /**
     * @return The transmitted data volume.
     */
    public Integer getDataVolume()
    {
        return i_dataVolume;
    }

    /**
     * @return An arrayList of dedicated accouns.
     */
    public ArrayList getDedicatedAccountUsed()
    {
        return i_dedicatedAccountUsed;
    }

    /**
     * @return The duration for the call.
     */
    public Integer getDuration()
    {
        return i_duration;
    }

    /**
     * @return The Family and Friends indicator.
     */
    public Integer getFafIndicator()
    {
        return i_fafIndicator;
    }

    /**
     * @return The location.
     */
    public String getLocation()
    {
        return i_location;
    }

    /**
     * @return The MSISDN for this call history data.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /**
     * @return The network id.
     */
    public String getNetworkId()
    {
        return i_networkId;
    }

    /**
     * @return The number of events.
     */
    public Integer getNumberOfEvents()
    {
        return i_numberOfEvents;
    }

    /**
     * @return The other party number, also called B-number.
     */
    public String getOtherPartyNumber()
    {
        return i_otherPartyNumber;
    }

    /**
     * @return The service class.
     */
    public ServiceClass getServiceClass()
    {
        return i_serviceClass;
    }

    /**
     * @return The service offerings.
     */
    public ServiceOfferings getServiceOfferings()
    {
        return i_serviceOfferings;
    }

    /**
     * @return The name of the service provider.
     */
    public String getServiceProviderId()
    {
        return i_serviceProviderId;
    }

    /**
     * @return The tele service.
     */
    public Integer getTeleService()
    {
        return i_teleService;
    }

    /**
     * @return The current time zone.
     */
    public String getTimezone()
    {
        return i_timezone;
    }

    /**
     * @return The current traffic case.
     */
    public Integer getTrafficCase()
    {
        return i_trafficCase;
    }

    /** Compare this object with another.
     * 
     * @param p_that Object to compare with.
     * @return Standard contract of compareTo.
     */
    public int compareTo(Object p_that)
    {
        return p_that instanceof CallHistoryData ? compareTo((CallHistoryData)p_that) : -1;
    }
    
    /** Compare this CallHistoryData object with another.
     * 
     * @param p_that CallHistoryData object to compare with.
     * @return Standard contract of compareTo.
     */
    public int compareTo(CallHistoryData p_that)
    {
        int l_result = i_callStartDateTime.compareTo(p_that.i_callStartDateTime);
        
        if (l_result == 0)
        {
            l_result = i_otherPartyNumber.compareTo(p_that.i_otherPartyNumber);
        }
        
        if (l_result == 0)
        {
            l_result = i_trafficCase.compareTo(p_that.i_trafficCase);
        }
        
        if (l_result == 0)
        {
            // Compare string versions of MSISDN - used to ensure uniqueness so should be accurate enough.
            l_result = i_msisdn.toString().compareTo(p_that.i_msisdn.toString());
        }
        
        return l_result;
    }
}
