//////////////////////////////////////////////////////////////////////////////
//
// FILE NAME       :       BonaBonusAccumParamsDataSet.java
// DATE            :       30-Jun-2007
// AUTHOR          :       Michael Erskine
// REFERENCE       :       PRD_ASCS_GEN_CA_128
//
// COPYRIGHT       :       WM-data 2007
//
// DESCRIPTION     :       A set of bonus accumulation parameter data config.
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Object containing a set of <code>BonaBonusAccumParamsData</code> objects. */
public class BonaBonusAccumParamsDataSet extends DataSetObject
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BonaBonusAccumParamsDataSet";
    
    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** Array of bonus scheme records. That is, the data of this DataSet object. */
    private BonaBonusAccumParamsData[] i_allRecords;
     
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new set of Bonus Scheme data.
     * @param p_request The request to process.
     * @param p_data Array holding the  bonus scheme data records to be stored by this object.
     */
    public BonaBonusAccumParamsDataSet(PpasRequest             p_request,
                                       BonaBonusAccumParamsData[]  p_data)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START, p_request, C_CLASS_NAME, 14510, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_allRecords = p_data;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 14590, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }
    
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Returns Bonus Scheme data associated with a specified case.
     * 
     * @param p_schemeId The bonus scheme.
     * @return BonaBonusAccumParamsData record from this DataSet with the given code. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BonaBonusAccumParamsData getRecord(String p_schemeId)
    {
        BonaBonusAccumParamsData l_record = null;
        
        for (int i = 0; i < i_allRecords.length; i++)
        {
            if ( i_allRecords[i].getBonusSchemeId().equals(p_schemeId))
            {
                l_record = i_allRecords[i];
                break;
            }
        }

        return l_record;
    }    
    
    /**
     * Returns available (that is, not marked as deleted) Bonus Scheme records associated 
     * with a specified case.
     * 
     * @param p_schemeId The bonus scheme.
     * @return BonaBonusAccumParamsData record from this DataSet with the given case. If
     *         no record can be found, then <code>null</code> is returned.
     */
    public BonaBonusAccumParamsData getAvailableRecord (String p_schemeId)
    {
        return getRecord(p_schemeId);
    }
    
    /**
     * Returns array of all records in this data set object.
     * @return Array of all data records.
     */
    public BonaBonusAccumParamsData[] getAllArray()
    {
        return i_allRecords;
    }    
    
    /**
     * Get a data set containing the all available  records. That is, those not marked as deleted.
     * @return Data set containing available records.
     */
    public BonaBonusAccumParamsData[] getAvailableArray()
    {
        return i_allRecords;
    }    
    
    /**
     * Description of this object.
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("BonaBonusAccumParamsDataSet", i_allRecords, p_sb);
    }
}
