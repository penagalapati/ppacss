/*
 * Created on 04-Dec-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.slb.sema.ppas.common.awd.alarmclient;

import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarm;
import com.slb.sema.ppas.common.awd.alarmcommon.SimpleAlarmEvent;
import com.slb.sema.ppas.util.support.Debug;
import com.slb.sema.ppas.util.support.UtilProperties;

/**
 * @author MVonka
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AlarmClientUT
{
    
    private AlarmClient         i_alarmClient;

    /**
     * 
     */
    public AlarmClientUT(
        String                  p_alarmServerNode,
        int                     p_alarmServerPortNumber
    )
    {
        super();
        UtilProperties          l_config;
        
        l_config = new UtilProperties();
        l_config.setProperty(AlarmClient.C_CFG_PARAM_ALARM_SERVER_NODE,
                p_alarmServerNode);
        l_config.setProperty(AlarmClient.C_CFG_PARAM_ALARM_SERVER_PORT_NUMBER,
                Integer.toString(p_alarmServerPortNumber));
        i_alarmClient = new AlarmClient(l_config);
    }

    public static void main(
        String[]                p_argsARR
    )
    {
        AlarmClientUT           l_alarmClientUT;
        String                  l_alarmServerNode;
        int                     l_alarmServerPortNumber;

        Debug.setDebugFullToSystemErr();
        // Debug.setDebugFullToFile();
        
        l_alarmServerNode = p_argsARR[0];
        l_alarmServerPortNumber = Integer.parseInt(p_argsARR[1]);
        
        l_alarmClientUT = new AlarmClientUT(
            l_alarmServerNode, l_alarmServerPortNumber
            );

        l_alarmClientUT.test1();
        
        try
        {
            Thread.sleep(1000 * 60 * 10);
        }
        catch(InterruptedException l_e)
        {
            // Ignore
        }

        System.exit(1);
    }
    
    private void test1()
    {
        SimpleAlarmEvent        l_event;
        SimpleAlarm             l_alarm;
         
        l_event = new SimpleAlarmEvent("NO_MATCH", "No %25%26 &% match",
                "NODE1", "TEST_PROC1");
       
        System.out.println("Sending event" + l_event);
        i_alarmClient.sendEvent(l_event);
        
        l_event = new SimpleAlarmEvent("EVENT_1_HIGH_SEV", "High sev &% match",
                "NODE1", "TEST_PROC1");
        System.out.println("Sending event" + l_event);
        i_alarmClient.sendEvent(l_event);
        
        l_event = new SimpleAlarmEvent("EVENT_2_MOD_SEV", "Mod match",
                "NODE2", "TEST_PROC2");
        System.out.println("Sending event" + l_event);
        i_alarmClient.sendEvent(l_event);
        
        l_alarm = new SimpleAlarm("ALARM_1_HIGH_SEV", "High sev &% match", 10,
                "NODE3", "TEST_PROC3");
        System.out.println("Sending alarm" + l_alarm);
        i_alarmClient.sendAlarm(l_alarm);

    }
}
