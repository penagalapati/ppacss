////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AsceAscsErrorCodesCache.java
//      DATE            :       17-Mar-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.AsceFailureReasonDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.AsceAscsErrorCodesSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * Holds a cache of the asce_ascs_error_codes data.
 */
public class AsceAscsErrorCodesCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AsceAscsErrorCodesCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /**
     * The F&F Charging Indicator Codes SQL service to use to load cache.
     */
    private AsceAscsErrorCodesSqlService i_asceSqlService = null;

    /** Cache of all valid asce codes (including ones marked as deleted). */
    private AsceFailureReasonDataSet i_allAsceCodes = null;

    /**
     * Cache of available Charging Indicators (not including ones marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated from the complete list each time it is required.
     */
    private AsceFailureReasonDataSet i_availableAsceCodes = null;


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new valid error codes cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public AsceAscsErrorCodesCache(PpasRequest    p_request,
                                   Logger         p_logger,
                                   PpasProperties p_properties)
    {
        super(p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_asceSqlService = new AsceAscsErrorCodesSqlService(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }

    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all valid Charging Indicators (including ones marked as deleted).
     * 
     * @return All valid Charging Indicators.
     */
    public AsceFailureReasonDataSet getAll()
    {
        return i_allAsceCodes;
    }

    /**
     * Returns available valid Charging Indicators (not including ones marked as deleted).
     * 
     * @return Available Charging Indicators.
     */
    public AsceFailureReasonDataSet getAvailable()
    {
        return i_availableAsceCodes;
    }

    /**
     * Reloads the static configuration into the cache.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload(PpasRequest            p_request,
                       JdbcConnection         p_connection)
        throws PpasSqlException
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                "Entered reload");
        }

        i_allAsceCodes = i_asceSqlService.readAll(p_request,
                                                  p_connection);

        i_availableAsceCodes = new AsceFailureReasonDataSet(i_allAsceCodes.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10110, this,
                "Leaving reload");
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allAsceCodes);
    }
}

