////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :      9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      CochCommunityChargingData.Java
//      DATE            :      11-Jul-2004
//      AUTHOR          :      Julien CHENELAT
//      REFERENCE       :       
//
//      COPYRIGHT       :      ATOS ORIGIN 2004
//
//      DESCRIPTION     :      Data from COCH_COMMUNITY_CHARGING.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                     | REFERENCE
//----------+---------------+---------------------------------+--------------------
// dd/mm/yy |               |                                 |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents the database table COCH_COMMUNITY_CHARGING.
 */
public class CochCommunityChargingData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "CochCommunityChargingData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Community Charging Id. */
    private int                         i_communityChgId        = 0;
    
    /** Community Charging Description. */
    private String                      i_communityChgDesc      = null;

    /** Indicator that coch row has been deleted, if it is "*". **/
    private  char                       i_communityDeleteFlag   = ' ';
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new row of CochCommunityCharging and initialises data.
     * 
     * @param p_request             Ppas Request.
     * @param p_communityChgId      Commmunity Charging Id.
     * @param p_communityChgDesc    Community Charging Description.
     * @param p_communityDeleteFlag Community Delete Flag.
     * @param p_opid    Operator that last changed this record.
     * @param p_genYmdHms Date/time of last change to this record.
     */
    public CochCommunityChargingData( PpasRequest        p_request,
                                      int                p_communityChgId,
                                      String             p_communityChgDesc,
                                      char               p_communityDeleteFlag,
                                      String             p_opid,
                                      PpasDateTime       p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_START,
                             p_request, 
                             C_CLASS_NAME, 
                             10010, 
                             this,
                             "Constructing " + C_CLASS_NAME);
        }

        i_communityChgId        = p_communityChgId;
        i_communityChgDesc      = p_communityChgDesc;
        i_communityDeleteFlag   = p_communityDeleteFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                             PpasDebug.C_ST_CONFIN_END,
                             p_request, 
                             C_CLASS_NAME, 
                             10050, 
                             this,
                             "Constructed " + C_CLASS_NAME);
        }
    }
    
    /**
     * Constructor when creating a new record.
     * @param p_communityChgId      Commmunity Charging Id.
     * @param p_communityChgDesc    Community Charging Description.
     * @param p_communityDeleteFlag Community Delete Flag.
     */
    public CochCommunityChargingData( int                p_communityChgId,
                                      String             p_communityChgDesc,
                                      char               p_communityDeleteFlag)
    {
        this(null, p_communityChgId, p_communityChgDesc, p_communityDeleteFlag, null, null);
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Return Community Charging Id.
     * 
     * @return int representing community charging Id.
     */
    public int getCommunityChgId()
    {
        return i_communityChgId;
    }

    /**
     * Return the Community Charging Description.
     * 
     * @return  String representing the Community Charging Description.
     */    
    public String getCommunityChgDesc()
    {
        return i_communityChgDesc;
    }

    /** 
     * Return true if a community charging has been marked as deleted.
     * 
     * @return Flag indicating whether the class has been deleted.
     */
    public boolean isDeleted()
    {
        boolean l_deleted = false;

        if ('*' == i_communityDeleteFlag)
        {
            l_deleted = true;
        }

        return(l_deleted);
    }

    /**
     * Returns a string representation of this object (suitable for trace/debug).
     * 
     * @return String representation of the class.
     */
    public String toString()
    {
        StringBuffer l_buffer = new StringBuffer();

        l_buffer.append(  "communityChargingId="       + i_communityChgId);
        l_buffer.append(", communityChargingDesc"      + i_communityChgDesc);
        l_buffer.append(", communityDeleteFlag="       + i_communityDeleteFlag);

        return(l_buffer.toString());
    }
    
    /**
     * Return a boolean indicating wether or not the current object is equals to the object given in 
     * parameter.
     * 
     * @param p_communityChgData    Community Charging Data object to compare with the current object.
     * 
     * @return  True if both objects are equals, false otherwise.
     */
    public boolean equals( Object p_communityChgData )
    {
        CochCommunityChargingData l_communityChgData = null;
        
        if (p_communityChgData == this) 
        {
            return true;
        }
        else if (p_communityChgData == null || getClass() != p_communityChgData.getClass())
        {
            return false;
        }
        else
        {
            l_communityChgData = (CochCommunityChargingData)p_communityChgData;
            
            if (i_communityChgId == l_communityChgData.getCommunityChgId())
            {
                if ( i_communityChgDesc != null )
                {
                    if (i_communityChgDesc.equals(l_communityChgData.getCommunityChgDesc()))
                    {
                        if (isDeleted() == l_communityChgData.isDeleted())
                        {
                            return true;
                        }
                    }
                }
                else if (l_communityChgData.getCommunityChgDesc() == null)
                {
                    if (isDeleted() == l_communityChgData.isDeleted())
                    {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
}
