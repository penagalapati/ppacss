////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TeleTeleserviceData.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8229, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Teleservice data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//27-June-06|M.Toernqvist| Changes due to code review      | PpasLon#2322/9154
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * This class keeps information for the businesscache retrieved from the database table: tele_teleservice.
 */
public class TeleTeleserviceData extends ConfigDataObject
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TeleTeleserviceData";

    //------------------------------------------------------------------------
    // Instance variables
    //------------------------------------------------------------------------
    /** The Tele service code. */
    private int i_teleServiceCode;
    
    /** The Tele service description. */
    private String i_teleserviceDescription;
    
    /** Flag indicating if this adjustment code is deleted. */
    private boolean i_deleteFlag = false;
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** 
     * Creates an Account Group Data object.
     * 
     * @param p_request The request to process.
     * @param p_teleServiceCode The teleService code.
     * @param p_teleserviceDescription The teleservice description.
     * @param p_deleteFlag The deleted flag.
     * @param p_opid    Operator that last changed this record.
     * @param p_genYmdHms Date/time of last change to this record.
     */
    public TeleTeleserviceData(PpasRequest  p_request,
                               int          p_teleServiceCode,
                               String       p_teleserviceDescription,
                               char         p_deleteFlag,
                               String       p_opid,
                               PpasDateTime p_genYmdHms)
    {
        super(p_opid, p_genYmdHms);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME );
        }

        i_teleServiceCode          = p_teleServiceCode;
        i_teleserviceDescription   = p_teleserviceDescription;
        i_deleteFlag               = (p_deleteFlag == '*');
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME );
        }
    }

    
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    
    /**
     * Return the account group id.
     * @return the account group id
     */
    public int getTeleserviceCode()
    {
        return i_teleServiceCode;
    }

    
    
    /**
     * Return the account group description.
     * @return the account group description
     */
    public String getTeleserviceDescription()
    {
        return i_teleserviceDescription;
    }

    
    
    /** Returns true if this account group has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_deleteFlag;
    }
//
//    
//    
//    public String toString()
//    {
//        final String L_NL = "\n";
//        StringBuffer l_tmp = new StringBuffer(200);
//        l_tmp.append("TeleServiceCode = " + i_teleServiceCode);
//        l_tmp.append(L_NL);
//        l_tmp.append("Description     = " + i_teleserviceDescription);
//        l_tmp.append(L_NL);
//        l_tmp.append("DeleteFlag      = " + i_deleteFlag );
//        l_tmp.append(L_NL);
//        
//        return l_tmp.toString();
//    }
}
