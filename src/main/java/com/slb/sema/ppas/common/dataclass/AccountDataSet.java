////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AccountDataSet.Java
//      DATE            :       23-Mar-2001
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Stores a set of account data records.
//                              Each record contains information associated
//                              with a single customer account.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy | <author>   | <description>                   | <reference>
//----------+------------+---------------------------------+--------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.Debug;

/** Store a set of account data records and defines methods to add, 
 *  delete and update these records. Each record contains information 
 *  associated with a customer account. All records will contain the same type
 *  of information and this will be either:
 *  <br> - Basic account information only
 *  <br> - Basic account information and properties of the account
 *  <br> - Full account information and properties of the account
 *  The type of information defined in the records is determined by the 
 *  i_recordType instance variable. This variable takes one of three values;
 *  C_BASIC_ACCOUNT_INFO, C_ACCOUNT_PROPERTIES and C_FULL_ACCOUNT_INFO.
 * <P>
 * <br>
 *  These objects will be created and populated by an AccountSqlService object from data in the datanase and
 *  they can store changes which are to be updated into the database.
 */
public class AccountDataSet
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    /** Value indicating that the record type held by this class has not yet been set. Value is {@value}. */
    public static final int C_RECORD_TYPE_NOT_SET = 0;

    /** Value defining the record type held by this class as BasicAccountData. Value is {@value}. */
    public static final int C_BASIC_ACCOUNT_INFO = 1;

    /** Value defining the record type held by this class as AccountProperties. Value is {@value}. */
    public static final int C_ACCOUNT_PROPERTIES = 2;

    /** Value defining the record type held by this class as FullAccountData. Value is {@value}. */
    public static final int C_FULL_ACCOUNT_INFO = 3;

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AccountDataSet";

    //------------------------------------------------------------------------
    // Private class instance attributes
    //------------------------------------------------------------------------

    /** The Logger object to be used in logging any exceptions arising from the use of this service. */
    private Logger   i_logger;

    /** The set of account data records that this object stores and accesses. */
    private Vector   i_accountDataV;

    /** Defines the type of account data held by this object. */
    private int      i_recordType;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /** Construct an instance of a AccountDataSet object. This
     *  object can then be used to access key account information from the database.
     * 
     *  @param p_logger The Logger to be used to log any exceptions that
     *                  occur while performing this service.
     *  @param p_initVecSize Initial size to be used in creating the Vector
     *                       of ShortAccountData records store by this object.
     *  @param p_growVecSize Size by which the Vector of ShortAccountData 
     *                       records store by this object is to grow by if necessary.
     */
    public AccountDataSet(Logger      p_logger,
                          int         p_initVecSize,
                          int         p_growVecSize)
    {
        if (Debug.on)
        {
            Debug.print(Debug.C_LVL_LOW, Debug.C_APP_SERVICE, Debug.C_ST_CONFIN_START,
                        C_CLASS_NAME, 10000, this,
                        "Constructing AccountDataSet(...)");
        }

        i_logger       = p_logger;
        i_accountDataV = new Vector(p_initVecSize, p_growVecSize);
        i_recordType   = C_RECORD_TYPE_NOT_SET;

        if (Debug.on)
        {
            Debug.print(Debug.C_LVL_LOW, Debug.C_APP_SERVICE, Debug.C_ST_CONFIN_END,
                        C_CLASS_NAME, 10010, this,
                        "Constructed AccountDataSet(...)");
        }
    }  // end of constructor AccountDataSet(...)

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_add = "add";
    /** Create and add a record of basic (key) account data 
     *  to the set of such records that this class defines.
     * 
     * @param p_request The request to process.
     * @param p_custId The customer id of the record to be added to the set.
     * @param p_btCustId The internal identifier of the account holder.
     * @param p_msisdn The MSISDN of the record to be added to the set.
     * @param p_surname The last name of the mobile user.
     * @param p_firstName The first name of the mobile user.
     * @param p_custStatus The status of the mobile phone.
     * @param p_scpId The SDP associated with this account.
     * @param p_market The market associated with this account.
     * @param p_agent The agent that sold this mobile phone.
     * @param p_subAgent The subagent identifier.
     * @param p_activationDate The date the account was activated.
     * @param p_lastRefillDateTime The date/time this account was last refilled.
     * @param p_isPromoSynch  Flag that indicate if the promotion plan synchronization is required or not.
     * @throws PpasSoftwareException If the mobile user cannot be added.
     */ 
    public void add(PpasRequest p_request,
                    String      p_custId,
                    String      p_btCustId,
                    Msisdn      p_msisdn,
                    String      p_surname,
                    String      p_firstName,
                    char        p_custStatus,
                    String      p_scpId,
                    Market      p_market,
                    String      p_agent,
                    String      p_subAgent,
                    PpasDate    p_activationDate,
                    PpasDateTime p_lastRefillDateTime,
                    boolean     p_isPromoSynch)
        throws PpasSoftwareException
    {
        if (Debug.on)
        {
            Debug.print(Debug.C_LVL_LOW, Debug.C_APP_BUSINESS, Debug.C_ST_START,
                        C_CLASS_NAME, 10100, this,
                        "Entering add(...)");
        }

        if (i_recordType != C_BASIC_ACCOUNT_INFO)
        {
            if (i_recordType == C_RECORD_TYPE_NOT_SET)
            {
                i_recordType = C_BASIC_ACCOUNT_INFO;
            }
            else
            {
                if (Debug.on)
                {
                    Debug.print(
                        Debug.C_LVL_LOW,
                        Debug.C_APP_BUSINESS,
                        Debug.C_ST_ERROR,
                        C_CLASS_NAME, 10200, this,
                        "Invalid attempt to add wrong type to Account Info Vector");
                }

                // This is a software exception, since we're attempting to
                // store different type objects in this class
                PpasSoftwareException l_softE =
                    new PpasSoftwareException
                          (C_CLASS_NAME,
                           C_METHOD_add,
                           10110,
                           this,
                           p_request,
                           0,
                           SoftwareKey.get().illegalVectorAdd("i_accountDataV", "BasicAccountData"));

                i_logger.logMessage(l_softE);
                throw(l_softE);
            }
        }

        // Create and add the new basic account information record to the 
        // Vector containing these records
        BasicAccountData l_record = new BasicAccountData(p_request,
                                                         p_custId,
                                                         p_btCustId,
                                                         p_msisdn,
                                                         p_surname,
                                                         p_firstName,
                                                         p_custStatus,
                                                         p_scpId,
                                                         p_market,
                                                         p_agent,
                                                         p_subAgent,
                                                         p_activationDate,
                                                         p_lastRefillDateTime,
                                                         p_isPromoSynch);

        if (Debug.on)
        {
            Debug.print(
                Debug.C_LVL_MODERATE,
                Debug.C_APP_BUSINESS,
                Debug.C_ST_APPLICATION_DATA,
                C_CLASS_NAME, 10120, this,
                "About to add(custId=" + p_custId +
                ",msisdn=" + p_msisdn + ")" );
        }

        i_accountDataV.addElement(l_record);

        if (Debug.on)
        {
            Debug.print(
                Debug.C_LVL_LOW,
                Debug.C_APP_BUSINESS,
                Debug.C_ST_END,
                C_CLASS_NAME, 10190, this,
                "Leaving add(...)");
        }
    }

    //CR#15/440 [End]

    /** Add a record of basic (key) account data to the set of such records that this class defines.
     * 
     * @param p_request The request to process.
     * @param p_accData The BasicAccountData object to be added to this set.
     * @throws PpasSoftwareException If the account cannot be added to the data set.
     */ 
    public void add(PpasRequest      p_request,
                    BasicAccountData p_accData)
        throws PpasSoftwareException
    {
        if (Debug.on)
        {
            Debug.print(
                Debug.C_LVL_LOW,
                Debug.C_APP_BUSINESS,
                Debug.C_ST_START,
                C_CLASS_NAME, 20100, this,
                "Entering " + C_METHOD_add);
        }

        if (i_recordType != C_BASIC_ACCOUNT_INFO)
        {
            if (i_recordType == C_RECORD_TYPE_NOT_SET)
            {
                i_recordType = C_BASIC_ACCOUNT_INFO;
            }
            else
            {
                if (Debug.on)
                {
                    Debug.print(
                        Debug.C_LVL_LOW,
                        Debug.C_APP_BUSINESS,
                        Debug.C_ST_ERROR,
                        C_CLASS_NAME, 20200, this,
                        "Invalid attempt to add wrong type to Account " +
                        "Info Vector");
                }

                // This is a software exception, since we're attempting to
                // store different type objects in this class
                PpasSoftwareException l_softE =
                    new PpasSoftwareException
                          (C_CLASS_NAME,
                           C_METHOD_add,
                           20110,
                           this,
                           p_request,
                           0,
                           SoftwareKey.get().illegalVectorAdd("i_accountDataV", "BasicAccountData"));

                i_logger.logMessage(l_softE);
                throw(l_softE);
            }
        }

        i_accountDataV.addElement(p_accData);

        if (Debug.on)
        {
            Debug.print(
                Debug.C_LVL_LOW,
                Debug.C_APP_BUSINESS,
                Debug.C_ST_END,
                C_CLASS_NAME, 20190, this,
                "Leaving " + C_METHOD_add);
        }
    }

    /** Get the Vector containing the account data associated with this class.
     *  @return Vector containing the account data held by this object.
     */
    public Vector getData()
    {
        return(i_accountDataV);
    }

    /** Generate a String representation of the data in this DataSet object.
     * 
     * @return String representation of this account data set.
     */
    public String toString()
    {
        int l_loop;
        StringBuffer  l_sb = new StringBuffer(this.getClass().getName() + "@" +
                                              this.hashCode() + "\n");

        for(l_loop = 0; l_loop < i_accountDataV.size(); l_loop++)
        {
            l_sb.append("Element[" + l_loop + "]: ");
            l_sb.append(i_accountDataV.elementAt(l_loop).toString() + "\n");
        }
        return(l_sb.toString());
    }
}
