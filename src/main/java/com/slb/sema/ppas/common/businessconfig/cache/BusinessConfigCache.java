////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BusinessConfigCache.Java
//      DATE            :       17-Feb-2001
//      AUTHOR          :       Marek Vonka
//      REFERENCE       :
//
//      COPYRIGHT       :       WM-data 2002
//
//      DESCRIPTION     :       A cache of business configuration data.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                     | REFERENCE
//----------+---------------+---------------------------------+--------------------
//21-Mar-06 |M.Toernqvist   | insert 2 new static instance    | PpaLon#2020/8243
//          |               | variables for teleservice and   |
//          |               | trafficCase                     |
//----------+---------------+---------------------------------+--------------------
//04-Jul-06 |M.Toernqvist   | changes after code review       | PpasLon#2322/9274
//----------+---------------+---------------------------------+--------------------
//26-Jul-06 |K Goswami      | Made significant changes to the | PpasLon#2645/9995
//          |               | class to resolve issues relating|
//          |               | to loading the cache with regard|
//          |               | to new products such as the chs |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache; 

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache of business configuration data. Each table of cached configuration data must be loaded by
 * application code. This allows the application to have some control of the memory usage of this object.
 */
public class BusinessConfigCache implements BusinessConfigTables 
{
    //------------------------------------------------------------------------
    // Class level constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BusinessConfigCache";
    
    //------------------------------------------------------------------------
    // instance variables
    //------------------------------------------------------------------------

    /** The Logger to be used to log exceptions generated within the object. */
    private Logger             i_logger = null;

    /** Context containing useful items like pools and configuration data. */
    protected PpasContext      i_ppasContext = null;

    /** The constant used by this class to get the property from ascs. Value is {@value}. */
    private static final String C_BUSINESS_CONFIG_TABLES =
        "com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache.tables";
    
    /**The Map where the cache list is stored. */
    private Map i_cacheMap;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------

    /**
     * Create a cache of business configuration data.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_ppasContext Context containing useful items like pools and configuration data.
     * @throws PpasConfigException If a configured table does not exist.
     */
    public BusinessConfigCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasContext            p_ppasContext)
        throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 98010, this,
                            "Constructing BusinessConfigCache");
        }

        // Set up the logger to be used
        i_logger = p_logger;

        i_ppasContext = p_ppasContext;

        i_cacheMap = getCacheList(p_ppasContext, p_request, i_logger);
        
        if (PpasDebug.on)
        {            
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 28438, this,
                            "Constructed BusinessConfigCache");
        }
    }
    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCacheList = "getCacheList";
    /**
     * Gets the cache list by reading the relevant property in the properties file.
     * 
     * @param p_ppasContext Context containing useful items like pools and configuration data.
     * @param p_request The request to process.
     * @param p_logger The logger used by this service to log events to.
     * @return Map The cache Map returned by this method.
     * @throws PpasConfigException If a configured table does not exist.
     */
    private Map getCacheList(PpasContext p_ppasContext, PpasRequest p_request, Logger p_logger)
        throws PpasConfigException
    {
        HashMap l_map = new HashMap();
        
        // Get property from properties
        PpasProperties l_prop = p_ppasContext.getProperties();
        Vector l_cacheProp = l_prop.getListFromProperty(C_BUSINESS_CONFIG_TABLES);

        if (l_cacheProp == null || l_cacheProp.size() == 0)
        {
            i_logger.logMessage(new LoggableEvent("No caches to be loaded - check property " +
                                                  C_BUSINESS_CONFIG_TABLES,
                                                  LoggableInterface.C_SEVERITY_INFO));
            
            return l_map;
        }
        
        // Iterate over list
        for (int i = 0; i < l_cacheProp.size(); i++)
        {
            String l_key = ((String)l_cacheProp.get(i)).toLowerCase();
            
            // Create individual caches.
            ConfigCache l_cache = null;
            if (l_key.equals(C_SHORT_VALA_VALID_LANGUAGE))
            {
                l_cache = new ValaValidLanguageCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_PRPL_PROMO_PLAN))
            {
                l_cache = new PrplPromoPlanCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_CMDF_CUST_MAST_DEFAULTS))
            {
                l_cache = new CmdfCustMastDefaultsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_CDGR_ACCE_CARD_GROUP))
            {
                l_cache = new CdgrAcceCardGroupCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_CONF_COMPANY_CONFIG))
            {
                l_cache = new ConfCompanyConfigCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_ACCF_ACCUMULATOR))
            {
                l_cache = new AccfAccumulatorCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SYFG_SYSTEM_CONFIG))
            {
                l_cache = new SyfgSystemConfigCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_BANK_BANK))
            {
                l_cache = new BankBankCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_MISC_CODE))
            {
                l_cache = new MiscCodeCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_GOPA_GUI_OPERATOR_ACCESS))
            {
                l_cache = new GopaGuiOperatorAccessCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_CUFM_CURRENCY_FORMATS))
            {
                l_cache = new CufmCurrencyFormatsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SRVA_ADJ_CODES))
            {
                l_cache = new SrvaAdjCodesCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SRVA_AGENT_MSTR))
            {
                l_cache = new SrvaAgentMstrCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SRVA_MSTR))
            {
                l_cache = new SrvaMstrCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_DIRE_DISCONNECT_REASON))
            {
                l_cache = new DireDisconnectReasonCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_PAPR_PAYMENT_PROFILE))
            {
                l_cache = new PaprPaymentProfileCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SCPI_SCP_INFO))
            {
                l_cache = new ScpiScpInfoCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_MFLT_MISC_FIELD_TITLES))
            {
                l_cache = new MfltMiscFieldTitlesCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_VOSP_VOUCHER_SPLITS))
            {
                l_cache = new VospVoucherSplitsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_DEDA_DEDICATED_ACCOUNTS))
            {
                l_cache = new DedaDedicatedAccountsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_NUBL_NUMBER_BLOCK))
            {
                l_cache = new NublNumberBlockCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_FABA_FAF_BARRED_LIST))
            {
                l_cache = new FabaFafBarredListCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_FACH_FAF_CHARGING_IND))
            {
                l_cache = new FachFafChargingIndCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_EMCR_EMU_CONVERSION_RATES))
            {
                l_cache = new EmuConversionRatesCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_DECI_DEFAULT_CHARGING_IND))
            {
                l_cache = new DeciDefaultChargingIndCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SRVA_FIELDS))
            {
                l_cache = new SrvaFieldsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_ASCE_ERROR_CODES))
            {
                l_cache = new AsceAscsErrorCodesCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_SCAP_ADJ_PARAM))
            {
                l_cache = new ScapServClassAdjParamCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_ACGR_ACC_GROUP))
            {
                l_cache = new AcgrAccountGroupCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_SEOF_SERV_OFF))
            {
                l_cache = new SeofServiceOfferingsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_COCH_COMM_CHG))
            {
                l_cache = new CochCommunityChargingCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_REGI_REGION))
            {
                l_cache = new RegiRegionCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_TELESERVICE))
            {
                l_cache = new TeleTeleserviceCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_TRAFFIC_CASE))
            {
                l_cache = new TrafTrafficCaseCache(p_request, p_logger, p_ppasContext);
            }
            else if (l_key.equals(C_SHORT_CHAV_CHANNEL))
            {
                l_cache = new ChavChannelCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_BONA_BONUS_ACCUM_PARAMS))
            {
                l_cache = new BonaBonusAccumParamsCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_BONE_BONUS_ELEMENTS))
            {
                l_cache = new BoneBonusElementCache(p_request, p_logger, l_prop);
            }
            else if (l_key.equals(C_SHORT_BONS_BONUS_SCHEME))
            {
                l_cache = new BonsBonusSchemeCache(p_request, p_logger, l_prop);
            }
            else
            {
                throw new PpasConfigException(C_CLASS_NAME, C_METHOD_getCacheList, 12000, this, p_request, 0,
                                              ConfigKey.get().badConfigValue(C_BUSINESS_CONFIG_TABLES,
                                                                             l_key,
                                                                             "Valid Configuration Tables"));
            }
            
            l_map.put(l_key,l_cache);
        }
        
        return l_map;
    }
    
    /**
     * Loads the specifed cache, adding/replacing the chache in the supplied
     * <code>Caches</code> object.
     *
     * @param p_ppasRequest The request to process.
     * @param p_jdbcConn <code>JdbcConnection</code> used to connect to the database
     * @param p_whichCache cache to load, specified by constants <code>BusinessConfigCache.C_LOAD_*</code>
     * @throws PpasSqlException If the cache cannot be loaded
     * @deprecated Do not use - this may have inconsistencies if a thread reads whilst it is being loaded.
     */
    public void loadSpecified(PpasRequest    p_ppasRequest,
                              JdbcConnection p_jdbcConn,
                              String         p_whichCache)
        throws PpasSqlException
    {
        ConfigCache l_cache = (ConfigCache)i_cacheMap.get(p_whichCache);
        l_cache.load(p_ppasRequest, p_jdbcConn);
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_loadAll = "loadAll";
    /**
     * Loads all the cached business config. It calls the load method on each separate configuration cache.
     * The instance cache is not updated/changed until all caches have been loaded.
     * 
     * @param p_ppasRequest The request to process.
     * @param p_jdbcConn    Database Connection.
     * @throws PpasSqlException If the cache could not be loaded.
     */
    public void loadAll(
        PpasRequest         p_ppasRequest,
        JdbcConnection      p_jdbcConn)
        throws PpasSqlException
    {
        // Create a local version of the cache so cannot be read during a cache load.
        Map l_map;
        
        try
        {
            l_map = getCacheList(i_ppasContext, p_ppasRequest, i_logger);
        }
        catch (PpasConfigException e)
        {
            // This cannot happen - the exception will already have been thrown in the constructor so we
            // will never reach here. If it does happen then we will throw a Runtime exception (belt/braces).
            throw new PpasSoftwareException(C_CLASS_NAME, C_METHOD_loadAll, 23000, this, p_ppasRequest, 0,
                                            SoftwareKey.get().unexpectedException(), e);
        }
        
        Iterator l_it = l_map.values().iterator();
        
        while (l_it.hasNext())
        {
            ConfigCache l_cache = (ConfigCache)l_it.next();
            
            l_cache.load(p_ppasRequest, p_jdbcConn);
        }

        // Now set the instance attribute to point to the updated cache.
        i_cacheMap = l_map;
    }

    /**
     * Reloads all the cached business config. It calls the reload method on
     * each separate configuration cache. The existing configuration is only
     * replaced once all new configuration is successfully loaded into memory.
     * If problems occur, existing configuration is untouched.
     * 
     * @param p_ppasRequest The request to process.
     * @param p_jdbcConn    Database Connection.
     * @throws PpasSqlException If the cache could not be loaded.
     */
    public void reloadAll(
        PpasRequest         p_ppasRequest,
        JdbcConnection      p_jdbcConn)
        throws PpasSqlException
    {
        loadAll(p_ppasRequest, p_jdbcConn);
    }

    /** Returns the valid language business configuration cache.
     * 
     * @return Language cache.
     */
    public ValaValidLanguageCache getValaValidLanguageCache()
    {
        return (ValaValidLanguageCache)getCache(C_SHORT_VALA_VALID_LANGUAGE);
    }

    /** Returns the Promotion Plans business configuration cache.
     * 
     * @return Promotion Plan cache.
     */
    public PrplPromoPlanCache getPrplPromoPlanCache()
    {
        return (PrplPromoPlanCache)getCache(C_SHORT_PRPL_PROMO_PLAN);
    }

    /** Returns the customer master defaults business configuration cache.
     * 
     * @return Customer default cache.
     */
    public CmdfCustMastDefaultsCache getCmdfCustMastDefaultsCache()
    {
        return (CmdfCustMastDefaultsCache)getCache(C_SHORT_CMDF_CUST_MAST_DEFAULTS);
    }

    /** Returns the card groups business configuration cache.
     * 
     * @return Card Group cache.
     */
    public CdgrAcceCardGroupCache getCdgrAcceCardGroupCache()
    {
        return (CdgrAcceCardGroupCache)getCache(C_SHORT_CDGR_ACCE_CARD_GROUP);
    }

    /** Returns the company configuration cache.
     * 
     * @return Company Configuration cache.
     */
    public ConfCompanyConfigCache getConfCompanyConfigCache()
    {
        return (ConfCompanyConfigCache)getCache(C_SHORT_CONF_COMPANY_CONFIG);
    }

    /** Returns the Accumulator Cache.
     * 
     * @return Accumulator cache.
     */
    public AccfAccumulatorCache getAccfAccumulatorCache()
    {
        return (AccfAccumulatorCache)getCache(C_SHORT_ACCF_ACCUMULATOR);
    }

    /** Returns the System Config Cache.
     * 
     * @return System Configuration cache.
     */
    public SyfgSystemConfigCache getSyfgSystemConfigCache()
    {
        return (SyfgSystemConfigCache)getCache(C_SHORT_SYFG_SYSTEM_CONFIG);
    }

    /** Returns the bank details business configuration cache.
     * 
     * @return Bank cache.
     */
    public BankBankCache getBankBankCache()
    {
        return (BankBankCache)getCache(C_SHORT_BANK_BANK);
    }

    /** Returns the misc codes business configuration cache.
     * 
     * @return Miscellaneous Codes cache.
     */
    public MiscCodeCache getMiscCodeCache()
    {
        return (MiscCodeCache)getCache(C_SHORT_MISC_CODE);
    }

    /** Returns the operator access profiles business configuration cache.
     * 
     * @return GUI Operator Access cache.
     */
    public GopaGuiOperatorAccessCache getGopaGuiOperatorAccessCache()
    {
        return (GopaGuiOperatorAccessCache)getCache(C_SHORT_GOPA_GUI_OPERATOR_ACCESS);
    }

    /** Returns the currency formats business configuration cache.
     * 
     * @return Currency Format cache.
     */
    public CufmCurrencyFormatsCache getCufmCurrencyFormatsCache()
    {
        return (CufmCurrencyFormatsCache)getCache(C_SHORT_CUFM_CURRENCY_FORMATS);
    }

    /** Returns the adjustment codes business configuration cache.
     * 
     * @return Adjustment Code cache.
     */
    public SrvaAdjCodesCache getSrvaAdjCodesCache()
    {
        return (SrvaAdjCodesCache)getCache(C_SHORT_SRVA_ADJ_CODES);
    }

    /** Returns the agents business configuration cache.
     * 
     * @return Agent cache.
     */
    public SrvaAgentMstrCache getSrvaAgentMstrCache()
    {
        return (SrvaAgentMstrCache)getCache(C_SHORT_SRVA_AGENT_MSTR);
    }

    /** Returns the markets business configuration cache.
     * 
     * @return Market cache.
     */
    public SrvaMstrCache getSrvaMstrCache()
    {
        return (SrvaMstrCache)getCache(C_SHORT_SRVA_MSTR);
    }

    /** Returns the disconnect reasons business configuration cache.
     * 
     * @return Disconnection Reason cache.
     */
    public DireDisconnectReasonCache getDireDisconnectReasonCache()
    {
        return (DireDisconnectReasonCache)getCache(C_SHORT_DIRE_DISCONNECT_REASON);
    }

    /** Returns the payment profiles business configuration cache.
     * 
     * @return Payment Profile cache.
     */
    public PaprPaymentProfileCache getPaprPaymentProfileCache()
    {
        return (PaprPaymentProfileCache)getCache(C_SHORT_PAPR_PAYMENT_PROFILE);
    }

    /** Returns the SCP information business configuration cache.
     * 
     * @return SCP cache.
     */
    public ScpiScpInfoCache getScpiScpInfoCache()
    {
        return (ScpiScpInfoCache)getCache(C_SHORT_SCPI_SCP_INFO);
    }

    /** Returns the miscellaneous field titles business configuration cache.
     * 
     * @return Miscellaneous Fields cache.
     */
    public MfltMiscFieldTitlesCache getMfltMiscFieldTitlesCache()
    {
        return (MfltMiscFieldTitlesCache)getCache(C_SHORT_MFLT_MISC_FIELD_TITLES);
    }

    /** Returns the voucher splits (defining dedicated account divisions)
     *  business configuration cache.
     * 
     * @return Voucher Split cache.
     */
    public VospVoucherSplitsCache getVospVoucherSplitsCache()
    {
        return (VospVoucherSplitsCache)getCache(C_SHORT_VOSP_VOUCHER_SPLITS);
    }

    /** Returns the dedicated Accounts cache.
     * 
     * @return Dedicated Account cache.
     */
    public DedaDedicatedAccountsCache getDedaDedicatedAccountsCache()
    {
        return (DedaDedicatedAccountsCache)getCache(C_SHORT_DEDA_DEDICATED_ACCOUNTS);
    }

    /** Returns the number block business configuration cache.
     * 
     * @return Number Block cache.
     */
    public NublNumberBlockCache getNublNumberBlockCache()
    {
        return (NublNumberBlockCache)getCache(C_SHORT_NUBL_NUMBER_BLOCK);
    }

    /** Returns the F&F Barred List configuration cache.
     * 
     * @return F&F Barred List cache.
     */
    public FabaFafBarredListCache getFabaFafBarredListCache()
    {
        return (FabaFafBarredListCache)getCache(C_SHORT_FABA_FAF_BARRED_LIST);
    }

    /** Returns the F&F Charging Indicator configuration cache.
     * 
     * @return F&F Charging Indicator cache.
     */
    public FachFafChargingIndCache getFachFafChargingIndCache()
    {
        return (FachFafChargingIndCache)getCache(C_SHORT_FACH_FAF_CHARGING_IND);
    }
    
    /**
     * Returns the default charging indicator cache.
     * 
     * @return default charging ind cache
     */
    public DeciDefaultChargingIndCache getDeciDefaultChargingIndCache()
    {
        return (DeciDefaultChargingIndCache)getCache(C_SHORT_DECI_DEFAULT_CHARGING_IND);
    }

    /**
     * Returns the Comment cache.
     * 
     * @return Comment description cache.
     */
    public SrvaFieldsCache getSrvaFieldsCache()
    {
        return (SrvaFieldsCache)getCache(C_SHORT_SRVA_FIELDS);
    }

    /** Returns the Emu Conversion Rates cache.
     * 
     * @return Emu Conversion Rates cache.
     */
    public EmuConversionRatesCache getEmuConversionRatesCache()
    {
        return (EmuConversionRatesCache)getCache(C_SHORT_EMCR_EMU_CONVERSION_RATES);
    }

    /** Returns the asce error codes cache.
     * 
     * @return asce error codes cache.
     */
    public AsceAscsErrorCodesCache getAsceErrorCodesCache()
    {
        return (AsceAscsErrorCodesCache)getCache(C_SHORT_ASCE_ERROR_CODES);
    }
    /** Returns the scap service class cache.
     * 
     * @return scap service class cache.
     */
    public ScapServClassAdjParamCache getScapAdjParamCache()
    {
        return (ScapServClassAdjParamCache)getCache(C_SHORT_SCAP_ADJ_PARAM);
    }
    /** Returns the number block business configuration cache.
     * 
     * @return Number Block cache.
     */
    public AcgrAccountGroupCache getAcgrAccountGroupCache()
    {
        return (AcgrAccountGroupCache)getCache(C_SHORT_ACGR_ACC_GROUP);
    }
    /** Returns the service offerings cache.
     * 
     * @return Service Offering cache.
     */
    public SeofServiceOfferingsCache getSeofServiceOfferingCache()
    {
        return (SeofServiceOfferingsCache)getCache(C_SHORT_SEOF_SERV_OFF);
    }

    /** Returns the Home Region Ids cache.
     * 
     * @return Home Region Ids cache.
     */
    public RegiRegionCache getRegiRegionCache()
    {
        return (RegiRegionCache)getCache(C_SHORT_REGI_REGION);
    }
    
    /** 
     * Returns the community charging cache.
     * 
     * @return Community Charging cache.
     */
    public CochCommunityChargingCache getCochCommunityChargingCache()
    {
        return (CochCommunityChargingCache)getCache(C_SHORT_COCH_COMM_CHG);
    }
    
    /**
     * Returns the Teleservice cache.
     * 
     * @return TeleTeleserviceCache.
     */
    public TeleTeleserviceCache getTeleTeleserviceCache()
    {
        return (TeleTeleserviceCache)getCache(C_SHORT_TELESERVICE);
    }

    /**
     * Returns the Traffic case cache.
     * 
     * @return TrafTrafficCaseCache.
     */
    public TrafTrafficCaseCache getTrafTrafficCaseCache()
    {
        return (TrafTrafficCaseCache)getCache(C_SHORT_TRAFFIC_CASE);
    }
    
    /**
     * Returns the Traffic case cache.
     * 
     * @return TrafTrafficCaseCache.
     */
    public ChavChannelCache getChavChannelCache()
    {
        return (ChavChannelCache)getCache(C_SHORT_CHAV_CHANNEL);
    }
    
    /**
     * Returns the Bonus Accumulation Parameters cache.
     * @return BonaBonusAccumParamsCache.
     */
    public BonaBonusAccumParamsCache getBonaBonusAccumParamsCache()
    {
        return (BonaBonusAccumParamsCache)getCache(C_SHORT_BONA_BONUS_ACCUM_PARAMS);
    }
    
    /**
     * Returns the Traffic case cache.
     * 
     * @return TrafTrafficCaseCache.
     */
    public BoneBonusElementCache getBoneBonusElementCache()
    {
        return (BoneBonusElementCache)getCache(C_SHORT_BONE_BONUS_ELEMENTS);
    }
    
    /**
     * Returns the Traffic case cache.
     * 
     * @return TrafTrafficCaseCache.
     */
    public BonsBonusSchemeCache getBonsBonusSchemeCache()
    {
        return (BonsBonusSchemeCache)getCache(C_SHORT_BONS_BONUS_SCHEME);
    }
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getCache = "getCache";
    
    /** Check if some cache has been loaded and error if not. This will stop a null pointer exception
     * when accessing cache that has not been loaded.
     * 
     * @param p_shortTableName Name of table (short version).
     * @return The cache.
     */
    private ConfigCache getCache(String p_shortTableName)
    {
        ConfigCache l_cache = (ConfigCache)i_cacheMap.get(p_shortTableName);
        
        if (l_cache == null)
        {
            PpasSoftwareException l_exception = new PpasSoftwareException(
                                            C_CLASS_NAME, C_METHOD_getCache, 23000, this, null, 0,
                                            SoftwareKey.get().configCacheNotFound(p_shortTableName));
            if (i_logger != null)
            {
                i_logger.logMessage (l_exception);
            }
            throw l_exception;
        }
        
        return l_cache;
    }

    /** Check if some cache has been loaded.
     * 
     * @param p_shortTableName Name of table (short version).
     * @return True if the cache has been loaded.
     */
    public boolean isCacheLoaded(String p_shortTableName)
    {
        return i_cacheMap.get(p_shortTableName) != null;
    }

    /**
     * Returns a verbose string representation of the object. Can be used for
     * trace and debugging.
     * 
     * @return String representation of this object.
     */
    public String toVerboseString()
    {
        StringBuffer           l_verboseSB;

        l_verboseSB = new StringBuffer(10000);

        l_verboseSB.append("BusinessConfigCache=[\n\n");

        Iterator l_it = i_cacheMap.values().iterator();
        
        while (l_it.hasNext())
        {
            ConfigCache l_cache = (ConfigCache)l_it.next();
            
            l_verboseSB.append(l_cache.toVerboseString());
            l_verboseSB.append("\n\n");
        }

        l_verboseSB.append("]");

        return(l_verboseSB.toString());
    }
}
