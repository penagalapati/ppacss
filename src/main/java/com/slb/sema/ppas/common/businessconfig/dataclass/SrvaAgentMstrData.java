////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaAgentMstrData.Java
//      DATE            :       15-Jul-2002
//      AUTHOR          :       Nick Fletcher
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       A record of agents data 
//                              (corresponding to a single row of the
//                              srva_agent_mstr table).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 15/09/03 | Remi       | Remove i_agentType, i_agentLevel| CR#15/440
//          | Isaacs     | i_agentGuideTo, i_userDept and  |
//          |            | corresponding get methods       |
//          |            | to reflect PRD_ASCS00_SYD_AS_003|
//----------+------------+---------------------------------+--------------------
// 30/03/07 | Andy Harris| Add support for suspect flag    | PpacLon#3011/11241
//          |            | column.                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * An object that represents a single record of agent data. That is,
 * a single row of the srva_agent_mstr table.
 */
public class SrvaAgentMstrData
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaAgentMstrData";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The market that this agent record belongs to.
     */
    private  Market            i_market = null;

    /** The agent identifier. */
    private  String            i_agent = null;

    /** Name of this agent. */
    private  String            i_agentName = null;

    /** Date this agent record was created/last accessed in the database. */
    private  PpasDateTime      i_genYmdhms = null;

    /** Operator Id who created/lst accessed this agent record in the database. */
    private  String            i_opid = null;
    
    /** Flag indicating if this agent is deleted. */
    private  char              i_delFlag = ' ';
    
    /** Flag indicating suspect status. */
    private  char              i_susFlag = ' ';

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates agent data object and initialises instance variables.
     * 
     * @param p_request The request to process.
     * @param p_market  The market to which this data is associated.
     * @param p_agent   The agent identifier.
     * @param p_agentName The name of the agent.
     * @param p_genYmdhms The date/time this data was last changed.
     * @param p_opid    The operator that last changed this data.
     * @param p_delFlag Flag indicating whether this data is marked as deleted.
     * @param p_susFlag Flag indicating suspect status.
     */
    public SrvaAgentMstrData(
        PpasRequest            p_request,
        Market                 p_market,
        String                 p_agent,
        String                 p_agentName,
        PpasDateTime           p_genYmdhms,
        String                 p_opid,
        char                   p_delFlag,
        char                   p_susFlag)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION,
                            PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME + " with Market: " + p_market + ", Agent: " + p_agent + ", Name: " +
                            p_agentName + ", Suspect ? " + p_susFlag + ", Deleted ? " + p_delFlag);
        }

        i_market       = p_market;
        i_agent        = p_agent;
        i_agentName    = p_agentName;
        i_genYmdhms    = p_genYmdhms;
        i_opid         = p_opid;
        i_delFlag      = p_delFlag;
        i_susFlag      = p_susFlag;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SESSION, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10090, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Get the market that this agent record belongs to.
     * 
     * @return The market to which this data is associated.
     */
    public Market getMarket ()
    {
        return i_market;
    }

    /** Get the agent identifier.
     * 
     * @return The identifier of this agent.
     */
    public String getAgent()
    {
        return i_agent;
    }

    /** Get the name of this agent.
     * 
     * @return The name of this agent.
     */
    public String getAgentName()
    {
        return i_agentName;
    }

    /** Get date this agent record was created/last accessed in the database.
     * 
     * @return The date/time this data was last updated.
     */
    public PpasDateTime getGenYmdhms()
    {
        return i_genYmdhms;
    }

    /** Get operator Id who created/lst accessed this agent record in the database.
     * 
     * @return The operator who last changed this data.
     */
    public String getOpid()
    {
        return i_opid;
    }

    /** Returns true if this agent has been marked as deleted.
     * 
     * @return Flag indicating whether the data is marked as deleted or not.
     */
    public boolean isDeleted()
    {
        return i_delFlag == '*';
    }
    
    /** Get suspect flag for this agent.
     * 
     * @return The suspect flag for this agent.
     */
    public char getSuspectFlag()
    {
        return i_susFlag;
    }

    /** Is this agent suspect ?
     * 
     * @return True if this agent is suspect, otherwise false.
     */
    public boolean isSuspect()
    {
        return i_susFlag == 'Y';
    }

    /** 
     * Returns a String containing the agent name padded to p_codeWidth 
     * characters and the description.
     * 
     * @param p_codeWidth Defines width of field to display agent in including
     *                    trailing spaces. Maximum value is 10 characters.
     * @return The agent identifier and name in a manner that can be dislayed on a GUI. 
     */
    public String getDisplayString(int p_codeWidth)
    {
        int l_codeWidth = (p_codeWidth > 10) ? 10 : p_codeWidth;
        String l_agent = i_agent + "          ";
        
        return (l_agent.substring(0, l_codeWidth) + i_agentName);
    }
    
    /** String representation of an Agent data record.
     * @return String representation of this agent.
     */
    public String toString()
    {
        return (i_agent + " - " + i_agentName);
    }

    /**
     * Compares this object with another SrvaAgentMstrData object.
     * @param p_agentData Object to compare with.
     * @return True if objects represent the same agent, false otherwise.
     */
    public boolean equals(Object p_agentData)
    {
        boolean l_result = false;
        
        if (p_agentData instanceof SrvaAgentMstrData)
        {
            l_result = i_agent.equals(((SrvaAgentMstrData)p_agentData).getAgent());
        }

        return l_result;
    }
}
