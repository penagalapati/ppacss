////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdjustmentHistoryDataSet.Java
//      DATE            :       01-Aug-2005
//      AUTHOR          :       MAGray
//      REFERENCE       :       PRD_PPAK00_GEN_CA_48
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Super class for sets of adjustments.
//
////////////////////////////////////////////////////////////////////////////////
//                      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Super class for sets of adjustments. */
public class AdjustmentHistoryDataSet extends EventHistoryDataSet
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AdjustmentHistoryDataSet";

    /** Empty array for getting underlying data set as an array. */
    private static final AdjustmentHistoryData[] C_EMPTY_ARRAY = new AdjustmentHistoryData[0];
    
    /** Standard constructor.
     * 
     * @param p_request The request being processed.
     */
    public AdjustmentHistoryDataSet(PpasRequest p_request)
    {
        super(p_request);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructing " + C_CLASS_NAME);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_BUSINESS, PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10020, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    /** Gets the whole set of adjustment history as an array.
     * 
     * @return Array of all the adjustment history.
     */
    public AdjustmentHistoryData[] getAdjustmentHistoryArr()
    {
        return (AdjustmentHistoryData[])asArray(C_EMPTY_ARRAY);
    }
}
