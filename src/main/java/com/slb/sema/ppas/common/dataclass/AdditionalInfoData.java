////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       AdditionalInfoData.Java
//      DATE            :       30-January-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1218/4958
//
//      COPYRIGHT       :       ATOS ORIGIN 2005
//
//      DESCRIPTION     :       Class containing customer miscellaneous data
//                              which is related to MlftMiscFieldTitles.
//                              This Data relates to the database
//                              table cust_misc.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.MfltMiscFieldTitlesData;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
/**
 * Class containing customer miscellaneous data which is related to 
 * MfltMiscFieldTitles. This data relates to the database table cust_misc.
 */
public class AdditionalInfoData extends DataObject
{

    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AdditionalInfoData";

    //------------------------------------------------------------------------
    // Instance Variables.
    //------------------------------------------------------------------------

    /** Subscriber Identifier. */
    private     String                   i_subscriberId        = null;

    /** Vector of miscellaneous fields (1-40). */
    private     Vector                   i_miscFieldV;

    /** The operator identifier of the operator that either created the row
     *  or performed the last update. 
     */
    private     String                   i_opid                 = null;

    /** The date and time of the last modification. */
    private     PpasDateTime i_genYmdhms;

    /** MfltMiscFieldTitlesData Object for this customers market. */
    private     MfltMiscFieldTitlesData  i_miscFieldTitlesData  = null;

    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------

    /**
     * Creates new additional information details and initialises data.
     * 
     * @param p_request The request to process.
     */
    public AdditionalInfoData (PpasRequest p_request)
    {
        this ( p_request,
               (String)null,
               (Vector)null,
               (String)null,
               (PpasDateTime)null);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 11010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 11050, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor AdditionalInfoData

    /**
     * Creates a new row of AdditionalInfoData and initialises data.
     * 
     * @param p_request The request to process.
     * @param p_subscriberId Internal identifier of the mobile phone user.
     * @param p_miscFieldV   Miscellaneous field values.
     * @param p_opid         Identifier of the operator that last changed this data.
     * @param p_genYmdhms    Date/time this data was last changed.
     */
    public AdditionalInfoData ( 
                  PpasRequest  p_request,
                  String       p_subscriberId,
                  Vector       p_miscFieldV,
                  String       p_opid,
                  PpasDateTime p_genYmdhms )
    {
        super(p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_subscriberId = p_subscriberId;
        i_miscFieldV   = p_miscFieldV;
        i_opid         = p_opid;
        i_genYmdhms    = p_genYmdhms;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10050, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
      //         AdditionalInfoData(PpasRequest)

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** Returns the subscriber identifier.
     * 
     * @return Internal identifier of the mobile phone user.
     */
    public String getSubscriberId()
    {
        return(i_subscriberId);
    }

    /** Returns a vector of 40 miscellaneous fields.
     * 
     * @return Set of values for the miscellaneous fields.
     */
    public Vector getMiscFieldV()
    {
        return(i_miscFieldV);
    }

    /** Returns the operator identifier.
     * 
     * @return Identifier of the operator that last changed this data.
     */
    public String getOpid()
    {
        return(i_opid);
    }
        
    /** Returns the last modification date/time.
     * 
     * @return Date/time this data was last changed.
     */
    public PpasDateTime getGenYmdhms()
    {
        return(i_genYmdhms);
    }

    /** Returns the MiscFieldTitlesData object for this customers market.
     * 
     * @return Titles of the miscellaneous fields.
     */
    public MfltMiscFieldTitlesData getMiscFieldTitlesData()
    {
        return(i_miscFieldTitlesData);
    }

    /** Set the Subscriber id.
     * 
     * @param p_subscriberId Internal identifier of the mobile phone user.
     */
    public void setSubscriberId( String p_subscriberId )
    {
        i_subscriberId = p_subscriberId;
    }

    /** Set the operator id.
     * 
     * @param p_opid Identifier of the operator that changed this data.
     */
    public void setOpid( String p_opid )
    {
        i_opid = p_opid;
    }

    /** Set the miscellaneous field Vector.
     * 
     * @param p_miscFieldV Set of miscellaneous field values.
     */
    public void setMiscFieldV( Vector p_miscFieldV )
    {
        i_miscFieldV = p_miscFieldV;
    }

    /** Set the MiscFieldTitlesData object for this customers market.
     * 
     * @param p_miscFieldTitlesData Titles for the miscellaneous fields.
     */
    public void setMiscFieldTitlesData(
                   MfltMiscFieldTitlesData   p_miscFieldTitlesData)
    {
        i_miscFieldTitlesData = p_miscFieldTitlesData;
    }

    /** Returns the number of fields that have been populated.
     * 
     * @return Number of fields that have been populated.
     */
    public int getFieldCount()
    {
        int l_count = 0;

        for (int l_i = 0; l_i < i_miscFieldV.size(); l_i++)
        {
            if ( !(i_miscFieldV.get(l_i).equals("")) )
            {
                l_count++;
            }
        }

        return (l_count);
    }

} // End of public class AdditionalInfoData

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////
