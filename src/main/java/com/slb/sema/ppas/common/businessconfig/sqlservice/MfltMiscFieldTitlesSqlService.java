////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MfltMiscFieldTitlesSqlService.Java
//      DATE            :       29-January-2002
//      AUTHOR          :       Sally Wells
//      REFERENCE       :       PpaLon#1218/4955
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Service to return a MfltMiscFieldTitlesData
//                              object from the database
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.MfltMiscFieldTitlesData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MfltMiscFieldTitlesDataSet;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Service to run SQL to obtain miscellaneous field titles details from the
 * database table mflt_misc_field_titles.
 */
public class MfltMiscFieldTitlesSqlService
{

    //------------------------------------------------------------------------
    // Class level Constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "MfltMiscFieldTitlesSqlService";

    //------------------------------------------------------------------------
    // Constructors and Finalizers
    //------------------------------------------------------------------------

    /**
     * Creates a new miscellaneous field titles sql service.
     * 
     * @param p_request The request to process.
     */
    public MfltMiscFieldTitlesSqlService(
        PpasRequest            p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 98010, this,
                "Constructing " + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 28438, this,
                "Constructed " + C_CLASS_NAME );
        }

    } // End of public constructor
      //         MfltMiscFieldTitlesSqlService(PpasRequest, long, Logger)
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readMiscFieldTitles =
                                        "readMiscFieldTitles";
    /**
     * Read the miscellaneous field titles details from the database.
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return MfltMiscFieldTitlesData
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public MfltMiscFieldTitlesDataSet readMiscFieldTitles(
        PpasRequest                p_request,
        JdbcConnection             p_connection)
        throws PpasSqlException
    {
        JdbcStatement              l_statement   = null;
        JdbcResultSet              l_result      = null;
        SqlString                  l_sql         = null;

        MfltMiscFieldTitlesData    l_miscFieldTitlesData    = null;
        MfltMiscFieldTitlesDataSet l_miscFieldTitlesDataSet = null;
        Vector                     l_fieldTitlesV;
        int                        l_srva;
        int                        l_sloc;
        Market                     l_market;


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                p_request, C_CLASS_NAME, 84650, this,
                "Entered " + C_METHOD_readMiscFieldTitles);
        }

        l_sql =   new SqlString(2000, 0, "SELECT mflt_srva, "         +
                         "mflt_sloc, "         +
                         "mflt_title, "        +
                         "mflt_field_title1, " +
                         "mflt_field_title2, " +
                         "mflt_field_title3, " +
                         "mflt_field_title4, " +
                         "mflt_field_title5, " +
                         "mflt_field_title6, " +
                         "mflt_field_title7, " +
                         "mflt_field_title8, " +
                         "mflt_field_title9, " +
                         "mflt_field_title10, " +
                         "mflt_field_title11, " +
                         "mflt_field_title12, " +
                         "mflt_field_title13, " +
                         "mflt_field_title14, " +
                         "mflt_field_title15, " +
                         "mflt_field_title16, " +
                         "mflt_field_title17, " +
                         "mflt_field_title18, " +
                         "mflt_field_title19, " +
                         "mflt_field_title20, " +
                         "mflt_field_title21, " +
                         "mflt_field_title22, " +
                         "mflt_field_title23, " +
                         "mflt_field_title24, " +
                         "mflt_field_title25, " +
                         "mflt_field_title26, " +
                         "mflt_field_title27, " +
                         "mflt_field_title28, " +
                         "mflt_field_title29, " +
                         "mflt_field_title30, " +
                         "mflt_field_title31, " +
                         "mflt_field_title32, " +
                         "mflt_field_title33, " +
                         "mflt_field_title34, " +
                         "mflt_field_title35, " +
                         "mflt_field_title36, " +
                         "mflt_field_title37, " +
                         "mflt_field_title38, " +
                         "mflt_field_title39, " +
                         "mflt_field_title40, " +
                         "mflt_opid, "          +
                         "mflt_gen_ymdhms "     +
                  "FROM   mflt_misc_field_titles");

        l_miscFieldTitlesDataSet = new MfltMiscFieldTitlesDataSet(p_request, 3, 2);

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readMiscFieldTitles,
                                       29000, this, p_request);

            l_result = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readMiscFieldTitles,
                                       29100, this, p_request, l_sql);

            while ( l_result.next(51000) )
            {
                l_fieldTitlesV = new Vector(40, 0);

                // Add the field title Strings to the field titles Vector.

                l_fieldTitlesV.addElement(l_result.getTrimString( 44700, "mflt_field_title1"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44702, "mflt_field_title2"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44705, "mflt_field_title3"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44708, "mflt_field_title4"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44710, "mflt_field_title5"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44712, "mflt_field_title6"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44715, "mflt_field_title7"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44718, "mflt_field_title8"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44720, "mflt_field_title9"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44722, "mflt_field_title10"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44725, "mflt_field_title11"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44728, "mflt_field_title12"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44730, "mflt_field_title13"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44732, "mflt_field_title14"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44735, "mflt_field_title15"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44738, "mflt_field_title16"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44740, "mflt_field_title17"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44742, "mflt_field_title18"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44745, "mflt_field_title19"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44748, "mflt_field_title20"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44750, "mflt_field_title21"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44752, "mflt_field_title22"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44755, "mflt_field_title23"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44758, "mflt_field_title24"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44760, "mflt_field_title25"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44762, "mflt_field_title26"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44765, "mflt_field_title27"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44768, "mflt_field_title28"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44770, "mflt_field_title29"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44772, "mflt_field_title30"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44775, "mflt_field_title31"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44778, "mflt_field_title32"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44780, "mflt_field_title33"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44782, "mflt_field_title34"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44785, "mflt_field_title35"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44788, "mflt_field_title36"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44790, "mflt_field_title37"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44792, "mflt_field_title38"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44795, "mflt_field_title39"));
                l_fieldTitlesV.addElement(l_result.getTrimString( 44798, "mflt_field_title40"));

                l_srva   = l_result.getInt(        44810, "mflt_srva");
                l_sloc   = l_result.getInt(        44800, "mflt_sloc");
                l_market = new Market(p_request, l_srva, l_sloc, "");

                l_miscFieldTitlesData =
                    new MfltMiscFieldTitlesData(
                            p_request,
                            l_market,
                            l_result.getTrimString( 44820, "mflt_title"),
                            l_fieldTitlesV,
                            l_result.getTrimString( 44830, "mflt_opid"),
                            l_result.getDateTime(   44840, "mflt_gen_ymdhms") );

                // Add the miscellaneous field titles data object to the
                // miscellaneous field titles data set.

                l_miscFieldTitlesDataSet.addMiscFieldTitle( l_miscFieldTitlesData);

            } // end of while next
        }
        finally
        {
            if (l_result != null)
            {
                l_result.close(53280);
            }

            if (l_statement != null)
            {
                l_statement.close( C_CLASS_NAME,
                                   C_METHOD_readMiscFieldTitles,
                                   90280,
                                   this,
                                   p_request);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                p_request, C_CLASS_NAME, 10140, this,
                "Leaving " + C_METHOD_readMiscFieldTitles);
        }

        return(l_miscFieldTitlesDataSet);

    } // End of public method
      // readMiscFieldTitles(PpasRequest, long, JdbcConnection)
} // End of public class MfltMiscFieldTitlesSqlService

////////////////////////////////////////////////////////////////////////////////
//                             End of file
////////////////////////////////////////////////////////////////////////////////