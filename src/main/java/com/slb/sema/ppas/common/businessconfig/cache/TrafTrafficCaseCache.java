////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       TrafTrafficCaseCache.java
//      DATE            :       21-Mars-2006
//      AUTHOR          :       Marianne Toernqvist
//      REFERENCE       :       PpaLon#2020/8240, PRD_ASCS00_GEN_CA_66
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       TrafTrafficCaseCache, stores data for the business cache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.TrafTrafficCaseDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.TrafTrafficCaseSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

public class TrafTrafficCaseCache extends ConfigCache
{

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "TrafTrafficCaseCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**
     * The Sql service to use to load cache.
     */
    private TrafTrafficCaseSqlService i_TrafTrafficCaseSqlService = null;

    /** Data set containing all configured teleservice records. */
    private TrafTrafficCaseDataSet    i_allTrafficCases           = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Creates a new Tele Teleservice Data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_ppasContext The PPAS context containing business and system configuration
     *         data required by this service.
     */
    public TrafTrafficCaseCache(PpasRequest            p_request,
                                Logger                 p_logger,
                                PpasContext            p_ppasContext)
    {
        super(p_logger, p_ppasContext.getProperties());
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 82110, this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_TrafTrafficCaseSqlService = new TrafTrafficCaseSqlService(p_request,
                                                                    p_logger,
                                                                    p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END, p_request, C_CLASS_NAME, 82120, this,
                            "Constructed " + C_CLASS_NAME);
        }

    } // End of public constructor
    

    
    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------
    /**
     * Returns all account group information.
     * @return Data set containing all account group data configured in the
     *         ACGR_ACCOUNT_GROUP table.
     */
    public TrafTrafficCaseDataSet getAll()
    {
        return i_allTrafficCases;

    } // End of public method getAll

    
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 82230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allTrafficCases = i_TrafTrafficCaseSqlService.readAll(p_request, p_connection);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 82290, this,
                "Leaving " + C_METHOD_reload);
        }
    } // End of method reload

    
    
    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allTrafficCases);
    }

}
