////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DeciDefaultChargingIndSqlService.java
//      DATE            :       17-Mar-2004
//      AUTHOR          :       Neil Raymond (41087r)
//      REFERENCE       :       PRD_ASCS_DEV_SS_?
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Sql service to retrieve data from
//                              deci_default_charging_ind
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 18/11/05 | K Goswami  | Added the insert, update, delete| PpacLon#1838/7448
//          |            | and MarkAsAvailable methods     |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.sqlservice;

import java.util.Vector;

import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DeciDefaultChargingIndDataSet;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlKey;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * Sql service to retrieve data from deci_default_charging_ind table.
 */
public class DeciDefaultChargingIndSqlService
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DeciDefaultChargingIndSqlService";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new default charging indicators sql service.
     * 
     * @param p_request The request to process.
     */
    public DeciDefaultChargingIndSqlService(PpasRequest p_request)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            p_request, C_CLASS_NAME, 10000, this,
                            "Constructing " + C_CLASS_NAME);
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            p_request, C_CLASS_NAME, 10010, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_readAll = "readAll";
    /**
     * Reads and returns all valid Charging Indicators (including ones marked as deleted).
     * 
     * @param p_request The request to process.
     * @param p_connection Database Connection.
     * @return Data set of all valid Tele Services.
     * @throws PpasSqlException If the data cannot be read.
     */
    public DeciDefaultChargingIndDataSet readAll(PpasRequest    p_request,
                                                 JdbcConnection p_connection)
        throws PpasSqlException
    {
        JdbcStatement                 l_statement = null;
        SqlString                     l_sql;
        
        int                           l_chargingInd = 0;
        String                        l_chargingIndDesc = "";
        String                        l_numberStart = null;
        String                        l_numberEnd = null;
        char                          l_delFlag;
        String                        l_opid;
        PpasDateTime                  l_genYmdhms;

        DeciDefaultChargingIndData    l_deciDefaultChargingIndData;
        Vector                        l_allDeciDataV = new Vector(20, 10);
        DeciDefaultChargingIndDataSet l_deciDefaultChargingIndDataSet;
        DeciDefaultChargingIndData[]  l_allDeciChargingIndARR;
        DeciDefaultChargingIndData[]  l_deciDefaultChargingIndEmptyARR = new DeciDefaultChargingIndData[0];
        JdbcResultSet                 l_results;
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 10100, this,
                            "Entered " +  C_METHOD_readAll);
        }
        
        l_sql =   new SqlString(500,
                                0,
                                "SELECT " +
                                "deci_number_series_start, " +
                                "deci_number_series_end, " +
                                "deci_charging_ind, " +
                                "fach_charging_ind_desc, " +
                                "deci_del_flag, " +
                                "deci_opid, " +
                                "deci_gen_ymdhms " +
                                "FROM deci_default_charging_ind, " +
                                "fach_faf_charging_ind " +
                                "WHERE fach_charging_ind = deci_charging_ind " +
                                "ORDER BY deci_number_series_start");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_SQL, p_request, C_CLASS_NAME, 10110, this,
                            "About to execute SQL " + l_sql);
        }

        // Create the statement object and exceute the SQL query
        l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_readAll, 10120, this, p_request);

        l_results = l_statement.executeQuery(C_CLASS_NAME, C_METHOD_readAll, 10130, this, p_request, l_sql);

        while ( l_results.next(10140) )
        {
            l_numberStart      = l_results.getTrimString(10142, "deci_number_series_start");
            l_numberEnd        = l_results.getTrimString(10145, "deci_number_series_end");
            l_chargingInd      = l_results.getInt       (10150, "deci_charging_ind");
            l_chargingIndDesc  = l_results.getTrimString(10160, "fach_charging_ind_desc");
            l_delFlag          = l_results.getChar      (10190, "deci_del_flag");
            l_opid             = l_results.getTrimString(10200, "deci_opid");
            l_genYmdhms        = l_results.getDateTime  (10210, "deci_gen_ymdhms");

            l_deciDefaultChargingIndData = new DeciDefaultChargingIndData(l_numberStart,
                                                                          l_numberEnd,
                                                                          "" + l_chargingInd,
                                                                          l_chargingIndDesc,
                                                                          (l_delFlag == '*'),
                                                                          l_opid,
                                                                          l_genYmdhms);
                    

            l_allDeciDataV.addElement(l_deciDefaultChargingIndData);
        }

        l_results.close(10230);

        l_statement.close(C_CLASS_NAME, C_METHOD_readAll, 10240, this, p_request);

        l_allDeciChargingIndARR = (DeciDefaultChargingIndData[])l_allDeciDataV.toArray(
                                l_deciDefaultChargingIndEmptyARR);

        l_deciDefaultChargingIndDataSet = new DeciDefaultChargingIndDataSet(l_allDeciChargingIndARR);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 10250, this,
                            "Leaving " + C_METHOD_readAll);
        }

        return l_deciDefaultChargingIndDataSet;
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_insert = "insert";
    
    /**
     * Attempts to insert a new row into the DECI__DEFAULT_CHARGING_IND database table.
     * @param p_request    The request being processed.
     * @param p_connection Database connection.
     * @param p_opid       Operator making the change.
     * @param p_startNo    Start number.
     * @param p_endNo      End Number.
     * @param p_deciChargInd Charging Indicator.
     * @return Number of rows inserted.
     * @throws PpasSqlException If an SQL error occurs.
     */
    public int insert(PpasRequest p_request, 
                       JdbcConnection p_connection, 
                       String p_opid, 
                       String p_startNo,
                       String p_endNo,
                       String p_deciChargInd) 
    throws PpasSqlException
    {
        JdbcStatement    l_statement = null;       
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 16000, this,
                            "Entered " +  C_METHOD_insert);
        }
            
        l_sql = "INSERT INTO deci_default_charging_ind " +
                "(DECI_NUMBER_SERIES_START," +
                " DECI_NUMBER_SERIES_END," +
                " DECI_CHARGING_IND," +
                " DECI_DEL_FLAG," +
                " DECI_OPID," +
                " DECI_GEN_YMDHMS)" +
                "VALUES( {0}, {1}, {2}, ' ', {3}, {4})";
                       
        l_sqlString = new SqlString(150, 4, l_sql);
           
        l_sqlString.setStringParam(0, p_startNo);
        l_sqlString.setStringParam(1, p_endNo);
        l_sqlString.setIntParam(2, p_deciChargInd);
        l_sqlString.setStringParam(3, p_opid);
        l_sqlString.setDateTimeParam(4, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_insert, 16050, this, p_request);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_insert,
                                                   16100,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }        
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_insert, 16600, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10040, this,
                                "Database error: unable to insert row in DECI_DEFAULT_CHARGING_IND table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_insert,
                                       10050,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 17590, this,
                            "Leaving " + C_METHOD_insert);
        }

        return l_rowCount;
    }
    
    /** Used in call to middleware. */
    private static final String C_METHOD_delete = "delete";
    /**
     * Mark a row as having been deleted in DECI_DEFAULT_CHARGING_IND.
     * @param p_request     The request being processed.
     * @param p_connection  Database connection.
     * @param p_opid        Operator identifier.
     * @param p_startNo     Identifier of the DECI record.
     * @throws PpasSqlException
     */
    public void delete(PpasRequest p_request, 
                       JdbcConnection p_connection, 
                       String p_opid, 
                       String p_startNo) 
        throws PpasSqlException
    {
        JdbcStatement    l_statement  = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 32100, this,
                            "Entered " + C_METHOD_delete);
        }

        l_sql = new String("UPDATE DECI_DEFAULT_CHARGING_IND " +
                           "SET DECI_DEL_FLAG = '*', " +
                           "DECI_OPID = {1}, " +
                           "DECI_GEN_YMDHMS = {2}" +
                           "WHERE DECI_NUMBER_SERIES_START = {0}");

        l_sqlString = new SqlString(150, 3, l_sql);
            
        l_sqlString.setStringParam(0, p_startNo);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());
        try
        {
            l_statement = p_connection.createStatement(C_CLASS_NAME, C_METHOD_delete, 32300, this, p_request);

            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_delete,
                                                   32400,
                                                   this,
                                                   p_request,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_delete, 32600, this, p_request);
            }
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 32610, this,
                                "Database error: unable to mark row as deleted in " +
                                "DECI_DEFAULT_CHARGING_IND table.");
            }

            throw new PpasSqlException(C_CLASS_NAME,
                                       C_METHOD_delete,
                                       32620,
                                       this,
                                       p_request,
                                       0,
                                       SqlKey.get().databaseInconsistency());
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 32700, this,
                            "Leaving " + C_METHOD_delete);
        }
    }
  
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_update = "update";
    /**
     * Attempts to update a row in the DECI_DEFAULT_CHARGING_IND database table.
     * @param p_request    The request being processed.
     * @param p_connection Database connection.
     * @param p_opid       Operator making the change.
     * @param p_startNo    Start number.
     * @param p_endNo      End Number.
     * @param p_deciChargInd Charging Indicator.
     * @return Number of rows updated.
     * @throws PpasSqlException If an SQL error occurs.
     */
    public int update(PpasRequest p_request, 
                       JdbcConnection p_connection, 
                       String p_opid, 
                       String p_startNo,
                       String p_endNo,
                       String p_deciChargInd) 
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;        
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            p_request, C_CLASS_NAME, 20000, this,
                            "Entered " +  C_METHOD_update);
        }
    
        l_sql = new String("UPDATE DECI_DEFAULT_CHARGING_IND " +
                           "SET DECI_DEL_FLAG = ' ', " +
                           "DECI_NUMBER_SERIES_END = {1}, " +
                           "DECI_CHARGING_IND = {2}, " +
                           "DECI_OPID = {3}, " +
                           "DECI_GEN_YMDHMS = {4}" +
                           "WHERE DECI_NUMBER_SERIES_START = {0}");
                       
        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_startNo);
        l_sqlString.setStringParam(1, p_endNo);
        l_sqlString.setIntParam(2, p_deciChargInd);
        l_sqlString.setStringParam(3, p_opid);
        l_sqlString.setDateTimeParam(4, DatePatch.getDateTimeNow());

        try
        {        
            l_statement = p_connection.createStatement(C_CLASS_NAME,
                                                       C_METHOD_update,
                                                       20050,
                                                       this,
                                                       (PpasRequest)null);
       
            l_rowCount = l_statement.executeUpdate(C_CLASS_NAME,
                                                   C_METHOD_update,
                                                   20100,
                                                   this,
                                                   (PpasRequest)null,
                                                   l_sqlString);
        }
        finally
        {
            if (l_statement != null)
            {
                l_statement.close(C_CLASS_NAME, C_METHOD_update, 20125, this, p_request);
            }
        }

        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 20127, this,
                                "Database error: unable to update row in DECI_DEFAULT_CHARGING_IND table.");
            }

            l_ppasSqlE = new PpasSqlException(C_CLASS_NAME,
                                              C_METHOD_update,
                                              11150,
                                              this,
                                              p_request,
                                              0,
                                              SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            p_request, C_CLASS_NAME, 20150, this,
                            "Leaving " + C_METHOD_update);
        }

        return l_rowCount;     
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_markAsAvailable = "markAsAvailable";
    /**
     * Marks a withdrawn Deci charging indicators record as available in the 
     * deci_default_charging_ind database table.
     * 
     * @param p_request     The request being processed.
     * @param p_connection  Database connection.
     * @param p_opid        Operator identifier.
     * @param p_startNo     Identifier of the DECI record.
     * @throws PpasSqlException Any exception derived from this Class.
     */
    public void markAsAvailable(PpasRequest p_request, 
                                JdbcConnection p_connection, 
                                String p_opid, 
                                String p_startNo) 
        throws PpasSqlException
    {
        JdbcStatement    l_statement = null;
        String           l_sql;
        SqlString        l_sqlString;
        int              l_rowCount = 0;
        PpasSqlException l_ppasSqlE = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10300, this,
                            "Entered " + C_METHOD_markAsAvailable);
        }

        l_sql = new String("UPDATE DECI_DEFAULT_CHARGING_IND " +
                           "SET DECI_DEL_FLAG = ' ', " +
                           "DECI_OPID = {1}, " +
                           "DECI_GEN_YMDHMS = {2} " +
                           "WHERE DECI_NUMBER_SERIES_START = {0}");
                       
        l_sqlString = new SqlString(150, 4, l_sql);

        l_sqlString.setStringParam(0, p_startNo);
        l_sqlString.setStringParam(1, p_opid);
        l_sqlString.setDateTimeParam(2, DatePatch.getDateTimeNow());

        try
        {
            l_statement = p_connection.createStatement(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10310,
                                       this,
                                       p_request);

            l_rowCount = l_statement.executeUpdate(
                                       C_CLASS_NAME,
                                       C_METHOD_markAsAvailable,
                                       10320,
                                       this,
                                       p_request,
                                       l_sqlString);
        }
        finally
        {
            l_statement.close( C_CLASS_NAME, C_METHOD_markAsAvailable, 10330, this, p_request);
        }
        
        if (l_rowCount != 1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10340, this,
                                "Database error: unable to update row in deci_default_charging_ind table.");
            }

            l_ppasSqlE = new PpasSqlException( C_CLASS_NAME,
                                               C_METHOD_markAsAvailable,
                                               10350,
                                               this,
                                               p_request,
                                               0,
                                               SqlKey.get().databaseInconsistency());

            throw (l_ppasSqlE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10360, this,
                            "Leaving " + C_METHOD_markAsAvailable);
        }
    }    
}
