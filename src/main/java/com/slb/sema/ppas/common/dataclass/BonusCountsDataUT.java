////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BonusCountsDataUT.java
//      DATE            :       05-Jul-2007
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Unit Test for Bonus Counts Data object
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.dataclass;

import java.text.ParseException;

import com.slb.sema.ppas.common.businessconfig.dataclass.BonaBonusAccumParamsData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** Unit Test for Bonus Counts Data object.
 * 
* @see com.slb.sema.ppas.common.dataclass.BonusCountsData
 */
public class BonusCountsDataUT  extends UtilTestCaseTT
{
    /** Money formatter. */
    private static final MoneyFormat  C_MONEY_FORMAT = new MoneyFormat("");
    
    /** Test currency. */
    private static final PpasCurrency C_CCY          = new PpasCurrency("EUR", 2);
    
    private static final BonaBonusAccumParamsData C_BONA_DATA = 
                                                      new BonaBonusAccumParamsData("S1",
                                                                                   null,
                                                                                   "XXX",
                                                                                   "YYY",
                                                                                   0,
                                                                                   new Double("50.00"),
                                                                                   null,
                                                                                   10,
                                                                                   new Double(1),
                                                                                   null,
                                                                                   false);
    
    /** Default constructor giving name of the test.
    *
    * @param p_title Name of test.
    */
    public BonusCountsDataUT(String p_title)
    {
        super(p_title);
    }
    
    /** Perform standard activities at end of a test, for example, clear date patching. */
    protected void tearDown()
    {
        say("---------------------------------------");
        say("Clearing date patch...");
        DatePatch.clearPpasOffset();
        say("---------------------------------------");
    }

    /** @ut.when Default data object created mid way through first period.
     *  @ut.then Period start date/time is the opt-in date/time, end date is end of period.
     */
    public void testFirstPeriodMid()
    {
        beginOfTest("testFirstPeriodMid");
        
        final String       L_SCHEME = "S1";
        final PpasDateTime L_OPT_IN = new PpasDateTime("01-Jul-2007 12:13:14");
        
        DatePatch.setDateTime("09-Jul-2007 15:34:26");
        
        BonusCountsData l_test = new BonusCountsData(L_SCHEME, L_OPT_IN, getZero(), C_BONA_DATA);

        checkData(l_test, L_SCHEME, L_OPT_IN, L_OPT_IN, new PpasDateTime("11-Jul-2007 23:59:59"));
        
        endOfTest();
    }

    /** @ut.when Default data object created on first day of first period.
     *  @ut.then Period start date/time is the opt-in date/time, end date is end of period.
     */
    public void testFirstPeriodStart()
    {
        beginOfTest("testFirstPeriodStart");
        
        final String       L_SCHEME = "S1";
        final PpasDateTime L_OPT_IN = new PpasDateTime("01-Jul-2007 12:13:14");

        DatePatch.setDateTime("01-Jul-2007 15:34:26");

        BonusCountsData l_test = new BonusCountsData(L_SCHEME, L_OPT_IN, getZero(), C_BONA_DATA);

        checkData(l_test, L_SCHEME, L_OPT_IN, L_OPT_IN, new PpasDateTime("11-Jul-2007 23:59:59"));
        
        endOfTest();
    }
    
    /** @ut.when Default data object created afetr end of first period.
     *  @ut.then Period start date/time is the day after end of first period.
     */
    public void testSecondPeriodMid()
    {
        beginOfTest("testSecondPeriodMid");
        
        final String       L_SCHEME = "S1";
        final PpasDateTime L_OPT_IN = new PpasDateTime("01-Jul-2007 12:13:14");

        DatePatch.setDateTime("15-Jul-2007 15:34:26");
        
        BonusCountsData l_test = new BonusCountsData(L_SCHEME, L_OPT_IN, getZero(), C_BONA_DATA);

        checkData(l_test, L_SCHEME, L_OPT_IN, new PpasDateTime("12-Jul-2007 00:00:00"), new PpasDateTime("21-Jul-2007 23:59:59"));

        endOfTest();
    }

    /** Validate created object is as expected.
     * 
     * @param p_data    Created object.
     * @param p_scheme  Scheme identifier.
     * @param p_optIn   Opt-in date/time.
     * @param p_expectedStart Expected start date/time.
     * @param p_expectedEnd   Expected end date/time.
     */
    private void checkData(BonusCountsData p_data,
                           String          p_scheme,
                           PpasDateTime    p_optIn,
                           PpasDateTime    p_expectedStart,
                           PpasDateTime    p_expectedEnd)
    {
        assertEquals("Scheme incorrect",     p_scheme,        p_data.getSchemeId());
        assertEquals("opt-In incorrect",     p_optIn,         p_data.getOptInStartDateTime());
        assertEquals("Start Time incorrect", p_expectedStart, p_data.getCountStartDateTime());
        assertEquals("End Time incorrect",   p_expectedEnd,   p_data.getCountEndDateTime());
    }
    
    /** Get a zero amount.
     * 
     * @return Zero amount.
     */
    private Money getZero()
    {
        Money l_return = null;
        
        try
        {
            l_return = C_MONEY_FORMAT.parse("0", C_CCY);
        }
        catch (ParseException e)
        {
            failedTestException(e);
        }
        
        return l_return;
    }
}
