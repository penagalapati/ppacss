////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BusinessConfigTables.Java
//      DATE            :       26-Sep-2006
//      AUTHOR          :       Kanta Goswami
//      REFERENCE       :
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       A class to hold all the constants used for the tables loaded by 
//                              BusinessConfigCache.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                     | REFERENCE
//----------+---------------+---------------------------------+--------------------
package com.slb.sema.ppas.common.businessconfig.cache;

/**
 * An Stringerface to hold the constants used for all the tables to be loaded by the BusinessConfigCache class
 */
public interface BusinessConfigTables
{        
    
    /** Indicator that the VALA Valid Language cache should be loaded. */
    public static final String C_SHORT_VALA_VALID_LANGUAGE                  = "vala";

    /** Indicator that the PRPL Promo Plan cache should be loaded. */
    public static final String C_SHORT_PRPL_PROMO_PLAN                      = "prpl";

    /** Indicator that the CMDF Cust Mast Defaults cache should be loaded. */
    public static final String C_SHORT_CMDF_CUST_MAST_DEFAULTS              = "cmdf";

    /** Indicator that the CDGR Acce Card Group cache should be loaded. */
    public static final String C_SHORT_CDGR_ACCE_CARD_GROUP                 = "cdgr";

    /** Indicator that the CONF Company Config cache should be loaded. */
    public static final String C_SHORT_CONF_COMPANY_CONFIG                  = "conf";

    /** Indicator that the ACCF Accumulator cache should be loaded. */
    public static final String C_SHORT_ACCF_ACCUMULATOR                     = "accf";

    /** Indicator that the SYFG System Config cache should be loaded. */ 
    public static final String C_SHORT_SYFG_SYSTEM_CONFIG                   = "syfg";

    /** Indicator that the BANK Bank cache should be loaded. */
    public static final String C_SHORT_BANK_BANK                            = "bank";

    /** Indicator that the MISC Miscellaneous Codes cache should be loaded. */
    public static final String C_SHORT_MISC_CODE                            = "misc";

    /** Indicator that the GOPA GUI Operator Access cache should be loaded. */
    public static final String C_SHORT_GOPA_GUI_OPERATOR_ACCESS             = "gopa";

    /** Indicator that the CUFM Currenncy Formats cache should be loaded. */
    public static final String C_SHORT_CUFM_CURRENCY_FORMATS                = "cufm";

    /** Indicator that the Service Area Adjustments Codes cache should be loaded. */
    public static final String C_SHORT_SRVA_ADJ_CODES                       = "srva_adj";

    /** Indicator that the Service Area Agent cache should be loaded. */
    public static final String C_SHORT_SRVA_AGENT_MSTR                      = "srva_agent";

    /** Indicator that the Service Area Master cache should be loaded. */
    public static final String C_SHORT_SRVA_MSTR                            = "srva_mstr";

    /** Indicator that the DIRE Disconnect Reason cache should be loaded. */
    public static final String C_SHORT_DIRE_DISCONNECT_REASON               = "dire";

    /** Indicator that the PAPR Payment Profile cache should be loaded. */
    public static final String C_SHORT_PAPR_PAYMENT_PROFILE                 = "papr";

    /** Indicator that the SCPI SCP Information cache should be loaded. */
    public static final String C_SHORT_SCPI_SCP_INFO                        = "scpi";

    /** Indicator that the MFLT Miscellaneous Field Titles cache should be loaded. */
    public static final String C_SHORT_MFLT_MISC_FIELD_TITLES               = "mflt";

    /** Indicator that the VOSP Voucher Splits cache should be loaded. */
    public static final String C_SHORT_VOSP_VOUCHER_SPLITS                  = "vosp";

    /** Indicator that the DEDA Dedicated Accounts cache should be loaded. */
    public static final String C_SHORT_DEDA_DEDICATED_ACCOUNTS              = "deda";

    /** Indicator that the NUBL Number Block cache should be loaded. */
    public static final String C_SHORT_NUBL_NUMBER_BLOCK                    = "nubl";

    /** Indicator that the FABA F&F Barred List cache should be loaded. */
    public static final String C_SHORT_FABA_FAF_BARRED_LIST                 = "faba";

    /** Indicator that the FACH F&F charging Ind cache should be loaded. */
    public static final String C_SHORT_FACH_FAF_CHARGING_IND                = "fach";

    /** Indicator that the Emu Conversion Rates cache should be loaded. */
    public static final String C_SHORT_EMCR_EMU_CONVERSION_RATES            = "emcr";

    /** Indicator that the Deci default charging ind cache should be loaded. */
    public static final String C_SHORT_DECI_DEFAULT_CHARGING_IND            = "deci";

    /** Indicator that the Comment cache should be loaded. */
    public static final String C_SHORT_SRVA_FIELDS                          = "srva_fields";

    /** Indicator that the Asce cache should be loaded. */
    public static final String C_SHORT_ASCE_ERROR_CODES                     = "asce";
    
    /** Indicator that the Scap cache should be loaded. */
    public static final String C_SHORT_SCAP_ADJ_PARAM                       = "scap";

    /** Indicator that the Acgr cache should be loaded. */
    public static final String C_SHORT_ACGR_ACC_GROUP                       = "acgr";
    
    /** Indicator that the Seof cache should be loaded. */
    public static final String C_SHORT_SEOF_SERV_OFF                        = "seof";
    
    /** Indicator that the Seof cache should be loaded. */
    public static final String C_SHORT_COCH_COMM_CHG                        = "coch";

    /** Indicator that the Regi cache should be loaded. */
    public static final String C_SHORT_REGI_REGION                          = "regi";

    /** Indicator that the Teleservice cache should be loaded. */
    public static final String C_SHORT_TELESERVICE                          = "tele";

    /** Indicator that the Teleservice cache should be loaded. */
    public static final String C_SHORT_TRAFFIC_CASE                         = "traf";

    /** Indicator that the CHAV cache should be loaded. */
    public static final String C_SHORT_CHAV_CHANNEL                         = "chav";

    /** Indicator that the BONE cache should be loaded. */
    public static final String C_SHORT_BONE_BONUS_ELEMENTS                  = "bone";

    /** Indicator that the BONS Bonus Scheme cache should be loaded. */
    public static final String C_SHORT_BONS_BONUS_SCHEME                    = "bons";

    /** Indicator that the BONA Bonus Accumulation Params cache should be loaded. */
    public static final String C_SHORT_BONA_BONUS_ACCUM_PARAMS              = "bona";
}
