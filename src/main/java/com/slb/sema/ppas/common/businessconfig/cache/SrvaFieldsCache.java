////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SrvaMstrCache.Java
//      DATE            :       13-Apr-2004
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Implements a cache for the commenmt descriptions
//                              held in the srva_fields table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// DD/MM/YY | <name>     | <brief description of           | <reference>
//          |            | change>                         |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.cache;

import com.slb.sema.ppas.common.businessconfig.dataclass.SrvaFieldsDataSet;
import com.slb.sema.ppas.common.businessconfig.sqlservice.SrvaFieldsSqlService;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A cache for the comment description configuration data (from the
 * srva_fields table).
 */
public class SrvaFieldsCache extends ConfigCache
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SrvaFieldsCache";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------

    /** The Sql service to use to load cache. */
    private SrvaFieldsSqlService i_srvaFieldsSqlService = null;

    /** Data set containing all configured Comment description records. */
    private SrvaFieldsDataSet i_allComments = null;

    /**
     * Cache of available comment descriptions (not including records marked as
     * deleted). This attribute exists for performance reasons so the array
     * is not generated each time it is required.
     */
    private SrvaFieldsDataSet i_availableComments = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Creates a new Comment Description data cache.
     *
     * @param p_request The request to process.
     * @param p_logger  The logger used by this service to log events to.
     * @param p_properties Configuration properties.
     */
    public SrvaFieldsCache(
        PpasRequest            p_request,
        Logger                 p_logger,
        PpasProperties         p_properties)
    {
        super (p_logger, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                p_request, C_CLASS_NAME, 82110, this,
                "Constructing " + C_CLASS_NAME);
        }

        i_srvaFieldsSqlService = new SrvaFieldsSqlService (p_request);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                p_request, C_CLASS_NAME, 82120, this,
                "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor

    //------------------------------------------------------------------------
    // Public methods
    //------------------------------------------------------------------------

    /**
     * Returns all Comment Description (including records marked as deleted).
     * @return Data set containing all Comment Descriptions configured in the
     *         srva_Fields table.
     */
    public SrvaFieldsDataSet getAll()
    {
        return i_allComments;
    }

    /**
     * Returns available Comment Descriptions.
     * The returned data set does not include records marked as deleted.
     * @return Data set containing Comment Descriptions that are not marked as deleted
     *         in the srva_fields table.
     */
    public SrvaFieldsDataSet getAvailable()
    {
        return i_availableComments;
    }

    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reload = "reload";
    /**
     * Reloads the caches data.
     *
     * @param p_request The request to process.
     * @param p_connection Database connection.
     * @throws PpasSqlException If the data cannot be reloaded.
     */
    public void reload (PpasRequest            p_request,
                        JdbcConnection         p_connection)
        throws PpasSqlException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START, p_request, C_CLASS_NAME, 82230, this,
                "Entered " + C_METHOD_reload);
        }

        i_allComments = i_srvaFieldsSqlService.readAll (p_request, p_connection);

        // Assignment is atomic, so callers of this object will either get
        // old configuration or new configuration, not configuration which is
        // in the process of being reloaded! It is theoretically possible for
        // callers to get new list of all markets and old list of
        // available markets, but rather than synchronise this is acceptable.
        i_availableComments = new SrvaFieldsDataSet(p_request, i_allComments.getAvailableArray());

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_BUSINESS | PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END, p_request, C_CLASS_NAME, 82290, this,
                "Leaving " + C_METHOD_reload);
        }
    }

    /** Get a list of the data objects.
     *  @return array of data objects in this cache.
     */
    protected Object[] getData()
    {
        return getData(i_allComments, i_availableComments);
    }
}

