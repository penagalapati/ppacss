////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PrilPrivilegeDataSet.Java
//      DATE            :       02-June-2004
//      AUTHOR          :       Micahel Erskine
//      REFERENCE       :       PpaLon#112/2508
//                              PRD_ASCS00_DEV_SS_088
//
//      COPYRIGHT       :       SCHLUMBERGERSEMA 2004
//
//      DESCRIPTION     :       Contains the data selected from the 
//                              PRIL_PRIVILEGE_LEVEL table.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE      | NAME          | DESCRIPTION                | REFERENCE
//-----------+---------------+----------------------------+---------------------
// dd-Mon-yy | <name>        | <brief description of      | <reference>
//           |               | change>                    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Contains PrilPrivilegeLevelData objects. */
public class PrilPrivilegeLevelDataSet
{
    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "PrilPrivilegeLevelDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------
    /** A set of PrilPrivilegeLevelData objects. */
    private Vector   i_prilPrivilegeLevelDataSetV;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Creates a new object containing an empty Vector.
     * 
     * @param p_request The request to process.
     * @param p_initialVSize Initial number of records to store.
     * @param p_vGrowSize Number of records to increase the data set by.
     */
    public PrilPrivilegeLevelDataSet(PpasRequest p_request,
                                     int         p_initialVSize,
                                     int         p_vGrowSize)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10000, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_prilPrivilegeLevelDataSetV = new Vector(p_initialVSize, p_vGrowSize);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructed " + C_CLASS_NAME);
        }
    }

    //------------------------------------------------------------------------
    // Overridden superclass methods.
    //------------------------------------------------------------------------
    /** Returns the contents of the this object in a String.
     * 
     * @return String representation of this object.
     */
    public String toString()
    {
        StringBuffer  l_stringBuffer = 
                          new StringBuffer("PrilPrivilegeLevelDataSet=\n[");
        int           l_loop;

        for(l_loop = 0; l_loop < i_prilPrivilegeLevelDataSetV.size(); l_loop++)
        {
            l_stringBuffer.append((i_prilPrivilegeLevelDataSetV
                                   .elementAt(l_loop)).toString());
            if ((l_loop + 1) < i_prilPrivilegeLevelDataSetV.size())
            {
                l_stringBuffer.append("\n");
            }
        }
        l_stringBuffer.append("]");

        return(l_stringBuffer.toString());
    }

    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /** 
     * Returns a specific <code>PrilPrivilegeLevelData</code> object,
     * according to the privilege level.
     * @param p_privilegeLevel The privilege level of the operator.
     * @return The <code>PrilPrivilegeLevelData</code> object associated
     *         with the supplied privilegeLevel
     */
    public PrilPrivilegeLevelData getPrilPrivilegeLevelData(int p_privilegeLevel)
    {
        boolean                   l_notFound = true;
        PrilPrivilegeLevelData    l_data = null;
        PrilPrivilegeLevelData    l_privilegeData = null;

        for (int l_loop = 0; 
             (l_loop < i_prilPrivilegeLevelDataSetV.size() && l_notFound);
             l_loop++)
        {
            l_data = (PrilPrivilegeLevelData)
                         i_prilPrivilegeLevelDataSetV.elementAt(l_loop);

            if (l_data.getPrivilegeLevel() == p_privilegeLevel)
            {
                l_privilegeData = l_data;
                l_notFound = false;
            }
        }

        return l_privilegeData;
    }


    /**
     * Returns all of the rows from PRIL_PRIVILEGE_LEVEL not marked as deleted
     * objects.
     * @return an array of <code>PrilPrivilegeData</code> objects not marked
     *         as deleted.
     */    
    public PrilPrivilegeLevelData[] getAvailablePrivileges()
    {
        ArrayList                 l_tempList = new ArrayList();
        int                       l_loop;
        int                       l_count = 0;
        PrilPrivilegeLevelData    l_data = null;
        PrilPrivilegeLevelData[]  l_privileges = new PrilPrivilegeLevelData[0];

        for (l_loop = 0; l_loop < i_prilPrivilegeLevelDataSetV.size(); l_loop++)
        {
            l_data = (PrilPrivilegeLevelData)
                         i_prilPrivilegeLevelDataSetV.elementAt(l_loop);

            if (l_data.getIsCurrent())
            {
                l_tempList.add(l_data);
            }
            l_count++;
        }

        return (PrilPrivilegeLevelData[])l_tempList.toArray(l_privileges);

    } // End of public method getPrivilegeIds()
    
    /**
     * Returns all of the rows from PRIL_PRIVILEGE_LEVEL as a set of data
     * objects.
     * @return an array of <code>PrilPrivilegeData</code> objects.
     */    
    public PrilPrivilegeLevelData[] getAllPrivileges()
    {
        ArrayList                 l_tempList = new ArrayList();
        int                       l_loop;
        int                       l_count = 0;
        PrilPrivilegeLevelData    l_data = null;
        PrilPrivilegeLevelData[]  l_privileges = new PrilPrivilegeLevelData[0];

        for (l_loop = 0; l_loop < i_prilPrivilegeLevelDataSetV.size(); l_loop++)
        {
            l_data = (PrilPrivilegeLevelData)
                         i_prilPrivilegeLevelDataSetV.elementAt(l_loop);
                         
            l_tempList.add(l_data);
            l_count++;
        }

        return (PrilPrivilegeLevelData[])l_tempList.toArray(l_privileges);

    } // End of public method getPrivileges()

    /** 
     * Adds an element to the data set.
     *
     * @param  p_prilPrivilegeLevelData The data object to be added.
     */
    public void addElement(PrilPrivilegeLevelData p_prilPrivilegeLevelData)
    {
        i_prilPrivilegeLevelDataSetV.addElement(p_prilPrivilegeLevelData);
    }
}