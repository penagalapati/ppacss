////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DedaDedicatedAccountsDataSet.java
//      DATE            :       28-Aug-2002 
//      AUTHOR          :       Simone Nelson
//      REFERENCE       :       PpaLon1484/6164
//                              PpaLon#1522_6421
//      COPYRIGHT       :       WM Data
//      DESCRIPTION     :       Class to hold a set of DedaDedicatedAccountsData
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 17/10/02 | N Fletcher | Add some Javadoc and fix some   | PpaLon#1636/6927
//          |            | typing errors in Javadoc.       |
//------------------------------------------------------------------------------
// 09/11/05 |Remi Isaacs |Add new method getDedicatedArray | CR 1825/7416
//          |            |where there are no restrictions  |
//          |            |such as market or service class  |
//          |            | to determine the dedicated      |
//          |            | account records returned.       |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.common.businessconfig.dataclass;

import java.util.Vector;

import com.slb.sema.ppas.common.dataclass.DataSetObject;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.dataclass.DedicatedAccountsData;

/**
 * A Set dedicated Account data.
 */
public class DedaDedicatedAccountsDataSet extends DataSetObject
{

    //------------------------------------------------------------------------
    // Class level constants.
    //------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "DedaDedicatedAccountsDataSet";

    //------------------------------------------------------------------------
    // Instance variables.
    //------------------------------------------------------------------------

    /** A Set of DedaDedicatedAccountsData objects. */
    private DedaDedicatedAccountsData[] i_dedaDataSetArr;
    
    //------------------------------------------------------------------------
    // Constructor
    //------------------------------------------------------------------------
    /** 
     * Creates a new set of deda data containing an empty Array.
     * 
     * @param p_request The request to process.
     * @param p_dedaDataSetArr A set of dedicated accounts data objects.
     */
    public DedaDedicatedAccountsDataSet(
        PpasRequest                     p_request,
        DedaDedicatedAccountsData []    p_dedaDataSetArr)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_START,
                 p_request, C_CLASS_NAME, 10010, this,
                 "Constructing " + C_CLASS_NAME );
        }

        i_dedaDataSetArr = p_dedaDataSetArr;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                 PpasDebug.C_LVL_MODERATE,
                 PpasDebug.C_APP_BUSINESS,
                 PpasDebug.C_ST_CONFIN_END,
                 p_request, C_CLASS_NAME, 10020, this,
                 "Constructed " + C_CLASS_NAME);
        }
    } // End of public constructor 
       
    //------------------------------------------------------------------------
    // Public Methods.
    //------------------------------------------------------------------------

    /**
     * Get the description of a given dedicated account.
     * 
     * @param p_market The market the dedicated account is associated with.
     * @param p_serviceClass The service class the dedicated account is associated with.
     * @param p_accountId Dedicated account Id 
     * @return The description of the dedicated account.. 
     *  
     */
    public String getDescription(Market       p_market,
                                 ServiceClass p_serviceClass,
                                 long         p_accountId)
    {
        DedaDedicatedAccountsData l_dedaData    = null;
        String                    l_description = "";
        boolean                   l_found       = false;
        int                       l_loop        = 0;

        if (i_dedaDataSetArr != null)
        {
            while ((l_loop < i_dedaDataSetArr.length) && (!l_found))
            {
                l_dedaData = i_dedaDataSetArr[l_loop];

                if ( l_dedaData.getDedaMarket().equals(p_market) &&
                     l_dedaData.getDedaServiceClass().equals(p_serviceClass) &&
                     l_dedaData.getDedaAccountId() == p_accountId)
                {
                    l_description = l_dedaData.getDedaDescription();
                    l_found = true;
                }
                l_loop++;
            }
        }

        return(l_description);

    } // end of getDescriptions(...)

    /** Returns a DedaDedicatedAccountsData description based on service class
     *  and the account identifier. Market is not taken into account in this
     *  method - it should be called from DataSet instances that contain 
     *  data for a single market only.
     *  @param p_serviceClass Service class that the dedicated account must
     *                        belong to.
     *  @param p_accountId The identifier of the dedicated account whose 
     *                     description is to be returned.
     *  @return A description of the dedicated account identified by the 
     *          methods input parameters. If no dedicated account was
     *          identified, an empty String is returned.
     */
    public String getDescription(ServiceClass p_serviceClass,
                                 int    p_accountId)
    {
        DedaDedicatedAccountsData l_dedaData    = null;
        String                    l_description = "";
        boolean                   l_found       = false;
        int                       l_loop        = 0;
        ServiceClass              l_serviceClass = null;


        if (i_dedaDataSetArr != null)
        {
            while ((l_loop < i_dedaDataSetArr.length) && (!l_found))
            {
                l_dedaData = i_dedaDataSetArr[l_loop];

                l_serviceClass = l_dedaData.getDedaServiceClass();


                if (p_serviceClass.equals(l_serviceClass) &&
                    l_dedaData.getDedaAccountId() == p_accountId)
                {
                    l_description = l_dedaData.getDedaDescription();
                    l_found = true;
                }
                l_loop++;
            }
        }

        return (l_description);

    } // end of method getDescription
    
    /** Returns a DedaDedicatedAccountsData description 
     *  the account identifier. 
     *  
     *  @param p_accountId The identifier of the dedicated account whose 
     *                     description is to be returned.
     *  @return A description of the dedicated account identified by the 
     *          methods input parameters. If no dedicated account was
     *          identified, an empty String is returned.
     */
    public String getDescription(int    p_accountId)
    {
        DedaDedicatedAccountsData l_dedaData    = null;
        String                    l_description = "";
        boolean                   l_found       = false;
        int                       l_loop        = 0;


        if (i_dedaDataSetArr != null)
        {
            while ((l_loop < i_dedaDataSetArr.length) && (!l_found))
            {
                l_dedaData = i_dedaDataSetArr[l_loop];

                if ( l_dedaData.getDedaAccountId() == p_accountId )
                {
                    l_description = l_dedaData.getDedaDescription();
                    l_found = true;
                }
                l_loop++;
            }
        }

        return (l_description);

    } // end of method getDescription

    /** Returns the dedaData Set as an Array.
     *  @return Array of dedaData.
     */
    public DedaDedicatedAccountsData [] getDedaArray()
    {
        return( i_dedaDataSetArr );
    }
    
    /** Returns the DedicatedAccountsData Set as an Array.
     *  @return Array of DedicatedAccountsData.
     */
    public DedicatedAccountsData [] getDedicatedArray()
    {
        int l_dedicatedAccountsCount  = i_dedaDataSetArr.length;
        DedicatedAccountsData[] l_dedAccountsDataArr = new DedicatedAccountsData[l_dedicatedAccountsCount];
                
        for (int l_index = 0; l_index < l_dedicatedAccountsCount; l_index++)
        {
            l_dedAccountsDataArr[l_index] =
                    new DedicatedAccountsData(i_dedaDataSetArr[l_index].getDedaAccountId(),
                                              i_dedaDataSetArr[l_index].getDedaDescription());
        }
      
        return( l_dedAccountsDataArr );
    }
        
    /**
     * For the market and service class returns the number of dedicated accounts configured.
     * @param p_market Market to search on.
     * @param p_serviceClass Service class to search on.
     * @return The number of Dedicated Accounts configured for the specified market and service class.
     */
    public int getDedaCount(Market p_market,
                            ServiceClass p_serviceClass )
    {
        int l_index = 0;
        int l_count = 0;
        DedaDedicatedAccountsData  l_dedaData = null;

        if (i_dedaDataSetArr != null)
        {
            while (l_index < i_dedaDataSetArr.length)
            {
                l_dedaData = i_dedaDataSetArr[l_index];
                if ( l_dedaData.getDedaMarket().equals(p_market) &&
                     l_dedaData.getDedaServiceClass().equals(p_serviceClass) )
                {
                    l_count++;
                }
                l_index++;
            }
        }

        return(l_count);
    }

    /** Get array of dedicated account data object with the given market.
     *  @param p_market Market which the dedicated account data must belong to.
     *  @return Array of dedicated account data object with the given market.
     */
    public DedaDedicatedAccountsData [] getDedaArray (Market p_market)
    {
        int                       l_loop;
        Vector                    l_availableV = new Vector (30, 20);
        DedaDedicatedAccountsData l_dedaArr[];
        DedaDedicatedAccountsData l_dedaEmptyArr[] = new DedaDedicatedAccountsData[0];

        if (i_dedaDataSetArr != null)
        {
            for (l_loop = 0;
                 l_loop < i_dedaDataSetArr.length;
                 l_loop++)
            {
                if ( i_dedaDataSetArr[l_loop].getDedaMarket().equals(p_market) )
                {
                    l_availableV.addElement (i_dedaDataSetArr[l_loop]);
                }
            }
        }

        l_dedaArr = (DedaDedicatedAccountsData [])
                                    l_availableV.toArray (l_dedaEmptyArr);

        return (l_dedaArr);
    }

    /** Get array of dedicated account data object with the given market and service class.
     *  @param p_market Market which the dedicated account data must belong to.
     *  @param p_serviceClass Market which the dedicated account data must belong to.
     *  @return Array of dedicated account data object with the given market and service class.
     */
    public DedaDedicatedAccountsData [] getDedaArray (Market p_market,
                                                      ServiceClass p_serviceClass)
    {
        int                       l_loop;
        Vector                    l_availableV = new Vector (30, 20);
        DedaDedicatedAccountsData l_dedaArr[];
        DedaDedicatedAccountsData l_dedaEmptyArr[] = new DedaDedicatedAccountsData[0];

        if (i_dedaDataSetArr != null)
        {
            for (l_loop = 0;
                 l_loop < i_dedaDataSetArr.length;
                 l_loop++)
            {
                if ( ( i_dedaDataSetArr[l_loop].getDedaMarket().equals(p_market) ) &&
                     ( i_dedaDataSetArr[l_loop].getDedaServiceClass().equals(p_serviceClass) ) )
                {
                    l_availableV.addElement (i_dedaDataSetArr[l_loop]);
                }
            }
        }

        l_dedaArr = (DedaDedicatedAccountsData []) l_availableV.toArray (l_dedaEmptyArr);

        return (l_dedaArr);
    }

    /** Description of this object.
     *
     * @param p_sb StringBuffer to which the description should be added.
     */
    protected void describeThis(StringBuffer p_sb)
    {
        describeData("All", i_dedaDataSetArr, p_sb);
    }
}

