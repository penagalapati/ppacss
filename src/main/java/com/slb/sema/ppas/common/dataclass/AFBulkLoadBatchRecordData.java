////////////////////////////////////////////////////////////////////////////////
//       ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//       FILE NAME       :       AFBulkLoadBatchRecordData.java
//       DATE            :       02-Nov-2004
//       AUTHOR          :       Neil Raymond (r41087d)
//       REFERENCE       :       PRD_ASCS00_GEN_CA_37
//
//       COPYRIGHT       :       Atos Origin 2004
//
//       DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//       CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.common.dataclass;

import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Represents an MSISDN and the SDP that it is installed on.
 */
public class AFBulkLoadBatchRecordData extends BatchRecordData
{
    //-------------------------------------------------------------------------
    // Instance attributes
    //-------------------------------------------------------------------------
    /** The MSISDN represented by this object. */
    private Msisdn i_msisdn;
    
    /** The SDP on which the MSISDN is installed. */
    private String i_sdpId;
    
    //-------------------------------------------------------------------------
    // Class constants
    //-------------------------------------------------------------------------
    /** Constant used for calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "AFBulkLoadBatchRecordData";

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Construct an instance of AFBulkLoadBatchRecordData.
     * @param p_msisdn an MSISDN
     * @param p_sdpId the SDP that this MSISDN is installed on
     */
    public AFBulkLoadBatchRecordData(Msisdn p_msisdn,
                                     String p_sdpId)
    {
        super();
        
        i_msisdn = p_msisdn;
        i_sdpId = p_sdpId;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END | PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10710, this,
                            "Constructed " + C_CLASS_NAME + " for MSISDN " + i_msisdn + " on SDP " + i_sdpId);
        }
    }
    

    //--------------------------------------------------------------------------
    // Public methods.                                                        --
    //--------------------------------------------------------------------------

    /**
     * @return Returns the msisdn.
     */
    public Msisdn getMsisdn()
    {
        return i_msisdn;
    }

    /**
     * @return Returns the sdpId.
     */
    public String getSdpId()
    {
        return i_sdpId;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_dumpRecord = "dumpRecord";
    /**
     * @return the record content in a readable format.
     */
    public String dumpRecord()
    {
        StringBuffer l_tmp = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10020,
                            this,
                            C_ENTERING + C_METHOD_dumpRecord);
        }

        // Get the super class data
        l_tmp.append( C_DELIMITER );
        l_tmp.append( "*** Class name = " );
        l_tmp.append( C_CLASS_NAME );
        l_tmp.append( " ***");
        l_tmp.append( C_DELIMITER );
        
        l_tmp.append( super.getDumpRecord() );
        
        // This class's data
        l_tmp.append( "MSISDN = ");
        if (i_msisdn != null)
        {
            l_tmp.append( this.i_msisdn.getCountryCode() );
            l_tmp.append( this.i_msisdn.getSignificantNumber());
        }
        else
        {
            l_tmp.append("'null'");
        }
        l_tmp.append( C_DELIMITER );

        l_tmp.append( "SDP ID = ");
        if (i_sdpId != null)
        {
            l_tmp.append(this.i_sdpId);
        }
        else
        {
            l_tmp.append("'null'");
        }
        l_tmp.append( C_DELIMITER );

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10030,
                            this,
                            C_LEAVING + C_METHOD_dumpRecord);
        }

        return l_tmp.toString();
    }
}
