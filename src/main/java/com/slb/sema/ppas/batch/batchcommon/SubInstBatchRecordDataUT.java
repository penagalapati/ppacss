 ////////////////////////////////////////////////////////////////////////////////
//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SubInstBatchReaderUT
//    DATE            :       16-April-2004
//    AUTHOR          :       Marianne Tornqvist
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       This class is the test class for SubInstBatchReader.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//        |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
 
package com.slb.sema.ppas.batch.batchcommon;

import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;

import junit.framework.Test;
import junit.framework.TestSuite;

/** Test class for subordinate install batch process.
 * @see com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData
 */
public class SubInstBatchRecordDataUT extends BatchTestCaseTT
{
    /**
     * Constructor for the SubInstBatchDataUT.
     * @param p_name The name of this UT test
     */
    public SubInstBatchRecordDataUT(String p_name)
    {
        super(p_name);
    }

    /** Processing to be performed before each test. */
    protected void setUp()
    {
        super.setUp(CommonTestCaseTT.C_FLAG_WITHOUT_JDBC_CONNECTION);
    }

    /** Processing to be performed after each test. */
    protected void tearDown()
    {
        super.tearDown();
    }

    /** @ut.when A request is made to load a valid record.
     *  @ut.then The request succeeds.
     *  @ut.attributes +f
     */
    public final void testLoadingBatchDataRecord()
    {
        final String   L_TEST_AGENT       = "Mobilia";
        final String   L_INPUT_FILENAME   = "INSTALL_0102_1234_20040514_00001.DAT";
//        final String   L_MSISDN           = "0987800700";
//        final String   L_MASTER_MSISDN    = "0987800703";       
        final String   L_MARKET           = "00010003";
        final String   L_SERVICE_AREA     = "0001";
        final String   L_SERVICE_LOCATION = "0003";
        final String   L_PROMOTION_PLAN   = "GOLD";
        final String   L_SERVICE_CLASS    = "12";
        final String   L_SDP_ID           = "09";
        final boolean  L_CORRUPT_LINE     = false;
        final String   L_ERROR_LINE       = "A string containing text to write to the error file";
        final String   L_INPUT_LINE       = "A string containing the original input line";
        final String   L_OUTPUT_LINE      = "A string containing text to write to recovery file";
        final String   L_RECOVERY_LINE    = "123;OK";
        final boolean  L_RETRY_RECORD     = false;
        final long     L_ROW_NUMBER       = 123;

        final String   L_ZZZZ             = "ZZZZ";
        
        SubInstBatchRecordData l_batchDataRecord = new SubInstBatchRecordData();

        System.out.println("L_TEST_AGENT=" + L_TEST_AGENT);        
        l_batchDataRecord.setAgent( L_TEST_AGENT );
        
        System.out.println("L_INPUT_FILENAME=" + L_INPUT_FILENAME);
        l_batchDataRecord.setInputFilename( L_INPUT_FILENAME );
        
// MSISDN needs a PpasContext ... TBD in BatchTestCaseTT...
//        l_batchDataRecord.setMsidsdn( L_MSISDN );
//        l_batchDataRecord.setMasterMsisdn( L_MASTER_MSISDN );
        
        System.out.println("L_MARKET=" + L_MARKET);
        l_batchDataRecord.setMarket( L_MARKET );
        
        System.out.println("L_SERVICE_AREA=" + L_SERVICE_AREA);        
        l_batchDataRecord.setServiceArea( L_SERVICE_AREA );
        
        System.out.println("L_SERVICE_LOCATION=" + L_SERVICE_LOCATION);                
        l_batchDataRecord.setServiceLocation( L_SERVICE_LOCATION );

        System.out.println("L_SERVICE_LOCATION=" + L_SERVICE_LOCATION);                
        l_batchDataRecord.setPromotionPlan( L_PROMOTION_PLAN );


        System.out.println("L_SERVICE_CLASS=" + L_SERVICE_CLASS);                
        l_batchDataRecord.setServiceClass( L_SERVICE_CLASS );
        
        System.out.println("L_SDP_ID=" + L_SDP_ID);                
        l_batchDataRecord.setSdpId( L_SDP_ID );
        
        System.out.println("L_CORRUPT_LINE=" + L_CORRUPT_LINE);                
        l_batchDataRecord.setCorruptLine( L_CORRUPT_LINE );
        
        System.out.println("L_ERROR_LINE=" + L_ERROR_LINE);                
        l_batchDataRecord.setErrorLine( L_ERROR_LINE );
        
        System.out.println("L_INPUT_LINE=" + L_INPUT_LINE);                
        l_batchDataRecord.setInputLine( L_INPUT_LINE );
        
        System.out.println("L_OUTPUT_LINE=" + L_OUTPUT_LINE);                
        l_batchDataRecord.setOutputLine( L_OUTPUT_LINE );
        
        System.out.println("L_RECOVERY_LINE=" + L_RECOVERY_LINE);                
        l_batchDataRecord.setRecoveryLine( L_RECOVERY_LINE );
        
        System.out.println("L_RETRY_RECORD=" + L_RETRY_RECORD);                
        l_batchDataRecord.setRetryRecord( L_RETRY_RECORD );
        
        System.out.println("L_ROW_NUMBER=" + L_ROW_NUMBER);                
        l_batchDataRecord.setRowNumber( L_ROW_NUMBER );

        System.out.println( "testLoadingBatchDataRecord: " + l_batchDataRecord.dumpRecord() );
        
        System.out.println("Check with assertTrue - all should be OK");
        assertEquals(getFailMessage(L_TEST_AGENT),
                                    l_batchDataRecord.getAgent(),
                                    L_TEST_AGENT);
        assertEquals(getFailMessage(L_INPUT_FILENAME),
                                    l_batchDataRecord.getInputFilename(),
                                    L_INPUT_FILENAME);
        assertEquals(getFailMessage(L_MARKET),
                                    l_batchDataRecord.getMarket(),
                                    L_MARKET);
        assertEquals(getFailMessage(L_PROMOTION_PLAN),
                                    l_batchDataRecord.getPromotionPlan(),
                                    L_PROMOTION_PLAN);       
        assertEquals(getFailMessage(L_SDP_ID),
                                    l_batchDataRecord.getSdpId(),
                                    L_SDP_ID);       
        assertEquals(getFailMessage(L_SERVICE_AREA),
                                    l_batchDataRecord.getServiceArea(),
                                    L_SERVICE_AREA);        
        assertEquals(getFailMessage(L_SERVICE_CLASS),
                                    l_batchDataRecord.getServiceClass().toString(),
                                    L_SERVICE_CLASS);       
        assertEquals(getFailMessage(L_SERVICE_LOCATION), l_batchDataRecord.getServiceLocation(),
                                    L_SERVICE_LOCATION);

        System.out.println("Check with assertFalse - test with 'ZZZZ' all should be OK");
        assertFalse(getFailMessage("L_ZZZ FAILED"), l_batchDataRecord.getAgent().equals(L_ZZZZ));
        assertFalse(getFailMessage("L_ZZZ FAILED"), l_batchDataRecord.getInputFilename().equals(L_ZZZZ));
        
        System.out.println("END of method testLoadingBatchDataRecord()");
        
        System.out.println("number of tests : " + this.countTestCases());
        return;
    }
    
    /** Get a string representation of the failure message.
     * 
     * @param p_message Base message.
     * @return Formatted fail message.
     */
    private String getFailMessage(String p_message )
    {
        final String L_FAILED = "FAILED with :";
        StringBuffer l_tmp = new StringBuffer();
        l_tmp.append( L_FAILED );
        l_tmp.append( p_message );
        return l_tmp.toString();
    }
    
    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(SubInstBatchRecordDataUT.class);    
    }
}
