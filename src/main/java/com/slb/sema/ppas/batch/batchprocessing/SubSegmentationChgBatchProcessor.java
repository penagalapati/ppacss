////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME            : SubSegmentationChgBatchProcessor.java
//      DATE                 : 22 June 2007
//      AUTHOR               : Ian James
//      REFERENCE            : PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT            : WM-data 2007
//
//      DESCRIPTION          : This class is responsible for calling the appropriate
//                             IS API method to perform the subscriber segmentation change.
//
/////////////////////////////////////////////////////////////////////////////////
//                               CHANGE HISTORY
/////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+-----------------
// 07/08/07 | Ian James     | Add PpasServiceException message | Ppacs#3258/11939
//          |               | keys thrown by the IS API.       |
//----------+---------------+----------------------------------+-----------------
/////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.SubSegmentationChgBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.SubSegChgBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This <code>SubSegmentationChgBatchProcessor</code> class is responsible for calling the appropriate IS API
 * method to perform the Promotion Plan Allocation Synchronisation.
 */
public class SubSegmentationChgBatchProcessor extends BatchProcessor
{
    //---------------------------------------------------------------
    //  Class level constants.
    //---------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String   C_CLASS_NAME                     = "SubSegmentationChgBatchProcessor";

    /** Constant holding the error code for "Non-numeric MSISDN". Value is {@value}. */
    private static final String   C_ERROR_CODE_MSISDN_NOT_NUMERIC  = "02";

    /** Constant holding the error code for "a change dates would result in overlap". Value is {@value}. */
    private static final String   C_ERROR_CODE_INACTIVE_ACCOUNT    = "03";

    /** Constant holding the error code for "invalid new account group Id". Value is {@value}. */
    private static final String   C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID = "05";

    /** Constant holding the error code for "more than one Accumulated Top Up Rewards schema allocation". Value is {@value}. */
    private static final String   C_ERROR_CODE_BONUS_SCHEME_NOT_ALLOWED    = "07";

    /** Error code. Miscellaneous processing errors (re-submit record). Value is {@value}. */
    private static final String C_ERROR_CODE_MISC_PROCESSING_ERRORS= "99";
    
    /** The mapping between the error codes and the corresponding 'PpasServiceException' message keys. */
    private static final String[][] C_ERROR_CODE_MAPPINGS =
    {
            // Mappings to 'PpasServiceException' message keys thrown by the IS API method itself.
            {PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE,          C_ERROR_CODE_INACTIVE_ACCOUNT},           
            {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS,           C_ERROR_CODE_INACTIVE_ACCOUNT},
            {PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT,           C_ERROR_CODE_INACTIVE_ACCOUNT},
            {PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE,          C_ERROR_CODE_INACTIVE_ACCOUNT},
            {PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT,        C_ERROR_CODE_INACTIVE_ACCOUNT},
            {PpasServiceMsg.C_KEY_INVALID_INPUT_MSISDN,        C_ERROR_CODE_MSISDN_NOT_NUMERIC},
            {PpasServiceMsg.C_KEY_BONUS_SCHEME_NOT_ALLOWED,    C_ERROR_CODE_BONUS_SCHEME_NOT_ALLOWED},
            {PpasServiceMsg.C_KEY_INVALID_VALUE_NEW_ACC_GROUP, C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID}
    };

    //---------------------------------------------------------------
    //  Instance variables.
    //---------------------------------------------------------------
    /** The IS API used to perform the Promotion Plan Allocation Synchronisation. */
    private PpasAccountService    i_ppasAccountService             = null;

    /** The current <code>Session</code> object reference. */
    private PpasSession           i_ppasSession                    = null;

    /** The the sleep interval between successive processing of a <code>SubSegmentationChgProcessor</code>
     * (Milliseconds).*/
    protected long i_waitPeriodBetweenRequest = 0;
    
    //---------------------------------------------------------------
    //  Constructors.
    //---------------------------------------------------------------
    /**
     * Constructs an instance of this <code>SubSegmentationChgBatchProcessor</code> class using the passed
     * parameters.
     * @param p_ppasContext the <code>PpasContext</code> reference.
     * @param p_logger the logger
     * @param p_batchController the batch controller
     * @param p_inQueue the input data queue
     * @param p_outQueue the output data queue
     * @param p_parameters the process paramters
     * @param p_properties the <code>PpasProperties</code> for the current process.
     */
    public SubSegmentationChgBatchProcessor(PpasContext p_ppasContext,
                                            Logger p_logger,
                                            SubSegmentationChgBatchController p_batchController,
                                            SizedQueue p_inQueue,
                                            SizedQueue p_outQueue,
                                            Map p_parameters,
                                            PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_parameters, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this, BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        i_ppasSession = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        PpasRequest l_ppasRequest = new PpasRequest(i_ppasSession);
        i_ppasAccountService = new PpasAccountService(l_ppasRequest, p_logger, p_ppasContext);
        i_waitPeriodBetweenRequest = p_properties.getLongProperty(BatchConstants.C_WAIT_PERIOD_BETWEEN_REQUEST, 0);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this, BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } //End of 'SubSegmentationChgBatchProcessor' Constructor.

    //---------------------------------------------------------------
    //  Public methods.
    //---------------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";

    /**
     * Processes and returns the given <code>BatchRecordData</code>. The given <code>BatchRecordData</code>
     * must be an instance of the <code>SubSegChgBatchRecordData</code> class. This method will call
     * the proper IS API method in order to perform a Subscriber Segmentation Change. Depending on
     * the success of the operation, the record will be updated with information used by the writer-component
     * before returned. This method is called from the super class 'doRun()' method.
     * @param p_record the <code>SubSegChgBatchRecordData</code> object to be processed.
     * @return the processed <code>BatchRecordData</code> updated with information to be used by the
     * writer-component.
     * @throws PpasServiceException if the business service fails to perform the Subscriber Segmentation
     * Change.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        String                  l_opId                = null;
        Msisdn                  l_msisdn              = null;
        PpasRequest             l_ppasRequest         = null;
        StringBuffer            l_tmpRecoveryLine     = null;
        String                  l_recoveryLine        = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this, BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        // Cast the record into the batch specific record type
        SubSegChgBatchRecordData l_record = (SubSegChgBatchRecordData)p_record;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10030, this, "Dump Record: '" + l_record.dumpRecord());
        }

        //Prepare recovery line - shall always be created
        l_tmpRecoveryLine = new StringBuffer();
        l_tmpRecoveryLine.append(l_record.getRowNumber());
        l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);

        if (!l_record.isCorruptLine())
        {
            l_opId = super.i_batchController.getSubmitterOpid();
            l_msisdn = l_record.getMsisdn();
            l_ppasRequest = new PpasRequest(i_ppasSession, l_opId, l_msisdn);

            try
            {
                // Update account group id and/or service offerings.
                i_ppasAccountService.updateSegmentation(l_ppasRequest,
                                                        super.i_isApiTimeout,
                                                        l_record.getOldAccountGroupId(),
                                                        l_record.getNewAccountGroupId(),
                                                        l_record.getOldServiceOfferings(),
                                                        l_record.getNewServiceOfferings());

                l_recoveryLine = l_record.getRowNumber() + 
                                    BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                                    BatchConstants.C_SUCCESSFULLY_PROCESSED;
                                    l_record.setRecoveryLine(l_recoveryLine);
                
                //l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);
            }
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10040, this,
                                    "A PpasServiceException is caught: " + p_psExe.getMessage() + ",  key: "
                                          + p_psExe.getMsgKey());
                }
            
                if (p_psExe.getLoggingSeverity() == PpasServiceException.C_SEVERITY_FATAL)
                {
                    // This is a fatal error, re-throw the 'PpasServiceException' in order to stop
                    // the complete process.
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10028, this,
                                        "***ERROR: " +
                                        "A fatal error has occurred during batch segmentation change -- " + 
                                        "msg key: " + p_psExe.getMsgKey());
                    }
                    throw p_psExe;
                }            
            
                // Map the thrown 'PpasServiceException' to an appropriate error code.
                mapException(l_record, p_psExe);
                
                // Create and set the recovery line.
                l_recoveryLine = l_record.getRowNumber() +
                                    BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                                    BatchConstants.C_NOT_PROCESSED;
                                    l_record.setRecoveryLine(l_recoveryLine);
            }
        }
        else
        {
            if (p_record.isCorruptLine())
            {
                // Append a suitable status to it and set the result as the recovery line for this record.
                l_recoveryLine = l_record.getRowNumber() +
                                 BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                                 BatchConstants.C_NOT_PROCESSED;
                l_record.setRecoveryLine(l_recoveryLine);
            }
        }

        // Waiting period between request.
        
        try
        {
            Thread.sleep(i_waitPeriodBetweenRequest);
        }
        catch (InterruptedException e)
        {
            e = null; // Do nothing
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10090, this, BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }

        return l_record;
    }

    //----------------------------------------------------------------------
    //--  Private methods.                                                --
    //----------------------------------------------------------------------
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_mapException = "mapException";        
    /**
     * Updates the passed <code>BatchRecordData</code> object's error line with an error code that 
     * corresponds to the passed <code>PpasServiceException</code>'s message key.
     * 
     * @param p_record      the <code>BatchRecordData</code>.
     * @param p_ppasServEx  the <code>PpasServiceException</code>.
     */
    private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
    {
        StringBuffer l_errorLine   = null;
        boolean      l_mappingDone = false;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10040, this,
                            BatchConstants.C_ENTERING + C_METHOD_mapException);
        }
        
        // Set the last part of the error line.
        l_errorLine = new StringBuffer(BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                       p_record.getInputLine());
        
        // Look for matching mapping through the whole error code mapping array or until a matching
        // mapping is done.
        for (int i = 0; (i < C_ERROR_CODE_MAPPINGS.length  &&  !l_mappingDone); i++)
        {
            if (p_ppasServEx.getMsgKey().equals(C_ERROR_CODE_MAPPINGS[i][0]))
            {
                l_errorLine.insert(0, C_ERROR_CODE_MAPPINGS[i][1]);
                l_mappingDone = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10042, this,
                                    C_METHOD_mapException + 
                                    " -- A matching mapping is found, key = " + C_ERROR_CODE_MAPPINGS[i][0] +
                                    ",  error code = " + C_ERROR_CODE_MAPPINGS[i][1]);
                }
            }
        }
        
        if (!l_mappingDone)
        {
            // None of the expected mapping matches the given message key.
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10044, this,
                            C_METHOD_mapException + 
                            " -- An unexpected PpasServiceException is detected, " +
                            "key = " + p_ppasServEx.getMsgKey() +
                            ",  Exception severity = " + p_ppasServEx.getLoggingSeverity());
            
            l_errorLine.insert(0, C_ERROR_CODE_MISC_PROCESSING_ERRORS);
        }
        
        p_record.setErrorLine(l_errorLine.toString());
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10050, this,
                            BatchConstants.C_LEAVING + C_METHOD_mapException);
        }
    }
}