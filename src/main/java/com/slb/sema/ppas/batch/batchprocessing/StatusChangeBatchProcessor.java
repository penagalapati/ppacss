////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchProcessor
//      DATE            :       10-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                      | REFERENCE
//----------+------------+----------------------------------+-------------------
// 02/07/07 | E Dangoor  | Use call to getDisconnectDateTime| PpacLon#3195/11774
//          |            | instead of getCudiDiscDateTime   |
//----------+------------+----------------------------------+-----------------
// 31/03/10 | Smita M    | Map error code-08 for            | PpacBan#3688/13680
//          |            | MSISDN_MISMATCH                     | 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.StatusChangeBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasDisconnectService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This batch is using two different administrative business services depending on the type of
 * status change: ChangeTemporaryBlocking or FutureDisconnect.
 *
 * ChangeTemporaryBlocking for: Active(A) / Not Active(N)               
 * FutureDisconnnect for: changing to Permanent Disconnect(P)
 */
public class StatusChangeBatchProcessor extends BatchProcessor
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                                = "StatusChangeBatchProcessor";

    /** Status indicating Active.  Value is {@value}. */
    private static final String C_ACTIVE                                    = "A";
    
    /** Status indicating Not Active.  Value is {@value}. */
    private static final String C_NOT_ACTIVE                                = "N";
    
    /** Status indicating Permanent Disconnected.  Value is {@value}. */
    private static final String C_PERMANENT_DISCONNECTED                    = "P";

    /** Constant holding the error code for other errors. Value is {@value}. */
    private static final String C_ERROR_CODE_OTHER_ERROR                    = "01";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_AVAILABLE           = "04";

    /** Constant holding the error code for database inconsitency. Value is {@value}. */
    private static final String C_ERROR_CODE_DATABASE_INCONSISTENCY         = "05";
    
    /** Constant holding the error code for 'transient account'. Value is {@value}. */
    private static final String C_ERROR_CODE_STATUS_CANNOT_BE_CHANGED       = "07";

    /** Constant holding the error code for MSISDN already disconnected. Value is {@value}. */
    private static final String C_ERROR_CODE_ALREADY_DISCONNECTED           = "08";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasAccountService    i_ppasAccountService   = null;

    /** The <code>IS API</code> reference. */    
    private PpasDisconnectService i_disconnectService    = null;

    /** The <code>Session</code> object. */
    private PpasSession           i_ppasSession          = null;
    
    /** Default disconnection reason. */
    private String                i_disconnectionReason  = null;

    /**
     * Constructs a StatusChangeBatchProcessor object using the specified
     * batch controller, input data queue and output data queue.

     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the Logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process parameters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public StatusChangeBatchProcessor(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_batchController,
        SizedQueue p_inQueue,
        SizedQueue p_outQueue,
        Map p_params,
        PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);
        
        PpasRequest         l_ppasRequest = null;
        BusinessConfigCache l_cache       = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                11000,
                this,
                BatchConstants.C_CONSTRUCTING );
        }


        i_ppasSession          = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest          = new PpasRequest(i_ppasSession);
        i_ppasAccountService   = new PpasAccountService(l_ppasRequest, p_logger, p_ppasContext);
        i_disconnectService    = new PpasDisconnectService(l_ppasRequest, p_logger, p_ppasContext);

        l_cache = (BusinessConfigCache) i_ppasContext.getAttribute("BusinessConfigCache");
        i_disconnectionReason = l_cache.getConfCompanyConfigCache()
            .getConfCompanyConfigData().getDiscReasonForRemoval();
       
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                11010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }
        
        return;

    } // End of constructor


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";        
    /**
     * Processes the given <code>StatusChangeBatchDataRecord</code> and returns it.
     * If the process succeeded the <code>StatusChangeBatchDataRecord</code> is updated with information
     * needed by the writer component before returning it.
     * If old status is Active and shall be changed to Not Active
     * or old status is Not Active and shall be changed to Active, then changeTemporaryBlocking
     * shall be called, the MSISDN and the new status are sufficient parameters.
     * But if the old status is Active/Not Active and shall be changed to Permanent Disconnect
     * then futureDisconnect shall be called, todays date and a default disconnection reason is 
     * passed along with the MSISDN.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @see BatchProcessor#processRecord(BatchRecordData)
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    public BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        String                      l_opId            = null;
        Msisdn                      l_msisdn          = null;
        PpasRequest                 l_ppasRequest     = null;
        StatusChangeBatchRecordData l_batchRecordData = null;
        String                      l_errorLine       = null;


        boolean                     l_temporaryBlockingStatus = false; // Assume not blocking
        String                      l_desiredStatus           = null;
        PpasDate                    l_disconnectionDate       = DatePatch.getDateToday();
        StringBuffer                l_tmpErrorLine            = null;
        StringBuffer                l_tmpRecoveryLine         = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                11020,
                this,
                BatchConstants.C_ENTERING + C_METHOD_processRecord );
        }
       
        l_batchRecordData = (StatusChangeBatchRecordData)p_record;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                11030,
                this,
                "Got the BatchDataRecord " + C_METHOD_processRecord + 
                "StatusChangeProcessor.processRecord -- " +
                "StatusChangeBatchRecordData:\n" + l_batchRecordData.dumpRecord() +  
                "\nDateAndTime=" + l_disconnectionDate.toString_yyyyMMdd());
        }  
        
        // Prepare recovery line - shall always be created
        l_tmpRecoveryLine = new StringBuffer();
        l_tmpRecoveryLine.append(l_batchRecordData.getRowNumber());
        l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);

        // Process data input line seems to be OK
        if ( !l_batchRecordData.isCorruptLine() )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11040,
                    this,
                    "DataRecord is OK");
            }  

            l_opId          = i_batchController.getSubmitterOpid();
            l_msisdn        = l_batchRecordData.getMsisdn();
            l_ppasRequest   = new PpasRequest(i_ppasSession, l_opId, l_msisdn);

            l_desiredStatus = l_batchRecordData.getNewStatus();
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11050,
                    this,
                    "PpasRequest created with MSISDN " + l_msisdn.toString() +
                    " desiredStatus= " + l_desiredStatus);
            }  
            try 
            {
                if ( l_desiredStatus.equals(C_ACTIVE) )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            11060,
                            this,
                            "ACTIVE - call changeTemporaryBlocking!!");
                    }  

                    l_temporaryBlockingStatus = false; // Not blocking
                    i_ppasAccountService.changeTemporaryBlocking(
                            l_ppasRequest, 
                            i_isApiTimeout, 
                            l_temporaryBlockingStatus );

                }
                else if ( l_desiredStatus.equals(C_NOT_ACTIVE) )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            11070,
                            this,
                            "NOT ACTIVE - call changeTemporaryBlocking!!");
                    }  

                    l_temporaryBlockingStatus = true;  // Set blocking
                    i_ppasAccountService.changeTemporaryBlocking(
                            l_ppasRequest, 
                            i_isApiTimeout, 
                            l_temporaryBlockingStatus );

                }
                else if ( l_desiredStatus.equals(C_PERMANENT_DISCONNECTED) )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            11080,
                            this,
                            "PERMANENT_DISCONNECTED call futureDisconnect!!");
                    }  

                    DisconnectData l_disconnectData = i_disconnectService.futureDisconnect(
                            l_ppasRequest, 
                            i_isApiTimeout,
                            i_disconnectionReason,
                            l_disconnectionDate );  
                    
                    
                    if (PpasDebug.on)
                    {
                        StringBuffer l_tmp = new StringBuffer();
                        l_tmp.append("DisconnectData\n          custId=" + l_disconnectData.getCustId());
                        l_tmp.append("\n          reason=" + l_disconnectData.getDisconnectReason());
                        l_tmp.append("\n          opId=" + l_disconnectData.getOpid());            
                        l_tmp.append("\n          batch=" + l_disconnectData.getBatchDisconnectedFg());
                        if ( l_disconnectData.getDisconnectDateTime() != null )
                        {
                            l_tmp.append("\n          cudi/current=" + 
                                         l_disconnectData.getDisconnectDateTime().toString_yyyyMMdd_HHmmss());
                        }
                        if ( l_disconnectData.getDisconnectDateTime() != null )
                        {
                            l_tmp.append("\n          discon=" + 
                                         l_disconnectData.getDisconnectDateTime().toString_yyyyMMdd_HHmmss());
                        }
                        if ( l_disconnectData.getDisconnectEndServDate().toString_yyyyMMdd() != null )
                        {
                            l_tmp.append("\n          disconnectEndServ=" + 
                                         l_disconnectData.getDisconnectEndServDate().toString_yyyyMMdd());
                        }
                        if (     l_disconnectData.getEndServDate() != null )
                        {
                            l_tmp.append("\n          endServ/future=" + 
                                         l_disconnectData.getEndServDate().toString_yyyyMMdd());
                        }

                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        11080,
                                        this,
                                        l_tmp.toString());
                    }  

                }
                else
                {
                    // Should not happen - already checked in the BatchReader!
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            11090,
                            this,
                            C_METHOD_processRecord + " Illegal Desired status : ]" + l_desiredStatus + "[" );
                    }
                }                
            }
            
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        11100,
                        this,
                        "PpasServiceException is caught: " + p_psExe.getMessage() + 
                        ",  key: " + p_psExe.getMsgKey());
                }
                
                // Create and set the recovery line for an erroneous record.
                l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);

                l_tmpErrorLine = new StringBuffer();

                if ( p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT))
                {
                    // A not available/non-existent/invalid MSISDN, set error line in batch record.
                    l_tmpErrorLine.append(C_ERROR_CODE_STATUS_CANNOT_BE_CHANGED);
                }

                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE) ||
                    p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS)    ||
                    p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_INPUT_MSISDN) )
                {
                    // A not available/non-existent/invalid MSISDN, set error line in batch record.
                    l_tmpErrorLine.append(C_ERROR_CODE_MSISDN_NOT_AVAILABLE);
                }

                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DATABASE_INCONSISTENCY)  )
                {
                    // A not available/non-existent/invalid MSISDN, set error line in batch record.
                    l_tmpErrorLine.append(C_ERROR_CODE_DATABASE_INCONSISTENCY);
                }

                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT) )
                {
                    // A not available/non-existent/invalid MSISDN, set error line in batch record.
                    l_tmpErrorLine.append(C_ERROR_CODE_ALREADY_DISCONNECTED);
                }
                
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_MISMATCH) )
                {
                    // A not available/non-existent/invalid MSISDN, set error line in batch record.
                    l_tmpErrorLine.append(C_ERROR_CODE_ALREADY_DISCONNECTED);
                }

                else
                {
                    // This is either a fatal or an unexpected error.
                    if (p_psExe.getLoggingSeverity() == PpasServiceException.C_SEVERITY_FATAL)
                    {
                        // This is a fatal error, re-throw the 'PpasServiceException' in order to stop
                        // the complete process.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                11110,
                                this,
                                "***ERROR: A fatal error has occurred during batch Status Change -- " + 
                                "msg key: " + p_psExe.getMsgKey());
                        }
                        throw p_psExe;
                    }

                    // An unexpected, but not fatal, error occurred, report it as invalid record.
                    l_tmpErrorLine.append(C_ERROR_CODE_OTHER_ERROR);
                }
                
                l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                l_tmpErrorLine.append(l_batchRecordData.getInputLine());
                l_errorLine = l_tmpErrorLine.toString();
                l_batchRecordData.setErrorLine(l_errorLine);

            }

            if (l_errorLine == null)
            {
                l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);
            }

        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11120,
                    this,
                    C_METHOD_processRecord + " NOT PROCESSED " );
            }

            l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
        }

      
        // Always create a recovery line
        l_batchRecordData.setRecoveryLine(l_tmpRecoveryLine.toString());


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11130,
                this,
                BatchConstants.C_LEAVING + C_METHOD_processRecord + l_batchRecordData.dumpRecord());
        }

        return l_batchRecordData;
        
    } // End of processRecord()

}
