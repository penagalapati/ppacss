////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RefillBatchWriterUT.java 
//      DATE            :       18-Jul-2006
//      AUTHOR          :       Yang Liu
//      REFERENCE       :       PRD_ASCS00_GEN_CA_093
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Unit test for RefillBatchWriter
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//14/08/06 | M Erskine     | Uncomment failing code.          | PpacLon#2479/9683
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.RefillBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.RefillBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**Unit test for RefillBatchWriter. */

public class RefillBatchWriterUT extends BatchTestCaseTT
{
    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService       i_controlService;
    
    /** Reference to RefillBatch controller object. */
    private static RefillBatchWriter      c_refillBatchWriter;
    
    /** Reference to refillBatch controller object. */
    private static RefillBatchController  c_refillBatchController;
    
    /**A short file name vid extension .DAT. */    
    private static final String C_FILE_NAME_DAT = "BATCH_REFILL_20060717_00003.DAT";
    
    /** A short file name vid extension .RPT. */
    private static final String           C_FILE_NAME_ERR = "BATCH_REFILL_20060717_00003.TMP";
    
    /** A short file name vid extension .RPT. */
    private static final String           C_FILE_NAME_RCV = "BATCH_REFILL_20060717_00003.RCV";
    
    /** The file date. */
    private static final String           C_FILE_DATE     = "15-Jul-2006";
    
    /** The sequence number. */
    private static final String           C_SEQ_NO        = "0003";
    
    /** The sub job counter. */
    private static final String           C_SUB_JOB_COUNT = "-1";
    /**
     * The constructor for the class RefillBatchWriterUT.
     * @param p_name the name for this test class.
     */
    public RefillBatchWriterUT(String p_name)
    {
        super(p_name, "batch_refill");
    }
    
    /**Tests getRecord(). */
    public void testGetRecord()
    {
        BatchRecordData l_dataRecord;

        super.beginOfTest("testGetRecord");

        try
        {
            l_dataRecord = c_refillBatchWriter.getRecord();

            assertNotNull("Data Record is Null", l_dataRecord);
            assertTrue("Data Record is not of type RefillBatchRecordData", 
                    (l_dataRecord instanceof RefillBatchRecordData) );
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        super.endOfTest();
    }
    
    /**
     * Test writeRecord().
     * <p> Inserts a row into baco_batch_control
     *     Creates a <code>RefillBatchRecordData</code> with a error line set.
     *     Updates the table baco_batch_control by calling updateControlInfo().
     *     Gets the updated row from the table and check if the value in the column baco_failure has been 
     *     updated.
     *     Sets the value for error line in the object <code>RefillBatchRecordData</code> to null and updates 
     *     the table baco_batch_control by calling updateStatus(). Gets the updated row from the table and 
     *     checks if the column baco_success has been incremented.
     * So by testing the writeRecord() method, the methods updateStatus() and updateControlInfo() are being 
     * tested.
     */
    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        createContext();
        RefillBatchRecordData l_dataRecord = new RefillBatchRecordData();

        String          l_actualText    = null;
        BatchJobControlData    l_batchData1    = null;
        BatchJobControlData    l_batchData     = null;
        String          l_expectedKey   = null;
        String          l_actualKey     = null;
        String          l_errorLine     = "2 ERR";
        String          l_recoveryLine  = "1 REC";
        PpasDateTime    l_executionDateTime = null;
        long            l_jsJobId           = 0;
        
        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_refillBatchController.getKeyedBatchJobControlData();
            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            
            l_batchData1.setJobType(BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE);
            l_batchData1.setStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS.charAt(0));

            l_batchData1.setSubJobCnt(Integer.parseInt(C_SUB_JOB_COUNT));
            l_batchData1.setFileDate(new PpasDate(C_FILE_DATE));
            l_batchData1.setSeqNo(Integer.parseInt(C_SEQ_NO));
            l_batchData1.setNoOfSuccessfullyRecs(0);
            l_batchData1.setNoOfRejectedRecs(0);          
            l_batchData1.setOpId(C_DEFAULT_OPID);

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                c_refillBatchWriter.writeRecord(l_dataRecord);

                l_batchData = i_controlService.getJobDetails(c_ppasRequest,
                                                             l_jsJobId,
                                                             l_executionDateTime,
                                                             C_TIMEOUT);

                l_expectedKey = l_jsJobId + l_executionDateTime.toString_yyyyMMdd() + "1" + "0";

                l_actualKey = l_batchData.getJsJobId() +
                              l_batchData.getExecutionDateTime().toString_yyyyMMdd() +
                              l_batchData.getNoOfRejectedRecs() + l_batchData.getNoOfSuccessfullyRecs();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
            
            //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_ERR,
                                                     BatchConstants.C_EXTENSION_TMP_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

            // Test when there is no error line set i.e. success count should be incremented by 1.
            try
            {
                l_dataRecord.setErrorLine(null);
                l_dataRecord.setIsRecord(true);
                c_refillBatchWriter.writeRecord(l_dataRecord);
                
                l_batchData = i_controlService.getJobDetails(c_ppasRequest,
                                                             l_jsJobId,
                                                             l_executionDateTime,
                                                             C_TIMEOUT);
                
                l_actualKey = l_batchData.getJsJobId() +
                              l_batchData.getExecutionDateTime().toString_yyyyMMdd() +
                              l_batchData.getNoOfRejectedRecs() + l_batchData.getNoOfSuccessfullyRecs();
                
                l_expectedKey = l_jsJobId + l_executionDateTime.toString_yyyyMMdd() + "1" + "1";

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);

            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
    
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(RefillBatchWriterUT.class);
    }
    
    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        createContext();
    }
    
    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    
    /** Creates the Context suitable for this test class. */
    private void createContext()
    {
        SizedQueue l_outQueue = null;
        RefillBatchRecordData l_batchRecordData = new RefillBatchRecordData();
        
        String          l_dir       = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String          l_fileReportName = l_dir + "/" + C_FILE_NAME_ERR; 
        String          l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String          l_fileRecoveryName = l_dir2 + "/" + C_FILE_NAME_RCV;
        
        // The update frequency needs to be 1 for the test to work.
        c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");
        
        
        if (c_refillBatchWriter == null)     
        {
            try
            {
                l_outQueue = new SizedQueue("BatchWriter", 1, null);
                
                l_outQueue.addFirst(l_batchRecordData);
            }
            catch (SizedQueueInvalidParameterException e)
            {
                super.failedTestException(e);
            }
            catch (SizedQueueClosedForWritingException e)
            {
                super.failedTestException(e);
            }
            Hashtable l_params = new Hashtable();
            l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_FILE_NAME_DAT);
            
            super.deleteFile(l_fileReportName);
            super.deleteFile(l_fileRecoveryName);
            
            try
            {
                c_refillBatchController = new RefillBatchController(
                                                  c_ppasContext,
                                                  BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                                  super.getJsJobID(),
                                                  "RefillBatchWriterUT",
                                                  l_params);
                
                c_refillBatchWriter = new RefillBatchWriter(
                                                  c_ppasContext,
                                                  c_logger,
                                                  c_refillBatchController,
                                                  l_params,
                                                  l_outQueue,
                                                  c_properties);
            }
            catch (PpasException e)
            {
                failedTestException(e);
            }
            catch (RemoteException e)
            {
                super.failedTestException(e);
            }
            catch (IOException e)
            {
                super.failedTestException(e);
            }
            
        }
    }
}
