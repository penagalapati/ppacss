// //////////////////////////////////////////////////////////////////////////////
//ASCS : 9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME : NPCutBatchReaderUT.java
//DATE : Aug 20, 2004
//AUTHOR : Urban Wigstrom
//REFERENCE : PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT : ATOS ORIGIN 2004
//
//DESCRIPTION : Unit test for Number Plan Cutover Reader.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE | NAME | DESCRIPTION | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name> | <brief description of change> | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for Number Plan Cutover Reader. */

public class NPCutBatchReaderUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** The name of the indata file used in normal mode. */
    private static final String     C_INDATA_FILENAME_NORMAL_MODE   = "NUMBER_CUTOVER_20040810_00001.DAT";

    /** The name of the indata file used in recovery mode. */
    private static final String     C_INDATA_FILENAME_RECOVERY_MODE = "NUMBER_CUTOVER_20040810_00001.IPG";

    /** The name of the in-queue. */
    private static final String     C_QUEUE_NAME                    = "In-queue";

    /** The maximum size of the queue. */
    private static final long       C_MAX_QUEUE_SIZE                = 100;

    /** Error code. Value is {@value}. */
    private static final String     C_ERROR_INVALID_RECORD_FORMAT   = "01     0208000001               ";
    
    /** Error code. Value is {@value}. */
    private static final String     C_ERROR_INVALID_RECORD_FORMAT2   = "01                    0208000002";
    
    /** Errornous record. Value is {@value}. */
    private static final String     C_ERROR_INVALID_RECORD_FORMAT3   = "01     0208000001     02080000023";
    
    /** A to long record line in the indata file. */
    private static final String     C_TO_LONG_RECORD_LINE           = "     0208000001     02080000023";

    /** Missing a new MSISDN record in the indata file. */
    private static final String     C_MISSING_NEW_MSISDN_LINE       = "     0208000001               ";

    /** Missing a old MSISDN record in the indata file. */
    private static final String     C_MISSING_OLD_MSISDN_LINE       = "                    0208000002";

    /** The record line in a indata file. Not comma seperated. */
    private static final String     C_RECORD_LINE                   = "     0208000001     0208000002";

    /** The record line in a indata file. Comma seperated. */
    private static final String     C_RECORD_LINE_COMMA             = "     0208000001,     0208000002";

    /** A reference to the <code>BatchController</code> that is used in this test. */
    private BatchController         i_controller                    = null;

    /** A reference to the <code>NPCutBatchReader</code> that is used in this test. */
    private static NPCutBatchReader c_batchReader                   = null;

    /**
     * Class constructor.
     * @param p_name the
     */
    public NPCutBatchReaderUT(String p_name)
    {
        super(p_name);
        //super.enableConsoleLogAll();
    }

    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(NPCutBatchReaderUT.class);
    }

    /** Test if a file name is valid. */
    public void testIsFileNameValid()
    {
        super.beginOfTest("testIsFileNameValid");

        boolean l_valid = false;

        l_valid = c_batchReader.isFileNameValid(C_INDATA_FILENAME_NORMAL_MODE);
        assertTrue(C_INDATA_FILENAME_NORMAL_MODE + " is not a valid filename ", l_valid);

        l_valid = c_batchReader.isFileNameValid(C_INDATA_FILENAME_RECOVERY_MODE);
        assertTrue(C_INDATA_FILENAME_RECOVERY_MODE + " is not a valid filename ", l_valid);

        super.endOfTest();
    }

    /** Test to get a record. Has to be before testRecoveryBatchMode(). */
    public void testGetRecord()
    {
        super.beginOfTest("testGetRecord");

        BatchRecordData l_recordData = null;

        //Test when the input line not is comma seperated.
        l_recordData = c_batchReader.getRecord();

        assertEquals("The input line in the record wrong", C_RECORD_LINE, l_recordData.getInputLine());

        //Test when the input line is comma seperated.
        l_recordData = c_batchReader.getRecord();

        assertEquals("The input line in the record wrong", C_RECORD_LINE_COMMA, l_recordData.getInputLine());

        //Test when the input line missing new msisdn.
        l_recordData = c_batchReader.getRecord();

        assertEquals("The error line for missing new Msisdn in the record wrong!",
                     C_ERROR_INVALID_RECORD_FORMAT,
                     l_recordData.getErrorLine());

        //Test when the input line missing old msisdn.
        l_recordData = c_batchReader.getRecord();

        assertEquals("The error line for missing old Msisdn in the record wrong!",
                     C_ERROR_INVALID_RECORD_FORMAT2,
                     l_recordData.getErrorLine());

        //Test when the input line is to long.
        l_recordData = c_batchReader.getRecord();

        assertEquals("The the input line is to long!",
                     C_ERROR_INVALID_RECORD_FORMAT3,
                     l_recordData.getErrorLine());

        super.endOfTest();
    }
    
    /**
     * Sets up before test execution.
     */
    protected void setUp()
    {
        c_writeUtText = true;
        super.setUp();
        createContext();
    }

    /**
     * Tidies up after test execution.
     */
    protected void tearDown()
    {
        super.tearDown();
    }

    /**
     * This tests context.
     */
    private void createContext()
    {
        StringBuffer l_tmpFullPathName = null;
        Hashtable l_parameters = new Hashtable();
        SizedQueue l_queue = null;
        String l_inputFileDirectory = null;
        String l_fullPathName = null;
        String[] l_linearray = {C_RECORD_LINE, C_RECORD_LINE_COMMA, C_MISSING_NEW_MSISDN_LINE,
            C_MISSING_OLD_MSISDN_LINE, C_TO_LONG_RECORD_LINE};

        //        String[] l_linearray = {i_recordLine, C_MISSING_NEW_MSISDN_LINE, C_MISSING_OLD_MSISDN_LINE};

        l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INDATA_FILENAME_NORMAL_MODE);

        l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

        if (c_batchReader == null)
        {
            //Delete the recovery file (.IPG) in the directory.
            l_tmpFullPathName = new StringBuffer();
            l_tmpFullPathName.append(l_inputFileDirectory);
            l_tmpFullPathName.append("/");
            l_tmpFullPathName.append(C_INDATA_FILENAME_RECOVERY_MODE);
            l_fullPathName = l_tmpFullPathName.toString();
            super.deleteFile(l_fullPathName);

            //Create a input file (.DAT) in the directory.
            l_tmpFullPathName = new StringBuffer();
            l_tmpFullPathName.append(l_inputFileDirectory);
            l_tmpFullPathName.append("/");
            l_tmpFullPathName.append(C_INDATA_FILENAME_NORMAL_MODE);
            l_fullPathName = l_tmpFullPathName.toString();

            super.createNewFile(l_fullPathName, l_linearray);

            try
            {
                l_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null);
            }
            catch (SizedQueueInvalidParameterException e)
            {
                super.failedTestException(e);
            }

            c_batchReader = new NPCutBatchReader(super.c_ppasContext,
                                                 super.c_logger,
                                                 i_controller,
                                                 l_queue,
                                                 l_parameters,
                                                 super.c_properties);

            assertNotNull("Failed to create a NPChBatchReader instance.", c_batchReader);

            try
            {
                Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
            }
            catch (InterruptedException e)
            {
                //Do nothing
            }
        }
    }

    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + (p_args.length == 0 ? "" : "ignored"));
        TestRunner.run(suite());
    }

} // End of class NPChBatchReaderUT
