////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME         : SubSegmentationChgBatchController.java
//      DATE              : 21 June 2007
//      AUTHOR            : Ian James
//      REFERENCE         : PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT         : WM-data 2007
//
//      DESCRIPTION       : This class is the main entry point for starting the
//                          Subscriber Segmentation Change batch.
//
////////////////////////////////////////////////////////////////////////////////
//                         CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+-----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.SubSegmentationChgBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.SubSegmentationChgBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.SubSegmentationChgBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/**
 * This class is the main entry point for starting the Subscriber Segmentation Change batch.
 */
public class SubSegmentationChgBatchController extends BatchController
{
    //-------------------------------------------------------------------------
    //  Class level constant.
    //-------------------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME           = "SubSegmentationChgController";

    /** The additional properties layer to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS  = "batch_ssc";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME     = "Invalid filename";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";

    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int C_INDEX_FILE_DATE         = 4; // 4th element in the filename is the date

    /** Array index to fine the sequence number from the filename.  Value is {@value}. */
    private static final int C_INDEX_SEQUENCE_NUMBER   = 5; // 6th element in the filename is the file seq no.

    //-------------------------------------------------------------------------
    // Instance variables.
    //-------------------------------------------------------------------------

    /** The js job id. */
    private long             i_jsJobId                 = 0;

    /** The execution dateTime. */
    private PpasDateTime     i_executionDateTime       = null;

    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * @param p_ppasContext A <code>PpasContext</code> object.
     * @param p_jobType The job type (e.g. batchSubscriberSegmentation).
     * @param p_jobId A unique id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_parameters A <code>Map</code> object holding parameters to use when executing this job.
     * Recovery - "yes" or "no". Indicating rev�covery or not. StartRange - Lowest customer id to disconnect.
     * EndRange - Highest customer to disconnect.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     * @throws PpasServiceException
     */
    public SubSegmentationChgBatchController(PpasContext p_ppasContext,
                                             String p_jobType,
                                             String p_jobId,
                                             String p_jobRunnerName,
                                             Map p_parameters)
    throws PpasConfigException, PpasServiceException
    {
        super(p_ppasContext, p_jobType, p_jobId, p_jobRunnerName, p_parameters);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this, BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        p_ppasContext.validateFeatureLicence(FeatureLicence.C_FEATURE_CODE_BATCH_SUBSCRIBER_SEGMENTATION_CHANGE);

        this.init();
        i_jsJobId = Long.parseLong(p_jobId);
        i_executionDateTime = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this, BatchConstants.C_CONSTRUCTED);
        }
    } // End of constructor SubSegmentaionChgBatchController(.....)

    //-------------------------------------------------------------------------
    // Protected instance methods.
    //-------------------------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialise this <code>SubSegmentationChgBatchController</code> instance.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this, BatchConstants.C_ENTERING + C_METHOD_init);
        }

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this, BatchConstants.C_LEAVING + C_METHOD_init);
        }

        return;

    } // End of init()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";

    /**
     * Creates and returns the correct reader-class instance and passes on the required parameters and also a
     * reference to this controller. The later is used for the reader-component to report status back to the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return a reference to a SubSegmentationChgBatchReader instance.
     */
    public BatchReader createReader()
    {
        SubSegmentationChgBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this, BatchConstants.C_ENTERING + C_METHOD_createReader);
        }
      
        // Instantiate the reader and pass reference to controller
        if (super.i_startComponents)
        {
             l_reader = new SubSegmentationChgBatchReader(super.i_ppasContext,
                                                          super.i_logger,
                                                          this,
                                                          super.i_inQueue,
                                                          super.i_params,
                                                          super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this, BatchConstants.C_LEAVING + C_METHOD_createReader);
        }

        return l_reader;
    } // End of createReader()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /**
     * Creates and returns the correct writer-class instance and passes on the required parameters and also a
     * reference to this controller. The later is used for the writer-component to report status back the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return a reference to a SubscriberSegmentationBatchWriter instance.
     * @throws IOException if the writer fails to open the output file.
     */
    protected BatchWriter createWriter() throws IOException
    {
        SubSegmentationChgBatchWriter l_writer = null;
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this, BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        // Instantiate the writer and pass reference to controller
        if (super.i_startComponents)
        {
            l_writer = new SubSegmentationChgBatchWriter(super.i_ppasContext,
                                                         super.i_logger,
                                                         this,
                                                         super.i_outQueue,
                                                         super.i_params,
                                                         super.i_properties);
        }    

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END, 
                            C_CLASS_NAME, 10060, this, BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /**
     * Creates and returns the correct processor-class instance and passes on the required parameters and also
     * a reference to this controller. The later is used for the processor-component to report status back the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return a reference to a SubSegmentationChgBatchProcessor instance.
     */
    protected BatchProcessor createProcessor()
    {
        SubSegmentationChgBatchProcessor l_processor = null;
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10070, this, BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        // Instantiate the processor and pass reference to controller
        if (super.i_startComponents)
        {
            l_processor = new SubSegmentationChgBatchProcessor(super.i_ppasContext,
                                                               super.i_logger,
                                                               this,
                                                               super.i_inQueue,
                                                               super.i_outQueue,
                                                               super.i_params,
                                                               super.i_properties);
        }    

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10080, this, BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }

        return l_processor;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";

    /**
     * This method is responsible to insert a record into table BACO using the defined service method
     * PpasBatchCOntrolService.addControlInformation(). The method will get a BatchJobData object and populate
     * it with required parameters. Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        BatchJobControlData l_jobCtrlData = null;
        String l_inFileName = null;

        String l_errorMsg = C_INVALID_FILENAME;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10090, this, BatchConstants.C_CONSTRUCTING + C_METHOD_addControlInfo);
        }

        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);

        l_jobCtrlData = this.getKeyedControlRecord();
        l_jobCtrlData.setJobType(BatchConstants.C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION);
        l_jobCtrlData.setOpId(super.getSubmitterOpid());
        l_jobCtrlData.setSubJobCnt(-1);

        if (isFileNameValid(l_inFileName,
                            BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_DAT,
                            BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_IPG,
                            BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_SCH,
                            C_INDEX_FILE_DATE, 
                            C_INDEX_SEQUENCE_NUMBER))
            
        {
            l_jobCtrlData.setStatus((BatchConstants.C_BATCH_STATUS_INPROGRESS).charAt(0));
            l_jobCtrlData.setFileDate(i_fileDate.length() > 8 ? 
                    new PpasDateTime(i_fileDate, PpasDateTime.C_DF_yyyyMMddHH_mm_ss)
                    : new PpasDate(i_fileDate, PpasDateTime.C_DF_yyyyMMdd));
            l_jobCtrlData.setSeqNo(i_seqNo == null ? -1 : new Integer(i_seqNo).intValue());
            l_jobCtrlData.setSubSeqNo(-1);
            l_jobCtrlData.setNoOfSuccessfullyRecs(0L);
            l_jobCtrlData.setNoOfRejectedRecs(0L);
        }
        else
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            // Invalid filename, special batch job info added.
            l_jobCtrlData.setStatus('F');

            if (l_inFileName != null)
            {
                if (l_errorMsg.length() + l_inFileName.length() + 2 <= 30)
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }

            l_jobCtrlData.setExtraData4(l_errorMsg);

            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + C_INVALID_FILENAME
                    + ": " + l_inFileName, LoggableInterface.C_SEVERITY_ERROR));
        }

        try
        {
            super.i_batchContService.addJobDetails(null, l_jobCtrlData, i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10100, this, C_METHOD_addControlInfo + " PpasServiceException from addJobDetails");
            }
            e.printStackTrace();
        }

        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10110, this, BatchConstants.C_CONSTRUCTED + C_METHOD_addControlInfo);
        }
    } // End of addControlInformation()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * It updates the database with a new status when a batch have got the status finished (c), stopped (x) or
     * error (f). The metohd is called from the super class. The <code>BatchJobData</code>object used in
     * the method are first fetched form <code>getKeyedControlRecord()</code> and then the status is
     * inserted into it.
     * @param p_status The new status.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobControlData l_jobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10120, this, BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        l_jobData = getKeyedControlRecord();
        l_jobData.setStatus(p_status.charAt(0));

        try
        {
            super.i_batchContService.updateJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10130, this, C_METHOD_updateStatus + " PpasServiceException from updateStatus");
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10140, this, BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }

        return;
    } // End of updateStatus()

    /** 
     * This method will return a BatchJobData-object with the correct key-values set 
     * (JsJobId and executionDateTime). The caller can then populate the record with data needed 
     * for the operation in question. For example, the writer will get such record and
     * add number of successfully/faulty records processed and then update.
     * @return BatchJobData
     */
    public BatchJobControlData getKeyedControlRecord()
    {
        BatchJobControlData l_jobCtrlData = new BatchJobControlData(this.i_executionDateTime, 
                                                                    this.i_jsJobId);

        return l_jobCtrlData;
    } // End of getKeyedControlRecord()
}