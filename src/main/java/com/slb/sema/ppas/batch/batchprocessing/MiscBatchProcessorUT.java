//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       MiscBatchProcessorUT.java
//    DATE            :       23-June-2004
//    AUTHOR          :       Olivier Duparc
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       WM-data 2005
//
//    DESCRIPTION     :       Unit Test for the 'MiscBatchProcessor' class.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
//26/04/07 | L. Lundberg   | Method processRecord(...)        | PpacLon#3033/11279
//         |               | is modified:                     |
//         |               | The MiscBatchController          |
//         |               | constructor does not throw any   |
//         |               | IOException anymore.             |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.text.ParseException;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.MiscBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.MiscBatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit tests for the Miscellaneous Data Upload batch. */
public class MiscBatchProcessorUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** The name of this class. Values is {@value}. */
    private static final String C_CLASS_NAME = "MiscBatchProcessorUT";

    /** Short file name with extention .DAT. Value is (@value). */
    private static final String C_FILE_SHORT_NAME_DAT = "INSTALL_20040615_00003.DAT";
    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    
    /** Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public MiscBatchProcessorUT(String p_title)
    {
        super(p_title, "batch_mdu");
    }


    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testConstructor = "testConstructor";
    /**
     * Test to construct a <code>MiscBatchProcessor</code> instance.
     */
    public void testConstructor()
    {
        MiscBatchProcessor l_miscBatchProcessor = null;
        SizedQueue         l_inQueue            = null;
        SizedQueue         l_outQueue           = null;

        try
        {
            l_inQueue  = new SizedQueue("MiscBatchProcessUT indata queue", 10, null);
            l_outQueue = new SizedQueue("MiscBatchProcessUT output queue", 10, null);
        }
        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        l_miscBatchProcessor = new MiscBatchProcessor(
                                    super.c_ppasContext, 
                                    super.c_logger,
                                    null,
                                    l_inQueue,
                                    l_outQueue,
                                    null,
                                    super.c_properties);
        
        assertNotNull("Failed to create a MiscBatchProcessor instance.", l_miscBatchProcessor);

        System.out.println(C_CLASS_NAME + C_METHOD_testConstructor + "-- Completed.");
    }


    /**
     * Processes a record.
     * @param p_miscRecordData the data record to process
     */
    private void processRecord(MiscBatchRecordData  p_miscRecordData)
    {
        MiscBatchProcessor   l_miscBatchProcessor  = null;
        SizedQueue           l_inQueue             = null;
        SizedQueue           l_outQueue            = null;
        MiscBatchController  l_miscBatchController = null;
        
        try
        {
            l_inQueue  = new SizedQueue("MiscBatchProcessUT indata queue", 10, null);
            l_outQueue = new SizedQueue("MiscBatchProcessUT output queue", 10, null);
        }
        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        // Create MiscBatchController
        try
        {
            HashMap l_params = new HashMap();
            l_params.put(BatchConstants.C_KEY_INPUT_FILENAME,   C_FILE_SHORT_NAME_DAT);
            l_params.put(BatchConstants.C_KEY_SERVICE_AREA,     "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_LOCATION, "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_CLASS,    "01");

            l_miscBatchController = new MiscBatchController(super.c_ppasContext,
                                                            "Sub_MISC",
                                                            "1",
                                                            "MiscUT",
                                                            l_params);
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }

        // Create MiscBatchProcessor
        l_miscBatchProcessor = new MiscBatchProcessor(
                                    super.c_ppasContext, 
                                    super.c_logger,
                                    l_miscBatchController,
                                    l_inQueue,
                                    l_outQueue,
                                    null,
                                    super.c_properties);
        
        assertNotNull("Failed to create a MiscBatchProcessor instance.", l_miscBatchProcessor);

        // Process record
        try 
        {
            l_miscBatchProcessor.processRecord(p_miscRecordData);
        }
        catch (PpasServiceException l_e)
        {
            l_e.printStackTrace();
            super.fail("Unexpected exception occurred");        
        }

        System.out.println(C_CLASS_NAME + C_METHOD_testRecordInsert + "-- Completed.");
    }


    /**
     * Retrieves a record and returns a MiscBatchRecordData object.
     * @param p_msisdn the MSISDN to query
     * @return MiscBatchRecordData Miscellaneous Batch data record for the given MSISDN
     */
    private MiscBatchRecordData getMiscValuesAsBatchDataRecord (String p_msisdn)
    {
        SqlString           l_sql          = null;
        MiscBatchRecordData l_ret          = null;
        JdbcResultSet       l_res          = null;
        Msisdn              l_msisdn       = null;
        MsisdnFormat        l_msisdnFormat = null;


        l_sql = new SqlString(
                        1000,1,
                        "select " + 
                        "misc.CUST_MISC_FIELD1,  " + 
                        "misc.CUST_MISC_FIELD2,  " + 
                        "misc.CUST_MISC_FIELD3,  " + 
                        "misc.CUST_MISC_FIELD4,  " + 
                        "misc.CUST_MISC_FIELD5,  " + 
                        "misc.CUST_MISC_FIELD6,  " + 
                        "misc.CUST_MISC_FIELD7,  " + 
                        "misc.CUST_MISC_FIELD8,  " + 
                        "misc.CUST_MISC_FIELD9,  " + 
                        "misc.CUST_MISC_FIELD10, " + 
                        "misc.CUST_MISC_FIELD11, " + 
                        "misc.CUST_MISC_FIELD12, " + 
                        "misc.CUST_MISC_FIELD13, " + 
                        "misc.CUST_MISC_FIELD14, " + 
                        "misc.CUST_MISC_FIELD15, " + 
                        "misc.CUST_MISC_FIELD16, " + 
                        "misc.CUST_MISC_FIELD17, " + 
                        "misc.CUST_MISC_FIELD18, " + 
                        "misc.CUST_MISC_FIELD19, " + 
                        "misc.CUST_MISC_FIELD20, " + 
                        "misc.CUST_MISC_FIELD21, " + 
                        "misc.CUST_MISC_FIELD22, " + 
                        "misc.CUST_MISC_FIELD23, " + 
                        "misc.CUST_MISC_FIELD24, " + 
                        "misc.CUST_MISC_FIELD25, " + 
                        "misc.CUST_MISC_FIELD26, " + 
                        "misc.CUST_MISC_FIELD27, " + 
                        "misc.CUST_MISC_FIELD28, " + 
                        "misc.CUST_MISC_FIELD29, " + 
                        "misc.CUST_MISC_FIELD30, " + 
                        "misc.CUST_MISC_FIELD31, " + 
                        "misc.CUST_MISC_FIELD32, " + 
                        "misc.CUST_MISC_FIELD33, " + 
                        "misc.CUST_MISC_FIELD34, " + 
                        "misc.CUST_MISC_FIELD35, " + 
                        "misc.CUST_MISC_FIELD36, " + 
                        "misc.CUST_MISC_FIELD37, " + 
                        "misc.CUST_MISC_FIELD38, " + 
                        "misc.CUST_MISC_FIELD39, " + 
                        "misc.CUST_MISC_FIELD40 " +
                        "from CUST_MISC misc, " + 
                        "     CUST_MAST mast " +
                        "where mast.CUST_MOBILE_NUMBER = {0} and " + 
                        "      misc.CUST_ID            = mast.CUST_ID ");
                        
        l_sql.setStringParam(0,p_msisdn);
        
        try
        {
            l_res = sqlQuery(l_sql);
            if (l_res != null)
            {
                while (l_res.next(10000))
                {
                    l_ret = new MiscBatchRecordData();
                    l_msisdnFormat = c_ppasContext.getMsisdnFormatInput();
                    
                    try
                    {
                        l_msisdn = l_msisdnFormat.parse( p_msisdn );
                    }
                    catch (ParseException e)
                    {
                        failedTestException(e);
                    }
                    
                    l_ret.setMsisdn(l_msisdn);
                    l_ret.setOperationCode("I");
                    for (int l_i = 1; l_i <= 40; l_i++)
                    {
                        l_ret.setField(l_i, l_res.getString(10100, l_i));
                    }
                    // Use only first one
                    break;
                }
            }
        }
        catch(PpasSqlException e)
        {
            failedTestException(e);        
        }
        return l_ret;
    }

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testRecordInsert = "testRecordInsert";
    /**
     * Test a record of type 'INSERT'.
     */
    public void testRecordInsert()
    {
        super.c_writeUtText = true;
        super.beginOfTest(C_METHOD_testRecordInsert);

        // Build Miscellaneous data record
        MiscBatchRecordData l_retMiscRecordData = null; 
        MiscBatchRecordData l_miscRecordData    = new MiscBatchRecordData(); 
        BasicAccountData    l_sub               = null;
        
        try
        {
            l_sub = c_dbService.installTestSubscriber(null);

            l_miscRecordData.setMsisdn(l_sub.getMsisdn());
            l_miscRecordData.setOperationCode("I");
            l_miscRecordData.setField(1,"VAL_1");
            l_miscRecordData.setField(2,"VAL_2");
            l_miscRecordData.setField(3,"VAL_3");

            processRecord(l_miscRecordData);
            
            // Check Misc Data record has been inserted
            l_retMiscRecordData = getMiscValuesAsBatchDataRecord(
                    c_ppasContext.getMsisdnFormatDb().format(l_sub.getMsisdn()));
            
        }
        catch (PpasServiceException e)
        {
            fail("MDU Creating subscriber:" + e.getMessage());
        }
        
        assertNotNull("getMiscValuesAsBatchDataRecord failed", l_retMiscRecordData);

        say("***** ACTUAL   ****** " +
            (l_retMiscRecordData != null ? l_retMiscRecordData.dumpRecord() : "null"));
        say("***** EXPECTED ****** " + l_miscRecordData.dumpRecord());
        assertTrue("The actual data is not equal the expected.",
                   l_miscRecordData.dataEquals(l_retMiscRecordData));
        
        super.endOfTest();
    }

    /**
     * Test a record of type 'UPDATE'.
     */
    public void testRecordUpdate()
    {
        super.c_writeUtText = true;
        super.beginOfTest("testRecordUpdate");

        // Build Miscellaneous data record
        MiscBatchRecordData l_retMiscRecordData = null; 
        MiscBatchRecordData l_miscRecordData    = new MiscBatchRecordData(); 
        BasicAccountData    l_sub               = null;
        
        try
        {
            l_sub = c_dbService.installTestSubscriber(null);
            assertNotNull("installTestSubscriber failed", l_sub);
            
            //Insert data
            l_miscRecordData.setMsisdn(l_sub.getMsisdn());
            l_miscRecordData.setOperationCode("I");
            l_miscRecordData.setField(1,"VAL_1");
            l_miscRecordData.setField(2,"VAL_2");
            l_miscRecordData.setField(3,"VAL_3");

            processRecord(l_miscRecordData);

            //Udpate data
            l_miscRecordData.setMsisdn(l_sub.getMsisdn());
            l_miscRecordData.setOperationCode("U");
            l_miscRecordData.setField(1,"VAL_1_upd");
            l_miscRecordData.setField(2,"VAL_2_upd");
            l_miscRecordData.setField(4,"VAL_4_upd");
            
            processRecord(l_miscRecordData);
        }
        catch (PpasServiceException e)
        {
            fail("MDU Creating subscriber:" + e.getMessage());
        }
        
        // Check Misc Data record has been updated
        l_retMiscRecordData = getMiscValuesAsBatchDataRecord(
                c_ppasContext.getMsisdnFormatDb().format(l_sub.getMsisdn()));
        
        // So that dataEquals() works ...
        l_miscRecordData.setOperationCode("I"); 
        System.out.println("***** ACTUAL   ****** " + l_retMiscRecordData.dumpRecord());
        System.out.println("***** EXPECTED ****** " + l_miscRecordData.dumpRecord());
        assertEquals("", l_retMiscRecordData.dataEquals(l_miscRecordData), true);
        
        super.endOfTest();
    }

    /**
     * Test a record of type 'DELETE'.
     */
    public void testRecordDelete()
    {
        super.c_writeUtText = true;
        super.beginOfTest("testRecordDelete");

        // Build Miscellaneous data record
        MiscBatchRecordData l_retMiscRecordData = null; 
        MiscBatchRecordData l_miscRecordData    = new MiscBatchRecordData(); 
        BasicAccountData    l_sub               = null;
        
        try
        {
            l_sub = c_dbService.installTestSubscriber(null);
            assertNotNull("installTestSubscriber failed", l_sub);
            
            //Insert data
            l_miscRecordData.setMsisdn(l_sub.getMsisdn());
            l_miscRecordData.setOperationCode("I");
            l_miscRecordData.setField(1,"VAL_1");
            l_miscRecordData.setField(2,"VAL_2");
            l_miscRecordData.setField(3,"VAL_3");

            processRecord(l_miscRecordData);

            l_miscRecordData = new MiscBatchRecordData();
            l_miscRecordData.setMsisdn(l_sub.getMsisdn());
            l_miscRecordData.setOperationCode("D");
            
            processRecord(l_miscRecordData);
        }
        catch (PpasServiceException e)
        {
            fail("MDU Creating subscriber:" + e.getMessage());
        }
        
        // Check Misc Data record has been updated
        l_retMiscRecordData = getMiscValuesAsBatchDataRecord(
                c_ppasContext.getMsisdnFormatDb().format(l_sub.getMsisdn()));

        // So that dataEquals() works ...
        l_miscRecordData.setOperationCode("I"); 
        System.out.println("***** ACTUAL   ****** " + l_retMiscRecordData.dumpRecord());
        System.out.println("***** EXPECTED ****** " + l_miscRecordData.dumpRecord());
        assertEquals("", l_retMiscRecordData.dataEquals(l_miscRecordData), true);
        
        super.endOfTest();
    }



    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /** Define test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(MiscBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        TestRunner.run(suite());
    }

} // End of public class MiscBatchProcessorUT

////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////}
