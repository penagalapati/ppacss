// //////////////////////////////////////////////////////////////////////////////
//          ASCS IPR ID : 9500
////////////////////////////////////////////////////////////////////////////////
//
//          FILE NAME   : SDPBalBatchReader.java
//          DATE        : 08-Jul-2004
//          AUTHOR      : Emmanuel-Pierre Hebe
//          REFERENCE   : PRD_ASCS_DEV_SS_083
//
//          COPYRIGHT   : Atos Origin 2004
//
//          DESCRIPTION : 
//
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
//DD/MM/YY | <name>         | <brief description of the     | <reference>
//          |               | changes>                      |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.dataclass.ScpiScpInfoData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.SDPBalBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to verify and extract data from the SDP balancing file.
 */

public class SDPBalBatchReader extends BatchLineFileReader
{
    //-------------------------------------------------------------------------
    // Variables
    //-------------------------------------------------------------------------

    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String   C_CLASS_NAME                                = "SDPBalBatchReader";

    /** Error code. Trailer not last record in file. Value is {@value}. */
    public static final String    C_ERROR_TRAILER_NOT_LAST_IN_FILE            = "01";

    /** Error code. Invalid record length. Value is {@value}. */
    public static final String    C_ERROR_INVALID_RECORD_LENGTH               = "02";

    /** Error code. Invalid end of record character. Value is {@value}. */
    public static final String    C_ERROR_INVALID_END_OF_RECORD_CHARACTER     = "03";

    /** Error code. Invalid end of record character. Value is {@value}. */
    public static final String    C_ERROR_EITHER_OLD_OR_NEW_SDP_IS_NOT_CONFIG = "04";

    /** Error code. Invalid numeric field. Value is {@value}. */
    public static final String    C_ERROR_INVALID_NUMERIC_FIELD               = "05";

    /** Error code. Record count in trailer is incorrect. Value is {@value}. */
    public static final String    C_ERROR_RECORD_COUNT_IN_TRAILER_INCORRECT   = "06";

    /** Error code. Invalid record type. Value is {@value}. */
    public static final String    C_ERROR_INVALID_RECORD_TYPE                 = "07";

    /** Error code for conferting MSISDN from SDP to ASCS format. Value is {@value}. */
    private static final String   C_ERROR_CODE_CONVERTING_MSISDN              = "08";

    /** Error code. Header missing or is not first in file. Value is {@value}. */
    public static final String    C_ERROR_HEADER_MISSING_OR_NOT_FIRST         = "11";

    /** Error code. Wrong number of fields in input record. Value is {@value}. */
    public static final String    C_ERROR_WRONG_NUMBER_OF_FIELDS              = "12";

    /** Error code. Trailer record missing. Value is {@value}. */
    public static final String    C_ERROR_TRAILER_RECORD_MISSING              = "13";

    /** Error code. Subscriber was not successfully processed by SDP. Value is {@value}. */
    public static final String    C_ERROR_NOT_PROCCESSED_IN_SDP               = "14";

    /** Sepcific SDP report file fields delimiter. Value is {@value}. */
    public static final String    C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER      = ",";

    /** One byte numeric. */
    private static final String   C_NUM                         = "(25[0-5]|2[0-4]\\d|[01]\\d\\d|\\d?\\d)";

    /** regular expression for an IP address. */
    private static final String   C_IP_ADDRESS_FORMAT                         = "^" + C_NUM + "\\." + C_NUM
                                                                                      + "\\." + C_NUM + "\\."
                                                                                      + C_NUM + "$";

    /** Result code. Value is {@value}. */
    private static final String   C_RESULT_CODE_SUCCESS                        = "00";

    /** Maximum length of hedear record. Value is {@value}. */
    private static final int      C_MAX_HEADER_RECORD_LENGTH                  = 36;

    /** Maximum length of detaild record. Value is {@value}. */
    private static final int      C_MAX_DETAILED_RECORD_LENGTH                = 20;

    /** Maximum length of trailer record. Value is {@value}. */
    private static final int      C_MAX_TRAILER_RECORD_LENGTH                 = 10;

    // Index in the batch record array

    /** Index for the field result code in the batchDataRecord array. Value is {@value}. */
    private static final int      C_RECORD_TYPE                               = 0;

    /** Index for the field old SDP IP address in the batchDataRecord array. Value is {@value}. */
    private static final int      C_OLD_SDP_IP_ADDRESS                        = 1;

    /** Index for the field new SDP IP address in the batchDataRecord array. Value is {@value}. */
    private static final int      C_NEW_SDP_IP_ADDRESS                        = 2;

    /** Index for the field result code in the batchDataRecord array. Value is {@value}. */
    private static final int      C_RESULT_CODE_HEADER                        = 3;

    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int      C_MSISDN                                    = 1;

    /** Index for the field result code in the batchDataRecord array. Value is {@value}. */
    private static final int      C_RESULT_CODE_RECORD                        = 2;

    /** Index for the field Account Group in the batchDataRecord array. Value is {@value} */
    private static final int      C_RECORD_COUNT                              = 1;

    /** The number of fields in the header. Value is {@value}. */
    private static final int      C_NUMBER_OF_FIELDS_HEADER                   = 4;

    /** The number of fields in the record line. Value is {@value}. */
    private static final int      C_NUMBER_OF_FIELDS_RECORD                   = 3;

    /** The number of fields in the trailer. Value is {@value}. */
    private static final int      C_NUMBER_OF_FIELDS_TRAILER                  = 2;

    /** New SDP Id. */
    private String                i_newSdpId                                  = null;

    /** Old SDP Id. */
    private String                i_oldSdpId                                  = null;

    /** Number of headers and detaild records. */
    private int                   i_nbOfLine                                  = 0;

    /** MSISDN. */
    private String                i_msisdn                                    = null;

    /** The BatchDataRecord to be sent into the Queue. */
    private SDPBalBatchRecordData i_sdpBalBatchRecordData                     = null;

    /** Used to check if the first record is a header record. */
    private boolean               i_firstRecord                               = true;

    /** Indicates that "missing trailer record" already has been reported.
     *  Otherwise it will be reported over and over again. */
    private boolean               i_missingTrailerRecordReported              = false;

    /** Used to check if a trailer record exist in the file abd if so in what line. */
    private int                   i_trailerRecordLineNumber                   = -1;

    /** To check if the error record is in the first line and if so is it a result code set to sucsses. */
    private boolean               i_erroneousHeaderRecord                     = false;

    /** Holds the trailer line. */
    private String                i_trailerLine                               = null;

    /** Trailer record has been processed indicator. */
    private boolean               i_trailerRecordProcessed                    = false;
    
    /** Trailer record is valid indicator. */
    private boolean               i_validTrailerRecord                        = true;
    
    /**Holding the trailer record�s record count. */
    private String                i_recordCount                               = null;

    /** The BatchDataRecord to be sent into the Queue. */
    private SDPBalBatchRecordData i_trailerRecordData                         = null;


    /**
     * The no-arg Constructor.
     * @param p_ppasContext The context
     * @param p_logger Logs
     * @param p_controller The controller
     * @param p_queue The queue
     * @param p_parameters The parameters
     * @param p_properties The properties
     */
    public SDPBalBatchReader(PpasContext p_ppasContext,
            Logger p_logger,
            BatchController p_controller,
            SizedQueue p_queue,
            Map p_parameters,
            PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTING);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            BatchConstants.C_CONSTRUCTED);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";

    /**
     * Check if filename is of the valid form.
     * @param p_filename The filename
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    public boolean isFileNameValid(String p_filename)
    {
        boolean l_validFileName = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10030,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_isFileNameValid);
        }

        if (p_filename != null)
        {
            if ((p_filename.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_DAT))
                    || (p_filename.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_SCH))
                    || (p_filename.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_IPG)))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_CONFIN_START,
                                    C_CLASS_NAME,
                                    10032,
                                    this,
                                    "FileName (" + p_filename + ") is valid");
                }
            }
            else
            {
                l_validFileName = false;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_CONFIN_START,
                                    C_CLASS_NAME,
                                    10034,
                                    this,
                                    "FileName (" + p_filename + ") is invalid");
                }
            }
            //          Check if recovery mode
            if (l_validFileName && !this.i_recoveryMode)
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                if (p_filename.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_SCH))
                {
                    renameInputFile(BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }
                else
                {
                    renameInputFile(BatchConstants.C_PATTERN_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }

                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10036,
                                    this,
                                    "After rename, inputFullPathName=" + super.i_fullPathName);
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_isFileNameValid);
        }

        return l_validFileName;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";

    /**
     * This method shall contain all logic necessary to create and populate the BatchDataRecord record.
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        String l_nextLine = null; // Current input line

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10060,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getRecord);
        }

        //      Get next line from input file.
        i_sdpBalBatchRecordData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10062,
                            this,
                            "l_nextLine [" + l_nextLine + "]");
        }

        //Don't read any more records if the header record is erroneous.
        if (!i_erroneousHeaderRecord)
        {
            if (super.readNextLine() != null)
            {
                i_sdpBalBatchRecordData = new SDPBalBatchRecordData();

                // Store input filename
                i_sdpBalBatchRecordData.setInputFilename(super.i_inputFileName);

                // Store line-number form the file
                i_sdpBalBatchRecordData.setRowNumber(super.i_lineNumber);

                // Store original data record
                i_sdpBalBatchRecordData.setInputLine(super.i_inputLine);

                // Extract data from line
                if (extractLineAndValidate())
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10064,
                                        this,
                                        "extractLineAndValidate() was successful");
                    }
                }
                else
                {
                    // Line is corrupt - invalid record
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10066,
                                        this,
                                        "extractLineAndValidate() failed!");
                    }

                    // Flag for corrupt line and set error code
                    i_sdpBalBatchRecordData.setCorruptLine(true);

                    if (i_sdpBalBatchRecordData.getErrorLine() == null)
                    {
                        // Other error than specific field errors
                        i_sdpBalBatchRecordData
                                .setErrorLine(C_ERROR_INVALID_RECORD_TYPE
                                        + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                        + i_sdpBalBatchRecordData.getInputLine());
                    }
                }

                i_sdpBalBatchRecordData.setNumberOfSuccessfullyProcessedRecords(
                    super.i_successfullyProcessed);
                i_sdpBalBatchRecordData.setNumberOfErrorRecords(super.i_notProcessed);
                
                if ( super.faultyRecord(i_lineNumber))
                {
                    i_sdpBalBatchRecordData.setFailureStatus(true);
                }

                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10068,
                                    this,
                                    "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
                }
            } // end if
            else
            {
                // End of file is reached.
                if (i_trailerRecordProcessed)
                {
                    // A trailer record has been processed.
                    // Check if it is the last record in the file.
                    if (i_trailerRecordLineNumber != super.i_lineNumber)
                    {
                        //Trailer record is not last in file.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10070,
                                            this,
                                            C_METHOD_getRecord +
                                            " ***ERROR: Trailer record is not the last record in the file.");
                        }
                        i_trailerRecordData.setErrorLine(C_ERROR_TRAILER_NOT_LAST_IN_FILE +
                                                         C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + 
                                                         i_trailerLine);
                    }
                    else
                    {
                        if (i_validTrailerRecord)
                        {
                            // Check the record counter value.
                            if (super.isRecovery())
                            {
                                i_nbOfLine += super.i_successfullyProcessed;
                            }
                            if (Integer.parseInt(i_recordCount) != i_nbOfLine)
                            {
                                // ERROR: The record counter value and the actual number of processed detailed
                                //        records (incl. the header record) differs.
                                if (PpasDebug.on)
                                {
                                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                    PpasDebug.C_APP_SERVICE,
                                                    PpasDebug.C_ST_TRACE,
                                                    C_CLASS_NAME,
                                                    10072,
                                                    this,
                                                    C_METHOD_getRecord +
                                                    " -- ***ERROR: The record counter value is incorrect: " +
                                                    i_recordCount + ", it should be = " + i_nbOfLine);
                                }
                                i_trailerRecordData.setErrorLine(C_ERROR_RECORD_COUNT_IN_TRAILER_INCORRECT +
                                                                 C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER +
                                                                 i_trailerLine);
                                i_trailerRecordData.setCorruptLine(true);
                            }
                        }
                    }
                }
                else
                {
                    // ERROR: Trailer record is missing (or it may have an incorrect record type).
                    if (!i_missingTrailerRecordReported)
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10074,
                                            this,
                                            C_METHOD_getRecord +
                                            " -- ***ERROR: The trailer record is missing.");
                        }

                        // Create a new extra record data with an empty input line in order to make it
                        // possible to  report the "trailer record missing" error in the report file.
                        i_sdpBalBatchRecordData = new SDPBalBatchRecordData();
                        i_sdpBalBatchRecordData.setErrorLine(C_ERROR_TRAILER_RECORD_MISSING +
                                                             C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER +
                                                             "");
                        i_sdpBalBatchRecordData.setCorruptLine(true);

                        // Set the row number to be a 'virtual' row number, i.e. the total number of rows in
                        // the file incr. by one (1).
//                      i_sdpBalBatchRecordData.setRowNumber(i_nbOfLine + super.i_successfullyProcessed + 1);
                        i_sdpBalBatchRecordData.setRowNumber(-1);

                        // Indicate that "trailer record missing" error is reported.
                        i_missingTrailerRecordReported = true;
                    }
                }
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10080,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_sdpBalBatchRecordData;

    } // end of method getRecord()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";

    /**
     * This method extracts information from the batch data record.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10090,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate);
        }

        // Valid record types.
        final String L_HEADER_RECORD_TYPE   = "0";
        final String L_DETAILED_RECORD_TYPE = "1";
        final String L_TRAILER_RECORD_TYPE  = "100";

        boolean l_validData     = true; //Assume it is valid record.
        String[] l_recordFields = super.i_inputLine.split(",");
        String l_recordType     = l_recordFields[C_RECORD_TYPE];

        if (i_firstRecord)
        {
            if (!l_recordType.equals(L_HEADER_RECORD_TYPE))
            {
                // ***ERROR: The header record is not the first record in the input file!
                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_HEADER_MISSING_OR_NOT_FIRST
                                               + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                               + super.i_inputLine);
                i_erroneousHeaderRecord = true;
                l_validData = false;
                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                return false;
            }
            i_firstRecord = false;
            i_nbOfLine++;
            return validateHeader(l_recordFields);
        }

        if (l_recordType.equals(L_HEADER_RECORD_TYPE))
        {
            // ERROR: Only one header record is allowed!
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10092,
                                this,
                                C_METHOD_extractLineAndValidate + 
                                " -- ***ERROR: Multiple header records exists: '" + super.i_inputLine +
                                "', only one is allowed.");
            }

            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_HEADER_MISSING_OR_NOT_FIRST
                                           + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                           + super.i_inputLine);
            i_erroneousHeaderRecord = true;
            l_validData = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
        }
        else if (l_recordType.equals(L_DETAILED_RECORD_TYPE))
        {
            i_nbOfLine++;
            i_sdpBalBatchRecordData.setIsRecord(true);
            l_validData = validateRecordLine(super.i_inputLine);
        }
        else if (l_recordType.equals(L_TRAILER_RECORD_TYPE))
        {
            if (i_trailerRecordLineNumber > 0)
            {
                // ERROR: Only one trailer record is allowed.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10094,
                                    this,
                                    C_METHOD_extractLineAndValidate + 
                                    " -- ***ERROR: Multiple trailer records exists: '" + super.i_inputLine +
                                    "', only one is allowed.");
                }

                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_TRAILER_NOT_LAST_IN_FILE
                                               + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                               + super.i_inputLine);
                l_validData = false;
            }
            else
            {
                i_trailerRecordLineNumber = super.i_lineNumber;
                i_trailerLine = super.i_inputLine;
                i_trailerRecordData = i_sdpBalBatchRecordData;
                l_validData = validateTrailer(l_recordFields);
                i_validTrailerRecord = l_validData;
                i_trailerRecordProcessed = true;
            }
        }
        else
        {
            // The record type is invalid.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10096,
                                this,
                                "Line: " + i_nbOfLine + " (" + super.i_inputLine + "), "
                                        + "the record type is invalid: " + l_recordType);
            }

            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_TYPE +
                                                 C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + super.i_inputLine);
            i_sdpBalBatchRecordData.setCorruptLine(true);
            l_validData = false;
            i_nbOfLine++; // Count the record even though the record type is invalid.
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate);
        }

        return l_validData;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateHeader = "validateHeader";

    /**
     * Validates the header of the batch data record.
     * @param p_headerFields  The header line fields.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */

    private boolean validateHeader(String[] p_headerFields)
    {
        boolean             l_validData      = true; // return code. Assume header record is valid.
        BusinessConfigCache l_cache          = null;
        String              l_oldIpAddress   = null;
        String              l_newIpAddress   = null;
        String              l_resultCode     = null;
        ScpiScpInfoData     l_infoDataOldSdp = null;
        ScpiScpInfoData     l_infoDataNewSdp = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10110,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateHeader);
        }

        // Check the number of fields.
        if (p_headerFields.length != C_NUMBER_OF_FIELDS_HEADER)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10112,
                                this,
                                C_METHOD_validateHeader +
                                " -- ***ERROR: Invalid number of header fields = " + p_headerFields.length +
                                ", it must be exact " + C_NUMBER_OF_FIELDS_HEADER + " fields.");
            }

            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_WRONG_NUMBER_OF_FIELDS
                                           + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                           + super.i_inputLine);
            i_erroneousHeaderRecord = true;
            l_validData = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
        }
        else
        {
            // Check the length of the header line.
            if (super.i_inputLine.length() > C_MAX_HEADER_RECORD_LENGTH)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10114,
                                    this,
                                    C_METHOD_validateHeader + " -- ***ERROR: Header line is too long: "
                                            + super.i_inputLine.length() + ",  it must not exceed "
                                            + C_MAX_HEADER_RECORD_LENGTH + " characters.");
                }

                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_LENGTH
                                                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                                     + super.i_inputLine);
                i_erroneousHeaderRecord = true;
                l_validData = false;
                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            }
            else
            {
                // Check the result code.
                l_resultCode = p_headerFields[C_RESULT_CODE_HEADER];
                if (!l_resultCode.equals(C_RESULT_CODE_SUCCESS))
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10116,
                                        this,
                                        C_METHOD_validateHeader
                                                + " -- ***ERROR: Invalid header result code = '"
                                                + l_resultCode + "', it must be '" + C_RESULT_CODE_SUCCESS
                                                + "'.");
                    }

                    i_sdpBalBatchRecordData.setErrorLine(C_ERROR_NOT_PROCCESSED_IN_SDP
                            + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + super.i_inputLine);
                    i_erroneousHeaderRecord = true;
                    l_validData = false;
                    sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                }
                else
                {
                    // Check 'old' and 'new' SDP IP-addresses.
                    l_oldIpAddress = p_headerFields[C_OLD_SDP_IP_ADDRESS];
                    l_newIpAddress = p_headerFields[C_NEW_SDP_IP_ADDRESS];

                    //Check the format.
                    if (!l_oldIpAddress.matches(C_IP_ADDRESS_FORMAT)
                            || !l_newIpAddress.matches(C_IP_ADDRESS_FORMAT))
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10118,
                                            this,
                                            C_METHOD_validateHeader +
                                            " -- ***ERROR: Either the 'old' or 'new' (or both) SDP IP "
                                                   + "address has an invalid format: old SDP IP address = '"
                                                   + l_oldIpAddress + "',  new SDP IP address = '"
                                                   + l_newIpAddress + "'.");
                        }

                        i_sdpBalBatchRecordData.setErrorLine(C_ERROR_EITHER_OLD_OR_NEW_SDP_IS_NOT_CONFIG
                                + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + super.i_inputLine);
                        i_erroneousHeaderRecord = true;
                        l_validData = false;
                        sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                    }
                    else
                    {
                        // Ok, the header fields are valid, let's validate the old and new SDP IP-address.
                        l_cache = (BusinessConfigCache)super.i_context.getAttribute("BusinessConfigCache");
                        l_infoDataOldSdp = l_cache.getScpiScpInfoCache().getAvailable()
                                .getRecordFromIpAddress(l_oldIpAddress);

                        l_infoDataNewSdp = l_cache.getScpiScpInfoCache().getAvailable()
                                .getRecordFromIpAddress(l_newIpAddress);

                        if (l_infoDataOldSdp == null || l_infoDataNewSdp == null)
                        {
                            // The old or the new (or both) SDP IP-address is invalid.
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                PpasDebug.C_APP_SERVICE,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME,
                                                10120,
                                                this,
                                                C_METHOD_validateHeader +
                                                " -- ***ERROR: Either the 'old' or 'new' (or both) SDP IP "
                                                + "address is invalid: old SDP IP address = '"
                                                + l_oldIpAddress + "',  new SDP IP address = '"
                                                + l_newIpAddress + "'.");
                            }

                            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_EITHER_OLD_OR_NEW_SDP_IS_NOT_CONFIG
                                    + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + super.i_inputLine);

                            i_erroneousHeaderRecord = true;
                            l_validData = false;
                            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                        }
                        else
                        {
                            i_oldSdpId = l_infoDataOldSdp.getScpId();
                            i_newSdpId = l_infoDataNewSdp.getScpId();

                            if (i_oldSdpId == null || i_newSdpId == null)
                            {
                                // No corresponding SDP ID found for either the old or new (or both) SDP
                                // IP-Addr.
                                if (PpasDebug.on)
                                {
                                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                    PpasDebug.C_APP_SERVICE,
                                                    PpasDebug.C_ST_TRACE,
                                                    C_CLASS_NAME,
                                                    10122,
                                                    this,
                                                    C_METHOD_validateHeader +
                                                    " -- ***ERROR: No corresponding ID found for either the "
                                                    + "'old' or 'new' (or both) SDP IP address: "
                                                    + "old SDP IP address = '" + l_oldIpAddress
                                                    + "', new SDP IP address = '" + l_newIpAddress
                                                    + "'.");
                                }

                                i_sdpBalBatchRecordData
                                        .setErrorLine(C_ERROR_EITHER_OLD_OR_NEW_SDP_IS_NOT_CONFIG
                                                + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + super.i_inputLine);

                                i_erroneousHeaderRecord = true;
                                l_validData = false;
                                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                            }
                        }
                    }
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateHeader);
        }
        return l_validData;
    }


    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateRecord = "validateRecord";

    /**
     * Validates one record of the batch data record.
     * @param p_recordLine Record: The format is: Record Type (1), MSISDN, Result code
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */

    private boolean validateRecordLine(String p_recordLine)
    {
        MsisdnFormat l_msisdnFormat = null;
        boolean l_validData = true; // return code. Assume it always true.
        String[] l_recordFields = new String[C_NUMBER_OF_FIELDS_RECORD];
        String l_resultCode = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10140,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateRecord);
        }

        // Initialise record validatation
        l_recordFields = new String[C_NUMBER_OF_FIELDS_RECORD];
        l_recordFields = p_recordLine.split(",");

        if (i_inputLine.length() > C_MAX_DETAILED_RECORD_LENGTH)
        {
            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_LENGTH +
                                                 C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + 
                                                 super.i_inputLine);

            l_validData = false;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10142,
                                this,
                                "Row: ]" + i_inputLine + "[ wrong record length " + " l_lineNumber= "
                                        + i_lineNumber + " " + i_sdpBalBatchRecordData.getErrorLine() + " length = "
                                        + i_inputLine.length());
            }

        }
        else if (l_recordFields.length != C_NUMBER_OF_FIELDS_RECORD)
        {
            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_WRONG_NUMBER_OF_FIELDS
                                           + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER 
                                           + super.i_inputLine);

            l_validData = false;
        }
        else
        {
            // Get the record fields
            i_msisdn        = l_recordFields[C_MSISDN];
            l_resultCode    = l_recordFields[C_RESULT_CODE_RECORD];

            if (i_msisdn.matches(BatchConstants.C_PATTERN_NUMERIC) &&
                l_resultCode.matches(BatchConstants.C_PATTERN_NUMERIC))

            {
                //Everything went fine: the record is valid
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10144,
                                    this,
                                    "Record: the record #" + i_nbOfLine + " is valid.");
                }
            }
            else
            {
                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_NUMERIC_FIELD +
                                                     C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER + 
                                                     super.i_inputLine);
                // The header is invalid
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10146,
                                    this,
                                    "Record: the record is invalid:" + "MSISDN:" + i_msisdn + "\n"
                                            + "Result Code:" + l_recordFields[C_RESULT_CODE_RECORD] + "\n");
                }

                // Flag for corrupt line
                l_validData = false;
            }
        }

        if (l_validData)
        {
            l_resultCode = l_recordFields[C_RESULT_CODE_RECORD];

            //If the result code is not 00.
            if (!l_resultCode.equals(C_RESULT_CODE_SUCCESS))
            {
                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_NOT_PROCCESSED_IN_SDP
                                               + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                               + super.i_inputLine);

                l_validData = false;
            }
        }

        if (l_validData)
        {
            l_msisdnFormat = this.i_context.getMsisdnFormatInput();

            try
            {
                i_sdpBalBatchRecordData.setMsisdn(l_msisdnFormat.parse(i_msisdn));
                i_sdpBalBatchRecordData.setOldSDP(i_oldSdpId);
                i_sdpBalBatchRecordData.setNewSDP(i_newSdpId);

            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10148,
                                    this,
                                    "Record: the msisdn is invalid:" + "MSISDN:" + i_msisdn + "\n"
                                            + "Result Code:" + l_recordFields[C_RESULT_CODE_RECORD] + "\n");
                }

                //Was not able to convert the msisdn from the SDP to ASCS format.
                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_CODE_CONVERTING_MSISDN
                                               + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                               + super.i_inputLine);

                // The msisdn is invalid, the record can not be validated
                l_validData = false;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10150,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateRecord);
        }

        return l_validData;

    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateTrailer = "validateTrailer";

    /**
     * Validates the trailer.
     * @param p_trailerFields  the trailer line fields.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */

    private boolean validateTrailer(String[] p_trailerFields)
    {
        boolean l_validData = true; // return code. Assume it always true.

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10160,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateTrailer);
        }

        // Check record length.
        if (super.i_inputLine.length() > C_MAX_TRAILER_RECORD_LENGTH)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10162,
                                this,
                                C_METHOD_validateHeader +
                                " -- ***ERROR: Trailer record too long = " + super.i_inputLine.length() +
                                ",  max length is " + C_MAX_TRAILER_RECORD_LENGTH + " characters.");
            }

            i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_LENGTH
                                           + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                           + super.i_inputLine);

            l_validData = false;
        }
        else
        {
            // Check the number of fields.
            if (p_trailerFields.length != C_NUMBER_OF_FIELDS_TRAILER)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10164,
                                    this,
                                    C_METHOD_validateHeader +
                                    " -- ***ERROR: Invalid number of trailer fields = " + p_trailerFields.length +
                                    ", it must be exact " + C_NUMBER_OF_FIELDS_TRAILER + " fields.");
                }

                i_sdpBalBatchRecordData.setErrorLine(C_ERROR_WRONG_NUMBER_OF_FIELDS
                                               + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                               + super.i_inputLine);
                l_validData = false;
            }
            else
            {
                // Check the record count format.
                i_recordCount = p_trailerFields[C_RECORD_COUNT];
                if (!i_recordCount.matches(BatchConstants.C_PATTERN_NUMERIC))
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10166,
                                        this,
                                        C_METHOD_validateHeader +
                                        " -- ***ERROR: Record count in trailer record is non-numerical: '" +
                                        i_recordCount + "'.");
                    }

                    i_sdpBalBatchRecordData.setErrorLine(C_ERROR_INVALID_NUMERIC_FIELD
                                                   + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                                                   + super.i_inputLine);
                    l_validData = false;
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10170,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateTrailer + ", valid trailer record: " +
                            l_validData);
        }
        return l_validData;
    }
} // End of class SDPBalBatchReader
