////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchWriterUT.java 
//      DATE            :       Jun 24, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Unit test for SCChangeBatchWriter.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.SCChangeBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.SCChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for SCChangeBatchWriter. */
public class SCChangeBatchWriterUT extends BatchTestCaseTT
{
    //-----------------------------------------------------
     //  Class level constant
     //  ------------------------------------------------------ 
       
    /** Reference to a Status Change Batch Writer object. */
    private static SCChangeBatchWriter     c_scChangeBatchWriter;

    /** Reference to a Status Change Batch controller object. */
    private static SCChangeBatchController c_scChangeBatchController;

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService        i_controlService;

    /** A file name vid extension .DAT. */
    private static final String            C_FILE_NAME_DAT = "BATCH_CHG_SERV_CLASS_20040615_00003.DAT";
    
    /** A file name vid extension .RPT. */
    private static final String            C_FILE_NAME_RPT = "BATCH_CHG_SERV_CLASS_20040615_00003.TMP";
    
    /** A file name vid extension .RCV. */
    private static final String            C_FILE_NAME_RCV = "BATCH_CHG_SERV_CLASS_20040615_00003.RCV";    

    /** The js job id. */
    private static final String            C_JS_JOB_ID           = "10001";

    /** The file date. */
    private static final String            C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final String            C_SEQ_NO              = "0003";

    /** The sub job counter. */
    private static final String            C_SUB_JOB_COUNT       = "-1";
    
    /**
     * Constructs a SCChangeBatchWriterUT object. 
     * @param p_name      Holding parameters used of the writer.  
     */
    public SCChangeBatchWriterUT(String p_name)
    {
        super(p_name, "batch_scc");
    }
    
    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table
     * <code>baco_batch_control</code> by calling how calls <code>updateControlInfo(...)</code>. Gets the
     * updated row from the table and check if the row <code>baco_failur</code> has been updated. Set the
     * value for error line in the object <code>SubInstBatchRecordData</code> to null and updates the table
     * <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */

    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        createContext();
        SCChangeBatchRecordData l_dataRecord = new SCChangeBatchRecordData();

        String          l_actualText    = null;
        BatchJobDataSet l_batchjobSet   = null;
        BatchJobData    l_batchData1    = new BatchJobData();
        BatchJobData    l_batchData     = null;
        String          l_expectedKey   = null;
        String          l_actualKey     = null;
        String          l_errorLine     = "2 ERR";
        String          l_recoveryLine  = "1 REC";
        String          l_executionDateTime = null;
        String          l_jsJobId           = null;
        Vector          l_vec               = null;
        
        try
        {           
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_scChangeBatchController.getKeyedControlRecord();
            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            
            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE);
            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);

            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");          
            l_batchData1.setOpId(c_scChangeBatchController.getSubmitterOpid());

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                c_scChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "0";

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
            
            //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RPT,
                                                     BatchConstants.C_EXTENSION_TMP_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            //Get the recovery text from the recovery logfile.
//            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RCV,
//                                                     BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                                     BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//
//            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                        ("No file found".equals(l_actualText)));
//            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);


            //Test when there is no error line set.
            try
            {
                l_dataRecord.setErrorLine(null);
                c_scChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 1.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "1";

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);

            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(SCChangeBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    
    /**
     * Creates context needed for the unit test.
     */
    private void createContext()
    {
        SizedQueue l_outQueue = null;
        SubInstBatchRecordData l_batchRecordData = new SubInstBatchRecordData();
        String          l_dir       = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String          l_fileReportName = l_dir + "/" + C_FILE_NAME_RPT; 
        String          l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String          l_fileRecoveryName = l_dir2 + "/" + C_FILE_NAME_RCV;
        
        //The update frequency needs to be 1 for the test to work.
        super.c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");
        
        try
        {
            l_outQueue =
                new SizedQueue(
                    "BatchWriter",
                    1,
                    null);
                    
            l_outQueue.addFirst(l_batchRecordData);     
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        Hashtable l_params = new Hashtable();
        l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_FILE_NAME_DAT);

        //Make sure that there are no files with the same name.
        super.deleteFile(l_fileReportName);
        super.deleteFile(l_fileRecoveryName); 
        
        try
        {
            c_scChangeBatchController =
                new SCChangeBatchController(
                    c_ppasContext,
                    BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE,
                    C_JS_JOB_ID,
                    "SCChangeBatchWriterUT",
                    l_params);
           
            c_scChangeBatchWriter =
                new SCChangeBatchWriter(
                    super.c_ppasContext,
                    super.c_logger,
                    c_scChangeBatchController,
                    l_params,
                    l_outQueue,
                    super.c_properties);    
                    
        }
        catch (RemoteException e)
        {
            super.failedTestException(e);
        }
        catch (PpasConfigException e)
        {
            super.failedTestException(e);
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

    }
}
