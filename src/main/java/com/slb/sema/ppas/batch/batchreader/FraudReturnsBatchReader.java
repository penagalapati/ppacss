////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME       :       FraudBatchReader.java
//        DATE            :       27-April-2006
//        AUTHOR          :       Marianne Toernqvist
//        REFERENCE       :       PpaLon#2203/8587, PRD_ASCS00_GEN_CA_080
//
//        COPYRIGHT       :       WM-data 2006
//
//        DESCRIPTION     :       Vodafone Ireland Fraud returns.
//                                This batch has a special behaviour - the Map parameter
//                                for input file - keeps a directory path, all files at this
//                                directory are the input files for this batch.
//                                Therefore this filedriven batch doesn't inherits from the 
//                                BatchLineFileReader.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
// 01/06/06 |M. Toernqvist  | FraudBatchRecordData constr.  | PpaLon#2349/8986
//          |               | has been changed              | 
//----------+---------------+-------------------------------+--------------------
// 31/05/06 |M. Toernqvist  | Add check if "file-handle" is | PpaLon#2344/8985
//          |               | != null                       | 
//----------+---------------+-------------------------------+--------------------
// 15/05/06 |M. Toernqvist  | Add recovery functionallity   | PpaLon#2288/8805
//----------+---------------+-------------------------------+--------------------
// 03/11/06 |M. Toernqvist  | Fix bug in recovery, when it  | PpaLon#2472/10362
//          |               | is run several times.         |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.FraudReturnsBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.FraudBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * @author wmmatrq
 *
 */
public class FraudReturnsBatchReader extends BatchReader //BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                     = "FraudReturnsBatchReader";
    
    public  static final String C_END_OF_FILE                    = "EOF";
    
    private static final String C_ERROR_INVALID_RECORD_FORMAT    = "<Invalid record format>";
    
    private static final String C_ERROR_RECORD_TO_LONG           = "<Record to long>";
    
    private static final String C_ERROR_WRONG_NUMBER_OF_FIELDS   = "<Wrong number of fields>";
    
    private static final String C_ERROR_INVALID_VOUCHER_SERIAL_NUMBER = "<Invalid voucher serial number>";
    
    private static final String C_ERROR_INVALID_TERMINAL_ID      = "<Invalid terminal id>";
    
    private static final String C_ERROR_INVALID_REASON_CODE      = "<Invalid reason code>";
    
    public  static final String C_DELIMITER_ERROR_LOG            = ", inputLine: ";
        
    /** Log message - Cannot open indata file. Value is {@value}. */
    private static final String C_INDATA_FILE_NOT_FOUND          = "Indata file not found - cannot open ";

    /** Log message - no indata files found. Value is {@value}. */
    private static final String C_NO_INDATA_FILES_FOUND          = "No indata files found in directory: ";
    
    /** Log message - illegal filename for input file. Value is {@value}. */
    private static final String C_NOT_A_VALID_FILENAME           = "Not a valid filename ";
    
    /** Exception Detail information showing it is a duplicate record. */
    private static final String C_DUPLICATE                      = "Duplicate";

    
    private static final String C_RECOVERY_FILENAME_NOT_FOUND    = "Recovery filename not found";
    private static final String C_RECOVERY_FILE_IO_EXCEPTION     = "Recovery IO Exception";
    private static final String C_RECOVERY_FILE_CANNOT_BE_CLOSED = "Recovery file cannot be closed";

    private static final int    C_AGENT_INDEX                    = 0;
    private static final int    C_NUMBER_OF_RECORDS_INDEX        = 2;
    private static final int    C_FILE_EXTENSION_INDEX           = 3;
    
    private static final String C_EXTENSION_INDATA_FILE          = "txt";

    private static boolean      i_recovery                       = false;


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
   
    /** Input directory. */
    private String               i_inputDirectory          = null;
    
    /** Input filenames to be processed. */    
    private String[]             i_inputFileNames          = null;
    
    /** The BatchDataRecord to be sent into the Queue. */
    private FraudBatchRecordData i_batchDataRecord         = null;
    
    /** Full path for the input file. */
    private String               i_fullInputFileName       = null;
    
    /** Agent id, from the input file name. */
    private String               i_agent                   = null;
    
    /** Number of expected Number of records in input file, from file name. */
    private int                  i_expectedNumberOfRecords = 0;
    
    /** Flag used by getRecord() to know if it is an ongoing reading, */
    private boolean              i_fileIsOpen              = false;

    /** Instance of the inner helper class. */
    private FraudFileReader      i_fraudFileReader         = new FraudFileReader(null);
    
    /** File counter, number of processed input files. */
    private int                  i_fileCounter             = 0;
    
    /** Maximum length of the Voucher Serial Number, read from property file. */
    private int                  i_maxSerialNumberLength;
    
    /** Help variable for recovery information. */
    private Hashtable            i_recoveryInformation     = null;
    
    private boolean              i_recoveryModeRequired    = false;

    
    //-------------------------------------------------------------------------
    // Contructor
    //-------------------------------------------------------------------------
    /**
     * Constructor for FraudBatchReader.
     * The FraudBatchController creates this object.
     * The directory with containing input filename is extracted from the p_parameters.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public FraudReturnsBatchReader(PpasContext     p_ppasContext,
                                   Logger          p_logger,
                                   BatchController p_controller,
                                   SizedQueue      p_queue,
                                   Map             p_parameters,
                                   PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 10300, this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        init();
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME, 10310, this,
                             BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    }


    
    //-------------------------------------------------------------------------
    // Private methods
    //-------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * This method is reponsible for all necesary initialisation related to file reading.
     * This batch has special behaviour, it recieves a directory path instead of one single
     * filename, and checks this directory for input files.
     * When this method returns, the instance member i_inputFIleHandle is initialised 
     * can be used.
     * 
     * In recovery mode - all generated reportfiles are deleted and the batch is run from the beginning.
     * 
     * If the process is in recovery mode the recovery file created in the last run will be
     * located and opened. Its content will be read into a hash-table, and thereafter the file
     * is closed.
     * The main reason for closing the file at this stage is that the writer will re-use that file.
     */
    protected void init()
    {
        File l_inputDirectory  = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10520, this,
                             BatchConstants.C_ENTERING + C_METHOD_init);
        }

        // Get input input file names.
        // The Map parameter for "input directory name" keeps the name of the input directory
        i_inputDirectory = (String)i_parameters.get(BatchConstants.C_KEY_INPUT_DIRECTORY_NAME);
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10550, this,
                             C_METHOD_init + " inputDirectory=" + i_inputDirectory);
        }

        l_inputDirectory = new File(i_inputDirectory);
        i_inputFileNames = l_inputDirectory.list();
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10550, this,
                             C_METHOD_init + " number of inputFilenames" + i_inputFileNames.length);
        }
        
        if ( i_inputFileNames.length == 0 )
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_NO_INDATA_FILES_FOUND + i_inputDirectory,
                                 LoggableInterface.C_SEVERITY_ERROR) );
        }

        if (PpasDebug.on)
        {
            for ( int i = 0; i_inputFileNames != null && i < i_inputFileNames.length; i++) 
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10550, this,
                                 C_METHOD_init + "file[" + i + "] : " + i_inputFileNames[i]);
            }
        }
        
        try
        {
            i_maxSerialNumberLength = i_properties.getIntProperty(
                                            FraudReturnsBatchWriter.C_VFR_PROPERTIES_SERIAL_NUMBER_LENGTH,
                                            FraudReturnsBatchWriter.C_DEFAULT_SERIAL_NUMBER_LENGTH);
            if (i_maxSerialNumberLength < FraudReturnsBatchWriter.C_DEFAULT_SERIAL_NUMBER_LENGTH)
            {
                i_maxSerialNumberLength = FraudReturnsBatchWriter.C_DEFAULT_SERIAL_NUMBER_LENGTH;
            }
        }
        catch ( Exception e)
        {
            i_maxSerialNumberLength = FraudReturnsBatchWriter.C_DEFAULT_SERIAL_NUMBER_LENGTH;
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10350, this,
                                 " Exception caught : "+e.getMessage());
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10550, this,
                             BatchConstants.C_LEAVING + C_METHOD_init);
        }

        return;
        
    } // end of method init()

    
    

   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the FraudBatchDataRecord record.
     * 
     * Scan the files in the input directory one by one.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10340, this,
                             BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }
        
        // Check if no file is opened - then if files are left open a new one
        if ( !isFileOpen() )
        {
            // No file open - try to open one. If none left - finish by sending NULL
            if ( !prepareForOpeningNextInputFile())
            {
                return null;
            }
        }
 
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10343, this,
                             "### filename validation OK, i_fraudFileReader =" +i_fraudFileReader);
        }

        
        // One input file is opened - start read record by record
        // Get next line from input file
        i_batchDataRecord = null;

        if ( i_fraudFileReader.readNextLine() != null )
        {
            // Flag in record if this file is running in recovery mode
            String l_tmpRecoveryStatus = getRecoveryStatus();
            i_batchDataRecord.setRecoveryStatus(l_tmpRecoveryStatus);
            String[] l_tmpRecoveryInformation = l_tmpRecoveryStatus.split(" ");
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10343, this,
                                 "MTR number of recovery info, status "+l_tmpRecoveryStatus+
                                 " length= :"+ l_tmpRecoveryInformation.length);
            }
            
            if ( l_tmpRecoveryInformation.length >= 2 )
            {
                i_batchDataRecord.setRecordType(Integer.parseInt(l_tmpRecoveryInformation[1]));
            }
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10343, this,
                                 "recovery info: "+i_batchDataRecord.dumpRecord() );
            }

            // Extract data from line
            if ( i_fraudFileReader.extractLineAndValidate() )
            {
                // Record information OK - and has been added to the BatchDataRecord in
                // the method extractLineAndValidation.
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10343, this,
                                     "extractLineAndValid failed!" );
                }
            }
                        
        } // end if
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10350, this,
                                 "END OF FILE REACHED... take next file");
            }
            tidyUp();
            
        }
        
        
        
        if ( i_batchDataRecord != null )
        {

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10350, this,
                                 "DumpRecord: " + i_batchDataRecord.dumpRecord());
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10350, this,
                                 "i_batchDataRecord is NULL!!!!!");
            }            
        }       

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10350, this,
                             BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;

    } // end of method getRecord()



    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";             
    /**
     * Check if filename is of the valid form: <DEALER SPECIFIC ID>_DDMMYYYY_<NUMBER OF RECORDS>.txt
     * 
     * @param p_fileName Filename to test.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {
        /** Expression for checking if a string has the format 'ddmmyyyy'.
         *  Note - it does not check that 31-february should be invalid.*/
        final String  L_PATTERN_DATE = "(([0][1-9])|([1-2][0-9])|([3][0-1]))(([0][1-9])|([1][0-2]))[0-9]{4}";

        /** Expression for checking filename pattern: <DEALER SPECIFIC ID>_DDMMYYYY_<NUMBER OF RECORDS>.txt.*/
        final String L_PATTERN_FILENAME = BatchConstants.C_PATTERN_ALPHA_NUMERIC + "_" + L_PATTERN_DATE +
                                          "[_][0-9]*[.]((txt)|(IPG))";



        boolean  l_validFileName      = true;  // Help variable - return value. Assume filename is OK.
        String[] l_fileNameComponents = null;  // After split on "_" and "."
        String   l_fileToDelete       = null;
        
 
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10400, this,
                             BatchConstants.C_ENTERING + C_METHOD_isFileNameValid + " fileName is : " + p_fileName );
        }

        // Create an empty recovery cache - if it should be recovery mode
        i_recoveryInformation = new Hashtable();

        if ( p_fileName != null ) 
        {
            
            if ( p_fileName.matches(L_PATTERN_FILENAME ) )
            {
            	// Check if a recovery file exist
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                     "## Filename OK : "+p_fileName+" search for IPG file");
                }                        

                i_recoveryModeRequired = recoveryModeRequired( p_fileName );
                if ( i_recoveryModeRequired )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                         PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                         "***** must be started in recovymode *****");
                    }                                       	
                }
                else
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                         PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                         "***** not required to be started in recovery mode *****");
                    }                                       	
                }
                
                l_fileNameComponents      = p_fileName.split(
                                                BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER );
                i_agent                   = l_fileNameComponents[C_AGENT_INDEX];
                i_expectedNumberOfRecords = Integer.parseInt(l_fileNameComponents[C_NUMBER_OF_RECORDS_INDEX]);
                if ( l_fileNameComponents[C_FILE_EXTENSION_INDEX].equals(BatchConstants.C_EXTENSION_RECOVERY_FILE))
                {
                    i_recovery = true;
                    if (initRecoveryInformation(p_fileName))
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                             "##OLD RECOVERY FILE was found## - information has been cached");
                        }                        
                    }
                    
                    // Delete old DUMMY, USED and AVAILABLE reports
                    String l_reportFileDirectory = FraudReturnsBatchWriter.getReportDirectory(i_properties);
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                         C_CLASS_NAME, 10400, this,
                                         "Report directory : " + l_reportFileDirectory);
                    }
                   
                    String l_reportFile = l_reportFileDirectory + "/" + p_fileName;
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                         C_CLASS_NAME, 10400, this,
                                         "Report file : " + l_reportFile);
                    }

                    
                    l_fileToDelete = l_reportFile.replaceAll(
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                         FraudReturnsBatchWriter.C_DUMMY_VOUCHER_REPORT_SUFFIX);
                    deleteFile( l_fileToDelete );
                    l_fileToDelete = l_reportFile.replaceAll(
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                         FraudReturnsBatchWriter.C_USED_VOUCHER_REPORT_SUFFIX);
                    deleteFile( l_fileToDelete );
                    l_fileToDelete = l_reportFile.replaceAll(
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                         FraudReturnsBatchWriter.C_AVAILABLE_VOUCHER_REPORT_SUFFIX);
                    deleteFile( l_fileToDelete );
 
                }
                else
                {
                	// filename not IPG - can it be started with txt-file
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                         C_CLASS_NAME, 10400, this,
                                         "INDATA file is not an IPG - can it be started ??? "+i_recoveryModeRequired);
                    }
                    if ( i_recoveryModeRequired )
                    {
                    	l_validFileName = false;
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                             C_CLASS_NAME, 10400, this,
                                             "INDATA file is not an IPG - it should have been because there is an existing RCV file");
                        }

                    }
                }
            }                        
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
            
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10400, this,
                                 "*** Before rename if it was a valid filename, validFileName=" +
                                 l_validFileName);
            }
       
            
            // Rename if it was a valid filename
            if ( l_validFileName )
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                renameFile( C_EXTENSION_INDATA_FILE,
                            BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10410, this,
                                     "After call to renameFile....." );
                }               
            }
            
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10410, this,
                             BatchConstants.C_LEAVING + C_METHOD_isFileNameValid + 
                             " validation : " + l_validFileName);
        }

        return l_validFileName;
    } // End of isFileNameValid()

    
    
    /**
     * Tidy up for the next input file. 
     * Create an EOF record to notify Processor and Writer that a new file will come.
     */
    protected void tidyUp()
    {
        i_fraudFileReader.tidyUp();
        i_batchDataRecord = new FraudBatchRecordData(0);
        i_batchDataRecord.setInputFilename(C_END_OF_FILE);
        i_fileCounter++;  //try next file
        
    }
 

    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_deleteFile = "deleteFile";
    /**
     * Deletes the defined file.
     * @param p_fullFileName Full path file name.
     */
    private void deleteFile( String p_fullFileName )
    {
       if (PpasDebug.on)
       {
           PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                           C_CLASS_NAME, 10020,  this,
                           BatchConstants.C_ENTERING + C_METHOD_deleteFile + " file: "+p_fullFileName);
       }
       
       File l_tmpFile = new File( p_fullFileName );
       if ( l_tmpFile.delete() )
       {
           if (PpasDebug.on)
           {
               PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                               C_CLASS_NAME, 10020,  this,
                               "Successfully deleted file: "+p_fullFileName);
           }           
       }
       else
       {
           if (PpasDebug.on)
           {
               PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                               C_CLASS_NAME, 10020,  this,
                               "#### COULD NOT DELETE file: " + p_fullFileName);
           }          
       }
      
       if (PpasDebug.on)
       {
           PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                           C_CLASS_NAME, 10020,  this,
                           BatchConstants.C_LEAVING + C_METHOD_deleteFile);
       }

       return;
    } // End of deleteFile
 
   
   
   
   
    /**
     * Get method for the created BatchDataRecord.
     * @return BatchDataRecord
     */
    public BatchRecordData getBatchDataRecord()
    {
        return this.i_batchDataRecord;
    }


    
    /** 
     * Check if a file has been opened.
     * @return TRUE if a file is open.
     */
    private boolean isFileOpen()
    {
        return i_fileIsOpen;
    }
    
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_prepareForOpeningNextInputFile = "prepareForOpeningNextInputFile";            
    /**
     * Searches for next valid input file.
     * Checks if filename is valid. Instantiates a new filereader for this file.
     * @return TRUE if a new input file has been found.
     */
    private boolean prepareForOpeningNextInputFile()
    {
        StringBuffer     l_tmpFullInputFileName = null;
        LineNumberReader l_inputFileHandle      = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10660, this,
                             BatchConstants.C_ENTERING + C_METHOD_prepareForOpeningNextInputFile);
        }
        
        // Dont know yet if it is a recovery run
        i_recovery = false;
        
        while ( !i_fileIsOpen            &&
                i_inputFileNames != null && 
                i_fileCounter    <  i_inputFileNames.length )
        {

            l_tmpFullInputFileName = new StringBuffer();
            l_tmpFullInputFileName.append(i_inputDirectory);
            l_tmpFullInputFileName.append("/");
            l_tmpFullInputFileName.append(i_inputFileNames[i_fileCounter]);
          
            i_fullInputFileName    = l_tmpFullInputFileName.toString();  
            l_tmpFullInputFileName = null;
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10690, this,
                                 C_METHOD_prepareForOpeningNextInputFile + " before checking filename:" +
                                 i_fullInputFileName);
            }

            
            if ( isFileNameValid(this.i_inputFileNames[i_fileCounter] ) )
            { 
                try
                {
                    l_inputFileHandle  = new LineNumberReader( new FileReader(i_fullInputFileName) );
                    i_fileIsOpen       = true;
                    
                    // OK - prepare for reading
                    i_fraudFileReader = new FraudFileReader(l_inputFileHandle);

                    break; // Process this file
                }
                catch (FileNotFoundException e)
                {
                    sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                        i_logger.logMessage( new LoggableEvent( C_INDATA_FILE_NOT_FOUND + i_fullInputFileName,
                                LoggableInterface.C_SEVERITY_ERROR) );
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                         C_CLASS_NAME, 10530, this,
                                         C_METHOD_prepareForOpeningNextInputFile+ " InputFile not found "
                                         + C_METHOD_init );
                    }
                }        
            }
            else
            {
                //  continue with next file...
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                     C_CLASS_NAME, 10530, this,
                                     "## skapa log meddelande, :"+i_recoveryModeRequired );
                }

            	if ( i_recoveryModeRequired )
            	{
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                         C_CLASS_NAME, 10530, this,
                                         "##### RECOVERY MODE ##### required must start with an IPG file" );
                    }
                    i_logger.logMessage( new LoggableEvent( "Cannot start with : " + i_fullInputFileName + ", recovery file exists for this indata file, inputfile must be an IPG file ",
                            LoggableInterface.C_SEVERITY_ERROR) );
            	}
            	else
            	{
                    i_logger.logMessage( new LoggableEvent( C_NOT_A_VALID_FILENAME + i_fullInputFileName,
                            LoggableInterface.C_SEVERITY_WARNING) );
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10540, this,
                                C_METHOD_prepareForOpeningNextInputFile +
                                " InputFile file name not valid " + this.i_fullInputFileName );
                    }
            	}
           }
        
           i_fileCounter++;  //try next file
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10660, this,
                             BatchConstants.C_LEAVING + C_METHOD_prepareForOpeningNextInputFile);
        }
        
        return i_fileIsOpen;
        
    } // End of prepareForOpeningNextInputFile()


        
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameFile = "renameFile";
    /**
     * Renames a file by changing extension.
     * @param p_fromExtension The current file extension.
     * @param p_toExtension   The file extension to be used.
     * @return
     */
    public boolean renameFile( String p_fromExtension,
                               String p_toExtension )
    {
        final String L_PATTERN_FROM_EXTENSION = "[.]" + p_fromExtension;
        File         l_currentFile             = null;
        String       l_newFullPathName         = null;
        
        l_currentFile = new File( i_fullInputFileName );

        
        // Check if the file already got the right extension
        if ( i_fullInputFileName.endsWith(p_toExtension ))
        {
            return true;
        }
        
        
        // Change file extension
        if ( i_fullInputFileName.endsWith(p_fromExtension) )
        {
            l_newFullPathName = this.i_fullInputFileName.replaceFirst( L_PATTERN_FROM_EXTENSION,
                                                                       p_toExtension );
            
            if (l_currentFile.renameTo( new File(l_newFullPathName) ) )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10710, this,
                                     "RenameFile - SUCCEEDED " + l_newFullPathName);
                }                
            }      
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10720, this,
                                     "RenameFile - FAILED " + l_newFullPathName);
                } 
                return false;
            }
            
            // Save the new full path name 
            i_fullInputFileName = l_newFullPathName;
            return true;
            
        }            
        else
        {
            return false;
        }

    } // End of renameFile(....)

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_initRecoveryInformation = "initRecoveryInformation";
    /**
     * Initialize recovery information for an input file.
     * @param p_fileName
     * @return
     */
    private boolean initRecoveryInformation( String p_fileName )
    {
        // Indexes for element in the RecoveryInformation record:
        // First element is the line number.
        final int    L_RECOVERY_ROW             = 0;
        // Second element is the status of processing the record with the specified line number.
        final int    L_RECOVERY_STATUS          = 1;

        // Directory + Filename of the file with recovery information.
        String       l_fullPathNameRecoveryFile = null;
        // Helpvariable for constructing full path.
        StringBuffer l_tmpFullPathName          = new StringBuffer();
        // Read data from the recovery file. Has the format #line;process status.
        String       l_tmpRecoveryRecord        = null;
        // Help variable to split recovery record.
        String[]     l_tmpRecoveryInformation   = null;

        boolean l_returnValue = false;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10560, this,
                             BatchConstants.C_ENTERING + C_METHOD_initRecoveryInformation + " " +
                             p_fileName);
        }       

                
        // If the input file extension is ".IPG" then the batch job has been interrupted in 
        // some way. The recovery file is needed to match already handled records in the
        // input file. The recovery file only has line numbers of the handled records and
        // process status.
        // This information is read into a Hashtable.
        // Checking if recovery mode done in isFileNameValid(.)
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10580, this,
                             "File needs RECOVERY!! " + C_METHOD_initRecoveryInformation);
        }

        String l_recoveryDirectory = FraudReturnsBatchWriter.getRecoveryDirectory(i_properties);
        
        l_returnValue = true;
        
        // Construct recovery file name               
        l_tmpFullPathName.append( l_recoveryDirectory );
        l_tmpFullPathName.append( "/" );
        l_tmpFullPathName.append( p_fileName.replaceFirst( BatchConstants.C_PATTERN_IN_PROGRESS_FILE,
                                                           BatchConstants.C_EXTENSION_RECOVERY_FILE ));
        l_fullPathNameRecoveryFile = l_tmpFullPathName.toString();
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10590, this,
                             "FullPathNameRecoveryFile=" + l_fullPathNameRecoveryFile);
        }

        BufferedReader l_reader = null;
        try
        {
            l_reader = new BufferedReader( new FileReader( new File(l_fullPathNameRecoveryFile) ) );
            
            // Save recovery line numbers in the hashtable
            while ( l_reader.ready() )
            {
                l_tmpRecoveryRecord      = l_reader.readLine();
                l_tmpRecoveryInformation = l_tmpRecoveryRecord.split(
                                               BatchConstants.C_PATTERN_RECOVERY_DELIMITER);

                String l_old =(String)i_recoveryInformation.put( l_tmpRecoveryInformation[L_RECOVERY_ROW],
                                                l_tmpRecoveryInformation[L_RECOVERY_STATUS]);
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW,  PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME, 10600, this,
                        "**READ RECOVERY FILE: old=" + l_old +
                        "   key=" + l_tmpRecoveryInformation[L_RECOVERY_ROW] +
                        " value=" + l_tmpRecoveryInformation[L_RECOVERY_STATUS]);
                }

            } // end while loop

            
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10600, this,
                                 "#### DEBUG #### - read recovery file");
            }
            for (Enumeration e = i_recoveryInformation.keys(); e.hasMoreElements();)
            {
                // Get file key
                String l_fileKey = (String)e.nextElement();
                String l_status  = (String)i_recoveryInformation.get(l_fileKey);
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10600, this,
                                     "#### DEBUG #### ++recovery information++ : " + l_fileKey + " " +
                                     l_status);
                }
            }

        } // end try block
        catch (FileNotFoundException e)
        {
            i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILENAME_NOT_FOUND,
                                                    LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 10620, this,
                    "Recovery file not found " + C_METHOD_initRecoveryInformation );
            }

        }        

        catch (IOException e)
        {
            i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILE_IO_EXCEPTION,
                                                    LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME, 10630, this,
                    "IO exeption reading recovery file " + C_METHOD_initRecoveryInformation );
            }

        }
        finally
        {
            try
            {
                if (l_reader != null)
                {
                    l_reader.close();
                }
            }
            catch (IOException e1)
            {
               i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILE_CANNOT_BE_CLOSED,
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME, 10640, this,
                        "Problems closing recovery file " + C_METHOD_initRecoveryInformation );
                }
            }
        } // end finally block
 
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                C_CLASS_NAME, 10650, this,
                BatchConstants.C_LEAVING + C_METHOD_initRecoveryInformation);
        }

        return l_returnValue;
        
    } // end of method checkBatchMode


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecoveryStatus = "getRecoveryStatus";    
    /**
     * This method checks if the record already has been correctly processed or not.
     * @return "OK" if already processed
     *         "ERR" if not successfully processed or not processed at all
     */
    protected String getRecoveryStatus()
    {
        String  l_recordStatus = null;   // Can be "ERR"/"OK"
        String  l_key          = Long.toString(i_batchDataRecord.getRowNumber());
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,PpasDebug.C_APP_SERVICE,PpasDebug.C_ST_START,
                C_CLASS_NAME,10800,this,
                BatchConstants.C_ENTERING + C_METHOD_getRecoveryStatus + "#### DEBUG #### l_key=" + 
                i_batchDataRecord.getRowNumber() + "  ->" + l_key);
        }

        l_recordStatus = (String)this.i_recoveryInformation.get( l_key );
        
        if ( l_recordStatus != null ) 
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,PpasDebug.C_APP_SERVICE,PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,10810,this,
                    "isProcessingRequired : recordStatus=" + l_recordStatus);
            }
            
        }
        else
        {
            l_recordStatus = BatchConstants.C_NOT_PROCESSED;
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,PpasDebug.C_APP_SERVICE,PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,10830,this,
                    "could not find this record in the hashtable - send it to processing!");
            }        
            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,PpasDebug.C_APP_SERVICE,PpasDebug.C_ST_END,
                C_CLASS_NAME,10840,this,
                BatchConstants.C_LEAVING + C_METHOD_getRecoveryStatus);
        }
                
        return l_recordStatus;
        
    } // end of method isProcessingRequired()



    private boolean recoveryModeRequired(String p_fileName)
    {
        final int L_INDEX_NAME_PART      = 0;
        final int L_INDEX_EXTENTION_PART = 1;
  
    	StringBuffer l_tmpFullPathName       = new StringBuffer();
        boolean      l_recoveryModeRequired  = false;
        
        // Construct recovery file name               
        l_tmpFullPathName.append( FraudReturnsBatchWriter.getRecoveryDirectory(i_properties) );
        l_tmpFullPathName.append( File.separator );
        String[] l_tmpFileParts = p_fileName.split("\\.");
        l_tmpFullPathName.append(l_tmpFileParts[L_INDEX_NAME_PART]);
        l_tmpFullPathName.append(BatchConstants.C_EXTENSION_RECOVERY_FILE);
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10590, this,
                             "FullPathNameRecoveryFile=" + l_tmpFullPathName.toString());
        }
  
        File l_tmpRecoveryFile = new File(l_tmpFullPathName.toString());
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10590, this,
                             "RCV exists??=" + l_tmpRecoveryFile.exists()+" l_tmpRecoveryFile: "+l_tmpRecoveryFile.getPath());
        }
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 10590, this,
                             "RCV isFile??=" + l_tmpRecoveryFile.isFile()+" l_tmpRecoveryFile.canRead: "+l_tmpRecoveryFile.canRead());
        }

        if ( l_tmpRecoveryFile.exists() )
        {
            l_recoveryModeRequired = true;

        	// The batch must be started in recovery mode!!
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                 "The batch must be started in recovery mode, filename must have extension IPG");
            }
            if ( l_tmpFileParts[L_INDEX_EXTENTION_PART].equals(BatchConstants.C_EXTENSION_IN_PROGRESS_FILE.substring(1,3)))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                     "Indata file is IPG : " + l_tmpFileParts[L_INDEX_EXTENTION_PART]);
                }
            }
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                     "NOT OK - indata file is NOT IPG : " + l_tmpFileParts[L_INDEX_EXTENTION_PART]+ " dont start batch!!!");
                }
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10400, this,
                                 "RCV file does not exist!!!!!!!!!!! - OK to start with txt-file");
            }
        	
        }

        return l_recoveryModeRequired;
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////
    //
    // private class FraudFileReader
    //
    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Inner class that handles reading from the input file.
     */
    class FraudFileReader
    {
        //------------------------------------------------------
        //  Class level constant
        //------------------------------------------------------ 

        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_CLASS_NAME                       = "FraudFileReader";
        
        /** Delimiter in batch record between line number and batch record. Value is {@value}. */
        private static final String C_BATCH_RECORD_DELIMITER           = ";";
        
        /** Delimiter in record. Value is {@value}. */
        private static final String C_RECORD_ELEMENT_DELIMITIER        = ",";

        /** Field length of Voucher Serial Number. Value is {@value}). */
        private static final int    C_MAX_VOUCHER_SERIAL_NUMBER_LENGTH = 20;
        
        /** Field length of Terminal id. Value is {@value}. */
        private static final int    C_MAX_TERMINAL_ID_LENGTH           = 20;
        
        /** Field length of Reason code. Value is {@value}. */
        private static final int    C_MAX_REASON_CODE_LENGTH           = 2;
        
        /** Number of fields in the batchDataRecord array. Value is {@value}. */
        private static final int    C_NUMBER_OF_FIELDS                 = 3;
        
        /** Index for the field Voucher Serial Number in the batchDataRecord array. Value is {@value}. */
        private static final int    C_VOUCHER_SERIAL_NUMBER_INDEX      = 0;
        
        /** Index for the field Terminal Id in the batchDataRecord array. Value is {@value}. */
        private static final int    C_TERMINAL_ID_INDEX                = 1;
        
        /** Index for the field Reason Code in the batchDataRecord array. Value is {@value}. */
        private static final int    C_REASON_CODE_INDEX                = 2;

        /** Record length. Value is [@value). */
        private static final int    C_MAX_RECORD_LENGTH = C_MAX_VOUCHER_SERIAL_NUMBER_LENGTH +
                                                          C_MAX_TERMINAL_ID_LENGTH           +
                                                          C_MAX_REASON_CODE_LENGTH + 2; // 2 ','

        /** Log message - cannot close recovery file. Value is {@value}. */
        private static final String C_INPUT_FILE_CANNOT_BE_CLOSED = "Cannot close input file.";

        /** Log message - cannot close recovery file. Value is {@value}. */
        private static final String C_IO_EXCEPTION_READING_INDATA = "IO Exception reading from indata file.";

        //-------------------------------------------------------------------------
        // Instance variables
        //-------------------------------------------------------------------------
        /** Reference to the file subject to processing by batch job. */
        private LineNumberReader i_inputFileHandle  = null;

        /** Reference to a hash-table containing processed record information. */
        private Hashtable      i_processedRecords   = null;
       
        /** Reference to a hash-table containing recovery information. */
        private boolean          i_endOfFileReached = false;
        
        /** Line number of the last read line from input file. */
        private int            i_lineNumber         = -1;
     
        /** Line number of the last read line from input file. */
        protected String         i_inputLine        = null;
        
        /** Counter, number of rows read. */
        private int            i_counter            = 0;
       

        /**
         * Public Constructor for BatchLineReader.
         * If the file type is .IPG the process will be run in recovery mode.
         * @param p_controller  reference to the batch controller.
         * @param p_ppasContext reference to the pPasContext.
         * @param p_logger      reference to the logger.
         * @param p_parameters  reference to the Map containing information such as filename.
         * @param p_queue       reference to the queue.
         * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
         */
        public FraudFileReader(LineNumberReader p_reader)
        {
            super();
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                 C_CLASS_NAME, 10500, this,
                                 BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
            }

            i_inputFileHandle  = p_reader;
            i_processedRecords = new Hashtable();
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                                 C_CLASS_NAME, 10510, this,
                                 BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
            }
            return;
            
        } // end of Constructor


        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_tidyUp = "tidyUp";
        /**
         * This method is responsible for closing the file and return any resources related to
         * the file operation. After this method return the instance member i_inputFileHandle is 
         * invalid for use. If the process crashes or is stopped before it is finisched the file
         * will not be renamed.
         * By keeping the filename *.IPG the process will automatically enter recovery mode
         * the next time it is started.
         */
        public void tidyUp()
        {
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10660, this,
                                 BatchConstants.C_ENTERING + C_METHOD_tidyUp);
            }
            
            // Close the input file
            if ( i_inputFileHandle != null )
            {
                try
                {
                    this.i_inputFileHandle.close();
                    
                    if ( i_endOfFileReached )
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, 
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 10680,  this,
                                             "TidyUp - end of file reached... ");
                        }

                        renameFile( "IPG", //BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                    BatchConstants.C_EXTENSION_PROCESSED_FILE );
                        
                        i_fileIsOpen = false;
                                                             
                    }

                }
                catch (IOException e)
                {
                    sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                    i_logger.logMessage( new LoggableEvent( C_INPUT_FILE_CANNOT_BE_CLOSED,
                                                            LoggableInterface.C_SEVERITY_ERROR) );

                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                         C_CLASS_NAME, 10670, this,
                                         "TidyUp - IO exception from close() of indata file");
                    }                
                }
            }
            else
            {
                // No files found in directory
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10670, this,
                                     "TidyUp - No files found in indata directory");
                }                

            }
                    
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 10690, this,
                                 BatchConstants.C_LEAVING + C_METHOD_tidyUp);
            }

            return;
        } // end of method isRecovery


        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_readNextLine = "readNextLine";
        /** This method reads the next line from the input file. The instance member i_fileHandle
         * will keep track of the filepointer. The i_fileHandler-object will not be synchronized
         * since only one thread operates on the file. Even if an enhancement of the reader is made no
         * synchronization is required since each file always will be processed by one dedicated thread.
         * When end-of-file is reached this method will return null to indicate that reader may finish.
         * Instance variables: i_rowNumber and i_inputLine are initialized.
         * @return Read line information: #line;readline
         */
        protected String readNextLine()
        {
            String       l_currentLine    = null;               // Line number;Record from the input file
            StringBuffer l_tmpReturnValue = new StringBuffer(); // Help variable for adding line number to
                                                                // the read line
            String[]     l_tmpFileNameComponents = null;
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10740, this,
                                 BatchConstants.C_ENTERING + C_METHOD_readNextLine);
            } 

            try
            {
                i_inputLine  = i_inputFileHandle.readLine();
                i_lineNumber = i_inputFileHandle.getLineNumber();
            }
            catch (IOException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_IO_EXCEPTION_READING_INDATA,
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VHIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10790, this,
                                     "Reading a line no=" + i_lineNumber + " record:" + i_inputLine);
                } 
                
            }
            
            // Add information to the data record
            if ( this.i_inputLine != null )
            {
                l_tmpReturnValue.append( this.i_lineNumber );
                l_tmpReturnValue.append( C_BATCH_RECORD_DELIMITER );
                l_tmpReturnValue.append( this.i_inputLine );
                l_currentLine = l_tmpReturnValue.toString();
                
                i_batchDataRecord = new FraudBatchRecordData(i_maxSerialNumberLength);

                l_tmpFileNameComponents = i_inputFileNames[i_fileCounter].split("[.]");
                i_batchDataRecord.setInputFilename( l_tmpFileNameComponents[0] );
                i_batchDataRecord.setRowNumber( i_lineNumber );
                i_batchDataRecord.setInputLine( i_inputLine );
                i_batchDataRecord.setAgent(i_agent);
                i_batchDataRecord.setExpectedNumberOfRecords(i_expectedNumberOfRecords);
                
                i_counter++;
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10790, this,
                                     C_METHOD_readNextLine + " Number of records read: " + i_counter +
                                     " (expected number of records: " + i_expectedNumberOfRecords + ")");
                } 

            }
            else
            {
                l_currentLine      = null;
                i_endOfFileReached = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10790, this,
                                     C_METHOD_readNextLine + " END OF FILE reached"+ i_lineNumber +
                                     " record:" + i_inputLine + " counter=" + i_fileCounter);
                } 

            }
            
            
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 10780, this,
                                 BatchConstants.C_LEAVING + C_METHOD_readNextLine +
                                 " currentLine= " + l_currentLine);
            }
            
            return l_currentLine;
            
        } // end of method readNextLine()



        
        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    
        /**
         * This method extracts information from the batch data record. The record must look like:
         * [VOUCHER SERIAL NUMBER],[TERMINAL ID],[REASON CODE]
         * 
         * All fields are checked to be alpha numeric.
         * @return <code>true</code> When extracted fields are valid else <code>false</code>.
         */
        private boolean extractLineAndValidate()
        {
            String[]     l_recordFields = null;            
            boolean      l_validData    = false;  // Assume not a valid record.
            StringBuffer l_errorCodes   = new StringBuffer();
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10320, this,
                                 BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
            }

            // Check record length
            if ( i_inputLine != null && i_inputLine.length() <= C_MAX_RECORD_LENGTH )
            {

                l_recordFields = i_inputLine.split( C_RECORD_ELEMENT_DELIMITIER );                        

                if ( l_recordFields.length == C_NUMBER_OF_FIELDS )
                {
                    // OK - test next field, Voucher Serial Number
                    if ( l_recordFields[C_VOUCHER_SERIAL_NUMBER_INDEX].length() <= C_MAX_VOUCHER_SERIAL_NUMBER_LENGTH &&
                         l_recordFields[C_VOUCHER_SERIAL_NUMBER_INDEX].matches(BatchConstants.C_PATTERN_NUMERIC) )
                    {

                        i_batchDataRecord.setVoucherSerialNumber(l_recordFields[C_VOUCHER_SERIAL_NUMBER_INDEX]);
                        
                        // OK - test next field, Terminal Id
                        if ( l_recordFields[C_TERMINAL_ID_INDEX].length() <= C_MAX_TERMINAL_ID_LENGTH &&
                             l_recordFields[C_TERMINAL_ID_INDEX].matches(BatchConstants.C_PATTERN_NUMERIC) )
                        {

                            i_batchDataRecord.setTerminalId(l_recordFields[C_TERMINAL_ID_INDEX]);
                            
                            // OK - test next field, Reason Code
                            if ( l_recordFields[C_REASON_CODE_INDEX].length() <= C_MAX_REASON_CODE_LENGTH &&
                                 l_recordFields[C_REASON_CODE_INDEX].matches(BatchConstants.C_PATTERN_NUMERIC) )
                            {

                                i_batchDataRecord.setReasonCode(Integer.parseInt(l_recordFields[C_REASON_CODE_INDEX]));
                                
                                // Check if this Voucher Serial number is duplicate
                                if ( isDuplicate() )
                                {                                    
                                    // Store information in data record
                                    i_batchDataRecord.setExceptionDetails(C_DUPLICATE);
                                }
                                else
                                {
                                
                                    // Save data as "processed" - to detect if a duplicate serial number comes
                                    i_processedRecords.put(l_recordFields[C_VOUCHER_SERIAL_NUMBER_INDEX],
                                                           i_inputLine);
                                }
                                
                                // All field are validated - OK
                                l_validData = true;                                

                            }
                            else
                            {
                                l_errorCodes.append(C_ERROR_INVALID_REASON_CODE);
                            }
                        }
                        else
                        {
                           l_errorCodes.append(C_ERROR_INVALID_TERMINAL_ID);
                        }
                    }
                    else
                    {
                        l_errorCodes.append(C_ERROR_INVALID_VOUCHER_SERIAL_NUMBER);
                    }
                }
                else
                {
                    l_errorCodes.append(C_ERROR_WRONG_NUMBER_OF_FIELDS);
                }
            }
            else
            {
                l_errorCodes.append(C_ERROR_RECORD_TO_LONG);
            }
                    
            if ( !l_validData )        
            {
                if ( i_batchDataRecord.getInputFilename().equals(C_END_OF_FILE))
                {
                    // This IS a legal "flag record" that has been generated to announce for the Writer
                    // that a new input file is going to processed.'
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                         C_CLASS_NAME, 10320, this,
                                         "* EOF RECORD GENERATED to prepare the Writer for the next file *" );
                    }
                    return true;
                }

                if ( l_recordFields != null )
                {
                    {
                        if ( l_recordFields.length == 3 )
                        {
                            // Illegal field format detected
                            if (PpasDebug.on)
                            {
                                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                                 C_METHOD_extractLineAndValidate, 10360, this,
                                                 "Row: ]" + i_inputLine + "[ is a INVALID record\n"                       +
                                                 " lineNumber          =" + i_lineNumber                                  + "\n" +
                                                 " voucherSerialNumber =" + l_recordFields[C_VOUCHER_SERIAL_NUMBER_INDEX] + "\n" +
                                                 " terminalId          =" + l_recordFields[C_TERMINAL_ID_INDEX]           + "\n" +
                                                 " reasonCode          =" + l_recordFields[C_REASON_CODE_INDEX]);
                            }
                        }
                        else
                        {
                            if (PpasDebug.on)
                            {
                                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, 
                                                 PpasDebug.C_ST_START, C_METHOD_extractLineAndValidate,
                                                 10360, this,
                                                 "Row: ]" + i_inputLine + 
                                                 "[ is a INVALID record, having the following fields: ");
                            }

                            // Wrong number of fields
                            for ( int i = 0; i < l_recordFields.length; i++)
                            {
                                if (PpasDebug.on)
                                {
                                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                                     PpasDebug.C_ST_TRACE, C_METHOD_extractLineAndValidate,
                                                     10360, this,
                                                     "Field[" + i + "] : " + l_recordFields[i]);
                                }
                            }                           
                        }
                    }
                }   
                
                if ( l_errorCodes.length() == 0 )
                {
                    l_errorCodes.append(C_ERROR_INVALID_RECORD_FORMAT);
                }
                else // ( l_errorCodes.length() != 0)
                {
                    i_batchDataRecord.setCorruptLine( true );
                    l_errorCodes.append(C_DELIMITER_ERROR_LOG);
                    l_errorCodes.append(i_batchDataRecord.getInputLine());
                    i_batchDataRecord.setErrorLine( l_errorCodes.toString());
                    i_batchDataRecord.setRecoveryStatus(BatchConstants.C_NOT_PROCESSED);
                }
                    
            }
                    
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 10390, this,
                                 BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
            }

            return l_validData;
            
        } // End of method extractLineAndValidate(.)


        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_isDuplicate = "isDuplicate";        
        /**
         * This method checks if the record already has been correctly processed, if it has this record
         * is a duplicate.
         * @return <code>true</code>  means it is a duplicate 
         *         <code>false</code> has already been successfully processed
         */
        protected boolean isDuplicate()
        {
            boolean l_duplicate = false;
            String  l_key       = i_batchDataRecord.getVoucherSerialNumber();
            String  l_inputLine = null;
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10800, this,
                                 BatchConstants.C_ENTERING + C_METHOD_isDuplicate + " l_key=" + l_key);
            }

            l_inputLine = (String)i_processedRecords.get( l_key );
            
            if ( l_inputLine != null ) 
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10810, this,
                                     C_METHOD_isDuplicate + " duplicate record : " + l_inputLine );
                }        
          
                l_duplicate = true;
            }
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 10810, this,
                                     C_METHOD_isDuplicate + " this was not a duplicate record" );
                }        
                 
            }

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 10840, this,
                                 BatchConstants.C_LEAVING + C_METHOD_isDuplicate );
            }
                    
            return l_duplicate;
            
        } // end of method isDuplicate

    }
}
