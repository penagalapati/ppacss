////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DisconBatchReader
//      DATE            :       1-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 28/06/07 | E Dangoor     | Remove duplicate line            | PpacLon#3195/11774
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isil.batchisilservices.DisconDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class is responsible to provide doRun() with BatchDataRecords representing customers 
 * due for disconnection.
 * Since this is a database driven batch process the creation and initial population of 
 * DisconBatchDataRecords are handled by the dedicated "Batch ISIL Service". 
 */
public class DisconBatchReader extends BatchReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME                    = "DisconBatchReader";
    
    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_START_RANGE_IS_MISSING        = "Start range is missing.";

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_END_RANGE_IS_MISSING          = "End range is missing.";

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_DISCONNECTION_DATE_IS_MISSING = "Disconnection date is missing.";

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_RECOVERY_FLAG_IS_MISSING      = "Recovery flag is missing.";

    /** Number of values in the array i_parameters. Value is {@value}. */
    private static final int      C_NUMBER_OF_PARAMETERS          = 3;
    
    /** Index for Start Range value in the array i_parameters.  Value is {@value}. */
    private static final int      C_START_RANGE                   = 0;

    /** Index for Stop Range value in the array i_parameters.  Value is {@value}. */
    private static final int      C_END_RANGE                     = 1;

    /** Index for Disconnection date value in the array i_parameters.  Value is {@value}. */
    private static final int      C_DISCONNECTION_DATE            = 2;
        
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    
    /** The 'batch isil' api for the disconnection batch. */
    private DisconDbBatchService i_isApi = null;

    /** Input paramters to the SQL statement. */
    private String[] i_inputParameters   = null;

    /** Variable to keep the properties value for the fetch size. */
    private int i_fetchSize              = 0;

    /**
     * Constructor for the DisconBatchReader.     
     * @param p_ppasContext Reference to the PpasContext.
     * @param p_logger      Reference to the Logger.
     * @param p_controller  Reference to the call-back Controller.
     * @param p_inQueue     Reference to the in-queue.
     * @param p_parameters  Reference to the start parameters.
     * @param p_properties  Reference to the batch properties.
     */
    public DisconBatchReader(
        PpasContext      p_ppasContext,
        Logger           p_logger,
        BatchController  p_controller,
        SizedQueue       p_inQueue,
        Map              p_parameters,
        PpasProperties   p_properties )
    {
        super(p_ppasContext, p_logger,  p_controller, p_inQueue, p_parameters, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        this.getProperties();
        this.getDisconnectionParameters(p_parameters);
        this.init();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }

    } // End of constructor





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
   /** 
    * This method call the Batch ISIL Service that returns a DisconBatchDataRecord.
    * No manipulation is required since the underlying database service has knowledge
    * of the record created. The customer id is extracted and used to call
    * addCustomerInprogress().
    * 
    * @return The DisconBatchDataRecord
    */
    protected BatchRecordData getRecord()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        DisconBatchRecordData l_record = null;
        
        // get one record by calling fetch
        try
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "innan i_isApi.fetch() - getRecord. " + C_METHOD_getRecord );
            }

            l_record = (DisconBatchRecordData)i_isApi.fetch();
            
            // Note that fetch can return null. This indicates 'no more records to fetch'.
            if ( l_record != null )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "after i_isApi.fetch() - getRecord. " + C_METHOD_getRecord + 
                        " record: " + l_record.dumpRecord());
                }
            }
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "after i_isApi.fetch() - getRecord. " + C_METHOD_getRecord +
                        " record IS NULL - no more to read");
                }                                   
            }
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "SQLException - getRecord. " + C_METHOD_getRecord );
            }
 
            e.printStackTrace();
        }        
        
        // Return the record to doRun() in super class
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return l_record;
        
    } // End of getRecord()






    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";    
    /**
     * This method instantiate and opens the BatchISIL Service.
     */
    protected void init()
    {
        PpasRequest l_request      = null;
        PpasSession l_session      = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }


        // Create and open the Batch ISIL Service Component
        l_session = new PpasSession();
        l_session.setContext(this.i_context);
        
        l_request = new PpasRequest(l_session);
        i_isApi   = new DisconDbBatchService(l_request, i_logger, i_context );

        // Call open on the API component
        try
        {
            i_isApi.open(i_inputParameters, i_fetchSize );
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "PpasSqlException! - getRecord. " + C_METHOD_init );
            }

            e.printStackTrace();
        }
        catch (SecurityException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "SecurityException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "SecurityException - getRecord. " + C_METHOD_getRecord );
            }

            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "IllegalArgumentException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "IllegalArgumentException - getRecord. " + C_METHOD_getRecord );
            }

            e.printStackTrace();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }

        return;
    } // End of init()




    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_tidyUp = "tidyUp";    

    /**
     * @see com.slb.sema.ppas.batch.batchreader.BatchReader#tidyUp() .
     */
    protected void tidyUp()
    {
        // Clean up connectivity
        try
        {
            finalise();
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( e );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "PpasServiceException - " + C_METHOD_tidyUp );
            }

            e.printStackTrace();
        }
                
        return;
    }  // End of tidyUp()





    /**
     * Clean up and closes isapi.
     * @throws PpasServiceException Unable to close the isapi.
     */
    protected void finalise() throws PpasServiceException
    {
        if ( this.i_isApi != null )
        {
            this.i_isApi.close();
            this.i_isApi = null;
            
        }
        
        return;
    }  // End of finalise()





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getDisconnectionParameters = "getDisconnectionParameters";    
    /**
     * This method reads the paramters: StartRange, EndRange, DicsconnectionDate and RecoveryFlag
     * from the in-parameter Map.
     * Checks if all paramters are given and if the ranges is numeric, the date a possible date and
     * that the recovery flag is either Yes or No.
     * @param p_parameters Start parameters.
     * @return true if valid parameters.
     */
    private boolean getDisconnectionParameters( Map p_parameters )
    {
        boolean l_returnValue  = true;   // Assume valid arguments in the Map
        String  l_recoveryMode = null;   // Help variable to read the recovery flag
        
        this.i_inputParameters = new String[C_NUMBER_OF_PARAMETERS];

        this.i_inputParameters[C_START_RANGE] = 
                (String)p_parameters.get(BatchConstants.C_KEY_START_RANGE);
        if ( this.i_inputParameters[C_START_RANGE] == null )
        {
            // Cannot find the start range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_START_RANGE_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "Start range is missing in the parameter list. " + C_METHOD_getDisconnectionParameters );
            }
            l_returnValue = false;
        }

        this.i_inputParameters[C_END_RANGE] =
                (String)p_parameters.get(BatchConstants.C_KEY_END_RANGE);
        if ( this.i_inputParameters[C_END_RANGE] == null )
        {
            // Cannot find the end range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_END_RANGE_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "End range is missing in the parameter list. " + C_METHOD_getDisconnectionParameters );
            }
            l_returnValue = false;

        }
        
        this.i_inputParameters[C_DISCONNECTION_DATE] =
            (String)p_parameters.get(BatchConstants.C_KEY_DISCONNECTION_DATE);
        if ( this.i_inputParameters[C_DISCONNECTION_DATE] == null )
        {
            // Cannot find the disconnection date in Map
            // Cannot find the end range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_DISCONNECTION_DATE_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "Disconnection date is missing in the parameter list. " + 
                        C_METHOD_getDisconnectionParameters );
            }
        }

        l_recoveryMode = (String)p_parameters.get(BatchConstants.C_KEY_RECOVERY_FLAG);
        if ( l_recoveryMode == null )
        {
            // Cannot find the recovery mode flag in the Map
            // Cannot find the end range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_RECOVERY_FLAG_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "Recovery flag is missing in the parameter list. " + 
                        C_METHOD_getDisconnectionParameters );
            }

        }

        if (l_returnValue )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "Paramters -- start range: '" + i_inputParameters[C_START_RANGE] +
                    "',  end range: '" + i_inputParameters[C_END_RANGE] +
                    "',  disc. date: '" + i_inputParameters[C_DISCONNECTION_DATE] +
                    "',  recovery: '" + l_recoveryMode + "'.");
            }
            
            // Parameters given - check if the ranges are numeric and disconnection date a 'date'
            // Recovery mode can be "Yes" or "No"
            if ( this.i_inputParameters[C_START_RANGE].matches(BatchConstants.C_PATTERN_NUMERIC) &&
                 this.i_inputParameters[C_END_RANGE].matches(BatchConstants.C_PATTERN_NUMERIC)   &&
                 this.i_inputParameters[C_DISCONNECTION_DATE].matches(BatchConstants.C_PATTERN_DATE) &&
                 l_recoveryMode.matches(BatchConstants.C_PATTERN_RECOVERY) )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "All paramters OK, so far.. " + C_METHOD_getDisconnectionParameters );
                }
                if ( l_recoveryMode.equals(BatchConstants.C_VALUE_RECOVERY_MODE_ON) )
                {
                    this.i_recoveryMode = true;                
                }
                else
                {
                    this.i_recoveryMode = false;
                }
            }
            else
            {
                // At least one of the paramters had an invalid value
                // Cannot find the end range in Map
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_END_RANGE_IS_MISSING,
                                     LoggableInterface.C_SEVERITY_ERROR) );

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "At least one of the paramters had an invalid value in the parameter list. " + 
                            C_METHOD_getDisconnectionParameters );
                }

                l_returnValue = false;
            }

        }
        return l_returnValue;
        
    } // End of getDisconnectionParameters(.)





    /**
     * Reads the property value for the fetchsize.
     */
    private void getProperties()
    {
        String l_tmpFetchSize = null;
        
        l_tmpFetchSize = i_properties.getProperty(BatchConstants.C_FETCH_ARRAY_SIZE);
        if ( l_tmpFetchSize != null )
        {
            this.i_fetchSize = Integer.valueOf(l_tmpFetchSize).intValue();
        }
        
        return;        
    }
    
} // End of DisconBatchReader
