////////////////////////////////////////////////////////////////////////////////
//
// FILE NAME   : SynchronisationBatch.java
// DATE        : 28-Jul-2004
// AUTHOR      : Alan Barrington-Hughes
// REFERENCE   : PRD_ASCS00_DEV_SS_83
//
// COPYRIGHT   : WM-data 2007
//
// DESCRIPTION : Main class for Synchronisation Batch
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 11/01/06 | M.Alm      | Added functionality to reload   | PpacLon#1908/7763
//          |            | Business Config Cache after the |
//          |            | new setup have been stored      |
//----------+------------+---------------------------------+--------------------
// 31/07/06 | Chris      | Added cross validation of Next  | PpacLon#2329/9464
//          | Harrison   | Tier Promotion Id's & Division  | PRD_ASCS_GEN_CA_096
//          |            | Table Id's                      |
//----------+------------+---------------------------------+-------------------
// 21/08/06 | Chris      | Review amendments.              | PpacLon#2329/9771
//          | Harrison   |                                 |
//----------+------------+---------------------------------+-------------------
// 20/03/07 | Lars L.    | Improved error handling.        | PpacLon#2076/7827
//          |            |                                 |
//----------+------------+---------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchsynch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.xml.sax.SAXParseException;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchInvalidParameterException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.DivisionSynchData;
import com.slb.sema.ppas.common.dataclass.PromotionSynchData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.instrumentation.RemoteInstrumentationClient;
import com.slb.sema.ppas.common.instrumentation.RemoteMngMthRequest;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcConnectionPool;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.is.isapi.PpasSynchService;
import com.slb.sema.ppas.js.jobrunner.SimpleJavaJob;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.ConfigException;
import com.slb.sema.ppas.util.support.ParseException;
import com.slb.sema.ppas.util.support.UtilDateFormat;

/**
 * Class to synchronise the AIR configuration with ASCS.
 */
public class SynchronisationBatch extends SimpleJavaJob
{
    /** Input file containing XML data from AIR node. */
    private String                     i_inFile                      = null;

    /** Input file containing AIR schema definition. */
    private String                     i_schemaFile                  = null;
    
    /** 
     * Whether or not the XML parser should validate the XML 
     * against the schema definition file (.xsd).
     */
    private boolean                    i_validation                  = false;

    /** Report filename to store outcome. */
    private String                     i_rptFileName                 = null;

    /** IS API access to batch control services. */
    private PpasBatchControlService    i_ppasBatchControlService     = null;

    /** Cache reloader object. */
    private RemoteInstrumentationClient i_remoteInstrClient          = null;

    /** Context to hold configuration. */
    private PpasContext                i_context                     = null;

    /** Job scheduling ID. */
    private String                     i_jsJobId                     = null;

    /** Timestamp when this batch started. */
    private PpasDateTime               i_executionDateTime           = null;

    /** Runtime parameters for this batch. */
    private Map                        i_params                      = null;

    /** Logger for event messages. */
    private Logger                     i_logger                      = null;

    /** Request object for interacting with IS. */
    private PpasRequest                i_request                     = null;

    /** Timeout value for IS requests. */
    private long                       i_isApiTimeout                = 0L;

    /** Filename Date element. */
    private String                     i_fileDate                    = null;

    /** Filename sequence element. */
    private String                     i_fileSeq                     = null;

    /** Report file writer. */
    private BufferedWriter             i_rptFile                     = null;

    /** Indicates a control record exists. */
    private boolean                    i_controlRecord               = false;

    /** Hidden property to disable validation towards DEDA. */
    private boolean                    i_dedaValidate                = true;

    /** Hidden property to disable the automatic reload of Business Config. */
    private boolean                    i_reloadBC                  = true;

    /** Prefix for this class to simplify code when retrieving properties. */
    static final String                C_CLASS_PREFIX                =
        "com.slb.sema.ppas.batch.batchsynch.SynchronisationBatch";

    /** Standard date/time formatter to generate strings of the form yyyyMMdd_HHmmss. */
    public static final UtilDateFormat C_DF_yyyyMMdd_HHmmss          = new UtilDateFormat("yyyyMMdd_HHmmss");

    /** Length of the sequence number to add to long filename. */
    private static final int           C_LENGTH_FILE_SEQUENCE_NUMBER = 6;

    /** Class description constant. */
    private static final String        C_CLASS_NAME                  = "SynchronisationBatch";
    

    /**
     * Class constructor. Instantiated by the JobRunner.
     * @param p_ppasContext Context to manage configuration
     * @param p_jobType The type of this job
     * @param p_jsJobId Job scheduling ID
     * @param p_jobRunnerName Name of the job runner that started this batch
     * @param p_params Runtime batch parameters
     * @throws PpasConfigException When unable to read in configuration
     * @throws PpasSqlException When unable to read in business config cache
     */
    public SynchronisationBatch(PpasContext p_ppasContext,
            String p_jobType,
            String p_jsJobId,
            String p_jobRunnerName,
            Map p_params) throws PpasConfigException, PpasSqlException
    {
        super(p_ppasContext, p_jobType, p_jsJobId, p_jobRunnerName, p_params);

        PpasSession l_session = new PpasSession();
        BusinessConfigCache l_cache = null;
        JdbcConnection l_conn = null;
        JdbcConnectionPool l_pool = null;
        PpasProperties l_props = null;
        
        i_context = p_ppasContext;
        i_logger = p_ppasContext.getLoggerPool().getLogger();
        i_remoteInstrClient = new RemoteInstrumentationClient(i_logger, i_properties);
        
        try
        {
            l_props = p_ppasContext.getPropertiesWithAddedLayers("batch_syn");
        }
        catch (PpasConfigException e)
        {
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + "Constructor" + " Reading configuration.",
                                                  LoggableInterface.C_SEVERITY_FATAL));
            throw e;
        }

        // This batch should neither be pausible nor stoppable.
        super.setPausable(false);
        super.setStoppable(false);
        
        i_jsJobId = p_jsJobId;
        i_params = p_params;

        i_executionDateTime = DatePatch.getDateTimeNow();

        l_session.setContext(p_ppasContext);
        i_request = new PpasRequest(l_session);
        
        l_pool = (JdbcConnectionPool)p_ppasContext.getAttribute("JdbcConnectionPool");
        i_isApiTimeout = super.i_properties.getLongProperty(BatchConstants.C_TIMEOUT, 10000L);
        
        // Get a JdbcConnection.
        try
        {
            l_conn = l_pool.getConnection(C_CLASS_NAME,
                                          C_CLASS_NAME,
                                          10010,
                                          this,
                                          i_request,
                                          0,
                                          i_isApiTimeout);

            l_cache = (BusinessConfigCache)p_ppasContext.getAttribute("BusinessConfigCache");
            //Load cache ready for use in Division parser.
            l_cache.loadAll(i_request, l_conn);
        }
        catch (PpasSqlException e)
        {
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + ":Constructor:"
                    + "Exception load business config cache" + e, LoggableInterface.C_SEVERITY_FATAL));
            throw e;
        }
        finally
        {
            if (l_conn != null)
            {
                // Put the connection back in the pool.
                l_pool.putConnection(0, l_conn);
            }
        }
        
        this.i_inFile = l_props.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY) + File.separator
                + i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);

        // Get schema from properties
        this.i_schemaFile = l_props.getTrimmedProperty(C_CLASS_PREFIX + ".schemaLocation");
        
        // Are we going to validate against the schema file?
        i_validation = l_props.getBooleanProperty(C_CLASS_PREFIX + ".validation", true);

        this.i_rptFileName = l_props.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY)
                + File.separator + "BATCH_CFG_SYNCH_CLASS_"
                + C_DF_yyyyMMdd_HHmmss.format(i_executionDateTime) + ".RPT";

        // Get hidden property to disable validation towards DEDA
        String l_tmp = l_props.getTrimmedProperty(C_CLASS_PREFIX + ".dedaValidate", "on");
        if (l_tmp.equals("off"))
        {
            this.i_dedaValidate = false;
        }
        else
        {
            this.i_dedaValidate = true;
        }

        // Get hidden property to disable the automatic reload of Business Config
        l_tmp = l_props.getTrimmedProperty(C_CLASS_PREFIX + ".reloadBusinessConfig", "on");
        if (l_tmp.equals("off"))
        {
            this.i_reloadBC = false;
        }
        else
        {
            this.i_reloadBC = true;
        }

        // Instantiate the batch control service method (IS API)
        i_ppasBatchControlService = new PpasBatchControlService(null, i_logger, super.getContext());
        
    }

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_doJobProcessing = "doJobProcessing";

    /**
     * Does the processing.
     */
    public void doJobProcessing()
    {
        
        i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                + " Synchronisation Batch Starting.", LoggableInterface.C_SEVERITY_INFO));

        String l_status = BatchConstants.C_BATCH_STATUS_FAILED;
        boolean l_isValidFileName = false;
        boolean l_isShortFileName = false;
        boolean l_validParams = true;
        String l_newFileName = null;
        String l_fileName = null;
        File l_fileToRename = null;
        int l_jStatus = JobStatus.C_JOB_EXIT_STATUS_FAILURE;
        
        AIRXMLFileReader l_fileReader = new AIRXMLFileReader(i_logger, 
                                                             i_context, 
                                                             getSubmitterOpid(),
                                                             i_dedaValidate);
        
        // Vector holding all promotion configuration from file
        Vector l_promotionConfigFromFile = null;

        // Vector holding all refill configuration from file
        Vector l_refillConfigFromFile = null;

        // Vector holding all division configuration from file
        Vector l_divisionConfigFromFile = null;

        // IS API service to perform database interaction for batch services.
        PpasSynchService l_isApi = new PpasSynchService(i_request, i_logger, i_context);
        
        //Notify that the job has started.
        super.startDone();
        
        try
        {
            l_fileName = (String)i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
            if (null != i_inFile
                    && null != l_fileName
                    && (l_fileName.matches(BatchConstants.C_PATTERN_SYNCH_LONG_FILENAME_SCH) || l_fileName
                            .matches(BatchConstants.C_PATTERN_SYNCH_LONG_FILENAME)))
            {
                // Parse the file name to get date & sequence. (split on under-score)
                String[] l_parsedPath = this.i_inFile.split("AIRDATASYNC");
                String[] l_parsedFile = l_parsedPath[1].split("_");
                i_fileDate = l_parsedFile[1];
                String[] l_fileSeqAndExtension = l_parsedFile[2].split("\\.");
                i_fileSeq = l_fileSeqAndExtension[0];
                l_isValidFileName = true;
            }
            else if (null != i_inFile
                    && null != l_fileName
                    && (l_fileName.matches(BatchConstants.C_PATTERN_SYNCH_SHORT_FILENAME_SCH) || l_fileName
                            .matches(BatchConstants.C_PATTERN_SYNCH_SHORT_FILENAME)))
            {
                l_isValidFileName = true;
                l_isShortFileName = true;

                i_fileDate = DatePatch.getDateToday().toString_yyyyMMdd();
                i_fileSeq = getFileSequenceNumber();

                if (null == i_fileDate || null == i_fileSeq)
                {
                    i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                          " Failed to supply file date and sequence number: "
                                                          + i_inFile,
                                                          LoggableInterface.C_SEVERITY_FATAL));
                    l_validParams = false;
                }
            }

            if (!l_isValidFileName)
            {
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Incorrect filename format: " + "\nAbsolute filename: " + i_inFile
                        + "\nInput Filename from Job Runner: " + l_fileName + "\nShould match pattern: "
                        + BatchConstants.C_PATTERN_SYNCH_SHORT_FILENAME_SCH + "\nor pattern: "
                        + BatchConstants.C_PATTERN_SYNCH_SHORT_FILENAME, LoggableInterface.C_SEVERITY_FATAL));
            }

            if (l_isValidFileName && l_validParams)
            {
                this.init();

                // Rename input file to IPG
                l_fileToRename = new File(i_inFile);
             
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Renaming file: " + i_inFile, LoggableInterface.C_SEVERITY_INFO));
             
                if (i_inFile.endsWith("SCH"))
                {
                    l_newFileName = i_inFile.replaceFirst("[.\\.]SCH", ".IPG");
                }
                else if (i_inFile.endsWith("XML"))
                {
                    l_newFileName = i_inFile.replaceFirst("[.\\.]XML", ".IPG");
                }
                else if (i_inFile.endsWith("xml"))
                {
                    l_newFileName = i_inFile.replaceFirst("[.\\.]xml", ".IPG");
                }
           
                if (l_isShortFileName)
                {
                    // Need to incorporate the date and sequence number
                    String[] l_part = l_newFileName.split("[\\.]");
                    l_newFileName = l_part[0] + "_" + i_fileDate + "_" + i_fileSeq + "." + l_part[1];
                }
           
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " To file: " + l_newFileName, LoggableInterface.C_SEVERITY_INFO));
           
                l_fileToRename.renameTo(new File(l_newFileName));
                i_inFile = l_newFileName;
           
                //Create report file
                i_rptFile = new BufferedWriter(new FileWriter(i_rptFileName));
           
                // Instantiate and initialize the XMLFileReader.
                
                l_fileReader.init(i_validation, i_schemaFile, i_inFile);
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Parsed AIR Data schema.", LoggableInterface.C_SEVERITY_INFO));
                
                // Get the new configuration for Promotions
                l_promotionConfigFromFile = l_fileReader.getPromotionConfig();
                // Get the new configuration for Refills
                l_refillConfigFromFile = l_fileReader.getRefillConfig();
                // Get the new configuration for Divisions
                l_divisionConfigFromFile = l_fileReader.getDivisionConfig();

                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Loaded AIR Data.", LoggableInterface.C_SEVERITY_INFO));
                
                // Cross validate the Next Tier Promotion Id's
                // & Division Table Id's in the Promotion Plan configuration
                validatePromotionConfig(l_promotionConfigFromFile, l_divisionConfigFromFile);
                
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Committing synchronised configuration.", LoggableInterface.C_SEVERITY_INFO));

                // Call the database loader to execute synchronization.
                l_isApi.synchronise(i_request,
                                    i_isApiTimeout,
                                    l_refillConfigFromFile,
                                    l_divisionConfigFromFile,
                                    l_promotionConfigFromFile);
                
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Data synchronisation committed.", LoggableInterface.C_SEVERITY_INFO));

                
                // Rename IPG file to DNE
                l_fileToRename = new File(i_inFile);

                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " Renaming file: " + i_inFile, LoggableInterface.C_SEVERITY_INFO));
                l_newFileName = i_inFile.replaceFirst("[.\\.]IPG", ".DNE");
                
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                        + " To file: " + l_newFileName, LoggableInterface.C_SEVERITY_INFO));
                l_fileToRename.renameTo(new File(l_newFileName));
                
                // We'll set status to completed if we reach this stage
                l_status = BatchConstants.C_BATCH_STATUS_COMPLETED;
            }
        }
        catch (ConfigException l_configEx)
        {
            i_logger.logMessage(l_configEx);
            String l_errorMsg = " Exception setting XML parser. ";
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing + l_errorMsg,
                                                  LoggableInterface.C_SEVERITY_FATAL));

            // Print error line in the report file.
            if (i_rptFile != null)
            {
                try
                {
                    i_rptFile.write("99" + l_errorMsg + "See log files for details.\n");
                }
                catch (IOException l_ioEx)
                {
                    i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                                                          + " Exception writing to report file. " + l_ioEx,
                                                          LoggableInterface.C_SEVERITY_FATAL));
                }
            }
        }
        catch (PpasServiceException l_ppasServEx)
        {
            i_logger.logMessage(l_ppasServEx);
            String l_errorMsg = " Exception interacting with IS. ";
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing + l_errorMsg,
                                                  LoggableInterface.C_SEVERITY_ERROR));

            // Print error line in the report file.
            if (i_rptFile != null)
            {
                try
                {
                    i_rptFile.write("99" + l_errorMsg + "See log files for details.\n");
                }
                catch (IOException l_ioEx)
                {
                    i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                                                          + " Exception writing to report file. " + l_ioEx,
                                                          LoggableInterface.C_SEVERITY_FATAL));
                }
            }
        }
        catch (ParseException l_parseEx)
        {
            i_logger.logMessage(l_parseEx);
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                  " Exception parsing XML file. ",
                                                  LoggableInterface.C_SEVERITY_FATAL));
            try
            {
                reportParseException(l_parseEx);
            }
            catch (IOException l_ioEx)
            {
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                      " Exception writing to report file. " + l_ioEx,
                                                      LoggableInterface.C_SEVERITY_FATAL));
            }
        }
        catch (IOException l_ioEx)
        {
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                  " Exception opening report file. " + l_ioEx,
                                                  LoggableInterface.C_SEVERITY_FATAL));
        }
        catch (BatchInvalidParameterException l_bipEx)
        {
            i_logger.logMessage(l_bipEx);
            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                  " Exception validating input file. ",
                                                  LoggableInterface.C_SEVERITY_ERROR));
            try
            {
                reportBatchParamException(l_bipEx);
            }
            catch (IOException l_ioEx)
            {
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                      " Exception writing to report file. " + l_ioEx,
                                                      LoggableInterface.C_SEVERITY_FATAL));
            }
        }
        finally
        {

            if (i_reloadBC && l_status.equals(BatchConstants.C_BATCH_STATUS_COMPLETED))
            {
                l_status = doBusinessConfigReload();
            }

            if (i_rptFile != null)
            {
                try
                {
                    i_rptFile.write(("C".equals(l_status) ? "000001SUCCESS" : "000000FAILED"));
                    i_rptFile.flush();
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10020,
                                        this,
                                        C_METHOD_doJobProcessing + " closing report file ");
                    }                    
                    i_rptFile.close();
                }
                catch (IOException e)
                {
                    i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing
                            + " Exception closing report file. " + e, LoggableInterface.C_SEVERITY_ERROR));
                }
            }

            // Depending on outcome (exceptions etc) an intelligent desicion of the over-all
            // outcome is made. If success, "C" is passed and if failure "F" is passed.
            this.close(l_status);

            i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " " + C_METHOD_doJobProcessing +
                                                  " Synchronisation Batch terminating with status = " +
                                                  l_status + ".",
                                                  LoggableInterface.C_SEVERITY_INFO));

            // Notify that the job has finished.
            if ("C".equals(l_status))
            {
                l_jStatus = JobStatus.C_JOB_EXIT_STATUS_SUCCESS;
            }
            else
            {
                l_jStatus = JobStatus.C_JOB_EXIT_STATUS_FAILURE;
            }

            super.finishDone(l_jStatus);
        }
    }

    
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_setBusinessConfigReload = "setBusinessConfigReload";

    /**
     * This method can be used to enable/disable the automatic BC reload.
     * @param p_reload parameter uesd to enable/disable the reload of BC.
     */
    void setBusinessConfigReload(boolean p_reload)
    {


        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10500,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_setBusinessConfigReload);
        }
        i_reloadBC = p_reload;


        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10590,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_setBusinessConfigReload);
        }
    }  
    
    /**
     * This method will insert a control record into BACO.
     * @throws PpasServiceException when encoutering a problem adding a control record to the database
     */
    private void init() throws PpasServiceException
    {
        // Create a record for the batch job control table (BACO).
        BatchJobData l_batchJobData = new BatchJobData();

        l_batchJobData.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_SYNCH); // Passed to constructor
        l_batchJobData.setExecutionDateTime(i_executionDateTime.toString()); // Now
        l_batchJobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS); // 'I' (In progress)
        l_batchJobData.setJsJobId(i_jsJobId); // Passed to constructor
        l_batchJobData.setSubJobCnt("-1"); // has no sub-jobs
        l_batchJobData.setBatchDate(this.i_fileDate); // Extracted from filename
        l_batchJobData.setFileSeqNo(this.i_fileSeq); // Extracted from filename
        l_batchJobData.setNoOfRejectedRec("0"); // '0' (records not counted)
        l_batchJobData.setNoOfSuccessRec("0"); // '0' (records not counted)
        l_batchJobData.setOpId(super.getSubmitterOpid()); // from super-class

        // Insert the record into BACO
        try
        {
            i_ppasBatchControlService.addJobDetails(null, l_batchJobData, i_isApiTimeout);
        }
        catch (PpasServiceException e)
        {
            i_logger.logMessage(e);
            i_logger.logMessage(new LoggableEvent("SynchronisationBatch:init:"
                                                  + " Exception adding job control record. "
                                                  + l_batchJobData + ".",
                                                  LoggableInterface.C_SEVERITY_ERROR));
            throw e;
        }
        i_controlRecord = true;
    }

    /**
     * This method will update the control record with the final status of this batch run.
     * @param p_status Status will be either 'C' (Completed) or 'F' (Failed)
     */
    private void close(String p_status)
    {
        if (i_controlRecord)
        {
            BatchJobData l_batchJobData = new BatchJobData();
            l_batchJobData.setExecutionDateTime(i_executionDateTime.toString()); // Key-value
            l_batchJobData.setJsJobId(i_jsJobId); // Key-value
            l_batchJobData.setBatchJobStatus(p_status); // Final status
            if ("C".equals(p_status))
            {
                l_batchJobData.setNoOfRejectedRec("0"); // to indicate success
                l_batchJobData.setNoOfSuccessRec("1"); // to indicate success
            }
            else
            {
                l_batchJobData.setNoOfRejectedRec("1"); // to indicate failure
                l_batchJobData.setNoOfSuccessRec("0"); // to indicate failure
            }

            try
            {
                i_ppasBatchControlService.updateJobDetails(null, l_batchJobData, i_isApiTimeout);
            }
            catch (PpasServiceException e)
            {
                i_logger.logMessage(e);

                i_logger.logMessage(new LoggableEvent("SynchronisationBatch:close:"
                                                      + " Exception closing job control record. "
                                                      + l_batchJobData + ".",
                                                      LoggableInterface.C_SEVERITY_ERROR));
            }
        }
    }

    /**
     * Reports a BatchInvalidParameterException to report file error code and reports.
     * @param p_ex the exception raised during parsing.
     * @throws IOException when encountering a problem writing to the report file
     */
    private void reportBatchParamException(BatchInvalidParameterException p_ex) throws IOException
    {
        StringBuffer l_errorLine   = new StringBuffer();
        Map          l_dataMap     = p_ex.getAllData();
        String       l_errorCode   = (String)l_dataMap.get("error_code");
        String       l_configType  = (String)l_dataMap.get("config_type");
        String       l_id          = (String)l_dataMap.get("identifier");
        String       l_currency    = (String)l_dataMap.get("currency_code");
        String       l_startDate   = (String)l_dataMap.get("start_date");

        l_errorLine.append(l_errorCode + " ");
        l_errorLine.append(l_configType + " ");
        l_errorLine.append(l_id + " ");
        l_errorLine.append(l_currency + " ");
        l_errorLine.append((l_startDate != null  ?  l_startDate : ""));
        l_errorLine.append("\n");

        i_rptFile.write(l_errorLine.toString());
    }

    /**
     * Translates XML ParseException to report file error code and reports.
     * @param p_ex the exception raised during parsing.
     * @throws IOException when encountering a problem writing to the report file
     */
    private void reportParseException(ParseException p_ex) throws IOException
    {
        StringBuffer l_errorLine = new StringBuffer();

        if (p_ex.getSourceException() instanceof SAXParseException)
        {
            SAXParseException l_saxParseEx = (SAXParseException)p_ex.getSourceException();

            if (l_saxParseEx.getMessage().indexOf("-valid") != -1)
            {
                l_errorLine.append("06");
            }
            else
            {
                l_errorLine.append("01");
            }

            l_errorLine.append(" parse error ");
            l_errorLine.append("Line=" + l_saxParseEx.getLineNumber() + " ");
            int l_beginIx = l_saxParseEx.getMessage().indexOf("cvc-");
            l_beginIx = l_saxParseEx.getMessage().indexOf(":", l_beginIx) + 1;
            l_errorLine.append(l_saxParseEx.getMessage().substring(l_beginIx).trim());
        }
        else
        {
            l_errorLine.append("01");
            l_errorLine.append(" parse error ");
            l_errorLine.append(p_ex.getMessage());
        }
        l_errorLine.append("\n");
        i_rptFile.write(l_errorLine.toString());
    }

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getFileSequenceNumber = "getFileSequenceNumber";

    /**
     * Returns a file sequence number which is obtained from the database.
     * @return a file sequence number obtained from the database.
     */
    private String getFileSequenceNumber()
    {
        String l_seqNumberStr = null;
        StringBuffer l_tmpSeqNumber = null;
        long l_seqNumber = 0;
        long l_timeout = 0;
        String l_batchJobType = null;
        PpasDate l_currentDate = null;
        PpasDailySequencesService l_ppasDailySequencesService = null;
        PpasRequest l_request = new PpasRequest(new PpasSession());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10440,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getFileSequenceNumber);
        }

        l_currentDate = DatePatch.getDateToday();
        l_batchJobType = BatchConstants.C_JOB_TYPE_BATCH_SYNCH;
        l_ppasDailySequencesService = new PpasDailySequencesService(l_request, i_logger, i_context);

        try
        {
            l_seqNumber = l_ppasDailySequencesService.getNextSequenceNumber(l_request,
                                                                            l_timeout,
                                                                            l_batchJobType,
                                                                            l_currentDate);
            l_seqNumberStr = String.valueOf(l_seqNumber);
            l_tmpSeqNumber = new StringBuffer();
            for (int i = 0; i < C_LENGTH_FILE_SEQUENCE_NUMBER - l_seqNumberStr.length(); i++)
            {
                l_tmpSeqNumber.append("0");
            }
            l_tmpSeqNumber.append(l_seqNumberStr);
            l_seqNumberStr = l_tmpSeqNumber.toString();
        }
        catch (PpasServiceException l_ppasServExe)
        {
            i_logger.logMessage(l_ppasServExe);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10460,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getFileSequenceNumber);
        }
        return l_seqNumberStr;
    }
    
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_doBusinessConfigReload = "doBusinessConfigReload";

    /**
     * This method will do business config cache reload on the servers that are configured to do it.
     * It will also write to the report file if any reload fails.
     * @return The status.
     */
    private String doBusinessConfigReload()
    {
        RemoteMngMthRequest l_remoteRequest   = null;
        ArrayList           l_reloadResults   = null;
        String              l_status          = null;
        boolean             l_reloadSucceeded = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10500,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_doBusinessConfigReload);
        }

        l_reloadResults = i_remoteInstrClient.doRemoteBusinessConfigReload();

        for (int i = 0; i < l_reloadResults.size(); i++)
        {
            l_remoteRequest = (RemoteMngMthRequest)l_reloadResults.get(i);
            if (!l_remoteRequest.isSuccess())
            {               
                l_reloadSucceeded = false;
            }
        }

        if (!l_reloadSucceeded && i_rptFile != null)
        {     
            try
            {
                i_rptFile.write("98 The AIR Config Synchronisation was successfully completed," +
                                "but the Business Config Cache reload failed. See log files for details\n");
            }
            catch (IOException e1)
            {
                i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + 
                                                      " " + 
                                                      C_METHOD_doBusinessConfigReload+ 
                                                      " Exception writing to report file. " +
                                                      e1, 
                                                      LoggableInterface.C_SEVERITY_FATAL));
            }
        }

        l_status = (l_reloadSucceeded ? BatchConstants.C_BATCH_STATUS_COMPLETED : 
                                        BatchConstants.C_BATCH_STATUS_FAILED);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10590,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_doBusinessConfigReload);
        }
        return l_status;
    }
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_validatePromotionConfig = "validatePromotionConfig";

    /**
     * Validate values for Next Tier Promotion Plan Id and Division Table Id 
     * against Promotion Plan and Division records.
     * @param p_promotionConfigFromFile
     * @param p_divisionConfigFromFile
     * @throws BatchInvalidParameterException
     */
    private void validatePromotionConfig(Vector p_promotionConfigFromFile, Vector p_divisionConfigFromFile)
        throws BatchInvalidParameterException
    {
        PromotionSynchData l_promotionData = null;
        DivisionSynchData l_divisionData = null;
        HashMap l_promHM = new HashMap();
        HashMap l_divisionHM = new HashMap();

        // Load configured Promotion Id's.
        for (int l_record_index = 0; l_record_index < p_promotionConfigFromFile.size(); l_record_index++)
        {
            l_promotionData = (PromotionSynchData) p_promotionConfigFromFile.elementAt(l_record_index);
            l_promHM.put(l_promotionData.getPromotion(), " ");
        }

        // Load configured Division Table Id's.
        for (int l_record_index = 0; l_record_index < p_divisionConfigFromFile.size(); l_record_index++)
        {
            l_divisionData = (DivisionSynchData) p_divisionConfigFromFile.elementAt(l_record_index);
            l_divisionHM.put(l_divisionData.getDivisionId(), " ");
        }
 
        // Check values against configuration records. 
        for (int l_record_index = 0; l_record_index < p_promotionConfigFromFile.size(); l_record_index++)
        {
            l_promotionData = (PromotionSynchData) p_promotionConfigFromFile.elementAt(l_record_index);
            
            // Validate the Next Tier Promotion Id against the configured Promotion Plans.
            if ((l_promHM.get(l_promotionData.getNextTier()) == null) &&
                (!l_promotionData.getNextTier().equals(PromotionSynchData.C_SYNCH_NULL_STRING)))
            {
                BatchInvalidParameterException l_bipEx =
                    new BatchInvalidParameterException(C_CLASS_NAME,
                                                       C_METHOD_validatePromotionConfig,
                                                       10000,
                                                       this,
                                                       null,
                                                       0,
                                                       BatchKey.get().batchInvalidNextTierPromotionId(
                                                                             l_promotionData.getNextTier(),
                                                                             l_promotionData.getPromotion()));
                HashMap l_dataMap = new HashMap();
                l_dataMap.put("error_code",    "04");
                l_dataMap.put("config_type",   "promotion plan");
                l_dataMap.put("identifier",    l_promotionData.getPromotion());
                l_dataMap.put("currency_code", l_promotionData.getCurrency());
                l_dataMap.put("start_date",    l_promotionData.getStartDate());
                l_bipEx.setAllData(l_dataMap);
                throw l_bipEx;
            }

            // Validate the Division Table Id against the configured Divisions. 
            if ((l_divisionHM.get(l_promotionData.getDivisionId()) == null) &&
                (!l_promotionData.getDivisionId().equals(PromotionSynchData.C_SYNCH_NULL_STRING)))
            {
                BatchInvalidParameterException l_bipEx =
                    new BatchInvalidParameterException(C_CLASS_NAME,
                                                       C_METHOD_validatePromotionConfig,
                                                       11000,
                                                       this,
                                                       null,
                                                       0,
                                                       BatchKey.get().batchInvalidDivisionId(
                                                                           l_promotionData.getDivisionId(),
                                                                           l_promotionData.getPromotion()));
                HashMap l_dataMap = new HashMap();
                l_dataMap.put("error_code",    "05");
                l_dataMap.put("config_type",   "promotion plan");
                l_dataMap.put("identifier",    l_promotionData.getPromotion());
                l_dataMap.put("currency_code", l_promotionData.getCurrency());
                l_dataMap.put("start_date",    l_promotionData.getStartDate());
                l_bipEx.setAllData(l_dataMap);
                throw l_bipEx;
            }
        }
    }
}

