////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME :    SubSegmentationChgBatchProcessorUT.java
//      DATE      :    10-July-2007
//      AUTHOR    :    Ian James
//      REFERENCE :    PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT :    WM-data 2007
//
//      DESCRIPTION :
//
/////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
/////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                           | REFERENCE
//----------+------------+---------------------------------------+---------------
// 07/08/06 | M Erskine  | Formatting and debugging.             | PpacLon#2479/9483
// //////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit Test for the <code>SubSegmentationChgBatchProcessor</code> class.
 */
public class SubSegmentationChgBatchProcessorUT extends BatchTestCaseTT
{
    // ------------------------------------------------------------------------
    // Private constants
    // ------------------------------------------------------------------------
    /** Class name. Value is {@value}. */
    private static final String C_CLASS_NAME = "SubSegmentationChgBatchProcessorUT";

    // ------------------------------------------------------------------------
    // Constructors
    // ------------------------------------------------------------------------
    /**
     * Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public SubSegmentationChgBatchProcessorUT(String p_title)
    {
        super(p_title, "batch_ssc");
    }

    // ------------------------------------------------------------------------
    // Public instance methods
    // ------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testConstructor = "testConstructor";

    /**
     * Test to construct a <code>SubSegmentationChgBatchProcessor</code> instance.
     */
    public void testConstructor()
    {
        SubSegmentationChgBatchProcessor l_subSegChgBatchProcessor = null;
        SizedQueue l_inQueue = null;
        SizedQueue l_outQueue = null;

        try
        {
            l_inQueue = new SizedQueue("SubSegmentationChgBatchProcessorUT indata queue", 10, null);
            l_outQueue = new SizedQueue("SubSegmentationChgBatchProcessorUT output queue", 10, null);
        }
        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        l_subSegChgBatchProcessor = new SubSegmentationChgBatchProcessor(c_ppasContext,
                                                                         c_logger,
                                                                         null,
                                                                         l_inQueue,
                                                                         l_outQueue,
                                                                         null,
                                                                         c_properties);

        assertNotNull("Failed to create a SubSegmentationChgBatchProcessor instance.", l_subSegChgBatchProcessor);

        say(C_CLASS_NAME + C_METHOD_testConstructor + "-- Completed.");
    }

    // ------------------------------------------------------------------------
    // Public static methods
    // ------------------------------------------------------------------------
    /**
     * Define test suite. This unit test uses a standard JUnit method to derive a list of test cases from the
     * class.
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(SubSegmentationChgBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }

} // End of public class SubSegmentationChgBatchProcessorUT

