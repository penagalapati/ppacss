////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchMsg.java
//      DATE            :       08-Aug-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Batch keys resource bundle loader.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//26/07/06  | Chris      | Added                           | PpacLon#2329/9464
//          | Harrison   | C_KEY_BATCH_INVALID_END_DATE    | PRD_ASCS_GEN_CA_096
//          |            | C_KEY_BATCH_INVALID_NEXT_TIER_PROMOTION_ID 
//          |            | C_KEY_BATCH_INVALID_SYFG_PARAMETER
//----------+------------+---------------------------------+-------------------
//20/03/07  | Lars L.    | Improved Synch. batch error     | PpacLon#2076/7827
//          |            | handling.                       |
//          |            | One key is removed:             |
//          |            | C_KEY_BATCH_INVALID_DIVISION_TABLE_ID.
//          |            | Two new keys are added:         |
//          |            | C_KEY_BATCH_INVALID_DEDICATED_ACCOUNT and
//          |            | C_KEY_BATCH_INVALID_EXPIRY_DATE.|
//----------+---------------+------------------------------+-------------------
//26/04/07  | Lars L.    | One key is removed:             | PpacLon#3033/11279
//          |            | C_KEY_BATCH_INVALID_FILENAME_EXTENSION
//          |            | Two new keys are added:         |
//          |            | C_KEY_BATCH_INVALID_INDATA_FILENAME
//          |            | C_KEY_BATCH_ILLEGAL_INPUT_FILENAME_EXT
//----------+------------+---------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.exceptions;

import java.io.IOException;

import com.slb.sema.ppas.common.exceptions.XmlExceptionBundle;

/**
 * This class implements the resource bundle containing the text messages
 * associated with Batch exceptions and contains the keys into that
 * resource bundle.
 */
public class BatchMsg extends XmlExceptionBundle
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /**
     * Define the name of the file which will contain all the message texts
     * to be associated with service exceptions.
     */
    private static final String C_BATCH_MSG_FILE = "batch_msg.xml";

    /** 
     * Key into the message formats file describing the format of the output
     * for all service related exceptions.
     */
    public static final String C_BATCH_MSG_FMT_KEY = "ppas_format";


    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    /**
     * Name of the message texts file containing the messages associated with
     * all service type exceptions.
     */
    public static final String C_BATCH_MSG_BASENAME =
                         "com.slb.sema.ppas.batch.exceptions.BatchMsg";


    /* BATCH Exception Keys */
    /**
     *  A input parameter to the batch program is invalid or missing.
     */
    public static final String C_KEY_BATCH_INVALID_PARAMETER = 
                                    "BATCH_INVALID_PARAMETER";

    /** Expected length [{0}] does not equal actual length [{1}] of record. */ 
    public static final String C_KEY_BATCH_WRONG_RECORD_LENGTH = 
                                    "BATCH_WRONG_RECORD_LENGTH";

    /** Error attempting to parse MSISDN {0}. */ 
    public static final String C_KEY_BATCH_PARSING_OF_MSISDN_FAILED = 
                                    "BATCH_PARSING_OF_MSISDN_FAILED";
    
    /** Unexpected NON-AlphaNumeric String {0}. */ 
    public static final String C_KEY_BATCH_NOT_ALPHANUMERIC = 
                                    "BATCH_NOT_ALPHANUMERIC";
    
    /** Cannot create a date from {0} pattern: {1}. */
    public static final String C_KEY_BATCH_DATE_FORMAT_ERROR = 
                                    "BATCH_DATE_FORMAT_ERROR";
    
    /** Failed to get the account data for MSISDN {0}. */
    public static final String C_KEY_BATCH_FAILED_TO_GET_ACCOUNT_DATA = 
                                    "BATCH_FAILED_TO_GET_ACCOUNT_DATA";
    
    /** Promotion plan id [{0}] not recognised.*/
    public static final String C_KEY_BATCH_PROM_ID_NOT_RECOGNISED = 
                                    "BATCH_PROM_ID_NOT_RECOGNISED";
    
    /** Currency code [{0}] in {1} configuration not recognised.*/
    public static final String C_KEY_BATCH_CURRENCY_NOT_RECOGNISED = 
                                    "BATCH_CURRENCY_NOT_RECOGNISED";
    
    /** Service Class [{0}] not recognised. */
    public static final String C_KEY_BATCH_SERVICE_CLASS_NOT_RECOGNISED =
                                    "BATCH_SERVICE_CLASS_NOT_RECOGNISED";
    
    /** Division ID [{0}] not recognised. */
    public static final String C_KEY_BATCH_INVALID_DIVISION_ID =
                                    "BATCH_INVALID_DIVISION_ID";
    
    /** Invalid dedicated account. */
    public static final String C_KEY_BATCH_INVALID_DEDICATED_ACCOUNT =
                                    "BATCH_INVALID_DEDICATED_ACCOUNT";

    /** The End Date must not be before the Start Date. */
    public static final String C_KEY_BATCH_INVALID_END_DATE =
                                    "BATCH_INVALID_END_DATE";
    /** Failed to insert a new record into the BACO_BATCH_CONTROL table. */
    public static final String C_KEY_BATCH_FAILED_INSERT_INTO_CONTROL_TABLE =
                                    "BATCH_FAILED_INSERT_INTO_CONTROL_TABLE";

    /** Next Tier Promotion ID [{0}] not recognised. */
    public static final String C_KEY_BATCH_INVALID_NEXT_TIER_PROMOTION_ID =
                                    "BATCH_INVALID_NEXT_TIER_PROMOTION_ID";
    
    /** The system configuration parameter [{0}], [{1}] is either missing from the Syfg table or has an invalid value. */
    public static final String C_KEY_BATCH_INVALID_SYFG_PARAMETER =
                                    "BATCH_INVALID_SYFG_PARAMETER";
    
    
    /** Failed to insert a new record into the BACO_BATCH_CONTROL table. */
    public static final String C_KEY_BATCH_FAILED_GET_CONTROL_DATA =
                                    "BATCH_FAILED_GET_CONTROL_DATA";

    /** Failed to insert a new record into the BACO_BATCH_CONTROL table. */
    public static final String C_KEY_BATCH_FAILED_UPDATE_CONTROL_DATA =
                                    "BATCH_FAILED_UPDATE_CONTROL_DATA";

    /** A numerical batch property is non-numerical. */
    public static final String C_KEY_BATCH_NON_NUMERICAL_PROP =
                                    "BATCH_NON_NUMERICAL_PROP";

    /** A numerical batch property is too big. */
    public static final String C_KEY_BATCH_TOO_BIG_VALUE_PROP =
                                    "BATCH_TOO_BIG_VALUE_PROP";

    /** A numerical batch property is too low. */
    public static final String C_KEY_BATCH_TOO_LOW_VALUE_PROP =
                                    "BATCH_TOO_LOW_VALUE_PROP";

    /** A mandatory batch property is missing. */
    public static final String C_KEY_BATCH_MISSING_MANDATORY_PROP =
                                    "BATCH_MISSING_MANDATORY_PROP";

    /** A numerical batch parameter is non-numerical. */
    public static final String C_KEY_BATCH_NON_NUMERICAL_PARAM =
                                    "BATCH_NON_NUMERICAL_PARAM";

    /** A numerical batch parameter is too big. */
    public static final String C_KEY_BATCH_TOO_BIG_VALUE_PARAM =
                                    "BATCH_TOO_BIG_VALUE_PARAM";

    /** A mandatory batch parameter is missing. */
    public static final String C_KEY_BATCH_MISSING_MANDATORY_PARAM =
                                    "BATCH_MISSING_MANDATORY_PARAM";

    /** The given input data filename has an invalid prefix. */
    public static final String C_KEY_BATCH_INVALID_FILENAME_PREFIX =
                                    "BATCH_INVALID_FILENAME_PREFIX";

    /** The given input data filename has an invalid or missing extension. */
    public static final String C_KEY_BATCH_INVALID_INDATA_FILENAME =
                                    "BATCH_INVALID_INDATA_FILENAME";

    /** The given input data filename has an invalid body. */
    public static final String C_KEY_BATCH_INVALID_FILENAME_BODY =
                                    "BATCH_INVALID_FILENAME_BODY";

    /** The given input data file is invalid. */
    public static final String C_KEY_BATCH_INVALID_INDATA_FILE =
                                    "BATCH_INVALID_INDATA_FILE";

    /** The required recovery file is invalid. */
    public static final String C_KEY_BATCH_INVALID_RECOVERY_FILE =
                                    "BATCH_INVALID_RECOVERY_FILE";

    /** Failed to change the input data file extension to the "in progress" extension. */
    public static final String C_KEY_BATCH_FAILED_RENAME_INDATA_FILE =
                                    "BATCH_FAILED_RENAME_INDATA_FILE";

    /** Failed to change the input data file extension to the "in progress" extension. */
    public static final String C_KEY_BATCH_FAILED_TO_CREATE_OR_OPEN_FILE =
                                    "BATCH_FAILED_TO_CREATE_OR_OPEN_FILE";

    /** Failed to change the input data file extension to the "in progress" extension. */
    public static final String C_KEY_BATCH_FAILED_TO_READ_FROM_FILE =
                                    "BATCH_FAILED_TO_READ_FROM_FILE";

    /** Failed to change the input data file extension to the "in progress" extension. */
    public static final String C_KEY_BATCH_FAILED_TO_CLOSE_FILE =
                                    "BATCH_FAILED_TO_CLOSE_FILE";

    /** An invalid (negative) relative expiry date is found. */
    public static final String C_KEY_BATCH_INVALID_EXPIRY_DATE =
                                    "BATCH_INVALID_EXPIRY_DATE";

    /** An illegal input data filename extension is used when a recovery file exists. */
    public static final String C_KEY_BATCH_ILLEGAL_INPUT_FILENAME_EXT =
                                    "BATCH_ILLEGAL_INPUT_FILENAME_EXT";

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs an Msi message resource bundle object.
     * @throws IOException when unable to get resource bundle as stream
     */
    public BatchMsg() throws IOException
    {
        super(BatchMsg.class.getResourceAsStream(C_BATCH_MSG_FILE));
    }

} // end public class BatchMsg
