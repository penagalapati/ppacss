////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchWriter
//      DATE            :       23-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars Lundberg | Method updateStatus() is         | PpacLon#3033/11279
//          |               | modified: It now uses the        |
//          |               | BatchJobControlData class        |
//          |               | instead of the BatchJobData class|
//----------+-----------+---+----------------------------------+----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * Class responsible for writing the outcome of each 
 * attempt to load miscellaneous data. For each error, a line
 * will be written to the report file. For each attempt, 
 * either successful or failed, a line will be written
 * to a recovery file.
 */
public class MiscBatchWriter extends BatchWriter
{   
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 
    
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscBatchWriter";

    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY = 
                                "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - missing property for Error File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_ERROR_FILE_DIRECTORY = 
                                "Property for ERROR_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Input File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY = 
                                "Property for INPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY = 
                                "Property for RECOVERY_FILE_DIRECTORY not found";
    
    /** Logger printout - missing value in Map for Input File Name.  Value is {@value}. */
    private static final String C_NOT_FOUND_INPUT_FILENAME_KEY = 
                                "Missing value in the Map for INPUT_FILE_NAME";
    
    /** Logger printout - cannot open report- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_REPORT_OR_RECOVERY_FILE = 
                                "Cannot open report- or recovery-file";


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
   
    /** Gets how many records to be written. Gets from <code>p_properties</code>. */
    private int i_interval  = 0;
    
    /** Number of successfully processed records. */
    private int i_success   = 0;
    /** Total number of records that has been processed. */
    private int i_processed = 0;    

    /** Directory for the report files. */
    private String i_reportFileDirectory    = null;
    /** Directory for the input files. */
    private String i_inputFileDirectory    = null;
    /** Directory for recovery files. */
    private String i_recoveryFileDirectory = null;
    /** Input file name. */
    private String i_inputFileName         = null;
    
    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord          = true;
    
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;
    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;



    /**
     * Constructor.
     * @param p_ppasContext The PPAS context
     * @param p_logger The logger
     * @param p_controller The controller
     * @param p_parameters The parameters
     * @param p_outQueue The out queue
     * @param p_properties The properties
     */
    public MiscBatchWriter(
        PpasContext     p_ppasContext,
        Logger          p_logger,
        BatchController p_controller,
        Map             p_parameters,
        SizedQueue      p_outQueue,
        PpasProperties  p_properties)
    {
        super(
            p_ppasContext, 
            p_logger, 
            p_controller, 
            p_parameters, 
            p_outQueue, 
            p_properties);

        String l_reportFileFullPath   = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if ( this.getProperties() )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10400,
                    this,
                    "Got all properties" );
            }
           
            // Get the full path filename for the report-file 
            l_reportFileFullPath   = generateFileName( this.i_reportFileDirectory,
                                                       this.i_inputFileName,
                                                       BatchConstants.C_EXTENSION_TMP_FILE );
            super.i_recoveryFileName = generateFileName( this.i_recoveryFileDirectory,
                                                         this.i_inputFileName,
                                                         BatchConstants.C_EXTENSION_RECOVERY_FILE );
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10500,
                    this,
                    "reportFileFullPath= " + l_reportFileFullPath + 
                    ", recoveryFileFullPath= " + super.i_recoveryFileName );
            }       
            try
            {
                super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFileFullPath));
                super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
            }
            catch (IOException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( 
                                            C_CANNOT_OPEN_REPORT_OR_RECOVERY_FILE,
                                            LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10600,
                        this,
                        C_PROPERTY_NOT_FOUND_ERROR_FILE_DIRECTORY + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10700,
                        this,
                        "Finally - after openFile..." );
                }
            }    
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10800,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

        return;
    } // Constructor


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";    
    /** 
     * Writes to files. 
     * If error occurs in current record, writes to report file.
     * Always writes to record file.
     * @param p_record The current data record being processed.
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeRecord(com.slb.sema.ppas.common.dataclass.BatchRecordData)
     */
    protected void writeRecord( BatchRecordData p_record ) 
        throws IOException, PpasServiceException
    {        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                20000,
                this,
                BatchConstants.C_ENTERING + C_METHOD_writeRecord );
        }
        
        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = p_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = p_record.getNumberOfErrorRecords();
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                "noSuccessfully processed="+ this.i_numberOfSuccessfullyProcessedRecords+
                " noFailured="+this.i_numberOfFaultyRecords);
        }

        // Check if error occurred and write to report file.
        if (p_record.getErrorLine() != null)
        {
            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, p_record.getErrorLine() );
            super.i_errors++;
        }
        else
        {
            i_success++;
        }

        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if ( p_record.getFailureStatus())
        { 
            this.i_numberOfFaultyRecords--;
        }

        i_processed++;

        // Always write to recovery file.
        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, p_record.getRecoveryLine() );

        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }
          
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                20100,
                this,
                BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }
        
        return;
    } // End of writeRecord(.)


    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws PpasServiceException
    {
        // Flush all currently open files.
        super.flushFiles();

        // Update batch job control info record counters.
        BatchJobControlData l_batchJobControlData = i_controller.getKeyedBatchJobControlData();
        l_batchJobControlData.setNoOfRejectedRecs(i_errors+i_numberOfFaultyRecords);
        l_batchJobControlData.setNoOfSuccessfullyRecs(i_success+i_numberOfSuccessfullyProcessedRecords);
        
        super.updateControlInfo(l_batchJobControlData);
    }

    /** Writes out the trailer record.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(
                l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors += i_numberOfFaultyRecords;

        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * This method tries to read the properies for :
     * <br>Table update frequency
     * <br>directory for the input file
     * <br>directory for the report file
     * <br>directory for the recovery file
     * @return false if one of the properties could not be retrieved
     */
    private boolean getProperties()
    {
        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        boolean l_returnValue = true;  // Assume all found
        
        // Get "input filename" from the Map
        this.i_inputFileName = (String)this.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME); 
        if ( this.i_inputFileName == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( 
                                        C_NOT_FOUND_INPUT_FILENAME_KEY,
                                        LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    30000,
                    this,
                    C_NOT_FOUND_INPUT_FILENAME_KEY + " " + C_METHOD_getProperties );
            }
        }

        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( 
                                        C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                        LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    30100,
                    this,
                    C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY + " " + C_METHOD_getProperties );
            }
        }

        // Get the "Input File Directory"
        i_inputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        if ( i_inputFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( 
                                        C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY,
                                        LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    30200,
                    this,
                    C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
        
        // Get the "Recovery File Directory"
        i_recoveryFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        if ( i_recoveryFileDirectory == null )
        {
            l_returnValue = false; 
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( 
                                        C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY,
                                        LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    30300,
                    this,
                    C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        // Get the "Error File Directory"
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        if ( i_reportFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( 
                                        C_PROPERTY_NOT_FOUND_ERROR_FILE_DIRECTORY,
                                        LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    30400,
                    this,
                    C_PROPERTY_NOT_FOUND_ERROR_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
                
        return l_returnValue;
        
     } // End of method getProperties()

} // End of MiscBatchWriter
