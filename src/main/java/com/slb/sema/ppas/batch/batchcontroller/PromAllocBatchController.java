////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromAllocBatchController
//      DATE            :       9-Aug-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.PromAllocBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.PromAllocBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.PromAllocBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;



/**
 * This class is the main entry point for starting Promotion Plan Allocation Provisioning.
 * It is instantiated by the Job Scheduler.
 */
public class PromAllocBatchController extends BatchController
{
    //---------------------------------------------------------------
    //  Class level constant
    //  -------------------------------------------------------------

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME          = "PromAllocBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_pro";

    /** Control block table. */
    private static final String C_MASTER_TABLE_NAME   = "BACO_BATCH_CONTROL";
    
    /** Control block table for sub jobs. */
    private static final String C_SUB_TABLE_NAME      = "BSCO_BATCH_SUB_CONTROL";

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    
    /** Sub job id, used to update control record in database. */
    private String       i_masterJsJobId        = null;
    
    /** Execution date and time for this batch. */
    private PpasDateTime i_executionDateAndTime = null;
    
    /** Sub job id. */
    private String       i_subJobId             = null;
    
//    /** Recovery flag - from paramters. */
//    private String       i_recoveryFlag         = null;
    
    /** Session used for constructing PpasRequests. */
    private PpasSession  i_ppasSession          = null;
    
    /** PpasRequest required by the isapi. */
    private PpasRequest  i_ppasRequest          = null;

    
    
    /**
     * @param p_context      A <code>PpasContext</code> object.
     * @param p_jobType      The type of job (e.g. BatchInstall).
     * @param p_jobId        The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_parameters   A Map holding parameters to use when executing this job. 
     *                       Recovery   - "yes" or "no". Indicating recovery or not.
     *                       StartRange - Lowest customer id for provisioning.
     *                       EndRange   - Highest customer to provisioning.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     */
    public PromAllocBatchController( PpasContext  p_context,
                                     String       p_jobType,
                                     String       p_jobId,
                                     String       p_jobRunnerName,
                                     Map          p_parameters )
        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_parameters);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10100,
                             this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        // The i_ppasRequest used in addControlInformation()
        i_ppasSession  = new PpasSession();
        i_ppasSession.setContext(p_context);
        i_ppasRequest  = new PpasRequest(i_ppasSession);

        this.init();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             10200,
                             this,
                             BatchConstants.C_CONSTRUCTED );
        }

        return;

    } // End of constructor PromAllocBatchController(.....)



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             20100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);        
        super.init();


        // Retrieve parameters from the Map
        i_masterJsJobId        = (String)super.i_params.get(BatchConstants.C_KEY_MASTER_JS_JOB_ID);

        i_executionDateAndTime = new PpasDateTime((String)super.i_params.get(
                                                                BatchConstants.C_KEY_EXECUTION_DATE_TIME));
        i_subJobId             = (String)super.i_params.get(BatchConstants.C_KEY_SUB_JOB_ID);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             20300,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_init+
                             " masterJsJobId=" + i_masterJsJobId +
                           "\n executionTime=" + i_executionDateAndTime.toString_yyyyMMdd_HHcmmcss());
        }

        return;

    } // End of init()





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * This method will instantiate the correct reader-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the reader-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a PromAllocBatchReader.
     */
    public BatchReader createReader()
    {
        PromAllocBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             30100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_createReader );
        }

        // Instantiate the relevant type of reader and pass reference to controller
        l_reader = new PromAllocBatchReader( super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_inQueue,
                                             super.i_params,
                                             super.i_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             30200,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_createReader );
        }

        return l_reader;

    } // End of createReader()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /**
     * This method will instantiate the correct writer-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the writer-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a PromAllocBatchWriter.
     * @throws IOException Could not open output files.
     */
    public BatchWriter createWriter() throws IOException
    {
        PromAllocBatchWriter l_writer = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             40100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        l_writer = new PromAllocBatchWriter( super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_params,
                                             super.i_outQueue,
                                             super.i_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             40200,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_createWriter );
        }

        return l_writer;

    } // End of createWriter()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a PromAllocBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        PromAllocBatchProcessor l_processor = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             50100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_createProcessor );
        }

        l_processor = new PromAllocBatchProcessor( super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_inQueue,
                                                   super.i_outQueue,
                                                   super.i_params,
                                                   super.i_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             50200,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_createProcessor );
        }
        return l_processor;

    } // End of createProcessor()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
     * This method is responsible to insert a record into table BACO using the defined
     * service method PpasBatchControlService.addControlInformation(). The method will
     * get a BatchJobData object and populate it with required parameters.
     * Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             60100,
                             this,
                             BatchConstants.C_CONSTRUCTING +
                             C_METHOD_addControlInfo + "DUMMY - NOT IMPLEMENTED FOR DB_driven batch!" );
        }
     
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             60200,
                             this,
                             BatchConstants.C_CONSTRUCTED + C_METHOD_addControlInfo );
        }
        
    } // End of addControlInformation()





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * This method call the service defined by PpasBatchControlService. In this case
     * for a filedriven  updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             70100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_updateStatus );
        }

        try
        {
            i_batchContService.updateSubJobStatus( i_ppasRequest, 
                                                   this.i_masterJsJobId,           // sub job type
                                                   this.i_executionDateAndTime,    // l_jobData 
                                                   this.i_subJobId,                // sub job id l_jobData
                                                   p_status,                       // passed parameter
                                                   C_MASTER_TABLE_NAME,
                                                   C_SUB_TABLE_NAME,
                                                   super.i_timeout );

        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 70200,
                                 this,
                                 C_METHOD_addControlInfo + " PpasServiceException from updateJobDetails" );
            }

            e.printStackTrace();
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             70300,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }
        return;

    } // End of updateStatus()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";        
    /**
     * This method will return a BatchSubJobData-object with the correct key-values set 
     * (MasterJobId, ExecutionDataAndTime and SubJobId).
     * The caller can then populate the record with data needed 
     * for the operation in question.
     * @return a record with a sub job data record.
     */
    public BatchSubJobData getKeyedControlRecord()
    {
        BatchSubJobData l_jobData = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             80100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_getKeyedControlRecord +
                             " ... prepare with the database keys" );
        }

        l_jobData = new BatchSubJobData();
        l_jobData.setMasterJsJobId(this.i_masterJsJobId);
        l_jobData.setExecutionDateTime(this.i_executionDateAndTime.toString());
        l_jobData.setSubJobId(this.i_subJobId);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             80200,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_getKeyedControlRecord +
                             " MasterId: [" + this.i_masterJsJobId + "]\n" +
                             " ExecutionDateAndTime: [" + this.i_executionDateAndTime + "]\n" +
                             " SubJobId: [" + this.i_subJobId + "]" );
        }
        return l_jobData;

    } // End of getKeyedControlRecord()



} // End of class PromAllocBatchController
