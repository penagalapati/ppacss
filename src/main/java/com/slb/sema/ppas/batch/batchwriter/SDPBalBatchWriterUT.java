///////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SDPBalBatchWriterUT.java 
//      DATE            :       2004-maj-24
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Unit test for SDPBalBatchWriter
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.SDPBalBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SDPBalBatchRecordData;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**Unit test for SubInstBatchWriter. */

public class SDPBalBatchWriterUT extends BatchTestCaseTT
{
    /** Reference to SubInstBatch controller object. */
    private static SDPBalBatchWriter c_sdpBalBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static SDPBalBatchController c_sdpBalBatchController;

    /**A short file name vid extension .DAT. */    
    private static final String C_FILE_NAME_DAT = "CHANGE_SDP_00100_02.DAT";
 
    /**The type of batch. */
    private static final String C_BATCH_TYPE    = "SDPBalBatchWriterUT";

    /**
     * The constructor for the class SDPBalBatchWriterUT.
     * @param p_name the name for this test class.
     */
    public SDPBalBatchWriterUT(String p_name)
    {
        super(p_name, "batch_sdp");
    }

    /**Tests getRecord(). */
    public void testGetRecord()
    {
        BatchRecordData l_dataRecord;

        super.beginOfTest("testGetRecord");

        try
        {
            l_dataRecord = c_sdpBalBatchWriter.getRecord();

            assertNotNull("Data Record is Null", l_dataRecord);
            assertTrue("Data Record is not of type SDPBalBatchRecordData", 
                    (l_dataRecord instanceof SDPBalBatchRecordData) );
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        super.endOfTest();
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(SDPBalBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        createContext();
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    /** Creates the Context suitable for this test class. */
    private void createContext()
    {
        SizedQueue l_outQueue = null;
        SDPBalBatchRecordData l_batchRecordData = new SDPBalBatchRecordData();
        
        if (c_sdpBalBatchWriter == null)     
        {
            try
            {
                l_outQueue = new SizedQueue("BatchWriter", 1, null);

                l_outQueue.addFirst(l_batchRecordData);
            }
            catch (SizedQueueInvalidParameterException e)
            {
                super.failedTestException(e);
            }
            catch (SizedQueueClosedForWritingException e)
            {
                super.failedTestException(e);
            }
            Hashtable l_params = new Hashtable();
            l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_FILE_NAME_DAT);
            l_params.put(BatchConstants.C_KEY_SERVICE_AREA, "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_LOCATION, "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_CLASS, "01");

            try
            {
                c_sdpBalBatchController = new SDPBalBatchController(super.c_ppasContext,
                                                                    C_BATCH_TYPE,
                                                                    getJsJobID(),
                                                                    "SDPBalBatchWriterUT",
                                                                    l_params);

                c_sdpBalBatchWriter = new SDPBalBatchWriter(super.c_ppasContext,
                                                            super.c_logger,
                                                            c_sdpBalBatchController,
                                                            l_params,
                                                            l_outQueue,
                                                            super.c_properties);

            }
            catch (RemoteException e)
            {
                super.failedTestException(e);
            }
            catch (PpasConfigException e)
            {
                super.failedTestException(e);
            }
            catch (IOException e)
            {
                super.failedTestException(e);
            }
            catch (PpasSqlException e)
            {
                super.failedTestException(e);
            }
        }
    }
}
