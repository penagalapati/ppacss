////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NPChBatchReader
//      DATE            :       10-August-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
// 13/03/07 | L. Lundberg   | Correction of a problem reported | PpacLon#2987/11149
//          |               | by the 'FindBugs' tool:          |
//          |               | "Null pointer dereference of     |
//          |               | l_record in method getRecord().  |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.NPChBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isil.batchisilservices.NPChDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class is responsible to validate and create NPChBatchRecordData that are put on the in-queue.
 * This batch combines the two techniques both reading input data from a file and also retrieving data
 * from the database.
 */
public class NPChBatchReader extends BatchLineFileReader
{
    // -----------------------------------------------------
    //  Class level constant
    // ----------------------------------------------------- 
    
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                       = "NPChBatchReader";
    
    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD             = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INCORRECT_USE_OF_WILDCARDS = "02";

    /** Field length of MSISDNto be installed. Value is {@value}). */
    private static final int    C_OLD_MSISDN_LENGTH                = 15;
    
    /** Field length of Master MSISDN if the first MSISDN was a subordinate one. Value is {@value}. */
    private static final int    C_NEW_MSISDN_LENGTH                = 15;

    /** Max record length, will be get a value in the constructor. */
    private static int          C_MAX_RECORD_LENGTH;

    /** Wildcard token for specifing old and new MSISDN. Value is {@value}. */
    public static final char    C_MSISDN_WILDCARD                   = 'X';

    /** Regular expression for MSISDN wildcard. Value is {@value}. */
    public static final String  C_PATTERN_MSISDN_WILDCARD           = "[X]";
    
    /** Regular expression for checking if a string is alphanumeric. Value is {@value}. */
    public static final String  C_PATTERN_MSISDN_NUMERIC_AND_WILDCARD  = "[X0-9]*";


    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS                 = 2;
    
    /** Index for the field old MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_OLD_MSISDN                       = 0;
    
    /** Index for the field new MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NEW_MSISDN                       = 1;

    /** Message for the log. */
    private static final String C_SERVICE_EXCEPTION_GET_RECORD =
        "PpasServiceException when getRecord() ";
    
    /** Message for the log. */
    private static final String C_SERVICE_EXCEPTION_REOPEN_ISAPI =
        "PpasServiceException when reopenIsapi()";

    /** Delimiter used in the key for recovery information. */
    private static final String C_DELIMITER_RECOVERY_KEY = " ";
 
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
       
    /** The BatchDataRecord to be sent into the Queue. */
    private NPChBatchRecordData  i_batchRecordDataFromFile = null;
    
    /** Old MSISDN, if wildcards are used then this is the start value in a range. */
    private String               i_oldMSISDN               = null;
    
    /** New MSISDN, if wildcards use the first characters as the new prefix. */
    private String               i_newMSISDN               = null;
    
    /** Reads from database to get SDP ID and CUST_ID. */
    private NPChDbBatchService   i_isApi                   = null;
    
    /** Database fetch size. */
    private int                  i_fetchSize               = 0;
    
    /** Flag to control that the is_api is only initalized once. */
    private boolean              i_firstTimeCall           = true;
    
    /** Flag to control that new parameters shall be sent to the isapi or not. */
    private boolean              i_cursorIsEmpty           = true;
    
    /** "Forever" loop until all records are read from the database. */
    private boolean              i_moreRowsToRead          = true;
    
    /** Flag to show if the read line has wildcards. */
    private boolean              i_wildcards               = false;
    
    /** Old MSISDN prefix. */
    private String               i_oldPrefix               = null;
    
    /** New MSISDN prefix. */
    private String               i_newPrefix               = null;

    /** A table with successfully processed wildcard-MSISDN. Used during recovery. */
    private Hashtable            i_alreadyProcessed        = null;

    /** Flag to catch the error if the oldMSISDN is not defined in database. */
    private int                  i_countFetch              = 0;
    
    
    /**
     * Constructor.
     * @param p_ppasContext PpasContext.
     * @param p_logger      Logger.
     * @param p_controller  BatchController call-back reference.
     * @param p_queue       In-queue.
     * @param p_parameters  Start parameters.
     * @param p_properties  Batch properties.
     */
    public NPChBatchReader(
        PpasContext     p_ppasContext,
        Logger          p_logger,
        BatchController p_controller,
        SizedQueue      p_queue,
        Map             p_parameters,
        PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }        
        
        NPChBatchReader.C_MAX_RECORD_LENGTH = C_OLD_MSISDN_LENGTH + C_NEW_MSISDN_LENGTH;
        this.getProperties();
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10303,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;

    } // End of constructor




    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * The MSISDNs in the indata file might have wildcards.
     * If wildcards are present this will generate a BatchRecordData element
     * for each expanded wildcard.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        //  The Db-component will be reused to return the result for several different select-criteria.
        //  If a record from the input file contains wildcards the following logic is executed:
        //  1) Verify that the number of wildcards is equal in new and old Msisdn and that they appear on 
        //     the end only.
        //  2) Exchange the wildcards in old Msisdn with �0� for queryParams[0] and with �9� for 
        //     queryParams[1].
        //     This corresponds to the maximal range for Msisdn�s that are subject to the number plan change
        //     process ordered in this record. (E.g. 0708123XXX  -> 0708123000 � 0708123999)
        //     If no wildcards are used, queryParams[0] and queryParams[1] will be the same, i.e. same as old
        //     Msisdn.

        String[]            l_queryParameters = null;  // sql paramters
        NPChBatchRecordData l_record          = null;  // from database isil-call

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10304,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        while ( i_moreRowsToRead )
        {
            if ( i_cursorIsEmpty )
            {
                // Get a new next line from input file
                i_batchRecordDataFromFile = null;

                if ( readNextLine() != null )
                {
                    i_cursorIsEmpty = false;

                    // Extract data from line
                    // The i_batchRecordDataFromFile is created and initialized with:
                    // filename, inputline, (errorline)
                    if ( extractLineAndValidate() )
                    {

                        // Record information OK - and has been added to a BatchRecordData in
                        // the method extractLineAndValidation. (i_batchRecordDataFromFile) 
                      
                        // Get some information from the database
                        l_queryParameters = new String[] {i_oldMSISDN};                        
                                                    
                        if ( i_firstTimeCall )
                        {
                            this.initializeIsapi( l_queryParameters );
                            i_firstTimeCall = false;
                        }
                        else
                        {                            
                            this.reopenIsapi( l_queryParameters );
                        }
                        
                        // Flag new query in progress
                        i_countFetch = 0;

                    } // End of validateLine OK
                    
                    else
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10305,
                                this,
                                "CORRUPT LINE after extractAndValidate..... "
                                    + i_batchRecordDataFromFile.dumpRecord() );
                        } 

                        // Line is corrupt - invalid record
                        i_batchRecordDataFromFile.setCorruptLine(true);
                        if ( i_batchRecordDataFromFile.getErrorLine() == null )
                        {
                            // Errorcode NOT prepared in extractLineAndValidate
                            i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD ); 
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(
                                    PpasDebug.C_LVL_MODERATE,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10306,
                                    this,
                                    "errorline NOT prepared in extractAndValidate()" );
                            } 

                        }

                        // no records to be fetched - corrupt line! 
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10307,
                                this,
                                "CORRUPT LINE - set cursor is empty!!!! "
                                    + i_batchRecordDataFromFile.dumpRecord() );
                        } 
                        
                        // Flag to prepare a new query 
                        i_cursorIsEmpty = true;
                        
                        // Send read record with error information
                        
                        // *** RETURNS ***
                        i_batchRecordDataFromFile.setSuccessfullyProcessed(i_alreadyProcessed);  
                        i_batchRecordDataFromFile.setNumberOfSuccessfullyProcessedRecords(
                            super.i_successfullyProcessed );
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                11307,
                                this,
                                "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
                        }

                        return i_batchRecordDataFromFile;

                    } // End else - corrupt line..

                } // End read line not empty
                else
                {
                    // EOF - exit
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10308,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getRecord + "*** EOF !!! returns null *****");
                    }   
                    i_moreRowsToRead = false;
                    
                    // *** RETURNS ***
                    return null;
                }
            } // End if i_cursorIsEmpty


            
            // Get information from the database
            try
            {
                l_record = (NPChBatchRecordData)i_isApi.fetch();
            }
            catch (PpasServiceException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_SERVICE_EXCEPTION_GET_RECORD,
                                     LoggableInterface.C_SEVERITY_ERROR) );

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10309,
                        this,
                        "**EXCEPTION PpasServiceFailedException **" + e.getMessage() );
                }    
            }        

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10309,
                    this,
                    "** TRACE ** before test l_record != null" );
            }
            
            if ( l_record != null )
            {

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10309,
                        this,
                        "** TRACE ** after if l_record != null, wildcards=" 
                            + i_wildcards + l_record.dumpRecord());
                }

                i_countFetch++;
                                 
                /// if wildcards, save the values that are common for all records in this range
                if ( i_wildcards )
                {
                    l_record.setNewMsisdn( l_record.getOldMsisdn().replaceFirst(i_oldPrefix,i_newPrefix) );
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10310,
                            this,
                            "==WILDCARD replacement old=" + l_record.getOldMsisdn() +
                                " new:"+l_record.getNewMsisdn() +
                                "\n            where oldPrefix:" + i_oldPrefix+" newPrefix:"+i_newPrefix);
                    }

                }
                else
                {
                    l_record.setNewMsisdn(this.i_newMSISDN);
                }

                // Copy standard variables into the record that will be returned
                l_record.setInputFilename(i_batchRecordDataFromFile.getInputFilename());
                l_record.setRowNumber(i_batchRecordDataFromFile.getRowNumber());
                l_record.setInputLine( i_batchRecordDataFromFile.getInputLine());
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10311,
                        this,
                        BatchConstants.C_LEAVING + C_METHOD_getRecord 
                            + "Record returned : "+l_record.dumpRecord() );
                }
                
                // *** RETURNS ***
                MsisdnFormat l_msisdnFormat = null;
                Msisdn       l_newMsisdn    = null;
                Msisdn       l_oldMsisdn    = null;
                // Test if it is possible to parse old- and new MSISDN into a MSISDN
                l_msisdnFormat = this.i_context.getMsisdnFormatInput();           

                try
                {               
                    l_newMsisdn = l_msisdnFormat.parse( l_record.getNewMsisdn() );                 
                    l_oldMsisdn = l_msisdnFormat.parse( l_record.getOldMsisdn() );
                    l_record.setParsedNewMsisdn(l_newMsisdn);
                    l_record.setParsedOldMsisdn(l_oldMsisdn);
                }
                catch (ParseException e)
                {

                    // Cannot create a valid MSISDN from the newMsisdn
                    i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD ); 
                    l_record.setCorruptLine(true);              
                 
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10022,
                            this,
                            "ParseException "+l_record.dumpRecord());
                    }
                }
               
                l_record.setSuccessfullyProcessed(i_alreadyProcessed);  
                l_record.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
                l_record.setNumberOfErrorRecords(super.i_notProcessed);
                if ( super.faultyRecord(i_lineNumber))
                {
                    l_record.setFailureStatus(true);
                }

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        11311,
                        this,
                        "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
                }

                return l_record;  

            }

            
            // no record fetched
            i_cursorIsEmpty = true;

            if ( i_countFetch == 0 )
            {
                // Not found in database
                i_batchRecordDataFromFile.setOldMsisdn(i_oldMSISDN);
                i_batchRecordDataFromFile.setNewMsisdn(i_newMSISDN);

                // Send error code, rest of the input line shall be leftjustified
                i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD );
                i_batchRecordDataFromFile.setCorruptLine(true);

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10312,
                        this,
                        BatchConstants.C_LEAVING + C_METHOD_getRecord 
                            + " NOT found in database, return record: "
                            + i_batchRecordDataFromFile.dumpRecord());
                }
                
                // *** RETURNS ***
                i_batchRecordDataFromFile.setSuccessfullyProcessed(i_alreadyProcessed); 
                i_batchRecordDataFromFile.setNumberOfSuccessfullyProcessedRecords(
                    super.i_successfullyProcessed);
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        11312,
                        this,
                        "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
                }

                return i_batchRecordDataFromFile;                
            }
                        
        }    


        // Check for 'l_record' not to be 'null'.
        // TODO: Need to investigate if the following code could be reached or not.
        if (l_record != null)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME,
                    10313,
                    this,
                    BatchConstants.C_LEAVING + C_METHOD_getRecord + l_record.dumpRecord());
            }
    
            l_record.setSuccessfullyProcessed(i_alreadyProcessed); 
            l_record.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11313,
                    this,
                    "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
            }
        }

        return l_record;

    } // end of method getRecord()







    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form: INSTALL_STATUS_yyyymmdd_sssss.DAT /IPG.
     * @param p_fileName Filename to check.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {                
        boolean  l_validFileName = true;       // Help variable - return value. Assume filename is OK
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10314,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_IPG)  )
            {
                // OK Filename: NUMBER_CHANGE_yyyymmdd_sssss.DAT.
                // Rename the file to .IPG to indicate that processing is in progress.
                // If it is recovery mode, the file already got the "in_progress_extension"
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10315,
                            this,
                            "After rename, inputFullPathName="+i_fullPathName );
                    }
                                   
                }

            }
            else if (p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_SCH))
            {
                // OK Filename: NUMBER_CHANGE_yyyymmdd_sssss.SCH (see above)
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10316,
                            this,
                            "After rename, inputFullPathName="+i_fullPathName );
                    }
                                   
                }
            }
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
                    
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10317,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
        
    } // End of isFileNameValid(.)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    
    /**
     * This method extracts information from the batch data record.
     * The record must look like: [oldMSISDN][newMSISDN].
     * The line number will be used to check against the recovery file if the batch mode is
     * recovery.
     * All fields are checked to be alpha numberic.
     * This method also do the checks necessary for the wildcard handling, such as that there 
     * are the same number of wildcards both in old- and new MSISDN.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        final String L_SQL_WILDCARD                 = "_";
        
        // Help array to keep the input line elements
        String[]     l_recordFields                 = new String[C_NUMBER_OF_FIELDS];
        
        int          l_numberOfWildcardsInOldMSISDN = 0;
        int          l_numberOfWildcardsInNewMSISDN = 0;                
        boolean      l_validData                    = true;  // return value - assume it is valid
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10318,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate 
                    + " inputLine=]" + i_inputLine + "[ row=" + i_lineNumber);
        }

        i_wildcards = false;
        
        // Create a data record to keep the information retreived from file.
        // Another datarecord is retrieved from the database
        // The record from the database needs to be updated with information from the datarecord created here.
        i_batchRecordDataFromFile = new NPChBatchRecordData();
        i_batchRecordDataFromFile.setInputFilename(i_inputFileName);
        
        // Extract the line number from the batch record into line# and inputline
        i_batchRecordDataFromFile.setRowNumber( i_lineNumber );
        i_batchRecordDataFromFile.setInputLine( i_inputLine );
        
        
        // Check max record length
        if ( i_inputLine.length() > NPChBatchReader.C_MAX_RECORD_LENGTH )
        {
            // Send error code, rest of the input line shall be leftjustified
            this.i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD );
            l_validData = false; 
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10319,
                    this,
                    "Row: " + i_inputLine + " wrong record length " +
                        " l_lineNumber   =" + i_lineNumber + " " + i_batchRecordDataFromFile.getErrorLine() +
                        " len= " + i_inputLine.length() );
            }

        }
        
        else
        {
            
            // OK indata a record or the correct size - check field by field
            initializeRecordValidation( i_inputLine, l_recordFields, i_batchRecordDataFromFile );

            // Get the record fields and check if it is numeric/alphanumberic
            //            Start                End                  #Field        Format check                           Errorcode
            if ( getField(0,                   C_OLD_MSISDN_LENGTH, C_OLD_MSISDN, C_PATTERN_MSISDN_NUMERIC_AND_WILDCARD, C_ERROR_INVALID_RECORD ) &&
                 getField(C_OLD_MSISDN_LENGTH, C_NEW_MSISDN_LENGTH, C_NEW_MSISDN, C_PATTERN_MSISDN_NUMERIC_AND_WILDCARD, C_ERROR_INVALID_RECORD ) )
            {
                // Got a valid record? - no wildcards                
                // Both old and new MSISDN must be specified
                if ( l_recordFields[C_OLD_MSISDN] == null ||
                     l_recordFields[C_NEW_MSISDN] == null )
                {
                    // Field not specified
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10320,
                            this,
                            "Row: " + i_inputLine + " is an INVALID record, null MSISDN from getField" +
                                    " l_lineNumber   =" + i_lineNumber + "\n" +
                                    " l_old_msisdn   =" + l_recordFields[C_OLD_MSISDN] + "\n" +
                                    " l_new_Msisdn   =" + l_recordFields[C_NEW_MSISDN] );
                    }
                    // send error code, rest of the input line shall be leftjustified
                    this.i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD );
                    i_batchRecordDataFromFile.setCorruptLine(true);
                    l_validData = false;                
                }
                // it is a valid record...
            }
            
            else 
            {
                // Invalid record - not alphanumeric
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME,
                        10321,
                        this,
                        "Row: " + i_inputLine + " is a INVALID record, deteted in getField()" +
                                " l_lineNumber   =" + i_lineNumber + "\n" +
                                " l_old_msisdn   =" + l_recordFields[C_OLD_MSISDN] + "\n" +
                                " l_new_Msisdn   =" + l_recordFields[C_NEW_MSISDN] );
                }
                
                // send error code, rest of the input line shall be leftjustified
                this.i_batchRecordDataFromFile.setErrorLine( C_ERROR_INVALID_RECORD );
                i_batchRecordDataFromFile.setCorruptLine(true);
                l_validData = false;                

            }
            
            if ( l_validData )
            {
                // Check for wildcards
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME,
                        10321,
                        this,
                        "Row: " + i_inputLine +" is a VALID record, deteted in getField() check wildcards???"+
                                " l_lineNumber   =" + i_lineNumber + "\n" +
                                " l_old_msisdn   =" + l_recordFields[C_OLD_MSISDN] + "\n" +
                                " l_new_Msisdn   =" + l_recordFields[C_NEW_MSISDN] );
                }

                
                // Check for wildcards and that there are the same number
                // of wildcards both in the old- and new MSISDN                
                l_numberOfWildcardsInOldMSISDN = this.getNumberOfWildcards( l_recordFields[C_OLD_MSISDN]);
                l_numberOfWildcardsInNewMSISDN = this.getNumberOfWildcards( l_recordFields[C_NEW_MSISDN]);
                
                if ( l_numberOfWildcardsInOldMSISDN == l_numberOfWildcardsInNewMSISDN )
                {
                    i_oldMSISDN = l_recordFields[C_OLD_MSISDN];
                    i_newMSISDN = l_recordFields[C_NEW_MSISDN];
                    i_newPrefix = i_newMSISDN.substring(0,i_newMSISDN.length()-l_numberOfWildcardsInNewMSISDN);
                    i_oldPrefix = i_oldMSISDN.substring(0,i_oldMSISDN.length()-l_numberOfWildcardsInOldMSISDN);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10322,
                            this,
                            "OLDprefix:]" + i_oldPrefix + "[\n"+
                            "NEWprefix:]" + i_newPrefix + "[" );
                    }
                    
                    if ( l_numberOfWildcardsInOldMSISDN != 0 )
                    {
                        // Wildcards! Create a sql-parameter with a % at the end
                        i_oldMSISDN = l_recordFields[C_OLD_MSISDN].replaceAll(C_PATTERN_MSISDN_WILDCARD,
                                                                              L_SQL_WILDCARD );
                        i_wildcards = true;
                    }
                    

                    // ---------------------------------------------------------------------------------------
                    // This is a batch that uses both a indata file which specifies the old- and new- MSISDN
                    // As there might be wildcards in these MSISDN we need to go to the database
                    // and get the right SDPID and customer id for each MSISDN. 
                    // Create a batchRecordData and insert it into the in-queue for each MSISDN. 
                    // (E.g. if no wildcards there will only be one record.
                    // ---------------------------------------------------------------------------------------
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10323,
                            this,
                            "OK : " +  i_inputLine + " retrieve information from the database, oldMSISDN="+
                                i_oldMSISDN + "[ newMSISDN=" + i_newMSISDN + "[");
                    }  

                }
                else
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10324,
                            this,
                            "Incorrect use of wildcards: \n" +  i_inputLine);
                    }  
                    // Send error code, rest of the input line shall be leftjustified 
                    // (overwrite the errorline from the getField)
                    this.i_batchRecordDataFromFile.setErrorLine( C_ERROR_INCORRECT_USE_OF_WILDCARDS );
                    i_batchRecordDataFromFile.setCorruptLine(true);
                    l_validData = false;                                  
                }
                                                                    
            } // End - check for wildcards
            
        }
            
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10325,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return l_validData;
        
    }


    
    
    
    /**
     * Help method to get the number of wildcards in a MSISDN.
     * @param p_msisdn The MSISDN to check if it contains a wildcard
     * @return Number of found wildcards in the MSISDN
     */
    private int getNumberOfWildcards( String p_msisdn )
    {
        int l_returnValue   = 0;
        
        int l_length        = p_msisdn.length();
        int l_wildcardIndex = p_msisdn.indexOf( C_MSISDN_WILDCARD );
        
        if ( l_wildcardIndex != -1 )
        {
            // Count the wildcards
            // The field has been trimmed - C_MSISDN_LENGTH not valid here
            l_returnValue = l_length - l_wildcardIndex;
        }
        
        return l_returnValue; 
    } // end of method extractLineAndValidate(.)


    
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_initialize = "initializeIsapi";        
    /**
     * Opens the batchisil api.
     * @param p_queryParameters
     */
    private void initializeIsapi( String[] p_queryParameters )
    {
        PpasRequest l_request      = null;
        PpasSession l_session      = null;

        // Create and open the Batch ISIL Service Component
        l_session = new PpasSession();
        l_session.setContext(i_context);
        
        l_request = new PpasRequest(l_session);
        i_isApi   = new NPChDbBatchService(l_request, i_logger, i_context );

        // Call open on the API component
        try
        {
            i_isApi.open(p_queryParameters, i_fetchSize );
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10326,
                    this,
                    "**EXCEPTION PpasServiceFailedException **" + C_METHOD_initialize + " " +e.getMessage() );
            }
        }        
        catch (SecurityException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "SecurityException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10327,
                    this,
                    "SecurityException - getRecord. " + C_METHOD_getRecord );
            }
        }
        catch (IllegalArgumentException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "IllegalArgumentException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10328,
                    this,
                    "IllegalArgumentException - getRecord. " + C_METHOD_getRecord );
            }
        }

        return;
    } // End of initialize()

    
    
 
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_reopenIsapi = "reopenIsapi";        
    /**
     * Reuse the created sql-statement.
     * @param p_queryParameters
     */
    private void reopenIsapi( String[] p_queryParameters )
    {

        StringBuffer l_tmp = new StringBuffer();
        if ( p_queryParameters == null )
        {
            l_tmp.append("**REOPEN - got null arguments ");
        }
        else
        {
            l_tmp.append("**REOPEN - arguments: ");
            for ( int i = 0; i < p_queryParameters.length; i++ )
            {
                l_tmp.append(" "+p_queryParameters[i]);
            }
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_ERROR,
                C_CLASS_NAME,
                10329,
                this,
                l_tmp.toString() );
        }
       
        // Call open on the API component
        try
        {
            i_isApi.reopen(p_queryParameters );
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_SERVICE_EXCEPTION_REOPEN_ISAPI,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10330,
                    this,
                    "**EXCEPTION PpasServiceFailedException **" + C_METHOD_reopenIsapi + " " +e.getMessage());
            }
 
            e.printStackTrace();
        }        
        catch (SecurityException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "SecurityException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10331,
                    this,
                    "SecurityException - getRecord. " + C_METHOD_reopenIsapi );
            }
        }
        catch (IllegalArgumentException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "IllegalArgumentException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10332,
                    this,
                    "IllegalArgumentException - getRecord. " + C_METHOD_reopenIsapi + " " + l_tmp.toString());
            }
        }

        return;
    } // End of reopenIsapi()


    
    
    
    /** 
     * Retrieves the properties needed for the Reader.
     */
    private void getProperties()
    {
        String l_tmpFetchSize = null;
        
        l_tmpFetchSize = i_properties.getProperty(BatchConstants.C_FETCH_ARRAY_SIZE);
        if ( l_tmpFetchSize != null )
        {
            i_fetchSize = Integer.valueOf(l_tmpFetchSize).intValue();
        }
        
        return;
        
    }

 

    /** 
     * @see com.slb.sema.ppas.batch.batchreader.BatchLineFileReader#tidyUp()
     */
    protected void tidyUp()
    {
        // Clean up connectivity
        try
        {
            finalise();
            super.tidyUp();
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10333,
                    this,
                    "PpasServiceException - tidyUp(). ");
            }

       }
                
        return;
    }  // End of tidyUp()





    /**
     * Closes the isapi.
     * @throws PpasServiceException
     */
    protected void finalise() throws PpasServiceException
    {
        if ( this.i_isApi != null )
        {
            this.i_isApi.close();
            this.i_isApi = null;
            
        }
        
        return;
    }  // End of finalise()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isProcessingRequired = "isProcessingRequired";
    
    /**
     * This method checks if the record already has been correctly processed or not.
     * @return <code>true</code>  means it needs to be re-processed 
     *         <code>false</code> has already been successfully processed
     */
    // need to redefined this method for the NumberPlanChange batch
    protected boolean isProcessingRequired()
    {
        boolean      l_processTheLine = false;
        String       l_recordStatus   = null;    // Can be "ERR"/"OK"
        StringBuffer l_tmp            = new StringBuffer();
        StringBuffer l_tmpKey         = new StringBuffer();
        String       l_key            = null;
 
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10334,
                this,
                "** NEW isProcessingRequired "+BatchConstants.C_ENTERING+C_METHOD_isProcessingRequired + 
                    " ]" +i_inputLine+"[");
        }

        // Has input line any wildcards
        String l_oldMsisdn           = i_inputLine.substring(0,BatchConstants.C_LENGTH_MSISDN).trim();
        int    l_numberOfWildcards   = getNumberOfWildcards(l_oldMsisdn);
        int    l_endRangeOfWildcards = (new BigInteger("10")).pow(l_numberOfWildcards).intValue();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10335,
                this,
                "**isProcessingRequired** old=" + l_oldMsisdn + " nowild=" + l_numberOfWildcards + 
                    " end=" + l_endRangeOfWildcards );
        }
        
        if ( l_numberOfWildcards > 0 )
        {
            // The input row has wildcards!!
            l_tmp.append(i_lineNumber);
            l_tmp.append(" ");
            l_tmp.append(l_oldMsisdn.substring(0,l_oldMsisdn.length()-l_numberOfWildcards));
            String l_keyPrefix = l_tmp.toString();
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10336,
                    this,
                    "**createKeyPrefix** ="+l_keyPrefix);
            }        

            
            
            // Check if any of the msisdn's in the wildcard range has failed
            // Construct hash map key as: #line MSISDN
            i_alreadyProcessed = new Hashtable();
            for ( int i = 0; i < l_endRangeOfWildcards; i++ )
            {
                // Zero-pad the ending number
                String l_msisdnSuffix = BatchConstants.C_MSISDN_ZEROS + i;
                l_msisdnSuffix        = l_msisdnSuffix.substring(l_msisdnSuffix.length()-l_numberOfWildcards);
                l_key                 = l_keyPrefix + l_msisdnSuffix;
                l_recordStatus        = (String)i_recoveryInformation.get( l_key );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10337,
                        this,
                        "=VALUE FOUND= key="+l_key+" status="+l_recordStatus+" l_msisdnSuffix="+l_msisdnSuffix);
                }        

                if ( l_recordStatus != null ) 
                {

                    // Prepare recovery map for the processor. Key=msisdn Value=OK
                    String[] l_tmpArray = l_key.split(" ");

                    if ( l_recordStatus.equals(BatchConstants.C_SUCCESSFULLY_PROCESSED))
                    {
                        i_alreadyProcessed.put( l_tmpArray[1], l_recordStatus);
                    }
                    else
                    {
                        l_processTheLine = true;
                    }
                }
                else
                {
                    // Not found in the hashtable - must be processed
                    l_processTheLine = true;
                }
                
            }
        }
        else
        {
            // No wildcards in this row
            l_tmpKey.append(Integer.toString(i_lineNumber));
            l_tmpKey.append(C_DELIMITER_RECOVERY_KEY);
            l_tmpKey.append(i_inputLine.substring(0,BatchConstants.C_LENGTH_MSISDN).trim());
            l_key = l_tmpKey.toString();
            
            l_recordStatus = (String)i_recoveryInformation.get( l_key );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10338,
                    this,
                    "==NO WILDCARDS== key="+l_key+" recordStatus="+l_recordStatus);
            }        
            
            if ( l_recordStatus != null ) 
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10339,
                        this,
                        "isProcessingRequired : recordStatus="+l_recordStatus);
                }        
          
                if ( l_recordStatus.equals(BatchConstants.C_NOT_PROCESSED))
                {
                    l_processTheLine = true;
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10340,
                            this,
                            "isProcessingRequired : process the line!!");
                    }        

                }
            }
            else
            {
                l_processTheLine = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10341,
                        this,
                        "could not find this record in the hashtable - send it to processing!");
                }        
                
            }           
            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10342,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isProcessingRequired+ " returnValue="+l_processTheLine);
        }
                
        return l_processTheLine;
        
    } // end of method isProcessingRequired()
    
} // End of class NPChBatchReader
