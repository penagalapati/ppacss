////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       CCIChangeBatchWriterUT.java 
//DATE            :       Sep 16, 2004
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       ATOS ORIGIN 2004
//
//DESCRIPTION     :       Unit test for CCIChangeBatchWriter.
//                        In the property file batch_cci.properties the
//                        property com.slb.sema.ppas.batch.controlTableUpdateFrequency
//                        has to be set to 1 for this test to work.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of change>    | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.CCIChangeBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.CCIChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for CCIChangeBatchWriter. */
public class CCIChangeBatchWriterUT extends BatchTestCaseTT
{
    /** Reference to SubInstBatch controller object. */
    private static CCIChangeBatchWriter     c_cciChangeBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static CCIChangeBatchController c_cciChangeBatchController;

    /** An input file name vid extension .DAT. */
    private static final String             C_INPUT_FILE_NAME_DAT = "COMCHARGE_20040615_00003.DAT";

    /** A file name vid extension .RPT. */
    private static final String             C_FILE_NAME_RPT       = "COMCHARGE_20040615_00003.RPT";
    
    /** A file name with extension of .TMP. */
    private static final String             C_FILE_NAME_TMP_RPT       = "COMCHARGE_20040615_00003.TMP";

    /** A file name vid extension .RCV. */
    private static final String             C_FILE_NAME_RCV       = "COMCHARGE_20040615_00003.RCV";

    /** The js job id. */
    private static final String             C_JS_JOB_ID           = "10001";

    /** The file date. */
    private static final String             C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final String             C_SEQ_NO              = "0003";

    /** The sub job counter. */
    private static final String             C_SUB_JOB_COUNT       = "-1";

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService         i_controlService      = null;

    /**
     * The constructor for the class NPCutBatchWriterUT.
     * @param p_name This test's class name.
     */
    public CCIChangeBatchWriterUT(String p_name)
    {
        super(p_name, "batch_cci");
    }
    
    /** Perform standard activities at start of a test. */
    protected void setUp()
    {
        super.setUp();
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        createContext();
    }
    
    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if
     * the row <code>baco_failur</code> has been updated.
     * Set the value for error line in the object <code>SubInstBatchRecordData</code> to null and updates
     * the table <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */

    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        CCIChangeBatchRecordData l_dataRecord = new CCIChangeBatchRecordData();

        BatchJobDataSet l_batchjobSet = null;
        BatchJobData l_batchData1 = new BatchJobData();
        BatchJobData l_batchData = null;
        String l_expectedKey = null;
        String l_actualKey = null;
        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";
        String l_executionDateTime = null;
        String l_jsJobId = null;
        Vector l_vec = null;
        String l_actualText = null;

        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_cciChangeBatchController.getKeyedControlRecord();

            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE);

            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setOpId(c_cciChangeBatchController.getSubmitterOpid());

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
//                l_dataRecord.setSdpId("0001");
                c_cciChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "0";

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }

            //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_TMP_RPT,
                                                     BatchConstants.C_EXTENSION_TMP_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_REPORT_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            // Get the recovery text from the recovery logfile.
//            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RCV,
//                                                     BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                                     BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//
//            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                        ("No file found".equals(l_actualText)));
//            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);

            //Test when there is no error line set.
            try
            {
                l_dataRecord.setErrorLine(null);
                c_cciChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 1.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "1";

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);

            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
    
    /**
     * @ut.when Attempting to write trailer lines to the output files for this job.
     * @ut.then The output files are named correctly.
     */
    public void testWriteTrailerRecordSuccess()
    {
        beginOfTest("testWriteTrailerRecordSuccess");
  
        CCIChangeBatchRecordData l_dataRecord = new CCIChangeBatchRecordData();

        BatchJobData l_batchData1 = new BatchJobData();
        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";

        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_cciChangeBatchController.getKeyedControlRecord();

            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE);

            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setOpId(c_cciChangeBatchController.getSubmitterOpid());

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                
                c_cciChangeBatchWriter.writeRecord(l_dataRecord);
                c_cciChangeBatchWriter.writeTrailerRecord(BatchConstants.C_TRAILER_SUCCESS);
                
                // Check report file has been renamed from .TMP to .DAT
                String l_return = getValueFromLogFile(C_FILE_NAME_RPT,
                                                      BatchConstants.C_EXTENSION_REPORT_FILE, 
                                                      BatchConstants.C_REPORT_FILE_DIRECTORY);
                
                assertFalse("Failure: Found not file with extension " + 
                                BatchConstants.C_EXTENSION_INDATA_FILE,
                            ("No file found".equals(l_return)));
            }
            catch (PpasServiceException e)
            {
                failedTestException(e);
            }
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(CCIChangeBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to run all the tests in this
     * class.
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /**
     * Create context neccessary for this test.
     */
    private void createContext()
    {
        NPCutBatchRecordData l_batchRecordData = new NPCutBatchRecordData();
        Hashtable l_params = new Hashtable();
        SizedQueue l_outQueue = null;
        String l_dir = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String l_fileReportName = l_dir + "/" + C_FILE_NAME_RPT;
        String l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String l_fileRecoveryName = l_dir2 + "/" + C_FILE_NAME_RCV;
        
        //The update frequency needs to be 1 for the test to work.
        super.c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");

        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_batchRecordData);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }

        //Make sure that there are no files with the same name.
        super.deleteFile(l_fileReportName);
        super.deleteFile(l_fileRecoveryName);

        l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_FILE_NAME_DAT);

        try
        {
            c_cciChangeBatchController = 
                new CCIChangeBatchController(super.c_ppasContext,
                                                BatchConstants.C_JOB_TYPE_BATCH_CCI,
                                                C_JS_JOB_ID,
                                                "CCIChangeBatchWriterUT",
                                                l_params);

            c_cciChangeBatchWriter = new CCIChangeBatchWriter(super.c_ppasContext,
                                                                    super.c_logger,
                                                                    c_cciChangeBatchController,
                                                                    l_params,
                                                                    l_outQueue,
                                                                    super.c_properties);

        }
        catch (PpasConfigException e)
        {
            super.failedTestException(e);
        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }
    }

}
