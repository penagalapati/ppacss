////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME       :       FraudBatchProcessor.java
//        DATE            :       02-May-2006
//        AUTHOR          :       Marianne Toernqvist
//        REFERENCE       :       PpaLon#2203/8587, PRD_ASCS00_GEN_CA_080
//
//        COPYRIGHT       :       WM-data 2006
//
//        DESCRIPTION     :       Vodafone Ireland Fraud returns.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
// 02/06/06 | M.Toernqvist  | If it is a non-standard       | PpaLon#2355/8996
//          |               | voucher the amount/currency   |
//          |               | can be null. Handling to set  |
//          |               | amount=0 and currency default |
//          |               | added.                        |
//----------+---------------+-------------------------------+--------------------
// 14/06/06 | M.Toernqvist  | Move basic test 'hardcoded    | PpaLon#2400/9073
//          |               | ISAPI stubs' to the UTprogram |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.FraudReturnsBasicTestData;
import com.slb.sema.ppas.batch.batchreader.FraudReturnsBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.ExtRechargeData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.FraudBatchRecordData;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.VoucherData;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.dataclass.VoucherStatus;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.Exceptionable;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasVoucherService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for calling the appropriate IS API method to get information and
 * create Fraud Returns reports.  This is achieved by using PpasVoucherService, and use the following
 * methods: getRecharge(), getExtRecharge(), getVoucher() and changeVoucherState().
 * NOTE. When this batch is started from the UT-program it does not use the real ISAPI, instead it
 * uses 'hardcoded ISAPI stubs' created in the UT-program.
 */
public class FraudReturnsBatchProcessor extends BatchProcessor
{

    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "FraudReturnsBatchProcessor";
    
    private static final String C_DUMMY_MSISDNS_FILE_NOT_FOUND = 
        "Could not find the file :dummy_msisdn.dba - assume this is OK and continue without DummyMSISDNs";
    
    private static final String C_DUMMMY_MSISDN_IO_EXCEPTION   = 
        "Could not read from the file : dummy_msisdn.dba";
    
    private static final String C_DUMMY_MSISDN_FILE_CANNOT_BE_CLOSED = 
        "Could not close the file : dummy_msisdn.dba";
    
    /** Error message if failing to create a default Money amount. Value is {@value}.*/
    private static final String C_NON_STANDARD_VOUCHER         = 
        "<Non standard voucher - failed to create an amount of 0.0>";
    
    /** Voucher has status damaged. Value is {@value}. */
    private static final String C_VOUCHER_HAS_STATUS_DAMAGED   = 
        "<Voucher has status DAMAGED>";        

    /** Voucher does not exist.  Value is {@value}. */
    private static final String C_VOUCHER_DOES_NOT_EXIST       = 
        "<Voucher does not exist>";        

    /** Invalid voucher status.  Value is {@value}. */
    private static final String C_INVALID_VOUCHER_STATUS       = 
        "<Invalid voucher status ";
    
    /** Serial number was not found on the Voucher Server.  Value is {@value}. */
    private static final String C_SERIAL_NUMBER_NOT_FOUND_ON_VOUCHER_SERVER = 
        "<Serial number not found on Voucher Server>";
    
    /** Suffix for the first row in the Finance Summary Report. Value is {@value}. */
    private static final String C_DUMMY_VOUCHER           = "_DUMMY";
    
    /** Message for the exception section in the summary report.  Value is {@value}.*/
    private static final String C_ALREADY_EXPIRED         = "Already Expired";

    /** Delimiter for fields in report file. Value is {@value}. */
    private static final String C_REPORT_FIELD_DELIMITER  = ",";
    
    /** Timeout in ms when callins ISAPI. */
    private static final long   C_TIMEOUT                 = 20000L;

    /** Voucher Service status, available. */
    private static final int    C_STATUS_AVAILABLE        = 0;
    
    /** Voucher Service status, assigned. */
    private static final int    C_STATUS_ASSIGNED         = 1;
    
    /** Voucher Service status, damaged. */
    private static final int    C_STATUS_DAMAGED          = 2;
    
    /** Voucher Service status, does not exist. */
    private static final int    C_STATUS_NOT_EXIST        = 10;
    
    /** Voucher belongs to the group 'Expired'. Value is {@value}. */
    private static final String C_VOUCHER_GROUP_EX        = "EX";
    

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>Session</code> object. */
    private PpasSession        i_ppasSession       = null;
    
    /** The <code>MoneyFormat</code> object. */
    private MoneyFormat        i_moneyFormat       = null;
    
    /** The <code>MsisdnFormat</code> object. */
    private MsisdnFormat       i_msisdnFormat      = null;
    
    /** The <code>PpasVoucherService</code> object. */
    private PpasVoucherService i_voucherService    = null;
    
    /** The <code>PpasRequest</code> object. */
    private PpasRequest        i_ppasRequest       = null;
    
    /** Handles Dummy records. */
    private DummyMSISDNs       i_dummyMSISDNs      = null;
    
    /** Handles Used records. */
    private UsedVouchers       i_usedVouchers      = null;
    
    /** Handles Available records. */
    private Available          i_available         = null;
    
    /** For Basic Test purposes, to get the 'hardcoded UT ISAPI'. */
    private FraudReturnsBasicTestData i_fraudReturnsBasicTestData = null;
    
    

    /**
     * Constructs a FraudReturnsBatchProcessor object using the specified
     * batch controller, input data queue and output data queue.
     *
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the Logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process parameters
     * @param p_properties       <code>PpasProperties</code> for the batch subsystem.
     */
    public FraudReturnsBatchProcessor( PpasContext     p_ppasContext,
                                       Logger          p_logger,
                                       BatchController p_batchController,
                                       SizedQueue      p_inQueue,
                                       SizedQueue      p_outQueue,
                                       Map             p_params,
                                       PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 11000, this,
                             BatchConstants.C_CONSTRUCTING );
        }

        i_ppasSession  = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);        
        i_moneyFormat  = p_ppasContext.getMoneyFormat();
        i_msisdnFormat = p_ppasContext.getMsisdnFormatInput();
        
        // Create report files - set counters to zero
        // Initialize dummy Msisdn information.
        i_dummyMSISDNs = new DummyMSISDNs();
        i_usedVouchers = new UsedVouchers();
        i_available    = new Available();
        
        
        // If the batch was started from the UT-program, it will use a 'hardcoded ISAPI', the returned
        // data-records are created by inner classes in the special basic test data class.
        // When the batch is started the normal way with ascsJobs or BOI it will use the real ISAPI.
        i_fraudReturnsBasicTestData = (FraudReturnsBasicTestData)p_ppasContext.
                                          getAttribute(FraudReturnsBasicTestData.C_KEY_BASIC_TEST);
        
        if ( i_fraudReturnsBasicTestData == null )
        {
            i_ppasRequest    = new PpasRequest(i_ppasSession);
            i_voucherService = new PpasVoucherService(i_ppasRequest, i_logger, i_ppasContext);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME, 11010, this,
                             BatchConstants.C_CONSTRUCTED );
        }
        
        return;

    } // End of constructor


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";        
    /**
     * Processes the given <code>FraudReturnsBatchDataRecord</code> and returns it.
     * If the process succeeded the <code>FraudReturnsBatchDataRecord</code> is updated with information
     * needed by the writer component before returning it.
     * Check if the indata record is in ACIN table and not a 'duplicate' or 'expired' then create a 
     * row for the USED report. 
     * Else, if the indata record is in EXRE table and not a 'duplicate' or 'expired' then create a
     * row for the USED report.
     * Else, if the voucher is available on the voucher server having state=0 and not a 'duplicate'
     * or 'expired' then create a row for the AVAIL report. These records shall also generate a request 
     * to the voucherServer to change status to 2.
     * If the status was = 1, then write a record to the USED report.
     * Any other statuses from the VoucherServer - log an error in the Batch's logfile.
     * 
     * If the record is 'duplicate' or 'expired' then is shall be listed in the exception section of the
     * summary report.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @see BatchProcessor#processRecord(BatchRecordData)
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    public BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        
        String               l_voucherSerialNumber  = null;  // Voucher Serial Number.
        FraudBatchRecordData l_batchRecordData      = null;  // Data record to be sent to the writer.
        VoucherData          l_voucherData          = null;  // Data record from the ACIN table.
        ExtRechargeData      l_externalRechargeData = null;  // Data record from the EXRE table.
        VoucherEnquiryData   l_voucherServerData    = null;  // Data record from the Voucher Server.
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 11020, this,
                             BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }
        
        l_batchRecordData = (FraudBatchRecordData)p_record;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11030, this,
                             "===== START processRecord() with : " + l_batchRecordData.dumpRecord());
        }  
        
        
        // Check if this record is the last record in the current file.
        if ( l_batchRecordData.getInputFilename().equals(FraudReturnsBatchReader.C_END_OF_FILE))
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11040, this,
                                 "===== END OF FILE record, flag for the writer =====");
            }
            return l_batchRecordData;  // Flag to the Writer that this was the last record
        }

        
        // If it is a corrupt record send it for error log prinout        
        if ( l_batchRecordData.isCorruptLine() )
        {
            l_batchRecordData.setRecoveryStatus(BatchRecordData.C_NOT_PROCESSED);
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11050, this,
                                 "Corrupt record detected - don't process, go to writer");
            }
            return l_batchRecordData;
        }    

        
        
        // Ready to start to process this record
        l_voucherSerialNumber = l_batchRecordData.getVoucherSerialNumber();

        
        // =====================================================================
        // ACIN Accessory Install
        // =====================================================================
        // Check if the voucher serial number is found in acin_accessory_install
        // It cannot be : - duplicate
        //                - the voucher group is not 'EX'  // ACIN_VOUCHER_GROUP
        //                - check if MSISDN for this voucher is 'DUMMY'
        if ( i_fraudReturnsBasicTestData == null )
        {
            // Use real ISAPI - PpasVoucherService
            try
            {
                l_voucherData = i_voucherService.getRecharge(i_ppasRequest,C_TIMEOUT,
                                                             l_voucherSerialNumber);
            }
            catch (PpasServiceException l_grExp)
            {
                failedTestException(l_grExp);                
            }
        }
        else
        {
            // BASIC TEST - Use hard coded ISAPI.
            l_voucherData = i_fraudReturnsBasicTestData.getRecharge( l_voucherSerialNumber );
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11060, this,
                             "** AFTER getRecharge: ");
        }  
        
        if ( l_voucherData != null )
        {
            // Serial number found in ACIN table.
            return handleTableData( l_batchRecordData,
                                    l_voucherData.getValue(),
                                    l_voucherData.getCurrency(),
                                    l_voucherData.getGroup(),
                                    l_voucherData.getInitiatingMsisdn(),
                                    l_voucherSerialNumber,
                                    l_voucherData.getAssignedDateTime() );
        }
 
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11070, this,
                             "** CONTINUE with external recharges....");
        }  
        // =====================================================================
        // EXRE External Recharges
        // =====================================================================
        // It cannot be : - duplicate
        //                - the voucher group is not 'EX'  // EXRE_CARD_GROUP
        //                - check if MSISDN for this voucher is 'DUMMY'                    
        if ( i_fraudReturnsBasicTestData == null )
        {
            // Use real ISAPI - PpasVoucherService
            try
            {
                l_externalRechargeData = i_voucherService.getExtRecharge(i_ppasRequest,
                                                                         C_TIMEOUT,
                                                                         l_voucherSerialNumber);
            }
            catch (PpasServiceException l_erExp)
            {
                failedTestException(l_erExp);                
            }
        }
        else
        {
            // BASIC TEST - Use hard coded ISAPI.
            l_externalRechargeData = i_fraudReturnsBasicTestData.getExtRecharge( l_voucherSerialNumber );
        }
        
        if ( l_externalRechargeData != null )
        {
            // Serial number found in EXRE table.            
            return handleTableData( l_batchRecordData,
                                    l_externalRechargeData.getAmount(),
                                    (l_externalRechargeData.getAmount()==null)? 
                                            null:l_externalRechargeData.getAmount().getCurrency(),
                                    l_externalRechargeData.getVoucherGroup(),
                                    l_externalRechargeData.getMsisdn(),
                                    l_voucherSerialNumber,
                                    l_externalRechargeData.getRefillDateTime() );            
        }

        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11080, this,
                             "** CONTINUE with Voucher Server ....");
        }  
        // =======================================================
        // Raise a GetVoucherDetails request on the Voucher Server
        // =======================================================
        // Get information from the Voucher Server
        // It cannot be : - duplicate
        //                - the voucher group is not 'EX'  // VoucherServer
        if ( i_fraudReturnsBasicTestData == null )
        {
            // Use real ISAPI - PpasVoucherService
            try
            {
                l_voucherServerData = i_voucherService.getVoucher(i_ppasRequest,
                                                                  C_TIMEOUT,
                                                                  l_voucherSerialNumber);
            }
            catch (PpasServiceException l_vsExp)
            {
                failedTestException(l_vsExp);                
            }
        }
        else
        {
            // BASIC TEST - Use hard coded ISAPI.
            l_voucherServerData = i_fraudReturnsBasicTestData.getVoucher( l_voucherSerialNumber );
        }
        
        if ( l_voucherServerData != null )
        {        	        	
            return handleVoucherServerData( l_batchRecordData,
                                            l_voucherServerData.getValue(),
                                            (l_voucherServerData.getValue()==null)? null:l_voucherServerData.getCurrency(),
                                            l_voucherServerData.getGroup(),
                                            l_voucherServerData.getVoucherStatus(),
                                            l_voucherSerialNumber,
                                            new PpasDateTime(l_voucherServerData.getExpiryDate()) );
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11090, this,
                                 "### FINISH with this record... voucher server data = null ####");
            }
            setErrorMessage(l_batchRecordData, C_SERIAL_NUMBER_NOT_FOUND_ON_VOUCHER_SERVER);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 11100, this,
                             BatchConstants.C_LEAVING + C_METHOD_processRecord +
                             l_batchRecordData.dumpRecord());
        }
        
        return l_batchRecordData;
        
    } // End of processRecord(.)
    

    
   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_handleTableData = "handleTableData";        
   /**
     * This method checks if the indata record is:
     *     - duplicate, if so a record for the Exception part in the File Summary report is created.
     *     - expired, if so a record for the Exception part in the File Summary report is created.
     *     - dummy, if so a record for the DUMMY report is created. 
     * if not a record for the USED report is created.
     * IF the voucher is a non-standard voucher, then the amount is set to 0, and currency to 
     * the default currency.
     * @param p_record A row from the indata file.
     * @param p_amount Voucher amount.
     * @param p_currency Voucher currency.
     * @param p_group The group to which this voucherSerialNumber belongs.
     * @param p_msisdn MSISDN
     * @param p_voucherSerialNumber The current voucherSerialNubmer
     * @param p_firstUsedDate First date and time it was used.
     * @return updated datarecord to be sent to the Writer
     */
    private FraudBatchRecordData handleTableData( FraudBatchRecordData p_record,
                                                  Money                p_amount,
                                                  PpasCurrency         p_currency,
                                                  String               p_group,
                                                  Msisdn               p_msisdn,
                                                  String               p_voucherSerialNumber,
                                                  PpasDateTime         p_firstUsedDate)
    {        
        String l_strMSISDN   = null;    // String with MSISDN.
        String l_dummyRecord = null;    // Dummy record for the DUMMY report.
        String l_usedRecord  = null;    // Used record for the USED report.
 
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 11110, this,
                             BatchConstants.C_ENTERING + C_METHOD_handleTableData);
        }

        // Save information to the batchrecord - needed for the finance report
        if ( !setDefaultAmount( p_record, p_amount, p_currency) )
        {
            // Couldn't save amount and currency
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11120, this,
                                 "Could not save amount and currency have been stored ");
            }  
            setErrorMessage(p_record, C_NON_STANDARD_VOUCHER);
            return p_record;
        }


        if ( !isDuplicate(p_record) )
        {
            // It is NOT flagged as duplicate

            if ( !p_group.equals(C_VOUCHER_GROUP_EX) )
            {            
                // It is NOT of voucher group type: "EX"
                
                l_strMSISDN = i_msisdnFormat.format(p_msisdn);

                if ( i_dummyMSISDNs.isDummy(l_strMSISDN) )
                {
                    // It is a dummy record - write to Dummy Report
                    l_dummyRecord = i_dummyMSISDNs.createDummyRecord(p_voucherSerialNumber,
                                                                     p_firstUsedDate,
                                                                     p_record.getCurrency(),
                                                                     p_record.getFormattedAmount());
                    p_record.setDummyVoucherReportRecord(l_dummyRecord);
                    p_record.setType(C_DUMMY_VOUCHER);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, 
                                         PpasDebug.C_ST_TRACE,C_CLASS_NAME, 11130, this,
                                         "** DUMMY record created");
                    }  

                }
                else
                {
                    // It must be a 'Used Voucher' - write to Used Report
                    l_usedRecord = i_usedVouchers.createUsedRecord(p_voucherSerialNumber,
                                                                   p_firstUsedDate,
                                                                   p_record.getCurrency(),
                                                                   p_record.getFormattedAmount());
                    p_record.setUsedVoucherReportRecord(l_usedRecord);
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, 
                                         PpasDebug.C_ST_TRACE,C_CLASS_NAME, 11140, this,
                                         "** USED record created");
                    }  
                }                             
            }
            else
            {
                // It is an expired voucher
                p_record.setExceptionDetails(C_ALREADY_EXPIRED);
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 11150, this,
                                     "** ALEADY EXPIRED!!!! AFTER setExceptionDetails");
                }  
            }                        
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11160, this,
                                 "Record was duplicate");
            }                    
        }
        
        // This record is fully processed
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11170, this,
                             "ProcessRecord returns: "+p_record.dumpRecord());
        }
        
        p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 11180, this,
                             BatchConstants.C_LEAVING + C_METHOD_handleTableData);
        }

        return p_record;
        
    } // End handleTableData(.......)
        
        

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_handleVoucherServerData = "handleVoucherServerData";        
    /**
     * This method checks if the indata record is:
     *     - duplicate, if so a record for the Exception part in the File Summary report is created.
     *     - expired, if so a record for the Exception part in the File Summary report is created.
     * if not the record is checked and handled differently depending on what status the Voucher Server
     * gave.
     * AVAILABLE - the status will be changed to DAMAGED, and flagged as AVAILABLE in the finance report.
     * ASSIGNED  - an entry for the USED report will be created.
     * DAMAGED   - if it is not a 'recovery run' an error message will be sent to the log. If it is
     *             a 'recovery run', then this record will be handled as AVAILABLE. BUT the status will
     *             not be set again to DAMAGED.
     * NOT_EXISTS - an error message will be sent to the log.
     * All other status will generate an errormessage to the log file.
     * 
     * IF the voucher is a non-standard voucher, then the amount is set to 0, and currency to 
     * the default currency.
     * 
     * @param p_record A row from the indata file.
     * @param p_amount Voucher amount.
     * @param p_currency Voucher currency.
     * @param p_group The group to which this voucherSerialNumber belongs.
     * @param p_status The status from the Voucher Server.
     * @param p_voucherSerialNumber The current voucherSerialNubmer
     * @param p_firstUsedDate First date and time it was used.
     * @return updated datarecord to be sent to the Writer
     */
    private FraudBatchRecordData handleVoucherServerData(FraudBatchRecordData p_record,
                                                         Money                p_amount,
                                                         PpasCurrency         p_currency,
                                                         String               p_group,
                                                         int                  p_status,
                                                         String               p_voucherSerialNumber,
                                                         PpasDateTime         p_firstUsedDate)
    
    {
        // Used record for the USED report.
        String   l_usedRecord             = null;
        // If recovery, status is "ERR/OK" followed by " <last status>"
        String   l_tmpRecoveryStatus      = null;
        // Help array for the "splitted recovery status"
        String[] l_tmpRecoveryInformation = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 11190, this,
                             BatchConstants.C_ENTERING + C_METHOD_handleVoucherServerData);
        }  
        
        // Save information to the batchrecord - needed for the finance report
        if ( !setDefaultAmount( p_record, p_amount, p_currency) )
        {
            // Couldn't save amount and currency
            setErrorMessage(p_record, C_NON_STANDARD_VOUCHER);
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 11200, this,
                                 BatchConstants.C_LEAVING + C_METHOD_handleVoucherServerData + 
                                 "Could not set amount and currency have been stored.");
            }
            return p_record;
        }

        
        if ( !isDuplicate(p_record) )
        {
            // It is not flagged as duplicate
            if ( !p_group.equals(C_VOUCHER_GROUP_EX) )
            {  
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 11210, this,
                                     "NOT duplicate - Before checking status, p_status=" + p_status);
                }  

                // It is not a voucher that had expired...
                // Write to Available Report file
                switch ( p_status )
                {
                    case C_STATUS_AVAILABLE: // Available voucher
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11220, this,
                                             "status = AVAILABLE");
                        }
                        
                        i_available.createAvailableRecord(p_group, p_record);                           
                        
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, 
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11230, this,
                                             "*** check RECOVERY STATUS***  = " + 
                                             p_record.getRecoveryStatus());
                        }      

                        l_tmpRecoveryStatus      = p_record.getRecoveryStatus();
                        l_tmpRecoveryInformation = l_tmpRecoveryStatus.split(" ");
                        
                        // Send to VOucherServer
                        if ( l_tmpRecoveryInformation[0].equals(FraudBatchRecordData.C_NOT_PROCESSED) )
                        {
                            // For available vouchers a ChangeVoucherState message should also be sent to the
                            // VoucherServer to change the voucher state from available to damaged (state= 2).
                            if (PpasDebug.on)
                            {
                                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                                 PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11240, this,
                                                 "Before sending request to changeVoucher status to DAMAGED");
                            }      
                            
                            if ( i_fraudReturnsBasicTestData == null )
                            {
                                // Not recovery mode - change status on Voucher Server to DAMAGED
                                try
                                {
                                    i_voucherService.changeVoucherState(
                                                         i_ppasRequest,
                                                         C_TIMEOUT,
                                                         p_voucherSerialNumber,
                                                         VoucherStatus.C_AVAILABLE, //p_oldState,
                                                         VoucherStatus.C_DAMAGED ); //p_newState)
                                    p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);
                                }
                                catch ( PpasServiceException l_cvse )
                                {
                                    //PpasServiceMsg#C_KEY_VOUCHER_EXTERNAL};
                                    //PpasServiceMsg#C_KEY_VOUCHER_NOT_FOUND};
                                    //PpasServiceMsg#C_KEY_INVALID_SERIAL_NUMBER};
                                    //PpasServiceMsg#C_KEY_INVALID_VOUCHER_TRANSITION};
                                    failedTestException(l_cvse); 
                                    setErrorMessage(p_record, l_cvse.getFullMessage());
                                }
                            }                     
                            else
                            {
                                // BASIC TEsT.. not processed, just flag record as processed
                                p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);
                            }
                         }
                         // Save status for recovery
                         break;
                   
                 
                    case C_STATUS_ASSIGNED: // Used voucher    
                        l_usedRecord = i_usedVouchers.createUsedRecord(
                                           p_voucherSerialNumber,
                                           p_firstUsedDate,
                                           p_record.getCurrency(),
                                           p_record.getFormattedAmount());
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11250, this,
                                             "status = ASSIGNED, USEDRecord = ]"+l_usedRecord+"[");
                        }
                        p_record.setUsedVoucherReportRecord(l_usedRecord);
                        p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);
                        break;

                    case C_STATUS_DAMAGED:
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11260, this,
                                             "status = DAMAGED - log to errorlog, no counting...");
                        }
                        
                        if ( p_record.getRecordType() == C_STATUS_AVAILABLE)
                        {
                            if (PpasDebug.on)
                            {
                                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                                 PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11270, this,
                                                 "***RECOVERY RUN*** - create an available record!!! ");
                            }
                            
                            // Running in recovery mode - first processing found this record "available"
                            // Therefore it must be "available" in this processing too
                            i_available.createAvailableRecord(p_group, p_record);                                
                        }
                        else
                        {
                            // First run - found to be damaged
                            if (PpasDebug.on)
                            {
                                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                                 PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11280, this,
                                                 "First run - found to be damaged....");
                            }

                            setErrorMessage(p_record, C_VOUCHER_HAS_STATUS_DAMAGED);
                        }
                        break;
                         
                    case C_STATUS_NOT_EXIST: // Voucher does not exists
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11290, this,
                                             "status = NOT_EXIST, Voucher does not exist");
                        }
                        setErrorMessage(p_record, C_VOUCHER_DOES_NOT_EXIST );
                        break;
                         
                    default:
                        if (PpasDebug.on)
                        {
                            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE,
                                             PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11300, this,
                                             "DEFAULT handling, status = "+p_status);
                        }
                        setErrorMessage(p_record, C_INVALID_VOUCHER_STATUS + p_status + ">");

                        break;
                        
                } // end switch for voucherStatus
                
                // Save status if recovery will be needed in the future
                p_record.updateRecoveryStatus(p_status);
              
            }
            else
            {
                // It was of voucher group "EX"
                p_record.setExceptionDetails(C_ALREADY_EXPIRED);
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 11310, this,
                                     "ALREADY_EXPIRED...."+p_record.getExceptionDetails());
                }
                p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);
            
            }
        } // if duplicate...
        else
        {
            // It was a duplicate input line for the VS
//            p_record.setExceptionDetails(C_ALREADY_EXPIRED); duplicate already set
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 11310, this,
                                 "DUPLICATE row..."+p_record.getExceptionDetails());
            }
            p_record.setRecoveryStatus(BatchRecordData.C_SUCCESSFULLY_PROCESSED);        
        }
        
        // This record is fully processed
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 11320, this,
                             BatchConstants.C_LEAVING + C_METHOD_handleVoucherServerData + 
                             p_record.dumpRecord());
        }  

        return p_record;
    } // End of handleVoucherServerData(.......)
    
    
        
    /**
     * If the voucher is of a non-standard type, for example ValueVoucher or a SuperValueVoucher
     * it doesn't have an amount and currency. These values are null.
     * In this case set the amount to zero and currency to 'default' currency.
     * 
     * The 'default' amount and currency are stored in the FraudBatchDataRecord.
     * 
     * @param p_record The FraudBatchRecordData.
     * @param p_amount The amount.
     * @param p_currency The current currency.
     * @return TRUE if the default values were successfully stored in the RecordData
     *         FALSE if it the default amount couldn't be created.
     */
    private boolean setDefaultAmount( FraudBatchRecordData p_record,
                                      Money                p_amount,
                                      PpasCurrency         p_currency )
    {
        /** Default amount for non-stanard vouchers. */
        final String L_DEFAULT_AMOUNT   = "0.0";
        /** Default currency for non-standard vouchers. */
        final long   L_DEFAULT_CURRENCY = 0L;

        Money        l_value            = p_amount;
        PpasCurrency l_currency         = p_currency;
 
        if ( l_currency == null )
        {
            l_currency = i_ppasContext.getCurrency(L_DEFAULT_CURRENCY);              
        }
        
        if ( l_value == null )
        {
            try
            {
                // Set the amount == 0, so it can behave like a standard voucher in the following code.
                l_value = i_moneyFormat.parse(L_DEFAULT_AMOUNT, l_currency);
            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_ST_TRACE, 
                                     PpasDebug.C_ST_TRACE, C_CLASS_NAME, 11330, this,
                                     "Could not create a default amount = 0");
                }
                return false;
            }
        }

        //OK - store default values in the RecordData
        p_record.setAmount(i_moneyFormat.format(l_value));
        p_record.setCurrency(l_currency.getCurrencyCode());
       
        return true;
        
    } // End of setDefaultAmount(...)
 
 
        
    private void setErrorMessage( FraudBatchRecordData p_record,
                                  String               p_message )
    {
        StringBuffer l_tmp = new StringBuffer();
        
        p_record.setCorruptLine( true );
        l_tmp.append(p_message);
        l_tmp.append(FraudReturnsBatchReader.C_DELIMITER_ERROR_LOG);
        l_tmp.append(p_record.getInputLine());
        p_record.setErrorLine( l_tmp.toString());
        p_record.setRecoveryStatus(BatchConstants.C_NOT_PROCESSED);
        
        return;
        
    } // End of setErrorMessage(..)

    
    
    private void failedTestException(Exception p_ex)
    {
        StringBuffer l_tmp = new StringBuffer();
        
        if (p_ex instanceof Exceptionable)
        {
            Exceptionable l_ex = (Exceptionable)p_ex;
            l_tmp.append("[ASCS EXCEPTION]: " + l_ex.getMessage() + l_ex.getMessageText());

            if (l_ex.getSourceException() != null)
            {
                l_tmp.append("Orginating...");
                failedTestException(l_ex.getSourceException());
            }
        }
        else
        {
            l_tmp.append("[OTHER EXCEPTION]: " + p_ex);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME, 11340, this,
                             l_tmp.toString());
        }

        i_logger.logMessage( new LoggableEvent( l_tmp.toString(),
                                                LoggableInterface.C_SEVERITY_ERROR) );
        
        return;

    } // End of failedTestException(.)

    

    private boolean isDuplicate( FraudBatchRecordData p_record )
    {
        // ===============================================================
        // The record that shall be processed cannot be 'duplicate within
        // the current input file ( the exceptionDetails cannot be set by the reader )
        // ===============================================================
        return ( p_record.getExceptionDetails() != null );
    }

    
    
    
    //////////////////////////////////////////////////////////////////////////////////////
    //
    // PRIVATE INNER HELPER CLASSes
    //
    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * This class initializes a Hashtable with information about dummy MSISDNs, this Hashtable is
     * used for testing all following indata records, if their vouchers have been assign to a 
     * DUMMY MSISDNs.
     */
    class DummyMSISDNs extends ReportRecord
    {
        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_CLASS_NAME = "DummyMSISDNs";

        /** The Hashtable with information about the defined DUMMY MSISDNs. */
        private Hashtable i_dummyMSISDNs = null;

        /**
         * Public constructor.
         */
        public DummyMSISDNs()
        {
            super();
            initializeDummyMsisdns();
        }
        

        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_initializeDummyMsisdns = "initializeDummyMsisdns";
        /**
         * Reads the dummy file and saves information in the Hashtable.
         */
        private void initializeDummyMsisdns()
        {
            /** Name of the file defining all dummy msisdns. Value is @value. */
            final String   L_DUMMY_MSISDNS_FILENAME      = "dummy_msisdn.dba";
            
            BufferedReader l_dummyMsisdns                = null;
            String         l_inputDirectory              = null;
            String         l_fullPathNameDummyMsisdnFile = null;
            StringBuffer   l_tmp                         = new StringBuffer();
            String         l_record                      = null;
            
            i_dummyMSISDNs = new Hashtable();
            
            try
            {
                // Assume that the "Dummy Msisdn file" is found at the input directory
                // Get input input file names.
                // The Map parameter for "input file name" keeps the name of the input directory
                l_inputDirectory = (String)i_params.get(BatchConstants.C_KEY_INPUT_DIRECTORY_NAME);
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 11350, this,
                                     C_METHOD_initializeDummyMsisdns + " inputDirectory=" + l_inputDirectory);
                }

                l_tmp.append(l_inputDirectory);
                l_tmp.append("/");
                l_tmp.append(L_DUMMY_MSISDNS_FILENAME);
                l_fullPathNameDummyMsisdnFile = l_tmp.toString();
                l_dummyMsisdns = new BufferedReader(new FileReader(new File(l_fullPathNameDummyMsisdnFile)));
                
                // Save dummy numbers in the hashtable
                while ( l_dummyMsisdns.ready() )
                {
                    l_record = l_dummyMsisdns.readLine();
                    
                    if ( !i_dummyMSISDNs.containsKey(l_record))
                    {
                        // Not already in the dummy list - add this one
                        i_dummyMSISDNs.put(l_record, l_record);
                    }
                } // end while loop

                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 11360, this,
                                     "**number of dummy MSISDNs :" + i_dummyMSISDNs.size());
                }
            } // end try block
            catch (FileNotFoundException e)
            {
                i_logger.logMessage( new LoggableEvent( C_DUMMY_MSISDNS_FILE_NOT_FOUND,
                                                        LoggableInterface.C_SEVERITY_WARNING) );

                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME, 11370, this,
                                    "Dummy MSISDNs file not found " + C_METHOD_initializeDummyMsisdns );
                }
            }        

            catch (IOException e)
            {
                i_logger.logMessage( new LoggableEvent( C_DUMMMY_MSISDN_IO_EXCEPTION,
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME, 11380, this,
                                    "IO exeption reading dummy MSISDNs file " +
                                    C_METHOD_initializeDummyMsisdns );
                }

            }
            finally
            {
                try
                {
                    if (l_dummyMsisdns != null)
                    {
                        l_dummyMsisdns.close();
                    }
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                        C_CLASS_NAME, 11390, this,
                                        "Dummy MSISDNs are : " + i_dummyMSISDNs );
                    }
                }
                catch (IOException e1)
                {
                    i_logger.logMessage( new LoggableEvent( C_DUMMY_MSISDN_FILE_CANNOT_BE_CLOSED,
                                                            LoggableInterface.C_SEVERITY_ERROR) );
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                        C_CLASS_NAME, 11400, this,
                                        "Problems closing dummy file " + C_METHOD_initializeDummyMsisdns );
                    }
                }
            } // End of finally block
            
            return;
            
        } // End of initializeDummyMsisdns()
        

        
        /**
         * Checks if a MSISDN is defined as a DUMMY MSISDN.
         * @param p_MSISDN The MSISDN to check
         * @return TRUE if the MSISDN is a DUMMY.
         */
        public boolean isDummy( String p_MSISDN )
        {
            return i_dummyMSISDNs.containsKey(p_MSISDN);
        } // End of isDummy(.)
        

        
        /** 
         * Creates a record for the DUMMY report.
         * @param p_voucherSerialNumber The Voucher serial number.
         * @param p_firstUsedDate First date of use for the voucher.
         * @param p_originalCurrency Original currency.
         * @param p_originalAmount Original amount.
         * @return
         */
        public String createDummyRecord( String       p_voucherSerialNumber,
                                         PpasDateTime p_firstUsedDate,
                                         String       p_originalCurrency,
                                         String       p_originalAmount)
        {
            return createDefaultReportRecord( p_voucherSerialNumber,
                                              p_firstUsedDate,
                                              p_originalCurrency,
                                              p_originalAmount );
        } // End of createDummyRecord

    } // End of class FraudReturnsBatchProcessor.DummyMSISDNs

    
    
    /**
     * This is a help class to create records for the USED report. 
     */
    class UsedVouchers extends ReportRecord
    {
        /**
         * Contructor.
         */
        public UsedVouchers()
        {
            super();
        }
        
        
        /** Creates a record for the USED report. */
        public String createUsedRecord( String       p_voucherSerialNumber,
                                        PpasDateTime p_firstUsedDate,
                                        String       p_originalCurrency,
                                        String       p_originalAmount)
        {
            return createDefaultReportRecord( p_voucherSerialNumber,
                                              p_firstUsedDate,
                                              p_originalCurrency,
                                              p_originalAmount );
        } // End of createdUsedRecord(....)
        
    } // End of class FraudReturnsBatchProcessor.UsedVouchers

    
    
    /** 
     * This is a help class to create records for the AVAILABLE report.
     */
    class Available extends ReportRecord
    {
        /**
         * Constructor.
         */
        public Available()
        {
            super();
        }
        
        
        
        /**
         * Creates a record for the AVAILABLE report.
         * @param p_voucherGroup Voucher group for this record.
         * @param p_record Data record with information for the different parts in the batch.
         */
        public void createAvailableRecord( String               p_voucherGroup,
                                           FraudBatchRecordData p_record)
        {
            /** Suffix for the first row in the Finance Summary Report.  Value is {@value}.*/
            final String L_AVAILABLE_VOUCHER    = "_AVAIL";
            
            StringBuffer l_tmp = new StringBuffer(100);
            
            p_record.setType(L_AVAILABLE_VOUCHER);

            l_tmp = new StringBuffer();
            l_tmp.append(p_record.getTerminalId());
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_record.getVoucherSerialNumber());
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_voucherGroup);
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_record.getInputFilename());
            l_tmp.append(".txt");
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_record.getCurrency());
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_record.getFormattedAmount());

            p_record.setAvailableVoucherReportRecord(l_tmp.toString());
            return;
            
        } // end of createAvailableRecord(..)
        
     } // End of class FraudReturnsBatchProcessor.Available
    

    
    /**
     * Help class for creating records to the reports.
     */
    abstract class ReportRecord
    {
        /**
         * Constructor.
         */
        protected ReportRecord()
        {
            super();
        }
        
        /**
         * Creates a record with "default" layout as:
         * <serial number>,<first used date>,<original currency>,<original amount>
         * 
         * @param p_voucherSerialNumber
         * @param p_firstUsedDate
         * @param p_originalCurrency
         * @param p_originalAmount
         * @return
         */
        protected String createDefaultReportRecord( String       p_voucherSerialNumber,
                                                    PpasDateTime p_firstUsedDate,
                                                    String       p_originalCurrency,
                                                    String       p_originalAmount )
        {
            StringBuffer l_tmp = new StringBuffer();
            
            l_tmp.append(p_voucherSerialNumber);
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_firstUsedDate.toString_ddMMMyyyy_HHmmss());
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_originalCurrency);
            l_tmp.append(C_REPORT_FIELD_DELIMITER);
            l_tmp.append(p_originalAmount);
            
            return l_tmp.toString();
            
        } // End of createDefaultReportRecord(....)
        
    } // End of abstract class FraudReturnsBatchProcessor.ReportRecord
 
    
} // End of class FraudReturnsBatchProcessor
