////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchReader
//      DATE            :       10-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.StatusChangeBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * The reader class for the StatusChangeBatch batch.
 * This class implements methods for reading status change orders from file. 
 * It extends the abstract class BatchFileReader that contributes with several auxilary metods.
 * The file is processed on a line-per-line basis and each line extrated
 * is evaluated and put into a StatusChangeBatchDataRecord.
 */
public class StatusChangeBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "StatusChangeBatchReader";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN    = "02";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_STATUS        = "03";
               

    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS  = 7;

    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_MSISDN            = 0;
    
    /** Index for the field Master MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NEW_STATUS        = 1;

    /** Field length of MSISDNto be changed. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH     = 15;
    
    /** Field length of new status. Value is {@value}. */
    private static final int    C_NEW_STATUS_LENGTH = 1;
    
    /** Record length. Value is {value}. */
    private static final int    C_RECORD_LENGTH     = C_MSISDN_LENGTH + C_NEW_STATUS_LENGTH;

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
   
    /** The BatchDataRecord to be sent into the Queue. */
    private StatusChangeBatchRecordData i_batchDataRecord = null;
    


    /**
     * Constructs a StatusChangeBatchReader object.
     * 
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public StatusChangeBatchReader( PpasContext      p_ppasContext,
                                    Logger           p_logger,
                                    BatchController  p_controller,
                                    SizedQueue       p_queue,
                                    Map              p_parameters,
                                    PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;
        
    } // end of public Constructor StatusChangeBatchReader(.....)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [MSISDN][NewStatus]
     * 
     * All fields are checked to be alpha numeric.
     * @return <code>true</code> When extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat = null;
        // Help variable to convert from string to Msisdn object.
        Msisdn       l_tmpMsisdn    = null;
        // Help array to keep the input line elements.
        String[]     l_recordFields = new String[C_NUMBER_OF_FIELDS];
        // Return value - assume it is a valid record.
        boolean      l_validData    = true;  
        StringBuffer l_tmpErrorLine = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10320,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
        }

        // Check record length
        if ( i_inputLine.length() == C_RECORD_LENGTH )
        {
            
            initializeRecordValidation( i_inputLine, l_recordFields, i_batchDataRecord );

            // Get the record fields and check if it is numeric/alphanumberic
            //            Start            End                  #Field        Format check                                                    Errorcode
            if ( getField(0,               C_MSISDN_LENGTH,     C_MSISDN,     BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC,                C_ERROR_NON_NUMERIC_MSISDN ) &&
                 getField(C_MSISDN_LENGTH, C_NEW_STATUS_LENGTH, C_NEW_STATUS, BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_VALID_STATUS, C_ERROR_INVALID_STATUS ) )
            {
                
                // Both fields are manatory
                if ( l_recordFields[C_MSISDN] == null ||
                     l_recordFields[C_NEW_STATUS] == null )
                {
                    l_validData = false;
                }
                
                else
                {
                    // Got a valid record
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10350,
                            this,
                            "Row: " + i_lineNumber + " ]" + i_inputLine + "[ is a valid record" );
                    }
                    
                    // Convert the MSISDN string to a Msisdn object
                    // This may give an ParseException - if so flag corrupt line!
                    l_msisdnFormat = i_context.getMsisdnFormatInput();
                    try
                    {
                        l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_MSISDN] );
                        this.i_batchDataRecord.setMsisdn(l_tmpMsisdn);                
                    }
                    catch (ParseException e)
                    {
                        l_validData = false;
                        if ( this.i_batchDataRecord.getErrorLine() == null )
                        {
                            this.i_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_MSISDN);                
                        }
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10370,
                                this,
                                "Row: " + i_lineNumber + " ]" + i_inputLine + "[ has invalid MSISDN" );
                        }
                        e.printStackTrace();
                    }
                
                    // Save new status
                    this.i_batchDataRecord.setNewStatus(l_recordFields[C_NEW_STATUS]);
                }
                             
            }
            else
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME,
                        10360,
                        this,
                        "Row: ]" + i_inputLine + "[ is a INVALID record\n" +
                                " l_lineNumber     =" + i_lineNumber + "\n" +
                                " l_msisdn         =" + l_recordFields[C_MSISDN] + "\n" +
                                " l_newStatus      =" + l_recordFields[C_NEW_STATUS]);
                }
                                                                    
                // Flag for corrupt line
                l_validData = false;
                if ( this.i_batchDataRecord.getErrorLine() == null)
                {
                    l_tmpErrorLine = new StringBuffer();
                    l_tmpErrorLine.append(C_ERROR_INVALID_RECORD_FORMAT);
                    l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmpErrorLine.append(this.i_batchDataRecord.getInputLine());
                    this.i_batchDataRecord.setErrorLine( l_tmpErrorLine.toString());
                }
                
            } // end else - illegal field format detected
                
                               
        } // end if - record length OK
        
        else
        {
            l_validData = false;
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return l_validData;
        
    } // End of method extractLineAndValidate(.)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return NULL When "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        StringBuffer l_tmp      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchDataRecord = null;

        if ( readNextLine() != null )
        {
            i_batchDataRecord = new StatusChangeBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename( i_inputFileName );
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber( i_lineNumber );
            
            // Store original data record
            i_batchDataRecord.setInputLine( i_inputLine );
            
            // Extract data from line
            if ( extractLineAndValidate() )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10410,
                        this,
                        "extractLineAndValid was successful " + i_batchDataRecord.dumpRecord() );
                }
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10420,
                        this,
                        "extractLineAndValid failed!" );
                }

                // Flag for corrupt line and set error code
                i_batchDataRecord.setCorruptLine( true );
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    l_tmp = new StringBuffer();
                    l_tmp.append(C_ERROR_INVALID_RECORD_FORMAT);
                    l_tmp.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmp.append(i_batchDataRecord.getInputLine());
                    i_batchDataRecord.setErrorLine( l_tmp.toString() );
                }
                                
            }
            
            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            i_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                i_batchDataRecord.setFailureStatus(true);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10340,
                    this,
                    "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
            }

            
        } // end if
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;
        
    } // end of method getRecord()
    




    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form: INSTALL_STATUS_yyyymmdd_sssss.DAT /IPG.
     * @param p_fileName Filename to test.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {                
        boolean  l_validFileName = true;       // Help variable - return value. Assume filename is OK
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10470,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_SCH) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_IPG) )
            {
                // OK Filename: INSTALL_STATUS_yyyymmdd_sssss.*
                // Rename the file to .IPG to indicate that processing is in progress.
                // If it is recovery mode, the file already got the "in_progress_extension"
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    if (p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_SCH))
                    {
                        renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                    else
                    {
                        renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10480,
                            this,
                            "After rename, inputFullPathName=" + i_fullPathName );
                    }
                                   
                }

            } // End if - valid filename!
      
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
                    
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10490,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
        
    } // End of isFileNameValid(.)

} // End of StatusChangeBatchReader