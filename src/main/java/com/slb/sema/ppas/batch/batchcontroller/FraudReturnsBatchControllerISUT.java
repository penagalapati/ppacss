////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       FraudReturnsBatchControllerISUT.java 
//      DATE            :       27-March-2007
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Unit test for FraudReturnsBatchController, this UT program
//                              uses the ISAPI. To test the batch's internals use the UT-
//                              program: FraudReturnsBatchControllerUT
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 27/03/07 | M. T�rnqvist  | End-to-end tests are introduced. | PpacLon#2859/11224
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/**
 * Class to unit test the Subscriber Install Status Change batch.
 */
/** Unit tests the Miscellaneous DataUpload batch controller. */
public class FraudReturnsBatchControllerISUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String     C_CLASS_NAME               = "FraudReturnsBatchControllerISUT";

	private static final String     C_REPORT_FAILURE           = 
		"Verification of FraudReturn REPORT file failed, ";

    /** The name of the Status Change batch. */
    private static final String     C_FRAUD_RETURNS_BATCH_NAME = 
    	BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS;

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String     C_FILENAME_SUCCESSFUL      = "FRAUDRETURNS_SUCCESS.DAT";

    private static final String     C_ACTIVATION_NUMBER        = "1234567890";
    
    /** Filename consist of 3 parts. <STEM>_<DDMMYYYY>_<NO ROWS>.txt
     * STEM can be a dealer specific prefix. */
    private static final String     C_STEM                     = "FRAUDUT";
    
    private static final int        C_NO_ROWS_IN_INDATA_FILE   = 52;
    
    /** Data file extension. */
    private static String           C_DATAFILE_EXTENSION       = ".txt";
    
    /** Number of dummy msisdns to generate. */
    private static int              C_NO_DUMMY_MSISDN          = 5;

    /** Indata rows consist of 3 fields, comma separated. 
     * <VOUCHER SERIAL NUMBER>,<TERMINAL ID>,<REASON CODE>. */
    private static final int        C_TERMINAL_ID              = 1234567;
    
    // NOTE - The Reason Codes are defined in the property file for this batch.
    /** Number of specified reason codes.  */
    private static final int        C_NO_SPECIFIED_REASON_CODES= 10;
    
    /**  Reason code for 'unknown reason'. */
    private static final int        C_NONE_SPECIFIED           = 0;
    /** Reason code for 'Paper jam'. */
    private static final int        C_PAPER_JAM                = 1;
    /** Reason code for 'Poor printing'. */
    private static final int        C_POOR_PRINTING            = 2;
    /** Reason code for 'Customer returns e-card'. */
    private static final int        C_CUSTOMER_RETURNS_E_CARD  = 3;
    /** Reason code for 'Printed in error'. */
    private static final int        C_PRINTED_IN_ERROR         = 4;
    /** Reason code for  'No paper in terminal'. */
    private static final int        C_NO_PAPER_IN_TERMINAL     = 5;
    /** Reason code for 'Incorrect denomination'. */
    private static final int        C_INCORRECT_DENOMINATION   = 6;
    /** Reason code for 'Incorrect operator'. */
    private static final int        C_INCORRECT_OPERATOR       = 7;
    /** Reason code for 'Failed top up'. */
    private static final int        C_FAILED_TOP_UP            = 8;
    /** Reason code for 'Applied to dummy number'. */
    private static final int        C_APPLIED_TO_DUMMY_NUMBER  = 9;
    
    /** Expected recovery information after a normal run. */
    private static final String[]   C_RECOVERY_ROWS            = {"1,OK 0",
                                                                  "2,OK 1",
                                                                  "3,ERR 2",
                                                                  "4,ERR 3",
                                                                  "5,ERR 4",
                                                                  "6,ERR 5",
                                                                  "7,ERR 6",
                                                                  "8,ERR",
                                                                  "9,OK 0",
                                                                  "10,OK",
                                                                  "11,OK 1",
                                                                  "12,OK",
                                                                  "13,OK",
                                                                  "14,OK",
                                                                  "15,OK",
                                                                  "16,OK",
                                                                  "17,OK",
                                                                  "18,OK",
                                                                  "19,OK",
                                                                  "20,OK",
                                                                  "21,OK",
                                                                  "22,OK",
                                                                  "23,OK",
                                                                  "24,OK",
                                                                  "25,OK",
                                                                  "26,OK",
                                                                  "27,OK",
                                                                  "28,OK",
                                                                  "29,OK",
                                                                  "30,OK",
                                                                  "31,OK",
                                                                  "32,OK",
                                                                  "33,OK",
                                                                  "34,OK",
                                                                  "35,OK",
                                                                  "36,OK",
                                                                  "37,OK",
                                                                  "38,OK",
                                                                  "39,OK",
                                                                  "40,OK",
                                                                  "41,OK",
                                                                  "42,OK",
                                                                  "43,OK",
                                                                  "44,OK",
                                                                  "45,OK",
                                                                  "46,OK",
                                                                  "47,OK",
                                                                  "48,OK",
                                                                  "49,OK",
                                                                  "50,OK",
                                                                  "51,OK",
                                                                  "52,OK"};

    /** Reason code texts. (defined in the property file.) */
    private static final String[]   C_REASON_CODES_TEXT        = { "None specified",
    	                                                           "Paper jam",
    	                                                           "Poor printing",
    	                                                           "Customer returns e card",
    	                                                           "Printed in Error",
    	                                                           "No paper in terminal",
    	                                                           "Incorrect Denomination",
    	                                                           "Incorrect Operator",
    	                                                           "Failed top up",
    	                                                           "Applied to Dummy Number" };

    /** Exception details message for an expired voucher. */
    private static final String     C_ALREADY_EXPIRED          = "Already Expired";
    /** Exception details message for a duplicate voucher. */
    private static final String     C_DUPLICATE                = "Duplicate";
    /** Empty line, in reports. */
    private static final String     C_EMPTY_LINE               = "";
    
    /** VS test data - Available voucher. */
    private static final String[]   C_VS_AVAILABLE             = {"100000000750",
    	                                                          "100000000751"};
    /** VS test data - Used voucher. */
    private static final String[]   C_VS_USED                  = {"100000000800",
    	                                                          "100000000801"};
    /** VS test data - Damaged voucher. */
    private static final String     C_VS_DAMAGED               = "100000000900";
    /** VS test data - Stolen voucher. */
    private static final String     C_VS_STOLEN                = "100000001000";
    /** VS test data - Pending voucher. */
    private static final String     C_VS_PENDING               = "100000001100";
    /** VS test data - Unavailable voucher. */
    private static final String     C_VS_UNAVAILABLE           = "100000001200";
    /** VS test data - Reserved voucher. */
    private static final String     C_VS_RESERVED              = "100000001300";
    /** VS test data - Not existing voucher. */
    private static final String     C_VS_NOT_EXISTING          = "199999999999";
    
    /** ACIN test data - Used serial numbers. */
    private static final String[]   C_ACIN_USED                = {"100000000700", 
    	                                                          "100000000701",
    	                                                          "100000000702",
    	                                                          "100000000703",
    	                                                          "100000000704",
    	                                                          "100000000705",
    	                                                          "100000000706",
    	                                                          "100000000707",
    	                                                          "100000000708",
    	                                                          "100000000709",
    	                                                          "100000000710",
    	                                                          "100000000711",
    	                                                          "100000000712",
    	                                                          "100000000713",
    	                                                          "100000000714",
    	                                                          "100000000715"};
    
    /** EXRE test data - Used serial numbers. */
    private static final String[]   C_EXRE_USED                = {"100000000720", 
                                                                  "100000000721",
                                                                  "100000000722",
                                                                  "100000000723",
                                                                  "100000000724",
                                                                  "100000000725",
                                                                  "100000000726",
                                                                  "100000000727",
                                                                  "100000000728",
                                                                  "100000000729",
                                                                  "100000000730",
                                                                  "100000000731",
                                                                  "100000000732",
                                                                  "100000000733",
                                                                  "100000000734",
                                                                  "100000000735"};

    /** Counter for vouchers assign to dummy MSISDNs, note test records defined as both dummy and expired
     *  shall be treated as expired vouchers by the batch.  */
    private static int   i_dummyCounter     = 0;
    /** Counter for expired vouchers. */
    private static int   i_expiredCounter   = 0;
    /** Counter for duplicate voucher records. */
    private static int   i_duplicateCounter = 0;
    /** Counter for used vouchers, in ACIN, EXRE and VS. */
    private static int   i_usedCounter      = 0;
    /** Number of available vouchers. */
    private static int   i_noAvailableVS    = C_VS_AVAILABLE.length;
	
    /** Array of counters for the different reason codes. */
    private static int[] i_reasonCodeCounter = new int[C_NO_SPECIFIED_REASON_CODES];
    
    
    /** Starting index for routing MSISDNs. */
    private static int   i_serialNumberIndex;

    /** List containing defined msisdns for the DUMMY subscribers. */
    private ArrayList i_dummyMsisdns = new ArrayList();
    /** List containing defined Custid's for the DUMMY subscribers. */
    private ArrayList i_dummyCustid  = new ArrayList();

    /** Array with generated VS test data. */
    private Object[]  i_vsTestData = 
    { 
    	//                       Serial Number      Terminal Id    Reason code               Expired Dummy  Duplicate
    	// --------------------------------------------------------------------------------------------------------------------------
	    new FraudReturnsTestRow(C_VS_AVAILABLE[0], C_TERMINAL_ID, C_NONE_SPECIFIED,          false,  false, false),
	    new FraudReturnsTestRow(C_VS_USED[0],      C_TERMINAL_ID, C_PAPER_JAM,               false,  false, false),
	    new FraudReturnsTestRow(C_VS_DAMAGED,      C_TERMINAL_ID, C_POOR_PRINTING,           false,  false, false),
	    new FraudReturnsTestRow(C_VS_STOLEN,       C_TERMINAL_ID, C_CUSTOMER_RETURNS_E_CARD, false,  false, false),
	    new FraudReturnsTestRow(C_VS_PENDING,      C_TERMINAL_ID, C_PRINTED_IN_ERROR,        false,  false, false),
	    new FraudReturnsTestRow(C_VS_UNAVAILABLE,  C_TERMINAL_ID, C_NO_PAPER_IN_TERMINAL,    false,  false, false),
	    new FraudReturnsTestRow(C_VS_RESERVED,     C_TERMINAL_ID, C_INCORRECT_DENOMINATION,  false,  false, false),
	    new FraudReturnsTestRow(C_VS_NOT_EXISTING, C_TERMINAL_ID, C_INCORRECT_OPERATOR,      false,  false, false),	    
	    new FraudReturnsTestRow(C_VS_AVAILABLE[1], C_TERMINAL_ID, C_FAILED_TOP_UP,           false,  false, true),
	    new FraudReturnsTestRow(C_VS_USED[1],      C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, false,  false, true)    
    };
    
    /** Array with generated ACIN test data. */
    private Object[] i_acinTestData =
    {
    	//                      Serial Number    Terminal Id    Reason code                Expired Dummy  Duplicate
    	// --------------------------------------------------------------------------------------------------------------------------
	    new FraudReturnsTestRow(C_ACIN_USED[0],  C_TERMINAL_ID, C_NONE_SPECIFIED,          false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[1],  C_TERMINAL_ID, C_PAPER_JAM,               false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[2],  C_TERMINAL_ID, C_POOR_PRINTING,           false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[3],  C_TERMINAL_ID, C_CUSTOMER_RETURNS_E_CARD, false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[4],  C_TERMINAL_ID, C_PRINTED_IN_ERROR,        false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[5],  C_TERMINAL_ID, C_NO_PAPER_IN_TERMINAL,    false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[6],  C_TERMINAL_ID, C_INCORRECT_DENOMINATION,  false,  false, false),
	    new FraudReturnsTestRow(C_ACIN_USED[7],  C_TERMINAL_ID, C_INCORRECT_OPERATOR,      false,  false, false),	    
	    new FraudReturnsTestRow(C_ACIN_USED[8],  C_TERMINAL_ID, C_FAILED_TOP_UP,           false,  false, false),	    
	    new FraudReturnsTestRow(C_ACIN_USED[9],  C_TERMINAL_ID, C_NONE_SPECIFIED,          true,   false, false),    
	    new FraudReturnsTestRow(C_ACIN_USED[10], C_TERMINAL_ID, C_NONE_SPECIFIED,          true,   false, true),
	    new FraudReturnsTestRow(C_ACIN_USED[11], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, true,   true,  false),
	    new FraudReturnsTestRow(C_ACIN_USED[12], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, true,   true,  true),	    
	    new FraudReturnsTestRow(C_ACIN_USED[13], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, false,  true,  false),    
	    new FraudReturnsTestRow(C_ACIN_USED[14], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, false,  true,  true),	    
	    new FraudReturnsTestRow(C_ACIN_USED[15], C_TERMINAL_ID, C_NONE_SPECIFIED,          false,  false, true)};  
 
    /** Array with generated EXRE test data. */
    private Object[] i_exreTestData =
    {
    	//                      Serial Number    Terminal Id    Reason code                Expired Dummy  Duplicate
    	// --------------------------------------------------------------------------------------------------------------------------
	    new FraudReturnsTestRow(C_EXRE_USED[0],  C_TERMINAL_ID, C_NONE_SPECIFIED,          false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[1],  C_TERMINAL_ID, C_PAPER_JAM,               false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[2],  C_TERMINAL_ID, C_POOR_PRINTING,           false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[3],  C_TERMINAL_ID, C_CUSTOMER_RETURNS_E_CARD, false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[4],  C_TERMINAL_ID, C_PRINTED_IN_ERROR,        false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[5],  C_TERMINAL_ID, C_NO_PAPER_IN_TERMINAL,    false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[6],  C_TERMINAL_ID, C_INCORRECT_DENOMINATION,  false,  false, false),
	    new FraudReturnsTestRow(C_EXRE_USED[7],  C_TERMINAL_ID, C_INCORRECT_OPERATOR,      false,  false, false),	    
	    new FraudReturnsTestRow(C_EXRE_USED[8],  C_TERMINAL_ID, C_FAILED_TOP_UP,           false,  false, false),	    
	    new FraudReturnsTestRow(C_EXRE_USED[9],  C_TERMINAL_ID, C_NONE_SPECIFIED,          true,   false, false),    
	    new FraudReturnsTestRow(C_EXRE_USED[10], C_TERMINAL_ID, C_NONE_SPECIFIED,          true,   false, true),
	    new FraudReturnsTestRow(C_EXRE_USED[11], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, true,   true,  false),
	    new FraudReturnsTestRow(C_EXRE_USED[12], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, true,   true,  true),	    
	    new FraudReturnsTestRow(C_EXRE_USED[13], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, false,  true,  false),    
	    new FraudReturnsTestRow(C_EXRE_USED[14], C_TERMINAL_ID, C_APPLIED_TO_DUMMY_NUMBER, false,  true,  true),	    
	    new FraudReturnsTestRow(C_EXRE_USED[15], C_TERMINAL_ID, C_NONE_SPECIFIED,          false,  false, true)};  

    /** Expected exeption details. */
    private static Vector i_expectedExceptionDetails = new Vector();
    /** Expected used vouchers. */
    private static Vector i_usedVouchers             = new Vector();
    /** Expected vouchers assigned to dummy MSISDNs. */
    private static Vector i_vouchersAssignedToDummy  = new Vector();
    
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>FraudReturnsBatchControllerISUT</code> instance to be used for
     * unit tests of the <code>MsisdnRoutingProvisioningBatchController</code> class.
     * 
     * @param p_name  the name of the current test.
     */
    public FraudReturnsBatchControllerISUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testFraudReturns_Successful =
        "testFraudReturns_Successful";
    /**
     * Performs a fully functional test of a successful 'MSISDN Routing Provisioning'.
     * A test file with a valid name containing 10 valid Msisdn Routing Provisioning 
     * records are created and placed in the batch indata directory. The MSISDN Routing 
     * Provisioning batch is started in order to process the test file and update the
     * MSISDN table.
     *
     * @ut.when        A test file containing 52 Fraud Return records are
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        The EXRE_EXTERNAL_REFILL table will have 16 new rows.
     *                 The ACIN_ACCESSORY_INSTALL table will have 16 new rows.
     *                 29 new subscribers are created.
     *                 New record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 A recovery file is created, containg 52 rows, where 6 shall be
     *                 ERR-records.
     *                 Several report files will be created:
     *                 FRAUDUT_yyyymmdd_52_AVAIL.TXT
     *                 FRAUDUT_yyyymmdd_52_DUMMY.TXT
     *                 FRAUDUT_yyyymmdd_52_ERRORS.log
     *                 FRAUDUT_yyyymmdd_52_REPORT.TXT
     *                 FRAUDUT_yyyymmdd_52_USED.TXT
     *                 RET_SUMMARY_yyyymmdd.TXT
     *                 The test file will be renamed with a new extension, DNE. 
     * 
     * @ut.attributes  +f
     */
    public void testFraudReturns_Successful()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 46;
    	final int      L_NO_ERRORNOUS_RECORDS  = 6;
        final String[] L_REPORT_ROWS           = {getTrailerRecord(L_NO_SUCCESSFUL_RECORDS)};

        beginOfTest(C_METHOD_testFraudReturns_Successful);

        
        FraudReturnsTestDataTT l_testDataTT =
            new FraudReturnsTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                        UtilTestCaseTT.c_logger,
                                        CommonTestCaseTT.c_ppasContext,
                                        C_FRAUD_RETURNS_BATCH_NAME,
                                        c_batchInputDataFileDir,
                                        C_FILENAME_SUCCESSFUL,
                                        JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                        BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                        L_NO_SUCCESSFUL_RECORDS,
                                        L_NO_ERRORNOUS_RECORDS );

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(C_RECOVERY_ROWS );

        // Starting index for Voucher serial number. 
        i_serialNumberIndex          = 0;
        
        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }

    

    // =========================================================================
    // == Public class method(s).                                             ==
    // =========================================================================
    
    /** 
     * Get the number of expected Vouchers assigned to a dummy MSISDN. 
     * @return Number of expected Vouchers assigned to a dummy MSISDN.
     */
	static public int getNoDummy()
	{
		return i_dummyCounter;
	}
	
	/**
	 * Get the number of expected expired vouchers.
	 * @return Number of expected expired vouchers.
	 */
	static public int getNoExpired()
	{
		return i_expiredCounter;
	}
	
	/**
	 * Get number of expected duplicate voucher records.
	 * @return Number of expected duplicate voucher records.
	 */
	static public int getNoDuplicate()
	{
		return i_duplicateCounter;
	}

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite( FraudReturnsBatchControllerISUT.class );
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println( "Parameters are: " + p_args );
        junit.textui.TestRunner.run( suite() );
    }
    

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>FraudReturnsBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>FraudReturnsTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController( BatchTestDataTT p_batchTestDataTT )
    {
        FraudReturnsTestDataTT l_testDataTT = (FraudReturnsTestDataTT)p_batchTestDataTT;
        
        return createFraudReturnsBatchController( l_testDataTT.getTestFilename() );
    }


    /**
     * Returns the required additional properties layers for the status change batch.
     * 
     * @return the required additional properties layers for the misc data upload batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_vfr";
    }


    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();

        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty( BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1" );

    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>FraudReturnsBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>FraudReturnsBatchController</code> class.
     */
    private FraudReturnsBatchController createFraudReturnsBatchController( String p_testFilename )
    {
        FraudReturnsBatchController l_FraudReturnsBatchController = null;
        HashMap                     l_batchParams                 = new HashMap();
        
        l_batchParams.put( BatchConstants.C_KEY_INPUT_DIRECTORY_NAME,
                           c_batchInputDataFileDir.getAbsolutePath() );

        try
        {
            l_FraudReturnsBatchController =
                new FraudReturnsBatchController( CommonTestCaseTT.c_ppasContext,
                                                 BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                                 super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                                 C_CLASS_NAME,
                                                 l_batchParams );
        }
        catch (Exception l_Ex)
        {
            sayTime( "***ERROR: Failed to create a 'FraudReturnsBatchController' instance!" );
            super.failedTestException( l_Ex );
        }

        return l_FraudReturnsBatchController;
    }

    
    
    //==========================================================================
    // Inner class(es).
    //==========================================================================
    private class FraudReturnsTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_VOUCHER_SERIAL  = "<VOUCHER_SERIAL>";
        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_VOUCHER_SERIAL    = "^(" + C_TEMPLATE_DATA_TAG_VOUCHER_SERIAL + ").*$";

        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;
        
        /** Date extracted from the indata filename. */
        private String  i_filenameDate         = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>FraudReturnsTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private FraudReturnsTestDataTT( PpasRequest p_ppasRequest,
                                        Logger      p_logger,
                                        PpasContext p_ppasContext,
                                        String      p_batchName,
                                        File        p_batchInputFileDir,
                                        String      p_templateTestFilename,
                                        int         p_expectedJobExitStatus,
                                        char        p_expectedBatchJobControlStatus,
                                        int         p_expectedNoOfSuccessRecords,
                                        int         p_expectedNoOfFailedRecords )
        {
            super( p_ppasRequest,
                   p_logger,
                   p_ppasContext,
                   p_batchName,
                   p_batchInputFileDir,
                   p_expectedJobExitStatus,
                   p_expectedBatchJobControlStatus,
                   p_expectedNoOfSuccessRecords,
                   p_expectedNoOfFailedRecords );

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile( C_REGEXP_DATA_TAG_VOUCHER_SERIAL );           
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
        	String l_testFilename = createTestDataFilename( C_STEM,
                                                            C_NO_ROWS_IN_INDATA_FILE, 
                                                            C_DATAFILE_EXTENSION);
            createTestFile(l_testFilename, C_FILENAME_SUCCESSFUL);
            i_testFilename = l_testFilename;
        }


        
        
        /**
         * Returns the expected batch job control data as a <code>BatchJobControlData</code> object.
         * NOTE this batch does not have special filedate since many files from one directory
         * can be processed in one batch run
         * 
         * @param p_batchController  the currently tested <code>BatchController</code> object.
         * 
         * @return the expected batch job control data as a <code>BatchJobControlData</code> object.
         */
        protected BatchJobControlData getExpectedBatchJobControlData(BatchController p_batchController)
        {
            BatchJobControlData l_batchJobControlData = 
            	super.getExpectedBatchJobControlData(p_batchController);
            
            l_batchJobControlData.setFileDate(new PpasDate(""));
            
            return l_batchJobControlData;
        }

        
        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataRow  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags( String p_dataRow )
        {
            final int    L_DATA_TAG_GROUP_NUMBER  = 1;

            StringBuffer l_dataRowSB              = null;
            Matcher      l_matcher                = null;
            String       l_tmpVoucherSerialNumber = null;
            int          l_tagBeginIx             = 0;
            int          l_tagEndIx               = 0;            
            String       l_tmpRow                 = p_dataRow;
            
            
            if (p_dataRow != null)
            {
                l_dataRowSB = new StringBuffer( l_tmpRow );
                l_matcher   = i_msisdnDataTagPattern.matcher( l_tmpRow );
                if (l_matcher.matches())
                {
                    l_tagBeginIx              = l_matcher.start( L_DATA_TAG_GROUP_NUMBER );
                    l_tagEndIx                = l_matcher.end( L_DATA_TAG_GROUP_NUMBER );
                    l_tmpVoucherSerialNumber  = (String)getSubscriberData( i_serialNumberIndex++ );
                    l_dataRowSB.replace( l_tagBeginIx, l_tagEndIx, l_tmpVoucherSerialNumber );
                }
            }

            return l_dataRowSB.toString();
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
        	say("*** No verification of the database needed for the FraudReturn batch. ***");
        }

        /**
         * Returns the date used in the generated report files.
         * @return String with the date from the generated report filenames.
         */
        private String getFilenameDate()
        {
        	return i_filenameDate;
        }
        
        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
        	final String   L_PATTERN_DATE          = "yyyymmdd";        	
        	final String[] L_EXPECTED_REPORT_FILES = { "FRAUDUT_yyyymmdd_52_AVAIL.TXT",
        		                                       "FRAUDUT_yyyymmdd_52_DUMMY.TXT",
        		                                       "FRAUDUT_yyyymmdd_52_ERRORS.log",
        		                                       "FRAUDUT_yyyymmdd_52_REPORT.TXT",
        		                                       "FRAUDUT_yyyymmdd_52_USED.TXT",
        		                                       "RET_SUMMARY_yyyymmdd.TXT" };
    		
        	say("*** START to verify all the generated FraudReturnBatch's report files. ***");

        	Vector   l_expectedReportFiles  = new Vector();
            String   l_tmp                  = null;

            
        	// Get all report filenames
        	String[] l_generatedReportFiles = c_batchReportDataFileDir.list();
        	
        	assertEquals( C_REPORT_FAILURE + "unexpected number of report files generated.",
			              L_EXPECTED_REPORT_FILES.length, l_generatedReportFiles.length );

        	
        	// Compare generated report filenames with the expected filenames.
    		for ( int i = 0; i < L_EXPECTED_REPORT_FILES.length; i++ )
    		{
    			l_tmp = L_EXPECTED_REPORT_FILES[i].replaceFirst( L_PATTERN_DATE, getFilenameDate() );
    			l_expectedReportFiles.add( l_tmp );
    		}        		        	
        	for ( int i = 0; i < l_generatedReportFiles.length; i++ )
        	{
        		assertTrue( C_REPORT_FAILURE + "unexpected filename found: " + l_generatedReportFiles[i],
        				    l_expectedReportFiles.contains(l_generatedReportFiles[i]));
        	}

        	// Check the generated FraudReturns report file one by one.        	
            verifyErrorLog();
        	verifyGeneratedReport();
        	verifyUsedReport();
            verifyDUMMYReport();
        	verifyAvailableReport();
        	verifySummaryReport();
        	
        	say("*** SUCCEEDED to verify all the generated FraudReturnBatch's report files. ***");

        }

        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyErrorLog() throws IOException
        {
        	final String   L_ERROR_LOG_EXTENSION = "_ERRORS.log";
            final String[] L_EXPECTED_ERROR_MESSAGES =
                {"<Voucher has status DAMAGED>, inputLine: 100000000900,1234567,2",
        	     "<Invalid voucher status 3>, inputLine: 100000001000,1234567,3",
        	     "<Invalid voucher status 4>, inputLine: 100000001100,1234567,4",
        	     "<Invalid voucher status 5>, inputLine: 100000001200,1234567,5",
        	     "<Invalid voucher status 6>, inputLine: 100000001300,1234567,6",
        	     "<Serial number not found on Voucher Server>, inputLine: 199999999999,1234567,7"};

            Vector   l_errorLogMessages     = null;
        	String   l_foundErrorMessage    = null;
			String[] l_tmp                  = null;
			int      l_reasonCodeIndex      = 0;

        	say("*** START to verify Error log. ***");
        	
        	assertEquals( C_REPORT_FAILURE + "mismatch between number of expected logmessages in errorlog",
        			      L_EXPECTED_ERROR_MESSAGES.length, i_expectedNoOfFailedRecords);

        	try
        	{
            	l_errorLogMessages = readNewFile( c_batchReportDataFileDir, 
            			                          i_testFilename, 
            			                          "\\.txt", 
            			                          L_ERROR_LOG_EXTENSION );
            	
           		for ( int i = 0; i < l_errorLogMessages.size(); i++)
        		{
        			l_foundErrorMessage    = (String)l_errorLogMessages.get(i);
        			assertTrue( C_REPORT_FAILURE + "got message : " + l_foundErrorMessage +
        					    " but expected:" + L_EXPECTED_ERROR_MESSAGES[i], 
        					    l_foundErrorMessage.equals(L_EXPECTED_ERROR_MESSAGES[i]));
        			
        			// Check reasoncode for failing record
                    // To get counters rightdecrease the counter for the failed record's
        			// reason code
        			l_tmp             = l_foundErrorMessage.split(",");
        			l_reasonCodeIndex = Integer.parseInt( l_tmp[l_tmp.length-1] );
        			i_reasonCodeCounter[l_reasonCodeIndex]--;
        		}  
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught in verifyErrorLog(), "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of Error log done. ***");
        	}
        }

        
        
        /**
         * Verifies that the generated report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        private void verifyGeneratedReport()
        {
        	final String   L_REPORT_FILE_EXTENSION               = "_REPORT.TXT";

        	final int      L_LINE_NO_RECORDS                     = 0;
        	final String   L_HEADER_NO_RECORDS                   = "Number of records in File";     	

        	final int      L_LINE_NO_AVAILABLE_VOUCHERS          = 1;
        	final String   L_HEADER_NO_AVAILABLE_VOUCHERS        = "Available Vouchers";        	

        	final int      L_LINE_NO_USED_VOUCHERS               = 2;
        	final String   L_HEADER_NO_USED_VOUCHERS             = "Used Vouchers";        	

        	final int      L_LINE_NO_ASSIGNED_TO_DUMMY_NUMBERS   = 3;
        	final String   L_HEADER_NO_ASSIGNED_TO_DUMMY_NUMBERS = "Assigned To Dummy Numbers";        	

        	final int      L_LINE_REASON_CODE                    = 5;
        	final String   L_HEADER_REASON_CODE                  = "Reasons For Returns (%)"; 

        	final String   L_HEADER_EXCEPTION_DETAILS            = "Exception Details";
        	final String   L_HEADER_DETAILS                      = "Agent    Serial #   Reason";

        	final int      L_LINE_FIRST_REASON_CODE_LINE         = 7;

        	final String[] L_HEADER_SECTION_REASONS_FOR_RETURNS  = { C_EMPTY_LINE,
        			                                                 L_HEADER_REASON_CODE,
        			                                                 C_EMPTY_LINE
                                                                   };

        	final String[] L_HEADER_SECTION_EXCEPTION_DETAILS    = { C_EMPTY_LINE,
        			                                                 C_EMPTY_LINE,
                                                                     L_HEADER_EXCEPTION_DETAILS,
                                                                     C_EMPTY_LINE,
        			                                                 L_HEADER_DETAILS,
        			                                                 C_EMPTY_LINE
                                                                   };
        	
        	Vector l_reportRows  = null;
        	String l_tmp         = null;
        	int    l_currentLine = 0;
            
            
    		say("*** START to verify FraudReturn's reportfile. ***");
    		
        	try
        	{
            	l_reportRows = readNewFile( c_batchReportDataFileDir, 
            			                    i_testFilename, 
            			                    "\\.txt", 
            			                    L_REPORT_FILE_EXTENSION );
            	
            	// Check number of rows in indata file
            	assertEquals( C_REPORT_FAILURE + "#line for 'Number of rows in indata file' information",
            			      L_LINE_NO_RECORDS, l_currentLine );
            	l_tmp = getInformation( (String)l_reportRows.get(l_currentLine++),
            			                L_HEADER_NO_RECORDS );
            	assertEquals( C_REPORT_FAILURE + "number of rows in indata file",
            			      C_NO_ROWS_IN_INDATA_FILE, Integer.parseInt(l_tmp) );
            	
            	// Check number of available vouchers that was in the indata file
            	assertEquals( C_REPORT_FAILURE + "#line for 'Available vouchers' information ",
            	              L_LINE_NO_AVAILABLE_VOUCHERS, l_currentLine );
                l_tmp = getInformation( (String)l_reportRows.get(l_currentLine++), 
                		                L_HEADER_NO_AVAILABLE_VOUCHERS );
            	assertEquals( C_REPORT_FAILURE + "number of available vouchers",
            			      i_noAvailableVS, Integer.parseInt(l_tmp));

            	// Check number of used vouchers that was in the indata file

            	assertEquals( C_REPORT_FAILURE + "#line for 'Number of used vouchers' information ",
            			      L_LINE_NO_USED_VOUCHERS, l_currentLine );
                l_tmp = getInformation( (String)l_reportRows.get(l_currentLine++),
                		                L_HEADER_NO_USED_VOUCHERS );
                
                // The current counter for used records have also counted ERRornous records and "Available records"
                // on the VS. Re-calculate this value.
            	// Vouchers that are available on the VS shall not be "USED" these records have been counted
            	// in the FraudReturnsTestRow, because they couldn't be discovered there
                i_usedCounter = i_usedCounter - i_expectedNoOfFailedRecords - C_VS_AVAILABLE.length;                
            	assertEquals( C_REPORT_FAILURE + "number of used vouchers",
            			      i_usedCounter, Integer.parseInt(l_tmp) );
               	
            	// Check number of vouchers assigned to dummy MSISDNs that was in the indata file
            	assertEquals( C_REPORT_FAILURE + "#line for 'Number of vouchers assigned to dummy MSISDN' information ",
       			              L_LINE_NO_ASSIGNED_TO_DUMMY_NUMBERS, l_currentLine );
                l_tmp = getInformation( (String)l_reportRows.get(l_currentLine++),
                		                L_HEADER_NO_ASSIGNED_TO_DUMMY_NUMBERS );
            	assertEquals( C_REPORT_FAILURE + "number of 'Vouchers assigned to dummy MSISDNs",
            			      i_dummyCounter, Integer.parseInt(l_tmp));

            	
           		// Check headers for the "Reasons for Returns" part
           		for ( int i = 0; i < L_HEADER_SECTION_REASONS_FOR_RETURNS.length; i++,l_currentLine++ )
           		{
           			l_tmp = (String)l_reportRows.get(l_currentLine);
           			assertTrue( C_REPORT_FAILURE + "header found: ]" + l_tmp + "[ expected: ]" + L_HEADER_SECTION_REASONS_FOR_RETURNS[i] + "[",
           					    l_tmp.equals(L_HEADER_SECTION_REASONS_FOR_RETURNS[i]));
           		}

           		
                // Calculate sum of 'Reason Code' counters - to be able to calculate the % of each reasoncode later.
                int l_tmpSum = 0;
                for ( int i = 0; i < C_NO_SPECIFIED_REASON_CODES; i ++ )
                {
                	l_tmpSum += i_reasonCodeCounter[i];
                }
                
                
                // Check reason codes
               	for ( int i = 0; i < C_NO_SPECIFIED_REASON_CODES; i++,l_currentLine++)
        		{
        			String l_tmpRow = (String)l_reportRows.get(l_currentLine); //(L_LINE_FIRST_REASON_CODE_LINE+i);
        			l_tmp = getInformation(l_tmpRow, C_REASON_CODES_TEXT[i] );
        			double l_reasonPercent = (Math.round((1000.0*i_reasonCodeCounter[i])/l_tmpSum))/10.0;
                	
        			assertTrue( C_REPORT_FAILURE + "'Reasons code' section: '" + C_REASON_CODES_TEXT[i] + "' expected value: " + l_reasonPercent + " but was: " + l_tmp, 
        					    (l_reasonPercent == Double.parseDouble(l_tmp)) );
        		}

               	
           		// Check headers for the "Exception Details" part
           		for ( int i = 0; i < L_HEADER_SECTION_EXCEPTION_DETAILS.length; i++,l_currentLine++ )
           		{
           			l_tmp = (String)l_reportRows.get(l_currentLine);
           			assertTrue( C_REPORT_FAILURE + "'Exception details headers', found: ]"+l_tmp+"[ expected: ]"+L_HEADER_SECTION_EXCEPTION_DETAILS[i]+"[",
           					    l_tmp.equals(L_HEADER_SECTION_EXCEPTION_DETAILS[i]));
           		}
           		

           		// Exception details
           		assertEquals( C_REPORT_FAILURE + "unexpected starting line for 'reason code' part",
           				      l_currentLine, 
           				      (L_LINE_FIRST_REASON_CODE_LINE + C_NO_SPECIFIED_REASON_CODES+L_HEADER_SECTION_EXCEPTION_DETAILS.length));
           		
                assertEquals(C_REPORT_FAILURE + "mismatch between expected number of 'Exception Details' rows and number of found rows",
                		     i_expectedExceptionDetails.size(), l_reportRows.size()-l_currentLine);
           		int l_detailsIndex = 0;
           		for ( int i = l_currentLine; i < l_reportRows.size(); i++, l_detailsIndex++ )
           		{
           			String l_tmpRow      = (String)l_reportRows.get(i);
           			String l_tmpExpected = (String)i_expectedExceptionDetails.get(l_detailsIndex);
           			assertEquals( C_REPORT_FAILURE + "mismatch between expected exception details and found",
           				          l_tmpExpected, l_tmpRow);
           		}
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught in verifyReportFile(), "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of FraudReturn's report file done. ***");
        	}
        }
        


        private void verifyUsedReport()
        {
        	say("*** START to verify USED reportfile. ***");
        	
        	Vector       l_reportRows = null;
        	String       l_tmp        = null;           
            StringBuffer l_tmpSelect  = null;
            String       l_sql        = null;

            // Construct SQL to get all MSISDNs from EXRE and ACIN tables that are not EXpired of DUMMY
            l_tmpSelect = new StringBuffer("select acin_serial_number from acin_accessory_install ");
            l_tmpSelect.append("where acin_cust_id not in (");
            for ( int i = 0; i < i_dummyCustid.size(); i++ )
            {
                l_tmpSelect.append((String)i_dummyCustid.get(i));
                l_tmpSelect.append(",");
            }
            int l_last  = l_tmpSelect.length();
            l_tmpSelect = l_tmpSelect.replace(l_last-1, l_last, ")");
            l_tmpSelect.append(" and acin_voucher_group <> 'EX' union ");
            l_tmpSelect.append("select exre_serial_number from exre_external_refill ");
            l_tmpSelect.append("where exre_msisdn not in (");
            for ( int i = 0; i < i_dummyMsisdns.size(); i++ )
            {
            	l_tmpSelect.append("'");
                l_tmpSelect.append((String)i_dummyMsisdns.get(i));
                l_tmpSelect.append("',");
            }
            l_last      = l_tmpSelect.length();
            l_tmpSelect = l_tmpSelect.replace(l_last-1, l_last, ")");
            l_tmpSelect.append(" and exre_voucher_group <> 'EX'");

            l_sql = l_tmpSelect.toString();
            
            try
            {
                JdbcResultSet l_resultSet = sqlQuery(new SqlString(200,0,l_sql));
                while ( l_resultSet.next(100))
                {
                	String l_tmpMsisdn = l_resultSet.getString(101, "acin_serial_number");
                    i_usedVouchers.add(l_tmpMsisdn);
                }
            }
            catch (Exception l_Ex)
            {
            	say("exeption reading used msisdns from ACIN & EXRE tables :"+l_Ex.getMessage());
            }
            
            // Add the USED vouchers on the VS
            for ( int i = 0; i < C_VS_USED.length; i++ )
            {
            	i_usedVouchers.add(C_VS_USED[i]);
            }
            
            
        	try
        	{
            	l_reportRows = readNewFile( c_batchReportDataFileDir, 
            			                    i_testFilename, 
            			                    "\\.txt", 
            			                    "_USED.TXT" );
            	
            	// Check number of rows in indata file
      			assertEquals(C_REPORT_FAILURE + "mismatch between USED voucher records in EXRE, ACIN and VS and records found in the USER report file",
  				             i_usedVouchers.size(), l_reportRows.size());

                // Check Used vouchers from EXRE, ACIN and VS with the the USED reportfile
                for ( int i = 0; i < l_reportRows.size(); i++)
                {
        			String   l_tmpRowFromFile = (String)i_usedVouchers.get(i);
        			String[] l_tmpSerial      = l_tmpRowFromFile.split(",");
        			
        			assertTrue( C_REPORT_FAILURE +"unexpected USED record found: "+l_tmpSerial[0],
        					    i_usedVouchers.contains(l_tmpSerial[0]) );
        		}
               	
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** verification of USED reportfile done. ***");
        	}
        }

        
        
        private void verifyDUMMYReport()
        {
        	
        	Vector       l_reportRows     = null;
        	String       l_tmp            = null;           
            StringBuffer l_tmpSelect      = null;
            String       l_sql            = null;
            String       l_tmpRowFromFile = null;
			String[]     l_tmpSerial      = null;
            

        	say("*** START to verify report file containing vouchers assigned to DUMMY MSISDNs ***");

        	try
        	{
            	l_reportRows = readNewFile( c_batchReportDataFileDir, 
            			                    i_testFilename, 
            			                    "\\.txt", 
            			                    "_DUMMY.TXT" );
            	
            	// Check number of rows in indata file
      			assertEquals(C_REPORT_FAILURE + "mismatch between expected no DUMMY voucher records and found in DUMMY report file ",
      					i_vouchersAssignedToDummy.size(), l_reportRows.size());

                // Check vouchers assigned to DUMMY MSISDNs and found in the DUMMY report file
                for ( int i = 0; i < l_reportRows.size(); i++)
                {
        			l_tmpRowFromFile = (String)l_reportRows.get(i);
        			l_tmpSerial      = l_tmpRowFromFile.split(",");

        			assertTrue( C_REPORT_FAILURE + "unexpected voucher: "+ l_tmpSerial[0]+" assigned to dummy MSISDN",
        				        i_vouchersAssignedToDummy.contains(l_tmpSerial[0]));
        		}
               	
            }
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of report file containing vouchers assigned to DUMMY MSISDNs done. ***");
        	}
        }
        

        private void verifyAvailableReport()
        {
        	
        	Vector       l_reportRows           = null;
        	String       l_tmp                  = null;           
            StringBuffer l_tmpSelect            = null;
            String       l_sql                  = null;
            boolean      l_isAnAvailableVoucher = false;
            String       l_tmpRowFromFile       = null;
			String[]     l_tmpSerial            = null;

        	say("*** START to verify FraudReturn report file of available vouchers. ***");
        	try
        	{
            	l_reportRows = readNewFile( c_batchReportDataFileDir, 
            			                    i_testFilename, 
            			                    "\\.txt", 
            			                    "_AVAIL.TXT" );
            	
            	// Check number of rows in indata file
       			assertEquals(C_REPORT_FAILURE + "mismatch between expected no AVAILABLE voucher records and found in AVAILABLE report file ",
      					     C_VS_AVAILABLE.length, l_reportRows.size());

                // Check vouchers found in theAVAILABLEY report file
                for ( int i = 0; i < l_reportRows.size(); i++)
                {
        			l_tmpRowFromFile = (String)l_reportRows.get(i);
        			l_tmpSerial = l_tmpRowFromFile.split(",");
        			l_isAnAvailableVoucher = false;
        			
        			for ( int j = 0; j < C_VS_AVAILABLE.length; j++ )
        			{
            			if ( l_tmpSerial[1].trim().equals(C_VS_AVAILABLE[j]) )
            			{
            				l_isAnAvailableVoucher = true;
            			}
        			}
        			assertTrue( C_REPORT_FAILURE + "unexpected available voucher : " + l_tmpSerial[i],
        					    l_isAnAvailableVoucher );
        		}
               	
            }
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of FraudReturn report file of available vouchers done. ***");
        	}
        }

        
        
        /**
         * The SUMMARY-report contains vouchers that have been assign to dummy MSISDNs or
         * are "available" on the VS.
         *
         */
        private void verifySummaryReport()
        {
        	say("*** START to verify FraudReturnBatch's Summary report. ***");
        	
        	final int    L_INDEX_START_TYPE         = 0;
        	final int    L_INDEX_END_TYPE           = 38;
        	final int    L_INDEX_START_TOTAL_AMOUNT = 39;
        	final int    L_INDEX_END_TOTAL_AMOUNT   = 54;
        	final int    L_INDEX_START_VOUCHER_INFO = 55;
        	
        	final int    L_INDEX_VOUCHER_AMOUNT     = 0;
        	final int    L_INDEX_NO_RECORDS         = 2;
        	
            final String L_PATTERN_DUMMY            = "DUMMY";
            final String L_PATTERN_AVAILABLE        = "AVAIL";
        	
        	Vector       l_summaryReportRows               = null;

            String       l_row                             = null;
            String       l_type                            = null;
            int          l_noVoucherRecords                = 0;
            String[]     l_nameParts                       = null;
            String[]     l_voucherInfoParts                = null;
            
            String       l_tmp                             = null;
            double       l_totalAmount                     = 0.0;
            double       l_voucherAmount                   = 0.0;
            int          l_noVouchersAssignedToDummyMSISDN = 0;
            int          l_noAvailableVouchers             = 0;
            
            
        	try
        	{
        		l_summaryReportRows = readNewFile( c_batchReportDataFileDir, 
            			                           "RET_SUMMARY_"+i_filenameDate+".TXT", 
            			                           null, 
            			                           null );
            	
                // Check vouchers found in theSUMMARYY report file
                for ( int i = 0; i < l_summaryReportRows.size(); i++)
                {
        			l_row              = (String)l_summaryReportRows.get(i);
        			
        			l_nameParts        = (l_row.substring(L_INDEX_START_TYPE,L_INDEX_END_TYPE).trim()).split("_");
        			l_type             = l_nameParts[l_nameParts.length-1];
        			
        			l_tmp              = l_row.substring(L_INDEX_START_TOTAL_AMOUNT,L_INDEX_END_TOTAL_AMOUNT).trim();
        			l_totalAmount      = Double.parseDouble(l_tmp);
        			
        			// VoucherInfoPart may look like: "10.0 EUR (6)"
          			l_voucherInfoParts = (l_row.substring(L_INDEX_START_VOUCHER_INFO)).split(" ");
        			l_tmp              = l_voucherInfoParts[L_INDEX_VOUCHER_AMOUNT];
                    l_voucherAmount    = Double.parseDouble( l_tmp );

        			// The no records are included in parentes, there for start at 1 and end one before end of string.
          			l_tmp              = l_voucherInfoParts[L_INDEX_NO_RECORDS].substring(1,l_voucherInfoParts[L_INDEX_NO_RECORDS].length()-1);
        			l_noVoucherRecords = Integer.parseInt( l_tmp );

        			if ( l_type.equals(L_PATTERN_DUMMY) )
        			{
        				l_noVouchersAssignedToDummyMSISDN += l_noVoucherRecords;
        			}
        			else if ( l_type.equals(L_PATTERN_AVAILABLE) )
        			{
        				l_noAvailableVouchers += l_noVoucherRecords;
        			}
        			
        			// Check that total amount is valid for voucherAmount and the number of vouchers with that amount
        			assertTrue( C_REPORT_FAILURE + "total amount is not calculated correct expected: " + l_noVoucherRecords*l_voucherAmount+" but was: "+l_totalAmount,
        					    (l_noVoucherRecords*l_voucherAmount == l_totalAmount));

      		    }
                assertEquals( C_REPORT_FAILURE + "unexpected number of vouchers assigned to dummy MSISDNs",
                		      i_dummyCounter, l_noVouchersAssignedToDummyMSISDN);
                assertEquals( C_REPORT_FAILURE + "unexpected number of available vouchers",
          		      i_noAvailableVS, l_noAvailableVouchers);
               	
            }
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of FraudReturnBatch's Summary reportdone. ***");
        	}
        }

        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRecoveryFile() throws IOException
        {
           	Vector l_recoveryData = null;
        	String l_found        = null;
			String l_expected     = null;
			
        	say("*** START to verify FraudReturnBatch's Recovery file. ***");
        	try
        	{
            	l_recoveryData = readNewFile( c_batchRecoveryDataFileDir, 
            			                      i_testFilename, 
            			                      "txt", 
            			                      "RCV" );
            	
            	if ( i_expectedNoOfFailedRecords == 0 )
            	{
            		// File should not exist
            		// SUCCESS expected!
            		// All successful - only one row in the result file
            		assertEquals( "No recovery record should be found.", 
            				      0, l_recoveryData.size());            		
            	}
            	else
            	{
            		for ( int i = 0; i < l_recoveryData.size(); i++)
            		{
            			l_found    = (String)l_recoveryData.get(i);
            			l_expected = i_expectedRecoveryData[i];
            			assertTrue( "VerifyRecoveryFile failed, found:" + l_found +
            					    " expected:" + l_expected, 
            					    l_found.equals(l_expected));
            		}            		
            	}
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
            	say("*** Verification of FraudReturnBatch's Recovery file done. ***");

        	}
        }


        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
        	say("*** START to verify renaming of input file to DNE. ***");
        	
        	String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "txt", 
                                                           "DNE" );        	
        	File   l_file             = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
            	say( "File DOES NOT EXIST : " + l_file);
            	throw new IOException();            	
            }
        	say("*** Verification of renaming the input file done.  ***");

        }
        
        ////////////////////////////////////////////////////////////////////////////
        // Redefined methods
        ////////////////////////////////////////////////////////////////////////////
        /**
         * Creates and returns a test data filename. This batch uses a special filename
         * style: RETAIL_ddmmyyyy_noRecords.txt
         * 
         * 
         * @return the test data filename.
         */
        protected String createTestDataFilename(String p_prefix, int p_noRecords, String p_fileType)
            throws PpasServiceException
        {
            StringBuffer l_testDataFilename = null;
            
            l_testDataFilename = new StringBuffer(p_prefix);
            l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
            i_filenameDate = createFileNameDate();
            l_testDataFilename.append(i_filenameDate);
            l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
            l_testDataFilename.append( p_noRecords );
            l_testDataFilename.append(p_fileType);
            
            return l_testDataFilename.toString();
        }

        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String createFileNameDate()
        {
        	StringBuffer l_filenameDate = null;
        	PpasDateTime l_dateTime         = DatePatch.getDateTimeNow();
        	
            l_filenameDate = new StringBuffer(8);
            l_filenameDate.append(1900+l_dateTime.getYear());
            l_filenameDate.append( constructFilledString("00",   "", l_dateTime.getMonth()+1, 2) );
            l_filenameDate.append( constructFilledString("00",   "", l_dateTime.getDate(),  2) );
            
            return l_filenameDate.toString();
        }
        
        private String getInformation( String p_row, String p_delimiter )
        {
        	String[] l_tmp = p_row.split(p_delimiter);
        	return  l_tmp[l_tmp.length-1].trim();        	
        }
        
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
           	// Set default currency to EUR
        	final String L_SET_DEFAULT_CURRENCY    = "update syfg_system_config set syfg_parameter_value = 'EUR' where SYFG_DESTINATION = 'CURRENCY'";
        	final String L_TRUNCATE_ACIN           = "truncate table ACIN_ACCESSORY_INSTALL";
        	final String L_TRUNCATE_EXRE           = "truncate table EXRE_EXTERNAL_REFILL";
        	final String L_GET_LICENCE_INFORMATION = "select * from feat_features where feat_feature_code = " + 
        	                                         FeatureLicence.C_FEATURE_CODE_VODAFONE_FRAUD_RETURNS;
        	final String L_INSERT_FRAUD_LICENCE    = "insert into feat_features (feat_feature_code,feat_feature_description,feat_licence_string) " +
                                                     "values (" +
                                                     FeatureLicence.C_FEATURE_CODE_VODAFONE_FRAUD_RETURNS +
                                                     ",'Fraud return Batch Process (ASCS)','PHEGBFYCDPELNSQKECIKHPSOPPRYGQ')";
        	BasicAccountData l_subscriber      = null;
        	int              l_noRowsSelected  = 0;
            String           l_tmpMsisdn       = null;
            String           l_tmpCustId       = null;

            // Set up database
        	SqlString        l_sql             = new SqlString(200, 3, L_SET_DEFAULT_CURRENCY);
        	int              l_noRowsInserted  = sqlUpdate( l_sql );

        	l_sql = new SqlString(50,0,L_TRUNCATE_ACIN);
        	sqlUpdate(l_sql);       	
        	l_sql = new SqlString(50,0,L_TRUNCATE_EXRE);
        	sqlUpdate(l_sql);
        	
        	// Check if FraudReturns licence is defined - if not insert it to the FEAT_FEATURE table
            l_sql = new SqlString(60,0,L_GET_LICENCE_INFORMATION);
            say("MT featurecode test :"+l_sql.toString());
            if ( sqlUpdate(l_sql) <= 0 )
            {
            	say(" FraudReturn licence not found - add it to the feat_feature licence table");
            	l_sql = new SqlString(180,0,L_INSERT_FRAUD_LICENCE);
            	sqlUpdate(l_sql);
            }
        	
        	// Create Dummy MSISDNs
        	for ( int i = 0; i < C_NO_DUMMY_MSISDN; i++ )
        	{
        		l_subscriber = installGlobalTestSubscriber(null);
        		l_tmpMsisdn  = getFormattedMsisdn(l_subscriber).trim();
                i_dummyMsisdns.add(l_tmpMsisdn);              // Save Dummy MSISDN for EXRE table
            	i_dummyCustid.add(l_subscriber.getCustId());  // Save Dummy CUSTID for ACIN table
           	}
           	prepareDummyMsisdnsFile(i_dummyMsisdns);
            

           	
        	// Create testdata for the Voucher Server
        	for ( int i = 0; i < i_vsTestData.length; i++ )
        	{
        		FraudReturnsTestRow l_tmpTestRow = (FraudReturnsTestRow)i_vsTestData[i];
        		
        		addSubscriberData(l_tmpTestRow.getRow());
        		
        		if (l_tmpTestRow.isDuplicate() )
        		{
            		addSubscriberData(l_tmpTestRow.getRow());
        		}        		
        	}
 
        	
        	// Create testdata for the ACIN table
        	for ( int i = 0; i < i_acinTestData.length; i++ )
        	{
        		FraudReturnsTestRow l_tmpTestRow = (FraudReturnsTestRow)i_acinTestData[i];
        		
        		addSubscriberData(l_tmpTestRow.getRow());        		
        		if (l_tmpTestRow.isDuplicate() )
        		{
           		addSubscriberData(l_tmpTestRow.getRow());
        		}

        		if (l_tmpTestRow.isDummy() )
        		{
        			l_tmpCustId = (String)(i_dummyCustid.get(0));
        		}
        		else
        		{
            		l_subscriber = installGlobalTestSubscriber(null);
            		l_tmpCustId  = l_subscriber.getCustId();
        		}
        		
        		prepareAcinTable( l_tmpCustId,
        				          l_tmpTestRow.getSerialNumber(),
        				          l_tmpTestRow.isExpired());
        	}

        	
        	// Create testdata for the EXRE table
        	for ( int i = 0; i < i_exreTestData.length; i++ )
        	{
        		FraudReturnsTestRow l_tmpTestRow = (FraudReturnsTestRow)i_exreTestData[i];
        		
        		addSubscriberData(l_tmpTestRow.getRow());        		
        		if (l_tmpTestRow.isDuplicate() )
        		{
            		addSubscriberData(l_tmpTestRow.getRow());
        		}

        		if (l_tmpTestRow.isDummy() )
        		{
        			l_tmpMsisdn = (String)(i_dummyMsisdns.get(1));
        		}
        		else
        		{
            		l_subscriber = installGlobalTestSubscriber(null);
            		l_tmpMsisdn  = getFormattedMsisdn(l_subscriber).trim();
        		}
        		
        		prepareExreTable( C_ACTIVATION_NUMBER+i,
        				          l_tmpTestRow.getSerialNumber(),
        				          l_tmpMsisdn,
        				          l_tmpTestRow.isExpired());
        	}
        }

        
        private void prepareAcinTable( String  p_custId,
                                       String  p_serialNumber,
                                       boolean p_group)
        {
            final String L_GROUP   = "GRP";
            final String L_EXPIRED = "EX";
        	final String L_INSERT_ACIN = "INSERT INTO acin_accessory_install " +
        	"( ACIN_CUST_ID,ACIN_START_DATE_TIME,ACIN_SERIAL_NUMBER,ACIN_VOUCHER_GROUP,ACIN_OPID,ACIN_GEN_YMDHMS,ACIN_ORIGINAL_AMOUNT,ACIN_APPLIED_VALUE,ACIN_RECHARGE_TYPE,ACIN_INITIATING_CUST_ID,ACIN_TOTAL_AMOUNT_APPLIED,ACIN_APPLIED_CURRENCY,ACIN_ORIGINAL_CURRENCY) " +
        	"values( {0}, sysdate, {1}, {2}, 'UTTEST', sysdate, 20., 20., 'U', {0}, 20.,'EUR','EUR')";

        	SqlString l_sql = new SqlString(200, 3, L_INSERT_ACIN);
        	l_sql.setStringParam(0, p_custId);
        	l_sql.setStringParam(1, p_serialNumber);
        	if ( p_group )
        	{
        	    l_sql.setStringParam(2, L_EXPIRED);
        	}
        	else
        	{
        		l_sql.setStringParam(2, L_GROUP);
        	}

        	int l_noRowsInserted = sqlUpdate( l_sql );
        	return;
        }

        
        private void prepareExreTable( String  p_activationNumber,
                                       String  p_serialNumber,
                                       String  p_msisdn,
                                       boolean p_group)
        {
            final String L_GROUP   = "GRP";
            final String L_EXPIRED = "EX";

            final String L_INSERT_EXRE = "INSERT INTO EXRE_EXTERNAL_REFILL " +
                  "(EXRE_ACTIVATION_NUMBER,"+
                   "EXRE_SERIAL_NUMBER,"+
                   "EXRE_MSISDN,"+
                   "EXRE_VOUCHER_GROUP,"+
                   "EXRE_ORIGINAL_AMOUNT,"+
                   "EXRE_ORIGINAL_CURRENCY,"+
                   "EXRE_REFILL_DATE_TIME,"+
                   "EXRE_OPID,"+
                   "EXRE_GEN_YMDHMS) " +
                  "VALUES " +
                  "({0},{1},{2},{3},100.000,'EUR',sysdate,'UTTEST',sysdate)";

            SqlString l_sql = new SqlString(200, 3, L_INSERT_EXRE);
            l_sql.setStringParam(0, p_activationNumber);
            l_sql.setStringParam(1, p_serialNumber);
            l_sql.setStringParam(2, p_msisdn);
        	if ( p_group )
        	{
        	    l_sql.setStringParam(3, L_EXPIRED);
        	}
        	else
        	{
        		l_sql.setStringParam(3, L_GROUP);
        	}

            int l_noRowsInserted = sqlUpdate( l_sql );
            return;
        }




        private void prepareDummyMsisdnsFile( ArrayList p_dummy )
        {
        	final String L_DUMMY_FILE      = "dummy_msisdn.dba";
        	PrintWriter  l_dummyFileWriter = null;
        	StringBuffer l_tmp             = new StringBuffer(100);
        	
        	l_tmp.append(c_batchInputDataFileDir);
        	l_tmp.append(File.separator);
        	l_tmp.append(L_DUMMY_FILE);
        	
            try
            {       	
                l_dummyFileWriter = new PrintWriter(  new BufferedWriter( new FileWriter(l_tmp.toString()) )  );

                for ( int i = 0; i < p_dummy.size(); i++ )
                {
                    l_dummyFileWriter.println( (String)(p_dummy.get(i)) );
                }
            }
            catch ( Exception l_io )
            {
            	say("prepareDummyMsisdns - write to dummyfile failed :"+l_io.getMessage());
            }
            finally
            {
                if (l_dummyFileWriter != null)
                {
                    l_dummyFileWriter.close();
                    l_dummyFileWriter = null;
                }
            }
        }

    } // End of inner class 'FraudReturnsTestDataTT'.
    
    
    /**
     * This class will create input data and calculate some of the expected results.
     */
    private class FraudReturnsTestRow
    {
    	static final String C_SEPARATOR = ",";

    	String       i_voucherSerialNumber;
    	int          i_terminalId;
    	int          i_reasonCode;
    	boolean      i_dummy;
    	boolean      i_expired;
    	boolean      i_duplicate;
    	boolean      i_validRecord;
    	

    	private FraudReturnsTestRow( String   p_voucherSerialNumber,
                                      int     p_terminalId,
                                      int     p_reasonCode, 
                                      boolean p_expired,
                                      boolean p_dummy,
                                      boolean p_duplicate )
    	{
    		String l_tmpExpectedExceptionDetails = null;
    		
    		i_voucherSerialNumber = p_voucherSerialNumber;
    		i_terminalId          = p_terminalId;
    		i_reasonCode          = p_reasonCode;
            i_expired             = p_expired;
            i_dummy               = p_dummy;
            i_duplicate           = p_duplicate;
            
        	if ( p_expired )
            {
            	i_expiredCounter++;
            	l_tmpExpectedExceptionDetails = getExpectedExceptionDetails( p_voucherSerialNumber,
                                                                             C_ALREADY_EXPIRED );
            	i_expectedExceptionDetails.add(l_tmpExpectedExceptionDetails);
            }
        	else if ( p_dummy )
            {
            	i_dummyCounter++;
            	i_vouchersAssignedToDummy.add(p_voucherSerialNumber);
            }
            if ( !p_expired && !p_dummy )
            {
            	i_usedCounter++;
            }
             
            if ( p_duplicate )
            {
            	i_reasonCodeCounter[p_reasonCode]++; // count twice - as all rows from input file shall be counted
            	i_duplicateCounter++;
            	l_tmpExpectedExceptionDetails = getExpectedExceptionDetails( p_voucherSerialNumber,
                                                                             C_DUPLICATE );
            	i_expectedExceptionDetails.add(l_tmpExpectedExceptionDetails);

            }    

        	i_reasonCodeCounter[p_reasonCode]++;
    	}
    	
        private boolean isDuplicate()
        {
        	return i_duplicate;
        }
        
        private boolean isDummy()
        {
        	return i_dummy;
        }
        
        private boolean isExpired()
        {
        	return i_expired;
        }
    	
        private String getSerialNumber()
        {
        	return i_voucherSerialNumber;
        }
        
    	private String getRow()
    	{
    		StringBuffer l_tmp = new StringBuffer();
    		l_tmp.append(i_voucherSerialNumber);
    		l_tmp.append(C_SEPARATOR);
    		l_tmp.append(i_terminalId);
    		l_tmp.append(C_SEPARATOR);
    		l_tmp.append(i_reasonCode);
    		
    		return l_tmp.toString();
    	}
    	
    	public String toString()
    	{
    		StringBuffer l_tmp = new StringBuffer(getRow());
    		if ( i_duplicate )
    		{
    			l_tmp.append(" DUPLICATE ");
    		}
    		if ( i_expired )
    		{
    			l_tmp.append(" EXPIRED ");
    		}
    		if ( i_dummy )
    		{
    			l_tmp.append(" DUMMY ");
    		}
    		
    		return l_tmp.toString();   		
    	}
    	
    }
    
	public String getExpectedExceptionDetails( String p_voucherSerialNumber,
                                               String p_reason )
    {
		StringBuffer l_tmp = new StringBuffer();

		l_tmp.append(C_STEM);
		l_tmp.append("  ");
		l_tmp.append(p_voucherSerialNumber);
		l_tmp.append(" ");
		l_tmp.append(p_reason);
		return l_tmp.toString();
    }

} // End of class 'FraudReturnsBatchControllerISUT'.