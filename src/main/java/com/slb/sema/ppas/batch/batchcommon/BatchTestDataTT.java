////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchTestDataTT.java 
//      DATE            :       23-Jan-2007
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       A basic test data generator to be used by batch
//                              UT tests.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars Lundberg | Removed method:                  | PpacLon#3033/11279
//          |               | installLocalTestSubscribers(...) |
//          |               | A lot of comments are added and  |
//          |               | changed.                         |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcommon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * A basic test data generator to be used by the batch sub-system UT tests.
 */
public abstract class BatchTestDataTT
{
    //==========================================================================
    // Protected constant(s).                                                 ==
    //==========================================================================
    /** The suffix for the ordinary test data filename. */
    protected static final String   C_SUFFIX_ORDINARY_TEST_DATA_FILENAME = ".DAT";

    /** The file date to be used for all UT test data files. */
    protected static final PpasDate C_DATE_TEST_DATA_FILENAME            = new PpasDate(
                                                                                      "20000101",
                                                                                      PpasDate.C_DF_yyyyMMdd);

    //==========================================================================
    // Private constant(s).                                                   ==
    //==========================================================================
    /** The file sequence number template to be used for all UT test data files. */
    private static final String C_TEMPLATE_SEQ_NO          = "00000";

    /** The filename parts delimiter to be used for all UT test data files. */
    protected static final char C_DELIM_TEST_DATA_FILENAME = '_';

    /** The MSISDN number template to be used for all UT test data files. */
    protected static final String C_TEMPLATE_MSISDN          = "               ";

    
    //==========================================================================
    // Protected attribute(s).                                                ==
    //==========================================================================
    /** The <code>PpasRequest</code> object. */
    protected PpasRequest i_ppasRequest                   = null;

    /** The <code>Logger</code> object. */
    protected Logger      i_logger                        = null;

    /** The <code>PpasContext</code> object. */
    protected PpasContext i_ppasContext                   = null;

    /** The name of the batch that is currently tested. */
    protected String      i_batchName                     = null;

    /** The name of the test file. */
    protected String      i_testFilename                  = null;

    /** The file sequence number of the test file. */
    protected int         i_fileSequenceNumber            = -1;

    /** The file sub-sequence number of the test file. */
    protected int         i_fileSubSequenceNumber         = -1;

    /** The expected js job (in this case the batch) exit status. */
    protected int         i_expectedJobExitStatus         = JobStatus.C_JOB_EXIT_STATUS_UNDEFINED;

    /** The expected batch control status. */
    protected char        i_expectedBatchJobControlStatus = 0;

    /** The expected number of successfully processed test data records. */
    protected int         i_expectedNoOfSuccessRecords    = 0;

    /** The expected number of failing test data records. */
    protected int         i_expectedNoOfFailedRecords     = 0;

    /** The expected report file data as a <code>String</code> array. */
    protected String[]    i_expectedReportData            = null;
    
    /** The expected recovery file data as a <code>String</code> array. */
    protected String[]    i_expectedRecoveryData          = null;

    // =========================================================================
    // == Private attribute(s).                                               ==
    // =========================================================================
    /** The locally installed test subscriber's as a list of <code>BasicAccountData</code> objects. */
    private ArrayList   i_localTestSubs        = null;

    /** The batch input files directory as a <code>File</code> object. */
    private File        i_batchInputFileDir    = null;


    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs an <code>BatchTestDataTT</code> using the given parameters.
     * 
     * @param p_ppasRequest                    The <code>PpasRequest</code> object.
     * @param p_logger                         The <code>Logger</code> object.
     * @param p_ppasContext                    The <code>PpasContext</code> object.
     * @param p_batchName                      The name of the batch.
     * @param p_batchInputFileDir              The input data file directory.
     * @param p_expectedJobExitStatus          The expected batch job exit status.
     * @param p_expectedBatchJobControlStatus  The expected batch job control data status.
     * @param p_expectedNoOfSuccessRecords     The expected number of successfully processed records.
     * @param p_expectedNoOfFailedRecords      The expected number of failed records.
     */
    public BatchTestDataTT(PpasRequest p_ppasRequest,
                           Logger      p_logger,
                           PpasContext p_ppasContext,
                           String      p_batchName,
                           File        p_batchInputFileDir,
                           int         p_expectedJobExitStatus,
                           char        p_expectedBatchJobControlStatus,
                           int         p_expectedNoOfSuccessRecords,
                           int         p_expectedNoOfFailedRecords)
    {
        i_ppasRequest                   = p_ppasRequest;
        i_logger                        = p_logger;
        i_ppasContext                   = p_ppasContext;
        i_batchName                     = p_batchName;
        i_batchInputFileDir             = p_batchInputFileDir;
        i_expectedJobExitStatus         = p_expectedJobExitStatus;
        i_expectedBatchJobControlStatus = p_expectedBatchJobControlStatus;
        i_expectedNoOfSuccessRecords    = p_expectedNoOfSuccessRecords;
        i_expectedNoOfFailedRecords     = p_expectedNoOfFailedRecords;
        i_localTestSubs                 = new ArrayList();
        
    }


    //==========================================================================
    // Protected abstract method(s).                                          ==
    //==========================================================================
    /**
     * Installs test subscribers.
     * 
     * @throws PpasServiceException  if it fails to install test subscribers.
     */
    protected abstract void installTestSubscribers() throws PpasServiceException;

    /**
     * Creates a test file.
     * 
     * @throws PpasServiceException  if it fails to get a new file sequence number.
     * @throws IOException           if it fails to create a test file.
     */
    protected abstract void createTestFile() throws PpasServiceException, IOException;


    /**
     * Replaces any found data tag in the given data string by real data and returns the resulting string.
     * 
     * @param p_dataStr  the data string.
     * 
     * @return  the resulting string after data tags have been replaced by real data.
     */
    protected abstract String replaceDataTags(String p_dataStr);


    /**
     * Verifies that the ASCS database has been properly updated.
     * @throws PpasServiceException
     */
    protected abstract void verifyAscsDatabase() throws PpasServiceException;

    /**
     * Verifies that the report file has the right information.
     * @throws PpasServiceException
     * @throws IOException
     */
    protected abstract void verifyReportFile() throws IOException;

    /**
     * Verifies that the recovery file has the right information.
     * @throws IOException
     */
    protected abstract void verifyRecoveryFile() throws IOException;
    
    /**
     * Verifies that the inputfile has been renamed.
     * @throws IOException
     */    
    protected abstract void verifyRenaming() throws IOException;


    /**
     * Saves expected report rows.
     * 
     * @param p_reportRows  the expected report rows as a <code>String</code> array.
     */
    public void defineReportRows(String[] p_reportRows)
    {
    	i_expectedReportData = p_reportRows;
    }
    
    /**
     * Saves the expected recovery rows.
     * @param p_recoveryRows  the expected recovery rows as a <code>String</code> array.
     */
    public void defineRecoveryRows(String[] p_recoveryRows)
    {
        i_expectedRecoveryData = p_recoveryRows;
    }
    
    //==========================================================================
    // Protected method(s).                                                   ==
    //==========================================================================
    /**
     * Returns a formatted MSISDN for the subscriber pointed out by the given subscriber index.
     * The length of the MSISDN will be 15 characters, right adjusted and space filled.
     * 
     * @param p_subscriberIx  the subscriber index.
     * 
     * @return a formatted MSISDN, length 15 characters, right adjusted and space filled.
     */
    protected String getFormattedMsisdn(int p_subscriberIx)
    {
        BasicAccountData l_subscriberBAD = (BasicAccountData)getSubscriberData(p_subscriberIx);

        return getFormattedMsisdn( l_subscriberBAD );
    }

    /**
     * Returns a formatted MSISDN for the subscriber pointed out by the given subscriber index.
     * The length of the MSISDN will be 15 characters, right adjusted and space filled.
     * 
     * @param p_subscriber  the subscriber's basic data.
     * 
     * @return a formatted MSISDN, length 15 characters, right adjusted and space filled.
     */
    protected String getFormattedMsisdn(BasicAccountData p_subscriber)
    {
        String           l_formattedMsisdnStr = null;

        l_formattedMsisdnStr = C_TEMPLATE_MSISDN +
                               new MsisdnFormat("n", "0044").format(p_subscriber.getMsisdn());
        l_formattedMsisdnStr = l_formattedMsisdnStr.substring((l_formattedMsisdnStr.length() -
                                                              C_TEMPLATE_MSISDN.length()));

        return l_formattedMsisdnStr;
    }
    
    /**
     * Returns a formatted MSISDN for the subscriber pointed out by the given subscriber index.
     * The length of the MSISDN will be 9 characters, right adjusted and space filled.
     * 
     * @param p_subscriberIx  the subscriber index.
     * 
     * @return a formatted MSISDN, length 9 characters, right adjusted and space filled.
     */
    protected String getFormattedMsisdnSDPBal(int p_subscriberIx)
    {
        String           l_formattedMsisdnStr = null;
        BasicAccountData l_subscriberBAD = (BasicAccountData)getSubscriberData(p_subscriberIx);
        
        l_formattedMsisdnStr = C_TEMPLATE_MSISDN +
                               new MsisdnFormat("n", "0044").format(l_subscriberBAD.getMsisdn());
        l_formattedMsisdnStr = l_formattedMsisdnStr.substring((l_formattedMsisdnStr.length() -
                                                              11));

        return l_formattedMsisdnStr;
    }
    

    /**
     * Returns the number of local test subscribers.
     * 
     * @return the number of local test subscribers.
     */
    protected int getNumberOfTestSubscribers()
    {
    	return (i_localTestSubs == null) ? 0 : i_localTestSubs.size();
    }

    
    /**
     * Returns the subcriber data as a <code>BasicAccountData</code> for the subscriber pointed out
     * by the given subscriber index (starting with 0 for the first subscriber).
     * 
     * @param p_subscriberIx  the subscriber index (starting with 0 for the first subscriber).
     * @return the subcriber data.
     */
    protected Object getSubscriberData(int p_subscriberIx)
    {
        return i_localTestSubs.get(p_subscriberIx);
    }


    /**
     * Creates a test file using the given template file with the given filename prefix and suffix.
     * 
     * @param p_prefix            the test filename prefix.
     * @param p_suffix            the test data filename suffix (extension).
     * @param p_templateFilename  the name of the template file.
     * @param p_utClassName       the name of the current UT test class.
     * @throws PpasServiceException  if it fails to get a new file sequence number.
     * @throws IOException           if it fails to create a test file.
     */
    protected void createTestFile(String p_prefix,
                                  String p_suffix,
                                  String p_templateFilename,
                                  String p_utClassName)
        throws PpasServiceException, IOException
    {
        String l_testFilename = null;

        l_testFilename = createTestDataFilename(p_prefix, p_suffix, p_utClassName);
        createTestFile(l_testFilename, p_templateFilename);
        i_testFilename = l_testFilename;
    }


    /**
     * Creates and returns a test data filename.
     * 
     * @param p_prefix  the test data filename prefix.
     * @param p_suffix  the test data filename suffix (extension).
     * @param p_utName  the name of the UT test (for instance "MiscDataUploadBatchUT").
     * 
     * @return the test data filename.
     * @throws PpasServiceException  if it fails to get a new file sequence number.
     */
    protected String createTestDataFilename(String p_prefix, String p_suffix, String p_utName)
        throws PpasServiceException
    {
        StringBuffer l_testDataFilename = null;
        String       l_fileSeqNumberStr = null;

        l_testDataFilename = new StringBuffer(p_prefix);
        l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
        l_testDataFilename.append(C_DATE_TEST_DATA_FILENAME.toString_yyyyMMdd());
        l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
        l_fileSeqNumberStr = getFileSequenceNumber(p_utName);
        i_fileSequenceNumber = Integer.parseInt(l_fileSeqNumberStr);
        l_testDataFilename.append(l_fileSeqNumberStr);
        l_testDataFilename.append(p_suffix);

        return l_testDataFilename.toString();
    }


    /**
     * Returns the actual batch control data stored in the BACO_BATCH_CONTROL table as a
     * <code>BatchJobControlData</code> object.
     * The database key is obtained by the js job id and execution date and time stored in the
     * given <code>BatchController</code> object.
     * 
     * @param p_batchController  the <code>BatchController</code> object.
     * 
     * @return the actual batch control data stored in the BACO_BATCH_CONTROL table as a
     *         <code>BatchJobControlData</code> object.
     *         
     * @throws NumberFormatException  if the js job id isn't a parsable long.
     * @throws PpasServiceException   if it fails to get the batch job control data from the database.
     */
    protected BatchJobControlData getActualBatchJobControlData(BatchController p_batchController)
        throws NumberFormatException, PpasServiceException
    {
        PpasBatchControlService l_ppasBatchControlService   = null;
        BatchJobControlData     l_actualBatchJobControlData = null;

        l_ppasBatchControlService = new PpasBatchControlService(i_ppasRequest,
                                                                i_logger,
                                                                i_ppasContext);

        // Get the actual batch control data.
        l_actualBatchJobControlData =
            l_ppasBatchControlService.getJobDetails(i_ppasRequest,
                                                    Long.parseLong(p_batchController.getJobId()),
                                                    p_batchController.getExecutionDateTime(),
                                                    30000L);

        return l_actualBatchJobControlData;
    }


    /**
     * Returns the expected batch job control data as a <code>BatchJobControlData</code> object. 
     * 
     * @param p_batchController  the currently tested <code>BatchController</code> object.
     * 
     * @return the expected batch job control data as a <code>BatchJobControlData</code> object.
     */
    protected BatchJobControlData getExpectedBatchJobControlData(BatchController p_batchController)
    {
        BatchJobControlData l_batchJobControlData = null;
        
        l_batchJobControlData = p_batchController.getKeyedBatchJobControlData();
        l_batchJobControlData.setJobType(i_batchName);
        l_batchJobControlData.setStatus(i_expectedBatchJobControlStatus);
        l_batchJobControlData.setFileDate(C_DATE_TEST_DATA_FILENAME);
        l_batchJobControlData.setSeqNo(i_fileSequenceNumber);
        l_batchJobControlData.setSubSeqNo(i_fileSubSequenceNumber);
        l_batchJobControlData.setSubJobCnt(-1);
        l_batchJobControlData.setNoOfSuccessfullyRecs(i_expectedNoOfSuccessRecords);
        l_batchJobControlData.setNoOfRejectedRecs(i_expectedNoOfFailedRecords);
        l_batchJobControlData.setOpId(p_batchController.getSubmitterOpid());
        
        return l_batchJobControlData;
    }


    //==========================================================================
    // Private method(s).                                                     ==
    //==========================================================================
    /**
     * Generates and returns a daily based unique sequence number for the given sequence name as
     * a 5 digits, right adjusted, zero-filled <code>String</code>.
     * 
     * @param p_sequenceName  the sequence name for which a new sequence number should be generated.
     * 
     * @return  a daily based unique sequence number for the given sequence name as
     *          a 5 digits, right adjusted, zero-filled <code>String</code>.
     * @throws PpasServiceException  if it fails to get a new file sequence number.
     */
    private String getFileSequenceNumber(String p_sequenceName) throws PpasServiceException
    {
        PpasDailySequencesService l_dailySeqServ  = null;
        long                      l_dailySeqNo    = 0L;
        String                    l_dailySeqNoStr = null;

        l_dailySeqServ = new PpasDailySequencesService(i_ppasRequest, i_logger, i_ppasContext);
        l_dailySeqNo   = l_dailySeqServ.getNextSequenceNumber(i_ppasRequest,
                                                              30000L,
                                                              p_sequenceName,
                                                              DatePatch.getDateToday());

        l_dailySeqNoStr = C_TEMPLATE_SEQ_NO + l_dailySeqNo;
        return l_dailySeqNoStr.substring(l_dailySeqNoStr.length() - C_TEMPLATE_SEQ_NO.length());
    }


    /**
     * Creates a test file.
     * 
     * @param p_testFilename      the name of the test file.
     * @param p_templateFilename  the template to be used when creating the test file.
     * @throws IOException if it fails to create a test file.
     */
    protected void createTestFile(String p_testFilename, String p_templateFilename) throws IOException
    {
        File           l_testDataFile       = null;
        File           l_templateDataFile   = null;
        String         l_dataRow            = null;
        BufferedReader l_templateFileReader = null;
        PrintWriter    l_dataFileWriter     = null;
        
        l_testDataFile     =
            new File(i_batchInputFileDir.getAbsolutePath() + File.separator + p_testFilename);
        l_templateDataFile =
            new File(i_batchInputFileDir.getAbsolutePath() + File.separator + p_templateFilename);

        try
        {
            l_dataFileWriter     = new PrintWriter(new BufferedWriter(new FileWriter(l_testDataFile)));
            l_templateFileReader = new BufferedReader(new FileReader(l_templateDataFile));

            while ((l_dataRow = l_templateFileReader.readLine()) != null)
            {
                // Replace any data tag found in the template file record.
                l_dataFileWriter.println(replaceDataTags(l_dataRow));
            }
        }
        finally
        {
            if (l_templateFileReader != null)
            {
                l_templateFileReader.close();
                l_templateFileReader = null;
            }

            if (l_dataFileWriter != null)
            {
                l_dataFileWriter.close();
                l_dataFileWriter = null;
            }
        }
    }

    /**
     * Construct a full path file name from the input arguments.
     * 
     * @param p_directory    Directory for file.
     * @param p_fileName     The file name.
     * @param p_regularExpr  Part of the name to be exhanged. (NOTE treated as a regularexpression)
     * @param p_newFileType  The new file type.
     * @return a full path file name.
     */
    protected String constructFileName(File   p_directory,
                                       String p_fileName,
    		                           String p_regularExpr,
                                       String p_newFileType)
    {        
        StringBuffer l_path        = new StringBuffer();
        String       l_tmpFileName = null;
        
        l_path.append(p_directory);
        l_path.append(File.separator);
        l_path.append(p_fileName);
        l_tmpFileName = l_path.toString();
        
        if ( p_regularExpr != null && p_newFileType != null )
        {
            l_tmpFileName = l_tmpFileName.replaceAll(p_regularExpr, p_newFileType);
        }    
        
        return l_tmpFileName;
    }
    
    
    /**
     * Reads a file and returns the contents as a <code>Vector</code> of <code>String</code> objects.
     * 
     * @param p_directory    Directory for file.
     * @param p_fileName     The file name.
     * @param p_regularExpr  Part of the name to be exhanged. (NOTE treated as a regularexpression)
     * @param p_newFileType  The new file type.
     * @return  the contents of the file as a <code>Vector</code> of <code>String</code> objects.
     * @throws IOException  if it fails to read the file.
     */
    protected Vector readNewFile(File   p_directory,
                                 String p_fileName,
    		                     String p_regularExpr,
                                 String p_newFileType) throws IOException
    {        
        Vector       l_readRows    = new Vector();
        String       l_tmpFileName = null;
        String       l_row         = null;
                
        l_tmpFileName = constructFileName(p_directory, p_fileName, p_regularExpr, p_newFileType);
        
        File l_file = new File(l_tmpFileName);
     
        // Create the File object
        BufferedReader l_tmpFileReader = null;
                 
    	l_tmpFileReader = new BufferedReader(new FileReader(l_file));
        while ((l_row = l_tmpFileReader.readLine()) != null)
        {
        	l_readRows.add(l_row);
        }
    	l_tmpFileReader.close();
    	l_tmpFileReader = null;
        return l_readRows;
    }

    /**
     * Adds a local test subscriber data.
     * 
     * @param p_subscriber  the test subscriber data.
     */
    protected void addSubscriberData(Object p_subscriber)
    {
    	i_localTestSubs.add(p_subscriber);
    }
    
    /**
     * Removes a local test subscriber data.
     * 
     * @param p_index  the index of test subscriber data.
     */
    protected void removeSubscriberData(int p_index)
    {
    	i_localTestSubs.remove(p_index);
    }
    
}
