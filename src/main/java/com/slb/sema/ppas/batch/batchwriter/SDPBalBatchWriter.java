////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       SDPBalBatchWriter.java
//  DATE            :       08-Jul-2004
//  AUTHOR          :       Emmanuel-Pierre Hebe
//  REFERENCE       :       PRD_ASCS_DEV_SS_083
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       This class represents a communication object for
//                          control of a batch process. 
//
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of the        | <reference>
//          |               | changes>                         |
//----------+---------------+----------------------------------+----------------
// 09/05/07 | Lars L.       | Method 'updateStatus()' is       | PpacLon#3033/11464
//          |               | modified in order to use the     |
//          |               | BatchJobControlData data class   |
//          |               | instead of the BatchJobData data |
//          |               | class.                           |
//----------+---------------+----------------------------------+----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SDPBalBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class will write the outcome of each attempt to move a subscriber.
 * For each failed attempt a line to an error file will be written.  For each attempt,
 * either successful or failed, a line will be written to a recovery file.
 * The processor-component has already created those strings so here they are only
 * written to file.
 */

public class SDPBalBatchWriter extends BatchWriter
{
    //-------------------------------------------------------------------------
    // Public constants.
    //-------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SDPBalBatchWriter";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /**Nb of records to be written. Gets from <code>p_params</code>. */    
    private int     i_intervall                            = 0;

    /** Count how many successfull records that has been processed. */
    private int     i_success                              = 0;

    /** Count how many  records that has been processed. */
    private int     i_processed                            = 0;
    
    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord                          = true;
    
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;

    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;

    /** Indicates if it is recovery mode (or not). */
    private boolean i_recoveryMode                         = false;


    //-------------------------------------------------------------------------
    // Private variables
    //-------------------------------------------------------------------------


    /**
     * The no-arg Constructor.
     * @param p_ppasContext  The context.
     * @param p_logger       Logger.
     * @param p_controller   Controller.
     * @param p_params       Params.
     * @param p_outQueue     The output queue.
     * @param p_properties   Properties.
     * 
     * @throws IOException Error when writing to output files.
     */
    public SDPBalBatchWriter( PpasContext      p_ppasContext,
                              Logger           p_logger,
                              BatchController  p_controller,
                              Map              p_params,
                              SizedQueue       p_outQueue,
                              PpasProperties   p_properties )
        throws IOException
    {
        super( p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10010,
                             this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        i_intervall = i_properties.getIntProperty( BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY );
        
        String l_recoverPath =
            i_properties.getTrimmedProperty( BatchConstants.C_RECOVERY_FILE_DIRECTORY );

        String l_errorPath = i_properties.getTrimmedProperty( BatchConstants.C_REPORT_FILE_DIRECTORY );

        String p_inputFile = (String)i_params.get( BatchConstants.C_KEY_INPUT_FILENAME );

        // Get the filname with out the prefix 
        String l_inputFileArr[] = p_inputFile.split( "[.]" );
        
        // Create an absolute file name and add prefix .rpt or .rec.
        String l_errorFile = l_errorPath + "/" + l_inputFileArr[0] + BatchConstants.C_EXTENSION_TMP_FILE;
        super.i_recoveryFileName = 
            l_recoverPath  + "/" + l_inputFileArr[0] + BatchConstants.C_EXTENSION_RECOVERY_FILE;
        
        super.openFile( BatchConstants.C_KEY_REPORT_FILE, new File(l_errorFile) );
        super.openFile( BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName) );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10020,
                             this,
                             BatchConstants.C_CONSTRUCTED + C_CLASS_NAME );
        }

    }



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Writes information to the recovery file and to the error file if there is any errors in the file. 
     * It also poulates a <code>BatchJobData</code> object with information from <code>p_params</code>. 
     * @param p_record Holds the information about the error line number and the recovery line number.
     * 
     * @throws IOException  Excepton raised.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             20010,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_writeRecord );
        }

        SDPBalBatchRecordData l_record = (SDPBalBatchRecordData) p_record;

        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
            if (i_numberOfFaultyRecords > 0)
            {
                // Assume that it is recovery mode.
                i_recoveryMode = true;
            }
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                "noSuccessfully processed=" + this.i_numberOfSuccessfullyProcessedRecords +
                " noFailured=" + this.i_numberOfFaultyRecords);
        }

        //Check if error file info exist and write it to file.

        if (l_record.getErrorLine() != null)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_HIGH,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 88881,
                                 this,
                                 "ERROR IN FILE [" + l_record.getErrorLine() + "]\n" +
                                 "rowNumber [" + l_record.getRowNumber() + "]" );
            }
            super.writeToFile( BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine() );
            
            super.i_errors++;

        }
        else
        {
            if (l_record.getIsRecord())
            {
                i_success++;
            }
        }
        
        if (l_record.getIsRecord())
        {
            i_processed++;

            // If this record failed during the previous run - decrease it otherwise it will be counted
            // twice as a failure record in the baco-table
            if ( l_record.getFailureStatus())
            { 
                i_numberOfFaultyRecords--;
            }
        }
        else
        {
            // It must be the header or the trailer record or a IsRecord with an invalid record type.
            // Count down the faulty records counter to avoid counting it twice if it is recovery mode.
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_HIGH,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 88888,
                                 this,
                                 C_METHOD_writeRecord + " -- A header, a trailer or a detailed record " +
                                 "with an invalid record type, rowNumber = " + l_record.getRowNumber() + ".");
            }
            
            if (i_recoveryMode)
            {
                i_numberOfFaultyRecords--;
            }
        }

        // Always write to recovery file.        
        super.writeToFile( BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine() );
       
        if ((i_processed % i_intervall) == 0)
        {
            updateStatus();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            20020,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }

    }



    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws PpasServiceException
    {
        // Flush all currently open files.
        super.flushFiles();

        // Update batch job control info record counters.
        BatchJobControlData l_batchJobControlData = i_controller.getKeyedBatchJobControlData();
        l_batchJobControlData.setNoOfRejectedRecs(i_errors + i_numberOfFaultyRecords);
        l_batchJobControlData.setNoOfSuccessfullyRecs(i_success + i_numberOfSuccessfullyProcessedRecords);
        super.updateControlInfo(l_batchJobControlData);
    }



    /** Writes out the trailer record.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS.
     * @throws IOException Error when writing to output files.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(
                l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile( BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome );
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            20022,
                            this,
                            "writeTrailerRecord -- Total number of faulty records = " + super.i_errors + ".");
        }
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }
} // End of class SDPBatchWriter
