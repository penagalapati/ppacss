////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :      BatchFile.java 
//      DATE            :      12th June 2008
//      AUTHOR          :      Michael Erskine
//      REFERENCE       :      PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :      Logica 2008
//
//      DESCRIPTION     :      Composition of components useful for handling files.
//                             At the moment this is only required for manipulation 
//                             of batch output files.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcommon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/**
 * Composition of components useful for handling files.
 * At the moment this is only required for manipulation 
 * of batch output files.
 */
public class BatchFile
{
    //
    // Class constants.
    //
    /** Name of class. */
    private static final String C_CLASS_NAME = "BatchFile";
    
    
    /** Reference to a <code>java.io.File</code>. This is useful for operations like renaming files. */
    private File i_file;
    
    /** Used for writing to an output stream in an efficient way. */
    private BufferedWriter i_buffWriter;
    
    /**
     * Constructor accepting references to File and BufferedWriter objects.
     * @param p_file       Reference to a <code>java.io.File</code> object.
     * @param p_buffWriter Reference to a <code>java.io.BufferedWriter</code> object.
     */
    public BatchFile(File p_file, BufferedWriter p_buffWriter)
    {
        i_file       = p_file;
        i_buffWriter = p_buffWriter;
        
        if ((p_file == null) || (p_buffWriter == null))
        {
            throw new PpasSoftwareException(C_CLASS_NAME,
                                            "BatchFile",
                                            10164,
                                            this,
                                            null,
                                            0L,
                                            SoftwareKey.get().invalidVariableValue("p_file or p_buffWriter",
                                                                                   "null"));
        }
    }
    
    /**
     * Returns a reference to a <code>java.io.File</code>.
     * @return Reference to a <code>java.io.File</code> object.
     */
    public File getFile()
    {
        return i_file;
    }

    /**
     * Returns a reference to a <code>BufferedWriter</code> for efficient output to an output steam.
     * @return Reference to a <code>java.io.BufferedWriter</code>.
     */
    public BufferedWriter getBuffWriter()
    {
        return i_buffWriter;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameFile = "renameFile";

    /**
     * Rename input file (changes the file extension).
     * @param p_pattern          Pattern to match.
     * @param p_currentExtension Current extension.
     * @param p_toExtension      New extension.
     * @throws IOException Any unexpected condition when attempting to rename.
     */
    public void renameFile(String p_pattern,
                           String p_currentExtension,
                           String p_toExtension)
        throws IOException
    {
        String l_newFullPathName = null;       
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10700,
                this,
                BatchConstants.C_ENTERING + C_METHOD_renameFile + " " + i_file.getPath());
        }

        // Change file extension
        if (i_file.getPath().endsWith(p_currentExtension))
        {
            l_newFullPathName = i_file.getPath().replaceFirst(p_pattern, p_toExtension);
            
            if (!i_file.renameTo(new File(l_newFullPathName)))
            {
                throw new IOException("Unable to rename file: " + i_file.getPath());
            }      
        }            
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10730,
                this,
                BatchConstants.C_LEAVING + C_METHOD_renameFile);
        }
        return;
        
    }
}
