////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchReader
//      DATE            :       22-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 08/05/07 | L Lundberg    | Method 'updateStatus()' is       | PpacLon#3033/11460
//          |               | modified in order to use the     |
//          |               | BatchJobControlData data class   |
//          |               | instead of the BatchJobData data |
//          |               | class.                           |
//----------+---------------+----------------------------------+----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.StatusChangeBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class will write the outcome of each attempt to change a subscribers' status. 
 * For each failed line to an error file will be written. For each attempt, 
 * either successful or failed, a line will be written to a recovery file.
 */
public class StatusChangeBatchWriter extends BatchWriter
{
    
    //------------------------------------------------------
    //  Class level constant
    //------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "StatusChangeBatchWriter";

    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - missing property for Report File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY =
        "Property for REPORT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Input File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY =
        "Property for INPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY =
        "Property for RECOVERY_FILE_DIRECTORY not found";
    
    /** Logger printout - missing value in Map for Input File Name.  Value is {@value}. */
    private static final String C_KEY_NOT_FOUND_INPUT_FILENAME =
        "Missing value in the Map for INPUT_FILE_NAME";
    
    /** Logger printout - cannot open report- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_REPORT_OR_RECOVERY_FILE =
        "Cannot open report- or recovery-file";


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /**Gets how many records to be written. Gets from <code>p_properties</code>. */
    private int i_interval  = 0;
    
    /** Number of records processed with success. */
    private int i_success   = 0;
    /** Total number of processed records. */
    private int i_processed = 0;    

    /** Directory for the report files, defined by properties. */
    private String i_reportFileDirectory    = null;
    /** Directory for input files, defined by properties. */
    private String i_inputFileDirectory    = null;
    /** Directory for recovery files, defined by properties. */
    private String i_recoveryFileDirectory = null;
    /** Input file name. */
    private String i_inputFileName         = null;
    
    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord          = true;
    
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;
    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;


    /**
     * Constructs a StatusChangeBatchWriter object.
     * @param p_ppasContext A PPpsCOntext
     * @param p_logger      The logger used of the writer.
     * @param p_controller  The batch jobs batch controller. 
     * @param p_parameters  Holding parameters used of the writer. 
     * @param p_outQueue    The batch jobs out queue.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public StatusChangeBatchWriter(
        PpasContext     p_ppasContext,
        Logger          p_logger,
        BatchController p_controller,
        Map             p_parameters,
        SizedQueue      p_outQueue,
        PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_parameters, p_outQueue, p_properties);

        String       l_reportFileFullPath   = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if ( this.getProperties() )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10310,
                    this,
                    "Got all properties" );
            }
           
            //Get the full path filename for the report-file 
            l_reportFileFullPath = generateFileName( i_reportFileDirectory,
                                                     i_inputFileName,
                                                     BatchConstants.C_EXTENSION_TMP_FILE);

            // Get the full path filename for the recovery-file
            super.i_recoveryFileName = generateFileName( i_recoveryFileDirectory,
                                                         i_inputFileName,
                                                         BatchConstants.C_EXTENSION_RECOVERY_FILE );
            
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10320,
                    this,
                    "reportFileFullPath= " + l_reportFileFullPath + " recoveryFileFullPath= " +
                    super.i_recoveryFileName );
            }       
            try
            {
                super.openFile(BatchConstants.C_KEY_REPORT_FILE,   new File(l_reportFileFullPath));
                super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
            }
            catch (IOException e)
            {
                
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_REPORT_OR_RECOVERY_FILE,
                                     LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10330,
                        this,
                        C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

        return;
        
    }





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        StatusChangeBatchRecordData l_record = (StatusChangeBatchRecordData)p_record;
        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                "noSuccessfully processed=" + this.i_numberOfSuccessfullyProcessedRecords +
                " noFailured=" + this.i_numberOfFaultyRecords);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                13333,
                this,
                "in writeRecord: "+l_record.dumpRecord() );
        }
        
        //Check if report file info exist and write it to file.
        if (l_record.getErrorLine() != null)
        {
            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine() );
            super.i_errors++;
        }
        else
        {
            i_success++;
        }

        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if ( l_record.getFailureStatus())
        { 
            this.i_numberOfFaultyRecords--;
        }

        i_processed++;

        //Always write to recovery file.
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                13333,
                this,
                "Write to recovery file: "+l_record.getRecoveryLine()  );
        }

        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine() );

        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }
          
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10370,
                this,
                BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }
        
        return;
    } // End of writeRecord(.)


    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws PpasServiceException
    {
        // Flush all currently open files.
        super.flushFiles();

        // Update batch job control info record counters.
        BatchJobControlData l_batchJobControlData = i_controller.getKeyedBatchJobControlData();
        l_batchJobControlData.setNoOfRejectedRecs(i_errors + i_numberOfFaultyRecords);
        l_batchJobControlData.setNoOfSuccessfullyRecs(i_success + i_numberOfSuccessfullyProcessedRecords);
        super.updateControlInfo(l_batchJobControlData);
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * This method tries to read the properies for:
     * Table update frequency and directories for the input-, report- and recovery-files
     * @return </code>true</code> if all properties read </code>false</code> otherwise
     */
    private boolean getProperties()
    {
        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        boolean l_returnValue = true;  // Assume all found
        
        // Get input filename from the Map
        this.i_inputFileName = (String)this.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME); 
        if ( this.i_inputFileName == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_KEY_NOT_FOUND_INPUT_FILENAME,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10380,
                    this,
                    C_KEY_NOT_FOUND_INPUT_FILENAME + " " + C_METHOD_getProperties );
            }
        }

        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10390,
                    this,
                    C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY + " " + C_METHOD_getProperties );
            }
        }

        // Get the Input File Directory
        i_inputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        if ( i_inputFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10400,
                    this,
                    C_PROPERTY_NOT_FOUND_INPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
        
        // Get the Recovery File Directory
        i_recoveryFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        if ( i_recoveryFileDirectory == null )
        {
            l_returnValue = false; 
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10410,
                    this,
                    C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        // Get the report file directory
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        if ( i_reportFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10420,
                    this,
                    C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
                
        return l_returnValue;
        
     } // End of method getProperties()




    /** Writes out the trailer record.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(
                l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;

        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }
} // End of StatusChangeBatchWriter
