////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchMasterUT.java
//    DATE            :       2004
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       WM-data 2006
//
//    DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//08/05/06 | Chris         | Test Disconnection Code filter   | PpacLon#2250/8691
//         | Harrison      |                                  |
//---------+---------------+----------------------------------+-----------------
//15/05/06 | Chris         | Test Disconnection Code filter   | PpacLon#2294/8812
//         | Harrison      |                                  |
//---------+---------------+----------------------------------+-----------------
//18/08/06 | Lars Lundberg | The Batch Disconnection Reason   | PpacLon#2577/9724
//         |               | code filtering test is modified. |
//         |               | Instead of waiting a certain time|
//         |               | for the batch to be completed the|
//         |               | status of the batch is checked in|
//         |               | order to track when the batch    |
//         |               | really is completed.             |
//         |               | A new method is added:           |
//         |               | waitForDiscSubJobsCompletion(..) |
//---------+---------------+----------------------------------+-----------------
//04/07/07 | E Dangoor     | Make sure that disconnect date is| PpacLon#3195/11793
//         |               | not output during test           |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchmaster;

import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.dataclass.DisconnectRequestData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasIllegalArgumentException;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.SoftwareKey;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.js.jsclient.JsClient;
import com.slb.sema.ppas.js.jscommon.JobActionException;
import com.slb.sema.ppas.js.jscommon.JsRequest;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.ReApplyProperty;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/**
 * Unit Test for the 'BatchMaster' class.
 */
public class BatchMasterUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** Constant holding the name of the current class. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchMasterUT";

    /** Constant defining the length of an msisdn in batch disconnection report record */
    private static final int    C_MSISDN_LENGTH = 15;

    /** Constant defining the start of string to compare in the disconnection report */
    private static final int    C_START_OF_COMPARISON_STRING = 0;

    /** Constant defining the end of string to compare in the disconnection report */
    private static final int    C_END_OF_COMPARISON_STRING = 17;

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public BatchMasterUT(String p_title)
    {
        super(p_title);
    }

    //------------------------------------------------------------------------
    // Public test methods
    //------------------------------------------------------------------------
    /**
     * Tests that it is possible to construct a BatchMaster instance.
     */
    public void testConstructor()
    {
        BatchMaster l_batchMaster = null;
        HashMap     l_params      = new HashMap();
        
        c_writeUtText = true;
        super.beginOfTest("testConstructor " + C_CLASS_NAME);
        
//        super.enableConsoleLog(PpasDebug.C_LVL_LOW, false);

        l_params.put(BatchConstants.C_KEY_BATCH_JOB_TYPE, BatchConstants.C_JOB_TYPE_BATCH_DISCONNECTION);
        l_params.put(BatchConstants.C_KEY_RECOVERY_FLAG,  BatchConstants.C_VALUE_RECOVERY_MODE_OFF);

        // Test the BatchMaster Constructor.
        try
        {
            l_batchMaster = new BatchMaster(c_ppasContext,
                                            "BatchMaster",       //Job type
                                            "101",           //Job id
                                            "BatchMasterUT",
                                            l_params);    //JobRunner name
        }
        catch (PpasConfigException p_configExe)
        {
            l_batchMaster = null;
            super.say("Failed to construct a BatchMaster instance, a PpasConfigException is caught.");
            super.failedTestException(p_configExe);
        }
        catch (Exception p_exe)
        {
            l_batchMaster = null;
            super.say("Failed to construct a BatchMaster instance, an unexpected Exception is caught.");
            super.failedTestException(p_exe);
        }
        
        super.assertNotNull(l_batchMaster);
        
        super.endOfTest();
    }


    /**
     * Test Batch Disconnection Reason code filtering works. Test accounts are created,
     * combinations of Immediate and Future disconnections are made using various
     * Disconnection Reason codes and then a Batch Disconnection is submitted.
     * The output files from the batch are examined and expected records compared
     * against the actuals. Disconnection Reason codes BA & DE will be filtered
     * and 2 BZ records should be in the output files.
     */
    public void testDisconnectionReasonCodeFiltering()
    {
        beginOfTest("testDisconnectionReasonCodeFiltering " + C_CLASS_NAME);

        final String L_DISCONNECTION_MASTER_JOB_TYPE = "BatchDisconnectionMaster";
        final int    L_MAX_POLL_TIME_SEC             = 600; // The maximum poll time in seconds.
        final int    L_MAX_NO_OF_POLLINGS            = 60;  // The maximum number of pollings.

        int                         l_max_msisdn_count = 6;
        int                         l_expected_count = 0;
        BasicAccountData[]          l_basicAccountData = new BasicAccountData[l_max_msisdn_count];
        DisconnectRequestData[]     l_disconnectRequestData = new DisconnectRequestData[l_max_msisdn_count];
        DisconnectData[]            l_disconnectData = new DisconnectData[l_max_msisdn_count];
        JsClient                    l_jsClient = null;
        String                      l_jsJobId    = null;
        String[]                    l_disconnectionReasonCode = { "BA", "DE" };
        Map                         l_params = null;
        PpasDateTime                l_dateTime = DatePatch.getDateTimeNow();
        boolean                     l_success  = false;
        Logger                      l_logger = null;
        InstrumentManager           l_instrumentManager = new InstrumentManager();
        PpasLoggerPool              l_loggerPool = null;
        PpasProperties              l_properties = new PpasProperties(l_instrumentManager);
        String                      l_propertiesFile = System.getProperty("ascs.localRoot") + 
                                                           "/conf/batch/batch.properties";
        String                      l_outputDiscDate;
        boolean                     l_resetProperty = false;

        try
        {
            l_properties.loadLayeredProperties("batch_dis");

            // Sets the default TimeZone on the JVM to that configured in properties
            // file.
            DatePatch.init("BatchMasterUT",
                           "setUp",
                           32457,
                           this,
                           null,
                           0L,
                           null,
                           l_properties); 

            l_loggerPool = new PpasLoggerPool("LoggerPool", l_properties, "BatchMasterUT");

            l_logger = l_loggerPool.getLogger();
            if (l_logger == null)
            {
                // The requested logger could not be found ... create and throw
                // appropriate exception
    
                say("Error: logger could not be found!");
            }

            l_logger.setInstrumentManager(l_instrumentManager);
        }
        catch (PpasConfigException l_ppasConfigEx)
        {
            sayTime("***ERROR: Failed to load batch properties or get a logger during set-up.");
            failedTestException(l_ppasConfigEx);
        }
        
        // Create some Msisdns...
        for (int l_count = 0; l_count < l_max_msisdn_count; ++l_count)
        {
            l_basicAccountData[l_count] = createTestSubscriber();

            assertNotNull("Can't createTestSubscriber", l_basicAccountData[l_count]);

            say("Created msisdn" + l_count + "=" + l_basicAccountData[l_count].getMsisdn().toString() +
                " status=" + l_basicAccountData[l_count].getCustStatusStr());
        }


        // Immediate Disconnect 1 Msisdn with a non-filtered disconnection reason code...
        l_disconnectRequestData[0] =
                disconnectTestSubscriber(l_basicAccountData[0].getMsisdn(), "BZ");
        
        assertNotNull("Can't disconnectTestSubscriber", l_disconnectRequestData[0]);

        say("Immediate Disconnection msisdn0=" + l_basicAccountData[0].getMsisdn().toString() +
            " status=" + l_disconnectRequestData[0].getCustStatus() +
            " Disconnection Reason=" + "BZ");

        
        // Future Disconnect 1 Msisdn with a non-filtered disconnection reason code...
        l_disconnectData[1] =
                futureDisconnectTestSubscriber(l_basicAccountData[1].getMsisdn(), "BZ");
        
        assertNotNull("Can't disconnectTestSubscriber", l_disconnectData[1]);

        say("Future Disconnect msisdn1=" + l_basicAccountData[1].getMsisdn().toString() +
                " Disconnection Reason=" + "BZ");
        
        
        // Immediate Disconnect some Msisdns with filtered disconnection reason codes...
        for (int l_count = 2; l_count < 4; ++l_count)
        {
            l_disconnectRequestData[l_count] =
                    disconnectTestSubscriber(l_basicAccountData[l_count].getMsisdn(),
                                             l_disconnectionReasonCode[l_count % 2]);
            
            assertNotNull("Can't disconnectTestSubscriber", l_disconnectRequestData[l_count]);

            say("Immediate Disconnection msisdn" + l_count + "=" + l_basicAccountData[l_count].getMsisdn().toString() +
                " status=" + l_disconnectRequestData[l_count].getCustStatus() + 
                " Disconnection Reason=" + l_disconnectionReasonCode[l_count % 2]);
        }

        // Future Disconnect some Msisdns with filtered disconnection reason codes...
        for (int l_count = 4; l_count < l_max_msisdn_count; ++l_count)
        {
            l_disconnectData[l_count] =
                    futureDisconnectTestSubscriber(l_basicAccountData[l_count].getMsisdn(),
                                             l_disconnectionReasonCode[l_count % 2]);
            
            assertNotNull("Can't disconnectTestSubscriber", l_disconnectData[l_count]);

            say("Future Disconnect msisdn" + l_count + "=" +
                    l_basicAccountData[l_count].getMsisdn().toString() +
                    " Disconnection Reason=" + l_disconnectionReasonCode[l_count % 2]);
        }

        // Make sure that the disconnect date is not sent to the batch output files
        l_outputDiscDate = l_properties.getProperty(BatchConstants.C_OUTPUT_DISC_DATE);
        if ("true".equals(l_outputDiscDate))
        {
            new ReApplyProperty(l_propertiesFile,
                    BatchConstants.C_OUTPUT_DISC_DATE, "false").updateFile(l_propertiesFile);
            
            l_resetProperty = true;
        }
        
        // Schedule a Batch Disconnection Master...
        l_params = new HashMap();
        l_params.put("recoveryFlag", "no");
        l_params.put("disconnectionOffset", "0");
        l_params.put("disconnectionDate", l_dateTime.toString_yyyyMMdd());
        l_params.put("batchJobType", L_DISCONNECTION_MASTER_JOB_TYPE);

        try
        {
            l_jsClient = new JsClient(c_ppasContext,
                                      l_logger,
                                      l_instrumentManager);

            l_jsJobId = l_jsClient.scheduleJob(new JsRequest("SUPER", "BatchMasterUT", "TestNode"),
                                             "batchMaster",
                                             l_params,
                                             l_dateTime,
                                             true);
                              
            // If there is a job id then the job must have been scheduled
            // successfully.                                 
            if (l_jsJobId != null)
            {
                l_success = true;
            }
        } 
        catch (PpasConfigException l_ppasConfigEx)
        {
            sayTime("***ERROR: Failed to construct a new JsClient.");
            failedTestException(l_ppasConfigEx);
        }
        catch (JobActionException l_jobActionException) 
        {
            sayTime("***ERROR: Scheduling of a Job in testDisconnectionReasonCodeFiltering failed " + 
                 "with a JobActionException");
            failedTestException(l_jobActionException);
        } 
        catch (PpasServiceException l_ppasServiceException) 
        {
            sayTime("***ERROR: Scheduling of a Job in testDisconnectionReasonCodeFiltering failed " + 
                 "with a PpasServiceException");
            failedTestException(l_ppasServiceException);
        }

        assertTrue("The Batch Disconnection Master job was not scheduled successfully!", l_success);
        
        say("The Batch Disconnection Master Job was successfully submitted, JsJobId = " + l_jsJobId);

        // Wait a substantial amount of time for all batch processing to complete...
        // (otherwise a/c's just don't get disconnected). This may even have to be
        // increased on slow machines.
//        snooze(60);

        // Wait until all disconnection sub-jobs are completed.
        assertTrue("The Disconnection Batch sub-jobs failed to be completed within the maximum poll time: " +
                   L_MAX_POLL_TIME_SEC + " seconds.",
                   waitForDiscSubJobsCompletion(l_jsJobId,
                                                L_DISCONNECTION_MASTER_JOB_TYPE,
                                                L_MAX_POLL_TIME_SEC,
                                                L_MAX_NO_OF_POLLINGS));

        // Build expected Batch Disconnection report record Strings...
        String[] l_resultString = new String[2];
        l_resultString[0] = buildBatchDisconOutputRecord(l_basicAccountData[0].getMsisdn(), "BZ"); 
        l_resultString[1] = buildBatchDisconOutputRecord(l_basicAccountData[1].getMsisdn(), "BZ"); 
        System.out.println("expect0=[" + l_resultString[0] + "]");
        System.out.println("expect1=[" + l_resultString[1] + "]");
        
        // Scan the batchfile output directory for disconnection report files...
        String l_batchOutputFileDirectory = l_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
        Vector l_fileVector = getFilenames(new File(l_batchOutputFileDirectory),
                                            false,
                                            new FileFilter()
                                            {
                                                public boolean accept(File p_file)
                                                {
                                                    return  (!p_file.isDirectory()) &&
                                                            p_file.getName().toUpperCase().startsWith("DISC_") && 
                                                            p_file.getName().endsWith(".DAT");
                                                }
                                            });
        
        // Check the contents of each file for the expected records...
        for (int l_fileCount = 0; l_fileCount < l_fileVector.size(); ++l_fileCount)
        {
            System.out.println("file[" + l_fileCount + "]=" + l_fileVector.elementAt(l_fileCount).toString());
            File l_disFile = new File(l_fileVector.elementAt(l_fileCount).toString());
            String[] l_disFileString = getFileOutput(l_disFile, 0);

            for (int l_lineCount = 0; l_lineCount < l_disFileString.length; ++l_lineCount)
            {
                System.out.println("[" + l_lineCount + "]=" + l_disFileString[l_lineCount]);
                            
                if (l_disFileString[l_lineCount].substring(C_START_OF_COMPARISON_STRING, C_END_OF_COMPARISON_STRING).equals(l_resultString[0]) ||
                    l_disFileString[l_lineCount].substring(C_START_OF_COMPARISON_STRING, C_END_OF_COMPARISON_STRING).equals(l_resultString[1]))
                {
                    ++l_expected_count;
                }
            }
        }

        say("Found " + l_expected_count + " matching records");
        assertEquals("Number of matching records is incorrect", 2, l_expected_count);

        // Reset the property that adds dates to the batch output files
        // if we changed it earlier
        if (l_resetProperty)
        {
            new ReApplyProperty(
                    l_propertiesFile,
                    BatchConstants.C_OUTPUT_DISC_DATE,
                    l_outputDiscDate).updateFile(l_propertiesFile);
        }

        endOfTest();
    }


    /**
     * Waits for the disconnection sub-jobs to be completed.
     * Polls the batch control status.
     * 
     * @param p_jsJobId
     * @param p_batchJobType     The batch job type to poll.
     * @param p_maxPollTime      The maximum poll time.
     * @param p_maxNoOfPollings  The maximum number of pollings.
     * 
     * @return <code>true</code> if the disconnection sub-jobs are completed within the given
     *         maximum poll time, <code>false</code> otherwise.
     */
    private boolean waitForDiscSubJobsCompletion(String p_jsJobId,
                                                 String p_batchJobType,
                                                 int    p_maxPollTime,
                                                 int    p_maxNoOfPollings)
    {
        boolean                 l_completed            = false;
        PpasBatchControlService l_ppasBatchControlServ = null;
        BatchJobDataSet         l_batchJobDataSet      = null;
        BatchJobData            l_batchJobData         = null;
        long                    l_sleepTime            = p_maxPollTime/p_maxNoOfPollings;

        super.sayTime("Let's wait for the disconnection sub-jobs to be completed. " +
                      "Max waiting time = " + p_maxPollTime + " sec, " +
                      "time between pollings = " + l_sleepTime + " sec.");
        l_ppasBatchControlServ = new PpasBatchControlService(CommonTestCaseTT.c_ppasRequest,
                                                             UtilTestCaseTT.c_logger,
                                                             CommonTestCaseTT.c_ppasContext);

        for (int l_ix = 0; (l_ix < p_maxNoOfPollings  &&  !l_completed); l_ix++)
        {
            try
            {
                l_batchJobDataSet =
                    l_ppasBatchControlServ.getLastRunJobDetails(CommonTestCaseTT.c_ppasRequest,
                                                                p_batchJobType,
                                                                30000);
            }
            catch (PpasServiceException l_ppasServEx)
            {
                super.sayTime("***ERROR: Failed to get the batch job '" + p_batchJobType + "' control data.");
                super.failedTestException(l_ppasServEx);
            }

            if (l_batchJobDataSet.getDataSet().size() > 0)
            {
                // Get the batch job data, it should only be one object in the batch job data set.
                l_batchJobData = (BatchJobData)l_batchJobDataSet.getDataSet().elementAt(0);
                if (l_batchJobData.getJsJobId() != null &&
                    l_batchJobData.getJsJobId().equals(p_jsJobId))
                {
                    // The batch is started!
                    if (l_batchJobData.getBatchType() != null &&
                        l_batchJobData.getBatchType().equals(p_batchJobType) &&
                        l_batchJobData.getBatchJobStatus().equals("C"))
                    {
                        l_completed = true;
                        sayTime("All Disconnection batch sub-jobs are completed!");
                    }
                    else
                    {
                        sayTime("The Disconnection batch sub-jobs are started but not yet completed!");
                        super.snooze(l_sleepTime);
                    }
                }
                else
                {
                    sayTime("The Disconnection batch sub-jobs are not yet started.");
                    super.snooze(l_sleepTime);
                }
            }
            else
            {
                sayTime("The Disconnection batch sub-jobs are not yet started!");
                super.snooze(l_sleepTime);
            }
        }

        return l_completed;
    }


    /** Method name constant. */
    private static final String C_METHOD_buildBatchDisconOutputRecord = "buildBatchDisconOutputRecord";
    /* Builds a properly formatted Batch Disconnection report record.
     * @param   p_msisdn                    The disconnected msisdn to detail in the record.
     * @param   p_disconnectionReasonCode   The disconnection reason code to detail in the record.
     * @return                              A String containing a properly formatted record. 
     */
    private String buildBatchDisconOutputRecord(Msisdn p_msisdn, String p_disconnectionReasonCode)
    {
        StringBuffer l_resultStringBuffer = new StringBuffer();
        MsisdnFormat l_msisdnFormat = c_ppasContext.getMsisdnFormatInput();
        String l_msisdn = l_msisdnFormat.format(p_msisdn);

        l_resultStringBuffer.append(l_msisdn);
        
        // Pad with spaces on the right...
        for (int l_count = 0; l_count < (C_MSISDN_LENGTH - l_msisdn.length()); l_count++ )
        {
            l_resultStringBuffer.append(" ");
        } 

        l_resultStringBuffer.append(p_disconnectionReasonCode);

        return l_resultStringBuffer.toString();
    }

    /** Method name constant. */
    private static final String C_METHOD_getFilenames = "getFilenames";
    /**
     * Retrieve a <code>Vector</code> of filenames found in a given directory that match certain criteria.
     * @param p_dir         The directory to scan.
     * @param p_recursive   If true then all sub-directories are also scanned, otherwise not.
     * @param p_fileFilter  If <code>FileFilter</code> is specified only matching filenames are retrieved
     *                      otherwise if p_fileFilter is null all filenames are retrieved.
     * @return              A <code>Vector</code> of filenames that were found to match.
     * 
     * @throws PpasIllegalArgumentException The p_dir parameter is not a valid File directory.  
     */
    private Vector getFilenames(File p_dir, boolean p_recursive, FileFilter p_fileFilter)
    {
        if (!p_dir.isDirectory())
        {
            throw new PpasIllegalArgumentException(C_CLASS_NAME,
                                                   C_METHOD_getFilenames,
                                                   10110,
                                                   this,
                                                   null,
                                                   0,
                                                   SoftwareKey.get().invalidDirectory(p_dir.toString()));
        }
         
        File[] l_files = p_dir.listFiles(p_fileFilter);
        Vector l_filesFound = new Vector();
        
        for (int l_loop = 0; (l_files != null) && (l_loop < l_files.length); l_loop++)
        {
            if (l_files[l_loop].isDirectory())
            {
                if (p_recursive)
                {
                    // Go into recursive mode, check this sub-directory...
                    l_filesFound.add(getFilenames(l_files[l_loop], p_recursive, p_fileFilter));
                }
            }         
            else
            {  
                // This is a file so add the file-object to the Vector.  
                l_filesFound.add(l_files[l_loop]);
            }
        }
        return l_filesFound;
    }
    
    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /** Define test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(BatchMasterUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }

} // End of public class BatchMasterUT
////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////

