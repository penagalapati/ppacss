////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME       :       FraudReturnsBatchController.java
//        DATE            :       02-May-2006
//        AUTHOR          :       Marianne Toernqvist
//        REFERENCE       :       PpaLon#2203/8587, PRD_ASCS00_GEN_CA_080
//
//        COPYRIGHT       :       WM-data 2006
//
//        DESCRIPTION     :       Vodafone Ireland Fraud returns.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of         | <reference>
//          |               | change>                       |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.FraudReturnsBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.FraudReturnsBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.FraudReturnsBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/**
 * This <code>FraudReturnsBatchController</code> class is the entry point for the Vodafone Ireland Fraud Returns
 * batch it is responsible to create and start all underlying components such as the reader, the processor
 * and the writer components.
 */
public class FraudReturnsBatchController extends BatchController
{

    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME             = "FraudReturnsBatchController";

    //TODO
    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS    = "batch_vfr";

//    /** Execution date used in the control block. */
//    private String i_executionDateTime                   = null;
//    
//    /** Job id from the job scheduler, used in the control block. */
//    private String i_jsJobId                             = null;

    /** Log message - invalid directory for input file. Value is {@value}. */
    private static final String C_INVALID_DIRECTORY_NAME = "Invalid directory name";

    /** Log message - invalid directory for input file. Value is {@value}. */
    private static final String C_DIRECTORY_NOT_DEFINED  = "Directory not defined";
    
    /** Log message - invalid directory for input file. Value is {@value}. */
    private static final String C_NOT_READABLE           = "Directory not readable";
    
    /** Log message - specified directory was not a directory. Value is {@value}. */
    private static final String C_NOT_A_DIRECTORY        = "Not a directory";

    /** Error message. */
    private String i_errorMsg = null;

        
    /**
     * Constructs a new FraudReturnsBatchController object.
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchInstall).
     * @param p_jobId       The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     * @throws PpasServiceException - If feature is not licensed.
     */
    public FraudReturnsBatchController( PpasContext p_context,
                                        String      p_jobType,
                                        String      p_jobId,
                                        String      p_jobRunnerName,
                                        Map         p_params)
        throws PpasConfigException, PpasServiceException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 10300, this,
                             BatchConstants.C_CONSTRUCTING );
        }

        this.init();

//        i_jsJobId           = p_jobId;
        i_executionDateTime = DatePatch.getDateTimeNow();

        p_context.validateFeatureLicence(FeatureLicence.C_FEATURE_CODE_VODAFONE_FRAUD_RETURNS);

        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME, 10310, this,
                             BatchConstants.C_CONSTRUCTED );
        }

    } // End of constructor


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise instance variables and read properties.
     * @throws PpasConfigException when configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 82601, this,
                             BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME, 82602, this,
                             BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";        
    /**
     * This method will instantiate the correct reader-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the reader-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a FraudReturnsBatchReader.
     */
    public BatchReader createReader()
    {
        FraudReturnsBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10320,  this,
                             "MTR*** "+BatchConstants.C_ENTERING + C_METHOD_createReader );
        }

        if (super.i_startComponents)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 10320,  this,
                                 "MTR*** innan instantiate reader...." );
            }

            l_reader = new FraudReturnsBatchReader(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_inQueue,
                                                   super.i_params,
                                                   super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10330, this,
                             "MTR*** "+BatchConstants.C_LEAVING + C_METHOD_createReader );
        }
        
        return l_reader;
        
    } // End of createReader()
    

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";        
    /**
     * This method will instantiate the correct writer-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the writer-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a FraudReturnsBatchWriter.
     * @throws IOException Could not create output files.
     */
    public BatchWriter createWriter() throws IOException
    {
        FraudReturnsBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10340, this,
                             BatchConstants.C_ENTERING + C_METHOD_createWriter );
        }

        if (super.i_startComponents)
        {
            l_writer = new FraudReturnsBatchWriter(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_params,
                                                   super.i_outQueue,
                                                   super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10350, this,
                             BatchConstants.C_LEAVING + C_METHOD_createWriter );
        }

        return l_writer;
        
    } // End of createWriter()


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";        
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a FraudReturnsBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        FraudReturnsBatchProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10360, this,
                             BatchConstants.C_ENTERING + C_METHOD_createProcessor );
        }

        if (super.i_startComponents)
        {
            l_processor = new FraudReturnsBatchProcessor(super.i_ppasContext,
                                                         super.i_logger,
                                                         this,
                                                         super.i_inQueue,
                                                         super.i_outQueue,
                                                         super.i_params,
                                                         this.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10370, this,
                             BatchConstants.C_LEAVING + C_METHOD_createProcessor );
        }

        return l_processor;
    }


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
     * This method is responsible to insert a record into table BACO using the defined
     * service method PpasBatchCOntrolService.addControlInformation(). The method will
     * get a BatchJobData object and populate it with required parameters.
     * Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 10380, this,
                             BatchConstants.C_ENTERING + C_METHOD_addControlInfo);
        }
        
        BatchJobData l_jobData         = null;
        String       l_inDirectoryName = null;   
        
        l_inDirectoryName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_DIRECTORY_NAME);
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME, 10380, this,
                             "In add controlInformation, inFileName=]" + l_inDirectoryName + "[");
        }


        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS);
        l_jobData.setSubJobCnt("-1");
        l_jobData.setOpId(super.getSubmitterOpid());
        l_jobData.setFileSubSeqNo(null);
        l_jobData.setExtraData1(null);
        l_jobData.setExtraData2(null);
        l_jobData.setExtraData3(null);
        l_jobData.setExtraData4(null);
        if (this.isFileNameValid(l_inDirectoryName, null, null, null, -1, -1))
        {
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_jobData.setBatchDate(i_fileDate);
            l_jobData.setFileSeqNo(i_seqNo);
            l_jobData.setNoOfSuccessRec("0");
            l_jobData.setNoOfRejectedRec("0");
        }
        else
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;
            
            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus("F");
            if (l_inDirectoryName != null )
            {
                if ( i_errorMsg == null )
                {
                    i_errorMsg = C_INVALID_DIRECTORY_NAME;
                }
                if (i_errorMsg.length() + l_inDirectoryName.length() + 2 <= 30)
                {
                    i_errorMsg += ": " + l_inDirectoryName;
                }
            }
            else
            {
                i_errorMsg = C_DIRECTORY_NOT_DEFINED;
            }

            l_jobData.setExtraData4(i_errorMsg);
            
            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + 
                                                        C_INVALID_DIRECTORY_NAME + ": " + l_inDirectoryName,
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }

        try
        {
            super.i_batchContService.addJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();
        }

        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME, 10390, this,
                             BatchConstants.C_LEAVING + C_METHOD_addControlInfo);
        }
    } // End of addControlInformation()


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * This method call the service defined by PpasBatchControlService. In this case
     * for a filedriven  updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobData l_jobData = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10400, this,
                             BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        l_jobData = getKeyedControlRecord();
        l_jobData.setBatchJobStatus( p_status );
        
        try
        {
            super.i_batchContService.updateJobDetails( null, l_jobData, super.i_timeout );
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 10410,  this,
                                 C_METHOD_addControlInfo + " PpasServiceException from updateJobDetails");
            }
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10420, this,
                             BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }

        return;
    }
 

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";        
    /**
     * This method will return a BatchJobData-object with the correct key-values set 
     * (JsJobId and executionDateTime).  The caller can then populate the record with data needed 
     * for the operation in question.  For example, the writer will get such record and
     * add number of successfully/faulty records processed and then update. 
     * @return BatchJobData job control block
     */    
    public BatchJobData getKeyedControlRecord()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10430, this,
                             BatchConstants.C_ENTERING + C_METHOD_getKeyedControlRecord );
        }

        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(i_executionDateTime.toString());
        l_jobData.setJsJobId(getJobId());

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10450, this,
                             BatchConstants.C_LEAVING + C_METHOD_getKeyedControlRecord );
        }

        return l_jobData;

    } // End of getKeyedControlRecord()

    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";        
    /**
     * The input is a directory NOT a filename.
     * Check that the directoryName really is a directory
     */
    protected boolean isFileNameValid(String p_directoryName, 
                                      String p_dummy1,
                                      String p_dummy2,
                                      String p_dummy3,
                                      int    p_dummy4,
                                      int    p_dummy5)
    {               
        File     l_tmpFile            = null;
        boolean  l_validDirectoryName = true;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                             C_CLASS_NAME, 10430, this,
                             BatchConstants.C_ENTERING + C_METHOD_isFileNameValid + " directoryName=]"+p_directoryName +"[");
        }

        if ( p_directoryName != null )
        {
            l_tmpFile = new File(p_directoryName);
            if ( l_tmpFile.isDirectory() )
            {
                if ( !l_tmpFile.canRead() )
                {
                    i_errorMsg = C_NOT_READABLE;
                    l_validDirectoryName = false;
                }
            }
            else
            {
                i_errorMsg = C_NOT_A_DIRECTORY;
                l_validDirectoryName = false;
            }
            
        }
        else
        {
            i_errorMsg = C_INVALID_DIRECTORY_NAME;
            l_validDirectoryName = false;                
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                             C_CLASS_NAME, 10450, this,
                             BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }
        
        return l_validDirectoryName;
    }    
    
}
