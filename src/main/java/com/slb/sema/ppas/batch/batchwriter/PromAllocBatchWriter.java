////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromoAllocBatchWriter
//      DATE            :       9-Aug-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       see javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 10/09/07 | K Bond        | Update the success / failure     | PpacLon#3112/12037
//          |               | counters.                        |
//----------+-----------+---+----------------------------------+-------------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.PromAllocBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.dataclass.AccountDetailBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class will write any failed attempts to allocate a promotion plan for allocation
 * at the SDP.
 */
public class PromAllocBatchWriter extends BatchWriter
{
    //--------------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "PromAllocBatchWriter";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY
        = "Property for OUTPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY
        = "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - cannot open error- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_REPORT_FILE = "Cannot open report-file";

    /** Prefix for report file name. Value is {@value}. */
    private static final String C_PREFIX_REPORT_FILE_NAME = "PALLPRV_";

    /** Help constant for constructing sequence number. Value is {@value}. */
    private static final String C_EMPTY_SEQUENCE_NUMBER = "000000";

    /** Length of sequence number in report filename. Value is {@value}. */
    private static final int    C_LENGTH_SEQUENCE_NUMBER = 6;

    /**Gets how many records to be written. Gets from start parameters. */
    private int    i_interval              = 0;
    
    /** Counter for number of successful processed records. */
    private int    i_successfulRecs      = 0;

    /** Counter for number of failed processed records. */
    private int    i_failedRecs          = 0;

    /** Help variable for full path to report file. */
    private String i_reportFileDirectory   = null;
 
    /** The <code>PpasDailySequencesService</code> object. Used to obtain the file sequence number. */
    private PpasDailySequencesService i_ppasDailySequencesService = null;

//    /** The last processed cust id. */
//    private String i_lastCustId          = null;


    /**
     * Constructor.
     * @param p_ppasContext PpasContext.
     * @param p_logger      Logger.
     * @param p_controller  BatchController.
     * @param p_params      Start parameters.
     * @param p_outQueue    Output queue.
     * @param p_properties  The batch's properties.
     */
    public PromAllocBatchWriter( PpasContext               p_ppasContext,
                                 Logger                    p_logger,
                                 PromAllocBatchController  p_controller,
                                 Map                       p_params,
                                 SizedQueue                p_outQueue,
                                 PpasProperties            p_properties )
    {
        super( p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties );

        String       l_reportFileFullPath = null;
        String       l_fileSequenceNumber = null;
        StringBuffer l_tmp                = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10500,
                             this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        i_ppasDailySequencesService = 
                new PpasDailySequencesService( null, super.i_logger, super.i_ppasContext );

        // Get the file sequence number.
        l_fileSequenceNumber = getFileSequenceNumber();
        if (l_fileSequenceNumber == null)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "Cannot obtain a sequence number from the database.",
                                                    LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_ERROR,
                                 C_CLASS_NAME,
                                 10501,
                                 this,
                                 "** WRITER ** Sequence number is null " + ",  method: Constructor." );
            }
        }

        if ( this.getProperties() )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 10502,
                                 this,
                                 "** WRITER ** Got all properties" );
            }

            //Get the full path filename for the error-file 
            l_tmp = new StringBuffer();
            l_tmp.append( this.i_reportFileDirectory );
            l_tmp.append( "/" );
            l_tmp.append( C_PREFIX_REPORT_FILE_NAME );
            l_tmp.append( DatePatch.getDateTimeNow().toString_yyyyMMdd_HHmmss().replaceFirst("-",
                 BatchConstants.C_DELIMITER_FILENAME_FIELDS) );
            l_tmp.append( BatchConstants.C_DELIMITER_FILENAME_FIELDS );
            l_tmp.append( l_fileSequenceNumber );
            l_tmp.append( BatchConstants.C_EXTENSION_TMP_FILE);

            l_reportFileFullPath = l_tmp.toString();

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 10503,
                                 this,
                                 "** WRITER ** Reportfile: ]" + l_reportFileFullPath + "[" );
            }

            try
            {
                super.openFile( BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFileFullPath) );
            }
            catch (IOException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_REPORT_FILE,
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                     PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_ERROR,
                                     C_CLASS_NAME,
                                     10504,
                                     this,
                                     C_CANNOT_OPEN_REPORT_FILE + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                     PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME,
                                     10505,
                                     this,
                                     "** WRITER ** Finally - after openFile...: ]"
                                     + l_reportFileFullPath + "[" );
                }
            }    
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             10506,
                             this,
                             BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    } // End of constructor



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Does all necessary final processing of the passed record, writing to file(s) and/or database.
     *
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException Error from updating the control information.
     */
    protected void writeRecord( BatchRecordData p_record ) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             10507,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_writeRecord );
        }

        AccountDetailBatchRecordData l_record = (AccountDetailBatchRecordData) p_record;
        
        if ( l_record != null )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 10507,
                                 this,
                                 "** WRITER ** starting with:" + l_record.dumpRecord() );
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 10507,
                                 this,
                                 "** WRITER ** record was NULL!!" );
            }
            
        }
        //Check if error file info exist and write it to file.
        if (l_record.getErrorLine() != null)
        {
            //Write to output file.
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 10507,
                                 this,
                                 "** WRITER ** before call to writeToFIle" + l_record.getErrorLine() );
            }

            super.writeToFile( BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine() );
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_START,
                                 C_CLASS_NAME,
                                 10507,
                                 this,
                                 "** WRITER ** after call to writeToFIle");
            }
            i_failedRecs++;
        }
        else
        {
        	i_successfulRecs++;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             10507,
                             this,
                             "***MTR WRITER*** i_processed=" + i_successfulRecs + " i_interval="+i_interval);
        }
        
        if ( (i_successfulRecs % i_interval) == 0 )
        {
            updateStatus();
        }

//        if (PpasDebug.on)
//        {
//            PpasDebug.print( PpasDebug.C_LVL_VLOW,
//                             PpasDebug.C_APP_SERVICE,
//                             PpasDebug.C_ST_START,
//                             C_CLASS_NAME,
//                             10507,
//                             this,
//                             "***MTR WRITER*** before get lastcustid");
//        }
//
//        i_lastCustId = l_record.getCustomerId();
//        if (PpasDebug.on)
//        {
//            PpasDebug.print( PpasDebug.C_LVL_VLOW,
//                             PpasDebug.C_APP_SERVICE,
//                             PpasDebug.C_ST_START,
//                             C_CLASS_NAME,
//                             10507,
//                             this,
//                             "** WRITER ** lastCustId="+i_lastCustId);
//        }
//
//        this.i_controller.removeCustomerInProgress(l_record.getCustomerId());
//        if (PpasDebug.on)
//        {
//            PpasDebug.print( PpasDebug.C_LVL_VLOW,
//                             PpasDebug.C_APP_SERVICE,
//                             PpasDebug.C_ST_START,
//                             C_CLASS_NAME,
//                             10507,
//                             this,
//                             "** WRITER ** after removeCustomerInProgress");
//        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             10508,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }
        return;

    } // End of writeRecord(.)



    /**
     * Does all necessary final processing update BACO-tables.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws PpasServiceException
    {
        BatchSubJobData l_jobData = null;
        super.flushFiles();
        l_jobData = ((PromAllocBatchController)super.i_controller).getKeyedControlRecord();
        // Update the 'last processed cust id' by with -1 (not used)
        l_jobData.setLastProcessedCustId("-1");
        updateSubJobRecordCounters(null,
                Integer.parseInt(l_jobData.getMasterJsJobId()),
                new PpasDateTime(l_jobData.getExecutionDateTime()),
                i_successfulRecs,
                i_failedRecs);
        super.updateControlInfo(l_jobData);

        return;
    }



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * This method tries to read the properies for:
     * Table update frequency and directories for the input-, error- and recovery-files.
     * @return True if all expected properties are found.
     */
    private boolean getProperties()
    {
        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        String  l_timeout     = null;
        boolean l_returnValue = true;  // Assume all found

        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty( BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY );
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt( l_interval );
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                                    LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_ERROR,
                                 C_CLASS_NAME,
                                 10509,
                                 this,
                                 "** WRITER ** "+C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY +
                                 " " + C_METHOD_getProperties );
            }
        }

        // Get the output file directory
        i_reportFileDirectory = i_properties.getTrimmedProperty( BatchConstants.C_REPORT_FILE_DIRECTORY );
        if ( i_reportFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
                                                    LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_ERROR,
                                 C_CLASS_NAME,
                                 10510,
                                 this,
                                 "** WRITER ** "+C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        return l_returnValue;

     } // End of method getProperties()

    /**
     * Write out the trailer record in report files where relevant - nothing in this case.
     * This will only apply to batches that have an input file.
     * @param p_outcome either SUCCESS if completed the batch or FAILURE if fatal error encountered
     * @throws IOException If it is not possible to write to the file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        //This batch does not write out trailer records    
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }

    /** Constant holding the name of this method. Value is {@value}. */
    private static final String C_METHOD_getFileSequenceNumber = "getFileSequenceNumber";
    /**
     * Returns a file sequence number which is obtained from the database.
     * 
     * @return a file sequence number obtained from the database.
     */
    private String getFileSequenceNumber()
    {
        String       l_seqNumberStr = null;
        StringBuffer l_tmpSeqNumber = null;
        long         l_seqNumber    = 0;
        String       l_batchJobType = null;
        PpasDate     l_currentDate  = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             10511,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_getFileSequenceNumber );
        }

        l_currentDate   = DatePatch.getDateToday();
        l_batchJobType  = (String) super.i_params.get(BatchConstants.C_KEY_BATCH_JOB_TYPE);
        try
        {
            l_seqNumber =
                i_ppasDailySequencesService.getNextSequenceNumber( null,
                                                                   i_timeout,
                                                                   l_batchJobType,
                                                                   l_currentDate );
            l_tmpSeqNumber = new StringBuffer();
            l_tmpSeqNumber.append(C_EMPTY_SEQUENCE_NUMBER);
            l_tmpSeqNumber.append(l_seqNumber);
            l_seqNumberStr = l_tmpSeqNumber.toString();
            l_seqNumberStr = l_seqNumberStr.substring(l_seqNumberStr.length() - C_LENGTH_SEQUENCE_NUMBER,
                                                      l_seqNumberStr.length() );
           if ( PpasDebug.on)
            {
                 PpasDebug.print(
                     PpasDebug.C_LVL_VLOW,
                     PpasDebug.C_APP_SERVICE,
                     PpasDebug.C_ST_START,
                     C_CLASS_NAME,
                     10512,
                     this,
                     "** WRITER ** "+C_METHOD_getFileSequenceNumber+" l_seqNumberStr=]"+l_seqNumberStr+"[");
            }

        }
        catch (PpasServiceException p_ppasServExe)
        {
            l_seqNumberStr = null;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             10513,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_getFileSequenceNumber );
        }
        
        return l_seqNumberStr;

    }


} // End of class PromAllocBatchWriter
