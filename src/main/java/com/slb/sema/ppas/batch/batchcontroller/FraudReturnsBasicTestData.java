////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME       :       FraudBatchBasicTestData.java
//        DATE            :       16-June-2006
//        AUTHOR          :       Marianne Toernqvist
//        REFERENCE       :       PpaLon#2400/9125, PRD_ASCS00_GEN_CA_080
//
//        COPYRIGHT       :       WM-data 2006
//
//        DESCRIPTION     :       Vodafone Ireland Fraud returns.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
// DD/MM/YY | <author>      | <description>                 | PpaLon#xxxx/yyyy
//----------+---------------+-------------------------------+--------------------
/////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.text.ParseException;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.DataObject;
import com.slb.sema.ppas.common.dataclass.ExtRechargeData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.VoucherData;
import com.slb.sema.ppas.common.dataclass.VoucherEnquiryData;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.MoneyFormat;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * Vodafone Ireland Fraud returns.
 */
public class FraudReturnsBasicTestData
{
    /** Name of test class. Value is {@value}. */
    public static final String  C_KEY_BASIC_TEST = "BasicTestOfFraudReturns";

    /** Currency instance. */
    private PpasCurrency      i_currency          = null;

    /** MSISDN formatter. */
    private MsisdnFormat      i_msisdnFormat      = null;
    
    /** The <code>MoneyFormat</code> object. */
    private MoneyFormat       i_moneyFormat       = null;

    /** ISAPI stub, handles ACIN records. */
    private AccessoryInstall  i_accessoryInstall  = null;
    
    /** ISAPI stub, handles EXRE records. */
    private ExternalRecharges i_externalRecharges = null;
    
    /** ISAPI stub, handels VoucherServer records. */
    private VoucherDetails    i_voucherDetails    = null;

    /** Standard constructor.
     * 
     * @param p_context Session context.
     */
    public FraudReturnsBasicTestData( PpasContext p_context )
    {
        super();
        i_currency          = p_context.getCurrency(0L);
        i_msisdnFormat      = p_context.getMsisdnFormatInput();        
        i_moneyFormat       = p_context.getMoneyFormat();
        
        i_accessoryInstall  = new AccessoryInstall();
        i_externalRecharges = new ExternalRecharges();
        i_voucherDetails    = new VoucherDetails();
    }

    /**
     * Get ACIN data.
     * @param p_voucherSerialNumber
     * @return Simulated data from the ACIN table.
     */
    public VoucherData getRecharge( String p_voucherSerialNumber )
    {
         return (VoucherData)i_accessoryInstall.getData(p_voucherSerialNumber);
    }
    
    /**
     * Get EXRE data.
     * @param p_voucherSerialNumber
     * @return Simulated data from the EXRE table.
     */
    public ExtRechargeData getExtRecharge( String p_voucherSerialNumber )
    {
        return (ExtRechargeData)i_externalRecharges.getData(p_voucherSerialNumber);
    }
    
    /**
     * Get Voucher Server data.
     * @param p_voucherSerialNumber
     * @return Simulated data from the Voucher Server.
     */
    public VoucherEnquiryData getVoucher( String p_voucherSerialNumber )
    {
        return (VoucherEnquiryData)i_voucherDetails.getData(p_voucherSerialNumber);
    }
    
    //////////////////////////////////////////////////////////////////////////////////////
    //
    // PRIVATE INNER HELPER CLASSes
    //
    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Handling data from the table ACIN_ACCESSORY_INSTALL table.
     */
    class AccessoryInstall extends GenerateBasicTestData
    {
        
        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_CLASS_NAME = "AccessoryInstall";
        
        /** Standard constructor. */
        AccessoryInstall()
        {
            super();
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                 C_CLASS_NAME, 11000, this,
                                 BatchConstants.C_CONSTRUCTING );
            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                                 C_CLASS_NAME, 11010, this,
                                 BatchConstants.C_CONSTRUCTED );
            }

        }
        
        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_getData = "getData";
        /**
         * Creates a data record as if it was the read from the ACIN table.
         * 
         * @param p_voucherSerialNumber Serial number of a voucher.
         * @return Voucher refill data object.
         */
        public DataObject getData( String p_voucherSerialNumber )
        {
            final String L_ACCIN_ORIGINAL_AMOUNT       = "500";
            final String L_ACCIN_NORMAL                = "086672630";
            final String L_ACCIN_EXPIRED               = "086672631";
            final String L_ACCIN_DUMMY                 = "086672632";
            final String L_ACCIN_NORMAL_MSISDN         = "0832000630";
            final String L_ACCIN_EXPIRED_MSISDN        = "0832000631";
            final String L_ACCIN_DUMMY_MSISDN          = "0832000000";
            final String L_ACCIN_NON_STANDARD_MSISDN   = "1234567890";
            
            final String L_ACCIN_EXPIRED_VOUCHER_GROUP = "EX";
            // Voucher status:
            // 0 = Available
            // 1 = Assigned
            // 2 = Damaged
            // 3 = Stolen
            // 4 = Pending
            // 5 = Unavailable
            // 6 = Unknown
            
            final int    L_VOSE_STATUS_UNKNOWN = 6;

            Money        l_tmpMoney   = null;
            Msisdn       l_msisdn     = null;
            
            String       l_testMsisdn = "08320000707";
            String       l_testType   = "AV";
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 13010, this,
                                 BatchConstants.C_ENTERING + C_METHOD_getData +
                                 "serial : " + p_voucherSerialNumber);
            }


            if ( p_voucherSerialNumber.equals(L_ACCIN_NORMAL))
            {
                l_testMsisdn = L_ACCIN_NORMAL_MSISDN;
            }
            else if ( p_voucherSerialNumber.equals(L_ACCIN_EXPIRED))
            {
                l_testMsisdn = L_ACCIN_EXPIRED_MSISDN;
                l_testType   = L_ACCIN_EXPIRED_VOUCHER_GROUP;
            }
            else if ( p_voucherSerialNumber.equals(L_ACCIN_NON_STANDARD_MSISDN))
            {
                l_testMsisdn = L_ACCIN_NON_STANDARD_MSISDN;
            }
            else if ( p_voucherSerialNumber.equals(L_ACCIN_DUMMY))
            {
                l_testMsisdn = L_ACCIN_DUMMY_MSISDN;
            }
            else
            {
                // Not in ACCIN
                return null;
            }

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME, 13020, this,
                                 "before parsing....");
            }

            try
            {
                if ( p_voucherSerialNumber.equals(L_ACCIN_NON_STANDARD_MSISDN))
                {
                    // simulate non-standard voucher, such as super value or value voucher
                    l_tmpMoney = null;
                    i_currency = null;
                }
                else
                {
                    l_tmpMoney = i_moneyFormat.parse(L_ACCIN_ORIGINAL_AMOUNT, i_currency);
                }
                l_msisdn = i_msisdnFormat.parse(l_testMsisdn);
            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 13030, this,
                                     BatchConstants.C_ENTERING + C_METHOD_getData +
                                     " ###NOTE### could not parse money or msisdn!!!!");
                }

                 e.printStackTrace();
            }
            
            VoucherData l_returnValue = new VoucherData( null,                       // p_request,
                                                         null,                       // p_custId,
                                                         l_testType,                 // p_voucherGroup,
                                                         DatePatch.getDateTimeNow(), // p_rechargeDateTime,
                                                         C_BASIC_TEST_OPERID,        // p_opid,
                                                         p_voucherSerialNumber,      // p_serialNumber,
                                                         new Long(1),                // p_linkId,
                                                         null,                       // p_initiatingCustId,
                                                         l_msisdn,                   // p_initiatingMsisdn,
                                                         null,                       // p_appliedValue,
                                                         l_tmpMoney,                 // p_originalAmount,
                                                         null,                       // p_totalAmountApplied,
                                                         null,                       // p_serviceClass,
                                                         L_VOSE_STATUS_UNKNOWN,      // p_state)
                                                         null,                       // Balance Before
                                                         null,                       // Balance After
                                                         "abcd");                    // Channel Id

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 13040, this,
                                 BatchConstants.C_LEAVING + C_METHOD_getData + " " + l_returnValue);
            }
            return l_returnValue;
        }
    } // End of class AccessoryInstall

    /**
     * Creates a data record as if it was read from the table EXRE_EXTERNAL_RECHARGES.
     */
    class ExternalRecharges extends GenerateBasicTestData
    {
        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_CLASS_NAME = "ExternalRecharges";

        /**
         * Constructor.
         */
        ExternalRecharges()
        {
            super();
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                 C_CLASS_NAME, 21000, this,
                                 BatchConstants.C_CONSTRUCTING );
            }

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                                 C_CLASS_NAME, 21010, this,
                                 BatchConstants.C_CONSTRUCTED );
            }
        } // End of contructor.

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_getData = "getData";  
        /**
         * This BASIC TEST class acts as if a call to the PpasVoucherService.getExtRecharge() has been made,
         * create hardcoded EXRE table data.
         * This will be used to check the reports layout.
         * @param p_voucherSerialNumber Voucher serial number.
         * @return Voucher refill data object.
         */
        public DataObject getData( String p_voucherSerialNumber )
        {
            final String L_EXRE_ORIGINAL_AMOUNT       = "1000";

            final String L_EXRE_NORMAL                = "086672633";
            final String L_EXRE_EXPIRED               = "086672634";
            final String L_EXRE_DUMMY                 = "086672635";
            final String L_EXRE_NORMAL_MSISDN         = "0832000630";
            final String L_EXRE_EXPIRED_MSISDN        = "0832000631";
            final String L_EXRE_DUMMY_MSISDN          = "0832000000";
            final String L_EXRE_EXPIRED_VOUCHER_GROUP = "EX";
            
            final String L_EXRE_ACTIVATION_NUMBER     = "1234567";
            
            Money        l_tmpMoney     = null;
            Msisdn       l_msisdn       = null;
            
            String       l_testMsisdn = "08320000707";
            String       l_testType   = "AV";
            if ( p_voucherSerialNumber.equals(L_EXRE_NORMAL))
            {
                l_testMsisdn = L_EXRE_NORMAL_MSISDN;
            }
            else if ( p_voucherSerialNumber.equals(L_EXRE_EXPIRED))
            {
                l_testMsisdn = L_EXRE_EXPIRED_MSISDN;
                l_testType   = L_EXRE_EXPIRED_VOUCHER_GROUP;
            }
            else if ( p_voucherSerialNumber.equals(L_EXRE_DUMMY))
            {
                l_testMsisdn = L_EXRE_DUMMY_MSISDN;
            }
            else
            {
                // Not in EXRE
                return null;
            }

            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 21030, this,
                                 BatchConstants.C_ENTERING + C_METHOD_getData + "serial : " +
                                 p_voucherSerialNumber);
            }

            try
            {
                l_tmpMoney = i_moneyFormat.parse(L_EXRE_ORIGINAL_AMOUNT, i_currency);
                l_msisdn = i_msisdnFormat.parse(l_testMsisdn);
            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 21040, this,
                                     BatchConstants.C_ENTERING + C_METHOD_getData +
                                     " ###NOTE### could not parse money or msisdn!!!!");
                }

                 e.printStackTrace();
            }
            
            ExtRechargeData l_returnValue = new ExtRechargeData(null,      // p_request,
                                               L_EXRE_ACTIVATION_NUMBER,   // p_activationNumber,
                                               p_voucherSerialNumber,      // p_serialNumber,
                                               l_msisdn,                   // p_msisdn,
                                               l_testType,                 // p_voucherGroup,
                                               l_tmpMoney,                 // p_originalAmount,
                                               DatePatch.getDateTimeNow(), // p_refillDateTime,
                                               C_BASIC_TEST_OPERID);       // p_opid)
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 21050, this,
                                 BatchConstants.C_LEAVING + C_METHOD_getData +l_returnValue);
            }
            return l_returnValue;
        }
        
    } // End of class ExternalRecharges

    /**
     * This BASIC TEST class acts as if a call to the PpasVoucherService.getVoucher() has been made,
     * create hardcoded ISAPI answer.
     * This will be used to check the reports layout.
     */
    class VoucherDetails extends GenerateBasicTestData
    {
        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_CLASS_NAME = "VoucherDetails";

        /** Standard constructor. */
        VoucherDetails()
        {
            super();
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                 C_CLASS_NAME, 11100, this,
                                 BatchConstants.C_CONSTRUCTING );
            }

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                                 C_CLASS_NAME, 11110, this,
                                 BatchConstants.C_CONSTRUCTED );
            }

        }

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_getData = "getData";        
        /**
         * This BASIC TEST class acts as if a call to the PpasVoucherService.getRecharge() has been made,
         * create hardcoded ACIN table data.
         * This will be used to check the reports layout.
         * @param p_voucherSerialNumber Voucher serial number.
         * @return Voucher refill data object.
         */
        public DataObject getData( String p_voucherSerialNumber )
        {
            final String L_VOSE_ORIGINAL_AMOUNT       = "1500";

            final String L_VOSE_USED                  = "086672636";
            final String L_VOSE_EXPIRED               = "086672637";
            final String L_VOSE_DUMMY                 = "086672638"; 
            final String L_VOSE_AVAILABLE             = "086672639";
            final String L_VOSE_USED_MSISDN           = "0832000630";
            final String L_VOSE_EXPIRED_MSISDN        = "0832000631";
            final String L_VOSE_DUMMY_MSISDN          = "0832000000";
            final String L_VOSE_AVAILABLE_MSISDN      = "0832000639";
            final String L_VOSE_PENDING_MSISDN        = "0832001639";
            final String L_VOSE_DAMAGED_MSISDN        = "0832001640";
            final String L_VOSE_EXPIRED_VOUCHER_GROUP = "EX";
            
            // Voucher status:
            // 0 = Available
            // 1 = Assigned
            // 2 = Damaged
            // 3 = Stolen
            // 4 = Pending
            // 5 = Unavailable
            // 6 = Unknown
            final int    L_VOSE_STATUS_AVAILABLE      = 0;
            final int    L_VOSE_STATUS_USED           = 1;
            final int    L_VOSE_STATUS_DAMAGED        = 2;
            final int    L_VOSE_STATUS_PENDING        = 4;
            final int    L_VOSE_STATUS_UNAVAILABLE    = 5;
            final int    L_VOSE_STATUS_UNKNOWN        = 6;
            final int    L_VOSE_STATUS_NOT_EXIST      = 10;
            
            Money        l_tmpMoney     = null;
            Msisdn       l_msisdn       = null;
            int          l_status       = L_VOSE_STATUS_UNKNOWN;
            
            String       l_testMsisdn = "08320000707";
            String       l_testType   = "AV";
            if ( p_voucherSerialNumber.equals(L_VOSE_USED))
            {
                l_testMsisdn = L_VOSE_USED_MSISDN;
                l_status     = L_VOSE_STATUS_USED;
            }
            else if ( p_voucherSerialNumber.equals(L_VOSE_EXPIRED))
            {
                l_testMsisdn = L_VOSE_EXPIRED_MSISDN;
                l_testType   = L_VOSE_EXPIRED_VOUCHER_GROUP;
                l_status     = L_VOSE_STATUS_UNAVAILABLE;
            }
            else if ( p_voucherSerialNumber.equals(L_VOSE_AVAILABLE))
            {
                l_testMsisdn = L_VOSE_AVAILABLE_MSISDN;
                l_status     = L_VOSE_STATUS_AVAILABLE;
            }
            else if ( p_voucherSerialNumber.equals(L_VOSE_DUMMY))
            {
                l_testMsisdn = L_VOSE_DUMMY_MSISDN;
                l_status     = L_VOSE_STATUS_USED;
            }
            else if ( p_voucherSerialNumber.equals(L_VOSE_PENDING_MSISDN))
            {
                l_testMsisdn = L_VOSE_PENDING_MSISDN;
                l_status     = L_VOSE_STATUS_PENDING;
            }
            else if ( p_voucherSerialNumber.equals(L_VOSE_DAMAGED_MSISDN))
            {
                l_testMsisdn = L_VOSE_DAMAGED_MSISDN;
                l_status     = L_VOSE_STATUS_DAMAGED;
            }
            else
            {
                // Not in VOSE
                l_status = L_VOSE_STATUS_NOT_EXIST;
            }

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                 C_CLASS_NAME, 12010, this,
                                 BatchConstants.C_ENTERING + C_METHOD_getData + "serial : " +
                                 p_voucherSerialNumber);
            }

            try
            {
                l_tmpMoney = i_moneyFormat.parse(L_VOSE_ORIGINAL_AMOUNT, i_currency);
                l_msisdn = i_msisdnFormat.parse(l_testMsisdn);
            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                     C_CLASS_NAME, 12020, this,
                                     BatchConstants.C_ENTERING + C_METHOD_getData +
                                     " ###NOTE### could not parse money or msisdn!!!!");
                }

                 e.printStackTrace();
            }
            
            VoucherEnquiryData l_returnValue = new VoucherEnquiryData(
                                                  null,                        // p_request,
                                                  p_voucherSerialNumber,       // p_serialNumber,
                                                  null,                        // p_agent,
                                                  null,                        // p_batchId,
                                                  DatePatch.getDateToday(),    // p_expiryDate,
                                                  C_BASIC_TEST_OPERID,         // p_opid,
                                                  l_msisdn,                    // p_subscriberId,
                                                  l_status,                    // p_voucherStatus,
                                                  l_tmpMoney,                  // p_value,
                                                  l_testType,                  // p_voucherGroup,
                                                  DatePatch.getDateTimeNow()); // p_timeStamp)

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                                 C_CLASS_NAME, 12030, this,
                                 BatchConstants.C_LEAVING + C_METHOD_getData );
            }
            return l_returnValue;
        }
    } // End of class VoucherDetails

    /** Super class for all classes that generate basic test data. */
    abstract class GenerateBasicTestData 
    {
        /** Test operator. Value is {@value}. */
        protected static final String     C_BASIC_TEST_OPERID = "TEST";

        /** Instance of a test data object. */
        protected              DataObject i_dataObject  = null;

        /** Get a test data object.
         * 
         * @param p_voucherSerialNumber Serial number of a voucher.
         * @return Test data object.
         */
        abstract public DataObject getData( String p_voucherSerialNumber );
    } // End of abstract class GenerateBasicTestData
}
