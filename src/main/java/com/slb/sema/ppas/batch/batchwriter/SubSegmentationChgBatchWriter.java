////////////////////////////////////////////////////////////////////////////////
//
//     FILE NAME       :       SubSegmentationChgBatchWriter.java
//     DATE            :       21 June 2007
//     AUTHOR          :       Ian James
//     REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//     COPYRIGHT       :       WM-data 2007
//
//     DESCRIPTION     :       Responsible for the final processing of the
//                             Subscriber Segmentation Change.
//
////////////////////////////////////////////////////////////////////////////////
//                            CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
//----------+-----------+--------------------------------------+-----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.SubSegmentationChgBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubSegChgBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for the final processing of the Subscriber Segmentation Change,
 * that is, to write any rejected subcriber accounts to a report file.
 */
public class SubSegmentationChgBatchWriter extends BatchWriter
{
    //---------------------------------------------------------------
    //  Class level constants.
    //---------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME               = "SubSegmentationChgBatchWriter";

    /** File prefix for the output filename. */
    private static final String C_REPORT_FILENAME_PREFIX   = "BATCH_CHG_SUB_SEG";

    /** Report file key for open call. Value is {@value}. */
    private static final String C_KEY_REPORT_FILE          = BatchConstants.C_KEY_REPORT_FILE;

    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int C_INDEX_FILE_DATE             = 4; // 5th element in the filename is the date
    
    /**  Array index to fine the sequence number from the filename. Value is {@value}. */
    private static final int C_INDEX_SEQ_NUMBER            = 5; // 6th element in the filename
    //--------------------------------------------------------
    //  Instance variables.
    //  ------------------------------------------------------

    /**Gets how many records to be written. Gets from <code>p_params</code>. */    
    private int              i_interval  = 0;

    /**Used to count how many successfull records that has been processed. */
    private int              i_success   = 0;

    /**Used to count how many  records that has been processed. */
    private int              i_processed = 0;

    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord                          = true;
   
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;

    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;
    
    //---------------------------------------------------------------
    //  Constructors.
    //---------------------------------------------------------------
    /**
     * Constructs an instance of this <code>SubSegmentationChgBatchWriter</code> class using the passed
     * parameters.
     * @param p_ppasContext the <code>PpasContext</code> reference.
     * @param p_logger the logger
     * @param p_batchController the batch controller
     * @param p_outQueue the output data queue
     * @param p_parameters the process paramters
     * @param p_properties the <code>PpasProperties</code> for the current process.
     * @throws IOException if it fails to open the report file.
     */
    public SubSegmentationChgBatchWriter(PpasContext p_ppasContext,
                                         Logger p_logger,
                                         BatchController p_batchController,
                                         SizedQueue p_outQueue,
                                         Map p_parameters,
                                         PpasProperties p_properties)
           throws IOException
    {
        super(p_ppasContext, p_logger, p_batchController, p_parameters, p_outQueue, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this, "Constructing " + C_CLASS_NAME);
        }
        
        i_interval = i_properties.getIntProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        
        String l_recoverPath = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);

        String l_reportPath = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);

        String l_inputFile = (String)i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);        

        //Create an absolute file name
        String l_reportFile   = this.getFullReportFileName(l_reportPath, l_inputFile );
        
        super.i_recoveryFileName = super.generateFileName(l_recoverPath, 
                                                          l_inputFile, 
                                                          BatchConstants.C_EXTENSION_RECOVERY_FILE );
        
        super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFile));
        super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this, "Constructing " + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------
    //  Protected instance methods.
    //--------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";

    /**
     * Writes the passed record's error info to the report file.
     * @param p_record The record that holds the info to be printed.
     * @throws IOException if it fails to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this, BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        SubSegChgBatchRecordData l_record = (SubSegChgBatchRecordData)p_record;

        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "noSuccessfully processed=" + this.i_numberOfSuccessfullyProcessedRecords+
                            " noFailured=" + this.i_numberOfFaultyRecords);
        }

        //If any error info exist, write it to file.
        if (l_record.getErrorLine() != null)
        {
            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine());
            super.i_errors++;
        }
        else
        {
            i_success++;
        }

        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if ( l_record.getFailureStatus())
        { 
            this.i_numberOfFaultyRecords--;
        }

        i_processed++;
        
        //Allways write to recovery file.
        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine());

        if ( (i_processed % i_interval) == 0)       
        {
            updateStatus();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this, BatchConstants.C_LEAVING + C_METHOD_writeRecord);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeTrailerRecord = "writeTrailerRecord";

    /**
     * Writes out the trailing record to the report file. The trailer record layout is a string composed of
     * the number of successfully processed records as a 6 digits string (right adjusted and padded with
     * leading zeros) followed by a 'SUCCESS' text.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCEEDED.
     * @throws IOException if it fails to write the trailing record to the report file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10050, this, BatchConstants.C_ENTERING + C_METHOD_writeTrailerRecord);
        }

        // Set the record counter to the number of successfully processed records, that is total number of
        // processed records minus the number of rejected records.
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_success - i_numberOfSuccessfullyProcessedRecords);
        l_count = l_count.substring(l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(C_KEY_REPORT_FILE, l_count + p_outcome);

        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10060, this, BatchConstants.C_LEAVING + C_METHOD_writeTrailerRecord);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */

    protected void updateStatus() throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10070, this, BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        // Flush buffered info to file.
        super.flushFiles();

        BatchJobControlData l_jobControlData = ((SubSegmentationChgBatchController)super.i_controller).getKeyedControlRecord();
        l_jobControlData.setJobType(BatchConstants.C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION);        
        l_jobControlData.setNoOfRejectedRecs(super.i_errors + i_numberOfFaultyRecords);
        l_jobControlData.setNoOfSuccessfullyRecs(i_success + i_numberOfSuccessfullyProcessedRecords);

        super.updateControlInfo(l_jobControlData);
 
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10080, this, BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    }

    /**
     * Construct full path to the report file, BATCH_CHG_SUB_SEG_yyyymmdd_sssss.RPT.
     * Information about yyyymmdd and sssss is retrieved from the original
     * input filename. Information is picked differently depending on if it is
     * short- or long filename.
     * @param p_reportPath Path to the reportfile directory.
     * @param p_inputFileName Input file name.
     * @return full path to the report file.
     */
    private String getFullReportFileName(String p_reportPath, 
                                         String p_inputFileName )
    {
        // Create the output file name like BATCH_CHG_SUB_SEG_yyyymmdd_sssss.RPT
        String l_fileDate       = null;
        String l_fileSequence   = null;
        String l_inputFileArr[] = 
        p_inputFileName.split(BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER);
        
        // Filename syntax already validated in SubSegmentationBatchReader.
        l_fileDate     = l_inputFileArr[C_INDEX_FILE_DATE];
        l_fileSequence = l_inputFileArr[C_INDEX_SEQ_NUMBER];

        StringBuffer l_tmp = new StringBuffer();
        l_tmp.append(p_reportPath);
        l_tmp.append("/");
        l_tmp.append(C_REPORT_FILENAME_PREFIX);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(l_fileDate);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(l_fileSequence);
        l_tmp.append(BatchConstants.C_EXTENSION_TMP_FILE);

        return l_tmp.toString();
        
    } // End of getFullReportFileName(..)
    
}