////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       PromPlChBatchReaderUT.java 
//DATE            :       Oct 12, 2004
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       ATOS ORIGIN 2004
//
//DESCRIPTION     :      
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of change>    | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.PromPlChBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Test program of the reader for Promotion Plan Change.
 */
public class PromPlChBatchReaderUT extends BatchTestCaseTT
{

     //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
 
//    /** The name of the indata file used in normal mode. */
//    private static final String            C_INDATA_FILENAME_NORMAL_MODE   = 
//        "PALLOAD_20040615_00004.DAT";

    /** The name of the indata file used in recovery mode. */
    private static final String            C_INDATA_FILENAME_RECOVERY_MODE = 
        "PALLOAD_20040905_00014.IPG";

//    /** Error code. Value is {@value}. */
//    private static final String            C_ERROR_INVALID_RECORD          = "01";

    /** The name of the in-queue. */
    private static final String            C_QUEUE_NAME                    = "In-queue";

    /** The maximum size of the queue. */
    private static final long              C_MAX_QUEUE_SIZE                = 100;

    /** A reference to the <code>SCChangeBatchController</code> that is used in this test. */
    private static PromPlChBatchController c_prPlChBatchController       = null;

    /** A reference to the <code>SCChangeBatchReader</code> that is used in this test. */
    private static PromPlChBatchReader     c_prPlChBatchReader           = null;

    /** The record line in a indata file. */
    private static final String            C_NORMAL_RECORD_LINE          = "     0208000001P0000299910010000";

//    /** The missing Msisdn record line in a indata file. */
//    private static final String            C_MISSING_MSISDN_LINE         = "               00000001";

//    /** The missing old service class line in a indata file. */
//    private static final String            C_MISSING_OLD_SERVICE_CLASS   = "     0208000001    0001";

//    /** The missing new service class line in a indata file. */
//    private static final String            C_MISSING_NEW_SERVICE_CLASS   = "     02080000010000    ";

//    /** The to long line line in a indata file. */
//    private static final String            C_TO_LONG_LINE                = "     020800000100000001A";     

    /**
     * Creates one instance of the unit test.
     * @param p_name This unit test's class name.
     */
    public PromPlChBatchReaderUT(String p_name) 
    {
        super(p_name);
    }
    
    /** 
     * Sets up any batch specific requirements.
     */
    protected void setUp()
    {
        super.setUp();
    }

    /** 
     * Perform standard activities at end of a test.
     */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    /** Test if a file name is valid. */
//    public void testIsFileNameValid()
//    {       
//        super.beginOfTest("testIsFileNameValidDat");
//        
//        boolean l_valid = false;
//       
//        this.createContext();   
//               
//        l_valid = c_prPlChBatchReader.isFileNameValid(C_INDATA_FILENAME_NORMAL_MODE);    
//        assertTrue(C_INDATA_FILENAME_NORMAL_MODE + " is not a valid filename ", l_valid);
//        
//        l_valid = c_prPlChBatchReader.isFileNameValid(C_INDATA_FILENAME_RECOVERY_MODE);
//        assertTrue(C_INDATA_FILENAME_RECOVERY_MODE + " is not a valid filename ", l_valid);  
//        
//        super.endOfTest();
//        
//    }
    
    /** Test to get a record. Has to be before testRecoveryBatchMode(). */   
    public void testGetRecord()
    {
        this.createContext();
        super.beginOfTest("testGetRecord");
        BatchRecordData l_recordData = null;
//        String l_wrongRecordFormat = C_ERROR_INVALID_RECORD + BatchConstants.C_DELIMITER_REPORT_FIELDS;
          
        //Test a normal input line.
        l_recordData = c_prPlChBatchReader.getRecord();

        assertEquals("The input line in the record wrong!", C_NORMAL_RECORD_LINE, l_recordData.getInputLine());

        super.endOfTest();
    }
       
    /**
     * Creates this tests context.
     */
    private void createContext()
    {
//        StringBuffer l_tmpFullPathName  = null;
        Hashtable l_parameters          = new Hashtable();
        SizedQueue l_queue              = null;
//        String l_inputFileDirectory     = null;
//        String l_fullPathName           = null;
//        String [] l_linearray           = {C_NORMAL_RECORD_LINE, 
//                                           C_MISSING_MSISDN_LINE, 
//                                           C_MISSING_OLD_SERVICE_CLASS,
//                                           C_MISSING_NEW_SERVICE_CLASS,
//                                           C_TO_LONG_LINE};
//        
//        String [] l_linearray           = {C_NORMAL_RECORD_LINE};
        
        l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INDATA_FILENAME_RECOVERY_MODE);  
  /*      
        l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INDATA_FILENAME_NORMAL_MODE);  
        l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

        //Delete the recovery file (.IPG) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(C_INDATA_FILENAME_RECOVERY_MODE);
        l_fullPathName = l_tmpFullPathName.toString();
        super.deleteFile(l_fullPathName);
        
        //Create a input file (.DAT) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(C_INDATA_FILENAME_NORMAL_MODE);
        l_fullPathName = l_tmpFullPathName.toString();
        
        super.createNewFile(l_fullPathName, l_linearray);
  */     
        try
        {
            l_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        
        try
        {
            c_prPlChBatchController = new PromPlChBatchController(super.c_ppasContext,
                                                                  "BatchPromPlCh",
                                                                  null,
                                                                  null,
                                                                  l_parameters);
        }
        catch (PpasConfigException e1)
        {
            // The configuration is missing or incomplete.
            super.failedTestException(e1);
        }
                
        c_prPlChBatchReader = new PromPlChBatchReader(super.c_ppasContext,
                                             super.c_logger,
                                             c_prPlChBatchController,
                                             l_queue,
                                             l_parameters,
                                             super.c_properties);

        assertNotNull("Failed to create a SCChangeBatchReader instance.", c_prPlChBatchReader);
        
        try
        {
            
            Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
        }
        catch (InterruptedException e)
        {
           /** Nothing done for Interrupt exception. */
        }
    }


    /** Static method that allows the framework to
     * to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase
     * classes
     * calling this suite method and get an instance of the TestCase
     * which it then
     * executes. See the main method in class for an example.
     * @return A JUnit <code>Test</code>. */
    public static Test suite()
    {
        return new TestSuite(PromPlChBatchReaderUT.class);
    }

    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        TestRunner.run(suite());
    }

}