////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutDelProcessor
//  DATE            :       03 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       This class is responsible for calling the appropriate
//                          IS API method to perform the Automated MSISDN Routing 
//                          Deletion.
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE    | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.AutoMsisdnRoutDelBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AutoMsisdnRoutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This <code>AutoMsisdnRoutDelProcessor</code> class is responsible for calling the appropriate IS API
 * method to perform the Automated MSISDN Routing Deletion.
 */
public class AutoMsisdnRoutDelProcessor extends BatchProcessor
{
    //--------------------------------------------------------------------------
    //-- Class level constant. --
    //--------------------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME               = "AutoMsisdnRoutDelProcessor";

    /** Field length of the MSISDN in the output file record. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH = 15;


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** The <code>Session</code> object. */
    private PpasSession           i_ppasSession           = null;
    
    /**
     * Constructs an instance of this <code>AutoMsisdnRoutDelProcessor</code> class using the specified
     * parameters.
     * @param p_ppasContext the PpasContext reference
     * @param p_logger the Logger
     * @param p_batchController the batch controller
     * @param p_inQueue the input data queue
     * @param p_outQueue the output data queue
     * @param p_params the start process parameters
     * @param p_properties <code>PpasProperties </code> for the batch subsystem.
     */
    public AutoMsisdnRoutDelProcessor(PpasContext p_ppasContext,
                                      Logger p_logger,
                                      AutoMsisdnRoutDelBatchController p_batchController,
                                      SizedQueue p_inQueue,
                                      SizedQueue p_outQueue,
                                      Map p_params,
                                      PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        i_ppasSession  = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            BatchConstants.C_CONSTRUCTED);
        }

    }

    //--------------------------------------------------------------------------
    //-- Protected instance methods. --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";

    /**
     * Processes and returns the given <code>BatchRecordData</code>.
     * The given <code>BatchRecordData</code> must be an instance of the
     * <code>AutoMsisdnRoutBatchRecordData</code> class.
     * This method will call the proper IS API method in order to perform a Automated MSISDN 
     * Routing Deletion.
     * Depending on the success of the operation, the record will be updated with
     * information used by the writer-component before returned.
     * This method is called from the super class 'doRun()' method.
     * 
     * @param p_record  the <code>AutoMsisdnRoutBatchRecordData</code> object to be processed.
     * @return the processed <code>BatchRecordData</code> updated with information to be used by the 
     *         writer-component.
     * @throws PpasServiceException if a fatal error occurs. 
     */
    
    public BatchRecordData processRecord(BatchRecordData p_record)
            throws PpasServiceException
    {
        AutoMsisdnRoutBatchRecordData  l_record        = null;
        String                         l_recoveryLine  = null;
        StringBuffer                   l_tmp           = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10030,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        // Cast the record into the batch specific record type
        l_record = (AutoMsisdnRoutBatchRecordData)p_record;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10040,
                this,
                "Dump Record: '" + l_record.dumpRecord());
        }
        
        // Create and set the recovery line for a successfully processed record.
        l_recoveryLine = l_record.getRowNumber() + 
                         BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                         BatchConstants.C_SUCCESSFULLY_PROCESSED;
        l_record.setRecoveryLine(l_recoveryLine);

        String l_msisdnStr = l_record.getMsisdn().trim();
        
        for ( int j = 0; j < (C_MSISDN_LENGTH - l_msisdnStr.length()); j++ )    
        {
            l_tmp.append(" ");
        }

        l_tmp.append(l_msisdnStr);
        l_record.setOutputLine(l_tmp.toString());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }

        return l_record;
    
    } // End of processRecord(.)
}        