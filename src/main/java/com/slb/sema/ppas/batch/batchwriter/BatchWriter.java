////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME : BatchWriter.java
//      DATE      : 16-April-2004
//      AUTHOR    : Urban Wigstrom
//      REFERENCE : PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT : WM-data 2007
//
//      DESCRIPTION : Responsible for establishing the required number
//                              of file-handles.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 28/03/07 | Lars L.       | One new method is added:         | PpacLon#1861/11237
//          |               | updateSubJobRecordCounters(...)  | 
//          |               | To be used by database driven    | 
//          |               | batches to update their          | 
//          |               | corresponding BatchMaster record | 
//          |               | in the BACO_BATCH_CONTROL table. | 
//----------+---------------+----------------------------------+----------------
// 24/06/08 | M Erskine | Change i_timeout to read the JDBC    | PpacLon#3650/13158
//          |           | connection timeout from the JsContext|
//          |           | . To clarify, it should NOT be the   |
//          |           | timeout for an IsClient to wait for a|
//          |           | response from an IsServer.           |
//----------+-----------+--------------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcommon.ThreadControl;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;

/** Responsible for establishing the required number of file-handles. */
public abstract class BatchWriter
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String     C_CLASS_NAME       = "BatchWriter";

    //-----------------------------------------------------
    //   Instance variables
    //------------------------------------------------------

    /** The number of threads the processor component will start and run. */
    private int                     i_numberOfThreads    = 0;

    /** Reference to an array of thread_statuses. */
    private ThreadControl[]         i_threadControlArr   = null;

    /** Reference to hashtable containing BufferedWriters. */
    protected Hashtable             i_filePointers       = null;

    /** Referens to an service in the isAPI for writning control information. */
    private PpasBatchControlService i_batchContService   = null;

    /** Reference to the out queue. */
    protected SizedQueue            i_outQueue           = null;

    /** Reference to the controller component. */
    protected BatchController       i_controller         = null;

    /** A PpasContext. */
    protected PpasContext           i_ppasContext        = null;

    /** The logger. */
    protected Logger                i_logger             = null;

    /** Holding the properties. */
    protected PpasProperties        i_properties         = null;

    /** Holding system paramters. */
    protected Map                   i_params             = null;
    
    /** Used to count how many unsuccessfull records that has been processed. */
    protected int                   i_errors    = 0;
    
    /** Recovery full path file name - needed for deletion of the file. */
    protected String                i_recoveryFileName   = null;
    
    /**
     * Maximum time to wait to get a JDBC connection from the pool. Default is 
     * <code>BatchConstants.C_DEFAULT_TIMEOUT</code>.
     */
    protected long                  i_timeout            = 0;

    /**
     * Constructs a BatchWriter object.
     * @param p_ppasContext A PPpsCOntext
     * @param p_logger The logger used of the writer.
     * @param p_controller The batch jobs batch controller.
     * @param p_params Holding parameters used of the writer.
     * @param p_outQueue The batch jobs out queue.
     * @param p_properties <code>PpasProperties </code> for the batch subsystem.
     */
    public BatchWriter(PpasContext p_ppasContext,
            Logger p_logger,
            BatchController p_controller,
            Map p_params,
            SizedQueue p_outQueue,
            PpasProperties p_properties)
    {
        i_controller = p_controller;
        i_outQueue = p_outQueue;
        i_ppasContext = p_ppasContext;
        i_params = p_params;
        i_logger = p_logger;
        i_properties = p_properties;

        init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_start = "start";

    /** Creats instances of the inner class corresponding to the number of threads to start. */
    public void start()
    {
        ThreadObject l_workerThread = null;
        i_threadControlArr = new ThreadControl[i_numberOfThreads];

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_start);
        }

        for (int i = 0; i < i_numberOfThreads; i++)
        {
            i_threadControlArr[i] = new ThreadControl();
            i_threadControlArr[i].setRunning(true);

            l_workerThread = new WriterThread(i);
            i_threadControlArr[i].setWorkerThread(l_workerThread);

            i_threadControlArr[i].getWorkerThread().start();

            i_threadControlArr[i].setPaused(false);

            i_threadControlArr[i].setState(ThreadControl.C_THREAD_UNKNOWN);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_start);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_stop = "stop";

    /** Stops all running threads. */

    public void stop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_stop);
        }

        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(false);
                i_threadControlArr[i].setRunning(false);
                i_threadControlArr[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_stop);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_pause = "pause";

    /** Pauses the all existing threads. */

    public void pause()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_pause);
        }

        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(true);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_pause);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_resume = "resume";

    /** Resuems threads that temporary are paused. */

    public void resume()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_resume);
        }

        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(false);
                i_threadControlArr[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_resume);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_tidyUp = "tidyUp";

    /**
     * Closes all opened files and disconnect from the database. Is called just before the thread terminates.
     */

    protected void tidyUp()
    {
        BufferedWriter l_tmpFile = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_tidyUp);
        }

        for (Enumeration l_enum = i_filePointers.elements(); l_enum.hasMoreElements();)
        {
            try
            {
                l_tmpFile = ((BatchFile)l_enum.nextElement()).getBuffWriter();
                l_tmpFile.flush();
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10020,
                                    this,
                                    C_METHOD_tidyUp + " closing file ");
                }
                l_tmpFile.close();
            }
            catch (IOException l_ie)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10020,
                                    this,
                                    C_METHOD_tidyUp + " failed closing file");
                }
                i_logger.logMessage(new LoggableEvent("BatchWriter:tidyUp: Exception " + l_ie,
                                                      LoggableInterface.C_SEVERITY_ERROR));

            }
        }
        
        // Avoid trying to close the files again if this method is called more than once.
        i_filePointers.clear();

        if ( ( this.i_errors == 0 ) &&
             ( this.i_recoveryFileName != null ) )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10020,
                                this,
                                C_METHOD_tidyUp + " No errors discovered the recoveryfile can be deleted ");
            }
            
            File l_tmpRecoveryFile = new File( this.i_recoveryFileName );
            l_tmpRecoveryFile.delete();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_tidyUp);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /** Performs necessary initialisation. */
    protected void init()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_init);
        }

        i_filePointers = new Hashtable(3);
        // Maybe it shall be possible to set the number of writer threads via a property in the future,
        // but for the moment it shall only be one (1) writer thread!
//        i_numberOfThreads = i_properties.getIntProperty(BatchConstants.C_NUMBER_OF_WRITER_THREADS);
        i_numberOfThreads = 1;
        
        String l_jdbcConnTimeoutLong = String.valueOf(i_ppasContext.getAttribute(
                                           "com.slb.sema.ppas.support.PpasContext.connResponseTimeout"));
        
        i_timeout = (l_jdbcConnTimeoutLong != null) ? Long.valueOf(l_jdbcConnTimeoutLong).longValue() :
                                                      BatchConstants.C_DEFAULT_TIMEOUT;

        i_batchContService = new PpasBatchControlService(null, i_logger, i_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_init);
        }
    }

    /**
     * Returns the first record in the gueue.
     * @return The first record in the queue
     * @throws SizedQueueClosedForWritingException if the queue is not available for writing.
     */

    protected BatchRecordData getRecord() throws SizedQueueClosedForWritingException
    {
        return (BatchRecordData)(i_outQueue.removeFirst());
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_openFile = "openFile";
    /**
     * Creates a new file.
     * @param p_fileKey The key used to store the file in the hashmap <code>i_filePointers</code>.
     * @param p_fileName The filename of the file that will be created.
     * @throws IOException If the file cant be found.
     */
    protected void openFile(String p_fileKey, File p_file) throws IOException
    {
        BufferedWriter l_tmpFile = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_openFile);
        }

        //Create the file based on the passed absolute filename
        try
        {
            // To avoid loss of information in the recovery file
            // if the recovery run is interrupted, the 
            // recovery file has been opened with "append=true".
            // The Reader will handle this without any changes,
            // the caching of recovery records will replace earlier
            // inserted records by later ones.
            // Also database driven batches should append to the output-file
            if ( (p_fileKey.equals(BatchConstants.C_KEY_RECOVERY_FILE)) ||
                 (p_fileKey.equals(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN)) )
            {
                l_tmpFile = new BufferedWriter(new FileWriter(p_file, true));
            }
            else
            {
                l_tmpFile = new BufferedWriter(new FileWriter(p_file));
            }
            i_filePointers.put(p_fileKey, new BatchFile(p_file, l_tmpFile));
        }
        catch (IOException l_ie)
        {
            i_logger.logMessage(new LoggableEvent("BatchWriter:openFile: Exception "
                    + "couldn't create the file " + l_ie, LoggableInterface.C_SEVERITY_ERROR));
            throw l_ie;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_openFile);
        }
    }

    /**
     * Checks if the file with the key <code>p_fileKey</code> matches an open file in
     * <code>i_filePointers</code>.
     * @param p_fileKey The key for the file in <code>i_filePointers</code>.
     * @return true if the file exist in <code>i_filePointers</code>.
     */

    protected boolean fileExists(String p_fileKey)
    {
        return i_filePointers.containsKey(p_fileKey);
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeToFile = "writeToFile";

    /**
     * Writes a line <code>p_line</code> to the file specified by <code>p_fileKey</code>.
     * @param p_fileKey The key that specifies the file to write to.
     * @param p_line The line that will be written to the file.
     * @throws IOException If it is not possible to write to the file.
     */
    protected void writeToFile(String p_fileKey, String p_line) throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_writeToFile);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10111,
                            this,
                            "file key: " + p_fileKey + ",  line: " + p_line);
        }
        
        BufferedWriter l_tmpFile = ((BatchFile)i_filePointers.get(p_fileKey)).getBuffWriter();

        try
        {
            if (p_line != null)
            {
                l_tmpFile.write(p_line);
                l_tmpFile.newLine();
            }
        }
        catch (IOException l_ie)
        {
            i_logger.logMessage(new LoggableEvent("BatchWriter:writeToFile: Exception "
                    + "was not able to write to file " + l_ie, LoggableInterface.C_SEVERITY_ERROR));
            throw l_ie;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_writeToFile);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_flushFiles = "flushFiles";

    /** Flush's all currently open files. */
    protected void flushFiles()
    {
        BufferedWriter l_tmpFile = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_flushFiles);
        }

        for (Enumeration l_enum = i_filePointers.elements(); l_enum.hasMoreElements();)
        {
            l_tmpFile = ((BatchFile)l_enum.nextElement()).getBuffWriter();
            try
            {
                l_tmpFile.flush();
            }
            catch (IOException l_ie)
            {
                i_logger.logMessage(new LoggableEvent("BatchWriter:flushFiles: Exception "
                        + "was not able to flush a file " + l_ie, LoggableInterface.C_SEVERITY_ERROR));
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_flushFiles);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateControlInfo_1 = "updateControlInfo_1";

    /**
     * Updates the control information record for each batch process.
     * @param p_jobData The data record to insert or update the control information record with.
     * @throws PpasServiceException No specific keys are anticipated.
     */

    protected void updateControlInfo(BatchJobData p_jobData) throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_updateControlInfo_1);
        }

        i_batchContService.updateJobDetails(null, p_jobData, i_timeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_updateControlInfo_1);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateControlInfo = "updateControlInfo";
    /**
     * Updates the control information record for each batch process.
     * @param p_jobControlData The data record to insert or update the control information record with.
     * @throws PpasServiceException No specific keys are anticipated.
     */

    protected void updateControlInfo(BatchJobControlData p_jobControlData) throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Enter " + C_METHOD_updateControlInfo);
        }

        i_batchContService.updateJobDetails(null, p_jobControlData, i_timeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Leaving " + C_METHOD_updateControlInfo);
        }
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateSubJobRecordCounters = "updateSubJobRecordCounters";
    /**
     * Updates the number of successfully processed and failed records in the corresponding master record
     * in the batch control table.
     * 
     * @param p_ppasRequest        The request being processed.
     * @param p_masterJsJobId      The corresponding master's js job id.
     * @param p_executionDateTime  The time stamp when the batch was executed.
     * @param p_noOfSuccessRecs    The number of successfully processed records.
     * @param p_noOfFailedRecs     The number of failed records.
     * @throws PpasServiceException  Any exception derived from the PpasServiceException class.
     *                               No specific keys are anticipated.
     */
    protected void updateSubJobRecordCounters(PpasRequest  p_ppasRequest,
                                              long         p_masterJsJobId,
                                              PpasDateTime p_executionDateTime,
                                              int          p_noOfSuccessRecs,
                                              int          p_noOfFailedRecs) throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11010,
                            this,
                            "Enter " + C_METHOD_updateSubJobRecordCounters);
        }

        i_batchContService.updateSubJobRecordCounters(p_ppasRequest,
                                                      p_masterJsJobId,
                                                      p_executionDateTime,
                                                      p_noOfSuccessRecs,
                                                      p_noOfFailedRecs,
                                                      i_timeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            11020,
                            this,
                            "Leaving " + C_METHOD_updateSubJobRecordCounters);
        }
    }


    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateControlInfo_2 = "updateControlInfo_2";
    /**
     * Updates the control information record for each sub batch process.
     * @param p_jobData The data record to insert or update the control information record with.
     * @throws PpasServiceException No specific keys are anticipated.
     */

    protected void updateControlInfo(BatchSubJobData p_jobData)
//    throws PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11010,
                            this,
                            "Enter " + C_METHOD_updateControlInfo_2);
        }

//        i_batchContService.updateSubJobDetails(null, p_jobData, i_timeout);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            11020,
                            this,
                            "Leaving " + C_METHOD_updateControlInfo_2);
        }
    }

    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected abstract void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException;

    /**
     * Writes out the trailer record in report files. This will only apply to batches that have an input file.
     * @param p_outcome either SUCCESS if completed the batch or FAILURE if fatal error encountered
     * @throws IOException If it is not possible to write to the file.
     */
    protected abstract void writeTrailerRecord(String p_outcome) throws IOException;

    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected abstract void updateStatus() throws IOException, PpasServiceException;

    /**
     * Checks if the thread is running.
     * @param p_threadId The thread's id.
     * @return True if the thread is running.
     */
    private boolean isRunning(int p_threadId)

    {
        return i_threadControlArr[p_threadId].isRunning();
    }

    /**
     * Checks if the thread is paused.
     * @param p_threadId The id of the thread to be checked.
     * @return True if the thread is paused.
     */
    private boolean isPaused(int p_threadId)
    {
        return i_threadControlArr[p_threadId].isPaused();
    }


    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_reportState = "reportState";

    /**
     * Constructs a BatchMessage for state change of a thread.
     * @param p_threadId Internal thread id
     * @param p_state The thread state e.g. Running, pause..
     */
    public synchronized void reportState(int p_threadId, int p_state)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_reportState);
        }

        // Update the status for this thread
        i_threadControlArr[p_threadId].setState(p_state);

        // Since there only will be one writer thread we don't need to loop.
         switch (i_threadControlArr[p_threadId].getState())
        {
            case ThreadControl.C_THREAD_ERROR:
                this.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                break;

            case ThreadControl.C_THREAD_RUNNING:
                this.sendBatchMessage(BatchMessage.C_STATUS_STARTED);
                break;

            case ThreadControl.C_THREAD_FINISHED:
                this.sendBatchMessage(BatchMessage.C_STATUS_FINISHED);
                break;

            case ThreadControl.C_THREAD_PAUSED:
                this.sendBatchMessage(BatchMessage.C_STATUS_PAUSED);
                break;

            case ThreadControl.C_THREAD_RESUMED:
                this.sendBatchMessage(BatchMessage.C_STATUS_RESUMED);
                break;

            case ThreadControl.C_THREAD_STOPPED:
                this.sendBatchMessage(BatchMessage.C_STATUS_STOPPED);
                break;

            default: 
                this.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                break;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10110,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_reportState);
        }

        return;

    } // end of method reportState(..)

    /**
     * Creates a BatchMessage for a thread in the BatchWriter.
     * @param p_state - the thread state
     */
    protected void sendBatchMessage(int p_state)
    {
        BatchMessage l_message = new BatchMessage();

        l_message.setSendingComponent(BatchMessage.C_COMPONENT_WRITER);
        l_message.setTargetComponent(BatchMessage.C_COMPONENT_MANAGER);
        l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
        l_message.setStatus(p_state);

        if (i_controller != null)
        {
            i_controller.postMessage(l_message);
        }
    } // end of private method sendBatchMessage(.)

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getYesNoParam = "getYesNoParam";

    /**
     * Gets a yes/no string from the parameter map and converts to boolean. The
     * parameter must be in the map.
     * @param p_key Key of parameter.
     * @return Boolean value of the key.
     */
    protected boolean getYesNoParam(
        String                        p_key)
    {
        String                         l_value = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11100, this,
                "Entered " + C_METHOD_getYesNoParam);
        }

        l_value = (String)i_params.get(p_key);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11190, this,
                "Leaving " + C_METHOD_getYesNoParam);
        }

        return l_value.equals(BatchConstants.C_VALUE_YES);
    } // end of getYesNoParam
    /** The engine in the batch data collector. */

    
    
    /**
     * This help method generates a full file name with directory, filename and new filetype.
     * It does not matter if the input file is a .DAT .SCH or a .IPG.
     * @param p_directoryPath  Full directory path.
     * @param p_inputFileName  Input file name.
     * @param p_fileSuffix New file extension.
     * @return Full path and filename.
     */
    protected String generateFileName( String p_directoryPath,
                                       String p_inputFileName,
                                       String p_fileSuffix )
    {
        StringBuffer l_tmp               = new StringBuffer();
        String[]     l_tmpFileComponents = null;
        
        l_tmp.append(p_directoryPath);
        l_tmp.append( "/");
        l_tmpFileComponents = p_inputFileName.split(BatchConstants.C_PATTERN_FILENAME_EXTENSION_DELIMITER);
        l_tmp.append(l_tmpFileComponents[0]);
        l_tmp.append( p_fileSuffix );

        return l_tmp.toString();

    } // End of generateFileName(...)
   

    /**
     * This class is the engine in the batch data collector.
     * Each stated thread will have its own instance of this class.
     */
    private class WriterThread extends ThreadObject
    {
        /** The threads id. */
        private int i_threadId = 0;

        /**
         * Creates a WriterThread object.
         * @param p_threadId The threads id.
         */
        public WriterThread(int p_threadId)
        {
            i_threadId = p_threadId;
        }

        /** Method name constant used in calls to middleware. Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";

        /** Gets the records from the out queue. */
        public void doRun()
        {
            BatchRecordData l_record = null;
            boolean l_continue = true;
            boolean l_stopped = false;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_START,
                                C_CLASS_NAME,
                                10010,
                                this,
                                "Enter " + C_METHOD_doRun);
            }

            try
            {
                BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_RUNNING);

                while (l_continue)
                {
                    //Check if thread is ordered to stop.
                    // If it is ordered to stop, it will not really stop until the queue is empty and closed
                    // for writing, i.e. no more records will be added to the queue.
                    if (!BatchWriter.this.isRunning(i_threadId)
                            && BatchWriter.this.i_outQueue.isClosedForWriting()
                            && BatchWriter.this.i_outQueue.isEmpty())
                    {
                        BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_STOPPED);
                        BatchWriter.this.writeTrailerRecord(BatchConstants.C_TRAILER_FAILED);
                        l_continue = false;
                        l_stopped = true;
                        continue;
                    }

                    // Check if it is ordered to pause.
                    // If it is ordered to pause, it will not really pause until the queue is empty and closed
                    // for writing, i.e. no more records will be added to the queue.
                    if (BatchWriter.this.isPaused(i_threadId)
                            && BatchWriter.this.i_outQueue.isClosedForWriting()
                            && BatchWriter.this.i_outQueue.isEmpty())
                    {
                        BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_PAUSED);
                        //                        BatchWriter.this.i_threadControlArr[i_threadId].wait();
                        synchronized (BatchWriter.this.i_threadControlArr[i_threadId])
                        {
                            try
                            {
                                BatchWriter.this.i_threadControlArr[i_threadId].wait();
                            }
                            catch (InterruptedException p_iEx)
                            {
                                // No specific handling of the 'InterruptedException'.
                                p_iEx = null;
                            }
                        }
                        BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_RESUMED);
                    }

                    // Let's read and process a record.
                    try
                    {
                        l_record = BatchWriter.this.getRecord();
                    }
                    catch (SizedQueueClosedForWritingException l_qE)
                    {
                        // If the writer process is neither stopped nor paused then the work is completed and
                        // no more records needs to be processed.
                        if (BatchWriter.this.isRunning(i_threadId) && !BatchWriter.this.isPaused(i_threadId))
                        {
                            l_continue = false;
                        }
                        l_record = null;
                        continue;
                    }

                    // Process record.
                    BatchWriter.this.writeRecord(l_record);
                } //end while

                // Update the sub job control record.
                BatchWriter.this.updateStatus();

                if (!l_stopped)
                {
                    // Write a trailer record.
                    BatchWriter.this.writeTrailerRecord(BatchConstants.C_TRAILER_SUCCESS);

                    // Report that the writer process has finished.
                    BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_FINISHED);
                }
            }
            catch (Exception l_Ex)
            { //We don't care about the type of exception. All will be handled in the same way.
                i_logger.logMessage(new LoggableEvent("BatchWriter.doRun:" +
                                                      "***ERROR: An unexpected Exception occurred: " + l_Ex,
                                                      LoggableInterface.C_SEVERITY_ERROR));
                BatchWriter.this.reportState(i_threadId, ThreadControl.C_THREAD_ERROR);
            }

            BatchWriter.this.tidyUp();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_END,
                                C_CLASS_NAME,
                                10020,
                                this,
                                "Leaving " + C_METHOD_doRun);
            }
        }

        /**
         * Returns the name to use for the thread name of the thread this object is running in.
         * @return the unique thread name of the thread running in this object.
         */
        protected String getThreadName()
        {
            return "batWritThd";
        }
    }

}
