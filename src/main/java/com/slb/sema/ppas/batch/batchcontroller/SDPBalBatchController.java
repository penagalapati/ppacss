////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       SDPBalBatchController.java
//  DATE            :       08-Jul-2004
//  AUTHOR          :       Emmanuel-Pierre Hebe
//  REFERENCE       :       PRD_ASCS_DEV_SS_083
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                        | REFERENCE
//----------+------------+------------------------------------+-----------------
// DD/MM/YY | <name>     | <brief description of the          | <reference>
//          |            | changes>                           |
//----------+------------+------------------------------------+-----------------
// 09/05/07 | Lars L.    | Extends the super class            | PpacLon#3033/11464
//          |            | FileDrivenBatchController instead  |
//          |            | of  the BatchController class.     |
//          |            | Added methods:                     |
//          |            | getBatchJobType()                  |
//          |            | getValidFilenameRegEx()            |
//          |            | setBatchControlDataAttribs()       |
//          |            | Removed methods:                   |
//          |            | addControlInformation()            |
//          |            | getKeyedControlRecord()            |
//          |            | isFileNameValid(...)               |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.SDPBalBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.SDPBalBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.SDPBalBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;


/**
 * This class is the entry point for SDP Balancing and will be initiated by either
 * the Job Scheduler or via its own bootstrap main.  It is responsible to create
 * and start all underlying components and will also be used for "call-back" to
 * the initiating client.
 */

public class SDPBalBatchController extends FileDrivenBatchController
{
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** The class name. */
    private static final String C_CLASS_NAME = "SDPBalBatchController";

    /** The regular expression that defines a valid file sequence number.
     *  NB: This regular expression defines only one capturing group that will contain the sub-seq number. */
    private static final String C_REG_EX_VALID_FILE_SUBSEQNO = "(\\d{2})";
    
    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_sdp";
    

    /**
     * The Constructor.
     * @param p_context         The context
     * @param p_jobType         The job type
     * @param p_jobId           The job Id
     * @param p_jobRunnerName   The job runner name
     * @param p_params          Params
     * 
     * @throws PpasConfigException  Exception
     */
    public SDPBalBatchController(PpasContext  p_context,
                                 String       p_jobType,
                                 String       p_jobId,
                                 String       p_jobRunnerName,
                                 Map          p_params)
        throws PpasConfigException
    {
        super( p_context, p_jobType, p_jobId, p_jobRunnerName, p_params );

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10010, this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10020, this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10110, this,
                            BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10120, this,
                            BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates a <code>SDPBalBatchReader</code>.
     * @return  The created SDPBalBatchReader.
     */
    protected BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10210, this,
                            BatchConstants.C_ENTERING + C_METHOD_createReader);
        }
        
        SDPBalBatchReader l_reader = null;
        
        if (super.i_startComponents)
        {
            /* Instantiate the relevant type of reader and pass reference to controller */
            l_reader = new SDPBalBatchReader(super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_inQueue,
                                             super.i_params,
                                             super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10220, this,
                            BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
        return l_reader;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /** 
     * Creates a <code>SDPBalBatchWriter</code>.
     * @return  The created SDPBalBatchWriter.
     * 
     * @throws IOException  Exception
     */
    public BatchWriter createWriter()
        throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10310, this,
                            BatchConstants.C_ENTERING + C_METHOD_createWriter );
        }
        
        SDPBalBatchWriter l_writer = null;
        
        if (super.i_startComponents)
        {
            /* Instantiate the relevant type of writer and pass reference to controller */
            l_writer = new SDPBalBatchWriter(super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_params,
                                             super.i_outQueue,
                                             super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10320, this,
                            BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        return l_writer;
    }



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";
    /** 
      * Creates a <code>SDPBalBatchProcessor</code>.
      * @return  The created SDPBalBatchProcessor. 
      */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10410, this,
                            BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }
        
        SDPBalBatchProcessor l_processor = null;
        
        if (super.i_startComponents)
        {

            /* Instantiate the relevant type of processor and pass reference to controller */
            l_processor = new SDPBalBatchProcessor(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_inQueue,
                                                   super.i_outQueue,
                                                   super.i_params,
                                                   super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10420, this,
                            BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }
        return l_processor;
    }


    /**
     * Returns the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected String getBatchJobType()
    {
        return BatchConstants.C_JOB_TYPE_BATCH_SDP_BALANCING;
    }


    /**
     * Returns a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected String getValidFilenameRegEx()
    {
        StringBuffer l_filenameRegExSB = new StringBuffer("CHANGE_SDP_");
        l_filenameRegExSB.append(super.C_REG_EX_VALID_FILE_SEQNO + "_");
        l_filenameRegExSB.append(C_REG_EX_VALID_FILE_SUBSEQNO);
        l_filenameRegExSB.append(super.C_REG_EX_VALID_EXTENSION_DELIM);
        l_filenameRegExSB.append(super.C_REG_EX_VALID_EXTENSION);
        return l_filenameRegExSB.toString();
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_setBatchControlDataAttribs = "setBatchControlDataAttribs";
    /**
     * Sets the batch control data (instance) attributes.
     * NB: The length of the given attributes <code>String</code> array is equal to the number of
     *     capturing groups defined in the regular expression returned by the method
     *     <code>getValidFilenameRegEx()</code>.
     * 
     * @param p_controlDataAttribArr  the batch control data attributes as a <code>String</code> array.
     */
    protected void setBatchControlDataAttribs(String[] p_controlDataAttribArr)
    {
        final int L_FILE_SEQNO_IX         = 0;
        final int L_FILE_SUB_SEQNO_IX     = 1;
        final int L_FILENAME_EXTENSION_IX = 2;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10430, this,
                            BatchConstants.C_ENTERING + C_METHOD_setBatchControlDataAttribs);
        }

        if (p_controlDataAttribArr != null)
        {
            if (p_controlDataAttribArr.length > 0)
            {
                // Set the file sequence number.
                i_fileSeqNo = Integer.parseInt(p_controlDataAttribArr[L_FILE_SEQNO_IX]);
            }

            if (p_controlDataAttribArr.length > 1)
            {
                // Set the file sequence number.
                i_fileSubSeqNo = Integer.parseInt(p_controlDataAttribArr[L_FILE_SUB_SEQNO_IX]);
            }

            if (p_controlDataAttribArr.length > 2)
            {
                // Set the file sequence number.
                i_extension = p_controlDataAttribArr[L_FILENAME_EXTENSION_IX];
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10440, this,
                            BatchConstants.C_LEAVING + C_METHOD_setBatchControlDataAttribs);
        }
    }


    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * Updates the status of the job.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobControlData l_batchJobControlData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10450, this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        l_batchJobControlData = getKeyedBatchJobControlData();
        l_batchJobControlData.setStatus(p_status.charAt(0));

        try
        {
            super.i_batchContService.updateJobDetails(null, l_batchJobControlData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10455, this,
                                C_METHOD_updateStatus + " PpasServiceException from updateStatus");
            }
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10460, this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10470, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10480, this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }
        return l_batchJobControlData;
    }


//    /** Method name constant used in calls to middleware.  Value is {@value}. */
//    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
//
//    /**
//     * isFileNameValid : check if the filename is valid.
//     *
//     * @param p_fileName  The filename
//     * 
//     * @return boolean: True is it's valid
//     */
//    public boolean isFileNameValid( String p_fileName )
//    {
//        boolean  l_validFileName = true; // Assume it's always true
//
//        if (PpasDebug.on)
//        {
//            PpasDebug.print( PpasDebug.C_LVL_VLOW,
//                             PpasDebug.C_APP_SERVICE,
//                             PpasDebug.C_ST_START,
//                             C_CLASS_NAME,
//                             19400,
//                             this,
//                             BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
//        }
//        
//        if (p_fileName != null) 
//        {
//            if (!p_fileName.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_DAT) &&
//                !p_fileName.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_SCH) &&
//                !p_fileName.matches(BatchConstants.C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_IPG) )
//            {
//                // invalid fileName
//                l_validFileName = false;
//            }
//        }        
//        else
//        {
//            l_validFileName = false; 
//        }
//
//        if (PpasDebug.on)
//        {
//            PpasDebug.print( PpasDebug.C_LVL_VLOW,
//                             PpasDebug.C_APP_SERVICE,
//                             PpasDebug.C_ST_END,
//                             C_CLASS_NAME,
//                             19410,
//                             this,
//                             BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
//        }
//
//        return l_validFileName;
//    } // End of isFileNameValid
//
//
}
