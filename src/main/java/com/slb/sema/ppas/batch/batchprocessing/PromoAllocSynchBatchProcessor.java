////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       PromoAllocSynchBatchProcessor.java
//DATE            :       Sep 1, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       WM-data 2005
//
//DESCRIPTION     :       This class is responsible for calling the appropriate
//                        IS API method to perform the promotion plan allocation
//                        synchronisation.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
//16/11/05 | Lars. L.      | The Exception handling is mod. in| PpacLon#1820/7395
//         |               | order to not stop the whole      |
//         |               | process when an un-mapped        |
//         |               | Exception is caught.             |
//         |               | The Exception key                |
//         |               | PROMO_ALLOC_SYNCH_FAILURE is now |
//         |               | mapped to error code '04'.       |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.PromoAllocSynchBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.PromoAllocSynchBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasCustPromoAllocService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This <code>PromoAllocSynchBatchProcessor</code> class is responsible for calling the appropriate IS API
 * method to perform the Promotion Plan Allocation Synchronisation.
 */
public class PromoAllocSynchBatchProcessor extends BatchProcessor
{
    //---------------------------------------------------------------
    //  Class level constants.
    //---------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                            = "PromoAllocSynchBatchProcessor";

    /** Constant holding the error code for "allocation already started". Value is {@value}. */
    private static final String C_ERROR_CODE_ALLOCATION_ALREADY_STARTED = "01";

//    /** Constant holding the error code for "allocation already ended". Value is {@value}. */
//    private static final String C_ERROR_CODE_ALLOCATION_ALREADY_ENDED   = "02";

    /** Constant holding the error code for "a change dates would result in overlap". Value is {@value}. */
    private static final String C_ERROR_CODE_DATES_CHANGED_OVERLAP      = "03";

    /** Constant holding the error code for "inconsistent data". Value is {@value}. */
    private static final String C_ERROR_CODE_INCONSISTENT_DATA          = "04";

    /** The mapping between the error codes and the corresponding 'PpasServiceException' message keys. */
    private static final String[][] C_ERROR_CODE_MAPPINGS               =
    {
        // Mappings to 'PpasServiceException' message keys thrown by the IS API method.
//      {PpasServiceMsg.C_KEY_GENERAL_FAILURE,                   ???},
//      {PpasServiceMsg.C_KEY_SERVICE_REQUEST_TIMEOUT,           ???},

        {PpasServiceMsg.C_KEY_PROMO_ALLOC_ALREADY_SYNCHRONIZED,  C_ERROR_CODE_ALLOCATION_ALREADY_STARTED},
        {PpasServiceMsg.C_KEY_PROMO_ALLOC_OVERLAP_NOT_ALLOWED,   C_ERROR_CODE_DATES_CHANGED_OVERLAP},
        {PpasServiceMsg.C_KEY_DATABASE_INCONSISTENCY,            C_ERROR_CODE_INCONSISTENT_DATA},
        {PpasServiceMsg.C_KEY_PROMO_ALLOC_SYNCH_FAILURE,         C_ERROR_CODE_INCONSISTENT_DATA},
        {PpasServiceMsg.C_KEY_PROMO_ALLOC_NOT_FOUND,             C_ERROR_CODE_INCONSISTENT_DATA},
        {PpasServiceMsg.C_KEY_DUPLICATE_KEY,                     C_ERROR_CODE_INCONSISTENT_DATA}
    };

    /** The 'PpasServiceException' message key index in the error code mapping array C_ERROR_CODE_MAPPINGS. */
    private static final int C_INDEX_MESSAGE_KEY = 0;

    /** The error code index in the error code mapping array C_ERROR_CODE_MAPPINGS. */
    private static final int C_INDEX_ERROR_CODE  = 1;


    //--------------------------------------------------------------------------
    //--  Class level variables.                                              --
    //--------------------------------------------------------------------------
    /** The trace print out base statement number. */
    private static int c_traceStatementNumber = 12000;


    //---------------------------------------------------------------
    //  Instance variables.
    //---------------------------------------------------------------
    /** The IS API used to perform the Promotion Plan Allocation Synchronisation. */
    private PpasCustPromoAllocService i_isApi       = null;

    /** The current <code>Session</code> object reference. */
    private PpasSession               i_ppasSession = null;

    
    //---------------------------------------------------------------
    //  Constructors.
    //---------------------------------------------------------------
    /**
     * Constructs an instance of this <code>PromoAllocSynchBatchProcessor</code> class using the passed
     * parameters.
     *
     * @param p_ppasContext      the <code>PpasContext</code> reference.
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_parameters       the process paramters
     * @param p_properties       the <code>PpasProperties</code> for the current process.
     */
    public PromoAllocSynchBatchProcessor(PpasContext                    p_ppasContext,
                                         Logger                         p_logger,
                                         PromoAllocSynchBatchController p_batchController,
                                         SizedQueue                     p_inQueue,
                                         SizedQueue                     p_outQueue,
                                         Map                            p_parameters,
                                         PpasProperties                 p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_parameters, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10000,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        i_isApi       = new PpasCustPromoAllocService(null, p_logger, p_ppasContext);
        i_ppasSession = new PpasSession();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    } //End of 'PromoAllocSynchBatchProcessor' Constructor.


    //---------------------------------------------------------------
    //  Public methods.
    //---------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * Processes and returns the given <code>BatchRecordData</code>.
     * The given <code>BatchRecordData</code> must be an instance of the
     * <code>PromoAllocSynchBatchRecordData</code> class.
     * This method will call the proper IS API method in order to perform a Promotion Plan Allocation 
     * Synchronisation.
     * Depending on the success of the operation, the record will be updated with
     * information used by the writer-component before returned.
     * This method is called from the super class 'doRun()' method.
     * 
     * @param p_record  the <code>PromoAllocSynchBatchRecordData</code> object to be processed.
     * 
     * @return the processed <code>BatchRecordData</code> updated with information to be used by the 
     *         writer-component.
     * 
     * @throws PpasServiceException  if the business service fails to perform the Promotion Plan Allocation 
     *                               Synchronisation.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        PromoAllocSynchBatchRecordData l_record       = null;
        PpasRequest                    l_ppasRequest  = null;
        String                         l_opId         = null;
        String                         l_recoveryLine = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        // Cast the record into the batch specific record type
        l_record = (PromoAllocSynchBatchRecordData)p_record;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10022,
                this,
                "Dump Record: '" + l_record.dumpRecord());
        }

        l_opId        = super.i_batchController.getSubmitterOpid();
        l_ppasRequest = new PpasRequest(i_ppasSession, l_opId, l_record.getCustomerId());
        l_ppasRequest.setContext(super.i_ppasContext);

        try
        {
            // Call the IS API.
            i_isApi.synchronizePromo(l_ppasRequest, super.i_isApiTimeout);
            
            // Create and set the recovery line for a successfully processed record.
            l_recoveryLine = l_record.getRowNumber() + 
                             BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                             BatchConstants.C_SUCCESSFULLY_PROCESSED;
            l_record.setRecoveryLine(l_recoveryLine);
        }
        catch (PpasServiceException p_ppasServiceEx)
        {
            PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_ERROR,
                            C_CLASS_NAME,
                            10024,
                            this,
                            "PpasServiceException is caught -- msg: " + p_ppasServiceEx.getMessage() +
                            ",  key: " + p_ppasServiceEx.getMsgKey());

            // Create and set the recovery line for an erroneous record.
            l_recoveryLine = l_record.getRowNumber() +
                             BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                             BatchConstants.C_NOT_PROCESSED;
            l_record.setRecoveryLine(l_recoveryLine);
            mapException(l_record, p_ppasServiceEx);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10026,
                this,
                BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }
        return l_record;
    }


    //---------------------------------------------------------------
    //  Private methods.
    //---------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_adjustString = "adjustString";
    /**
     * Adjusts and returns the given string to the given length, padded with the given character.
     * The given string is right or left adjusted depending on the given boolean indicator, if it is set to
     * <code>true</code> the string is right adjusted, otherwise it is left adjusted.
     * 
     * @param p_string         the string to be adjusted.
     * @param p_maxLength      the maximum length of the string.
     * @param p_padChar        the character used to pad the given string, if necessary.
     * @param p_rightAdjusted  the adjustment indicator, if it is set to <code>true</code> the string is
     *                         right adjusted, otherwise it is left adjusted.
     * 
     * @return the given string right or left adjusted, to the given length, padded with the given character.
     */
    private String adjustString(String p_string, int p_maxLength, char p_padChar, boolean p_rightAdjusted)
    {
        StringBuffer l_adjustedString = null;
        int          l_length         = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_adjustString + 
                ",  given string: '" + p_string + "'.");
        }

        l_adjustedString = new StringBuffer(p_string);
        l_length         = p_maxLength - p_string.length();

        for (int i = 0; i < l_length; i++)
        {
            if (p_rightAdjusted)
            {
                l_adjustedString.insert(0, p_padChar);
            }
            else
            {
                l_adjustedString.append(p_padChar);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_adjustString +
                ",  adjusted string: '" + l_adjustedString.toString() + "'.");
        }
        return l_adjustedString.toString();
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_mapException = "mapException";        
    /**
     * Updates the error line of the passed <code>PromoAllocSynchBatchRecordData</code> object
     * with an error code that corresponds to the passed <code>PpasServiceException</code>'s message key.
     * If there is no corresponding error code the message "(An unexpected PpasServiceException: msg_key)"
     * will be appended to the error line instead of an error code.
     * 
     * @param p_record      the <code>PromoAllocSynchBatchRecordData</code>.
     * @param p_ppasServEx  the <code>PpasServiceException</code>.
     */
    private void mapException(PromoAllocSynchBatchRecordData p_record, PpasServiceException p_ppasServEx)
    {
        StringBuffer l_errorLine   = null;
        boolean      l_mappingDone = false;
        boolean      l_ignoreError = false;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_mapException);
        }

        // Prepare the error line.
        l_errorLine = new StringBuffer(adjustString(p_record.getMsisdn(), 15, ' ', false));
        l_errorLine.append((p_record.getSDPPromoPlanId() != null ? p_record.getSDPPromoPlanId() : ""));
        l_errorLine.append(p_record.getSDPPromoPlanStartDateTime().toString_yyyyMMdd());
        l_errorLine.append(p_record.getSDPPromoPlanEndDateTime().toString_yyyyMMdd());
        l_errorLine.append((p_record.getASCSPromoPlanId() != null ? p_record.getASCSPromoPlanId() : ""));
        l_errorLine.append(p_record.getASCSPromoPlanStartDateTime().toString_yyyyMMdd());
        l_errorLine.append(p_record.getASCSPromoPlanEndDateTime().toString_yyyyMMdd());

        trace(this, C_METHOD_mapException, "Number of error code mappings = " + C_ERROR_CODE_MAPPINGS.length);
        // Look for matching mapping through the whole error code mapping array or until a matching
        // mapping is done.
        for (int i = 0; (i < C_ERROR_CODE_MAPPINGS.length  &&  !l_mappingDone); i++)
        {
            if (p_ppasServEx.getMsgKey().equals(C_ERROR_CODE_MAPPINGS[i][C_INDEX_MESSAGE_KEY]))
            {
                if (C_ERROR_CODE_MAPPINGS[i][C_INDEX_ERROR_CODE] != null)
                {
                    l_errorLine.append(C_ERROR_CODE_MAPPINGS[i][C_INDEX_ERROR_CODE]);
                    trace(this, C_METHOD_mapException, "matching mapping found, key = " + 
                                                       C_ERROR_CODE_MAPPINGS[i][C_INDEX_MESSAGE_KEY] +
                                                       ",  error code = " + 
                                                       C_ERROR_CODE_MAPPINGS[i][C_INDEX_ERROR_CODE]);
                }
                else
                {
                    trace(this, C_METHOD_mapException, "matching mapping found, key = " + 
                                                       C_ERROR_CODE_MAPPINGS[i][C_INDEX_MESSAGE_KEY] +
                                                       ",  but it is ignored " +
                                                       "(i.e. not reported as an error).");
                    l_ignoreError = true;
                }
                l_mappingDone = true;
            }
        }
        if (!l_mappingDone)
        {
            // None of the expected error codes matches the given message key.
            trace(this, "",
                "An unexpected PpasServiceException is detected: " + p_ppasServEx.getMsgKey());
            l_errorLine.append("(An unexpected PpasServiceException: " + p_ppasServEx.getMsgKey() + ")");
        }

        if (!l_ignoreError)
        {
            p_record.setErrorLine(l_errorLine.toString());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_mapException);
        }
    }


    /**
     * Prints trace print outs.
     * 
     * @param p_method         the name of th calling method.
     * @param p_callingObject  the calling <code>Object</code>.
     * @param p_message        the trace message to be printed.
     */
    private static void trace(Object p_callingObject, String p_method, String p_message)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_ALL,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            c_traceStatementNumber++,
                            p_callingObject,
                            "TRACE: " + p_method + " -- " + p_message);
        }
    }
}
