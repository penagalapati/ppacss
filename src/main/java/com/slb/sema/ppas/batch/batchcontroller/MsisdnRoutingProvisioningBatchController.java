////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       MsisdnRoutingProvisioningBatchController.java
//DATE            :       Oct 26, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       See Javadoc
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.MsisdnRoutingProvisioningBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.MsisdnRoutingProvisioningBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.MsisdnRoutingProvisioningBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/**
 * The entry point for the batch Msisdn Routing Provisioning.
 */
public class MsisdnRoutingProvisioningBatchController extends BatchController
{
    //--------------------------------------------------------------------------
    //--  Class level constant.                                               --
    //--------------------------------------------------------------------------

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME            = "MsisdnRoutingProvisioningBatchController";

    /** The additional properties layers to be loaded for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS   = "batch_mrp";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME      = "Invalid filename";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";

    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int C_INDEX_FILE_DATE          = 2; // 3rd element in the filename is the date

    /** Sequence number not used in filenames for this batch. Value is {@value}. */
    private static final int C_INDEX_NO_SEQUENCE_NUMBER = -1;


    //--------------------------------------------------------------------------
    //--  Instance variables.                                                 --
    //--------------------------------------------------------------------------
//    /** The js job id. */
//    private String i_jsJobId           = null;
//
//    /** The execution date and time.*/
//    private String i_executionDateTime = null;


    //--------------------------------------------------------------------------
    //--  Constructors.                                                       --
    //--------------------------------------------------------------------------

    /**
     * Constructs an <code>MsisdnRoutingProvisioningBatchController</code> instance using the given params.
     * 
     * @param p_context       A <code>PpasContext</code> object.
     * @param p_jobType       The type of job (e.g. BatchInstall).
     * @param p_jobId         The unique js id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params        Holding parameters to use when executing this job.
     *  
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     */
    public MsisdnRoutingProvisioningBatchController(PpasContext p_context,
                                                    String p_jobType,
                                                    String p_jobId,
                                                    String p_jobRunnerName,
                                                    Map p_params)
        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }    

//        i_jsJobId           = p_jobId;
        i_executionDateTime = DatePatch.getDateTimeNow();

        // Initialize this instance.
        this.initialize();

        // Make this batch neither pausible nor stoppable.
        super.setPausable(false);
        super.setStoppable(false);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }


    //--------------------------------------------------------------------------
    //--  Public instance methods.                                            --
    //--------------------------------------------------------------------------

    /** 
     * Returns a <code>BatchJobData</code> object with the correct database table key values set.
     * The key values are the js job id and execution date & time.
     * The caller can populate the returned <code>BatchJobData</code> object with data needed
     * for the operation in question.
     * 
     * @return  a <code>BatchJobData</code> object with the correct database table key values set.
     */
    public BatchJobData getKeyedControlRecord()
    {
        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(i_executionDateTime.toString());
        l_jobData.setJsJobId(getJobId());
        return l_jobData;
    }

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }


    //--------------------------------------------------------------------------
    //--  Protected instance methods.                                         --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates and returns a <code>MsisdnRoutingProvisioningBatchReader</code> instance.
     * 
     * @return  a <code>MsisdnRoutingProvisioningBatchReader</code> instance or 'null'.
     */
    protected BatchReader createReader()
    {
        MsisdnRoutingProvisioningBatchReader l_reader = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createReader);
        }
         
        if (super.i_startComponents)
        {
            l_reader = new MsisdnRoutingProvisioningBatchReader(super.i_ppasContext,
                                                                super.i_logger,
                                                                this,
                                                                super.i_inQueue,
                                                                super.i_params,
                                                                super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10110,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
         
        return l_reader;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /** 
     * Creates and returns a <code>MsisdnRoutingProvisioningBatchWriter</code> instance.
     * 
     * @return  a <code>MsisdnRoutingProvisioningBatchWriter</code> instance or 'null'.
     */
    protected BatchWriter createWriter()
    {
        MsisdnRoutingProvisioningBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10200,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        if (super.i_startComponents)
        {
            l_writer = new MsisdnRoutingProvisioningBatchWriter(super.i_ppasContext,
                                                                super.i_logger,
                                                                this,
                                                                super.i_params,
                                                                super.i_outQueue,
                                                                super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10210,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";        
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a MiscBatchProcessor.
     */
    protected BatchProcessor createProcessor()
    {
        MsisdnRoutingProvisioningBatchProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10700,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        if (super.i_startComponents)
        {
            l_processor = new MsisdnRoutingProvisioningBatchProcessor(super.i_ppasContext,
                                                                      super.i_logger,
                                                                      this,
                                                                      super.i_inQueue,
                                                                      super.i_outQueue,
                                                                      super.i_params,
                                                                      this.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10800,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }

        return l_processor;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
     * Adds a record into the batch control table, <code>BACO_BATCH_CONTROL</code>.
     * Checks if the given input filename is valid, and if it is found invalid the creation of the reader, 
     * writer and processor components is  prohibited.
     */   
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11010,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_addControlInfo);
        }
        
        BatchJobData l_jobData            = null;
        String       l_inFileName         = null;
        String       l_errorMsg           = C_INVALID_FILENAME;
        String       l_inputFileDirectory = null;
        
        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
        
        // If the given input file has the 'in progress file' extension (i.e. this is a recovery run) rename
        // it to an ordinary indata file, i.e. change the extension to the 'indata file' extension.
        // NOTE: This is a work-around to make recovery run work for this batch.
        //       When (if) the complete filename check is re-designed this might be handled in some other way.
        if ( l_inFileName != null && l_inFileName.endsWith(BatchConstants.C_EXTENSION_IN_PROGRESS_FILE))
        {
            l_inputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
            l_inFileName = renameFile(l_inFileName,
                                      l_inputFileDirectory,
                                      BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                      BatchConstants.C_EXTENSION_INDATA_FILE);
            
            // Put the new filename into the parameter map since this is passed as an argument when 
            // constructing the underlying components (reader, writer and processor).
            super.i_params.put(BatchConstants.C_KEY_INPUT_FILENAME, l_inFileName);
        }

        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING);
        l_jobData.setSubJobCnt("-1");
        l_jobData.setOpId(super.getSubmitterOpid());
        l_jobData.setFileSubSeqNo(null);
        l_jobData.setExtraData1(null);
        l_jobData.setExtraData2(null);
        l_jobData.setExtraData3(null);
        l_jobData.setExtraData4(null);
        if (super.isFileNameValid(l_inFileName,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_DAT,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_IPG,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_SCH,
                                  C_INDEX_FILE_DATE,
                                  C_INDEX_NO_SEQUENCE_NUMBER))
        {
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_jobData.setBatchDate(super.convertDateString(i_fileDate));
            l_jobData.setFileSeqNo(i_seqNo);
            l_jobData.setNoOfSuccessRec("0");
            l_jobData.setNoOfRejectedRec("0");
        }
        else
        {
            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus("F");
            if (l_inFileName != null )
            {
                if (l_errorMsg.length() + l_inFileName.length() + 2 <= 30)
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }

            l_jobData.setExtraData4(l_errorMsg);
            
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;
            
            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + 
                                                        C_INVALID_FILENAME + ": " + l_inFileName,
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }

        try
        {
            super.i_batchContService.addJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();
        }

        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_addControlInfo);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /** 
     * Updates the status of the job.
     * @param  p_status The status of the job
     */  
    protected void updateStatus(String p_status)
    {
        BatchJobData l_jobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11100,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        l_jobData = getKeyedControlRecord();
        l_jobData.setBatchJobStatus(p_status);
        
        try
        {
            super.i_batchContService.updateJobDetails( null, l_jobData, super.i_timeout );
        }
        catch (PpasServiceException p_ppasServiceEx)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10410,
                                this,
                                C_METHOD_updateStatus + " PpasServiceException from updateJobDetails");
            }

//            i_logger.logMessage(p_ppasServiceEx);
//            p_ppasServiceEx.printStackTrace();
        }                
      
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11200,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    } // End of updateStatus()
    
    //--------------------------------------------------------------------------
    //--  Private instance methods.                                           --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_initialize = "initialize";
    /**
     * Initialises this <code>MsisdnRoutingProvisioningBatchController</code> instance.
     * 
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    private void initialize() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            82601,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_initialize);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            82602,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_initialize);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameFile = "renameFile";
    /**
     * Renames the file with the given filename at the given path. That is, change the current file extension
     * to the specified new extension.
     * If the rename succeeds the new filename is returned, otherwise the given filename will be returned.
     * 
     * @param p_filename      the name of the file that will be renamed.
     * @param p_path          the full directory path in which the given file is stored.
     * @param p_extension     the current filename extension to be replaced, includiing the leading dot (".").
     * @param p_newExtension  the new filename extension includiing the leading dot (e.g. ".DAT").
     * 
     * @return  the new filename is returned if the rename succeeded, otherwise the given filename will be 
     *          returned.
     */
    protected String renameFile(String p_filename, String p_path, String p_extension, String p_newExtension)
    {
        String l_filename        = null;
        File   l_file            = null;
        String l_fullPathName    = null;
        String l_newFullPathName = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10700,
                            this,
                            C_METHOD_renameFile + " -- " + BatchConstants.C_ENTERING);
        }

        l_filename     = p_filename;
        l_fullPathName = p_path + File.separator + p_filename;
        l_file         = new File(l_fullPathName);

        if (l_file.exists())
        {
            l_newFullPathName = l_fullPathName.replaceFirst(p_extension, p_newExtension);
            
            if (l_file.renameTo(new File(l_newFullPathName)))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10710,
                                    this,
                                    C_METHOD_renameFile + " -- SUCCEEDED: " + l_newFullPathName);
                }
                l_filename = p_filename.replaceFirst(p_extension, p_newExtension);
            }      
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10720,
                                    this,
                                    C_METHOD_renameFile + " -- ***FAILED: " + l_newFullPathName);
                }                
            }
        }            

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10730,
                            this,
                            C_METHOD_renameFile + " -- " + BatchConstants.C_LEAVING + 
                            ", returning filename: " + l_filename);
        }
        return l_filename;
        
    } // End of renameFile(..)
}
