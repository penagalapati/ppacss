////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CCIChangeBatchCOntroller
//      DATE            :       30-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class is the entry point for Batch Community CHange and will be
//                              initiated by either the Job Scheduler or via its own bootstrap main.
//                              It is responsible to create and start all underlying components
//                              and will aslo be used for "call-back" to the initating client.
//                              It extends the generic batch controller class BatchCOntroller and
//                              contains no specific logic.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.CCIChangeBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.CCIChangeBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.CCIChangeBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/** This class is the entry point for Batch Community Change and will be initiated by either the Job Scheduler
 *  or via its own bootstrap main. It is responsible to create and start all underlying components and will
 *  also be used for "call-back" to the initating client. It extends the generic batch controller class
 *  BatchCOntroller and contains no specific logic.
 */
public class CCIChangeBatchController extends BatchController
{
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "CCIChangeBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_cci";
    
    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int C_INDEX_FILE_DATE        = 1; // 2nd element in the filename is the date

    /** Array index to fine the sequence number from the filename.  Value is {@value}. */
    private static final int C_INDEX_SEQUENCE_NUMBER  = 2; // 3rd element in the filename is the file seq no.

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME    = "Invalid filename";

    /** Log message - input file not defined. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";

    /** Session information. */
    private PpasSession i_ppasSession  = null;
    /** Request information. */
    private PpasRequest i_ppasRequest  = null;

    /**
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchInstall).
     * @param p_jobId       The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     * @throws PpasServiceException - If feature is not licensed.
     */
    public CCIChangeBatchController(
        PpasContext p_context,
        String p_jobType,
        String p_jobId,
        String p_jobRunnerName,
        Map p_params)
        throws PpasConfigException, PpasServiceException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }    

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();
        
        // The i_ppasRequest used in addControlInformation()
        i_ppasSession          = new PpasSession();
        i_ppasSession.setContext(p_context);
        i_ppasRequest          = new PpasRequest(i_ppasSession);

        p_context.validateFeatureLicence(FeatureLicence.C_FEATURE_CODE_COMMUNITY_CHARGING);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
    } // End of constructor CCIChangeBatchController


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                82601,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                82602,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }
      
        return;
      
    } // End of init()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates a <code>SubInstBatchReader</code>.
     * @return The created SubInstBatchReader.
     */
    public BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createReader);
        }
        
        CCIChangeBatchReader l_reader =
            new CCIChangeBatchReader(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_inQueue,
                super.i_params,
                super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10110,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
        
        return l_reader;
        
    } // End of createReader()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /** 
     * Creates a <code>SubInstBatchWriter</code>.
     * @return The created SubInstBatchWriter.
     * @throws IOException when encountering problems opening output file.
     */
    public BatchWriter createWriter() throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10200,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }
        CCIChangeBatchWriter l_writer =
            new CCIChangeBatchWriter(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_params,
                super.i_outQueue,
                super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10210,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        
        return l_writer;
        
    } // End of createWriter()
    


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";
    /** 
      * Creates a <code>SubInstBatchProcessor</code>.
      * @return The created SubInstBatchProcessor. 
      */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        CCIChangeBatchProcessor p_processor =
            new CCIChangeBatchProcessor(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_inQueue,
                super.i_outQueue,
                super.i_params,
                this.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }
        return p_processor;
    }



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
     * This method is responsible to insert a record into table BACO using the defined
     * service method PpasBatchCOntrolService.addControlInformation(). The method will
     * get a BatchJobData object and populate it with required parameters.
     * Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10380,
                this,
                BatchConstants.C_ENTERING + C_METHOD_addControlInfo);
        }
        
        BatchJobData l_jobData    = null;
        String       l_inFileName = null; 
        String       l_errorMsg   = C_INVALID_FILENAME;
        
        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);

        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_CCI);
        l_jobData.setOpId(super.getSubmitterOpid());
        l_jobData.setSubJobCnt("-1");

        if (isFileNameValid(l_inFileName,
                            BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_DAT,
                            BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_IPG,
                            BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_SCH,
                            C_INDEX_FILE_DATE, 
                            C_INDEX_SEQUENCE_NUMBER) )
        {
            
            l_jobData.setBatchDate(i_fileDate);
            l_jobData.setFileSeqNo(i_seqNo);
            l_jobData.setFileSubSeqNo(null);
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_jobData.setNoOfSuccessRec("0");
            l_jobData.setNoOfRejectedRec("0");
            l_jobData.setFileSubSeqNo(null);
            l_jobData.setExtraData1(null);
            l_jobData.setExtraData2(null);
            l_jobData.setExtraData3(null);
            l_jobData.setExtraData4(null);
        }
        else
        {
            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_FAILED);
            
            if (l_inFileName != null )
            {
                if ( l_errorMsg.length() + l_inFileName.length() + 2 < 30 )
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }

            l_jobData.setExtraData4(l_errorMsg);
            
            // Do not start any reader, writer or processor component
            super.i_startComponents = false;
            
            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + 
                                                        C_INVALID_FILENAME + ": " + l_inFileName,
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }
        
        
        try
        {
            super.i_batchContService.addJobDetails(i_ppasRequest, l_jobData, i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();
        }
        
        if ( !super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_LEAVING + C_METHOD_addControlInfo);
        }
    } // End of addControlInformation()





    /** 
     * This method will return a BatchJobData-object with the correct key-values set 
     * (JsJobId and executionDateTime). The caller can then populate the record with data needed 
     * for the operation in question. For example, the writer will get such record and
     * add number of successfully/faulty records processed and then update.
     * @return BatchJobData the details of the batch job
     */
    public BatchJobData getKeyedControlRecord()
    {
        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(this.i_executionDateTime.toString());
        l_jobData.setJsJobId(super.getJobId());
       
        return l_jobData;
        
    } // End of getKeyedInformation()
     
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }     

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";
    
    /**
     * It updates the databse with a new status when a batch have got the status finished (c), 
     * stopped (x) or error (f). The metohd is called from the super class. 
     * The <code>BatchJobData</code>object used in the method are  
     * first fetched form getKeyedControlRecord and then the status is inserted into that object.
     * @param p_status the status to set
     */     
    protected void updateStatus(String p_status)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10380,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        try
        {
            BatchJobData l_jobData = getKeyedControlRecord();
            l_jobData.setBatchJobStatus(p_status);
            super.i_batchContService.updateJobDetails(this.i_ppasRequest, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10381,
                    this,
                    C_METHOD_updateStatus + " PpasServiceException " + e.getMessage());
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10382,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        
        return;
        
    } // End of updateStatus(.)
    
     
     
} // End of class CCIChangeBatchController