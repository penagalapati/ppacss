////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME   :    RefillBatchReader.java
//      DATE        :    12-Jul-2006
//      AUTHOR      :    Yang Liu
//      REFERENCE   :    PRD_ASCS_GEN_CA_093
//
//      COPYRIGHT   :    WM-data 2007
//
//      DESCRIPTION :    Reader class for batch refill input files
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
//04/08/06  | M Erskine     | Format file correctly. Debug  | PpacLon#2479/9483
//          |               | contents and add header record|
//          |               | validation.                   |
//----------+---------------+-------------------------------+--------------------
//18/08/06  | M Erskine     | Get default currency from     | PpacLon#2479/9757
//          |               | SYFG_SYSTEM_CONFIG cache.     |
//----------+---------------+-------------------------------+--------------------
//25/08/06  | Yang Liu      | Recovery mode fixed.          | PpacLon#2587/9782
//----------+---------------+-------------------------------+--------------------
//11/09/06  | Yang Liu      | RPT info correction under     |
//          |               | recovery mode                 | PpacLon#2587/9936
//----------+---------------+-------------------------------+--------------------
//12/09/06  | Yang Liu      | default currency extracted    |
//          |               | correctly.                    | PpacLon#2620/9952
//----------+---------------+-------------------------------+--------------------
//14/12/06  | Yang Liu      | Validation of 'RefillMethod'  | PpacLon#2817/10660
//          |               | amended, to accepts Refill    |
//          |               | type for all the Markets.     |
//----------+---------------+-------------------------------+--------------------
//14/12/06  | Yang Liu      | Validation of 'Currency' check| PpacLon#2811/10667
//          |               | CUFM rather than EMCR table.  |
//----------+---------------+-------------------------------+--------------------
//20/04/07  | Lars Lundberg | Method 'validateRecordLine()':| PpacLon#3049/11342
//          |               | Don't continue validating the |
//          |               | record is the MSISDN is not   |
//          |               | valid (i.e. subscriber is not |
//          |               | installed).                   |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchreader;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchFileErrorCodeException;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.dataclass.CufmCurrencyFormatsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.MiscCodeData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PaprPaymentProfileData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.RefillBatchRecordData;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to verify and extract data from the SDP balancing file.
 */

public class RefillBatchReader extends BatchLineFileReader
{
    //-------------------------------------------------------------------------
    // Variables
    //-------------------------------------------------------------------------
    
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String   C_CLASS_NAME                                = "RefillBatchReader";
    
    /** Error code. Invalid record format. Value is {@value}. */
    public static final String    C_ERROR_CODE_INVALID_RECORD_FORMAT          = "01";
    
    /** Error code. Invalid header length. Value is {@value}. */
    public static final String    C_ERROR_CODE_INVALID_HEADER_LENGTH          = "02";
    
    /** Error code. Total number of refill non-numeric. Value is {@value}. */
    public static final String    C_ERROR_CODE_TOTAL_REFILLS_NON_NUMERIC      = "03";
    
    /** Error code. Total number of refill transactions does not match total number in the header. Value is {@value}. */
    public static final String    C_ERROR_CODE_HEADER_TOTAL_ROWS_MISMATCH     = "04";
    
    /** Error code. Total amount of refill transactions does not match total amount in the header. Value is {@value}. */
    public static final String    C_ERROR_CODE_HEADER_REFILL_AMOUNT_MISMATCH  = "05";
    
    /** Error code. Invalid currency code. Value is {@value}. */
    public static final String    C_ERROR_CODE_INVALID_CURRENCY_CODE          = "06";
    
    /** Error code. Invalid refill profile. Value is {@value}. */
    public static final String    C_ERROR_CODE_INVALID_REFILL_PROFILE         = "07";
    
    /** Error code. Msisdn not install. Value is {@value}. */
    private static final String   C_ERROR_CODE_MSISDN_NOT_INSTALLED           = "09";
    
    /** Error code. Non-numeric Msisdn. Value is {@value}. */
    public static final String    C_ERROR_CODE_NON_NUMERIC_MSISDN             = "10";
    
    /** Error code. refill amount is non numeric. Value is {@value}. */
    public static final String    C_ERROR_CODE_REFILL_AMOUNT_NON_NUMERIC      = "11";
    
    /** Error code. refill amount is non numeric. Value is {@value}. */
    public static final String    C_ERROR_CODE_REFILL_AMOUNT_ZERO             = "12";
    
    /** Error code. Wrong number of fields in input record. Value is {@value}. */
    public static final String    C_ERROR_WRONG_NUMBER_OF_FIELDS              = "13";
    
    /** Error code. Header missing or not the first record. Value is {@value}. */
    public static final String    C_ERROR_HEADER_MISSING_OR_NOT_FIRST         = "14";
    
    /** Error code. Invalid refill method. Value is {@value}. */
    public static final String    C_ERROR_CODE_INVALID_REFILL_METHOD          = "15";
    
    /** Error code. Total value of refills non-numeric. Value is {@value}. */
    public static final String    C_ERROR_CODE_TOTAL_REFILL_VALUE_NON_NUMERIC = "22";
    
    // Header length.
    
    /** Maximum length of header record. Value is {@value}. */
    private static final int      C_MAX_HEADER_RECORD_LENGTH                  = 24;
    
    /** Number of transactions length in the header record. Value is {@value}. */
    private static final int      C_HEADER_NUMER_TRANSACTION_LENGTH           = 8;
    
    /** Value of transaction length in the header record. Value is {@value}. */
    private static final int      C_HEADER_VALUE_TRANSACTION_LENGTH           = 13;
        
    // Detail record length.
    
    /** Maximum length of detaild record. Value is {@value}. */
    private static final int      C_MAX_DETAILED_RECORD_LENGTH   = 57;
    
    /** Msisdn length of detaild record. Value is {@value}. */
    private static final int      C_MSISDN_LENGTH                = 15;
    
    /** Refill Amount length of detaild record. Value is {@value}. */
    private static final int      C_REFILL_AMOUNT_LENGTH         = 12;
    
    /** Refill Profile length of detaild record. Value is {@value}. */
    private static final int      C_REFILL_PROFILE_LENGTH        = 4;
    
    /** Transaction Id length of detaild record. Value is {@value}. */
    private static final int      C_TRANSACTION_ID_LENGTH        = 20;
    
    /** Refill Method length of detaild record. Value is {@value}. */
    private static final int      C_REFILL_METHOD_LENGTH         = 6;
        
    
    // Index in the batch record array
    
    /** Index for the fields in the batchDataRecord array. Value is {@value}. */
    private static final int      C_NUMBER_OF_RECORD_FIELDS      = 5;
    
    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int      C_MSISDN                       = 0;
    
    /** Index for the field payment amount in the batchDataRecord array. Value is {@value} */
    private static final int      C_REFILL_AMOUNT                = 1;
    
    /** Index for the field payment profile in the batchDataRecord array. Value is {@value} */
    private static final int      C_REFILL_PROFILE               = 2;
    
    /** Index for the field transaction Id in the batchDataRecord array. Value is {@value} */
    private static final int      C_TRANS_ID                     = 3;
    
    /** Index for the field payment method in the batchDataRecord array. Value is {@value} */
    private static final int      C_REFILL_METHOD                = 4;
    
    //Header indexes in the batch record array
    
    /**
     * The number of fields that can be held in a <code>RefillBatchRecordData</code>. There are 5 fields in 
     * a detail record and 3 in a header record. Value is {@value}. */
    private static final int      C_NB_OF_FIELDS                 = 8;
        
    /** Index for the field number of transactions in the batchDataRecord array. Value is {@value}. */
    private static final int      C_HEADER_NB_OF_TRANS           = 5;
    
    /** Index for the field number of transactions in the batchDataRecord array. Value is {@value}. */
    private static final int      C_HEADER_TOTAL_REFILL_VALUE    = 6;
    
    /** Index for the field currency in the batchDataRecord array. Value is {@value} */
    private static final int      C_HEADER_CURRENCY              = 7;    
        
    /** Specific Refill header info. Value is {@value}. */
    public static final String    C_REFILL_RECOVERY_INFO         = "REFILL_BATCH_HEADER";
    
    /** Currency. */
    private String                i_currency                     = null;
    
    
    /** Payment Profile. */
    private String                i_refillProfile                = null;
    
    /** Payment Method. */
    private String                i_refillMethod                 = null;
        
    /** The BatchDataRecord to be sent into the Queue. */
    private RefillBatchRecordData i_refillBatchRecordData        = null;
    
    /** Used to check if the first record is a header record. */
    private boolean               i_firstRecord                  = true;
        
    /** Number of header and detail records. */
    private int                   i_nbOfLine                     = 0;
    
    /** To check if the error record is in the first line and if so is it a result code set to success. */
    private boolean               i_erroneousHeaderRecord        = false;
    
    /** Number of detaild records. */
    private int                   i_nbRefillRecordsInFile        = 0;
    
    /** Total refill amoount in the detaild records. */
    private long                  i_totalRefillAmountInFile      = 0;
    
    /**
     * Constructor for <code>RefillBatchReader</code>.
     * @param p_ppasContext Reference to a <code>PpasContext</code>.
     * @param p_logger      Reference to a <code>Logger</code> object.
     * @param p_controller  Reference to the <code>BatchController</code> for this type of batch job.
     * @param p_queue       The queue on which to place a records for processing by the 
     *                      </code>RefillBatchController</code>.
     * @param p_parameters  Parameters specific to this batch job.
     * @param p_properties  Reference to a <code>PpasProperties</code> object.
     */
    public RefillBatchReader(PpasContext p_ppasContext,
                             Logger p_logger,
                             BatchController p_controller,
                             SizedQueue p_queue,
                             Map p_parameters,
                             PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTING);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            BatchConstants.C_CONSTRUCTED);
        }
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";
    
    /**
     * Check if filename is in a valid format.
     * @param p_filename The filename submitted for validation.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    public boolean isFileNameValid(String p_filename)
    {
        boolean l_validFileName = true;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10030,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_isFileNameValid);
        }
        
        if (p_filename != null)
        {
            if ((p_filename.matches(BatchConstants.C_PATTERN_BATCH_REFILL_FILENAME_DAT))
                    || (p_filename.matches(BatchConstants.C_PATTERN_BATCH_REFILL_FILENAME_SCH))
                    || (p_filename.matches(BatchConstants.C_PATTERN_BATCH_REFILL_FILENAME_IPG)))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_CONFIN_START,
                                    C_CLASS_NAME,
                                    10032,
                                    this,
                                    "FileName (" + p_filename + ") is valid");
                }
            }
            else
            {
                l_validFileName = false;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_CONFIN_START,
                                    C_CLASS_NAME,
                                    10034,
                                    this,
                                    "FileName (" + p_filename + ") is invalid");
                }
            }
            
            // Check if recovery mode
            if (l_validFileName && !this.i_recoveryMode)
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                if (p_filename.matches(BatchConstants.C_PATTERN_BATCH_REFILL_FILENAME_SCH))
                {
                    renameInputFile(BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }
                else
                {
                    renameInputFile(BatchConstants.C_PATTERN_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_INDATA_FILE,
                                    BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10036,
                                    this,
                                    "After rename, inputFullPathName=" + super.i_fullPathName);
                }
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_isFileNameValid);
        }

        return l_validFileName;
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";
    
    /**
     * This method shall contain all logic necessary to create and populate the BatchDataRecord record.
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        String l_nextLine = null; // Current input line
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10060,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getRecord);
        }
        
        // Get next line from input file.
        i_refillBatchRecordData = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10062,
                            this,
                            "l_nextLine [" + l_nextLine + "]");
        }
        
        //Don't read any more records if the header record is erroneous.
        if ( i_erroneousHeaderRecord)
        {
            super.i_endOfFileReached = true;
            return null;
        }
        
        if (super.readNextLine() != null)
        {
            i_refillBatchRecordData = new RefillBatchRecordData();
            
            // Store input filename
            i_refillBatchRecordData.setInputFilename(super.i_inputFileName);
            
            // Store line-number form the file
            i_refillBatchRecordData.setRowNumber(super.i_lineNumber);
            
            // Store original data record
            i_refillBatchRecordData.setInputLine(super.i_inputLine);
            
            // Extract data from line
            if (extractLineAndValidate())
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10064,
                                    this,
                    "extractLineAndValidate() was successful");
                }
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10066,
                                    this,
                    "extractLineAndValidate() failed!");
                }
                
                // Flag for corrupt line and set error code
                i_refillBatchRecordData.setCorruptLine(true);
                
                if (i_refillBatchRecordData.getErrorLine() == null)
                {
                    // Other error than specific field errors
                    i_refillBatchRecordData.setErrorLine(
                                                         C_ERROR_CODE_INVALID_RECORD_FORMAT + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                         + i_refillBatchRecordData.getInputLine());
                }
            }
            
            i_refillBatchRecordData.setNumberOfSuccessfullyProcessedRecords(i_successfullyProcessed);
            i_refillBatchRecordData.setNumberOfErrorRecords(super.i_notProcessed);
            
            if ( super.faultyRecord(i_lineNumber))
            {
                i_refillBatchRecordData.setFailureStatus(true);
            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10068,
                                this,
                                "After setNumberOfSuccessfully...." + i_successfullyProcessed);
            }
        }
        else
        {
            // End of file is reached.                
            super.i_endOfFileReached = true; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10080,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }
        
        return i_refillBatchRecordData;
        
    } // end of method getRecord()
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";
    
    /**
     * This method extracts information from the batch data record.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        String[] l_recordFields = new String[C_NB_OF_FIELDS];
        boolean  l_validData    = true;

        initializeRecordValidation( i_inputLine, l_recordFields, i_refillBatchRecordData);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10090,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate);
        }

        i_nbOfLine++;  // Keep a count of the number of input lines read.
        
        if (i_firstRecord) 
        {      
            i_firstRecord = false;
            i_refillBatchRecordData.setIsRecord(false);
            
            if (this.i_recoveryMode)
            {
                return validateRecoveryHeader();
            }
            
            return validateHeader();
        }        
                       
        i_refillBatchRecordData.setIsRecord(true);
        l_validData = validateRecordLine(); 
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate);
        }

        return l_validData;
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateHeader = "validateHeader";

    /**
     * Validates the header of the batch refill data file. Note that the header is also validated against 
     * certain fields in the detail records, so header validation involves reading all the rows in the input 
     * file. The format is: [NumberOfRefills][TotalRefillAmount][Currency].
     * @return false when extracted fields are invalid and true otherwise.
     */
    private boolean validateHeader()
    {
        boolean                    l_validData             = true; //Assume header record is valid.
       
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10110,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateHeader);
        }
        
        // Check the length of the header line.
        if (super.i_inputLine.length() > C_MAX_HEADER_RECORD_LENGTH)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10114,
                                this,
                                C_METHOD_validateHeader + " -- ***ERROR: Header line is not correct: "
                                + super.i_inputLine.length() + ",  it must not exceed "
                                + C_MAX_HEADER_RECORD_LENGTH + " characters.");
            }
            
            i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_HEADER_LENGTH
                                                 + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                 + super.i_inputLine);
            i_erroneousHeaderRecord = true;
            l_validData = false;
        }
        else
        {  
            // Get the header fields
            //            Start                              End                                #Field               Format check                            Errorcode
            if ( getField(0,                                 C_HEADER_NUMER_TRANSACTION_LENGTH, C_HEADER_NB_OF_TRANS, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_CODE_TOTAL_REFILLS_NON_NUMERIC )   &&
                    getField(C_HEADER_NUMER_TRANSACTION_LENGTH, C_HEADER_VALUE_TRANSACTION_LENGTH, C_HEADER_TOTAL_REFILL_VALUE,BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_CODE_TOTAL_REFILL_VALUE_NON_NUMERIC )   &&
                    validateCurrency(super.i_inputLine.substring(C_HEADER_NUMER_TRANSACTION_LENGTH + C_HEADER_VALUE_TRANSACTION_LENGTH, C_MAX_HEADER_RECORD_LENGTH)))
            {
                // everything is fine                
            }
            else
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                                    PpasDebug.C_LVL_MODERATE,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10365,
                                    this,
                                    "Row: " + i_lineNumber + " ]"+ i_inputLine + "[ is a INVALID record" +
                                    " l_lineNumber            =" + i_lineNumber + "\n" +
                                    " l_numberOfTransactions  =" + i_recordFields[C_HEADER_NB_OF_TRANS] + "\n" +
                                    " l_totallAmount          =" + i_recordFields[C_HEADER_TOTAL_REFILL_VALUE] + "\n" +
                                    " l_currency              =" + i_recordFields[C_HEADER_CURRENCY] + "\n" );
                }
                
                // Flag for corrupt line
                i_erroneousHeaderRecord = true;
                l_validData = false;
            }
        }
        
        if (l_validData)
        {
            i_refillBatchRecordData.setRecoveryLine("1," + C_REFILL_RECOVERY_INFO);
            l_validData = validateHeaderRefillAmountAndNbOfRows();
        }        
        
                
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateHeader);
        }

        
        return l_validData;
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateRecoveryHeader = "validateRecoveryHeader";
    
    /**
     * Validates the header of the batch refill data file.
     * The format is: [NumberOfReills][TotalRefillAmount][Currency].
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    
    private boolean validateRecoveryHeader()
    {
        boolean                    l_validData             = true; //Assume header record is valid.
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10110,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateRecoveryHeader);
        }        
        
        if (validateCurrency(super.i_inputLine.substring(C_HEADER_NUMER_TRANSACTION_LENGTH + C_HEADER_VALUE_TRANSACTION_LENGTH, C_MAX_HEADER_RECORD_LENGTH)))
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10110,
                                this,
                                "into RecoveryHeader, after getCurrency" + i_currency);
            }              
        }        
        
        else
        {
            i_erroneousHeaderRecord = true;
            l_validData = false;
        }
        
        if(l_validData)
        {            
           i_refillBatchRecordData.setRecoveryLine("1," + C_REFILL_RECOVERY_INFO);
        }            
                
        if ( PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateRecoveryHeader );
        }
        
        return l_validData;
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateRecordLine = "validateRecordLine";
    
    /**
     * Validates one record of the batch data record. 
     * The format is: [MSISDN][ReillAmount][RefillProfile][TransactionId][RefillMethod].
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    
    private boolean validateRecordLine()
    {
        boolean                  l_validData                 = true; // return code. Assume it always true.
        MsisdnFormat             l_msisdnFormat              = null;
        BusinessConfigCache      l_cache                     = null;
        PaprPaymentProfileData   l_paprPmtData               = null;
        MiscCodeData             l_miscCodeData              = null;     
        BasicAccountData         l_basicAccountData          = null;
        Msisdn                   l_msisdn                    = null;
        
        // Help array to keep the input line elements
        String[]     l_recordFields = new String[C_NUMBER_OF_RECORD_FIELDS];
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10140,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateRecordLine);
        }
                
        if (i_inputLine.length() > C_MAX_DETAILED_RECORD_LENGTH ) //TODO: || l_recordFields.length != C_NUMBER_OF_RECORD_FIELDS)
        {
            i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_RECORD_FORMAT +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                 super.i_inputLine);
            l_validData = false;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10142,
                                this,
                                C_METHOD_validateRecordLine + " -- ***ERROR: Record line is not correct: "
                                + super.i_inputLine.length() + ",  it must not exceed "
                                + C_MAX_DETAILED_RECORD_LENGTH + " characters." );
            }
            
        }
        else 
        {
            initializeRecordValidation( i_inputLine, l_recordFields, i_refillBatchRecordData );
            
            //          Start                              End             #Field               Format check                            Errorcode
            if ( getField(0,                       C_MSISDN_LENGTH,         C_MSISDN,         BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC,       C_ERROR_CODE_NON_NUMERIC_MSISDN ) &&
                 getField(C_MSISDN_LENGTH,         C_REFILL_AMOUNT_LENGTH,  C_REFILL_AMOUNT,  BatchConstants.C_PATTERN_RIGHT_ADJUSTED_ZERO_FILLED_NUMERIC,  C_ERROR_CODE_REFILL_AMOUNT_NON_NUMERIC ) &&
                 getField(C_REFILL_AMOUNT_LENGTH,  C_REFILL_PROFILE_LENGTH, C_REFILL_PROFILE, BatchConstants.C_PATTERN_ALPHA_NUMERIC, C_ERROR_CODE_INVALID_RECORD_FORMAT)&&
                 getField(C_REFILL_PROFILE_LENGTH, C_TRANSACTION_ID_LENGTH, C_TRANS_ID,       BatchConstants.C_PATTERN_ALPHA_NUMERIC,       C_ERROR_CODE_INVALID_RECORD_FORMAT ) &&
                 getField(C_TRANSACTION_ID_LENGTH, C_REFILL_METHOD_LENGTH,  C_REFILL_METHOD,  BatchConstants.C_PATTERN_ALPHA_NUMERIC, C_ERROR_CODE_INVALID_RECORD_FORMAT))                
            {   
                if( new Long(l_recordFields[C_REFILL_AMOUNT]).longValue() == 0)
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10120,
                                        this,
                                        C_METHOD_validateHeader +
                                        " -- ***ERROR: The refill amount is zero.");
                    }
                    
                    i_refillBatchRecordData.setErrorLine(
                        C_ERROR_CODE_REFILL_AMOUNT_ZERO + BatchConstants.C_DELIMITER_REPORT_FIELDS +
                        super.i_inputLine);
                    
                    l_validData = false;                   
                    
                }
                
                // Ok, let's check each record field individually.          
                l_cache = (BusinessConfigCache)super.i_context.getAttribute("BusinessConfigCache");
                l_msisdnFormat = super.i_context.getMsisdnFormatInput();
                
                try
                {
                    l_msisdn = l_msisdnFormat.parse(l_recordFields[C_MSISDN]);
                    l_basicAccountData = super.getBasicAccountDataFromMsisdn(l_msisdn);
                }
                catch (ParseException e)
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_START,
                                        C_CLASS_NAME,
                                        10148,
                                        this,
                                        "Record: the msisdn is invalid:" + "MSISDN:" + l_recordFields[C_MSISDN]+ "\n");
                    }
                    
                    l_validData = false;
                }
                
                catch (BatchFileErrorCodeException l_batchFileErrorCodeException)
                {
                    i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_MSISDN_NOT_INSTALLED
                                                         + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                         + super.i_inputLine);
                    l_validData = false;
                }                      
                
                if (l_validData)
                {
                    l_paprPmtData = l_cache.getPaprPaymentProfileCache().getAll()
                                                .getPaymentProfileData(l_recordFields[C_REFILL_PROFILE]);
                        
                    l_miscCodeData =
                        l_cache.getMiscCodeCache().getAvailablePaymentTypes(
                                 l_basicAccountData.getMarket()).getPaymentTypeData(
                                              l_basicAccountData.getMarket(),l_recordFields[C_REFILL_METHOD]);  
                    
                    
                    // Check refill profile.
                    if (l_paprPmtData == null)
                    {
                        //  The refill profile is invalid.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10120,
                                            this,
                                            C_METHOD_validateHeader +
                                            " -- ***ERROR: paprPmtData, The refill profile in the record is invalid: refill Profile  = '"
                                            + l_recordFields[C_REFILL_PROFILE] + "'.");
                        }
                        
                        i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_REFILL_PROFILE
                                                             + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                             + super.i_inputLine);
                        
                        l_validData = false;                    
                    }   
                    else 
                    { 
                        i_refillProfile = l_paprPmtData.getPaymentProfile();
                        
                        if (i_refillProfile == null )
                        {
                            //  The refill profile is invalid.
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                PpasDebug.C_APP_SERVICE,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME,
                                                10120,
                                                this,
                                                C_METHOD_validateHeader +
                                                " -- ***ERROR: refillProfile, The refill profile in the record is invalid: refill Profile  = '"
                                                + l_recordFields[C_REFILL_PROFILE] + "'.");
                            }
                            
                            i_refillBatchRecordData.setErrorLine(
                                C_ERROR_CODE_INVALID_REFILL_PROFILE +
                                BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
    
                            l_validData = false;
                        }
    
                        // Check refill method.
                        if (l_miscCodeData == null )
                        {
                            // The refill method is invalid.
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                PpasDebug.C_APP_SERVICE,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME,
                                                10120,
                                                this,
                                                C_METHOD_validateHeader +
                                                " -- ***ERROR: The refill Method in the record is invalid: refill Method  = '"
                                                + l_recordFields[C_REFILL_METHOD] + "'.");
                            }
                            
                            i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_REFILL_METHOD
                                                                 + BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
                            l_validData = false;
                        }                           
                        else 
                        { 
                            i_refillMethod = l_miscCodeData.getCode();
                            
                            if (i_refillMethod == null )
                            {
                                //  The refill method is invalid.
                                if (PpasDebug.on)
                                {
                                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                    PpasDebug.C_APP_SERVICE,
                                                    PpasDebug.C_ST_TRACE,
                                                    C_CLASS_NAME,
                                                    10120,
                                                    this,
                                                    C_METHOD_validateHeader +
                                                    " -- ***ERROR: The refill profile in the record is invalid: refill method  = '"
                                                   + l_recordFields[C_REFILL_METHOD] + "'.");
                                }
                                
                                i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_REFILL_METHOD
                                                                     + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                                     + super.i_inputLine);
                                l_validData = false;
                            }
                        }
                    }
                }
            }
            else
            {
                // Illegal field format detected

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                                    PpasDebug.C_LVL_MODERATE,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10365,
                                    this,
                                    "Row: " + i_lineNumber + " ]"+ i_inputLine + "[ is a INVALID record" +
                                    " l_lineNumber     =" + i_lineNumber + "\n" +
                                    " l_msisdn         =" + l_recordFields[C_MSISDN] + "\n" +
                                    " l_refillAmount   =" + l_recordFields[C_REFILL_AMOUNT] + "\n" +
                                    " l_refillProfile  =" + l_recordFields[C_REFILL_PROFILE] + "\n" +
                                    " l_refillMethod   =" + l_recordFields[C_REFILL_METHOD] + "\n" +
                                    " l_transId        =" + l_recordFields[C_TRANS_ID] + "\n" );
                }
                
                // Flag for corrupt line
                l_validData = false;
            }      
            
            if (l_validData && l_recordFields[C_MSISDN] != null && l_recordFields[C_REFILL_AMOUNT] != null 
                    && l_recordFields[C_REFILL_PROFILE] != null && l_recordFields[C_REFILL_METHOD] !=null)
            {                               
                    i_refillBatchRecordData.setMsisdn(l_msisdn);                    
                    i_refillBatchRecordData.setRefillAmount(
                        new Long(l_recordFields[C_REFILL_AMOUNT]).longValue());      
                    i_refillBatchRecordData.setRefillProfile(i_refillProfile);
                    i_refillBatchRecordData.setRefillMethod(i_refillMethod);
                    if (l_recordFields[C_TRANS_ID] != null)
                    {    
                        i_refillBatchRecordData.setTransactionId(l_recordFields[C_TRANS_ID]);
                    }
                    i_refillBatchRecordData.setCurrency(i_currency);
               
            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10150,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_validateRecordLine);
            }                 
        }
        
        return l_validData;
    }    
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateHeaderRefillAmountAndNbOfRows =
                                                                    "validateHeaderRefillAmountAndNumOfRows";
    /**
     * Method to perform :
     *  The value of the first field in the header should be equal to the number of records in the file.
     *  The value of the second field in the header should be equal to the sum of the refill amounts contained in each of the records.
     * @return <code>true</code> when record fields are valid else <code>false</code>.
     */
    private boolean validateHeaderRefillAmountAndNbOfRows()
    {
        boolean      l_valid = true;
        String       l_headerLine;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10140,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateHeaderRefillAmountAndNbOfRows);
        }

        l_headerLine = super.i_inputLine;

        try
        {
            i_inputFileHandle.mark(i_fileLength);
            
        }
        catch (IOException l_e)
        {
            i_logger.logMessage( new LoggableEvent( "Could not mark beginning of file" + l_e.getMessage(),
                                                    LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10120,
                                this,
                                C_METHOD_validateHeaderRefillAmountAndNbOfRows +
                                " ERROR: Could not mark beginning of file");
            }
        }
        
        while ((super.readNextLine() != null) && l_valid)
        {
            i_startIndex = 0;
            i_endIndex   = 0;
            i_refillBatchRecordData.setInputLine(super.i_inputLine);
            i_refillBatchRecordData.setErrorLine(null);
            i_batchRecord = super.i_inputLine;
            
            // Extract the expected refill amount from the header.
            if (!getField(C_MSISDN_LENGTH,
                          C_MSISDN_LENGTH + C_REFILL_AMOUNT_LENGTH,  C_REFILL_AMOUNT,  
                          BatchConstants.C_PATTERN_NUMERIC,
                          C_ERROR_CODE_REFILL_AMOUNT_NON_NUMERIC))
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                                PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10365,
                                this,
                                "Row: " + i_lineNumber + " ]"+ i_inputLine + "[ is a INVALID record" +
                                " l_lineNumber     =" + i_lineNumber + "\n" +
                                " l_msisdn         =" + i_recordFields[C_MSISDN] + "\n" +
                                " l_refillAmount   =" + i_recordFields[C_REFILL_AMOUNT] + "\n" +
                                " l_refillProfile  =" + i_recordFields[C_REFILL_PROFILE] + "\n" +
                                " l_refillMethod   =" + i_recordFields[C_REFILL_METHOD] + "\n" +
                                " l_transId        =" + i_recordFields[C_TRANS_ID] + "\n" );
                }
            
                // Flag for corrupt line
                i_refillBatchRecordData.setInputLine(l_headerLine);
                i_erroneousHeaderRecord = true;
                l_valid = false;
            }    
            
            if (l_valid)
            { 
                // Add it to the total
                i_totalRefillAmountInFile += new Long(i_recordFields[C_REFILL_AMOUNT]).longValue();
                i_nbRefillRecordsInFile++;
            }
        }
        
        if (l_valid)
        {  
            // Check that the total refill amount matches that in the header.
            if (i_totalRefillAmountInFile == Long.parseLong(i_recordFields[C_HEADER_TOTAL_REFILL_VALUE]))
            {                
                l_valid = true;    
            }
            else
            {               
                i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_HEADER_REFILL_AMOUNT_MISMATCH +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                 l_headerLine);
                i_erroneousHeaderRecord = true;
                l_valid = false;
            }
        }
        
        if (l_valid)
        {
            // Check that the number of records in the file matches the number specified in the header.
            try
            {
                if (i_nbRefillRecordsInFile != Integer.parseInt(i_recordFields[C_HEADER_NB_OF_TRANS]))
                {
                    i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_HEADER_TOTAL_ROWS_MISMATCH +
                                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                         l_headerLine);
                    i_erroneousHeaderRecord = true;
                    l_valid = false;
                }
            }
            //TODO: Scenario already covered by getFields method above??
            catch (NumberFormatException l_nFE)
            {
                i_logger.logMessage( new LoggableEvent("Could not parse number of refill transactions field" +
                                                       "in header.",
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10120,
                                    this,
                                    C_METHOD_validateHeaderRefillAmountAndNbOfRows +
                                    " ERROR: Could not parse number of refill transactions field in header");
                }
                
                i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_TOTAL_REFILL_VALUE_NON_NUMERIC +
                                                     BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                     l_headerLine);
                i_erroneousHeaderRecord = true;
                l_valid = false;
            }
        }
        
        if (l_valid)
        {            
            // Reset the file pointer and recovery info. Note that the file will subsequently be read from
            // the first input record onwards
            super.i_inputLine   = l_headerLine;
            super.i_batchRecord = l_headerLine;
            i_refillBatchRecordData.setInputLine(l_headerLine);
            super.i_lineNumber = 0;
        }
        
        try
        {
            i_inputFileHandle.reset();      
            
        }
        catch (IOException e)
        {
            i_logger.logMessage( new LoggableEvent( "Could not reset file pointer",
                                                    LoggableInterface.C_SEVERITY_ERROR) );
        }
        i_inputFileHandle.setLineNumber(1);
         
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10150,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateHeaderRefillAmountAndNbOfRows);
        }
        
        return l_valid;
    }
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_validateCurrency = "validateCurrency";
    
    /**
     * Get Currency from the Header.
     * @param p_currency The currency String will be validated.
     * @return <code>true</code> when extracted currency field else <code>false</code>.
     */
    private boolean validateCurrency(String p_currency)
    {
        boolean                    l_validData             = true; 
        BusinessConfigCache        l_cache                 = null;
        CufmCurrencyFormatsData    l_formatsDataCurrency   = null;
        String                     l_currency              = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10150,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_validateCurrency);
        }        
        
        // Validate header record.                
        l_currency = p_currency;            
        
        // Check currency field.       
        if ( !l_currency.equals("   "))   // if it is not a empty string.
        {
            l_cache = (BusinessConfigCache)super.i_context.getAttribute("BusinessConfigCache");
            
            l_formatsDataCurrency = l_cache.getCufmCurrencyFormatsCache().getAvailableCurrencyFormats().getCurrencyFormatData(l_currency);
            
            if (l_formatsDataCurrency == null )
            {
                // The currency is invalid.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10120,
                                    this,
                                    C_METHOD_validateCurrency +
                                    " -- ***ERROR: The currency in the header is invalid: currency  = '"
                                    + l_currency + "'.");
                }
                
                i_refillBatchRecordData.setErrorLine(C_ERROR_CODE_INVALID_CURRENCY_CODE
                                                     + BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
                i_erroneousHeaderRecord = true;
                l_validData = false;
            }
            else
            {
                i_currency = l_formatsDataCurrency.getCurrencyCode();
                
                if (i_currency == null )
                {
                    // No  currency code found.
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10122,
                                        this,
                                        C_METHOD_validateCurrency +
                                        " -- ***ERROR: No currency found for " + l_currency
                                        + "'.");
                    }
                    
                    i_refillBatchRecordData
                    .setErrorLine(C_ERROR_CODE_INVALID_CURRENCY_CODE
                                  + BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
                    
                    i_erroneousHeaderRecord = true;
                    l_validData = false;
                }                        
                i_refillBatchRecordData.setCurrency(i_currency);
            }
        }
        else
        {
            l_cache = (BusinessConfigCache)super.i_context.getAttribute("BusinessConfigCache");
            
            try
            {
                i_currency = l_cache.getSyfgSystemConfigCache().
                                 getSyfgSystemConfigData().get("CURRENCY", "DEFAULT_CURRENCY");
                i_refillBatchRecordData.setCurrency(i_currency);
                
            }
            catch (PpasConfigException e)
            {
                i_logger.logMessage(
                    new LoggableEvent("Could not read SYFG default currency" + e.getMessageText(),
                                      LoggableInterface.C_SEVERITY_ERROR));

                i_refillBatchRecordData.setErrorLine(
                    C_ERROR_CODE_INVALID_CURRENCY_CODE +
                    BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
                
                i_erroneousHeaderRecord = true;
                l_validData = false;
            }                       
        }        
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10122,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_validateCurrency );
        }     
        
        return l_validData;        
    }
}