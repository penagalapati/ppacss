////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CCIChangeBatchReader
//      DATE            :       30-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.CCIChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.CommunitiesIdListData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class is responsible for implementation of the method getRecord. 
 * For each read line from file an CCIChangeBatchRecordData is created and stored in the in-queue.
 *
 * NOTE The file layout is [MSISDN][Old CCI1][Old CCI2]..[New CCI1][New CCI2]..
 * The number of Old and New fields are now defined with BatchConstants.C_NUMBER_OF_CCI (now set to 3). 
 */
public class CCIChangeBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                  = "CCIChangeBatchReader";
    
    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN    = "02";
               
    /** Field length of MSISDNto be installed. Value is {@value}). */
    private static final int    C_LENGTH_MSISDN               = 15;
    
    /** Field length of Master MSISDN if the first MSISDN was a subordinate one. Value is {@value}. */
    private static final int    C_LENGTH_CCI_FIELD            = 7;
        
    

    // Index in the batch record array
    /** Maximum number of fields in the batchDataRecord array, will be get a value in the constructor. */
    private static int          c_NUMBER_OF_FIELDS;

    /** Max record length, will be get a value in the constructor. */
    private static int          c_MAX_RECORD_LENGTH;
    
    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_INDEX_MSISDN                      = 0;

    /** Index for the first "old cci". Value is {@value}. */
    private static final int    C_INDEX_FIRST_CCI_FIELD             = 1;
    
    /** Regular expression to check if all tokens are spaces OR all numeric. */
    private static final String C_PATTERN_CCI_FIELD                 = "(^[ ]*[0-9[^ ]]*|^[0]*[0-9[^ ]]*)$";

    /** Regular expression to check if all tokens are spaces OR all numeric. */
    private static final String C_PATTERN_EMPTY_CCI_FIELD           = "([ ]*|[0]*)";

    /** Name of the cache to get refill method and profile information . */
    private static final String C_CACHE_NAME                        = "BusinessConfigCache";
    
    /** Destination name for the Max Number Of Community Charging Identity. */
    private static final String C_SYFG_DESTINATION_COMMU_CHARGING   = "COMMU_CHARGING";
    
    /** Parameter name for the Max Number Of Community Charging Identity. */
    private static final String C_SYFG_PARAMETER_NO_OF_COMMUNITY_ID = "NO_OF_COMMUNITY_ID";
    
    /** Default value for the number of Community Charging Identities. Value is {@value}. */
    private static final int    C_DEFAULT_NUMBER_OF_CCI_FIELDS      = 3;


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** The BatchDataRecord to be sent into the Queue. */
    private CCIChangeBatchRecordData i_batchDataRecord = null;
    
    /** Max number of CCIs - defined in the SYFG table. */
    private int                      i_maxNumberOfCCI = C_DEFAULT_NUMBER_OF_CCI_FIELDS;
    


    /**
     * Constructor for SubInstBatchReader.
     * Information about service area, service location and service class
     * is retrieved from the indata filename.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public CCIChangeBatchReader( PpasContext      p_ppasContext,
                                 Logger           p_logger,
                                 BatchController  p_controller,
                                 SizedQueue       p_queue,
                                 Map              p_parameters,
                                 PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );
        
        String              l_tmpMaxNumberOfCCI = null;
        BusinessConfigCache l_businessCache     = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }
        
        // Get max number of CCI from the SYFG table
        try
        {
            l_businessCache = (BusinessConfigCache) i_context.getAttribute(C_CACHE_NAME);
            l_tmpMaxNumberOfCCI  = l_businessCache.getSyfgSystemConfigCache().
                                    getSyfgSystemConfigData().
                                    get(C_SYFG_DESTINATION_COMMU_CHARGING,     // Destination
                                        C_SYFG_PARAMETER_NO_OF_COMMUNITY_ID);  // Parameter
            i_maxNumberOfCCI = Integer.parseInt(l_tmpMaxNumberOfCCI);
        }
        catch ( PpasConfigException p_ppasConfigE )
        {
            // Not in the SYFG table
            i_logger.logMessage(p_ppasConfigE);                    
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10450,
                this,
                " maxNOCCI=" +i_maxNumberOfCCI);
        }

        CCIChangeBatchReader.c_NUMBER_OF_FIELDS  = 1 + i_maxNumberOfCCI * 2;
        CCIChangeBatchReader.c_MAX_RECORD_LENGTH = C_LENGTH_MSISDN + i_maxNumberOfCCI * 2 * C_LENGTH_CCI_FIELD;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;
        
    } // end of public Constructor SubInstBatchReader(.....)

 





    /**
     * Get method for the created BatchDataRecord.
     * @return BatchDataRecord
     */
    public BatchRecordData getBatchDataRecord()
    {
        return this.i_batchDataRecord;
    }





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        String l_nextLine  = null;  // Current input line

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10320,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchDataRecord = null;
        l_nextLine        = readNextLine();

        if ( l_nextLine != null )
        {
            i_batchDataRecord = new CCIChangeBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename( i_inputFileName );
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber( i_lineNumber );
            
            // Store original data record
            i_batchDataRecord.setInputLine( i_inputLine );
            
            // Extract data from line
            if ( extractLineAndValidate() )
            {
                // BatchRecordData has been updated in the method extractLineAndValidate(.)
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10330,
                        this,
                        "extractLineAndValidate OK, DumpRecord: "+i_batchDataRecord.dumpRecord());
                }

            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10340,
                        this,
                        "extractLineAndValid failed!" );
                }

                i_batchDataRecord.setCorruptLine( true );
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    i_batchDataRecord.setErrorLine( C_ERROR_INVALID_RECORD_FORMAT + 
                                                    BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                    i_batchDataRecord.getInputLine() );
                }
                
            } // End of else... extractLineAndValidate failed
            
            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            i_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                i_batchDataRecord.setFailureStatus(true);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10340,
                    this,
                    "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
            }
            
        } // end if ... nextLine != null


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;
        
    } // end of method getRecord()




    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form:
     *     COMCHARGE_yyyymmdd_sssss.DAT /IPG.
     * @param p_fileName indata file name.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid(String p_fileName )
    {                
        boolean  l_validFileName = true;       // Help variable - return value. Assume filename is OK
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10460,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_IPG) )
            {
                // OK Filename: INSTALL_STATUS_yyyymmdd_sssss.DAT
                // Rename the file to .IPG to indicate that processing is in progress.
                // If it is recovery mode, the file already got the "in_progress_extension"
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    renameInputFile( 
                        BatchConstants.C_PATTERN_INDATA_FILE,
                        BatchConstants.C_EXTENSION_INDATA_FILE,
                        BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10470,
                            this,
                            "After rename, inputFullPathName="+i_fullPathName );
                    }
                                   
                }

            }
            // OK Filename: INSTALL_STATUS_yyyymmdd_sssss.SCH (see above)
            else if (p_fileName.matches(BatchConstants.C_PATTERN_CCI_CHANGE_FILENAME_SCH))
            {
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    renameInputFile( 
                        BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                        BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                        BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10475,
                            this,
                            "After rename, inputFullPathName="+i_fullPathName );
                    }
                                   
                }
                
            }
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
                    
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10480,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
        
    } // End of isFileNameValid(.)

    



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [Line number];[MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
     * The line number will be used to check against the recovery file if the batch mode is
     * recovery.
     * All fields are checked to be alpha numberic.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat          l_msisdnFormat         = null;
        Msisdn                l_tmpMsisdn            = null;  // Help variable to convert from string to Msisdn object.
        String[]              l_recordFields         = new String[c_NUMBER_OF_FIELDS];   // Help array to keep the input line elements, sent to super.
 
        StringBuffer          l_tmp                  = null;  // Help variable for debug printout 
        boolean               l_validData            = true;  // return value - assume it is valid
        Vector                l_oldCCIList           = null;
        Vector                l_newCCIList           = null;
        
        CommunitiesIdListData l_oldCommunitiesIsList = null;
        CommunitiesIdListData l_newCommunitiesIsList = null;
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
        }

            
        if ( i_inputLine.length() != CCIChangeBatchReader.c_MAX_RECORD_LENGTH )
        {
            // Record must have the length: MSISDNlength + 2*N*CCIlenght.
            // No idea to continue..
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10390,
                    this,
                    "ILLEGAL Row: ]"+i_lineNumber+" length="+i_inputLine.length()+" ]"+i_inputLine+"[");
            }

            return false;
        }
        
        
        // Store read record, result array and BatchRecordData in super class    
        initializeRecordValidation( i_inputLine, l_recordFields, i_batchDataRecord );
        

        // Input file layout:
        // ------------------
        // Get the record fields and check if it is numeric/alphanumberic
        // File record : <MSISDN><Old CCI 1><Old CCI 2>...<New CCI 1><New CCI 2>...
        // A record with a single MSISDN is illegal
        // A record with a MSISDN, spaces in all the oldCCIs and one or more newCCIs defined means that it shall add these new 
        // The max number of CCI-fields is defined in SYFG

        // First check that there is one MSISDN and that the rest of the fields matches to be spaces OR numeric
        if ( getFieldNoTrim( 0, C_LENGTH_MSISDN, C_INDEX_MSISDN, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN ) &&
             getGroupOfCCIFields( CCIChangeBatchReader.c_NUMBER_OF_FIELDS-1, 
                                  C_INDEX_FIRST_CCI_FIELD, 
                                  C_LENGTH_MSISDN,  
                                  C_LENGTH_CCI_FIELD,
                                  C_PATTERN_CCI_FIELD ))
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10420,
                    this,
                    "OK Row: " + i_lineNumber +" :]"+ i_inputLine+"[,"+ " ]"+ l_recordFields[0]+"[  ]"+
                                                      l_recordFields[1]+"[  ]"+
                                                      l_recordFields[2]+"[  ]"+
                                                      l_recordFields[3]+"[  ]"+
                                                      l_recordFields[4]+"[  ]"+
                                                      l_recordFields[5]+"[  ]"+
                                                      l_recordFields[6]+"[");
            } 

            // MSISDN is mandatory
            if ( l_recordFields[C_INDEX_MSISDN] == null )
            {
                l_validData = false;
            }
            
            else
            {
                // Got perhaps a valid record. ( Don't know yet how many newCCIs there are. These may not have spaces.)
                // Convert the MSISDN string to a Msisdn object
                // This may give an ParseException - if so flag currupt line!
                l_msisdnFormat = i_context.getMsisdnFormatInput();
                try
                {
                    l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_INDEX_MSISDN].trim() );
                    i_batchDataRecord.setMsisdn(l_tmpMsisdn );
                }
                catch (ParseException e)
                {
                    l_validData = false;
                
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10450,
                            this,
                            "Row: " + i_lineNumber + " ]"+ i_inputLine + "[  has invalid MSISDN"+ " maxNOCCI=" +i_maxNumberOfCCI );
                    }
                }
 
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10450,
                        this,
                        "Row: " + i_lineNumber + " ]"+ i_inputLine + "[  has a valid MSISDN!!"+ " maxNOCCI=" +i_maxNumberOfCCI);
                }

                l_oldCCIList = new Vector(i_maxNumberOfCCI);
                l_newCCIList = new Vector(i_maxNumberOfCCI);
                
                for ( int i = 1; i < i_maxNumberOfCCI+1; i++ )
                { 
                    try
                    {
                        //Don't add nulls or empty requests to the Vector
                        if ( (l_recordFields[i] != null) 
                              && !(l_recordFields[i].matches(C_PATTERN_EMPTY_CCI_FIELD)) )
                        {
                            l_oldCCIList.add(new Integer(l_recordFields[i].trim()));    
                        }
                        
                        if ( (l_recordFields[i + i_maxNumberOfCCI] != null)
                              && !(l_recordFields[i + i_maxNumberOfCCI].matches(C_PATTERN_EMPTY_CCI_FIELD))  )
                        {
                            l_newCCIList.add(new Integer(l_recordFields[i + i_maxNumberOfCCI].trim()) );    
                        }
                    }
                    catch (NumberFormatException e)
                    {
                        l_validData = false;
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10450,
                                this,
                                "*** MTR *** NumberFOrmatException "+i+":]"+ l_recordFields[i]+"[ "+(i + i_maxNumberOfCCI)+":] "+l_recordFields[(i + i_maxNumberOfCCI)]);
                        }

                        break;
                    }
                }
                
                try
                {
                    // Store retrieved Old CCIs from file into data record 
                    l_oldCommunitiesIsList = new CommunitiesIdListData(null, l_oldCCIList);
                    i_batchDataRecord.setOldCCIList(l_oldCommunitiesIsList);
                    
                    // Store retrieved New CCIs from file into data record    
                    l_newCommunitiesIsList = new CommunitiesIdListData(null, l_newCCIList);
                    i_batchDataRecord.setNewCCIList(l_newCommunitiesIsList);
                }
                catch (PpasParseException e1)
                {
                    l_validData = false;
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10450,
                            this,
                            "*** MTR *** PpasParseException cannot create CommunitiesIdListData....");
                    }

                }
                
            } // End - mandatory field test
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10420,
                    this,
                    "ILLEGAL Row: " + i_lineNumber +" :]"+ i_inputLine+"[, "+" ]"+ l_recordFields[0]+"[  ]"+
                                                      l_recordFields[1]+"[  ]"+
                                                      l_recordFields[2]+"[  ]"+
                                                      l_recordFields[3]+"[  ]"+
                                                      l_recordFields[4]+"[  ]"+
                                                      l_recordFields[5]+"[  ]"+
                                                      l_recordFields[6]);
            } 

            // Illegal field format detected
            if (PpasDebug.on)
            {
                l_tmp = new StringBuffer();
                l_tmp.append("Row: ");
                l_tmp.append(i_lineNumber);
                l_tmp.append(" is an INVALID record\n");
                
                for ( int i = 0; i < l_recordFields.length; i++ )
                {
                    l_tmp.append(i);
                    l_tmp.append(": ");
                    l_tmp.append( l_recordFields[i]);
                    l_tmp.append("\n");
                }

                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10430,
                    this,
                    l_tmp.toString());
            }
            l_tmp = null;
                                                                
            // Flag for corrupt line
            l_validData = false;
        }
                                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10480,
                this,
                "\n**************************************\n"
                + BatchConstants.C_LEAVING 
                + C_METHOD_extractLineAndValidate
                + " returns :"
                + l_validData
                + "\n**************************************\n" );
        }

        return l_validData;
        
    } // End of method extractLineAndValidate(.)


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getGroupOfCCIFields = "getGroupOfCCIFields";    
    /**
     * This method retrieves a specified number of concecutive fields from the indata record.
     * Starting with field number : p_startIndex.
     * @param p_numberOfFieldsToRead - specifies the number of fields to be read in this group
     * @param p_startIndex - field number to start with
     * @param p_lastFieldLength - the length of the previous read field
     * @param p_currentFieldLength - the length of the fields in this group
     * @param p_pattern   the regex pattern for qualify a CCI
     * @return TRUE if valid groups of CCI fields are found.
     */
    private boolean getGroupOfCCIFields( 
            int p_numberOfFieldsToRead, 
            int p_startIndex, 
            int p_lastFieldLength, 
            int p_currentFieldLength, 
            String p_pattern )
    {
        int     l_tmpIndex  = p_startIndex;
        boolean l_validData = true;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10490,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getGroupOfCCIFields );
        }

        // The method getField must get the valid length of the previous field that was read.
        // Therefore first read one field and then loop 
        if ( getFieldNoTrim(p_lastFieldLength, p_currentFieldLength, l_tmpIndex++, p_pattern, C_ERROR_INVALID_RECORD_FORMAT ))
        {
            // First field in group was OK - read the rest of the fields in this group
            for ( int i = 1; i < p_numberOfFieldsToRead; i++ ) 
            { 
                if ( !getFieldNoTrim(p_currentFieldLength, p_currentFieldLength, l_tmpIndex++, p_pattern, C_ERROR_INVALID_RECORD_FORMAT ))
                {                    
                    // Got an invalid record
                    l_validData = false;
                    break;
                }                
            }
        }            
        else
        {            
            l_validData = false;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10550,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getGroupOfCCIFields + "  returns="+l_validData );
        }
        
        return l_validData;
        
    } // End of getCCIGroupFields
    
    

} // End of class CCIChangeBatchReader

