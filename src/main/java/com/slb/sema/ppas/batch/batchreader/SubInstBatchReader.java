////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SubInstBatchReader
//      DATE            :       16-April-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to read installation orders from flat file.
 * The actual readoperation from file is hidden within the super class so this
 * class operates on strings that origins from the file. For each read order (line)
 * a SubInstBatchDataRecord is created and populated.
 */
public class SubInstBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                          = "SubInstBatchReader";
    
    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT         = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN            = "02";
    
    /** Constant holding the error code for invalid account group id. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_ACCOUNT_GROUP_ID = "18";

    /** Constant holding the error code for invalid data. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_DATA             = "19";

    /** Field length of MSISDNto be installed. Value is {@value}). */
    private static final int    C_MSISDN_LENGTH                       = 15;
    
    /** Field length of Master MSISDN if the first MSISDN was a subordinate one. Value is {@value}. */
    private static final int    C_MASTER_MSISDN_LENGTH                = 15;
    
    /** Field length of SDP ID. Value is {@value}. */
    private static final int    C_SDP_ID_LENGTH                       = 2;
    
    /** Field length of Agent. Value is {@value}. */
    private static final int    C_AGENT_LENGTH                        = 8;

    /** Field length of Promotion Plan. Value is {@value}. */
    private static final int    C_PROMOTION_PLAN_LENGTH               = 4;
    
    /** Field length of Account Group. Value is {@value} */
    private static final int    C_ACCOUNT_GROUP_LENGTH                = 10;
    
    /** Field length of Service Offering. Value is {@value}*/
    private static final int    C_SERVICE_OFFERINGS_LENGTH            = 10;
    
    /** Field length of UUSD EoCN Structure ID. Value is {@value}*/
    private static final int    C_USSD_EOCN_LENGTH                    = 3;

    /** Length of Market (concatination of Service Area and Service Location. Value is {@value}*/
    private static final int    C_MARKET_LENGTH                       = 8;

    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS                    = 8;
    
    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_MSISDN                              = 0;
    
    /** Index for the field Master MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_MASTER_MSISDN                       = 1;
    
    /** Index for the field SCP ID in the batchDataRecord array. Value is {@value}. */
    private static final int    C_SCP_ID                              = 2;
    
    /** Index for the field Agent in the batchDataRecord array. Value is {@value}. */
    private static final int    C_AGENT                               = 3;
    
    /** Index for the field Promotion Plan in the batchDataRecord array. Value is {@value}. */
    private static final int    C_PROMOTION_PLAN                      = 4;
    
    /** Index for the field Account Group in the batchDataRecord array. Value is {@value} */
    private static final int    C_ACCOUNT_GROUP                       = 5;
    
    /** Index for the field field length of Service Offering in the batchDataRecord array. Value is {@value}*/
    private static final int    C_SERVICE_OFFERINGS                   = 6;
    
    /** Index for the field field length of USSD EoCN in the batchDataRecord array. Value is {@value}*/
    private static final int    C_USSD_EOCN                           = 7;
     
    /** Record length. Value is [@value). */
    private static final int    C_RECORD_LENGTH               = C_MSISDN_LENGTH + 
                                                                C_MASTER_MSISDN_LENGTH + 
                                                                C_SDP_ID_LENGTH + 
                                                                C_AGENT_LENGTH + 
                                                                C_PROMOTION_PLAN_LENGTH + 
                                                                C_ACCOUNT_GROUP_LENGTH + 
                                                                C_SERVICE_OFFERINGS_LENGTH + 
                                                                C_USSD_EOCN_LENGTH;

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
   
    /** The service area can be 2/4 characters. Retrieved from either from the filename if the
     * filename has that informtaion or an argument to the constructor.
     */
    private String                 i_serviceArea                   = null;

    /** The service location can be 2/4 characters. Retrieved from either from the filename if the
     * filename has that informtaion or an argument to the constructor.
     */    
    private String                 i_serviceLocation               = null;
    /** Help variable to verify serviceLocation retrieved from the filename. */
    
    /** The concatination of the Service Area and Service Location, must be 8 characters. */
    private String                 i_market                        = null;

    /** The Service Class is retrieved from the filename. */    
    private String                 i_serviceClass                  = null;
    
    /** The BatchDataRecord to be sent into the Queue. */
    private SubInstBatchRecordData i_batchDataRecord = null;
    

    /**
     * Constructor for SubInstBatchReader.
     * The SubInstBatchController creates this object.
     * The input filename is extracted from the p_parameters.
     * The extension of the file determines whether this is a recovery
     *  run or not (*.IPG = recovery, *.DAT = normal run).
     * If the service class, location and area are not included in p_params they 
     * will be extracted from the input file name.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public SubInstBatchReader( PpasContext      p_ppasContext,
                               Logger           p_logger,
                               BatchController  p_controller,
                               SizedQueue       p_queue,
                               Map              p_parameters,
                               PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }
        //TODO Quick fix to set up internal sloc/srva/sc with sohort filename until proper solution is found
        this.isFileNameValid((String)p_parameters.get(BatchConstants.C_KEY_INPUT_FILENAME));
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;
        
    } // end of public Constructor SubInstBatchReader(.....)

    /**
     * Get method for the created BatchDataRecord.
     * @return BatchDataRecord
     */
    public BatchRecordData getBatchDataRecord()
    {
        return this.i_batchDataRecord;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the SubInstBatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchDataRecord = null;

        if ( readNextLine() != null )
        {
            i_batchDataRecord = new SubInstBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename( i_inputFileName );
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber( i_lineNumber );
            
            // Store original data record
            i_batchDataRecord.setInputLine( i_inputLine );
            
            // Extract data from line
            if ( extractLineAndValidate() )
            {

                // Record information OK - and has been added to the BatchDataRecord in
                // the method extractLineAndValidation.
                // Add Service data information
                i_batchDataRecord.setServiceArea( this.i_serviceArea );
                i_batchDataRecord.setServiceLocation( this.i_serviceLocation );
                i_batchDataRecord.setMarket( this.i_market );
                i_batchDataRecord.setServiceClass( this.i_serviceClass );  
                            
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10343,
                        this,
                        "extractLineAndValid failed!" );
                }

                i_batchDataRecord.setCorruptLine( true );
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    i_batchDataRecord.setErrorLine( C_ERROR_INVALID_RECORD_FORMAT +
                                                    BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                    super.i_inputLine);
                }

            }

            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            i_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                i_batchDataRecord.setFailureStatus(true);
            }
                        
        } // end if
             
        if ( i_batchDataRecord != null )
        {

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "DumpRecord: " + i_batchDataRecord.dumpRecord());
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "i_batchDataRecord is NULL!!!!!");
            }            
        }       
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;
        
    } // end of method getRecord()
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form: INSTALL_yyyymmdd_sssss.DAT /IPG.
     *     INSTALL_xxzz_cccc_yyyymmdd_sssss.DAT /IPG
     * @param p_fileName Filename to test.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {
       
        // Market converted into 4+4 characters, retrieved from 3rd element from 
        //splitMarketIntoServiceAreaAndLocation.
        final int    L_MARKET_INDEX           = 2;

        boolean      l_validFileName          = true;  // Help variable - return value. Assume filename is OK.
        String[]     l_fileNameComponents     = null;  // After split on "_"
        String[]     l_serviceAreaAndLocation = null;  // Containing Service area and location, and the 
                                                       //concatinated string of service area and location 
                                                       //called market.

 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_LONG_FILENAME_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_LONG_FILENAME_SCH) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_LONG_FILENAME_IPG) )
            {
                // Filename: INSTALL_xxyy_cccc_yyyymmdd_sssss.*
                // Retrieve ServiceArea, ServiceLocation, Market and ServiceClass from the filename
                l_fileNameComponents     = p_fileName.split( 
                     BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER );            
                l_serviceAreaAndLocation = splitMarketIntoServiceAreaAndLocation( 
                     l_fileNameComponents[BatchConstants.C_INDEX_MARKET_IN_FILENAME_INDEX] );
            
                this.i_serviceArea     = l_serviceAreaAndLocation[BatchConstants.C_INDEX_SERVICE_AREA_INDEX];
                this.i_serviceLocation = 
                    l_serviceAreaAndLocation[BatchConstants.C_INDEX_SERVICE_LOCATION_INDEX];
                this.i_market          = l_serviceAreaAndLocation[L_MARKET_INDEX];
                this.i_serviceClass    = 
                    l_fileNameComponents[BatchConstants.C_INDEX_SERVICE_CLASS_IN_FILENAME_INDEX];
            
            }
            
            else if (p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_DAT) ||
                     p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_SCH) ||
                     p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_IPG) )
            {
                // Filename: INSTALL_yyyymmdd_sssss.*
                // Retrieve ServiceArea, ServiceLocation, Market and ServiceClass from the Map            
                this.i_serviceArea     = (String)i_parameters.get(BatchConstants.C_KEY_SERVICE_AREA);
                this.i_serviceLocation = (String)i_parameters.get(BatchConstants.C_KEY_SERVICE_LOCATION);
                this.i_serviceClass    = (String)i_parameters.get(BatchConstants.C_KEY_SERVICE_CLASS);
                this.i_market          = getMarket();
            }
            
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
        
            // Check if recovery mode
            if ( l_validFileName && !i_recoveryMode )
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                if (p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_LONG_FILENAME_SCH) || 
                    p_fileName.matches(BatchConstants.C_PATTERN_SUBINST_SHORT_FILENAME_SCH))
                {
                    renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                }
                else
                {
                    renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_INDATA_FILE,
                                     BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10410,
                        this,
                        "After rename, inputFullPathName=" + i_fullPathName );
                }
               
            }
            
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10410,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
    }
 
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
     * All fields are checked to be alpha numeric.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat = null;
        
        // Help variable to convert from string to Msisdn object.
        Msisdn       l_tmpMsisdn    = null;
        
        // Help array to keep the input line elements
        String[]     l_recordFields = new String[C_NUMBER_OF_FIELDS];
 
        boolean      l_validData    = true;  // return value - assume it is valid
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
        }

        // Check record length - not required
        if ( i_inputLine.length() <= C_RECORD_LENGTH )
        {

            initializeRecordValidation( i_inputLine, l_recordFields, i_batchDataRecord );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10361,
                    this,
                    "extractLineAndValidate innan IF getfields....." );
            }

            // Get the record fields and check if it is numeric/alphanumberic
            //            Start                       End                         #Field               Format check                            Errorcode
            if ( getField(0,                          C_MSISDN_LENGTH,        C_MSISDN,        BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN )   &&
                 getField(C_MSISDN_LENGTH,            C_MASTER_MSISDN_LENGTH, C_MASTER_MSISDN, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN )   &&
                 getField(C_MASTER_MSISDN_LENGTH,     C_SDP_ID_LENGTH,            C_SCP_ID,            BatchConstants.C_PATTERN_NUMERIC,       C_ERROR_INVALID_RECORD_FORMAT) &&
                 getField(C_SDP_ID_LENGTH,            C_AGENT_LENGTH,             C_AGENT,             BatchConstants.C_PATTERN_ALPHA_NUMERIC, C_ERROR_INVALID_RECORD_FORMAT) &&
                 getField(C_AGENT_LENGTH,             C_PROMOTION_PLAN_LENGTH,    C_PROMOTION_PLAN,    BatchConstants.C_PATTERN_ALPHA_NUMERIC, C_ERROR_INVALID_RECORD_FORMAT) &&
                 getField(C_PROMOTION_PLAN_LENGTH,    C_ACCOUNT_GROUP_LENGTH,     C_ACCOUNT_GROUP,     BatchConstants.C_PATTERN_NUMERIC,       C_ERROR_INVALID_RECORD_FORMAT) &&
                 getField(C_ACCOUNT_GROUP_LENGTH,     C_SERVICE_OFFERINGS_LENGTH, C_SERVICE_OFFERINGS, BatchConstants.C_PATTERN_NUMERIC,       C_ERROR_INVALID_RECORD_FORMAT) && 
                 getField(C_SERVICE_OFFERINGS_LENGTH, C_USSD_EOCN_LENGTH,         C_USSD_EOCN,         BatchConstants.C_PATTERN_NUMERIC,       C_ERROR_INVALID_RECORD_FORMAT))
            {

                // MSISDN is mandatory
                if ( l_recordFields[C_MSISDN] == null  )
                {
                    l_validData = false;
                }
                
                // VALID RECORD!
                
            }
            else
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME,
                        10365,
                        this,
                        "Row: " + i_lineNumber + " ]"+ i_inputLine + "[ is a INVALID record" +
                                " l_lineNumber     =" + i_lineNumber + "\n" +
                                " l_msisdn         =" + l_recordFields[C_MSISDN] + "\n" +
                                " l_masterMsisdn   =" + l_recordFields[C_MASTER_MSISDN] + "\n" +
                                " l_scpId          =" + l_recordFields[C_SCP_ID] + "\n" +
                                " l_agent          =" + l_recordFields[C_AGENT] + "\n" +
                                " l_promotionPlan  =" + l_recordFields[C_PROMOTION_PLAN] + "\n" +
                                " l_accountGroup   =" + l_recordFields[C_ACCOUNT_GROUP] + "\n" +
                                " l_serviceOffering=" + l_recordFields[C_SERVICE_OFFERINGS] + "\n" +
                                " l_ussdEocn   =" + l_recordFields[C_USSD_EOCN] + "\n");
                }
                                                                    
                // Flag for corrupt line
                l_validData = false;
            }
                
            // Store information into the BatchDataRecord
            // If some of the fields were invalid their value will be NULL
                    
            //TODO default data - get the default value for SCP_ID
            
            // Convert the MSISDN string (master and subordinate) to a Msisdn object
            // This may give an ParseException - if so flag currupt line!
            if (l_validData)
            {
                l_msisdnFormat = i_context.getMsisdnFormatInput();
                try
                {
                    l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_MSISDN] );
                    i_batchDataRecord.setMsidsdn( l_tmpMsisdn );
                    
                    if (null != l_recordFields[C_MASTER_MSISDN])
                    {
                        l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_MASTER_MSISDN] );
                    }
                    i_batchDataRecord.setMasterMsisdn( l_tmpMsisdn );
                }
                catch (ParseException e)
                {
                    l_validData = false;
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10366,
                                this,
                                "Row: " + i_lineNumber + " ]"+ i_inputLine + "[ has invalid MSISDN" );
                    }
                }
            }
            
            if ((l_validData) && null != l_recordFields[C_SCP_ID] )
            {
                i_batchDataRecord.setSdpId( l_recordFields[C_SCP_ID] );    
            }
            
            if ((l_validData) && null != l_recordFields[C_AGENT] )
            {
                i_batchDataRecord.setAgent( l_recordFields[C_AGENT] );
            }
            
            if ((l_validData) && null != l_recordFields[C_PROMOTION_PLAN] )
            {
                i_batchDataRecord.setPromotionPlan( l_recordFields[C_PROMOTION_PLAN] );
            }            
            if ((l_validData) && null != l_recordFields[C_ACCOUNT_GROUP])
            {
                if (l_recordFields[C_ACCOUNT_GROUP].length() < C_ACCOUNT_GROUP_LENGTH)
                {
                    i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                                   + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                   + super.i_inputLine);

                    l_validData = false;
                }
                else
                {
                    try
                    {
                        i_batchDataRecord.setAccountGroupId(l_recordFields[C_ACCOUNT_GROUP]);
                    }
                    catch (PpasServiceFailedException e1)
                    {
                        i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_ACCOUNT_GROUP_ID
                                                       + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                       + super.i_inputLine);

                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10366,
                                            this,
                                            "Invalid AccountGroupId: " + l_recordFields[C_ACCOUNT_GROUP]);
                        }

                        l_validData = false;
                    }
                }
            }            
            
            if ((l_validData) && null != l_recordFields[C_SERVICE_OFFERINGS])
            {
                if (l_recordFields[C_SERVICE_OFFERINGS].length() < C_SERVICE_OFFERINGS_LENGTH)
                {
                    i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                                   + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                   + super.i_inputLine);

                    l_validData = false;
                }
                else
                {
                    try
                    {
                        i_batchDataRecord.setServiceOfferings(l_recordFields[C_SERVICE_OFFERINGS]);
                    }
                    catch (PpasServiceFailedException e1)
                    {
                        i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_DATA
                                                       + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                       + super.i_inputLine);

                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10366,
                                            this,
                                            "Invalid ServiceOfferings: "
                                                    + l_recordFields[C_SERVICE_OFFERINGS]);
                        }
                        l_validData = false;
                    }
                }
            }
            if ((l_validData) && null != l_recordFields[C_USSD_EOCN])
            {
                if (l_recordFields[C_USSD_EOCN].length() < C_USSD_EOCN_LENGTH)
                {
                    i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                                   + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                   + super.i_inputLine);

                    l_validData = false;
                }
                else
                {
                    try
                    {
                        i_batchDataRecord
                                .setUssdEocn(new EndOfCallNotificationId(l_recordFields[C_USSD_EOCN]));
                    }
                    catch (PpasParseException e)
                    {
                        i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_DATA
                                                       + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                       + super.i_inputLine);
                        l_validData = false;
                    }
                }
            }
            
        }  // End if - check max record length
        else
        {
            i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                           + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                           + super.i_inputLine);

            l_validData = false;
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10370,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return l_validData;
        
    } // end of method extractLineAndValidate(.)

    /**
     * Creates a Markt from the service area <code>i_serviceArea</code> and the sevice location
     * <code>i_serviceLocation</code>.
     * @return The market.
     */
    private String getMarket()
    {
        String       l_tmpServiceArea     = null;
        String       l_tmpServiceLocation = null;
        StringBuffer l_tmp                = new StringBuffer(C_MARKET_LENGTH);
        String       l_market             = null;
        
        l_tmpServiceArea     = padWithZero(i_serviceArea);
        l_tmpServiceLocation = padWithZero(i_serviceLocation);
        
        if ( l_tmpServiceArea     != null &&
             l_tmpServiceLocation != null )
        {
            l_tmp.append(l_tmpServiceArea);
            l_tmp.append(l_tmpServiceLocation);
            l_market = l_tmp.toString();
        }
        return l_market;
    }
}
