////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromAllocBatchUT
//      DATE            :       18-Aug-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class is the main entry point for starting Promotion
//                              Plan Allocation Provisioning. It is instantiated by the
//                              Job Scheduler.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.common.dataclass.AccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;

/** Test promotion allocations (Batch).
 * 
 * @see com.slb.sema.ppas.batch.batchcontroller.PromAllocBatchController
 */
public class PromAllocBatchUT extends BatchTestCaseTT
{
    /** Master account. */
    AccountData     i_masterAccount = null;
    /** Account data. */
    AccountData     i_accountData   = null;
    /** DB service test tool. */
    DbServiceTT     i_dbServiceTT   = null;
    /** Test market. */
    Market          i_market        = new Market( DbServiceTT.C_DEFAULT_SRVA,
                                                  DbServiceTT.C_DEFAULT_SLOC );
    /** Test SDP. */
    String          i_sdpId         = "00";
    /** Test MSISDN. */
    Msisdn          i_msisdn        = null;
    /** Data record 1. */
    BatchRecordData i_record1       = null;
    /** Data record 2. */
    BatchRecordData i_record2       = null;

    /** Standard constructor.
     * 
     * @param p_name Name of test.
     */
    public PromAllocBatchUT( String p_name )
    {
        super( p_name );

        // Install a new subscriber
        try
        {
            i_masterAccount = i_dbServiceTT.installTestSubscriber(null);
            i_accountData   = i_dbServiceTT.installTestSubscriber( i_masterAccount );

            i_msisdn = i_dbServiceTT.getNextFreeMsisdn( i_market, i_sdpId );
        }
        catch( PpasServiceException e )
        {
            failedTestException(e);
        }
        catch( PpasSqlException e )
        {
            failedTestException(e);
        }
    }
}