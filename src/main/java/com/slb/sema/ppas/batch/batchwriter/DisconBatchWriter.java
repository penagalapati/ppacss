////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DisconBatchWriter
//      DATE            :       4-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 05/05/06 | Chris         | Filter using configured list of  | PpacLon#2250/8691
//          | Harrison      | Disconnection Reason codes.      | PRD_ASC00_GEN_CA_82
//----------+---------------+----------------------------------+-------------------
// 28/03/07 | Lars L.       | Update record counters (success  | PpacLon#1861/11237
//          |               | and failed) on the corresponding | 
//          |               | BatchMaster record in the        | 
//          |               | BACO_BATCH_CONTROL table.        | 
//----------+-----------+---+----------------------------------+--------------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job    |
//----------+-----------+--------------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.DisconBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for doing the final processing of a disconnection, 
 * that is, write the outcome to file. Only one type if file is used for "Batch Disconnection" -
 * output file. All disconnection's orders processed will contribute with one
 * line in the output file.
 * Since the processing-component knows the 'outcome' of an operation has been kind enough
 * to create this information for us so all we do is the actual file-creation and file
 * writing.
 */
public class DisconBatchWriter extends BatchWriter
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "DisconBatchWriter";
    
    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY          =
        "Property for OUTPUT_FILE_DIRECTORY not found";

    /** Logger printout - cannot open error- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_OUTPUT_FILE     = "Cannot open output-file";

    /** File prefix for the output filename. */
    private static final String C_PREFIX_OUTPUT_FILE_NAME     = "DISC_";
    
    /** File suffix for a temporary output filename. */
    private static final String C_SUFFIX_TEMP_OUTPUT_FILE_NAME = ".TMP";
    
    /** File suffix for the output filename. */
    private static final String C_SUFFIX_OUTPUT_FILE_NAME     = ".DAT";
    
    /** The length of the output file sequence number. */
    private static final int    C_LENGTH_FILE_SEQUENCE_NUMBER = 5;
    
    //--------------------------------------------------------
    //  Private variables.
    //  ------------------------------------------------------ 
    /** Counter for number of precessed records. */
    private int    i_successfulRecs      = 0;

    /** Counter for number of precessed records. */
    private int    i_failedRecs          = 0;

    /** Output file directory. */
    private String i_outputFileDirectory = null;

    /** Disconnection Reason codes to filter records with */
    private String i_disconnectionCodes  = null;

    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Constructs a <code>DisconBatchWriter</code> instance using the passed parameters.
     * 
     * @param p_ppasContext The PpasContext for the batch.
     * @param p_logger      The Logging facility for the batch.
     * @param p_controller  The call-back to the batch controller.
     * @param p_params      The start arguments.
     * @param p_outQueue    The output queue.
     * @param p_properties  The batch properties.
     */
    public DisconBatchWriter(
        PpasContext           p_ppasContext,
        Logger                p_logger,
        DisconBatchController p_controller,
        Map                   p_params,
        SizedQueue            p_outQueue,
        PpasProperties        p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);
        
        String       l_outputFileFullPath = null;
        StringBuffer l_tmp                = null;
        String       l_fileSequenceNumber = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        // Get the file sequence number.
        try
        {
            l_fileSequenceNumber = getFileSequenceNumber();
        }
        catch (PpasServiceException l_Ex)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "Cannot obtain a sequence number from the database.",
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME, 10330, this,
                                C_CANNOT_OPEN_OUTPUT_FILE + ",  method: Constructor." );
            }
        }

        if ( this.getProperties() )
        {
            //Get the full path filename for the output-file 
            l_tmp = new StringBuffer();
            l_tmp.append(this.i_outputFileDirectory);
            l_tmp.append("/");
            l_tmp.append(C_PREFIX_OUTPUT_FILE_NAME);        
            l_tmp.append(DatePatch.getDateTimeNow().toString_yyyyMMdd());
            l_tmp.append("_");
            l_tmp.append(l_fileSequenceNumber );
            l_tmp.append(C_SUFFIX_TEMP_OUTPUT_FILE_NAME);

            l_outputFileFullPath = l_tmp.toString();
           
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10310,
                    this,
                    "Output file full path: '" + l_outputFileFullPath + "'" );
            }

            try
            {
                super.openFile(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN, new File(l_outputFileFullPath));
            }
            catch (IOException e)
            {
                
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_OUTPUT_FILE,
                                     LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10335,
                        this,
                        C_CANNOT_OPEN_OUTPUT_FILE + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10340,
                        this,
                        "Finally - efter openFile..." );
                }
            }    
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10360, this,
                            BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        DisconBatchRecordData l_record         = (DisconBatchRecordData)p_record;
        String[]              l_outputLineArr  = null;
        boolean               l_recordFiltered = false;

        if (l_record.getErrorLine() == null)
        {
            // Filter any record that has a Disconnection Reason code that
            // matches the criteria specified in the properties file.
            if ((i_disconnectionCodes != null) &&
                (i_disconnectionCodes.indexOf(l_record.getDisconnectReason()) != -1))
            {
                l_recordFiltered = true;
            }
    
            if (!l_recordFiltered)
            {
                //Check if error file info exist and write it to file.
                if (l_record.getOutputLine() != null)
                {
                    //Write to output file, it could be a comma separated list of output lines (a master subs.
                    //and its subordinates. See DisconBatchProcessor.processRecord(...)).
                    l_outputLineArr = l_record.getOutputLine().split(",");
                    i_successfulRecs += l_outputLineArr.length;
                    for (int i = 0; i < l_outputLineArr.length; i++)
                    {
                        super.writeToFile(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN, l_outputLineArr[i]);
                    }
                 }
            }
        }
        else
        {
            i_failedRecs++;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10370, this,
                            BatchConstants.C_LEAVING + C_METHOD_writeRecord);
        }
        
        return;
    } // End of writeRecord(.)



    /**
     * Does all necessary final processing that is, writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws PpasServiceException
    {
        // Flush's all currently open files.
        super.flushFiles();

        // Update record counters.
        BatchSubJobData l_batchSubJobData =
            ((DisconBatchController)super.i_controller).getKeyedControlRecord();
        
        updateSubJobRecordCounters(null,
                                   Integer.parseInt(l_batchSubJobData.getMasterJsJobId()),
                                   new PpasDateTime(l_batchSubJobData.getExecutionDateTime()),
                                   i_successfulRecs,
                                   i_failedRecs);
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * This method tries to read the properies for:
     * Table update frequency and directories for the input-, error- and recovery-files.
     * @return true if the properties were defined.
     */
    private boolean getProperties()
    {
//        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        boolean l_returnValue = true;  // Assume all found
        
        // Get list of Disconnection Reason codes to use for filtering records.
        i_disconnectionCodes = i_properties.getTrimmedProperty(
                                                    BatchConstants.C_FILTER_DISCONNECTION_REASON_CODES);

        // Get the output file directory
        i_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
        if ( i_outputFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10420,
                    this,
                    C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
                
        return l_returnValue;
        
     } // End of method getProperties()



    /** 
     * This batch does not write out trailer records.
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        //This batch does not write out trailer records
 
        //Rename the report file from .TMP to .RPT.
        BatchFile l_outFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN);
        
        if (l_outFile != null && l_outFile.getFile().exists())
        {
            l_outFile.renameFile(C_SUFFIX_TEMP_OUTPUT_FILE_NAME,
                                 BatchConstants.C_EXTENSION_TMP_FILE,
                                 C_SUFFIX_OUTPUT_FILE_NAME);
        }
        else
        {
            throw new IOException("Output file does not exist");
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getFileSequenceNumber = "getFileSequenceNumber";
    /**
     * Returns a file sequence number which is obtained from the database.
     * 
     * @return a file sequence number obtained from the database.
     * @throws PpasServiceException  if it failes to obtain a file sequence number.
     */
    private String getFileSequenceNumber() throws PpasServiceException
    {
        String                    l_seqNumberStr              = null;
        StringBuffer              l_tmpSeqNumber              = null;
        long                      l_seqNumber                 = 0;
        long                      l_timeout                   = 0;
        String                    l_batchJobType              = null;
        PpasDate                  l_currentDate               = null;
        PpasDailySequencesService l_ppasDailySequencesService = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10440, this,
                            BatchConstants.C_ENTERING + C_METHOD_getFileSequenceNumber);
        }

        l_currentDate  = DatePatch.getDateToday();       
        l_batchJobType = (String)super.i_params.get(BatchConstants.C_KEY_BATCH_JOB_TYPE);
        l_timeout = super.i_properties.getLongProperty(BatchConstants.C_TIMEOUT);
        l_ppasDailySequencesService = new PpasDailySequencesService(null,
                                                                    super.i_logger,
                                                                    super.i_ppasContext);
        l_seqNumber = l_ppasDailySequencesService.getNextSequenceNumber(null,
                                                                        l_timeout,
                                                                        l_batchJobType,
                                                                        l_currentDate);
        l_seqNumberStr = String.valueOf(l_seqNumber);
        l_tmpSeqNumber = new StringBuffer();
        for ( int i = 0; i < C_LENGTH_FILE_SEQUENCE_NUMBER - l_seqNumberStr.length(); i++ )
        {
            l_tmpSeqNumber.append("0");
        }
        l_tmpSeqNumber.append(l_seqNumberStr);
        l_seqNumberStr = l_tmpSeqNumber.toString();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10460, this,
                BatchConstants.C_LEAVING + C_METHOD_getFileSequenceNumber);
        }
        return l_seqNumberStr;
    }
}
