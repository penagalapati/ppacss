////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       StatusChangeBatchWriterUT.java 
//DATE            :       Sep 16, 2004
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       ATOS ORIGIN 2004
//
//DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of change>    | <reference>
//----------+--------------+----------------------------------+-----------------
// 08/05/07 | L Lundberg   | Method 'testWriteRecord()' is    | PpacLon#3033/11460
//          |              | modified in order to use the     |
//          |              | BatchJobControlData data class   |
//          |              | instead of the BatchJobData data |
//          |              | class.                           |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.StatusChangeBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.StatusChangeBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for StatusChangeBatchWriter.
 * In the property file batch_sta.properties the 
 * property com.slb.sema.ppas.batch.controlTableUpdateFrequency
 * has to be set to 1 for this test to work.
 */
public class StatusChangeBatchWriterUT extends BatchTestCaseTT
{
    /** Reference to SubInstBatch controller object. */
    private static StatusChangeBatchWriter     c_statusChangeBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static StatusChangeBatchController c_statusChangeBatchController;

    /** An input file name vid extension .DAT. */
    private static final String                C_INPUT_FILE_NAME_DAT = "INSTALL_STATUS_20040615_00003.DAT";

    /** A file name vid extension .RPT. */
    private static final String                C_FILE_NAME_RPT       = "INSTALL_STATUS_20040615_00003.RPT";

    /** A file name vid extension .RCV. */
    private static final String                C_FILE_NAME_RCV       = "INSTALL_STATUS_20040615_00003.RCV";

    /** The js job id. */
    private static final String                C_JS_JOB_ID           = "10001";

    /** The file date. */
    private static final String                C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final int                   C_SEQ_NO              = 3;

    /** The sub job counter. */
    private static final int                   C_SUB_JOB_COUNT       = -1;

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService     i_controlService      = null;

    /**
     * The constructor for the class NPCutBatchWriterUT.
     * @param p_name This test's class name.
     */
    public StatusChangeBatchWriterUT(String p_name)
    {
        super(p_name, "batch_sta");
        createContext();
    }

    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if
     * the row <code>baco_failur</code> has been updated.
     * Set the value for error line in the object <code>SubInstBatchRecordData</code> to null and updates
     * the table <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */

    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        StatusChangeBatchRecordData l_dataRecord = new StatusChangeBatchRecordData();

        BatchJobControlData l_batchJobControlData1;
        BatchJobControlData l_batchJobControlData;
        String l_expectedKey = null;
        String l_actualKey = null;
        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";
        PpasDateTime l_executionDateTime = null;
        long l_jsJobId;
        String l_actualText = null;

        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchJobControlData1 = c_statusChangeBatchController.getKeyedBatchJobControlData();

            l_executionDateTime = l_batchJobControlData1.getExecutionDateTime();
            l_jsJobId = l_batchJobControlData1.getJsJobId();
            l_batchJobControlData1.setJobType(BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE);

            l_batchJobControlData1.setStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS.charAt(0));
            l_batchJobControlData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchJobControlData1.setFileDate(new PpasDate(C_FILE_DATE, PpasDate.C_DF_yyyyMMdd));
            l_batchJobControlData1.setSeqNo(C_SEQ_NO);
            l_batchJobControlData1.setNoOfSuccessfullyRecs(0);
            l_batchJobControlData1.setNoOfRejectedRecs(0);
            l_batchJobControlData1.setOpId(c_statusChangeBatchController.getSubmitterOpid());

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchJobControlData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                c_statusChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchJobControlData = i_controlService.getJobDetails(c_ppasRequest,
                                                                       l_jsJobId,
                                                                       l_executionDateTime,
                                                                       C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = "" + l_jsJobId + l_executionDateTime + "1" + "0";

                l_actualKey = "" + l_batchJobControlData.getJsJobId() +
                             l_batchJobControlData.getExecutionDateTime() +
                             l_batchJobControlData.getNoOfRejectedRecs() +
                             l_batchJobControlData.getNoOfSuccessfullyRecs();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }

            //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RPT,
                                                     BatchConstants.C_EXTENSION_REPORT_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_REPORT_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);


            //Test when there is no error line set.
            try
            {
                l_dataRecord.setErrorLine(null);
                c_statusChangeBatchWriter.writeRecord(l_dataRecord);

                l_batchJobControlData = i_controlService.getJobDetails(c_ppasRequest,
                                                                       l_jsJobId,
                                                                       l_executionDateTime,
                                                                       C_TIMEOUT);

                l_actualKey = "" + l_batchJobControlData.getJsJobId() +
                l_batchJobControlData.getExecutionDateTime() +
                l_batchJobControlData.getNoOfRejectedRecs() +
                l_batchJobControlData.getNoOfSuccessfullyRecs();

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 1.
                l_expectedKey = "" + l_jsJobId + l_executionDateTime + 1 + 1;

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);

            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
  

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(StatusChangeBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to run all the tests in this
     * class.
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /**
     * Create context neccessary for this test.
     */
    private void createContext()
    {
        NPCutBatchRecordData l_batchRecordData = new NPCutBatchRecordData();
        Hashtable l_params = new Hashtable();
        SizedQueue l_outQueue = null;
        String l_dir = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String l_fileReportName = l_dir + "/" + C_FILE_NAME_RPT;
        String l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String l_fileRecoveryName = l_dir2 + "/" + C_FILE_NAME_RCV;
        
        //The update frequency needs to be 1 for the test to work.
        super.c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");

        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_batchRecordData);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }

        //Make sure that there are no files with the same name.
        super.deleteFile(l_fileReportName);
        super.deleteFile(l_fileRecoveryName);

        l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_FILE_NAME_DAT);

        try
        {
            c_statusChangeBatchController = 
                new StatusChangeBatchController(super.c_ppasContext,
                                                BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                                C_JS_JOB_ID,
                                                "StatusChangeBatchWriterUT",
                                                l_params);

            c_statusChangeBatchWriter = new StatusChangeBatchWriter(super.c_ppasContext,
                                                                    super.c_logger,
                                                                    c_statusChangeBatchController,
                                                                    l_params,
                                                                    l_outQueue,
                                                                    super.c_properties);
        }
        catch (PpasConfigException e)
        {
            super.failedTestException(e);
        }
    }
}
