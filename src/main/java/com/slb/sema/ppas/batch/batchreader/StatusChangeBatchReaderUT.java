////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchReaderUT
//      DATE            :       18-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class is the UT-test for StatusChangeBatchReader
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit Test for the 'SubInstBatchProcessor' class.
 */
public class StatusChangeBatchReaderUT extends BatchTestCaseTT
{

    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    
    /** The name of the indata file used in normal mode. */
    private static final String            C_INDATA_FILENAME_NORMAL_MODE   = 
        "INSTALL_STATUS_20040615_00004.DAT";

    /** The name of the indata file used in recovery mode. */
    private static final String            C_INDATA_FILENAME_RECOVERY_MODE = 
        "INSTALL_STATUS_20040615_00004.IPG";

    /** Error code. Value is {@value}. */
    private static final String            C_ERROR_INVALID_RECORD          = "01";

    /** Error code. Value is {@value}. */
    private static final String            C_ERROR_INVALID_STATUS          = "03";

    /** The name of the in-queue. */
    private static final String            C_QUEUE_NAME                    = "In-queue";

    /** The maximum size of the queue. */
    private static final long              C_MAX_QUEUE_SIZE                = 100;

    /** A reference to the <code>SCChangeBatchController</code> that is used in this test. */
    private static BatchController         c_batchController               = null;

    /** A reference to the <code>SCChangeBatchReader</code> that is used in this test. */
    private static StatusChangeBatchReader c_scChangeBatchReader           = null;

    /** The record line in a indata file. */
    private static final String            C_NORMAL_RECORD_LINE            = "     0208000001A";

    /** The missing Msisdn record line in a indata file. */
    private static final String            C_MISSING_MSISDN_LINE           = "               A";

    /** The missing status line in a indata file. */
    private static final String            C_MISSING_STATUS_LINE           = "     0208000001 ";

    /** The to long line line in a indata file. */
    private static final String            C_TO_LONG_LINE                  = "     0208000001AA";

    /** The a invlaid status in record line in a indata file. */
    private static final String            C_INVALID_STATUS_LINE           = "     0208000001Q";
               
    /**
     * Class constructor.
     * @param p_name the name of this test. 
     */
    public StatusChangeBatchReaderUT(String p_name)
    {
        super(p_name, "batch_sta");
    }  
    
    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(StatusChangeBatchReaderUT.class);    
    }

    /** Test if a file name is valid. */
    public void testIsFileNameValid()
    {       
        super.beginOfTest("testIsFileNameValidDat");
        
        boolean l_valid = false;
                      
        l_valid = c_scChangeBatchReader.isFileNameValid(C_INDATA_FILENAME_NORMAL_MODE);    
        assertTrue(C_INDATA_FILENAME_NORMAL_MODE + " is not a valid filename ", l_valid);
        
        l_valid = c_scChangeBatchReader.isFileNameValid(C_INDATA_FILENAME_RECOVERY_MODE);
        assertTrue(C_INDATA_FILENAME_RECOVERY_MODE + " is not a valid filename ", l_valid);  
        
        super.endOfTest();
        
    }
    
    /** Test to get a record. Has to be before testRecoveryBatchMode(). */   
    public void testGetRecord()
    {
        super.beginOfTest("testGetRecord");
        BatchRecordData l_recordData = null;
        String l_wrongRecordFormat = C_ERROR_INVALID_RECORD + BatchConstants.C_DELIMITER_REPORT_FIELDS;
        String l_invalidStatusError = C_ERROR_INVALID_STATUS + BatchConstants.C_DELIMITER_REPORT_FIELDS;

        //Test a normal input line.
        l_recordData = c_scChangeBatchReader.getRecord();

        assertEquals("The input line in the record wrong!",
                     C_NORMAL_RECORD_LINE, l_recordData.getInputLine());

        // TODO Make the code active when the checks is implemetneted in ChangeBatchReader. 
        //Test when the Msisdn is missing in the input line.
        l_recordData = c_scChangeBatchReader.getRecord();
        assertEquals("No error reported for missing Msisdn in the input line!",
                     l_wrongRecordFormat + C_MISSING_MSISDN_LINE,
                     l_recordData.getErrorLine());

        //Test when status is missing in the input line.
        l_recordData = c_scChangeBatchReader.getRecord();

        assertEquals("No error reported for missing status in the input line!",
                     l_invalidStatusError + C_MISSING_STATUS_LINE,
                     l_recordData.getErrorLine());
        
        //Test when status is invalid in the input line.
        l_recordData = c_scChangeBatchReader.getRecord();
        assertEquals("No error reported for invalid status in the input line!",
                     l_invalidStatusError + C_INVALID_STATUS_LINE,
                     l_recordData.getErrorLine());

//        //Test a to long input line.
        l_recordData = c_scChangeBatchReader.getRecord();

        assertEquals("No error reported that the input line was to long!", 
                     l_wrongRecordFormat + C_TO_LONG_LINE, 
                     l_recordData.getErrorLine());

        super.endOfTest();
    }
    
    /** Perform standard setUp stuff. */
    protected void setUp()
    {
        super.setUp(CommonTestCaseTT.C_FLAG_WITHOUT_JDBC_CONNECTION);
        this.createContext();
    }


    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }
       
    /**
     * Creates this tests context.
     */
    private void createContext()
    {
        StringBuffer l_tmpFullPathName  = null;
        Hashtable l_parameters          = new Hashtable();
        SizedQueue l_queue              = null;
        String l_inputFileDirectory     = null;
        String l_fullPathName           = null;
        String [] l_linearray           = {C_NORMAL_RECORD_LINE, 
                                           C_MISSING_MSISDN_LINE, 
                                           C_MISSING_STATUS_LINE,
                                           C_INVALID_STATUS_LINE,
                                           C_TO_LONG_LINE};
        
//        String[] l_linearray = {C_NORMAL_RECORD_LINE, C_INVALID_STATUS_LINE, C_TO_LONG_LINE};
        
        if (c_scChangeBatchReader == null)
        {
            l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INDATA_FILENAME_NORMAL_MODE);

            l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

            //Delete the recovery file (.IPG) in the directory.
            l_tmpFullPathName = new StringBuffer();
            l_tmpFullPathName.append(l_inputFileDirectory);
            l_tmpFullPathName.append("/");
            l_tmpFullPathName.append(C_INDATA_FILENAME_RECOVERY_MODE);
            l_fullPathName = l_tmpFullPathName.toString();
            super.deleteFile(l_fullPathName);

            //Create a input file (.DAT) in the directory.
            l_tmpFullPathName = new StringBuffer();
            l_tmpFullPathName.append(l_inputFileDirectory);
            l_tmpFullPathName.append("/");
            l_tmpFullPathName.append(C_INDATA_FILENAME_NORMAL_MODE);
            l_fullPathName = l_tmpFullPathName.toString();

            super.createNewFile(l_fullPathName, l_linearray);

            try
            {
                l_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null);
            }
            catch (SizedQueueInvalidParameterException e)
            {
                super.failedTestException(e);
            }

            c_scChangeBatchReader = new StatusChangeBatchReader(super.c_ppasContext,
                                                                super.c_logger,
                                                                c_batchController,
                                                                l_queue,
                                                                l_parameters,
                                                                super.c_properties);

            assertNotNull("Failed to create a SCChangeBatchReader instance.", c_scChangeBatchReader);

            try
            {

                Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
            }
            catch (InterruptedException e)
            {
                /** Do nothing. */
            }
        }
    }
 
    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        TestRunner.run(suite());
    }

}
