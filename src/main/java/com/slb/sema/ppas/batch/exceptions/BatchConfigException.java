////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchConfigException.java
//      DATE            :       06-Oct-2005
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       A BatchConfigException is created and thrown if a
//                              configuration problem is discovered when running a
//                              batch job, for instance if a mandatory input property
//                              or parameter is missing or has an illegal value.
//                              
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.exceptions;

import com.slb.sema.ppas.common.support.PpasRequest;

/**
 * A <code>BatchConfigException</code> instance is created and thrown if a configuration
 * problem is discovered when running a batch job, for instance if a mandatory
 * input property or parameter is missing or has an illegal value.
 */
public class BatchConfigException extends BatchException
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchInvalidParameterException";


    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructs a <code>BatchConfigException</code> instance using the given parameters.
     * 
     * @param p_callingClass   The name of the class invoking this constructor.
     * @param p_callingMethod  The name of the method from which this constructor was invoked.
     * @param p_stmtNo         Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject  The object from which this constructor was 
     *                         invoked ... null if the constructor was invoked from a static method.
     * @param p_request        The Request from which this object was created.
     * @param p_flags          Bitmask flag ... if bit 0 is set, then a stack
     *                         trace will be output when some exception objects are logged. 
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     */
    public BatchConfigException(String                     p_callingClass,
                                String                     p_callingMethod,
                                int                        p_stmtNo,
                                Object                     p_callingObject,
                                PpasRequest                p_request,
                                long                       p_flags,
                                BatchKey.BatchExceptionKey p_exceptionKey)
    {
        super(p_callingClass, p_callingMethod, p_stmtNo, p_callingObject, p_request, p_flags, p_exceptionKey);
    }


    /**
     * Constructs a <code>BatchConfigException</code> instance using the given parameters.
     * 
     * @param p_callingClass    The name of the class invoking this constructor.
     * @param p_callingMethod   The name of the method from which this constructor was invoked.
     * @param p_stmtNo          Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject   The object from which this constructor was 
     *                          invoked ... null if the constructor was invoked from a static method.
     * @param p_request         The Request from which this object was created.
     * @param p_flags           Bitmask flag ... if bit 0 is set, then a stack
     *                          trace will be output when some exception objects are logged. 
     * @param p_exceptionKey    Key defining the look-up key and any associated parameters.
     * @param p_sourceException An earlier exception which gave rise to this exception.
     */
    public BatchConfigException(String                     p_callingClass,
                                String                     p_callingMethod,
                                int                        p_stmtNo,
                                Object                     p_callingObject,
                                PpasRequest                p_request,
                                long                       p_flags,
                                BatchKey.BatchExceptionKey p_exceptionKey,
                                Exception                  p_sourceException)
    {
        super(p_callingClass,
              p_callingMethod,
              p_stmtNo,
              p_callingObject,
              p_request,
              p_flags,
              p_exceptionKey,
              p_sourceException);
    }
}
