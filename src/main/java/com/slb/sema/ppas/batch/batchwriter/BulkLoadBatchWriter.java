////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BulkLoadBatchWriter
//      DATE            :       27-July-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 10/09/07 | K Bond        | Update the success / failure     | PpacLon#3112/12037
//          |               | counters.                        |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.util.Map;
//import java.util.NoSuchElementException;


import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BulkLoadBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AFBulkLoadBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
//import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for doing the final processing
 * off a bulk load of a account finder.
 * If configured an output file is written containing all
 * subscribers inserted into the account found.
 */
public class BulkLoadBatchWriter extends BatchWriter
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                                        = "BulkLoadBatchWriter";
    
    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /**Gets how many records to be written. Gets from <code>p_params</code>. */
    private int    i_interval              = 0;
    
    /** Counter for number of precessed records. */
    private int    i_processed             = 0;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * @param p_ppasContext the reference to PpasContext.
     * @param p_logger      the reference to Logger.
     * @param p_controller  the callback reference to BatchController.
     * @param p_params      Start parameters.
     * @param p_outQueue    Output queue.
     * @param p_properties  The properties defined for this batch.
     */
    public BulkLoadBatchWriter(
        PpasContext             p_ppasContext,
        Logger                  p_logger,
        BulkLoadBatchController p_controller,
        Map                     p_params,
        SizedQueue              p_outQueue,
        PpasProperties          p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        getInterval();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException An error was raised during update of Information data.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        String l_subJobId = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        AFBulkLoadBatchRecordData l_record = (AFBulkLoadBatchRecordData)p_record;
        
        i_processed++;
        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }
          
        if (PpasDebug.on)
        {
            l_subJobId = (String)i_params.get(BatchConstants.C_KEY_SUB_JOB_ID);
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10166,
                this,
                "Processed MSISDN: " + l_record.getMsisdn() +
                " By subJobId: " + l_subJobId);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10370,
                this,
                BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }
        
        return;
    } // End of writeRecord(.)

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws IOException, PpasServiceException
    {
        BatchSubJobData l_jobData;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11065,
                            this,
                            "Enter " + C_METHOD_updateStatus);
        }

        super.flushFiles();
        l_jobData = ((BulkLoadBatchController)super.i_controller).getKeyedControlRecord();
        updateSubJobRecordCounters(null,
                Integer.parseInt(l_jobData.getMasterJsJobId()),
                new PpasDateTime(l_jobData.getExecutionDateTime()),
                i_processed,
                0);
        
//        try
//        {
//            l_jobData.setLastProcessedCustId(super.i_controller.getLowestCustomerInProgress());
//        }
//        catch (NoSuchElementException p_noSuchElementEx)
//        {
            // This is probably the final update and no more cust id has been processed.
            // Update the 'last processed cust id' by using the saved last cust id.
            l_jobData.setLastProcessedCustId("-1");
//        }

        super.updateControlInfo(l_jobData);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11066,
                            this,
                            "Leaving " + C_METHOD_updateStatus);
        }


    }

    /**
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        //This batch does not write out trailer records        
    }

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getInterval = "getInterval";

    /**
     * Returns the interval at which the status is updated.
     * @return Whether the interval was found.
     */
    private boolean getInterval()
    {
        String  l_interval    = null;
        boolean l_returnValue = true;
        

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11500, this,
                "Entered " + C_METHOD_getInterval);
        }

        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10390,
                    this,
                    C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY +
                    " " + C_METHOD_getInterval );
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11590, this,
                "Leaving " + C_METHOD_getInterval);
        }

        return l_returnValue;
    } // end of getInterval


//    /** Method name used for calls to Middleware. Value is {@value}. */
//    private static final String C_METHOD_getOutputFileName = "getOutputFileName";
//
//    /**
//     * Gets the name of the output file.
//     * @param p_params Map of parameters.
//     * @return Full output file name, null if cannot be found.
//     */
//    // =========================================    
//    // AT THE MOMENT THIS METHOD IS NEVER USED!!
//    // =========================================    
//    private String getOutputFileName(
//        Map                           p_params)
//    {
//        String       l_outputFileFullPath = null;
//        String       l_outputFileDirectory = null;
//        StringBuffer l_tmp                = null;
//        String       l_fileSequenceNumber = null;
//        StringBuffer l_tmpSequenceNumber  = null;
//        
//        if (PpasDebug.on)
//        {
//            PpasDebug.print(
//                PpasDebug.C_LVL_VHIGH,
//                PpasDebug.C_APP_SERVICE,
//                PpasDebug.C_ST_START,
//                C_CLASS_NAME, 11600, this,
//                "Entered " + C_METHOD_getOutputFileName);
//        }
//
//        l_fileSequenceNumber = (String)p_params.get(BatchConstants.C_KEY_SUB_JOB_ID);
//        if ( l_fileSequenceNumber != null )
//        {
//            if (PpasDebug.on)
//            {
//                PpasDebug.print(
//                    PpasDebug.C_LVL_VLOW,
//                    PpasDebug.C_APP_SERVICE,
//                    PpasDebug.C_ST_TRACE,
//                    C_CLASS_NAME,
//                    10300,
//                    this,
//                    C_CLASS_NAME + " Temporary solution, using subJobId as file sequence number: "+l_fileSequenceNumber );
//            }
//            l_tmpSequenceNumber = new StringBuffer();
//            for ( int i = 0; i < C_LENGTH_FILE_SEQUENCE_NUMBER - l_fileSequenceNumber.length(); i++ )
//            {
//                l_tmpSequenceNumber.append("0");
//            }
//            l_tmpSequenceNumber.append(l_fileSequenceNumber);
//            l_fileSequenceNumber = l_tmpSequenceNumber.toString();
//        }
//        else
//        {
//            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
//            i_logger.logMessage( new LoggableEvent( "CAnnot read subJobId from Map",
//                                 LoggableInterface.C_SEVERITY_ERROR) );
//            if (PpasDebug.on)
//            {
//                PpasDebug.print(
//                    PpasDebug.C_LVL_VLOW,
//                    PpasDebug.C_APP_SERVICE,
//                    PpasDebug.C_ST_ERROR,
//                    C_CLASS_NAME,
//                    10330,
//                    this,
//                    C_CANNOT_OPEN_OUTPUT_FILE + " " + C_METHOD_getOutputFileName );
//            }
//        }
//
//        // Get the output file directory
//        l_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
//        if ( l_outputFileDirectory == null )
//        {
//            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
//            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
//                                 LoggableInterface.C_SEVERITY_ERROR) );
//            if (PpasDebug.on)
//            {
//                PpasDebug.print(
//                    PpasDebug.C_LVL_VLOW,
//                    PpasDebug.C_APP_SERVICE,
//                    PpasDebug.C_ST_ERROR,
//                    C_CLASS_NAME,
//                    10420,
//                    this,
//                    C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getOutputFileName );
//            }
//        }
//                
//        if ( l_fileSequenceNumber != null && l_outputFileDirectory != null )
//        {
//            //Get the full path filename for the error-file 
//            l_tmp = new StringBuffer();
//            l_tmp.append(l_outputFileDirectory);
//            l_tmp.append("/");
//            l_tmp.append(C_PREFIX_OUTPUT_FILE_NAME);        
//            l_tmp.append(DatePatch.getDateTimeNow().toString_yyyyMMdd());
//            l_tmp.append("_");
//            l_tmp.append(l_fileSequenceNumber );
//
//            l_outputFileFullPath = l_tmp.toString();
//        }
//
//        if (PpasDebug.on)
//        {
//            PpasDebug.print(
//                PpasDebug.C_LVL_VHIGH,
//                PpasDebug.C_APP_SERVICE,
//                PpasDebug.C_ST_END,
//                C_CLASS_NAME, 11690, this,
//                "Leaving " + C_METHOD_getOutputFileName);
//        }
//
//        return l_outputFileFullPath;
//    } // end of getOutputFileName


//    /** Method name used for calls to Middleware. Value is {@value}. */
//    private static final String C_METHOD_openOutputFile = "openOutputFile";
//
//    /**
//     * Opens the output file.
//     * @param p_outputFileName Name of output file.
//     */
//    // =========================================    
//    // AT THE MOMENT THIS METHOD IS NEVER USED!!
//    // =========================================    
//    private void openOutputFile(
//        String                        p_outputFileName)
//    {
//        if (PpasDebug.on)
//        {
//            PpasDebug.print(
//                PpasDebug.C_LVL_VHIGH,
//                PpasDebug.C_APP_SERVICE,
//                PpasDebug.C_ST_START,
//                C_CLASS_NAME, 11700, this,
//                "Entered " + C_METHOD_openOutputFile);
//        }
//
//        try
//        {
//            super.openFile(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN, p_outputFileName);
//        }
//        catch (IOException e)
//        {
//            
//            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
//            i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_OUTPUT_FILE,
//                                 LoggableInterface.C_SEVERITY_ERROR) );
//            if (PpasDebug.on)
//            {
//                PpasDebug.print(
//                    PpasDebug.C_LVL_VLOW,
//                    PpasDebug.C_APP_SERVICE,
//                    PpasDebug.C_ST_ERROR,
//                    C_CLASS_NAME,
//                    10330,
//                    this,
//                C_CANNOT_OPEN_OUTPUT_FILE + " " + C_METHOD_openOutputFile );
//            }
//            e.printStackTrace();
//        }
//
//        if (PpasDebug.on)
//        {
//            PpasDebug.print(
//                PpasDebug.C_LVL_VHIGH,
//                PpasDebug.C_APP_SERVICE,
//                PpasDebug.C_ST_END,
//                C_CLASS_NAME, 11790, this,
//                "Leaving " + C_METHOD_openOutputFile);
//        }
//
//    } // end of openOutputFile
}
