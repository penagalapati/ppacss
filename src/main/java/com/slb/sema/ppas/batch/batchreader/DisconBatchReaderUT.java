////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DisconBatchReaderUT
//      DATE            :       21-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       This class is the test program for DisconBatchReader
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.MeterInstrument;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Class to unit test the Disconnection batch.
 */
public class DisconBatchReaderUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** Class name. Value is {@value}. */
    private static final String C_CLASS_NAME = "DisconBatchProcessorUT";

    /** Normal batch mode. */
    private static final boolean C_NORMAL_MODE        = true;
    /** Queue name. Value is {@value}. */
    private static final String  C_QUEUE_NAME         = "In-queue";
    /** Queue size. Value is {@value}. */
    private static final long    C_MAX_QUEUE_SIZE     = 100;
    /** Start range. Value is {@value}. */
    private static final String  C_START_RANGE        = "10";
    /** End range. Value is {@value}. */
    private static final String  C_END_RANGE          = "30";
    /** Disconnection date. Value is {@value}. */
    private static final String  C_DISCONNECTION_DATE = "20040701";
    /** Execution date - current date and time. */
    private static final String  C_EXECUTION_DATE_TIME = DatePatch.getDateTimeNow().toString_yyyyMMddHHmmss();

    /** Reference to a BatchController. */
    BatchController             i_controller  = null;
    /** Reference to start parameters. */
    Hashtable                   i_parameters  = null;
    /** Reference to defined properties. */
    PpasProperties              i_properties  = null;
    /** In-queue. */
    SizedQueue                  i_queue       = null;
    /** Reference to a read record. */
    DisconBatchRecordData       i_record      = null;
    /** Reference to the BatchReader. */
    BatchReader                 i_batchReader = null;



    /**
     * Class constructor.
     * @param p_name the name of this unit test class
     */
    public DisconBatchReaderUT(String p_name)
    {
        super( p_name );
        //super.enableConsoleLogAll();
    }

    /**
     * @see com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT#setUp()
     */
    protected void setUp()
    {
//        super.setUp(CommonTestCaseTT.C_FLAG_WITHOUT_JDBC_CONNECTION);
    }


    /**
     * @see com.slb.sema.ppas.common.support.CommonTestCaseTT#tearDown()
     */
    protected void tearDown()
    {
        super.tearDown();
    }


    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(DisconBatchReaderUT.class);    
    }




    /** Tests the batch in Normal mode. */
    public final void testNormalBatchMode()
    {
        say("******* NORMAL BATCH MODE ******");
        say("================================\n");
    
        doTestStart(C_NORMAL_MODE);
    
        say("\n******* END OF NORMAL BATCH MODE TEST******");
        say("===========================================\n");

    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testStart = "testStart";
    /**
     * @param p_recovery Recovery mode.
     */
    private void doTestStart(boolean p_recovery)
    {
        InstrumentSet               l_instrumentSet  = null;
        CounterInstrument           l_queuedCounter  = null;
        CounterInstrument           l_removedCounter = null;
        MeterInstrument             l_sizeMeter      = null;
        DisconBatchRecordData       l_record         = null;
        int                         l_recordCounter  = 0;
    
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_testStart );
        }

        say("*** START OF "+ C_METHOD_testStart + " ***");
  
        // Create variables needed to create a SubInstBatchReader
        this.createContext(p_recovery);       
    
        i_batchReader = new DisconBatchReader(super.c_ppasContext,
                                              super.c_logger,
                                              i_controller,
                                              i_queue,
                                              i_parameters,
                                              super.c_properties);
                                   
        assertNotNull("Failed to create a DisconBatchProcessor instance.", i_batchReader);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "OK - Just created an DisconBatchReader" );
        }



        i_batchReader.start();
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "After start() ...." );
        }

        try
        {
            say(C_CLASS_NAME + C_METHOD_testStart + "-- Wait for " + 
                    BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT + " secs...");
            Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
        }
        catch (InterruptedException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10400,
                    this,
                    "Something wrong in waiting.. " );
            }
        
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "After wait... - before getInstrument" );
        }

        l_instrumentSet = i_queue.getInstrumentSet();
        assertNotNull("Failed to get the InstrumentSet from the SizedQueue.", l_instrumentSet);

        l_queuedCounter  = (CounterInstrument)l_instrumentSet.getInstrument("Queued counter");
    

        l_removedCounter = (CounterInstrument)l_instrumentSet.getInstrument("Removed counter");


        l_sizeMeter      = (MeterInstrument)l_instrumentSet.getInstrument("Queue size");
    
   
        if ( l_queuedCounter != null )
        {
            assertEquals("Wrong number of elements added to the outdata queue (SizedQueue)."    ,
                         4, l_queuedCounter.getValue());
        }
        if ( l_removedCounter != null )
        {
            assertEquals("Wrong number of elements removed from the outdata queue (SizedQueue).",
                         0, l_removedCounter.getValue());
        }
        if ( l_sizeMeter != null )
        {
            assertEquals("Wrong size of the outdata queue (SizedQueue)."                        ,
                         4, l_sizeMeter.getValue());
        }

        // Loop through the queue and write the records from the queue  
        while ( !i_queue.isEmpty() ) 
        {  
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10400,
                    this,
                    "Something in the queue..." );
            }        
            try
            {
                l_recordCounter++;
                l_record = (DisconBatchRecordData)i_queue.removeFirst();
                say( "Record number : " + l_recordCounter + "\n" + l_record.dumpRecord() );
            
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        "record number: "+l_recordCounter + l_record.dumpRecord() );
                }        
             
            }
            catch (SizedQueueClosedForWritingException e1)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        "Could not get record number: "+l_recordCounter );
                }        
                e1.printStackTrace();
            }
        
        }

    
        say("*** END of method " + C_METHOD_testStart + " ***");
    
        say("Number of tests : "+this.countTestCases());
    
        System.gc();
        say(C_CLASS_NAME + C_METHOD_testStart + "-- Completed.");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_testStart );
        }

        return;
    }


    /**
     * @param p_recovery Recovery mode.
     */
    private void createContext( boolean p_recovery )
    {
        say("start createContext");

        try
        {
            i_parameters = new Hashtable();
            i_parameters.put(BatchConstants.C_KEY_START_RANGE,         C_START_RANGE );
            i_parameters.put(BatchConstants.C_KEY_END_RANGE,           C_END_RANGE );
            i_parameters.put(BatchConstants.C_KEY_DISCONNECTION_DATE,  C_DISCONNECTION_DATE );
            i_parameters.put(BatchConstants.C_KEY_EXECUTION_DATE_TIME, C_EXECUTION_DATE_TIME );

            if ( p_recovery )
            {
                i_parameters.put(BatchConstants.C_KEY_RECOVERY_FLAG, "Yes" );
            }
            else
            {
                i_parameters.put(BatchConstants.C_KEY_RECOVERY_FLAG, "No" );                    
            }

            i_properties = new PpasProperties((InstrumentManager) null);
            i_properties.loadLayeredProperties();

            try
            {
                i_queue = new SizedQueue( C_QUEUE_NAME,
                                          C_MAX_QUEUE_SIZE,
                                          null );
            }
            catch (SizedQueueInvalidParameterException e1)
            {
                say("FAILED to create SizedQUEUE");
                e1.printStackTrace();
            }
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }
    
        say("end createContext");

    }


    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        // Nothing ...
    }


}
