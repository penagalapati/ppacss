////////////////////////////////////////////////////////////////////////////////
//     ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//     FILE NAME       :       MsisdnRoutingProvisioningBatchWriter.java
//     DATE            :       Oct 26, 2004
//     AUTHOR          :       Lars Lundberg
//     REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//     COPYRIGHT       :       Atos Origin 2004
//
//     DESCRIPTION     :       See Javadoc
//
////////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
// ---------+-----------+--------------------------------------+-----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.MsisdnRoutingProvisioningBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.MsisdnRoutingBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class creates output files for the Msisdn Routing Provisioning.
 */
public class MsisdnRoutingProvisioningBatchWriter extends BatchWriter
{
    //--------------------------------------------------------------------------
    //--  Class level constant.                                               --
    //--------------------------------------------------------------------------

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                   = "MsisdnRoutingProvisioningBatchWriter";


    //--------------------------------------------------------------------------
    //--  Instance variables.                                                 --
    //--------------------------------------------------------------------------

    /** Directory for the report file. Defined by a property. */
    private String i_reportFileDirectory   = null;

    /** The file and database update interval. Defined by a property. */
    private int    i_interval              = 0;

    /** Input file name. */
    private String i_inputFilename         = null;

    /** The number of records processed with success. */
    private int    i_success               = 0;

    /** The total number of processed records. */
    private int    i_processed             = 0;    

    //--------------------------------------------------------------------------
    //--  Constructors.                                                       --
    //--------------------------------------------------------------------------

    /**
     * Constructs a StatusChangeBatchWriter object.
     * @param p_ppasContext A PPpsCOntext
     * @param p_logger      The logger used of the writer.
     * @param p_controller  The batch jobs batch controller. 
     * @param p_parameters  Holding parameters used of the writer. 
     * @param p_outQueue    The batch jobs out queue.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public MsisdnRoutingProvisioningBatchWriter(PpasContext     p_ppasContext,
                                                Logger          p_logger,
                                                BatchController p_controller,
                                                Map             p_parameters,
                                                SizedQueue      p_outQueue,
                                                PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_parameters, p_outQueue, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10300,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        // Init. this instance.
        initialize();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

        return;
        
    }


    //--------------------------------------------------------------------------
    //--  Protected instance methods.                                         --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Writes to the output files.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }


        MsisdnRoutingBatchRecordData l_record = (MsisdnRoutingBatchRecordData)p_record;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            13333,
                            this,
                            "in writeRecord: " + l_record.dumpRecord());
        }
        
        //Check if report file info exist and write it to file.
        if (l_record.getErrorLine() != null)
        {
            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine() );
            super.i_errors++;
        }
        else
        {
            i_success++;
        }
        
        i_processed++;

        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }
          
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_writeRecord);
        }
    } // End of writeRecord(.)


    /**
     * Prints the trailer record into the report file.
     * 
     * @param p_outcome  Whether the batch FAILED (could not reach the end of the file) or SUCCESS.
     * 
     * @throws IOException If it is not possible to write to the file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_success);

        l_count = l_count.substring(
                l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }


    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * 
     * @throws IOException           if it is not possible to write to the file.
     * @throws PpasServiceException  no specific keys are anticipated.
     */
    protected void updateStatus() throws IOException, PpasServiceException
    {
        BatchJobData l_jobData = null;
        super.flushFiles();
        l_jobData = ((MsisdnRoutingProvisioningBatchController)super.i_controller).getKeyedControlRecord();
        l_jobData.setNoOfRejectedRec(new Integer(super.i_errors).toString());
        l_jobData.setNoOfSuccessRec(new Integer(i_success).toString());
        
        super.updateControlInfo(l_jobData);
    }


    //--------------------------------------------------------------------------
    //--  Private instance methods.                                           --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_initialize = "initialize";    
    /**
     * Initializes this <code>MsisdnRoutingProvisioningBatchWriter</code> instance.
     * 
     * @return  <code>true</code> if this instance is successfully initialized, otherwise
     *          it returns <code>false</code>.
     */
    private boolean initialize()
    {
        boolean l_success = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_initialize);
        }

        if (getParameters() &&      // Get the batch parameters.
            getProperties() &&      // Get the batch properties.
            openReportFile() )      // Open the report file.
        {
            l_success = true;
        }
        else
        {
            l_success = false;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_initialize);
        }
        return l_success;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getParameters = "getParameters";    
    /**
     * Extracts the parameters from the <code>Map</code> object passed as one of the Constructor parameters.
     * The extracted parameter is the 'input filename'.
     * 
     * @return  <code>true</code> if all parameters are successfully extraced, otherwise it returns
     *          <code>false</code>.
     */
    private boolean getParameters()
    {
        boolean l_retVal = true;
        
        // Get the 'input filename' parameter.
        i_inputFilename = (String)this.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
        if (i_inputFilename == null)
        {
            // The 'input filename' parameter is missing.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10380,
                                this,
                                C_METHOD_getParameters + " -- The 'input filename' parameter, " +
                                BatchConstants.C_KEY_INPUT_FILENAME + ", is missing.");
            }

            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage(new LoggableEvent("The 'input filename' parameter, " +
                                                  BatchConstants.C_KEY_INPUT_FILENAME + ", is missing.",
                                                  LoggableInterface.C_SEVERITY_ERROR));
            l_retVal = false;
        }

        return l_retVal;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * Retrieves the properties given in the loaded property files.
     * The retrieved properties are: 
     * <br> the 'file and database update interval'
     * <br> the 'recovery file directory'
     * <br> the 'report file directory'
     * 
     * @return  <code>true</code> if all properties are retrieved successfully, otherwise it returns
     *          <code>false</code>.
     */
    private boolean getProperties()
    {
        String  l_intervalProp = null;
        boolean l_returnValue  = true;  // Assume all found
        

        // Get the 'file and database update interval' property.
        l_intervalProp = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_intervalProp != null )
        {
            i_interval = Integer.parseInt(l_intervalProp);
        }
        else
        {
            // The 'file and database update interval' property is missing.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10390,
                                this,
                                C_METHOD_getProperties + 
                                " -- The 'file and database update interval' property, " +
                                BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY +
                                ", is missing." );
            }
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent("The 'file and database update interval' property, " +
                                                   BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY +
                                                   ", is missing.",
                                                   LoggableInterface.C_SEVERITY_ERROR));
            l_returnValue = false;
        }

        // Get the 'report file directory'.
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        if ( i_reportFileDirectory == null )
        {
            // The 'report file directory' property is missing.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10390,
                                this,
                                C_METHOD_getProperties + 
                                " -- The 'report file directory' property, " +
                                BatchConstants.C_REPORT_FILE_DIRECTORY +
                                ", is missing." );
            }
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent("The 'report file directory' property, " +
                                                   BatchConstants.C_REPORT_FILE_DIRECTORY +
                                                   ", is missing.",
                                                   LoggableInterface.C_SEVERITY_ERROR));
            l_returnValue = false;
        }
                
        return l_returnValue;
        
    } // End of method getProperties()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_openReportFile = "openReportFile";    
    /**
     * Opens the report file.
     * 
     * @return  <code>true</code> if the report file is opened successfully, otherwise it returns
     *          <code>false</code>.
     */
    private boolean openReportFile()
    {
        boolean l_success            = true;
        String  l_reportFileFullPath = null;

        // Get the full path filename for the report-file
        l_reportFileFullPath = super.generateFileName(i_reportFileDirectory,
                                                      i_inputFilename,
                                                      BatchConstants.C_EXTENSION_TMP_FILE);

        // Open the report file.
        try
        {
            super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFileFullPath));
        }
        catch (IOException e)
        {
            // Failed to open the report file.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10330,
                                this,
                                C_METHOD_openReportFile + " -- Failed to open the report file '" +
                                l_reportFileFullPath + "'.");
            }
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent("Failed to open the report file '" +
                                                   l_reportFileFullPath + "'.",
                                                   LoggableInterface.C_SEVERITY_ERROR));
            l_success = false;
        }
        
        return l_success;
    }
}
