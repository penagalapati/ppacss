////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchController
//      DATE            :       23-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       This class is the entry point for Miscellaneous 
//                              Data Batch and will be initated by either the Job 
//                              Scheduler or via its own bootstrap main. It is 
//                              responsible to create and start all underlying 
//                              components and will also be used for "call-back" 
//                              to the initiating client. It extends the generic 
//                              batch controller class BatchController and contains
//                              no specific logic.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars Lundberg | Extends the super class          | PpacLon#3033/11279
//          |               | FileDrivenBatchController instead|
//          |               | of  the BatchController class.   |
//          |               | Added method:                    |
//          |               | getValidFilenameRegEx()          |
//          |               | Removed methods:                 |
//          |               | addControlInformation()          |
//          |               | getKeyedControlRecord()          |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.MiscBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.MiscBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.MiscBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class is the entry point for Miscellaneous Data Batch. 
 */
public class MiscBatchController extends FileDrivenBatchController
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                    = "MiscBatchController";

    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS           = "batch_mdu";

    
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a new MiscBatchController object.
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchInstall).
     * @param p_jobId       The unique id for this job.
     * @param p_jobName     The name of this job.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasException - If configuration data is missing or incomplete.
     * @throws IOException - If problem locating or loading properties file.
     */
    public MiscBatchController(PpasContext p_context,
                               String      p_jobType,
                               String      p_jobId,
                               String      p_jobName,
                               Map         p_params) throws PpasException
//                               , IOException
    {
        super(p_context, p_jobType, p_jobId, p_jobName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10100, this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        this.init();

        i_executionDateTime = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10200, this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } // End of 'MiscBatchController' Constructor
    

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 82601, this,
                            BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 82602, this,
                            BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }


    /**
     * Returns the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected String getBatchJobType()
    {
        return BatchConstants.C_JOB_TYPE_BATCH_MISC;
    }


    /**
     * This method should return a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected String getValidFilenameRegEx()
    {
        return "UPDATE_MISC_" + C_REG_EX_VALID_DEFAULT_FILENAME;


    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";        
    /**
     * This method will instantiate the correct reader-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the reader-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a MiscBatchReader.
     */
    public BatchReader createReader()
    {
        MiscBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10300, this,
                            BatchConstants.C_ENTERING + C_METHOD_createReader);
        }

        if (super.i_startComponents)
        {
            l_reader = new MiscBatchReader(i_ppasContext, i_logger, this, i_inQueue, i_params, i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10400, this,
                            BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
        return l_reader;
    } // End of createReader()
 

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";        
    /**
     * This method will instantiate the correct writer-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the writer-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a MiscBatchWriter.
     * @throws IOException when encountering problems writing to the report file
     */
    public BatchWriter createWriter()
    {
        MiscBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10500, this,
                            BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        if (super.i_startComponents)
        {
            l_writer = new MiscBatchWriter(i_ppasContext, i_logger, this, i_params, i_outQueue, i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10600, this,
                            BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }

        return l_writer;
    } // End of createWriter()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";        
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a MiscBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        MiscBatchProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10700, this,
                            BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        if (super.i_startComponents)
        {
            l_processor = new MiscBatchProcessor(i_ppasContext,
                                                 i_logger,
                                                 this,
                                                 i_inQueue,
                                                 i_outQueue,
                                                 i_params,
                                                 i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10800, this,
                            BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }
        return l_processor;
    } // End of createProcessor()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /** 
     * Updates the status of the job.
     * @param  p_status The status of the job
     */  
    protected void updateStatus(String p_status)
    {
        BatchJobControlData l_batchJobControlData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11100, this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        l_batchJobControlData = getKeyedBatchJobControlData();
        l_batchJobControlData.setStatus(p_status.charAt(0));
        
        try
        {
            super.i_batchContService.updateJobDetails( null, l_batchJobControlData, i_timeout);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10410, this,
                                C_METHOD_updateStatus + " PpasServiceException from updateJobDetails(...)");
            }
        }                
      
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 11200, this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    } // End of updateStatus()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData;
        long                l_jsJobId;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 11400, this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }
        return l_batchJobControlData;
    }
}
