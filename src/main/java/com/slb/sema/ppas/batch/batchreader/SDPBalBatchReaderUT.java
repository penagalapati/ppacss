////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SDPBalBatchReaderUT
//      DATE            :       19-July-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class tests the classes BatchReader and BatchLineFileReader
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.SDPBalBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * This class tests the class SDPBalBatchReader.
 */
public class SDPBalBatchReaderUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
 
    /** The name of the indata file used in normal mode. */
    private static final String          C_INDATA_FILENAME_NORMAL_MODE   = "CHANGE_SDP_00011_00.DAT";
    
    /** The name of the indata file used in normal mode Used in testGetRecord. */
    private static final String          C_INDATA_FILENAME_NORMAL1_MODE   = "CHANGE_SDP_00001_00.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord2. */
    private static final String          C_INDATA_FILENAME_NORMAL2_MODE  = "CHANGE_SDP_00002_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord3.*/
    private static final String          C_INDATA_FILENAME_NORMAL3_MODE  = "CHANGE_SDP_00003_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord4.*/
    private static final String          C_INDATA_FILENAME_NORMAL4_MODE  = "CHANGE_SDP_00004_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord5.*/
    private static final String          C_INDATA_FILENAME_NORMAL5_MODE  = "CHANGE_SDP_00005_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord6.*/
    private static final String          C_INDATA_FILENAME_NORMAL6_MODE  = "CHANGE_SDP_00006_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord7.*/
    private static final String          C_INDATA_FILENAME_NORMAL7_MODE  = "CHANGE_SDP_00007_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord8.*/
    private static final String          C_INDATA_FILENAME_NORMAL8_MODE  = "CHANGE_SDP_00008_00.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord8.*/
    private static final String          C_INDATA_FILENAME_NORMAL9_MODE  = "CHANGE_SDP_00009_00.DAT";
    

    /** The name of the indata file used in recovery mode. */
    private static final String          C_INDATA_FILENAME_RECOVERY_MODE = "CHANGE_SDP_00011_00.IPG";

    /** The name of the in-queue. */
    private static final String          C_QUEUE_NAME                    = "In-queue";

    /** The maximum size of the queue. */
    private static final long            C_MAX_QUEUE_SIZE                = 100;

    /** A reference to the <code>SCChangeBatchController</code> that is used in this test. */
    private static SDPBalBatchController c_sdpBalBatchController         = null;

    /** A reference to the <code>SCChangeBatchReader</code> that is used in this test. */
    private SDPBalBatchReader            i_sdpBalBatchReader             = null;

    /** The header line in a indata file. */
    private static final String          C_NORMAL_HEADER_LINE            = "0,127.0.0.0,127.0.0.1,00";

    /** The record line in a indata file. */
    private static final String          C_NORMAL_RECORD_LINE            = "1,708666555,00";

    /** The trailer line in a indata file. */
    private static final String          C_NORMAL_TRAILER_LINE           = "100,2";
    
    /** The record count in the trailer record wrong. */
    private static final String          C_WRONG_TRAILER_RECORD_COUNT    = "100,3";
    
    /** The result code in the header line is invalid. */
    private static final String          C_INVALID_HEADER_RESULT_CODE   = "0,127.0.0.0,127.0.0.1,20";
    
    /** The result code in the header line is invalid. */
    private static final String          C_INVALID_DETAILD_RESULT_CODE   = "1,708666555,02";
    
    /**The record type in the detaild record is invalid. */
    private static final String         C_INVALID_RECORD_TYPE           = "2,708666555,00";

    /** To many fields in header line. */                                  
    private static final String          C_TO_LONG_HEADER_LINE           = 
                        "0,127.100.100.100,127.100.100.200,000";

    /** Wrong number of fields in a header record in a indata file. */
    private static final String          C_WRONG_NUMBER_HEADER_FIELDS   = "0,127.0.0.0,127.0.0.1";

    /** A to long record in a indata file. */
    private static final String          C_TO_LONG_RECORD_LINE           = "1,0000070866655555,00";

    /** Wrong number of fields in a detaild record in a indata file. */
    private static final String          C_WRONG_NUMBER_RECORD_FIELDS    = "1,708666555";

    /** Wrong number of fields in a trailer record in a indata file. */
    private static final String          C_WRONG_NUMBER_TRAILER_FIELDS   = "100";  

    /** Sepcific SDP report file fields delimiter. */
    public static final String    C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER = ",";


    /**
     * Class constructor.
     * @param p_name the
     */
    public SDPBalBatchReaderUT( String p_name )
    {
        super( p_name );
    }

    /**
     * Sets up before test execution.
     */
    protected void setUp()
    {
        super.setUp();
    }

    /** 
     * Tidies up after test execution.
     */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    
    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(SDPBalBatchReaderUT.class);
    }
    
    /** Test if a file name is valid. */
    public void testIsFileNameValid()
    {    
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, 
                                 C_NORMAL_RECORD_LINE,
                                 C_NORMAL_TRAILER_LINE};

        this.createContext(C_INDATA_FILENAME_NORMAL_MODE, l_inputLines); 
        
        super.beginOfTest("testIsFileNameValidDat");
        
        boolean l_valid = false;
      
        l_valid = i_sdpBalBatchReader.isFileNameValid(C_INDATA_FILENAME_NORMAL_MODE);    
        assertTrue(C_INDATA_FILENAME_NORMAL_MODE + " is not a valid filename ", l_valid);
        
        l_valid = i_sdpBalBatchReader.isFileNameValid(C_INDATA_FILENAME_RECOVERY_MODE);
        assertTrue(C_INDATA_FILENAME_RECOVERY_MODE + " is not a valid filename ", l_valid);  
        
        super.endOfTest();
    }
    
    /** Test to testGetRecord. */   
    public void testGetRecord()
    {
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, 
                                 C_NORMAL_RECORD_LINE, 
                                 C_INVALID_RECORD_TYPE,
                                 C_TO_LONG_RECORD_LINE, 
                                 C_WRONG_NUMBER_RECORD_FIELDS,
                                 C_INVALID_DETAILD_RESULT_CODE,
                                 C_NORMAL_TRAILER_LINE};

        this.createContext(C_INDATA_FILENAME_NORMAL1_MODE, l_inputLines);         
        super.beginOfTest("testGetRecord");
        BatchRecordData l_recordData = null;
        
        //Test a normal header record.
        l_recordData = i_sdpBalBatchReader.getRecord();

        assertEquals("Failure when testing a normal header record!", 
                     C_NORMAL_HEADER_LINE, 
                     l_recordData.getInputLine());

        //Test a normal detailed record.
        l_recordData = i_sdpBalBatchReader.getRecord();
    
        assertEquals("Failure when testing a normal detaild record!",
                     C_NORMAL_RECORD_LINE,
                     l_recordData.getInputLine());
        
        //Test a invalid record type.
        l_recordData = i_sdpBalBatchReader.getRecord();

        assertEquals("Failure when testing an invalid record type!", 
                     SDPBalBatchReader.C_ERROR_INVALID_RECORD_TYPE
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER 
                     + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());
        
        //Test when the detailed record line is to long. Error code 02.
        l_recordData = i_sdpBalBatchReader.getRecord();
       
        assertEquals("Failure when testing a to long detailed record!",
                     SDPBalBatchReader.C_ERROR_INVALID_RECORD_LENGTH
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());
        
        //Test when the detailed record has wrong number of fields. Error code 12.
        l_recordData = i_sdpBalBatchReader.getRecord();
 
        assertEquals("Failure when testing a to detailed record with wrong number of fields!",
                     SDPBalBatchReader.C_ERROR_WRONG_NUMBER_OF_FIELDS
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());
        
        //Test when the detailed record has wrong result code. Error code 14.
        l_recordData = i_sdpBalBatchReader.getRecord();
 
        assertEquals("Failure when testing a detailed record with wrong result code!",
                     SDPBalBatchReader.C_ERROR_NOT_PROCCESSED_IN_SDP
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());
        
        //Test a normal trailer record.
        l_recordData = i_sdpBalBatchReader.getRecord();
     
        assertEquals("Failure when testing a normal trailer record!",
                     C_NORMAL_TRAILER_LINE,
                     l_recordData.getInputLine());

        super.endOfTest();
    }
    
    /** Test getRecord() when the header record is missing in the input file. */
    public void testGetRecord2()
    {
        String[] l_inputLines = {C_NORMAL_RECORD_LINE, C_NORMAL_RECORD_LINE, C_NORMAL_TRAILER_LINE};
        
        this.createContext(C_INDATA_FILENAME_NORMAL2_MODE, l_inputLines); 
        super.beginOfTest("testGetRecord2");
        BatchRecordData l_recordData = null;
        
        //Test when the header record is missing in the input file.
        l_recordData = i_sdpBalBatchReader.getRecord();

        assertEquals("No error reported for missing header record the input file!",
                     SDPBalBatchReader.C_ERROR_HEADER_MISSING_OR_NOT_FIRST
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + C_NORMAL_RECORD_LINE,
                     l_recordData.getErrorLine());
        
        super.endOfTest();
    }
    
    /** Test getRecord() when the  the trailer record not is last in file.. */
    public void testGetRecord3()
    {        
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, 
                                 C_NORMAL_RECORD_LINE, 
                                 C_NORMAL_TRAILER_LINE,
                                 C_NORMAL_RECORD_LINE};
        
        this.createContext(C_INDATA_FILENAME_NORMAL3_MODE, l_inputLines);
        super.beginOfTest("testGetRecord3");
        BatchRecordData l_recordData        = null;
        BatchRecordData l_trailerRecordData = null;
        
        //Test when the trailer record has wrong record count.
        l_recordData        = i_sdpBalBatchReader.getRecord();
        l_recordData        = i_sdpBalBatchReader.getRecord();
        l_trailerRecordData = i_sdpBalBatchReader.getRecord(); 
        l_recordData        = i_sdpBalBatchReader.getRecord();
        l_recordData        = i_sdpBalBatchReader.getRecord();

        assertNotNull("No trailer record data returned when the trailer record is not the last line " +
                      "in the indata file.",
                      l_trailerRecordData);
        assertNotNull("No error reported when the trailer record is not the last line in the indata file.",
                      l_trailerRecordData.getErrorLine());
        assertEquals("Wrong error code used when the trailer record is not the last line in the indata file.",
                     SDPBalBatchReader.C_ERROR_TRAILER_NOT_LAST_IN_FILE,
                     l_trailerRecordData.getErrorLine().split(C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER)[0]);
        
        super.endOfTest();
    }
    
    /** Test getRecord() when the the trailer record is missing. */
    public void testGetRecord4()
    {  
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, C_NORMAL_RECORD_LINE, C_NORMAL_RECORD_LINE};
        
        this.createContext(C_INDATA_FILENAME_NORMAL4_MODE, l_inputLines);
        super.beginOfTest("testGetRecord4");
        BatchRecordData l_recordData = null;
        
        //Test when the trailer is missing.
        l_recordData = i_sdpBalBatchReader.getRecord();
        l_recordData = i_sdpBalBatchReader.getRecord();
        l_recordData = i_sdpBalBatchReader.getRecord();
        l_recordData = i_sdpBalBatchReader.getRecord();

        assertNotNull("No record data returned when the trailer record is missing in the indata file.",
                      l_recordData);
        assertNotNull("No error reported when the trailer record is missing in the indata file.",
                      l_recordData.getErrorLine());
        assertEquals("Wrong error code used when the trailer record is missing in the indata file.",
                     SDPBalBatchReader.C_ERROR_TRAILER_RECORD_MISSING,
                     l_recordData.getErrorLine().split(C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER)[0]);
        
        super.endOfTest();
    }
    
    /** Test getRecord() when the result code in the header is not 00 (sucess). */
    public void testGetRecord5()
    {         
        String[] l_inputLines = {C_INVALID_HEADER_RESULT_CODE, C_NORMAL_RECORD_LINE, C_NORMAL_TRAILER_LINE};
           
        this.createContext(C_INDATA_FILENAME_NORMAL5_MODE, l_inputLines);
        super.beginOfTest("testGetRecord5");
        BatchRecordData l_recordData = null;
        
        //Test when the trailer is missing.
        l_recordData = i_sdpBalBatchReader.getRecord();
        
        assertEquals("No error reported invalid result code in the header line!",
                     SDPBalBatchReader.C_ERROR_NOT_PROCCESSED_IN_SDP
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + C_INVALID_HEADER_RESULT_CODE,
                     l_recordData.getErrorLine());
        
        super.endOfTest();
    }
    
    /** Test getRecord() when the to long header line. */
    public void testGetRecord6()
    {  
        String[] l_inputLines = {C_TO_LONG_HEADER_LINE, C_NORMAL_RECORD_LINE, C_NORMAL_TRAILER_LINE};
        
        this.createContext(C_INDATA_FILENAME_NORMAL6_MODE, l_inputLines);
        super.beginOfTest("testGetRecord6");
        BatchRecordData l_headerRecordData = null;
        
        //Test when the header line is too long.
        l_headerRecordData = i_sdpBalBatchReader.getRecord();

        assertNotNull("No record data returned when the header line is too long.",
                      l_headerRecordData);
        assertNotNull("No error reported when the header line is too long.",
                      l_headerRecordData.getErrorLine());
        assertEquals("Wrong error code used when the header line is too long.",
                     SDPBalBatchReader.C_ERROR_INVALID_RECORD_LENGTH,
                     l_headerRecordData.getErrorLine().split(C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER)[0]);
        
        super.endOfTest();
    }
    
    /** Test getRecord() when wrong number of header fields. */
    public void testGetRecord7()
    {  
        String[] l_inputLines = {C_WRONG_NUMBER_HEADER_FIELDS, C_NORMAL_RECORD_LINE, C_NORMAL_TRAILER_LINE};
        
        this.createContext(C_INDATA_FILENAME_NORMAL7_MODE, l_inputLines);
        super.beginOfTest("testGetRecord7");
        BatchRecordData l_recordData = null;
        
        //Test when the trailer is missing.
        l_recordData = i_sdpBalBatchReader.getRecord();
        
        assertEquals("No error reported wrong number of fields in the header line!",
                     SDPBalBatchReader.C_ERROR_WRONG_NUMBER_OF_FIELDS
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + C_WRONG_NUMBER_HEADER_FIELDS,
                     l_recordData.getErrorLine());
        
        super.endOfTest();
    }
    
    
    /** Test getRecord() when wrong number of fields in the trailer record. */
    public void testGetRecord8()
    {  
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, C_NORMAL_RECORD_LINE, C_WRONG_NUMBER_TRAILER_FIELDS};
        
        this.createContext(C_INDATA_FILENAME_NORMAL8_MODE, l_inputLines);
        super.beginOfTest("testGetRecord8");
        BatchRecordData l_recordData = null;
        
        
        //Test when the trailer is missing.
        l_recordData = i_sdpBalBatchReader.getRecord();
        l_recordData = i_sdpBalBatchReader.getRecord();
        l_recordData = i_sdpBalBatchReader.getRecord();
        
        assertEquals("No error reported wrong number of fields in the trailer record!",
                     SDPBalBatchReader.C_ERROR_WRONG_NUMBER_OF_FIELDS
                     + C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER
                     + C_WRONG_NUMBER_TRAILER_FIELDS,
                     l_recordData.getErrorLine());
        
        super.endOfTest();
    }
    
    /** Test getRecord() when the the record count is wrong. */
    public void testGetRecord9()
    {        
        String[] l_inputLines = {C_NORMAL_HEADER_LINE, C_NORMAL_RECORD_LINE, C_WRONG_TRAILER_RECORD_COUNT};
        
        this.createContext(C_INDATA_FILENAME_NORMAL9_MODE, l_inputLines);
        super.beginOfTest("testGetRecord9");
        BatchRecordData l_recordData        = null;
        BatchRecordData l_trailerRecordData = null;
        
        //Test when the trailer record has wrong record count.
        l_recordData        = i_sdpBalBatchReader.getRecord();
        l_recordData        = i_sdpBalBatchReader.getRecord();
        l_trailerRecordData = i_sdpBalBatchReader.getRecord(); 
        l_recordData        = i_sdpBalBatchReader.getRecord();

        assertNotNull(
            "No trailer record data returned when the trailer record has an incorrect record count.",
            l_trailerRecordData);
        assertNotNull(
            "No error reported when the trailer record has an incorrect record count.",
            l_trailerRecordData.getErrorLine());
        assertEquals(
            "Wrong error code used when the trailer record has an incorrect record count.",
            SDPBalBatchReader.C_ERROR_RECORD_COUNT_IN_TRAILER_INCORRECT,
            l_trailerRecordData.getErrorLine().split(C_SDP_SPECIFIC_REPORT_FIELDS_DELIMITER)[0]);
 
        super.endOfTest();
    }
    
       
    /**
     * Creates this tests context.
     * @param p_fileName The name of the input file to test.
     * @param p_linearray An array with the lines that will be in the file <code>p_fileName</code>.
     */
    private void createContext(String p_fileName, String [] p_linearray)
    {
        StringBuffer l_tmpFullPathName  = null;
        Hashtable l_parameters          = new Hashtable();
        SizedQueue l_queue              = null;
        String l_inputFileDirectory     = null;
        String l_fullPathName           = null;
        String l_fileName               = null;

        String [] l_fileArray = p_fileName.split("[.]");
        
        l_fileName = l_fileArray[0] + ".IPG";    
        
        l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

        //Delete the recovery file (.IPG) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(l_fileName);
        l_fullPathName = l_tmpFullPathName.toString();
        super.deleteFile(l_fullPathName);
        
        //Create a input file (.DAT) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(p_fileName);
        l_fullPathName = l_tmpFullPathName.toString();
        
        super.createNewFile(l_fullPathName, p_linearray);
        
        l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, p_fileName);
        
        try
        {
            l_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
                
        i_sdpBalBatchReader = new SDPBalBatchReader(super.c_ppasContext,
                                                    super.c_logger,
                                                    c_sdpBalBatchController,
                                                    l_queue,
                                                    l_parameters,
                                                    super.c_properties);

        assertNotNull("Failed to create a SCChangeBatchReader instance.", i_sdpBalBatchReader);
    }



    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + (p_args.length == 0 ? "" : "ignored"));
        TestRunner.run(suite());
    }
}