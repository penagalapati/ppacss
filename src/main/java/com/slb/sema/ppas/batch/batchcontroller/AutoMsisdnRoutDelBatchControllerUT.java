////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutDelBatchControllerUT
//  DATE            :       17 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       This is the test program for the Disconnect batch.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of change>    | <reference>
//          |               |                                  |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchSubJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;

/** The unit test for the Disconnect batch. */
public class AutoMsisdnRoutDelBatchControllerUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** The start range. */
    private static final long              C_START_RANGE         = 707;

    /** The end range. */
    private static final long              C_END_RANGE           = 717;

    /** The state of recovery. */
    private static final String            C_RECOVERY_ON         = "yes";

    /** The master js job id. */
    private static final long              C_MASTER_JS_JOB_ID    = 12345;

    /** The js job id for the sub job. */
    private static final int               C_SUB_JOB_ID          = 3;

    /** The disconnection date. */
    private static final String            C_DISCON_DATE         = "20991231";

    /** The "in progress" status. */
    private static final char              C_STATUS_IN_PROGRESS  = 'I';

    /** The execution date time. */
    private static final PpasDateTime      C_EXECUTION_DATE_TIME = DatePatch.getDateTimeNow();

    /** Holding the parameters needed for this batch. */
    private Hashtable                      i_params              = null;

    /** An instance of <code>PpasBatchControlService</code>. */
    private static PpasBatchControlService c_batchControlService = null;

    /** An instance of <code>DisconBatchController</code>. */
    private static AutoMsisdnRoutDelBatchController c_autoMsisdnRoutDelBatchController = null;

    /**
     * @param p_name The class name of this test.
     */
    public AutoMsisdnRoutDelBatchControllerUT(String p_name)
    {
        super(p_name);
        //super.enableConsoleLogAll();
    }

    /** This test tests testCreateProcessor. */
    public void testCreateProcessor()
    {
        super.beginOfTest("testCreateProcessor");

        if (c_autoMsisdnRoutDelBatchController.createProcessor() == null)
        {
            assertFalse("No BatchProcessor was created", true);
        }
        super.endOfTest();
    }

    /** This test tests testCreateReader. */
    public void testCreateReader()
    {
        super.beginOfTest("testCreateReader");

        if (c_autoMsisdnRoutDelBatchController.createReader() == null)
        {
            assertFalse("No BatchReader was created", true);
        }
        super.endOfTest();
    }

    /** This test tests testCreateWriter. */
    public void testCreateWriter()
    {
        super.beginOfTest("testCreateWriter");

        try
        {
            if (c_autoMsisdnRoutDelBatchController.createWriter() == null)
            {
                assertFalse("No BatchWriter was created", true);
            }
        }
        catch (IOException e)
        {
            fail();
        }
        super.endOfTest();
    }

    /**
     * Test the updateStatus() method. In the method context the staus has been set
     * <code>BatchConstants.C_BATCH_STATUS_INPROGRESS</code>. In this method the status is updated to
     * <code>BatchConstants.C_BATCH_STATUS_COMPLETED</code>. Then a call is done to the databse to check
     * that the status has been changed.
     */

    public void testUpdateStatus()
    {
        BatchSubJobControlData l_batchSubJobControlData = null;

        try
        {
            c_autoMsisdnRoutDelBatchController.updateStatus(BatchConstants.C_BATCH_STATUS_COMPLETED);

            l_batchSubJobControlData = c_batchControlService.getSubJobDetails(c_ppasRequest,
                                                                              C_MASTER_JS_JOB_ID,
                                                                              C_EXECUTION_DATE_TIME,
                                                                              C_SUB_JOB_ID,
                                                                              C_TIMEOUT);

            super.assertEquals("Wrong status!", BatchConstants.C_BATCH_STATUS_COMPLETED, Character
                    .toString(l_batchSubJobControlData.getStatus()));

        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }
    }

    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        createContext();
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }

    /** Create this test's context. */

    private void createContext()
    {
        c_writeUtText = true;

        if (c_autoMsisdnRoutDelBatchController == null)
        {
            BatchSubJobControlData l_batchSubJobControlData = null;

            try
            {
                i_params = new Hashtable();
                i_params.put(BatchConstants.C_KEY_BATCH_JOB_TYPE,
                             BatchConstants.C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION);
                i_params.put(BatchConstants.C_KEY_MASTER_JS_JOB_ID, Long.toString(C_MASTER_JS_JOB_ID));
                i_params.put(BatchConstants.C_KEY_SUB_JOB_ID, Integer.toString(C_SUB_JOB_ID));
                i_params.put(BatchConstants.C_KEY_START_RANGE, Long.toString(C_START_RANGE));
                i_params.put(BatchConstants.C_KEY_END_RANGE, Long.toString(C_END_RANGE));
                i_params.put(BatchConstants.C_KEY_EXECUTION_DATE_TIME, C_EXECUTION_DATE_TIME.toString());
                i_params.put(BatchConstants.C_KEY_RECOVERY_FLAG, C_RECOVERY_ON);
                i_params.put(BatchConstants.C_KEY_DISCONNECTION_DATE, C_DISCON_DATE);

                c_batchControlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

                c_autoMsisdnRoutDelBatchController = 
                    new AutoMsisdnRoutDelBatchController(c_ppasContext,
                                                         BatchConstants.C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION,
                                                         Integer.toString(C_SUB_JOB_ID),
                                                         "AutoMsisdnRoutDelBatchControllerUT",
                                                         i_params);

                //Create a BatchSubJobControlData object and save it in table BSCO_BATCH_SUB_CONTROL.
                l_batchSubJobControlData =
                    new BatchSubJobControlData(BatchConstants.C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION,
                                               C_EXECUTION_DATE_TIME,
                                               C_SUB_JOB_ID,
                                               -1,
                                               C_MASTER_JS_JOB_ID,
                                               C_STATUS_IN_PROGRESS,
                                               C_START_RANGE,
                                               C_END_RANGE,
                                               -1,
                                               " ",
                                               " ",
                                               c_ppasRequest.getOpid());

                c_batchControlService.addSubJobDetails(c_ppasRequest,
                                                       l_batchSubJobControlData,
                                                       C_TIMEOUT);
            }

            catch (PpasException e)
            {
                super.failedTestException(e);
            }
        }
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(AutoMsisdnRoutDelBatchControllerUT.class);
    }

    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        TestRunner.run(suite());
    }

}