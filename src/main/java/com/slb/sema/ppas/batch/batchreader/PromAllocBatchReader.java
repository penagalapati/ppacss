////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromAllocBatchReader
//      DATE            :       9-Aug-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AccountDetailBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isil.batchisilservices.PromotionAllocationDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class will identify all subscribers within a given range that has non-expired promotion plan
 * allocations that can be sent to the SDP for provisioning. 
 */
public class PromAllocBatchReader extends BatchReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME                    = "PromAllocBatchReader";
    
    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_START_RANGE_IS_MISSING        = "Start range is missing.";

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_END_RANGE_IS_MISSING          = "End range is missing.";

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String   C_RECOVERY_FLAG_IS_MISSING      = "Recovery flag is missing.";

    /** Number of values in the array i_parameters. Value is {@value}. */
    private static final int      C_NUMBER_OF_PARAMETERS          = 2;
    
    /** Index for Start Range value in the array i_parameters.  Value is {@value}. */
    private static final int      C_START_RANGE                   = 0;

    /** Index for Stop Range value in the array i_parameters.  Value is {@value}. */
    private static final int      C_END_RANGE                     = 1;

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** ISAPI that retrieves all master custId in a specified range of custIds. */
    private PromotionAllocationDbBatchService i_isApi             = null;

    /** Input paramters to the SQL statement. */
    private String[]                          i_inputParameters   = null;

    /** Retrieved from properties. */
    private int                               i_fetchSize         = 0;
    
    /** Retrieved from context. */
    private PpasSession                       i_session           = null;

    /**
     * Constructor for the PromAllocBatchReader.
     * @param p_ppasContext  Session information.
     * @param p_logger       Logger for directing loggable messages.
     * @param p_controller   Batch Controller.
     * @param p_inQueue      Data 'in' queue.
     * @param p_parameters   General parameters. Keep information about the range of custIds to process.
     * @param p_properties   Properties.
     */
    public PromAllocBatchReader( PpasContext      p_ppasContext,
                                 Logger           p_logger,
                                 BatchController  p_controller,
                                 SizedQueue       p_inQueue,
                                 Map              p_parameters,
                                 PpasProperties   p_properties )
    {
        super(p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10100,
                             this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        this.getProperties();
        this.getPromotionAllocationParameters(p_parameters);
        this.init();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             10101,
                             this,
                             BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } // End of constructor(......)

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
   /** 
    * This method call the Batch ISIL Service that returns a AccountDetailBatchRecordData.
    * No manipulation is required since the underlying database service has knowledge
    * of the record created. The customer id is extracted and used to call
    * addCustomerInprogress().
    *
    * @return The AccountDetailBatchRecordData.
    */
    protected BatchRecordData getRecord()
    {
        AccountDetailBatchRecordData l_record = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             20103,
                             this,
                             "** READER ** " + BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Find accounts with an expired promotion allocation.
        do
        {
            try
            {
                // Get next subscriber from PromotionAllocationDbBatchService
                l_record = (AccountDetailBatchRecordData) i_isApi.fetch();

                // If no more subscribers return null to indicate finished
                if ( l_record == null )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                         PpasDebug.C_APP_SERVICE,
                                         PpasDebug.C_ST_CONFIN_END,
                                         C_CLASS_NAME,
                                         10105,
                                         this,
                                         "** READER ** null record from db, no more custIDs found" );
                    }
                    // No customers found for provisioning
                    return null;
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                     PpasDebug.C_APP_SERVICE,
                                     PpasDebug.C_ST_CONFIN_END,
                                     C_CLASS_NAME,
                                     10106,
                                     this,
                                     "** READER ** fetched record" + l_record.dumpRecord() );
                }
                
                return l_record;
            }
            catch (PpasServiceException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10122,
                        this,
                        "** READER ** ===PpasSqlException! - i_isApi.open(). ===" + C_METHOD_init +
                        "PpasServiceException is caught: " + e.getMessage() +
                        ",  key: " + e.getMsgKey());
                }

                e.printStackTrace();
            }
            
        }
        while (true);   // Will loop until a PP due for provisioning is found
                        // or until all subscribers are checked
    } // End of getRecord()
        
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";    
    /**
     * This method instantiate and opens the BatchISIL Service.
     */
    protected void init()
    {
        PpasRequest l_request      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             10121,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_init );
        }
        
        // Create and open the Batch ISIL Service Component
        i_session = new PpasSession();
        i_session.setContext(this.i_context);
        
        l_request = new PpasRequest(i_session);

        // Create and open the Batch ISIL Service Component
        i_isApi = new PromotionAllocationDbBatchService( l_request, i_logger, i_context );

        // Call open on the API component
        try
        {
            i_isApi.open(i_inputParameters, i_fetchSize );
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10122,
                    this,
                    "** READER ** PpasSqlException! - i_isApi.open(). " + C_METHOD_init +
                    "PpasServiceException is caught: " + e.getMessage() +
                    ",  key: " + e.getMsgKey());
            }

            e.printStackTrace();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             10125,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_init );
        }
    } // End of init()

    /** 
     * @see com.slb.sema.ppas.batch.batchreader.BatchReader#tidyUp() .
     */
    protected void tidyUp()
    {
        // Clean up connectivity
        try
        {
            finalise();
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_ERROR,
                                 C_CLASS_NAME,
                                 10126,
                                 this,
                                 "PpasServiceException - tidyUp. " + e.getMsgKey() );
            }
            e.printStackTrace();
        }
    }  // End of tidyUp()

    /**
     * This method will be called before the reader component terminates and will release the
     * jdbc-connection and perform general tidy-up tasks.
     * @throws PpasServiceException Error when closing isapi.
     */
    protected void finalise() throws PpasServiceException
    {
        if ( i_isApi != null )
        {
            i_isApi.close();
            i_isApi = null;
        }
        return;
    }  // End of finalise()
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getPromotionAllocationParameters =
                            "getPromotionAllocationParameters";    
    /**
     * This method reads the paramters: StartRange, EndRange and RecoveryFlag
     * from the in-parameter Map.
     * Checks if all paramters are given and if the ranges is numeric, the date a possible date and
     * that the recovery flag is either Yes or No.
     * @param p_parameters Start parameters.
     * @return true if valid parameters.
     */
    private boolean getPromotionAllocationParameters( Map p_parameters )
    {
        boolean l_returnValue  = true;   // Assume valid arguments in the Map
        String  l_recoveryMode = null;   // Help variable to read the recovery flag
        
        this.i_inputParameters = new String[C_NUMBER_OF_PARAMETERS];

        this.i_inputParameters[C_START_RANGE] = (String)p_parameters.get(BatchConstants.C_KEY_START_RANGE);
        if ( this.i_inputParameters[C_START_RANGE] == null )
        {
            // Cannot find the start range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_START_RANGE_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10127,
                    this,
                    "** READER ** Start range is missing in the parameter list. " +
                    C_METHOD_getPromotionAllocationParameters );
            }
            l_returnValue = false;
        }

        this.i_inputParameters[C_END_RANGE] = (String)p_parameters.get(BatchConstants.C_KEY_END_RANGE);
        if ( this.i_inputParameters[C_END_RANGE] == null )
        {
            // Cannot find the end range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_END_RANGE_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10128,
                    this,
                    "** READER ** End range is missing in the parameter list. " +
                    C_METHOD_getPromotionAllocationParameters );
            }
            
            l_returnValue = false;
        }

        l_recoveryMode = (String)p_parameters.get(BatchConstants.C_KEY_RECOVERY_FLAG);
        if ( l_recoveryMode == null )
        {
            // Cannot find the recovery mode flag in the Map
            // Cannot find the end range in Map
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_RECOVERY_FLAG_IS_MISSING,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10129,
                    this,
                    "** READER ** Recovery flag is missing in the parameter list. " + 
                        C_METHOD_getPromotionAllocationParameters );
            }
        }

        if (l_returnValue )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10130,
                    this,
                    "** READER ** Paramters -- start range: '" + i_inputParameters[C_START_RANGE] +
                    "',  end range: '" + i_inputParameters[C_END_RANGE] +
                    "',  recovery: '" + l_recoveryMode + "'.");
            }
            
            // Parameters given - check if the ranges are numeric 
            // Recovery mode can be "Yes" or "No"
            if ( this.i_inputParameters[C_START_RANGE].matches(BatchConstants.C_PATTERN_NUMERIC) &&
                 this.i_inputParameters[C_END_RANGE].matches(BatchConstants.C_PATTERN_NUMERIC)   &&
                 l_recoveryMode.matches(BatchConstants.C_PATTERN_RECOVERY) )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10131,
                        this,
                        "** READER ** All paramters OK, so far.. " +
                                        C_METHOD_getPromotionAllocationParameters);
                }
                if ( l_recoveryMode.equals(BatchConstants.C_VALUE_RECOVERY_MODE_ON) )
                {
                    this.i_recoveryMode = true;                
                }
                else
                {
                    this.i_recoveryMode = false;
                }
            }
            else
            {
                // At least one of the parameters had an invalid value
                // Cannot find the end range in Map
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_END_RANGE_IS_MISSING,
                                     LoggableInterface.C_SEVERITY_ERROR) );

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10132,
                        this,
                        "At least one of the paramters had an invalid value in the parameter list. " + 
                            C_METHOD_getPromotionAllocationParameters );
                }

                l_returnValue = false;
            }

        }

        return l_returnValue;
        
    } // End of getPromotionAllocationParameters(.)
    
    /** Load properties. */
    private void getProperties()
    {
        String l_tmpFetchSize = null;

        l_tmpFetchSize = i_properties.getProperty( BatchConstants.C_FETCH_ARRAY_SIZE );
        if (l_tmpFetchSize != null)
        {
            this.i_fetchSize = Integer.valueOf(l_tmpFetchSize).intValue();
        }

        return;
    } // End of getProperties()
} // End of PromAllocBatchReader
