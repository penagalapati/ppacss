////////////////////////////////////////////////////////////////////////////////
//
//        FILE NAME       :       FraudBatchControllerUT.java
//        DATE            :       05-June-2006
//        AUTHOR          :       Marianne Toernqvist
//        REFERENCE       :       PpaLon#2400/9073, PRD_ASCS00_GEN_CA_080
//
//        COPYRIGHT       :       WM-data 2006
//
//        DESCRIPTION     :       Vodafone Ireland Fraud returns.
//
////////////////////////////////////////////////////////////////////////////////
//        CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                   | REFERENCE
//----------+---------------+-------------------------------+--------------------
// DD/MM/YY | <author>      | <description>                 | PpaLon#xxxx/yyyy
//----------+---------------+----------------------------------+-----------------
// 26/04/07 | L. Lundberg   | Method runFraudReturnsBatch() | PpacLon#3033/11279
//          |               | is modified:                  |
//          |               | Another 'startBatch(...)'     |
//          |               | is used which requires to be  |
//          |               | surrounded by a 'try-catch'   |
//          |               | clause.                       |
//----------+---------------+-------------------------------+--------------------
/////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchutil.BatchProperties;
import com.slb.sema.ppas.batch.exceptions.BatchConfigException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** Unit tests the Fraud Returns batch controller. */
public class FraudReturnsBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constants                                                   ==
    // =========================================================================
	/** Batch indata file. Value is {@value}. */
    private static final String C_FILENAME_INDATA   = "INPUTFILE_20061108_15.txt";
    /** Batch indata file, to run the batch in "in progress mode". Value is {@value}. */
    private static final String C_FILENAME_IPG      = "IPGFILE_20061108_15.txt";
    /** Batch recovery file. Value is {@value}. */
    private static final String C_FILENAME_RECOVERY = "IPGFILE_20061108_15.RCV";
    /** File defining "dummy msisdns". Value is {@value}. */
    private static final String C_FILENAME_DUMMY    = "dummy_msisdn.dba";
    /** Indata for the input file. */
    private static final String[] C_DATA_INDATA = {"086672630,33303439,1",
                                                   "086672631,33303439,2",
                                                   "086672632,33303439,3a",
                                                   "086672632,33303439,3",
                                                   "086672633,33303439,4",
                                                   "086672634,33303439,5",
                                                   "086672635,33303439,6",
                                                   "086672636,3330xxx9,7",
                                                   "086672636,33303439,7",
                                                   "086672637,33303439,8",
                                                   "086672638,33303439,9",
                                                   "086672639,33303439,0",
                                                   "08HEJ2633,33303439,4",
                                                   "086672633,33303439,4",
                                                   "086672630,33303439,1"};
    /** Data for the batch's recovery file. */
    private static final String[] C_DATA_RECOVERY = {"1,OK",
                                                     "2,OK",
                                                     "3,ERR",
                                                     "4,OK",
                                                     "5,OK",
                                                     "6,OK",
                                                     "7,OK",
                                                     "8,ERR",
                                                     "9,OK 1",
                                                     "10,OK",
                                                     "11,OK 1",
                                                     "12,OK 0",
                                                     "13,ERR",
                                                     "14,OK",
                                                     "15,OK"};
    /** Data for the file defining "dummy msisdns". */
    private static final String[]  C_DATA_DUMMY = {"086672632",
                                                   "0832000000",
                                                   "0832000630",
                                                   "0832000000",
                                                   "086672638",
                                                   "0832000000"};

    // =========================================================================
    // == Private instance attribute(s).                                      ==
    // =========================================================================
    /** The <code>BatchProperties</code> container. */       
    private BatchProperties  i_batchProperties    = null;

    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructor.
     * Unit test for FraudReturnsBatchController.
     * @param p_name the name of this test
     */
    public FraudReturnsBatchControllerUT(String p_name)
    {
        super(p_name, "batch_vfr");
    }

    
    /** Hide default contructor. */
    private FraudReturnsBatchControllerUT()
    {
        super(null, null);
    }

    
    
    // =========================================================================
    // == Public method(s).                                                   ==
    // =========================================================================
    /***************************************************************************
     * Creates and returns a <code>TestSuite</code> object to enabling the
     * execution of multiple tests automatically.
     * 
     * @return a <code>TestSuite</code> object to enabling the execution of
     *         multiple tests automatically.
     */
    public static Test suite()
    {
        return new TestSuite(FraudReturnsBatchControllerUT.class);
    }


    /***************************************************************************
     * Performs a functional test of an indata file to FraudReturns
     * A test file with a valid name containing several test cases is generated.
     * The FraudBatchReturn batch is started in order to process the test file 
     * and report and log files are generated.
     *
     * @ut.when        A test file containing a mix of valid/invalid record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        Reports are generated and put on the batch report directory.
     *                 An error log file will also be created.
     *                 A recovery file will be generated - because the input file
     *                 includes errornous records.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
//    public void testFraudReturnsBatch_BasicTest()
//    {
//        FraudReturnsBatchController l_fraudReturnsBatchController = null;
//
//        super.beginOfTest("testFraudReturnsBatch_xx");
//
//        // Create a test file for the requested action.
//        createFraudReturnsTestFile(i_batchProperties.getIndataDir());
//        sayTime("Indata directory '" + /*l_successfulInsertTestDataSet.getIndataFilename()*/ ""+ "' is created.");
//
//        // Start the batch, i.e. process the test file.
//        l_fraudReturnsBatchController = runFraudReturnsBatch(JobStatus.C_JOB_EXIT_STATUS_SUCCESS);
//        sayTime("The batch has finished.");
//
//        sayTime("The complete result of the batch is verfied, OK.");
//
//        super.endOfTest();
//    }
    
    /***************************************************************************
     * Performs a functional test of an indata file to FraudReturns
     * A test file with a valid name containing several test cases is generated.
     * The FraudBatchReturn batch is started in order to process the test file 
     * and report and log files are generated.
     *
     * @ut.when        A test file containing a mix of valid/invalid record is
     *                 created and placed in the batch indata directory. Here has the input-
     *                 file the extenstion IPG, and a recovery file is also created. 
     *                 Therefore the batch will be run in recovery mode.
     *
     * @ut.then        Reports are generated and put on the batch report directory.
     *                 An error log file will also be created.
     *                 A recovery file will be generated - because the input file
     *                 includes errornous records.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testFraudReturnsBatch_BasicTestRecovery()
    {
        FraudReturnsBatchController l_fraudReturnsBatchController = null;

        super.beginOfTest("testFraudReturnsBatch_BasicTestRecovery");

        // Create a test file for the requested action.
        createFraudReturnsTestFile(i_batchProperties.getIndataDir(),
        		                   C_FILENAME_IPG,
        		                   C_DATA_INDATA);
        createFraudReturnsTestFile(i_batchProperties.getIndataDir(),
        		                   C_FILENAME_DUMMY,
        		                   C_DATA_DUMMY);
        createFraudReturnsTestFile(i_batchProperties.getRecoveryDir(),
        		                   C_FILENAME_RECOVERY,
        		                   C_DATA_RECOVERY);
        
        sayTime("Indata directory '" + i_batchProperties.getRecoveryDir() + "' is created.");

        // Start the batch, i.e. process the test file.
        l_fraudReturnsBatchController = runFraudReturnsBatch(JobStatus.C_JOB_EXIT_STATUS_SUCCESS);
        sayTime("The batch has finished.");

        sayTime("The complete result of the batch is verfied, OK.");

        super.endOfTest();
    }
       
    
    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================    
   /**
     * Creates a test file with the given parameters.
     * 
     * @param p_testFileDir  the test file directory.
     * @param p_fileName     the test file name
     * @param p_data         the set of file data.
     */
    protected void createFraudReturnsTestFile(String   p_testFileDir,
    		                                  String   p_fileName,
    		                                  String[] p_data)
    {
                 
        PrintWriter l_testFileWriter = null;
        File        l_testFile       = new File(p_testFileDir + File.separator + p_fileName);
        
        try
        {
            l_testFileWriter     = new PrintWriter(new BufferedWriter(new FileWriter(l_testFile, false)));
        }
        catch (IOException l_ioEx)
        {
            sayTime("***ERROR: createFraudBatchTestFile -- Failed to create a test file.");
            super.failedTestException(l_ioEx);
        }

        for (int l_ix = 0; l_ix < p_data.length; l_ix++)
        {
            l_testFileWriter.println(p_data[l_ix]);
        }
        l_testFileWriter.close();
               
        sayTime("createFraudReturnsTestFile -- '" + l_testFile.getName()  + "' is created!");
        
        return;
    }
    
    

    /***************************************************************************
     * Runs the misc. data upload batch.
     * <br>The following steps are performed:
     * <br>1. A <code>MiscDataUploadBatchController</code> object is created.
     * <br>2. The batch is started.
     * <br>3. Waits until the batch has finished with the expected status.
     * 
     * @param p_expectedExitStatus  the expected exit status.
     */
    private FraudReturnsBatchController runFraudReturnsBatch(int p_expectedExitStatus)
    {
        FraudReturnsBatchController l_fraudReturnsBatchController = null;

        // Create a 'MiscDataUploadBatchController' object.
        l_fraudReturnsBatchController = createFraudReturnsBatchController(i_batchProperties.getIndataDir());
        assertNotNull("***ERROR: Failed to create a MiscDataUploadBatchController object.",
                      l_fraudReturnsBatchController);
        sayTime("A FraudReturnsBatchController object is created.");

        // Set the number of processor threads to 1 in order to process the records in the same order that
        // they are written in the test file (otherwise are the records in the report and recovery files
        // inserted in an unpredicable way).
        i_batchProperties.setNoOfProcessorThreads(1);

        // Start the batch.
        try
        {
            startBatch(l_fraudReturnsBatchController);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to start the 'Fraud Returns' batch.");
            super.failedTestException(l_Ex);
        }

        sayTime("The Fraud Returns batch is started.");

        // Wait until the batch has finished, i.e. the exit status should be as expected.
        super.waitForExitStatus(l_fraudReturnsBatchController,
                                "Fraud Returns",
                                p_expectedExitStatus,
                                5000L,
                                36);
        sayTime("The batch has exit properly.");

        return l_fraudReturnsBatchController;
    }

    
    
    /***************************************************************************
     * Creates and returns a <code>FraudReturnsBatchController</code> object
     * using the given input data directory.
     * NB: If it fails to insert the test subscriber the complete test will fail
     *     (i.e. an <code>AssertionFailedError</code> will be thrown).
     * 
     * @param p_indataDirectory  the name of the indata directory.
     * 
     * @return a <code>FraudReturnsBatchController</code> object.
     */
    private FraudReturnsBatchController createFraudReturnsBatchController(String p_indataDirectory)
    {
        FraudReturnsBatchController l_fraudReturnsBatchController = null;
        HashMap                     l_batchParamsMap              = new HashMap();
        FraudReturnsBasicTestData   l_basicTestData               = null;
        l_batchParamsMap.put("inDirectoryName", p_indataDirectory);

        try
        {
            // Create an instance with "hardcoded basic test data"
            l_basicTestData = new FraudReturnsBasicTestData(c_ppasContext);
            c_ppasContext.setAttribute(FraudReturnsBasicTestData.C_KEY_BASIC_TEST,l_basicTestData);
            
            l_fraudReturnsBatchController =
                new FraudReturnsBatchController(CommonTestCaseTT.c_ppasContext,
                                                BatchConstants.C_JOB_TYPE_BATCH_FRAUD_RETURNS,
                                                  getJsJobID(),
                                                  "FraudUT",
                                                  l_batchParamsMap);
        }
        catch (PpasConfigException l_ppasConfigEx)
        {
            sayTime("***ERROR: Failed to create a FraudReturnsBatchController object.");
            super.failedTestException(l_ppasConfigEx);
        }
        catch (PpasSqlException l_ppasSqlEx)
        {
            sayTime("***ERROR: Failed to create a FraudReturnsBatchController object.");
            super.failedTestException(l_ppasSqlEx);
        }
        catch (PpasServiceException l_ppasServ)
        {
            sayTime("***ERROR: Failed to create a FraudReturnsBatchController object.");
            super.failedTestException(l_ppasServ);
            l_ppasServ.printStackTrace();
        }

        return l_fraudReturnsBatchController;
    }

    
    
    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /***************************************************************************
     * Sets up batch specific requirements.
     */
    protected void setUp()
    {
        super.setUp();

        if (i_batchProperties == null)
        {
            try
            {
                CommonTestCaseTT.c_properties =
                    CommonTestCaseTT.c_properties.getPropertiesWithAddedLayers("batch_vfr");
                i_batchProperties =
                    new BatchProperties(CommonTestCaseTT.c_properties, UtilTestCaseTT.c_logger);
            }
            catch (PpasConfigException l_ppasConfigEx)
            {
                sayTime("***ERROR: Failed to load batch properties during set-up.");
                super.failedTestException(l_ppasConfigEx);
            }
            catch (BatchConfigException l_batchConfigEx)
            {
                sayTime("***ERROR: Failed to load batch properties during set-up.");
                super.failedTestException(l_batchConfigEx);
            }
        }
    }
    
}
