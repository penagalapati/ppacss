////////////////////////////////////////////////////////////////////////////////
//  ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       BatchMessage.java
//  DATE            :       16-Apr-2004
//  AUTHOR          :       Lars Lundberg
//  REFERENCE       :       PpacLon#219/2129
//                          PRD_ASCS_DEV_SS_083
//
//  COPYRIGHT       :       Atos Origin 2004
//
//  DESCRIPTION     :       This class is a data container that holds information
//                          related to a worker thread.
//                          It is used as a communication object between a worker
//                          thread and a component when exchanging information.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY  | <name>     | <brief description of the       | <reference>
//        |            | changes>                        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchcommon;

import com.slb.sema.ppas.util.lang.ThreadObject;


/**
 * This class is a data container that holds information related to a worker thread.
 * It is used as a communication object between a worker thread and a component when exchanging information.
 */
public class ThreadControl
{
    //-------------------------------------------------------------------------
    // Public constants
    //-------------------------------------------------------------------------

    // Constants used to identify the different thread states.

    /** Constant used to identify an unknown thread state. */
    public static final int C_THREAD_UNKNOWN  = 0;

    /** The state of a running thread. */
    public static final int C_THREAD_RUNNING  = 1;

    /** The state of a paused thread. */
    public static final int C_THREAD_PAUSED   = 2;

    /** The state of a resumed thread. */
    public static final int C_THREAD_RESUMED  = 3;

    /** The state of a stopped thread. */
    public static final int C_THREAD_STOPPED  = 4;

    /** The state of a finished thread. */
    public static final int C_THREAD_FINISHED = 5;

    /** The state of an erroneous thread. */
    public static final int C_THREAD_ERROR    = 6;

    //-------------------------------------------------------------------------
    // Private variables
    //-------------------------------------------------------------------------

    /** The worker thread objecthat is, the object that encapsultes the real <code>Thread</code> object. */
    private ThreadObject i_workerThread = null;

    /** The worker tread's current state. */
    private int          i_state        = C_THREAD_UNKNOWN;

    /** Worker thread running indicator. Is set to <code>true</code> if the worker thread is running. */
    private boolean      i_running      = false;

    /** Worker thread paused indicator. Is set to <code>true</code> if the worker thread is paused. */
    private boolean      i_paused      = false;


    /**
     * The no-arg Constructor.
     */
    public ThreadControl()
    {
        super();
    }


    /** Check if this thread is paused.
     * @return True if this thread is paused, otherwise false.
     */
    public boolean isPaused()
    {
        return i_paused;
    }

    /** Check if this thread is running.
     * @return True if this thread is running, otherwise false.
     */
    public boolean isRunning()
    {
        return i_running;
    }

    /** Get the worker thread's current state.
     * @return Current state of the worker thread.
     */
    public int getState()
    {
        return i_state;
    }

    /** Get a reference to the worker thread.
     * @return Reference to the worker thread.
     */
    public ThreadObject getWorkerThread()
    {
        return i_workerThread;
    }

    /**
     * @param p_paused Flag for paused status.
     */
    public void setPaused( boolean p_paused )
    {
        i_paused = p_paused;
    }

    /**
     * @param p_running Flag for running status.
     */
    public void setRunning( boolean p_running )
    {
        i_running = p_running;
    }

    /**
     * @param p_state Current status.
     */
    public void setState( int p_state )
    {
        i_state = p_state;
    }

    /**
     * @param p_threadObject Reference to the thread object.
     */
    public void setWorkerThread( ThreadObject p_threadObject )
    {
        i_workerThread = p_threadObject;
    }
}
