////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       PromoAllocSynchBatchController.java
//DATE            :       Aug 31, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       This class is the main entry point for starting the 
//                        Promotion Plan Allocation Synchronisation batch.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.PromoAllocSynchBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.PromoAllocSynchBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.PromoAllocSynchBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class is the main entry point for starting the Promotion Plan Allocation Synchronisation batch.
 */
public class PromoAllocSynchBatchController extends BatchController
{
    //-------------------------------------------------------------------------
    //  Class level constant.
    //-------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME               = "PromoAllocSynchBatchController";

    /** The additional properties layer to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS      = "batch_pas";

    /** The name of the master job control table. */
    private static final String C_MASTER_JOB_CONTROL_TABLE = "BACO_BATCH_CONTROL";
    
    /** The name of the sub job control table. */
    private static final String C_SUB_JOB_CONTROL_TABLE    = "BSCO_BATCH_SUB_CONTROL";


    //-------------------------------------------------------------------------
    // Instance variables.
    //-------------------------------------------------------------------------
    /** The master job's js job id. */
    private String      i_masterJsJobId        = null;

//    /** The lowest customer id to be processed by this batch. */
//    private String      i_startId              = null;
//
//    /** The highest customer id to be processed by this batch. */
//    private String      i_endId                = null;

    /** The execution date and time for this batch. */
    private String      i_executionDateAndTime = null;
    
    /** The sub job id for this batch. */
    private String      i_subJobId             = null;


    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * @param p_ppasContext  A <code>PpasContext</code> object.
     * @param p_jobType      The job type (e.g. batchSynchPromo).
     * @param p_jobId        A unique id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_parameters   A <code>Map</code> object holding parameters to use when executing this job. 
     *                       Recovery   - "yes" or "no". Indicating rev�covery or not.
     *                       StartRange - Lowest customer id to disconnect.
     *                       EndRange   - Highest customer to disconnect.
     * 
     * @throws PpasConfigException If configuration data is missing or incomplete.
     * 
     */
    public PromoAllocSynchBatchController(PpasContext p_ppasContext,
                                          String      p_jobType,
                                          String      p_jobId,
                                          String      p_jobRunnerName,
                                          Map         p_parameters)
        throws PpasConfigException
    {
        super(p_ppasContext, p_jobType, p_jobId, p_jobRunnerName, p_parameters);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        this.init();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }
    } // End of constructor PromoAllocSynchBatchController(.....)


    //-------------------------------------------------------------------------
    // Protected instance methods.
    //-------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise this <code>PromoAllocSynchBatchController</code> instance.
     * 
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);        
        super.init();


        // Retrieve parameters from the Map
        i_masterJsJobId        = (String)super.i_params.get(BatchConstants.C_KEY_MASTER_JS_JOB_ID);
//        i_startId              = (String)super.i_params.get(BatchConstants.C_KEY_START_RANGE);
//        i_endId                = (String)super.i_params.get(BatchConstants.C_KEY_END_RANGE);
        i_executionDateAndTime = (String)super.i_params.get(BatchConstants.C_KEY_EXECUTION_DATE_TIME);
        i_subJobId             = (String)super.i_params.get(BatchConstants.C_KEY_SUB_JOB_ID);
        
//        // Always activate recovery info (using the batch sub job control table) since 
//        // this is a database driven batch.
//        super.activateRecovery();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10110,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }
        
        return;
        
    } // End of init()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates and returns the correct reader-class instance and passes on
     * the required parameters and also a reference to this controller. The later is used
     * for the reader-component to report status back to the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return  a reference to a PromoAllocSynchBatchReader instance.
     */
    public BatchReader createReader()
    {
        PromoAllocSynchBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10200,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createReader);
        }

        // Instantiate the reader and pass reference to controller
        l_reader = new PromoAllocSynchBatchReader(super.i_ppasContext,
                                                  super.i_logger,
                                                  this,
                                                  super.i_inQueue,
                                                  super.i_params,
                                                  super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10210,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
        return l_reader;
    } // End of createReader()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /**
     * Creates and returns the correct writer-class instance and passes on
     * the required parameters and also a reference to this controller. The later is used
     * for the writer-component to report status back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return  a reference to a PromoAllocSynchBatchWriter instance.
     * 
     * @throws IOException  if the writer fails to open the output file.
     */
    protected BatchWriter createWriter() throws IOException
    {
        PromoAllocSynchBatchWriter l_writer = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        // Instantiate the writer and pass reference to controller
        l_writer = new PromoAllocSynchBatchWriter(super.i_ppasContext,
                                                  super.i_logger,
                                                  this,
                                                  super.i_outQueue,
                                                  super.i_params,
                                                  super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        return l_writer;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";
    /**
     * Creates and returns the correct processor-class instance and passes on
     * the required parameters and also a reference to this controller. The later is used
     * for the processor-component to report status back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return  a reference to a PromoAllocSynchBatchProcessor instance.
     */
    protected BatchProcessor createProcessor()
    {
        PromoAllocSynchBatchProcessor l_processor = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        // Instantiate the processor and pass reference to controller
        l_processor = new PromoAllocSynchBatchProcessor(super.i_ppasContext,
                                                  super.i_logger,
                                                  this,
                                                  super.i_inQueue,
                                                  super.i_outQueue,
                                                  super.i_params,
                                                  super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10410,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }
        return l_processor;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * Updates the job status via the <code>PpasBatchControlService</code>.
     * Since this is a DB-driven batch this is done by calling the <code>updateSubJobStatus</code> method.
     * 
     * @param p_status  the status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10600,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        try
        {
              
            i_batchContService.updateSubJobStatus(null,
                                                  this.i_masterJsJobId,
                                                  new PpasDateTime(this.i_executionDateAndTime),
                                                  this.i_subJobId,
                                                  p_status,
                                                  C_MASTER_JOB_CONTROL_TABLE,
                                                  C_SUB_JOB_CONTROL_TABLE,
                                                  super.i_timeout);
        }
        catch (PpasServiceException p_ppasServiceExe)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10610,
                    this,
                    C_METHOD_updateStatus + " PpasServiceException from updateJobDetails");
            }
//            super.i_logger.logMessage(p_ppasServiceExe);
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10620,
                this,
                BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }
    } // End of updateStatus()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";        
    /**
     * Returns a BatchSubJobData-object into which the correct key-values are set (MasterJobId,
     * ExecutionDataAndTime and SubJobId).
     * The caller can then populate the returned record with the data needed for the operation in question.
     * 
     * @return  a BatchSubJobData-object into which the correct key-values are set.
     */   
    public BatchSubJobData getKeyedControlRecord()
    {
        BatchSubJobData l_subJobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10700,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getKeyedControlRecord);
        }

        l_subJobData = new BatchSubJobData();
        l_subJobData.setMasterJsJobId(this.i_masterJsJobId);
        l_subJobData.setExecutionDateTime(this.i_executionDateAndTime);
        l_subJobData.setSubJobId(this.i_subJobId);
                       
        if (PpasDebug.on)
        {
            PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10710,
                            this,
                            C_METHOD_getKeyedControlRecord + " -- BatchSubJobData: " + l_subJobData);

            PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10720,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedControlRecord);
        }
        return l_subJobData;
    } // End of getKeyedControlRecord()
}
