////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchRefillController.java 
//      DATE            :       10-Jul-2006
//      AUTHOR          :       Yang Liu
//      REFERENCE       :       PRD_ASCS00_GEN_CA_093
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//                      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME        | DESCRIPTION                         | REFERENCE
//----------+-------------+-------------------------------------+---------------
// 07/08/06 | M Erskine   | Code formatting and debugging.      | PpacLon#2479/9483
//----------+-------------+-------------------------------------+---------------
// 07/05/07 | L Lundberg  | Extends the super class             | PpacLon#3033/11459
//          |             | FileDrivenBatchController instead   |
//          |             | of  the BatchController class.      |
//          |             | Added methods:                      |
//          |             | getBatchJobType()                   |
//          |             | getValidFilenameRegEx()             |
//          |             | Removed method:                     |
//          |             | addControlInformation()             |
//----------+-------------+-------------------------------------+---------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;


import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.RefillBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.RefillBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.RefillBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * The entry point for "Batch Subscriber Installation". This batch is both pausable and stoppable this is
 * automatically indicated when the super class�s constructor is called. This is the only batch that uses the
 * "retry" queue if processing fails.
 */
public class RefillBatchController extends FileDrivenBatchController
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME           = "RefillBatchController";

    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS  = "batch_ref";


    /**
     * Constructs a RefillBatchController with the given parameters.
     * @param p_context A <code>PpasContext</code> object.
     * @param p_jobType The type of job (e.g. BatchInstall).
     * @param p_jobId The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params Holding parameters to use when executing this job.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     * @throws PpasServiceException If feature is not licensed.
     */
    public RefillBatchController(PpasContext p_context,
                                 String      p_jobType,
                                 String      p_jobId,
                                 String      p_jobRunnerName,
                                 Map         p_params) throws PpasConfigException, PpasServiceException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10001, this,
                            "Constructing " + C_CLASS_NAME);
        }

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();
        
        p_context.validateFeatureLicence(FeatureLicence.C_FEATURE_CODE_BATCH_REFILLS);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10002, this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialization.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10010, this,
                            "Enter " + C_METHOD_init);
        }

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10020, this,
                            "Leaving " + C_METHOD_init);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";

    /**
     * Creates a <code>RefillBatchReader</code>. This method instantiates the correct reader-class and
     * passes on the required parameters and also a reference to itself. The later is used for the
     * reader-component to report back the controller-component. The method is called from the super-class
     * inner class method doRun() when start is ordered.
     * @return The created RefillBatchReader.
     */
    public BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10030, this,
                            "Enter " + C_METHOD_createReader);
        }

        RefillBatchReader l_reader = null;
        if (super.i_startComponents)
        {
            l_reader = new RefillBatchReader(super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_inQueue,
                                             super.i_params,
                                             super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10040, this,
                            "Leaving " + C_METHOD_createReader);
        }

        return l_reader;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /**
     * Creates a <code>RefillBatchWriter</code>. This method instantiates the correct writer-class and
     * passes on the required parameters and also a reference to itself. The later is used for the
     * writer-component to report back the controller-component. The method is called from the super-class
     * inner class method doRun() when start is ordered.
     * @return The created RefillBatchWriter.
     * @throws IOException If there was some error when the writer was created.
     */
    public BatchWriter createWriter() throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10200, this,
                            "Enter " + C_METHOD_createWriter);
        }

        RefillBatchWriter l_writer = null;
        if (super.i_startComponents)
        {
            l_writer = new RefillBatchWriter(super.i_ppasContext,
                                             super.i_logger,
                                             this,
                                             super.i_params,
                                             super.i_outQueue,
                                             super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10210,
                            this,
                            "Leaving " + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /**
     * Creates a <code>RefillBatchProcessor</code>. This method instantiates the correct processor-class
     * and passes on the required parameters and also a reference to itself. The later is used for the
     * processor-component to report back the controller-component. The method is called from the super-class
     * inner class method doRun() when start is ordered. Note that only one instance of RefillBatchProcessor
     * is created irrelevant of the number of internal threads that the processor-component will consist of.
     * @return The created RefillBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10300, this,
                            "Enter " + C_METHOD_createProcessor);
        }

        RefillBatchProcessor p_processor = null;
        if (super.i_startComponents)
        {
            p_processor = new RefillBatchProcessor(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_inQueue,
                                                   super.i_outQueue,
                                                   super.i_params,
                                                   this.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10310, this,
                            "Leaving " + C_METHOD_createProcessor);
        }
        return p_processor;
    }

    /**
     * Returns the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected String getBatchJobType()
    {
        return BatchConstants.C_JOB_TYPE_BATCH_REFILL;
    }


    /**
     * This method should return a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected String getValidFilenameRegEx()
    {
        return "BATCH_REFILL_" + C_REG_EX_VALID_DEFAULT_FILENAME;


    }


    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * It updates the database with a new status when a batch have got the status finished (c), stopped (x) or
     * error (f). The metohd is called from the super class. The <code>BatchJobControlData</code>object used
     * in the method are first fetched form <code>getKeyedControlRecord()</code> and then the status is
     * inserted into it.
     * @param p_status The new status.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobControlData l_jobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10400, this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        l_jobData = getKeyedBatchJobControlData();
        l_jobData.setStatus(p_status.charAt(0));

        try
        {
            super.i_batchContService.updateJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10410, this,
                                C_METHOD_updateStatus + " PpasServiceException from updateStatus");
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10420, this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }

        return;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10500, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10510, this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }
}

