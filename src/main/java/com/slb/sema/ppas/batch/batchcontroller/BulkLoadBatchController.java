////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BulkLoadBatchController
//      DATE            :       27-July-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.BulkLoadBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.BulkLoadBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.BulkLoadBatchWriter;
import com.slb.sema.ppas.batch.exceptions.BatchInvalidParameterException;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;

/** This class is the main entry point for starting Batch Bulk Load.
 * Typically it is instantiated by the JobRunner.
 */
public class BulkLoadBatchController extends BatchController
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME          = "BulkLoadBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_bkl";

    /** Master batch control table. Value is {@value}. */
    private static final String C_MASTER_TABLE_NAME   = "BACO_BATCH_CONTROL";
    
    /** Sub-batch control table. Value is {@value}. */
    private static final String C_SUB_TABLE_NAME      = "BSCO_BATCH_SUB_CONTROL";

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** Master job scheduler job identifier. */
    private String      i_masterJsJobId        = null;
    /** Sub job scheduler job identifier. */
    private String      i_subJobId             = null;
    /** Execution date/time of process. */
    private PpasDateTime i_executionDateAndTime = null;

//    /** Current date/time. */
//    private String      i_currentDateAndTime   = null;
//    /** Job scheduler job identifier. */
//    private String      i_jsJobId              = null;
    /** Session information. */
    private PpasSession i_ppasSession          = null;
    /** Request information. */
    private PpasRequest i_ppasRequest          = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * @param p_context      A <code>PpasContext</code> object.
     * @param p_jobType      The type of job (e.g. BatchInstall).
     * @param p_jobId        The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_parameters   A Map holding parameters to use when executing this job. 
     *                       Recovery                 "yes" or "no". Indicating rev�covery or not.
     *                       StartRange               Lowest customer id to disconnect.
     *                       EndRange                 Highest customer to disconnect.
     *                       DisconnectionDate        Use current date if not supplied.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     */
    public BulkLoadBatchController(
        PpasContext      p_context,
        String           p_jobType,
        String           p_jobId,
        String           p_jobRunnerName,
        Map              p_parameters)
        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_parameters);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10500,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
//        i_jsJobId            = p_jobId;
//        i_currentDateAndTime = DatePatch.getDateTimeNow().toString();

        // The i_ppasRequest used in addControlInformation()
        i_ppasSession          = new PpasSession();
        i_ppasSession.setContext(p_context);
        i_ppasRequest          = new PpasRequest(i_ppasSession);

        init(p_parameters);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }
        return;
        
    } // End of constructor BulkLoadBatchController(.....)

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * This method will instantiate the correct reader-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the reader-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a BulkLoadBatchReader.
     */
    public BatchReader createReader()
    {
        BulkLoadBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createReader);
        }

        // Instantiate the relevant type of reader and pass reference to controller
        l_reader = new BulkLoadBatchReader( super.i_ppasContext,
                                          super.i_logger,
                                          this,
                                          super.i_inQueue,
                                          super.i_params,
                                          super.i_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createReader);
        }

        return l_reader;
        
    } // End of createReader()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";
    /**
     * This method will instantiate the correct writer-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the writer-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a BulkLoadBatchWriter.
     * @throws IOException If the writer cannot be created.
     */
    public BatchWriter createWriter() throws IOException
    {
        BulkLoadBatchWriter l_writer = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        l_writer = new BulkLoadBatchWriter( super.i_ppasContext,
                                          super.i_logger,
                                          this,
                                          super.i_params,
                                          super.i_outQueue,
                                          super.i_properties );
                                          
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }

        return l_writer;
        
    } // End of createWriter()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a BulkLoadBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        BulkLoadBatchProcessor l_processor = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        l_processor = new BulkLoadBatchProcessor( super.i_ppasContext,
                                                super.i_logger,
                                                this,
                                                super.i_inQueue,
                                                super.i_outQueue,
                                                super.i_params,
                                                super.i_properties);
                                                  
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }

        return l_processor;
        
    } // End of createProcessor()

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @param p_parameters Map of input parameters.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init(
        Map                           p_parameters)
        throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                82601,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);        
        super.init();


        // Retrieve parameters from the Map
        try
        {
            validateBulkLoadBatchParameters(p_parameters);
        }
        catch (BatchInvalidParameterException l_batchInvalidParameterException)
        {
            i_logger.logMessage(l_batchInvalidParameterException);
            throw new PpasConfigException(
                C_CLASS_NAME,
                C_METHOD_init,
                12311,
                this,
                null,
                0,
                ConfigKey.get().missingConfigDataInMap(),
                l_batchInvalidParameterException);
        }

        getControllerParameters(p_parameters);

//        // Activate recovery info (using the batch sub job control table) since 
//        // this is a database driven batch.
//        super.activateRecovery();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                82602,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }
    } // End of init()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
     * This method is responsible to insert a record into table BACO using the defined
     * service method PpasBatchCOntrolService.addControlInformation(). The method will
     * get a BatchJobData object and populate it with required parameters.
     * Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10380,
                this,
                BatchConstants.C_CONSTRUCTING + C_METHOD_addControlInfo + 
                " NOT IMPLEMENTED FOR DB_driven batch!");
        }
     
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_CONSTRUCTED + C_METHOD_addControlInfo);
        }
        
    } // End of addControlInformation()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * This method call the service defined by PpasBatchControlService. In this case
     * for a filedriven  updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        try
        {
            i_batchContService.updateSubJobStatus( i_ppasRequest, 
                                                   this.i_masterJsJobId,             // master l_jobData
                                                   this.i_executionDateAndTime, // l_jobData 
                                                   this.i_subJobId,             // sub job id l_jobData
                                                   p_status,                    // passed parameter
                                                   C_MASTER_TABLE_NAME,
                                                   C_SUB_TABLE_NAME,
                                                   super.i_timeout );
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10410,
                    this,
                    C_METHOD_addControlInfo + " PpasServiceException from updateJobDetails");
            }

            e.printStackTrace();
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10420,
                this,
                BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }
    } // End of updateStatus()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";        
    /**
     * This method will return a BatchSubJobData-object with the correct key-values set 
     * (MasterJobId, ExecutionDataAndTime and SubJobId).  The caller can then populate the record with data
     * needed for the operation in question.  
     * @return Control record details.
     */   
    public BatchSubJobData getKeyedControlRecord()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10430,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getKeyedControlRecord +
                " ... prepare with the database keys");
        }

        BatchSubJobData l_jobData = new BatchSubJobData();

        l_jobData.setMasterJsJobId(i_masterJsJobId);
        l_jobData.setExecutionDateTime(i_executionDateAndTime.toString());
        l_jobData.setSubJobId(i_subJobId);
                       
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10450,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedControlRecord +
                " MasterId=" + i_masterJsJobId + " ExecutionDateAndTime=" + i_executionDateAndTime +
                " SubJobId=" + i_subJobId);
        }

        return l_jobData;
    } // End of getKeyedControlRecord()

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateBulkLoadBatchParameters = "validateBulkLoadBatchParameters";
    /**
     * Validates the input parameters to the Bulk Load Batch Program. Throws an exception
     * if a parameter is missing or invalid.
     * @param p_parameters Map of input parameters.
     * @throws BatchInvalidParameterException A parameter is missing or invalid.
     */
    private void validateBulkLoadBatchParameters(
        Map                           p_parameters)
        throws BatchInvalidParameterException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11100, this,
                "Entered " + C_METHOD_validateBulkLoadBatchParameters);
        }

        validateMandatoryParameter(
            BatchConstants.C_KEY_START_RANGE,
            BatchConstants.C_PATTERN_NUMERIC,
            p_parameters);

        validateMandatoryParameter(
            BatchConstants.C_KEY_END_RANGE,
            BatchConstants.C_PATTERN_NUMERIC,
            p_parameters);

        validateMandatoryParameter(
            BatchConstants.C_KEY_RECOVERY_FLAG,
            BatchConstants.C_PATTERN_YES_OR_NO,
            p_parameters);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11190, this,
                "Leaving " + C_METHOD_validateBulkLoadBatchParameters);
        }

    } // end of validateBulkLoadBatchParameters

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getControllerParameters = "getControllerParameters";
    /** Gets the parameters use by the Controller from the Hashmap.
     * @param p_parameters Map of input parameters.
     */
    private void getControllerParameters( Map p_parameters )
    {
        if (PpasDebug.on) 
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10200,
                this,
                "Entered " + C_METHOD_getControllerParameters);
        }

        i_masterJsJobId        = (String)p_parameters.get(BatchConstants.C_KEY_MASTER_JS_JOB_ID);
        i_executionDateAndTime = new PpasDateTime((String)p_parameters.get(
                                     BatchConstants.C_KEY_EXECUTION_DATE_TIME));
        i_subJobId             = (String)p_parameters.get(BatchConstants.C_KEY_SUB_JOB_ID);

        if (PpasDebug.on) 
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10290,
                this,
                "Leaving " + C_METHOD_getControllerParameters);
        }

    } // end of getControllerParameters
} // End of class BulkLoadBatchController
