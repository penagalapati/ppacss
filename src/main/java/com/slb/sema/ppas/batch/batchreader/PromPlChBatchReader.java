////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromPlChBatchReader
//      DATE            :       03-Augil-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchFileErrorCodeException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanData;
import com.slb.sema.ppas.common.businessconfig.dataclass.PrplPromoPlanDataSet;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.PromPlChBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasBusinessConfigService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to read promotion plan updates from a flat file.
 * The actual read operation from tile is hidden within the super class so this
 * class operates on strings that origins from the file. For each read order (line)
 * a PromPlChBatchDataRecord is created and populated.
 */
public class PromPlChBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                  = "PromPlChBatchReader";
    
    // Error Codes.

    /** Error code. Value is {@value}. */
    public static final String C_ERROR_INVALID_RECORD_FORMAT   = "01";

    /** Error code. Value is {@value}. */
    public static final String C_ERROR_INVALID_MSISDN          = "02";
               
    /** Error code. Value is {@value}. */
    public static final String C_ERROR_INVALID_DATES           = "03";
               
    /** Error code. Value is {@value}. */
    public static final String C_ERROR_INVALID_PROMO_PLAN_CODE = "04";
               
    // Field Lengths

    /** Field length of MSISDN to be updated. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH                = 15;
    
    /** Field length of Promotion Plan Code. Value is {@value}. */
    private static final int    C_PROMO_PLAN_CODE_LENGTH       = 4;
    
    /** Field length of Promotion Start Date. Value is {@value}. */
    private static final int    C_PROMO_PLAN_START_DATE_LENGTH = 8;
    
    /** Field length of Promotion End Date. Value is {@value}. */
    private static final int    C_PROMO_PLAN_END_DATE_LENGTH   = 8;
    
    // Extra Constants

    /** Value for end date in input file, that indicates to use the default
     *  allocation period for the new promotion insert/update.
     */
    private static final String C_DEFAULT_END_DATE = "00000000";

    /** Time partition to add to the batch start date. */
    private static final String C_BATCH_START_TIME = " 00:00:00";

    /** Time partition to add to the batch end date. */
    private static final String C_BATCH_END_TIME   = " 23:59:59";


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /**
     *  To hold the valid promotion plan codes.
     */
    private PrplPromoPlanDataSet i_prplPromoPlanDataSet = null;
   
    /**
     * Constructor for PromPlChBatchReader, which sets up the promotion data
     * for the processer.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public PromPlChBatchReader( PpasContext      p_ppasContext,
                               Logger           p_logger,
                               BatchController  p_controller,
                               SizedQueue       p_queue,
                               Map              p_parameters,
                               PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        i_prplPromoPlanDataSet = getValidPromotionPlanCodes(
            p_logger,
            p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;
        
    } // end of public Constructor PromPlChBatchReader(.....)


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        PromPlChBatchRecordData l_promPlChBatchRecordData = null;
        int l_currentPosition = 0;
        String l_errorCode = null;
        Msisdn l_msisdn = null;
        String l_promoPlanCode = null;
        PpasDateTime l_promoStartDateTime = null;
        PpasDateTime l_promoEndDateTime = null;
        PpasDateTime l_promoExpiryDateTime = null;
        String l_custId = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10340,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getRecord);
        }

        if (this.readNextLine() != null)
        {
            l_promPlChBatchRecordData = new PromPlChBatchRecordData(super.i_inputFileName,
                                                                    super.i_lineNumber,
                                                                    super.i_inputLine,
                                                                    false);

            try
            {
                // Remove line number
                l_errorCode = C_ERROR_INVALID_RECORD_FORMAT;
                validateRecordLength(super.i_inputLine, 
                                     C_MSISDN_LENGTH
                                     + C_PROMO_PLAN_CODE_LENGTH
                                     + C_PROMO_PLAN_START_DATE_LENGTH
                                     + C_PROMO_PLAN_END_DATE_LENGTH);

                l_errorCode = C_ERROR_INVALID_MSISDN;
                l_currentPosition = 0;
                l_msisdn = validateMsisdnField(super.i_inputLine.substring(l_currentPosition,
                                                                           l_currentPosition
                                                                           + C_MSISDN_LENGTH));

                l_errorCode = C_ERROR_INVALID_PROMO_PLAN_CODE;
                l_currentPosition = l_currentPosition + C_MSISDN_LENGTH;
                l_promoPlanCode = validateAlphaNumericField(super.i_inputLine
                                                            .substring(l_currentPosition, 
                                                                       l_currentPosition 
                                                                       + C_PROMO_PLAN_CODE_LENGTH));

                l_errorCode = C_ERROR_INVALID_DATES;
                l_currentPosition = l_currentPosition + C_PROMO_PLAN_CODE_LENGTH;
                l_promoStartDateTime = validateStartDateField(super.i_inputLine
                                                              .substring(l_currentPosition, 
                                                                         l_currentPosition 
                                                                         + C_PROMO_PLAN_START_DATE_LENGTH));

                l_currentPosition = l_currentPosition + C_PROMO_PLAN_START_DATE_LENGTH;
                l_promoEndDateTime = validateEndDateField(super.i_inputLine
                                                          .substring(l_currentPosition, 
                                                                     l_currentPosition 
                                                                     + C_PROMO_PLAN_END_DATE_LENGTH));

                l_errorCode = C_ERROR_INVALID_MSISDN;
                l_custId = validateMasterMsisdn(l_msisdn);

                l_errorCode = C_ERROR_INVALID_PROMO_PLAN_CODE;
                l_promoExpiryDateTime = validatePromPlanCode(l_promoPlanCode);

                // If the promotion end date from files equals
                // PromPlChBatchReader.C_DEFAULT_END_DATE,
                // use the promotion expiry date for end date.
                if (l_promoEndDateTime == null)
                {
                    l_promoEndDateTime = l_promoExpiryDateTime;
                }

                l_promPlChBatchRecordData.setMsisdn(l_msisdn);
                l_promPlChBatchRecordData.setCustId(l_custId);
                l_promPlChBatchRecordData.setPromoPlanCode(l_promoPlanCode);
                l_promPlChBatchRecordData.setPromoStartDateTime(l_promoStartDateTime);
                l_promPlChBatchRecordData.setPromoEndDateTime(l_promoEndDateTime);
                
                l_promPlChBatchRecordData.setNumberOfSuccessfullyProcessedRecords(
                    super.i_successfullyProcessed);
                l_promPlChBatchRecordData.setNumberOfErrorRecords(super.i_notProcessed);
                if ( super.faultyRecord(i_lineNumber))
                {
                    l_promPlChBatchRecordData.setFailureStatus(true);
                }

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10340,
                        this,
                        "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
                }

            }
            catch (BatchFileErrorCodeException l_batchFileErrorCodeException)
            {
                l_promPlChBatchRecordData.setErrorCode(l_errorCode);
            }
        }

        return l_promPlChBatchRecordData;

    } // end of method getRecord()
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form.
     * @param   p_filename  The filename.
     * @return  <code>true</code> if filename is valid else <code>false</code>.
     */
    public boolean isFileNameValid(String p_filename)
    {
        boolean l_validFileName = true; 

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10110,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if ( ( p_filename.matches( BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_DAT ) ) ||
             ( p_filename.matches( BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_IPG ) ) )
        {
            // Correct filename format with extension .DAT or .IPG. 
            // Rename the .DAT file to .IPG to indicate that processing is in progress.
            // If it is recovery mode, the file already got the "in_progress_extension"
            if ( !this.i_recoveryMode )
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                                  
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10480,
                        this,
                        "After rename, inputFullPathName = " + i_fullPathName );
                }
            }
        }
        else if ( p_filename.matches( BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_SCH ) )
        {
            // Correct filename format with extension .SCH. 
            // Rename the .SCH file to .IPG to indicate that processing is in progress.
            if ( !this.i_recoveryMode )
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                                  
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10481,
                        this,
                        "After rename, inputFullPathName = " + i_fullPathName );
                }
            }
        }
        else
        {
            l_validFileName = false;
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_CONFIN_START,
                                 C_CLASS_NAME,
                                 10130,
                                 this,
                                 "FileName (" + p_filename + ") is invalid" );
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10140,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
    }

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateStartDateField = "validateStartDateField";

    /**
     * Converts a string in the YYYYMMDD format to a PpasDate. Throws an exception
     * if cannot be converted.
     * @param p_dateString Date string in the YYYYMMDD format.
     * @return Date as a PpasDate.
     * @throws BatchFileErrorCodeException Cannot be converted to PpasDate.
     */
    protected PpasDateTime validateStartDateField(
        String                        p_dateString)
        throws BatchFileErrorCodeException
    {
        PpasDateTime                   l_ppasDateTime = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11400, this,
                "Entered " + C_METHOD_validateStartDateField);
        }

        l_ppasDateTime = validateDateField(p_dateString + C_BATCH_START_TIME);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11490, this,
                "Leaving " + C_METHOD_validateStartDateField);
        }

        return l_ppasDateTime;
    } // end of validateStartDateField

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateEndDateField = "validateEndDateField";

    /**
     * Converts a string in the YYYYMMDD format to a PpasDate. Throws an exception
     * if cannot be converted. If the string equal PromPlChBatchReader.C_DEFAULT_END_DATE,
     * returns null.
     * @param p_dateString Date string in the YYYYMMDD format.
     * @return Date as a PpasDate.
     * @throws BatchFileErrorCodeException Cannot be converted to PpasDate.
     */
    protected PpasDateTime validateEndDateField(
        String                        p_dateString)
        throws BatchFileErrorCodeException
    {
        PpasDateTime                   l_ppasDateTime = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11400, this,
                "Entered " + C_METHOD_validateEndDateField);
        }

        if ( !p_dateString.equals(C_DEFAULT_END_DATE) )
        {
            l_ppasDateTime = validateDateField(p_dateString + C_BATCH_END_TIME);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11490, this,
                "Leaving " + C_METHOD_validateEndDateField);
        }

        return l_ppasDateTime;
    } // end of validateEndDateField


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getValidPromotionPlanCodes = "getValidPromotionPlanCodes";

    /**
     * Returns a set of valid promotion plan codes from the database.
     * @param p_logger   The logger
     * @param p_context  The context
     * @return Set of valid promotion plan codes.
     */
    private PrplPromoPlanDataSet getValidPromotionPlanCodes(
                                      Logger       p_logger,
                                      PpasContext  p_context )
    {
        PpasBusinessConfigService      l_ppasBusinessConfigService = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11500, this,
                "Entered " + C_METHOD_getValidPromotionPlanCodes);
        }

        try
        {
            l_ppasBusinessConfigService = new PpasBusinessConfigService(
                null,
                p_logger,
                p_context);
        }
        catch (PpasServiceFailedException l_pSFE)
        {
            getController().doRequestStop();
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VHIGH,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME, 11501, this,
                    C_METHOD_getValidPromotionPlanCodes + " PpasServiceFailedException caugth : " +
                    l_pSFE.getMsgKey() + " " + l_pSFE.getMessage());
            }

        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11590, this,
                "Leaving " + C_METHOD_getValidPromotionPlanCodes);
        }

        return l_ppasBusinessConfigService.getAvailablePromoPlans();
    } // end of getValidPromotionPlanCodes

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validatePromPlanCode = "validatePromPlanCode";

    /**
     * Validates the Promotion Plan Code on the database. Throws an exception if plan
     * is invalid or expired.
     * @param p_promoPlanCode Promotion Plan Code to be validated.
     * @return The default expiry date of the promotion plan.
     * @throws BatchFileErrorCodeException Invalid Promotion Plan Code.
     */
    private PpasDateTime validatePromPlanCode(
        String                        p_promoPlanCode)
        throws BatchFileErrorCodeException
    {
        PrplPromoPlanData              l_prplPromoPlanData = null;
        PpasDateTime                   l_ppasDateTime = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11600, this,
                "Entered " + C_METHOD_validatePromPlanCode);
        }

        l_prplPromoPlanData = i_prplPromoPlanDataSet.getWithCode(p_promoPlanCode);

        if ( l_prplPromoPlanData != null )
        {
            l_ppasDateTime = l_prplPromoPlanData.getEndDateTime();
        }
        else
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validatePromPlanCode,
                    20040,
                    this,
                    null,
                    0,
                    BatchKey.get().batchPromIdNotRecognised(p_promoPlanCode));
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11690, this,
                "Leaving " + C_METHOD_validatePromPlanCode);
        }

        return l_ppasDateTime;
    } // end of validatePromPlanCode

}

