////////////////////////////////////////////////////////////////////////////////
//ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       SdpRpcResponseUT.java
//DATE            :       23-Jul-2004
//AUTHOR          :       Alan Barrington-Hughes
//REFERENCE       :       PRD_ASCS00_DEV_SS_83
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       JUnit Test for the Synchronisation batch.     
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.PromPlChBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.PpasConfigException;
/** 
* Class to test the Synchronization of Recharge and Refill class.
*/
public class PromPlChBatchUT extends BatchTestCaseTT
{
    /** Flag to show if the object has been initialized or not. */
    boolean i_init = false;
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as it's argument
     *
     * @param   title The testcase name.
     */
    public PromPlChBatchUT (String title)
    {
        super(title);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        TestRunner.run(suite());
    }

    /** Static method that allows the framework to
     * to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes
     * calling this suite method and get an instance of the TestCase which it then
     * executes. See the main method in class for an example.
     * @return a suite of tests to execute
     */
    public static Test suite()
    {
        return new TestSuite(PromPlChBatchUT.class);
    }

    /**
     * Initialization.
     * @throws Exception Any error that might happen.
     */
    private void init() throws Exception
    {
        //Insert any specific initialisation for this batch
        i_init = true;
    }
    /** This method is used to setup anything required by each test. 
     */
    protected void setUp()
    {
        super.setUp();
        try 
        {
            init();
        }
        catch (Exception l_e) 
        {
            l_e.printStackTrace();
            fail("Intialization failed : " + l_e.getMessage());
        }
    } // end of setUp

    /** This method is called after each test is run.
     */
    protected void tearDown()
    {
        // nothing
    } // end of tearDown

    /** xxxtestInstall. */
    public void xxxtestInstall()
    {
        BasicAccountData    l_sub               = null;

        try
        {
            for  ( int j = 0; j < 10 ; j++ )
            {
                l_sub = c_dbService.installTestSubscriber(null);
            }
        }
        catch (PpasServiceException e)
        {
            fail("MDU Creating subscriber:" + e.getMessage());
        }
        
    }

    /**
     * Test promotion.
     */
    public void testPromotion()
    {
        HashMap l_map = new HashMap();
        l_map.put(BatchConstants.C_KEY_INPUT_FILENAME, "PALLOAD_20040615_00003.DAT");
        l_map.put(BatchConstants.C_KEY_BATCH_JOB_TYPE, BatchConstants.C_JOB_TYPE_BATCH_PROMOTION_PLAN_CHANGE);

        try
        {
            PromPlChBatchController l_inst = new PromPlChBatchController(
                    c_ppasContext,
                    "SUB_INST",
                    getJsJobID(),
                    "JS_RUN_A1001",
                    l_map);
            l_inst.doRequestStart();
            Thread.sleep(30000);
        }
        catch (PpasConfigException e)
        {
            fail("Retrieving Configuration");
        }
        catch (PpasSqlException e)
        {
            fail("Getting Dummy Js Job ID");
        }
        catch (InterruptedException e)
        {
            // ignore
        }
    }
}
