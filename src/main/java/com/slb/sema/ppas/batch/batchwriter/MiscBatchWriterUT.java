////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchWriterUT.java 
//      DATE            :       23-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Unit test for MiscBatchWriter
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars Lundberg | Method createContext(...) is     | PpacLon#3033/11279
//          |               | modified:                        |
//          |               | The MiscBatchController          |
//          |               | constructor does not throw any   |
//          |               | IOException anymore.             |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.MiscBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.MiscBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**Unit test for MiscBatchWriter. */

public class MiscBatchWriterUT extends BatchTestCaseTT
{

    /** Reference to MiscBatchWriter object. */
    private static MiscBatchWriter     c_miscBatchWriter;

    /** Reference to MiscBatchController object. */
    private static MiscBatchController c_miscBatchController;

    /**A short file name vid extension .DAT. */    
    private static final String C_FILE_SHORT_NAME_1_DAT = "UPDATE_MISC_20040702_00001.DAT";
    /**A short file name vid extension .DAT. */    
    private static final String C_FILE_SHORT_NAME_2_DAT = "UPDATE_MISC_20040702_00002.DAT";
    
    /**A short file name vid extension .TMP. */   
    private static final String C_FILE_SHORT_NAME_1_RPT = "UPDATE_MISC_20040702_00001.TMP";
    /**A short file name vid extension .TMP. */   
    private static final String C_FILE_SHORT_NAME_2_RPT = "UPDATE_MISC_20040702_00002.TMP";

    /** The type of batch. */
    private static final String C_BATCH_TYPE          = "MiscBatchWriterUT";


    /**
     * The constructor for the class MiscBatchWriterUT.
     * @param p_name Name of the test.
     */
    public MiscBatchWriterUT(String p_name)
    {
        super(p_name, "batch_mdu");
    }

    /** Tests getRecord(). */
    public void testGetRecord()
    {
        BatchRecordData l_dataRecord   = null;
        boolean         l_assertResult = false;

        super.beginOfTest("testGetRecord");

        createContext(C_FILE_SHORT_NAME_1_DAT);
        
        try
        {
            l_dataRecord = c_miscBatchWriter.getRecord();

            if (l_dataRecord != null && (l_dataRecord instanceof MiscBatchRecordData))
            {
                l_assertResult = true;

            }
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }

        assertTrue("testGetRecord failed", l_assertResult);

        super.endOfTest();
    }

    /** Tests both openFile(...) and fileExist(...). */
    public void testOpenFileAndFileExists()
    {
        super.beginOfTest("testOpenFileAndFileExists");
        String l_directory =
            c_miscBatchWriter.i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        try
        {
            c_miscBatchWriter.openFile(
                "11",
                new File(l_directory + "/" + C_FILE_SHORT_NAME_1_DAT));
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        //If the assert method don't fails then the openFile and fileExist metohds works.
        assertTrue(c_miscBatchWriter.fileExists("11"));

        super.endOfTest();
    }

    /** Tests fileExist(...) when the file don't exist. */
    public void testFileExists()
    {
        super.beginOfTest("testFileExists");

        assertFalse(c_miscBatchWriter.fileExists("12"));

        super.endOfTest();
    }    

    /**
     * Test of writing to file.
     */
    public void testWriteToFile()
    {
        MiscBatchRecordData l_dataRecord = new MiscBatchRecordData();

        String l_errorLine    = "2 ERR";
        String l_recoveryLine = "1 REC";
        String l_actualText    = null;

        try
        {
            l_dataRecord.setErrorLine    (l_errorLine);
            l_dataRecord.setRecoveryLine((l_recoveryLine));

            // Write to the files
            //c_miscBatchWriter.writeRecord(l_dataRecord);
            
            c_miscBatchWriter.writeToFile("report",    l_dataRecord.getErrorLine());
            c_miscBatchWriter.writeToFile("recovery", l_dataRecord.getRecoveryLine());
            
            // Close the files after writing.
            c_miscBatchWriter.tidyUp();

            l_actualText = super.getValueFromLogFile(
                                    C_FILE_SHORT_NAME_1_RPT,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_REPORT_FILE_DIRECTORY);

            if (l_actualText == "No file found")
            {
                fail("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE); 
            }
            
            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);
            
//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            l_actualText = super.getValueFromLogFile(
//                                    C_FILE_SHORT_NAME_1_RCV,
//                                    BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                    BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//            
//            if(l_actualText == "No file found")
//            {
//                fail("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE); 
//            } 
//            
//            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }
        
        super.endOfTest();
    }

    /** Test writeRecord() when an error occured. */
    public void testWriteRecordError()
    {
        MiscBatchRecordData l_dataRecord = new MiscBatchRecordData();

        String l_errorLine    = "x ERRORLINE";
        String l_recoveryLine = "x RECOVERYLINE";
        String l_inputLine    = "x INPUTLINE";
        String l_actualText   = null;
        String l_expectedText = null;

        try
        {
            l_dataRecord.setErrorLine   (l_errorLine);
            l_dataRecord.setRecoveryLine(l_recoveryLine);
            l_dataRecord.setInputLine   (l_inputLine);

            // Use file #2
            createContext(C_FILE_SHORT_NAME_2_DAT);

            // Write a record.
            try
            {
            c_miscBatchWriter.writeRecord(l_dataRecord);
            }
            catch(PpasServiceException e)
            {
                super.failedTestException(e);
            }
            
            // Close the files after writing.
            c_miscBatchWriter.tidyUp();

            // ----------------
            // Check ERROR file
            // ----------------
            l_expectedText = l_errorLine;
            l_actualText = super.getValueFromLogFile(
                                    C_FILE_SHORT_NAME_2_RPT,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_REPORT_FILE_DIRECTORY);

            if (l_actualText == "No file found")
            {
                fail("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE); 
            }
            
            assertEquals("Failure: Wrong data in the error file", l_expectedText, l_actualText);
            
//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            // -------------------
//            // Check RECOVERY file
//            // -------------------
//            l_expectedText = l_recoveryLine;
//            l_actualText = super.getValueFromLogFile(
//                                    C_FILE_SHORT_NAME_2_RCV,
//                                    BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                    BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//            
//            if(l_actualText == "No file found")
//            {
//                fail("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE); 
//            } 
//            
//            assertEquals("Failure: Wrong data in the recovery file", l_expectedText, l_actualText);
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }
        
        super.endOfTest();
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(MiscBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }

    
    
    /**
     * Creates the test envirironment.
     * @param p_filename Input file name.
     */
    private void createContext(String p_filename)
    {
        SizedQueue l_outQueue = null;
        MiscBatchRecordData l_batchRecordData = new MiscBatchRecordData();
                        
        try
        {
            l_outQueue =
                new SizedQueue(
                    "BatchWriter",
                    1,
                    null);

            l_outQueue.addFirst(l_batchRecordData);     
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        HashMap p_params = new HashMap();
        p_params.put(BatchConstants.C_KEY_INPUT_FILENAME,   p_filename);

        try
        {
            c_miscBatchController =
                new MiscBatchController(
                    super.c_ppasContext,
                    C_BATCH_TYPE,
                    super.getJsJobID(),
                    "JS_RUN_A1001",
                    p_params);
           
            c_miscBatchWriter =
                new MiscBatchWriter(
                    super.c_ppasContext,
                    super.c_logger,
                    c_miscBatchController,
                    p_params,
                    l_outQueue,
                    super.c_properties);    
                    
        }
        catch (PpasConfigException e)
        {
            super.failedTestException(e);
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }

    }

}
