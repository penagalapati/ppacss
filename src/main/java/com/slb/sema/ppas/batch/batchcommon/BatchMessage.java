////////////////////////////////////////////////////////////////////////////////
//  ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       BatchMessage.java
//  DATE            :       16-Apr-2004
//  AUTHOR          :       Lars Lundberg
//  REFERENCE       :       PpacLon#219/2129
//                          PRD_ASCS_DEV_SS_083
//
//  COPYRIGHT       :       Atos Origin 2004
//
//  DESCRIPTION     :       This class represents a communication object for
//                          control of a batch process. 
//
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
//DD/MM/YY  | <name>     | <brief description of the       | <reference>
//        |            | changes>                        |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchcommon;


/**
 * This class represents a communication object for control of a batch process.
 */
public class BatchMessage
{
    //-------------------------------------------------------------------------
    // Public constants
    //-------------------------------------------------------------------------

    // Constants used to identify the different components recognized by the batch process.

    /** Constant used to identify the reader component. */
    public static final int C_COMPONENT_READER    = 0;

    /** Constant used to identify the writer component. */
    public static final int C_COMPONENT_WRITER    = 1;

    /** Constant used to identify the processor component. */
    public static final int C_COMPONENT_PROCESSOR = 2;

    /** Constant used to refer to all components. */
    public static final int C_COMPONENT_ALL       = 3;

    /** Constant used to identify the external client. */
    public static final int C_COMPONENT_CLIENT    = 4;

    /** Constant used to identify the manager component. */
    public static final int C_COMPONENT_MANAGER   = 5;

    /** Constant used to identify an unknown component. */
    public static final int C_COMPONENT_UNKNOWN   = 6;


    // Constants used to describe all valid actions.

    /** Constant indicating that start is requested. */
    public static final int C_REQUEST_START       = 0;

    /** Constant indicating that stop is requested. */
    public static final int C_REQUEST_STOP        = 1;

    /** Constant indicating that pause is requested. */
    public static final int C_REQUEST_PAUSE       = 2;

    /** Constant indicating that resume is requested. */
    public static final int C_REQUEST_RESUME      = 3;

    /** Constant indicating that a message should be sent to a client. */
    public static final int C_REQUEST_REPORT      = 4;

    /** Constant indicating that a status change has occurred. */
    public static final int C_REQUEST_STATUS      = 5;

    /** Constant indicating that no action is yet requested. */
    public static final int C_REQUEST_NONE        = 6;


    // Constants representing the different statuses.
    
    /** Constant indicating status = STARTED. */
    public static final int C_STATUS_STARTED      = 0;

    /** Constant indicating status = STOPPED. */
    public static final int C_STATUS_STOPPED      = 1;

    /** Constant indicating status = PAUSED. */
    public static final int C_STATUS_PAUSED       = 2;

    /** Constant indicating status = RESUMED. */
    public static final int C_STATUS_RESUMED      = 3;

    /** Constant indicating status = FINISHED. */
    public static final int C_STATUS_FINISHED     = 4;

    /** Constant indicating status = ERROR. */
    public static final int C_STATUS_ERROR        = 5;

    /** Constant indicating status = NONE. */
    public static final int C_STATUS_NONE         = 6;


    //-------------------------------------------------------------------------
    // Private variables
    //-------------------------------------------------------------------------
    
    /** The sending component. */
    private int i_sendingComponent = C_COMPONENT_UNKNOWN;

    /** The target component. */
    private int i_targetComponent  = C_COMPONENT_UNKNOWN;

    /** The requested action. */
    private int i_request          = C_REQUEST_NONE;

    /** The current status. */
    private int i_status           = C_STATUS_NONE;


    /**
     * The no-arg Constructor.
     */
    public BatchMessage()
    {
        super();
    }

    /**
     * @return the requested action.
     * 
     * @see #C_REQUEST_START
     * @see #C_REQUEST_STOP
     * @see #C_REQUEST_PAUSE
     * @see #C_REQUEST_RESUME
     * @see #C_REQUEST_REPORT
     * @see #C_REQUEST_STATUS
     * @see #C_REQUEST_NONE
     */
    public int getRequest()
    {
        return i_request;
    }

    /**
     * @return the sending component.
     */
    public int getSendingComponent()
    {
        return i_sendingComponent;
    }

    /**
     * @return the current status.
     */
    public int getStatus()
    {
        return i_status;
    }

    /**
     * @return the target component.
     */
    public int getTargetComponent()
    {
        return i_targetComponent;
    }

    /**
     * @param p_request the 
     */
    public void setRequest( int p_request )
    {
        i_request = p_request;
    }

    /**
     * @param p_sendingComponent Sending component.
     */
    public void setSendingComponent( int p_sendingComponent )
    {
        i_sendingComponent = p_sendingComponent;
    }

    /**
     * @param p_status New status.
     */
    public void setStatus( int p_status )
    {
        i_status = p_status;
    }

    /**
     * @param p_targetComponent Target component.
     */
    public void setTargetComponent( int p_targetComponent )
    {
        i_targetComponent = p_targetComponent;
    }


    /**
     * Returns a string that "textually represents" this object.
     * 
     * @return a string that "textually represents" this object.
     */
    public String toString()
    {
        StringBuffer l_toStringBuf = new StringBuffer("BatchMessage=[");
        l_toStringBuf.append("i_sendingComponent="  + getComponentAsString(i_sendingComponent));
        l_toStringBuf.append(", i_targetComponent=" + getComponentAsString(i_targetComponent));
        l_toStringBuf.append(", i_request="         + getRequestAsString(i_request));
        l_toStringBuf.append(", i_status="          + getStatusAsString(i_status));
        l_toStringBuf.append("]");
        
        return l_toStringBuf.toString();
    }


    /***************************************************************************
     * Returns the given component as a string.
     * 
     * @param p_component  the component.
     * 
     * @return the given component as a string.
     */
    private String getComponentAsString(int p_component)
    {
        String l_componentStr = "Unknown";
        
        switch (p_component)
        {
            case C_COMPONENT_MANAGER:
                l_componentStr = "Manager";
                break;

            case C_COMPONENT_READER:
                l_componentStr = "Reader";
                break;

            case C_COMPONENT_WRITER:
                l_componentStr = "Writer";
                break;

            case C_COMPONENT_PROCESSOR:
                l_componentStr = "Processor";
                break;

            case C_COMPONENT_ALL:
                l_componentStr = "ALL";
                break;

            default:
                // No need to set it to "Unknown" since it's already done.
                break;
        }
        
        return l_componentStr;
    }


    /***************************************************************************
     * Returns the given request as a string.
     * 
     * @param p_request  the request.
     * 
     * @return the given request as a string.
     */
    private String getRequestAsString(int p_request)
    {
        String l_request = "None";
        
        switch (p_request)
        {
            case C_REQUEST_PAUSE:
                l_request = "Pause";
                break;

            case C_REQUEST_REPORT:
                l_request = "Report";
                break;

            case C_REQUEST_RESUME:
                l_request = "Resume";
                break;

            case C_REQUEST_START:
                l_request = "Start";
                break;

            case C_REQUEST_STATUS:
                l_request = "Status";
                break;

            case C_REQUEST_STOP:
                l_request = "Stop";
                break;

            default:
                // No need to set it to "None" since it's already done.
                break;
        }
        
        return l_request;
    }


    /***************************************************************************
     * Returns the given status as a string.
     * 
     * @param p_status  the request.
     * 
     * @return the given request as a string.
     */
    private String getStatusAsString(int p_status)
    {
        String l_status = "None";
        
        switch (p_status)
        {
            case C_STATUS_ERROR:
                l_status = "Error";
                break;

            case C_STATUS_FINISHED:
                l_status = "Finished";
                break;

            case C_STATUS_PAUSED:
                l_status = "Paused";
                break;

            case C_STATUS_RESUMED:
                l_status = "Resumed";
                break;

            case C_STATUS_STARTED:
                l_status = "Started";
                break;

            case C_STATUS_STOPPED:
                l_status = "Stopped";
                break;

            default:
                // No need to set it to "None" since it's already done.
                break;
        }
        
        return l_status;
    }
}
