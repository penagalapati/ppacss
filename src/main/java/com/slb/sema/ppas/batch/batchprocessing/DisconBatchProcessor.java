////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       DisconBatchProcessor
//      DATE            :       4-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+-------------------
// 28/03/07 | Lars L.       | Method: processRecord(...)       | PpacLon#1861/11237
//          |               | Update the record's error line   |
//          |               | if an Exception is thrown by the | 
//          |               | IS API.                          | 
//----------+---------------+----------------------------------+-------------------
// 28/06/07 | E Dangoor     | Add disconnection date to output | PpacLon#3195/11774
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.text.ParseException;
import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.DisconBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnDataSet;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasDisconnectService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for calling the appropriate IS API method to perform the
 * disconnection of customers. This is acheived by calling the business service 
 * PpasDisconnectService, method disconnectSubscriber().
 * NOTE. This service will atuomatically disconnect any subordinates when a master
 * subscription is disconnected. Therefore, an attempt to disconnect a subordinate
 * that fails.
 * 
 */
public class DisconBatchProcessor extends BatchProcessor
{
    //---------------------------------------------------------------
    //  Class level constant
    //  -------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME    = "DisconBatchProcessor";
    
    /** Field length of the MSISDN in the output file record. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH = 15;


    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** The <code>Session</code> object. */
    private PpasSession           i_ppasSession           = null;
    
    /** The <code>MsisdnFormat</code> object. */
    private MsisdnFormat          i_msisdnFormat          = null;

    /** The IS API used by the <code>DisconBatchProcessor</code>. */
    private PpasDisconnectService i_ppasDisconnectService = null;

    /** The flag to indicate whether to output the disconnection date. */
    private boolean               i_outputDiscDate        = false;
    
    
    /**
     * Constructor for the class DisconBatchProcessor.
     * 
     * @param p_ppasContext      the PpasContext reference.
     * @param p_logger           the logger.
     * @param p_batchController  the batch controller.
     * @param p_inQueue          the input data queue.
     * @param p_outQueue         the output data queue.
     * @param p_parameters       the start process paramters.
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public DisconBatchProcessor(PpasContext           p_ppasContext,
                                Logger                p_logger,
                                DisconBatchController p_batchController,
                                SizedQueue            p_inQueue,
                                SizedQueue            p_outQueue,
                                Map                   p_parameters,
                                PpasProperties        p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_parameters, p_properties);
        PpasRequest l_ppasRequest = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }


        i_ppasSession  = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest  = new PpasRequest(i_ppasSession);
        
        i_msisdnFormat = p_ppasContext.getMsisdnFormatInput();

        i_ppasDisconnectService = new PpasDisconnectService(l_ppasRequest, p_logger, p_ppasContext);

        i_outputDiscDate = i_properties.getBooleanProperty(BatchConstants.C_OUTPUT_DISC_DATE);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }

    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * This method will be called from doRun() passed a record to process. It will call the method
     * disconnectSubscriber() in the IS API with paramters extraced from the record.
     * Depending on the success of the operation, the record will be updated with
     * information used by the writer-component before returned.
     * NOTE that the error "Subcriber alreadyu disconnected" will be treated as an successful
     * disconnection. The reason is that when disconnecting a master subscription, all its 
     * subordinates will be automatically disconnected as per definition in the
     * Business Service. Effectively this means that no errors are expected during disconnection.
     * Inconsistent data is not considered since it orgins from ASCS database, table CUST_MAST.
     * 
     * @param p_record  the <code>DisconBatchRecordData</code>.
     * 
     * @return the given <code>DisconBatchRecordData</code> updated with info for the writer component.
     * 
     * @throws PpasServiceException  if an error occurs when diconnecting a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        DisconBatchRecordData l_record        = null;
        PpasRequest           l_request       = null;
        StringBuffer          l_tmp           = new StringBuffer();
        Msisdn                l_msisdn        = null;
        String                l_opId          = null;
        PpasDateTime          l_endServDate   = null;
        PpasDateTime          l_discDate      = null;
        MsisdnDataSet         l_msisdnDataSet = null;
        Vector                l_msisdnDataVec = null;
        Msisdn                l_tmpMsisdn     = null;
        String                l_tmpMsisdnStr  = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        // Cast the record into the batch specific record type
        l_record = (DisconBatchRecordData)p_record;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10050,
                this,
                "Dump Record: '" + l_record.dumpRecord());
        }
        
        // Create PpasRequest object
        l_opId         = i_batchController.getSubmitterOpid();

        try
        {
            l_msisdn = i_msisdnFormat.parse( l_record.getMsisdn().trim() );
        }
        catch (ParseException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10050,
                    this,
                    C_METHOD_processRecord + " ParseException - after format Msisdn..");
            }
            e.printStackTrace();
        }
        l_request   = new PpasRequest(i_ppasSession, l_opId, l_msisdn);
        l_request.setTransId(l_record.getCustomerId());

        // Call the appropriate IS API with data extraced from record
        try
        {
            // Set attributes in request object
            l_endServDate = new PpasDateTime( l_record.getDisconnectionDate() );
            l_request.setRequestDateTime( l_endServDate );
   
            // Call the IS API with required parameters.
            l_msisdnDataSet = i_ppasDisconnectService.disconnectSubscriber(l_request, 
                                                           super.i_isApiTimeout, 
                                                           l_record.getDisconnectReason(), 
                                                           true);

            l_msisdnDataVec = l_msisdnDataSet.getMsisdnSetAsVector();
            for (int i = 0; i < l_msisdnDataVec.size(); i++)
            {
                l_tmpMsisdn = (Msisdn)l_msisdnDataVec.elementAt(i);
                l_tmpMsisdnStr = i_msisdnFormat.format(l_tmpMsisdn);
                l_tmp.append(l_tmpMsisdnStr);
                for ( int j = 0; j < (C_MSISDN_LENGTH - l_tmpMsisdnStr.length()); j++ )
                {
                    l_tmp.append(" ");
                } 
                l_tmp.append(l_record.getDisconnectReason());

                if (i_outputDiscDate)
                {
                    l_request   = new PpasRequest(i_ppasSession, l_opId, l_msisdn);

                    // Get the actual disconnection date
                    while((l_discDate == null) || ( ! l_discDate.isSet()))
                    {
                        l_discDate = i_ppasDisconnectService.
                                        getDisconnectDetails(l_request, i_isApiTimeout).
                                        getDisconnectDateTime();
                    }

                    String l_date = l_discDate.toString_yyyyMMddHHmmss();
                    l_tmp.append(l_date);
                }

                if ((i + 1) < l_msisdnDataVec.size())
                {
                    // This was not the last MSISDN to add, add a 'comma' as separator.
                    l_tmp.append(",");
                }
            }
            l_record.setOutputLine(l_tmp.toString()); 
        }
        catch ( PpasServiceException p_ppasServiceEx )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10030, this,
                                "PpasServiceException is caught: " + p_ppasServiceEx.getMessage() +
                                ",  key: " + p_ppasServiceEx.getMsgKey());
    
                // Don't re-throw the exception - We want to carry on processing the batch.
                // Note also that the exception will already have been logged by ISC.
                
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10030, this,
                                "PpasServiceException is silently ignored since it's expected and accepted");
            }
            l_record.setErrorLine(p_ppasServiceEx.getMessage());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }
        
        return l_record;
        
    } // End of processRecord(.)



} // End of class DisconBatchProcess
