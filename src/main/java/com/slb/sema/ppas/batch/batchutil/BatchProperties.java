// //////////////////////////////////////////////////////////////////////////////
// ASCS : 9500
// //////////////////////////////////////////////////////////////////////////////
//
// FILE NAME : BatchProperties.java
// DATE :      06-Oct-2005
// AUTHOR :    Lars Lundberg
// REFERENCE : PRD_ASCS00_DEV_SS_083
//
// COPYRIGHT : WM-data 2005
//
// DESCRIPTION : This class is a batch properties container.
//
// //////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
// //////////////////////////////////////////////////////////////////////////////
// DATE | NAME | DESCRIPTION | REFERENCE
// ----------+---------------+----------------------------------+----------------
// DD/MM/YY  | <name>        | <brief description of changes>   | <reference>
// ----------+---------------+----------------------------------+----------------
// //////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchutil;

import com.slb.sema.ppas.batch.exceptions.BatchConfigException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;

/**
 * This <code>BatchProperties</code> class is a batch properties container and it
 * provides a number of convenient methods in order to retreive the value of each property.
 */
public class BatchProperties
{
    // =========================================================================
    // == Private constants.                                                  ==
    // =========================================================================
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "BatchProperties";

    // Batch properties.
    /** The batch package identifier. Value is {@value}. */
    private static final String C_BATCH_PACKAGE                 = "com.slb.sema.ppas.batch.";


    // Batch files directories properties.
    /** The ASCS_LOCAL_ROOT directory. */
    private static final String C_ASCS_LOCAL_ROOT               = System.getProperty("ascs.localRoot", "/");

    /** The indata (files) directory property key. Value is {@value}. */
    private static final String C_PROP_INDATA_DIR               = C_BATCH_PACKAGE + "inputFileDirectory";
    
    /** The indata (files) directory default value. */
    private static final String C_DEFVAL_INDATA_DIR             = C_ASCS_LOCAL_ROOT + "/batchfiles/data";
    
    /** The output data (files) directory property key. Value is {@value}. */
    private static final String C_PROP_OUTPUT_DATA_DIR          = C_BATCH_PACKAGE + "outputFileDirectory";
    
    /** The output data (files) directory default value. */
    private static final String C_DEFVAL_OUTPUT_DATA_DIR        = C_ASCS_LOCAL_ROOT + "/batchfiles/output";
    
    /** The recovery (files) directory property key. Value is {@value}. */
    private static final String C_PROP_RECOVERY_DIR             = C_BATCH_PACKAGE + "recoveryFileDirectory";
    
    /** The recovery (files) directory default value. */
    private static final String C_DEFVAL_RECOVERY_DIR           = C_ASCS_LOCAL_ROOT + "/batchfiles/recovery";
    
    /** The report (files) directory property key. Value is {@value}. */
    private static final String C_PROP_REPORT_DIR               = C_BATCH_PACKAGE + "reportFileDirectory";
    
    /** The report (files) directory default value. */
    private static final String C_DEFVAL_REPORT_DIR             = C_ASCS_LOCAL_ROOT + "/batchfiles/report";
    

    // Data queues properties.
    /** The indata queue max size property key. Value is {@value}. */
    private static final String C_PROP_INDATA_QUEUE_SIZE        = C_BATCH_PACKAGE + "inQueueMaxSize";

    /** The indata queue max size default value. Value is {@value}. */
    private static final long   C_DEFVAL_INDATA_QUEUE_SIZE      = 1000;

    /** The output data queue max size property key. Value is {@value}. */
    private static final String C_PROP_OUTPUT_DATA_QUEUE_SIZE   = C_BATCH_PACKAGE + "outQueueMaxSize";

    /** The output data queue max size default value. Value is {@value}. */
    private static final long   C_DEFVAL_OUTPUT_DATA_QUEUE_SIZE = C_DEFVAL_INDATA_QUEUE_SIZE;

    /** The delay time before processing the retry queue property key. Value is {@value}. */
    private static final String C_PROP_RETRY_QUEUE_DELAY        = C_BATCH_PACKAGE + "delayBeforeRetry";

    /** The delay time before processing the retry queue default value. Value is {@value}. */
    private static final long   C_DEFVAL_RETRY_QUEUE_DELAY      = 10000; //10 sec.


    // IS API properties.
    /** The timeout time (in milliseconds) property key.
     *  This is the max time the batch will wait for the IS API response. Value is {@value}. */
    private static final String C_PROP_ISAPI_TIMEOUT            = C_BATCH_PACKAGE + "timeout";
    
    /** The timeout time (in milliseconds) default value. Value is {@value}. */
    private static final long   C_DEFVAL_ISAPI_TIMEOUT          = 60000; //60 sec.


    // Sub-processes division properties.
    // These properties are used by the specific CA37 batches such as the MSISDN routing provisioning batch.
    /** The max chunk size property key. Value is {@value}.
     *  This is the max number of records that will be processed by each call to the IS API. */
    private static final String C_PROP_CHUNK_SIZE               = C_BATCH_PACKAGE + "maxChunkSize";

    /** The max chunk size default value. Value is {@value}. */
    private static final int    C_DEFVAL_CHUNK_SIZE             = 200;

    /** The max number of inner process threads property key.
     *  This is the max number of parallel chunk buffer processing. Value is {@value}. */
    private static final String C_PROP_MAX_NOOF_CHUNK_PROC      = C_BATCH_PACKAGE + "maxNoOfThreads";

    /** The max number of inner process threads default value. Value is {@value}. */
    private static final int    C_DEFVAL_MAX_NOOF_CHUNK_PROC    = 10;


    // MSISDN string format properties.
    /** The MSISDN input data strip size property key.
     *  This is the number of characters that shall be removed from the beginning of an incoming MSISDN
     *  string. Value is {@value}. */
    private static final String C_PROP_MSISDN_INPUT_STRIP_SIZE    = C_BATCH_PACKAGE + "msisdnInputStripSize";

    /** The MSISDN input data strip size default value. Value is {@value}. */
    private static final int    C_DEFVAL_MSISDN_INPUT_STRIP_SIZE  = 0;

    /** The MSISDN input data prefix property key.
     *  This string shall be added at the beginning of an incoming MSISDN string. Value is {@value}. */
    private static final String C_PROP_MSISDN_INPUT_PREFIX        = C_BATCH_PACKAGE + "msisdnInputPrefix";

    /** The MSISDN input data prefix default value. Value is {@value}. */
    private static final String C_DEFVAL_MSISDN_INPUT_PREFIX      = "";

    /** The MSISDN output data strip size property key.
     *  This is the number of characters that shall be removed from the beginning of an output MSISDN
     *  string. Value is {@value}. */
    private static final String C_PROP_MSISDN_OUTPUT_STRIP_SIZE   = C_BATCH_PACKAGE + "msisdnOutputStripSize";

    /** The MSISDN output data strip size default value. Value is {@value}. */
    private static final int    C_DEFVAL_MSISDN_OUTPUT_STRIP_SIZE = 0;

    /** The MSISDN output data prefix property key.
     *  This string shall be added at the beginning of an incoming MSISDN string. Value is {@value}. */
    private static final String C_PROP_MSISDN_OUTPUT_PREFIX       = C_BATCH_PACKAGE + "msisdnOutputPrefix";

    /** The MSISDN output data prefix default value. Value is {@value}. */
    private static final String C_DEFVAL_MSISDN_OUTPUT_PREFIX     = "";


    // Batch process components thread properties.
    // These properties defines the number of internal threads that should be used for each batch process
    // component (i.e. reader, writer and processor).
    /** The number of reader threads property key. Value is {@value}. */
    private static final String C_PROP_NOOF_READER_THREADS      = C_BATCH_PACKAGE + "numberOfReaderThreads";

    /** The number of reader threads default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_READER_THREADS    = 1;

    /** The number of writer threads property key. Value is {@value}. */
    private static final String C_PROP_NOOF_WRITER_THREADS      = C_BATCH_PACKAGE + "numberOfWriterThreads";

    /** The number of writer threads default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_WRITER_THREADS    = 1;

    /** The number of processor threads property key. Value is {@value}. */
    private static final String C_PROP_NOOF_PROCESSOR_THREADS   = C_BATCH_PACKAGE + "numberOfProcessorThreads";

    /** The number of processor threads default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_PROCESSOR_THREADS = 1;


    // Database properties.
    /** The batch control table update frequency property key. Value is {@value}. */
    private static final String C_PROP_CNTRL_TABLE_UPD_FREQ = C_BATCH_PACKAGE + "controlTableUpdateFrequency";

    /** The batch control table update frequency default value. Value is {@value}. */
    private static final int    C_DEFVAL_CNTRL_TABLE_UPD_FREQ = 100;

    /** The fetch size property key.
     *  This value is the number of rows to fetch from the database when more rows are needed.
     *  NB: It is used only by a database driven batch job. Value is {@value}. */
    private static final String C_PROP_FETCH_SIZE             = C_BATCH_PACKAGE + "fetchArraySize";

    /** The fetch size default value. Value is {@value}. */
    private static final int    C_DEFVAL_FETCH_SIZE           = 100;


    // BatchMaster properties.
    // These properties defines the number of (sub) batch processes that should be started by the BatchMaster.
    /** The batch master package identifier. Value is {@value}. */
    private static final String C_BATCH_MASTER_PACKAGE       = C_BATCH_PACKAGE + "batchmaster.";

    /** The number of disconnect batch processes property key. Value is {@value}. */
    private static final String C_PROP_NOOF_DISC_PROCESSES   =
        C_BATCH_MASTER_PACKAGE + "numberOfDisconnectSubProcesses";

    /** The number of disconnect batch processes default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_DISC_PROCESSES = 4;

    /** The number of Bulk Load of AccountFinder batch processes property key. Value is {@value}. */
    private static final String C_PROP_NOOF_BULK_LOAD_AF_PROCESSES   =
        C_BATCH_MASTER_PACKAGE + "numberOfBulkLoadOfAccountSubProcesses";

    /** The number of Bulk Load of AccountFinder batch processes default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_BULK_LOAD_AF_PROCESSES = 1;

    /** The number of Promotion Allocation Provisioning batch processes property key. Value is {@value}. */
    private static final String C_PROP_NOOF_PROMO_ALLOC_PROV_PROCESSES   =
        C_BATCH_MASTER_PACKAGE + "numberOfPromoAllocProvisioningSubProcesses";

    /** The number of Promotion Allocation Provisioning batch processes default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_PROMO_ALLOC_PROV_PROCESSES = 4;

    /** The number of Promo. Plan Alloc. Synch. batch processes property key. Value is {@value}. */
    private static final String C_PROP_NOOF_PROMO_PLAN_ALLOC_SYNCH_PROCESSES   =
        C_BATCH_MASTER_PACKAGE + "numberOfPromoAllocSynchSubProcesses";

    /** The number of Promo. Plan Alloc. Synch. batch processes default value. Value is {@value}. */
    private static final int    C_DEFVAL_NOOF_PROMO_PLAN_ALLOC_SYNCH_PROCESSES = 4;


    // Input data filename properties (prefix, body pattern, extension).
    /** The input data filename prefix property key. Value is {@value}. */
    private static final String C_PROP_INDATA_FILENAME_PREFIXES      =
        C_BATCH_PACKAGE + "indataFilenamePrefix";

    /** The input data filename extension property key. Value is {@value}. */
    private static final String C_PROP_INDATA_FILENAME_EXTENSIONS    =
        C_BATCH_PACKAGE + "indataFilenameExtension";


    // Filename extension properties.
    /** The 'in progress' filename extension property key. Value is {@value}. */
    private static final String C_PROP_INPROGRESS_EXTENSION   = C_BATCH_PACKAGE + "inProgressFileExtension";
    
    /** The 'in progress' filename extension default value. Value is {@value}. */
    private static final String C_DEFVAL_INPROGRESS_EXTENSION = "IPG";
    
    /** The 'done' filename extension property key. Value is {@value}. */
    private static final String C_PROP_DONE_EXTENSION         = C_BATCH_PACKAGE + "doneFileExtension";
    
    /** The 'done' filename extension default value. Value is {@value}. */
    private static final String C_DEFVAL_DONE_EXTENSION       = "DNE";
    
    /** The 'recovery' filename extension property key. Value is {@value}. */
    private static final String C_PROP_RECOVERY_EXTENSION     = C_BATCH_PACKAGE + "recoveryFileExtension";
    
    /** The 'recovery' filename extension default value. Value is {@value}. */
    private static final String C_DEFVAL_RECOVERY_EXTENSION   = "RCV";
    
    /** The 'report' filename extension property key. Value is {@value}. */
    private static final String C_PROP_REPORT_EXTENSION       = C_BATCH_PACKAGE + "reportFileExtension";
    
    /** The 'report' filename extension default value. Value is {@value}. */
    private static final String C_DEFVAL_REPORT_EXTENSION     = "RPT";
    

    // =========================================================================
    // == Private variables.                                                  ==
    // =========================================================================
    /** The <code>PpasProperties</code> object. */
    private PpasProperties i_properties                 = null;

    /** The <code>Logger</code> object. */
    private Logger         i_logger                     = null;

    /** The indata directory. */
    private String         i_indataDir                  = null;

    /** The output data directory. */
    private String         i_outputDataDir              = null;

    /** The recovery directory. */
    private String         i_recoveryDir                = null;

    /** The report directory. */
    private String         i_reportDir                  = null;

    /** The indata queue size. */
    private long           i_indataQueueSize            = 0;

    /** The indata queue size. */
    private long           i_outputDataQueueSize        = 0;

    /** The delay time before processing the retry queue (in ms). */
    private long           i_retryQueueProcessingDelay  = 0;

    /** The IS API timeout time (in ms). */
    private long           i_isApiTimeout               = 0;

    /** The specific CA37 batches chunk size. */
    private int            i_chunkSize                  = 0;

    /** The specific CA37 batches number of parallel chunk buffer processes (threads). */
    private int            i_maxNoOfChunkProc           = 0;

    /** The MSISDN input data strip size. */
    private int            i_msisdnInputStripSize       = 0;

    /** The MSISDN input data prefix. */
    private String         i_msisdnInputPrefix          = null;

    /** The MSISDN output data strip size. */
    private int            i_msisdnOutputStripSize      = 0;

    /** The MSISDN output data prefix. */
    private String         i_msisdnOutputPrefix         = null;

    /** The number of reader threads. */
    private int            i_noOfReaderThreads           = 0;

    /** The number of writer threads. */
    private int            i_noOfWriterThreads           = 0;

    /** The number of processor threads. */
    private int            i_noOfProcessorThreads        = 0;

    /** The batch control table update frequency. */
    private int            i_controlTableUpdFreq         = 0;

    /** The fetch size (used by database driven batch job). */
    private int            i_fetchSize                   = 0;

    /** The number of disconnect batch processes. */
    private int            i_noOfDiscProc                = 0;

    /** The number of Bulk Load of AccountFinder batch processes. */
    private int            i_noOfBulkLoadAfProc          = 0;

    /** The number of Promotion Allocation Provisioning batch processes. */
    private int            i_noOfPromoAllocProvProc      = 0;

    /** The number of Promo. Plan Alloc. Synch. batch processes. */
    private int            i_noOfPromoPlanAllocSynchProc = 0;

    /** The input data filename prefix(es). Could be a comma delimited list. */
    private String         i_indataFilenamePrefixes       = null;

    /** The input data filename extensions. Could be a comma delimited list. */
    private String         i_indataFilenameExtensions     = null;

    /** The 'in progress' filename extension. */
    private String         i_inProgressFileExtension      = null;

    /** The 'done' filename extension. */
    private String         i_doneFileExtension            = null;

    /** The 'recovery' filename extension. */
    private String         i_recoveryFileExtension        = null;

    /** The 'report' filename extension. */
    private String         i_reportFileExtension          = null;


    // =========================================================================
    // == Constructors.                                                       ==
    // =========================================================================
    /**
     * Constructs a <code>BatchProperties</code> instance using the given parameters.
     * 
     * @param p_properties  The <code>PpasProperties</code> object.
     * @param p_logger      A default value for a non-mandatory property.
     * 
     * @throws BatchConfigException  if a mandatory property is missing or
     *                               if a property has a bad value (isn't numerical when it should be or
     *                               if the value of a numerical property is too big).
     */
    public BatchProperties(PpasProperties p_properties, Logger p_logger)
        throws BatchConfigException
    {
        i_properties = p_properties;
        i_logger     = p_logger;
        
        // Initiate all batch properties.
        init();
    }

    // =========================================================================
    // == Public methods.                                                     ==
    // =========================================================================
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getNumberPropertyValue = "getNumberPropertyValue";
    /**
     * Reads, verfies and returns a numerical (batch) property.
     * If it is missing and mandatory a <code>PpasConfigException</code> is thrown.
     * If it is missing but not mandatory the default value is returned.
     * 
     * @param p_propertyKey   The name of a property.
     * @param p_mandatory     <code>true</code> for a mandatory property, otherwise <code>false</code>.
     * @param p_defaultValue  A default value for a non-mandatory property.
     * @param p_maxValue      The max value allowed for the current property, for instance it could be
     *                        <code>Integer.MAX_VALUE</code> if it is limited to an integer value.
     * @param p_minValue      The min value allowed for the current property.
     * 
     * @return the value of the given property.
     * 
     * @throws BatchConfigException  if a mandatory property is missing or
     *                               if the given property isn't numerical or
     *                               if the value of the given property is too big.
     */
    public long getNumberPropertyValue(String  p_propertyKey,
                                       boolean p_mandatory,
                                       long    p_defaultValue,
                                       long    p_maxValue,
                                       long    p_minValue)
        throws BatchConfigException
    {
        double                     l_tmpPropValue  = -1;
        String                     l_propValueStr  = null;
        BatchKey.BatchExceptionKey l_batchExKey    = null;
        BatchConfigException       l_batchConfigEx = null;

        l_propValueStr = getPropertyValue(p_propertyKey, p_mandatory, Long.toString(p_defaultValue));
        if (l_propValueStr.matches(BatchConstants.C_PATTERN_NUMERIC))
        {
            l_tmpPropValue = Double.parseDouble(l_propValueStr);
            if (l_tmpPropValue > p_maxValue)
            {
                l_batchExKey = BatchKey.get().batchTooBigNumericalProp(p_propertyKey,
                                                                       l_propValueStr,
                                                                       Long.toString(p_maxValue));
            }
            if (l_tmpPropValue < p_minValue)
            {
                l_batchExKey = BatchKey.get().batchTooLowNumericalProp(p_propertyKey,
                                                                       l_propValueStr,
                                                                       Long.toString(p_minValue));
            }
        }
        else
        {
            l_batchExKey = BatchKey.get().batchNonNumericalProp(p_propertyKey, l_propValueStr);
        }

        if (l_batchExKey != null)
        {
            // Error, log and throw a BatchConfigException.
            l_batchConfigEx = new BatchConfigException(C_CLASS_NAME,
                                                       C_METHOD_getNumberPropertyValue,
                                                       10000,
                                                       this,
                                                       null,
                                                       0,
                                                       l_batchExKey);

            i_logger.logMessage(l_batchConfigEx);
            throw l_batchConfigEx;
        }

        return Long.parseLong(l_propValueStr);
    }


    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getPropertyValue = "getPropertyValue";
    /**
     * Reads and verfies a (batch) property.
     * If it is missing and mandatory a <code>PpasConfigException</code> is thrown.
     * If it is missing but not mandatory the default value is returned.
     * 
     * @param p_propertyKey   The name of a property.
     * @param p_mandatory     <code>true</code> for a mandatory property, otherwise <code>false</code>.
     * @param p_defaultValue  A default value for a non-mandatory property.
     * 
     * @return the value of the given property.
     * 
     * @throws BatchConfigException  if a mandatory property is missing.
     */
    public String getPropertyValue(String  p_propertyKey,
                                   boolean p_mandatory,
                                   String  p_defaultValue)
        throws BatchConfigException
    {
        String               l_propValue     = null;
        BatchConfigException l_batchConfigEx = null;

        l_propValue = i_properties.getTrimmedProperty(p_propertyKey);
        if (l_propValue == null  ||  l_propValue.length() == 0)
        {
            if (p_mandatory)
            {
                // ERROR: A mandatory property is missing, log and throw a BatchConfigException.
                l_batchConfigEx =
                    new BatchConfigException(C_CLASS_NAME,
                                            C_METHOD_getPropertyValue,
                                            10082,
                                            this,
                                            null,
                                            0,
                                            BatchKey.get().batchMissingMandatoryProp(p_propertyKey));
                i_logger.logMessage(l_batchConfigEx);
                throw l_batchConfigEx;
            }
            else
            {
                l_propValue = p_defaultValue;
            }
        }
        
        return l_propValue;
    }


    /**
     * Returns the size of the indata queue.
     */
    public long getIndataQueueSize()
    {
        return i_indataQueueSize;
    }


    /**
     * Returns the name of the indata queue size property.
     */
    public String getIndataQueueSizePropertyName()
    {
        return C_PROP_INDATA_QUEUE_SIZE;
    }


    /**
     * Returns the size of the output data queue.
     */
    public long getOutputDataQueueSize()
    {
        return i_outputDataQueueSize;
    }


    /**
     * Returns the name of the output data queue size property.
     */
    public String getOutputDataQueueSizePropertyName()
    {
        return C_PROP_OUTPUT_DATA_QUEUE_SIZE;
    }


    /**
     * @return Returns the indataDir.
     */
    public String getIndataDir()
    {
        return i_indataDir;
    }


    /**
     * @return Returns the outputDataDir.
     */
    public String getOutputDataDir()
    {
        return i_outputDataDir;
    }


    /**
     * @return Returns the recoveryDir.
     */
    public String getRecoveryDir()
    {
        return i_recoveryDir;
    }


    /**
     * @return Returns the reportDir.
     */
    public String getReportDir()
    {
        return i_reportDir;
    }


    /**
     * @return Returns the isApiTimeout.
     */
    public long getIsApiTimeout()
    {
        return i_isApiTimeout;
    }


    /**
     * @return Returns the chunkSize.
     */
    public int getChunkSize()
    {
        return i_chunkSize;
    }


    /**
     * @return Returns the noOfChunkProcesses.
     */
    public int getNoOfChunkProcesses()
    {
        return i_maxNoOfChunkProc;
    }


    /**
     * @return Returns the msisdnInputPrefix.
     */
    public String getMsisdnInputPrefix()
    {
        return i_msisdnInputPrefix;
    }


    /**
     * @return Returns the msisdnInputStripSize.
     */
    public int getMsisdnInputStripSize()
    {
        return i_msisdnInputStripSize;
    }


    /**
     * @return Returns the msisdnOutputPrefix.
     */
    public String getMsisdnOutputPrefix()
    {
        return i_msisdnOutputPrefix;
    }


    /**
     * @return Returns the msisdnOutputStripSize.
     */
    public int getMsisdnOutputStripSize()
    {
        return i_msisdnOutputStripSize;
    }


    /**
     * @return Returns the noOfProcessorThreads.
     */
    public int getNoOfProcessorThreads()
    {
        return i_noOfProcessorThreads;
    }


    /**
     * Sets the noOfProcessorThreads.
     */
    public void setNoOfProcessorThreads(int p_noOfProcessorThreads)
    {
        i_noOfProcessorThreads = p_noOfProcessorThreads;
        i_properties.setProperty(C_PROP_NOOF_PROCESSOR_THREADS, "" + p_noOfProcessorThreads);
    }


    /**
     * @return Returns the noOfReaderThreads.
     */
    public int getNoOfReaderThreads()
    {
        return i_noOfReaderThreads;
    }


    /**
     * @return Returns the noOfWriterThreads.
     */
    public int getNoOfWriterThreads()
    {
        return i_noOfWriterThreads;
    }


    /**
     * @return Returns the controlTableUpdFreq.
     */
    public int getControlTableUpdFreq()
    {
        return i_controlTableUpdFreq;
    }


    /**
     * @return Returns the fetchSize.
     */
    public int getFetchSize()
    {
        return i_fetchSize;
    }


    /**
     * @return Returns the noOfBulkLoadAfProc.
     */
    public int getNoOfBulkLoadAfProc()
    {
        return i_noOfBulkLoadAfProc;
    }


    /**
     * @return Returns the noOfDiscProc.
     */
    public int getNoOfDiscProc()
    {
        return i_noOfDiscProc;
    }


    /**
     * @return Returns the noOfPromoAllocProvProc.
     */
    public int getNoOfPromoAllocProvProc()
    {
        return i_noOfPromoAllocProvProc;
    }


    /**
     * @return Returns the noOfPromoPlanAllocSynchProc.
     */
    public int getNoOfPromoPlanAllocSynchProc()
    {
        return i_noOfPromoPlanAllocSynchProc;
    }


    /**
     * Returns the input data filename extension(s).
     * It could be a comma delimited list.
     * 
     * @param p_defaultExtensions  the default extension(s).
     * 
     * @return Returns the input data filename extension(s).
     */
    public String getIndataFilenameExtensions(String p_defaultExtensions)
    {
        if (i_indataFilenameExtensions == null)
        {
            try
            {
                i_indataFilenameExtensions = getPropertyValue(C_PROP_INDATA_FILENAME_EXTENSIONS,
                                                              false,
                                                              p_defaultExtensions);
            }
            catch (BatchConfigException l_bcEx)
            {
                // Do nothing, the BatchConfigException will never be thrown for a non-mandatory property.
                l_bcEx = null;
            }
        }
        return i_indataFilenameExtensions;
    }


    /**
     * Returns the input data filename prefix(es).
     * It could be a comma delimited list.
     * 
     * @param p_defaultPrefixes  the default prefix(es).
     * 
     * @return Returns the input data filename prefix(es).
     */
    public String getIndataFilenamePrefixes(String p_defaultPrefixes)
    {
        if (i_indataFilenamePrefixes == null)
        {
            try
            {
                i_indataFilenamePrefixes = getPropertyValue(C_PROP_INDATA_FILENAME_PREFIXES,
                                                            false,
                                                            p_defaultPrefixes);
            }
            catch (BatchConfigException l_bcEx)
            {
                // Do nothing, the BatchConfigException will never be thrown for a non-mandatory property.
                l_bcEx = null;
            }
        }
        return i_indataFilenamePrefixes;
    }


    /**
     * @return Returns the inProgressExtension.
     */
    public String getInProgressExtension()
    {
        return i_inProgressFileExtension;
    }


    /**
     * @return Returns the i_doneFileExtension.
     */
    public String getDoneExtension()
    {
        return i_doneFileExtension;
    }


    /**
     * @return Returns the recoveryFileExtension.
     */
    public String getRecoveryFileExtension()
    {
        return i_recoveryFileExtension;
    }


    /**
     * @return Returns the reportFileExtension.
     */
    public String getReportFileExtension()
    {
        return i_reportFileExtension;
    }


    /**
     * @return Returns the retryQueueProcessingDelay.
     */
    public long getRetryQueueProcessingDelay()
    {
        return i_retryQueueProcessingDelay;
    }


    /**
     * Displays the batch sub job details (control data) as a <code>String</code>.
     * 
     * @return a <code>String</code> representation of the batch sub job details.
     */
    public String toString()   
    {
        StringBuffer l_stringBuf = new StringBuffer();

        l_stringBuf.append("BatchProperties=[");
        l_stringBuf.append("i_indataDir="                     + i_indataDir);
        l_stringBuf.append(", i_outputDataDir="               + i_outputDataDir);
        l_stringBuf.append(", i_recoveryDir="                 + i_recoveryDir);
        l_stringBuf.append(", i_reportDir="                   + i_reportDir);
        l_stringBuf.append(", i_indataQueueSize="             + i_indataQueueSize);
        l_stringBuf.append(", i_outputDataQueueSize="         + i_outputDataQueueSize);
        l_stringBuf.append(", i_retryQueueProcessingDelay="   + i_retryQueueProcessingDelay);
        l_stringBuf.append(", i_isApiTimeout="                + i_isApiTimeout);
        l_stringBuf.append(", i_chunkSize="                   + i_chunkSize);
        l_stringBuf.append(", i_maxNoOfChunkProc="            + i_maxNoOfChunkProc);
        l_stringBuf.append(", i_msisdnInputStripSize="        + i_msisdnInputStripSize);
        l_stringBuf.append(", i_msisdnInputPrefix="           + i_msisdnInputPrefix);
        l_stringBuf.append(", i_msisdnOutputStripSize="       + i_msisdnOutputStripSize);
        l_stringBuf.append(", i_msisdnOutputPrefix="          + i_msisdnOutputPrefix);
        l_stringBuf.append(", i_noOfReaderThreads="           + i_noOfReaderThreads);
        l_stringBuf.append(", i_noOfWriterThreads="           + i_noOfWriterThreads);
        l_stringBuf.append(", i_noOfProcessorThreads="        + i_noOfProcessorThreads);
        l_stringBuf.append(", i_controlTableUpdFreq="         + i_controlTableUpdFreq);
        l_stringBuf.append(", i_fetchSize="                   + i_fetchSize);
        l_stringBuf.append(", i_noOfDiscProc="                + i_noOfDiscProc);
        l_stringBuf.append(", i_noOfBulkLoadAfProc="          + i_noOfBulkLoadAfProc);
        l_stringBuf.append(", i_noOfPromoAllocProvProc="      + i_noOfPromoAllocProvProc);
        l_stringBuf.append(", i_noOfPromoPlanAllocSynchProc=" + i_noOfPromoPlanAllocSynchProc);
        l_stringBuf.append(", i_indataFilenamePrefixes="      + i_indataFilenamePrefixes);
        l_stringBuf.append(", i_indataFilenameExtensions="    + i_indataFilenameExtensions);
        l_stringBuf.append(", i_inProgressFileExtension="     + i_inProgressFileExtension);
        l_stringBuf.append(", i_doneFileExtension="           + i_doneFileExtension);
        l_stringBuf.append(", i_recoveryFileExtension="       + i_recoveryFileExtension);
        l_stringBuf.append(", i_reportFileExtension="         + i_reportFileExtension);
        l_stringBuf.append("]");

        return l_stringBuf.toString();
    }


    // =========================================================================
    // == Private methods.                                                    ==
    // =========================================================================
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initiates this <code>BatchProperties</code> instance.
     * 
     * @throws BatchConfigException  if a mandatory property is missing or
     *                               if a property has a bad value (isn't numerical when it should be or
     *                               if the value of a numerical property is too big).
     */
    private void init() throws BatchConfigException
    {
        // Init. directories properties.
        i_indataDir     = getPropertyValue(C_PROP_INDATA_DIR,      false, C_DEFVAL_INDATA_DIR);
        i_outputDataDir = getPropertyValue(C_PROP_OUTPUT_DATA_DIR, false, C_DEFVAL_OUTPUT_DATA_DIR);
        i_recoveryDir   = getPropertyValue(C_PROP_RECOVERY_DIR,    false, C_DEFVAL_RECOVERY_DIR);
        i_reportDir     = getPropertyValue(C_PROP_REPORT_DIR,      false, C_DEFVAL_REPORT_DIR);


        // Init. data queue properties.
        i_indataQueueSize = getNumberPropertyValue(C_PROP_INDATA_QUEUE_SIZE,
                                                   false,
                                                   C_DEFVAL_INDATA_QUEUE_SIZE,
                                                   Long.MAX_VALUE,
                                                   1L);

        i_outputDataQueueSize = getNumberPropertyValue(C_PROP_OUTPUT_DATA_QUEUE_SIZE,
                                                       false,
                                                       C_DEFVAL_OUTPUT_DATA_QUEUE_SIZE,
                                                       Long.MAX_VALUE,
                                                       1L);

        i_retryQueueProcessingDelay = getNumberPropertyValue(C_PROP_RETRY_QUEUE_DELAY,
                                                             false,
                                                             C_DEFVAL_RETRY_QUEUE_DELAY,
                                                             Long.MAX_VALUE,
                                                             0L);

        i_isApiTimeout = getNumberPropertyValue(C_PROP_ISAPI_TIMEOUT,
                                                false,
                                                C_DEFVAL_ISAPI_TIMEOUT,
                                                Long.MAX_VALUE,
                                                0L);


        // Init. specific CA37 batches properties.
        i_chunkSize = (int)getNumberPropertyValue(C_PROP_CHUNK_SIZE,
                                                  false,
                                                  C_DEFVAL_CHUNK_SIZE,
                                                  Integer.MAX_VALUE,
                                                  1);

        i_maxNoOfChunkProc = (int)getNumberPropertyValue(C_PROP_MAX_NOOF_CHUNK_PROC,
                                                         false,
                                                         C_DEFVAL_MAX_NOOF_CHUNK_PROC,
                                                         Integer.MAX_VALUE,
                                                         1);


        // Init. MSISDN string format properties.
        i_msisdnInputStripSize = (int)getNumberPropertyValue(C_PROP_MSISDN_INPUT_STRIP_SIZE,
                                                             false,
                                                             C_DEFVAL_MSISDN_INPUT_STRIP_SIZE,
                                                             Integer.MAX_VALUE,
                                                             0);

        i_msisdnInputPrefix =
            getPropertyValue(C_PROP_MSISDN_INPUT_PREFIX, false, C_DEFVAL_MSISDN_INPUT_PREFIX);

        i_msisdnOutputStripSize = (int)getNumberPropertyValue(C_PROP_MSISDN_OUTPUT_STRIP_SIZE,
                                                              false,
                                                             C_DEFVAL_MSISDN_OUTPUT_STRIP_SIZE,
                                                             Integer.MAX_VALUE,
                                                             0);

        i_msisdnOutputPrefix =
            getPropertyValue(C_PROP_MSISDN_OUTPUT_PREFIX, false, C_DEFVAL_MSISDN_OUTPUT_PREFIX);


        // Init. batch process components thread properties.
        i_noOfReaderThreads = (int)getNumberPropertyValue(C_PROP_NOOF_READER_THREADS,
                                                          false,
                                                          C_DEFVAL_NOOF_READER_THREADS,
//                                                          Integer.MAX_VALUE,
                                                          1, //NB: Only one reader thread is allowed.
                                                          1);

        i_noOfWriterThreads = (int)getNumberPropertyValue(C_PROP_NOOF_WRITER_THREADS,
                                                          false,
                                                          C_DEFVAL_NOOF_WRITER_THREADS,
//                                                          Integer.MAX_VALUE,
                                                          1, //NB: Only one writer thread is allowed.
                                                          1);

        i_noOfProcessorThreads = (int)getNumberPropertyValue(C_PROP_NOOF_PROCESSOR_THREADS,
                                                             false,
                                                             C_DEFVAL_NOOF_PROCESSOR_THREADS,
                                                             Integer.MAX_VALUE,
                                                             1);


        // Init. database properties.
        i_controlTableUpdFreq = (int)getNumberPropertyValue(C_PROP_CNTRL_TABLE_UPD_FREQ,
                                                            false,
                                                            C_DEFVAL_CNTRL_TABLE_UPD_FREQ,
                                                            Integer.MAX_VALUE,
                                                            0);

        i_fetchSize = (int)getNumberPropertyValue(C_PROP_FETCH_SIZE,
                                                  false,
                                                  C_DEFVAL_FETCH_SIZE,
                                                  Integer.MAX_VALUE,
                                                  1);


        // BatchMaster properties.
        i_noOfDiscProc = (int)getNumberPropertyValue(C_PROP_NOOF_DISC_PROCESSES,
                                                     false,
                                                     C_DEFVAL_NOOF_DISC_PROCESSES,
                                                     Integer.MAX_VALUE,
                                                     1);

        i_noOfBulkLoadAfProc = (int)getNumberPropertyValue(C_PROP_NOOF_BULK_LOAD_AF_PROCESSES,
                                                           false,
                                                           C_DEFVAL_NOOF_BULK_LOAD_AF_PROCESSES,
                                                           Integer.MAX_VALUE,
                                                           1);

        i_noOfPromoAllocProvProc = (int)getNumberPropertyValue(C_PROP_NOOF_PROMO_ALLOC_PROV_PROCESSES,
                                                               false,
                                                               C_DEFVAL_NOOF_PROMO_ALLOC_PROV_PROCESSES,
                                                               Integer.MAX_VALUE,
                                                               1);

        i_noOfPromoPlanAllocSynchProc =
            (int)getNumberPropertyValue(C_PROP_NOOF_PROMO_PLAN_ALLOC_SYNCH_PROCESSES,
                                        false,
                                        C_DEFVAL_NOOF_PROMO_PLAN_ALLOC_SYNCH_PROCESSES,
                                        Integer.MAX_VALUE,
                                        1);


        // Init. filename properties.
        i_indataFilenamePrefixes     = getPropertyValue(C_PROP_INDATA_FILENAME_PREFIXES, false, null);
        i_indataFilenameExtensions   = getPropertyValue(C_PROP_INDATA_FILENAME_EXTENSIONS, false, null);


        // Init. filename extension properties.
        i_inProgressFileExtension =
            getPropertyValue(C_PROP_INPROGRESS_EXTENSION, false, C_DEFVAL_INPROGRESS_EXTENSION);

        i_doneFileExtension = getPropertyValue(C_PROP_DONE_EXTENSION, false, C_DEFVAL_DONE_EXTENSION);

        i_recoveryFileExtension =
            getPropertyValue(C_PROP_RECOVERY_EXTENSION, false, C_DEFVAL_RECOVERY_EXTENSION);

        i_reportFileExtension =
            getPropertyValue(C_PROP_REPORT_EXTENSION, false, C_DEFVAL_REPORT_EXTENSION);
    }
}
