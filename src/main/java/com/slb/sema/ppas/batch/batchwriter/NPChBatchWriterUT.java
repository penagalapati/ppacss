////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       NPChBatchWriterUT.java 
//    DATE            :       Sep 17, 2004
//    AUTHOR          :       Urban Wigstrom
//    REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//    COPYRIGHT       :       ATOS ORIGIN 2004
//
//    DESCRIPTION     :       Unit test for Writer component of the Number Plan 
//                          Change batch job.
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
//16/06/08 | M Erskine     | Write to temporary outfiles and  | PpacLon#3650/13137
//         |               | rename on completion.            |
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.NPChBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.NPChBatchRecordData;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for NPChangeBatchWriter. */
public class NPChBatchWriterUT extends BatchTestCaseTT
{
    //
    // Class constants.
    //
    
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String        C_CLASS_NAME          = "NPCutBatchWriterUT";

    /** An input file name vid extension .DAT. */
    private static final String        C_INPUT_FILE_NAME_DAT = "NUMBER_CHANGE_20040615_00003.DAT";
    
    /** A file name vid extension .TMP. */
    private static final String        C_FILE_NAME_TMP_RPT   = "NUMBER_CHANGE_REJECT_20040615_00003.TMP";
    
    /** A file name vid extension .RPT. */
    private static final String        C_FILE_NAME_RPT   = "NUMBER_CHANGE_REJECT_20040615_00003.RPT";

    /** A file name vid extension .RCV. */
    private static final String        C_FILE_NAME_RCV       = "NUMBER_CHANGE_20040615_00003.RCV";

    /** The js job id. */
    private static final String        C_JS_JOB_ID           = "10001";

    /** The file date. */
    private static final String        C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final String        C_SEQ_NO              = "0003";

    /** The sub job counter. */
    private static final String        C_SUB_JOB_COUNT       = "-1";
    
    //
    // Class variables.
    //

    /** Reference to subInstBatch controller object. */
    private static NPChBatchController        c_npChBatchController;
    
    /** Reference to SubInstBatch controller object. */
    private static NPChBatchWriter            c_npChBatchWriter;

    /** ISAPI Service to Batch Control tables. */
    private static PpasBatchControlService    c_isapiControlService = null;

    /**
     * @param p_name Name of the test.
     */
    public NPChBatchWriterUT(String p_name)
    {
        super(p_name, "batch_npc");
        
        c_isapiControlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);
    }
    
    /**
     * @ut.when Error line is "2 ERR", no output line for a .OUT file.
     * @ut.then The record is processed successfully and with the correct output.
     * 
     * Test writeRecord(). Begins with creating a instance in table <code>table baco_batch_control</code>.
     * Creates a <code>NPChBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and checks if
     * the column <code>baco_failure</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */
    public void testWriteRecordSuccessWithErrorLine()
    {
        beginOfTest("testWriteRecordSuccessWithErrorLine");
        NPChBatchRecordData l_dataRecord = new NPChBatchRecordData();

        BatchJobDataSet l_batchjobSet = null;
        BatchJobData l_batchData1 = new BatchJobData();
        BatchJobData l_batchData = null;
        String l_expectedKey = null;
        String l_actualKey = null;
        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";
        String l_executionDateTime = null;
        String l_jsJobId = null;
        Vector l_vec = null;
        String l_actualText = null;
 
        try
        {
            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_npChBatchController.getKeyedControlRecord();

            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE);

            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setOpId(c_npChBatchController.getSubmitterOpid());

            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                c_isapiControlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                
                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                l_dataRecord.setSdpId("0001");
                c_npChBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = c_isapiControlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "0";

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }

            //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_TMP_RPT,
                                                     BatchConstants.C_EXTENSION_TMP_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

            //
            // Recovery file is deleted by tidyUp() 
            // This testcase is not possible to run anymore 
            // 

        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
    
    /**
     * @ut.when No error line is present, output line of "1 OUT" for a .OUT file.
     * @ut.then The record is processed successfully and with the correct output.
     * 
     * Test writeRecord(). Begins with creating a row in table <code>table baco_batch_control</code>.
     * Creates a <code>NPChBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling <code>updateControlInfo(...)</code>. Gets the updated row 
     * from the table and checks if the column <code>baco_success</code> has been updated.
     * So by testing <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> are being tested.
     */
    public void testWriteRecordSuccessNoErrorOneOutputLine()
    {
        beginOfTest("testWriteRecordSuccessNoErrorOneOutputLine");
        NPChBatchRecordData l_dataRecord = new NPChBatchRecordData();

        BatchJobDataSet l_batchjobSet = null;
        BatchJobData l_batchData1 = new BatchJobData();
        BatchJobData l_batchData = null;
        String l_expectedKey = null;
        String l_actualKey = null;
        String l_recoveryLine = "1 REC";
        String l_outputLine = "1 OUT";
        String l_executionDateTime = null;
        String l_jsJobId = null;
        Vector l_vec = null;
        String l_actualText = null;
        
        // The first part of An output file name.
        String l_outputfile_name        = "NPLAN_SDP0001_20040615_";

        try
        {
            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_npChBatchController.getKeyedControlRecord();

            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE);

            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setOpId(c_npChBatchController.getSubmitterOpid());

            try
            {
                //Updates the table baco_batch_control.
                c_isapiControlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                l_dataRecord.setErrorLine(null);
                l_dataRecord.setOutputLine(l_outputLine);
                l_dataRecord.setSdpId("0001");
                c_npChBatchWriter.writeRecord(l_dataRecord);

                l_batchjobSet = c_isapiControlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 0 + BACO_SUCSESS = 1.
                l_expectedKey = l_jsJobId + l_executionDateTime + "0" + "1";

                l_vec = l_batchjobSet.getDataSet();
                l_batchData = (BatchJobData)l_vec.get(0);

                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
                
                l_outputfile_name = l_outputfile_name + getSequenceNumber(BatchConstants.C_NP_SEQUENCE_NAME) +
                                        ".TMP";
                
                //Get the recovery text from the output logfile.
                l_actualText = super.getValueFromLogFile(l_outputfile_name,
                                                         BatchConstants.C_EXTENSION_TMP_FILE,
                                                         BatchConstants.C_OUTPUT_FILE_DIRECTORY);

                assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_INDATA_FILE,
                            ("No file found".equals(l_actualText)));
                assertEquals("Failure: Wrong data in the output file", l_outputLine, l_actualText);

            }
            catch (PpasServiceException e)
            {
                failedTestException(e);
            }
        }
        catch (IOException e)
        {
            failedTestException(e);
        }

        endOfTest();
    }
    
    /**
     * @ut.when Attempting to write trailer lines in the output files for this job.
     * @ut.then The output files are named correctly.
     */
    public void testWriteTrailerRecordSuccess()
    {
        beginOfTest("testWriteTrailerRecordSuccess");
        
        NPChBatchRecordData l_dataRecord = new NPChBatchRecordData();

        BatchJobData l_batchData1 = new BatchJobData();

        String l_recoveryLine = "1 REC";
        String l_outputLine   = "1 OUT";
        String l_return       = null;
        String l_outputFileName;
        
        try
        {
            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_npChBatchController.getKeyedControlRecord();

            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE);

            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setOpId(c_npChBatchController.getSubmitterOpid());

            try
            {
                //Updates the table baco_batch_control.
                c_isapiControlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                l_dataRecord.setErrorLine(null);
                l_dataRecord.setOutputLine(l_outputLine);
                l_dataRecord.setSdpId("0001");
        
                c_npChBatchWriter.writeRecord(l_dataRecord);
                c_npChBatchWriter.writeTrailerRecord(BatchConstants.C_TRAILER_SUCCESS);
                
                String l_seqNum = getSequenceNumber(BatchConstants.C_NP_SEQUENCE_NAME);
                
                l_outputFileName = "NPLAN_SDP0001_20040615_" + l_seqNum + ".DAT";
                
                // Check output file for SDP has been renamed from .TMP to .DAT
                l_return = getValueFromLogFile(l_outputFileName,
                                               BatchConstants.C_EXTENSION_INDATA_FILE, 
                                               BatchConstants.C_OUTPUT_FILE_DIRECTORY);
                
                assertEquals("Wrong text in output file for SDP NP Change.", l_outputLine, l_return);
                
                // Check report file has been renamed from .TMP to .DAT
                l_return = getValueFromLogFile(C_FILE_NAME_RPT,
                                               BatchConstants.C_EXTENSION_REPORT_FILE, 
                                               BatchConstants.C_REPORT_FILE_DIRECTORY);
                
                assertFalse("Failure: Found no file with extension " + BatchConstants.C_EXTENSION_INDATA_FILE,
                           ("No file found".equals(l_return)));
            }
            catch (PpasServiceException e)
            {
                failedTestException(e);
            }
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }
  

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(NPChBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to run all the tests in this
     * class.
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** Perform standard activities at start of a test. */
    protected void setUp()
    {
        super.setUp();
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        
        doSetUp();
    }
    
    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    /**
     * Create context neccessary for this test.
     */
    private static void doSetUp()
    {
        NPCutBatchRecordData l_batchRecordData = new NPCutBatchRecordData();
        Hashtable p_params = new Hashtable();
        SizedQueue l_outQueue = null;
        
        
        //The update frequency needs to be 1 for the test to work.
        c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");

        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_batchRecordData);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            failedTestException(e);
        }

        String l_dir = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String l_fileReportName = l_dir + "/" + C_FILE_NAME_TMP_RPT;
        String l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String l_fileRecoveryName = l_dir2 + "/" + C_FILE_NAME_RCV;
        
        //Make sure that there are no files with the same name.
        deleteFile(l_fileReportName);
        deleteFile(l_fileRecoveryName);

        p_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_FILE_NAME_DAT);

        try
        {
            c_npChBatchController = new NPChBatchController(c_ppasContext,
                                                            BatchConstants.C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE,
                                                            C_JS_JOB_ID,
                                                            "StatusChangeBatchWriterUT",
                                                            p_params);

            c_npChBatchWriter = new NPChBatchWriter(c_ppasContext,
                                                    c_logger,
                                                    c_npChBatchController,
                                                    p_params,
                                                    l_outQueue,
                                                    c_properties);
        }
        catch (IOException e)
        {
            failedTestException(e);
        }

        catch (PpasConfigException e)
        {
            failedTestException(e);
        }
    }
    
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSequence = "getSequence";
    
    /**
     * Gets the sequence number from table <code>NASE_NAMED_SEQUENCE</code>.
     * @param p_sequenceName The fetched sequence number.
     * @return The fetched sequence number.
     */
    private String getSequenceNumber(String p_sequenceName)
    {
        JdbcStatement   l_statement = null;
        JdbcResultSet   l_resultSet = null;
        SqlString       l_sql       = null;
        long            l_seqNo     = 0;
        long            l_maxSeqNo  = 0;
        String          l_returnSeqNo = null;
        String          l_maxSeqNoStr = null;
        
        l_sql = new SqlString(100, 
                              1, 
                              "SELECT NASE_SEQ_NO, NASE_SEQ_MAX_NO from NASE_NAMED_SEQUENCE" +
                              " where NASE_SEQ_NAME = {0}");

        l_sql.setStringParam(0, p_sequenceName);

        try
        {
            l_statement = super.i_connection.createStatement(C_CLASS_NAME,
                                                             C_METHOD_getSequence,
                                                             10220,
                                                             this,
                                                             super.c_ppasRequest);

            l_resultSet = l_statement.executeQuery(C_CLASS_NAME,
                                                   C_METHOD_getSequence,
                                                   10230,
                                                   this,
                                                   super.c_ppasRequest,
                                                   l_sql);

            if (l_resultSet.next(10240))
            {
                l_seqNo = l_resultSet.getLong(10241, "NASE_SEQ_NO");
                l_maxSeqNo = l_resultSet.getLong(10242, "NASE_SEQ_MAX_NO");
            }
            
            
            l_returnSeqNo = new Long(l_seqNo).toString();
            l_maxSeqNoStr = new Long(l_maxSeqNo).toString();       

             while (l_returnSeqNo.length() < l_maxSeqNoStr.length())
            {
                l_returnSeqNo = "0" + l_returnSeqNo;
            }
        
        }
        catch (PpasSqlException e)
        {
            super.failedTestException(e);
        }
        finally
        {
            try
            {
                if (l_resultSet != null)
                {
                    l_resultSet.close(10452);
                }

                if (l_statement != null)
                {
                    l_statement.close(C_CLASS_NAME, C_METHOD_getSequence, 10250, this, super.c_ppasRequest);

                }
            }
            catch (PpasSqlException e)
            {
                super.failedTestException(e);
            }
        }
        return l_returnSeqNo;
    }
}
