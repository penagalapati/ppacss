////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchLineFileReader
//      DATE            :       16-April-2004
//      AUTHOR          :       Urban Wigstrom / Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Implements logic for opening, maintaining and
//                              extracting data from flat files (ASCII files).
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 07/08/06 | M Erskine     | Make certain variables protected | PpacLon#2479/9483
//          |               | instead of private.              |
//          |               | Hold the length of the file, for |
//          |               | use when resetting the file      |
//          |               | pointer after header validation. |
//----------+---------------+----------------------------------+----------------
// 13/03/07 | L. Lundberg   | Correction of a problem reported | PpacLon#2987/11149
//          |               | by the 'FindBugs' tool:          |
//          |               | "In method checkBatchMode(...)   |
//          |               | ignores return value of the      |
//          |               | java.lang.String.replaceFirst(..)|
//          |               | method."                         |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchFileErrorCodeException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class extends BatchReader and implement logic for opening, maintaining
 * and extracting data from flat files (ASCII files). Classes that extract data
 * in a line-per-line basis will exstend this class. This class implements no
 * logic what so ever for making any intelligent decisions of the extracted data,
 * it simply read i tline by line.
 * The class is also responsible for loading recovery information (if in recovery mode)
 * and exclude reading of such records.
 */
public abstract class BatchLineFileReader extends BatchReader
{
    
    /** Delimiter in batch record between line number and batch record. Value is {@value}. */
    protected static final String C_BATCH_RECORD_DELIMITER = ";";

    //------------------------------------------------------
    //  Class level constant
    //------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                     = "BatchLineFileReader";
    
    /** Log message - cannot close recovery file. Value is {@value}. */
    private static final String C_RECOVERY_FILE_CANNOT_BE_CLOSED = "Cannot close recovery file.";
    
    /** Log message - IO exception when reading recovery file. Value is {@value}. */
    private static final String C_RECOVERY_FILE_IO_EXCEPTION     = "Cannot read from recovery file.";
    
    /** Log message - recovery file not found. Value is {@value}. */
    private static final String C_RECOVERY_FILE_NOT_FOUND        = "Recovery file not found.";
    
    /** Log message - No indatafile name given. Value is {@value}. */
    private static final String C_INDATA_FILE_WAS_NULL           = "Indata file was null - no value in Map";

    /** Log message - Cannot open indata file. Value is {@value}. */
    private static final String C_INDATA_FILE_NOT_FOUND          = "Indata file not found - cannot open ";
        
    /** Log message - illegal filename for input file. Value is {@value}. */
    private static final String C_NOT_A_VALID_FILENAME           = "Not a valid filename ";

    /** Log message - cannot close recovery file. Value is {@value}. */
    private static final String C_INPUT_FILE_CANNOT_BE_CLOSED    = "Cannot close input file.";

    /** Log message - cannot close recovery file. Value is {@value}. */
    private static final String C_IO_EXCEPTION_READING_INDATA    = "IO Exception reading from indata file.";

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** Reference to the file subject to processing by batch job. */
    protected LineNumberReader i_inputFileHandle     = null;

    /** String that holds the input file name. */
    protected String         i_inputFileName       = null;

    /** Reference to a hash-table containing recovery information. */
    // Need to be reached from the batch NumberPlanChange
    protected Hashtable      i_recoveryInformation = null;

    
    /** Reference to a hash-table containing recovery information. */
    protected boolean          i_endOfFileReached    = false;
    
    /** Line number of the last read line from input file. */
    protected int            i_lineNumber          = -1;
 
    /** Line number of the last read line from input file. */
    protected String         i_inputLine           = null;
    
    /** Full path and filename to the input file. */
    protected String         i_fullPathName        = null;

    // Variables used by initializeRecordValidation() and getField()
    /** Help variable for working through the input line. */
    protected int              i_startIndex          = -1;
    
    /** Help variable for working through the input line. */
    protected int              i_endIndex            = -1;

    /** Help variable to hold the BatchDataRecord. */
    protected String           i_batchRecord         = null; 

    /** Help array to keep the input line elements. */
    protected String[]         i_recordFields        = null;   
     
    /** The BatchDataRecord to be sent into the Queue. */
    private BatchRecordData  i_helpBatchDataRecord = null;

    /** The <code>Session</code> object. */
    private PpasSession      i_ppasSession         = null;

    /** Account Service to retrieve account data from the database. */
    private PpasAccountService i_ppasAccountService = null;
    
    /** Recovery help variable, counter for all previous successfully processed records. */
    protected int              i_successfullyProcessed    = 0;

    /** Recovery help variable, counter for all previous failed records. */
    protected int              i_notProcessed             = 0;
    
    /** The length of the file being read. */
    protected int              i_fileLength = 0; 

    /**
     * public Constructor for BatchLineReader.
     * If the file type is .IPG the process will be run in recovery mode.
     * @param p_controller  reference to the batch controller.
     * @param p_ppasContext reference to the pPasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  reference to the Map containing information such as filename.
     * @param p_queue       reference to the queue.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public BatchLineFileReader(
        PpasContext      p_ppasContext,
        Logger           p_logger,
        BatchController  p_controller,
        SizedQueue       p_queue,
        Map              p_parameters,
        PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue,  p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10500,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        this.init();
        createAccountService(
            p_ppasContext,
            p_logger);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10510,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        return;
        
    } // end of Constructor





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * This method is reponsible for all necesary initialisation related to file reading.
     * When this method returns, the instance member i_inputFIleHandle is initialised 
     * can be used.
     * If the process is in recovery mode the recovery file created in the last run will be
     * located and opened. Its content will be read into a hash-table, and thereafter the file
     * is closed.
     * The main reason for closing the file at this stage is that the writer will re-use that file.
     */
    protected void init()
    {
        StringBuffer l_tmpFullPathName = new StringBuffer();
        File         l_file = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10520,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }

        // Check if the batch is going to be run in recovery mode. If it is
        // save the recovery information into a Hashtable.
        this.checkBatchMode( (String)i_parameters.get(BatchConstants.C_KEY_INPUT_FILENAME) ); 
        
        
        l_tmpFullPathName.append(this.i_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(this.i_inputFileName);
        i_fullPathName = l_tmpFullPathName.toString();

        l_file = new File(i_fullPathName);
        i_fileLength = (int)l_file.length();

        if ( isFileNameValid(this.i_inputFileName ) )
        {                 
            // Locate and open input file 
            try
            {
                i_inputFileHandle = new LineNumberReader(new FileReader(new File(i_fullPathName)));
            }
            catch (FileNotFoundException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_INDATA_FILE_NOT_FOUND + i_fullPathName,
                                     LoggableInterface.C_SEVERITY_ERROR) );

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "InputFile not found " + C_METHOD_init );
                }
            }        
        }
        else
        {
            this.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent( C_NOT_A_VALID_FILENAME + i_fullPathName,
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10540,
                    this,
                    C_METHOD_init + " InputFile file name not valid " + this.i_inputFileName );
            }

        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10550,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }

        return;
        
    } // end of method init()




    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected abstract BatchRecordData getRecord();





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_checkBatchMode = "checkBatchMode";
    /**
     * Checks if the file extension is ".IPG" then it is recovery mode.
     * If it is opens and reads all rows in the recovery file and saves this
     * data in a hashtable.
     * @param p_fileName Indata filename.
     */
    private void checkBatchMode( String p_fileName )
    {
        // Indexes for element in the RecoveryInformation record:
        // First element is the line number.
        final int    L_RECOVERY_ROW             = 0;
        // Second element is the status of processing the record with the specified line number.
        final int    L_RECOVERY_STATUS          = 1;

        // Directory + Filename of the file with recovery information.
        String       l_fullPathNameRecoveryFile = null;
        // Helpvariable for constructing full path.
        StringBuffer l_tmpFullPathName          = new StringBuffer();
        // Read data from the recovery file. Has the format #line;process status.
        String       l_tmpRecoveryRecord        = null;
        // Help variable to split recovery record.
        String[]     l_tmpRecoveryInformation   = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10560,
                this,
                BatchConstants.C_ENTERING + C_METHOD_checkBatchMode + " " +
                p_fileName + " ext=" + BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
        }  
        if ( p_fileName == null )
        {
            // Report error to Controller
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10570,
                    this,
                    "Input filename was null, detected in : " + C_METHOD_checkBatchMode + 
                    " message sent to controller");
            }
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_INDATA_FILE_WAS_NULL,
                                                    LoggableInterface.C_SEVERITY_ERROR) );
               
        }
        else
        {
            this.i_inputFileName = p_fileName ;

            if ( p_fileName.endsWith(BatchConstants.C_EXTENSION_IN_PROGRESS_FILE)) 
            {
                i_recoveryMode = true;
                
                // If the input file extension is ".IPG" then the batch job has been interrupted in 
                // some way. The recovery file is needed to match already handled records in the
                // input file. The recovery file only has line numbers of the handled records and
                // process status.
                // This information is read into a Hashtable.
                // Checking if recovery mode done in isFileNameValid(.)
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10580,
                        this,
                        "File needs RECOVERY!! " + C_METHOD_checkBatchMode);
                }

                i_recoveryMode             = true;
                this.i_recoveryInformation = new Hashtable();
                
                // Construct recovery file name               
                l_tmpFullPathName.append( this.i_recoveryFileDirectory );
                l_tmpFullPathName.append( "/" );
                l_tmpFullPathName.append( 
                    this.i_inputFileName.replaceFirst( BatchConstants.C_PATTERN_IN_PROGRESS_FILE,
                                                       BatchConstants.C_EXTENSION_RECOVERY_FILE) );
                l_fullPathNameRecoveryFile = l_tmpFullPathName.toString();
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10590,
                        this,
                        "FullPathNameRecoveryFile=" + l_fullPathNameRecoveryFile);
                }

                BufferedReader l_reader = null;
                try
                {
                    l_reader = new BufferedReader( new FileReader( new File(l_fullPathNameRecoveryFile) ) );
                    
                    // Save recovery line numbers in the hashtable
                    while ( l_reader.ready() )
                    {
                        l_tmpRecoveryRecord      = l_reader.readLine();
                        l_tmpRecoveryInformation = l_tmpRecoveryRecord.split(
                            BatchConstants.C_PATTERN_RECOVERY_DELIMITER);

                        this.i_recoveryInformation.put( l_tmpRecoveryInformation[L_RECOVERY_ROW],
                                                        l_tmpRecoveryInformation[L_RECOVERY_STATUS]);
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10600,
                                this,
                                "**READ RECOVERY FILE: key=" + l_tmpRecoveryInformation[L_RECOVERY_ROW] +
                                " value=" + l_tmpRecoveryInformation[L_RECOVERY_STATUS]);
                        }

                    } // end while loop

                    for (Enumeration e = i_recoveryInformation.keys(); e.hasMoreElements();)
                    {
                        // Get file key
                        String l_fileKey = (String)e.nextElement();
                        String l_status  = (String)i_recoveryInformation.get(l_fileKey);
                        if ( l_status.equals(BatchConstants.C_SUCCESSFULLY_PROCESSED))
                        {
                            i_successfullyProcessed++;
                        }
                        else
                        {
                            i_notProcessed++;
                        }
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10600,
                                this,
                                "++recovery information++ : " + l_fileKey);
                        }
                    }
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10600,
                            this,
                            "**SUCCESSFULLY PROCESSED records :" + i_successfullyProcessed);
                    }


                } // end try block
                catch (FileNotFoundException e)
                {
                    sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                    i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILE_NOT_FOUND,
                                                            LoggableInterface.C_SEVERITY_ERROR) );

                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_ERROR,
                            C_CLASS_NAME,
                            10620,
                            this,
                            "Recovery file not found " + C_METHOD_init );
                    }

                }        

                catch (IOException e)
                {
                    sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                    i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILE_IO_EXCEPTION,
                                                            LoggableInterface.C_SEVERITY_ERROR) );
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_ERROR,
                            C_CLASS_NAME,
                            10630,
                            this,
                            "IO exeption reading recovery file " + C_METHOD_init );
                    }

                }
                finally
                {
                    try
                    {
                        if (l_reader != null)
                        {
                            l_reader.close();
                        }
                    }
                    catch (IOException e1)
                    {
                        sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                        i_logger.logMessage( new LoggableEvent( C_RECOVERY_FILE_CANNOT_BE_CLOSED,
                                                                LoggableInterface.C_SEVERITY_ERROR) );
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10640,
                                this,
                                "Problems closing recovery file " + C_METHOD_init );
                        }
                    }
                } // end finally block

            } 
        }
       
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10650,
                this,
                BatchConstants.C_LEAVING + C_METHOD_checkBatchMode);
        }

        return;
        
    } // end of method checkBatchMode
    

    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_faultyRecord = "faultyRecord";    
    /**
     * This method checks if the record did fail last run.
     * If it was successful or not prevoiusly processed the method will
     * return false - if a failed record it will return true.
     * @param p_rowNumber Current row number
     * @return true / false
     */
    protected boolean faultyRecord( int p_rowNumber )
    {
        boolean l_returnValue = false;  // Previously successfull or new record

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10660,
                this,
                BatchConstants.C_ENTERING + C_METHOD_faultyRecord + " rowNumber=" + p_rowNumber);
        }

        if ( i_recoveryMode )
        {
            String  l_status = (String)i_recoveryInformation.get(Integer.toString(p_rowNumber));            
            if ( l_status != null && l_status.equals(BatchConstants.C_NOT_PROCESSED) )
            {
                l_returnValue = true;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10660,
                this,
                BatchConstants.C_LEAVING + C_METHOD_faultyRecord + " returnValue=" + l_returnValue);
        }
        
        return l_returnValue;
    } // End of faultyRecord(.)

 
    
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_tidyUp = "tidyUp";
    /**
     * This method is responsible for closing the file and return any resources related to
     * the file operation. After this method return the instance member i_inputFileHandle is 
     * invalid for use. If the process crashes or is stopped before it is finisched the file
     * will not be renamed.
     * By keeping the filename *.IPG the process will automatically enter recovery mode
     * the next time it is started.
     */
    protected void tidyUp()
    {
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10660,
                this,
                BatchConstants.C_ENTERING + C_METHOD_tidyUp);
        }
        
        // Close the input file
        try
        {
            if (i_inputFileHandle != null)
            {
                this.i_inputFileHandle.close();
                i_inputFileHandle = null;
            }
        }
        catch (IOException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_INPUT_FILE_CANNOT_BE_CLOSED,
                                                    LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10670,
                    this,
                    "TidyUp - IO exception from close() of indata file");
            }                
        }
        
        if ( isAllRecordsProcessed() || this.i_endOfFileReached )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10680,
                    this,
                    "TidyUp - everything is processed, change filename");
            }
            
            renameInputFile( BatchConstants.C_PATTERN_IN_PROGRESS_FILE,
                        BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                        BatchConstants.C_EXTENSION_PROCESSED_FILE );
                                                 
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10690,
                this,
                BatchConstants.C_LEAVING + C_METHOD_tidyUp);
        }

        return;
    } // end of method isRecovery





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameInputFile = "renameInputFile";

    /**
     * Rename input file (changes the file extension).
     * @param p_pattern          Pattern to match.
     * @param p_currentExtension Current extension.
     * @param p_toExtension      New extension.
     */
    protected void renameInputFile( String p_pattern, String p_currentExtension, String p_toExtension )
    {
        String l_newFullPathName = null;       
        File   l_file            = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10700,
                this,
                BatchConstants.C_ENTERING + C_METHOD_renameInputFile + " " + this.i_fullPathName);
        }
        
        l_file = new File( this.i_fullPathName );

        // Change file extension
        if ( this.i_fullPathName.endsWith(p_currentExtension) )
        {
            l_newFullPathName = this.i_fullPathName.replaceFirst( p_pattern, p_toExtension );
            
            if (l_file.renameTo( new File(l_newFullPathName) ) )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10710,
                        this,
                        "RenameFile - SUCCEEDED " + l_newFullPathName);
                }                
            }      
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10720,
                        this,
                        "RenameFile - FAILED " + l_newFullPathName);
                }                
            }
            
            // Save the new full path name 
            this.i_fullPathName = l_newFullPathName;
            
        }            
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10730,
                this,
                BatchConstants.C_LEAVING + C_METHOD_renameInputFile);
        }
        return;
        
    } // End of renameFile(..)




    
    /**
     * @return true if all records has been processed
     */
    private boolean isAllRecordsProcessed()
    {
        boolean l_returnValue = false;
        
        if ( this.i_recoveryInformation != null &&
             this.i_recoveryInformation.size() == this.i_lineNumber )
        {
            l_returnValue = true;
        }
        
        return l_returnValue;
    } // end of method isAllRecordsProcessed()
    
    


    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_readNextLine = "readNextLine";
    /** This method reads the next line from the input file. The instance member i_fileHandle
     * will keep track of the filepointer. The i_fileHandler-object will not be synchronized
     * since only one thread operates on the file. Even if an enhancement of the reader is made no
     * synchronization is required since each file always will be processed by one dedicated thread.
     * When end-of-file is reached this method will return null to indicate that reader may finish.
     * Instance variables: i_rowNumber and i_inputLine are initialized.
     * @return Read line information: #line;readline
     */
    protected String readNextLine()
    {
        boolean      l_readNextRecord = true;               // Loop flag
        String       l_currentLine    = null;               // Line number;Record from the input file
        StringBuffer l_tmpReturnValue = new StringBuffer(); // Help variable for adding line number to
                                                            // the read line
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10740,
                this,
                BatchConstants.C_ENTERING + C_METHOD_readNextLine);
        }
              
        // Check in hash-table if already processed if in recovery mode
        if ( this.isRecovery() )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10750,
                    this,
                    "isRecovery TRUE");
            } 
            
            // Recovery mode
            while ( l_readNextRecord )
            {
                this.readFromInputFile(); // sets i_rowNumber and i_inputLine
                
                if (i_inputLine != null )
                {
                    // Check if this record already has been processed successfully by the BatchProcess
                    if ( isProcessingRequired() )
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10760,
                                this,
                                "BREAK while loop: processing required");
                        } 

                        l_readNextRecord = false;  // Break loop - send this line to processor
                    }
                    
                    // read next record.... 
                    
                }
                else
                {
                    l_readNextRecord = false; // Nothing more to read
                }
                   
            } // End of while loop
        }
        else
        {
            // Reading from an indata file for the first time - no recovery check
            // sets i_lineNumber and i_inputLine
            this.readFromInputFile();
        }
        
        // Add information about line number
        if ( this.i_inputLine != null )
        {
            l_tmpReturnValue.append( this.i_lineNumber );
            l_tmpReturnValue.append( C_BATCH_RECORD_DELIMITER );
            l_tmpReturnValue.append( this.i_inputLine );
            l_currentLine = l_tmpReturnValue.toString();
        }
        else
        {
            i_endOfFileReached = true;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10780,
                this,
                BatchConstants.C_LEAVING + C_METHOD_readNextLine + " currentLine= " + l_currentLine);
        }

        return l_currentLine;
        
    } // end of method readNextLine()




    
    /**
     * Reads a row from the input file, stores lineNumber and input line.
     */
    private void readFromInputFile()
    {
        // LALU: Temp fix.
        // Problem: If a valid input data filename parameter is given when a file driven batch is
        //          started but there is no physical file with that name in the input file directory
        //          the 'i_inputFileHandle' will be 'null' which will cause a 'NullPointerException'
        //          to be raised.
        // NOTE:    This problem should be taken care of in the controller in order to not even start
        //          the reader (or any of the other batch components) if the given input data file
        //          doesn't exist!
        if (i_inputFileHandle != null)
        {
            try
            {
                    this.i_inputLine  = this.i_inputFileHandle.readLine();
                    this.i_lineNumber = this.i_inputFileHandle.getLineNumber();
            }
            catch (IOException e)
            {
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_IO_EXCEPTION_READING_INDATA,
                                                        LoggableInterface.C_SEVERITY_ERROR) );
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10790,
                        this,
                        "Reading a line no=" + i_lineNumber + " record:" + this.i_inputLine);
                } 
            }
        }
        
    } // End of readFromInputFile()
    




    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isProcessingRequired = "isProcessingRequired";
    
    /**
     * This method checks if the record already has been correctly processed or not.
     * @return <code>true</code>  means it needs to be re-processed 
     *         <code>false</code> has already been successfully processed
     */
    // need to redefined this method for the NumberPlanChange batch
    protected boolean isProcessingRequired()
    {
        boolean l_processTheLine = false;
        String  l_recordStatus   = null;                                               // Can be "ERR"/"OK"
        String  l_key            = Integer.toString(this.i_lineNumber);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10800,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isProcessingRequired + " l_key=" + 
                this.i_lineNumber + "  ->" + l_key);
        }

        l_recordStatus = (String)this.i_recoveryInformation.get( l_key );
        
        if ( l_recordStatus != null ) 
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10810,
                    this,
                    "isProcessingRequired : recordStatus=" + l_recordStatus);
            }        
      
            if (!l_recordStatus.equals(BatchConstants.C_SUCCESSFULLY_PROCESSED))
            {
                l_processTheLine = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10820,
                        this,
                        "isProcessingRequired : process the line!!");
                }        

            }
        }
        else
        {
            l_processTheLine = true;
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10830,
                    this,
                    "could not find this record in the hashtable - send it to processing!");
            }        
            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10840,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isProcessingRequired);
        }
                
        return l_processTheLine;
        
    } // end of method isProcessingRequired()
    
    


    
    /**
     * Checks if the filename is in the right format.
     * Use regular expressions defined in batchutil.BatchConstants.java
     * @param p_fileName filename
     * @return           <code>true</code> if valid format else <code>false</code>.
     */
    protected abstract boolean isFileNameValid( String p_fileName );
    
    



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_splitMarketIntoServiceAreaAndLocation =
        "splitMarketIntoServiceAreaAndLocation";
   /**
    * This method splits the 'market' into Service Area and Service Location.
    * The market can be 4 or 8 characters (XXZZ or XXXXZZZZ)- this method handle this.
    * @param p_market 4 or 8 character long string representing the concatination of
    *                 ServiceArea and Service Location.
    * @return String array of three elements, first element is the Service Area and the second
    *         element is the Service Location. The third element is the 'market' (if it was 4
    *         characters from the beginning it now has been converted into 8 characters.
    */
    protected String[] splitMarketIntoServiceAreaAndLocation( String p_market )
    {
        final int    L_NUMBER_OF_ELEMENTS     = 3;    // Number of elements in the returned array
        final int    L_SERVICE_AREA_INDEX     = 0;    // First element is the Service Area
        final int    L_SERVICE_LOCATION_INDEX = 1;    // Second element is the Service Location
        final int    L_MARKET_INDEX           = 2;    // Third element is the market 8 characters long
        final int    L_SHORT_MARKET_LENGTH    = 4;    // Length of Market if only of the format XXZZ
        final int    L_LONG_MARKET_LENGTH     = 8;    // Length of Market if only of the format XXXXZZZZ
        final String L_ZERO_FILL              = "00"; // Zero pad
        
        String[]     l_serviceAreaAndLocation = new String[L_NUMBER_OF_ELEMENTS]; 
        StringBuffer l_tmp                    = new StringBuffer(L_LONG_MARKET_LENGTH); 
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10850,
                this,
                BatchConstants.C_ENTERING + C_METHOD_splitMarketIntoServiceAreaAndLocation);
        }
                
        switch ( p_market.length() )
        {
            case L_SHORT_MARKET_LENGTH:
                l_serviceAreaAndLocation[L_SERVICE_AREA_INDEX]     =
                                                          p_market.substring(0, (L_SHORT_MARKET_LENGTH / 2));
                l_serviceAreaAndLocation[L_SERVICE_LOCATION_INDEX] =
                                                          p_market.substring(L_SHORT_MARKET_LENGTH / 2);

                // Convert to format XXXXZZZZ
                l_tmp.append( L_ZERO_FILL );
                l_tmp.append( l_serviceAreaAndLocation[L_SERVICE_AREA_INDEX] );
                l_tmp.append( L_ZERO_FILL );
                l_tmp.append( l_serviceAreaAndLocation[L_SERVICE_LOCATION_INDEX] );
                l_serviceAreaAndLocation[L_MARKET_INDEX] = l_tmp.toString();

                break;                
 
            case L_LONG_MARKET_LENGTH:
                l_serviceAreaAndLocation[L_SERVICE_AREA_INDEX]     =
                                                          p_market.substring(0, (L_LONG_MARKET_LENGTH / 2));
                l_serviceAreaAndLocation[L_SERVICE_LOCATION_INDEX] =
                                                          p_market.substring(L_LONG_MARKET_LENGTH / 2);
                l_serviceAreaAndLocation[L_MARKET_INDEX]           = p_market;

                break;                
                
            default:
                System.out.println("Unexpected length of the XXYY - Service area and location field");
                l_serviceAreaAndLocation = null;
                break;                
               
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10860,
                this,
                BatchConstants.C_LEAVING + C_METHOD_splitMarketIntoServiceAreaAndLocation);
        }

        return l_serviceAreaAndLocation;    
        
    } // end of splitMarketIntoServiceAreaAndLocation(.)





    /**
     * This method pads "0" in front of p_string, maximum number
     * of characters is four.
     * @param p_string  the string to be padded.
     * @return the inputstring with zero padded.
     */
    protected String padWithZero( String p_string ) 
    {
        final String L_ZERO          = "0";
        final int    L_MAX_LENGTH    = 4;
        
        String       l_string        = "";
        int          l_numberOfZeros = 0;
        StringBuffer l_tmp           = null;
        String       l_returnValue   = null;
                
        if ( p_string != null &&
             p_string.length() <= L_MAX_LENGTH )
        {
            l_string        = p_string;
            l_numberOfZeros = L_MAX_LENGTH - p_string.length();
            l_tmp           = new StringBuffer(L_MAX_LENGTH);
       
            for ( int i = 0; i < l_numberOfZeros; i++ )
            {
                l_tmp.append(L_ZERO);
            }
            l_tmp.append(l_string);
            l_returnValue = l_tmp.toString();
        } 

        return l_returnValue;
    }

    /**
     * Initialize method - should be called before the getField().
     * @param p_batchRecord     The record.
     * @param p_recordFields    Array of fields.
     * @param p_batchRecordData Object containing the data.
     */
    protected void initializeRecordValidation(String          p_batchRecord,
                                              String[]        p_recordFields,
                                              BatchRecordData p_batchRecordData )
    {
        i_startIndex          = 0;
        i_endIndex            = 0;
        
        i_batchRecord         = p_batchRecord;
        i_recordFields        = p_recordFields;
        i_helpBatchDataRecord = p_batchRecordData;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    protected static final String C_METHOD_getField = "getField";        
    /**
     * This method selects the substring represented by the start- and end index.
     * Stores this substring in the private string array: i_recordFields
     * @param p_startIndex  start index of the substring
     * @param p_endIndex    end index of the substring. (e.g. the index after the last selected)
     * @param p_fieldNumber index in the recordField array for the selected substring.
     * @param p_format      a regular expression to verify the character in the substring defined by
     * start- and endindex
     * @param p_errorCode  the error code.
     * @return <code>true</code> substring is correct <code>false</code> substring not ok e.g. not
     * alphanumberic.
     */
    protected boolean getField(int    p_startIndex,
                               int    p_endIndex,
                               int    p_fieldNumber,
                               String p_format,
                               String p_errorCode)
    {
        boolean      l_validData = true;  // Assume it is OK
        StringBuffer l_tmp       = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10870,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getField );
        }

        i_startIndex = i_startIndex + p_startIndex;
        i_endIndex   = i_endIndex   + p_endIndex;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10890,
                this,
                "innan index test! start=" + i_startIndex + " end=" + i_endIndex +
                " length=" + i_batchRecord.length());
        }

        
        // Does the record have this field too?
        if ( i_startIndex >= i_batchRecord.length() )
        {
            // This field is not in the record - it is OK!
            this.i_recordFields[p_fieldNumber] = null;
        }
        else
        {
            if (i_endIndex > i_batchRecord.length() )
            {
                // This range is not in the record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10890,
                        this,
                        C_METHOD_getField + "End index out of range! start=" + i_startIndex +
                        " end=" + i_endIndex + " length=" + i_batchRecord.length());
                }

                l_validData = false;
            }
            else
            {
                // OK - this field is inside the record
                i_recordFields[p_fieldNumber] = i_batchRecord.substring(i_startIndex, i_endIndex).trim();
                
                if ( !this.i_recordFields[p_fieldNumber].matches(p_format) )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10900,
                            this,
                            "field:" + p_fieldNumber + " does not match format" );
                    }

                    l_tmp = new StringBuffer();
                    l_tmp.append(p_errorCode);
                    l_tmp.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmp.append(this.i_helpBatchDataRecord.getInputLine());
                    this.i_helpBatchDataRecord.setErrorLine( l_tmp.toString());
                          
                    l_validData = false;                   
                }

            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10900,
                    this,
                    "field:" + p_fieldNumber + " ]" + this.i_recordFields[p_fieldNumber] + "[" );
            }


            // If 'empty string' then use NULL instead
            if ( this.i_recordFields[p_fieldNumber] != null &&
                 this.i_recordFields[p_fieldNumber].length() == 0 )
            {
                this.i_recordFields[p_fieldNumber] = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10910,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getField );
        }

        return l_validData;

    } // end of method getField(...)
    
    
    
        

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    protected static final String C_METHOD_getFieldNoTrim = "getFieldNoTrim";        
    /**
     * This method selects the substring represented by the start- and end index.
     * Stores this substring in the private string array: i_recordFields
     * @param p_startIndex  start index of the substring
     * @param p_endIndex    end index of the substring. (i.e. the index after the last selected)
     * @param p_fieldNumber index in the recordField array for the selected substring.
     * @param p_format      a regular expression to verify the character in the substring defined by
     * start- and endindex
     * @param p_errorCode  the error code.
     * @return <code>true</code> substring is correct <code>false</code> substring not ok e.g. does 
     * not match the field pattern. Note right-adjusted and space-filled will need a 
     * pattern like: "(^[ ]*[0-9]*";
     */
    protected boolean getFieldNoTrim(int    p_startIndex,
                                     int    p_endIndex,
                                     int    p_fieldNumber,
                                     String p_format,
                                     String p_errorCode)
    {
        boolean      l_validData = true;  // Assume it is OK
        StringBuffer l_tmp       = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10870,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getFieldNoTrim );
        }

        i_startIndex = i_startIndex + p_startIndex;
        i_endIndex   = i_endIndex   + p_endIndex;
        
        // Does the record have this field too?
        if ( i_startIndex >= i_batchRecord.length() )
        {
            // This field is not in the record - it is OK!
            this.i_recordFields[p_fieldNumber] = null;
        }
        else
        {
            if (i_endIndex > i_batchRecord.length() )
            {
                // This range is not in the record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10890,
                        this,
                        C_METHOD_getFieldNoTrim + "End index out of range! start=" + i_startIndex +
                        " end=" + i_endIndex + " length=" + i_batchRecord.length());
                }

                l_validData = false;
            }
            else
            {
                // OK - this field is inside the record
                i_recordFields[p_fieldNumber] = i_batchRecord.substring(i_startIndex, i_endIndex);
        
                if ( !this.i_recordFields[p_fieldNumber].matches(p_format) )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10900,
                            this,
                            "field:" + p_fieldNumber + " does not match format: ]" + p_format + "[");
                    }

                    l_tmp = new StringBuffer();
                    l_tmp.append(p_errorCode);
                    l_tmp.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmp.append(this.i_helpBatchDataRecord.getInputLine());
                    this.i_helpBatchDataRecord.setErrorLine( l_tmp.toString());
                          
                    l_validData = false;                   
                }

            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10900,
                    this,
                    "field:" + p_fieldNumber + " ]" + this.i_recordFields[p_fieldNumber] + "[" );
            }


            // If 'empty string' or all blanks then use NULL instead
            if ( this.i_recordFields[p_fieldNumber] != null &&
                 this.i_recordFields[p_fieldNumber].trim().length() == 0 )
            {
                this.i_recordFields[p_fieldNumber] = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10910,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getFieldNoTrim );
        }

        return l_validData;

    } // end of method getFieldNoTrim(...)
    
    
    
    
    
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateRecordLength = "validateRecordLength";

    /**
     * Throws an exception if the given record is not the given length.
     * @param p_record Record to be tested.
     * @param p_expectedLength Expected length of record.
     * @throws BatchFileErrorCodeException Invalid length.
     */
    protected void validateRecordLength(
        String                        p_record,
        int                           p_expectedLength)
        throws BatchFileErrorCodeException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11100, this,
                "Entered " + C_METHOD_validateRecordLength);
        }

        if ( p_expectedLength != p_record.length() )
        {
            BatchFileErrorCodeException l_batchFileErrorCodeException =
                new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateRecordLength,
                    20010,
                    this,
                    null,
                    0,
                    BatchKey.get().batchWrongRecordLength(p_expectedLength, p_record.length()));

            throw l_batchFileErrorCodeException;
            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11190, this,
                BatchConstants.C_LEAVING + C_METHOD_validateRecordLength);
        }

    } // end of validateRecordLength


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateMsisdnField = "validateMsisdnField";

    /**
     * Converts a string representing a Msisdn in a batch input file to a Msisdn Object.
     * Throws an exception if it cannot be converted.
     * @param p_msisdnString String representing Msisdn.
     * @return The converted Msisdn.
     * @throws BatchFileErrorCodeException String cannot be converted.
     */
    protected Msisdn validateMsisdnField(
        String                        p_msisdnString)
        throws BatchFileErrorCodeException
    {
        Msisdn                         l_msisdn = null;
        MsisdnFormat                   l_msisdnFormat = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11200, this,
                BatchConstants.C_ENTERING + C_METHOD_validateMsisdnField);
        }

        l_msisdnFormat = i_context.getMsisdnFormatInput();
        try
        {
            l_msisdn = l_msisdnFormat.parse(p_msisdnString.trim());
        }
        catch (ParseException l_parseEx)
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateMsisdnField,
                    20020,
                    this,
                    null,
                    0,
                    BatchKey.get().batchParsingOfMsisdnFailed(p_msisdnString.trim()),
                    l_parseEx);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11290, this,
                BatchConstants.C_LEAVING + C_METHOD_validateMsisdnField);
        }

        return l_msisdn;
    } // end of validateMsisdnField


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateAlphaNumericField = "validateAlphaNumericField";

    /**
     * Verifies that the string input is an AlphaNumeric string, otherwise an
     * exception is thrown.
     * @param p_string The string input.
     * @return The inputted string.
     * @throws BatchFileErrorCodeException The string is not AlphaNumeric.
     */
    protected String validateAlphaNumericField(
        String                        p_string)
        throws BatchFileErrorCodeException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11300, this,
                BatchConstants.C_ENTERING + C_METHOD_validateAlphaNumericField);
        }

        if ( !p_string.matches(BatchConstants.C_PATTERN_ALPHA_NUMERIC) )
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateAlphaNumericField,
                    20030,
                    this,
                    null,
                    0,
                    BatchKey.get().batchNotAlphaNumeric(p_string));
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11390, this,
                BatchConstants.C_LEAVING + C_METHOD_validateAlphaNumericField);
        }

        return p_string;
    } // end of validateAlphaNumericField


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateDateField = "validateDateField";

    /**
     * Converts a string in the YYYYMMDD HH:mm:ss format to a PpasDateTime.
     * Throws an exception if cannot be converted.
     * @param p_dateString Date string in the YYYYMMDD HH:mm:ss format.
     * @return Date as a PpasDateTime.
     * @throws BatchFileErrorCodeException The string is not PpasDateTime.
     */
    protected PpasDateTime validateDateField(
        String                        p_dateString)
        throws BatchFileErrorCodeException
    {
        PpasDateTime l_ppasDateTime = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11400, this,
                BatchConstants.C_ENTERING + C_METHOD_validateDateField);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11490, this,
                BatchConstants.C_LEAVING + C_METHOD_validateDateField);
        }

        try
        {
            l_ppasDateTime = new PpasDateTime(p_dateString);
        }
        catch (PpasSoftwareException l_ppasSoftwareException)
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateDateField,
                    20040,
                    this,
                    null,
                    0,
                    BatchKey.get().batchDateFormatError(p_dateString, PpasDate.C_DF_yyyyMMdd.toPattern()),
                    l_ppasSoftwareException);
        }

        // Throws a BatchFileErrorCodeException if java Date class
        // converts the date incorrectly.
        checkJavaDateConversion(p_dateString, l_ppasDateTime);

        return l_ppasDateTime;
    } // end of validateDateField

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateMasterMsisdn = "validateMasterMsisdn";

    /**
     * Validates a msisdn against the database. Makes sure it is a standalone or
     * master.  If successful returns the cust_id, otherwise throws an exception.
     * @param p_msisdn Msisdn to validate.
     * @return Cust Id off Msisdn.
     * @throws BatchFileErrorCodeException If Msisdn is invalid.
     */
    protected String validateMasterMsisdn(
        Msisdn                        p_msisdn)
        throws BatchFileErrorCodeException
    {
        PpasRequest                    l_request      = null;
        String                         l_opId         = null;
        BasicAccountData               l_basicAccountData = null;
        long                           l_custStatusFlags = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11500, this,
                BatchConstants.C_ENTERING + C_METHOD_validateMasterMsisdn);
        }

        // Create PpasRequest object
        l_opId         = getController().getSubmitterOpid();
        l_request   = new PpasRequest(i_ppasSession, l_opId, p_msisdn);

        // Do not retrieve subordinates or disconnected subscribers.
        l_custStatusFlags = PpasService.C_FLAG_BLOCK_TRANS_ALL;

        try
        {
            l_basicAccountData = i_ppasAccountService.getBasicData(
                l_request,
                l_custStatusFlags,
                i_timeout);
                                                                 
        }
        catch (PpasServiceException l_ppasServEx)
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateMasterMsisdn,
                    20040,
                    this,
                    null,
                    0,
                    BatchKey.get().batchFailedToGetAccountData(p_msisdn),
                    l_ppasServEx);
        }
        
        if ( l_basicAccountData.isSubordinate() )
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_validateMasterMsisdn,
                    20042,
                    this,
                    null,
                    0,
                    BatchKey.get().batchFailedToGetAccountData(p_msisdn));
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11590, this,
                BatchConstants.C_LEAVING + C_METHOD_validateMasterMsisdn);
        }

        return l_basicAccountData.getCustId();
    } // end of validateMasterMsisdn

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getBasicAccountDataFromMsisdn = "getBasicAccountDataFromMsisdn";
    
    /**
     * Get BasicAccountData from MSISDN.
     * @param p_msisdn Msisdn retreived from Record.
     * @return BasicAccountData which is conresponding with MSISDN.
     * @throws BatchFileErrorCodeException If Msisdn is invalid.
     */
    protected BasicAccountData getBasicAccountDataFromMsisdn(
    Msisdn                        p_msisdn)
    throws BatchFileErrorCodeException
    {
        PpasRequest                    l_request      = null;
        String                         l_opId         = null;
        BasicAccountData               l_basicAccountData = null;
        long                           l_custStatusFlags = 0;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                            PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11500, this,
                            BatchConstants.C_ENTERING + C_METHOD_getBasicAccountDataFromMsisdn);
        }
        
        // Create PpasRequest object
        l_opId         = getController().getSubmitterOpid();
        l_request   = new PpasRequest(i_ppasSession, l_opId, p_msisdn);
        
        // Do not retrieve subordinates or disconnected subscribers.
        l_custStatusFlags = PpasService.C_FLAG_BLOCK_TRANS_ALL;
        
        try
        {
            l_basicAccountData = i_ppasAccountService.getBasicData(
                                                                   l_request,
                                                                   l_custStatusFlags,
                                                                   i_timeout);
            
        }
        catch (PpasServiceException l_ppasServEx)
        {
            throw new BatchFileErrorCodeException(
                                                  C_CLASS_NAME,
                                                  C_METHOD_validateMasterMsisdn,
                                                  20040,
                                                  this,
                                                  null,
                                                  0,
                                                  BatchKey.get().batchFailedToGetAccountData(p_msisdn),
                                                  l_ppasServEx);
        }       
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                            PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME, 11590, this,
                            BatchConstants.C_LEAVING + C_METHOD_getBasicAccountDataFromMsisdn);
        }
        
        return l_basicAccountData;
        
    }
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_createAccountService = "createAccountService";

    /**
     * Creates the PpasAccountService.
     * @param p_ppasContext reference to the pPasContext.
     * @param p_logger      reference to the logger.
     */
    private void createAccountService(
        PpasContext      p_ppasContext,
        Logger           p_logger)
    {
        PpasRequest                    l_ppasRequest = null;
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11700, this,
                BatchConstants.C_ENTERING + C_METHOD_createAccountService);
        }

        i_ppasSession        = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest        = new PpasRequest(i_ppasSession);

        i_ppasAccountService = new PpasAccountService(l_ppasRequest, p_logger, p_ppasContext);
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11790, this,
                BatchConstants.C_LEAVING + C_METHOD_createAccountService);
        }

    } // end of createAccountService


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_checkJavaDateConversion = "checkJavaDateConversion";

    /** Throws an exception if the java Date class has converted the date incorrectly.
     *  @param p_dateString Correct value of date.
     *  @param p_ppasDateTime Value after java Date conversion.
     * @throws BatchFileErrorCodeException  if the date string has an invalid format.
     */
    private void checkJavaDateConversion(
        String                        p_dateString,
        PpasDateTime                  p_ppasDateTime)
        throws BatchFileErrorCodeException
    {
        // The java Date class will convert for example 20050132 too 20050201.
        // This is a feature of java.  We must guide against this in batch.
        // Make sure the string of the new date class is equal to the original
        // date.

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10100, this,
                            BatchConstants.C_ENTERING + C_METHOD_checkJavaDateConversion);
        }

        if ( !p_dateString.equals(p_ppasDateTime.toString_yyyyMMdd_HHcmmcss()) )
        {
            throw new BatchFileErrorCodeException(
                    C_CLASS_NAME,
                    C_METHOD_checkJavaDateConversion,
                    20041,
                    this,
                    null,
                    0,
                    BatchKey.get().batchDateFormatError(p_dateString, PpasDate.C_DF_yyyyMMdd.toPattern()));
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10190, this,
                            BatchConstants.C_LEAVING + C_METHOD_checkJavaDateConversion);
        }

    } // end of checkJavaDateConversion

} // end of class BatchLineFileReader
