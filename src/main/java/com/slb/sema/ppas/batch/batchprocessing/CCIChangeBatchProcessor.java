////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       CCIChangeBatchProcessor
//      DATE            :       30-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.CCIChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.CommunitiesIdListData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class performs the record processing. It uses the administrative business service: 
 * updateCommunity (PpasAccountService)
 */
public class CCIChangeBatchProcessor extends BatchProcessor
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String C_CLASS_NAME = "CCIChangeBatchProcessor";

    /** Constant holding the error code for a non master MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_MASTER_ACCOUNT             = "02";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_EXISTS                     = "03";

    /** Constant holding the error code for a inactive account. Value is {@value}. */
    private static final String C_ERROR_CODE_INACTIVE_ACCOUNT                      = "04";

    /** Constant holding the error code for invalid community charging identity. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_COMMUNITY_CHARGING_IDENTITY   = "05";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_OLD_COMMUNITY_CHARGING_LIST   = "06";
    
    /** Constant holding the error code for invalid old community charging list. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_NEW_COMMUNITY_CHARGING_LIST   = "07";
        
    /** Constant holding the error code for invalid new community charging list. Value is {@value}. */
    private static final String C_ERROR_CODE_TEMPORARY_BLOCKING_APPLIED_TO_ACCOUNT = "08";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasAccountService i_ppasAccountService  = null;

    /** The <code>Session</code> object. */
    private PpasSession        i_ppasSession         = null;
    
    /**
     * Constructs an instance of this <code>CCIChangeBatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     *
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process paramters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public CCIChangeBatchProcessor(PpasContext     p_ppasContext,
                                   Logger          p_logger,
                                   BatchController p_batchController,
                                   SizedQueue      p_inQueue,
                                   SizedQueue      p_outQueue,
                                   Map             p_params,
                                   PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        PpasRequest l_ppasRequest = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        i_ppasSession        = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest        = new PpasRequest(i_ppasSession);
        i_ppasAccountService = new PpasAccountService(l_ppasRequest, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    }

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * Processes the given <code>BatchDataRecord</code> and returns it.
     * If the process succeeded the <code>BatchDataRecord</code> is updated with information
     * needed by the writer component before returning it.
     * If the process fails but the record shall be re-processed at the end of this process,
     * the record will be put in the retry queue and this method returns <code>null</code>.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @see BatchProcessor#processRecord(BatchRecordData)
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        String                   l_opId             = null;
        Msisdn                   l_msisdn           = null;
        PpasRequest              l_ppasRequest      = null;
        CCIChangeBatchRecordData l_record           = null;
        
        CommunitiesIdListData    l_oldCCI           = null;
        CommunitiesIdListData    l_newCCI           = null;
        
        StringBuffer             l_tmpErrorLine     = new StringBuffer();
        StringBuffer             l_tmpRecoveryLine  = new StringBuffer();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }
        
        l_record = (CCIChangeBatchRecordData)p_record;
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10030, this,
                            "Dump record: " + l_record.dumpRecord());
        }
        
        // Always prepare a recovery line
        l_tmpRecoveryLine.append(l_record.getRowNumber());
        l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);
        
        if ( !l_record.isCorruptLine() )
        {
            // Create an unique PpasRequest for the current MSISDN
            l_opId             = super.i_batchController.getSubmitterOpid();
            l_msisdn           = l_record.getMsisdn();
            l_ppasRequest      = new PpasRequest(i_ppasSession, l_opId, l_msisdn);
            
            // Retrieve arrays with old and new CCI values
            l_oldCCI = l_record.getOldCCIList();
            l_newCCI = l_record.getNewCCIList();

            try
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10040, this,
                                    "old= " + l_oldCCI + " new= " + l_newCCI);
                }
                i_ppasAccountService.updateCommunity(l_ppasRequest, i_isApiTimeout, l_oldCCI, l_newCCI );
                
                l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);
            }
            
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10050, this,
                                    "PpasServiceException is caught: " + p_psExe.getMessage() + ",  key: " + p_psExe.getMsgKey());
                }
                
                l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);

                // This can be an "retryable" error.
                if ( (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_NOT_MASTER)) ||
                     (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MASTER_IS_SUBORDINATE)) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_MSISDN_NOT_MASTER_ACCOUNT);

                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10060, this,
                                        "Invalid master account/not master: " + p_psExe.getMsgKey() +
                                        " " + l_record.getInputLine());
                    }
                }
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS) ||
                         p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT ) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_MSISDN_NOT_EXISTS);

                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10070, this,
                                        "Msisdn does not exist: " + p_psExe.getMsgKey() + " " + l_record.getInputLine());
                    }
                } 
                
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_INACTIVE_ACCOUNT);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10080,
                                        this,
                                        "Account not active: " + p_psExe.getMsgKey() + " " + l_record.getInputLine());
                    }
                }  
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_COMMUNITY_ID) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_INVALID_COMMUNITY_CHARGING_IDENTITY);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10090, this,
                                        "Invalid community charging identity: " + p_psExe.getMsgKey() + " " +
                                        l_record.getInputLine());
                    }
                }  
                
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_OLD_COMMUNITY_LIST) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_INVALID_OLD_COMMUNITY_CHARGING_LIST);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10100, this,
                                        "Invalid OLD community charging list: " + p_psExe.getMsgKey() + " " +
                                        l_record.getInputLine());
                    }
                }  
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_NEW_COMMUNITY_LIST) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_INVALID_NEW_COMMUNITY_CHARGING_LIST);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10110, this,
                                        "Invalid NEW community charging list: " + p_psExe.getMsgKey() + " " +
                                        l_record.getInputLine());
                    }
                } 
                else if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_CUST_IS_TEMP_BLOCKED) )
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_TEMPORARY_BLOCKING_APPLIED_TO_ACCOUNT);
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10120, this,
                                        "Temporary blocking applied to account: " + p_psExe.getMsgKey() + " " +
                                        l_record.getInputLine());
                    }
                }  
                else
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10130, this,
                                        "** ADD THIS MSG_KEY INTO CCIChargeBatchProcessor :  " + l_record.getInputLine() + 
                                        ",   msg key: " + p_psExe.getMsgKey());
                    }

                    l_tmpErrorLine.append(p_psExe.getMsgKey());
                }
                
                l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                l_tmpErrorLine.append(l_record.getInputLine());
                l_record.setErrorLine(l_tmpErrorLine.toString());
            }
        }
        else
        {
            l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10140, this,
                            BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }
        
        l_record.setRecoveryLine(l_tmpRecoveryLine.toString());

        return l_record;
    }
}
