////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchConstants
//      DATE            :       3-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class has constants used by several other
//                              classes in the batch module.
//
///////////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
///////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+-----------------------
// 05/05/06 | Chris         | Added Disconnection Reason Code  | PpacLon#2250/8691
//          | Harrison      | filtering constants.             | PRD_ASCS00_GEN_CA_82
//----------+---------------+----------------------------------+-----------------------
// 21/06/07 | Ian James     | Add Batch Subscriber Segmentaion | PpacLon#3183/11755
//          |               | constant.                        | PRD_ASCS00_GEN_CA_129
//----------+---------------+----------------------------------+-----------------------
// 05/07/07 | E Dangoor     | Added new property name constant | PpacLon#3195/11793
///////////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchutil;

/**
 * This class defines constants that are used by all batch programs.
 * For example properties names.
 * 
 */
public class BatchConstants
{
    //------------------------------------------------------------------------------
    // Constants for the properties name
    //------------------------------------------------------------------------------
    /** Specifies the name for the ASCS properties. Value is {@value}. */
    public static final String C_CONFIGURATION_PROPERTIES = "com.slb.sema.ppas.common.configProperties";

    // From properties file batch.properties
    /** Specifies the directory where input files are located if applicable. Value is {@value}. */
    public static final String C_INPUT_FILE_DIRECTORY     = "com.slb.sema.ppas.batch.inputFileDirectory";

    /** Specifies the directory where error files will be written if applicable. Value is {@value}. */
    public static final String C_REPORT_FILE_DIRECTORY    = "com.slb.sema.ppas.batch.reportFileDirectory";

    /** Specifies the directory where recovery files will be written if applicable. Value is {@value}. */
    public static final String C_RECOVERY_FILE_DIRECTORY  = "com.slb.sema.ppas.batch.recoveryFileDirectory";

    /** Specifies the directory where output files will be written if applicable. Value is {@value}. */
    public static final String C_OUTPUT_FILE_DIRECTORY    = "com.slb.sema.ppas.batch.outputFileDirectory";



    // From properties file batch_XX.properties
    /** Specifies the maximum number of objects (records) in the in queue. Value is {@value}. */  
    public static final String C_IN_QUEUE_MAX_SIZE              = "com.slb.sema.ppas.batch.inQueueMaxSize";

    /** Specifies the maximum number of objects (records) in the out queue. Value is {@value}. */
    public static final String C_OUT_QUEUE_MAX_SIZE             = "com.slb.sema.ppas.batch.outQueueMaxSize";
   
    /** Specifies how many reader threads that will be started. Should currently
     * always be 1 and this is ensured by over-reiding in the code. Value is {@value}.
     */
    public static final String C_NUMBER_OF_READER_THREADS       = 
        "com.slb.sema.ppas.batch.numberOfReaderThreads";

    /** Specifies how many writer threads that will be started. Should currently
     * always be 1 and this is ensured by over-reiding in the code. Value is {@value}.
     */
    public static final String C_NUMBER_OF_WRITER_THREADS       =
        "com.slb.sema.ppas.batch.numberOfWriterThreads";

    /** Specifies how many processor threads that will be started. This value is 
     * batch process specific. Value is {@value}.
     */
    public static final String C_NUMBER_OF_PROCESSOR_THREADS    = 
        "com.slb.sema.ppas.batch.numberOfProcessorThreads";

    /** Specifies in which interval the control table should be updated. Value is {@value}. */
    public static final String C_CONTROL_TABLE_UPDATE_FREQUENCY = 
        "com.slb.sema.ppas.batch.controlTableUpdateFrequency";

    /** Specifies the fetch array size used in select cursor for database driven batch jobs.
     * Value is {@value}. */
    public static final String C_FETCH_ARRAY_SIZE               = 
        "com.slb.sema.ppas.batch.fetchArraySize";

    /** Specifies the timeout time to wait for connection. Value is {@value}. */
    public static final String C_TIMEOUT                        = 
        "com.slb.sema.ppas.batch.timeout";

    /** Specifies the time the thread will sleep before it runs the retry queue. Value is {@value}. */
    public static final String C_TIME_BEFOR_RETRY = "com.slb.sema.ppas.batch.timeBeforeRetry";
    
    /** Specifies the max chunk size for the routing batches. */
    public static final String C_MAX_CHUNK_SIZE   = "com.slb.sema.ppas.batch.maxChunkSize";
    
    /** Specifies the max number of inner process threads for the routing batches. */
    public static final String C_MAX_NUMBER_OF_THREADS = "com.slb.sema.ppas.batch.maxNoOfThreads";

    /** Specifies Disconnection Reason codes used to filter records. */
    public static final String C_FILTER_DISCONNECTION_REASON_CODES = "com.slb.sema.ppas.batch.filterDisconnectionReasonCodes";

    /** Specifies the sleep interval between successive processing of records. */
    public static final String C_WAIT_PERIOD_BETWEEN_REQUEST = "com.slb.sema.ppas.batch.waitPeriodBetweenRequest";

    // Batch master properties. 
    /** Specifies the number of sub-processes used for the Disconnect batch. */
    public static final String C_NUMBER_OF_DISCONNECT_SUBPROCESSES                 =
        "com.slb.sema.ppas.batch.batchmaster.numberOfDisconnectSubProcesses";

    /** Specifies the number of sub-processes used for the Promotion plan allocation synchronisation batch. */
    public static final String C_NUMBER_OF_PROMO_ALLOC_SYNCH_SUBPROCESSES           =
        "com.slb.sema.ppas.batch.batchmaster.numberOfPromoAllocSynchSubProcesses";

    /** Specifies the number of sub-processes used for the Promotion Allocation Provisioning batch. */
    public static final String C_NUMBER_OF_PROMO_ALLOC_PROVISIONING_SUBPROCESSES   =
        "com.slb.sema.ppas.batch.batchmaster.numberOfPromoAllocProvisioningSubProcesses";

    /** Specifies the number of sub-processes used for the Bulk Load of Account Finder batch. */
    public static final String C_NUMBER_OF_BULK_LOAD_OF_ACCOUNT_FINDER_SUBPROCESSES =
        "com.slb.sema.ppas.batch.batchmaster.numberOfBulkLoadOfAccountSubProcesses";

    /** Specifies the number of sub-processes used for the Enhanced Subscriber Lifecycle batch. */
    public static final String C_NUMBER_OF_ENHANCED_SUBSCRIBER_LIFECYCLE_SUBPROCESSES =
        "com.slb.sema.ppas.batch.batchmaster.numberOfEnhancedSubscriberLifecycleProcesses";
    
    /** Specifies the number of sub-processes used for the Enhanced Subscriber Lifecycle batch. */
    public static final String C_NUMBER_OF_AUTO_MSISDN_ROUTING_DEL_SUBPROCESSES =
        "com.slb.sema.ppas.batch.batchmaster.numberOfAutoMsisdnRoutingDelProcesses";

    /** Specifies whether the disconnection date should be sent to the output. */
    public static final String C_OUTPUT_DISC_DATE = "com.slb.sema.ppas.batch.outputDiscDate";

    /** Specifies the number of sub-processes used for the Subscriber Segmentation batch. */
    public static final String C_NUMBER_OF_SUBSCRIBER_SEGMENTATION_SUBPROCESSES =
        "com.slb.sema.ppas.batch.batchmaster.numberOfSubscriberSegmentationProcesses";
    
    // KEY VALUES FOR THE MAP
    /** Specifies the keyname in the Map for the recovery flag. Value is {@value}. */
    public static final String C_KEY_RECOVERY_FLAG         = "recoveryFlag";

    /** Specifies the keyname in the Map for the batch job type. Value is {@value}. */
    public static final String C_KEY_BATCH_JOB_TYPE        = "batchJobType";

    /** Specifies the keyname in the Map for the first MSISDN. Value is {@value}. */
    public static final String C_KEY_START_RANGE           = "startRange";

    /** Specifies the keyname in the Map for the last MSISDN. Value is {@value}. */
    public static final String C_KEY_END_RANGE             = "endRange";

    /** Specifies the keyname in the Map for the sub job id. Value is {@value}. */
    public static final String C_KEY_SUB_JOB_ID            = "subJobId";

    /** Specifies the keyname in the Map for the execution time and date. Value is {@value}. */
    public static final String C_KEY_EXECUTION_DATE_TIME   = "executionDateTime";

    /** Specifies the keyname in the Map for input file name. Value is {@value}. */
    public static final String C_KEY_MASTER_JS_JOB_ID      = "jsMasterJobId";

    /** Specifies the keyname in the Map for input file name. Value is {@value}. */
    public static final String C_KEY_INPUT_FILENAME        = "inFileName";

    /** Specifies the keyname in the Map for input file name. Value is {@value}. */
    public static final String C_KEY_INPUT_DIRECTORY_NAME  = "inDirectoryName";

    /** Specifies the keyname in the Map for service area. Value is {@value}. */
    public static final String C_KEY_SERVICE_AREA          = "serviceArea";
    
    /** Specifies the keyname in the Map for service location. Value is {@value}. */
    public static final String C_KEY_SERVICE_LOCATION      = "serviceLocation";
    
    /** Specifies the keyname in the Map for service class. Value is {@value}. */
    public static final String C_KEY_SERVICE_CLASS         = "serviceClass";

    /** Specifies the keyname in the Map for disconnection date. Value is {@value}. */
    public static final String C_KEY_DISCONNECTION_DATE    = "disconnectionDate";

    /** Specifies the keyname in the Map for disconnection offset. Value is {@value}. */
    public static final String C_KEY_DISCONNECTION_OFFSET    = "disconnectionOffset";

    /** Specifies the keyname in the Map for synchronisation file date. Value is {@value}. */
    public static final String C_KEY_SYNCH_DATE            = "synchDate";

    /** Specifies the keyname in the Map for synchronisation seuqence number. Value is {@value}. */
    public static final String C_KEY_SYNCH_SEQ  = "synchSeq";
 
    /** Report file key for Open call.  Value is {@value}. */
    public static final String C_KEY_REPORT_FILE           = "report";

    /** Recovery file key for Open call.  Value is {@value}. */
    public static final String C_KEY_RECOVERY_FILE         = "recovery";

    /** Output file key for Open call (database driven batch).  Value is {@value}. */
    public static final String C_KEY_OUTPUT_FILE_DB_DRIVEN = "outputDB";

    /** Output file key for Open call (file driven batch).  Value is {@value}. */
    public static final String C_KEY_OUTPUT_FILE           = "output";

    // Parameter values that could be stored in map.

    /** Specifies the value "yes". Value is {@value}. */
    public static final String C_VALUE_YES = "yes";

    /** Specifies the value "no". Value is {@value}. */
    public static final String C_VALUE_NO = "no";

    // Vaules used by the batch master.
    /** Specifies the value indicating that the batch (sub-)process should be run in recovery mode. */
    public static final String C_VALUE_RECOVERY_MODE_ON                   = "yes";

    /** Specifies the value indicating that the batch (sub-)process should be run in normal mode,
     *  that is, not in recovery mode. */
    public static final String C_VALUE_RECOVERY_MODE_OFF                  = "no";

    /** Constant holding the batch job type for the subscriber installation batch. */
    public static final String C_JOB_TYPE_BATCH_INSTALLATION              = "BatchInstallation";

    /** Constant holding the batch job type for the subscriber installation status change batch. */
    public static final String C_JOB_TYPE_BATCH_STATUS_CHANGE             = "BatchStatusChange";

    /** Constant holding the batch job type for the subscriber installation status change batch. */
    public static final String C_JOB_TYPE_BATCH_SDP_BALANCING             = "BatchSdpBalancing";

    /** Constant holding the batch job type for the promotion plan change batch. */
    public static final String C_JOB_TYPE_BATCH_PROMOTION_PLAN_CHANGE     = "BatchPromotionPlanChange";

    /** Constant holding the batch job type for the service class change batch. */
    public static final String C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE      = "BatchServiceClassChange";

    /** Constant holding the batch job type for the disconnection master job. */
    public static final String C_JOB_TYPE_BATCH_DISCONNECTION             = "BatchDisconnectionMaster";

    /** Constant holding the batch job type for the promotion plan allocation synchronisation master job. */
    public static final String C_JOB_TYPE_BATCH_PROMO_ALLOC_SYNCH         = "BatchPromoAllocSynchMaster";

    /** Constant holding the batch job type for the promotion plan allocation synchronisation master job. */
    public static final String C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION = "BatchAutoMsisdnRoutDelMaster";

    /** Constant holding the batch job type for the Community Charging batch. */
    public static final String C_JOB_TYPE_BATCH_CCI                       = "BatchCCI";

    /** Constant holding the batch job type for the Miscellaneous Data Upload batch. */
    public static final String C_JOB_TYPE_BATCH_MISC                      = "BatchMiscDataUpload";

    /** Constant holding the batch job type for the Synchronisation of Recharge/Refill Data  batch. */
    public static final String C_JOB_TYPE_BATCH_SYNCH                     = "BatchSynchronisation";

    /** Constant holding the batch job type for the promotion allocation provisioning master job. */
    public static final String C_JOB_TYPE_PROMO_ALLOC_PROVISIONING        = "PromoAllocProvisioningMaster";

    /** Constant holding the batch job type for the bulk load of account finder master job. */
    public static final String C_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER     = "BulkLoadAFMaster";
    
    /** Constant holding the batch job type for the Promotion Plan Change batch. */
    public static final String C_JOB_TYPE_PROMO_PLAN_CHANGE               = "PromotionPlanChange";
    
    /** Constant holding the batch job type for the  batch. */
    public static final String C_JOB_TYPE_BATCH_NUMBER_PLAN_CHANGE        = "batchNPChange";
    
    /** Constant holding the batch job type for the  batch. */
    public static final String C_JOB_TYPE_NUMBER_PLAN_CUTOVER             = "BatchNPCutover";
    
    /** Constant holding the batch job type for the MSISDN routing provisioning batch. */
    public static final String C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING     = "BatchRoutingProvisioning";

    /** Constant holding the batch job type for the subscriber Segmentation batch. */
    public static final String C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION     = "batchSubSegmentationChange";
    
    /** Constant holding the batch job type for the MSISDN routing provisioning batch. */
    public static final String C_JOB_TYPE_MSISDN_ROUTING_DELETION         = "BatchRoutingDeletion";

    /** Constant holding the batch job type for the Automated MSISDN routing provisioning batch. */
    public static final String C_SUB_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION  = "batchAutoMsisdnRoutingDeletion";

    /** Constant holding the batch job type for the  batch. */
    public static final String C_JOB_TYPE_ENHANCED_SUBSCRIBER_LIFECYCLE   = "BatchEnhancedSubLifecycleMast";

    /** Constant holding the batch job type for the Vodafone Fraud Returns. */
    public static final String C_JOB_TYPE_BATCH_FRAUD_RETURNS             = "batchFraudReturns";
    
    /** Constant holding the batch job type for the  batch. */
    public static final String C_JOB_TYPE_BATCH_REFILL = "batchRefill";

    /** Constant holding the batch job type for the disconnection sub job. */
    public static final String C_SUB_JOB_TYPE_BATCH_DISCONNECTION         = "batchDisconnect";

    /** Constant holding the batch job type for the promotion plan allocation synchronisation sub job. */
    public static final String C_SUB_JOB_TYPE_BATCH_PROMO_ALLOC_SYNCH     = "batchPromoAllocSynch";

    /** Constant holding the batch job type for the promotion allocation provisioning batch. */
    public static final String C_SUB_JOB_TYPE_PROMO_ALLOC_PROVISIONING    = "batchPromoAllocProvisioning";

    /** Constant holding the batch job type for the  batch. */
    public static final String C_SUB_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER = "batchBulkLoadAF";

    /** Constant holding the batch job type for the  batch. */
    public static final String C_SUB_JOB_TYPE_ENHANCED_SUBSCRIBER_LIFECYCLE = "batchEnhancedSubLifecycle";
  
    // VALID FILE TYPES ( matching regular expressions next section )

    /** "in progress" file type. Value is {@value}. */
    public static final String C_EXTENSION_IN_PROGRESS_FILE = ".IPG";
        
    /** Indata file type. Value is {@value}. */
    public static final String C_EXTENSION_INDATA_FILE      = ".DAT";
    
    /** Additional input data file type for Business Operations Interface. Value is (@value). */
    public static final String C_EXTENSION_BOI_INDATA_FILE  = ".SCH";

    /** Recovery file type. Value is {@value}. */
    public static final String C_EXTENSION_RECOVERY_FILE    = ".RCV";
        
    /** Successfully prepared file type. Value is {@value}. */
    public static final String C_EXTENSION_PROCESSED_FILE   = ".DNE";
        
    /** Error file type. Value is {@value}. */
    public static final String C_EXTENSION_REPORT_FILE      = ".RPT";

    /** XML file type. Value is {@value}. */
    public static final String C_EXTENSION_XML_FILE         = ".XML";
    
    /** Temporary output file type. Value is {@value}. */
    public static final String C_EXTENSION_TMP_FILE         = ".TMP";


    // Recovery status
    /** Recovery file status ERR - then recovery is needed to re-process that record. Value is {@value}. */
    public static final String C_NOT_PROCESSED              = "ERR";
    
    /** Recovery file status OK - the record is not needed to be processed during recovery. 
     * Value is {@value}. */
    public static final String C_SUCCESSFULLY_PROCESSED     = "OK";
    
    /** In the recovery file is the information separated with this delimiter. Value is {@value}. */
    public static final String C_DELIMITER_RECOVERY_STATUS  = ",";
    
    /** Report file delimiter. Value is {@value}. */
    public static final String C_DELIMITER_REPORT_FIELDS               = "";
    /** Delimiter for file name fields. Value is {@value}. */
    public static final String C_DELIMITER_FILENAME_FIELDS             = "_";
    /** Delimiter for Number Plan Change Format 2. Value is {@value}. */
    public static final String C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2 = ",";

    // split() delimiters
    /** In the recovery file is the information separated with this delimiter. Balue is {@value}. */
    public static final String C_PATTERN_RECOVERY_DELIMITER            = "[,]";

    /** The indata filename separate parts are separated with this delimiter. Value is {@value}. */
    public static final String C_PATTERN_FILENAME_COMPONENTS_DELIMITER = "[_.]";
    
    /** The indata file delimiter between sequence number and file extension. Value is {@value} */
    public static final String C_PATTERN_FILENAME_EXTENSION_DELIMITER  = "[.]";

 
    // REGULAR EXPRESSIONS
    /** Expression for checking if a string is alphanumeric. Value is {@value}. */
    public static final String  C_PATTERN_ALPHA_NUMERIC  = "[a-zA-Z0-9]*";

    /** Expression for checking if a string is numeric. Value is {@value}. */
    public static final String  C_PATTERN_RIGHT_ADJUSTED_NUMERIC = "^[ ]*[0-9]*$";
    
    /** Expression for checking if a string is numeric. Value is {@value}. */
    public static final String  C_PATTERN_RIGHT_ADJUSTED_ZERO_FILLED_NUMERIC = "^[0]*[0-9]*$";

    /** Expression for checking if a string is numeric. Value is {@value}. */
    public static final String  C_PATTERN_NUMERIC        = "[0-9]*";

    /** Expression for checking if a string has the format 'yyyymmdd'.
     *  Note - it does not check that 31-february should be invalid. Value is {@value}. */
    public static final String  C_PATTERN_DATE           = 
        "[0-9]{4}(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))";
    
    /** Expression for checking if a string has the format 'hh'. */
    public static final String  C_PATTERN_HOUR           = "(([0][0-9])|([1][0-9])|([2][0-3]))";

    /** Expression for checking if a string has the format 'mm'. */
    public static final String  C_PATTERN_MINUTES        = "(([0][0-9])|([1-5][0-9]))";

    /** Expression for checking if a string has the format 'ss'. */
    public static final String  C_PATTERN_SECONDS        = "(([0][0-9])|([1-5][0-9]))";
 
    /** Expression for checking if a string has the format 'hhmmss'. */
    public static final String  C_PATTERN_TIME           = 
        C_PATTERN_HOUR + C_PATTERN_MINUTES + C_PATTERN_SECONDS;

    /** Expression for checking if a string has the value Yes or No. Value is {@value}. */
    public static final String  C_PATTERN_RECOVERY       = "yes||no";

    /** Expression for checking if a string has the value Yes or No. Value is {@value}. */
    public static final String  C_PATTERN_YES_OR_NO      = "yes||no";


    /** Expression for file in progress file extension. Value is {@value}. */
    public static final String C_PATTERN_IN_PROGRESS_FILE = "[.]IPG";
        
    /** Expression for indata file extension. Value is {@value}. */
    public static final String C_PATTERN_INDATA_FILE      = "[.]DAT";
    
    /** Expression for BOI indata file extension. Value is {@value}. */
    public static final String C_PATTERN_BOI_INDATA_FILE  = "[.]SCH"; 

    /** Expression for recovery file extension. Value is {@value}. */
    public static final String C_PATTERN_RECOVERY_FILE    = "[.]RCV";
        
    /** Expression for processed file extension. Value is {@value}. */
    public static final String C_PATTERN_PROCESSED_FILE   = "[.]DNE";
    
    /** Expression for processed file extension. Value is {@value}. */
    public static final String C_PATTERN_ERROR_FILE   = "[.]ERR";
    
    /** Expression for the extension of an output file that is being written to. */
    public static final String C_PATTERN_TEMP_OUTFILE = "[.]TMP";



    // Subscriber Install
    /** Expression for checking filename pattern: INSTALL_xxyy_cccc_yyyymmdd_sssss.DAT. Value is {@value}. */
    // datePattern = [0-9]{4}(([0][1-9])|([1][1-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))
    public static final String C_PATTERN_SUBINST_LONG_FILENAME_DAT  = 
        "INSTALL[_]([0-9]{4}[_]){2}" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: INSTALL_xxyy_cccc_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_SUBINST_LONG_FILENAME_SCH  = 
        "INSTALL[_]([0-9]{4}[_]){2}" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: INSTALL_xxyy_cccc_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_SUBINST_LONG_FILENAME_IPG  = 
        "INSTALL[_]([0-9]{4}[_]){2}" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    /** Expression for checking filename pattern: INSTALL_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_SUBINST_SHORT_FILENAME_DAT = 
        "INSTALL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: INSTALL_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_SUBINST_SHORT_FILENAME_SCH = 
        "INSTALL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: INSTALL_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_SUBINST_SHORT_FILENAME_IPG = 
        "INSTALL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Indexes used in SubInst- controller, reader and writer.
    /** Number of components for a Subscriber Install batch that is using short file name. */
    public static final int C_NUMBER_OF_COMPONENTS_IN_SHORT_FILENAME = 4;
    /** Number of components for a Subscriber Install batch that's using long filename. */
    public static final int C_NUMBER_OF_COMPONENTS_IN_LONG_FILENAME  = 6;
    
    // Indexes used in controller, reader and writer.
    /** Forth element in a long date is the file date. Value is {@value} */
    public static final int C_INDEX_FILE_DATE_LONG                   = 3; 
    /** Second element in a short date is the file date. Value is {@value} */
    public static final int C_INDEX_FILE_DATE_SHORT                  = 1; 
    /** Forth element in a long date is the file seq no. Value is {@value} */
    public static final int C_INDEX_SEQ_NUMBER_LONG                  = 4; 
    /** Second element in a short date is the file seq no. Value is {@value} */
    public static final int C_INDEX_SEQ_NUMBER_SHORT                 = 2; 
    // Array indexes for 'long' filenames
    /** Second element is a combination of service area and service location. Value is {@value} */
    public static final int C_INDEX_MARKET_IN_FILENAME_INDEX         = 1; 
    /** Third element is the Service Class. Value is {@value} */
    public static final int C_INDEX_SERVICE_CLASS_IN_FILENAME_INDEX  = 2;
    // Index constants to retrieve Service area and Service location from second element in 'long' filenames.
    // The third element is the concatination of the area and location. If needed padded with '0'
    /** First part in the market. Value is {@value} */
    public static final int C_INDEX_SERVICE_AREA_INDEX               = 0;
    /** Second part in the market. Value is {@value}. */
    public static final int C_INDEX_SERVICE_LOCATION_INDEX           = 1;



    // Batch Subscriber Status Change
    /** Expression for checking filename pattern: INSTALL_STATUS_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_DAT  = 
        "INSTALL_STATUS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: INSTALL_STATUS_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_SCH  = 
        "INSTALL_STATUS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: INSTALL_STATUS_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_STATUS_CHANGE_FILENAME_IPG  = 
        "INSTALL_STATUS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    /** Expression for verifing valid new status values. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_STATUS_CHANGE_VALID_STATUS  = "[NAP]";


    // Batch Service Class Change
    /** Expression for checking filename pattern: BATCH_CHG_SERV_CLASS_yyyymmdd_sssss.DAT.
     * Value is {@value}. */
    public static final String C_PATTERN_BATCH_CHG_SERV_FILENAME_DAT  = 
        "BATCH_CHG_SERV[_]CLASS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: BATCH_CHG_SERV_CLASS_yyyymmdd_sssss.SCH.
     * Value is {@value}. */
    public static final String C_PATTERN_BATCH_CHG_SERV_FILENAME_SCH  = 
        "BATCH_CHG_SERV[_]CLASS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: BATCH_CHG_SERV_CLASS_yyyymmdd_sssss.IPG.
     * Value is {@value}. */
    public static final String C_PATTERN_BATCH_CHG_SERV_FILENAME_IPG  = 
        "BATCH_CHG_SERV[_]CLASS[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";


    // Batch SDP Balancing
    /** Expression for checking filename pattern: CHANGE_SDP_ssssss_tt.DAT. Value is {@value}. */
    public static final String C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_DAT  = 
        "CHANGE_SDP[_][0-9]{5}[_][0-9]{2}[.]DAT";

    /** Expression for checking filename pattern: CHANGE_SDP_ssssss_tt.SCH. Value is {@value}. */
    public static final String C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_SCH  = 
        "CHANGE_SDP[_][0-9]{5}[_][0-9]{2}[.]SCH";

    /** Expression for checking filename pattern: CHANGE_SDP_ssssss_tt.IPG. Value is {@value}. */
    public static final String C_PATTERN_CHANGE_SDP_BALANCING_FILENAME_IPG  = 
        "CHANGE_SDP[_][0-9]{5}[_][0-9]{2}[.]IPG";


    // Batch Promotion Plan Change
    /** Expression for checking filename pattern: PALLOAD_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_DAT =
        "PALLOAD[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: PALLOAD_ssssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_IPG  =
        "PALLOAD[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    /** Expression for checking filename pattern: PALLOAD_ssssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_SCH  =
        "PALLOAD[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    // Miscellaneous Subscriber Data
    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_UPDATE_MISC_FILENAME_DAT  = 
        "UPDATE_MISC[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_UPDATE_MISC_FILENAME_SCH  = 
        "UPDATE_MISC[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_UPDATE_MISC_FILENAME_IPG  = 
        "UPDATE_MISC[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";


    // CCI Change / Community Change
    /** Expression for checking filename pattern: COMCHARGE_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_CCI_CHANGE_FILENAME_DAT  = 
        "COMCHARGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: COMCHARG_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_CCI_CHANGE_FILENAME_SCH  = 
        "COMCHARGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: COMCHARG_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_CCI_CHANGE_FILENAME_IPG  = 
        "COMCHARGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Synchronisation of Recharge/Refill Data
    /** Expression for checking filename pattern: AIRDATASYNC.XML or AirDataSync.xml. Value is {@value}. */
    public static final String C_PATTERN_SYNCH_SHORT_FILENAME  = "((AIRDATASYNC[.]XML)|(AirDataSync[.]xml))";

    /** Expression for checking filename pattern: AIRDATASYNC.SCH or AirDataSync.SCH. Value is {@value}. */
    public static final String C_PATTERN_SYNCH_SHORT_FILENAME_SCH  = "(AIRDATASYNC[.]SCH|AirDataSync[.]SCH)";

    /** Expression for checking filename pattern: AIRDATASYNC_yyyymmdd_sssss.XML. Value is {@value}. */
    public static final String C_PATTERN_SYNCH_LONG_FILENAME  = 
        "AIRDATASYNC[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]XML";

    /** Expression for checking filename pattern: AIRDATASYNC_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_SYNCH_LONG_FILENAME_SCH  = 
        "AIRDATASYNC[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    // Batch Number Plan Change
    /** Expression for checking filename pattern: NUMBER_CHANGE_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_DAT  = 
        "NUMBER_CHANGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: NUMBER_CHANGE_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_SCH  = 
        "NUMBER_CHANGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CHANGE_FILENAME_IPG  = 
        "NUMBER_CHANGE[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Batch Number Plan Cutover
    /** Expression for checking filename pattern: NUMBER_CUTOVER_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_DAT  = 
        "NUMBER_CUTOVER[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";

    /** Expression for checking filename pattern: NUMBER_CHANGE_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_SCH  = 
        "NUMBER_CUTOVER[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_IPG  = 
        "NUMBER_CUTOVER[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Batch Msisdn Routing Deletion
    /** Expression for checking filename pattern: MSISDN_DELETE_yyyymmddhhmmss.DAT. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_DAT  = 
        "MSISDN_DELETE[_]" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]DAT";

    /** Expression for checking filename pattern: MSISDN_DELETE_yyyymmddhhmmss.SCH. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_SCH  = 
        "MSISDN_DELETE[_]" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]SCH";

    /** Expression for checking filename pattern: MSISDN_DELETE_yyyymmddhhmmss.IPG. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_IPG  = 
        "MSISDN_DELETE[_]" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]IPG";

    // Batch MSISDN Routing Provisioning.
    /** Expression for checking filename pattern: MSISDN_LOAD_yyyymmdd_sssss.DAT. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_PROVISIONING_DAT  = 
        "MSISDN_LOAD_" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]DAT";

    /** Expression for checking filename pattern: NUMBER_CHANGE_yyyymmdd_sssss.SCH. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_PROVISIONING_SCH  = 
        "MSISDN_LOAD_" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]SCH";

    /** Expression for checking filename pattern: UPDATE_MISC_yyyymmdd_sssss.IPG. Value is {@value}. */
    public static final String C_PATTERN_MSISDN_ROUTING_PROVISIONING_IPG  = 
        "MSISDN_LOAD_" + C_PATTERN_DATE + C_PATTERN_TIME + "[.]IPG";

    /** Record type, detail record in report file. Value is {@value}. */
    public static final int    C_RECORD_TYPE_NUMBER_PLAN_CHANGE_DETAIL_RECORD  = 1;

    /** Record type, trailer record in report file. Value is {@value}. */
    public static final int    C_RECORD_TYPE_NUMBER_PLAN_CHANGE_TRAILER_RECORD = 100;

    // Batch Refill.
    /** Expression for checking filename pattern: BATCH_REFILL_yyyymmddhhmmss.DAT. Value is {@value}.    */
    public static final String C_PATTERN_BATCH_REFILL_FILENAME_DAT = 
        "BATCH[_]REFILL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";
    
    /** Expression for checking filename pattern: BATCH_REFILL_yyyymmddhhmmss.SCH. Value is {@value}.    */
    public static final String C_PATTERN_BATCH_REFILL_FILENAME_SCH = 
        "BATCH[_]REFILL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";
    
    /** Expression for checking filename pattern: BATCH_REFILL_yyyymmddhhmmss.IPG. Value is {@value}.    */
    public static final String C_PATTERN_BATCH_REFILL_FILENAME_IPG = 
        "BATCH[_]REFILL[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Batch Subscriber Segmentaion
    /** Expression for checking filename pattern: CHG_SUB_SEGMENTATION_yyyymmddhhmmss.DAT. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_DAT  = 
        "BATCH[_]CHG[_]SUB[_]SEG[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]DAT";
    
    /** Expression for checking filename pattern: CHG_SUB_SEGMENTATION_yyyymmddhhmmss.SCH. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_SCH  = 
        "BATCH[_]CHG[_]SUB[_]SEG[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]SCH";

    /** Expression for checking filename pattern: CHG_SUB_SEGMENTATION_yyyymmddhhmmss.IPG. Value is {@value}. */
    public static final String C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_IPG  = 
        "BATCH[_]CHG[_]SUB[_]SEG[_]" + C_PATTERN_DATE + "[_][0-9]{5}[.]IPG";

    // Default values
    /** Default value for the property timeout value.  Value is {@value}. */
    public static final long   C_DEFAULT_TIMEOUT = 10000L;
    

    // Debug
    /** Constant for Debug printout.  Value is {@value}. */
    public static final String C_CONSTRUCTING = "Constructing ";
    
    /** Constant for Debug printout.  Value is {@value}. */
    public static final String C_CONSTRUCTED  = "Constructed ";

    /** Constant for Debug printout.  Value is {@value}. */
    public static final String C_ENTERING     = "Entering ";

    /** Constant for Debug printout.  Value is {@value}. */
    public static final String C_LEAVING      = "Leaving ";


    // Batch statuses used in the control tables.
    /** Constant for batch status <code>Completed</code>.  Value is {@value}. */
    public static final String C_BATCH_STATUS_COMPLETED  = "C";

    /** Constant for batch status <code>In progress</code>.  Value is {@value}. */
    public static final String C_BATCH_STATUS_INPROGRESS = "I";

    /** Constant for batch status <code>Stopped</code>.  Value is {@value}. */
    public static final String C_BATCH_STATUS_STOPPED    = "S";

    /** Constant for batch status <code>Failed</code>.  Value is {@value}. */
    public static final String C_BATCH_STATUS_FAILED     = "F";


    /** Indicates an empty MSISDN, used for left or right justification. */
    public static final String C_EMPTY_MSISDN            = "               ";
    /** Indicates an MSISIDN will zero fill. */
    public static final String C_MSISDN_ZEROS            = "000000000000000";
    /** Length of MSISDN field. Value is {@value}. */
    public static final int    C_LENGTH_MSISDN           = 15;
    
    /** Constant for the zero padding of successfully processed records in the batch trailer record. */
    public static final String C_TRAILER_ZEROS           = "000000";
    
    /** Constant for the sucess message in a batch trailer record. */
    public static final String C_TRAILER_SUCCESS         = "SUCCESS";

    /** Constant for the failure message in a batch trailer record. */
    public static final String C_TRAILER_FAILED          = "FAILED";
    
    /** Constant for the sequence name in NASE_SEQ_NAME used to generate uniq sequence number for
     * Number Plan Change and Number Plan Cutover. */
    public static final String C_NP_SEQUENCE_NAME        = "NumberPl";
    

} // End of BatchConstants
