// //////////////////////////////////////////////////////////////////////////////
//
//FILE NAME         : NPCutBatchControllerUT.java
//DATE              : Aug 25, 2004
//AUTHOR            : Urban Wigstrom
//REFERENCE         : PRD_ASCS00_DEV_SS_
//
//COPYRIGHT         : WM-data 2005
//
//DESCRIPTION       : Unit test for NPCutBatchController.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE | NAME | DESCRIPTION | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name> | <brief description of change> | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** Unit test for NPCutBatchController.*/
public class NPCutBatchControllerUT extends BatchTestCaseTT
{
    //  =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "NPCutBatchControllerUT";

    /** The name of the Number plan cutover batch. */
    private static final String C_NP_CUTOVER_BATCH_NAME     = "BatchNPCutover";

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "NUMBER_CUTOVER_SUCCESSFUL.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_ERROR            = "NUMBER_CUTOVER_ERRORS.DAT";
    
    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static BasicAccountData  c_basic                = null;
    
    /** Standard constrictor.
     * @param p_name Name of test.
     */
    public NPCutBatchControllerUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(NPCutBatchControllerUT.class);
    }
    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testNPCutBatch_Successful =
        "testNPCutBatch_Successful";
    
    /**
     * Performs a fully functional test of a successful 'NumberPlan Cutover'.
     * A test file with a valid name containing 2 valid NumberPlan Custover record is
     * created and placed in the batch indata directory. The Number Plan Cutover
     * batch is started in order to process the test file and update the MSISDN table
     * table, CUST_COMMENTS table
     *
     * @ut.when        A test file containing 2 valid Service Class change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        A new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain a
     *                 success trailing record.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testNPCutBatch_Successful()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 1;
        final int      L_NUMBER_OF_ERRORNOUS_RECORDS  = 0;
        final String[] L_REPORT_ROWS = {getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS = {};

        beginOfTest(C_METHOD_testNPCutBatch_Successful);

        NPCutTestDataTT l_testDataTT =
            new NPCutTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                       UtilTestCaseTT.c_logger,
                                       CommonTestCaseTT.c_ppasContext,
                                       C_NP_CUTOVER_BATCH_NAME,
                                       c_batchInputDataFileDir,
                                       C_FILENAME_SUCCESSFUL,
                                       JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                       BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                       L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                       L_NUMBER_OF_ERRORNOUS_RECORDS);

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }
    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testNPCutBatch_Error =
        "testNPCutBatch_Error";
    /**
     * Performs a fully functional test of a failuer 'Number Plan Cutover'.
     * A test file with a valid name containing 6 invalid NumberPlan Cutover record is
     * created and placed in the batch indata directory. The NumberPlan Cutover
     * batch is started in order to process the test file and update the MSISDN table,
     * CUST_COMMENTS table
     *
     * @ut.when        A test file containing 6 invalid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be a recovery file, 6 rows indicating errors.
     *                 A report file will be created which will contain 6 error code rows and
     *                 a trailing record saying that no successful NP cutover have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testNPCutBatch_Error()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 0;
        final int      L_NUMBER_OF_ERRORNOUS_RECORDS  = 2;
        final String[] L_REPORT_ROWS = {"01",   //Invalid record field.
                                        "03",   //Valid record but error occurred during processing
                                        getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS = {"1,ERR",
                                          "2,ERR"};

        beginOfTest(C_METHOD_testNPCutBatch_Error);

        NPCutTestDataTT l_testDataTT =
            new NPCutTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                       UtilTestCaseTT.c_logger,
                                       CommonTestCaseTT.c_ppasContext,
                                       C_NP_CUTOVER_BATCH_NAME,
                                       c_batchInputDataFileDir,
                                       C_FILENAME_ERROR,
                                       JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                       BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                       L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                       L_NUMBER_OF_ERRORNOUS_RECORDS);
        
        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    } 
    
    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>NPCutBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>NPCutTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController(BatchTestDataTT p_batchTestDataTT)
    {
        NPCutTestDataTT l_testDataTT = null;
        
        l_testDataTT = (NPCutTestDataTT)p_batchTestDataTT;
        
        return createNPCutBatchController(l_testDataTT.getTestFilename());
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty(BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1");
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }  
    
    /**
     * Returns the required additional properties layers for the NumberPlan Cutover batch.
     * 
     * @return the required additional properties layers for the NumberPlan Cutover batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_nco";
    }
    
    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>NPCutBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>NPCutBatchController</code> class.
     */
    private NPCutBatchController createNPCutBatchController(String p_testFilename)
    {
        NPCutBatchController        l_NPCutBatchController = null;
        HashMap                     l_batchParams          = null;

        l_batchParams = new HashMap();
        l_batchParams.put(BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename);
        
        try
        {
            l_NPCutBatchController =
                new NPCutBatchController(CommonTestCaseTT.c_ppasContext,
                                        BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER,
                                        super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                        C_CLASS_NAME,
                                        l_batchParams);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to create a 'NPCutBatchController' instance!");
            super.failedTestException(l_Ex);
        }

        return l_NPCutBatchController;
    }
    
    //==========================================================================
    // Inner class(es).
    //==========================================================================
    private class NPCutTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "NUMBER_CUTOVER";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^.*(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";
        
        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>NPCutTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private NPCutTestDataTT(PpasRequest p_ppasRequest,
                                         Logger      p_logger,
                                         PpasContext p_ppasContext,
                                         String      p_batchName,
                                         File        p_batchInputFileDir,
                                         String      p_templateTestFilename,
                                         int         p_expectedJobExitStatus,
                                         char        p_expectedBatchJobControlStatus,
                                         int         p_expectedNoOfSuccessRecords,
                                         int         p_expectedNoOfFailedRecords)
        {
            super(p_ppasRequest,
                  p_logger,
                  p_ppasContext,
                  p_batchName,
                  p_batchInputFileDir,
                  p_expectedJobExitStatus,
                  p_expectedBatchJobControlStatus,
                  p_expectedNoOfSuccessRecords,
                  p_expectedNoOfFailedRecords);

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile(C_REGEXP_DATA_TAG_MSISDN);
            
        }

        //==========================================================================
        // Protected method(s).
        //==========================================================================
        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile(C_PREFIX_TEST_DATA_FILENAME,
                                 BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                 i_templateTestFilename,
                                 C_CLASS_NAME);
        }

        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataRow  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags(String p_dataRow)
        {
            final int    L_DATA_TAG_GROUP_NUMBER = 0;
            final String L_SPECIAL_MSISDN_TAG    = "<MSISDN2>";

            StringBuffer l_dataRowSB             = null;
            Matcher      l_matcher               = null;
            int          l_tagBeginIx            = 0;
            int          l_tagEndIx              = 0;        
            String       l_tmpRow                = p_dataRow;   
            
            say("**START replaceDataTags : ]"+p_dataRow+"[");
            if (p_dataRow != null)
            {                
                l_dataRowSB = new StringBuffer(l_tmpRow);
                l_matcher   = i_msisdnDataTagPattern.matcher(l_tmpRow);
                if (l_matcher.matches())
                {
                    l_tagBeginIx      = l_matcher.start( L_DATA_TAG_GROUP_NUMBER );
                    l_tagEndIx        = l_matcher.end( L_DATA_TAG_GROUP_NUMBER );
                    
                    say("l_tagBeginIx : "+ l_tagBeginIx +" l_tagEndIx : " +l_tagEndIx );                   
                    
                    String l_formattedMsisdn_old = getFormattedMsisdn(0); 
                    String l_formattedMsisdn_new = getFormattedMsisdn();
                    insertMsisdnRouting(l_formattedMsisdn_new);
                    updateMsisdnStatus("AV", l_formattedMsisdn_old );
                    completeNumberChange(c_basic.getBtCustId(), l_formattedMsisdn_new);
                    updateMsisdnStatus("AS", l_formattedMsisdn_new);
                    StringBuffer l_tempMsisdnSB_old = new StringBuffer(l_formattedMsisdn_old);
                    
                    if ( p_dataRow.indexOf( L_SPECIAL_MSISDN_TAG ) == 8 )
                    {
                        l_tempMsisdnSB_old.append(l_tempMsisdnSB_old);  
                    }
                    else
                    {
                        StringBuffer l_tempMsisdnSB_new = new StringBuffer(l_formattedMsisdn_new);                         
                        l_tempMsisdnSB_old.append(l_tempMsisdnSB_new);
                    }
                    
                    l_dataRowSB.replace( l_tagBeginIx, l_tagEndIx, l_tempMsisdnSB_old.toString() );
                    
                    say("finished  : "+l_dataRowSB.toString()+"[");
                }
            }

            return l_dataRowSB.toString();
        }
        
        
        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
            say("*** start verify database ***");
            PpasAdditionalInfoService l_additionalInfoServ = null;
            AdditionalInfoData        l_additionalInfoData = null;
            BasicAccountData          l_basicAccountData   = null;

            l_additionalInfoServ = new PpasAdditionalInfoService( super.i_ppasRequest, 
                                                                  super.i_logger, 
                                                                  super.i_ppasRequest.getContext() );
            for ( int i = 0; i < getNumberOfTestSubscribers(); i++ )
            {
                l_basicAccountData = (BasicAccountData)super.getSubscriberData(i);
                say("########### MTR - verifyAscsDatabase ############### i: " + i + " , l_basicAccountData=" + l_basicAccountData);
                super.i_ppasRequest.setBasicAccountData( l_basicAccountData );
                l_additionalInfoData = l_additionalInfoServ.getAdditionalInfo( super.i_ppasRequest, 30000L );
                
                sayTime("l_additionalInfoData = [" + l_additionalInfoData + "]");
            }
            
            say("*** end verify database ***");
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
            say("*** start verifyReportFile *** ");
            
            Vector l_reportData = readNewFile(c_batchReportDataFileDir, i_testFilename, "DAT", "RPT");
            
            say("***verifyReportFile*** expected no success= " + i_expectedNoOfSuccessRecords + "expected no failed= " + i_expectedNoOfFailedRecords);
            if ( i_expectedNoOfFailedRecords == 0 )
            {
                // SUCCESS expected!
                // All successful - only one row (the trailing record) in the result file
                assertEquals("***verifyReportFile*** Wrong number of rows in the report file.", 1, l_reportData.size());
                
                String l_tmpTrailer = i_expectedReportData[0];
                say("***verifyReportFile*** **MTR** first element: ]"+ (((String)l_reportData.firstElement())+"[ expected : ]"+(l_tmpTrailer))+"[") ;

                assertTrue("***verifyReportFile*** Trailer string not as expected : ", ((String)l_reportData.firstElement()).equals(l_tmpTrailer));
            }
            else
            {
                say("###verifyReportFile### report file has failed records..");
                
                int l_noGeneratedReportRows      = l_reportData.size();  // Rows with error codes and trailing record
                int l_noExpectedRowsInReportFile = i_expectedReportData.length;
                assertTrue("Report file has wrong number of rows expected:"+l_noExpectedRowsInReportFile+
                           " found:"+l_noGeneratedReportRows, l_noGeneratedReportRows==l_noExpectedRowsInReportFile);
                for ( int i = 0; i < l_noExpectedRowsInReportFile-1; i++ )
                {
                    String l_errorCode = ((String)l_reportData.get(i)).substring(0,2);
                    say("###verifyReportFile### i:"+i+"row in reportFile :"+l_errorCode+
                               " found :"+i_expectedReportData[i]);
                    assertTrue("Unexpected row in reportFile :"+l_errorCode+
                               " but was :"+i_expectedReportData[i], l_errorCode.equals(i_expectedReportData[i]));
                }
            }
            say("***verifyReportFile*** *** end verifyReportFile ***");
        }
        
        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */

        protected void verifyRecoveryFile() throws IOException
        {
            Vector l_recoveryData = null;
            say("*** start verifyRecoveryFile *** ");
            try
            {
                l_recoveryData = readNewFile(c_batchRecoveryDataFileDir, i_testFilename, "DAT", "RCV");
                
                say("expected no success= " + i_expectedNoOfSuccessRecords + "expected no failed= " + i_expectedNoOfFailedRecords);
                if ( i_expectedNoOfFailedRecords == 0 )
                {
                    // File should not exist
                    // SUCCESS expected!
                    // All successful - only one row in the result file
                    say("**MTR no recovery file should be found size:"+l_recoveryData.size());
                    assertEquals("No recovery record should be found.", 0, l_recoveryData.size());                  
                }
                else
                {
                    say("**MTR** failure records created");
                    for ( int i = 0; i < l_recoveryData.size(); i++)
                    {
                        String l_found    = (String)l_recoveryData.get(i);
                        String l_expected = i_expectedRecoveryData[i];
                        say("found :    "+l_found);
                        say("expected : "+l_expected);
                        
                        assertTrue("VerifyRecoveryFile failed, found:"+l_found+
                                   " expected:"+l_expected, l_found.equals(l_expected));
                    }                   
                }
            }
            catch (IOException i_exep)
            {
                say("IOEXCEPTION caught "+i_exep.getMessage());
            }
            finally
            {
                say("FINALLY - recovery file has been checked");
            }
            say("*** end verifyRecoverfyFile ***");
        }

        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
            say("*** start verifyRenaming *** ");

            File   l_file             = null;
            String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "DAT", 
                                                           "DNE" );         
            l_file = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
                say( "File DOES NOT EXIST : " + l_file);
                throw new IOException();                
            }
            say("*** end verifyRenaming ***");
        }
        

        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
            c_basic = installLocalTestSubscriber(null);
            addSubscriberData(c_basic);
            
        } 
                
        /**
         * @param p_oldCustId The new status.
         * @param p_newMsisdn If the date shoukd be in the future (true) or not (false). Can only be true in
         * combination with status 0 'RE'.
         */
        private void completeNumberChange(String p_oldCustId, String p_newMsisdn)
        {
            SqlString       l_sqlString = null;

            // Insert information about a new Number Plan Change
            l_sqlString = new SqlString(500, 6, "INSERT INTO cums_customer_msisdn " + "(CUMS_CUST_ID, "
                    + "CUMS_START_DATE_TIME, " + "CUMS_PRINCIPAL, " + "CUMS_GEN_YMDHMS, " + "CUMS_OPID, "
                    + "CUMS_MOBILE_NUMBER, " + "cums_end_date_time )" + "VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})");

            l_sqlString.setIntParam(0, p_oldCustId);
            l_sqlString.setDateTimeParam(1, DatePatch.getDateTimeNow());
            l_sqlString.setStringParam(2, ""); //principals
            l_sqlString.setDateTimeParam(3, DatePatch.getDateTimeNow());
            l_sqlString.setStringParam(4, "Batch");
            l_sqlString.setStringParam(5, p_newMsisdn.trim());
            l_sqlString.setDateTimeParam(6, null);

            int noRowsUpdated = sqlUpdate(l_sqlString);  
            say("noRowsUpdated: " + noRowsUpdated);
        }
        
        /**
         * Updates the tables CUMS_CUSTOMER_MSISDN when a subscription is cancelled.
         * @param p_newMsisdn The msisdn that is cancelled.
         * <ul>
         * <li><code>MSISDN_NOT_EXISTS</code>- No account was found for the given msisdn.
         * </ul>
         */
        private void insertMsisdnRouting(String p_newMsisdn)
        {
            SqlString       l_sqlString = null;           

            l_sqlString = new SqlString(100, 0, "INSERT INTO msis_msisdn (msis_status, msis_mobile_number, msis_sdp_id)"
                               + " values ({0}, {1}, {2})");

            l_sqlString.setStringParam(0, "AS");
            l_sqlString.setStringParam(1, p_newMsisdn.trim());
            l_sqlString.setStringParam(2, "00");
            sqlUpdate(l_sqlString);     
        }
         
        /**
         * @param p_status The new status.
         * @param p_msisdn old Msisdn.
         */
        private void updateMsisdnStatus(String p_status, String p_msisdn)
        {
            SqlString       l_sqlString = null;           

            l_sqlString = new SqlString(100, 3, "Update MSIS_MSISDN "
                    + "Set MSIS_STATUS = {0} " + "where MSIS_MOBILE_NUMBER = {1}");

            l_sqlString.setStringParam(0, p_status);
            l_sqlString.setStringParam(1, p_msisdn.trim());

            sqlUpdate(l_sqlString);
        }
        
        /**
         * Returns a formatted MSISDN for the subscriber pointed out by the given subscriber index.
         * The length of the MSISDN will be 15 characters, right adjusted and space filled.
         * @return a formatted MSISDN, length 15 characters, right adjusted and space filled.
         */
        private String getFormattedMsisdn()
        {  
            String l_formattedMsisdnStr = null;
            Msisdn l_msisdn = getNextMsisdn(new Market(DbServiceTT.C_DEFAULT_SRVA, DbServiceTT.C_DEFAULT_SLOC),
                                                  DbServiceTT.C_DEFAULT_SDP);
            
            l_formattedMsisdnStr = C_TEMPLATE_MSISDN + new MsisdnFormat("n", "0044").format(l_msisdn);
            
            l_formattedMsisdnStr = l_formattedMsisdnStr.substring((l_formattedMsisdnStr.length() -
                                           C_TEMPLATE_MSISDN.length()));

            return l_formattedMsisdnStr;
        }

    } // End of inner class 'NPCutTestDataTT'.

}