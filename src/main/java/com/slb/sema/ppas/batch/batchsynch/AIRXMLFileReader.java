////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       AIRXMLFileReader.java
//DATE            :       23-Jul-2004
//AUTHOR          :       Alan Barrington-Hughes
//REFERENCE       :       PRD_ASCS00_DEV_SS_83
//
//COPYRIGHT       :       WM-data 2007
//
//DESCRIPTION     :       XML File Reader     
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 13/03/06 | Lars L.    | Changed to meet the requirements| PpacLon#2005/8089
//          |            | to handle 10 dedicated accounts.|
//----------+------------+---------------------------------+--------------------
// 12/06/06 | Lars L.    | A new method is added:          | PpacLon#2336/9067
//          |            | getAdjustedAmount(...).         |
//----------+------------+---------------------------------+--------------------
// 12/06/06 | Lars L.    | Method: getPromotionConfig(...) | PpacLon#2336/9067
//          |            | The values given for the        |
//          |            | 'bonusAmount',                  |
//          |            | 'bonusAmountLimit' and          |
//          |            | 'progressionAmountLimit' tags   |
//          |            | has to be adjusted according to |
//          |            | the precision for the given     |
//          |            | currency.                       |
//----------+------------+---------------------------------+--------------------
// 31/07/06 | Chris      | Added validation of Start & End | PpacLon#2329/9464
//          | Harrison   | dates.                          | PRD_ASCS_GEN_CA_096
//----------+------------+---------------------------------+-------------------
//21/08/06  | Chris      | Review amendments.              | PpacLon#2329/9771
//          | Harrison   |                                 |
//----------+------------+---------------------------------+--------------------
// 20/03/07 | Lars L.    | Improved error handling.        | PpacLon#2076/7827
//          |            |                                 |
//----------+------------+---------------------------------+-------------------
// 29/11/07 | Paul Rosser| Changed percentage from Int to  | PpacLon#3491/12524
//          |            | float in line with latest spec  |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchsynch;

import java.util.HashMap;
import java.util.Vector;

import org.w3c.dom.Node;

import com.slb.sema.ppas.batch.exceptions.BatchInvalidParameterException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.dataclass.CmdfCustMastDefaultsData;
import com.slb.sema.ppas.common.businessconfig.dataclass.DedaDedicatedAccountsData;
import com.slb.sema.ppas.common.dataclass.DivisionSynchData;
import com.slb.sema.ppas.common.dataclass.PromotionSynchData;
import com.slb.sema.ppas.common.dataclass.RefillSynchData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.dataclass.SynchDataObject;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.util.file.XMLFileReader;
import com.slb.sema.ppas.util.logging.Logger;


/**
 * Class to read and parse an XML file based on the schema for AIR configuration.
 */
public class AIRXMLFileReader extends XMLFileReader
{
    /** Context. */
    private PpasContext i_ppasContext;
    
    /** Opid. */
    private String      i_opid;

    /** Business config cache used for validation. */
    private BusinessConfigCache i_cache = null;
    
    /** Hidden property to disable validation towards DEDA. */
    private boolean                    i_dedaValidate                = true;

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "AIRXMLFileReader";

    /**
     * Class constructor.
     * @param p_logger the logger to store SAX exceptions encoutered durng parsing.
     * @param p_ppasContext the context to use
     * @param p_opid the submitter opid
     * @param p_dedaValidate whther to validate towards DEDA or not.
     */
    public AIRXMLFileReader(Logger p_logger, PpasContext p_ppasContext, String p_opid, boolean p_dedaValidate)
    {
        super(p_logger);
        i_opid = p_opid;
        i_ppasContext = p_ppasContext;
        i_cache = (BusinessConfigCache) i_ppasContext.getAttribute("BusinessConfigCache");
        i_dedaValidate = p_dedaValidate;
    }
    
    /**
     * Loads promotion configuration.
     * @return the promotion configuration
     * @throws BatchInvalidParameterException when a business configuration item is invalid
     */
    public Vector getPromotionConfig( ) throws BatchInvalidParameterException
    {
        Node          l_node                = null;
        boolean       l_startTagFound       = false;
        String        l_nodeName            = null;
        PromotionSynchData l_promotionConfig     = null;
        Vector        l_promotionConfigList = null;
        int           l_nodeIndex           = 0;
        
        String        l_defaultPromoAllocPeriod = loadSyfgDefaultPromoAllocPeriod();
        
        
        // Find the start for promotion
        for (; l_nodeIndex < getNodeListSize(); l_nodeIndex++)   
        {
            // Get a node 
            l_node = getNodeList().item(l_nodeIndex);
            
            // Get element(node) name 
            if (l_node != null)
            {
                l_nodeName = l_node.getNodeName().trim();
            }
            else
            {
                break;
            }
            
            // Search for start of configuration
            if (l_nodeName.equals("promotionPlanData"))
            {
                l_startTagFound = true;
                break;
            }
        }
        
        l_promotionConfigList = new Vector();
        
        if (l_startTagFound)
        {
            // We now loop from the start of all promotion plan configuartion.
            for(; l_nodeIndex < getNodeListSize(); l_nodeIndex++)
            {
                //Get the next node-element 
                l_node = getNodeList().item(l_nodeIndex);     

                // Get element(node) name 
                if (l_node != null)
                {
                    l_nodeName = l_node.getNodeName().trim();
                }
                else
                {
                    break;
                }                
                // If nodeName is "refillProfileData" or "divisionTableData" 
                // there are no more promotion configuration
                // Ready for when /// update schema 
                // if (l_nodeName.equals("refillProfileData") ||
                if (l_nodeName.equals("paymentProfileData") || l_nodeName.equals("divisionTableData") )
                {
                    break;
                }
                // Start of configuration set.  Save old object and create new
                else if (l_nodeName.equals("promotionPlan"))
                {
                    if (l_promotionConfig != null)
                    {
                        l_promotionConfigList.add(l_promotionConfig);
                    }
                    l_promotionConfig = new PromotionSynchData(/*SynchDataObject.C_SOURCE_FILE*/);
                    l_promotionConfig.setDefAllocPeriod(l_defaultPromoAllocPeriod);
                }
                
                //Look for elements holding configuration for a promotionPlan
                else if (l_nodeName.equals("promotionPlanId"))
                {
                    l_promotionConfig.setPromoPlan(getTagValue(l_node));
                    l_promotionConfig.setPromotion(getTagValue(l_node));
                }
                else if (l_nodeName.equals("promotionPlanDescr"))
                {
                    l_promotionConfig.setDescription(getTagValue(l_node)); 
                }
                else if (l_nodeName.equals("startDate"))
                {
                    l_promotionConfig.setStartDate(getTagValue(l_node));
                }
                else if (l_nodeName.equals("endDate"))
                {
                    if (l_promotionConfig.getStartDate() != null)
                    {
                        validateStartEndDates(l_promotionConfig.getStartDate(),
                                              getTagValue(l_node),
                                              "promotion plan",
                                              l_promotionConfig.getPromotion(),
                                              "n/a");
                    }
                    l_promotionConfig.setEndDate(getTagValue(l_node));
                }
                else if (l_nodeName.equals("currency"))
                {
                    validateCurrency(getTagValue(l_node),
                                     "promotion plan",
                                     l_promotionConfig.getPromotion(),
                                     l_promotionConfig.getStartDate());
                    l_promotionConfig.setCurrency(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusAmount"))
                {
                    l_promotionConfig.setAirCreditAbs(getAdjustedAmount(getTagValue(l_node),
                                                                        l_promotionConfig.getCurrency()));
                }
                else if (l_nodeName.equals("bonusExtension"))
                {
                    l_promotionConfig.setAirCreditPct(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusAmountLimit"))
                {
                    l_promotionConfig.setQualifyingLimit(getAdjustedAmount(getTagValue(l_node),
                                                                           l_promotionConfig.getCurrency()));
                }
                else if (l_nodeName.equals("bonusRechargesLimit"))
                {
                    l_promotionConfig.setRefillCount(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusSupervisionExtension"))
                {
                    l_promotionConfig.setAirPeriodPct(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusSupervisionDays"))
                {
                    l_promotionConfig.setAirPeriodAbs(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusServiceFeeExtension"))
                {
                    l_promotionConfig.setServPeriodPct(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusServiceFeeDays"))
                {
                    l_promotionConfig.setServPeriodAbs(getTagValue(l_node));
                }
                else if (l_nodeName.equals("bonusAmountBase"))
                {
                    // Ignore. This configuration element is not stored in ASCS
                }
                else if (l_nodeName.equals("accAmountBase"))
                {
                    // Ignore. This configuration element is not stored in ASCS
                }
                else if (l_nodeName.equals("progressionPlanId"))
                {
                    l_promotionConfig.setNextTier(getTagValue(l_node));
                }
                else if (l_nodeName.equals("progressionAmountLimit"))
                {
                    l_promotionConfig.setProgLimit(getAdjustedAmount(getTagValue(l_node),
                                                                     l_promotionConfig.getCurrency()));
                }
                else if (l_nodeName.equals("progressionRechargesLimit"))
                {
                    l_promotionConfig.setProgCount(getTagValue(l_node));
                }
                else if (l_nodeName.equals("notificationCode"))
                {
                    // Ignore. This configuration element is not stored in ASCS
                }
                else if (l_nodeName.equals("dedicatedAccDivTab"))
                {
                    l_promotionConfig.setDivisionId(getTagValue(l_node));
                }
                else
                {
                    // Unexpected element.
                }
            } //End for(...
            
            // Add final configuration item
            if (l_promotionConfig != null)
            {
                l_promotionConfigList.add(l_promotionConfig);
            }
        } // End if (startTagFound...
        else
        {
            // No promotionPlanData exists !!!!
        }
        
        return l_promotionConfigList;
    }
    
    /**
     * Loads the refilll configuration. 
     * All promotion plan configuration will be between the element "refillProfileData"
     * and either "divisionTableData" or until no more elements exist.
     * @return the refill configuration
     * @throws BatchInvalidParameterException when a business configuration item is invalid
     */
    public Vector getRefillConfig() throws BatchInvalidParameterException
    {
        Node        l_node              = null;
        boolean     l_startTagFound     = false;
        String      l_nodeName          = null;
        RefillSynchData l_refillConfig     = null;
        Vector      l_refillConfigList = null;
        int         l_nodeIndex         = 0;
        
        l_refillConfigList = new Vector();

        // Find the start for promotion
        for(; l_nodeIndex < getNodeListSize(); l_nodeIndex++)   
        {
            // Get a node 
            l_node = getNodeList().item(l_nodeIndex);
            
            // Get element(node) name 
            if (l_node != null)
            {
                l_nodeName = l_node.getNodeName().trim();
            }
            else
            {
                break;
            }
            
            // Search for start of configuration
            // Ready for when /// update schema
            //if (l_nodeName.equals("refillProfileData"))
            if (l_nodeName.equals("paymentProfileData"))
            {
                l_startTagFound = true;
                break;
            }
        }
        
        if (l_startTagFound)
        {
            // We now loop from the start of all promotion plan configuartion.
            for(; l_nodeIndex < getNodeListSize(); l_nodeIndex++)
            {
                //Get the next node-element 
                l_node = getNodeList().item(l_nodeIndex);     
                
                // Get element(node) name 
                if (l_node != null)
                {
                    l_nodeName = l_node.getNodeName().trim();
                }
                else
                {
                    break;
                }
                
                // If nodeName is "divisionTableData" 
                // there are no more promotion configuration
                if ( l_nodeName.equals("divisionTableData") )
                {
                    break;
                }
                // Start of configuration set.  Save old object and create new
                // Ready for when /// update schema
                //if (l_nodeName.equals("refillProfile"))
                if (l_nodeName.equals("paymentProfile"))
                {
                    if (l_refillConfig != null)
                    {
                        l_refillConfigList.add(l_refillConfig);
                    }
                    l_refillConfig = new RefillSynchData(/*SynchDataObject.C_SOURCE_FILE*/);
                    l_refillConfig.setOpid((null == i_opid) ? " " : i_opid);
                }
                
                //Look for elements holding configuration for a promotionPlan
                // Ready for when /// update schema
                //if (l_nodeName.equals("refillProfileId"))
                if (l_nodeName.equals("paymentProfileId"))
                {
                    l_refillConfig.setPaymentProfile(getTagValue(l_node));
                }
                // Ready for when /// update schema
                //else if (l_nodeName.equals("refillProfileDescr"))
                else if (l_nodeName.equals("paymentProfileDescr"))
                {
                    l_refillConfig.setDescription(getTagValue(l_node)); 
                }
                else if (l_nodeName.equals("currency"))
                {
                    validateCurrency(getTagValue(l_node),
                                     "refill profile",
                                     l_refillConfig.getPaymentProfile(),
                                     "n/a");
                    l_refillConfig.setCurrency(getTagValue(l_node));
                }
                else
                {
                    // Unexpected element (or range that we don't care about)
                }
            } //End for(...

            // Add final configuration item
            if (l_refillConfig != null)
            {
                l_refillConfigList.add(l_refillConfig);
            }

        } // End if (startTagFound...
        else
        {
            // No refillPlanData exists !!!!
        }
        
        return l_refillConfigList;
    }
    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getDivisionConfig = "getDivisionConfig";
    /**
     * All division table configuration will be after element "DivisionTableData"
     * and carry on until the end of the list.
     * @throws BatchInvalidParameterException if service class is missing
     * @return the division configuration
     */
    public Vector getDivisionConfig() throws BatchInvalidParameterException
    {
        Node              l_node                = null;
        boolean           l_startTagFound       = false;
        String            l_nodeName            = null;
        DivisionSynchData l_divisionConfig      = null;
        Vector            l_divisionConfigList  = null;
        int               l_nodeIndex           = 0;
        boolean           l_firstServiceClass   = true;
        boolean           l_wildCardExists      = false;
        String            l_currentDedAccountId = null;
        String            l_dedAccEndDateStr    = null;
        PpasDate          l_dedAccEndDate       = null;
        
        // Find the start for promotion
        for (; l_nodeIndex < getNodeListSize(); l_nodeIndex++)   
        {
            
            // Get a node 
            l_node = getNodeList().item(l_nodeIndex);
            
            // Get element(node) name 
            if (l_node != null)
            {
                l_nodeName = l_node.getNodeName().trim();
            }
            else
            {
                break;
            }
            
            // Search for start of configuration
            if (l_nodeName.equals("divisionTableData"))
            {
                l_startTagFound = true;
                break;
            }
        }
        
        l_divisionConfigList = new Vector();

        if (l_startTagFound)
        {
            // We now loop from the start of all division table configuartion.
            // and examines all remaining elements in the node-list
            for(; l_nodeIndex < getNodeListSize(); l_nodeIndex++)
            {
        
                //Get the next node-element 
                l_node = getNodeList().item(l_nodeIndex);     
                
                // Get element(node) name 
                if (l_node != null)
                {
                    l_nodeName = l_node.getNodeName().trim();
                }
                else
                {
                    break;
                }
                
                // Start of configuration set.  Save old object and create new
                if (l_nodeName.equals("divisionTable"))
                {
                    if (l_divisionConfig != null)
                    {
                        l_divisionConfigList.add(l_divisionConfig);
                    }
                    
                    l_divisionConfig = new DivisionSynchData();
                    l_divisionConfig.setOpid((null == i_opid) ? " " : i_opid);
                    
                    // Indicate that this is the first serviceClass for this divisionTableId
                    l_firstServiceClass = true;
                }
                
                // This may also be the start of a new configuration set.
                // If a second service class is found, re-use divisionTableId and Descr.
                if (l_nodeName.equals("serviceClass") && l_firstServiceClass == false)
                {
                    // Save previous division table data if any....
                    if (l_divisionConfig != null)
                    {
                        l_divisionConfigList.add(l_divisionConfig);
                        // ... and create a new instance with same Id & Descr as previous configuration set.  
                        l_divisionConfig = (DivisionSynchData) l_divisionConfig.cloneKey();
                    }
                }
                if (l_nodeName.equals("serviceClass"))
                {
                    l_firstServiceClass = false;
                }
                
                //Look for elements holding configuration for a promotionPlan
                if (l_nodeName.equals("divisionTableId"))
                {
                    l_divisionConfig.setDivisionId(getTagValue(l_node));
                }
                else if (l_nodeName.equals("divisionTableDescr"))
                {
                    l_divisionConfig.setDescription(getTagValue(l_node)); 
                }
                else if (l_nodeName.equals("serviceClass"))
                {
                    if (null != getTagValue(l_node))
                    {
                        l_divisionConfig.setServiceClass(validateServiceClass(
                                                                           getTagValue(l_node),
                                                                           l_divisionConfig.getDivisionId()));
                    }
                    else
                    {
                        // Null-serviceClass indicates wildcard service class
                        l_divisionConfig.setServiceClass(null);
                        l_wildCardExists = true;
                    }
                     
                }
                else if (l_nodeName.equals("mainAccountPart"))
                {
                    l_divisionConfig.setMasterPercent(Math.round(Float.parseFloat(getTagValue(l_node))));
                }
                else if (l_nodeName.equals("dedicatedAccountId"))
                {
                    l_currentDedAccountId = getTagValue(l_node);
                    validateDedAccount(l_currentDedAccountId,
                                       l_divisionConfig.getServiceClass(),
                                       l_divisionConfig.getDivisionId());
                    l_divisionConfig.setDedAccId(l_currentDedAccountId);
                }
                else if (l_nodeName.equals("dedicatedAccountPart"))
                {
                    l_divisionConfig.setDedAccPercent(l_currentDedAccountId,
                                                      Math.round(Float.parseFloat(getTagValue(l_node))));
                }
                else if (l_nodeName.equals("relativeDateExtension"))
                {
                    int l_relDateExt = Integer.parseInt(getTagValue(l_node));
                    if (l_relDateExt >= 0)
                    {
                        l_divisionConfig.setDedAccPeriod(l_currentDedAccountId, l_relDateExt);
                    }
                    else
                    {
                        // ***ERROR: A negative relative date extension is found for the current
                        //           dedicated account that corresponds to the current division table data.
                        BatchInvalidParameterException l_bipEx =
                            new BatchInvalidParameterException(C_CLASS_NAME,
                                                               C_METHOD_getDivisionConfig,
                                                               20200,
                                                               this,
                                                               null,
                                                               0,
                                                               BatchKey.get().batchInvalidExpiryDate(
                                                                           "" + l_relDateExt,
                                                                           l_currentDedAccountId,
                                                                           l_divisionConfig.getDivisionId()));
                        HashMap l_dataMap = new HashMap();
                        l_dataMap.put("error_code",    "09");
                        l_dataMap.put("config_type",   "division details");
                        l_dataMap.put("identifier",    l_divisionConfig.getDivisionId());
                        l_dataMap.put("currency_code", "n/a");
                        l_dataMap.put("start_date",    "n/a");
                        l_bipEx.setAllData(l_dataMap);
                        throw l_bipEx;
                    }
                }
                else if (l_nodeName.equals("extensionType"))
                {
                    l_divisionConfig.setDedAccPeriodExtType(l_currentDedAccountId, getTagValue(l_node));
                }
                else if (l_nodeName.equals("absoluteDateExtension"))
                {
                    l_dedAccEndDateStr = getTagValue(l_node);
                    if (l_dedAccEndDateStr == null)
                    {
                        l_dedAccEndDateStr = SynchDataObject.C_SYNCH_NULL_END_DATE;
                    }
                    l_dedAccEndDate = new PpasDate(l_dedAccEndDateStr, PpasDate.C_DF_yyyy_MM_dd);

                    l_divisionConfig.setDedAccEndDate(l_currentDedAccountId, l_dedAccEndDate);
                }
                
            } //End for(...
            // Add the last configuration
            if (l_divisionConfig != null)
            {
                l_divisionConfigList.add(l_divisionConfig);
            }
        } // End if (startTagFound...
        else
        {
            // No refillPlanData exists !!!!
        }

        // Replace any wildcards for service class
        if (l_wildCardExists == true)
        {
            l_divisionConfigList = replaceSCWildCard(l_divisionConfigList);
        }
        
        return l_divisionConfigList;
    }
    
    /** Loads Service Classes from BusinessConfigCache into an array.
     * 
     * @return CmdfCustMastDefaultsData[] the array of Service Class definitions
     */
    private CmdfCustMastDefaultsData[] loadServiceClasses()
    {
        CmdfCustMastDefaultsData[] l_serviceClassArr = null;

        l_serviceClassArr = i_cache.getCmdfCustMastDefaultsCache().getAll().getAvailableArray();

        return l_serviceClassArr;
    }
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_loadSyfgDefaultPromoAllocPeriod = "loadSyfgDefaultPromoAllocPeriod";
    
    /** Returns the value of ("BUS_CONFIG","DEFAULT_PROMO_ALLOC_PERIOD")
     *  from the SYFG System Configuration table.
     * 
     * @return The value of ("BUS_CONFIG","DEFAULT_PROMO_ALLOC_PERIOD") as a String.
     * @throws BatchInvalidParameterException  if the requested property does not exist.
     */
    private String loadSyfgDefaultPromoAllocPeriod()
    throws BatchInvalidParameterException
    {
        String l_syfgString = null;
        
        try
        {
            l_syfgString = i_cache.getSyfgSystemConfigCache().getSyfgSystemConfigData().
                                get("BUS_CONFIG","DEFAULT_PROMO_ALLOC_PERIOD");
        }
        catch (PpasConfigException l_e)
        {
            throw new BatchInvalidParameterException(
                    C_CLASS_NAME,
                    C_METHOD_loadSyfgDefaultPromoAllocPeriod,
                    20200,
                    this,
                    null,
                    0,
                    BatchKey.get().batchInvalidSyfgParameter(
                                    "BUS_CONFIG","DEFAULT_PROMO_ALLOC_PERIOD"),
                    l_e);
        }

        return l_syfgString;
    }

    
    /** Method that will replace all service class wildcards (null) and filter out any duplicates.
     * @param p_list the list of objects with service class wildcards
     * @return Vector the object items with service class data
     * @throws BatchInvalidParameterException if the division ID is not configured with ASCS
     */
    private Vector replaceSCWildCard(Vector p_list)
        throws BatchInvalidParameterException
    {
        DivisionSynchData          l_tmp             = null;
        DivisionSynchData          l_tmp2            = null;
        int                        l_numberOfSC      = 0;
        Vector                     l_vect            = new Vector();
        Vector                     l_result          = new Vector();
        HashMap                    l_map             = new HashMap();
        CmdfCustMastDefaultsData[] l_serviceClassArr = null;
        String[]                   l_dedAccIdArr     = null;
        
        if (null != p_list)
        {
            l_serviceClassArr = loadServiceClasses();
            for (int i = 0; i < l_serviceClassArr.length; i++)
            {
                if (null == l_map.get(l_serviceClassArr[i].getCmdfCustClass().toString()))
                {
                    l_map.put(l_serviceClassArr[i].getCmdfCustClass().toString(), 
                              l_serviceClassArr[i].getCmdfCustClass().toString());   
                }
            }
            l_numberOfSC = l_map.size();
            
            for (int i = 0; i < p_list.size(); i++)
            {
                l_tmp = (DivisionSynchData)p_list.elementAt(i);
                if (null == l_tmp.getServiceClass())
                {
                    // Don't save this one with (the one with serviceClass=null)*.
                    for (int a = 0; a < l_numberOfSC; a++)
                    {
                        l_tmp2 = (DivisionSynchData) l_tmp.cloneMe();
                        l_tmp2.setServiceClass(l_serviceClassArr[a].getCmdfCustClass());
                        l_tmp2.setWildCard(true);
                        l_vect.add(l_tmp2);
                    }
                }
                else
                {
                    l_vect.add(l_tmp);
                }
            }
            
            // Finally we need to remove duplicates created due to wildcard.
            // Remove those origination from wild card.
            
            int l_k = l_vect.size();
            
            // Loop vector to find all elements created via wildcard service class..
            for (int i = 0; i < l_k; i++)
            {
                // If wildcard service class...
                if ( ((DivisionSynchData)l_vect.elementAt(i)).isWildCard())
                {
                    // Compare wiith all other configuaration to find matching tableId & ServiceClass
                    for (int l_i2 = 0; l_i2 < l_k; l_i2++)
                    {
                        if ( ((DivisionSynchData)l_vect.elementAt(i))
                                .compareKeyTo((DivisionSynchData)l_vect.elementAt(l_i2)))
                        {
                            // ...and mark the one with service class wild card as duplicate
                            // (unless we compare with our self)
                            if (i != l_i2)
                            {
                                ((DivisionSynchData)l_vect.elementAt(i)).setDuplicate(true);
                            }
                        }
                        
                    }
                }
            }
            
            // Loop the vector and remove all with duplicate.
            for (int i = 0; i < l_k; i++)
            {
                if (!((DivisionSynchData)l_vect.elementAt(i)).isDuplicate())
                {
                    // Validate so that every dedicated account id is defined for the service class
                    // in question according to configuration in DEDA.
                    // This has been checked earlier but not for wildcard service classes.
                    DivisionSynchData l_tmp3 = (DivisionSynchData)l_vect.elementAt(i);
                    ServiceClass l_currentServClass = l_tmp3.getServiceClass();      
                    l_dedAccIdArr = l_tmp3.getDedAccountIds();
                    for (int l_ix = 0; l_ix < l_dedAccIdArr.length; l_ix++)
                    {
                        validateDedAccount(l_dedAccIdArr[l_ix],
                                           l_currentServClass,
                                           l_tmp3.getDivisionId());
                    }
                    l_result.add(l_vect.elementAt(i));
                }
            }
        }
        
        return l_result;
    }

    /** String to hold method name for debugging purposes. */
    private static final String C_METHOD_validateServiceClass = "validateServiceClass";

    /** Validates the given service class exists within ASCS. 
     * 
     * @param p_sc the service class to check.
     * @param p_id the division table id.
     * @return int the service class represented as an integer 
     * @throws BatchInvalidParameterException if the service class is not numeric or defined in ASCS
     */
    private ServiceClass validateServiceClass(String p_sc,
                                              String p_id) throws BatchInvalidParameterException
    {
        CmdfCustMastDefaultsData[] l_allClasses = loadServiceClasses();
        ServiceClass               l_result     = null;
        boolean                    l_scFound    = false;
        int                        l_sc         = 0;
        
        try
        {
            l_sc = Integer.parseInt(p_sc);
        }
        catch (NumberFormatException l_nfEx)
        {
            // throw exception -- not numeric
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME, 
                                                   C_METHOD_validateServiceClass, 
                                                   10000, 
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchServiceClassNotRecognised(p_sc, p_id),
                                                   l_nfEx);
            
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "07");
            l_dataMap.put("config_type",   "division details");
            l_dataMap.put("identifier",    p_id);
            l_dataMap.put("currency_code", "n/a");
            l_dataMap.put("start_date",    "n/a");
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }
        
        for (int i = 0; i < l_allClasses.length; i++)
        {
            l_result = l_allClasses[i].getCmdfCustClass();
            if (l_result.getValue() == l_sc)
            {
                l_scFound = true;
                break;
            }
        }
        
        if (!l_scFound)
        {
            // throw exception -- not defined in ASCS
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME, 
                                                   C_METHOD_validateServiceClass, 
                                                   10100, 
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchServiceClassNotRecognised(p_sc, p_id));
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "07");
            l_dataMap.put("config_type",   "division details");
            l_dataMap.put("identifier",    p_id);
            l_dataMap.put("currency_code", "n/a");
            l_dataMap.put("start_date",    "n/a");
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }
        return l_result;
    }
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateCurrency = "validateCurrency";

    /**
     * Validates a given currency code against ASCS configuration.
     * 
     * @param p_currencyCode  the currency code to validate.
     * @param p_source        the configuration item the currency is associated with.
     * @param p_id            the id of the configuration item.
     * @param p_startDate     the start date of the configuration item or <code>null</code> if the
     *                        configuration item doesn't include any start date (refill profile).
     * @throws BatchInvalidParameterException if the currency is not configured
     */
    private void validateCurrency(String p_currencyCode,
                                  String p_source,
                                  String p_id,
                                  String p_startDate)
        throws BatchInvalidParameterException
    {
        if (null == i_ppasContext.getCurrency(p_currencyCode))
        {
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME,
                                                   C_METHOD_validateCurrency,
                                                   20010,
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchCurrencyNotRecognised(p_currencyCode,
                                                                                             p_source,
                                                                                             p_id));
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "03");
            l_dataMap.put("config_type",   p_source);
            l_dataMap.put("identifier",    p_id);
            l_dataMap.put("currency_code", p_currencyCode);
            l_dataMap.put("start_date",    p_startDate);
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }
    }

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateDedAccount = "validateDedAccount";

    /**
     * Validates a given division ID against ASCS configuration.
     * If Service Class passed is null no validation will take place.
     * 
     * @param p_dedAccountId     The dedicated account id.
     * @param p_serviceClass     The service class.
     * @param p_divisionTableId  The division table id.
     * @return String the validated division ID
     * @throws BatchInvalidParameterException if the division ID is not configured with ASCS
     */
    private String validateDedAccount(String       p_dedAccountId,
                                      ServiceClass p_serviceClass,
                                      String       p_divisionTableId) 
            throws BatchInvalidParameterException
    {
        DedaDedicatedAccountsData [] l_arr = null;
        boolean l_found = false;
        int l_id = 0;
  
        // If configured not to validate towards DEDA simply return passed value.
        if (i_dedaValidate == false)
            return p_dedAccountId;
        
        try
        {
            l_id = Integer.parseInt(p_dedAccountId);            
        }
        catch (NumberFormatException l_nfEx)
        {
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME,
                                                   C_METHOD_validateDedAccount,
                                                   20200,
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchInvalidDedicatedAccount(
                                                                               p_dedAccountId,
                                                                               "" + p_serviceClass.getValue(),
                                                                               p_divisionTableId),
                                                   l_nfEx);
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "08");
            l_dataMap.put("config_type",   "division details");
            l_dataMap.put("identifier",    p_divisionTableId);
            l_dataMap.put("currency_code", "n/a");
            l_dataMap.put("start_date",    "n/a");
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }

        if(p_serviceClass == null)
            return p_dedAccountId;
        
        l_arr = i_cache.getDedaDedicatedAccountsCache().getAll().getDedaArray();
        for (int i = 0; i < l_arr.length; i++)
        {
            // Check that we have the correct service class...
            if (l_arr[i].getDedaServiceClass().toString().equals(p_serviceClass.toString()))
            {
                //...and check that dedicated account id is defined for that service class.
                if (l_arr[i].getDedaAccountId() == l_id)
                {
                    l_found = true;
                    break;
                }
            }
        }
        
        if (!l_found)
        {
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME,
                                                   C_METHOD_validateDedAccount,
                                                   20202,
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchInvalidDedicatedAccount(
                                                                               p_dedAccountId,
                                                                               "" + p_serviceClass.getValue(),
                                                                               p_divisionTableId));
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "08");
            l_dataMap.put("config_type",   "division details");
            l_dataMap.put("identifier",    p_divisionTableId);
            l_dataMap.put("currency_code", "n/a");
            l_dataMap.put("start_date",    "n/a");
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }
        
        return p_dedAccountId;
    }


    /**
     * Returns the given amount adjusted to the precision of the given currency.
     * For instance, if the given amount is "5500" and the precision of the given currency is 2, then
     * the string "55.00" will be returned.
     * 
     * @param l_amountWithoutPrec  the amount as a <code>String</code> and without any precision.
     * @param l_currencyStr        the currency
     * 
     * @return  the given amount adjusted to the precision of the given currency.
     */
    private String getAdjustedAmount(String l_amountWithoutPrec, String l_currencyStr)
    {
        String       l_adjustedAmountStr = l_amountWithoutPrec;
        PpasCurrency l_currency          = null;
        float        l_adjustedAmount    = 0.0f;

        l_currency =
            i_cache.getCufmCurrencyFormatsCache().getCufmCurrencyFormats().getCurrency(l_currencyStr);
        if (l_currency != null)
        {
            l_adjustedAmount =
                Float.parseFloat(l_amountWithoutPrec)/(float)(Math.pow(10, l_currency.getPrecision()));
            l_adjustedAmountStr = Float.toString(l_adjustedAmount);
        }

        return l_adjustedAmountStr;
    }
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateStartEndDates = "validateStartEndDates";

    /**
     * Validates the end date and thows an exception if the end date is before the start date.
     * 
     * @param p_startDate     the start date of the configuration item or <code>null</code> if the
     *                        configuration item doesn't include any start date (refill profile).
     * @param p_endDate       the end date.
     * @param p_source        the configuration item the currency is associated with.
     * @param p_id            the id of the configuration item.
     * @param p_currencyCode  the currency code to validate.
     * @throws BatchInvalidParameterException if the end date is before the start date.
     */
    private void validateStartEndDates(String p_startDate,
                                       String p_endDate,
                                       String p_source,
                                       String p_id,
                                       String p_currencyCode)
        throws BatchInvalidParameterException
    {
        PpasDate l_startDate = new PpasDate(p_startDate, PpasDate.C_DF_yyyy_MM_dd);
        PpasDate l_endDate = new PpasDate(p_endDate, PpasDate.C_DF_yyyy_MM_dd);
        
        if (l_endDate.before(l_startDate))
        {
            BatchInvalidParameterException l_bipEx =
                new BatchInvalidParameterException(C_CLASS_NAME,
                                                   C_METHOD_validateStartEndDates,
                                                   20010,
                                                   this,
                                                   null,
                                                   0,
                                                   BatchKey.get().batchInvalidEndDate(p_endDate,
                                                                                      p_startDate,
                                                                                      p_id));
            HashMap l_dataMap = new HashMap();
            l_dataMap.put("error_code",    "02");
            l_dataMap.put("config_type",   p_source);
            l_dataMap.put("identifier",    p_id);
            l_dataMap.put("currency_code", p_currencyCode);
            l_dataMap.put("start_date",    p_startDate);
            l_bipEx.setAllData(l_dataMap);
            throw l_bipEx;
        }
    }
}
