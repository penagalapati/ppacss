////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       PromoAllocSynchBatchReaderUT.java
//DATE            :       Aug 27, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       Contains several UT tests for the PromoAllocSynchBatchReader class.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.PromoAllocSynchBatchRecordData;
import com.slb.sema.ppas.common.support.PpasDebug;


/**
 * Contains several UT tests for the class PromoAllocSynchBatchReader.
 * Using JUnit as the test engine.
 */
public class PromoAllocSynchBatchReaderUT extends BatchTestCaseTT
{
//    //------------------------------------------------------
//    //  Class level constant.
//    //------------------------------------------------------ 
//    /** Class name constant used in calls to middleware.  Value is {@value}. */
//    private static final String C_CLASS_NAME = "PromoAllocSynchBatchReader";


    //------------------------------------------------------
    //  Instance level constant.
    //------------------------------------------------------
    /** The <code>PromoAllocSynchBatchReader</code> object. */
    private static PromoAllocSynchBatchReader c_promoAllocSynchBatchReader = null;

    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Constructs a <code>PromoAllocSynchBatchReaderUT</code> with the given name.
     * 
     * @param p_name  the name 
     */
    public PromoAllocSynchBatchReaderUT(String p_name)
    {
        super(p_name);
    }


    //------------------------------------------------------------------------
    // Public test methods.
    //------------------------------------------------------------------------
    /**
     * Tests the <code>PromoAllocSynchBatchReader</code> Constructor.
     */
    public void testConstructor()
    {
        super.c_writeUtText = true;
        super.enableConsoleLog(PpasDebug.C_LVL_MODERATE, false);

        super.beginOfTest("testConstructor");

        HashMap l_params = new HashMap();
        l_params.put("startRange", "1");
        l_params.put("endRange", "10");
        PromoAllocSynchBatchReader l_promoAllocSynchBatchReader = null;
        l_promoAllocSynchBatchReader = new PromoAllocSynchBatchReader(super.c_ppasContext,
                                                                      super.c_logger,
                                                                      null,
                                                                      null,
                                                                      l_params,
                                                                      super.c_properties);
        super.assertNotNull("Failed to create an 'PromoAllocSynchBatchReader' instance.",
                            l_promoAllocSynchBatchReader);

        // Save the newly constructed 'PromoAllocSynchBatchReader' component.
        c_promoAllocSynchBatchReader = l_promoAllocSynchBatchReader;

        super.endOfTest();
    }


    //------------------------------------------------------------------------
    // Public test methods.
    //------------------------------------------------------------------------
    /**
     * Tests the <code>PromoAllocSynchBatchReader</code> 'getRecord()' method.
     */
    public void testGetRecord()
    {
        if (c_promoAllocSynchBatchReader != null)
        {
            super.c_writeUtText = true;
            super.enableConsoleLog(PpasDebug.C_LVL_MODERATE, false);

            super.beginOfTest("testGetRecord");
        
            BatchRecordData l_record = null;
            l_record = c_promoAllocSynchBatchReader.getRecord();
            if (l_record != null)
            {
                assertTrue("Invalid record type is returned.",
                           l_record instanceof PromoAllocSynchBatchRecordData);
            }
            super.endOfTest();
        }
    }


    //------------------------------------------------------------------------
    // Public static methods.
    //------------------------------------------------------------------------
    /** Defines test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(PromoAllocSynchBatchReaderUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }
} //End of class 'PromoAllocSynchBatchReaderUT'.
