////////////////////////////////////////////////////////////////////////////////
//      FILE NAME       :       SubInstBatchController.java 
//      DATE            :       14-May-2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 03/05/07 | Lars Lundberg | Extends the super class          | PpacLon#3033/11433
//          |               | FileDrivenBatchController instead|
//          |               | of  the BatchController class.   |
//          |               | Three new methods are added:     |
//          |               | getValidFilenameRegEx()          |
//          |               | getBatchJobType()                |
//          |               | setBatchControlDataAttribs(...)  |
//          |               | Three methods are removed:       |
//          |               | addControlInformation()          |
//          |               | isFileNameValid(...)             |
//          |               | splitMarketIntoServiceAreaAndLocation(...)
//----------+---------------+----------------------------------+--------------------
// 03/05/07 | Eric Dangoor  | Make changes to allow single-step| PpacLon#3072/11421
//          |               | installation                     |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.SubInstBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.SubInstBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.SubInstBatchWriter;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.businessconfig.dataclass.SyfgSystemConfigData;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasFeatureLicenceException;
import com.slb.sema.ppas.common.exceptions.PpasFeatureLicenceInvalidException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.is.isapi.PpasInstallService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/**
 * The entry point for "Batch Subscriber Installation".
 * This batch is both pausable and stoppable this is automatically indicated when 
 * the super class�s constructor is called. 
 * This is the only batch that uses the "retry" queue if processing fails.
 */
public class SubInstBatchController extends FileDrivenBatchController
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME          = "SubInstBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_ins";

    /** The regular expression defining a valid short version of the filename. Value is {@value}. */
    private static final String C_REG_EX_VALID_SHORT_FILENAME = "(?:INSTALL_" +
                                                                C_REG_EX_VALID_DEFAULT_FILENAME + ")";

    /** The regular expression defining a valid long version of the filename. Value is {@value}. */
    private static final String C_REG_EX_VALID_LONG_FILENAME = "(?:INSTALL_(\\d{2})(\\d{2})_(\\d{4})_" +
                                                               C_REG_EX_VALID_DEFAULT_FILENAME + ")";

    /** Property that specifies whether the installation is to be single-step or standard. Value is {@value}. */
    private static final String C_INSTALLATION_TYPE   = "com.slb.sema.ppas.batch.installationType";

    /**The Service area. */
    private String i_serviceArea        = null;

    /**The service location. */
    private String i_serviceLocation    = null;

    /**The Service class.*/
    private String i_serviceClass       = null;

    /** The routing method. */
    private String i_routingMethod      = null;

    /** The installation type. */
    private String i_installationType   = null;

    
    /**
     * Constructs a SubInstBatchController with the given parameters.
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchInstall).
     * @param p_jobId       The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     * @throws PpasFeatureLicenceException - If there was a problem accessing feature licensing data.
     */
    public SubInstBatchController(
        PpasContext p_context,
        String p_jobType,
        String p_jobId,
        String p_jobRunnerName,
        Map p_params)
        throws PpasConfigException, PpasFeatureLicenceException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }    

        BusinessConfigCache                 l_businessConfigCache   = null;
        PpasFeatureLicenceInvalidException  l_e = null;

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();

        i_installationType = i_properties.getProperty(C_INSTALLATION_TYPE);
        if ((i_installationType.equals(PpasInstallService.C_INSTALL_TYPE_SINGLE_STEP)) &&
            ( ! p_context.isValidFeatureLicence(FeatureLicence.C_FEATURE_CODE_SINGLE_STEP_INSTALL)))
        {
            // Single-step installation has been requested but
            // no valid licence is installed.
            l_e = new PpasFeatureLicenceInvalidException(C_CLASS_NAME,
                    C_CLASS_NAME,
                    10010,
                    this,
                    null,
                    0,
                    ServiceKey.get().invalidFeature(FeatureLicence.C_FEATURE_CODE_SINGLE_STEP_INSTALL));
            
            i_logger.logMessage(l_e);
            throw(l_e);
        }

        l_businessConfigCache = (BusinessConfigCache)p_context.getAttribute("BusinessConfigCache");
        i_routingMethod = l_businessConfigCache.getSyfgSystemConfigCache().getSyfgSystemConfigData().
                            get(SyfgSystemConfigData.C_BUS_SRV,
                                SyfgSystemConfigData.C_ROUTING_METHOD);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialization.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                82601,
                this,
                "Enter " + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                82602,
                this,
                "Leaving " + C_METHOD_init);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates a <code>SubInstBatchReader</code>.
     * This method instantiates the correct reader-class and passes on 
     * the required parameters and also a reference to itself.  
     * The later is used for the reader-component to report back 
     * the controller-component.  The method is called from the 
     * super-class inner class method doRun() when start is ordered.
     * @return The created SubInstBatchReader.
     */
    public BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                "Enter " + C_METHOD_createReader);
        }
        
        SubInstBatchReader l_reader = null;
        if (super.i_startComponents)
        {
            l_reader = new SubInstBatchReader(super.i_ppasContext,
                                              super.i_logger,
                                              this,
                                              super.i_inQueue,
                                              super.i_params,
                                              super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10110,
                this,
                "Leaving " + C_METHOD_createReader);
        }
        
        return l_reader;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /** 
     * Creates a <code>SubInstBatchWriter</code>.
     * This method instantiates the correct writer-class and passes
     * on the required parameters and also a reference to itself.  
     * The later is used for the writer-component to report back the
     * controller-component. The method is called from the super-class
     * inner class method doRun() when start is ordered.
     * @return The created SubInstBatchWriter.
     * @throws IOException If there was some error when the writer was created.
     */
    public BatchWriter createWriter() throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10200,
                this,
                "Enter " + C_METHOD_createWriter);
        }

        SubInstBatchWriter l_writer = null;
        if (super.i_startComponents)
        {
            l_writer = new SubInstBatchWriter(super.i_ppasContext,
                                              super.i_logger,
                                              this,
                                              super.i_params,
                                              super.i_outQueue,
                                              super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10210,
                this,
                "Leaving " + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /** 
     * Creates a <code>SubInstBatchProcessor</code>.
     * This method instantiates the correct processor-class and 
     * passes on the required parameters and also a reference to itself.
     * The later is used for the processor-component to report back the
     * controller-component.  The method is called from the super-class 
     * inner class method doRun() when start is ordered.  
     * Note that only one instance of SubInstBatchProcessor is created 
     * irrelevant of the number of internal threads that the processor-component 
     * will consist of.
     * @return The created SubInstBatchProcessor. 
     */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                "Enter " + C_METHOD_createProcessor);
        }

        SubInstBatchProcessor p_processor = null;
        if (super.i_startComponents)
        {
            p_processor = new SubInstBatchProcessor(super.i_ppasContext,
                                                    super.i_logger,
                                                    this,
                                                    super.i_inQueue,
                                                    super.i_outQueue,
                                                    super.i_params,
                                                    this.i_properties,
                                                    this.i_installationType,
                                                    this.i_routingMethod);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                "Leaving " + C_METHOD_createProcessor);
        }
        return p_processor;
    }


    /**
     * This method should return a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected String getValidFilenameRegEx()
    {
        return C_REG_EX_VALID_SHORT_FILENAME + "|" + C_REG_EX_VALID_LONG_FILENAME;
    }

    
    /**
     * Returns the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected String getBatchJobType()
    {
        return BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION;
    }

    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_setBatchControlDataAttribs = "setBatchControlDataAttribs";
    /**
     * Sets the batch control data (instance) attributes.
     * 
     * @param l_controlDataAttribArr  the batch control data attributes as a <code>String</code> array.
     */
    protected void setBatchControlDataAttribs(String[] p_controlDataAttribArr)
    {
        final int L_SERVICE_AREA_IX       = 3;
        final int L_SERVICE_LOCATION_IX   = 4;
        final int L_SERVICE_CLASS_IX      = 5;
        final int L_FILE_DATE_IX          = 6;
        final int L_FILE_SEQNO_IX         = 7;
        final int L_FILENAME_EXTENSION_IX = 8;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10000, this,
                            "Entering " + C_METHOD_setBatchControlDataAttribs +
                            "; p_controlDataAttribArr = " +
                            (p_controlDataAttribArr != null  ?  "" + p_controlDataAttribArr.length : "null"));

            for (int l_ix = 0; p_controlDataAttribArr != null && l_ix < p_controlDataAttribArr.length; l_ix++)
            {
                PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                C_CLASS_NAME, 10000, this,
                                "p_controlDataAttribArr[" + l_ix + "]: '" + 
                                p_controlDataAttribArr[l_ix] + "'");
            }
        }

        if (p_controlDataAttribArr != null)
        {
            if (p_controlDataAttribArr[0] != null)
            {
                // This must be the short filename.
                super.setBatchControlDataAttribs(p_controlDataAttribArr);
                i_serviceArea     = (String)i_params.get(BatchConstants.C_KEY_SERVICE_AREA);
                i_serviceLocation = (String)i_params.get(BatchConstants.C_KEY_SERVICE_LOCATION);
                i_serviceClass    = (String)i_params.get(BatchConstants.C_KEY_SERVICE_CLASS);
            }
            else
            {
                // This must be the long filename.
                if (p_controlDataAttribArr.length > 3)
                {
                    // Set the service area.
                    i_serviceArea = p_controlDataAttribArr[L_SERVICE_AREA_IX];
                }
                
                if (p_controlDataAttribArr.length > 4)
                {
                    // Set the service location.
                    i_serviceLocation = p_controlDataAttribArr[L_SERVICE_LOCATION_IX];
                }
                
                if (p_controlDataAttribArr.length > 5)
                {
                    // Set the service class.
                    i_serviceClass = p_controlDataAttribArr[L_SERVICE_CLASS_IX];
                }
                
                if (p_controlDataAttribArr.length > 6)
                {
                    // Set the file date. The default file date syntax is: yyyymmdd (example: 20070423).
                    i_fileDate = new PpasDate(p_controlDataAttribArr[L_FILE_DATE_IX], PpasDate.C_DF_yyyyMMdd);
                }

                if (p_controlDataAttribArr.length > 7)
                {
                    // Set the file sequence number.
                    i_fileSeqNo = Integer.parseInt(p_controlDataAttribArr[L_FILE_SEQNO_IX]);
                }

                if (p_controlDataAttribArr.length > 8)
                {
                    // Set the filename extension.
                    i_extension = p_controlDataAttribArr[L_FILENAME_EXTENSION_IX];
                }
            }
        }

        super.i_extraData1 = i_serviceClass;
        super.i_extraData2 = i_serviceArea;
        super.i_extraData3 = i_serviceLocation;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10000, this,
                            "Leaving " + C_METHOD_setBatchControlDataAttribs);
        }

    }

    /**
    * It updates the database with a new status when a batch have got the status finished (c), 
    * stopped (x) or error (f). The metohd is called from the super class. 
    * The <code>BatchJobData</code>object used in the method are first fetched
    * form <code>getKeyedControlRecord()</code> and then the status is inserted into it.
    * @param p_status The new status.
    */     
    protected void updateStatus(String p_status)
    {
        try
        {
            BatchJobControlData l_jobControlData = getKeyedControlRecord();
            l_jobControlData.setStatus(p_status.charAt(0));
            super.i_batchContService.updateJobDetails(null, l_jobControlData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME,
                    11020,
                    this,
                    "updateStatus - PpasServiceException caught : " + e.getMsgKey() + " " + e.getMessage());
            }
        }
    }

   
    /** 
     * This method will return a BatchJobData-object with the correct key-values set 
     * (JsJobId and executionDateTime). The caller can then populate the record with data needed 
     * for the operation in question. For example, the writer will get such record and
     * add number of successfully/faulty records processed and then update.
     * @return A <code>BatchJobData</code> object with excutionDataTime and jsJobId set.
     */    
    public BatchJobControlData getKeyedControlRecord()
    {

        BatchJobControlData l_jobControlData = new BatchJobControlData(super.i_executionDateTime, 
                                                                       Long.parseLong(super.getJobId()));
         
        return l_jobControlData;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }

}
