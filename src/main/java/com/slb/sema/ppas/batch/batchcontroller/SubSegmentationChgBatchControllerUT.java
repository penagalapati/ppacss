/////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SubSegmentationChgBatchControllerUT.java
//      DATE            :       03 June 2007
//      AUTHOR          :       Ian James
//      REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       This class tests the batch Subscriber Segmentation change controller.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;

/**
 * Class to unit test the Community Charging Change batch.
 */
public class SubSegmentationChgBatchControllerUT extends BatchTestCaseTT
{

    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Test file for normal mode. Value is {@value}. */
    private static final String C_INPUT_DATA_FILENAME   = "BATCH_CHG_SUB_SEG_20070703_00001.DAT";

    /** Reference to subInstBatch controller object. */
    private static SubSegmentationChgBatchController c_subSegChgBatchController;

    /** JS jobId. */
    private static String    c_jsJobId = null;

    /**Holding the file name. */
    private static Hashtable c_params   = null;
 
    /**
     * Class constructor.
     * @param p_name the name of this unit test class
     */
    public SubSegmentationChgBatchControllerUT(String p_name)
    {
        super( p_name );
    }
 
    /** This test tests testCreateProcessor. */
    public void testCreateProcessor()
    {
        beginOfTest("testCreateProcessor");
        
        if (c_subSegChgBatchController.createProcessor() == null)
        {
            assertFalse("No BatchProcessor was created", true);
        }
        endOfTest();
    }

    /** This test tests testCreateReader. */
    public void testCreateReader()
    {
        beginOfTest("testCreateReader");
        if (c_subSegChgBatchController.createReader() == null)
        {
            assertFalse("No BatchReader was created", true);
        }
        endOfTest();
    }

    /** This test tests testCreateWriter. */
    public void testCreateWriter()
    {
        beginOfTest("testCreateWriter");
        try
        {
            if (c_subSegChgBatchController.createWriter() == null)
            {
                assertFalse("No BatchWriter was created", true);
            }
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        endOfTest();
    }

    /**
     * Test addControllInformation().
     */
    public void testAddControlInformation()
    {
        beginOfTest("testAddControlInformation");

        String l_dateDat            = "03-Jul-2007";
        String l_seqNoDat           = "1";
        String l_actuallSeqNo       = null;
        String l_actuallDate        = null;
               
        JdbcResultSet l_resulSet = null;
        SqlString l_sqlString = null;
        
        try
        {
            c_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_DATA_FILENAME);
            c_subSegChgBatchController.addControlInformation();
            
            //Get the data in the db added of the method addControlInformation().
            l_sqlString = new SqlString(50,
                                        2,
                                        "Select * from BACO_BATCH_CONTROL where baco_js_job_id = {0} " +
                                        "and baco_file_date = {1}");

            l_sqlString.setStringParam(0, c_jsJobId);
            l_sqlString.setStringParam(1, l_dateDat);

            l_resulSet = sqlQuery(l_sqlString);

            //If there is a new row in the database the test is ok.
            if (l_resulSet.next(50001))
            {
                l_actuallSeqNo = l_resulSet.getString(50002, "baco_seq_no");
                l_actuallDate = l_resulSet.getDate(50003, "baco_file_date").toString();
                
                //Check that right seqno is set in db.
                assertEquals("Wrong sequence number in the database when running in normal mode.", 
                                   l_seqNoDat ,l_actuallSeqNo);
                
                //Check that right date is set in db.
                assertEquals("Wrong date in the database when running in normal mode.", 
                                   l_dateDat ,l_actuallDate);
            }
            else
            {
                assertTrue("No row was inserted in the db when running in normal mode." , false);
            }
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        catch (PpasException e)
        {
            failedTestException(e);
        }
        
        endOfTest();
    }

   /**
    * Test suite anabling the execution of multiple tests automatically.
    * @return Test.
    */
   public static Test suite()
   {
       return new TestSuite(SubSegmentationChgBatchControllerUT.class);
   }

   /**
    * Main method provided for convenience to get the JUnit test framework to
    * run all the tests in this class. 
    *
    * @param p_args not used.
    */
   public static void main(String[] p_args)
   {
       System.out.println("Parameters are: " + p_args);
       junit.textui.TestRunner.run(suite());
   }
   
   /** Sets up any batch specific requirements. */
   protected void setUp()
   {
       super.setUp();
       createContext();     
   }

   /** Perform standard activities at end of a test. */
   protected void tearDown()
   {
       super.tearDown();
   }
   
   /** The context used of this unit test. */
   private void createContext()
   {
       super.c_writeUtText = false;
       
       try
       {
           if(c_subSegChgBatchController == null)
           {
               c_jsJobId = super.getJsJobID();
               c_params = new Hashtable();
               c_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_DATA_FILENAME);

               c_subSegChgBatchController = new SubSegmentationChgBatchController(c_ppasContext,
                                                                                  BatchConstants.C_JOB_TYPE_BATCH_SUBSCRIBER_SEGMENTATION,
                                                                                  getJsJobID(),
                                                                                  "SubSegChgBatchUT",
                                                                                  c_params);
            }
       }
       catch (PpasException e)
       {
           failedTestException(e);
       }
   }
}
