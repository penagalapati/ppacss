////////////////////////////////////////////////////////////////////////////////
//      ASCS IPR ID     :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchException.java
//      DATE            :       08-Aug-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// dd/mm/yy |            |                                 |                 
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.exceptions;

import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasExceptionTypeConstants;
import com.slb.sema.ppas.common.support.PpasRequest;

/** Provides constants and methods for Batch responses.
 */
public abstract class BatchException extends PpasException
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------

    /** Standard class name constant to be used in calls to Debug methods. */
    private static final String C_CLASS_NAME = "BatchException";

    /** Unique identifier for this class of exception. */
    private static final String C_EXCEPTION_ID = PpasExceptionTypeConstants.C_EXCEPTION_ID_BATCH;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------

    /**
     * Construct a BatchException.
     *
     * @param p_callingClass   The name of the class invoking this constructor.
     * @param p_callingMethod  The name of the method from which this constructor was invoked.
     * @param p_stmtNo         Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject  The object from which this constructor was 
     *                         invoked ... null if the constructor was invoked from a static method.
     * @param p_request        The Request from which this object was created.
     * @param p_flags          Bitmask flag ... if bit 0 is set, then a stack
     *                         trace will be output when some exception objects are logged. 
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     */
    public BatchException(String      p_callingClass,
                        String      p_callingMethod,
                        int         p_stmtNo,
                        Object      p_callingObject,
                        PpasRequest p_request,
                        long        p_flags,
                        BatchKey.BatchExceptionKey p_exceptionKey)
    {
        this(p_callingClass,
             p_callingMethod,
             p_stmtNo,
             p_callingObject,
             p_request,
             p_flags,
             p_exceptionKey,
             (Exception)null);
             
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10000, this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    /**
     * Construct a BatchException with support for exception chaining. That is,
     * provide a parameter for the exception which gave rise to this exception.
     *
     * @param p_callingClass   The name of the class invoking this constructor.
     * @param p_callingMethod  The name of the method from which this constructor was invoked.
     * @param p_stmtNo         Statement number uniquely identifying where this constructor was invoked.
     * @param p_callingObject  The object from which this constructor was 
     *                         invoked ... null if the constructor was invoked from a static method.
     * @param p_request        The Request from which this object was created.
     * @param p_flags          Bitmask flag ... if bit 0 is set, then a stack
     *                         trace will be output when some exception objects are logged. 
     * @param p_exceptionKey   Key defining the look-up key and any associated parameters.
     * @param p_sourceException An earlier exception which gave rise to this  exception.
     */
    public BatchException(String      p_callingClass,
                        String      p_callingMethod,
                        int         p_stmtNo,
                        Object      p_callingObject,
                        PpasRequest p_request,
                        long        p_flags,
                        BatchKey.BatchExceptionKey p_exceptionKey,
                        Exception   p_sourceException)
    {
        super(p_callingClass,
              p_callingMethod,
              p_stmtNo,
              p_callingObject,
              p_request,
              p_flags,
              C_EXCEPTION_ID,
              BatchMsg.C_BATCH_MSG_BASENAME,
              p_exceptionKey,
              BatchMsg.C_BATCH_MSG_FMT_KEY,
              p_sourceException);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME, 10100, this,
                "Constructed " + C_CLASS_NAME);
        }
    }
}
