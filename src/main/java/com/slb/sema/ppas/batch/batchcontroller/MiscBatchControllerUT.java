////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchControllerUT.java 
//      DATE            :       02-July-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Unit test for MiscBatchController.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 11/01/07 | Lars L.       | End-to-end tests are introduced. | PpacLon#2859/10825
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars L.       | Modified to adapt the changes in | PpacLon#3033/11279
//          |               | the BatchTestCaseTT and the      |
//          |               | BatchTestDataTT classes.         |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** Unit tests the Miscellaneous DataUpload batch controller. */
public class MiscBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                   = "MiscBatchControllerUT";

    /** The name of the Misc Data Upload batch. */
    private static final String C_MISC_DATA_UPLOAD_BATCH_NAME  = "BatchMiscDataUpload";

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL_INSERT   = "UPDATE_MISC_SUCCESSFUL_INSERT.DAT";

//    /** The name of the test data template file to be used for the succesful insert test case. */
//    private static final String C_FILENAME_SUCCESSFUL_UPDATE   = "UPDATE_MISC_SUCCESSFUL_UPDATE.DAT";
//
//    /** Short file name with extention .DAT. */
//    private static final String C_FILENAME_TEMPLATE            = "UPDATE_MISC_TEMPLATE.DAT";
//
//    /** The 'Action type' test data tag to be replaced by a real action type. */
//    private static final String C_TEST_DATA_TAG_ACTION_TYPE    = "<ACTION_TYPE>";
//
//    /** The MSISDN sequence number template. */
//    private static final String C_TEMPLATE_MSISDN_TAG_SEQ_NO   = "nn";
//
//    /** The 'MSISDN' test data tag to be replaced by a real MSISDN number. */
//    private static final String C_TEST_DATA_TAG_MSISDN         = "<MSISDN_" + C_TEMPLATE_MSISDN_TAG_SEQ_NO + ">";
//
//    /** The 'Field number' test data tag to be replaced by a real field number. */
//    private static final String C_TEST_DATA_TAG_FIELD_NUMBER   = "<FIELD_NUMBER>";
//
//    /** The 'Field value' test data tag to be replaced by a real field value. */
//    private static final String C_TEST_DATA_TAG_FIELD_VALUE    = "<FIELD_VALUE>";
//
//    /** The MSISDN number template to be used for all UT test data files. */
//    private static final String C_TEMPLATE_MSISDN              = "               ";
//
//    /** The MSISDN sequence number template. */
//    private static final String C_TEMPLATE_MSISDN_SEQ_NO       = "00";

    
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>MiscBatchControllerUT</code> instance to be used for
     * unit tests of the <code>MiscBatchController</code> class.
     * 
     * @param p_name  the name of the current test.
     */
    public MiscBatchControllerUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testMiscDataUploadBatch_Successful_Insert =
        "testMiscDataUploadBatch_Successful_Insert";
    /**
     * Performs a fully functional test of a successful insert action.
     * A test file with a valid name containing one valid insert action record is
     * created and placed in the batch indata directory. The misc. data upload
     * batch is started in order to process the test file and a new record will
     * be inserted in the CUST_MISC table.
     *
     * @ut.when        A test file containing one valid insert action record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        A new record will be inserted in the CUST_MISC table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain a
     *                 success trailing record.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testMiscDataUploadBatch_Successful_Insert()
    {
        beginOfTest(C_METHOD_testMiscDataUploadBatch_Successful_Insert);

        MiscDataUploadTestDataTT l_testDataTT =
            new MiscDataUploadTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                         UtilTestCaseTT.c_logger,
                                         CommonTestCaseTT.c_ppasContext,
                                         C_MISC_DATA_UPLOAD_BATCH_NAME,
                                         c_batchInputDataFileDir,
                                         C_FILENAME_SUCCESSFUL_INSERT,
                                         JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                         BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                         1,
                                         0);

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testMiscDataUploadBatch_Successful_Update =
        "testMiscDataUploadBatch_Successful_Update";
    /**
     * Performs a fully functional test of a successful update action.
     * A test file with a valid name containing one valid insert action record
     * and one valid update record is created and placed in the batch indata directory.
     * The misc. data upload batch is started in order to process the test file
     * and a new record will be inserted and updated in the CUST_MISC table.
     *
     * @ut.when        A test file containing one valid insert action record
     *                 and one valid update record is created and placed in
     *                 the batch indata directory.
     *
     * @ut.then        A new record will be inserted and thereafter updated in
     *                 the CUST_MISC table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain a
     *                 success trailing record.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testMiscDataUploadBatch_Successful_Update()
    {
//        beginOfTest(C_METHOD_testMiscDataUploadBatch_Successful_Update);

//        MiscBatchTestDataTT l_miscBatchTestDataTT = null;

        // Create a 'MiscBatchTestDataTT' instance.
//        l_miscBatchTestDataTT = new MiscBatchTestDataTT(CommonTestCaseTT.c_ppasRequest,
//                                                        UtilTestCaseTT.c_logger,
//                                                        1,
//                                                        C_FILENAME_SUCCESSFUL_UPDATE,
//                                                        'C', //BatchTestCaseTT.C_BATCH_STATUS_COMPLETED,
//                                                        2,
//                                                        0);

        // Run an end-to-end batch test (i.e. the complete batch process).
//        runEndToEndTestCase(MiscBatchController.class,
//                            "MiscDataUpload",
//                            C_CLASS_NAME,
//                            l_miscBatchTestDataTT,
//                            1,
//                            JobStatus.C_JOB_EXIT_STATUS_SUCCESS);

//        endOfTest();
    }


    // =========================================================================
    // == Public class method(s).                                             ==
    // =========================================================================
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(MiscBatchControllerUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>MiscBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>MiscDataUploadTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController(BatchTestDataTT p_batchTestDataTT)
    {
        MiscDataUploadTestDataTT l_testDataTT = null;
        
        l_testDataTT = (MiscDataUploadTestDataTT)p_batchTestDataTT;
        
        return createMiscBatchController(l_testDataTT.getTestFilename());
    }


    /**
     * Returns the required additional properties layers for the misc data upload batch.
     * 
     * @return the required additional properties layers for the misc data upload batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_mdu";
    }


    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();

        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty(BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1");

//        createContext();
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>MiscBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>MiscBatchController</code> class.
     */
    private MiscBatchController createMiscBatchController(String p_testFilename)
    {
        MiscBatchController l_miscBatchController = null;
        HashMap             l_batchParams         = null;

        l_batchParams = new HashMap();
        l_batchParams.put(BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename);
        
        try
        {
            l_miscBatchController =
                new MiscBatchController(CommonTestCaseTT.c_ppasContext,
                                        BatchConstants.C_JOB_TYPE_BATCH_MISC,
                                        super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                        C_CLASS_NAME,
                                        l_batchParams);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to create a 'MiscBatchController' instance!");
            super.failedTestException(l_Ex);
        }

        return l_miscBatchController;
    }


    //==========================================================================
    // Inner class(es).
    //==========================================================================
    /**
     * The purpose of this <code>MiscDataUploadTestDataTT</code> inner class is to
     * hold and create necessary test data for each UT test.
     */
    private class MiscDataUploadTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "UPDATE_MISC";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^.*(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";


        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>MiscDataUploadTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest                    The <code>PpasRequest</code> object.
         * @param p_logger                         The <code>Logger</code> object.
         * @param p_ppasContext                    The <code>PpasContext</code> object.
         * @param p_batchName                      The name of the current batch.
         * @param p_batchInputFileDir              The input data file directory.
         * @param p_templateTestFilename           The name of the template test file.
         * @param p_expectedJobExitStatus          The expected batch job exit status.
         * @param p_expectedBatchJobControlStatus  The expected batch job control status.
         * @param p_expectedNoOfSuccessRecords     The expected number of successfully processed records.
         * @param p_expectedNoOfFailedRecords      The expected number of failed records.
         */
        private MiscDataUploadTestDataTT(PpasRequest p_ppasRequest,
                                         Logger      p_logger,
                                         PpasContext p_ppasContext,
                                         String      p_batchName,
                                         File        p_batchInputFileDir,
                                         String      p_templateTestFilename,
                                         int         p_expectedJobExitStatus,
                                         char        p_expectedBatchJobControlStatus,
                                         int         p_expectedNoOfSuccessRecords,
                                         int         p_expectedNoOfFailedRecords)
        {
            super(p_ppasRequest,
                  p_logger,
                  p_ppasContext,
                  p_batchName,
                  p_batchInputFileDir,
                  p_expectedJobExitStatus,
                  p_expectedBatchJobControlStatus,
                  p_expectedNoOfSuccessRecords,
                  p_expectedNoOfFailedRecords);

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile(C_REGEXP_DATA_TAG_MSISDN);
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
            // LALU: Assuming always install only 1 test subscriber.
            BasicAccountData l_subscriberData = installLocalTestSubscriber(null);
            super.addSubscriberData(l_subscriberData);
        }


        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile(C_PREFIX_TEST_DATA_FILENAME,
                                 BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                 i_templateTestFilename,
                                 C_CLASS_NAME);
        }


        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataStr  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags(String p_dataRow)
        {
            final int L_DATA_TAG_GROUP_NUMBER = 1;
            final int L_MSISDN_INDEX          = 0;

            StringBuffer l_dataRowSB       = null;
            Matcher      l_matcher         = null;
            String       l_formattedMsisdn = null;
            int          l_tagBeginIx      = 0;
            int          l_tagEndIx        = 0;

            if (p_dataRow != null)
            {
                l_dataRowSB = new StringBuffer(p_dataRow);
                l_matcher   = i_msisdnDataTagPattern.matcher(p_dataRow);
                if (l_matcher.matches())
                {
                    l_tagBeginIx      = l_matcher.start(L_DATA_TAG_GROUP_NUMBER);
                    l_tagEndIx        = l_matcher.end(L_DATA_TAG_GROUP_NUMBER);
                    l_formattedMsisdn = getFormattedMsisdn(L_MSISDN_INDEX);
                    l_dataRowSB.replace(l_tagBeginIx, l_tagEndIx, l_formattedMsisdn);
                }
            }

            return (l_dataRowSB != null  ?  l_dataRowSB.toString() : p_dataRow);
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
            PpasAdditionalInfoService l_additionalInfoServ = null;
            AdditionalInfoData        l_additionalInfoData = null;
            BasicAccountData          l_basicAccountData   = null;

            l_additionalInfoServ = new PpasAdditionalInfoService(super.i_ppasRequest, 
                                                                 super.i_logger, 
                                                                 super.i_ppasRequest.getContext());

            l_basicAccountData = (BasicAccountData)super.getSubscriberData(0);
            super.i_ppasRequest.setBasicAccountData(l_basicAccountData);
            l_additionalInfoData = l_additionalInfoServ.getAdditionalInfo(super.i_ppasRequest, 30000L);
            assertNotNull("***ERROR: Failed to validate the ASCS database, no additional info was returned.",
                          l_additionalInfoData);
            sayTime("l_additionalInfoData = [" + l_additionalInfoData + "]");

            sayTime("Number of updated fields = [" + l_additionalInfoData.getFieldCount() + "]");

//            assertEquals("***ERROR: The ASCS database has NOT been properly updated, wrong field value.",
//                         "Test case: Successful update.", l_additionalInfoData.getMiscFieldV().get(0));
        }


        /**
         * Verfies the contents of the report file.
         * 
         * @throws IOException  if it fails to open and read the report file.
         */
        protected void verifyReportFile()
        {
        	// TODO implement
        }

 
        /**
         * Verfies the contents of the recovery file.
         * 
         * @throws IOException  if it fails to open and read the recovery file.
         */
        protected void verifyRecoveryFile()
        {
        	// TODO implement
        }


        /**
         * Verfies that the input data test file has been properly renamed.
         */
        protected void verifyRenaming()
        {
        	// TODO implement
        }


        //==========================================================================
        // Private method(s).
        //==========================================================================
        /**
         * Returns the name of the current input data test file.
         * 
         * @return the name of the current input data test file.
         */
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
    } // End of inner class 'MiscDataUploadTestDataTT'.
} // End of class 'MiscBatchControllerUT'.
