// //////////////////////////////////////////////////////////////////////////////
//      ASCS : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME : NPCutBatchProcessorUT.java
//      DATE : Aug 26, 2004
//      AUTHOR : Urban Wigstrom
//      REFERENCE : PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT : WM-data 2005
//
//      DESCRIPTION : Unit test for NPCutBatchProcessor.
//                    To run a unit test of a processor the processes must
//                    be started and the test can only run on unix.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE | NAME | DESCRIPTION | REFERENCE
//---------+----------------+-----------------------------------+--------------------
//DD/MM/YY | <name> | <brief description of change> | <reference>
//---------+----------------+-----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchprocessing;

import java.util.ArrayList;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.NumberChangeData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasNumberChangeService;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.util.structures.SizedQueue;

/** Unit test for NPCutBatchProcessor. */
public class NPCutBatchProcessorUT extends BatchTestCaseTT
{
    /**
     * Error code used when the subscriber has no number plan in progress or the new subscriber identifier is
     * not the same as the one defined for the subscriber.
     */
    private static final String            C_ERROR_SUBSCRIBER_IN_PROGRESS = "03";

    /** Error code used if a the old msisdn not exist. */
    private static final String            C_MSISDN_DONT_EXIST            = "04";

    /** The BasicAccountData that is created for this test. */
    private static BasicAccountData        c_basic                        = null;

    /** The old msisdn. */
    private static Msisdn                  c_oldMsisdn                    = null;

    /** The old msisdn used to test when the old msisdn not exist. */
    private static Msisdn                  c_oldMsisdnDonExist            = null;

    /** The new msisdn. */
    private static Msisdn                  c_newMsisdn                    = null;

    /** The new msisdn to be used when the msisdn is not the same as the one that has been changed. */
    private static Msisdn                  c_newMsisdn2                   = null;

    /** Instance of NPCutBatchProcessor. */
    private NPCutBatchProcessor            i_cutProcess                   = null;

    /** Instance of PpasNumberChangeService. */
    private static PpasNumberChangeService c_numberChangeService          = null;

    /**
     * @param p_title The test's class name.
     */
    public NPCutBatchProcessorUT(String p_title)
    {
        super(p_title);

    }

    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
    }

    /**
     * Test that processRecord saves the right values in the data base and report right errors when the input
     * values is incorrect.
     */
    public void testProcessRecord()
    {
        String l_expectedKey = null;
        String l_actuallKey = null;
        NPCutBatchRecordData l_record = new NPCutBatchRecordData();
        NPCutBatchRecordData l_recordData = new NPCutBatchRecordData();

        createContext();

        try
        {
            //Test when the subscriber has no number plan change in progress.
            l_record.setOldMsisdn(c_oldMsisdn);
            l_record.setNewMsisdn(c_newMsisdn);
            l_recordData = (NPCutBatchRecordData)i_cutProcess.processRecord(l_record);

            l_expectedKey = createKey(c_oldMsisdn, c_newMsisdn, C_ERROR_SUBSCRIBER_IN_PROGRESS);

            l_actuallKey = createKey(l_recordData.getOldMsisdn(),
                                     l_recordData.getNewMsisdn(),
                                     C_ERROR_SUBSCRIBER_IN_PROGRESS);

            super.assertEquals("Error code " + C_ERROR_SUBSCRIBER_IN_PROGRESS + " wasn't reported!",
                               l_expectedKey,
                               l_actuallKey);

            //Test when the new subscriber identifier (new msisdn) is not the same as the one
            //defined for the subscriber.
            this.doNumberPlanChange();
            l_record.setNewMsisdn(c_newMsisdn2);
            l_recordData = (NPCutBatchRecordData)i_cutProcess.processRecord(l_record);

            l_expectedKey = createKey(c_oldMsisdn, c_newMsisdn2, C_ERROR_SUBSCRIBER_IN_PROGRESS);

            l_actuallKey = createKey(l_recordData.getOldMsisdn(),
                                     l_recordData.getNewMsisdn(),
                                     C_ERROR_SUBSCRIBER_IN_PROGRESS);

            super.assertEquals("Error code " + C_ERROR_SUBSCRIBER_IN_PROGRESS + " wasn't reported!",
                               l_expectedKey,
                               l_actuallKey);

            //Test when the old msisdn don't exist.
            l_record.setOldMsisdn(c_oldMsisdnDonExist);
            l_record.setNewMsisdn(c_newMsisdn);
            l_recordData = (NPCutBatchRecordData)i_cutProcess.processRecord(l_record);

            l_expectedKey = createKey(c_oldMsisdn, c_newMsisdn, C_MSISDN_DONT_EXIST);
            l_actuallKey = createKey(l_recordData.getOldMsisdn(),
                                     l_recordData.getNewMsisdn(),
                                     C_MSISDN_DONT_EXIST);

            super.assertEquals("Error code " + C_MSISDN_DONT_EXIST + " wasn't reported!",
                               l_expectedKey,
                               l_actuallKey);

        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }
    }

    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /**
     * Define test suite. This unit test uses a standard JUnit method to derive a list of test cases from the
     * class.
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(NPCutBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        TestRunner.run(suite());
    }

    /** Creates the context need for this test. */
    private void createContext()
    {
        Hashtable l_params = new Hashtable();
        SizedQueue l_inQueue = null;
        SizedQueue l_outQueue = null;

        i_cutProcess = new NPCutBatchProcessor(super.c_ppasContext,
                                               super.c_logger,
                                               null,
                                               l_inQueue,
                                               l_outQueue,
                                               l_params,
                                               super.c_properties);

        try
        {
            if (c_basic == null)
            {
                c_basic = c_dbService.installTestSubscriber(null);
            }

            c_oldMsisdn = c_basic.getMsisdn();
            Msisdn[] l_msisdns = c_dbService.getNextMsisdns(c_basic.getMarket(),
                                                            DbServiceTT.C_DEFAULT_SDP,
                                                            3);
            
            c_oldMsisdnDonExist = l_msisdns[0];
            c_newMsisdn         = l_msisdns[1];
            c_newMsisdn2        = l_msisdns[2];
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }
    }

    /**
     * Creates the key that is excpeted to be in the assert method.
     * @param p_oldMsisdn The old msisdn.
     * @param p_newMsisdn The new msisdn.
     * @param p_errorCode The error code.
     * @return Expected assert value.
     */
    private String createKey(Msisdn p_oldMsisdn, Msisdn p_newMsisdn, String p_errorCode)
    {
        StringBuffer l_tmpErrorLine = new StringBuffer(); // Helpvariable for creation of error line.
        MsisdnFormat l_msisdnFormat = null;
        String l_newMsisdn = null;
        String l_oldMsisdn = null;

        l_msisdnFormat = super.c_ppasContext.getMsisdnFormatInput();

        l_oldMsisdn = l_msisdnFormat.format(p_oldMsisdn);
        l_newMsisdn = l_msisdnFormat.format(p_newMsisdn);

        l_tmpErrorLine.append(p_errorCode);
        l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);

        StringBuffer l_tmp = new StringBuffer();

        l_tmp.append(l_oldMsisdn);
        l_tmp.append(BatchConstants.C_EMPTY_MSISDN);

        l_tmpErrorLine.append(l_tmp.substring(0, BatchConstants.C_LENGTH_MSISDN));

        l_tmp = new StringBuffer();
        l_tmp.append(l_newMsisdn);
        l_tmp.append(BatchConstants.C_EMPTY_MSISDN);

        l_tmpErrorLine.append(l_tmp.substring(0, BatchConstants.C_LENGTH_MSISDN));

        return l_tmpErrorLine.toString();
    }

    /** Do a number plan change. */

    private void doNumberPlanChange()
    {
        PpasRequest l_request = new PpasRequest(null, C_DEFAULT_OPID, c_basic.getCustId());
        try
        {
            c_numberChangeService = new PpasNumberChangeService(super.c_ppasRequest,
                                                                super.c_logger,
                                                                super.c_ppasContext);
            
            ArrayList l_list = new ArrayList();
            
            NumberChangeData l_change = new NumberChangeData(c_oldMsisdn, c_newMsisdn);
            l_list.add(l_change);
            
            c_numberChangeService.changeNumberPlan(l_request, 10000, l_list);
        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }
    }
}