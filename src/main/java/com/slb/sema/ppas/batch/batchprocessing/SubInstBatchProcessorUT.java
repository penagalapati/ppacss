////////////////////////////////////////////////////////////////////////////////
//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SubInstBatchProcessorUT.java
//    DATE            :       2004
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.is.isapi.PpasInstallService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit Test for the 'SubInstBatchProcessor' class.
 */
public class SubInstBatchProcessorUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** Class name. Value is {@value}. */
    private static final String C_CLASS_NAME = "SubInstBatchProcessorUT";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public SubInstBatchProcessorUT(String p_title)
    {
        super(p_title, "batch_ins");
    }


    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testConstructor = "testConstructor";
    /**
     * Test to construct a <code>SubInstBatchProcessor</code> instance.
     */
    public void testConstructor()
    {
        SubInstBatchProcessor l_subInstBatchProcessor = null;
        SizedQueue            l_inQueue               = null;
        SizedQueue            l_outQueue              = null;
        String                l_installationType      = null;
        String                l_routingMethod         = null;

        try
        {
            l_inQueue  = new SizedQueue("SubInstBatchProcessUT indata queue", 10, null);
            l_outQueue = new SizedQueue("SubInstBatchProcessUT output queue", 10, null);
        }
        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        // TODO: Need to test the various combinations of these 2 parameters
        l_installationType = PpasInstallService.C_INSTALL_TYPE_STANDARD;
        //l_installationType = PpasInstallService.C_INSTALL_TYPE_SINGLE_STEP;
        //l_installationType = "something else;
        //l_routingMethod = "SDP";
        l_routingMethod = "NBR";
        //l_routingMethod = "something else;
        
        l_subInstBatchProcessor = new SubInstBatchProcessor(super.c_ppasContext, 
                                                            super.c_logger,
                                                            null,
                                                            l_inQueue,
                                                            l_outQueue,
                                                            null,
                                                            super.c_properties,
                                                            l_installationType,
                                                            l_routingMethod);
        
        assertNotNull("Failed to create a SubInstBatchProcessor instance.", l_subInstBatchProcessor);

        System.out.println(C_CLASS_NAME + C_METHOD_testConstructor + "-- Completed.");
    }


//    /** Constant holding the name of this method. Value is (@value). */
//    private static final String C_METHOD_testStart = "testStart";
//    /**
//     * Test to construct a <code>SubInstBatchProcessor</code> instance.
//     */
//    public void testStart()
//    {
//        SubInstBatchProcessor  l_subInstBatchProcessor = null;
//        SizedQueue             l_inQueue               = null;
//        SizedQueue             l_outQueue              = null;
//        InstrumentSet          l_instrumentSet         = null;
//        InstrumentManager      l_inQueueInstrumentMgr  = new InstrumentManager();
//        InstrumentManager      l_outQueueInstrumentMgr = new InstrumentManager();
//        SubInstBatchRecordData l_record                = null;
//        CounterInstrument      l_queuedCounter         = null;
//        CounterInstrument      l_removedCounter        = null;
//        MeterInstrument        l_sizeMeter             = null;
//
//        super.enableConsoleLogAll();
//
//        try
//        {
//            l_inQueue  = new SizedQueue("SubInstBatchProcessUT indata queue", 10, l_inQueueInstrumentMgr);
//            l_outQueue = new SizedQueue("SubInstBatchProcessUT output queue", 10, l_outQueueInstrumentMgr);
//        }
//        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
//        {
//            p_sqInvParamExe = null;
//            super.fail("Failed to create either the indata queue or the output queue or both.");
//        }
//
//        l_subInstBatchProcessor = new SubInstBatchProcessor(super.c_ppasContext, 
//                                                            super.c_logger,
//                                                            null,
//                                                            l_inQueue,
//                                                            l_outQueue,
//                                                            null,
//                                                            super.c_properties);
//
//        assertNotNull("Failed to create a SubInstBatchProcessor instance.", l_subInstBatchProcessor);
//
//        l_subInstBatchProcessor.start();
//
//        l_record = new SubInstBatchRecordData();
//        try
//        {
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//            l_inQueue.addLast(l_record);
//        }
//        catch (SizedQueueClosedForWritingException p_sqCFWExe)
//        {
//            p_sqCFWExe = null;
//            super.fail("Failed to add a record to the indata queue.");
//        }
//
//        l_inQueue.closeForWriting();
////        l_subInstBatchProcessor.start();
//        try
//        {
//            System.out.println(C_CLASS_NAME + C_METHOD_testStart + "-- Wait for 10 secs...");
//            Thread.sleep(10000L);
//        }
//        catch (InterruptedException e)
//        {
//        }
//
//        l_outQueue.getInstrumentSet();
//        l_instrumentSet = l_outQueue.getInstrumentSet();
//        assertNotNull("Failed to get the InstrumentSet from the SizedQueue.", l_instrumentSet);
//
//        l_queuedCounter = (CounterInstrument)l_instrumentSet.getInstrument("Queued counter");
//        l_removedCounter = (CounterInstrument)l_instrumentSet.getInstrument("Removed counter");
//        l_sizeMeter = (MeterInstrument)l_instrumentSet.getInstrument("Queue size");
//
//        assertEquals("Wrong number of elements added to the outdata queue (SizedQueue)."    ,
//                     8, l_queuedCounter.getValue());
//        assertEquals("Wrong number of elements removed from the outdata queue (SizedQueue).",
//                     0, l_removedCounter.getValue());
//        assertEquals("Wrong size of the outdata queue (SizedQueue)."                        ,
//                     8, l_sizeMeter.getValue());
//
//        System.gc();
//        System.out.println(C_CLASS_NAME + C_METHOD_testStart + "-- Completed.");
//    }


    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /** Define test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(SubInstBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }

} // End of public class SubInstBatchProcessorUT
////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////}
