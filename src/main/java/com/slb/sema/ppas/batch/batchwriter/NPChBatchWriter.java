////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NPChBatchWriter
//      DATE            :       20-August-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE      | NAME      | DESCRIPTION                      | REFERENCE
//----------+-----------+----------------------------------+--------------------
//18/06/08  | M Erskine | Write to temporary files and     | PpacLon#3650/13137
//          |           | rename on completion of batch job|
//----------+-----------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.NPChBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.NPChBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasNamedSequenceService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class creates output files for the Number Plan Change batch.
 * If more than one SDP is involved, it creates an output file for each SDP.
 */
public class NPChBatchWriter extends BatchWriter
{

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                                        = "NPChBatchWriter";

    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - missing property for Report File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY          = 
        "Property for REPORT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY        = 
        "Property for RECOVERY_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY          = 
        "Property for OUTPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing value in Map for Input File Name.  Value is {@value}. */
    private static final String C_KEY_NOT_FOUND_INPUT_FILENAME                      = 
        "Missing value in the Map for INPUT_FILE_NAME";

    /** An instance of <code>PpasNamedSequenceService</code>. */
    private PpasNamedSequenceService i_namedSequenceService = null;

    /** Logger printout - cannot open report- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_RECOVERY_FILE = "Cannot open recovery-file";

    /** Name of report file starts with this prefix. Value is {@value}. */
    private static final String C_PREFIX_REPORT_FILENAME    = "NUMBER_CHANGE_REJECT_";

    /** Name of report file starts with this prefix. Value is {@value}. */
    private static final String C_PREFIX_OUTPUT_FILENAME    = "NPLAN_SDP";

    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int    C_INDEX_FILE_DATE           = 2; // 3rd element in the filename is the date

    /** Array index to fine the sequence number from the filename.  Value is {@value}. */
    private static final int    C_INDEX_SEQUENCE_NUMBER     = 3; // 4th element in the filename    


    /** Gets how many records to be written. Gets from <code>p_params</code>. */    
    private int       i_interval  = 0;

    /** Used to count how many successfull records that has been processed. */
    private int       i_success   = 0;

    /** Used to count how many  records that has been processed. */
    private int       i_processed = 0;    

    /** Report directory - read from properties. */
    private String    i_reportFileDirectory   = null;

    /** Recovery directory - read from properties. */
    private String    i_recoveryFileDirectory = null;

    /** Output directory - read from properties. */
    private String    i_outputFileDirectory   = null;

    /** Input filename - read from properties. */
    private String    i_inputFileName         = null;

    /** A hashMap used to hold the number of times a file has proccessed without errors for each sdp.*/
    private Hashtable i_sdpOutputMap          = new Hashtable();

    /** The date retrieved from the input filename. */
    private String    i_fileDate              = null;
    
    /** The sequence retrieved from the input filename. */
    private String    i_indataFileSequence    = null;

    /** The sequence retrieved from a sequence generator. */
    private String    i_fileSequence          = null;

    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean   i_firstRecord           = true;
    
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int       i_numberOfSuccessfullyProcessedRecords = 0;
    
    /** Help variable for recovery for counting the total number of failed records. */
    private int       i_numberOfFaultyRecords = 0;

    /** A hashMap used to hold the sequence number for each sdp. */
    private HashMap   i_seqMap = new HashMap();

    /** Full path and filename to output file. */
    private String    i_fullOutputFileName = null;

    
   /**
    * Constructs a SubInstBatchWriter object.
    * @param p_ppasContext A PPpsCOntext
    * @param p_logger      The logger used of the writer.
    * @param p_controller  The batch jobs batch controller. 
    * @param p_params      Holding parameters used of the writer. 
    * @param p_outQueue    The batch jobs out queue.
    * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
    * @throws IOException  When there is some error to write to file.
    */
    public NPChBatchWriter( PpasContext     p_ppasContext,
                            Logger          p_logger,
                            BatchController p_controller,
                            Map             p_params,
                            SizedQueue      p_outQueue,
                            PpasProperties  p_properties)
                            throws IOException
    {

        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);

        String[] l_inputFileArr       = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10600,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        if ( this.getProperties() )
        {
             
            // Get the date and sequence fields from the input filename
            l_inputFileArr         = i_inputFileName.split(BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER);
            i_fileDate             = l_inputFileArr[C_INDEX_FILE_DATE];
            i_indataFileSequence   = l_inputFileArr[C_INDEX_SEQUENCE_NUMBER];
            
            i_namedSequenceService = new PpasNamedSequenceService(null, p_logger, p_ppasContext);

            //Get the full path filename for the recovery-file 
            super.i_recoveryFileName = generateFileName( this.i_recoveryFileDirectory,
                                                         this.i_inputFileName,
                                                         BatchConstants.C_EXTENSION_RECOVERY_FILE);

            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10610,
                    this,
                    " recoveryFileFullPath= " + super.i_recoveryFileName +
                    " date= " + i_fileDate + " sequence= " + i_fileSequence );
            }       
            try
            {
                super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
                String l_fullReportFileName = getFullReportFileName(i_reportFileDirectory);
                super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_fullReportFileName));
            }
            catch (IOException e)
            {
                
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_RECOVERY_FILE,
                                     LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10620,
                        this,
                        C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10630,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

        return;

    } // End of constructor for NPChBatchWriter(.....)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";    
   /**
    * Writes information to the recovery file and to the report file if there is any errors in the file. 
    * It also poulates a <code>BatchJobData</code> object with information from <code>p_params</code>. 
    * @param p_record Holds the information about the error line number and the recovery line number. 
    * @throws IOException
    * @throws PpasServiceException
    */
    protected void writeRecord(BatchRecordData p_record) 
        throws IOException, PpasServiceException
    {

        NPChBatchRecordData l_record             = null;
        String              l_sdpId              = null;
        String              l_fullReportFileName = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10640,
                this,
                BatchConstants.C_ENTERING + C_METHOD_writeRecord );
        }
        
        l_record = (NPChBatchRecordData)p_record;
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10640,
                this,
                "** WRITERECORD **"+l_record.dumpRecord());
        }

        l_sdpId = l_record.getSdpId();

        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10360,
                this,
                "** WRITERECORD ** noSuccessfully processed="+ this.i_numberOfSuccessfullyProcessedRecords+
                " noFailured="+this.i_numberOfFaultyRecords);
        }

        // If no SDPId is retrieved - then assume the SdpId=""
        if ( l_sdpId == null )
        {
            l_sdpId = "";
        }
        
        // Create keys for file map
        final String L_KEY_OUTPUT_FILE = C_PREFIX_OUTPUT_FILENAME + l_sdpId;

        if (i_seqMap.isEmpty() || !i_seqMap.containsKey(l_sdpId))
        {
            try
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME,
                        10360,
                        this,
                        "** WRITERECORD ** start try for l_sdpId="+l_sdpId);
                }

                i_fileSequence = i_namedSequenceService.getNextSequence(null,
                                                                        BatchConstants.C_NP_SEQUENCE_NAME,
                                                                        super.i_timeout);

                i_seqMap.put(l_sdpId, i_fileSequence);
                
                // Output files are connected to one specific SDP
                i_fullOutputFileName    = this.getFullOutputFileName( l_sdpId,
                                                                      i_outputFileDirectory );

            }
            catch (PpasServiceException e1)
            {
                e1.printStackTrace();
            }
        }        
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                "** WRITERECORD ** after create keys for file map outputfile="+i_fullOutputFileName+" sdp="+l_sdpId+" seq="+i_fileSequence);
        }

        
        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if ( l_record.getFailureStatus())
        { 
            this.i_numberOfFaultyRecords--;
        }

        // Count every time a file is being processed.
        i_processed++;

        // Always create report file to collect report information for all SDPs
        if ( !super.fileExists(BatchConstants.C_KEY_REPORT_FILE) )
        {
            l_fullReportFileName    = this.getFullReportFileName( i_reportFileDirectory);
            super.openFile( BatchConstants.C_KEY_REPORT_FILE, new File(l_fullReportFileName));           
        }

        // Write to report file if so
        if ( l_record.getErrorLine() != null )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_LOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10650,
                    this,
                    "** WRITERECORD ** before write to report file: "+l_record.getErrorLine() );
            }

            super.writeToFile( BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine() );
            super.i_errors++;
        }
        
        else
        {
            // Successful record - count and store the counter for this sdp
            int l_sdpCounter = 0;
            
            // Check if output file info exist and if so write to output file.
            if (l_record.getOutputLine() != null)
            {
                if (!super.fileExists(L_KEY_OUTPUT_FILE))
                {
                    openFile(L_KEY_OUTPUT_FILE, new File(i_fullOutputFileName) );
                    
                    //Initialize the Report Map with 0 successful entries
                    i_sdpOutputMap.put(L_KEY_OUTPUT_FILE, new Integer(0));
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_LOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10650,
                        this,
                        "** WRITERECORD ** before write to output file: "+l_record.getOutputLine() );
                }

                super.writeToFile(L_KEY_OUTPUT_FILE, l_record.getOutputLine());            
            }

            // Count successed processd files for each sdp
            if ( i_sdpOutputMap.containsKey( L_KEY_OUTPUT_FILE) )
            {
                l_sdpCounter = ((Integer)i_sdpOutputMap.get(L_KEY_OUTPUT_FILE)).intValue();
                l_sdpCounter++;
            }
            i_sdpOutputMap.put(L_KEY_OUTPUT_FILE, new Integer(l_sdpCounter));
            
            i_success++;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10650,
                this,
                "** WRITERECORD ** before write to recovery file: "+l_record.getRecoveryLine() );
        }


        // Always write to recovery file. Opened in constructor.
        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine() );

        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10650,
                this,
                "** WRITERECORD ** "+BatchConstants.C_LEAVING + C_METHOD_writeRecord );
        }

        return;
    } // End of writeRecord(.)



    
    
   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";    

   /** 
    * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#updateStatus()
    */
    protected void updateStatus() throws PpasServiceException
    {
        BatchJobData l_jobData = null;
      
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10660,
                this,
                BatchConstants.C_CONSTRUCTING+C_METHOD_updateStatus);
        }
        
        super.flushFiles();
        
        l_jobData = ((NPChBatchController)super.i_controller).getKeyedControlRecord();
        l_jobData.setNoOfRejectedRec(Integer.toString(super.i_errors+i_numberOfFaultyRecords));
        l_jobData.setNoOfSuccessRec(Integer.toString(i_success+i_numberOfSuccessfullyProcessedRecords));
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10360,
                this,
                C_METHOD_updateStatus + " before updateControlInfo");
        }

        super.updateControlInfo(l_jobData);
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10670,
                this,
                BatchConstants.C_LEAVING + C_METHOD_updateStatus + " after updateControlInfo");
        }
        
        return;
    }
  


   /** Writes out the trailer record.
    * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS
    * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
    */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String       l_outputFileKey = null;
        String       l_reportTrailer = null;
        int          l_sdpCounter    = 0;
        StringBuffer l_tmp           = new StringBuffer();
        
        // Generate trailer records for each report file
        for ( Enumeration e = i_sdpOutputMap.keys(); e.hasMoreElements(); )
        {
            // Get file keys
            l_outputFileKey = (String)e.nextElement();
            
            // Get number of successed processed files.
            l_sdpCounter = ((Integer)i_sdpOutputMap.get(l_outputFileKey)).intValue();

            
            // Trailer for the update files to the SDPs - if file has been created
            if (super.fileExists(l_outputFileKey))
            {
                l_tmp.append(BatchConstants.C_RECORD_TYPE_NUMBER_PLAN_CHANGE_TRAILER_RECORD);
                l_tmp.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
                l_tmp.append(l_sdpCounter);
                super.writeToFile(l_outputFileKey, l_tmp.toString());
                
                // Rename output file for SDP
                File l_file = ((BatchFile)i_filePointers.get(l_outputFileKey)).getFile();
                
                if (l_file != null && l_file.getPath().endsWith(BatchConstants.C_EXTENSION_TMP_FILE))
                {
                    l_file.renameTo(new File(l_file.getPath().replaceFirst(
                                                                   BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                                                   BatchConstants.C_EXTENSION_INDATA_FILE)));
                }
            }
        }
        
        // Trailer for the report file
        l_reportTrailer = BatchConstants.C_TRAILER_ZEROS + (i_success + i_numberOfSuccessfullyProcessedRecords);
        if ( PpasDebug.on)
        {
             PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_START,
                 C_CLASS_NAME,
                 10361,
                 this,
                 "TRAILER l_Count=]"+l_reportTrailer+"[");
        }

        l_reportTrailer = l_reportTrailer.substring( l_reportTrailer.length() - BatchConstants.C_TRAILER_ZEROS.length(),
                                                     l_reportTrailer.length());
        super.writeToFile( BatchConstants.C_KEY_REPORT_FILE, l_reportTrailer+p_outcome );
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    } // End of writeTrailerRecord(.)
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
   /**
    * This method tries to read the properies for:
    * Table update frequency and directories for the input-, report- and recovery-files
    * @return true if all properties were found
    */
    private boolean getProperties()
    {
        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        boolean l_returnValue = true;  // Assume all found

        // Get input filename from the Map
        this.i_inputFileName = (String)this.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME); 
        if ( this.i_inputFileName == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_KEY_NOT_FOUND_INPUT_FILENAME,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10670,
                    this,
                    C_KEY_NOT_FOUND_INPUT_FILENAME + " " + C_METHOD_getProperties );
            }
        }

        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10680,
                    this,
                    C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY + " " + C_METHOD_getProperties );
            }
        }

        // Get the Recovery File Directory
        i_recoveryFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        if ( i_recoveryFileDirectory == null )
        {
            l_returnValue = false; 
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10690,
                    this,
                    C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        // Get the report file directory
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        if ( i_reportFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10700,
                    this,
                    C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        // Get the report file directory
        i_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
        if ( i_outputFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10710,
                    this,
                    C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }

        return l_returnValue;

    } // End of method getProperties()

    
    
    
    
   /**
    * Creates a fullpath filename for the reportfile.
    * @param p_reportPath    Full directory path
    * @return Full path to the reportfile
    */
    private String getFullReportFileName( String p_reportPath )
    {
        StringBuffer l_tmp = new StringBuffer();
       
        // Filename syntax already validated in SubInstBatchReader there are short or long format.
        l_tmp.append(p_reportPath);
        l_tmp.append("/");
        l_tmp.append(C_PREFIX_REPORT_FILENAME);
        l_tmp.append(i_fileDate);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(i_indataFileSequence);
        l_tmp.append(BatchConstants.C_EXTENSION_TMP_FILE);

        return l_tmp.toString();
      
    } // End of getFullReportFileName(..)

 
    
    
  
    /**
     * Creates a fullpath filename for the output file.
     * @param p_sdpid         Current sdp.
     * @param p_outputPath    Full directoryPath.
     * @return Full path to the outputfile.
     */
    private String getFullOutputFileName( String p_sdpid,
                                          String p_outputPath )
    {
        StringBuffer l_tmp = new StringBuffer();
             
        // Filename syntax already validated in SubInstBatchReader there are short or long format.
        l_tmp.append(p_outputPath);
        l_tmp.append("/");
        l_tmp.append(C_PREFIX_OUTPUT_FILENAME);
        l_tmp.append(p_sdpid);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(i_fileDate);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(i_fileSequence);
        l_tmp.append(BatchConstants.C_EXTENSION_TMP_FILE);

        return l_tmp.toString();
      
    } // End of getFullReportFileName(..)

    

    
    
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_openFile = "openFile";

    /**
     * Creates a new file.
     * @param p_fileKey The key used to store the file in the hashmap <code>i_filePointers</code>.
     * @param p_fileName The filename of the file that will be created.
     * @throws IOException If the file cant be found.
     */
    protected void openFile(String p_fileKey, File p_fileName) throws IOException
    {
        BufferedWriter l_tmpFile = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START | PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_METHOD_openFile +
                                " with fileKey: " + p_fileKey + 
                                " and fileName: " + p_fileName);
        }

        //Create the file based on the passed absolute filename
        try
        {
            l_tmpFile = new BufferedWriter(new FileWriter(p_fileName, true));
            i_filePointers.put(p_fileKey, new BatchFile(p_fileName, l_tmpFile));
        }
        catch (IOException l_ie)
        {
            i_logger.logMessage(new LoggableEvent("BatchWriter:openFile: Exception "
                    + "couldn't create the file " + l_ie, LoggableInterface.C_SEVERITY_ERROR));
            if (l_tmpFile != null)
            {
                l_tmpFile = null;

            }
            throw l_ie;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10020,
                            this,
                            BatchConstants.C_CONSTRUCTED + C_METHOD_openFile);
        }
        
        return;
        
    } // End of openFile(..)

} // End of class NPChBatchWriter
