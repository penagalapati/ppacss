////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchProcessor.java
//      DATE            :       11-May-2004
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       Responsible for the actual Subscriber installation.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+------------+---------------------------------+--------------------
// 29/09/05 | L Byrne    | Allow installation with         | PpacLon#824/7115
//          |            | no promotion.                   |
//----------+------------+---------------------------------+--------------------
// 03/05/07 |Eric Dangoor| Make changes to allow single    | PpacLon#3072/11421
//          |            | step installation               |
//----------+------------+---------------------------------+--------------------
// 12/07/07 |Eric Dangoor| Modify SDP ID validation rules  | PpacLon#3221/11831
//----------+------------+---------------------------------+--------------------
// 13/07/05 | Ian James  | Add new reject reason code 23 if| PpacLon#3183/11755
//          |            | the service offerings supplied  |
//          |            | specifies opting into more than |
//          |            | one accumulated bonus scheme.   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasInstallService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class is responsible for the actual Subscriber installation.
 */
public class SubInstBatchProcessor extends BatchProcessor
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String C_CLASS_NAME          = "SubInstBatchProcessor";

    /** Routing methods */
    private static final String C_ROUTING_METHOD_SDP  = "SDP";
    
    /** Routing by Number Range. */
    private static final String C_ROUTING_METHOD_NBR  = "NBR";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_AVAILABLE       = "03";

    /** Constant holding the error code for invalid SDP ID. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_SDP_ID             = "14";

    /** Constant holding the error code for invalid agent. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_AGENT              = "15";

    /** Constant holding the error code for invalid promotion plan. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_PROMO_PLAN         = "16";

    /** Constant holding the error code for invalid master MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_MASTER_MSISDN      = "17";

    /** Constant holding the error code for invalid account group id. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_ACCOUNT_GROUP_ID   = "18";

    /** Constant holding the error code for invalid data. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_DATA               = "19";

    /** Constant holding the error code for invalid market. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_MARKET             = "20";

    /** Constant holding the error code for routing info not available. Value is {@value}. */
    private static final String C_ERROR_CODE_ROUTING_INFO_NOT_AVAILABLE = "21";

    /** Constant holding the error code for 'A parameter has been supplied for which no valid licence is 
     * installed'. Value is {@value}. */
    private static final String C_ERROR_CODE_NO_VALID_LICENSE           = "22";

    /** Constant holding the error code for a newly installed subscriber opted into more than
     * one Accumulated Bonus Schemes'. Value is {@value}. */
    private static final String C_ERROR_CODE_BONUS_SCHEME_NOT_ALLOWED   = "23";

    /** The error code for an unexpected PpasServiceException.  Value is {@value}. */
    private static final String C_ERROR_CODE_UNEXPECTED_EXCEPTION       = "99";

    /**
     * The mapping between the error codes and the corresponding 'PpasServiceException' message keys.
     * Note that several message keys can be mapped to the same error code. */
    private static final String[][] C_ERROR_CODE_MAPPINGS               =
    {
        // Mappings between 'PpasServiceException' message keys thrown by the IS API method and a
        // specific error code.
        {PpasServiceMsg.C_KEY_INSTALL_MSISDN_QUARANTINED,      C_ERROR_CODE_MSISDN_NOT_AVAILABLE},
        {PpasServiceMsg.C_KEY_MSISDN_ALREADY_INSTALLED,        C_ERROR_CODE_MSISDN_NOT_AVAILABLE},
        {PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE,            C_ERROR_CODE_MSISDN_NOT_AVAILABLE},

        {PpasServiceMsg.C_KEY_INVALID_VALUE_IN_ID,             C_ERROR_CODE_INVALID_SDP_ID},
        
        {PpasServiceMsg.C_KEY_INVALID_INSTALL_AGENT,           C_ERROR_CODE_INVALID_AGENT},

        {PpasServiceMsg.C_KEY_INVALID_VALUE_PROMO_PLAN,        C_ERROR_CODE_INVALID_PROMO_PLAN},

        {PpasServiceMsg.C_KEY_INSTALL_MASTER_IS_SUBORDINATE,   C_ERROR_CODE_INVALID_MASTER_MSISDN},
        {PpasServiceMsg.C_KEY_INVALID_INSTALL_MASTER_MSISDN,   C_ERROR_CODE_INVALID_MASTER_MSISDN},

        {PpasServiceMsg.C_KEY_INVALID_ACCOUNT_GROUP_ID,        C_ERROR_CODE_INVALID_ACCOUNT_GROUP_ID},

        {PpasServiceMsg.C_KEY_DEFAULT_MISSING,                 C_ERROR_CODE_INVALID_DATA},
        {PpasServiceMsg.C_KEY_CONFIG_MISSING,                  C_ERROR_CODE_INVALID_DATA},
        {PpasServiceMsg.C_KEY_SERVICE_CLASS_NOT_CONFIGURED,    C_ERROR_CODE_INVALID_DATA},

        {PpasServiceMsg.C_KEY_INVALID_INSTALL_MARKET,          C_ERROR_CODE_INVALID_MARKET},
        {PpasServiceMsg.C_KEY_INVALID_MARKET,                  C_ERROR_CODE_INVALID_MARKET},

        {PpasServiceMsg.C_KEY_ROUTING_NOT_FOUND,               C_ERROR_CODE_ROUTING_INFO_NOT_AVAILABLE},
            
        {PpasServiceMsg.C_KEY_INVALID_FEATURE,                 C_ERROR_CODE_NO_VALID_LICENSE},
        {PpasServiceMsg.C_KEY_INVALID_FEATURE_PARAM,           C_ERROR_CODE_NO_VALID_LICENSE},
        {PpasServiceMsg.C_KEY_BONUS_SCHEME_NOT_ALLOWED,        C_ERROR_CODE_BONUS_SCHEME_NOT_ALLOWED}

//
//            // In the javadoc for the IS API the following message keys are specified as possible exception
//            // reasons but for this batch they are not expected to occur, i.e. they will be handled as 
//            // unexpected exceptions.
//            // NOTE: This is done by the code, they are just listed here as a notification comment.
//
//            {PpasServiceMsg.C_KEY_CUST_ID_CANNOT_GENERATE,           C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_CUST_ID_TOO_LONG},                 C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_ERROR_PERSISTED_REQUEST,           C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_ERROR_SETTING_CUST_STATUS},        C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_INCOMPATIBLE_MSISDN_SDP_ID,        C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_INVALID_CUST_STATUS},              C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            (PpasServiceMsg.C_KEY_INVALID_CUST_STATUS_INPUT},        C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_INVALID_CUST_STATUS_TRANSITION},   C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_INVALID_REQUEST_FOR_SUB_INSTALL},  C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS},                C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_NO_DEFAULT_AND_NO_PROVIDED_SDP_ID, C_ERROR_CODE_UNEXPECTED_EXCEPTION},
//            {PpasServiceMsg.C_KEY_PARSE_MSISDN_ERROR},               C_ERROR_CODE_UNEXPECTED_EXCEPTION},
    };


    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasInstallService     i_ppasInstallService  = null;

    /** The <code>Session</code> object. */
    private PpasSession            i_ppasSession         = null;

    /** The routing method. */
    private String                 i_routingMethod       = null;

    /** The installation type. */
    private String                 i_installationType    = null;

    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Constructs an instance of this <code>SubInstBatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     *
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process paramters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     * @param p_installationType the installation type (standard or single-step)
     * @param p_routingMethod    the routing method
     */
    public SubInstBatchProcessor(PpasContext     p_ppasContext,
                                 Logger          p_logger,
                                 BatchController p_batchController,
                                 SizedQueue      p_inQueue,
                                 SizedQueue      p_outQueue,
                                 Map             p_params,
                                 PpasProperties  p_properties,
                                 String          p_installationType,
                                 String          p_routingMethod)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        PpasRequest             l_ppasRequest = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        i_ppasSession        = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest        = new PpasRequest(i_ppasSession);
        i_ppasInstallService = new PpasInstallService(l_ppasRequest, p_logger, p_ppasContext);
        i_installationType   = p_installationType;
        i_routingMethod      = p_routingMethod;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }


    //-------------------------------------------------------------------------
    // Protected methods.
    //-------------------------------------------------------------------------
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * Processes the given <code>SubInstBatchRecordData</code> and returns it.
     * The <code>SubInstBatchRecordData</code> is updated with information
     * needed by the writer component before returning it.
     * If the process fails but the record shall be re-processed at the end of this process,
     * the record will be put in the retry queue.
     * Note that during a subordinate installation SDP-id, Service Class, 
     * Promotion Plan, Account Group and Service Offerings must be omitted, e.g. null.
     * 
     * @param p_record  the <code>SubInstBatchRecordData</code>.
     * 
     * @return the given <code>SubInstBatchRecordData</code> updated with info for the writer component.
     * 
     * @throws PpasServiceException  if an error occurs while installing a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        String                  l_opId             = null;
        Msisdn                  l_msisdn           = null;
        PpasRequest             l_ppasRequest      = null;
        String                  l_market           = null;
        Msisdn                  l_masterMsisdn     = null;
        String                  l_agent            = null;
        String                  l_sdpId            = null;
        ServiceClass            l_serviceClass     = null;
        String                  l_promotionPlanId  = null;
        AccountGroupId          l_accountGroupId   = null;
        ServiceOfferings        l_serviceOfferings = null;
        EndOfCallNotificationId l_endOfCallId      = null;
        SubInstBatchRecordData  l_subInstRecord    = null;
        String                  l_recoveryLine     = null;
        boolean                 l_subordinate;
        PpasServiceFailedException l_psExe         = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                "Entering " + C_METHOD_processRecord);
        }
        
        l_subInstRecord = (SubInstBatchRecordData)p_record;
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10022,
                this,
                "Dump record: " + l_subInstRecord.dumpRecord());
        }

        if (!l_subInstRecord.isCorruptLine())
        {
            l_opId             = super.i_batchController.getSubmitterOpid();
            l_msisdn           = l_subInstRecord.getMsisdn();
            l_ppasRequest      = new PpasRequest(i_ppasSession, l_opId, l_msisdn);
            l_market           = l_subInstRecord.getMarket();
            l_masterMsisdn     = l_subInstRecord.getMasterMsisdn();
            l_agent            = l_subInstRecord.getAgent();
            l_endOfCallId      = l_subInstRecord.getEndOfCallNotificationId();
            
            // Check if this is an installation of a subordinate subscriber.
            l_subordinate = false;
            if (l_masterMsisdn != null  &&  !l_msisdn.equals(l_masterMsisdn))
            {
                l_subordinate = true;
            }

            if (!l_subordinate)
            {
                // Only populate these fields when installing a Master or Standalone subscriber 
                l_sdpId            = l_subInstRecord.getSdpId();
                l_serviceClass     = l_subInstRecord.getServiceClass();
                l_promotionPlanId  = l_subInstRecord.getPromotionPlan();
                l_accountGroupId   = l_subInstRecord.getAccountGroupId();
                l_serviceOfferings = l_subInstRecord.getServiceOfferings();
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_LOW,
                                PpasDebug.C_APP_MWARE,
                                PpasDebug.C_ST_TRACE,
                                null, C_CLASS_NAME, 50001, this,
                                "Installation type: " + i_installationType +
                                "   Routing method: " + i_routingMethod + 
                                "   SDP: " + l_sdpId);
            }
            try
            {
                if (i_installationType.equals(PpasInstallService.C_INSTALL_TYPE_SINGLE_STEP)
                        && !l_subordinate)
                {
                    if ((i_routingMethod.equals(C_ROUTING_METHOD_SDP)) && (l_sdpId == null))
                    {
                        l_psExe = new PpasServiceFailedException(
                                C_CLASS_NAME,
                                C_METHOD_processRecord,
                                10023,
                                this,
                                l_ppasRequest,
                                0L,
                                ServiceKey.get().invalidValueInId(l_msisdn, l_sdpId, "installSubscriber"));

                        i_logger.logMessage(l_psExe);
                        throw(l_psExe);
                    }
                    else if ((i_routingMethod.equals(C_ROUTING_METHOD_NBR)) && (l_sdpId != null))
                    {
                        // Simply ignore any SDP ID sent in
                        l_sdpId = null;
                    }
                }
                else
                {
                    if (l_sdpId != null)
                    {
                        // Simply ignore any SDP ID sent in
                        l_sdpId = null;
                    }
                }

                // Install the Subscriber.
                i_ppasInstallService.installSubscriber(l_ppasRequest,
                                                       super.i_isApiTimeout,
                                                       new Market(Market.getSrvaFromMarketStr(l_market),
                                                                  Market.getSlocFromMarketStr(l_market)),
                                                       l_masterMsisdn,
                                                       l_agent,
                                                       l_sdpId,
                                                       l_serviceClass,
                                                       l_promotionPlanId,
                                                       l_accountGroupId,
                                                       l_serviceOfferings,
                                                       false,
                                                       l_endOfCallId,
                                                       null,
                                                       true, // no promo use default
                                                       i_installationType);

                l_recoveryLine = l_subInstRecord.getRowNumber() + 
                                 BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                                 BatchConstants.C_SUCCESSFULLY_PROCESSED;
                l_subInstRecord.setRecoveryLine(l_recoveryLine);
                
                // Make sure that the record is not handled as a retry record any more.
                l_subInstRecord.setRetryRecord(false);
            }
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10024,
                        this,
                        "A PpasServiceException is caught: " + p_psExe.getMessage() + 
                        ",  key: " + p_psExe.getMsgKey());
                }

                // Always retry a subordinate installation if it hasn't been retried.
                if (l_subordinate  &&  !l_subInstRecord.isRetryRecord())
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10026,
                            this,
                            "Current record is added into the retry queue: " + 
                            l_subInstRecord.getInputLine());
                    }
                    super.addRetryRecord(l_subInstRecord);
                }
                else
                {
                    // Make sure that the record is not handled as a retry record any more.
                    l_subInstRecord.setRetryRecord(false);

                    if (p_psExe.getLoggingSeverity() == PpasServiceException.C_SEVERITY_FATAL)
                    {
                        // This is a fatal error, re-throw the 'PpasServiceException' in order to stop
                        // the complete process.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                            PpasDebug.C_APP_SERVICE,
                                            PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME,
                                            10028,
                                            this,
                                            "***ERROR: " +
                                            "A fatal error has occurred during batch installation -- " + 
                                            "msg key: " + p_psExe.getMsgKey());
                        }
                        throw p_psExe;
                    }

                    // Map the thrown 'PpasServiceException' to an appropriate error code.
                    mapException(l_subInstRecord, p_psExe);

                    // Create and set the recovery line.
                    l_recoveryLine = l_subInstRecord.getRowNumber() +
                        BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                        BatchConstants.C_NOT_PROCESSED;
                        l_subInstRecord.setRecoveryLine(l_recoveryLine);


                }
            }
        }
        else
        {
            // This was a corrupted record, set the recovery line (the error line is set by the reader comp.).
            l_recoveryLine = l_subInstRecord.getRowNumber() +
                             BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                             BatchConstants.C_NOT_PROCESSED;
            l_subInstRecord.setRecoveryLine(l_recoveryLine);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10030,
                this,
                "Leaving " + C_METHOD_processRecord);
        }

        return l_subInstRecord;
    }


    //----------------------------------------------------------------------
    //--  Private methods.                                                --
    //----------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_mapException = "mapException";        
    /**
     * Updates the passed <code>BatchRecordData</code> object's error line with an error code that 
     * corresponds to the passed <code>PpasServiceException</code>'s message key.
     * 
     * @param p_record      the <code>BatchRecordData</code>.
     * @param p_ppasServEx  the <code>PpasServiceException</code>.
     */
    private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
    {
        StringBuffer l_errorLine   = null;
        boolean      l_mappingDone = false;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_mapException);
        }

        // Set the last part of the error line.
        l_errorLine = new StringBuffer(BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                       p_record.getInputLine());

        // Look for matching mapping through the whole error code mapping array or until a matching
        // mapping is done.
        for (int i = 0; (i < C_ERROR_CODE_MAPPINGS.length  &&  !l_mappingDone); i++)
        {
            if (p_ppasServEx.getMsgKey().equals(C_ERROR_CODE_MAPPINGS[i][0]))
            {
                l_errorLine.insert(0, C_ERROR_CODE_MAPPINGS[i][1]);
                l_mappingDone = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10042,
                                    this,
                                    C_METHOD_mapException + 
                                    " -- A matching mapping is found, key = " + C_ERROR_CODE_MAPPINGS[i][0] +
                                    ",  error code = " + C_ERROR_CODE_MAPPINGS[i][1]);
                }
            }
        }
        if (!l_mappingDone)
        {
            // None of the expected mapping matches the given message key.
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10044,
                            this,
                            C_METHOD_mapException + 
                            " -- An unexpected PpasServiceException is detected, " +
                            "key = " + p_ppasServEx.getMsgKey() +
                            ",  Exception severity = " + p_ppasServEx.getLoggingSeverity());
            l_errorLine.insert(0, C_ERROR_CODE_UNEXPECTED_EXCEPTION);
        }

        p_record.setErrorLine(l_errorLine.toString());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_mapException);
        }
    }
}
// End of public class SubInstBatchProcessor
////////////////////////////////////////////////////////////////////////////////
//                        End of file
////////////////////////////////////////////////////////////////////////////////}
