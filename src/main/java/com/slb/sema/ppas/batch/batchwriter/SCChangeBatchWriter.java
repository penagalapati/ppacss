////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchWriter.java 
//      DATE            :       Jun 23, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Responsible for doing the final processing of a 
//                              service class change; It will write the outcome to file.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
//----------+-----------+--------------------------------------+--------------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.SCChangeBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SCChangeBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * Responsible for doing the final processing of a 
   service class change; It will write the outcome to file.
 */
public class SCChangeBatchWriter extends BatchWriter
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SCChangeBatchWriter";
    
    /**Gets how many records to be written. Gets from <code>p_params</code>. */    
    private int i_intervall = 0;
    /**Used to count how many successfull records that has been processed. */
    private int i_success   = 0;
    /**Used to count how many  records that has been processed. */
    private int i_processed = 0;    
    
    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord = true;
    
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;
    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;

    
    /**
     * Constructs a SubInstBatchWriter object.
     * @param p_ppasContext A PPasContext
     * @param p_logger      The logger used of the writer.
     * @param p_controller  The batch jobs batch controller. 
     * @param p_params      Holding parameters used of the writer. 
     * @param p_outQueue    The batch jobs out queue.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     * @throws IOException  When there is some error to write to file.
     */
    public SCChangeBatchWriter(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_controller,
        Map p_params,
        SizedQueue p_outQueue,
        PpasProperties p_properties)
        throws IOException
    {
        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }
        
        i_intervall = i_properties.getIntProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        
        String l_recoverPath =
            i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);

        String l_reportPath = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);

        String p_inputFile = (String)i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);        

        //Get the filname with out the prefix 
        String l_inputFileArr[] = p_inputFile.split("[.]");
       
        //Create an absolute file name and add prefix .rpt or .rec.
        String l_reportFile = l_reportPath + "/" + l_inputFileArr[0] + BatchConstants.C_EXTENSION_TMP_FILE;
        super.i_recoveryFileName = 
            l_recoverPath  + "/" + l_inputFileArr[0] + BatchConstants.C_EXTENSION_RECOVERY_FILE;
       
        super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFile));
        super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructing " + C_CLASS_NAME);
        }
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
   
    /**
     * Writes information to the recovery file and to the report file if there is any errors in the file. 
     * It also poulates a <code>BatchJobData</code> object with information from <code>p_params</code>. 
     * @param p_record Holds the information about the report line number and the recovery line number. 
     * @throws IOException when failing to write to the report file.
     *      * @throws PpasServiceException No specific keys are anticipated.
     */
     protected void writeRecord(BatchRecordData p_record)throws IOException, PpasServiceException
     {
         if (PpasDebug.on)
         {
             PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_START,
                 C_CLASS_NAME,
                 10100,
                 this,
                 "Enter " + C_METHOD_writeRecord);
         }
         SCChangeBatchRecordData l_record = (SCChangeBatchRecordData)p_record;
        
         // Save number of already processed records for the trailer printout.
         if ( this.i_firstRecord )
         {
             this.i_firstRecord = false;
             this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
             this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
         }
         if (PpasDebug.on)
         {
             PpasDebug.print(
                 PpasDebug.C_LVL_VLOW,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_START,
                 C_CLASS_NAME,
                 10360,
                 this,
                 "noSuccessfully processed="+ this.i_numberOfSuccessfullyProcessedRecords+
                 " noFailured="+this.i_numberOfFaultyRecords);
         }

         //Check if error file info exist and write it to file.
         if (l_record.getErrorLine() != null)
         {
             super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine());
             super.i_errors++;
         }
         else
         {
             i_success++;
         }
         
         // If this record failed during the previous run - decrease it otherwise it will be counted
         // twice as a failure record in the baco-table
         if ( l_record.getFailureStatus())
         { 
             this.i_numberOfFaultyRecords--;
         }
        
         i_processed++;

         //Always write to recovery file.
         super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine());

        if ((i_processed % i_intervall) == 0)
        {
            updateStatus();
        }
        
         if (PpasDebug.on)
         {
             PpasDebug.print(
                 PpasDebug.C_LVL_HIGH,
                 PpasDebug.C_APP_SERVICE,
                 PpasDebug.C_ST_END,
                 C_CLASS_NAME,
                 10110,
                 this,
                 "Leaving " + C_METHOD_writeRecord);
         }
     }

     /**
      * Does all necessary final processing that is writing to file(s) and/or database.
      * @throws IOException If it is not possible to write to the file.
      * @throws PpasServiceException No specific keys are anticipated.
      */
    protected void updateStatus() throws IOException, PpasServiceException
    {
        super.flushFiles();
        BatchJobData l_jobData = ((SCChangeBatchController)super.i_controller).getKeyedControlRecord();
        l_jobData.setNoOfRejectedRec(Integer.toString(super.i_errors + i_numberOfFaultyRecords));
        l_jobData.setNoOfSuccessRec(Integer.toString(i_success + i_numberOfSuccessfullyProcessedRecords));

        super.updateControlInfo(l_jobData);
    }
    
    /** Writes out the trailer record.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS
     * @throws IOException Error from writing to output file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS +
                         (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(
                l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;

        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }

}

