////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchController
//      DATE            :       10-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME        | DESCRIPTION                         | REFERENCE
//----------+-------------+-------------------------------------+---------------
// DD/MM/YY | <name>      | <brief description of               | <reference>
//          |             | change>                             |
//----------+-------------+-------------------------------------+---------------
// 08/05/07 | L Lundberg  | Extends the super class             | PpacLon#3033/11460
//          |             | FileDrivenBatchController instead   |
//          |             | of  the BatchController class.      |
//          |             | Added methods:                      |
//          |             | getBatchJobType()                   |
//          |             | getValidFilenameRegEx()             |
//          |             | Removed methods:                    |
//          |             | addControlInformation()             |
//          |             | getKeyedControlRecord()             |
//----------+-------------+-------------------------------------+---------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.StatusChangeBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.StatusChangeBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.StatusChangeBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;


/**
 * This <code>StatusChangeBatchController</code> class is the entry point for the Subscriber Status Change
 * batch it is responsible to create and start all underlying components such as the reader, the processor
 * and the writer components.
 */
public class StatusChangeBatchController extends FileDrivenBatchController
{
    //------------------------------------------------------
    //  Class level constant
    //------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME          = "StatusChangeBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_sta";

    
    /**
     * Constructs a new <code>StatusChangeBatchController</code> object.
     * 
     * @param p_context        A <code>PpasContext</code> object.
     * @param p_jobType        The type of job (e.g. BatchInstall).
     * @param p_jobId          The unic id for this job.
     * @param p_jobRunnerName  The name of the job runner in which this job is running.
     * @param p_params         Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     */
    public StatusChangeBatchController(PpasContext p_context,
                                       String      p_jobType,
                                       String      p_jobId,
                                       String      p_jobRunnerName,
                                       Map         p_params) throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10300, this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10310, this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise instance variables and read properties.
     * @throws PpasConfigException when configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10320, this,
                            BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10330, this,
                            BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";        
    /**
     * This method will instantiate the correct reader-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the reader-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a StatusChangeBatchReader.
     */
    public BatchReader createReader()
    {
        StatusChangeBatchReader l_reader = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10340, this,
                            BatchConstants.C_ENTERING + C_METHOD_createReader);
        }

        if (super.i_startComponents)
        {
            l_reader = new StatusChangeBatchReader(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_inQueue,
                                                   super.i_params,
                                                   super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10350, this,
                            BatchConstants.C_LEAVING + C_METHOD_createReader);
        }
        return l_reader;
    }
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";        
    /**
     * This method will instantiate the correct writer-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the writer-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a StatusChangeBatchWriter.
     * @throws IOException Could not create output files.
     */
    public BatchWriter createWriter()
    {
        StatusChangeBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10360, this,
                            BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        if (super.i_startComponents)
        {
            l_writer = new StatusChangeBatchWriter(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_params,
                                                   super.i_outQueue,
                                                   super.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10370, this,
                            BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }
        return l_writer;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";        
    /**
     * This method will instantiate the correct processor-class and passes on
     * the required parameters and also a reference to itself. The later is used
     * for the processor-component to report back the controller-component. The method
     * is called from the super-class inner class method doRun() when start is ordered.
     * 
     * @return Reference to a StatusChangeBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        StatusChangeBatchProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10380, this,
                            BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        if (super.i_startComponents)
        {
            l_processor = new StatusChangeBatchProcessor(super.i_ppasContext,
                                                         super.i_logger,
                                                         this,
                                                         super.i_inQueue,
                                                         super.i_outQueue,
                                                         super.i_params,
                                                         this.i_properties);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10390, this,
                            BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }
        return l_processor;
    }


    /**
     * Returns the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected String getBatchJobType()
    {
        return BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE;
    }


    /**
     * Returns a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected String getValidFilenameRegEx()
    {
        return "INSTALL_STATUS_" + C_REG_EX_VALID_DEFAULT_FILENAME;
    }

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * This method call the service defined by PpasBatchControlService. In this case
     * for a filedriven  updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobControlData l_batchJobControlData = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10400, this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        l_batchJobControlData = getKeyedBatchJobControlData();
        l_batchJobControlData.setStatus(p_status.charAt(0));
        
        try
        {
            super.i_batchContService.updateJobDetails(null, l_batchJobControlData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10410, this,
                                C_METHOD_updateStatus + ": PpasServiceException from updateJobDetails");
            }
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10420, this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    }
 
 
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10430, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10440, this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }
        return l_batchJobControlData;
    }
}
