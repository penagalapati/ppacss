////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchProcessor.java
//      DATE            :       23-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class gets MiscBatchDataRecords from the in
//                              queue and processes them by calling the business 
//                              service modifyAdditionalInfo.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
//----------+-----------+--------------------------------------+----------------
// 24/06/08 | M Erskine | Change timeout to read the JDBC      | PpacLon#3650/13158
//          |           | connection timeout from the JsContext|
//          |           | instead of just hard-coding it.      |
//----------+-----------+--------------------------------------+----------------
// 20/05/10 |Siva Sanker| Miscellaneous Data Upload should not | PpacBan#3688/13752
//          |Reddy      | be done for a NPC cut over MSISDN    |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.MiscBatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class gets MiscBatchDataRecords from the in queue and processes 
 * them by calling the business service modifyAdditionalInfo.
 */
public class MiscBatchProcessor extends BatchProcessor
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                      = "MiscBatchProcessor";

    /** Action type for Insert. Value is {@value}. */
    private static final String C_ACTION_TYPE_INS                 = "I"; 

    /** Action type for Update. Value is {@value}. */
    private static final String C_ACTION_TYPE_UPD                 = "U"; 

    /** Action type for Delete. Value is {@value}. */
    private static final String C_ACTION_TYPE_DEL                 = "D"; 

    /** Constant holding the error code for MSISDN doesn't exist. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_AVAILABLE = "04";

    /** Constant holding the error code for insert action failed. Value is {@value}. */
    private static final String C_ERROR_CODE_INSERT_FAILED        = "08";

    /** Constant holding the error code for update action failed. Value is {@value}. */
    private static final String C_ERROR_CODE_UPDATE_FAILED        = "09";

    /** Constant holding the error code for delete action failed. Value is {@value}. */
    private static final String C_ERROR_CODE_DELETE_FAILED        = "10";

    /** Default non-data field value. Value is {@value}. */
    private static final String C_DEFAULT_FIELD_VALUE             = " ";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasAdditionalInfoService i_ppasAdditionalInfoService = null;

    /** The <code>Session</code> object. */
    private PpasSession               i_ppasSession               = null;

    /**
     * Constructs an instance of this <code>MiscBatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the Logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process parameters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public MiscBatchProcessor(
        PpasContext     p_ppasContext,
        Logger          p_logger,
        BatchController p_batchController,
        SizedQueue      p_inQueue,
        SizedQueue      p_outQueue,
        Map             p_params,
        PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        PpasRequest l_ppasRequest;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10001, this,
                            BatchConstants.C_CONSTRUCTING );
        }

        i_ppasSession = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest = new PpasRequest(i_ppasSession);
        
        i_ppasAdditionalInfoService = new PpasAdditionalInfoService(l_ppasRequest, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            BatchConstants.C_CONSTRUCTED );
        }
    } // End of constructor

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";        
    /**
     * Processes the given <code>MiscBatchDataRecord</code> and returns it.
     * The returned <code>MiscBatchDataRecord</code> is updated with information
     * needed by the writer component.
     * 
     * @param p_record  the <code>MiscBatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component.
     * @throws PpasServiceException  if a fatal error occurs while updating miscellaneous data.
     */
    public BatchRecordData processRecord(BatchRecordData p_record) 
        throws PpasServiceException
    {
        String              l_opId            = null;
        Msisdn              l_msisdn          = null;
        PpasRequest         l_ppasRequest     = null;
        long                l_timeout         = BatchConstants.C_DEFAULT_TIMEOUT;
        MiscBatchRecordData l_batchRecordData = null;
        String              l_recoveryLine    = null;
        String              l_errorLine       = null;
        Vector              l_miscValues      = null;
        AdditionalInfoData  l_infoData        = null;
        PpasAccountService  l_ppasAccountService = null;
        BasicAccountData    l_basicAccountData= null;
        long                l_custStatusFlags = 0;
        String              l_custId          = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 17000, this,
                            BatchConstants.C_ENTERING + C_METHOD_processRecord +
                            " -- " + "MiscBatchRecordData:\n" + p_record.dumpRecord());
        }
       
        l_batchRecordData = (MiscBatchRecordData)p_record;
        
        if (!l_batchRecordData.isCorruptLine())
        {
            l_opId        = i_batchController.getSubmitterOpid();
            l_msisdn      = l_batchRecordData.getMsisdn();
            l_ppasRequest = new PpasRequest(i_ppasSession, l_opId, l_msisdn);
            
            l_miscValues = l_batchRecordData.getFields();

            // Call Additional Info service
            try
            {
                l_ppasAccountService = new PpasAccountService(l_ppasRequest, i_logger, i_ppasContext);
                
                //Fetch the cust_id of the msisdn
                l_custId = (l_ppasAccountService.getBasicData(
                        l_ppasRequest,
                        l_custStatusFlags,
                        l_timeout)).getCustId();
                
                if(l_custId != null || l_custId != "" )
                {
                    l_ppasRequest = new PpasRequest(i_ppasSession, l_opId,
                                                    l_msisdn, l_custId);
                    
                    l_basicAccountData = l_ppasAccountService.getBasicData(
                            l_ppasRequest, l_custStatusFlags, l_timeout);
                    
                    //To check if the msisdn is Number Plan Cutovered (NPC)
                    if(!l_msisdn.equals(l_basicAccountData.getMsisdn()))
                    {    
                        // Create 'warning' level exception, log and throw it.
                        PpasServiceException p_psExe = new PpasServiceFailedException
                                            (C_CLASS_NAME,
                                             C_METHOD_processRecord,
                                             12557,
                                             this,
                                             l_ppasRequest,
                                             0,
                                             ServiceKey.get().msisdnNotExists(l_msisdn));                        
                        i_logger.logMessage (p_psExe);
                        throw (p_psExe);
                     }   
                }
                if (!l_batchRecordData.getOperationCode().equals(C_ACTION_TYPE_DEL))
                {
                    // Get the data for the current MSISDN.
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 17010, this,
                                        C_METHOD_processRecord + " -- Get misc. info.");
                    }

                    l_infoData = i_ppasAdditionalInfoService.getAdditionalInfo(l_ppasRequest, 5000L);
                    if (l_infoData != null)
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL, PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME, 17020, this,
                                            C_METHOD_processRecord + " -- Misc. info received.");
                        }

                        if (l_infoData.getMiscFieldV() != null)
                        {
                            l_miscValues = updateFields(l_infoData.getMiscFieldV(),
                                                        l_batchRecordData.getFields());
                        }
                        else
                        {
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL,
                                                PpasDebug.C_ST_TRACE,
                                                C_CLASS_NAME, 17030, this,
                                                C_METHOD_processRecord +
                                                " -- Misc. info contains no fields.");
                            }

                            l_miscValues = l_batchRecordData.getFields();
                        }
                    }
                    else
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL, PpasDebug.C_ST_TRACE,
                                            C_CLASS_NAME, 17040, this,
                                            C_METHOD_processRecord + " -- No misc. info received.");
                        }

                        l_miscValues = l_batchRecordData.getFields();
                    }
                }
                else
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 17050, this,
                                        C_METHOD_processRecord + " -- 'Delete' action, create emtpy Vector.");
                    }

                    l_miscValues = new Vector();
                }
                
                String l_jdbcConnTimeoutLong = String.valueOf(i_ppasContext.getAttribute(
                           "com.slb.sema.ppas.support.PpasContext.connResponseTimeout"));

                l_timeout = (l_jdbcConnTimeoutLong != null)? Long.valueOf(l_jdbcConnTimeoutLong).longValue() :
                                                             BatchConstants.C_DEFAULT_TIMEOUT;

                i_ppasAdditionalInfoService.modifyAdditionalInfo(l_ppasRequest, l_timeout, l_miscValues);

                l_recoveryLine = l_batchRecordData.getRowNumber() + 
                                BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                                BatchConstants.C_SUCCESSFULLY_PROCESSED;
                l_batchRecordData.setRecoveryLine(l_recoveryLine);
            }
            catch (PpasServiceException p_pServExc)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 17060, this,
                                    "PpasServiceException is caught: " + p_pServExc.getMessage() + 
                                    ",  key: " + p_pServExc.getMsgKey());
                }

                l_recoveryLine = l_batchRecordData.getRowNumber() + 
                                 BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                                 BatchConstants.C_NOT_PROCESSED;
                l_batchRecordData.setRecoveryLine(l_recoveryLine);

                if (p_pServExc.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS) ||
                    p_pServExc.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE) ||
                    p_pServExc.getMsgKey().equals(PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT))
                {
                    // Report it as "MSISDN not available".
                    l_errorLine = C_ERROR_CODE_MSISDN_NOT_AVAILABLE +
                                  BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                  l_batchRecordData.getInputLine();
                }
                else
                {
                    if (p_pServExc.getLoggingSeverity() == PpasServiceException.C_SEVERITY_FATAL)
                    {
                        // Log the exception and re-throw it.
//                        super.i_logger.logMessage(p_pServExc);
                        throw p_pServExc;
                    }

                    if (l_batchRecordData.getOperationCode().equals(C_ACTION_TYPE_INS))
                    {
                        // Report it as "Insert failed".
                        l_errorLine = C_ERROR_CODE_INSERT_FAILED +
                                      BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                      l_batchRecordData.getInputLine();
                    }
                    else if (l_batchRecordData.getOperationCode().equals(C_ACTION_TYPE_UPD))
                    {
                        // Report it as "Update failed".
                        l_errorLine = C_ERROR_CODE_UPDATE_FAILED +
                                      BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                      l_batchRecordData.getInputLine();
                    }
                    else
                    {
                        // The action must have been the delete action.
                        // Report it as "Delete failed".
                        l_errorLine = C_ERROR_CODE_DELETE_FAILED +
                                      BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                      l_batchRecordData.getInputLine();
                    }
                }
            } // End of catch() clause.
        }
        else
        {
            l_recoveryLine = l_batchRecordData.getRowNumber() + 
                             BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                             BatchConstants.C_NOT_PROCESSED;
            l_batchRecordData.setRecoveryLine(l_recoveryLine);
            
        } // if (!l_batchRecordData.isCorruptLine())

        // Set error line in batch data record if an error has occurred.
        if (l_errorLine != null)
        {
            l_batchRecordData.setErrorLine(l_errorLine);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10095, this,
                            BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }

        return l_batchRecordData;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateFields = "updateFields";
    /**
     * Creates a new field value <code>Vector</code> from the given current info <code>Vector</code> and
     * updates it with field values from the new info <code>Vector</code>.
     * 
     * @param p_currentInfo  the current info <code>Vector</code>.
     * @param p_newInfo      the new info <code>Vector</code>.
     * @return  a new field value <code>Vector</code> from the given current info <code>Vector</code> and
     *          updates it with field values from the new info <code>Vector</code>.
     */
    private Vector updateFields(Vector p_currentInfo, Vector p_newInfo)
    {
        Vector   l_fieldVec     = new Vector();
        String   l_newField     = null;
        String[] l_currFieldArr = new String[ p_currentInfo.size() ];
        
        p_currentInfo.toArray(l_currFieldArr);
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_ALL, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 18000, this,
                            C_METHOD_updateFields + " -- current fields array size = " +
                            l_currFieldArr.length);
        }

        for (int i = 0; i < p_newInfo.size(); i++)
        {
            l_newField = (String)p_newInfo.elementAt(i);
            if (!l_newField.equals(C_DEFAULT_FIELD_VALUE))
            {
                l_currFieldArr[i] = l_newField;
            }
        }

        for (int i = 0; i < l_currFieldArr.length; i++)
        {
            l_fieldVec.add(l_currFieldArr[i]);
        }
        
        return l_fieldVec;
    }
} // End of Class MiscBatchProcessor
