////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchController.java
//      DATE            :       16-April-2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       This class implements all common logic for 
//                              starting and maintaining batch processes.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 23/08/06 | S. Hagstrom   | Retrofitted changes from 3020    | PpacLon#2374
//          |               | PpacLon#1996                     |
//----------+---------------+----------------------------------+----------------
// 13/03/07 | L. Lundberg   | Correction of a problem reported | PpacLon#2987/11149
//          |               | by the 'FindBugs' tool:          |
//          |               | "The parameter p_status to method|
//          |               | updateStatus(String) is dead upon|
//          |               | entry but overwritten."          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | L. Lundberg   | One new method is added:         | PpacLon#3033/11279
//          |               | verifyPreConditions().           |
//          |               | This method is called from the   |
//          |               | doRun() method in the Manager    |
//          |               | class before starting the process|
//          |               | loop.                            |
//          |               | The addControlInformation()      |
//          |               | method is also called before the |
//          |               | the process loop is started.     |
//----------+---------------+----------------------------------+----------------
// 24/06/08 | M Erskine | Change i_timeout to read the JDBC    | PpacLon#3650/13158
//          |           | connection timeout from the JsContext|
//          |           | . To clarify, it should NOT be the   |
//          |           | timeout for an IsClient to wait for a|
//          |           | response from an IsServer.           |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.exceptions.BatchInvalidParameterException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.batch.exceptions.BatchServiceException;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.logging.PpasLoggerPool;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcConnectionPool;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.js.jobrunner.JavaJob;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Implemetns all common logic for starting and maintaining batch processes.*/
public abstract class BatchController extends JavaJob
{
    //--------------------------------------------------------------------------
    //  Private class level constants.                                        --
    //-------------------------------------------------------------------------- 
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                    = "BatchController";

    /** The name of the msisdn input string strip size property. */
    private static final String C_PROP_INPUT_MSISDN_STRIP_SIZE  =
        "com.slb.sema.ppas.batch.msisdnInputStripSize";

    /** The name of the msisdn input string prefix property. */
    private static final String C_PROP_INPUT_MSISDN_PREFIX      =
        "com.slb.sema.ppas.batch.msisdnInputPrefix";

    /** The name of the msisdn output string strip size property. */
    private static final String C_PROP_OUTPUT_MSISDN_STRIP_SIZE =
        "com.slb.sema.ppas.batch.msisdnOutputStripSize";

    /** The name of the msisdn output string prefix property. */
    private static final String C_PROP_OUTPUT_MSISDN_PREFIX     =
        "com.slb.sema.ppas.batch.msisdnOutputPrefix";


    //-------------------------------------------------------------------------- 
    //   Protected instance variables.                                        --
    //-------------------------------------------------------------------------- 
    /** Queue to store messages in. */
    protected ArrayList               i_messageQueue          = null;

    /** Reference to the in queue. */
    protected SizedQueue              i_inQueue               = null;

    /** Reference to the out queue. */
    protected SizedQueue              i_outQueue              = null;

    /** <code>Logger </code> for the batch subsystem.*/
    protected Logger                  i_logger                = null;

    /** The PpasContex. */
    protected PpasContext             i_ppasContext           = null;

    /**Holding paramters for the controller. */
    protected Map                     i_params                = null;
    
    /**A referens to the <code>PpasBatchControlService</code> in the <code>IsApi</code>. */       
    protected PpasBatchControlService i_batchContService      = null;
   
    /**Timeout read from the property file or a "batchDefault" of 10000L. */
    protected long                    i_timeout               = BatchConstants.C_DEFAULT_TIMEOUT;

    /** The execution date and time stamp for the current batch job. */
    protected PpasDateTime            i_executionDateTime     = null;

    /** Saves the date retrieved from the filename. */
    protected String                  i_fileDate              = null;

    /** Saves the sequence number retrieved from the filename. */
    protected String                  i_seqNo                 = null;

    /** A global flag that indicates if the underlying components (reader, writer and processor)
      * shall be started. */
    protected boolean                 i_startComponents       = true;
    
    /** The number of characters that shall be removed from the beginning of an incoming MSISDN string. */
    protected int                     i_msisdnInputStripSize  = 0;

    /** Incoming MSISDN string prefix. */
    protected String                  i_msisdnInputPrefix     = null;

    /** The number of characters that shall be removed from the beginning of an output MSISDN string. */
    protected int                     i_msisdnOutputStripSize = 0;

    /** Output MSISDN string prefix. */
    protected String                  i_msisdnOutputPrefix    = null;


    //-------------------------------------------------------------------------- 
    //   Private instance variables.                                          --
    //--------------------------------------------------------------------------
    /** Reference to the inner class Manager. */
    private Manager           i_manager            = new Manager();

    /** <code>InstrumentManager</code> to register/unregister with (or null if not to register).*/
    private InstrumentManager i_instrumentManager  = null;

    /** The maximum size of the in queue. It is set in a property file. */
    private int               i_inQueueMaxSize     = 0;

    /** The maximum size of the out queue. It is set in a property file. */
    private int               i_outQueueMaxSize    = 0;

   
    /**
     * Constructs a new BatchController object.
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchInstall).
     * @param p_jobId       The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     */
    
    public BatchController(PpasContext p_context,
                           String      p_jobType,
                           String      p_jobId,
                           String      p_jobRunnerName,
                           Map         p_params)
//        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }
 
        JdbcConnection      l_connection     = null;
        JdbcConnectionPool  l_connectionPool = null;
        PpasSession         l_session        = new PpasSession();
        PpasRequest         l_request        = null; 
        BusinessConfigCache l_cache          = null;

        // Get business configuration cache. 
        // Must be done in the controller, while some Readers also need this information.
        try
        {
            l_session.setContext(p_context);
            l_request = new PpasRequest(l_session);
            l_connectionPool = (JdbcConnectionPool) p_context.getAttribute("JdbcConnectionPool");

            // Get a JdbcConnection.
            l_connection = l_connectionPool.getConnection ( C_CLASS_NAME,
                    C_CLASS_NAME,
                    10010,
                    this,
                    l_request,
                    0,
                    BatchConstants.C_DEFAULT_TIMEOUT );
            
            l_cache = (BusinessConfigCache)p_context.getAttribute("BusinessConfigCache");
            //Load cache ready for batches to use in DB services.
            l_cache.loadAll(l_request, l_connection);
        }
        catch (PpasSqlException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10002,
                    this,
                    "Constructor - exception load business config cache" );
            }
        }
        finally
        {
            if (l_connectionPool != null && l_connection != null)
            {
                // Put the connection back in the pool.
                l_connectionPool.putConnection(0, l_connection);
            }            
        }

        i_messageQueue = new ArrayList();
        i_ppasContext  = p_context;
        i_params       = p_params;
        
        // Set all batches to be both pausible and stoppable by default.
        // This could be overridden in the sub-class Constructor for an individual batch.
        super.setPausable(true);
        super.setStoppable(true);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_postMessage = "postMessage";

    /**
     * The entry-point to the message queue. All message are posted on the queue and 
     * the manager thread will carry out the actual processing of each request.
     * @param p_msg A <code>BatchMessage</code>.
     */
    public void postMessage(BatchMessage p_msg)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                "Enter " + C_METHOD_postMessage);

        }
        
        synchronized (i_messageQueue)
        {
            i_messageQueue.add(p_msg);
            
            //Wake-up the manager thread so request can be committed 
            i_messageQueue.notify();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10120,
                this,
                "Leaving " + C_METHOD_postMessage);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_doRequestStart = "doRequestStart";

    /**
     * Starts the batch job. Which batch to start is specified by the sub-class.
     */
    public void doRequestStart()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10310,
                this,
                "Enter " + C_METHOD_doRequestStart);
        }

        BatchMessage l_msg = new BatchMessage();
        l_msg.setSendingComponent(BatchMessage.C_COMPONENT_CLIENT);
        l_msg.setTargetComponent(BatchMessage.C_COMPONENT_ALL);
        l_msg.setRequest(BatchMessage.C_REQUEST_START);
        l_msg.setStatus(BatchMessage.C_STATUS_NONE);

        postMessage(l_msg);

        //Start the Manager thread.
        i_manager.start();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10320,
                this,
                "Leaving " + C_METHOD_doRequestStart);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_doRequestStop = "doRequestStop";

    /**
     * Stops the entire batch processing. 
     */
    public void doRequestStop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10410,
                this,
                "Enter " + C_METHOD_doRequestStop);
        }
        
        BatchMessage l_msg = new BatchMessage();
        l_msg.setSendingComponent(BatchMessage.C_COMPONENT_CLIENT);
        l_msg.setTargetComponent(BatchMessage.C_COMPONENT_ALL);
        l_msg.setRequest(BatchMessage.C_REQUEST_STOP);
        l_msg.setStatus(BatchMessage.C_STATUS_NONE);

        postMessage(l_msg);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10420,
                this,
                "Leaving " + C_METHOD_doRequestStop);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_doRequestPause = "doRequestPause";

    /**
     * Pauses all running threads.
     */
    public void doRequestPause()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10510,
                this,
                "Enter " + C_METHOD_doRequestPause);
        }

        BatchMessage l_msg = new BatchMessage();
        l_msg.setSendingComponent(BatchMessage.C_COMPONENT_CLIENT);
        l_msg.setTargetComponent(BatchMessage.C_COMPONENT_ALL);
        l_msg.setRequest(BatchMessage.C_REQUEST_PAUSE);
        l_msg.setStatus(BatchMessage.C_STATUS_NONE);

        postMessage(l_msg);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10520,
                this,
                "Leaving " + C_METHOD_doRequestPause);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_doRequestResume = "doRequestResume";

    /**
     * Is called to resume processing after a pause has been initiated.
     */
    public void doRequestResume()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10610,
                this,
                "Enter " + C_METHOD_doRequestResume);
        }        

        BatchMessage l_msg = new BatchMessage();
        l_msg.setSendingComponent(BatchMessage.C_COMPONENT_CLIENT);
        l_msg.setTargetComponent(BatchMessage.C_COMPONENT_ALL);
        l_msg.setRequest(BatchMessage.C_REQUEST_RESUME);
        l_msg.setStatus(BatchMessage.C_STATUS_NONE);

        postMessage(l_msg);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10620,
                this,
                "Leaving " + C_METHOD_doRequestResume);
        }
    }


    /**
     * Returns the execution timestamp (date & time) as a <code>PpasDateTime</code> object.
     * 
     * @return the execution timestamp (date & time) as a <code>PpasDateTime</code> object.
     */
    public PpasDateTime getExecutionDateTime()
    {
        return i_executionDateTime;
    }


    /** 
     * This is a hook method to be used (overridden) by any sub-class of this super-class.
     * This method should return a <code>BatchJobControlData</code> object updated with
     * the correct key values (js job id and execution date and time).
     * 
     * @return a <code>BatchJobControlData</code> object updated with the correct key values
     *        (js job id and execution date and time).
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        return null;
    }

    
    /**
     * An abstract method that creates a <code>BatchReader</code>.
     * @return A <code>BatchReader</code>.
     */
    protected abstract BatchReader createReader();

    /**
     * An abstract method that creates a <code>BatchWriter</code>.
     * @return A <code>BatchWriter</code>.
     * @throws IOException When something going wrong when creating the writer.
     */
    protected abstract BatchWriter createWriter() throws IOException;

    /**
     * An abstract method that creates a <code>BatchProcessor</code>.
     * @return A <code>BatchProcessor</code>.
     */
    protected abstract BatchProcessor createProcessor();

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Get the properties, create a logger.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */ 
    protected void init() throws PpasConfigException
    {
        PpasLoggerPool l_loggerPool = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                11310,
                this,
                "Entering " + C_METHOD_init);
        }

        i_instrumentManager = new InstrumentManager();

        // Get the loggerpool from the context.
        l_loggerPool = i_ppasContext.getLoggerPool();
        // Get the default logger. The default logger is determined by the
        // process name, which in this case is the job runner.
        i_logger = l_loggerPool.getLogger();
            
        i_batchContService = new PpasBatchControlService(null, i_logger, i_ppasContext);

        //Get the properties
        i_inQueueMaxSize  = i_properties.getIntProperty(BatchConstants.C_IN_QUEUE_MAX_SIZE);
        i_outQueueMaxSize = i_properties.getIntProperty(BatchConstants.C_OUT_QUEUE_MAX_SIZE);
        
        String l_jdbcConnTimeoutLong = String.valueOf(i_ppasContext.getAttribute(
                                           "com.slb.sema.ppas.support.PpasContext.connResponseTimeout"));

        i_timeout = (l_jdbcConnTimeoutLong != null) ? Long.valueOf(l_jdbcConnTimeoutLong).longValue() :
                                                      BatchConstants.C_DEFAULT_TIMEOUT;
        
        // Get the msisdn format properties.
        i_msisdnInputStripSize  = i_properties.getIntProperty(C_PROP_INPUT_MSISDN_STRIP_SIZE, 0);
        i_msisdnInputPrefix     = i_properties.getTrimmedProperty(C_PROP_INPUT_MSISDN_PREFIX, "");
        i_msisdnOutputStripSize = i_properties.getIntProperty(C_PROP_OUTPUT_MSISDN_STRIP_SIZE, 0);
        i_msisdnOutputPrefix    = i_properties.getTrimmedProperty(C_PROP_OUTPUT_MSISDN_PREFIX, "");

//        //Start the thread
//        i_manager.start();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                11320,
                this,
                "Leaving " + C_METHOD_init);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_formatMsisdn = "formatMsisdn";
    /**
     * Formats and returns the given MSISDN string.
     * If the <code>p_inputFormat</code> parameter is <code>true</code> the given MSISDN string is assumed to
     * be an input string, otherwise it is assumed to be an output string.
     * <br>The formatting is done in two steps:
     * <br>Step 1: the 'strip size' number of characters will removed from the beginning of the passed
     *             MSISDN string.
     * <br>Step 2: the 'prefix' string will be inserted in the front of the remaining MSISDN string.
     * <br>Both the 'strip size' and 'prefix' values is set by properties in a corresponding property file.
     * 
     * @param p_msisdnStr    the msisdn string.
     * @param p_inputFormat  if <code>true</code> the given MSISDN string is assumed to be an input string
     *                       and it will be formatted accordingly, otherwise it is assumed to be an output
     *                       string.
     * 
     * @return  a formatted MSISDN string.
     */
    public String formatMsisdn(String p_msisdnStr, boolean p_inputFormat)
    {
        StringBuffer l_msisdnFormattedStr = null;
        int          l_stripSize          = 0;
        String       l_prefix             = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10400,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_formatMsisdn);
        }

        if (p_inputFormat)
        {
            // Format an input msisdn string.
            l_stripSize = i_msisdnInputStripSize;
            l_prefix    = i_msisdnInputPrefix;
        }
        else
        {
            // Format an output msisdn string.
            l_stripSize = i_msisdnOutputStripSize;
            l_prefix    = i_msisdnOutputPrefix;
        }

        // Remove the specified 'strip size' number of characters from the beginning of 
        // the given MSISDN string and create a 'StringBuffer' of the remaining part.
        l_msisdnFormattedStr = new StringBuffer(p_msisdnStr.substring(l_stripSize));
        
        // Insert the specified 'prefix'.
        l_msisdnFormattedStr.insert(0, l_prefix);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10400,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_formatMsisdn);
        }
        return l_msisdnFormattedStr.toString();
    }
    

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form:
     *     XXXXXXXX_yyyymmdd_sssss.DAT /IPG.
     * @param p_fileName The name of the file.
     * @param p_patternDAT regular expression for filename with .DAT
     * @param p_patternIPG regular expression for filename with .IPG
     * @param p_patternSCH regular expression for filename with .SCH
     * @param p_dateIndex The dates position in the file name.
     * @param p_sequenceIndex The sequence numbers position in the file name. If it is
     *                        <0 then no sequence number is present in the filename.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid(String p_fileName, 
                                      String p_patternDAT,
                                      String p_patternIPG,
                                      String p_patternSCH,
                                      int p_dateIndex,
                                      int p_sequenceIndex)
    {               
        
        boolean  l_validFileName          = true;   // Help variable - return value. Assume filename is OK.
        String[] l_fileNameComponents     = null;   // After split on "_".
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }
        
        if (p_fileName != null) 
        {
            if ( p_fileName.matches(p_patternDAT) ||
                 p_fileName.matches(p_patternIPG) ||
                 p_fileName.matches(p_patternSCH) )
            {
 
                // Filename: XXXXXXXX_yyyymmdd_sssss.*
                // Retrieve file date and  sequence number from the filename
                l_fileNameComponents = p_fileName
                        .split(BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER);            
                i_fileDate           = l_fileNameComponents[p_dateIndex];
                if ( p_sequenceIndex > 0 )
                {
                    i_seqNo = l_fileNameComponents[p_sequenceIndex]; 
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        C_METHOD_isFileNameValid + " date=" + i_fileDate + " seqNo=" + i_seqNo);
                }
         
            }           
            else
            {
                // invalid fileName
                l_validFileName = false;
            }
        } // end - if fileName != null        
        else
        {
            l_validFileName = false; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10410,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
        
    } // end of isFileNameValid


 
    
    /**
     * This method converts a date and timestring: yyyymmddHHmiss to
     * yyyymmddHH_mi_ss. This is needed by the PpasDateTime in addControlInformation.
     * @param p_string p_string Date string to convert.
     * @return the new date and time string. 
    */
    protected String convertDateString( String p_string )
    {
        final int    L_LENGTH_FULL_DATE_TIME      = 14;
        final int    L_LENGTH_DATE                = 8;
        final int    L_LENGTH_MINUTES_AND_SECONDS = 2;

        String       l_returnValue = null;
        StringBuffer l_tmp         = new StringBuffer();
        int          l_startIndex  = 0;
        int          l_endIndex    = L_LENGTH_DATE;
        

        if (p_string != null &&
            p_string.length() == L_LENGTH_FULL_DATE_TIME)
        {
            l_tmp.append(p_string.substring(l_startIndex, l_endIndex));
            l_tmp.append( " ");
            l_startIndex = l_endIndex;
            l_endIndex   += L_LENGTH_MINUTES_AND_SECONDS;
            l_tmp.append(p_string.substring(l_startIndex, l_endIndex));
            l_tmp.append( ":");
            l_startIndex = l_endIndex;
            l_endIndex   += L_LENGTH_MINUTES_AND_SECONDS;
            l_tmp.append(p_string.substring(l_startIndex, l_endIndex));
            l_tmp.append( ":");
            l_startIndex = l_endIndex;
            l_tmp.append(p_string.substring(l_startIndex));
            l_returnValue = l_tmp.toString();
        }
        else
        {
            l_returnValue = p_string;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 11500, this,
                            "convertDateString -- l_returnValue: '" + l_returnValue + "'.");
        }
        return l_returnValue;
    } // End - convertDateString(.)

    
    
    
    
    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_validateMandatoryParameter = "validateMandatoryParameter";
    /**
     * Checks if parameter is in the map and has the correct pattern. If not, throws
     * an exception.
     * @param p_key Key of parameter to map.
     * @param p_pattern Pattern parameter must adhere too.
     * @param p_parameters Map of input parameters.
     * @throws BatchInvalidParameterException Parameter is missing or invalid.
     */
    protected void validateMandatoryParameter(
        String                        p_key,
        String                        p_pattern,
        Map                           p_parameters)
        throws BatchInvalidParameterException
    {
        String                         l_value = null;
        boolean                        l_valid = false;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11500, this,
                "Entered " + C_METHOD_validateMandatoryParameter);
        }

        l_value = (String)p_parameters.get(p_key);
        if ( l_value != null )
        {
            l_valid = l_value.matches(p_pattern);
        }

        if ( !l_valid )
        {
            BatchInvalidParameterException l_batchInvalidParameterException =
                new BatchInvalidParameterException(
                    C_CLASS_NAME,
                    C_METHOD_validateMandatoryParameter,
                    20010,
                    this,
                    null,
                    0,
                    BatchKey.get().batchInvalidParameter(p_key));

            throw l_batchInvalidParameterException;
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11590, this,
                "Leaving " + C_METHOD_validateMandatoryParameter);
        }

    } // end of validateMandatoryParameter

    /**  
     * This method is declared as 'abstract' in order to be implemented in the sub class.
     * It updates the database with a new status when a batch have got the status finished (c), 
     * stopped (x) or error (f).
     * The method should call the appropriate service defined by <code>PpasBatchControlService</code>.  
     * For a database driven batch sub-process <code>updateSubJobStatus(...)</code> should be called and 
     * for other standalone batch processes <code>updateJobDetails(...)</code> should be called. 
     * The <code>BatchJobData</code>/<code>BatchSubJobData</code> object used in these methods are  
     * first fetched form <code>getKeyedControlRecord</code> and then the status is inserted into that object.
     *
     * @param p_status The new status.
     */    
    protected abstract void updateStatus(String p_status);
    

    /**
     * This method should only be implemented by file-driven batch processes (standalone).  
     * It is responsible to insert a record into table <code>BACO_BATCH_CONTROL</code> using the  
     * defined service method addControlInformation on PpasBatchControlService.  The method will get a 
     * <code>BatchJobData</code> object from getKeyedControlRecord() and populate it with required 
     * parameters.
     * 
     * @throws BatchServiceException  if it fails to insert a new record into the database.
     */
    protected void addControlInformation() throws BatchServiceException
    {
        // Nothing to do, should be implemented by the sub-class.
        return;
    }


    /**
     * This method is a hook method to be used by sub-classes of this class to implement their own
     * specific pre-condition checks.
     * It should return <code>true</code> if the pre-conditions are fulfilled, otherwise
     * <code>false</code> to prevent any further processing.
     * By default it returns <code>true</code>.
     * 
     * @return <code>true</code> if the pre-conditions are fulfilled, otherwise <code>false</code>.
     */
    protected boolean verifyPreConditions()
    {
        return true;
    }


    /**
     * Monitor the message queue and immeiatley carry out requests.
     */
    private final class Manager extends ThreadObject
    {
        /** Reference to the <code>BatchReader</code>.*/
        private BatchReader i_reader        = null;

        /** The readers status. */
        private int i_readerStatus          = BatchMessage.C_STATUS_NONE;

        /** Reference to the <code>BatchWriter</code>.*/
        private BatchWriter i_writer        = null;

        /** The writers status. */
        private int i_writerStatus          = BatchMessage.C_STATUS_NONE;

        /** Reference to the <code>BatchProcessor</code>.*/
        private BatchProcessor i_processor  = null;

        /** The processors status. */
        private int i_processorStatus       = BatchMessage.C_STATUS_NONE;

        /**
         * Constructs a new <code>Manager</code> object.
         */
        private Manager()
        {
            super();
        }

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";

        /**
         * The main processing method.
         */
        public void doRun()
        {
            BatchMessage l_message = null;
            PpasConfigException l_confE = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                                C_CLASS_NAME, 11410, this,
                                BatchConstants.C_ENTERING + C_METHOD_doRun);

            }

            try
            {
                addControlInformation();
            }
            catch (BatchServiceException l_Ex)
            {
                // ***FATAL ERROR: Not possible to update the database, abort process!
                BatchController.super.stopDone();
                return;
            }

            boolean l_keepRunning = verifyPreConditions();
            if (!l_keepRunning)
            {
                BatchController.this.updateStatus(BatchConstants.C_BATCH_STATUS_FAILED);
                BatchController.super.stopDone();
            }
            while (l_keepRunning)
            {
                synchronized (BatchController.this.i_messageQueue)
                {
                    //Check if there are any messages in the queue
                    if (BatchController.this.i_messageQueue.size() == 0)
                    {
                        try
                        {
                            //Wait until there is a new message in the queue
                            BatchController.this.i_messageQueue.wait();
                        }
                        catch (InterruptedException p_intEx)
                        {
                            // No specific handling.
                            p_intEx = null;
                        }
                    }
                    //Get the first message from the queue and remove it from the queue
                    l_message = (BatchMessage)BatchController.this.i_messageQueue.remove(0);
                }

                switch (l_message.getRequest())
                {
                    case BatchMessage.C_REQUEST_START :
                        try
                        {
//                            BatchController.this.addControlInformation();
                            if (!i_startComponents)
                            {
                                l_keepRunning = false;
                                continue;
                            }
                            i_inQueue = new SizedQueue("In queue", i_inQueueMaxSize, i_instrumentManager);

                            i_reader = BatchController.this.createReader();
                            if (i_reader != null)
                            {
                                i_reader.start();
                            }
                        }
                        catch (SizedQueueInvalidParameterException e2)
                        {
                            //First inform client that an error has occured
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                            l_message.setStatus(BatchMessage.C_STATUS_ERROR);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);

                            //Secondly stop all other components
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STOP);
                            l_message.setStatus(BatchMessage.C_STATUS_NONE);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);

                            l_confE = new PpasConfigException(C_CLASS_NAME,
                                                              C_METHOD_init,
                                                              20050,
                                                              this,
                                                              null,
                                                              0,
                                                              ConfigKey.get().badConfigValue(
                                                                  BatchConstants.C_IN_QUEUE_MAX_SIZE,
                                                                  "" + i_inQueueMaxSize,
                                                                  ">0"),
                                                              e2);

                            i_logger.logMessage(l_confE);

                            break;
                        }
                        try
                        {
                            i_outQueue = new SizedQueue("Out queue", i_outQueueMaxSize, i_instrumentManager);

                            i_writer = BatchController.this.createWriter();
                            if (i_writer != null)
                            {
                                i_writer.start();
                            }

                            i_processor = BatchController.this.createProcessor();
                            if (i_processor != null)
                            {
                                i_processor.start();
                            }
                            break;
                        }
                        catch (SizedQueueInvalidParameterException e2)
                        {
                            //First inform client that an error has occured
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                            l_message.setStatus(BatchMessage.C_STATUS_ERROR);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);

                            //Secondly stop all other components
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STOP);
                            l_message.setStatus(BatchMessage.C_STATUS_NONE);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);

                            l_confE = new PpasConfigException(C_CLASS_NAME,
                                                              C_METHOD_init,
                                                              20050,
                                                              this,
                                                              null,
                                                              0,
                                                              ConfigKey.get().badConfigValue(
                                                                  BatchConstants.C_OUT_QUEUE_MAX_SIZE,
                                                                  "" + i_outQueueMaxSize,
                                                                  ">0"),
                                                              e2);

                            i_logger.logMessage(l_confE);

                            break;
                        }
                        catch (IOException e)
                        {
                            //First inform client that an error has occured
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                            l_message.setStatus(BatchMessage.C_STATUS_ERROR);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);

                            //Secondly stop all other components
                            l_message = new BatchMessage();
                            l_message.setRequest(BatchMessage.C_REQUEST_STOP);
                            l_message.setStatus(BatchMessage.C_STATUS_NONE);
                            l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                            l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                            postMessage(l_message);
                            break;
                        }

                    case BatchMessage.C_REQUEST_STOP :
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10230,
                                this,
                                C_METHOD_doRun + ": case C_REQUEST_STOP. " +
                                "target = " + l_message.getTargetComponent() +
                                ",  sender: " + l_message.getSendingComponent());
                        }
                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_WRITER
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_writer != null)
                            {
                                i_writer.stop();
                                // If the writer already has finished it couldn't report that it has been
                                // stopped.
                                if (i_writerStatus  == BatchMessage.C_STATUS_FINISHED)
                                {
                                    i_writerStatus = BatchMessage.C_STATUS_STOPPED;
                                }
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_PROCESSOR
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_processor != null)
                            {
                                i_processor.stop();
                                // If the processor already has finished it couldn't report that it has been
                                // stopped.
                                if (i_processorStatus  == BatchMessage.C_STATUS_FINISHED)
                                {
                                    i_processorStatus = BatchMessage.C_STATUS_STOPPED;
                                }
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_READER
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_reader != null)
                            {
                                i_reader.stop();
                                // If the reader already has finished it couldn't report that it has been
                                // stopped.
                                if (i_readerStatus  == BatchMessage.C_STATUS_FINISHED)
                                {
                                    i_readerStatus = BatchMessage.C_STATUS_STOPPED;
                                }
                            }
                        }

                        break;

                    case BatchMessage.C_REQUEST_PAUSE :
                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_WRITER
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_writer != null)
                            {
                                i_writer.pause();
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_PROCESSOR
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_processor != null)
                            {
                                i_processor.pause();
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_READER
                            || l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_reader != null)
                            {
                                i_reader.pause();
                            }
                        }

                        break;

                    case BatchMessage.C_REQUEST_RESUME :
                        // To make sure that the batch starts correctly after a pause (for instance that the
                        // input queue is re-opened before the processor is resumed, and that the output queue
                        // is re-opened before the writer is resumed), the resume order should be:
                        // first the reader, secondly the processor and at last the writer.
                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_READER ||
                            l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_reader != null)
                            {
                                i_reader.resume();
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_PROCESSOR ||
                            l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_processor != null)
                            {
                                i_processor.resume();
                            }
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_WRITER ||
                            l_message.getTargetComponent() == BatchMessage.C_COMPONENT_ALL)
                        {
                            if (i_writer != null)
                            {
                                i_writer.resume();
                            }
                        }

                        break;

                    case BatchMessage.C_REQUEST_STATUS :
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10230,
                                this,
                                C_METHOD_doRun + ": case C_REQUEST_STATUS. status = " + l_message.getStatus()
                                                + ",  sender: " + l_message.getSendingComponent());
                        }

                        if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_CLIENT)
                        {
                            if (l_message.getStatus() == BatchMessage.C_STATUS_STARTED)
                            {
                                BatchController.super.startDone();
                            }
                            
                            if (l_message.getStatus() == BatchMessage.C_STATUS_PAUSED)
                            {
                                BatchController.super.pauseDone();
                            }
                            
                            if (l_message.getStatus() == BatchMessage.C_STATUS_RESUMED)
                            {
                                BatchController.super.resumeDone();
                            }
                            
                            if (l_message.getStatus() == BatchMessage.C_STATUS_STOPPED)
                            {
                                BatchController.this.updateStatus("X");
                                BatchController.super.stopDone();
                                l_keepRunning = false;
                            }
                            
                            if (l_message.getStatus() == BatchMessage.C_STATUS_FINISHED)
                            {
                                BatchController.this.updateStatus("C");
                                BatchController.super.finishDone(JobStatus.C_JOB_EXIT_STATUS_SUCCESS);
                                l_keepRunning = false;
                            }
                            
                            if (l_message.getStatus() == BatchMessage.C_STATUS_ERROR)
                            {
                                BatchController.this.updateStatus("F");                               
                                BatchController.super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
                                l_keepRunning = false;
                            }
                        }
                        else
                            if (l_message.getTargetComponent() == BatchMessage.C_COMPONENT_MANAGER)
                            {
                                if (l_message.getSendingComponent() == BatchMessage.C_COMPONENT_READER)
                                {
                                    i_readerStatus = l_message.getStatus();
                                }

                                if (l_message.getSendingComponent() == BatchMessage.C_COMPONENT_WRITER)
                                {
                                    i_writerStatus = l_message.getStatus();
                                }

                                if (l_message.getSendingComponent() == BatchMessage.C_COMPONENT_PROCESSOR)
                                {
                                    i_processorStatus = l_message.getStatus();
                                }

                                analyseComponents();
                            }
                        break;

                    default :
                        break;
                } //End switch                                 
            } //End while 

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME,
                    11420,
                    this,
                    "Leaving " + C_METHOD_doRun);

            }
        }

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_analyseComponents = "analyseComponents";

        /**
         * Analyse all components staus after a component has changed status.
         * Dependig on the overall status of the components the controller will
         * make decisions upon to act.
         */
        private void analyseComponents()
        {
            BatchMessage l_message = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_START,
                    C_CLASS_NAME,
                    11510,
                    this,
                    "Enter " + C_METHOD_analyseComponents);

            }

            //Check if ALL components has reported started
            if (i_readerStatus == BatchMessage.C_STATUS_STARTED
                && i_writerStatus == BatchMessage.C_STATUS_STARTED
                && i_processorStatus == BatchMessage.C_STATUS_STARTED)
            {
                //Create a new message and set it's attributes
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_STARTED);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            //Check if ALL components has reported stopped
            if (i_readerStatus == BatchMessage.C_STATUS_STOPPED
                && i_writerStatus == BatchMessage.C_STATUS_STOPPED
                && i_processorStatus == BatchMessage.C_STATUS_STOPPED)
            {
                //Create a new message and set it's attributes
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_STOPPED);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            //Check if ALL components has reported paused
            if (i_readerStatus == BatchMessage.C_STATUS_PAUSED
                && i_writerStatus == BatchMessage.C_STATUS_PAUSED
                && i_processorStatus == BatchMessage.C_STATUS_PAUSED)
            {
                //Create a new message and set it's attributes
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_PAUSED);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            //Check if ALL components has reported resumed            
            if (i_readerStatus == BatchMessage.C_STATUS_RESUMED
                && i_writerStatus == BatchMessage.C_STATUS_RESUMED
                && i_processorStatus == BatchMessage.C_STATUS_RESUMED)
            {
                //Create a new message and set it's attributes
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_RESUMED);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            //Check if ALL components has reported finished            
            if (i_readerStatus == BatchMessage.C_STATUS_FINISHED
                && i_writerStatus == BatchMessage.C_STATUS_FINISHED
                && i_processorStatus == BatchMessage.C_STATUS_FINISHED)
            {
                //Create a new message and set it's attributes
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_FINISHED);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            //Check ir ANY component has reported an error
            if (i_readerStatus == BatchMessage.C_STATUS_ERROR
                || i_writerStatus == BatchMessage.C_STATUS_ERROR
                || i_processorStatus == BatchMessage.C_STATUS_ERROR)
            {

                // 1. Stop all components (besides the controller).
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STOP);
                l_message.setStatus(BatchMessage.C_STATUS_NONE);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_ALL);

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        11520,
                        this,
                        C_METHOD_analyseComponents + " -- posting stop all message: " + l_message.toString());
                }
                postMessage(l_message);

                // 2. Handle the error.
                l_message = new BatchMessage();
                l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_message.setStatus(BatchMessage.C_STATUS_ERROR);
                l_message.setSendingComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_message.setTargetComponent(BatchMessage.C_COMPONENT_CLIENT);

                postMessage(l_message);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME,
                    11520,
                    this,
                    "Leaving " + C_METHOD_analyseComponents);
            }
        }

        /**
         * Returns the name to use for the thread name of the thread this object
         * is running in.
         * 
         * @return the unique thread name of the thread running in this object.
         */
        protected String getThreadName()
        {
            return "batContThd";
        }
    }
}
