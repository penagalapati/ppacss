////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME        : SubSegmentationChgBatchReaderUT.java
//      DATE             : 04 June 2007
//      AUTHOR           : Ian James
//      REFERENCE        : PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT        : ATOS ORIGIN 2004
//
//      DESCRIPTION      : Unit test of SubSegmentationChgBatchReader.
//
////////////////////////////////////////////////////////////////////////////////
//                      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME         | DESCRIPTION                      | REFERENCE
//----------+--------------+----------------------------------+--------------------
// DD/MM/YY | <name>       | <brief description of change>    | <reference>
//----------+--------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.RefillBatchController;
import com.slb.sema.ppas.batch.batchcontroller.SubSegmentationChgBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.CommentKey;
import com.slb.sema.ppas.common.dataclass.EndOfCallNotificationId;
import com.slb.sema.ppas.common.dataclass.InstallRequestData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.dataclass.SubSegChgBatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubscriberLifecycleSnapshotData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasParseException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasSnapshotService;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.MeterInstrument;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;


/** Unit test of SCChangeBatchReader. */
public class SubSegmentationChgBatchReaderUT extends BatchTestCaseTT
{

    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------

    /** Class name. Value is {@value}. */
    private static final String            C_CLASS_NAME                   = "SubSegmentationChgBatchReaderUT";

    /** The name of the indata file used in normal mode. */
    private static final String            C_INPUT_DATA_FILENAME          = "BATCH_CHG_SUB_SEG_20070703_00001.DAT";

    /** The name of the indata file used in recovery mode. */
    private static final String            C_INPUT_DATA_FILENAME_RECOVERY = "BATCH_CHG_SUB_SEG_20070703_00002.IPG";

    /** The name of the in-queue. */
    private static final String            C_QUEUE_NAME                   = "In-queue";

    /** The maximum size of the queue. */
    private static final long              C_MAX_QUEUE_SIZE               = 100;

    /** The record line in a input data file. */
    private static String C_VALID_RECORD_WITH_OLD_ACC_GRP; 

    /** The record line in a input data file. */
    private static String C_VALID_RECORD_WITH_OLD_SERV_OFF; 

    /** The record line in a input data file. */
    private static String C_VALID_RECORD_WITH_GRP_SERV;

    /** The record line in a input data file. */
    private static String C_INVALID_RECORD_WITH_OLD_GROUP;
    
    /** The record line in a input data file. */
    private static String C_INVALID_RECORD_WITH_OLD_SERV_OFF;
    
    /** The js job id. */
    private static final String             C_JS_JOB_ID           = "10001";
    
    /** A reference to the <code>SubSegmentationChgBatchController</code> that is used in this test. */
    private static SubSegmentationChgBatchController c_subSegChgBatchController  = null;
    
    /** The BasicAccountData that is created for this test. */
    private static BasicAccountData        c_basic                        = null;

    /** The old msisdn. */
    private static String                  c_msisdnAsString               = null;
    
    /** Reference to instance of BatchController. */
    BatchController                        i_controller                   = null;

    /** Reference to instance of hashtable with in-parameters. */
    Hashtable                              i_parameters                   = null;

    /** Reference to instance of PpasProperties. */
    PpasProperties                         i_properties                   = null;

    /** Reference to instance of in-queue. */
    SizedQueue                             i_queue                        = null;   

    /** Refence to instance of Subscriber install record. */
    BatchReader                            i_record                       = null; 

    /** Reference to instance of BatchReader. */
      SubSegmentationChgBatchReader   i_subSegChgBatchReader              = null;

    /** The service to be tested. */
    private PpasSnapshotService i_snapshotService;

    /**
     * Class constructor.
     * @param p_name Test name.
     */
    public SubSegmentationChgBatchReaderUT(String p_name)
    {
        super( p_name ); 
        
        i_snapshotService = new PpasSnapshotService(c_ppasRequest, c_logger, c_ppasContext);
    }

    /**
     * Sets up before test execution.
     */
    protected void setUp()
    {
        String       l_msisdnAsStr     = null;
        String       l_tempString      = null;
        SubscriberLifecycleSnapshotData l_snapshot = null;        

        super.setUp();

        boolean l_snapshotEnabled = setFeatureLicence(FeatureLicence.C_FEATURE_CODE_SUBSCRIBER_LIFECYCLE_SNAPSHOT, true);

        if (c_basic == null)
        {
            c_basic = createTestSubscriber();
            snooze(5); 
        
            l_msisdnAsStr = c_ppasContext.getMsisdnFormatDb().format(c_basic.getMsisdn());
            say("l_msisdnAsString: " + l_msisdnAsStr);
        
            // Msisdn String to be right-adjusted, space-filled to be of length 15  
            l_tempString = "               " + l_msisdnAsStr;
        
            c_msisdnAsString = l_tempString.substring(l_tempString.length() - 15);
        
            say("c_msisdnAsString is: '" + c_msisdnAsString + "'");
        }

        c_ppasRequest = createRequestWithMsisdn(c_basic.getMsisdn());

        try
        {
            l_snapshot = i_snapshotService.readSnapshot(c_ppasRequest, C_TIMEOUT);
        }
        catch (PpasServiceException e)
        {
            failedTestException(e);
        }
        finally
        {
            setFeatureLicence(FeatureLicence.C_FEATURE_CODE_SUBSCRIBER_LIFECYCLE_SNAPSHOT, l_snapshotEnabled);
        }

        assertNotNull("Snapshot object should not be null", l_snapshot);

        say("Group ID of account snapshot is: '" + l_snapshot.getAccountGroupId() + "'");
      
        say("Service Offering of account snapshot is: '" + l_snapshot.getServiceOfferings() + "'");

        C_VALID_RECORD_WITH_OLD_ACC_GRP    = c_msisdnAsString + "         1         2                    ";
        C_VALID_RECORD_WITH_OLD_SERV_OFF   = c_msisdnAsString + "                          1234      4321";
        C_VALID_RECORD_WITH_GRP_SERV       = c_msisdnAsString + "         1        25      1234    654321";
        C_INVALID_RECORD_WITH_OLD_GROUP    = c_msisdnAsString + "         2        25      4321    654321";
        C_INVALID_RECORD_WITH_OLD_SERV_OFF = c_msisdnAsString + "         1        25      4321    654321";
    }

    /** 
     * Tidies up after test execution.
     */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    
    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(SubSegmentationChgBatchReaderUT.class);
    }

    /**
     * Test method for running Subscriber Segmentation Change from an input file.
     */
    public final void testBatchFromInputFile()
    {
        beginOfTest("Test Subscriber Segmentation Change From Input File");
                
        doTestStart(C_INPUT_DATA_FILENAME);

        endOfTest();
    }
    
//    /**
//     * Test method for running a test in recovery mode.
//     */
//    public final void testRecoveryBatchMode()
//    {
//        System.out.println("******* RECOVERY BATCH MODE ******");
//        System.out.println("================================\n");
//
//        doTestStart(C_INPUT_DATA_FILENAME_RECOVERY);
//        
//        System.out.println("\n******* END OF RECOVERY BATCH MODE TEST******");
//        System.out.println("=============================================\n");
//
//    }

    public void testValidOldAndNewGroupId()
    {
        beginOfTest("testOldAndNewGroupId");
        String[] l_inputLines = {C_VALID_RECORD_WITH_OLD_ACC_GRP};

        BatchRecordData l_recordData = null;
        createContext(C_INPUT_DATA_FILENAME, l_inputLines);

        l_recordData = i_subSegChgBatchReader.getRecord();

        assertNotNull("Old Group ID record should not be null", l_recordData);
        say("Record: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid Old Group ID record!",
                     C_VALID_RECORD_WITH_OLD_ACC_GRP,
                     l_recordData.getInputLine());

        endOfTest();
    }

    public void testValidOldAndNewServiceOfferings()
    {
        beginOfTest("testValidOldAndNewServiceOfferings");
        String[] l_inputLines = {C_VALID_RECORD_WITH_OLD_SERV_OFF};

        BatchRecordData l_recordData = null;
        createContext(C_INPUT_DATA_FILENAME, l_inputLines);

        l_recordData = i_subSegChgBatchReader.getRecord();

        assertNotNull("Old Service Offerings record should not be null", l_recordData);
        say("Record: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid Old Service Offerings record!",
                     C_VALID_RECORD_WITH_OLD_SERV_OFF,
                     l_recordData.getInputLine());

        endOfTest();
    }

    public void testInvalidOldGroupId()
    {
        beginOfTest("testInvalidGroupId");
        String[] l_inputLines = {C_INVALID_RECORD_WITH_OLD_GROUP};

        BatchRecordData l_recordData = null;
        createContext(C_INPUT_DATA_FILENAME, l_inputLines);

        l_recordData = i_subSegChgBatchReader.getRecord();

        assertNotNull("Old Group ID record should not be null", l_recordData);
        say("Record: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a invalid Group ID record!",
                     C_INVALID_RECORD_WITH_OLD_GROUP,
                     l_recordData.getInputLine());

        endOfTest();
    }

    public void testInvalidOldServiceOfferings()
    {
        beginOfTest("testInvalidServiceOfferings");
        String[] l_inputLines = {C_INVALID_RECORD_WITH_OLD_SERV_OFF};

        BatchRecordData l_recordData = null;
        createContext(C_INPUT_DATA_FILENAME, l_inputLines);

        l_recordData = i_subSegChgBatchReader.getRecord();

        assertNotNull("Old Service Offerings record should not be null", l_recordData);
        say("Record: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a invalid Service Offerings record!",
                     C_INVALID_RECORD_WITH_OLD_SERV_OFF,
                     l_recordData.getInputLine());

        endOfTest();
    }

    public void testValidGroupIdServiceOfferings()
    {
        beginOfTest("testValidGroupIdServiceOfferings");
        String[] l_inputLines = {C_VALID_RECORD_WITH_GRP_SERV};

        BatchRecordData l_recordData = null;
        createContext(C_INPUT_DATA_FILENAME, l_inputLines);

        l_recordData = i_subSegChgBatchReader.getRecord();

        assertNotNull("Old Service Offerings record should not be null", l_recordData);
        say("Record: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a invalid Service Offerings record!",
                     C_VALID_RECORD_WITH_GRP_SERV,
                     l_recordData.getInputLine());

        endOfTest();
    }

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testStart = "testStart";
    /**
     * Methode that will start the batchReader.
     * @param p_fileName indata file name
     */
    private void doTestStart(String p_fileName)
    {
        InstrumentSet            l_instrumentSet  = null;
        CounterInstrument        l_queuedCounter  = null;
        CounterInstrument        l_removedCounter = null;
        MeterInstrument          l_sizeMeter      = null;
        SubSegChgBatchRecordData l_record         = null;
        int                      l_recordCounter  = 0;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10400, this, BatchConstants.C_ENTERING + C_METHOD_testStart );
        }

        // Create variables needed to create a SubSegmentationChgBatchReader
        this.createContext(p_fileName, null);       
                                       
       // assertNotNull("Failed to create a SubSegmentationChgBatchProcessor instance.", i_subSegChgBatchReader);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10400, this, "OK - Just created an SubSegmentationChgBatchReader" );
        }

        //i_record.start();
        i_subSegChgBatchReader.start();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10400, this, "After start() ...." );
        }

        try
        {
            System.out.println(C_CLASS_NAME + C_METHOD_testStart + "-- Wait for " +
                    + BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT + " secs...");
            Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
        }
        catch (InterruptedException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10400, this, "Something wrong in waitin.. " );
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10400, this, "After wait... - before getInstrument" );
        }

        l_instrumentSet = i_queue.getInstrumentSet();
        assertNotNull("Failed to get the InstrumentSet from the SizedQueue.", l_instrumentSet);

        l_queuedCounter  = (CounterInstrument)l_instrumentSet.getInstrument("Queued counter");

        l_removedCounter = (CounterInstrument)l_instrumentSet.getInstrument("Removed counter");

        l_sizeMeter      = (MeterInstrument)l_instrumentSet.getInstrument("Queue size");
        
        if ( l_queuedCounter != null )
        {
            assertEquals("Wrong number of elements added to the outdata queue (SizedQueue)."    ,
                         4, l_queuedCounter.getValue());
        }
        if ( l_removedCounter != null )
        {
            assertEquals("Wrong number of elements removed from the outdata queue (SizedQueue).",
                         0, l_removedCounter.getValue());
        }
        if ( l_sizeMeter != null )
        {
            assertEquals("Wrong size of the outdata queue (SizedQueue)."                        ,
                         4, l_sizeMeter.getValue());
        }

        // Loop through the queue and write the records from the queue  
        while ( !i_queue.isEmpty() ) 
        {  
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME, 10400, this, "Something in the queue..." );
            }        
            try
            {
                l_recordCounter++;
                l_record = (SubSegChgBatchRecordData)i_queue.removeFirst();
                System.out.println( "Record number : " + l_recordCounter + "\n" + l_record.dumpRecord() );
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10400, this, "record number: "+l_recordCounter + l_record.dumpRecord() );
                }        
                 
            }
            catch (SizedQueueClosedForWritingException e1)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10400, this, "Could not get record number: "+l_recordCounter );
                }        
                e1.printStackTrace();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10400, this, BatchConstants.C_LEAVING + C_METHOD_testStart );
        }
    }

    /**
     * Create test context.
     * @param p_fileName Input filename.
     * @param p_linearray An array with the lines that will be in the file <code>p_fileName</code>.
     */
    private void createContext(String p_fileName, String[] p_linearray)
    {
        StringBuffer l_tmpFullPathName = null;
        String l_inputFileDirectory = null;
        String l_fullPathName = null;
        String l_fileName = null;
        
        String[] l_fileArray = p_fileName.split("[.]");

        l_fileName = l_fileArray[0] + ".IPG";

        l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

        // Delete the recovery file (.IPG) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(l_fileName);
        l_fullPathName = l_tmpFullPathName.toString();
        super.deleteFile(l_fullPathName);

        // Create a input file (.DAT) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(p_fileName);
        l_fullPathName = l_tmpFullPathName.toString();

        if (p_linearray != null)
        {
            super.createNewFile(l_fullPathName, p_linearray);    
        }

        try
        {
            i_parameters = new Hashtable();
            i_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, p_fileName );

            i_properties = new PpasProperties((InstrumentManager) null);
            i_properties.loadLayeredProperties();

            try
            {
                i_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null );
            }
            catch (SizedQueueInvalidParameterException e1)
            {
                System.out.println("FAILED to create SizedQUEUE");
                e1.printStackTrace();
            }
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }

        try
        {
            c_subSegChgBatchController = new SubSegmentationChgBatchController(super.c_ppasContext,
                                                                               "BatchSubSegmentation",
                                                                               super.getJsJobID(),
                                                                               "SubSegmentationChgBatchReaderUT",
                                                                               i_parameters);
        }
        catch (PpasException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        i_subSegChgBatchReader = new SubSegmentationChgBatchReader(super.c_ppasContext,
                                                                   super.c_logger,
                                                                   c_subSegChgBatchController,
                                                                   i_queue,
                                                                   i_parameters,
                                                                   super.c_properties);
    }

    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + (p_args.length == 0 ? "" : "ignored"));
        TestRunner.run(suite());
    }
}

