////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchReaderUT.java
//      DATE            :       02-July-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       This class tests the classes MiscBatchReader
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.MiscBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.instrumentation.CounterInstrument;
import com.slb.sema.ppas.util.instrumentation.InstrumentManager;
import com.slb.sema.ppas.util.instrumentation.InstrumentSet;
import com.slb.sema.ppas.util.instrumentation.MeterInstrument;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * This class tests the classes MiscBatchReader.
 */
public class MiscBatchReaderUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** Class name. Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscBatchReaderUT";

//    * Check if filename is of the valid form:
//    *     UPDATE_MISC_yyyymmdd_sssss.DAT /IPG
//    *     UPDATE_MISC_xxzz_cccc_yyyymmdd_sssss.DAT /IPG

    /** Test file for normal batch mode. Value is {@value}. */
    private static final String C_UPDATE_MISC_FILENAME_NORMAL_MODE   = "UPDATE_MISC_20040702_00001.DAT";
//    /** Test file for recovery mode. Value is {@value}. */
//    private static final String C_UPDATE_MISC_FILENAME_RECOVERY_MODE = "UPDATE_MISC_20040702_00002.IPG";
    /** Name of in-queue. Value is {@value}. */
    private static final String C_QUEUE_NAME                    = "In-queue";
    /** Queue size. Value is {@value}. */
    private static final long   C_MAX_QUEUE_SIZE                = 100;
    
    /** Reference to the BatchController. */
    private BatchController     i_controller  = null;
    /** Reference to the start parameters. */
    private Hashtable           i_parameters  = null;
    /** Reference to the batch properties. */
    private PpasProperties      i_properties  = null;
    /** Reference to the in-queue. */
    private SizedQueue          i_queue       = null;
    /** Reference to the batchReader. */
    private BatchReader         i_batchReader = null;
 
    /**
     * Constructor. 
     * @param p_name Name of current UT test.
     */
    public MiscBatchReaderUT( String p_name )
    {
        super( p_name );
        //super.enableConsoleLogAll();
    }

    /**
     * @see com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT#setUp()
     */
    protected void setUp()
    {
        super.setUp(CommonTestCaseTT.C_FLAG_WITHOUT_JDBC_CONNECTION);
    }

    /**
     * @see com.slb.sema.ppas.common.support.CommonTestCaseTT#tearDown()
     */
    protected void tearDown()
    {
        super.tearDown();
    }
    
    
    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(MiscBatchReaderUT.class);    
    }

    /**
     * Test for a normal batch run.
     */
    public final void testNormalBatchMode()
    {
        System.out.println("******* NORMAL BATCH MODE ******");
        System.out.println("================================\n");
        
        doTestStart(C_UPDATE_MISC_FILENAME_NORMAL_MODE);
        
        System.out.println("\n******* END OF NORMAL BATCH MODE TEST******");
        System.out.println("===========================================\n");

    }
    
    
    
    /**
     * Test for a recovery batch run.
     */
    public final void testRecoveryBatchMode()
    {
        System.out.println("******* RECOVERY BATCH MODE ******");
        System.out.println("================================\n");

        //doTestStart(C_UPDATE_MISC_FILENAME_RECOVERY_MODE);
        
        System.out.println("\n******* END OF RECOVERY BATCH MODE TEST******");
        System.out.println("=============================================\n");

    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testStart = "testStart";
    /**
     * @param p_fileName Indata file name.
     */
    private void doTestStart(String p_fileName)
    {
        InstrumentSet          l_instrumentSet  = null;
        CounterInstrument      l_queuedCounter  = null;
        CounterInstrument      l_removedCounter = null;
        MeterInstrument        l_sizeMeter      = null;
        MiscBatchRecordData    l_record         = null;
        int                    l_recordCounter  = 0;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_testStart );
        }

        System.out.println("*** START OF " + C_METHOD_testStart + " ***");
      
        // Create variables needed to create a SubInstBatchReader
        this.createContext(p_fileName);       
        
        i_batchReader = new MiscBatchReader(
                                super.c_ppasContext,
                                super.c_logger,
                                i_controller,
                                i_queue,
                                i_parameters,
                                i_properties);
                                       
        assertNotNull("Failed to create a MiscBatchProcessor instance.", i_batchReader);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "OK - Just created an MiscBatchReader" );
        }

        i_batchReader.start();
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "After start() ...." );
        }

        try
        {
            System.out.println(C_CLASS_NAME + C_METHOD_testStart + "-- Wait for " +
                   + BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT + " secs...");
            Thread.sleep(BatchTestCaseTT.C_BATCH_SHORT_TIMEOUT);
        }
        catch (InterruptedException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10400,
                    this,
                    "Something wrong in waitin.. " );
            }
            
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10400,
                this,
                "After wait... - before getInstrument" );
        }

        l_instrumentSet = i_queue.getInstrumentSet();
        assertNotNull("Failed to get the InstrumentSet from the SizedQueue.", l_instrumentSet);

        l_queuedCounter  = (CounterInstrument)l_instrumentSet.getInstrument("Queued counter");
        l_removedCounter = (CounterInstrument)l_instrumentSet.getInstrument("Removed counter");
        l_sizeMeter      = (MeterInstrument)  l_instrumentSet.getInstrument("Queue size");
       
        if ( l_queuedCounter != null )
        {
            assertEquals("Wrong number of elements added to the outdata queue (SizedQueue).",
                         4, l_queuedCounter.getValue());
        }
        if ( l_removedCounter != null )
        {
            assertEquals("Wrong number of elements removed from the outdata queue (SizedQueue).",
                         0, l_removedCounter.getValue());
        }
        if ( l_sizeMeter != null )
        {
            assertEquals("Wrong size of the outdata queue (SizedQueue).",
                         4, l_sizeMeter.getValue());
        }

        // Loop through the queue and write the records from the queue  
        while ( !i_queue.isEmpty() ) 
        {  
            System.out.println("Something in the queue...");

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10400,
                    this,
                    "Something in the queue..." );
            }        
            try
            {
                l_recordCounter++;
                l_record = (MiscBatchRecordData)i_queue.removeFirst();
                System.out.println( "Record number : " + l_recordCounter + "\n" + l_record.dumpRecord() );
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        "record number: " + l_recordCounter + l_record.dumpRecord() );
                }        
                 
            }
            catch (SizedQueueClosedForWritingException e1)
            {
                // The queue is both empty and closed for writing.
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        "Could not get record number: " + l_recordCounter );
                }        
                e1.printStackTrace();
            }
            
        }

        
        System.out.println("*** END of method " + C_METHOD_testStart + " ***");
        
        System.out.println("Number of tests : " + this.countTestCases());
        
        System.gc();
        System.out.println(C_CLASS_NAME + C_METHOD_testStart + "-- Completed.");

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_testStart );
        }

        return;
    }


    /**
     * Create context for the test.
     * @param p_fileName Indata filename.
     */
    private void createContext(String p_fileName)
    {
        System.out.println("start createContext");

        try
        {
            i_parameters = new Hashtable();
            i_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, p_fileName );

            i_properties = new PpasProperties((InstrumentManager) null);
            i_properties.loadLayeredProperties();

            try
            {
                i_queue = new SizedQueue( C_QUEUE_NAME,
                                          C_MAX_QUEUE_SIZE,
                                          null );
            }
            catch (SizedQueueInvalidParameterException e1)
            {
                System.out.println("FAILED to create SizedQUEUE");
                e1.printStackTrace();
            }
        }
        catch (PpasException e)
        {
            super.failedTestException(e);
        }
        
        System.out.println("end createContext");

    }


    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        TestRunner.run(suite());
    }
}


