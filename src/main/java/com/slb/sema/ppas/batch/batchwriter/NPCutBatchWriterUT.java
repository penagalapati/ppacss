////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       NPCutBatchWriterUT.java 
//DATE            :       Aug 19, 2004
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       ATOS ORIGIN 2004
//
//DESCRIPTION     :      Unit test for NPCutBatchWriter.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of change>    | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchwriter;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.NPCutBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**Unit test for NPCutBatchWriter.*/

public class NPCutBatchWriterUT extends BatchTestCaseTT
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "NPCutBatchWriterUT";
    
    /** Reference to SubInstBatch controller object. */
    private static NPCutBatchWriter     c_npCutBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static NPCutBatchController c_npCutBatchController;

    /** An input file name vid extension .DAT. */
    private static final String         C_INPUT_FILE_NAME_DAT = "NUMBER_CUTOVER_20040615_00003.DAT";
    
    /** A file name vid extension .RPT. */
    private static final String         C_FILE_NAME_RPT       = "NUMBER_CUTOVER_20040615_00003.TMP";

    /** A file name vid extension .RCV. */
    private static final String         C_FILE_NAME_RCV       = "NUMBER_CUTOVER_20040615_00003.RCV";

    /** The js job id. */
    private static final String         C_JS_JOB_ID           = "10001";

    /** The file date. */
    private static final String         C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final String         C_SEQ_NO              = "0003";

    /** The service class. */
    private static final String         C_SERVICE_CLASS       = "0001";

    /** The service location. */
    private static final String         C_SERVICE_LOCATION    = "01";

    /** The sevice area. */
    private static final String         C_SERVICE_AREA        = "01";

    /** The sub job counter. */
    private static final String         C_SUB_JOB_COUNT       = "-1";

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService     i_controlService      = null;

    /**
     * The constructor for the class NPCutBatchWriterUT.
     * @param p_name Test name.
     */
    public NPCutBatchWriterUT(String p_name)
    {
        super(p_name, "batch_nco");
        createContext();
    }
    
    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if
     * the row <code>baco_failur</code> has been updated.
     * Set the value for error line in the object <code>SubInstBatchRecordData</code> to null and updates
     * the table <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()<code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */

    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        NPCutBatchRecordData l_dataRecord = new NPCutBatchRecordData();
 
        BatchJobDataSet l_batchjobSet   = null;
        BatchJobData l_batchData1       = new BatchJobData();
        BatchJobData l_batchData        = null;
        String l_expectedKey            = null;
        String l_actualKey              = null;
        String l_errorLine              = "2 ERR";
        String l_recoveryLine           = "1 REC";
        String l_outputLine             = "1 OUT";
        String l_executionDateTime      = null;
        String l_jsJobId                = null;
        Vector l_vec                    = null;
        String l_actualText             = null;
        /** The first part of An output file name vid extension .DAT. */
        String l_outputfile_name        = "NPLAN_SDP0001_";
  
        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);
            
            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_npCutBatchController.getKeyedControlRecord();
            
            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();
            l_batchData1.setBatchType(BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER);          
            l_batchData1.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setBatchDate(C_FILE_DATE);
            l_batchData1.setFileSeqNo(C_SEQ_NO);
            l_batchData1.setFileSubSeqNo(null);
            l_batchData1.setNoOfSuccessRec("0");
            l_batchData1.setNoOfRejectedRec("0");
            l_batchData1.setExtraData1(C_SERVICE_CLASS);
            l_batchData1.setExtraData2(C_SERVICE_AREA);
            l_batchData1.setExtraData3(C_SERVICE_LOCATION);
            l_batchData1.setOpId(c_npCutBatchController.getSubmitterOpid());
            
            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                l_dataRecord.setSdpId("0001");
                c_npCutBatchWriter.writeRecord(l_dataRecord);
                 
                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);
                
               //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "0";

                l_vec = l_batchjobSet.getDataSet();
                
                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime() 
                    + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();
 
                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
            
           //Test that the method has written to report file and recovery file.
            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RPT,
                                                     BatchConstants.C_EXTENSION_TMP_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            //Get the recovery text from the recovery logfile.
//            l_actualText = super.getValueFromLogFile(C_FILE_NAME_RCV,
//                                                     BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                                     BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//
//            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                        ("No file found".equals(l_actualText)));
//            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);
            
            
            //Test when there is no error line set.       
            try
            {
                l_dataRecord.setErrorLine(null);
                l_dataRecord.setOutputLine(l_outputLine);
                c_npCutBatchWriter.writeRecord(l_dataRecord);
 
                l_batchjobSet = i_controlService.getJobDetails(c_ppasRequest,
                                                               l_jsJobId,
                                                               l_executionDateTime,
                                                               C_TIMEOUT);

                l_vec = l_batchjobSet.getDataSet();

                l_batchData = (BatchJobData)l_vec.get(0);
                l_actualKey = l_batchData.getJsJobId() + l_batchData.getExecutionDateTime()
                        + l_batchData.getNoOfRejectedRec() + l_batchData.getNoOfSuccessRec();

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 1.
                l_expectedKey = l_jsJobId + l_executionDateTime + "1" + "1";

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
                
                l_outputfile_name = l_outputfile_name + getSequenceNumber(BatchConstants.C_NP_SEQUENCE_NAME)
                                    + ".TMP";
                
                //Get the output text from the output logfile.
                l_actualText = super.getValueFromLogFile(l_outputfile_name,
                                                         BatchConstants.C_EXTENSION_TMP_FILE,
                                                         BatchConstants.C_OUTPUT_FILE_DIRECTORY);

                assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_TMP_FILE,
                            ("No file found".equals(l_actualText)));
                assertEquals("Failure: Wrong data in the output file", l_outputLine, l_actualText);


            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
  

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(NPCutBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to run all the tests in this
     * class.
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** Perform standard activities at start of a test. */
    protected void setUp()
    {
        super.setUp();
    }
    
    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }

    
    /**
     * Create context neccessary for this test.
     */
    private void createContext()
    {
        NPCutBatchRecordData l_batchRecordData = new NPCutBatchRecordData();
        Hashtable  l_params         = new Hashtable();
        SizedQueue l_outQueue       = null;
        String     l_dirRpt         = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String     l_fileReportName = l_dirRpt + "/" + C_FILE_NAME_RPT; 
        String     l_dirRcv       = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String     l_fileRecoveryName = l_dirRcv + "/" + C_FILE_NAME_RCV;
//        String     l_dirOut       = c_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
//        String     l_fileOutPutName = l_dirOut + "/" + C_OUTPUT_FILE_NAME_DAT;
        
        //The update frequency needs to be 1 for the test to work.
        super.c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");
        
        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_batchRecordData);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        
        //Make sure that there are no files with the same name.
        super.deleteFile(l_fileReportName);
        super.deleteFile(l_fileRecoveryName);
//        super.deleteFile(l_fileOutPutName); 
        
        l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_INPUT_FILE_NAME_DAT);
                
        try
        {
            c_npCutBatchController = new NPCutBatchController(super.c_ppasContext,
                                                              BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER,
                                                              C_JS_JOB_ID,
                                                              "NPCutBatchWriterUT",
                                                              l_params);

            c_npCutBatchWriter = new NPCutBatchWriter(super.c_ppasContext,
                                                       super.c_logger,
                                                       c_npCutBatchController,
                                                       l_params,
                                                       l_outQueue,
                                                       super.c_properties);

        }
        catch (PpasConfigException e)
        {
            super.failedTestException(e);
        }
    }
    /** Name of method. Used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getSequence = "getSequence";
    
    /**
     * Gets the sequence number from table <code>NASE_NAMED_SEQUENCE</code>.
     * @param p_sequenceName The fetched sequence number.
     * @return The fetched sequence number.
     */
    private String getSequenceNumber(String p_sequenceName)
    {
        JdbcStatement   l_statement = null;
        JdbcResultSet   l_resultSet = null;
        SqlString       l_sql       = null;
        long            l_seqNo     = 0;
        long            l_maxSeqNo  = 0;
        String          l_returnSeqNo = null;
        String          l_maxSeqNoStr = null;
        
        l_sql = new SqlString(100, 
                              1, 
                              "SELECT NASE_SEQ_NO, NASE_SEQ_MAX_NO from NASE_NAMED_SEQUENCE" +
                              " where NASE_SEQ_NAME = {0}");

        l_sql.setStringParam(0, p_sequenceName);

        try
        {
            l_statement = super.i_connection.createStatement(C_CLASS_NAME,
                                                             C_METHOD_getSequence,
                                                             10220,
                                                             this,
                                                             super.c_ppasRequest);

            l_resultSet = l_statement.executeQuery(C_CLASS_NAME,
                                                   C_METHOD_getSequence,
                                                   10230,
                                                   this,
                                                   super.c_ppasRequest,
                                                   l_sql);

            if (l_resultSet.next(10240))
            {
                l_seqNo = l_resultSet.getLong(10241, "NASE_SEQ_NO");
                l_maxSeqNo = l_resultSet.getLong(10242, "NASE_SEQ_MAX_NO");
            }
            
            
            l_returnSeqNo = new Long(l_seqNo).toString();
            l_maxSeqNoStr = new Long(l_maxSeqNo).toString();       

            while (l_returnSeqNo.length() < l_maxSeqNoStr.length())
            {
                l_returnSeqNo = "0" + l_returnSeqNo;
            }
        
        }
        catch (PpasSqlException e)
        {
            super.failedTestException(e);
        }
        finally
        {
            try
            {
                if (l_resultSet != null)
                {
                    l_resultSet.close(10452);
                }

                if (l_statement != null)
                {
                    l_statement.close(C_CLASS_NAME, C_METHOD_getSequence, 10250, this, super.c_ppasRequest);

                }
            }
            catch (PpasSqlException e)
            {
                super.failedTestException(e);
            }
        }
        return l_returnSeqNo;
    }
}

