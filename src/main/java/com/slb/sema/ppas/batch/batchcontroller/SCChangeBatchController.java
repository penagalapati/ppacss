// //////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME : SCChangeBatchController.java
//      DATE : Jun 23, 2004
//      AUTHOR : Urban Wigstrom
//      REFERENCE : PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT : WM-data 2007
//
//      DESCRIPTION : 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE | NAME | DESCRIPTION | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name> | <brief description of | <reference>
//          | | change> |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.SCChangeBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.SCChangeBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.SCChangeBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;

/** The entry point for "Batch Change Service Class". */
public class SCChangeBatchController extends BatchController
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME          = "SCChangeBatchController";

    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_scc";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME    = "Invalid filename";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";

    /**
     * Constructs a SCChangeBatchController.
     * @param p_context A <code>PpasContext</code> object.
     * @param p_jobType The type of job (e.g. BatchInstall).
     * @param p_jobId The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params Holding parameters to use when executing this job.
     * @throws PpasException - If configuration data is missing or incomplete.
     * @throws IOException - If problem locating or loading properties
     */
    public SCChangeBatchController(PpasContext p_context,
            String p_jobType,
            String p_jobId,
            String p_jobRunnerName,
            Map p_params) throws PpasException, IOException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10002,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            82601,
                            this,
                            "Enter " + C_METHOD_init);
        }

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            82602,
                            this,
                            "Leaving " + C_METHOD_init);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";

    /**
     * Creates a <code>SCChangeBatchReader</code>.
     * @return The created SCChangeBatchReader.
     */
    public BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Enter " + C_METHOD_createReader);
        }

        SCChangeBatchReader l_reader = null;

        if (super.i_startComponents)
        {
            l_reader = new SCChangeBatchReader(super.i_ppasContext,
                                               super.i_logger,
                                               this,
                                               super.i_inQueue,
                                               super.i_params,
                                               super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving " + C_METHOD_createReader);
        }

        return l_reader;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /**
     * Creates a <code>SCChangeBatchWriter</code>.
     * @return The created SCChangeBatchWriter.
     * @throws IOException Could not create output files.
     */
    public BatchWriter createWriter() throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10200,
                            this,
                            "Enter " + C_METHOD_createWriter);
        }

        SCChangeBatchWriter l_writer = null;

        if (super.i_startComponents)
        {

            l_writer = new SCChangeBatchWriter(super.i_ppasContext,
                                               super.i_logger,
                                               this,
                                               super.i_params,
                                               super.i_outQueue,
                                               super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10210,
                            this,
                            "Leaving " + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /**
     * Creates a <code>SCChangeBatchProcessor</code>.
     * @return The created SCChangeBatchProcessor.
     */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10300,
                            this,
                            "Enter " + C_METHOD_createProcessor);
        }

        SCChangeBatchProcessor l_processor = null;

        if (super.i_startComponents)
        {

            l_processor = new SCChangeBatchProcessor(super.i_ppasContext,
                                                     super.i_logger,
                                                     this,
                                                     super.i_inQueue,
                                                     super.i_outQueue,
                                                     super.i_params,
                                                     this.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10310,
                            this,
                            "Leaving " + C_METHOD_createProcessor);
        }
        return l_processor;
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";

    /**
     * Updates the control information record for each batch process.
     */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11010,
                            this,
                            "Enter " + C_METHOD_addControlInfo);
        }
        //Array indexes for filenames
        final int L_FILE_DATE = 4; // Third element in a long date is the file date.
        // Array indexes for sequence number
        final int L_SEQ_NO = 5; // Third element in a long date is the file date.
        BatchJobData l_jobData = null;
        String l_inFileName = null;
        boolean l_fileNameValid = false;
        String l_errorMsg = C_INVALID_FILENAME;

        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);

        //Check if the file name is valid,
        l_fileNameValid = super.isFileNameValid(l_inFileName,
                                                BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_DAT,
                                                BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_IPG,
                                                BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_SCH,
                                                L_FILE_DATE,
                                                L_SEQ_NO);

        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE);
        l_jobData.setSubJobCnt("-1");
        l_jobData.setOpId(super.getSubmitterOpid());
        
        if (l_fileNameValid)
        {
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_jobData.setBatchDate(super.i_fileDate);
            l_jobData.setFileSeqNo(super.i_seqNo);  
            l_jobData.setNoOfSuccessRec("0");
            l_jobData.setNoOfRejectedRec("0"); 
        }
        else
        {
            //Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus("F");

            if (l_inFileName != null )
            {
                if (l_errorMsg.length() + l_inFileName.length() + 2 <= 30)
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }

            l_jobData.setExtraData4(l_errorMsg);

            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME
                                                        + " -- *** ERROR: "
                                                        + C_INVALID_FILENAME
                                                        + ": " + l_inFileName, 
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }

        try
        {
            super.i_batchContService.addJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                12030,
                                this,
                                C_METHOD_addControlInfo + "Error calling addJobdetails()");
            }
        }

        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            11020,
                            this,
                            "Leaving " + C_METHOD_addControlInfo);
        }
    }

    /**
     * It updates the databse with a new status when a batch have got the status finished (c), stopped (x) or
     * error (f). The metohd is called from the super class. The <code>BatchJobData</code> object used in
     * the method are first fetched form getKeyedControlRecord and then the status is inserted into that
     * object.
     * @param p_status The new status.
     */
    protected void updateStatus(String p_status)
    {
        try
        {
            PpasBatchControlService l_controlService = new PpasBatchControlService(null,
                                                                                   i_logger,
                                                                                   i_ppasContext);
            BatchJobData l_jobData = getKeyedControlRecord();
            l_jobData.setBatchJobStatus(p_status);
            l_controlService.updateJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                11021,
                                this,
                                "updateStatus - PpasServiceException caught : " + e.getMsgKey() +
                                " " + e.getMessage());
            }
        }
    }

    /**
     * This method will return a BatchJobData-object with the correct key-values set (JsJobId and
     * executionDateTime). The caller can then populate the record with data needed for the operation in
     * question. For example, the writer will get such record and add number of successfully/faulty records
     * processed and then update.
     * @return a <code>BatchJobData</code> object.
     */
    public BatchJobData getKeyedControlRecord()
    {
        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(super.i_executionDateTime.toString());
        l_jobData.setJsJobId(super.getJobId());

        return l_jobData;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }
}