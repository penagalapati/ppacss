////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SDPBalBatchControllerUT.java 
//      DATE            :       19-July-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Unit test for SDPBalBatchController.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 26/04/07 | Lars Lundberg | Method installTestSubscribers()  | PpacLon#3033/11279
//          |               | is modified:                     |
//          |               | The method                       |
//          |               | installLocalTestSubscribers(int) |
//          |               | is removed from BatchTestDataTT  |
//          |               | and replaced by a similar method |
//          |               | in BatchTestCaseTT.              |
//----------+---------------+----------------------------------+----------------
// 16/05/07 | Lars Lundberg | Several changes made to improve  | PpacLon#3038/11475
//          |               | the UT tests.                    |
//----------+---------------+----------------------------------+----------------
// 21/05/07 | Lars Lundberg | Method installTestSubscribers()  | PpacLon#3038/11532
//          |               | is modified; it now uses the     |
//          |               | installGlobalTestSubscriber(...) |
//          |               | method instead of the            |
//          |               | installLocalTestSubscriber(...)  |
//          |               | method.                          |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.RoutingData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** Unit test for SDPBalBatchController. */
public class SDPBalBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "SDPBalBatchControllerUT";

    /** The name of the Service Class Change batch. */
    private static final String C_SDP_BALANCE_BATCH_NAME    = "BatchSdpBalancing";

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "CHANGE_SDP_SUCCESSFUL.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_ERROR            = "CHANGE_SDP_ERRORS.DAT";

    /** The file sequence number template to be used for all UT test data files. */
    private static final String C_TEMPLATE_SEQ_NO           = "00000";

    /** The status indicating that the (test) file has been completely processed (with or without
     *  erroneous records). Value is {@value}. */
    private static final char  C_BATCH_JOB_STATUS_COMPLETED =
                                                           BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0);
    

    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Unit test for SDPBalBatchController.
     * @param p_name Name of the test.
     */
    public SDPBalBatchControllerUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public static method(s).                                            ==
    // =========================================================================
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(SDPBalBatchControllerUT.class);
    }


    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        junit.textui.TestRunner.run(suite());
    }
    


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSDPBalBatch_Successful =
        "testSDPBalBatch_Successful";
    /**
     * Performs a fully functional test of a successful 'Service Class change'.
     * A test file with a valid name containing 2 valid Service Class change record is
     * created and placed in the batch indata directory. The Service Class change
     * batch is started in order to process the test file and update the MSISDN table
     *  table, CUST_COMMENTS table
     *
     * @ut.when        A test file containing 2 valid Service Class change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        A new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain a
     *                 success trailing record.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testSDPBalBatch_Successful()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 1;
        final int      L_NUMBER_OF_ERRONEOUS_RECORDS  = 0;
        final String[] L_REPORT_FILE_CONTENTS         = {getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_FILE_CONTENTS       = null;

        beginOfTest(C_METHOD_testSDPBalBatch_Successful);

        SDPBalTestDataTT l_testDataTT = new SDPBalTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                                             UtilTestCaseTT.c_logger,
                                                             CommonTestCaseTT.c_ppasContext,
                                                             C_SDP_BALANCE_BATCH_NAME,
                                                             c_batchInputDataFileDir,
                                                             C_FILENAME_SUCCESSFUL,
                                                             JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                                             C_BATCH_JOB_STATUS_COMPLETED,
                                                             L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                                             L_NUMBER_OF_ERRONEOUS_RECORDS,
                                                             L_REPORT_FILE_CONTENTS,
                                                             L_RECOVERY_FILE_CONTENTS,
                                                             1);

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSDPBalBatch_MultipleErrors = "testSDPBalBatch_MultipleErrors";
    /**
     * Performs a fully functional test of a failuer 'service class change'.
     * A test file with a valid name containing 6 invalid service class change record is
     * created and placed in the batch indata directory. The service class change
     * batch is started in order to process the test file and update the MSISDN table,
     * CUST_COMMENTS table
     *
     * @ut.when        A test file containing 6 invalid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be a recovery file, 6 rows indicating errors.
     *                 A report file will be created which will contain 6 error code rows and
     *                 a trailing record saying that no successful statusChanges have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testSDPBalBatch_MultipleErrors()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 0;
        final int      L_NUMBER_OF_ERRONEOUS_RECORDS  = 6;
        final String[] L_REPORT_FILE_CONTENTS         = {"05,1,NON_NUM_MSISDN,00",//Invalid numeric field
                                                         "07,2,9999999999,00",    //Invalid record type
                                                         "14,1,9999999999,02",    //Subs. not processed on SDP
                                                         "02,1,999999999999999,000", // Invalid record length
                                                         "12,1,9999999999",          // Wrong no of fields
                                                         "06,100,3",              //Record count is incorrect
                                                         getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_FILE_CONTENTS       = {"1,SDP_BAL_BATCH_HEADER_OR_TRAILER",
                                                         "2,ERR",
                                                         "3,ERR",
                                                         "4,ERR",
                                                         "5,ERR",
                                                         "6,ERR",
                                                         "7,ERR"};

        beginOfTest(C_METHOD_testSDPBalBatch_MultipleErrors);

        SDPBalTestDataTT l_testDataTT = new SDPBalTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                                             UtilTestCaseTT.c_logger,
                                                             CommonTestCaseTT.c_ppasContext,
                                                             C_SDP_BALANCE_BATCH_NAME,
                                                             c_batchInputDataFileDir,
                                                             C_FILENAME_ERROR,
                                                             JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                                             C_BATCH_JOB_STATUS_COMPLETED,
                                                             L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                                             L_NUMBER_OF_ERRONEOUS_RECORDS,
                                                             L_REPORT_FILE_CONTENTS,
                                                             L_RECOVERY_FILE_CONTENTS,
                                                             0);
        
        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }    
    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSDPBalBatch_ErrorTrailerNotLast =
        "testSDPBalBatch_ErrorTrailerNotLast";
    /**
     * Performs a fully functional test of a failuer 'service class change'.
     * A test file with a valid name containing 6 invalid service class change record is
     * created and placed in the batch indata directory. The service class change
     * batch is started in order to process the test file and update the MSISDN table,
     * CUST_COMMENTS table
     *
     * @ut.when        A test file containing 6 invalid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be a recovery file, 6 rows indicating errors.
     *                 A report file will be created which will contain 6 error code rows and
     *                 a trailing record saying that no successful statusChanges have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testSDPBalBatch_ErrorTrailerNotLast()
    {
        final String   L_TEST_FILENAME                = "CHANGE_SDP_TRAILERERROR.DAT";
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 1;
        final int      L_NUMBER_OF_ERRONEOUS_RECORDS  = 1;
        final String[] L_REPORT_FILE_CONTENTS         = {"01,100,2", // Trailer not last record in file
                                                         getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};

        final String[] L_RECOVERY_FILE_CONTENTS       = {"1,SDP_BAL_BATCH_HEADER_OR_TRAILER",
                                                         "2,SDP_BAL_BATCH_HEADER_OR_TRAILER",
                                                         "3,OK"};
        beginOfTest(C_METHOD_testSDPBalBatch_ErrorTrailerNotLast);

        SDPBalTestDataTT l_testDataTT = new SDPBalTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                                             UtilTestCaseTT.c_logger,
                                                             CommonTestCaseTT.c_ppasContext,
                                                             C_SDP_BALANCE_BATCH_NAME,
                                                             c_batchInputDataFileDir,
                                                             L_TEST_FILENAME,
                                                             JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                                             C_BATCH_JOB_STATUS_COMPLETED,
                                                             L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                                             L_NUMBER_OF_ERRONEOUS_RECORDS,
                                                             L_REPORT_FILE_CONTENTS,
                                                             L_RECOVERY_FILE_CONTENTS,
                                                             1);
        
        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty(BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1");

    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }

    /**
     * Returns the required additional properties layers for the Service Class change batch.
     * 
     * @return the required additional properties layers for the Service Class change batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_sdp";
    }
    
    /**
     * Returns an instance of the <code>SDPBalBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>SDPBalTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController(BatchTestDataTT p_batchTestDataTT)
    {
        SDPBalTestDataTT l_testDataTT = null;
        
        l_testDataTT = (SDPBalTestDataTT)p_batchTestDataTT;
        
        return createSDPBalBatchController(l_testDataTT.getTestFilename());
    }
    
    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>StatusChangeBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>StatusChangeBatchController</code> class.
     */
    private SDPBalBatchController createSDPBalBatchController(String p_testFilename)
    {
        SDPBalBatchController     l_SDPBalBatchController = null;
        HashMap                   l_batchParams                 = null;

        l_batchParams = new HashMap();
        l_batchParams.put(BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename);
        
        try
        {
            l_SDPBalBatchController =
                new SDPBalBatchController(CommonTestCaseTT.c_ppasContext,
                                        BatchConstants.C_JOB_TYPE_BATCH_SDP_BALANCING,
                                        super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                        C_CLASS_NAME,
                                        l_batchParams);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to create a 'SDPBalBatchController' instance!");
            super.failedTestException(l_Ex);
        }

        return l_SDPBalBatchController;
    }
    
    //==========================================================================
    // Inner class(es).
    //==========================================================================
    /**
     * The purpose of this <code>SDPBalTestDataTT</code> inner class is to
     * hold and create necessary test data for each SDP Balancing batch UT test.
     */
    private class SDPBalTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "CHANGE_SDP";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^.*(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";

        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        /** The number of required test subscribers. */
        private int     i_noOfTestSubscribers  = 0;
        /** Indicates whether the ASCS database should be verified or not. */
        private boolean i_verifyAscsDatabase   = false;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>SDPBalTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest                    The <code>PpasRequest</code> object.
         * @param p_logger                         The <code>Logger</code> object.
         * @param p_ppasContext                    The <code>PpasContext</code> object.
         * @param p_batchName                      The name of the batch.
         * @param p_batchInputFileDir              The input data file directory.
         * @param p_templateTestFilename           The name of the template test file.
         * @param p_expectedJobExitStatus          The expected batch job exit status.
         * @param p_expectedBatchJobControlStatus  The expected batch job control data status.
         * @param p_expectedNoOfSuccessRecords     The expected number of successfully processed records.
         * @param p_expectedNoOfFailedRecords      The expected number of failed records.
         * @param p_expectedReportFileContents     The expected contents of the report file.
         * @param p_expectedRecoveryFileContents   The expected contents of the recovery file.
         * @param p_noOfTestSubscribers            The number of required test subscribers.
         */
        private SDPBalTestDataTT(PpasRequest p_ppasRequest,
                                 Logger      p_logger,
                                 PpasContext p_ppasContext,
                                 String      p_batchName,
                                 File        p_batchInputFileDir,
                                 String      p_templateTestFilename,
                                 int         p_expectedJobExitStatus,
                                 char        p_expectedBatchJobControlStatus,
                                 int         p_expectedNoOfSuccessRecords,
                                 int         p_expectedNoOfFailedRecords,
                                 String[]    p_expectedReportFileContents,
                                 String[]    p_expectedRecoveryFileContents,
                                 int         p_noOfTestSubscribers)
        {
            super(p_ppasRequest,
                  p_logger,
                  p_ppasContext,
                  p_batchName,
                  p_batchInputFileDir,
                  p_expectedJobExitStatus,
                  p_expectedBatchJobControlStatus,
                  p_expectedNoOfSuccessRecords,
                  p_expectedNoOfFailedRecords);

            i_templateTestFilename = p_templateTestFilename;

            // Save a consistent copy of the report file contents.
            if (p_expectedReportFileContents != null)
            {
                i_expectedReportData = new String[p_expectedReportFileContents.length];
                if (p_expectedReportFileContents.length > 0)
                {
                    System.arraycopy(p_expectedReportFileContents,
                                     0,
                                     i_expectedReportData,
                                     0,
                                     p_expectedReportFileContents.length);
                }
            }

            // Save a consistent copy of the recovery file contents.
            if (p_expectedRecoveryFileContents != null)
            {
                i_expectedRecoveryData = new String[p_expectedRecoveryFileContents.length];
                if (p_expectedRecoveryFileContents.length > 0)
                {
                    System.arraycopy(p_expectedRecoveryFileContents,
                                     0,
                                     i_expectedRecoveryData,
                                     0,
                                     p_expectedRecoveryFileContents.length);
                }
            }

            i_noOfTestSubscribers = p_noOfTestSubscribers;
            i_verifyAscsDatabase  = (p_expectedNoOfSuccessRecords > 0  &&  p_expectedNoOfFailedRecords == 0);

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile(C_REGEXP_DATA_TAG_MSISDN);
            
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile(C_PREFIX_TEST_DATA_FILENAME,
                                 BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                 i_templateTestFilename,
                                 C_CLASS_NAME);
        }

        ////////////////////////////////////////////////////////////////////////////
        // Redefined methods
        ////////////////////////////////////////////////////////////////////////////
        /**
         * Creates and returns a test data filename. This batch uses a special filename
         * style: CHANGE_SDP_sssss_tt.DAT
         * 
         * @param p_testDataFilenamePrefix  the test data filename prefix.
         * @param p_utName                  the name of the UT test (for instance "MiscDataUploadBatchUT").
         * 
         * @return the test data filename.
         */
        protected String createTestDataFilename(String p_prefix, String p_suffix, String p_utName)
            throws PpasServiceException
        {
            StringBuffer l_testDataFilename    = null;            
            String       l_fileSeqNumberStr    = null;
            String       l_fileSubSeqNumberStr = null;

            l_testDataFilename = new StringBuffer(p_prefix);
            l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
            l_fileSeqNumberStr = getFileSequenceNumber(p_utName);
            i_fileSequenceNumber = Integer.parseInt(l_fileSeqNumberStr);
            l_testDataFilename.append(l_fileSeqNumberStr);
            l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
            i_fileSubSequenceNumber = 1; // Must be < 100!
            l_fileSubSeqNumberStr = "00" + i_fileSubSequenceNumber;
            l_testDataFilename.append(l_fileSubSeqNumberStr.substring(l_fileSubSeqNumberStr.length() - 2));
            l_testDataFilename.append(p_suffix);

            return l_testDataFilename.toString();
        }
        
        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataRow  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags(String p_dataRow)
        {
            final int L_DATA_TAG_GROUP_NUMBER = 1;
            final int L_MSISDN_INDEX          = 0;

            StringBuffer l_dataRowSB       = null;
            Matcher      l_matcher         = null;
            String       l_formattedMsisdn = null;
            int          l_tagBeginIx      = 0;
            int          l_tagEndIx        = 0;

            if (p_dataRow != null)
            {
                say("start to replaceDataTags... " + p_dataRow);
                l_dataRowSB = new StringBuffer(p_dataRow);
                l_matcher   = i_msisdnDataTagPattern.matcher(p_dataRow);
                if (l_matcher.matches())
                {
                    l_tagBeginIx      = l_matcher.start(L_DATA_TAG_GROUP_NUMBER);
                    l_tagEndIx        = l_matcher.end(L_DATA_TAG_GROUP_NUMBER);
                    l_formattedMsisdn = getFormattedMsisdnSDPBal(L_MSISDN_INDEX);
                    l_dataRowSB.replace(l_tagBeginIx, l_tagEndIx, l_formattedMsisdn);
                }
            }            
            return (l_dataRowSB != null  ?  l_dataRowSB.toString() : "null");
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
            if (i_verifyAscsDatabase)
            {
                sayTime("*** Start verifying the ASCS database ***");
                PpasAccountService l_accountServ = new PpasAccountService(c_ppasRequest,
                                                                          c_logger,
                                                                          c_ppasContext);
                Msisdn l_msisdn = ((BasicAccountData)super.getSubscriberData(0)).getMsisdn();
                PpasRequest l_ppasRequest = createRequestWithMsisdn(l_msisdn);
                BasicAccountData l_bac = l_accountServ.getBasicData(l_ppasRequest, 0L, 15000L);
                sayTime("Current SDP id from CUST_MAST = '" + l_bac.getScpId() + "'");
                assertEquals("***ERROR: The current SDP id from CUST_MAST is not as expected.",
                             "01",
                             l_bac.getScpId());

                try
                {
                    RoutingData l_routingData = c_dbService.getRouting(l_msisdn);
                    sayTime("Current SDP id from MSIS_MSISDN = '" + l_routingData.getSdpId() + "'");
                    assertEquals("***ERROR: The current SDP id from MSIS_MSISDN is not as expected.",
                                 "01",
                                 l_routingData.getSdpId());
                }
                catch (PpasSqlException l_Ex)
                {
                    sayTime("***ERROR: Failed to get the routing data for the current MSISDN!");
                    failedTestException(l_Ex);
                }
                sayTime("*** ASCS database is successfully verified! ***");
            }
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
            sayTime("*** Start verifying the report file contents. *** ");
            
            Vector l_reportData = readNewFile(c_batchReportDataFileDir, i_testFilename, "DAT", "RPT");
            String[] l_reportFileContents = new String[l_reportData.size()];
            l_reportData.toArray(l_reportFileContents);

            // Verify the size of the report file contents.
            assertEquals("***ERROR: The size of the report file is not as expected.",
                         i_expectedReportData.length,
                         l_reportFileContents.length);

            // Verify the contents.
            for (int l_ix = 0; l_ix < l_reportFileContents.length; l_ix++)
            {
//                if (i_expectedNoOfSuccessRecords == 0 && (l_ix < l_reportFileContents.length - 1))
                if (l_ix < (l_reportFileContents.length - 1))
                {
                    sayTime("Verifying error code " +
                            getErrorCodeDescription(i_expectedReportData[l_ix].substring(0, 2)));
                }
                assertEquals("***ERROR: The current record in the report file is not as expected.",
                             i_expectedReportData[l_ix],
                             l_reportFileContents[l_ix]);
            }

            sayTime("*** The report file contents is successfully verified! ***");
        }
        
        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */

        protected void verifyRecoveryFile()
        {
            sayTime("*** Start verifying the recovery file contents. *** ");

            Vector l_recoveryData = null;
            try
            {
                l_recoveryData = readNewFile(c_batchRecoveryDataFileDir, i_testFilename, "DAT", "RCV");
            }
            catch (IOException l_ioEx)
            {
                if (l_ioEx instanceof FileNotFoundException)
                {
                    assertNull("***ERROR: Expected recovery contents could not be verified since " +
                               "the recovery file could no be found.",
                               i_expectedRecoveryData);
                }
                else
                {
                    sayTime("***ERROR: Failed to read the recovery file!");
                    failedTestException(l_ioEx);
                }
            }

            if (l_recoveryData != null)
            {
                String[] l_recoveryFileContents = new String[l_recoveryData.size()];
                l_recoveryData.toArray(l_recoveryFileContents);
                // Verify the size of the report file contents.
                assertEquals("***ERROR: The size of the recovery file is not as expected.",
                             i_expectedRecoveryData.length,
                             l_recoveryFileContents.length);
    
                // Verify the contents.
                for (int l_ix = 0; l_ix < l_recoveryFileContents.length; l_ix++)
                {
                    assertEquals("***ERROR: The current record in the report file is not as expected.",
                                 i_expectedRecoveryData[l_ix],
                                 l_recoveryFileContents[l_ix]);
                }
            }

            sayTime("*** The recovery file contents is successfully verified! ***");
        }

        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming()
        {
            sayTime("*** start verifyRenaming *** ");

            String l_newInputFileName = constructFileName(c_batchInputDataFileDir,
                                                          i_testFilename,
                                                          "DAT",
                                                          "DNE");
            File l_file = new File(l_newInputFileName);
            assertTrue("***ERROR: The input data file '" + i_testFilename + "' " +
                       "has not been properly renamed to '" + l_file.getName() + "'.",
                       l_file.exists());
            
            sayTime("*** end verifyRenaming ***");
        }        
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers()
        {
            for (int l_ix = 0; l_ix < i_noOfTestSubscribers; l_ix++)
            {
                addSubscriberData(installGlobalTestSubscriber(null));
            }
        }
        
        /**
         * Returns the expected batch job control data as a <code>BatchJobControlData</code> object. 
         * 
         * @param p_batchController  the currently tested <code>BatchController</code> object.
         * 
         * @return the expected batch job control data as a <code>BatchJobControlData</code> object.
         */
        protected BatchJobControlData getExpectedBatchJobControlData(BatchController p_batchController)
        {
            BatchJobControlData l_batchJobControlData = super.getExpectedBatchJobControlData(p_batchController);
            
            l_batchJobControlData.setFileDate(new PpasDate(""));
            
            return l_batchJobControlData;
        }
        
        //==========================================================================
        // Private method(s).
        //==========================================================================
        /**
         * Returns the name of the test file.
         * 
         * @return the name of the test file.
         */
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Generates and returns a daily based unique sequence number for the given sequence name as
         * a 5 digits, right adjusted, zero-filled <code>String</code>.
         * 
         * @param p_sequenceName  the sequence name for which a new sequence number should be generated.
         * 
         * @return  a daily based unique sequence number for the given sequence name as
         *          a 5 digits, right adjusted, zero-filled <code>String</code>.
         * @throws PpasServiceException  if it fails to get FileSequenceNumber.
         */
        
        private String getFileSequenceNumber(String p_sequenceName) throws PpasServiceException
        {
            PpasDailySequencesService l_dailySeqServ  = null;
            long                      l_dailySeqNo    = 0L;
            String                    l_dailySeqNoStr = null;

            l_dailySeqServ = new PpasDailySequencesService(i_ppasRequest, i_logger, i_ppasContext);
            l_dailySeqNo   = l_dailySeqServ.getNextSequenceNumber(i_ppasRequest,
                                                                  30000L,
                                                                  p_sequenceName,
                                                                  DatePatch.getDateToday());

            l_dailySeqNoStr = C_TEMPLATE_SEQ_NO + l_dailySeqNo;
            return l_dailySeqNoStr.substring(l_dailySeqNoStr.length() - C_TEMPLATE_SEQ_NO.length());
        }


        /**
         * Creates and returns a description of the given error code.
         * If the given error code is not recognized an empty description is returned.
         * 
         * @param p_errorCodeStr  the error code as a <code>String</code>.
         * @return a description of the given error code.
         */
        private String getErrorCodeDescription(String p_errorCodeStr)
        {
            int l_errorCode;
            StringBuffer l_errorCodeSB = new StringBuffer();

            try
            {
                l_errorCode = Integer.parseInt(p_errorCodeStr);
                l_errorCodeSB.append("'" + p_errorCodeStr + "' (");
            }
            catch (NumberFormatException l_Ex)
            {
                // Create and return an empty string.
                l_errorCodeSB.append("");
                l_errorCode = 999;
            }

            switch (l_errorCode)
                {
                    case 1:
                        l_errorCodeSB.append("Trailer not last record in file.");
                        break;

                    case 2:
                        l_errorCodeSB.append("Invalid record length.");
                        break;

                    case 5:
                        l_errorCodeSB.append("Invalid numeric field.");
                        break;

                    case 6:
                        l_errorCodeSB.append("Record count in trailer is incorrect.");
                        break;

                    case 7:
                        l_errorCodeSB.append("Invalid record type.");
                        break;

                    case 12:
                        l_errorCodeSB.append("Wrong number of fields in input record.");
                        break;

                    case 14:
                        l_errorCodeSB.append("Subscriber was not successfully processed on SDP.");
                        break;

                    default:
                        break;
                }

            l_errorCodeSB.append(')');
            return l_errorCodeSB.toString();
        }
    } // End of inner class 'SDPBalTestDataTT'.
}
