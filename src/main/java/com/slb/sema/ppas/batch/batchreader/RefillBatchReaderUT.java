////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RefillBatchReaderUT
//      DATE            :       17-July-2006
//      AUTHOR          :       Yang Liu
//      REFERENCE       :       PRD_ASCS00_GEN_CA_093
//
//      COPYRIGHT       :       WM-dada 2006
//
//      DESCRIPTION     :       This class tests the classes BatchReader and BatchLineFileReader
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//04/08/06 | M Erskine     | Format file correctly. Debug     | PpacLon#2479/9483
//         |               | contents and add more tests.     |
//---------+---------------+----------------------------------+-----------------
//01/11/06 | M Erskine     | Use an MsisdnFormatter.          | PpacLon#2723/10354
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.RefillBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit test class for <code>RefillBatchReader</code>
 */
public class RefillBatchReaderUT extends BatchTestCaseTT
{
    // ------------------------------------------------------------------------
    // Private constants
    // ------------------------------------------------------------------------

    /** The name of the indata file used in normal mode. */
    private static final String C_INDATA_FILENAME_NORMAL_MODE   = "BATCH_REFILL_20060725_00000.DAT";

    /** The name of the indata file used in normal mode Used in testGetRecord. */
    private static final String C_INDATA_FILENAME_NORMAL1_MODE  = "BATCH_REFILL_20060725_00001.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord2. */
    private static final String C_INDATA_FILENAME_NORMAL2_MODE  = "BATCH_REFILL_20060725_00002.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord3. */
    private static final String C_INDATA_FILENAME_NORMAL3_MODE  = "BATCH_REFILL_20060725_00003.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord4. */
    private static final String C_INDATA_FILENAME_NORMAL4_MODE  = "BATCH_REFILL_20060725_00004.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord5. */
    private static final String C_INDATA_FILENAME_NORMAL5_MODE  = "BATCH_REFILL_20060725_00005.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord6. */
    private static final String C_INDATA_FILENAME_NORMAL6_MODE  = "BATCH_REFILL_20060725_00006.DAT";

    /** The name of the indata file used in normal mode. Used in testGetRecord7. */
    private static final String C_INDATA_FILENAME_NORMAL7_MODE  = "BATCH_REFILL_20060725_00007.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord9. */
    private static final String C_INDATA_FILENAME_NORMAL9_MODE  = "BATCH_REFILL_20060725_00009.DAT";
    
    /** The name of the indata file used in normal mode. Used in testGetRecord10. */
    private static final String C_INDATA_FILENAME_NORMAL10_MODE  = "BATCH_REFILL_20060725_00010.DAT";
        
    /** The name of the indata file used in recovery mode. */
    private static final String C_INDATA_FILENAME_RECOVERY_MODE = "BATCH_REFILL_20060725_00008.IPG";

    /** The name of the in-queue. */
    private static final String C_QUEUE_NAME                    = "In-queue";

    /** The maximum size of the queue. */
    private static final long   C_MAX_QUEUE_SIZE                = 100;

    /** A reference to the <code>RefillBatchController</code> that is used in this test. */
    private static RefillBatchController c_refillBatchController  = null;
    
    /** The BasicAccountData that is created for this test. */
    private static BasicAccountData      c_basic                  = null;

    /** The old msisdn. */
    private static String                c_msisdnAsString         = null;
    
    /** A reference to the <code>RefillBatchReader</code> that is used in this test. */
    private RefillBatchReader   i_refillBatchReader               = null;

    /** The header line in a indata file. */
    private static final String C_HEADER_LINE_NORMAL                   = "000000010000000000100KWD";    
   
    /** The header line in a indata file. */
    private static final String C_HEADER_LINE_NORMAL_2_REFILLS         = "000000020000000000200EUR";
    
    /** The header line in a indata file. */
    private static final String C_HEADER_LINE_NORMAL_3_REFILLS         = "000000030000000000200EUR";
    
    /** The header line in a indata file. */
    private static final String C_HEADER_LINE_NORMAL_3_REFILLS_AMOUNT_150 = "000000030000000000150EUR";
    
    /** The header line in a indata file. */
    private static final String C_HEADER_LINE_NORMAL_5_REFILLS         = "000000050000000000200EUR";
    
    /** The total number of transactions in the detaild record is invalid. */
    private static final String C_HEADER_INVALID_TOTAL_NUMBER_REFILLS  = "Abcdefgd0000000021000EUR";

    /** The total value of transactions in the header record is invalid. */
    private static final String C_HEADER_INVALID_TOTAL_VALUE_REFILL    = "00000006asdfghkjlEdddEUR";

    /** The currency in the header record is invalid. */
    private static final String C_HEADER_INVALID_CURRENCY              = "000000060000000021000XYZ";
    
    /** The currency in the header record is empty string. */
    private static final String C_HEADER_DEFAULT_CURRENCY              = "000000010000000000100   ";  

    /** A too long header record in a indata file. */
    private static final String C_HEADER_LINE_TOO_LONG                 = "000000000000600000000210EUR";

    /** The record line in a indata file. */
    private static String C_RECORD_LINE_NORMAL; 
    
    /** The record line in a indata file. */
    private static String C_RECORD_LINE_NORMAL_AMOUNT_50; 
    
    /** The record line in a indata file. */
    private static String C_RECORD_LINE_NORMAL_REFILL_VALUE_50; 
    
    /** The record line in a indata file. */
    private static String C_RECORD_LINE_NORMAL_AMOUNT_100;         
    
    /** Wrong msisdn in a detaild record in a indata file. */
    private static final String C_RECORD_LINE_NON_NUMERIC_MSISDN       = "004600832ASDFGH000000000050A0  12345678901234567890229000";
    
    /** Wrong refill amount in a detaild record in a indata file. */
    private static  String C_RECORD_LINE_NON_NUMERIC_REFILL_AMOUNT; 
    
    /** Zero refill amount in a detaild record in a indata file. */
    private static String C_RECORD_LINE_ZERO_REFILL_AMOUNT;
    
    /** Zero refill amount in a detaild record in a indata file. */
    private static String C_RECORD_LINE_INVALID_REFILL_PROFILE;
    
    /** Wrong refill method in a detaild record in a indata file. */
    private static String C_RECORD_LINE_INVALID_REFILL_METHOD;
    
    /** The record line in a indata file. */
    private static final String C_RECORD_LINE_TOO_LONG                 = "004600832100100000000000100A0  12345678901234567890229000TOOLONG";
    
    /** The transactionId in the detailed record is empty string on thansaID. */
    private static String C_RECORD_OPTIONAL_TRANSID;   
    
    /** The PayMethod in the detailed record is empty string on Payment Method. */
    private static String C_RECORD_EMPTY_PAYMETHOD; 
    
    /** Sepcific Refill report file fields delimiter. */
    public static final String  C_DELIMITER_REPORT_FIELD = "";

    /**
     * Class constructor.
     * @param p_name the
     */
    public RefillBatchReaderUT(String p_name)
    {
        super(p_name);
    }

    /**
     * Sets up before test execution.
     */
    protected void setUp()
    {
        String       l_msisdnAsStr     = null;
        String       l_tempString      = null;
        
        super.setUp();
        
        if (c_basic == null)
        {
            c_basic = createTestSubscriber();
            snooze(5); 
        
            l_msisdnAsStr = c_ppasContext.getMsisdnFormatDb().format(c_basic.getMsisdn());
            say("l_msisdnAsString: " + l_msisdnAsStr);
        
            // Msisdn String to be right-adjusted, space-filled to be of length 15  
            l_tempString = "               " + l_msisdnAsStr;
        
            c_msisdnAsString = l_tempString.substring(l_tempString.length() - 15);
        
            say("c_msisdnAsString is: '" + c_msisdnAsString + "'");
        
            C_RECORD_LINE_NORMAL                    = c_msisdnAsString + "000000000100A   12345678901234567890229000";
        
            C_RECORD_LINE_NORMAL_AMOUNT_50          = c_msisdnAsString + "000000000050A   12345678901234567890229000";
        
            C_RECORD_LINE_NORMAL_REFILL_VALUE_50    = c_msisdnAsString + "000000000050A   12345678901234567890229000";
        
            C_RECORD_LINE_NORMAL_AMOUNT_100         = c_msisdnAsString + "000000000100A   12345678901234567890229000";
        
            C_RECORD_LINE_NON_NUMERIC_REFILL_AMOUNT = c_msisdnAsString + "00000000ASCDA   12345678901234567890229000";
        
            C_RECORD_LINE_ZERO_REFILL_AMOUNT        = c_msisdnAsString + "000000000000A   12345678901234567890229000";
        
            C_RECORD_LINE_INVALID_REFILL_PROFILE    = c_msisdnAsString + "000000000100abGT00000000000000000001229000";
        
            C_RECORD_LINE_INVALID_REFILL_METHOD     = c_msisdnAsString + "000000000100A   00000000000000000001CCc000";
        
            C_RECORD_OPTIONAL_TRANSID               = c_msisdnAsString + "000000000100A                       229000"; 
        
            C_RECORD_EMPTY_PAYMETHOD                = c_msisdnAsString + "000000000100A   00000000000000000001      ";
        }
    }

    /**
     * Tidies up after test execution.
     */
    protected void tearDown()
    {
        super.tearDown();
    }

    /**
     * Test Suite method for running the particular tests.
     * @return the test suite containing all the BatchDataRecord tests
     */
    public static Test suite()
    {
        return new TestSuite(RefillBatchReaderUT.class);
    }

    /**
     * @ut.when A valid file name is passed into the RefillBatchReader.
     * @ut.then A value of true is returned.
     */
    public void testIsFileNameValidSuccess()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL};

        this.createContext(C_INDATA_FILENAME_NORMAL_MODE, l_inputLines);

        super.beginOfTest("testIsFileNameValidSuccess");

        boolean l_valid = false;

        l_valid = i_refillBatchReader.isFileNameValid(C_INDATA_FILENAME_NORMAL_MODE);
        assertTrue(C_INDATA_FILENAME_NORMAL_MODE + " is not a valid filename ", l_valid);

        l_valid = i_refillBatchReader.isFileNameValid(C_INDATA_FILENAME_RECOVERY_MODE);
        assertTrue(C_INDATA_FILENAME_RECOVERY_MODE + " is not a valid filename ", l_valid);

        super.endOfTest();
    }

    /**
     * @ut.when The method getRecord is called on a valid header row and a valid detail record row.
     * @ut.then The expected <code>BatchRecordData</code> object is returned.
     */
    public void testGetRecordValidHeaderValidRecordSuccess()
    {        
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL};
        beginOfTest("testGetRecordValidHeaderValidRecordSuccess");
        
        BatchRecordData l_recordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL_MODE, l_inputLines);

        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Header record should not be null", l_recordData);
        say("Header: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid header record!",
                     C_HEADER_LINE_NORMAL,
                     l_recordData.getInputLine());
        
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid detail record!",
                     C_RECORD_LINE_NORMAL,
                     l_recordData.getInputLine());

        endOfTest();
    }

    /**
     * @ut.when The method getRecord is called on a valid header row and a record row with a 
     *          non-numeric refill amount.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordValidHeaderNonNumericRefillAmountInRecordFailure()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL_3_REFILLS,
                                 C_RECORD_LINE_NON_NUMERIC_MSISDN,
                                 C_RECORD_LINE_NON_NUMERIC_REFILL_AMOUNT,
                                 C_RECORD_LINE_ZERO_REFILL_AMOUNT};

        beginOfTest("testGetRecordValidHeaderNonNumericRecordFieldsFailure");
        BatchRecordData l_recordData = null;

        this.createContext(C_INDATA_FILENAME_NORMAL1_MODE, l_inputLines);

        // Test a normal header record.
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Header record should not be null", l_recordData);
        say("Header: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a normal header record!",
                     C_HEADER_LINE_NORMAL_3_REFILLS,
                     l_recordData.getInputLine());
        
        assertEquals("Failure when testing a normal hreader record!",
                     RefillBatchReader.C_ERROR_CODE_REFILL_AMOUNT_NON_NUMERIC + C_DELIMITER_REPORT_FIELD
                         + C_RECORD_LINE_NON_NUMERIC_REFILL_AMOUNT,
                     l_recordData.getErrorLine());
        
        super.endOfTest();
    }
    
    
    /**
     * @ut.when The method getRecord is called on a valid header row and a record row with a 
     *          non-numeric MSISDN.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordValidHeaderNonNumericMsisdnFailure()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL_3_REFILLS_AMOUNT_150,
                                 C_RECORD_LINE_NON_NUMERIC_MSISDN,
                                 C_RECORD_LINE_NORMAL_AMOUNT_50,
                                 C_RECORD_LINE_NORMAL_AMOUNT_50};

        beginOfTest("testGetRecordValidHeaderNonNumericMsisdnFailure");
        BatchRecordData l_recordData = null;

        this.createContext(C_INDATA_FILENAME_NORMAL1_MODE, l_inputLines);

        // Test a normal header record.
        l_recordData = i_refillBatchReader.getRecord();
    
        assertNotNull("Header record should not be null", l_recordData);
        say("Header: '" + l_recordData.dumpRecord());

        // Test when the detailed record has wrong msisdn. Error code 10.
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a detailed record with non numeric msisdn!",
                 RefillBatchReader.C_ERROR_CODE_NON_NUMERIC_MSISDN + C_DELIMITER_REPORT_FIELD
                     + l_recordData.getInputLine(),
                 l_recordData.getErrorLine());

        // Test when the detailed record has wrong refill amount. Error code 11.
        l_recordData = i_refillBatchReader.getRecord();
        
        endOfTest();
    }
      
    /**
     * @ut.when The method getRecord is called on a valid header row and a detail record row with a 
     *          zero refill amount.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordValidHeaderZeroRefillAmountFieldFailure()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL_3_REFILLS,
                                 C_RECORD_LINE_ZERO_REFILL_AMOUNT,
                                 C_RECORD_LINE_NORMAL_AMOUNT_100,
                                 C_RECORD_LINE_NORMAL_AMOUNT_100};

        beginOfTest("testGetRecordValidHeaderZeroRefillAmountFieldFailure");

        BatchRecordData l_recordData = null;

        this.createContext(C_INDATA_FILENAME_NORMAL1_MODE, l_inputLines);

        // Test a normal header record.
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Header record should not be null", l_recordData);
        say("Header: '" + l_recordData.dumpRecord());

        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a detailed record with zero refill amount!",
                     RefillBatchReader.C_ERROR_CODE_REFILL_AMOUNT_ZERO
                             + C_DELIMITER_REPORT_FIELD + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());
        
        endOfTest();
    }
    

    /**
     * @ut.when The method getRecord is called on a valid header row, a detail record row with an 
     *          invalid refill profile and a detail record row with an invalid refill method.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordValidHeaderInvalidProfileInvalidMethod()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL_2_REFILLS,
                                 C_RECORD_LINE_INVALID_REFILL_PROFILE,
                                 C_RECORD_LINE_INVALID_REFILL_METHOD};
        beginOfTest("testGetRecordValidHeaderInvalidProfileInvalidMethod");
        
        BatchRecordData l_recordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL2_MODE, l_inputLines);

        l_recordData = i_refillBatchReader.getRecord(); // Get header record line and discard
        
        assertNotNull("Header record should not be null", l_recordData);
        say("Header: '" + l_recordData.dumpRecord());

        // Test when the detailed record has wrong refill profile. Error code 07.
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail(1) record should not be null", l_recordData);
        say("Detail(1): '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a detailed record with wrong refill profile!",
                     RefillBatchReader.C_ERROR_CODE_INVALID_REFILL_PROFILE + C_DELIMITER_REPORT_FIELD
                             + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());

        // Test when the detailed record has wrong refill method. Error code 15.
        l_recordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Detail(2) record should not be null", l_recordData);
        say("Detail(2): '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a detailed record with wrong refill method!",
                     RefillBatchReader.C_ERROR_CODE_INVALID_REFILL_METHOD + C_DELIMITER_REPORT_FIELD
                             + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());

        endOfTest();
    }

    /**
     * @ut.when The method getRecord is called on a header row that is too long.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordHeaderLineTooLong()
    {
        String[] l_inputLines = {C_HEADER_LINE_TOO_LONG,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordHeaderLineTooLong");
        
        BatchRecordData l_headerRecordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL3_MODE, l_inputLines);

        // Test when the header line is too long.
        l_headerRecordData = i_refillBatchReader.getRecord();

        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error code used when the header line is too long.",
                     RefillBatchReader.C_ERROR_CODE_INVALID_HEADER_LENGTH + C_DELIMITER_REPORT_FIELD
                             + l_headerRecordData.getInputLine(),
                     l_headerRecordData.getErrorLine());

        super.endOfTest();
    }

    /** 
     * Test getRecord() when total number of refills is wrong in the header.
     * @ut.when The header row contains the wrong total number of refills.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordHeaderTotalRefillsNonNumeric()
    {
        String[] l_inputLines = {C_HEADER_INVALID_TOTAL_NUMBER_REFILLS,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordHeaderTotalRefillsNonNumeric");
        
        BatchRecordData l_headerRecordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL4_MODE, l_inputLines);
        
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error line returned when total number of refills is non-numeric in the header.",
                     RefillBatchReader.C_ERROR_CODE_TOTAL_REFILLS_NON_NUMERIC + C_DELIMITER_REPORT_FIELD
                             + l_headerRecordData.getInputLine(),
                     l_headerRecordData.getErrorLine());
        endOfTest();
    }
    
    /** 
     * Test getRecord() when total number of refills is wrong in the header.
     * @ut.when The header row contains the wrong total number of refills.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordHeaderWrongNbOfRefills()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL_REFILL_VALUE_50,
                                 C_RECORD_LINE_NORMAL_REFILL_VALUE_50};
        
        beginOfTest("testGetRecordHeaderWrongNbOfRefills");
        
        BatchRecordData l_headerRecordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL4_MODE, l_inputLines);
        
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error line returned when total number of refills is wrong in the header.",
                     RefillBatchReader.C_ERROR_CODE_HEADER_TOTAL_ROWS_MISMATCH + C_DELIMITER_REPORT_FIELD
                             + C_HEADER_LINE_NORMAL,
                     l_headerRecordData.getErrorLine());
        endOfTest();
    }

    /**
     * @ut.when The header row contains an invalid total refill value.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordHeaderInvalidRefillTotal()
    {
        String[] l_inputLines = {C_HEADER_INVALID_TOTAL_VALUE_REFILL,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordHeaderInvalidRefillTotal");
        
        BatchRecordData l_headerRecordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL5_MODE, l_inputLines);
        
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error code used when total value of refills is wrong.",
                     RefillBatchReader.C_ERROR_CODE_TOTAL_REFILL_VALUE_NON_NUMERIC + C_DELIMITER_REPORT_FIELD
                             + C_HEADER_INVALID_TOTAL_VALUE_REFILL,
                     l_headerRecordData.getErrorLine());
        endOfTest();
    }
    
    /**
     * @ut.when The header row contains the wrong total refill value.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordHeaderWrongRefillTotal()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL_5_REFILLS,
                                 C_RECORD_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordHeaderWrongRefillTotal");
        
        BatchRecordData l_headerRecordData = null;
        this.createContext(C_INDATA_FILENAME_NORMAL5_MODE, l_inputLines);
        
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error code used when total value of refills is wrong.",
                     RefillBatchReader.C_ERROR_CODE_HEADER_REFILL_AMOUNT_MISMATCH + C_DELIMITER_REPORT_FIELD
                         + C_HEADER_LINE_NORMAL_5_REFILLS,
                     l_headerRecordData.getErrorLine());
        endOfTest();
    }
    

    /**
     * @ut.when The method getRecord is called on a header row that contains an invalid currency.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordInvalidCurrencyFail()
    {
        String[] l_inputLines = {C_HEADER_INVALID_CURRENCY,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordInvalidCurrencyFail");
        
        BatchRecordData l_headerRecordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL6_MODE, l_inputLines);
        
        // Test when invalid currency in the header line.
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Wrong error code used when currency is invalid.",
                     RefillBatchReader.C_ERROR_CODE_INVALID_CURRENCY_CODE + C_DELIMITER_REPORT_FIELD
                             + l_headerRecordData.getInputLine(),
                     l_headerRecordData.getErrorLine());

        endOfTest();
    }
    
    /**
     * @ut.when The method getRecord is called on a header row that contains an emptyString currency.
     * @ut.then The expected <code>BatchRecordData</code> object is returned
     */
    public void testGetRecordDefaultCurrency()
    {
        String[] l_inputLines = {C_HEADER_DEFAULT_CURRENCY,
                                 C_RECORD_LINE_NORMAL};
        
        beginOfTest("testGetRecordDefaultCurrency");
        
        BatchRecordData l_headerRecordData = null;
        BatchRecordData l_recordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL7_MODE, l_inputLines);
        
        // Test when default currency in the header line.
        l_headerRecordData = i_refillBatchReader.getRecord();      
       
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Failure when testing a valid header record!",
                     C_HEADER_DEFAULT_CURRENCY,
                     l_headerRecordData.getInputLine());
        
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid detail record!",
                     C_RECORD_LINE_NORMAL,
                     l_recordData.getInputLine());
        endOfTest();
    }

    /**
     * @ut.when The method getRecord is called on a record row that contains an emptyString on transId.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordEmptyMethod()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_EMPTY_PAYMETHOD};
        
        beginOfTest("testGetRecordEmptyMethod");
        
        BatchRecordData l_headerRecordData = null;
        BatchRecordData l_recordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL9_MODE, l_inputLines);
        
        // Test when default currency in the header line.
        l_headerRecordData = i_refillBatchReader.getRecord();         

        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Wrong error code used when payment Method is empty.",
                     RefillBatchReader.C_ERROR_CODE_INVALID_REFILL_METHOD + C_DELIMITER_REPORT_FIELD
                             + l_recordData.getInputLine(),
                     l_recordData.getErrorLine());

        endOfTest();
    }
    
    /**
     * @ut.when The method getRecord is called on a record row that contains an emptyString on payment method.
     * @ut.then The expected <code>BatchRecordData</code> object is returned
     */
    public void testGetRecordEmptyTransId()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_OPTIONAL_TRANSID};
        
        beginOfTest("testGetRecordEmptyThansId");
        
        BatchRecordData l_headerRecordData = null;
        BatchRecordData l_recordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL10_MODE, l_inputLines);
        
        // Test when default currency in the header line.
        l_headerRecordData = i_refillBatchReader.getRecord();      
       
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        assertEquals("Failure when testing a valid header record!",
                     C_HEADER_LINE_NORMAL,
                     l_headerRecordData.getInputLine());
        
        l_recordData = i_refillBatchReader.getRecord();

        assertNotNull("Detail record should not be null", l_recordData);
        say("Detail: '" + l_recordData.dumpRecord());

        assertEquals("Failure when testing a valid detail record!",
                     C_RECORD_OPTIONAL_TRANSID,
                     l_recordData.getInputLine());
        
        endOfTest();
    }
    
    /**
     * @ut.when The method getRecord is called on a record line with an invalid record format.
     * @ut.then The correct error line is returned.
     */
    public void testGetRecordInvalidRecordFormat()
    {
        String[] l_inputLines = {C_HEADER_LINE_NORMAL,
                                 C_RECORD_LINE_TOO_LONG};
        beginOfTest("testGetRecordInvalidRecordFormat");
        
        BatchRecordData l_headerRecordData = null;
        BatchRecordData l_detailRecordData = null;
        
        this.createContext(C_INDATA_FILENAME_NORMAL6_MODE, l_inputLines);
        
        // Discard
        l_headerRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Header record should not be null", l_headerRecordData);
        say("Header: '" + l_headerRecordData.dumpRecord());

        l_detailRecordData = i_refillBatchReader.getRecord();
        
        assertNotNull("Detail record should not be null", l_detailRecordData);
        say("Detail: '" + l_detailRecordData.dumpRecord());

        assertEquals("Wrong error code used when currency is invalid.",
                     RefillBatchReader.C_ERROR_CODE_INVALID_RECORD_FORMAT + C_DELIMITER_REPORT_FIELD
                         + l_detailRecordData.getInputLine(),
                     l_detailRecordData.getErrorLine());

        endOfTest();
    }
    
    //TODO: Test multiple record defects in the same file. 
//    String[] l_inputLines = {C_HEADER_LINE_NORMAL_3_REFILLS,
//            C_RECORD_LINE_NON_NUMERIC_MSISDN,
//            C_RECORD_LINE_NON_NUMERIC_REFILL_AMOUNT,
//            C_RECORD_LINE_ZERO_REFILL_AMOUNT};

    /**
     * Creates this tests context.
     * @param p_fileName The name of the input file to test.
     * @param p_linearray An array with the lines that will be in the file <code>p_fileName</code>.
     */
    private void createContext(String p_fileName, String[] p_linearray)
    {
        StringBuffer l_tmpFullPathName = null;
        Hashtable l_parameters = new Hashtable();
        SizedQueue l_queue = null;
        String l_inputFileDirectory = null;
        String l_fullPathName = null;
        String l_fileName = null;
        
        String[] l_fileArray = p_fileName.split("[.]");

        l_fileName = l_fileArray[0] + ".IPG";

        l_inputFileDirectory = c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);

        // Delete the recovery file (.IPG) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(l_fileName);
        l_fullPathName = l_tmpFullPathName.toString();
        super.deleteFile(l_fullPathName);

        // Create a input file (.DAT) in the directory.
        l_tmpFullPathName = new StringBuffer();
        l_tmpFullPathName.append(l_inputFileDirectory);
        l_tmpFullPathName.append("/");
        l_tmpFullPathName.append(p_fileName);
        l_fullPathName = l_tmpFullPathName.toString();

        super.createNewFile(l_fullPathName, p_linearray);

        l_parameters.put(BatchConstants.C_KEY_INPUT_FILENAME, p_fileName);

        try
        {
            l_queue = new SizedQueue(C_QUEUE_NAME, C_MAX_QUEUE_SIZE, null);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }

        try
        {
            c_refillBatchController = new RefillBatchController(super.c_ppasContext,
                                                                  "batchRefill",
                                                                  super.getJsJobID(),
                                                                  "RefillBatchReaderUT",
                                                                  l_parameters);
        }
        catch (PpasException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        
        i_refillBatchReader = new RefillBatchReader(c_ppasContext,
                                                    c_logger,
                                                    c_refillBatchController,
                                                    l_queue,
                                                    l_parameters,
                                                    c_properties);

        assertNotNull("Failed to create a RefillBatchReader instance.", i_refillBatchReader);
    }

    /**
     * Main method entry point for JUnit.
     * @param p_args the argument list
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + (p_args.length == 0 ? "" : "ignored"));
        TestRunner.run(suite());
    }
}
