////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchProcessorUT.java 
//      DATE            :       Jun 24, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.SCChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * Unit test for SCChangeBatchProcessor.
 * To run a unit test of a processor the processes must
 * be started and the test can only run on unix.
 */
public class SCChangeBatchProcessorUT extends BatchTestCaseTT
{
     
    /** Instance of SCChangeBatchProcessor. */
    private static SCChangeBatchProcessor c_scChangeBatchProcessor = null;
    
    /** The BasicAccountData that is created for this test. */
    private static BasicAccountData        c_basic                        = null;
    
    /** The old msisdn. */
    private static Msisdn                  c_msisdn                    = null;

    /** The old msisdn used to test when the msisdn not is installed. */
    private static Msisdn                  c_msisdnNotInstalled        = null;

    
    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public SCChangeBatchProcessorUT(String p_title)
    {
        super(p_title);
        createContext();
    }
    
    /** Test the method ProcessRecord(). */
    public void testProcessRecord()
    {
        //Since this test needs that the processes are started I haven't finished
        //the coding.
        SCChangeBatchRecordData l_record = new SCChangeBatchRecordData();
        
        l_record.setOldServiceClass(new ServiceClass(1));
        l_record.setNewServiceClass(new ServiceClass(2));
        l_record.setMsisdn(c_msisdn);
        
        try
        {
            //Test when the msisdn isn't installed.
            l_record.setOldServiceClass(new ServiceClass(1));
            l_record.setNewServiceClass(new ServiceClass(2));
            l_record.setMsisdn(c_msisdnNotInstalled);
            
            c_scChangeBatchProcessor.processRecord(l_record);
            
            //Test when the msisdn is invalid.
            l_record.setOldServiceClass(new ServiceClass(1));
            l_record.setNewServiceClass(new ServiceClass(2));
            l_record.setMsisdn(c_msisdnNotInstalled);
            
            c_scChangeBatchProcessor.processRecord(l_record);
            
            //Test when the old service class is invalid.
            l_record.setOldServiceClass(new ServiceClass(99));
            l_record.setNewServiceClass(new ServiceClass(2));
            l_record.setMsisdn(c_msisdnNotInstalled);
            
            c_scChangeBatchProcessor.processRecord(l_record);
            
            //Test when the new service class is invalid.
            l_record.setOldServiceClass(new ServiceClass(1));
            l_record.setNewServiceClass(new ServiceClass(99));
            l_record.setMsisdn(c_msisdnNotInstalled);
            
            c_scChangeBatchProcessor.processRecord(l_record);
            
            
        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }

    }
    
    /**Creates the context need for this test. */
    private void createContext()
    {
        Hashtable l_params          = new Hashtable();
        SizedQueue l_inQueue        = null;
        SizedQueue l_outQueue       = null;
         
        c_scChangeBatchProcessor = new SCChangeBatchProcessor(super.c_ppasContext, 
                                                              super.c_logger,
                                                              null,
                                                              l_inQueue,
                                                              l_outQueue,
                                                              l_params,
                                                              super.c_properties);
        try
        {
            super.c_ppasRequest.setContext(super.c_ppasContext);
            
            if (c_basic == null)
            {
                c_basic = super.c_dbService.installTestSubscriber(null);
            }
            
            c_msisdn = c_basic.getMsisdn();
            c_msisdnNotInstalled  = c_dbService.getNextFreeMsisdn(c_basic.getMarket(),
                                                                  DbServiceTT.C_DEFAULT_SDP);
        }
        catch (PpasServiceException e)
        {
            super.failedTestException(e);
        }
        catch (PpasSqlException e)
        {
            super.failedTestException(e);
        }       
    }
        
    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /** Define test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(SCChangeBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        TestRunner.run(suite());
    }

} 
////////////////////////////////////////////////////////////////////////////////
//                         End of file
////////////////////////////////////////////////////////////////////////////////}
