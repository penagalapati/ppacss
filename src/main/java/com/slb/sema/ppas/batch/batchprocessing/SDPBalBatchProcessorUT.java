////////////////////////////////////////////////////////////////////////////////
//    ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       SDPBalBatchProcessorUT.java
//    DATE            :       19/07/2004
//    AUTHOR          :       Emmanuel-Pierre Hebe
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       Atos Origin 2004
//
//    DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit Test for the 'SDPBalBatchProcessor' class.
 */
public class SDPBalBatchProcessorUT extends BatchTestCaseTT
{
    //------------------------------------------------------------------------
    // Private constants
    //------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SDPBalBatchProcessorUT";

    //------------------------------------------------------------------------
    // Constructors
    //------------------------------------------------------------------------
    /** Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public SDPBalBatchProcessorUT(String p_title)
    {
        super(p_title, "batch_sdp");
    }


    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testConstructor = "testConstructor";
    /**
     * Test to construct a <code>SDPBalBatchProcessor</code> instance.
     */
    public void testConstructor()
    {
        SDPBalBatchProcessor l_sdpBalBatchProcessor = null;
        SizedQueue           l_inQueue              = null;
        SizedQueue           l_outQueue             = null;

        try
        {
            l_inQueue  = new SizedQueue( "SDPBalBatchProcessorUT indata queue", 10, null );
            l_outQueue = new SizedQueue( "SDPBalBatchProcessorUT output queue", 10, null );
        }
        catch( SizedQueueInvalidParameterException p_sqInvParamExe )
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        l_sdpBalBatchProcessor = new SDPBalBatchProcessor( super.c_ppasContext, 
                                                           super.c_logger,
                                                           null,
                                                           l_inQueue,
                                                           l_outQueue,
                                                           null,
                                                           super.c_properties );
        
        assertNotNull("Failed to create a SDPBalBatchProcessor instance.", l_sdpBalBatchProcessor);

        System.out.println(C_CLASS_NAME + C_METHOD_testConstructor + "-- Completed.");
    }



    //------------------------------------------------------------------------
    // Public static methods
    //------------------------------------------------------------------------
    /** Define test suite. This unit test uses a standard JUnit method to derive a list
     * of test cases from the class.
     * 
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(SDPBalBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }

} // End of public class SDPBalBatchProcessorUT
