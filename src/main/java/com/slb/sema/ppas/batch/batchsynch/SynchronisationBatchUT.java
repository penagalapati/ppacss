////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SynchronisationBatchUT.java
//      DATE            :       23-Jul-2004
//      AUTHOR          :       Alan Barrington-Hughes
//      REFERENCE       :       PRD_ASCS00_DEV_SS_83
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       JUnit Test for the Synchronisation batch.     
//
////////////////////////////////////////////////////////////////////////////////
//                          CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME       | DESCRIPTION                     | REFERENCE
//---------+------------+---------------------------------+---------------------
//04/08/06 | Chris      | Mods required since existing    | PpacLon#2329/9464
//         | Harrison   | config data is no longer        | PRD_ASCS_GEN_CA_096
//         |            | truncated before batch loading. |
//         |            | Instead existing config is      |
//         |            | Updated or marked as Deleted &  |
//         |            | new config Inserted.            |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchsynch;

import java.util.HashMap;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.js.jscommon.JobStatus;
/**
* Class to test the Synchronization of Recharge and Refill class.
*/
public class SynchronisationBatchUT extends BatchTestCaseTT
{
    /** Test tool. */
    private DbServiceTT i_dbServiceTT;
    
    /** Identifier of tables used to hold preserved data. */
    private String i_copyId;

    /** Tables affected by this process. */
    private static final String[] C_TABLES = new String[] {"prpl_promo_plan", "vosp_voucher_splits",
            "vosd_voucher_split_divisions", "prom_promotion", "papr_payment_profile"};

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Required constructor for JUnit testcase.
     * Any subclass of TestCase must implement a constructor
     * that takes a test case name as it's argument
     *
     * @param   p_title The testcase name.
     */
    public SynchronisationBatchUT (String p_title)
    {
        super(p_title);

        i_dbServiceTT = new DbServiceTT(c_ppasRequest, c_logger);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        TestRunner.run(suite());
    }

    /** Static method that allows the framework to
     * to automatically run all the tests in the class. A program
     * provided by the JUnit framework can traverse a list of TestCase classes
     * calling this suite method and get an instance of the TestCase which it then
     * executes. See the main method in class for an example.
     * @return a suite of tests to execute
     */
    public static Test suite()
    {
        return new TestSuite(SynchronisationBatchUT.class);
    }

    /** This method is called before each test is run. */
    protected void setUp()
    {
        super.setUp();
        
        copyData(C_TABLES);
    }

    /** This method is called after each test is run. */
    protected void tearDown()
    {
        replaceData(C_TABLES);
        
        super.tearDown();
    }
    
    /** Copy data from tables that will be changed as part of this test.
     * 
     * @param p_tables List of tables to be copied.
     */
    private void copyData(String[] p_tables)
    {
        i_copyId = Long.toString(System.currentTimeMillis());
        
        for (int i = 0; i < p_tables.length; i++)
        {
            String l_table = p_tables[i].substring(0, 4) + "_" + i_copyId;

            say("Copying " + p_tables[i] + " to " + l_table);
            
            try
            {
                execSqlUpdate(new SqlString(100, 0,
                                            "create table " + l_table + " as select * from " +
                                            p_tables[i]));
            }
            catch (PpasSqlException e)
            {
                failedTestException(e);
            }

            assertEquals("Wrong row count in " + l_table, countRowsInTable(p_tables[i]),
                                                          countRowsInTable(l_table));
        }
    }

    /** Replace copied data back into the real tables.
     * 
     * @param p_tables List of tables to be replaced.
     */
    private void replaceData(String[] p_tables)
    {
        for (int i = 0; i < p_tables.length; i++)
        {
            String l_table = p_tables[i].substring(0, 4) + "_" + i_copyId;

            say("Replacing " + p_tables[i] + " from " + l_table);
            
            try
            {
                execSqlUpdate(new SqlString(100, 0, "delete from " + p_tables[i]));
                execSqlUpdate(new SqlString(100, 0,
                                            "insert into " + p_tables[i] + " select * from " + l_table));
                execSqlUpdate(new SqlString(100, 0, "drop table " + l_table));
            }
            catch (PpasSqlException e)
            {
                failedTestException(e);
            }
        }
    }
    
    /** Check a specified table contains the expected number of rows for the <code>Batch</code>.
     * This method also checks that the total number of rows are the same as the expected number. This
     * ensures that there are no additional rows in the table.
     * 
     * @param p_table    Name of table to check.
     * @param p_expected Expected number of rows.
     */
    private void checkRowCountBatch(String p_table, int p_expected)
    {
        say("Dump of table: " + p_table);
        try
        {
            say(i_dbServiceTT.dumpTable("select * from " + p_table, new Object[]{}));
        }
        catch (PpasServiceException e)
        {
            failedTestException(e);
        }

        String l_prefix = p_table.substring(0, 4);
        
        assertEquals("Wrong row count in " + l_prefix, p_expected, countRowsInTable(p_table));
    }

    /** @ut.when Load synchronisation files with records containing very long descriptions
     *  that have to be truncated before loading into the db.
     *  @ut.attributes +f
     */
    public void testDescriptions()
    {
        /* Load long descriptions
         */
        /* Prior to the batch run, current test/default static data has:
         * 
         * 7 prpl rows
         * 5 vosp rows
         * 0 vosd rows
         * 7 prom rows
         * 17 papr rows
         * 
         * IF someone changes these counts then these tests may fail. 
         */
        say("Dump static base data");
        checkRowCountBatch("prpl_promo_plan",      7);
        checkRowCountBatch("vosp_voucher_splits",  5);
        checkRowCountBatch("vosd_voucher_split_divisions",  0);
        checkRowCountBatch("prom_promotion",       7);
        checkRowCountBatch("papr_payment_profile", 17);
        
        say("Run Batch Sync using long descriptions");
        performSyncFromFile("AIRDATASYNC_20040715_00007.XML");

        say("Check long descriptions result rows");

        // 10 = 3 Batch Inserts + 3 Batch Deletes. 4 out-of-date prpl's are untouched. 
        checkRowCountBatch("prpl_promo_plan",      10);
        
        // 7  = 2 Inserts + 5 existing untouched
        // Note: the AIR XML service class for division 23 is a wildcard so vosp rows
        // for all Service Classes are generated for division 23
        // Note: vosp/vosd start & end dates don't exist in the AIR XML file
        // so they are defaulted to 1-jan-1970 & 31-dec-2070 respectively.
        checkRowCountBatch("vosp_voucher_splits",  8);

        // 14  = 14 inserts (0 rows originally which is not realistic, there should be vosp rows, but...)
        checkRowCountBatch("vosd_voucher_split_divisions",  16);

        // 11 = 4 Inserts, 7 existing untouched
        checkRowCountBatch("prom_promotion",       11);

        // 19 = 2 Batch Inserts + 17 Batch Updates.
        checkRowCountBatch("papr_payment_profile", 19);
    }
    
    /** @ut.when Load synchronisation files with various names.
     *  @ut.attributes +f
     */
    public void testFilenames()
    {
        /* Load a long filename
         */
        /* Prior to the batch run, current test/default static data has:
         * 
         * 7 prpl rows
         * 5 vosp rows
         * 0 vosd rows
         * 7 prom rows
         * 17 papr rows
         * 
         * IF someone changes these counts then these tests may fail. 
         */
        say("Dump static base data");
        checkRowCountBatch("prpl_promo_plan",      7);
        checkRowCountBatch("vosp_voucher_splits",  5);
        checkRowCountBatch("vosd_voucher_split_divisions",  0);
        checkRowCountBatch("prom_promotion",       7);
        checkRowCountBatch("papr_payment_profile", 17);
        
        say("Run first Batch Sync using long filename");
        performSyncFromFile("AIRDATASYNC_20040715_00006.XML");

        say("Check long filename result rows");

        // 10 = 3 Batch Inserts + 3 Batch Deletes. 4 out-of-date prpl's are untouched. 
        checkRowCountBatch("prpl_promo_plan",      10);
        
        // 7  = 2 Inserts + 5 existing untouched
        // Note: the AIR XML service class for division 23 is a wildcard so vosp rows
        // for all Service Classes are generated for division 23
        // Note: vosp/vosd start & end dates don't exist in the AIR XML file
        // so they are defaulted to 1-jan-1970 & 31-dec-2070 respectively.
        checkRowCountBatch("vosp_voucher_splits",  8);

        // 14  = 14 inserts (0 rows originally which is not realistic, there should be vosp rows, but...)
        checkRowCountBatch("vosd_voucher_split_divisions",  16);

        // 11 = 4 Inserts, 7 existing untouched
        checkRowCountBatch("prom_promotion",       11);

        // 19 = 2 Batch Inserts + 17 Batch Updates.
        checkRowCountBatch("papr_payment_profile", 19);


        /* Load a short filename
         *
         * Note: the results of the previous Batch Sync still exist in the db.
         */
        say("Run second Batch Sync using short filename");
        performSyncFromFile("AirDataSync.xml");

        say("Check short filename result rows");
        checkRowCountBatch("prpl_promo_plan",      10);
        checkRowCountBatch("vosp_voucher_splits",  8);
        checkRowCountBatch("vosd_voucher_split_divisions",  16);
        checkRowCountBatch("prom_promotion",       11);
        checkRowCountBatch("papr_payment_profile", 19);
    }
    
    /** Synchronise data using a specified file name.
     *  @param p_fileName Name of file containing data.
     */
    private void performSyncFromFile(String p_fileName)
    {
        beginOfTest("Load from " + p_fileName);

        HashMap l_map = new HashMap();

        l_map.put(BatchConstants.C_KEY_INPUT_FILENAME, p_fileName);
        l_map.put(BatchConstants.C_KEY_BATCH_JOB_TYPE, BatchConstants.C_JOB_TYPE_BATCH_SYNCH);
        
        SynchronisationBatch l_synch = null;
        try
        {
            l_synch = new SynchronisationBatch(
                     c_ppasContext,
                     BatchConstants.C_JOB_TYPE_BATCH_SYNCH,
                     getJsJobID(),
                     "JS_RUN_A1001",
                     l_map);
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        catch (PpasConfigException e)
        {
            failedTestException(e);
        }
        
        //disable reload of business config cache.
        l_synch.setBusinessConfigReload(false);
        
        l_synch.doJobProcessing();         

        assertEquals("Job has not finish", JobStatus.C_STATUS_FINISHED, l_synch.getStatus());        
        assertEquals("Unexpected job status", JobStatus.C_JOB_EXIT_STATUS_SUCCESS, l_synch.getExitStatus());

        endOfTest();
    }
}
